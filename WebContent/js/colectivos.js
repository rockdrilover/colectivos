var requerido = [];
var nombre = "";

function imprimir() {
 	if (window.print) {
		alert("Asegurate que tu impresora esta configurada correctamente.");
    	window.print();
	} else {
    	alert("Tu navegador no soporta la impresion.");
	}
}

function verDetalle(opcion, nRegistro, nTamano){
	switch (opcion) {
		case 1:
			for(i = 0; i < nTamano; i++){				
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colCoberturas", 0);
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colComponentes", 0);
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colProdPlan", 0);
			}
			ocultaSeccion("frmConsultaTarifas:listaTarifas:" + nRegistro + ":colCoberturas", 1);
			break;
	  
		case 2:
			for(i = 0; i < nTamano; i++){				
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colCoberturas", 0);
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colComponentes", 0);
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colProdPlan", 0);
			}
			ocultaSeccion("frmConsultaTarifas:listaTarifas:" + nRegistro + ":colComponentes", 1);
			break;
		
		case 3:		
			for(i = 0; i < nTamano; i++){				
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colCoberturas", 0);
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colComponentes", 0);
				ocultaSeccion("frmConsultaTarifas:listaTarifas:" + i + ":colProdPlan", 0);
			}
			ocultaSeccion("frmConsultaTarifas:listaTarifas:" + nRegistro + ":colProdPlan", 1);
			break; 
	}
}


function verCteCert(opcion, nRegistro, nTamCte, nTamCert ){
	for(i = 0; i < nTamCte; i++){                  
        ocultaSeccion("frmconscred:listaCreditos:" + i + ":colClientes", 0);
        ocultaSeccion("frmconscred:listaCreditos:" + i + ":colCertificados", 0);
        for(j = 0; j < nTamCert; j++){                 
    		ocultaSeccion("frmconscred:listaCreditos:" + i + ":listaCertificados:" + j + ":colCoberturas", 0);
            ocultaSeccion("frmconscred:listaCreditos:" + i + ":listaCertificados:" + j + ":colAsistencias", 0);    
            ocultaSeccion("frmconscred:listaCreditos:" + i + ":listaCertificados:" + j + ":colComplemento", 0);  
            ocultaSeccion("frmconscred:listaCreditos:" + i + ":listaCertificados:" + j + ":colAsegurados", 0); 
        }
        ocultaSeccion("trDetalleCert_"  + i, 0);
	}   
	
	switch (opcion) {
    	case 1:
    		ocultaSeccion("frmconscred:listaCreditos:" + nRegistro + ":colClientes", 1);
            break;
      
    	case 2:
    		ocultaSeccion("frmconscred:listaCreditos:" + nRegistro + ":colCertificados", 1);
            break;
    }
}


function verDetalleCert(nRegistro, nTamCert, nColPrinc){
	verCobAsis(0, 0, nTamCert, nColPrinc);
	for(j = 0; j < nTamCert; j++){                  
		ocultaSeccion("trDetalleCert_"  + j, 0);
	}
	ocultaSeccion("trDetalleCert_"  + nRegistro, 1);
}

function verCobAsis(opcion, nRegistro, nTamano, nColPrinc){
	for(i = 0; i < nTamano; i++){                                    
		ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + i + ":colCoberturas", 0);
        ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + i + ":colAsistencias", 0);
        ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + i + ":colComplemento", 0);
        ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + i + ":colAsegurados", 0);
        ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + i + ":colBeneficiarios", 0);
        ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + i + ":colEndosos", 0);
    }
	switch (opcion) {
    	case 21:           
    		ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + nRegistro + ":colCoberturas", 1);
            break;
                
    	case 22:  
    		ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + nRegistro + ":colAsistencias", 1);
            break; 
            
    	case 23:  
    		ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + nRegistro + ":colComplemento", 1);
            break;
            
    	case 24:  
    		ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + nRegistro + ":colAsegurados", 1);
            break;
    	
    	case 25:  
    		ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + nRegistro + ":colBeneficiarios", 1);
            break;
            
    	case 26:  
    		ocultaSeccion("frmconscred:listaCreditos:" + nColPrinc + ":listaCertificados:" + nRegistro + ":colEndosos", 1);
            break;
    }
}

/* Funciones Isaac */
function exibirPanel(opcion){
	switch (opcion) {
		case 1:
		case 2:
	    case 6:
		case 7:
			ocultaSeccion("trVentas", 1);
			ocultaSeccion("trRadiosLCI", 0);
			ocultaSeccion("trRadiosHP", 0);
			ocultaSeccion("trCuentas", 0);
			ocultaSeccion("trBT", 0);
			break;
			
		case 8:
			ocultaSeccion("trVentas", 0);
			ocultaSeccion("trRadiosLCI", 0);
			ocultaSeccion("trRadiosHP", 0);
			ocultaSeccion("trCuentas", 0);
			ocultaSeccion("trResultados", 0);
			ocultaSeccion("trBT", 1);
			break;
	  
		case 3:
			ocultaSeccion("trVentas", 0);
			ocultaSeccion("trRadiosLCI", 1);
			ocultaSeccion("trRadiosHP", 0);
			ocultaSeccion("trCuentas", 0);
			ocultaSeccion("trResultados", 0);
			ocultaSeccion("trBT", 0);
			break;
			
		case 4:
			ocultaSeccion("trVentas", 0);
			ocultaSeccion("trRadiosLCI", 0);
			ocultaSeccion("trRadiosHP", 0);
			ocultaSeccion("trCuentas", 1);
			ocultaSeccion("trResultados", 0);
			ocultaSeccion("trBT", 0);
			break;
		
		case 5:
			ocultaSeccion("trVentas", 0);
			ocultaSeccion("trRadiosLCI", 0);
			ocultaSeccion("trRadiosHP", 1);	
			ocultaSeccion("trCuentas", 0);
			ocultaSeccion("trResultados", 0);
			ocultaSeccion("trBT", 0);
			break;
		
		case 10:			
			ocultaSeccion("trVentas", 0);
			ocultaSeccion("trRadiosLCI", 0);
			ocultaSeccion("trRadiosHP", 0);
			ocultaSeccion("trCuentas", 0);
			ocultaSeccion("trResultados", 1);
			ocultaSeccion("trBT", 0);
			break;
		
		default:		  
			ocultaSeccion("trVentas", 0);
			ocultaSeccion("trRadiosLCI", 0);
			ocultaSeccion("trRadiosHP", 0);
			ocultaSeccion("trCuentas", 0);
			ocultaSeccion("trResultados", 0);
			ocultaSeccion("trBT", 0);
			break;
		
		
	}
	
}


function showPanel(opcion){		
	if(opcion == 3){		
		opcion = '3';
	}
	
	switch (opcion) {
		case '1':				
			ocultaSeccion("taCargaManual", 1);	
			ocultaSeccion("taCargaRamos", 1);
			ocultaSeccion("taArchivos", 1);			
			ocultaSeccion("trResultados", 0);
			break;
			
		case '2':			
			ocultaSeccion("taCargaManual", 0);	
			ocultaSeccion("taCargaRamos", 0);
			ocultaSeccion("taArchivos", 1);			
			ocultaSeccion("trResultados", 0);
			break;
			
		case '3':						
			ocultaSeccion("taCargaManual", 0);	
			ocultaSeccion("taCargaRamos", 0);
			ocultaSeccion("taArchivos", 0);
			ocultaSeccion("trResultados", 1);
			break;
			
	}
	
}

function muestraOcultaSeccion(nombre_seccion, opcion) {
	idImgO 	= 'img_sec_' + nombre_seccion + '_o';
	idImgM 	= 'img_sec_' + nombre_seccion + '_m';
	trSec	= 'tr_sec_' + nombre_seccion;
	
	if (opcion == 1) { // Mostrar Capas
		ocultaSeccion(idImgM, 1);
		ocultaSeccion(idImgO, 0);
		ocultaSeccion(trSec, 1);
	} else {	// OcultarCapas
		ocultaSeccion(idImgM, 0);
		ocultaSeccion(idImgO, 1);
		ocultaSeccion(trSec, 0);
	}
}

function ocultaSeccion(id, valor){	
	if(document.getElementById(id)){
		if(valor == '1') { //Muestra Seccion
			document.getElementById(id).style.display = "";
		} else if(valor == '0') { // Oculta Seccion
			document.getElementById(id).style.display = "none";	
		}
	}
}

function exibirBeneficiarios(seccion, opcion){
	if(opcion == '1') {
		if(seccion == '1'){
			ocultaSeccion("panelBene1", 1);
			ocultaSeccion("img_sec_panelBene1_o", 1);
			ocultaSeccion("img_sec_panelBene1_m", 0);	
		}else {
			ocultaSeccion("panelBene1", 0);
			ocultaSeccion("img_sec_panelBene1_o", 0);
			ocultaSeccion("img_sec_panelBene1_m", 1);	
		}
	}
	if(opcion == '2') {
		if(seccion == '1'){
			ocultaSeccion("panelBene2", 1);
			ocultaSeccion("img_sec_panelBene2_o", 1);
			ocultaSeccion("img_sec_panelBene2_m", 0);	
		}else {
			ocultaSeccion("panelBene2", 0);
			ocultaSeccion("img_sec_panelBene2_o", 0);
			ocultaSeccion("img_sec_panelBene2_m", 1);	
		}
	}
	if(opcion == '3') {
		if(seccion == '1'){
			ocultaSeccion("panelBene3", 1);
			ocultaSeccion("img_sec_panelBene3_o", 1);
			ocultaSeccion("img_sec_panelBene3_m", 0);	
		}else {
			ocultaSeccion("panelBene3", 0);
			ocultaSeccion("img_sec_panelBene3_o", 0);
			ocultaSeccion("img_sec_panelBene3_m", 1);	
		}
	}
	if(opcion == '4') {
		if(seccion == '1'){
			ocultaSeccion("panelBene4", 1);
			ocultaSeccion("img_sec_panelBene4_o", 1);
			ocultaSeccion("img_sec_panelBene4_m", 0);	
		}else {
			ocultaSeccion("panelBene4", 0);
			ocultaSeccion("img_sec_panelBene4_o", 0);
			ocultaSeccion("img_sec_panelBene4_m", 1);	
		}
	}
	if(opcion == '5') {
		if(seccion == '1'){
			ocultaSeccion("panelBene5", 1);
			ocultaSeccion("img_sec_panelBene5_o", 1);
			ocultaSeccion("img_sec_panelBene5_m", 0);	
		}else {
			ocultaSeccion("panelBene5", 0);
			ocultaSeccion("img_sec_panelBene5_o", 0);
			ocultaSeccion("img_sec_panelBene5_m", 1);	
		}
	}
}


function exibirPanel5758(opcion){
	if(opcion == '1') { //Muestra Seccion para Facturar
		ocultaSeccion("trFacturar", 1);
		ocultaSeccion("trRenovar", 0);
	} else if(opcion == '2') { // Muestra Seccion para Renovar
		ocultaSeccion("trFacturar", 0);
		ocultaSeccion("trRenovar", 1);	
	} else {
		ocultaSeccion("trFacturar", 0);
		ocultaSeccion("trRenovar", 0);
	}
}

function exibirPanelReportes(opcion, nCantidad){
	for(i = 1; i <= nCantidad; i++){               		
		ocultaSeccion("trReportes_" + i, 0);
    }
	if(opcion != 0) { 
		ocultaSeccion("trReportes_" + opcion, 1);
	}
}

function exibirPanelesTareas(opcion, tarea){
	//alert(opcion + " + " + tarea);
	if(opcion == '1') {
		if(tarea == '1' || tarea == '2' || tarea == '3' || tarea == '4' ){
			ocultaSeccion("panel1", 1);
			ocultaSeccion("panel2", 0);
		} else if(tarea == 5){
			ocultaSeccion("panel1", 0);
			ocultaSeccion("panel2", 1);
		}
		ocultaSeccion("ocultas", 1);
		ocultaSeccion("muestras", 0);
		ocultaSeccion("panel3", 0);
	}else if(opcion == '2') {
		ocultaSeccion("ocultas", 0);
		ocultaSeccion("muestras", 1);
		ocultaSeccion("panel1", 0);
		ocultaSeccion("panel2", 0);
		ocultaSeccion("panel3", 0);
		if(tarea == 7){
			ocultaSeccion("panel3", 1);
			ocultaSeccion("ocultas", 1);
			ocultaSeccion("muestras", 0);
		}
	}
}

function exibirPanelesFlujo(opcion, tarea){
	//alert(opcion + " + " + tarea);
	if(opcion == '1') {
		if(tarea == '1'){
			ocultaSeccion("panel1", 1);
			ocultaSeccion("panel2", 0);
		} else if(tarea == '2'){
			ocultaSeccion("panel2", 1);
			ocultaSeccion("panel1", 0);
		}
		ocultaSeccion("ocultas", 1);
		ocultaSeccion("muestras", 0);
	}else if(opcion == '2') {
		ocultaSeccion("ocultas", 0);
		ocultaSeccion("muestras", 1);
		ocultaSeccion("panel1", 0);
		ocultaSeccion("panel2", 0);
	}
}

function exibirPanelesImpresion(opcion){
//	alert(opcion);
	if(opcion == '1') {
		ocultaSeccion("panel1", 1);
		ocultaSeccion("panelAgrega", 0);
		ocultaSeccion("panelAgrega2", 0);
		ocultaSeccion("panel2", 0);
		ocultaSeccion("panelArchivo", 0);
	}else if(opcion == '0') {
		ocultaSeccion("panel1", 0);
		ocultaSeccion("panelAgrega", 0);
		ocultaSeccion("panelAgrega2", 0);
		ocultaSeccion("panel2", 0);
		ocultaSeccion("panelArchivo", 0);
		document.getElementById("frmImpre:inPoliza").value = '-1';
		document.getElementById("frmImpre:inVenta").value = '0';
	}
}

function validaDatos(objeto){
	
	if(document.getElementById("frmImpre:inRamo").value != 0){
		if(document.getElementById("frmImpre:inRamo").value == 57 || document.getElementById("frmImpre:inRamo").value == 58){
			if(document.getElementById("frmImpre:inProducto").value != 0){
				if(document.getElementById("frmImpre:idPlan").value != 0){
					objeto.show();
				}
			}
		}else if(document.getElementById("frmImpre:inPoliza").value != -1){
			if(document.getElementById("frmImpre:idProducto").value != 0){
				if(document.getElementById("frmImpre:inPlan").value != 0){
					objeto.show();
				}
			}
		}
	}
}

function exibirPanelTRG(opcion){		
	if(opcion=='1'){
		ocultaSeccion("idTmpReal", 1);
		ocultaSeccion("idGuarda", 0);				
	}else if(opcion=='2'){
		ocultaSeccion("idTmpReal", 0);
		ocultaSeccion("idGuarda", 1);		
	}else{	
		ocultaSeccion("idTmpReal", 0);
		ocultaSeccion("idGuarda", 0);	
		ocultaSeccion("panelSumas", 0);
		ocultaSeccion("resultados", 0);
	}
}

function checkSelect(chBox){
	
	var id = chBox.name.substring(chBox.name.lastIndexOf(':'));
	var el = chBox.form.elements;
	
	document.getElementById("endoso:tipoEndoso").readOnly=false;
	
	for (var i = 0; i < el.length; i++) {
        if (el[i].name.substring(el[i].name.lastIndexOf(':')) === id) {
        	el[i].checked = false;
        }
    }
	chBox.checked = true;
	
	document.getElementById("endoso:tipoEndoso").value=0;
	
	for(var j=1; j<= 10; j++){
		document.getElementById("endoso:panelCriteriosE"+j).style.display="none";
	}
	document.getElementById("endoso:panelBotonEndoso").style.display="none";
}

function suma(chBox){
	
	var id = chBox.name.substring(chBox.name.lastIndexOf(':'));
	
	//alert(id);
	
	var el  = chBox.form.elements;
	var el2 = chBox.form.elements;
	var total = 0;
	
	for (var i = 0; i < el.length; i++) {
		
        if (el[i].name.substring(el[i].name.lastIndexOf(':')) == id) {
        	if(el[i].checked){
//---------------------------------------------------
        		for (var j = 0; j < el2.length; j++) {
        			
        	        if (el2[i].name.substring(el2[i].name.lastIndexOf(':')) == ":primaHidden") {
        	        	//alert(el2[i].value);
        	        }
        	    }
//---------------------------------------------------
        	
        	}
        }
    }
	
	//alert(total);
	//chBox.checked = true;
}

function valida(){
	//alert("inicio");
	var id = ':chkReq';
	var el = document.getElementById("endoso").elements;
	var sinDatos = [];
	//alert(el);
	var contador=0;
	var mensaje = "";
	
	for (var i = 0; i < el.length; i++) {
        if (el[i].name.substring(el[i].name.lastIndexOf(':')) == id) {
        	if(el[i].checked){
        		contador++;
        	}
        }
    }

	for(var i=0;i<this.requerido.length;i++){
		var htmlCampo = document.getElementById(this.requerido[i].name).value;
		
		if(this.requerido[i].value == htmlCampo){
			
		}
	}
	
	for(var i=0;i<this.requerido.length;i++){
		//alert(this.requerido[i].name);
		//alert(document.getElementById(this.requerido[i].name).name+' valor:'+document.getElementById(this.requerido[i].name).value);
		var htmlCampo = document.getElementById(this.requerido[i].name).value;
		if(this.requerido[i]!=undefined && (this.requerido[i].value!="" || htmlCampo!="")){
			delete this.requerido[i];
		}
	}
	
	for(var i=0;i<this.requerido.length;i++){
		if(this.requerido[i]!=undefined && this.requerido[i].value==""){
			//mensaje+= "EL CAMPO "+this.requerido[i]+" ES REQUERIDO.<br>";
			//mensaje+= "EL CAMPO "+ this.requerido[i].name.lastindexOf(':') +" ES REQUERIDO.<br>";
			var str =this.requerido[i].name ;
			var n = str.lastIndexOf(':');
			var result = str.substring(n + 1);
			mensaje+= "EL CAMPO: "+ result +" ES REQUERIDO.<br>";
			
		}
	}
	//alert(str);
	if(contador==0){
		mensaje += "SELECCIONE UN CERTIFICADO.<br>";
	}
	
	if(document.getElementById("endoso:tipoEndoso").value==0){
		mensaje += "SELECCIONE UN TIPO DE ENDOSO.";
		
	}
	
	//alert(mensaje);
	
	if(mensaje!=""){
		document.getElementById("endoso:respuesta2").innerHTML = mensaje;
	}else{
		document.getElementById("endoso:respuesta2").innerHTML = "";
		this.requerido = [];
		Richfaces.showModalPanel('panelCancela',{height:'120px'});
	}
}

function required(obj, nombre){
	
	//obj.name = nombre;

	//this.requerido.push(obj);
	

}

function mostrar(){
	
	document.getElementById("botonera").style.display="block";
}

function valida2(){

	var id = ':chkReq';
	var el = document.getElementById("endoso").elements;
	var camposValidos = ['1:NOMBRE','1:APELLIDO_PATERNO','1:APELLIDO_MATERNO',
	                     '2:CALLE_Y_NUMERO','2:COLONIA','2:DELEGACION_MUNICIPIO','2:CODIGO_POSTAL','2:CIUDAD',
	                     '3:FECHA_NACIMIENTO',
	                     '4:RFC',
	                     '5:SEXO'];
	var contador=0;
	var mensaje = "";
	
	for (var i = 0; i < el.length; i++) {
        if (el[i].name.substring(el[i].name.lastIndexOf(':')) == id) {
        	if(el[i].checked){
        		contador++;
        	}
        }
    }

	if(contador==0){
		mensaje += "SELECCIONE UN CERTIFICADO.<br>";
	}
	
	if(document.getElementById("endoso:tipoEndoso").value==0){
		mensaje += "SELECCIONE UN TIPO DE ENDOSO.<br>";
		
	}
	
	
	/********************* VALIDA CAMPOS ***************/
	for (var i = 0; i < el.length; i++) {
		
		var nombreCampo = el[i].name.substring(el[i].name.lastIndexOf(':')+1);
		var valorCampo  = el[i].value;
		
		for(var j=0; j< camposValidos.length; j++ ){

			if( nombreCampo == camposValidos[j].substring(camposValidos[j].lastIndexOf(':')+1)){
				
				if(valorCampo == '' && document.getElementById("endoso:tipoEndoso").value!=0){
					
					var indice = camposValidos[j].substring(0,camposValidos[j].lastIndexOf(':'));
					
					if(document.getElementById("endoso:tipoEndoso").value == indice){
						mensaje += "EL CAMPO "+nombreCampo+" ES REQUERIDO.<br>";
					}
				
				}
			}	
		}
		
	}
	/**************************************************/
	
	if(mensaje!=""){
		document.getElementById("endoso:respuesta2").innerHTML = mensaje;
	}else{
		document.getElementById("endoso:respuesta2").innerHTML = "";
		//this.requerido = [];
		Richfaces.showModalPanel('panelCancela',{height:'120px'});
	}
	
}
function setFecha(id, valor){
	fecha = valor.substring(0,valor.lastIndexOf("/")+1);
	anio  = parseInt(valor.substring(valor.lastIndexOf("/")+1, valor.length));
	document.getElementById(id).value = fecha + (anio+50);
}

function habilitaPreguntas(val) {
    if (val == 1) {
    	ocultaSeccion("respCual1", 1);
    	ocultaSeccion("respCual2", 0);   
    	ocultaSeccion("respCual3", 0);    
    } else if (val == 2) {
    	ocultaSeccion("respCual1", 0);
    	ocultaSeccion("respCual2", 1);
    	ocultaSeccion("respCual3", 0);    
    } else {
    	ocultaSeccion("respCual1", 0);
    	ocultaSeccion("respCual2", 0);
    	ocultaSeccion("respCual3", 1);  
    }
}

function showOptFechas(val, opcion) {
	if (val == 1) {
    	ocultaSeccion("tdMes" + opcion, 1);
    	ocultaSeccion("tdAnio" + opcion, 1);
    	ocultaSeccion("tdFechasIni" + opcion, 0);
    	ocultaSeccion("tdFechasFin"  + opcion, 0);
    } else  if(val == 2) {
    	ocultaSeccion("tdMes" + opcion, 0);
    	ocultaSeccion("tdAnio" + opcion, 0);
    	ocultaSeccion("tdFechasIni" + opcion, 1);
    	ocultaSeccion("tdFechasFin"  + opcion, 1);
    } else {
    	ocultaSeccion("tdMes" + opcion, 0);
    	ocultaSeccion("tdAnio" + opcion, 0);
    	ocultaSeccion("tdFechasIni" + opcion, 0);
    	ocultaSeccion("tdFechasFin"  + opcion, 0);
    }
}

/**
* Oculta parametros comisiones en configuracion parametros
* @param valor valor combo
* @param idPanel idpanel
* @param idtxt1 idtxtcomision1
* @param idtxt2 idtxtcomision2
* @returns
*/
function ocultaComisiones (valor,idPanel, idtxt1, idtxt2) {
	if (valor == "C") {
		document.getElementById(idPanel).style.display = "";
		document.getElementById(idtxt1).value = 0;
		document.getElementById(idtxt2).value = 0;
	} else {
		document.getElementById(idPanel).style.display = "none";
		document.getElementById(idtxt1).value = 0;
		document.getElementById(idtxt2).value = 0;
	}
}

function inicializaEdicionCobertura(){
	
	if (document.getElementById('editarProducto:txtComTecnica').value > 0 || document.getElementById('editarProducto:txtComContigente').value > 0) {
		document.getElementById('editarProducto:cosuExamMedico').value = "C";
		document.getElementById('editarProducto:panelComisiones').style.display = "";
	} else {
		document.getElementById('editarProducto:cosuExamMedico').value = "P";
		document.getElementById('editarProducto:panelComisiones').style.display = "none";
		
	}
}

function checkOneSelect(chBox){	
	var id = chBox.name.substring(chBox.name.lastIndexOf(':'));
	var el = chBox.form.elements;		
	
	for (var i = 0; i < el.length; i++) {
		if (el[i].name.substring(el[i].name.lastIndexOf(':')) === id) {
        	el[i].checked = false;
        }
    }
	chBox.checked = true;
}

function muestraMsg(valor){
	var strMesange;
	var datos = valor.split("-");		
	
	if (datos[2] > 0) {
		strMesange = "Asegurarse que la plantilla que van a configurar tenga los siguientes campos: <br/>";
		strMesange = strMesange + "	- nombrebenef" + datos[2] + "<br/>";
		strMesange = strMesange + "	- fechanacbenef" + datos[2] + "<br/>";
		strMesange = strMesange + "	- parentescobenef" + datos[2] + "<br/>";
		strMesange = strMesange + "	- porcentajebenef" + datos[2] + "<br/>";		
	} else {
		strMesange = "";
	}
	document.getElementById("newModif:msgBeneficiarios").innerHTML = strMesange;
	
}

function bloqueaPantalla(){
	posicionacortina('cortina', 0);
	document.getElementById('cortina').style.display = 'block';
}

function desbloqueaPantalla(){	 	
	document.getElementById('cortina').style.display = 'none';
}

function posicionacortina(idcapa, top){
	var strPY = posicionscroll();	
	var posYC = strPY + top;
	document.getElementById(idcapa).style.top= posYC + "px";	
}

function posicionscroll(){
	if (window.scrollHeight) {		
		return document.documentElement.scrollTop;
	} else if (document.documentElement && document.documentElement.scrollHeight !== 0) {
		return document.documentElement.scrollTop;
	} else if (document.body) {
		return document.body.scrollTop;
	}
	return false;
}

function exibirPanelCancela(opcion){
	switch (opcion) {
		case 1:
			ocultaSeccion("trAutomatico", 1);
			ocultaSeccion("trManual", 0);
			break;
			
		case 2:
			ocultaSeccion("trAutomatico", 0);
			ocultaSeccion("trManual", 1);
			break;
	  
		default:
			ocultaSeccion("trAutomatico", 0);
			ocultaSeccion("trManual", 0);
			break;
		
	}

	muestraOcultaSeccion('resultado', 0);
}

function panelAvisoCancelacion() {
	 var noSiniestro = document.getElementById("cancelacion:noSin").value;

	 if(noSiniestro === '0') {
		 Richfaces.showModalPanel('panelCancela');
	 } else {
		 Richfaces.showModalPanel('panelNotSin');
	 }
}

function exibirPanelTimbrado(opcion) {
	switch (opcion) {
		case 1:
			ocultaSeccion("trCertificado", 1);
			ocultaSeccion("trRecibo", 0);
			ocultaSeccion("trUuid", 0);
			break;
			
		case 2:
			ocultaSeccion("trCertificado", 0);
			ocultaSeccion("trRecibo", 1);
			ocultaSeccion("trUuid", 0);
			break;
	  
		case 3:
			ocultaSeccion("trCertificado", 0);
			ocultaSeccion("trRecibo", 0);
			ocultaSeccion("trUuid", 1);
			break;
			
		default:
			ocultaSeccion("trCertificado", 0);
			ocultaSeccion("trRecibo", 0);
			ocultaSeccion("trUuid", 0);
			break;
		
	}
	
	muestraOcultaSeccion('resultado', 0);
}

/**
* Muestra u oculta filtros para realizar peticion de reportes Cobranza
* @param valor valor combo
* @returns nada
*/
function muestraFiltrosRepCob(valor) {
	//Se inicializan valores
	setValoresDefaultRepCob();
		
	//Si no selecciono tipo reporte
	if(valor === '0') {
		return;
	}
		
	//Se muestran fechas para realizar filtro
	ocultaSeccion("trFeIni", 1);
	ocultaSeccion("trFeFin", 1);
		
	//Se valida para mostrar filtro ramo poliza
	if(!(valor === '4' || valor === '7')) {
		ocultaSeccion("trRamo", 1);
		ocultaSeccion("trPoliza", 1);
	}
		
	//Se valida para mostrar filtro estatus
	if(valor === '2' || valor === '3') {
		ocultaSeccion("trEstatus", 1);
	}
}

/**
 * Metodo que inicializa secciones y valores
 */
function setValoresDefaultRepCob() {
	//Se ocultan filtros	
	ocultaSeccion("trRamo", 0);
	ocultaSeccion("trPoliza", 0);
	ocultaSeccion("trEstatus", 0);
	ocultaSeccion("trFeIni", 0);
	ocultaSeccion("trFeFin", 0);
	
	//Se setean valores
	document.getElementById("frmReporteador:inRamo").value = 0;
	document.getElementById("frmReporteador:inPoliza").value = 0;
	document.getElementById("frmReporteador:estatus").value = 0;
	document.getElementById("frmReporteador:mensaje").value = '';
	
}
