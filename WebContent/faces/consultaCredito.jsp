<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<title>Consulta por Cr�dito</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table width="785px">
						<tbody>
							<tr>														
								<td align="center">
									<div align="center">
										<table bgcolor="#555242" cellpadding="0" cellspacing="0" class="encabezado">
											<tbody>
												<tr>												
												<td bgcolor="#8F8B70" width="10%" align="center">Secci�n</td>
												<td width="4%" align="center" bgcolor="#d90909"></td>												
												<td align="left" width="70%">Consulta de Certificado</td>												
												<td align="right" width="14%" >Fecha</td>
												<td align="left" width="10%">
												<input name="fecha" readonly="readonly" size="10">									
												</tr>
											</tbody>
										</table>
									</div>									
								</td>				
							</tr>
						</tbody>				
					</table>
					</div>
					<div align="center">
					<!-- otra tabla -->
				    <h:form>
				     <table class="tabla">
				     <tbody>
				    	<tr>
				     		<td align="right"><h:outputText value="Numero Identificador:" /></td>
				     		<td align="left">
				     			<h:inputText id="identificador"  value="#{beanIdCertificado.idCertificado}"/>
				     		
				     		</td>
			     		</tr>

			     		<tr>
				     		<td align="right"><h:outputText value="Nombre:" /></td>
				     		<td align="left">
				     			<h:inputText id="nombre"  value="#{beanIdCertificado.nombre}"/>
				     			
				     		</td>
			     		</tr>
			     		
			     		<tr>
				     		<td align="right"><h:outputText value="Apellido Paterno:" /></td>
				     		<td align="left">
				     			<h:inputText id="ap"  value="#{beanIdCertificado.ap}"/>
				     			
				     		</td>
			     		</tr>
			     		
                        <tr>
				     		<td align="right"><h:outputText value="Apellido Materno:" /></td>
				     		<td align="left">
				     			<h:inputText id="am"  value="#{beanIdCertificado.am}"/>
				     			
				     		</td>
			     		</tr>
			     		<tr>
			     			<td></td>
			     			<td align="right">
			     				<h:commandButton action="#{beanIdCertificado.consultaidCertificado}" value="Consulta" />
			     			</td>
			     		</tr>
			     	</tbody>     		         		    		
				    </table>
			     	<div align="center">
			     	<table class="tabla">
			     	<tbody>
				     	<tr>
				     		<td align="center">
					      	<rich:dataTable id="dato" value="#{beanIdCertificado.lista}" var="beanClienteCertif" rows="12">
					      		<f:facet name="header">
					      			<h:outputText  value="Consulta de Certificados" /> 
					      		</f:facet>
					      		<rich:column width="10px">
						      		<f:facet name="header">
						      			<h:outputText value="ID" /> 
						      		</f:facet>    
						      		<h:outputText value="#{beanClienteCertif.id.coccIdCertificado}" />		      		  			
					      		</rich:column>
					      		<rich:column>
					      		<f:facet name="header">
					      			<h:outputText value="Nombre" /> 
					      		</f:facet>    
					      		<h:outputText value="#{beanClienteCertif.clientes.cocnNombre} #{beanClienteCertif.clientes.cocnApellidoPat} #{beanClienteCertif.clientes.cocnApellidoMat}" />		      		  			
					      		</rich:column>
					      		
					      		
					      	    <rich:column>
					      		<f:facet name="header">
					      			<h:outputText value="Datos de Certificado" /> 
					      		</f:facet>    
								<h:outputText value="#{beanClienteCertif.certificado.id}" />      		  			
					      		</rich:column>
					      		<rich:column>
						      		<f:facet name="header">
						      			<h:outputText value="Fecha Emisi�n" /> 
						      		</f:facet>    
									<h:outputText value="#{beanClienteCertif.certificado.coceFeEmision}" />      		  			
					      		</rich:column>

					      		<rich:column>
						      		<f:facet name="header">
						      			<h:outputText value="Estatus" /> 
						      		</f:facet>    
									<h:outputText value="#{beanClienteCertif.certificado.alternaEstatus}" />      		  			
					      		</rich:column> 
					      		
					      		<f:facet name="footer">
						          <rich:datascroller align="center" for="dato" maxPages="100" page="#{beanIdCertificado.numpages}" />
					            </f:facet>
					      	</rich:dataTable>
						      </td>
						     </tr>
					</tbody>
					</table>
					</div>
			     	</h:form>
					</div>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<div>
<%@include file="footer.jsp" %>
</div>
</f:view>
</body>
</html>