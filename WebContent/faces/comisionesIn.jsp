<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<title>Comisiones In</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
				<div align="center">
				<h:form>
				<table cellpadding="0" cellspacing="2" border="0" align="center">
				<tbody>
					<tr>
						<td></td>
						<td align="right"><h:outputText value="Canal:" /></td>
						<td align="left"><h:inputText value="#{beanComisionesIn.canal}" /></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td align="right"><h:outputText value="Ramo:" /></td>
						<td align="left"><h:inputText value="#{beanComisionesIn.ramo}" /></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td align="right"><h:outputText value="Sucursal:" /></td>
						<td align="left"><h:inputText value="#{beanComisionesIn.sucursal}" /></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td align="right"><h:outputText value="P�liza:" /></td>
						<td align="left"><h:inputText value="#{beanComisionesIn.poliza}" /></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td align="right"><h:outputText value="Fecha inicial:" /></td>
						<td align="left">
							<rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy" required="true" value="#{beanComisionesIn.fechaInicial}"/>
							<h:message for="fechaInicial" styleClass="errorMessage" />
						</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td align="right"><h:outputText value="Fecha final:" /></td>
						<td align="left">
							<rich:calendar id="fechaFinal" datePattern="dd/MM/yyyy" required="true" value="#{beanComisionesIn.fechaFinal}"/>
							<h:message for="fechaFinal" styleClass="errorMessage" />
						</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td align="left"></td>
						<td align="right">
							<h:commandButton action="#{beanComisionesIn.comisionesIn}" value="Generar" title="Generar reporte"/>
						</td>
						<td></td>
					</tr>
				</tbody>
				</table>
				</h:form>
				</div>
				<div align="center">
					<h:messages globalOnly="true" styleClass="errorMessage"/>
				</div>
				</td>
			</tr>
		</tbody>
		</table>
		</td>
	</tr>
</tbody>
</table>		
</div>
<div align="center">
<%@include file="footer.jsp" %>
</div>
</f:view>
</body>
</html>