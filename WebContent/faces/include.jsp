<%@ taglib prefix="f"        uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h"        uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="a4j"      uri="http://richfaces.org/a4j" %>
<%@ taglib prefix="rich"     uri="http://richfaces.org/rich" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c"        uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="jr"       uri="http://jasperreportjsf.sf.net/tld/jasperreports-jsf-1_0.tld"  %>
<%@ taglib prefix="fn" 		uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    	 uri="http://java.sun.com/jsp/jstl/fmt" %>