<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Reservas</title>
</head>
<body>
	<f:view>
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center"><%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>
						<td align="center">
							<table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<div align="center">
												<table class="encabezadoTabla">
													<tbody>
														<tr>
															<td width="10%" align="center">Secci�n</td>
															<td width="4%" align="center" bgcolor="#d90909">||||</td>
															<td align="left" width="64%">Reservas</td>
															<td align="right" width="12%">Fecha</td>
															<td align="left" width="10%"><input name="fecha"
																readonly="readonly"
																value="<%=request.getSession().getAttribute("fecha")%>"
																size="7"></td>
														</tr>
													</tbody>
												</table>
											</div> <rich:spacer height="2px"></rich:spacer> <h:form
												id="frmCargaSuscripcion">
												<rich:messages style="color:red;"></rich:messages>

												<div align="center">
													<table class="botonera">
														<tbody>
															<tr>
																<td class="frameTL"></td>
																<td class="frameTC"></td>
																<td class="frameTR"></td>
															</tr>
															<tr>
																<td class="frameCL"></td>
																<td class="frameC" width="600px"><rich:panel
																		id="paBandejaSuscripcion" styleClass="paneles">
																		<f:facet name="header">
																			<h:outputText value="Generar Reservas" />
																		</f:facet>
																		
																		<table>

																				<a4j:region id="filtroProceso">
																				<tbody>
																					<tr id="tdMes2">
																						<td align="right" height="30"><h:outputText
																								value="Mes generar: " /></td>
																						<td align="left" height="30"><h:selectOneMenu
																								id="inMes2" value="#{beanReserva.mes}"
																								style="width: 150px" title="Seleccione un mes">
																								<f:selectItems value="#{beanReserva.listaMes}" />
																							</h:selectOneMenu></td>
																					</tr>
																					<tr>
																						<td colspan="2" align="center" width="60%"><a4j:status
																								for="filtroProceso" stopText=" ">
																								<f:facet name="start">
																									<h:graphicImage value="/images/ajax-loader.gif" />
																								</f:facet>
																							</a4j:status></td>
																					</tr>

																					<tr>

																						<td align="center" colspan="2"><a4j:commandButton
																								styleClass="boton" value="Procesar"
																								action="#{beanReserva.procesaReservas}"
																								onclick="this.disabled=true"
																								oncomplete="this.disabled=false" /></td>
																					</tr>
																				</tbody>
																			</a4j:region>

																		</table>
																	</rich:panel> <rich:spacer height="30px"></rich:spacer> <a4j:region
																		id="filtroCarga">
																		<rich:panel id="paConsultarResrvas"
																			styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="Consultar" />
																			</f:facet>
																			
																			<table>
																				<tbody>
																					
																					<tr>
																						<td align="right"><h:outputText value="Ramo:" /></td>
																						<td align="left"><h:selectOneMenu id="ramo"
																								value="#{beanReserva.ramoFiltro}"
																								style="width: 150px">

																								<f:selectItem itemValue="0"
																									itemLabel="Selecione un ramo" />
																								<f:selectItem itemValue="57"
																									itemLabel="57--VIDAEMPRESARIAL-SERFIN" />
																								<f:selectItem itemValue="58"
																									itemLabel="58--VIDAEMPRESARIAL-SANTANDER" />

																								
																							</h:selectOneMenu></td>

																					</tr>
																					<tr>
																						<td align="right" height="30"><h:outputText
																								value="Mes consulta: " /></td>
																						<td align="left" height="30"><h:selectOneMenu
																								value="#{beanReserva.mesFiltro}"
																								style="width: 150px" title="Seleccione un mes">
																								<f:selectItems value="#{beanReserva.listaMes}" />
																							</h:selectOneMenu></td>
																					</tr>
																					<tr>
																						<td align="center" colspan="2" width="60%"><a4j:status
																								for="filtroCarga" stopText=" ">
																								<f:facet name="start">
																									<h:graphicImage value="/images/ajax-loader.gif" />
																								</f:facet>
																							</a4j:status></td>
																					</tr>

																					<tr>

																						<td align="center" colspan="2"><a4j:commandButton
																								styleClass="boton" value="Consultar"
																								action="#{beanReserva.consultarReservas}"
																								onclick="this.disabled=true"
																								oncomplete="this.disabled=false" /></td>
																					</tr>
																				</tbody>
																			</table>
																		</rich:panel>
																		<br />
																	</a4j:region></td>
																<td class="frameCR"></td>
															<tr>
																<td class="frameBL"></td>
																<td class="frameBC"></td>
																<td class="frameBR"></td>
															</tr>
														</tbody>
													</table>
												</div>
											</h:form> <h:form id="frmDescargaArchivo">
												<h:commandLink id="btnDescarga" value="  "
													action="#{beanSuscripcion.descargarArchivoResultados}">
												</h:commandLink>
											</h:form>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<rich:spacer height="10px" />
		<div align="center"><%@ include file="../footer.jsp"%>
		</div>
	</f:view>
</body>