<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Endoso de P�liza</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
			<table class="tablaSecundaria">
				<tbody>
					<tr>
						<td align="center">
							<div align="center">
							<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Endosos</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
							</table>
						    </div>
						    <rich:spacer height="2px"></rich:spacer>
						    <h:form id="frmEndoso">
							    <div align="center">
							    <rich:spacer height="15"/>	
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">	
									    <a4j:region id="filtroCarga">		    
									    <table>
									    <tbody>
									        <tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Ramo:" /></td>
									    		<td align="left" width="76%" height="30">
													<h:selectOneMenu id="inRamo" value="#{beanListaEndoso.beanGen.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
													</h:selectOneMenu>
													<h:message for="inRamo" styleClass="errorMessage" />
												</td>
									    	</tr>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Poliza:"/></td>
												<td align="left" width="76%" height="30">
													<a4j:outputPanel id="polizas" ajaxRendered="true">
														<h:selectOneMenu id="inPoliza"  value="#{beanListaEndoso.beanGen.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																<f:selectItem itemValue="0" itemLabel="Prima �nica carga"/>
																<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta" />
														</h:selectOneMenu>
														<h:message for="inPoliza" styleClass="errorMessage" />
													</a4j:outputPanel>
												</td>
									    	</tr>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Canal de Venta:"/></td>
												<td align="left" width="76%" height="30">
													<a4j:outputPanel id="idVenta" ajaxRendered="true">
														<h:selectOneMenu id="inVenta"  value="#{beanListaEndoso.beanGen.idVenta}" binding="#{beanListaParametros.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
														         disabled="#{beanListaParametros.habilitaComboIdVenta}">
																<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
														</h:selectOneMenu>
													</a4j:outputPanel>
												</td>
									    	</tr>
									    	<tr>  
							    				<td align="right">
							    					<a4j:commandButton styleClass="boton" value="Tipos de Endoso" 
														   action="#{beanListaParametros.cargaTipoEndoso}" 
														   reRender="inOperEndoso"
														   title="header=[Endosos] body=[Tipos de endosos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>	   
												</td>
												<td align="left" width="76%" height="30">
													<h:selectOneMenu id="inOperEndoso" value="#{beanListaEndoso.inOperEndoso}" binding="#{beanListaParametros.inOperEndoso}" 
													     title="header=[Ramo] body=[Seleccione opoeracion para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
													     required="true">
														<f:selectItem itemValue="0" itemLabel="Seleccione el tipo de Endoso" />
														<f:selectItems value="#{beanListaParametros.listaTipoEndoso}"/>
													</h:selectOneMenu>
													<h:message for="inOperEndoso" styleClass="errorMessage" />
												</td>
								    		</tr>
									    </tbody>
									    </table>
									    </a4j:region>
									    <a4j:region id="archivoCarga" >
								    	
								    	<rich:spacer height="30px"></rich:spacer>
									    <table>
									    	<tbody>
									    		<tr>  
								    				<td align="center">
								    				    <a4j:region id="regionInfo">
									    					<a4j:commandButton styleClass="boton" value="Consultar Recibo" 
									    					       id="btConsultarRecibo"
																   action="#{beanListaEndoso.consultarRecibo}" 
																   onclick="this.disabled=true" oncomplete="this.disabled=false" 
																   reRender="mensaje,listaRecibos"
																   title="header=[Endosos] body=[Recibos a endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
																   
															
										 
															<rich:panel id="rgFechas" 
															        header="Rango de Fechas" 
															        styleClass="tablaSecundaria" style="width: 70%">
																<table>
																	<tr>
																		<td><h:outputLabel value="Fecha Desde"/></td>
																		<td>
																		    <rich:calendar value="#{beanListaEndoso.beanGen.feDesde}" inputSize="10" 
															    				popup="true" cellHeight="20px" id="feDesde"
															    				cellWidth="50px" datePattern="dd/MM/yyyy" styleClass="calendario">
															    			</rich:calendar>
					    												</td>
																		<td><h:outputLabel value="Fecha Hasta"/></td>
																		<td>
																			<rich:calendar value="#{beanListaEndoso.beanGen.feHasta}" inputSize="10" 
															    				popup="true" cellHeight="20px" id="feHasta"
															    				cellWidth="50px" datePattern="dd/MM/yyyy" styleClass="calendario">
															    			</rich:calendar>
															    		</td>
																	</tr>
																</table>
															</rich:panel>	  		
														</a4j:region>
													</td>
									    		</tr>
									    	</tbody>
									    	</table>
									    	<br/>
									    	<table>
									    	<tbody>
									    		<tr>
									    			<td align="center">
										    			<a4j:status for="archivoCarga" stopText=" ">
															<f:facet name="start">
																<h:graphicImage value="/images/ajax-loader.gif" />
															</f:facet>
														</a4j:status>
													</td>
												</tr>
												<tr>
													<td align="center">
														<br/>
														<h:outputText id="mensaje" value="#{beanListaEndoso.beanGen.strRespuesta}" styleClass="respuesta"/>
														<br/>
														<br/>
													</td>
												</tr>
											</tbody>
											</table>
											</a4j:region>
									    </td>
									    <td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
							    </table>
							    <br/>
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC">
									     	<table>
									     	<tbody>
										     	<tr>
										     		<td align="center">
										     		<rich:dataTable id="listaRecibos" value="#{beanListaEndoso.listaRecibos}" columns="9" 
										     		       columnsWidth="8px,8px,15px,15px,15px,15px,20px,8px,20px" var="beanReciboCol">
											      		<f:facet name="header"><h:outputText value="Datos del Recibo" /></f:facet>
											      		
											      		<rich:column>  
											      	        <f:facet name="header"><h:outputText value="Canal" /></f:facet>
															<h:outputText value="#{beanReciboCol.id.coreCasuCdSucursal}" />      		  			
											      		</rich:column>
											      		<rich:column> 
											      		    <f:facet name="header"><h:outputText value="Ramo" /></f:facet> 
															<h:outputText value="#{beanReciboCol.coreCarpCdRamo}" />      		  			
											      		</rich:column>
											      		<rich:column filterBy="#{beanReciboCol.coreCapoNuPoliza}">  
											      		    <f:facet name="header"><h:outputText value="P�liza" /></f:facet>
															<h:outputText value="#{beanReciboCol.coreCapoNuPoliza}" />      		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Recibo" /></f:facet>
															<h:commandLink value="#{beanReciboCol.id.coreNuRecibo}" action="#{beanReciboCol.imprimeRecibo}" /> 
															<br/>
															<h:commandLink value="#{beanReciboCol.reciboAnterior}" action="#{beanReciboCol.imprimeRecibo2}" rendered="#{not empty beanReciboCol.reciboAnterior}"/>     		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Consecutivo" /></f:facet>
															<h:outputText value="#{beanReciboCol.recibo.coreNuConsecutivoCuota}" />      		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Estatus" /></f:facet>
															<h:outputText value="#{beanReciboCol.coreStRecibo}" />      		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Emisi�n" /></f:facet>
															<h:outputText value="#{beanReciboCol.coreFeEmision}" >
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>      		  			
											      		</rich:column> 
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Endosar" /></f:facet>
															<h:selectBooleanCheckbox value="#{beanReciboCol.endosar}" />     		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Estatus Endoso" /></f:facet>
															<h:outputText value="#{beanReciboCol.mensajeEndoso}" />      		  			
											      		</rich:column>
											      	</rich:dataTable>
											      	
												      </td>
												     </tr>
											</tbody>
											</table>
									    </td>
										<td class="frameCR"></td>
								    </tr>
								    <tr>
								        <td class="frameCL"></td>
								    	<td align="center">
								    		<br/>
								    		<br/>
								    		<br/>
								    		<a4j:commandButton styleClass="boton" value="Endosar Polizas" 
					    					       id="btEndosar"
												   action="#{beanListaEndoso.endosarPoliza}" 
												   onclick="this.disabled=true" oncomplete="this.disabled=false" 
												   reRender="listaEndosos,listaRecibos"
												   title="header=[Endosos] body=[Endosa P�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
								    		<br/>
								    		<br/>
								    		<br/>
								    	</td>
								    	<td class="frameCR"></td>
								    	
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC">
									    	<div align="center">
												<rich:dataTable id="listaEndosos" value="#{beanListaEndoso.listaEndosos}" columns="7" 
										     		       columnsWidth="8px,8px,15px,15px,15px,15px,20px" var="endoso" rows="10">
											      		<f:facet name="header"><h:outputText value="Datos del Endoso" /></f:facet>
											      		
											      		<rich:column>  
											      	        <f:facet name="header"><h:outputText value="Canal" /></f:facet>
															<h:outputText value="#{endoso.endoso.id.coedCasuCdSucursal}" />      		  			
											      		</rich:column>
											      		<rich:column> 
											      		    <f:facet name="header"><h:outputText value="Ramo" /></f:facet> 
															<h:outputText value="#{endoso.endoso.id.coedCarpCdRamo}" />      		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="P�liza" /></f:facet>
															<h:outputText value="#{endoso.endoso.id.coedCapoNuPoliza}" />      		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Recibo" /></f:facet>
															<h:outputText value="#{endoso.endoso.coedNuRecibo}" /> 
														</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="No. Endoso" /></f:facet>
															<h:outputText  />  
															<h:commandLink value="#{endoso.endoso.coedCampov1}" action="#{endoso.imprimeEndoso}"/>     		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Desde Endoso" /></f:facet>
															<h:outputText value="#{endoso.endoso.coedFechaInicio}" >
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>     		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Hasta Endoso" /></f:facet>
															<h:outputText value="#{endoso.endoso.coedFechaFin}" >
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>      		  			
											      		</rich:column> 
											      		<f:facet name="footer">
											      			<rich:datascroller align="right" for="listaEndosos" maxPages="10" page="#{beanListaEndoso.beanGen.numPagina}"/>
											      		</f:facet>
											      	</rich:dataTable>
												<br/>
											</div>
									    </td>
										<td class="frameCR"></td>
								    	</tr>
								    	
								    	<tr>
									    <td class="frameCL"></td>
									    <td class="frameC">
									    	<br/>
								    		<br/>
								    		<br/>
								    		<h:commandButton styleClass="boton" value="Endosos de Hoy" 
					    					       id="consultaEndoso"
												   action="#{beanListaEndoso.reporteEndosos}"  
												   title="header=[Endosos] body=[Consulta Endosos de Hoy.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
								    		<br/>
								    		<br/>
								    		<br/>	
								        </td>							    
										<td class="frameCR"></td>
								    	</tr>
								    	
								    	<tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
							    	</tbody>
							    	</table>												   
								</div>
						    </h:form>
						    <rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>