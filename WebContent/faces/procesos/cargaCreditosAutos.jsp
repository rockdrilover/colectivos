<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Carga creditos de auto</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
			<table class="tablaSecundaria">
				<tbody>
					<tr>
						<td align="center">
							<div align="center">
							<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Carga Creditos de Auto</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
							</table>
						    </div>
						    <rich:spacer height="2px"></rich:spacer>
						    <h:form id="frmCargaCreditosAutos">
							    <div align="center">
							    <rich:spacer height="15"/>	
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">	
									    <rich:spacer height="30px"></rich:spacer>
									    <a4j:region id="archivoCarga" >
								    	<table>
							    		<tbody>
							    			<tr>
							    				
							    				<td align="right" width="24%" height="30"><h:outputText value="Cargar archivo:" /></td>
							    				<td align="left" width="76%" height="30" title="header=[Archivo] body=[Archivo a cargar extensi�n CSV, cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
							    					<rich:fileUpload fileUploadListener="#{beanCreditosAutos.listener}" 
					    								addControlLabel="Examinar" uploadControlLabel="Cargar" maxFilesQuantity="5" stopControlLabel="Detener" immediateUpload="true" 
					    								clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" styleClass="archivo" acceptedTypes="csv,txt"
					    								listHeight="65px" addButtonClass="botonArchivo" listWidth="90%">
					    								<a4j:support event="onuploadcomplete" reRender="mensaje" />
							    						<f:facet name="label">
							    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
							    						</f:facet>
							    					</rich:fileUpload>
							    				</td>
							    			</tr>
							    		</tbody>
								    	</table>
								    	<rich:spacer height="30px"></rich:spacer>
									    <table>
									    	<tbody>
									    		<tr>
								    				<td align="center">
								    					<h:commandButton styleClass="boton" value="Errores" 
															   action="#{beanCreditosAutos.generaReporteEmisionAutos}"
															   title="header=[Errores] body=[Registros con errores despu�s de realizar el proceso de carga y emisi�n.Asegurese de guardar el archivo ya que despu�s de su generaci�n los errores se eliminar�n de la base de datos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
								    				</td>
								    				<td align="center">
								    					<a4j:commandButton styleClass="boton" value="Emitir" 
															   action="#{beanCreditosAutos.cargar}" 
															   onclick="this.disabled=true" oncomplete="this.disabled=false" 
															   reRender="mensaje, listaCertificados"
															   title="header=[Carga masiva] body=[Carga unicamente la informaci�n en la tabla.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
								    				</td>  
									    		</tr>
									    	</tbody>
									    	</table>
									    	<br/>
									    	<table>
									    	<tbody>
									    		<tr>
									    			<td align="center">
										    			<a4j:status for="archivoCarga" stopText=" ">
															<f:facet name="start">
																<h:graphicImage value="/images/ajax-loader.gif" />
															</f:facet>
														</a4j:status>
													</td>
												</tr>
												<tr>
													<td align="center">
														<br/>
														<h:outputText id="mensaje" value="#{beanCreditosAutos.mensaje}" styleClass="respuesta"/>
														<br/>
														<br/>
													</td>
												</tr>
											</tbody>
											</table>
											</a4j:region>
									    </td>
									    <td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
							    </table>
							    <br/>											   
						    <table class="botonera">
						    <tbody>
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="600px">
								     	<table>
								     	<tbody>
									     	<tr>
									     		<td align="center">
										      	<rich:dataTable id="listaCertificados" value="#{beanCreditosAutos.listaCertificados}" columns="2" columnsWidth="10px,10px" var="beanCreditosAutosL" rows="10">
										      		<f:facet name="header">
										      			<rich:columnGroup>
										      				<rich:column><h:outputText value="Estatus" /></rich:column>
										      				<rich:column><h:outputText value="Total" /></rich:column>
										      			</rich:columnGroup>
										      		</f:facet>
										      		<rich:column> 
											      		<h:outputText value="#{beanCreditosAutosL.coceCampov5}" />		      		  			
										      		</rich:column>
										      		<rich:column>
											      		<h:outputText value="#{beanCreditosAutosL.coceCampov1}" />		      		  			
										      		</rich:column>
													<f:facet name="footer">
										      			<rich:datascroller align="right" for="listaCertificados" maxPages="10" page="#{beanCreditosAutos.numPagina}"/>
										      		</f:facet>
										      	</rich:dataTable>
										      	
											      </td>
											     </tr>
										</tbody>
										</table>
									<td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>								</div>
						    </h:form>
						    <rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>