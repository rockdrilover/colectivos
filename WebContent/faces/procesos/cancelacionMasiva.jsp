<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Cancelacion masiva</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal" >
<tbody>
	<tr>
		<td>
			<table class="tablaSecundaria">
			<tbody>
				<tr>
					<td align="center">
						<div align="center">
						<table class="encabezadoTabla">
						<tbody>
							<tr>
								<td width="10%" align="center">Secci�n</td>
								<td width="4%" align="center" bgcolor="#d90909">||||</td>
								<td align="left" width="64%">Cancelacion masiva</td>
								<td align="right" width="12%" >Fecha</td>
								<td align="left" width="10%">
									<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
								</td>
							</tr>
						</tbody>
						</table>
					    </div>
					    <rich:spacer height="10px"></rich:spacer>
					    <h:form id="frmCancelacionMasiva">
					    <div align="center">
						    <rich:spacer height="15"/>	
						    <table class="botonera">
						    <tbody>
						    	<tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL" />
								    <td class="frameC" >
									    <a4j:region id="filtrosCancelacion">
									    <table>
									    <tbody>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Ramo:" /></td>
									    		<td align="left" width="76%" height="30">
									    			<h:selectOneMenu id="inRamo" value="#{beanCancelacionMasiva.inRamo}" binding="#{beanListaParametros.inRamo}" >
									    				<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
									    				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
									    				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas"  />
									    			</h:selectOneMenu>
									    		</td>
									    	</tr>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Poliza:"/></td>
												<td align="left" width="76%" height="30">
													<a4j:outputPanel id="polizas" ajaxRendered="true">
														<h:selectOneMenu id="inPoliza"  value="#{beanCancelacionMasiva.inPoliza}" binding="#{beanListaParametros.inPoliza}">
																<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																<f:selectItem itemValue="0" itemLabel="Prima �nica carga"/>
																<f:selectItem itemValue="-1" itemLabel="Prima �nica automatica"/>
																<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta,opc" />
														</h:selectOneMenu>
													</a4j:outputPanel>
												</td>				
									    	</tr>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Canal de Venta:"/></td>
												<td align="left" width="76%" height="30">
													<a4j:outputPanel id="idVenta" ajaxRendered="true">	
														<h:selectOneMenu id="inIdVenta"  value="#{beanCancelacionMasiva.inIdVenta}" >
																<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																<a4j:support event="onchange" ajaxSingle="true" reRender="inIdVenta" />
														</h:selectOneMenu>
													</a4j:outputPanel>
												</td>
									    	</tr>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Causa anulaci�n:" /></td>
									    		<td align="left"  width="76%" height="30">
									    			<a4j:outputPanel id="causa" ajaxRendered="true">	
										    			<h:selectOneMenu id="inCausa"  value="#{beanCancelacionMasiva.inCausa}" >
															<f:selectItem itemValue="" itemLabel="Causa de cancelaci�n"/>
															<f:selectItems value="#{beanListaEstatus.listaEstatusCancelacion}"/>
															<a4j:support event="onchange" ajaxSingle="true" reRender="inCausa" />
														</h:selectOneMenu>
													</a4j:outputPanel>
									    		</td>
									    	</tr>
									    </tbody>
									    </table>
									    </a4j:region>
									    <a4j:region id="archivoCancelacion">
									    <table>
								    		<tbody>
								    			<tr>
								    				<td align="right" width="24%" height="30">Cargar archivo:</td>
								    				<td align="left" width="76%" height="30">
								    					
								    					<rich:fileUpload id="uploadCancelacion" fileUploadListener="#{beanCancelacionMasiva.listener}"
						    								 addControlLabel="Examinar" uploadControlLabel="Cargar"  
						    								stopControlLabel="Detener" maxFilesQuantity="10"  immediateUpload="true" 
						    								clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" styleClass="archivo" acceptedTypes="csv"
						    								listHeight="65px" addButtonClass="botonArchivo" listWidth="90%">
						    								<a4j:support event="onuploadcomplete" reRender="message" />
								    						<f:facet name="label">
								    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
								    						</f:facet>
								    					</rich:fileUpload>
								    				</td>
								    			</tr>
								    			<tr>
									    	  		<td align="center" colspan="2">
										    	    <br/>
										    	    <a4j:region id="regionOpciones">
										    	    <rich:panel id="opciones" header="Opciones" style=" width: 80%; align:center; background-color:#E5E0EC" styleClass="tablaSecundaria">
										    	        <f:facet name="header"><h:outputText value="Opciones de Emision" /></f:facet>
										    	        <table width="500%">
													    <tbody>
													    	<tr>
											    			    <td align="center">
											    			    	<h:selectManyCheckbox id="opc" value="#{beanCancelacionMasiva.opciones}" disabled="#{beanCancelacionMasiva.habilitar}">
																		<f:selectItem itemLabel="Aplicar Cancelaci�n:" itemValue="1" />
																		<f:selectItem itemLabel="Calcular Devoluci�n:" itemValue="2" />
																		<f:selectItem itemLabel="Validar Devoluci�n:"  itemValue="3" itemDisabled="#{beanCancelacionMasiva.habValDev}"/>
																		<f:selectItem itemLabel="Validar Prima a Devolver:" itemValue="4" />
																		<a4j:support event="onchange"  ajaxSingle="true" reRender="opc" />
																	</h:selectManyCheckbox>
																</td>
											    			</tr>
											    			<tr>
											    				<td align="left">
											    					<h:selectBooleanCheckbox id="c1"  value="#{beanCancelacionMasiva.chkValPmaDev}" />
											    					<h:outputText value="Validar Prima a Devolver (Banco vs Sistema)"/> 
											    				</td>
											    			</tr>
											    		</tbody>
											    		</table>
											    	</rich:panel>
											    	</a4j:region>
										    		<br/>
									    	  		</td>
									    		</tr>
									    		<tr>
									    		 	<td colspan="2">
									    		 	    <rich:spacer height="15" />
											    		<table>
											    		<tbody>
											    			<tr>
											    				<td align="center">
											    					<h:commandButton action="#{beanCancelacionMasiva.erroresCancelacionMasiva}" styleClass="boton" value="Errores" 
											    					    title="header=[Errores de cancelaci�n] body=[Registros con errores despu�s de realizar el proceso de cancelaci�n. Asegurese de guardar el archivo, ya que despu�s de su generaci�n la informaci�n de errores se elimina de la base.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
													    		</td>
													    		
											    				<td align="center">
											    					<a4j:commandButton styleClass="boton" value="Cancelar" 
																		   action="#{beanCancelacionMasiva.cancelacionMasivaH}" 
																		   reRender="barra"
																		   onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   title="header=[Proceso de cancelaci�n] body=[Ejecuta el proceso de cancelaci�n] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" ajaxSingle="true"/>
																</td>
											    			</tr>
											    		</tbody>
												    	</table>
										    		</td>
									    		</tr>
								    		</tbody>
								    		</table>
								    		</a4j:region>
								    		
									    	<rich:spacer height="15"/>
									    	<table>
									    	<tbody>
									    		<tr>
									    			<td>
									    				<rich:progressBar value="#{beanCancelacionMasiva.progressValue}" id="barra"  
														     enabled= "#{beanCancelacionMasiva.progressHabilitar}" style=" width : 250px;" interval="500" rendered="true" reRenderAfterComplete="tablasPanel">
															<h:outputText value="Procesando Cancelaci�n" />
															<f:facet name="complete">
																<h:outputText value="Proceso Completado" style=" color : #FFFFFF; font-size: 15px; font-weight: bold;"/>
															</f:facet>
														</rich:progressBar>
									    			</td>
									    		</tr>
									    		<tr>
									    			<td align="center">
														<h:outputText id="message" value="#{beanCancelacionMasiva.respuesta}" styleClass="respuesta" />
									    			</td>
									    		</tr>
									    	</tbody>
									    	</table>
									    
									</td>
									<td class="frameCR"></td>
								</tr>
								<tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
							</tbody>
							</table>
							<br/>
						    <table class="botonera">
						    <tbody>
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC">
								    	<div align="center">
								    	<a4j:outputPanel id="tablasPanel">
											<rich:dataTable id="cancelacion" value="#{beanCancelacionMasiva.listaEstatusCancelacion}" var="registro" columns="3" columnsWidth="10px,150px, 20px" width="180px">
												<f:facet name="header"><h:outputText value="Resultado de la emisi�n" /></f:facet>
												<rich:column>
													<f:facet name="header"><h:outputText value="Registro" /></f:facet>
													<h:outputText value="#{registro[0]}" />
												</rich:column>
												<rich:column>
													<f:facet name="header"><h:outputText value="Tabla" /> </f:facet>
													<h:outputText value="#{registro[1]}" />
												</rich:column>
												<rich:column>
													<f:facet name="header"><h:outputText value="Total" /> </f:facet>
													<h:outputText value="#{registro[2]}" />
												</rich:column>
											</rich:dataTable>
											<br/>
											<br/>
										</a4j:outputPanel>
										</div>
								    </td>
									<td class="frameCR"></td>
							   	</tr>
							   	
							   	<tr>
								    <td class="frameCL"></td>
								    <td class="frameC">
								        
									    	<div align="center">
												<rich:dataTable id="cifrasControl" value="#{beanCancelacionMasiva.cifrasControl}" var="cifras" columns="1" columnsWidth="50px" width="300px">
													<rich:column style="text-align: center">
													    <f:facet name="header"><h:outputText value="Cifras Control Archivo:" /></f:facet>
														<h:outputText value="#{cifras}"/>
													</rich:column>													
												</rich:dataTable>
												<br/>
												<br/>
											</div>
								    </td>
									<td class="frameCR"></td>
							    </tr>
							   	<tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
						   </div>
					    </h:form>
					</td>
				</tr>
			</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"></rich:spacer>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>