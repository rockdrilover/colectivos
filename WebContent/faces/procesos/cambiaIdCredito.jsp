<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Procesos
- Fecha: 22/11/2020
- Descripcion: Pantalla para Cambiar de ID Credito
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallavii}" /></title>
	</head>
	
	<body onload="muestraOcultaSeccion('resultado', 0);">
	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemavii}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallavii}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmCambiaCredito">
										<h:inputHidden value="#{beanCambiaCredito.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanCambiaCredito.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemafiltro}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemacre}:" />
																		</td>
																		<td class="left80">
																			<h:inputText id="nombreGpo" value="#{beanCambiaCredito.credito}" styleClass="wid250"  />
																		</td>
																	</tr>
																	
																	<tr class="texto_normal">
																		<td class="right-div" colspan="2"> 
																			<a4j:commandButton
																				styleClass="boton" value="Consultar"
																				action="#{beanCambiaCredito.consulta}"
																				onclick="this.disabled=true"
																				oncomplete="this.disabled=false;
																							muestraOcultaSeccion('resultado', 0);"
																				reRender="mensaje,secResultados,regTabla"
																				title="header=[Consulta Informacion] body=[Consulta los certificados para poder realizar cambio de credito.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</td>
																	</tr>

																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen90">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<tr class="texto_normal" id="tr_sec_resultado">
																		<td>
																			<c:if
																				test="${beanCambiaCredito.listaCertificados == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanCambiaCredito.listaCertificados  != null}">
																				<rich:dataTable id="listaCertificados" value="#{beanCambiaCredito.listaCertificados }"
																						columnsWidth="8px,8px,12px,12px,20px,20px,5px"
																						var="beanRegistro" rows="15" width="100%">
																					<f:facet name="header">
																						<h:outputText value="#{msgs.cambiocredito}" />
																					</f:facet>

																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.sistemacanal}" /></f:facet>
																						<h:outputText value="#{beanRegistro.id.cedaCasuCdSucursal}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.sistemaramo}" /></f:facet>
																						<h:outputText value="#{beanRegistro.id.cedaCarpCdRamo}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.sistemapoliza}" /></f:facet>
																						<h:outputText value="#{beanRegistro.id.cedaCapoNuPoliza}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.sistemacerti}" /></f:facet>
																						<h:outputText value="#{beanRegistro.id.cedaNuCertificado}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cambiocreditoid1}" /></f:facet>
																						<h:outputText value="#{beanRegistro.cedaNombreAnt}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cambiocreditoid2}" /></f:facet>
																						<h:outputText value="#{beanRegistro.cedaNombreNvo}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" 
					                    													id="detallelink" 
					                    													reRender="listaDetalle" 							                    													 			                    												
					                    													oncomplete="#{rich:component('detPanel')}.show();
					                    															muestraOcultaSeccion('resultado', 0);">
					                        													<h:graphicImage value="../../images/edit_page.png" styleClass="bor0" />                        												
					                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanCambiaCredito.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="detallelink" value="Cambiar ID's" />
																                	</rich:column>
																					
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaCertificados" maxPages="10" page="#{beanCambiaCredito.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>


										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<rich:modalPanel id="detPanel" autosized="true" width="360" height="250">
			<div class="scroll-div">
	   		<rich:toolTip id="toolTip" mode="ajax" value="Cerrar Pantalla." direction="top-right" />
	   		
	       	<f:facet name="header"><h:outputText value="Confirmar Cambio" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl attachTo="hidelinkNew" operation="show" for="toolTip" event="onmouseover" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="datosCambio">
	            <rich:messages styleClass="errorMessage"></rich:messages>
	            <h:panelGrid columns="1" width="95%">
	            	<a4j:outputPanel id="panelDet" ajaxRendered="true">
		            	<h:panelGrid columns="5">	            		
		            		<h:outputText value="#{msgs.endosodatosdatos}:   "/>
		            		<h:outputText value="#{beanCambiaCredito.seleccionado.id.cedaCasuCdSucursal}-" styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanCambiaCredito.seleccionado.id.cedaCarpCdRamo}-" styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanCambiaCredito.seleccionado.id.cedaCapoNuPoliza}-" styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanCambiaCredito.seleccionado.id.cedaNuCertificado}" styleClass="texto_mediano"/>
		            	</h:panelGrid>
		            	
		            	<rich:separator height="4" lineType="double" />
		                
		                <h:panelGrid columns="1">
			                <rich:spacer height="10px" />
			                <rich:spacer height="10px" />
			                
			                <h:outputText value="#{msgs.cambiocreditomsg}"/>
		                </h:panelGrid>
		                
		                <h:panelGrid columns="2">
		                	<rich:spacer height="10px" />
		                	<rich:spacer height="10px" />
		                
		                	<h:outputText value="#{msgs.cambiocreditoid1}:   "/>
	                    	<h:outputText value="#{beanCambiaCredito.seleccionado.cedaNombreAnt}" styleClass="texto_mediano"/>
							
							<h:outputText value="#{msgs.cambiocreditoid2}:   "/>
	                    	<h:outputText value="#{beanCambiaCredito.seleccionado.cedaNombreNvo}" styleClass="texto_mediano"/>
		                </h:panelGrid>
		                
		                <rich:spacer height="10px" />
		                
		                <h:panelGrid columns="2">
		                	<a4j:commandButton value="  Si  "
		                		action="#{beanCambiaCredito.cambiar}"
							    reRender="mensaje,regTabla,listaCertificados"
							    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } muestraOcultaSeccion('resultado', 0);" />
							
							<a4j:commandButton value="  No  "
							    reRender="mensaje,regTabla,listaCertificados"
							    onclick="#{rich:component('detPanel')}.hide();"	                    
							    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } muestraOcultaSeccion('resultado', 1);" />
		                </h:panelGrid>	                	               				
					</a4j:outputPanel>
	            </h:panelGrid>
	        </h:form>
	        </div>
	   	</rich:modalPanel>
	   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>