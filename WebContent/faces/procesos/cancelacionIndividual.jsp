<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Cancelaci�n individual</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Cancelaci�n individual</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>
				    
				    <rich:spacer height="10px"></rich:spacer>
				    <rich:dragIndicator id="indicador" />
				    
				    <h:form id="cancelacion">
				    <div align="center">
				    <a4j:region id="consulta">
				    <table class="botonera">
				    <tbody>
					    <tr>
						    <td class="frameTL"></td>
						    <td class="frameTC"></td>
						    <td class="frameTR"></td>
					    </tr>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC">
							    <table class="tabla" >
							    	<tr>
							    		<td width="5%">&nbsp;</td>
							    		<td>
							    			<table>
							    				<tr>
													<td width="5%">&nbsp;</td>
													<td align="center"> Criterio de busqueda: </td>
													<td align="left">
										    			<h:selectOneMenu value="#{beanCancelacionIndividual.criterioBusqueda}">
															<f:selectItem itemValue="0" itemLabel="Seleccione un criterio de busqueda"/>
															<f:selectItem itemValue="1" itemLabel="Tarjeta de Credito"/>
															<f:selectItem itemValue="2" itemLabel="BUC"/>
															<f:selectItem itemValue="3" itemLabel="Cr�dito"/>
															<f:selectItem itemValue="4" itemLabel="Canal/Ramo/Poliza/Certificado"/>
															<f:selectItem itemValue="5" itemLabel="Nombre"/>
															<a4j:support event="onchange"  action="#{beanCancelacionIndividual.muestraCriterio}" reRender="panelCriterios1,panelCriterios2,panelCriterios3,panelCriterios4,panelCriterios5,panelPolizaEspecifica"/>
														</h:selectOneMenu>
													</td>
													<td width="5%">&nbsp;</td>
												</tr>		
												<tr><td colspan="4">&nbsp;</td></tr>
												<tr>
													<td width="5%">&nbsp;</td>
													<td colspan="2" align="left">
														<h:panelGrid id="panelCriterios1" columns="2" style="display:#{beanCancelacionIndividual.display1};">
															<h:outputText value="Tarjeta de Cr�dito:" />
															<h:inputText id="tdc"  value="#{beanCancelacionIndividual.tdc}"/>
														</h:panelGrid>
														
														<!---------- panelCriterios 2-->
														<h:panelGrid id="panelCriterios2" columns="2" style="display:#{beanCancelacionIndividual.display2};">
															<h:outputText value="BUC:" />
															<h:inputText id="buc"  value="#{beanCancelacionIndividual.buc}"/>
														</h:panelGrid>
														
														<!------- panelCriterios 3-->										
														<h:panelGrid id="panelCriterios3" columns="2" style="display:#{beanCancelacionIndividual.display3};">
															<h:outputText value="Cr�dito:" />
															<h:inputText id="identificador"  value="#{beanCancelacionIndividual.identificador}"/>
														</h:panelGrid>
													
														<!------------ panelCriterios 4-->										
														<h:panelGrid id="panelCriterios4" columns="3" style="display:#{beanCancelacionIndividual.display4};" >
															<h:outputText value=" "/> 
															<h:outputText value="Ramo:" />
															<h:selectOneMenu id="ramo" value="#{beanCancelacionIndividual.inRamo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
																<f:selectItem itemValue="0" itemLabel="Ramo" />
																<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
															</h:selectOneMenu>
														
															<h:outputText value=" "/> 
															<h:outputText value="Poliza:"/>
															<h:selectOneMenu id="polizas"  value="#{beanCancelacionIndividual.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
																	<f:selectItem itemValue="0" itemLabel="Seleccione una p�liza"/>
																	<f:selectItem itemValue="1" itemLabel="Prima �nica"/>
																	<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																	<a4j:support event="onchange" action="#{beanCancelacionIndividual.muestraPE}" ajaxSingle="true"></a4j:support>
															</h:selectOneMenu>
															
															<h:outputText value=" "/> 
															<h:outputText value="Poliza Especifica:"/>
															<h:inputText id="polEspecifica"  value="#{beanCancelacionIndividual.polizaEspecifica}"/>
															
															<h:outputText value=" "/> 
															<h:outputText value="Certificado:" />
															<h:inputText id="certificado"  value="#{beanCancelacionIndividual.certificado}"/>
														</h:panelGrid> 	
													
														<!------------ panelCriterios 5-->
														<h:panelGrid id="panelCriterios5" columns="2" style="display:#{beanCancelacionIndividual.display5};">
															<h:outputText value="Nombre:" />
															<h:inputText id="nombre"  value="#{beanCancelacionIndividual.nombre}"/>
														
															<h:outputText value="Apellido Paterno:" />
															<h:inputText id="ap"  value="#{beanCancelacionIndividual.apellidoPaterno}"/>
														
															<h:outputText value="Apellido Materno:" />
															<h:inputText id="am"  value="#{beanCancelacionIndividual.apellidoMaterno}"/>
														</h:panelGrid>
													</td>
													<td width="5%">&nbsp;</td>
												</tr>	
							    			</table>
							    		</td>
							    		<td width="5%">&nbsp;</td>
							    		<td>
											<a4j:commandButton styleClass="boton" id="btnConultar"
						     					action="#{beanCancelacionIndividual.consultaCertificados}" value="Consultar" reRender="listaCertificados,opciones"
						     					onclick="this.disabled=true" oncomplete="this.disabled=false" 
												title="header=[Consulta certificados] body=[Consulta los criterios con base al filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" ></a4j:commandButton>
										</td>
							    	</tr>
									<tr><td colspan="4">&nbsp;</td></tr>
					            </table>
						    	<table>
						    	<tbody>
						    		<tr>
						    			<td align="center">
							    			<a4j:status for="consulta" stopText=" " id="estatusCon">
												<f:facet name="start">
													<h:graphicImage value="/images/ajax-loader.gif" />
												</f:facet>
											</a4j:status>
											<rich:spacer height="20px"></rich:spacer>
											<div align="center" >
												<h:outputText id="respuesta1" value="#{beanCancelacionIndividual.respuesta}" styleClass="respuesta" />
											</div>
										
										</td>
									</tr>
								</tbody>
								</table>
				            </td>
						    <td class="frameCR"></td>
					    </tr>
					    <tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
				    </tbody>
				    </table>
				    </a4j:region>
				    <br/>
				    <a4j:region id="cancelaciones">
				    <table class="botonera">
				    <tbody>
					    <tr>
						    <td class="frameTL"></td>
						    <td class="frameTC"></td>
						    <td class="frameTR"></td>
					    </tr>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC">	
						    <rich:panel styleClass="panel">
						    <rich:dataTable id="listaCertificados" value="#{beanCancelacionIndividual.listaCertificados}" var="beanClienteCertif" columns="13" width="700px" rows="5">
					      		<f:facet name="header">
					      			<rich:columnGroup>
					      				<rich:column colspan="13">
					      					<h:outputText  value="Consulta de Certificados" />
					      				</rich:column>
					      				<rich:column breakBefore="true" rowspan="2"><h:outputText value="ID" /></rich:column>
					      				<rich:column rowspan="2"><h:outputText value="Nombre" /> </rich:column>
					      				<rich:column colspan="4"><h:outputText value="Datos certificado" /></rich:column>
					      				<rich:column rowspan="2"><h:outputText value="Emisi�n" /></rich:column>
					      				<rich:column rowspan="2"><h:outputText value="Estatus" /></rich:column>
					      				<rich:column rowspan="2"><h:outputText value="No.Tarjeta" /></rich:column>
					      				<rich:column rowspan="2"><h:outputText value="Importe" /></rich:column>
					      				<%--<rich:column rowspan="2"><h:outputText value="Devolver" /></rich:column>--%>
					      				<rich:column rowspan="2"><h:outputText value="Cancelar" /></rich:column>
					      				<rich:column rowspan="2"><h:outputText value="Observaciones 1" /></rich:column>
					      				<rich:column rowspan="2"><h:outputText value="Observaciones 2" /></rich:column>
					      				<rich:column breakBefore="true"><h:outputText value="Canal" /></rich:column>
					      				<rich:column><h:outputText value="Ramo" /></rich:column>
					      				<rich:column><h:outputText value="P�liza" /></rich:column>
					      				<rich:column><h:outputText value="Certificado" /></rich:column>
					      			</rich:columnGroup>
					      			 
					      		</f:facet>
					      		<rich:column> 
						      		<h:outputText value="#{beanClienteCertif.id.coccIdCertificado}" />	      		  			
					      		</rich:column>
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.cliente.cocnNombre} #{beanClienteCertif.cliente.cocnApellidoPat} #{beanClienteCertif.cliente.cocnApellidoMat}" />
					      		</rich:column>
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.id.coceCasuCdSucursal}" /> 
					      		</rich:column>
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.id.coceCarpCdRamo}" />
					      		</rich:column>
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.id.coceCapoNuPoliza}" />
					      		</rich:column>
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.id.coceNuCertificado}" />
					      		</rich:column>
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.coceFeEmision}" >
					      				<f:convertDateTime pattern="dd/MM/yyyy"/>
					      			</h:outputText>
					      		</rich:column>
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.estatus.alesCdEstatus}-#{beanClienteCertif.certificado.estatus.alesDescripcion}" />
					      		</rich:column>
					      		
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.coceNuCuenta}" /> <%--tarjeta de credito --%>
					      		</rich:column>
					      		
					      		<rich:column>
					      			<h:outputText value="#{beanClienteCertif.certificado.coceMtPrimaSubsecuente}" id="prima" /> <%--importe --%>
					      		</rich:column>
					      		 
					      		<%-- <rich:column>
					      			<h:selectBooleanCheckbox value="#{beanClienteCertif.check}" />
					      		</rich:column>--%>
					      		
					      		<rich:column>
					      			<h:selectBooleanCheckbox value="#{beanClienteCertif.check}" id="elCheck">
					      				<a4j:support event="onclick" ajaxSingle="true"  
					      					reRender="opciones,noSin,sinpro" 
					      					oncomplete="javascript:checkOneSelect(this);"
					      					action="#{beanCancelacionIndividual.verificaValDev}">
					      					<f:setPropertyActionListener value="#{beanClienteCertif.certificado.coceSubCampana}" target="#{beanCancelacionIndividual.idVentaSel}" />
					      				</a4j:support>
					      			</h:selectBooleanCheckbox>
					      		</rich:column>
					      		
					      		<rich:column>
					      		    <h:inputTextarea id="Ob1" value="#{beanClienteCertif.certificado.coceObservaciones}" />
					      		</rich:column>
					      		
					      		 
					      		<rich:column>
					      			<h:inputTextarea id="Ob2" value="#{beanClienteCertif.observaciones2}" />
					      		</rich:column>
					      		
					      		<f:facet name="footer">
					      			<rich:datascroller align="center" for="listaCertificados" maxPages="10" page="#{beanCancelacionIndividual.numPagina}" ></rich:datascroller>
					      		</f:facet>
					      	</rich:dataTable>
					      	</rich:panel>
						    </td>
						    <td class="frameCR"></td>
					    </tr>
					    <tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
				    </tbody>
				    </table>
					<rich:spacer height="10px"></rich:spacer>
					<table class="botonera">
				    <tbody>
					    <tr>
						    <td class="frameTL"></td>
						    <td class="frameTC"></td>
						    <td class="frameTR"></td>
					    </tr>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC">	
						    <table width="600px" cellpadding="2">
						    <tbody>
						       <tr>
						        <td width="5%"/>
						       	 <td width="45%">
									<table>
									    <tr>
									    	<td>
									    		Causa anulaci�n: 
									    	</td>
									    </tr>
										<tr>							    		
								    		<td align="center">
								    			<h:selectOneMenu value="#{beanCancelacionIndividual.causaAnulacion}">
													<f:selectItem itemValue="" itemLabel="<Seleccione una causa de anulaci�n>"/>
													<f:selectItems value="#{beanListaEstatus.listaEstatusCancelacion}"/>
												</h:selectOneMenu>
												<br/>
												<br/>
								    		</td>
								    	</tr>
								    	<tr>
								    		<td>
											    <a4j:region id="regionOpciones">
										    	    <rich:panel id="opciones" header="Opciones" style=" width: 100%; align:center; background-color:#E5E0EC" styleClass="tablaSecundaria">
										    	        <f:facet name="header"><h:outputText value="Opciones de Cancelaci�n" /></f:facet>
										    	        <table>
													    <tbody>
													    	<tr>
											    			    <td align="left">
											    			    	<h:selectManyCheckbox id="opc" value="#{beanCancelacionIndividual.opciones}" layout="pageDirection">
																		<f:selectItem itemLabel="Aplicar Cancelaci�n:" itemValue="1" />
																		<f:selectItem itemLabel="Calcular Devoluci�n:" itemValue="2" />
																		<f:selectItem itemLabel="Validar Devoluci�n:"  itemValue="3" itemDisabled="#{beanCancelacionIndividual.habValDev}"/>
																		<a4j:support event="onchange"  ajaxSingle="true"  reRender="opcDevo" action="#{beanCancelacionIndividual.cambioOpciones}"/>
																	</h:selectManyCheckbox>
																</td>
																<td>
																<f:selectItem itemLabel="Calcular Devoluci�n" itemValue="2" />
																		<f:selectItem itemLabel="Validar Devoluci�n"  itemValue="3" />
																	<rich:panel id="opcDevo"  header="opcDevo" style=" width: 100%; align:center; background-color:#E5E0EC" styleClass="tablaSecundaria">
										    	        				<f:facet name="header"><h:outputText value="Devoluci�n" /></f:facet>
										    	        				<table>
													    				<tbody>
													    					<tr>
											    			    				<td align=center>
											    			    					Tasa Amortiza. Mensual
																				</td>
																			</tr>
																			<tr>
											    			    				<td align=center>
											    			    					<h:inputText id="tasaAmortiza"  value="#{beanCancelacionIndividual.tasaAmortiza.copaNvalor7}" />
																				</td>
																			</tr>
																			<tr>
																				<td align="center">
											    			    					Monto Devolucion
																				</td>
																			</tr>
																			<tr>
											    			    				<td align=center>
											    			    					<h:inputText id="montoDevo"  value="#{beanCancelacionIndividual.primaDevuelta}"/>
																				</td>
																			</tr>
																		</tbody>
																	    </table>
																	</rich:panel>
																</td>
											    			</tr>
											    			<tr>
											    				<td colspan="2" align="left">
											    					<h:selectBooleanCheckbox id="c1"  value="#{beanCancelacionIndividual.chkValPmaDev}" />
											    					<h:outputText value="Validar Prima a Devolver (Banco vs Sistema)"/> 
											    				</td>
											    			</tr>
											    		</tbody>
											    		</table>
											    	</rich:panel>
											    </a4j:region>
							    			</td>
								    	</tr>
								    	<tr>
								    		<td>
								    			La devoluci�n solo aplicar� para p�liza de tipo Prima �nica.
								    		</td>
								    	</tr>
									</table>						       	 
						       	 </td>
						       	 <td width="10%"/>
						       	 <td align="left" width="30%">
						       	    Fecha anulaci�n:
						    	  	<rich:calendar value="#{beanCancelacionIndividual.fechaAnulacion}" inputSize="10" 
					    				popup="false" cellHeight="20px"  
					    				cellWidth="20px" datePattern="dd/MM/yyyy" styleClass="calendario">
					    			</rich:calendar>
						    	  </td>
						    	  <td/>
						       </tr>
						       <tr>
						         <td colspan="5">
						            <br/>
						       		<table>
						       			<caption></caption>
						    				<tr>
						    					<td align="center">
						    					    <a4j:commandButton action="#{beanCancelacionIndividual.suma}" 
						    					    		styleClass="boton" 
						    					    		value="Cancelar" 
						    					    		reRender="tQty,tTotal,noSin"
						    					    		onclick="this.disabled=true"
						    					    		oncomplete="panelAvisoCancelacion();this.disabled=false;"
						    					    		id="hdnBtnCan"
						    					    		title="header=[Cancelaci�n] body=[Cancela los certificados seleccionados] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
													<h:inputHidden id="noSin" rendered="true" value="#{beanCancelacionIndividual.tasaAmortiza.copaVvalor2}"></h:inputHidden>
									    		</td>
						    				</tr>
						    				<tr>
								    			<td align="center">
									    			<a4j:status for="cancelaciones" stopText=" ">
														<f:facet name="start">
															<h:graphicImage value="/images/ajax-loader.gif" />
														</f:facet>
													</a4j:status>
													<rich:spacer height="20px"></rich:spacer>
													<div align="center" >
														<h:outputText id="respuesta2" value="#{beanCancelacionIndividual.respuesta}" styleClass="respuesta" />
													</div>
												</td>
												
											</tr>
						    			</table>
						    		</td>
						    	</tr>
						    </tbody>
						    </table>
						    <table>
						    <tbody>
						    	<tr>
						    		<td colspan="4" align="center">
						    			<rich:panel styleClass="panel">
							    			<rich:dataTable style="text-align:center" id="listaCertificadosCancel" align="center" value="#{beanCancelacionIndividual.listaCertificadosCancelacion}" var="beanClienteCertif" columns="9" rows="5" width="700px">
							    				<f:facet name="header">
							    					<rich:columnGroup>
									      				<rich:column colspan="9">
									      					<h:outputText  value="Certificados cancelados" />
									      				</rich:column>
									      				<rich:column breakBefore="true" rowspan="2"><h:outputText value="ID" /></rich:column>
									      				<rich:column colspan="4"><h:outputText value="Datos certificado" /></rich:column>
									      				<rich:column rowspan="2"><h:outputText value="Fecha Cancelaci�n" /></rich:column>
									      				<rich:column rowspan="2"><h:outputText value="Devoluci�n Calculada" /></rich:column>
									      				<rich:column rowspan="2"><h:outputText value="Devoluci�n Reportada" /></rich:column>
									      				<rich:column rowspan="2"><h:outputText value="Observaciones" /></rich:column>
									      				<rich:column breakBefore="true"><h:outputText value="Canal" /></rich:column>
									      				<rich:column><h:outputText value="Ramo" /></rich:column>
									      				<rich:column><h:outputText value="P�liza" /></rich:column>
									      				<rich:column><h:outputText value="Certificado" /></rich:column>
									      			</rich:columnGroup>
							    				</f:facet>	
							    				<rich:column> 
										      		<h:outputText value="#{beanClienteCertif.id.coccIdCertificado}" />		      		  			
									      		</rich:column>
									      		<rich:column>
									      			<h:outputText value="#{beanClienteCertif.certificado.id.coceCasuCdSucursal}"/> 
									      		</rich:column>
									      		<rich:column>
									      			<h:outputText value="#{beanClienteCertif.certificado.id.coceCarpCdRamo}" />
									      		</rich:column>
									      		<rich:column>
									      			<h:outputText value="#{beanClienteCertif.certificado.id.coceCapoNuPoliza}" />
									      		</rich:column>
									      		<rich:column>
									      			<h:outputText value="#{beanClienteCertif.certificado.id.coceNuCertificado}" />
									      		</rich:column>
									      		<rich:column>
									      			<h:outputText value="#{beanClienteCertif.certificado.coceFeAnulacionCol}">
									      				<f:convertDateTime pattern="dd/MM/yyyy"/>
									      			</h:outputText>
									      		</rich:column>
									      		<rich:column>
									      			<h:outputText value="#{beanClienteCertif.certificado.coceMtDevolucion}" />
									      		</rich:column>
									      		<rich:column>
									      			<h:outputText value="#{beanClienteCertif.certificado.coceMtBcoDevolucion}" />
									      		</rich:column>
									      		<rich:column style="text-align:left">
									      		    <h:graphicImage value="../../images/Select.png" height="7%" width="7%" rendered="#{!not empty beanClienteCertif.certificado.coceCampov5}"/>
									      		    <h:graphicImage value="../../images/Error.png" height="7%" width="7%" rendered="#{not empty beanClienteCertif.certificado.coceCampov5}"/>
									      		    &nbsp
									      		    <h:outputText value="Cancelaci�n Correcta"  rendered="#{!not empty beanClienteCertif.certificado.coceCampov5}"/>
									      			<h:outputText value="#{beanClienteCertif.certificado.coceCampov5}" />
									      		</rich:column>
									      		<f:facet name="footer">
									      			<rich:datascroller align="center" for="listaCertificadosCancel" maxPages="10" page="#{beanCancelacionIndividual.numPaginaC}"></rich:datascroller>
									      		</f:facet>
							    			</rich:dataTable>
						    			</rich:panel>
						    		</td>
						    	</tr>
						    </tbody>
						    </table>
						    </td>
						    <td class="frameCR"></td>
					    </tr>
					    <tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
				    </tbody>
				    </table>
				    </a4j:region>				
			     	
			     	<%@include file="/WEB-INF/includes/panelesCancelacion.jsp"%>
			     	</h:form>
					</div>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>


<div align="center">
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>