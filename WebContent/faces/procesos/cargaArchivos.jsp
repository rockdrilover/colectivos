<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Carga archivos ENLACE</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
			<table class="tablaSecundaria">
				<tbody>
					<tr>
						<td align="center">
							<div align="center">
							<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Carga archivos ENLACE</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
							</table>
						    </div>
						    <rich:spacer height="2px"></rich:spacer>
						    <h:form id="frmCargaEnlace">
							    <div align="center">
							    <rich:spacer height="15"/>	
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">	
									    <a4j:region id="filtroCarga">		    
									    <table>
									    <tbody>
									    	<tr>
									    		<td align="right" width="24%" height="30">
									    			<h:outputText value="Ramo:" />
									    	</td>
									    		<td align="left" width="76%" height="30">
													<h:selectOneMenu id="inRamo" value="#{beanListaPreCarga.inRamo}" title="header=[Ramo] body=[Seleccione un ramo para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItem id="inRamo5" itemLabel="5 - SEGURO COLECTIVO ACCIDENTES PERSONALES" itemValue="5" />
														<f:selectItem id="inRamo65" itemLabel="65 - SEGURO COLECTIVO VIDA" itemValue="65" />
														<f:selectItem id="inRamo82" itemLabel="82 - SAFE COLECTIVO" itemValue="82" />
													</h:selectOneMenu>
													<h:message for="inRamo" styleClass="errorMessage" />
												</td>
									    	</tr>
									    </tbody>
									    </table>
									    </a4j:region>
								    	<table>
							    		<tbody>
							    			<tr>
							    				
							    				<td align="right" width="24%" height="30"><h:outputText value="Cargar archivo:" /></td>
							    				<td align="left" width="76%" height="30" title="header=[Archivo] body=[Archivo a cargar extensi�n CSV o TXT, cualquier otra extensi�n el sistema no lo tomar� en cuenta).] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
							    					<rich:fileUpload fileUploadListener="#{beanListaPreCarga.listener}" 
					    								addControlLabel="Examinar" uploadControlLabel="Cargar" maxFilesQuantity="5" stopControlLabel="Detener" immediateUpload="true" 
					    								clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" styleClass="archivo" acceptedTypes="csv,txt"
					    								listHeight="65px" addButtonClass="botonArchivo" listWidth="90%">
					    								<a4j:support event="onuploadcomplete" reRender="mensaje" />
							    						<f:facet name="label">
							    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
							    						</f:facet>
							    					</rich:fileUpload>
							    				</td>
							    			</tr>
							    		</tbody>
								    	</table>
								    	<rich:spacer height="30px"></rich:spacer>
									    <table>
									    	<tbody>
									    		<tr>
								    				<td align="center">
														<h:commandButton action="#{beanListaPreCarga.cargaArchivoEnlace}" value="Descargar LayOut" 
                                                                                                                     title="header=[Descargar] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
													</td>
													
									    		</tr>
									    	</tbody>
								    	</table>
								    	<br/>
									    </td>
									    <td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
							    </table>
								</div>
						    </h:form>
						    <rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>