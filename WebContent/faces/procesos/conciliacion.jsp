<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<title>Conciliaci�n</title>
		
		<style>
			.filtroR {text-align: right;}
			.filtroL {text-align: left;}
			.top {vertical-align: top; width: 25%;}
			.info {height: 250px; width: 100%; overflow: auto;}
			.paneles {width: 90%;}
			.texto_normal {font-weight: bold; font-size: 11px}
		</style>
	</head>
	
	<body>
	<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
	<div align="center"><%@include file="../header.jsp" %></div>
	
	<div align="center">
	<table class="tablaPrincipal">
		<tr>	
			<td align="center">
				<table class="tablaSecundaria">
					<tr>
						<td align="center">
							<table class="encabezadoTabla" >
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Conciliaci�n</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%"><input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7"></td>
								</tr>
							</table>
						    		
						    <rich:spacer height="2px"></rich:spacer>
						    <h:form id="frmConciliar">
							<table class="botonera">
								<tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
								<tr>
									<td class="frameCL"></td>
									<td class="frameC">	
										<a4j:region id="filtroConciliar">		    
									    <table width="80%">
									    	<tr>
												<td align="center" width="60%">
													<h:outputText id="mensaje" value="#{beanConciliar.respuesta}" styleClass="respuesta"/>
												</td>
												<td align="center" width="30%">
											    	<a4j:status for="filtroConciliar" stopText=" ">
														<f:facet name="start"><h:graphicImage value="/images/ajax-loader.gif" /></f:facet>
													</a4j:status>
												</td>
												<td width="10%">&nbsp;</td>
											</tr>
											<tr><td colspan="3">&nbsp;</td></tr>
											<tr>
												<td align="right" colspan="2">
													<a4j:commandButton styleClass="boton" value="Conciliar" 
															action="#{beanConciliar.conciliar}" 
															onclick="this.disabled=true" oncomplete="this.disabled=false"
															reRender="mensaje"
															title="header=[Conciliar] body=[Concilia la informacion cargada, segun el Origen.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
															
													<a4j:commandButton styleClass="boton" value="Agregar Creditos" 
															action="#{beanConciliar.agregarCreditos}" 
															onclick="this.disabled=true" oncomplete="this.disabled=false" 
															reRender="mensaje, regResultados"
															title="header=[Agregar Creditos] body=[Agrega los creditos a una tabla para despues poder descargar el Lay Out, segun el Origen.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
												</td>
												<td>&nbsp;</td>
											</tr>
											<tr><td colspan="3">&nbsp;</td></tr>
								    		<tr>
								    			<td colspan="3" >
								    				<rich:panel styleClass="paneles">
								               			<f:facet name="header"><h:outputText value="Opciones de conciliacion" /></f:facet>
								               			<h:panelGrid columns="2" columnClasses="filtroR, filtroL" >																							          
											        		<h:outputText value="Tipo Conciliacion:" styleClass="texto_normal"/>
											        		<h:selectOneRadio id="opcion" 
																	value="#{beanConciliar.nTipoConciliacion}" 
																	title="Selecciona una opci�n" 
																	layout="lineDirection">
																<f:selectItem itemLabel="Conciliacion Automatica" itemValue="1" />
																<f:selectItem itemLabel="Conciliacion Manual" itemValue="2" />
															</h:selectOneRadio>	
											        		
											        		<h:outputText value=""/><h:outputText value=""/>
											        		
											        		<h:outputText value="Origen:          " styleClass="texto_normal"/>														        	
															<h:selectOneMenu id="inOrigen" value="#{beanConciliar.nOrigen}" 
																	title="header=[Origen] body=[Seleccione el origen] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
																	required="true">
																<f:selectItem itemValue="0" itemLabel="Seleccione el origen" />
																<f:selectItems value="#{beanConciliar.listaComboOrigen}"/>																
															</h:selectOneMenu>
															
															<h:outputText value="Numero Creditos:          " styleClass="texto_normal"/>
															<h:inputTextarea value="#{beanConciliar.strCreditos}"  style="width: 230px; height: 50px" />																
											        	</h:panelGrid>
								               		</rich:panel>
												</td>
								    		</tr>
										</table>
									    		
										<a4j:outputPanel id="regResultados" ajaxRendered="true" >	
										<c:if test="${beanConciliar.lstResultados != null}">							    		
										<rich:panel styleClass="panelesResultados">
											<f:facet name="header"><h:outputText value="Resultados" /></f:facet>
											<table>
												<tr><td colspan="2">&nbsp;</td></tr>
												
												<tr>
													<td>
													<c:if test="${beanConciliar.nOrigen == 1}">
														<h:outputText value="Tipo Lay Out:" styleClass="texto_normal"/>
														<h:selectOneRadio id="optTipoLay" 
																value="#{beanConciliar.nLayOutPmaTrad}" 
																title="Selecciona una opci�n" 
																layout="lineDirection">
															<f:selectItem itemLabel="61 - Vida" itemValue="1" />
															<f:selectItem itemLabel=" 9 - Desempleo" itemValue="2" />
														</h:selectOneRadio>	
													</c:if>
													<c:if test="${beanConciliar.nOrigen == 6}">
														<h:outputText value="Tipo Lay Out:" styleClass="texto_normal"/>
														<h:selectOneRadio id="optTipoLayGap"
																value="#{beanConciliar.nLayOutGap}"
																title="Selecciona una opci�n"
																layout="lineDirection">
															<f:selectItem itemLabel="89 - GAP Colectivo" itemValue="1"/>
															<f:selectItem itemLabel="61 - Vida" itemValue="2"/>
														</h:selectOneRadio>
													</c:if>
													
													</td>
													
													<td>
														<h:commandButton action="#{beanConciliar.getArchivo}" value="Descargar" 
															title="header=[Descarga Archivos] body=[Descarga el Lay Out correspondiente, un archivo con extension .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" /> 
													</td>
												</tr>
												
												<tr><td colspan="2">&nbsp;</td></tr>
										    	<tr id="trResultados">
										    		<td colspan="2">
														<rich:dataTable value="#{beanConciliar.lstResultados}" id="taRespuesta" var="resultado" columns="3" columnsWidth="15%,30%" rows="8" width="80%">
															<f:facet name="header">
												      			<rich:columnGroup>
												      				<rich:column colspan="3"><h:outputText value="Creditos a Descargar" /></rich:column>
												      				<rich:column breakBefore="true"><h:outputText value="Numero Credito" /></rich:column>
												      				<rich:column><h:outputText value="Fecha Ingreso" /></rich:column>
												      				<rich:column><h:outputText value="Nombre Cliente" /></rich:column>
												      			</rich:columnGroup>
												      		</f:facet>
												      		 
												      		<rich:column><h:outputText value="#{resultado.numcredito}"/></rich:column>
															<rich:column>
													      		<h:outputText value="#{resultado.fecha}">
													      			<f:convertDateTime pattern="dd/MM/yyyy"/>
													      		</h:outputText>		      		  			
												      		</rich:column>
												      		<rich:column><h:outputText value="#{resultado.nombreCliente}" /></rich:column>
												      		<f:facet name="footer">
												      			<rich:datascroller for="taRespuesta" maxPages="10"/>
												      		</f:facet>
														</rich:dataTable>
													</td>
												</tr>
											</table>
										</rich:panel>
										</c:if>
										</a4j:outputPanel>
										
										</a4j:region>
									</td>
									<td class="frameCR"></td>
								</tr>
								<tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
							</table>
				    		</h:form>
				    		<rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<rich:spacer height="10px"/>
	<div align="center"><%@ include file="../footer.jsp" %></div>
	</f:view>
	</body>
</html>