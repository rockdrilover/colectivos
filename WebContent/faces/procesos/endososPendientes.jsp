<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Endoso Pendientes</title>
</head>
<body>
	<f:view>
 		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" /> 
		<div class="center_div">
			<%@include file="../header.jsp"%>
		</div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaxv}" /></td>
												<td class="encabezado3">Endosos Pendientes por Autorizar</td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" />
												</td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmEndososPendientes">
									<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td>
														<h:outputText id="mensaje" value="#{beanEndososPendientes.respuesta}" styleClass="error" />
													</td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<table class="tablaCen90">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<a4j:outputPanel id="secEndososPen" ajaxRendered="true">
																	<span id="img_sec_endosospen_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('endosospen', 0);" title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_endosospen_o"  style="display: none;"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('endosospen', 1);" title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones">Endosos Pendientes</span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<tr class="texto_normal" id="tr_sec_endosospen">
																		<td>
																		<c:if test="${beanEndososPendientes.lstEndosos == null}">
																			<table class="tablaCen100">
																				<caption></caption>
																				<tr>
																					<td>NO SE ENCONTRARON ENDOSOS DE DATOS PENDIENTES POR AUTORIZAR.</td>
																				</tr>
																			</table>
																			</c:if> 
																			<c:if test="${beanEndososPendientes.lstEndosos != null}">
																				<rich:dataTable value="#{beanEndososPendientes.lstEndosos}" id="listaEndoso" var="beanEndoso" rows="15" headerClass="detalleEncabezado2" styleClass="detalleEncabezado" width="100%">
																	      			<f:facet name="header">
																	      				<h:outputText value="Endosos Pendientes" />																	      			
																	      			</f:facet>
																	      		
																		       		<rich:column width="15%" style="text-align:center; border:0">
																		       			<f:facet name="header"><h:outputText value="#{msgs.sistemacerti}" /></f:facet>
																						<h:outputText value="#{beanEndoso.id.cedaCasuCdSucursal}" />-
																						<h:outputText value="#{beanEndoso.id.cedaCarpCdRamo}" />-
																						<h:outputText value="#{beanEndoso.id.cedaCapoNuPoliza}" />-
																						<h:outputText value="#{beanEndoso.id.cedaNuCertificado}" />
																					</rich:column>
																		      		<rich:column width="15%" style="text-align:center; border:0">
																		      			<f:facet name="header"><h:outputText value="#{msgs.sistemafolio}" /></f:facet>
																						<h:outputText value="#{beanEndoso.cedaCampov1}" />																				
																					</rich:column>
																					<rich:column width="20%" style="text-align:center; border:0">
																						<f:facet name="header"><h:outputText value="#{msgs.endosodatosrecep}" /></f:facet>
																						<h:outputText value="#{beanEndoso.cedaFeRecepcion}" >
																							<f:convertDateTime pattern="dd/MM/yyyy HH:mm" />
																						</h:outputText>																				
																					</rich:column>
																					<rich:column width="20%" style="text-align:center; border:0">
																						<f:facet name="header"><h:outputText value="#{msgs.loginusuario}" /></f:facet>
																						<h:outputText value="#{beanEndoso.cedaCdUsuarioReg}" />
																					</rich:column>
																					<rich:column width="15%" style="text-align:center; border:0">
																		      			<f:facet name="header"><h:outputText value="#{msgs.sistematipo}" /></f:facet>
																						<h:outputText value="#{beanEndoso.cedatpEndoso}" />																					
																					</rich:column>
																					<rich:column width="10%" style="text-align:center; border:0">
																						<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>																					
																						<a4j:commandLink ajaxSingle="true" id="detallelink" oncomplete="#{rich:component('detPanel')}.show()">
					                        												<h:graphicImage value="../../images/certificados.png" style="border:0" />
					                        												<f:setPropertyActionListener value="#{beanEndoso}" target="#{beanEndososPendientes.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="editlink" value="Ver Detalle Endoso" />
																					</rich:column>
																					
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaEndoso" maxPages="10" />
																					</f:facet>
																		    	</rich:dataTable>																			
																			</c:if>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>											
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<%@include file="/WEB-INF/includes/modalDetEndoso.jsp"%>
		
	</f:view>
</body>
</html>