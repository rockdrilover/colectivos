<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Carga y Emision</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
			<table class="tablaSecundaria">
				<tbody>
					<tr>
						<td align="center">
							<div align="center">
							<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Carga y emisi�n</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
							</table>
						    </div>
						    <rich:spacer height="2px"></rich:spacer>
						    <h:form id="frmCargaEmision">
							    <div align="center">
							    <rich:spacer height="15"/>	
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">	
									    <a4j:region id="filtroCarga">		    
									    <table>
									    <tbody>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Ramo:" /></td>
									    		<td align="left" width="76%" height="30">
													<h:selectOneMenu id="inRamo" value="#{beanListaPreCarga.inRamo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
													</h:selectOneMenu>
													<h:message for="inRamo" styleClass="errorMessage" />
												</td>
									    	</tr>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Poliza:"/></td>
												<td align="left" width="76%" height="30">
													<a4j:outputPanel id="polizas" ajaxRendered="true">
														<h:selectOneMenu id="inPoliza"  value="#{beanListaPreCarga.inPoliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																<f:selectItem itemValue="0" itemLabel="Prima �nica carga"/>
																<f:selectItem itemValue="-1" itemLabel="Prima �nica automatica"/>
																<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta" />
														</h:selectOneMenu>
														<h:message for="inPoliza" styleClass="errorMessage" />
													</a4j:outputPanel>
												</td>
									    	</tr>
									    	<tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Canal de Venta:"/></td>
												<td align="left" width="76%" height="30">
													<a4j:outputPanel id="idVenta" ajaxRendered="true">
														<h:selectOneMenu id="inVenta"  value="#{beanListaPreCarga.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
														         disabled="#{beanListaParametros.habilitaComboIdVenta}">
																<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																<a4j:support event="onchange"  action="#{beanListaPreCarga.setValor}" ajaxSingle="true" reRender="idVenta" />
														</h:selectOneMenu>
													</a4j:outputPanel>
												</td>
									    	</tr>
									    </tbody>
									    </table>
									    </a4j:region>
									    <a4j:region id="archivoCarga" >
								    	<table>
							    		<tbody>
							    			<tr>
							    				
							    				<td align="right" width="24%" height="30"><h:outputText value="Cargar archivo:" /></td>
							    				<td align="left" width="76%" height="30" title="header=[Archivo] body=[Archivo a cargar extensi�n CSV, cualquier otra extensi�n el sistema no lo tomar� en cuenta).] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
							    					<rich:fileUpload fileUploadListener="#{beanListaPreCarga.listener}" 
					    								addControlLabel="Examinar" uploadControlLabel="Cargar" maxFilesQuantity="5" stopControlLabel="Detener" immediateUpload="true" 
					    								clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" styleClass="archivo" acceptedTypes="csv,txt"
					    								listHeight="65px" addButtonClass="botonArchivo" listWidth="90%">
					    								<a4j:support event="onuploadcomplete" reRender="mensaje" />
							    						<f:facet name="label">
							    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
							    						</f:facet>
							    					</rich:fileUpload>
							    				</td>
							    			</tr>
							    		</tbody>
								    	</table>
								    	<rich:spacer height="30px"></rich:spacer>
									    <table>
									    	<tbody>
									    		<tr>
								    				<td align="center">
								    					<h:commandButton styleClass="boton" value="Errores" 
															   action="#{beanListaPreCarga.erroresEmision}"
															   title="header=[Errores] body=[Registros con errores despu�s de realizar el proceso de carga y emisi�n.Asegurese de guardar el archivo ya que despu�s de su generaci�n los errores se eliminar�n de la base de datos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
								    				</td>
								    				<!--td align="center">
								    					<a4j:commandButton styleClass="boton" value="Cargar" 
															   action="#{beanListaPreCarga.cargar}" 
															   onclick="this.disabled=true" oncomplete="this.disabled=false" 
															   reRender="emision, mensaje"
															   title="header=[Carga masiva] body=[Carga unicamente la informaci�n en la tabla temporal sin ejecutar el proceso de emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
								    				</td-->  
								    				<td align="center">
								    					<a4j:commandButton styleClass="boton" value="Emitir" 
															   action="#{beanListaPreCarga.preEmision}" 
															   onclick="this.disabled=true"
															   reRender="barra,tMsg,tOperation,respValidacion"
															   oncomplete="llamaEmision();"
															   ajaxSingle="true"
															   id="hdnBtnPre"
															   title="header=[Carga y emisi�n masiva] body=[Ejecuta el proceso de carga y emisi�n, el tiempo de procesamiento depender� del archivo cargado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
														</a4j:commandButton>
														<a4j:commandButton id="hdnBtn" 
														action="#{beanListaPreCarga.emitirh}"
														reRender="barraPro" style="display: none;"
														oncomplete="document.getElementById('frmCargaEmision:hdnBtnPre').disabled = false;"
														 />	   
													</td>
									    		</tr>
									    	</tbody>
									    	</table>
									    	<br/>
									    	<table>
									    	<tbody>
									    		<tr>
									    			<td align="center">
														<rich:progressBar value="#{beanListaPreCarga.progressValue}" id="barraPro"  
														     enabled= "#{beanListaPreCarga.progressHabilitar}" style=" width : 250px;"   interval="500" rendered="true" reRenderAfterComplete="tablasPanel,mensaje">
															<h:outputText value="Procesando Emisi�n" />
															<f:facet name="complete">
																<h:outputText value="Proceso Completado" style=" color : #FFFFFF; font-size: 15px; font-weight: bold;"/>
															</f:facet>
														</rich:progressBar>
													</td>
												</tr>
												<tr>
													<td align="center">
														<br/>
														<h:outputText id="mensaje" value="#{beanListaPreCarga.mensaje}" style=" color : #FFFFFF; font-size: 15px; font-weight: bold;"/>
														<br/>
														<br/>
													</td>
												</tr>
											</tbody>
											</table>
											</a4j:region>
									    </td>
									    <td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
							    </table>
							    <br/>
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC">
									    	<div align="center">
									    	<a4j:outputPanel id="tablasPanel">
												<rich:dataTable id="emision" value="#{beanListaPreCarga.resultado}" var="registro" columns="4" columnsWidth="50px,10px,160px,20px" width="300px">
													<f:facet name="header"><h:outputText value="Resultado de la emisi�n" /></f:facet>
													<rich:column>
														<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
														<h:outputText value="#{registro[2]}" styleClass="polizaAzul"/>
													</rich:column>
													<rich:column filterBy="#{registro[0]}" filterEvent="onkeyup">
														<f:facet name="header"><h:outputText value="Codigo de Registro" /></f:facet>
														<h:outputText value="#{registro[0]}" />
													</rich:column>
													<rich:column>
														<f:facet name="header"><h:outputText value="Concepto" /> </f:facet>
														<h:outputText value="#{registro[1]}" />
														<f:facet name="footer">      
														  <h:outputText value="Resgistros Totales" style="FONT-SIZE: small; FONT-WEIGHT: bold;"/> 
                								    	</f:facet>
													</rich:column>
													
													<rich:column>
														<f:facet name="header"><h:outputText value="Total" /> </f:facet>
														<h:outputText value="#{registro[3]}" />
														<f:facet name="footer">         										
                											<h:outputText value="#{beanListaPreCarga.totalCarga}" style="FONT-SIZE: small; FONT-WEIGHT: bold;"/>
                								    	</f:facet>
													</rich:column>
													
												</rich:dataTable>
												<br/>
											</a4j:outputPanel>
											</div>
									    </td>
										<td class="frameCR"></td>
								    	</tr>
								    	
								    	<tr>
									    <td class="frameCL"></td>
									    <td class="frameC">
									        
										    	<div align="center">
													<rich:dataTable id="cifrasControl" value="#{beanListaPreCarga.cifrasControl}" var="cifras" columns="1" columnsWidth="50px" width="300px">
														<rich:column style="text-align: center">
														    <f:facet name="header"><h:outputText value="Cifras Control Archivo:" /></f:facet>
															<h:outputText value="#{cifras}"/>
														</rich:column>													
													</rich:dataTable>
													<br/>
													<br/>
												</div>
											
									    </td>
										<td class="frameCR"></td>
								    	</tr>
								    	
								    	<tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
							    	</tbody>
							    	</table>												   
								</div>
						    </h:form>		
						    	<h:inputHidden id="barra" rendered="true" value="#{beanListaPreCarga.codigoResp}"></h:inputHidden>
						    	<h:inputHidden id="respValidacion" rendered="true" value="#{beanListaPreCarga.respValidacion}"></h:inputHidden> 
						    	 
						    <rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
<rich:modalPanel id="panelConfirma" width="322" height="94">
      <f:facet name="header">
      		<h:outputText value="#{msgs.confirmar}" />
      </f:facet>
      		�Esta seguro de continuar con la operaci�n?
      		<br/>
      		<h:outputText value="#{beanListaPreCarga.mensaje}" id="tOperation"/>
      		<br/>
      		<h:form id="frmEmite">
      		<table width="100%">
      			<tr>
      				<td align="center">
      					<a4j:commandButton styleClass="boton" 
      						action="#{beanListaPreCarga.emitirh}" 
			  				value="Si"
			  				id="btnEmitir"
			   				onclick="Richfaces.hideModalPanel('panelConfirma');" 
			   				oncomplete="this.disabled=false" 
			   				reRender="barraPro"/> 		  			
							</td>
      						<td width="10%"/>
      							<td align="center">
      								<a onclick="Richfaces.hideModalPanel('panelConfirma');" href="#1"><a4j:commandButton styleClass="boton" value="No"/></a>
      							</td>
      			</tr>
      		</table>
      		</h:form>
		<br/>
		<br/>
		<br/>										    	
	</rich:modalPanel>
	<rich:modalPanel id="panelNoEmision" width="322" height="94">
      <f:facet name="header">
      		<h:outputText value="#{msgs.mensaje}" />
      </f:facet>
      		No se puede Realizar la Emisi�n
      		<br/>
      		<h:outputText value="#{beanListaPreCarga.mensaje}" id="tMsg"/>
      		<br/>
      		<h:form id="frmNoEmision">
      		<table width="100%">
      			<tr>
      				<td width="10%">
      					<a4j:commandButton styleClass="boton" onclick="Richfaces.hideModalPanel('panelNoEmision');" value="Aceptar" style="margin-left: 36%;"/>
      				</td>
      			</tr>
      		</table>
      		</h:form>
		<br/>
		<br/>
		<br/>										    	
	</rich:modalPanel>
</f:view>
</body>
</html>