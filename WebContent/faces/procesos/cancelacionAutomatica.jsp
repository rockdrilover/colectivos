<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Cancelacion
- Fecha: 09/10/2020
- Descripcion: Pantalla para Cancelacion Automatica
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallaxv}" /></title>
	</head>		
	
	<body onload="exibirPanelCancela(3);">	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaxv}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallaxv}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmCancelacionAut">
										<h:inputHidden value="#{beanCancelaAtm.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanCancelaAtm.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right15">
																			<h:outputText value="#{msgs.cancelatipoproc}:" />
																		</td>
																		<td class="left30">
																			<h:selectOneMenu id="inTipoProc" value="#{beanCancelaAtm.tipoProc}" 
																					title="header=[Tipo Proceso] body=[Seleccione un tipo proceso] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																				<f:selectItem itemValue="0" itemLabel="#{msgs.sistemaseleccione}" />
																				<f:selectItem itemValue="1" itemLabel="#{msgs.cancelaatm}" />
																				<f:selectItem itemValue="2" itemLabel="#{msgs.cancelamanual}" />
																				<a4j:support oncomplete="javascript:exibirPanelCancela(#{beanCancelaAtm.tipoProc});" event="onchange" action="#{beanCancelaAtm.limpiaResultados}"/>
																			</h:selectOneMenu>	
																		</td>
																	</tr>
																	
																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	
																	<tr class="texto_normal" id="trAutomatico">
																		<td colspan="2">
																			<table class="tablaCen100">
																				<caption></caption>
																				<tr class="texto_normal">
																					<td class="right15">
																						<h:outputText value="#{msgs.sistemafechad}:" />
																					</td>
																					<td class="left30">
																						<rich:calendar id="fechIni" popup="true"
																								value="#{beanCancelaAtm.feDesde}"
																								datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
																					</td>
																				</tr>
																				<tr class="texto_normal">
																					<td class="right15">
																						<h:outputText value="#{msgs.sistemafechah}:" />
																					</td>
																					<td class="left30">
																						<rich:calendar id="fechFin" popup="true"
																								value="#{beanCancelaAtm.feHasta}"
																								datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
																					</td>
																				</tr>
																				
																				<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.erroreslayout}:" />
																					</td>
																					<td class="left80">
																						<h:inputText id="nombreGpo" value="#{beanCancelaAtm.nombreLayout}" styleClass="wid250" />
																					</td>
																				</tr>
																				
																				<tr class="texto_normal">
																					<td class="right-div" colspan="2"> 
																						<a4j:commandButton
																							styleClass="boton" value="#{msgs.botonconsultar}"
																							action="#{beanCancelaAtm.consulta}"
																							onclick="this.disabled=true"
																							oncomplete="this.disabled=false;muestraOcultaSeccion('resultado', 0);"
																							reRender="mensaje,secResultados,regTabla"
																							title="header=[Consulta Errores] body=[Consulta el agrupado de errores por LayOut.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																					</td>
																				</tr>		
																			</table>
																		</td>
																	</tr>																			
																	
																	<tr class="texto_normal" id="trManual">
																		<td colspan="2">
																			<rich:panel id="paLayOut" styleClass="paneles">
																	        	<f:facet name="header"><h:outputText value="Cargar Lay Out Cancelacion" /></f:facet>
																	            
																	            <h:panelGrid columns="1"> 
																		            <a4j:commandButton styleClass="boton" value="#{msgs.botoncargar}" 
																						action="#{beanCancelaAtm.cargar}" 
																						onclick="this.disabled=true" oncomplete="this.disabled=false;muestraOcultaSeccion('resultado', 0);" 
																						reRender="mensaje,consultaDatos,secResultados,regTabla"
																						title="header=[Procesa Archivo] body=[Carga unicamente la información en la tabla.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
																																													          
																					<rich:fileUpload fileUploadListener="#{beanCancelaAtm.listener}"
																	    					maxFilesQuantity="5" id="loadArchivo"  
															    							addControlLabel="Examinar" uploadControlLabel="Cargar"  
															    							stopControlLabel="Detener" immediateUpload="true" 
															    							clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
															    							styleClass="archivo" acceptedTypes="csv"
															    							listHeight="60px" addButtonClass="botonArchivo" onuploadcomplete="muestraOcultaSeccion('resultado', 0);" >
															    					<a4j:support event="onuploadcomplete" reRender="mensaje" />
																	    			<f:facet name="label">
																	    				<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
																	    			</f:facet>                      
																	    			</rich:fileUpload>
																				</h:panelGrid>
																			</rich:panel>		
																		</td>
																	</tr>	

																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen100">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<tr class="texto_normal" id="tr_sec_resultado">
																		<td>
																			<c:if
																				test="${beanCancelaAtm.listaResumen == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanCancelaAtm.listaResumen != null}">
																				<rich:dataTable id="listaResumen" value="#{beanCancelaAtm.listaResumen}"
																						columnsWidth="5px,15px,15px,30px,15px,10px,10px,5px"
																						var="beanResumen" rows="30" width="100%">
																					<f:facet name="header">
																						<h:outputText value="#{msgs.cancelaresumen}" />
																					</f:facet>

																					<rich:column styleClass="texto_centro" title="#{beanResumen.archivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.cancelacant}" /></f:facet>
																						<h:outputText value="#{beanResumen.cantidad}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" title="#{beanResumen.archivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.cancelapoliza}" /></f:facet>
																						<h:outputText value="#{beanResumen.cdSucursal}" />-
																						<h:outputText value="#{beanResumen.cdRamo}" />-
																						<h:outputText value="#{beanResumen.nuPoliza}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" title="#{beanResumen.archivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.cancelaidventa}" /></f:facet>
																						<h:outputText value="#{beanResumen.descVenta}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" title="#{beanResumen.archivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.cancelacausa}" /></f:facet>
																						<h:outputText value="#{beanResumen.causaAnulacion}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" title="#{beanResumen.archivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.cancelaopcion}" /></f:facet>
																						<h:outputText value="#{beanResumen.descTipo}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" title="#{beanResumen.archivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.cancelapmadev}" /></f:facet>
																						<h:outputText value="#{beanResumen.pmaDev}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" title="#{beanResumen.archivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.cancelapmacan}" /></f:facet>
																						<h:outputText value="#{beanResumen.pmaCan}" />
																					</rich:column>
																					
																					<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" id="detallelink" reRender="listaDetalle" action="#{beanCancelaAtm.getDetalle}" oncomplete="#{rich:component('detPanel')}.show();muestraOcultaSeccion('resultado', 1);">
					                        												<h:graphicImage value="../../images/certificados.png" styleClass="bor0" />                        												
					                        												<f:setPropertyActionListener value="#{beanResumen}" target="#{beanCancelaAtm.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="detallelink" value="Ver Detalle" />
																                	</rich:column>
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaResumen" maxPages="10" page="#{beanCancelaAtm.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<rich:modalPanel id="detPanel" autosized="true" width="1200" height="600">
			<div class="scroll-div">
	   		<rich:toolTip id="toolTip" mode="ajax" value="Dar clic en Aceptar, Cancelar o Salir." direction="top-right" />
	   		
	       	<f:facet name="header"><h:outputText value="Detalle Creditos" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="detPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	                <rich:componentControl attachTo="hidelinkNew" operation="show" for="toolTip" event="onmouseover" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="detalleCreditos">
	            <rich:messages styleClass="errorMessage"></rich:messages>
	            <h:panelGrid columns="1" width="100%">
	                <a4j:outputPanel id="panelDet" ajaxRendered="true">
						<h:panelGrid columns="5" width="95%"  cellpadding="2">
	                    	<h:outputText value="#{msgs.cancelapoliza}:" styleClass="texwid15"/>
	                    	<h:outputText value="#{beanCancelaAtm.seleccionado.cdSucursal}-#{beanCancelaAtm.seleccionado.cdRamo}-#{beanCancelaAtm.seleccionado.nuPoliza}" styleClass="texwid30"/>
							<h:outputText value="" styleClass="texwid5"/>
							<h:outputText value="#{msgs.cancelaidventa}:" styleClass="texwid15"/>
	                    	<h:outputText value="#{beanCancelaAtm.seleccionado.descVenta}" styleClass="texwid30"/>
	                    	
	                    	<h:outputText value="#{msgs.cancelacausa}:" styleClass="texwid15"/>
	                    	<h:outputText value="#{beanCancelaAtm.seleccionado.causaAnulacion}" styleClass="texwid30"/>
							<h:outputText value="" styleClass="texwid5"/>
							<h:outputText value="#{msgs.cancelaopcion}:" styleClass="texwid15"/>
	                    	<h:outputText value="#{beanCancelaAtm.seleccionado.descTipo}" styleClass="texwid30"/>
	                    
	                    	<h:outputText value="" styleClass="texwid5"/>
	                    	<h:outputText value="" styleClass="texwid5"/>
							<h:outputText value="" styleClass="texwid5"/>
							<h:outputText value="#{msgs.erroresarchivo}:" styleClass="texwid15"/>
	                    	<h:outputText value="#{beanCancelaAtm.seleccionado.archivo}" styleClass="texwid30"/>
	                    	
	                    	<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
					
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
									
							<h:outputText value="#{beanCancelaAtm.conteo}" styleClass="texwid5"/>
							<h:selectBooleanCheckbox value="#{beanCancelaAtm.chkAll}" id="chkAll">
								<a4j:support event="onclick"  action="#{beanCancelaAtm.checkAll}" reRender="panelDet,listaDetalle" oncomplete="muestraOcultaSeccion('resultado', 1);"/>
							</h:selectBooleanCheckbox>
							<h:outputText value="" styleClass="texwid5"/>
	 						<a4j:commandButton 
				            	value="Cancelar"
				                action="#{beanCancelaAtm.updateCreditos}" 
				                reRender="mensaje,regTabla,consultaDatos"	                    
				                oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } muestraOcultaSeccion('resultado', 0);" />	
							
	 						<a4j:commandButton 
	 							value="Eliminar" 
	 						    action="#{beanCancelaAtm.deleteCreditos}" 
	 						    reRender="mensaje,regTabla,consultaDatos"	 
	 						    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } muestraOcultaSeccion('resultado', 0);" />
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                	               
					<rich:dataTable id="listaDetalle" value="#{beanCancelaAtm.lstDetalle}" var="beanDetalle" rows="15" width="100%">
						<f:facet name="header">
							<h:outputText value="Detalle Creditos" />							 						
						</f:facet>
										
						<rich:column width="3%">
							<h:selectBooleanCheckbox value="#{beanDetalle.check}" />			      		
			      		</rich:column>
						<rich:column width="13%">
							<f:facet name="header"><h:outputText value="#{msgs.sistemacerti}" /></f:facet>
							<h:outputText value="#{beanDetalle.certificado}" />
						</rich:column>														
						<rich:column width="13%">
							<f:facet name="header"><h:outputText value="#{msgs.sistemacre}" /></f:facet>
							<h:outputText value="#{beanDetalle.credito}" />
						</rich:column>
						<rich:column width="<10%">
							<f:facet name="header"><h:outputText value="#{msgs.cancelabaja}"/></f:facet>
							<h:outputText value="#{beanDetalle.fechaBaja}">
								<f:convertDateTime pattern="dd-MM-yyyy" />
							</h:outputText>
						</rich:column>
						<rich:column width="10%">
							<f:facet name="header"><h:outputText value="#{msgs.cancelaingreso}"/></f:facet>
							<h:outputText value="#{beanDetalle.fechaIngreso}">
								<f:convertDateTime pattern="dd-MM-yyyy" />
							</h:outputText>
						</rich:column>
						<rich:column width="10%">
							<f:facet name="header"><h:outputText value="#{msgs.cancelapmadev}" /></f:facet>
							<h:outputText value="#{beanDetalle.pmadev}" />
						</rich:column>
						<rich:column width="10%">
							<f:facet name="header"><h:outputText value="#{msgs.cancelapmacan}" /></f:facet>
							<h:outputText value="#{beanDetalle.pmacan}" />
						</rich:column>
						<rich:column width="30%">
							<f:facet name="header"><h:outputText value="#{msgs.erroredesc}" /></f:facet>
							<h:outputText value="#{beanDetalle.error}" />
						</rich:column>		
						
						<f:facet name="footer">
							<rich:datascroller align="center" for="listaDetalle" maxPages="10" page="#{beanCancelaAtm.numPagina1}" />
						</f:facet>																							
					</rich:dataTable>
	                
	                <rich:spacer height="10px" />
	                
	            </h:panelGrid>
	        </h:form>
	        </div>
	   	</rich:modalPanel>
	   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>

