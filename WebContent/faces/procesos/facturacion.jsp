<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Facturaci�n</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
	<tr>	
		<td align="center">
	    <table class="tablaSecundaria">
				<tr>
					<td align="center">
						<table class="encabezadoTabla" >
							<tr>
								<td width="10%" align="center">Secci�n</td>
								<td width="4%" align="center" bgcolor="#d90909">||||</td>
								<td align="left" width="64%">Facturaci�n</td>
								<td align="right" width="14%" >Fecha:</td>
								<td align="left" width="10%">
									<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
								</td>
							</tr>
						</table>
						<br>
						<h:form id="frmFactura">
						<div align="center">
						<rich:spacer height="15"/>	
							<table class="botonera">
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">	
											<table>
												<tr>
													<td align="center" colspan="2">
														<br/>
														<h:outputText id="mensaje" value="#{beanListaCertificado.respuesta}" styleClass="respuesta"/>
														<br/>
													</td>
												</tr>
											
												<tr>
													<td align="right" width="35%" height="20"><h:outputText value="Ramo:" /></td>
													<td align="left" width="35%" height="20">
													 	<h:selectOneMenu id="ramo" value="#{beanListaCertificado.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
															<f:selectItem itemValue="0" itemLabel="Ramo" />
															<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
															<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
													    </h:selectOneMenu>
														<h:message for="ramo" styleClass="errorMessage"/>
													</td>  
													<td/>
												</tr>
												<tr>
										    		<td align="right" width="35%" height="20"><h:outputText value="Poliza:"/></td>
													<td align="left" width="65%" height="20">
														<a4j:outputPanel id="polizas" ajaxRendered="true">
															<h:selectOneMenu id="inPoliza"  value="#{beanListaCertificado.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																<f:selectItem itemValue="-1" itemLabel="Seleccione una p�liza"/>
																<f:selectItem itemValue="0" itemLabel="Prima �nica carga"/>
																<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta" />
															</h:selectOneMenu>
															<h:message for="inPoliza" styleClass="errorMessage" />
														</a4j:outputPanel>
													</td>
									    		</tr>
									    		<tr>
										    		<td align="right" width="35%" height="20"><h:outputText value="Canal de Venta:"/></td>
													<td align="left" width="65%" height="20">
														<a4j:outputPanel id="idVenta" ajaxRendered="true">
															<h:selectOneMenu id="inVenta"  value="#{beanListaCertificado.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" disabled="#{beanListaParametros.habilitaComboIdVenta}">
																<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																<a4j:support event="onchange"  action="#{beanListaPreCarga.setValor}" ajaxSingle="true" reRender="idVenta" />
															</h:selectOneMenu>
														</a4j:outputPanel>
													</td>
										    	</tr>
												<tr>
													<td align="center" colspan="3">
													    <h:commandButton action="#{beanListaCertificado.consultaPreFacturas}" value="Consultar" />
													</td>
													<td/>
												</tr>							
											</table>	
										</td>
									    <td class="frameCR"></td>
								    </tr>										
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							</table>
							<br/>
						</div>
						</h:form>
						<table>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    	<div align="center">
							    	    <h:form>
										<rich:dataTable id="factura" value="#{beanListaCertificado.listaPrefact}" var="registro1" columns="13" columnsWidth="10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 5px" width="125px">
											<f:facet name="header"><h:outputText value="Montos Prefacturaci�n" /></f:facet>
											<rich:column>
												<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
												<h:outputText value="#{registro1.id.cofaCarpCdRamo}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
												<h:outputText value="#{registro1.id.cofaCapoNuPoliza}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="No. Recibo Col." /></f:facet>
												<h:outputText value="#{registro1.cofaCampov1}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="No. Recibo Fiscal" /> </f:facet>
												<h:outputText value="#{registro1.cofaNuReciboFiscal}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Monto Recibo" /></f:facet>
												<h:inputText value="#{registro1.cofaMtRecibo}" />																									
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Total Certificados" /> </f:facet>
												<h:inputText value="#{registro1.cofaTotalCertificados}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Suma Asegurada" /> </f:facet>
												<h:inputText value="#{registro1.cofaMtTotSua}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Prima Neta" /></f:facet>
												<h:inputText value="#{registro1.cofaMtTotPma}" />													
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="IVA" /> </f:facet>
												<h:inputText value="#{registro1.cofaMtTotIva}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="DPO" /> </f:facet>
												<h:inputText value="#{registro1.cofaMtTotDpo}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="RFI" /> </f:facet>
												<h:inputText value="#{registro1.cofaMtTotRfi}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Comisi�n" /> </f:facet>
												<h:inputText value="#{registro1.cofaMtTotCom}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="chk" /> </f:facet>
												<h:selectBooleanCheckbox value="#{registro1.chkUpdate}" />
											</rich:column>													
										</rich:dataTable>
										<br/>
										 <h:commandButton action="#{beanListaCertificado.guardamontosnuevosprefactura}" value="Guarda Montos" />
										 <h:commandButton action="#{beanListaCertificado.imprime_PreRecibo}" value="PreRecibo" />
										 <h:commandButton action="#{beanListaCertificado.factura}" value="Facturar" />
										</h:form>
										<br/>
									</div>
							    </td>
								<td class="frameCR"></td>
					    	</tr>								    	
					    	<tr>
								<td class="frameBL"></td>
								<td class="frameBC"></td>
								<td class="frameBR"></td>
							</tr>
						</table>
						<table>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    	<div align="center">
							    	    <h:form>
										<rich:dataTable id="factura" value="#{beanListaCertificado.listaRecibos}" var="registro2" columns="5" columnsWidth="40px, 40px, 60px, 80px, 20px" width="160px">
											<f:facet name="header"><h:outputText value="Recibos Generados" /></f:facet>
											<rich:column>
												<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
												<h:outputText value="#{registro2.id.cofaCarpCdRamo}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
												<h:outputText value="#{registro2.id.cofaCapoNuPoliza}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Recibo Fiscal" /></f:facet>
												<h:outputText value="#{registro2.cofaNuReciboFiscal}" />																									
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Estatus Sellado" /></f:facet>
												<h:outputText value="#{registro2.cofaCampov3}" />																									
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Sellar" /> </f:facet>
												<h:selectBooleanCheckbox value="#{registro2.chkUpdate}" />
											</rich:column>
										</rich:dataTable>
										<br/>
										 <h:commandButton action="#{beanListaCertificado.sellarRecibos}" value="Sellar Recibos" />
										</h:form>
										<br/>
									</div>
							    </td>
								<td class="frameCR"></td>
					    	</tr>								    	
					    	<tr>
								<td class="frameBL"></td>
								<td class="frameBC"></td>
								<td class="frameBR"></td>
							</tr>
						</table>
						<h:messages styleClass="errorMessage" globalOnly="true"/>			
					</td>
				</tr>
		</table>
	</td>
</tr>
</table>
</div>
<div align="center">
<br/>
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
 </html>