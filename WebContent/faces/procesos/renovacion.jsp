<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<title>Renovaci�n</title>
	</head>
	<body>
		<f:view>
			<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
			<div align="center">
			<%@include file="../header.jsp" %>
			</div>
			<div align="center">
				<table class="tablaPrincipal">
					<tbody>
						<tr>	
							<td align="center">
							    <table class="tablaSecundaria">
									<tbody>
										<tr>
											<td align="center">
													<table class="encabezadoTabla" >
														<tbody>
															<tr>
																<td width="10%" align="center">Secci�n</td>
																<td width="4%" align="center" bgcolor="#d90909">||||</td>
																<td align="left" width="64%">Renovaci�n</td>
																<td align="right" width="14%" >Fecha:</td>
																<td align="left" width="10%">
																	<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
																</td>
															</tr>
														</tbody>
													</table>
											    <rich:spacer height="2px"></rich:spacer>
												<h:form id="frmRenovacion">
													<div align="center">
													    <rich:spacer height="15"/>	
														<table class="botonera">
															<tbody>
															    <tr>
																    <td class="frameTL"></td>
																    <td class="frameTC"></td>
																    <td class="frameTR"></td>
															    </tr>
															    <tr>
																    <td class="frameCL"></td>
																    <td class="frameC" width="600px">	
																    <table>
																    <tbody>
																    	<tr>
																			<td align="center" colspan="3">
																			    <h:commandButton action="#{beanConsultaRenovacion.consultaPolizasRenovacion}" value="Consultar polizas" />
																			</td>  
																    	</tr>
																    </tbody>
																    </table>
																    </td>
																    <td class="frameCR"></td>
															    </tr>										
															    <tr>
																	<td class="frameBL"></td>
																	<td class="frameBC"></td>
																	<td class="frameBR"></td>
																</tr>
															</tbody>
														</table>
													</div>
													<br/>
												</h:form>
												<table>
												    <tr>
													    <td class="frameTL"></td>
													    <td class="frameTC"></td>
													    <td class="frameTR"></td>
												    </tr>
													<tr>
													    <td class="frameCL"></td>
													    <td class="frameC">
													    	<div align="center">
													    	    <h:form>
																<rich:dataTable id="renovacion" value="#{beanConsultaRenovacion.listaCertificados}" var="registro" columns="7" columnsWidth="10px,50px, 20px, 20px, 10px, 20px, 5px" width="135px">
																	<f:facet name="header"><h:outputText value="Polizas a renovar" /></f:facet>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
																		<h:outputText value="#{registro.id.coceCarpCdRamo}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
																		<h:outputText value="#{registro.id.coceCapoNuPoliza}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Fecha Desde" /></f:facet>
																		<h:outputText value="#{registro.coceFeDesde}" >
																			<f:convertDateTime pattern="dd-MM-yyyy"/>
																		</h:outputText>
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Fecha Hasta" /></f:facet>
																		<h:outputText value="#{registro.coceFeHasta}" >
																			<f:convertDateTime pattern="dd-MM-yyyy"/>
																		</h:outputText>
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Recibo" /></f:facet>
																		<h:outputText value="#{registro.coceCampon1}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Fecha Renovaci�n" /></f:facet>
																		<h:outputText value="#{registro.coceFeHasta}" >
																			<f:convertDateTime pattern="dd-MM-yyyy"/>
																		</h:outputText>
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="chk" /> </f:facet>
																		<h:selectBooleanCheckbox value="#{registro.chkUpdate}" />
																	</rich:column>													
																</rich:dataTable>
																<br/>
																
																 <h:commandButton action="#{beanConsultaRenovacion.renovar}" value="Renovar" />
																 <br/>
																 <br/>
																 <rich:progressBar value="#{beanConsultaRenovacion.progressValue}" id="barra"  
																     enabled= "#{beanConsultaRenovacion.progressHabilitar}" style=" width : 250px;" interval="500" rendered="true" reRenderAfterComplete="tablasPanel">
																	<h:outputText value="Procesando Renovaci�n" />
																	<f:facet name="complete">
																		<h:outputText value="Proceso Completado" style=" color : #FFFFFF; font-size: 15px; font-weight: bold;"/>
																	</f:facet>
																</rich:progressBar>
																</h:form>
																<br/>
															</div>
													    </td>
														<td class="frameCR"></td>
												    </tr>								    	
											    	<tr>
														<td class="frameBL"></td>
														<td class="frameBC"></td>
														<td class="frameBR"></td>
													</tr>
												</table>
												<table>
												    <tr>
													    <td class="frameTL"></td>
													    <td class="frameTC"></td>
													    <td class="frameTR"></td>
												    </tr>
													<tr>
													    <td class="frameCL"></td>
													    <td class="frameC">
													    	<div align="center">
													    	    <h:form>
													    	    <a4j:outputPanel id="tablasPanel">
																<rich:dataTable id="resumenrenov" value="#{beanConsultaRenovacion.listaPolizasRenovadas}" var="regpolizas" columns="6" columnsWidth="10px,10px, 30px, 45px, 20px, 20px" width="135px">
																	<f:facet name="header"><h:outputText value="Polizas a renovar" /></f:facet>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Canal" /> </f:facet>
																		<h:outputText value="#{regpolizas.id.coceCasuCdSucursal}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
																		<h:outputText value="#{regpolizas.id.coceCarpCdRamo}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
																		<h:outputText value="#{regpolizas.id.coceCapoNuPoliza}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Estatus" /> </f:facet>
																		<h:outputText value="#{regpolizas.coceCampov1}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Certificados" /> </f:facet>
																		<h:outputText value="#{regpolizas.coceCampon1}" />
																	</rich:column>
																	<rich:column style="text-align:center">
																		<f:facet name="header"><h:outputText value="Prima Total" /> </f:facet>
																		<h:outputText value="#{regpolizas.coceCampon5}">
																		<f:convertNumber pattern="#,##0.00" />
																		</h:outputText>
																	</rich:column>
																</rich:dataTable>
																<br/>
    															<h:commandButton action="#{beanConsultaRenovacion.detallePolizasRenovadas}" value="Detalle Polizas" />
																</a4j:outputPanel>
																</h:form>
															</div>
													    </td>
														<td class="frameCR"></td>
												    </tr>								    	
											    	<tr>
														<td class="frameBL"></td>
														<td class="frameBC"></td>
														<td class="frameBR"></td>
													</tr>
												</table>
												<h:messages styleClass="errorMessage" globalOnly="true"/>			
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div align="center">
				<br/>
				<%@ include file="../footer.jsp" %>
			</div>
		</f:view>
	</body>
</html>