<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Ccobranza
- Fecha: 15/12/2022
- Descripcion: Pantalla para realizar un reverso de estatus a recibos cancelados
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.cobranzareverso}" /></title>
	</head>
	
	<body onload="muestraOcultaSeccion('resultado', 0);">
	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaiii}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.cobranzareverso}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmconscred">
										<h:inputHidden value="#{beanReversoCobranza.beanGen.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanReversoCobranza.beanGen.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<caption></caption>
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemafiltro}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemaidentificador}:" />
																		</td>
																		<td class="left80">
																			<h:inputText id="identificador" value="#{beanReversoCobranza.credito}" styleClass="wid250"  />
																		</td>
																	</tr>
																	
																	<tr class="texto_normal">
																		<td class="right-div" colspan="2"> 
																			<a4j:commandButton
																				styleClass="boton" value="Consultar"
																				action="#{beanReversoCobranza.consulta}"
																				onclick="this.disabled=true;bloqueaPantalla();"
																				oncomplete="this.disabled=false;desbloqueaPantalla();muestraOcultaSeccion('resultado', 0);"
																				reRender="mensaje,secResultados,regTabla"
																				title="header=[Consulta Informacion] body=[Consulta los registros de acuerdo al filtro seleccionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</td>
																	</tr>

																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen90">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<caption></caption>
																	<tr class="texto_normal" id="tr_sec_resultado">
																		<td>
																			<c:if
																				test="${beanReversoCobranza.lstCertificados == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanReversoCobranza.lstCertificados != null}">
																				<rich:dataTable id="listaCertificados" value="#{beanReversoCobranza.lstCertificados}"
																					columnsWidth="20%,25%,10%,10%,10%,5%,20%,5%"
																					var="beanReg" rows="15" width="100%" styleClass="detalleEncabezado">
																					<f:facet name="header">
																						<h:outputText value="#{msgs.cambiocredito}" />
																					</f:facet>

																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.sistemacerti}" /></f:facet>
																						<h:outputText value="#{beanReg.id.coceCasuCdSucursal}-#{beanReg.id.coceCarpCdRamo}-#{beanReg.id.coceCapoNuPoliza}-#{beanReg.id.coceNuCertificado}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.sistemaEst}" /></f:facet>
																						<h:outputText value="#{beanReg.coceCampon1}-#{beanReg.coceCampov2}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.timbradofechac}" /></f:facet>
																						<h:outputText value="#{beanReg.coceFeAnulacionCol}" >
																							<f:convertDateTime pattern="dd-MM-yyyy"/>
																						</h:outputText>
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.timbradorecibo}" /></f:facet>
																						<h:outputText value="#{beanReg.coceNoRecibo}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.timbradofefac}" /></f:facet>
																						<h:outputText value="#{beanReg.coceFeFacturacion}" >
																							<f:convertDateTime pattern="dd-MM-yyyy"/>
																						</h:outputText>
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.timbradocuota}" /></f:facet>
																						<h:outputText value="#{beanReg.coceNuMovimiento}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.timbradoestcob}" /></f:facet>
																						<h:outputText value="#{beanReg.coceCampov4}" />
																					</rich:column>
																					
																					<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" 
					                    													id="detallelink" 
					                    													reRender="detPanel,panelDet" 							                    													 			                    												
					                    													oncomplete="#{rich:component('detPanel')}.show();
					                    															muestraOcultaSeccion('resultado', 0);">
					                        													<h:graphicImage value="../../images/certificados.png" styleClass="bor0" />                        												
					                        													<f:setPropertyActionListener value="#{beanReg}" target="#{beanReversoCobranza.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="detallelink" value="Ver Detalle" />
																                	</rich:column>
																                	
																				</rich:dataTable>
																				
																				<f:facet name="footer">
																					<rich:datascroller align="center" for="listaCertificados" maxPages="10" page="#{beanReversoCobranza.beanGen.numPagina}" />
																				</f:facet>
																				
																			</c:if>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
	   	
	   	<rich:modalPanel id="detPanel" autosized="true" width="600">
		   	<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle} #{msgs.timbradoCobranza}" /></f:facet>
		    <f:facet name="controls">
		        <h:panelGroup>
		            <h:graphicImage value="../../images/cerrarModal.png" 
		            	id="hidelink" 
		            	styleClass="hidelink" 
		            	onclick="muestraOcultaSeccion('resultado', 1);"/>            
		            <rich:componentControl for="detPanel" attachTo="hidelink" operation="hide" event="onclick" />
		        </h:panelGroup>
		    </f:facet>
		    
		    <h:form id="detalleCertificado">
		        <rich:messages style="color:red;"></rich:messages>
		        <h:panelGrid columns="1">
		            <a4j:outputPanel id="panelDet" ajaxRendered="true">
		            	<h:panelGrid columns="8">	            		
		            		<h:outputText value="#{msgs.sistemacerti}:   " styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanReversoCobranza.seleccionado.id.coceCasuCdSucursal} -" styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanReversoCobranza.seleccionado.id.coceCarpCdRamo} -" styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanReversoCobranza.seleccionado.id.coceCapoNuPoliza} -" styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanReversoCobranza.seleccionado.id.coceNuCertificado} (" styleClass="texto_mediano"/>
		            		<h:outputText value="#{beanReversoCobranza.seleccionado.coceNuCredito} )" styleClass="texto_mediano"/>
		            		
		            		<h:graphicImage value="../../images/espacio.png"/>
							<c:if test="${beanReversoCobranza.seleccionado.coceNuCobertura == 3}">
								<a4j:commandButton id="btnRehabilitar"
									styleClass="boton" value="#{msgs.cobranzareverso}"
									action="#{beanReversoCobranza.aplicaCobranza}"
									onclick="this.disabled=true;bloqueaPantalla();"
									oncomplete="this.disabled=false;desbloqueaPantalla();if (#{facesContext.maximumSeverity==null}) #{rich:component('detPanel')}.hide();"
									reRender="regTabla,mensaje"/>
		            		</c:if>
		            		<c:if test="${beanReversoCobranza.seleccionado.coceNuCobertura != 3}">
		            			<h:graphicImage value="../../images/espacio.png"/>
		            		</c:if>
		            	</h:panelGrid>
		            	
		            	<rich:separator height="4" lineType="double" />
		                
		                <rich:spacer height="10px" />
		                
		                <table class="tablacen100b">
		                	<caption></caption>
							
							<tr> 
								<td class="leftdivbo"><h:outputText value="#{msgs.timbradorecibo}:" /></td>
								<td class="right-div"><h:outputText value="#{beanReversoCobranza.seleccionado.coceNoRecibo}" styleClass="texto_mediano"/></td>
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
								
								<td class="leftdivbo"><h:outputText value="#{msgs.timbradoestcob}:"/></td>
								<td class="right-div">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceNuCobertura} - "/>
		            				<h:outputText value="#{beanReversoCobranza.seleccionado.coceCampov4}"/>
							    </td>  		  			
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>
								
								<td class="leftdivbo1"><h:outputText value="#{msgs.detcrecertfeven}:"/></td>
								<td class="right-div">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceFeDesde}" >
										<f:convertDateTime pattern="dd-MM-yyyy"/>
									</h:outputText>
								</td>
							</tr>
							<tr>
								<td class="leftdivbo"><h:outputText value="#{msgs.sistemapmatot}:"/></td>
								<td class="right-div"><h:outputText value="#{beanReversoCobranza.seleccionado.coceMtPrimaSubsecuente}" /></td>  		  			
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td> 
								
								<td class="leftdivbo"><h:outputText value="#{msgs.reversocobranzapma}:"/></td>
								<td class="right-div"><h:outputText value="#{beanReversoCobranza.seleccionado.coceMtTotalPrestamo}" /></td>  		  			
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td> 
								
								<td class="leftdivbo"><h:outputText value="#{msgs.reversocobranzapmanc}:"/></td>
								<td class="right-div"><h:outputText value="#{beanReversoCobranza.seleccionado.coceMtSumaAsegurada}" /></td>  		  			
							</tr>
							<tr>
								<td class="leftdivbo"><h:outputText value="#{msgs.timbradofefac}:"/></td>
								<td class="right-div">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceFeFacturacion}" >
										<f:convertDateTime pattern="dd-MM-yyyy"/>
									</h:outputText>
								</td>
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
								
								<td class="leftdivbo"><h:outputText value="#{msgs.reversocobranzafeco}:"/></td>
								<td class="right-div">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceFeHasta}" >
										<f:convertDateTime pattern="dd-MM-yyyy"/>
									</h:outputText>
								</td>
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
								
								<td class="leftdivbo"><h:outputText value="#{msgs.reversocobranzafenc}:"/></td>
								<td class="right-div">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceFeCarga}" >
										<f:convertDateTime pattern="dd-MM-yyyy"/>
									</h:outputText>
								</td>						
							</tr>
							<tr>
								<td class="leftdivbo"><h:outputText value="#{msgs.timbradofeemi}:"/></td>
								<td class="right-div">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceFeEmision}">
										<f:convertDateTime pattern="dd-MM-yyyy"/>
									</h:outputText>
								</td>
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
								
							    <td class="leftdivbo"><h:outputText value="#{msgs.timbradoestsis}:"/></td>
								<td class="left_div" colspan="4">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceCampon1} - " />
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceCampov2} ( " />
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceCampov1} )" />
								</td>																							
							</tr>
							<tr>
								<td class="leftdivbo"><h:outputText value="#{msgs.detcrecertfeanu}:"/></td>
								<td class="right-div">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceFeAnulacionCol}">
										<f:convertDateTime pattern="dd-MM-yyyy"/>
									</h:outputText>
								</td>
								<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
								
							    <td class="leftdivbo"><h:outputText value="#{msgs.detcrecertmotanu}:"/></td>
								<td class="left_div" colspan="4">
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceCdCausaAnulacion} - " />
									<h:outputText value="#{beanReversoCobranza.seleccionado.coceCampov3}" />
								</td>																							
							</tr>
		                </table>
		            </a4j:outputPanel>	                	              
		        </h:panelGrid>
		    </h:form>
		</rich:modalPanel>
	   	
	   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>