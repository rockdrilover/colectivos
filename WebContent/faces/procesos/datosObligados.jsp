<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Mantenimiento Obligados</title>
</head>
<body>
	<f:view>
		<!-- Desde aqui comienzo -->
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center">
			<%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tr>
					<td align="center">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div align="center">
										<table class="encabezadoTabla">
											<tr>
												<td width="10%" align="center">Secci�n</td>
												<td width="4%" align="center" bgcolor="#d90909">|||</td>
												<td align="left" width="64%">Mantenimiento Obligados</td>
												<td align="right" width="12%">Fecha</td>
												<td align="left" width="10%"><input name="fecha"
													readonly="readonly"
													value="<%=request.getSession().getAttribute("fecha")%>"
													size="7" /></td>
											</tr>
										</table>
									</div> <h:form id="frmRecibos">
										<div align="center">
											<table width="90%" border="0" cellpadding="0" cellspacing="0"
												align="center">
												<tr class="espacio_15">
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanObligado.respuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15">
													<td>&nbsp;</td>
												</tr>
											</table>

											<a4j:region id="consultaDatos">
												<table width="90%" border="0" cellpadding="0"
													cellspacing="0" align="center">
													<tr>
														<td>
															<fieldset style="border: 1px solid white">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> <h:graphicImage
																				value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones">Filtro</span>
																	</a4j:outputPanel>
																</legend>

																<table width="100%" border="0" cellpadding="0" cellspacing="3" align="center">
																	<tr>
																		<td align="right" width="20%">
																			<h:outputText value="Ramo:" />
																		</td>
																		<td align="left" width="80%" colspan="3">
																			<h:selectOneMenu id="inRamo" value="#{beanObligado.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																				<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
																				<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
																				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
																			</h:selectOneMenu> 
																			<h:message for="inRamo" styleClass="errorMessage" />
																		</td>
																	</tr>
																	<tr>
																		<td align="right" width="20%">
																			<h:outputText value="Poliza:" />
																		</td>
																		<td align="left" width="80%" colspan="3">
																			<a4j:outputPanel id="polizas" ajaxRendered="true">
																				<h:selectOneMenu id="inPoliza" value="#{beanObligado.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																					<f:selectItem itemValue="0" itemLabel="Seleccione una p�liza" />
																					<f:selectItem itemValue="1" itemLabel="Prima �nica" />
																					<f:selectItems value="#{beanListaParametros.listaComboPolizas}" />																					
																				</h:selectOneMenu>
																				<h:message for="inPoliza" styleClass="errorMessage" />
																			</a4j:outputPanel></td>
																	</tr>
																	
																	<tr>
																		<td align="right" width="20%">
																			<h:outputText value="Poliza Especifica:" />
																		</td>
																		<td align="left" width="80%" colspan="3">
																			<h:inputText id="polEspecifica"  value="#{beanObligado.polizaEspecifica}"/>	
																		</td>
																	</tr>
																	<tr>
																		<td align="right" width="15%">
																			<h:outputText value="Certificado:" />
																		</td>
																		<td align="left" width="80%" colspan="3">
																			<h:inputText id="certificado"  value="#{beanObligado.certificado}"/>	
																		</td>
																	</tr>																	
																	<tr>
																		<td colspan="3"></td>
																		<td align="left">
																			<a4j:commandButton styleClass="boton" value="Consultar" action="#{beanObligado.consultar}" onclick="this.disabled=true"
																					oncomplete="this.disabled=false" reRender="mensaje,secTitular,regTabla,secObligados,regTablaObligados"
																					title="header=[Consulta Informacion] body=[Consulta los datos de titulat y obligados de un certificado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</td>
																	</tr>

																	<tr class="espacio_15">
																		<td>&nbsp;</td>
																	</tr>
																	<tr>
																		<td align="center" colspan="4">
																			<a4j:status for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<!-- Inicio TITULAR -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<a4j:outputPanel id="secTitular" ajaxRendered="true">
																	<span id="img_sec_titular_m" style="display: none;">
																		<h:graphicImage 
																			value="../../images/mostrar_seccion.png" 
																			onclick="muestraOcultaSeccion('titular', 0);" 
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_titular_o"> 
																		<h:graphicImage
																			value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('titular', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones">Datos Titular</span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_titular" style="display: none;">
																		<td>
																			<c:if test="${beanObligado.listaTitular == null}">
																				<table width="90%" align="center" border="0">
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL TITULAR.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanObligado.listaTitular != null}">
																				<rich:dataTable id="listaTitulares" value="#{beanObligado.listaTitular}"																					
																						columnsWidth="5px,5px,20px,8px,5px,20px,12px,10px,5px,5px"
																						var="beanTitular" rows="10">
																					<f:facet name="header">
																						<h:outputText value="Datos del Titular" />
																					</f:facet>

																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Numero" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.id.coccNuCliente}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Buc" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnBucCliente}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Nombre" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnNombre}" />&nbsp;&nbsp;
																						<h:outputText value="#{beanTitular.cliente.cocnApellidoPat}" />&nbsp;&nbsp;
																						<h:outputText value="#{beanTitular.cliente.cocnApellidoMat}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="RFC" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnRfc}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Fecha Nac" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnFeNacimiento}">
																							<f:convertDateTime pattern="dd/MM/yyyy" />
																						</h:outputText>
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Calle" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnCalleNum}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Colonia" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnColonia}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Del/Mun" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnDelegmunic}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Estado" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnCdEstado}" />
																					</rich:column>																					
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="CP" />
																						</f:facet>
																						<h:outputText value="#{beanTitular.cliente.cocnCdPostal}" />
																					</rich:column>						
																					<f:facet name="footer">
																						<rich:datascroller align="right" for="listaTitulares" maxPages="10" page="#{beanObligado.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>

											<!-- Inicio Obligados -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0"
												align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<a4j:outputPanel id="secObligados" ajaxRendered="true">
																	<span id="img_sec_obligados_m" style="display: none;">
																		<h:graphicImage
																			value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('obligados', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_obligados_o"> <h:graphicImage
																			value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('obligados', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones">Datos Obligados</span>
																</a4j:outputPanel>
															</legend>

															<a4j:outputPanel id="regTablaObligados" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_obligados" style="display: none;">
																		<td>
																			<c:if test="${beanObligado.listaObligados == null}">
																				<table width="90%" align="center" border="0">
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION DE OBLIGADO SOLIDARIO.</td>
																					</tr>
																					<tr><td align="left"></td></tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanObligado.listaObligados != null}">
																				<a4j:commandButton ajaxSingle="true" value="Asociar Obligado" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" rendered="#{beanObligado.showBoton == 1}"/>
                    															<rich:toolTip for="btnNuevo" value="Agregar Asociado" />
																			
																				<rich:dataTable id="listaObligados" value="#{beanObligado.listaObligados}"																					
																						columnsWidth="5px,5px,20px,8px,5px,20px,12px,10px,5px,5px"
																						var="beanObli" rows="10">
																					<f:facet name="header">
																						<h:outputText value="Datos de Obligados" />
																					</f:facet>

																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Numero" />
																						</f:facet>
																						<h:outputText value="#{beanObli.id.coccNuCliente}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Buc" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnBucCliente}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Nombre" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnNombre}" />&nbsp;&nbsp;
																						<h:outputText value="#{beanObli.cliente.cocnApellidoPat}" />&nbsp;&nbsp;
																						<h:outputText value="#{beanObli.cliente.cocnApellidoMat}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="RFC" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnRfc}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Fecha Nac" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnFeNacimiento}">
																							<f:convertDateTime pattern="dd/MM/yyyy" />
																						</h:outputText>
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Calle" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnCalleNum}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Colonia" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnColonia}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Del/Mun" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnDelegmunic}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Estado" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnCdEstado}" />
																					</rich:column>																					
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="CP" />
																						</f:facet>
																						<h:outputText value="#{beanObli.cliente.cocnCdPostal}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Quitar" />
																						</f:facet>
																						<a4j:commandLink styleClass="boton" id="btnQuitar"
																							action="#{beanObligado.quitarObligado}"
																							onclick="this.disabled=true" oncomplete="this.disabled=false"
																							reRender="mensaje,secTitular,regTabla,secObligados,regTablaObligados">
																							<f:setPropertyActionListener
																								value="#{beanObli}"
																								target="#{beanObligado.seleccionado}" />
																							<h:graphicImage value="../../images/obligado_eliminar.png" style="border:0" />
																						</a4j:commandLink>
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Cambiar" />
																						</f:facet>
																						<a4j:commandLink styleClass="boton" id="btnCambiar"
																							action="#{beanObligado.cambiarObligado}"
																							onclick="this.disabled=true" oncomplete="this.disabled=false"
																							reRender="mensaje,secTitular,regTabla,secObligados,regTablaObligados">
																							<f:setPropertyActionListener
																								value="#{beanObli}"
																								target="#{beanObligado.seleccionado}" />
																							<h:graphicImage value="../../images/obligado_cambiar.png" style="border:0" />
																						</a4j:commandLink>
																					</rich:column>		
																					<f:facet name="footer">
																						<rich:datascroller align="right" for="listaObligados" maxPages="10" page="#{beanObligado.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>										
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<rich:modalPanel id="newPanel" autosized="true" width="600">
        	<f:facet name="header"><h:outputText value="Asociar Obligado" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" onclick="ocultaSeccion('tabla_alta_cliente', 0);"/>
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="asociarObligado">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:commandButton 
	                	value="Buscar"
	                    action="#{beanObligado.buscarClientes}" />
	                	                	                
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	<h:outputText value="Buscar por:" />
							<h:selectOneRadio id="radioBusqueda" value="#{beanObligado.tipoBusqueda}" >  
							   <f:selectItem itemValue="0" itemLabel="BUC"/>   
							   <f:selectItem itemValue="1" itemLabel="RFC"/>
							   <f:selectItem itemValue="2" itemLabel="Num. Cliente"/>   
							   <f:selectItem itemValue="3" itemLabel="Nombre"/> 
							   <a4j:support event="onclick"  action="#{beanObligado.muestraCriterio}"/>
							</h:selectOneRadio>
						</h:panelGrid>		

						<h:panelGrid id="pBuc" columns="2" style="display:#{beanObligado.panelBuc};">
							<h:outputText value="BUC:"/>
							<h:inputText id="inBuc" value="#{beanObligado.datoBusqueda}"/>
						</h:panelGrid>
							
						<h:panelGrid id="pRfc" columns="2" style="display:#{beanObligado.panelRfc};">
							<h:outputText value="RFC:"/>
							<h:inputText id="inRfc" value="#{beanObligado.datoBusquedaRFC}"/>
						</h:panelGrid>
						
						<h:panelGrid id="pClie" columns="2" style="display:#{beanObligado.panelClie};">
							<h:outputText value="Num Cliente:"/>
							<h:inputText id="inClie" value="#{beanObligado.datoBusquedaClie}"/>
						</h:panelGrid>
							
						<h:panelGrid id="pNombre" columns="2" style="display:#{beanObligado.panelNombre};">
							<h:outputText value="Nombre:" />
							<h:inputText value="#{beanObligado.nombre}" />		
							<h:outputText value="Apellido Paterno:" />
							<h:inputText value="#{beanObligado.paterno}" />
							<h:outputText value="Apellido Materno:" />
							<h:inputText value="#{beanObligado.materno}" />
						</h:panelGrid>
						
						<c:if test="${fn:length(beanObligado.listaClientes) == 0 && beanObligado.consulta == true}">
							<rich:spacer height="15px"></rich:spacer>
							<table id="tb_resultado_cliente">
								<tr>
									<td>
										<h:panelGrid id="resultado_cliente" columns="1" border="1" cellpadding="4" cellspacing="1">
											<f:facet name="header">
												<h:outputText value="Resultado Consulta." />
											</f:facet>
											
											<h:outputLabel value="NO HAY CLIENTES CON FILTRO SELECCIONADO." />
																						
											<h:graphicImage id="btnNewClie" value="../../images/obligado_nuevo.png" style="border:0" onclick="ocultaSeccion('tabla_alta_cliente', 1);ocultaSeccion('tb_resultado_cliente', 0);"/>
			  								<rich:toolTip for="btnNewClie" value="Agregar Asociado" />
										</h:panelGrid>
									</td>
								</tr>
							</table>
						</c:if> 
						
						<c:if test="${fn:length(beanObligado.listaClientes) > 0 && beanObligado.consulta == true}">
							<rich:dataTable id="listaClientes" value="#{beanObligado.listaClientes}"																					
									columnsWidth="5px,25px,25px,15px,15px,5px,5px,5px"
									var="beanCliente" rows="10">
								<f:facet name="header">
									<h:outputText value="Clientes Encontrados" />
								</f:facet>

								<rich:column>
									<f:facet name="header">
										<h:outputText value="Numero" />
									</f:facet>
									<h:outputText value="#{beanCliente.cocnNuCliente}" />
								</rich:column>
								<rich:column>
									<f:facet name="header">
										<h:outputText value="Nombre" />
									</f:facet>
									<h:outputText value="#{beanCliente.cocnNombre}" />&nbsp;&nbsp;
									<h:outputText value="#{beanCliente.cocnApellidoPat}" />&nbsp;&nbsp;
									<h:outputText value="#{beanCliente.cocnApellidoMat}" />
								</rich:column>
								<rich:column>
									<f:facet name="header">
										<h:outputText value="Calle" />
									</f:facet>
									<h:outputText value="#{beanCliente.cocnCalleNum}" />
								</rich:column>
								<rich:column>
									<f:facet name="header">
										<h:outputText value="Colonia" />
									</f:facet>
									<h:outputText value="#{beanCliente.cocnColonia}" />
								</rich:column>
								<rich:column>
									<f:facet name="header">
										<h:outputText value="Del/Mun" />
									</f:facet>
									<h:outputText value="#{beanCliente.cocnDelegmunic}" />
								</rich:column>
								<rich:column>
									<f:facet name="header">
										<h:outputText value="Estado" />
									</f:facet>
									<h:outputText value="#{beanCliente.cocnCdEstado}" />
								</rich:column>																					
								<rich:column>
									<f:facet name="header">
										<h:outputText value="CP" />
									</f:facet>
									<h:outputText value="#{beanCliente.cocnCdPostal}" />
								</rich:column>								
								<rich:column>
									<f:facet name="header">
										<h:outputText value="Asociar" />
									</f:facet>
									<a4j:commandLink styleClass="boton" id="btnCambiarr"
										action="#{beanObligado.asociarCliente}"
										onclick="this.disabled=true" 
										oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();"
										reRender="mensaje,secTitular,regTabla,secObligados,regTablaObligados">
										<f:setPropertyActionListener
											value="#{beanCliente}"
											target="#{beanObligado.clienteSel}" />
										<h:graphicImage value="../../images/clientes.png" style="border:0" />
									</a4j:commandLink>
								</rich:column>		
								<f:facet name="footer">
									<rich:datascroller align="right" for="listaClientes" maxPages="10" page="#{beanObligado.numPagina}" />
								</f:facet>
							</rich:dataTable>
						</c:if>
	                </a4j:outputPanel>

					<table width="90%" align="center" border="0" style="display: none;" id="tabla_alta_cliente" >
						<tr class="espacio_10"><td>&nbsp;&nbsp;</td></tr>
						<tr class="espacio_10">
							<td>
								<a4j:outputPanel ajaxRendered="true">
	                    			<h:panelGrid columns="2" border="1" cellpadding="4" cellspacing="1" style="display:#{beanObligado.panelNuevo};">
	                    				<f:facet name="header">
											<h:outputText value="Nuevo Cliente." />
										</f:facet>
	                    				
	                    				<h:outputText value="BUC: "/>
				                    	<h:inputText value="#{beanObligado.newCliente.cocnBucCliente}" style="width: 100px" maxlength="20" required="false" requiredMessage="* BUC - Campo Requerido"/>
										
										<h:outputText value="Nombre: "/>
				                    	<h:inputText value="#{beanObligado.newCliente.cocnNombre}" style="width: 150px" maxlength="100" required="false" requiredMessage="* NOMBRE - Campo Requerido"/>
				                    	
				                    	<h:outputText value="Apellido Paterno: "/>
				                    	<h:inputText value="#{beanObligado.newCliente.cocnApellidoPat}" style="width: 150px" maxlength="100" required="false" requiredMessage="* APELLIDO PATERNO - Campo Requerido"/>
				                    	
				                    	<h:outputText value="Apellido Materno: "/>
				                    	<h:inputText value="#{beanObligado.newCliente.cocnApellidoMat}" style="width: 150px" maxlength="100" required="false" requiredMessage="* APELLIDO MATERNO - Campo Requerido"/>
										
										<h:outputText value="RFC: "/>
										<h:inputText value="#{beanObligado.newCliente.cocnRfc}" style="width: 80px" maxlength="15" required="false" requiredMessage="* RFC - Campo Requerido"/>
										
				                        <h:outputText value="Fecha de Nacimiento: "/>
				                        <rich:calendar id="fecha_cons2" value="#{beanObligado.newCliente.cocnFeNacimiento}" 
				                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="false" datePattern="dd/MM/yyyy"
				                        		inputSize="10" popup="true" requiredMessage="* FECHA NACIMIENTO - Campo Requerido"/>
				                        
				                        <h:outputText value="Estado Civil: "/>
				                        <h:selectOneMenu id="inEstado2"  value="#{beanObligado.newCliente.cocnCdEdoCivil}" required="false" requiredMessage="* ESTADO CIVIL - Campo Requerido">
											<f:selectItem itemLabel="--- Seleccione ---" itemValue="" />  
							            	<f:selectItem itemLabel="SOLTERO" itemValue="S" />  
							            	<f:selectItem itemLabel="CASADO" itemValue="C" />  
							            	<f:selectItem itemLabel="DIVORCIADO" itemValue="D" />
							            	<f:selectItem itemLabel="VIUDO" itemValue="V" />  
							            	<f:selectItem itemLabel="OTRO" itemValue="O" />  
										</h:selectOneMenu>
										
										<h:outputText value="Sexo: "/>
										<h:selectOneMenu id="inSexo"  value="#{beanObligado.newCliente.cocnCdSexo}" required="false" requiredMessage="* SEXO - Campo Requerido">
											<f:selectItem itemLabel="--- Selecccione ---" itemValue="0" />  
							            	<f:selectItem itemLabel="FEMENINO" itemValue="FE" />  
							            	<f:selectItem itemLabel="MASCULINO" itemValue="MA" />
							            	<f:selectItem itemLabel="OTROS" itemValue="OT" /> 
										</h:selectOneMenu>								             
				                		
				                		<h:outputText value="Tipo Cliente: "/>
										<h:selectOneMenu id="inTC"  value="#{beanObligado.newCliente.cocnTpCliente}" required="false" requiredMessage="* TIPO CLIENTE Requerido">
											<f:selectItem itemLabel="TITULAR" itemValue="1" />  
							            	<f:selectItem itemLabel="OBLIGADO" itemValue="2" />  
										</h:selectOneMenu>
				                		
				                		<h:outputText value=""/>
				                		<a4j:commandButton 
							                	value="Guardar"
							                    action="#{beanObligado.guardarCliente}" reRender="mensaje,regTabla,dsContratante,comboContra"	                    
							                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />       
									</h:panelGrid>
				                </a4j:outputPanel>
				                																						
							</td>
						</tr>
					</table>	                
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
		
	</f:view>
</body>
</html>