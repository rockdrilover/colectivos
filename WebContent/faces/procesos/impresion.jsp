<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<script type="text/javascript" src="../../js/colectivos.js"></script>
<title>Impresi�n</title>
</head>

<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
	    <table class="tablaSecundaria">
			<tbody>
				<tr>
					<td align="center">
						<table class="encabezadoTabla" >
						<tbody>
							<tr>
								<td width="10%" align="center">Secci�n</td>
								<td width="4%" align="center" bgcolor="#d90909">||||</td>
								<td align="left" width="64%">Impresi�n</td>
								<td align="right" width="14%" >Fecha:</td>
								<td align="left" width="10%">
									<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
								</td>
							</tr>
						</tbody>
						</table>
						<br>
						
						<div align="center">
						<rich:spacer height="15"/>	
							<table class="botonera">								
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC">	
											<h:form id="frmCombosImpresion">
											<a4j:region id="filtroCarga">
											<table  border="0">				
												<a4j:outputPanel id="mensajes" ajaxRendered="true">																														
					    						<tr class="espacio_15"><td>&nbsp;</td></tr>												
												<tr><td><h:outputText value="#{beanImpresion.respuesta}" styleClass="error"/></td></tr>	
												</a4j:outputPanel>
												
												<tr><td colspan="1">&nbsp;</td></tr>
												<tr id="trImpresion" >		
													<td align="center" colspan="3">
														<rich:panel id="paImpresion" styleClass="paneles">
												        	<f:facet name="header"><h:outputText value="Seleccionar Ramo y P�liza para Impresi�n" /></f:facet>
												        		<h:outputText value="Ramo:" />
																<h:selectOneMenu id="ramo" value="#{beanImpresion.ramoFac}" binding="#{beanListaParametros.inRamo}">
																	<f:selectItem itemValue="0" itemLabel="Selecione un ramo"/>
																	<f:selectItem itemValue="57" itemLabel="57--VIDAEMPRESARIAL-SERFIN"/>
																	<f:selectItem itemValue="58" itemLabel="58--VIDAEMPRESARIAL-SANTANDER"/>
															 		<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="resultado"/>
															 	</h:selectOneMenu>													
															 	<br/><br/>
															 	<a4j:outputPanel id="polizas" ajaxRendered="true">
													        		<h:outputText value="P�liza:" />
																	<h:selectOneMenu id="poliza"  value="#{beanImpresion.inPoliza}" binding="#{beanListaParametros.inPoliza}">
																		<f:selectItem itemValue="-1" itemLabel="Seleccione una p�liza"/>
																		<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																 		<a4j:support oncomplete="javascript:exibirPanel5758(#{beanImpresion.opcion});" event="onchange" action="#{beanImpresion.cargaPolizas}" reRender="resultado" />
																 	</h:selectOneMenu>
															 	</a4j:outputPanel>
															 	<br/><br/>
																<a4j:outputPanel id="resultado" ajaxRendered="true">
																	<rich:dataTable id="factura" value="#{beanImpresion.listaCerti}" var="registro" columns="8" columnsWidth="10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px" width="90px">
																		<f:facet name="header"><h:outputText value="Polizas a Imprimir" /></f:facet>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Canal" /></f:facet>
																			<h:outputText value="#{registro.id.coceCasuCdSucursal}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
																			<h:outputText value="#{registro.id.coceCarpCdRamo}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
																			<h:outputText value="#{registro.id.coceCapoNuPoliza}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="N.Certificado" /></f:facet>
																			<h:outputText value="#{registro.coceCampon1}" />																									
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Prima Total" /> </f:facet>
																			<h:outputText value="#{registro.coceCampon2}" />
																		</rich:column>
																		<rich:column style="align: center;">
																			<f:facet name="header"><h:outputText value="Genera Zip Certificados" /> </f:facet>																			
   																			<a4j:commandButton styleClass="boton" id="btnCertificados" action="#{beanImpresion.getReportePlantilla}"   
     																				onclick="this.disabled=true" reRender="resultado" value="Generar">  
  																			</a4j:commandButton>   
																		</rich:column>
																		<rich:column style="align: center;">
																			<f:facet name="header"><h:outputText value="Descargar Zip" /> </f:facet>
																			<h:commandLink id="btnDescarga" action="#{beanImpresion.getZip}" >
					 										      				<h:graphicImage value="../../images/zip.jpg"/> 
															      				<f:setPropertyActionListener value="#{registro}" target="#{beanImpresion.id}" />
															      			</h:commandLink>
																		</rich:column>
																		<rich:column style="align: center;">
																			<f:facet name="header"><h:outputText value="Descargar Lista Asegurados" /> </f:facet>
																			<h:commandLink action="#{beanImpresion.reporteAsegurado}"  >
					 										      				<h:graphicImage value="../../images/download.png"/> 
															      				<f:setPropertyActionListener value="#{registro}" target="#{beanImpresion.id}" />
  															 				</h:commandLink>
																		</rich:column>
																	</rich:dataTable>
																	<br/>
																</a4j:outputPanel>
														</rich:panel>
													</td>
												</tr>
												<tr class="espacio_15"><td>&nbsp;</td></tr>
												
												<tr>
													<td align="left"><h:outputText value="Instrucciones para descargar todos los certificados de la poliza." /> </td>
												</tr>
												<tr>
													<td align="left"><h:outputText value="     1.- Dar Click al boton generar de columna Genera Certificados" /> </td>
												</tr>
												<tr>
													<td align="left"><h:outputText value="     2.- Dar Click en imagen de columna Descargar Zip" /> </td>
												</tr>
												
													
					                    		<tr class="espacio_15"><td>&nbsp;</td></tr>
					                    		<tr>
									    			<td align="center">
										    			<a4j:status for="filtroCarga" stopText=" "> 
															<f:facet name="start">
																<h:graphicImage value="/images/ajax-loader.gif" />
															</f:facet>
													    </a4j:status>
													</td>
												</tr>
											</table>
											</a4j:region>											
											</h:form>
										</td>
									    <td class="frameCR"></td>
								    </tr>
								    										
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							</table>
							<br/>
						</div>
						
						<h:messages styleClass="errorMessage" globalOnly="true"/>			
					</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
</div>
<div align="center">
<br/>
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
 </html>