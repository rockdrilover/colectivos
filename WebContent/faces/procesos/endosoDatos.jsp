<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">		
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title>Endoso Datos</title>
	</head>
	
	<body>
	
	<f:view>
		<!-- Desde aqui comienzo -->
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />
		<div class="center_div"><%@include file="../header.jsp"%> </div>
		<div class="center_div">
		<table class="tablaPrincipal">
			<tr>
				<td class="center_div">
					<table class="tablaSecundaria">
						<tr>
							<td>
								<div class="center_div">
								<table class="encabezadoTabla">
									<tr>
										<td class="encabezado1">Secci�n</td>
										<td class="encabezado2">||||</td>
										<td class="encabezado3">Endoso Datos</td>
										<td class="encabezado4">Fecha</td>
										<td class="encabezado5">
											<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7">
										</td>
									</tr>
								</table>
								</div>
		
								<rich:spacer style="height: 10px"></rich:spacer> <rich:dragIndicator id="indicador" /> 
								
								<h:form id="endoso">
									<div class="center_div">
									<a4j:region id="consulta">
									<table class="botonera">
										<tr>
											<td class="frameTL"></td>
											<td class="frameTC"></td>
											<td class="frameTR"></td>
										</tr>
										<tr>
											<td class="frameCL"></td>
											<td class="frameC">
												<table class="tabla">
													<tr>
														<td class="wid5por">&nbsp;</td>
														<td>
															<table>
																<tr>
																	<td class="wid5por">&nbsp;</td>
																	<td class="center_div">Criterio de busqueda:</td>
																	<td class="left_div">
																		<h:selectOneMenu value="#{beanEndosoDatos.criterioBusqueda}">
																			<f:selectItem itemValue="0" itemLabel="Seleccione un criterio de busqueda" />
																			<f:selectItem itemValue="1" itemLabel="Tarjeta de Credito" />
																			<f:selectItem itemValue="2" itemLabel="BUC" />
																			<f:selectItem itemValue="3" itemLabel="Folio" />
																			<f:selectItem itemValue="4" itemLabel="Canal/Ramo/Poliza/Certificado" />
																			<f:selectItem itemValue="5" itemLabel="Nombre" />
																			<a4j:support event="onchange" action="#{beanEndosoDatos.muestraCriterio}"
																				reRender="respuesta1,respuesta2,panelCriterios1,panelCriterios2,panelCriterios3,panelCriterios4,panelCriterios5,panelPolizaEspecifica" />
																		</h:selectOneMenu>
																	</td>
																	<td class="wid5por">&nbsp;</td>
																</tr>
																
																<tr><td colspan="4">&nbsp;</td></tr>
															
																<tr>
																	<td class="wid5por">&nbsp;</td>
																	<td colspan="2" class="left_div">
																		<h:panelGrid id="panelCriterios1" columns="2" style="display:#{beanEndosoDatos.display1};">
																			<h:outputText value="Tarjeta de Cr�dito:" />
																			<h:inputText id="tdc" value="#{beanEndosoDatos.tdc}" />
																		</h:panelGrid> 
																		
																		<h:panelGrid id="panelCriterios2" columns="2" style="display:#{beanEndosoDatos.display2};">
																			<h:outputText value="BUC:" />
																			<h:inputText id="buc" value="#{beanEndosoDatos.buc}" />
																		</h:panelGrid> 
																		
																		<h:panelGrid id="panelCriterios3" columns="2" style="display:#{beanEndosoDatos.display3};">
																			<h:outputText value="Folio:" />
																			<h:inputText id="identificador" value="#{beanEndosoDatos.identificador}" />
																		</h:panelGrid> 
																		
																		<h:panelGrid id="panelCriterios4" columns="3" style="display:#{beanEndosoDatos.display4};">
																			<h:outputText value=" " />
																			<h:outputText value="Ramo:" />
																			<h:selectOneMenu id="ramo" value="#{beanEndosoDatos.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																				<f:selectItem itemValue="0" itemLabel="Ramo" />
																				<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
																				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
																			</h:selectOneMenu>

																			<h:outputText value=" " />
																			<h:outputText value="Poliza:" />
																			<h:selectOneMenu id="polizas" value="#{beanEndosoDatos.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																				<f:selectItem itemValue="0" itemLabel="Seleccione una p�liza" />
																				<f:selectItem itemValue="1" itemLabel="Prima �nica" />
																				<f:selectItems value="#{beanListaParametros.listaComboPolizas}" />
																				<a4j:support event="onchange" action="#{beanEndosoDatos.muestraPE}" ajaxSingle="true" />
																			</h:selectOneMenu>

																			<h:outputText value=" " />
																			<h:outputText value="Poliza Especifica:" />
																			<h:inputText id="polEspecifica" value="#{beanEndosoDatos.polizaEspecifica}" />

																			<h:outputText value=" " />
																			<h:outputText value="Certificado:" />
																			<h:inputText id="certificado" value="#{beanEndosoDatos.certificado}" />
																		</h:panelGrid> 
																		
																		<h:panelGrid id="panelCriterios5" columns="2" style="display:#{beanEndosoDatos.display5};">
																			<h:outputText value="Nombre:" />
																			<h:inputText id="nombre" value="#{beanEndosoDatos.nombre}" />

																			<h:outputText value="Apellido Paterno:" />
																			<h:inputText id="ap" value="#{beanEndosoDatos.apellidoPaterno}" />

																			<h:outputText value="Apellido Materno:" />
																			<h:inputText id="am" value="#{beanEndosoDatos.apellidoMaterno}" />
																		</h:panelGrid>
																	</td>
																	<td class="wid5por">&nbsp;</td>
																</tr>
															</table>
														</td>
														
														<td class="wid5por">&nbsp;</td>
														
														<td>
															<a4j:commandButton styleClass="boton" id="btnConultar"
																action="#{beanEndosoDatos.consultaCertificados}"
																value="Consultar" reRender="listaCertificados,respuesta1,respuesta2,tipoEndoso,panelCriteriosE1,panelCriteriosE2,panelCriteriosE3,panelCriteriosE4,panelCriteriosE5,panelCriteriosE6,panelCriteriosE7,panelCriteriosE8,panelCriteriosE9,panelCriteriosE10,panelBotonEndoso"
																onclick="this.disabled=true"
																oncomplete="this.disabled=false; mostrar();"
																title="header=[Consulta certificados] body=[Consulta los criterios con base al filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
															</a4j:commandButton>
														</td>
													</tr>
													<tr><td colspan="4">&nbsp;</td></tr>
												</table>

												<table>
													<tr>
														<td class="center_div">
															<a4j:status for="consulta" stopText=" " id="estatusCon">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status> 
															<rich:spacer height="20px"></rich:spacer>
															<div class="center_div">
																<h:outputText id="respuesta1" value="#{beanEndosoDatos.respuesta}" styleClass="respuesta" />
															</div>
														</td>
													</tr>
												</table>
											</td>
											<td class="frameCR"></td>
										</tr>
										<tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
									</table>
									</a4j:region>
									</div> 
									
									<br />
									<a4j:region id="cancelaciones">
									<table class="botonera">
										<tr>
											<td class="frameTL"></td>
											<td class="frameTC"></td>
											<td class="frameTR"></td>
										</tr>
										<tr>
											<td class="frameCL"></td>
											<td class="frameC">
												<rich:panel styleClass="panel">
													<rich:dataTable id="listaCertificados" value="#{beanEndosoDatos.listaCertificados}"
														var="beanClienteCertif" columns="8" width="700px"
														columnsWidth="10px,200px,10px,10px,15px,5px,10px,5px"
														rows="5" rowKeyVar="row">
														<f:facet name="header">
															<rich:columnGroup>
																<rich:column colspan="14">
																	<h:outputText value="Consulta de Certificados" />
																</rich:column>
																<rich:column breakBefore="true" rowspan="2">
																	<h:outputText value="ID" />
																</rich:column>
																<rich:column rowspan="2">
																	<h:outputText value="Nombre" />
																</rich:column>
																<rich:column colspan="4">
																	<h:outputText value="Datos certificado" />
																</rich:column>
																<rich:column rowspan="2">
																	<h:outputText value="Tipo de Cliente" />
																</rich:column>
																<rich:column rowspan="2">
																	<h:outputText value="Endosar" />
																</rich:column>
																<rich:column breakBefore="true">
																	<h:outputText value="Canal" />
																</rich:column>
																<rich:column>
																	<h:outputText value="Ramo" />
																</rich:column>
																<rich:column>
																	<h:outputText value="P�liza" />
																</rich:column>
																<rich:column>
																	<h:outputText value="Certificado" />
																</rich:column>
															</rich:columnGroup>
														</f:facet>
														
														<rich:column>
															<h:outputText value="#{beanClienteCertif.id.coccIdCertificado}" />
														</rich:column>
														<rich:column>
															<h:outputText value="#{beanClienteCertif.cliente.cocnNombre} #{beanClienteCertif.cliente.cocnApellidoPat} #{beanClienteCertif.cliente.cocnApellidoMat}" />
														</rich:column>
														<rich:column>
															<h:outputText value="#{beanClienteCertif.certificado.id.coceCasuCdSucursal}" />
														</rich:column>
														<rich:column>
															<h:outputText value="#{beanClienteCertif.certificado.id.coceCarpCdRamo}" />
														</rich:column>
														<rich:column>
															<h:outputText value="#{beanClienteCertif.certificado.id.coceCapoNuPoliza}" />
														</rich:column>
														<rich:column>
															<h:outputText value="#{beanClienteCertif.certificado.id.coceNuCertificado}" />
														</rich:column>
														<rich:column>
															<h:outputText value="#{beanClienteCertif.coccVvalor1}" />
														</rich:column>
														<rich:column>
															<h:selectBooleanCheckbox value="#{beanClienteCertif.checkE}" onclick="checkSelect(this);" id="chkReq" />
														</rich:column>
														<f:facet name="footer">
															<rich:datascroller align="center" for="listaCertificados" maxPages="10" page="#{beanEndosoDatos.numPagina}" />
														</f:facet>
													</rich:dataTable>
												</rich:panel>
											</td>
											<td class="frameCR"></td>
										</tr>
										<tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
									</table>
									
									<rich:spacer height="10px"></rich:spacer>
									<table class="botonera" id="botonera" style="display: none;">
										<tr>
											<td class="frameTL"></td>
											<td class="frameTC"></td>
											<td class="frameTR"></td>
										</tr>
										<tr>
											<td class="frameCL"></td>
											<td class="frameC">
												<table style="width: 600px; padding: 20px;">
													<tr>
														<td style="width: 100%">
															<table class="tabla" border="0">
																<tr>
																	<td class="wid5por">&nbsp;</td>
																	<td class="center_div">Tipo de endoso:</td>
																	<td class="left_div">
																		<h:selectOneMenu id="tipoEndoso" value="#{beanEndosoDatos.tipoEndoso}">
																			<f:selectItem itemValue="0" itemLabel="<Seleccione un tipo de endoso>"/>
																			<f:selectItems value="#{beanEndosoDatos.lstComboTpEndoso}" />
																			<a4j:support event="onchange" action="#{beanEndosoDatos.muestraDisplayEndoso}"
																				reRender="listaAddBene,panelCriteriosE1,panelCriteriosE2,panelCriteriosE3,panelCriteriosE4,panelCriteriosE5,
																					panelCriteriosE6,panelCriteriosE7,panelCriteriosE8,panelCriteriosE9,panelCriteriosE10,
																					panelBotonEndoso,respuesta2,tipoEndoso,modalConfirmar" />
																		</h:selectOneMenu>
																	</td>																		
																	<td class="wid5por"><h:outputText value=""/></td>
																	<td></td>
																</tr>
																<tr class="espacio_15"><td>&nbsp;</td></tr>														
																<tr>
																	<td class="wid5por"><h:outputText value=""/></td>
																	<td colspan="2">
																		<h:panelGrid id="panelCriteriosE1" columns="2" style="display:#{beanEndosoDatos.displayE1};">
																			<h:outputText value="Nombre:" />
																			<h:inputText id="NOMBRE" value="#{beanEndosoDatos.nombreEndoso}" maxlength="100" style="width: 200px"/>

																			<h:outputText value="Apellido Paterno:" />
																			<h:inputText id="APELLIDO_PATERNO" value="#{beanEndosoDatos.apellidoPaternoEndoso}" maxlength="60" style="width: 200px"/>

																			<h:outputText value="Apellido Materno:" />
																			<h:inputText id="APELLIDO_MATERNO" value="#{beanEndosoDatos.apellidoMaternoEndoso}" maxlength="60" style="width: 200px"/>
																		</h:panelGrid> 
																		
																		<h:panelGrid id="panelCriteriosE2" columns="2" style="display:#{beanEndosoDatos.displayE2};">
																			<h:outputText value="Calle y n�mero:" />
																			<h:inputText id="CALLE_Y_NUMERO" value="#{beanEndosoDatos.calleNumero}" maxlength="150" style="width: 250px"/>

																			<h:outputText value="Colonia:" />
																			<h:inputText id="COLONIA" value="#{beanEndosoDatos.colonia}" maxlength="80" style="width: 250px"/>

																			<h:outputText value="Delegaci�n o Municipio:" />
																			<h:inputText id="DELEGACION_MUNICIPIO" value="#{beanEndosoDatos.delegacionMunicipio}" maxlength="80" style="width: 250px" />
																			
																			<h:outputText value="C�digo Postal:" />
																			<h:inputText id="CODIGO_POSTAL" value="#{beanEndosoDatos.codigoPostal}" maxlength="5" />

																			<h:outputText value="Estado: " />
																			<h:selectOneMenu id="ESTADO" value="#{beanEndosoDatos.estado}">
																				<f:selectItem itemValue="0" itemLabel="<Seleccione Estado>" />
																				<f:selectItems value="#{beanListaEstado.listaComboEstados}" />
																				<a4j:support event="onchange" action="#{beanEndosoDatos.consultaCiudad}" ajaxSingle="true" reRender="inCiudad" />
																			</h:selectOneMenu>

																			<h:outputText value="Ciudad: " />
																			<h:selectOneMenu id="inCiudad" value="#{beanEndosoDatos.ciudad}" required="false">
																				<f:selectItem itemValue="0" itemLabel="<Seleccione Ciudad>" />
																				<f:selectItems value="#{beanEndosoDatos.listaComboCiudades}" />
																			</h:selectOneMenu>
																		</h:panelGrid> 
																		
																		<h:panelGrid id="panelCriteriosE3" columns="2" style="display:#{beanEndosoDatos.displayE3};">
																			<h:outputText value="Fecha de Nacimiento:" />																		
																			<rich:calendar value="#{beanEndosoDatos.fechaNacimiento}" inputSize="10" 
															    				popup="true" cellHeight="20px" id="FECHA_NACIMIENTO"
															    				cellWidth="100px" datePattern="dd/MM/yyyy" styleClass="calendario">
															    			</rich:calendar>																			 
																		</h:panelGrid> 
																		
																		<h:panelGrid id="panelCriteriosE4" columns="8" style="display:#{beanEndosoDatos.displayE4};">
																			<h:outputText value="RFC del Asegurado:" />
																			<h:inputText id="idRFC1" value="#{beanEndosoDatos.rfc1}" maxlength="4" style="width: 40px"/>
																			<h:inputText id="idRFC2" value="#{beanEndosoDatos.rcf2}" maxlength="6" style="width: 50px" disabled="true"/>
																			<h:inputText id="idRFC3" value="#{beanEndosoDatos.rfc3}" maxlength="3" style="width: 30px"/>
																			<h:inputHidden id="generfc" value="#{beanEndosoDatos.tipoRFC}"></h:inputHidden>
																							<a4j:commandButton styleClass="boton" value="Genera RFC" action="#{beanEndosoDatos.generaRFC}" reRender="idRFC1,idRFC2,idRFC3,generfc">
																							</a4j:commandButton>
																						
																				</h:panelGrid> 
																		<h:panelGrid id="panelCriteriosE5" columns="2" style="display:#{beanEndosoDatos.displayE5};">
                                                                         <h:outputText value="Sexo del asegurado:" />
                                                                        <%-- <h:inputText id="SEXO" value="#{beanEndosoDatos.sexo}" maxlength="2"/> --%>
                                                                               <h:selectOneMenu id="SEXO" value = "#{beanEndosoDatos.sexo}"> 
                                                                                       <f:selectItem itemValue = "MA" itemLabel = "Masculino" /> 
                                                                                        <f:selectItem itemValue = "FE" itemLabel = "Femenino" /> 
                                                                               </h:selectOneMenu>
                                                                           </h:panelGrid>

																		<h:panelGrid id="panelCriteriosE6" columns="2" style="display:#{beanEndosoDatos.displayE6};">
																			<h:outputText value="Cambio y/o alta de correo electr�nico:" />
																			<h:inputText id="correoElectronico" value="#{beanEndosoDatos.correoElectronico}" style="width: 250px"/>
																		</h:panelGrid>
																		
																		<h:panelGrid id="panelCriteriosE7" columns="1" style="display:#{beanEndosoDatos.displayE7};">
																			<a4j:commandButton ajaxSingle="true" value="Administrar Beneficiario" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                															<rich:toolTip for="btnNuevo" value="Administrar Beneficiario" />									
																			
																			<rich:spacer height="20px"></rich:spacer>	
																			<rich:dataTable id="listaBeneficiarios" value="#{beanEndosoDatos.lstBeneficiarios}"																					
																					columnsWidth="5px,70px,10px,5px,5px"
																					var="beanBene" rows="5">
																				<f:facet name="header">
																					<h:outputText value="Beneficiarios" />
																				</f:facet>

																				<rich:column>
																					<f:facet name="header">
																						<h:outputText value="Numero" />
																					</f:facet>
																					<h:outputText value="#{beanBene.id.cobeNuBeneficiario}" />
																				</rich:column>																				
																				<rich:column>
																					<f:facet name="header">
																						<h:outputText value="Nombre" />
																					</f:facet>
																					<h:outputText value="#{beanBene.cobeNombre}" />&nbsp;&nbsp;
																					<h:outputText value="#{beanBene.cobeApellidoPat}" />&nbsp;&nbsp;
																					<h:outputText value="#{beanBene.cobeApellidoMat}" />
																				</rich:column>
																				<rich:column>
																					<f:facet name="header">
																						<h:outputText value="Relacion" />
																					</f:facet>
																					<h:outputText value="#{beanBene.cobeRelacionBenef}" />&nbsp;-&nbsp;
																					<h:outputText value="#{beanBene.cobeVcampo2}" />
																				</rich:column>
																				<rich:column>
																					<f:facet name="header">
																						<h:outputText value="%" />
																					</f:facet>
																					<h:outputText value="#{beanBene.cobePoParticipacion}" />
																				</rich:column>
																				<rich:column>
																					<f:facet name="header">
																						<h:outputText value="Fecha Alta" />
																					</f:facet>
																					<h:outputText value="#{beanBene.cobeFeDesde}">
																						<f:convertDateTime pattern="dd/MM/yyyy" />
																					</h:outputText>
																				</rich:column>																				
																																									
																				<f:facet name="footer">
																					<rich:datascroller align="right" for="listaBeneficiarios" maxPages="10" page="#{beanEndosoDatos.numPagina}" />
																				</f:facet>
																			</rich:dataTable>
																		</h:panelGrid>
																		
																		<h:panelGrid id="panelCriteriosE8" columns="2" style="display:#{beanEndosoDatos.displayE8};">
																			<h:outputText value="Fechas Desde:" />																		
																			<rich:calendar value="#{beanEndosoDatos.fechaDesde}" inputSize="10" 
															    				popup="true" cellHeight="20px" id="FECHA_DESDE"
															    				cellWidth="100px" datePattern="dd/MM/yyyy" styleClass="calendario">
															    			</rich:calendar>
															    			
															    			<h:outputText value="Fechas Hasta:" />
															    			<rich:calendar value="#{beanEndosoDatos.fechaHasta}" inputSize="10" 
															    				popup="true" cellHeight="20px" id="FECHA_HASTA"
															    				cellWidth="100px" datePattern="dd/MM/yyyy" styleClass="calendario">
															    			</rich:calendar>																			 
																		</h:panelGrid>
																		
																		<h:panelGrid id="panelCriteriosE9" columns="2" style="display:#{beanEndosoDatos.displayE9};">
																			<h:outputText value="Fechas Suscripcion:" />																		
																			<rich:calendar value="#{beanEndosoDatos.fechaSuscripcion}" inputSize="10" 
															    				popup="true" cellHeight="20px" id="FECHA_SUS"
															    				cellWidth="100px" datePattern="dd/MM/yyyy" styleClass="calendario">
															    			</rich:calendar>
																		</h:panelGrid>
																		
																		<h:panelGrid id="panelCriteriosE10" columns="2" style="display:#{beanEndosoDatos.displayE10};">
																			<h:outputText value="Suma Asegurada:" />																		
																			<h:inputText id="SUMA" value="#{beanEndosoDatos.sumaAsegurada}" maxlength="20"/>
																		</h:panelGrid>
																	</td>
																	<td class="wid5por"><h:outputText value=""/></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>														
														<td>
															<br />
															<table>
																<tr>
																	<td><td/>
																	<td><td/>
																	<td class="center_div">
																		<h:panelGrid id="panelBotonEndoso" columns="1" style="display:#{beanEndosoDatos.dBtnEndoso};">
																			<h:inputHidden id="modalConfirmar" value="#{beanEndosoDatos.mostrarModalConfirmar}"/>																			
																			<a4j:commandButton styleClass="boton" value="Endosar" onclick="var mc= document.getElementById('endoso:modalConfirmar').value; if (mc==='true') valida2();"
																				title="header=[Endosar] body=[Endosa el certificado seleccionado] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />																			
																			
																			<rich:modalPanel id="panelCancela">
																				<f:facet name="header">
																					<h:outputText value="Confirmar" />
																				</f:facet>      														
					      														Se endosaran los campos que ha modificado
					      														�Esta seguro de continuar con la operaci�n?
					      														<br /><br />
																				<table style="width: 100%">
																					<caption></caption>
																					<tr>
																						<td class="center_div">
																							<a4j:commandButton styleClass="boton"
																								action="#{beanEndosoDatos.endosar}" value="Si"
																								onclick="Richfaces.hideModalPanel('panelCancela');"
																								oncomplete="this.disabled=false"
																								reRender="respuesta1,respuesta2" />
																						</td>
																						<td style="width: 10%" />
																						<td class="center_div">
																							<a onclick="Richfaces.hideModalPanel('panelCancela');" href="#1">
																								<a4j:commandButton styleClass="boton" value="No" />
																							</a>
																						</td>
																					</tr>
																				</table>
																				<br /><br /><br />
																			</rich:modalPanel>
																		</h:panelGrid>
																	</td>
																</tr>
																<tr>
																	<td class="center_div">
																		<a4j:status for="cancelaciones" stopText=" ">
																			<f:facet name="start">
																				<h:graphicImage value="/images/ajax-loader.gif" />
																			</f:facet>
																		</a4j:status> 
																		<rich:spacer height="20px"></rich:spacer>
																		<div class="center_div">
																			<h:outputText id="respuesta2" value="#{beanEndosoDatos.respuesta2}" styleClass="respuesta" /> 
																			<h:messages styleClass="respuesta"></h:messages>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
											<td class="frameCR"></td>
										</tr>
										<tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
									</table>
									</a4j:region>
								</h:form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</div>
	
		<rich:spacer height="10px"></rich:spacer>
		<div class="center_div"><%@include file="../footer.jsp"%></div>
		
	   	<rich:modalPanel id="newPanel" autosized="true" width="450">
	   		<rich:toolTip id="toolTip" mode="ajax" value="Dar clic en Guardar o Cancelar." direction="top-right" />
	   		
	       	<f:facet name="header"><h:outputText value="Nuevo Beneficiario" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl attachTo="hidelinkNew" operation="show" for="toolTip" event="onmouseover" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevoBeneficiario">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1" width="400">
	                <a4j:outputPanel id="panelNew" ajaxRendered="true">
						<h:panelGrid columns="2">
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />
	                    	
							<h:outputText value="Nombre: "/>
	                    	<h:inputText value="#{beanEndosoDatos.nuevo.cobeNombre}" style="width: 200px" maxlength="200" required="false" requiredMessage="* NOMBRE - Campo Requerido"/>	
	                    	
	                    	<h:outputText value="Apellido Paterno: "/>
	                    	<h:inputText value="#{beanEndosoDatos.nuevo.cobeApellidoPat}" style="width: 200px" maxlength="200" required="false" requiredMessage="* APELLIDO PATERNO - Campo Requerido"/>
															
							<h:outputText value="Apellido Materno: "/>
	                    	<h:inputText value="#{beanEndosoDatos.nuevo.cobeApellidoMat}" style="width: 200px" maxlength="200" required="false" requiredMessage="* APELLIDO MATERNO - Campo Requerido"/>
			     			
			     			<h:outputText value="Parentesco: "/>
			     			<h:selectOneMenu value="#{beanEndosoDatos.nuevo.cobeRelacionBenef}" required="false" requiredMessage="* PARENTESCO - Campo Requerido">								
								<f:selectItems value="#{beanEndosoDatos.listaCmbParentesco}"/>
						    </h:selectOneMenu>
	                    	
	                    	<h:outputText value="% Participacion: "/>
	                    	<h:inputText value="#{beanEndosoDatos.nuevo.cobePoParticipacion}" style="width: 40px" maxlength="3" required="false" requiredMessage="* PORCENTAJE - Campo Requerido"/>
	                    	                    	
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                
	                <rich:spacer height="10px" />
	                
	                <a4j:commandButton value="Agregar" action="#{beanEndosoDatos.agregar}" reRender="listaAddBene"	onclick="this.disabled=true" oncomplete="this.disabled=false;" />
	                
					<rich:spacer height="10px" />
					
					<rich:dataTable id="listaAddBene" value="#{beanEndosoDatos.lstAddBeneficiario}"																					
							columnsWidth="60px,10px,10px,10px"
							var="beanAddBene" rows="10" width="400">
						<f:facet name="header">
							<h:outputText value="Nuevos Beneficiarios" />
						</f:facet>
																								
						<rich:column>
							<f:facet name="header">
								<h:outputText value="Nombre" />
							</f:facet>
							<h:outputText value="#{beanAddBene.cobeNombre}" />&nbsp;&nbsp;
							<h:outputText value="#{beanAddBene.cobeApellidoPat}" />&nbsp;&nbsp;
							<h:outputText value="#{beanAddBene.cobeApellidoMat}" />
						</rich:column>
						<rich:column>
							<f:facet name="header">
								<h:outputText value="#{msgs.endosodatosrelacion}"/>
							</f:facet>
							<h:outputText value="#{beanAddBene.cobeRelacionBenef}" />&nbsp;-&nbsp;
							<h:outputText value="#{beanAddBene.cobeVcampo2}" />
						</rich:column>
						<rich:column>
							<f:facet name="header">
								<h:outputText value="%" />
							</f:facet>
							<h:outputText value="#{beanAddBene.cobePoParticipacion}" />
						</rich:column>																									
						<rich:column title="Quitar" width="5%" styleClass="texto_centro">
							<f:facet name="header">
								<h:outputText value="#{msgs.sistemaquitar}"/>
							</f:facet>
							<a4j:commandLink styleClass="boton" id="btnQuitar"
								action="#{beanEndosoDatos.quitarNuevoBeneficiario}"
								onclick="this.disabled=true" oncomplete="this.disabled=false"
								reRender="listaAddBene">
								<f:setPropertyActionListener
									value="#{beanAddBene.id.cobeNuBeneficiario}"
									target="#{beanEndosoDatos.nNumBeneficiario}" />
								<h:graphicImage value="../../images/obligado_eliminar.png" style="border:0" />
							</a4j:commandLink>
							<rich:toolTip for="editlink" value="Quitar" />
	                	</rich:column>		
						<rich:column title="Modificar" width="5%" styleClass="texto_centro">
							<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
							<a4j:commandLink styleClass="boton" id="editlink" ajaxSingle="true"  
								action="#{beanEndosoDatos.setDatosEdit}"
								onclick="this.disabled=true" oncomplete="this.disabled=false"
								reRender="listaAddBene,panelNew">							
								<f:setPropertyActionListener value="#{beanAddBene}" target="#{beanEndosoDatos.seleccionado}" />
								<h:graphicImage value="../../images/edit_page.png" style="border:0" />
							</a4j:commandLink>
							<rich:toolTip for="editlink" value="Editar" />
						</rich:column>																	
					</rich:dataTable>
	                
	                <rich:spacer height="10px" />
	                
	                <h:panelGrid columns="2">
	                	<a4j:commandButton 
		                	value="Guardar"
						    action="#{beanEndosoDatos.guardar}" 
						    reRender="listaAddBene,listaCertificados,respuesta1,respuesta2,tipoEndoso,panelCriteriosE1,panelCriteriosE2,panelCriteriosE3,panelCriteriosE4,panelCriteriosE5,panelCriteriosE6,panelCriteriosE7,panelCriteriosE8,panelCriteriosE9,panelCriteriosE10,panelBotonEndoso"
						    onclick="#{rich:component('newPanel')}.hide();bloqueaPantalla();"	                    
						    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('newPanel')}.hide(); } else { #{rich:component('newPanel')}.show(); } desbloqueaPantalla();" />
						
						<a4j:commandButton 
		                	value="Cancelar"
						    action="#{beanEndosoDatos.cancelar}" 
						    reRender="listaAddBene,listaCertificados,respuesta1,respuesta2,tipoEndoso,panelCriteriosE1,panelCriteriosE2,panelCriteriosE3,panelCriteriosE4,panelCriteriosE5,panelCriteriosE6,panelCriteriosE7,panelCriteriosE8,panelCriteriosE9,panelCriteriosE10,panelBotonEndoso"
						    onclick="#{rich:component('newPanel')}.hide();bloqueaPantalla();"	                    
						    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('newPanel')}.hide(); } else { #{rich:component('newPanel')}.show(); } desbloqueaPantalla();" />
	                </h:panelGrid>
	                
	            </h:panelGrid>
	        </h:form>
	   	</rich:modalPanel>
	</f:view>
	</body>
</html>
