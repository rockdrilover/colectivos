<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Carga Cobranza
- Fecha: 10/11/2022
- Descripcion: Pantalla para Carga de cobranza
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.cobranzacarga}" /></title>
	</head>		
	
	<body>	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemax}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.cobranzacarga}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmCargaCobranza">
										<h:inputHidden value="#{beanCargaCobranza.beanGen.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanCargaCobranza.beanGen.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablaCen90">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemaramo}:" />
																		</td>
																		<td class="left80">
																			<h:selectOneMenu id="inRamo" value="#{beanCargaCobranza.beanGen.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
															    				<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
															    				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
															    				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas"/>
															    			</h:selectOneMenu>	
																		</td>
																	</tr>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemapoliza}:" />
																		</td>
																		<td class="left80">
																			<a4j:outputPanel id="polizas">
																				<h:selectOneMenu id="inPoliza"  value="#{beanCargaCobranza.beanGen.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[Póliza] body=[Seleccione una póliza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" style=" width : 190px;">
																					<f:selectItem itemValue="0" itemLabel="Seleccione una póliza"/>
																					<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																				</h:selectOneMenu>
																			</a4j:outputPanel>
																		</td>
																	</tr>																			
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.cobranzatipocarga}:" />
																		</td>
																		<td class="left80">
																			<h:selectOneMenu id="tipo" value="#{beanCargaCobranza.tipo}" title="header=[Id venta] body=[Seleccione una tipo de carga.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																				<f:selectItem itemValue="20" itemLabel="Por Credito" />
																				<f:selectItem itemValue="30" itemLabel="Por Póliza" />
																			</h:selectOneMenu>
																		</td>
																	</tr>	
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.cobranzaarchivo}:" />
																		</td>
																		<td class="left80" title="header=[Archivo] body=[Archivo a cargar extensión CSV, cualquier otra extensión el sistema no lo tomará en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																			<rich:fileUpload fileUploadListener="#{beanCargaCobranza.beanGen.listener}"
																						maxFilesQuantity="1" id="uploadCobranza"
													    								addControlLabel="Examinar" uploadControlLabel="Cargar" 
													    								stopControlLabel="Detener" immediateUpload="true" 
													    								clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
													    								styleClass="archivo" acceptedTypes="csv"
													    								listHeight="65px" addButtonClass="botonArchivo" listWidth="70%" >
											    								<a4j:support event="onuploadcomplete" reRender="mensaje" />
													    						<f:facet name="label">
													    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
													    						</f:facet>
													    					</rich:fileUpload>
																		</td>
																	</tr>
																	<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
																	<tr class="texto_normal">
										                    			<td class="right-div" colspan="2">	
																			<h:commandButton id="btnErrores" 
			                                                            			action="#{beanCargaCobranza.descargaErroresCarga}"
			                                                            			onclick="bloqueaPantalla();" onblur="desbloqueaPantalla();"
			                                                                		value="#{msgs.cobranzaerror}">
			                                                                	<a4j:support event="onclick"
			                                                                			ajaxSingle="true"                                                                		
			                                                                    		reRender="mensaje" />
			                                                            	</h:commandButton>															
																			<rich:toolTip for="btnErrores" value="Reporte de Errores" />
																																
																			<a4j:commandButton styleClass="boton" id="btnCargar" action="#{beanCargaCobranza.cargaCobranza()}" 
																							onclick="this.disabled=true;bloqueaPantalla();" oncomplete="this.disabled=false;desbloqueaPantalla();"
																						   	reRender="secResultados,regTabla,tResultados,mensaje" value="#{msgs.cobranzacarga}">
																			</a4j:commandButton>																
																			<rich:toolTip for="btnCargar" value="Carga Archivo" />											
																		</td>
										                    		</tr>
										                    		
																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen100">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m" style="display: none;">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<caption></caption>
																	<tr class="texto_normal" id="tr_sec_resultado" style="display:none;">
																		<td>
																			<rich:dataTable id="tResultados" value="#{beanCargaCobranza.lstResultados}" var="registro" columns="4" width="90%">
																				<f:facet name="header"><h:outputText value="Resultado de la carga" /></f:facet>
																				<rich:column width="10%" styleClass="texto_centro">
																					<f:facet name="header"><h:outputText value="#{msgs.sistemapoliza}" /> </f:facet>
																					<h:outputText value="#{registro.nuPoliza}"/>
																				</rich:column>
																				<rich:column width="60%" styleClass="texto_centro">
																					<f:facet name="header"><h:outputText value="#{msgs.erroresarchivo}" /></f:facet>
																					<h:outputText value="#{registro.archivo}"/>
																				</rich:column>
																				<rich:column>
																					<f:facet name="header"><h:outputText value="#{msgs.cobranzaerror}" /> </f:facet>
																					<h:outputText value="#{registro.cantidad}" style="FONT-SIZE: small; FONT-WEIGHT: bold;"/>
																				</rich:column>
																				<rich:column>
																					<f:facet name="header"><h:outputText value="#{msgs.cobranzaok}" /> </f:facet>
																					<h:outputText value="#{registro.cdRamo}" style="FONT-SIZE: small; FONT-WEIGHT: bold;"/>
																				</rich:column>
																				<f:facet name="footer">
																					<h:outputText value="Resgistros Totales: #{beanCargaCobranza.registros}" style="FONT-SIZE: small; FONT-WEIGHT: bold;"/>     										
						                								    	</f:facet>
																			</rich:dataTable>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
	   	
	   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>

