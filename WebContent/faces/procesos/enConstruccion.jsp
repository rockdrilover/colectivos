<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Construccion</title>
</head>
<body>
<f:view>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td>
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td align="center">
					<img src="../../images/construccion.png" width="40%" height="40%">
				</td>
			</tr>
			<tr>
				<td align="center">
					<h1><b style="COLOR: #000000;">EN CONSTRUCCION </b></h1>
				</td>
			</tr>
		</tbody>
		</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>