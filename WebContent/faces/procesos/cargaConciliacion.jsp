<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<title>Carga Conciliaci�n</title>
		
		<style>
			.filtroR {text-align: right;}
			.filtroL {text-align: left;}
			.top {vertical-align: top; width: 25%;}
			.info {height: 250px; width: 100%; overflow: auto;}
			.paneles {width: 90%;}
			.texto_normal {font-weight: bold; font-size: 11px}
		</style>
	</head>
	
	<body>
	<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
	<div align="center"><%@include file="../header.jsp" %></div>
	
	<div align="center">
	<table class="tablaPrincipal">
		<tr>	
			<td align="center">
				<table class="tablaSecundaria">
					<tr>
						<td align="center">
							<table class="encabezadoTabla" >
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Carga Conciliaci�n</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%"><input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7"></td>
								</tr>
							</table>
						    		
						    <rich:spacer height="2px"></rich:spacer>
						    <h:form id="frmCargaConciliacion">
							<table class="botonera">
								<tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
								<tr>
									<td class="frameCL"></td>
									<td class="frameC">	
										<a4j:region id="filtroCarga">		    
									    <table width="80%">
									    	<tr>
										    	<td align="center" width="20%">
											    	<a4j:status for="filtroCarga" stopText=" ">
														<f:facet name="start"><h:graphicImage value="/images/ajax-loader.gif" /></f:facet>
													</a4j:status>
												</td>
												<td align="center" width="60%">
													<h:outputText id="mensaje" value="#{beanListaCargaConcilia.respuesta}" styleClass="respuesta"/>
												</td>
												<td align="left" width="20%">
													<a4j:commandButton styleClass="boton" value="Cargar" 
															action="#{beanListaCargaConcilia.cargar}" 
															onclick="this.disabled=true" oncomplete="this.disabled=false;exibirPanel(#{beanListaCargaConcilia.nResultado})" 
															reRender="mensaje, loadArchivo, upload, taRespuesta"
															title="header=[Carga masiva] body=[Carga unicamente la informaci�n en la tabla.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
												</td>
											</tr>
											
											<tr><td colspan="3">&nbsp;</td></tr>
								    		<tr>
								    			<td colspan="3">
								    				<rich:panel styleClass="paneles">
								               			<f:facet name="header"><h:outputText value="Opciones de carga" /></f:facet>
								               			<h:panelGrid columns="2" columnClasses="filtroR, filtroL">																							          
											        		<h:outputText value="Origen:   " styleClass="texto_normal"/>														        	
															<h:selectOneMenu id="inOrigen" value="#{beanListaCargaConcilia.inOrigen}" 
																	title="header=[Origen] body=[Seleccione el origen] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
																	required="true">
																<f:selectItem itemValue="0" itemLabel="Seleccione el tipo de archivo" />
																<f:selectItems value="#{beanListaCargaConcilia.listaComboOrigen}"/>
																<a4j:support oncomplete="javascript:exibirPanel(#{beanListaCargaConcilia.inOrigen});" event="onchange" action="#{beanListaCargaConcilia.limpiaResultados}"/>
															</h:selectOneMenu>	
														
															<h:outputText id="lblFechaIni" value="Fecha Inicio:   " styleClass="texto_normal"/>
								    						<rich:calendar id="fechaIni" value="#{beanListaCargaConcilia.fechaIni}" datePattern="dd/MM/yyyy"
								    								enableManualInput="false" inputStyle="width:130px;"/>
								    					
								    						<h:outputText id="lblFechaFin" value="Fecha Fin:   " styleClass="texto_normal"/>
								    						<rich:calendar id="fechaFin" value="#{beanListaCargaConcilia.fechaFin}" datePattern="dd/MM/yyyy" 
										    						enableManualInput="false" inputStyle="width:130px;"/>
											        	</h:panelGrid>
								               		</rich:panel>
												</td>
								    		</tr>
										</table>
									    		
								    	<table>
									    	<tr><td colspan="2">&nbsp;</td></tr>
									    	
									    	<tr id="trVentas" style="display:none;">
									    		<td colspan="2" title="header=[Archivo Ventas] body=[Archivo a cargar extensi�n CSV o TXT, cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
													<rich:panel id="paVentas" styleClass="paneles">
											        	<f:facet name="header"><h:outputText value="Cargar archivo de ventas" /></f:facet>
											            
											            <h:panelGrid columns="1">																							          
															<rich:fileUpload fileUploadListener="#{beanListaCargaConcilia.listener}"
											    					maxFilesQuantity="5" id="loadArchivo"  
									    							addControlLabel="Examinar" uploadControlLabel="Cargar"  
									    							stopControlLabel="Detener" immediateUpload="true" 
									    							clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
									    							styleClass="archivo" acceptedTypes="csv,txt"
									    							listHeight="65px" addButtonClass="botonArchivo" listWidth="90%" >
									    					<a4j:support event="onuploadcomplete" reRender="mensaje" />
											    			<f:facet name="label">
											    				<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
											    			</f:facet>                      
											    			</rich:fileUpload>
														</h:panelGrid>
													</rich:panel>
									    		</td>	
									    	</tr>
											
											<tr id="trRadiosHP" style="display:none;">
												<td colspan="2" title="header=[Archivo Ventas] body=[Archivo a cargar extensi�n CSV o TXT, cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
													<rich:panel id="paVentasHP" styleClass="paneles">
											        	<f:facet name="header"><h:outputText value="Cargar archivos de ventas" /></f:facet>
											            <h:panelGrid columns="2" columnClasses="top, top1">																							          
															<h:selectOneRadio id="tpCarga" value="#{beanListaCargaConcilia.nTipoArchivoHPU}" title="Selecciona una opci�n" layout="pageDirection" required="false" border="1" style="text-align: left;">
																<f:selectItem itemLabel="Archivo Operaciones" itemValue="1" />
																<f:selectItem itemLabel="Archivo Work Flow" itemValue="2" />
															</h:selectOneRadio>
															
															<rich:fileUpload fileUploadListener="#{beanListaCargaConcilia.listener}"
											    					maxFilesQuantity="5" id="loadArchivoHP"  
									    							addControlLabel="Examinar" uploadControlLabel="Cargar"  
									    							stopControlLabel="Detener" immediateUpload="true" 
									    							clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
									    							styleClass="archivo" 
									    							listHeight="65px" addButtonClass="botonArchivo" listWidth="90%" >
									    						<a4j:support event="onuploadcomplete" reRender="mensaje" />
												    			<f:facet name="label">
												    				<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
												    			</f:facet>                      
											    			</rich:fileUpload>
														</h:panelGrid>
													</rich:panel>
									    		</td>
											</tr>
											
											<!-- APV -->
											<tr id="trBT" style="display:none;">
												<td colspan="2" title="header=[Archivo Ventas] body=[Archivo a cargar extensi�n CSV o TXT, cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
													<rich:panel id="paVentasBT" styleClass="paneles">
											        	<f:facet name="header"><h:outputText value="Cargar archivos de ventas" /></f:facet>
											            <h:panelGrid columns="2" columnClasses="top, top1">																							          
															<h:selectOneRadio id="tpCargaBT" value="#{beanListaCargaConcilia.nTipoArchivoBT}" title="Selecciona una opci�n" layout="pageDirection" required="false" border="1" style="text-align: left;">
																<f:selectItem itemLabel="Archivo Operaciones" itemValue="1" />
																<f:selectItem itemLabel="Archivo Pampa" itemValue="2" />
															</h:selectOneRadio>
															
															<rich:fileUpload fileUploadListener="#{beanListaCargaConcilia.listener}"
											    					maxFilesQuantity="5" id="loadArchivoBT"  
									    							addControlLabel="Examinar" uploadControlLabel="Cargar"  
									    							stopControlLabel="Detener" immediateUpload="true" 
									    							clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
									    							styleClass="archivo" 
									    							listHeight="65px" addButtonClass="botonArchivo" listWidth="90%" >
									    						<a4j:support event="onuploadcomplete" reRender="mensaje" />
												    			<f:facet name="label">
												    				<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
												    			</f:facet>                      
											    			</rich:fileUpload>
														</h:panelGrid>
													</rich:panel>
									    		</td>
											</tr>
											<!--FIN  APV -->
											
											<tr id="trRadiosLCI" style="display:none;">
												<td colspan="2" title="header=[Archivo Ventas] body=[Archivo a cargar extensi�n CSV o TXT, cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
													<rich:panel id="paVentasLCI" styleClass="paneles">
											        	<f:facet name="header"><h:outputText value="Cargar archivos de ventas" /></f:facet>
											            <h:panelGrid columns="2" columnClasses="top, top1">																							          
															<h:selectOneRadio id="tpCargaLCI" value="#{beanListaCargaConcilia.nTipoArchivo}" title="Selecciona una opci�n" layout="pageDirection" required="false" border="1" style="text-align: left;">
																<f:selectItem itemLabel="Archivo Altas" itemValue="1" />
															</h:selectOneRadio>
															
															<rich:fileUpload fileUploadListener="#{beanListaCargaConcilia.listener}"
											    					maxFilesQuantity="5" id="loadArchivoLCI"  
									    							addControlLabel="Examinar" uploadControlLabel="Cargar"  
									    							stopControlLabel="Detener" immediateUpload="true" 
									    							clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
									    							styleClass="archivo" 
									    							listHeight="65px" addButtonClass="botonArchivo" listWidth="90%" >
									    						<a4j:support event="onuploadcomplete" reRender="mensaje" />
												    			<f:facet name="label">
												    				<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
												    			</f:facet>                      
											    			</rich:fileUpload>
														</h:panelGrid>
													</rich:panel>
									    		</td>
											</tr>
																    	
									    	<tr id="trCuentas" style="display:none;">									    	
									    		<td colspan="2" title="header=[Archivo Cuentas] body=[Archivo a cargar extensi�n (TXT), cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
													<rich:panel bodyClass="info" styleClass="paneles" id="paCuentas">
											        	<f:facet name="header"><h:outputText value="Cargar archivos de cuentas" /></f:facet>
											            <h:panelGrid columns="2" columnClasses="top, top1">
															<h:selectManyCheckbox value="#{beanListaCargaConcilia.cuentas }" layout="pageDirection" styleClass="texto_normal">
											               		<f:selectItems value="#{beanListaCargaConcilia.listaCuentas}" />
											               	</h:selectManyCheckbox>
																										           
														    <rich:fileUpload fileUploadListener="#{beanListaCargaConcilia.listenerCuentas}"
														    		maxFilesQuantity="#{beanListaCargaConcilia.nTotalArchivos}" id="upload"
														            addControlLabel="Examinar" uploadControlLabel="Cargar" 
														            stopControlLabel="Detener" immediateUpload="true"
														            clearAllControlLabel="Borrar todo" clearControlLabel="Borrar"
														            styleClass="archivo" acceptedTypes="txt" 
														            listHeight="180px" addButtonClass="botonArchivo" listWidth="90%" >
															<a4j:support event="onuploadcomplete" reRender="mensaje" />
														    </rich:fileUpload>
														</h:panelGrid>
													</rich:panel>
									    		</td>
									    	</tr>
									    	
									    	<tr><td colspan="2">&nbsp;</td></tr>									    			
										</table>
												
										<table>
									    	<tr id="trResultados" style="display:none;">
									    		<td>
												<rich:dataTable value="#{beanListaCargaConcilia.lstResultados}" id="taRespuesta" var="resultado" columns="3" columnsWidth="15%,30%" rows="5" width="80%">
													<f:facet name="header">
										      			<rich:columnGroup>
										      				<rich:column colspan="3"><h:outputText value="Resultado de la carga" /></rich:column>
										      				<rich:column breakBefore="true"><h:outputText value="Cuenta" /></rich:column>
										      				<rich:column><h:outputText value="Fecha" /></rich:column>
										      				<rich:column><h:outputText value="Descricpcion" /></rich:column>
										      			</rich:columnGroup>
										      		</f:facet>
										      		 
										      		<rich:column><h:outputText value="#{resultado.cuenta}"/></rich:column>
													<rich:column>
											      		<h:outputText value="#{resultado.fecha}">
											      			<f:convertDateTime pattern="dd/MM/yyyy"/>
											      		</h:outputText>		      		  			
										      		</rich:column>
										      		<rich:column><h:outputText value="#{resultado.descripcion}" /></rich:column>
										      		<f:facet name="footer">
										      			<rich:datascroller for="taRespuesta" maxPages="10"/>
										      		</f:facet>
												</rich:dataTable>
												</td>
											</tr>
										</table>
										
										</a4j:region>
									</td>
									<td class="frameCR"></td>
								</tr>
								<tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
							</table>
				    		</h:form>
				    		<rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<rich:spacer height="10px"/>
	<div align="center"><%@ include file="../footer.jsp" %></div>
	</f:view>
	</body>
</html>