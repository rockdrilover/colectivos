<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Rehabilitacion P�lizas</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal" >
<tbody>
	<tr>
		<td>
			<table class="tablaSecundaria">
				<tr>
					<td align="center">
						<div align="center">
						<table class="encabezadoTabla">
							<tr>
								<td width="10%" align="center">Secci�n</td>
								<td width="4%" align="center" bgcolor="#d90909">-</td>
								<td align="left" width="64%"> Rehabilitacion P�lizas </td>
								<td align="right" width="12%" >Fecha</td>
								<td align="left" width="10%">
									<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
								</td>
							</tr>
						</table>
					    </div>
					    <rich:spacer height="10px"></rich:spacer>
					    <h:form id="frmRehabilitacionPolizas">
					    <div align="center">
						    <rich:spacer height="15"/>	
						    <table class="botonera">
						    <tbody>
						    	<tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL" />
								    <td class="frameC" width="600px">
										<a4j:region id="Rehabilitacion">
										<table>
									    	<tr>
									    		<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
									    		<td align="left" width="80%" >
									    			<h:selectOneMenu id="inRamo" value="#{beanRehabilitacion.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
									    				<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
									    				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
									    				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas"/>
									    			</h:selectOneMenu>
									    		</td>
									    	</tr>
									    	
									    	<tr><td>&nbsp;</td></tr>
									    		
									    	<tr>
									    		<td align="right" width="20%"><h:outputText value="Poliza:"/></td>
												<td align="left" width="80%" >
													<a4j:outputPanel id="polizas" ajaxRendered="true">
													<h:selectOneMenu id="inPoliza"  value="#{beanRehabilitacion.poliza}" binding="#{beanListaParametros.inPoliza}" required="true" title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
														<f:selectItem itemValue="0" itemLabel="Seleccione una p�liza"/>
														<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
													<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
													</h:selectOneMenu>
													</a4j:outputPanel>
												</td>				
									    	</tr>
									    </table>
									    </a4j:region>
									    <rich:spacer height="15px"></rich:spacer>									    
									    <a4j:region id="archivoRehabilitacion">
									    <table>
							    			<tr>
							    				<td align="right" width="20%" height="30"><h:outputText value="Cargar archivo:" /></td>
							    				<td align="left" width="80%" height="30" title="header=[Archivo] body=[Archivo a cargar extensi�n CSV, cualquier otra extensi�n el sistema no lo tomar� en cuenta).] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
							    					<rich:fileUpload id="uploadRehabilitacion" fileUploadListener="#{beanRehabilitacion.listener}"
					    								addControlLabel="Examinar" uploadControlLabel="Cargar" maxFilesQuantity="5"   
					    								stopControlLabel="Detener" immediateUpload="true" 
					    								clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" styleClass="archivo" acceptedTypes="csv"
					    								listHeight="65px" addButtonClass="botonArchivo" listWidth="70%">
					    								<a4j:support event="onuploadcomplete" reRender="message" />
							    						<f:facet name="label">
							    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
							    						</f:facet>
							    					</rich:fileUpload>
							    				</td>
							    			</tr>
										</table>
									    
									    <rich:spacer height="15px"></rich:spacer>
										<table>
							    		    <tr>
							    		        <td align="center">
							    					<a4j:commandButton styleClass="boton" value="Rehabilitar" 
														   action="#{beanRehabilitacion.rehabilitar}" 
														   onclick="this.disabled=true" oncomplete="this.disabled=false" 
														   title="header=[Proceso de Rehabilitacion] body=[Ejecuta el proceso de rehabilitacion] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" ajaxSingle="true"/>
												</td>
							    				<td align="center">
							    					<h:commandButton action="#{beanRehabilitacion.erroresRehabilitacion}" styleClass="boton" value="Errores" 
							    					    title="header=[Errores de Rehabilitacion] body=[Registros con errores despu�s de realizar el proceso de rehabilitaci�n. Asegurese de guardar el archivo, ya que despu�s de su generaci�n la informaci�n de errores se elimina de la base.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
									    		</td>
							    			</tr>
										</table>
										
										<br/>
										<table>
   									    	<tr>
						    			  		<td align="center">
						    						<a4j:status for="archivoRehabilitacion" stopText=" ">
														<f:facet name="start">									
															<h:graphicImage value="/images/ajax-loader.gif" />
														</f:facet>
													</a4j:status>
						    			  		</td>
						    		    	</tr>
									    </table>
									  </a4j:region>
									</td>
									<td class="frameCR"></td>
								</tr>
								<tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
							</tbody>
							</table>
							<br/>
						    <table class="botonera">
						    <tbody>
						    </tbody>
						    </table>
						   </div>
					    </h:form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"></rich:spacer>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>