<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>PreFactura</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
	<tr>	
		<td align="center">
	    <table class="tablaSecundaria">
			<tr>
				<td align="center">
					<table class="encabezadoTabla" >
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">PreFactura</td>
							<td align="right" width="14%" >Fecha:</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</table>

				    <rich:spacer height="2px"></rich:spacer>

					<h:form id="frmPrefactura">
						<div align="center">
						    <rich:spacer height="15"/>	
							<table class="botonera">
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="600px" align="center">	
									    <table>
									    	<tr>
												<td align="center" colspan="2">
													<br/>
													<h:outputText id="mensaje" value="#{beanListaCertificado.respuesta}" styleClass="respuesta"/>
													<br/>
												</td>
											</tr>
									    
									    	<tr>
												<td align="right" width="35%" height="30"><h:outputText value="Ramo:" /></td>
												<td align="left" width="65%" height="30">
												 	<h:selectOneMenu id="ramo" value="#{beanListaCertificado.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
												    </h:selectOneMenu>
													<h:message for="ramo" styleClass="errorMessage"/>
												</td>  
									    	</tr>
									    	<tr>
									    		<td align="right" width="35%" height="30"><h:outputText value="Poliza:"/></td>
												<td align="left" width="65%" height="30">
													<a4j:outputPanel id="polizas" ajaxRendered="true">
														<h:selectOneMenu id="inPoliza"  value="#{beanListaCertificado.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
															<f:selectItem itemValue="-1" itemLabel="Seleccione una p�liza"/>
															<f:selectItem itemValue="0" itemLabel="Prima �nica carga"/>
															<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
															<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta" />
														</h:selectOneMenu>
														<h:message for="inPoliza" styleClass="errorMessage" />
													</a4j:outputPanel>
												</td>
								    		</tr>
								    		<tr>
									    		<td align="right" width="35%" height="30"><h:outputText value="Canal de Venta:"/></td>
												<td align="left" width="65%" height="30">
													<a4j:outputPanel id="idVenta" ajaxRendered="true">
														<h:selectOneMenu id="inVenta"  value="#{beanListaCertificado.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" disabled="#{beanListaParametros.habilitaComboIdVenta}">
															<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
															<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
															<a4j:support event="onchange"  action="#{beanListaPreCarga.setValor}" ajaxSingle="true" reRender="idVenta" />
														</h:selectOneMenu>
													</a4j:outputPanel>
												</td>
									    	</tr>
									    </table>
								    </td>
								    <td class="frameCR"></td>
							    </tr>
							    
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="600px"  align="center" >	
								    <table>
								    	<tr>
											<td align="center" colspan="3">
											    <h:commandButton action="#{beanListaCertificado.consultaPreFactura}" 
											    			onclick="bloqueaPantalla();" 
									 						onblur="desbloqueaPantalla();"
											    			value="Consultar" />
											</td>  
								    	</tr>
								    </table>
								    </td>
								    <td class="frameCR"></td>
							    </tr>
							    										
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
							</table>
						</div>
						<br/>
					</h:form>
					
					<table>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC"  align="center" >
						    	<div align="center">
						    	    <h:form>
									<rich:dataTable id="emision" value="#{beanListaCertificado.listaCertificados}" var="registro" columns="6" columnsWidth="15px,15px,15px,20px,5px,30px" width="90px">
										<f:facet name="header"><h:outputText value="Polizas a  Prefacturar" /></f:facet>
										<rich:column>
											<f:facet name="header"><h:outputText value="Canal" /></f:facet>
											<h:outputText value="#{registro.id.coceCasuCdSucursal}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
											<h:outputText value="#{registro.id.coceCarpCdRamo}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
											<h:outputText value="#{registro.id.coceCapoNuPoliza}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Certificados" /> </f:facet>
											<h:outputText value="#{registro.coceCampon3}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="chk" /> </f:facet>
											<h:selectBooleanCheckbox value="#{registro.chkUpdate}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="#{msgs.gruposmsg}" /> </f:facet> 
											<h:outputText value="#{registro.coceCampov8}" />
										</rich:column>													
									</rich:dataTable>
									<br/>
									
									 <a4j:commandButton action="#{beanListaCertificado.upchk}" 
									 				value="Prefactura" 
									 				reRender="mensaje,emision,factura"
									 				onclick="bloqueaPantalla();" 
									 				onblur="desbloqueaPantalla();">
									 </a4j:commandButton>
									 
									<br/>
									<rich:progressBar value="#{beanListaCertificado.progressValue}" id="barra"  
									     enabled= "#{beanListaCertificado.progressHabilitar}" style=" width : 250px;" interval="500" rendered="true" reRenderAfterComplete="factura">
										<h:outputText value="Procesando Prefacturaci�n" />
										<f:facet name="complete">
											<h:outputText value="Proceso Completado" style=" color : #FFFFFF; font-size: 15px; font-weight: bold;"/>
										</f:facet>
									</rich:progressBar>
									</h:form>
									<br/>
								</div>
						    </td>
							<td class="frameCR"></td>
					    </tr>								    	
				    	<tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
					</table>
					<table>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC" align="center">
						    	<div align="center">
						    	    <h:form>
									<rich:dataTable id="factura" value="#{beanListaCertificado.listaPrefact}" var="registro1" columns="14" columnsWidth="10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 10px, 5px" width="125px">
										<f:facet name="header"><h:outputText value="Montos Prefacturaci�n" /></f:facet>
										<rich:column>
											<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
											<h:outputText value="#{registro1.id.cofaCarpCdRamo}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
											<h:outputText value="#{registro1.id.cofaCapoNuPoliza}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="No. Recibo Col." /></f:facet>
											<h:outputText value="#{registro1.cofaCampov1}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="No. Recibo Fiscal" /> </f:facet>
											<h:outputText value="#{registro1.cofaNuReciboFiscal}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Monto Recibo" /></f:facet>
											<h:inputText value="#{registro1.cofaMtRecibo}" />																									
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Total Certificados" /> </f:facet>
											<h:inputText value="#{registro1.cofaTotalCertificados}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Suma Asegurada" /> </f:facet>
											<h:inputText value="#{registro1.cofaMtTotSua}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Prima Neta" /></f:facet>
											<h:inputText value="#{registro1.cofaMtTotPma}" />													
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="IVA" /> </f:facet>
											<h:inputText value="#{registro1.cofaMtTotIva}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="DPO" /> </f:facet>
											<h:inputText value="#{registro1.cofaMtTotDpo}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="RFI" /> </f:facet>
											<h:inputText value="#{registro1.cofaMtTotRfi}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="Comisi�n" /> </f:facet>
											<h:inputText value="#{registro1.cofaMtTotCom}" />
										</rich:column>
										<rich:column>
											<f:facet name="header"><h:outputText value="chk" /> </f:facet>
											<h:selectBooleanCheckbox value="#{registro1.chkUpdate}" />
										</rich:column>	
										<rich:column>
											<f:facet name="header"><h:outputText value="Timbrado" /> </f:facet>
											
											<h:outputText value="#{registro1.resTimbrado}" />
											
											
										</rich:column>												
									</rich:dataTable>									<br/>
									 <h:commandButton action="#{beanListaCertificado.guardamontosnuevosprefactura}" value="Guarda Montos" />
									 <h:commandButton action="#{beanListaCertificado.imprime_PreRecibo}" value="PreRecibo" />
									  <a4j:commandButton action="#{beanListaCertificado.sellarRecibosPrefactura}" value="Sellar Recibos" reRender="factura" >
									 	 
									  </a4j:commandButton>
									</h:form>
									<br/>
								</div>
						    </td>
							<td class="frameCR"></td>
				    	</tr>								    	
				    	<tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
					</table>
					<h:messages styleClass="errorMessage" globalOnly="true"/>			
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</div>
<div align="center">
<br/>
<%@ include file="../footer.jsp" %>
</div>

<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
   	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
   	<h:graphicImage value="../../images/reloj.png"/>
   	<h:outputText value="    Por favor espere..." />
</rich:modalPanel>

</f:view>
</body>
 </html>