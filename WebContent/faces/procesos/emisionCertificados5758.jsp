<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Emisi&oacute;n de Certificados</title>
</head>
<body>
	<f:view>
		<!-- Desde aqui comienzo -->
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center"><%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>
						<td align="center">
							<table class="tablaSecundaria">


								<tbody>
									<tr>
										<td>
											<div align="center">
												<table class="encabezadoTabla">
													<tbody>
														<tr>
															<td width="10%" align="center">Secci&oacute;n</td>
															<td width="4%" align="center" bgcolor="#d90909">||||</td>
															<td align="left" width="64%">Emisi&oacute;n
																Certificados</td>
															<td align="right" width="12%">Fecha</td>
															<td align="left" width="10%"><input name="fecha"
																readonly="readonly"
																value="<%=request.getSession().getAttribute("fecha")%>"
																size="7"></td>
														</tr>
													</tbody>
												</table>
											</div> <rich:spacer height="10px"></rich:spacer> <rich:dragIndicator
												id="indicador" /> <h:form id="emision">
												<div align="left">

													<a4j:region id="consulta">

														<!-- --------------------- INICIO BLOQUE CON CONTENIDO 1 --------------------- -->
														<table class="botonera">
															<tbody>
																<tr>
																	<td class="frameCL"></td>
																	<td class="frameC"><%@ include
																			file="/WEB-INF/includes/emisionCert/datosPoliza.jsp"%>

																	</td>
																	<td class="frameCR"></td>
																</tr>
															</tbody>
														</table>
														<!-- --------------------- FIN BLOQUE CON CONTENIDO 1 --------------------- -->

														<br />

														<!-- --------------------- INICIO BLOQUE CON CONTENIDO 1 --------------------- -->
														<rich:spacer height="10px"></rich:spacer>

														<table class="botonera">
															<tbody>
																<tr>
																	<td ><%@ include
																			file="/WEB-INF/includes/emisionCert/datosAsegurado.jsp"%>
																	</td>
																</tr>
															</tbody>
														</table>


														<!-- --------------------- FIN BLOQUE CON CONTENIDO 1 --------------------- -->

														<br />

														<!-- --------------------- INICIO BLOQUE CON CONTENIDO 1 --------------------- -->
														<rich:spacer height="10px"></rich:spacer>
														<table class="botonera">
															<tbody>
																<tr>
																	<td ><%@ include
																			file="/WEB-INF/includes/emisionCert/datosCuestionario.jsp"%>

																	</td>
																</tr>

															</tbody>
														</table>
														<!-- --------------------- FIN BLOQUE CON CONTENIDO 1 --------------------- -->

														<br />

														<!-- ---------------------  INICIO BLOQUE CON CONTENIDO 2 --------------------- -->
														<rich:spacer height="10px"></rich:spacer>
														<table class="botonera" id="botonera">
															<tbody>
																<tr>
																	<td ><%@ include
																			file="/WEB-INF/includes/emisionCert/datosBeneficiario.jsp"%>
																	</td>
																</tr>
															</tbody>
														</table>
														<!-- --------------------- FIN BLOQUE CON CONTENIDO 2 --------------------- -->
														<br />

														<!-- --------------------- INICIO BLOQUE CON CONTENIDO 1 --------------------- -->
														<rich:spacer height="10px"></rich:spacer>
														<table class="botonera">
															<tbody>
																<!-- Rows superiores para formato Top Left, Top Center, Top Right -->
																<tr>
																	<td class="frameTL"></td>
																	<td class="frameTC"></td>
																	<td class="frameTR"></td>
																</tr>

																<!-- ---------- INICIO ROWS CENTRALES donde va el contenido Center Left, Center, Center Right ---------- -->
																<tr>
																	<td class="frameCL"></td>
																	<td class="frameC"><%@ include
																			file="/WEB-INF/includes/emisionCert/resultados.jsp"%>

																	</td>
																	<td class="frameCR"></td>
																</tr>
																<!-- --------- FIN ROWS CENTRALES ---------- -->

																<!-- Rows inferiores para formato Buttom Left, Buttom Center, Buttom Right -->
																<tr>
																	<td class="frameBL"></td>
																	<td class="frameBC"></td>
																	<td class="frameBR"></td>
																</tr>
															</tbody>
														</table>
														<!-- --------------------- FIN BLOQUE CON CONTENIDO 1 --------------------- -->

													</a4j:region>
												</div>
											</h:form>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<rich:spacer height="10px"></rich:spacer>
		<div align="center"><%@include file="../footer.jsp"%>
		</div>

		<rich:modalPanel id="sra" autosized="true" moveable="true"
			height="100">
			<h:outputText value="POR FAVOR ESPERE ..." styleClass="respuesta" />
			<h:graphicImage value="/images/ajax-loader.gif" />
		</rich:modalPanel>


	</f:view>
</body>
</html>