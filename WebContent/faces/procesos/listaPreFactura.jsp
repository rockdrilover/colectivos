<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Pre-factura</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
			<table class="tablaSecundaria">
				<tbody>
					<tr>
						<td align="center">
							<div align="center">
							<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Pre-factura</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
							</table>
						    </div>
						    <rich:spacer height="10px"></rich:spacer>
						    <h:form>
							    <div align="center">
							    <rich:spacer height="15"/>	
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">	
									    <a4j:region id="consultaPreFactura">		    
									    <table>
									    <tbody>
									    	<tr>
									    		<td align="right" width="40%"><h:outputText value="Ramo:" /></td>
									    		<td align="left" width="60%">
													<h:selectOneMenu id="inRamo" value="#{beanListaPreCarga.inRamo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
													</h:selectOneMenu>
													<h:message for="inRamo" styleClass="errorMessage" />
												</td>
									    	</tr>
									    	<tr>
									    		<td align="right" width="40%"><h:outputText value="Poliza:"/></td>
												<td align="left" width="60%">
													<a4j:outputPanel id="polizas" ajaxRendered="true">
														<h:selectOneMenu id="inPoliza"  value="#{beanListaPreCarga.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																<f:selectItem itemValue="0" itemLabel="Prima �nica"/>
																<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
														</h:selectOneMenu>
														<h:message for="inPoliza" styleClass="errorMessage" />
													</a4j:outputPanel>
												</td>
									    	</tr>
									    	<tr>
									    		<td align="right" colspan="2">
								    				<a4j:commandButton styleClass="boton" value="Consultar" 
															   action="#{beanListaPreCarga.emitir}" 
															   onclick="this.disabled=true" oncomplete="this.disabled=false" 
															   reRender=""
															   title="header=[Pre-factura] body=[Consulta la � las polizas prefacturadas.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>	
								    			</td>
									    	</tr>
									    </tbody>
									    </table>
								    	<rich:spacer height="15px"></rich:spacer>
									    	<table>
									    	<tbody>
									    		<tr>
									    			<td align="center">
										    			<a4j:status for="consultaPreFactura" stopText=" ">
															<f:facet name="start">
																<h:graphicImage value="/images/ajax-loader.gif" />
															</f:facet>
														</a4j:status>
													</td>
												</tr>
											</tbody>
											</table>
											</a4j:region>
									    </td>
									    <td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
							    </table>
							    <br/>
							    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC">
									    	<div align="center">
												<rich:dataTable id="emision" value="#{beanListaPreCarga.resultado}" var="registro" columns="3" columnsWidth="20px,150px, 20px" width="300px">
													<f:facet name="header">
														<rich:columnGroup>
															
														</rich:columnGroup>
													</f:facet>
													<rich:column>
														<f:facet name="header"><h:outputText value="Registro" /></f:facet>
														<h:outputText value="#{registro[0]}" />
													</rich:column>
													<rich:column>
														<f:facet name="header"><h:outputText value="Tabla" /> </f:facet>
														<h:outputText value="#{registro[1]}" />
													</rich:column>
													<rich:column>
														<f:facet name="header"><h:outputText value="Total" /> </f:facet>
														<h:outputText value="#{registro[2]}" />
													</rich:column>
												</rich:dataTable>
												<br/>
												<br/>
											</div>
									    </td>
										<td class="frameCR"></td>
								    	</tr>
								    	<tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
							    	</tbody>
							    	</table>												   
								</div>
						    </h:form>
						    <rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>