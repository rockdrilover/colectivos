<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<script type="text/javascript" src="../../js/colectivos.js"></script>
<title>Facturación y Renovación 57 y 58</title>
</head>

<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
	    <table class="tablaSecundaria">
			<tbody>
				<tr>
					<td align="center">
						<table class="encabezadoTabla" >
						<tbody>
							<tr>
								<td width="10%" align="center">Sección</td>
								<td width="4%" align="center" bgcolor="#d90909">||||</td>
								<td align="left" width="64%">Facturación y Renovación 57 y 58</td>
								<td align="right" width="14%" >Fecha:</td>
								<td align="left" width="10%">
									<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
								</td>
							</tr>
						</tbody>
						</table>
						<br>
						
						<div align="center">
						<rich:spacer height="15"/>	
							<table class="botonera">								
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC">	
											<h:form id="frmCombos">
											<a4j:region id="filtroCarga">
											<table  border="0" align="center">				
												<a4j:outputPanel id="mensajes" ajaxRendered="true">																														
					    						<tr class="espacio_15"><td>&nbsp;</td></tr>												
												<tr><td><h:outputText value="#{beanListaCertificado.respuesta}" styleClass="error"/></td></tr>	
												<tr class="espacio_15"><td>&nbsp;</tr>																								
												</a4j:outputPanel>
												
												<tr> 
													<td>
														<h:outputText value="Accion:" />&nbsp;
														<h:selectOneMenu value="#{beanListaCertificado.opcion}">
															<f:selectItem itemValue="0" itemLabel="- Seleccione una opcion -" />
														    <f:selectItem itemValue="1" itemLabel="Facturar" />
														    <f:selectItem itemValue="2" itemLabel="Renovar" />
														    <a4j:support oncomplete="javascript:exibirPanel5758(#{beanListaCertificado.opcion});" event="onchange" action="#{beanListaCertificado.getOpcion}"/>
														</h:selectOneMenu>	
													</td>
												</tr>
												
												<tr><td colspan="3">&nbsp;</td></tr>				
												<tr id="trFacturar" style="display:none;">		
													<td align="center" colspan="3">
														<rich:panel id="paFacturar" styleClass="paneles">
												        	<f:facet name="header"><h:outputText value="Facurar Ramos 57 y 58" /></f:facet>
												        		<h:outputText value="Ramo:" />
																<h:selectOneMenu id="ramo" value="#{beanListaCertificado.ramoFac}">
																	<f:selectItem itemValue="0" itemLabel="Selecione un ramo"/>
																	<f:selectItem itemValue="57" itemLabel="57--VIDAEMPRESARIAL-SERFIN"/>
																	<f:selectItem itemValue="58" itemLabel="58--VIDAEMPRESARIAL-SANTANDER"/>
															 		<a4j:support oncomplete="javascript:exibirPanel5758(#{beanListaCertificado.opcion});" event="onchange" action="#{beanListaCertificado.cargaPolizas}" reRender="resultado"/>
															 	</h:selectOneMenu>													
															 	<br/><br/>
																<a4j:outputPanel id="resultado" ajaxRendered="true">
																	<rich:dataTable id="factura" value="#{beanListaCertificado.listaCerti}" var="registro" columns="7" columnsWidth="10px, 10px, 10px, 10px, 10px, 10px, 10px" width="90px">
																		<f:facet name="header"><h:outputText value="Polizas a Facturar" /></f:facet>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Canal" /></f:facet>
																			<h:outputText value="#{registro.id.coceCasuCdSucursal}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
																			<h:outputText value="#{registro.id.coceCarpCdRamo}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
																			<h:outputText value="#{registro.id.coceCapoNuPoliza}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="N.Certificado" /></f:facet>
																			<h:outputText value="#{registro.coceCampon1}" />																									
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Prima Total" /> </f:facet>
																			<h:outputText value="#{registro.coceCampon2}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Seleccionar" /> </f:facet>
																			<h:selectBooleanCheckbox value="#{registro.chkUpdate}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header"><h:outputText value="Observaciones" /> </f:facet>
																			<h:outputText value="#{registro.desc1}" />
																		</rich:column>
																	</rich:dataTable>
																	<br/>
																	<a4j:commandButton  value="Facturar"
																			action="#{beanListaCertificado.factura5758}" 
																			onclick="this.disabled=true" oncomplete="javascript:exibirPanel5758(#{beanListaCertificado.opcion});" reRender="resultado" >
																	</a4j:commandButton>
																	<h:commandButton action="#{beanListaCertificado.reporteFacturados5758}" value="Reporte Facturados" />
																	<h:commandButton action="#{beanListaCertificado.reporteErrores5758}" value="Reporte Errores" />
																</a4j:outputPanel>
														</rich:panel>
													</td>
												</tr>
												
												<tr id="trRenovar" style="display:none;">		
													<td align="center" colspan="3"> 
														<rich:panel id="paRenovar" styleClass="paneles">													
												        	<f:facet name="header"><h:outputText value="Renovar Ramos 57 y 58" /></f:facet>												        													        	
												        		<h:outputText value="Ramo:" />&nbsp;
																<h:selectOneMenu id="dos" value="#{beanListaCertificado.ramo}">
																	<f:selectItem itemValue="0" itemLabel="Selecione un ramo"/>
																	<f:selectItem itemValue="57" itemLabel="57--VIDAEMPRESARIAL-SERFIN"/>
																	<f:selectItem itemValue="58" itemLabel="58--VIDAEMPRESARIAL-SANTANDER"/>															 		
															 	</h:selectOneMenu>
															 	<h:outputText value="" />&nbsp;
												        		<h:outputText value="Mes:" />&nbsp; 
																<h:selectOneMenu value="#{beanListaCertificado.rmes}" required="true">
																	<f:selectItem itemValue="0" itemLabel="- Seleccione un mes -" />
																	<f:selectItem itemValue="01" itemLabel="Enero" />
																	<f:selectItem itemValue="02" itemLabel="Febrero" />
																	<f:selectItem itemValue="03" itemLabel="Marzo" />
																	<f:selectItem itemValue="04" itemLabel="Abril" />
																	<f:selectItem itemValue="05" itemLabel="Mayo" />
																	<f:selectItem itemValue="06" itemLabel="Junio" />
																	<f:selectItem itemValue="07" itemLabel="Julio" />
																	<f:selectItem itemValue="08" itemLabel="Agosto" />
																	<f:selectItem itemValue="09" itemLabel="Septiembre" />
																	<f:selectItem itemValue="10" itemLabel="Octubre" />
																	<f:selectItem itemValue="11" itemLabel="Noviembre" />
																	<f:selectItem itemValue="12" itemLabel="Diciembre" />
																	<a4j:support oncomplete="javascript:exibirPanel5758(#{beanListaCertificado.opcion});" event="onchange" action="#{beanListaCertificado.cargaPolizasRenovar}" reRender="resultados,mensajes"/>
																</h:selectOneMenu>												        														        																	 																 	 																																									
																
																<br/><br/>																
																<a4j:outputPanel id="resultados" ajaxRendered="true">
																<rich:dataTable id="renueva" value="#{beanListaCertificado.listaCertiRenovar}" var="registro" columns="6" columnsWidth="10px, 10px, 10px, 10px, 10px, 10px" width="90px">
																	<f:facet name="header"><h:outputText value="Polizas a Renovar" /></f:facet>
																	
																	<rich:column>
																		<f:facet name="header"><h:outputText value="Ramo" /> </f:facet>
																		<h:outputText value="#{registro.id.coceCarpCdRamo}" />
																	</rich:column>
																	<rich:column>
																		<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
																		<h:outputText value="#{registro.id.coceCapoNuPoliza}" />
																	</rich:column>
																	<rich:column>
																		<f:facet name="header"><h:outputText value="Fecha desde" /></f:facet>
																		<h:outputText value="#{registro.coceFeDesde}" >
																			<f:convertDateTime pattern="dd/MM/yyyy"/>
																		</h:outputText>
																	</rich:column>
																	<rich:column>
																		<f:facet name="header"><h:outputText value="Fecha hasta" /></f:facet>
																		<h:outputText value="#{registro.coceFeHasta}" >
																			<f:convertDateTime pattern="dd/MM/yyyy"/>
																		</h:outputText>																									
																	</rich:column>
																	<rich:column>
																		<f:facet name="header"><h:outputText value="Seleccionar" /> </f:facet>
																		<h:selectBooleanCheckbox value="#{registro.chkUpdate}" /> 
																	</rich:column>
																	<rich:column>
																		<f:facet name="header"><h:outputText value="Observaciones" /> </f:facet>
																		 <h:outputText value="#{registro.coceCampov6}" />
																	</rich:column>
																	<br/><br/>
																</rich:dataTable>

																<br/><br/>
																<h:commandButton action="#{beanListaCertificado.renovar5758}" value="Renovar" /> 
															</a4j:outputPanel>
														</rich:panel>
													</td>
												</tr>
											</table>
											</a4j:region>											
											</h:form>
										</td>
									    <td class="frameCR"></td>
								    </tr>
								    										
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							</table>
							<br/>
						</div>
						
						<h:messages styleClass="errorMessage" globalOnly="true"/>			
					</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
</div>
<div align="center">
<br/>
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
 </html>