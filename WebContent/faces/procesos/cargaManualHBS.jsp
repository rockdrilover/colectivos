<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<title>Carga Conciliaci�n</title>
		
		<style>
			.filtroR {text-align: right;}
			.filtroL {text-align: left;}
			.top {vertical-align: top; width: 25%;}
			.info {height: 250px; width: 100%; overflow: auto;}
			.paneles {width: 90%;}
			.texto_normal {font-weight: bold; font-size: 11px}
		</style>
	</head>
	
	<body>
	<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
	<div align="center"><%@include file="../header.jsp" %></div>
	
	<div align="center">
	<table class="tablaPrincipal">
		<tr>	
			<td align="center">
				<table class="tablaSecundaria">
					<tr>
						<td align="center">
							<table class="encabezadoTabla" >
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">X</td>
									<td align="left" width="64%">Carga Manual Creditos Hipotecario Recurrente</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%"><input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7"></td>
								</tr>
							</table>
						    		
						    <rich:spacer height="2px"></rich:spacer>
						    <h:form id="frmCargaManualHBS">
							<table class="botonera">
								<tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
								<tr>
									<td class="frameCL"></td>
									<td class="frameC">	
										<a4j:region id="filtroCarga">		    
									    <table width="80%" >
									    	<tr>
										    	<td align="center" width="20%">
											    	<a4j:status for="filtroCarga" stopText=" ">
														<f:facet name="start"><h:graphicImage value="/images/ajax-loader.gif" /></f:facet>
													</a4j:status>
												</td>
												<td align="center" width="60%">
													<h:outputText id="mensaje" value="#{beanCargaManualHBS.respuesta}" styleClass="respuesta"/>
												</td>
												<td align="left" width="20%">
													<a4j:commandButton styleClass="boton" value="Cargar" 
															action="#{beanCargaManualHBS.cargar}" 
															onclick="this.disabled=true" oncomplete="this.disabled=false;showPanel(#{beanCargaManualHBS.nResultado})" 
															reRender="mensaje, loadArchivo, upload, taRespuesta"
															title="header=[Carga masiva] body=[Carga y procesa los registros del Lay Out.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
												</td>
											</tr>
											
											<tr><td colspan="3">&nbsp;</td></tr>
								    		<tr>
								    			<td colspan="3" align="center">
													<rich:panel styleClass="paneles">
								               			<f:facet name="header"><h:outputText value="Opciones de carga" /></f:facet>
								               			<h:panelGrid columns="2">																							          
											        		<h:outputText value="Tipo Carga:   " styleClass="texto_normal"/>														        	
															<h:selectOneRadio id="tpCarga" value="#{beanCargaManualHBS.nTipoCarga}" title="Selecciona una opci�n" 
																	required="false" onclick="showPanel(this.value)"
																	border="0" style="text-align: left;">
																<f:selectItem itemLabel="Carga Manual Creditos" itemValue="1" />
																<f:selectItem itemLabel="Carga Archivo WF Firmas" itemValue="2" />
															</h:selectOneRadio>																													
											        	</h:panelGrid>
								               		</rich:panel>
												</td>
								    		</tr>
								    		
								    		<tr><td colspan="3">&nbsp;</td></tr>
								    		<tr>
								    			<td id="taCargaManual" style="display:none;"  colspan="3" align="center">
								    				<rich:panelBar width="88%" styleClass="panelRich" contentClass="texto_normal_blanco">
														<rich:panelBarItem label="   Opciones Lay Out." headerStyle="text-align: left;">														
															<h:panelGrid columns="1" width="100%">
																<h:outputText  value="1.- Credito: Se toman registros tal y como esta."/>
																<h:outputText  value="2.- Credito y Suma Asegurada: Se cambia la Suma segurada al registro y se calcula Prima."/>
																<h:outputText  value="3.- Credito y Prima: Se cambia la Prima al registro."/>
																<h:outputText  value="4.- Credito y Tarifa: Se cambia la Tarifa al registro y se calcula Prima."/>
																<h:outputText  value="5.- Credito, Suma Asegurada y Tarifa: Se cambia la Suma segurada y Tarifa al registro, se calcula Prima."/>
																<h:outputText  value="6.- Credito, Fecha Nacimiento: Se cambia la Fecha Nacimiento."/>
																<h:outputText  value="7.- Credito, Codigo Postal: Se cambia el Codigo Postal."/>
															</h:panelGrid>
														</rich:panelBarItem>
													</rich:panelBar>																									
								    			</td>
											</tr>								    										    													    												
								    		
								    		<tr><td colspan="3">&nbsp;</td></tr>
								    		<tr>
								    			<td id="taCargaRamos" style="display:none;"  colspan="3" align="center">								    												    				
													<h:outputText  value="Remesa: "/>
													<h:inputText id="inRemesa"  value="#{beanCargaManualHBS.inRemesa}">  
			                                        	<f:validateLength  maximum="6"/>
			     									</h:inputText>					
			     									&nbsp;&nbsp;&nbsp;     										
													<h:outputText  value="Ramo: "/>
													<h:selectOneMenu id="inRamo" value="#{beanCargaManualHBS.inRamo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>														
													</h:selectOneMenu>
																										
								    			</td>
								    		</tr>
								    		
								    		<tr><td colspan="3">&nbsp;</td></tr>
								    		<tr>
								    			<td id="taArchivos" style="display:none;" colspan="3" align="center" title="header=[Archivo] body=[Archivo a cargar extensi�n TXT(Cargas Manuales) o XLS(Archivo Fimras WF).] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
													<rich:panel id="paArchivos" styleClass="paneles">
											        	<f:facet name="header"><h:outputText value="Seleccionar Archivo a Cargar." /></f:facet>
											            
											            <h:panelGrid columns="1" style="align:center;">																							          
															<rich:fileUpload fileUploadListener="#{beanCargaManualHBS.listener}"
											    					maxFilesQuantity="5" id="loadArchivo"  
									    							addControlLabel="Examinar" uploadControlLabel="Cargar"  
									    							stopControlLabel="Detener" immediateUpload="true" 
									    							clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
									    							styleClass="archivo" acceptedTypes="csv,txt,xlsx"
									    							listHeight="65px" addButtonClass="botonArchivo" listWidth="90%" >
									    					<a4j:support event="onuploadcomplete" reRender="mensaje" />
											    			<f:facet name="label">
											    				<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
											    			</f:facet>                      
											    			</rich:fileUpload>
														</h:panelGrid>
													</rich:panel>
									    		</td>													    
								    		</tr>
								    		
								    		<tr><td colspan="3">&nbsp;</td></tr>
								    		<tr>
									    		<td  id="trResultados" style="display:none;" colspan="3" align="center">
												<rich:dataTable value="#{beanCargaManualHBS.lstResultados}" id="taRespuesta" var="resultado" columns="3" columnsWidth="15%,15%,40%,15%" rows="5" width="80%">
													<f:facet name="header">
										      			<rich:columnGroup>
										      				<rich:column colspan="4"><h:outputText value="Resultado de la carga" /></rich:column>
										      				<rich:column breakBefore="true"><h:outputText value="Poliza" /></rich:column>
										      				<rich:column><h:outputText value="Cantidad" /></rich:column>
										      				<rich:column><h:outputText value="Estatus" /></rich:column>
										      				<rich:column><h:outputText value="Prima" /></rich:column>
										      			</rich:columnGroup>
										      		</f:facet>
										      		 
										      		<rich:column><h:outputText value="#{resultado.poliza}"/></rich:column>
													<rich:column><h:outputText value="#{resultado.totalregistros}"/></rich:column>
										      		<rich:column><h:outputText value="#{resultado.descripcion}" /></rich:column>
										      		<rich:column><h:outputText value="#{resultado.prima}"/></rich:column>
										      		<f:facet name="footer">
										      			<rich:datascroller for="taRespuesta" maxPages="12"/>
										      		</f:facet>
												</rich:dataTable>
												</td>
											</tr>
								    	</table>								    									    																																												
										</a4j:region>
									</td>
									<td class="frameCR"></td>
								</tr>
								<tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
							</table>
				    		</h:form>
				    		<rich:spacer height="15px"></rich:spacer>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<rich:spacer height="10px"/>
	<div align="center"><%@ include file="../footer.jsp" %></div>
	</f:view>
	</body>
</html>