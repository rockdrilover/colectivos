<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title></title>
</head>
<body>
	<f:view>  <!-- Desde aqui comienzo --> 
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
	<div align="center">
	<%@include file="../header.jsp" %>
	</div> 
	<div align="center">
		<table class="tablaPrincipal">
			<tbody>
				<tr>
					<td align="center">
						<table class="tablaSecundaria">
							<tbody>
								<tr>
									<td>
											<%@ include file="/WEB-INF/includes/tituloParametrizacion.jsp" %>
										<rich:spacer height="10px"></rich:spacer>
										<h:form>
											<div align="center">
											<rich:spacer height="15"/>	
												<table class="botonera">
													<tbody>
														<tr>
															<td class="frameTL"></td>
															<td class="frameTC"></td>
															<td class="frameTR"></td>
														</tr>
														<tr>
															<td class="frameCL"></td>
															<td class="frameC" width="600px">
															<a4j:region id="consultaDatos">
																<table cellpadding="2" cellspacing="0">
																	<tbody>
																		
																		<div align="center"> ALTA DE CONTRATANTE  	</div>
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText value="Razon Social:"/></td>
																			<td align="left" width="76%" height="30"><h:inputText id="rsocial" value="#{beanProducto.alprDeProducto}" maxlength="100" size="50"/></td>
																			
																		</tr>
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText value="RFC:"/></td>
																			<td align="left" width="76%" height="30"><h:inputText id="rfc" value="#{beanProducto.alprDeProducto}" maxlength="100" size="50"/></td>
																		</tr>
																		
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText value="Fecha constitucion:"/></td>
																			<td align="left" > 
																			 <rich:calendar id="date" value="#{bean.dateTest}">
          																	   <a4j:support event="onchanged" reRender="mainTable"/>
																			 </rich:calendar>
																			 </td>
																			
																		</tr>
																		
																		<TR>
																		<TD align="right" colspan="1"> DOMICILIO </TD> 	
																		</TR>
																		
																	    <tr>
																			<td align="right" width="24%" height="30"><h:outputText value="Calle/Numero:"/></td>
																			<td align="left" width="76%" height="30"><h:inputText id="calle" value="#{beanProducto.alprDeProducto}" maxlength="100" size="50"/></td>
																		</tr> 	
																		<tr>	
																			<td align="right" width="24%" height="30"><h:outputText value="Colonia:"/></td>
																			<td align="left" width="76%" height="30"><h:inputText id="colonia" value="#{beanProducto.alprDeProducto}" maxlength="100" size="50"/></td>
																			
																		</tr>
																		
																		
																		<tr>	
																			<td align="right" width="24%" height="30"><h:outputText value="Estado:"/></td>
																			<td align="left" width="76%" height="30"><h:inputText id="edo" value="#{beanProducto.alprDeProducto}" maxlength="100" size="50"/></td>
																			
																		</tr>
																		
																		
																		<tr>	
																			<td align="right" width="24%" height="30"><h:outputText value="Codigo Postal:"/></td>
																			<td align="left" width="76%" height="30"><h:inputText id="cp" value="#{beanProducto.alprDeProducto}" maxlength="100" size="50"/></td>
																			
																		</tr>
																		
																		<tr>
																			<td align="right" width="24%" height="30"></td>
																			<td align="left" width="76%" height="30">
																				<%-- <h:commandButton styleClass="boton" value="Guardar" action="#{beanProducto.altaProducto}" /> --%>
																				<a4j:commandButton styleClass="boton" value="Guardar" 
																				   action="#{beanProducto.altaProducto}" 
																				   onclick="this.disabled=true" oncomplete="this.disabled=false" 
																				   reRender="barra"
																				   title="header=[Carga y emisi�n masiva] body=[Ejecuta el proceso de carga y emisi�n, el tiempo de procesamiento depender� del archivo cargado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>	   
													
																				
																				--<h:outputText id="output" value="#{beanProducto.output}"/>--
																			</td>
																		</tr>
																	</tbody>
																</table>

															</a4j:region>								    

															<td class="frameCR"></td>
														</tr>
														<tr>
															<td class="frameBL"></td>
															<td class="frameBC"></td>
															<td class="frameBR"></td>
														</tr>
													</tbody>
												</table>
											<rich:spacer height="10px"></rich:spacer>

																							</div>
										</h:form>
										<rich:spacer height="10px"></rich:spacer>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>   	
	</div>
	<rich:spacer height="15px"></rich:spacer>
	<div>
	<%@include file="../footer.jsp" %>
	</div>
	</f:view>
</body>
</html>