<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Parametrizacion</title>
</head>
<body>
	<f:view>  <!-- Desde aqui comienzo --> 
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
	<div align="center">
	<%@include file="../header.jsp" %>
	</div> 
	<div align="center">
		<table class="tablaPrincipal">
			<tbody>
				<tr>
					<td align="center">
						<table class="tablaSecundaria">
							<tbody>
								<tr>
									<td>
											<%@ include file="/WEB-INF/includes/tituloParametrizacion.jsp" %>
										<rich:spacer height="10px"></rich:spacer>
										<a4j:form id="forma1">
										 
										 <a4j:region id="region">
											<div align="center">
											<rich:spacer height="15"/>	

												<table class="botonera">
													<tbody>
														<tr>
															<td class="frameTL"></td>
															<td class="frameTC"></td>
															<td class="frameTR"></td>
														</tr>
														<tr>
															<td class="frameCL"></td>
															<td class="frameC" width="600px">
																<%@ include file="/WEB-INF/includes/productoView.jsp" %>
															<td class="frameCR"></td>
														</tr>
														<tr>
															<td class="frameBL"></td>
															<td class="frameBC"></td>
															<td class="frameBR"></td>
														</tr>
													</tbody>
												</table>

											<rich:spacer height="10px"></rich:spacer>

												<table class="botonera">
													<tbody>
														<tr>
															<td class="frameTL"></td>
															<td class="frameTC"></td>
															<td class="frameTR"></td>
														</tr>
														<tr>
															<td class="frameCL"></td>
															<td class="frameC" width="600px">
																<%@ include file="/WEB-INF/includes/planesView.jsp" %>
															</td>

															<td class="frameCR"></td>
														</tr>
														<tr>
															<td class="frameBL"></td>
															<td class="frameBC"></td>
															<td class="frameBR"></td>
														</tr>
													</tbody>
												</table>
											<rich:spacer height="15"/>	

											<table class="botonera">
												<tbody>
													<tr>
														<td class="frameTL"></td>
														<td class="frameTC"></td>
														<td class="frameTR"></td>
													</tr>
													<tr>
														<td class="frameCL"></td>
														<td class="frameC" width="600px">
															<%@ include file="/WEB-INF/includes/listaPlanesView.jsp" %>
														<td class="frameCR"></td>
													</tr>
													<tr>
														<td class="frameBL"></td>
														<td class="frameBC"></td>
														<td class="frameBR"></td>
													</tr>
												</tbody>
											</table>

											</div>
										 </a4j:region>
											
										</a4j:form>
										<rich:spacer height="10px"></rich:spacer>
									</td>
								</tr>
							</tbody>
						</table>

					<%@include file="/WEB-INF/includes/coberturasView.jsp"  %>						
					<%@include file="/WEB-INF/includes/componentesView.jsp" %>
					<%@include file="/WEB-INF/includes/homologaView.jsp"    %>
						
					</td>
				</tr>
			</tbody>
		</table>   	
	</div>
	<rich:spacer height="15px"></rich:spacer>
	<div>
	<%@include file="../footer.jsp" %>
	</div>
	</f:view>
</body>
</html>