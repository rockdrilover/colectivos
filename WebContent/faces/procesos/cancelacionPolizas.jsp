<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Cancelaci�n de Polizas</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
						<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Cancelaci�n Polizas</td>
									<td align="right" width="12%">Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
						</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmCancelacionPolizas">
				      <div align="center">
					    <rich:spacer height="15px"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="700px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
										<tr>
											<td width="34%"></td>
											<td width="66%"></td>
										</tr>
										<tr>
											<td align="right">
												<h:outputText value="Ramo:" />
											</td>
											<td align="left">
											 	<h:selectOneMenu id="inRamo" value="#{beanListaCertificado.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
								    				<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
								    				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
								    				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas"/>
								    			</h:selectOneMenu>
												<h:message for="ramo" styleClass="errorMessage"/>
											</td>  
										</tr>
										<tr>
											<td align="right">
												<h:outputText value="Poliza:" />
											</td>
											<td align="left">
												<a4j:outputPanel id="polizas" ajaxRendered="true">
												<h:selectOneMenu id="inPoliza"  value="#{beanListaCertificado.poliza1}" binding="#{beanListaParametros.inPoliza}" required="true" title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
													<f:selectItem itemValue="0" itemLabel="Seleccione una p�liza"/>
													<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
												<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
												</h:selectOneMenu>
												</a4j:outputPanel>
											</td>
										</tr>
								    	<tr>
								    		<td align="right" width="34%" height="30"><h:outputText value="Causa anulaci�n:" /></td>
								    		<td align="left"  width="66%" height="30">
								    			<h:selectOneMenu id="inCausa"  value="#{beanListaCertificado.causa}" >
													<f:selectItem itemValue="" itemLabel="Causa de cancelaci�n"/>
													<f:selectItems value="#{beanListaEstatus.listaEstatusCancelacion}"/>
												</h:selectOneMenu>
								    		</td>
								    	</tr>
						                <tr>
											<td align="right">
	     	                                	<h:outputText value="Fecha Cancelacion:" />
											</td>
											<td colspan="2" align="left">
		                                        <rich:calendar id="fechaCancela" datePattern="dd/MM/yyyy" required="true" value="#{beanListaCertificado.feccan}" />
		                                        <h:message for="fechaCancela" styleClass="errorMessage" />
	     	                                </td>
	                                    </tr>
										<tr height="10px">
											<td colspan="2" ></td>
										</tr>
	           	                		<tr>
	           	                		  	<td colspan="2" align="center">
	           	                		  	<h:commandButton action="#{beanListaCertificado.cancelaPoliza}" value="Cancelar" />  
											</td>
										</tr>	
										<tr>
											<td colspan="2" align="center">
												<br/>
												<h:outputText id="mensaje" value="#{beanListaCertificado.mensaje}" styleClass="respuesta"/>
												<br/>
												<br/>
											</td>
										</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
					    </table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>