<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<%@ page import="mx.com.santander.aseguradora.colectivos.view.bean.BeanListaClienteCertif" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:c="http://java.sun.com/jsp/jstl/core">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
    <script type="text/javascript" src="../../js/boxover.js"></script>
    <script type="text/javascript" src="../../js/colectivos.js"></script>
 	 <title>Extra Prima (Operaciones)</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
  <tr>
   <td>
     <div align="center">
	  <table class="encabezadoTabla" >
		<tbody>
	  	   <tr>
			  <td width="10%" align="center">Secci�n</td>
			  <td width="4%" align="center" bgcolor="#d90909">-</td>
			  <td align="left" width="64%">Detalle Extra Prima Operaciones</td>
			  <td align="right" width="12%" >Fecha</td>
			  <td align="left" width="10%">
				<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
			  </td>
		    </tr>
	     </tbody>
	 	 </table>
		   </div>
		   	  <rich:spacer height="10px"></rich:spacer>
		   	  <rich:dragIndicator id="indicador" />
		      <h:form id="frmextpma">
				<div align="center">
				 <a4j:region id="consulta">
				   <table class="botonera">
				<tbody>
				  <tr>
					<td class="frameTL"></td>
					<td class="frameTC"></td>
					<td class="frameTR"></td>
				  </tr>
				  <tr>
				    <td class="frameCL"></td>
					<td class="frameC">
			        <table class="tabla" >
				 <tbody>
					<tr>
					  <td>
				    	<table>
						   <tr>
							  <td align="right"><h:outputText value="Ramo:" /></td>  
							  <td align="left">
							     <h:selectOneMenu style="width:180px" id="ramo" value="#{beanExtraPrima.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
									<f:selectItem itemValue="" itemLabel="Ramo" />
									<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
									<a4j:support event="onchange"  action="#{beanExtraPrima.cargaProductos}" ajaxSingle="true" reRender="productos" />
								 </h:selectOneMenu>
								 <h:message for="ramo" styleClass="errorMessage"/>
							  </td>  
							  <td align="right"><h:outputText value="Negocio:" /></td>  
							  <td align="left">
							     <a4j:outputPanel id="productos" ajaxRendered="true">
								 <h:selectOneMenu style="width:180px" id="negocio" value="#{beanExtraPrima.valorProducto}" title="header=[Negocio] body=[Seleccione un negocio.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
								     <f:selectItem itemValue="" itemLabel="Negocio" />
								     <f:selectItems value="#{beanExtraPrima.listaProductos}" />
								 </h:selectOneMenu>
								 <h:message for="negocio" styleClass="errorMessage"/>
								 </a4j:outputPanel> 
							  </td>  
							</tr>
							
							<tr>
								 <td align="center" colspan="4">&nbsp;</td>
							</tr>
							
							<tr>
								<td align="right"><h:outputText value="ID:" /></td>  
								<td align="left"><h:inputText value="#{beanExtraPrima.idCertificado}"/></td>  
							    <td align="right"><h:outputText  value="R.F.C:" /></td>
								<td align="left">
									<h:inputText id="rfc" value="#{beanExtraPrima.rfc}">
										<a4j:support event="onblur" reRender="rfc"/>
									</h:inputText>
							</tr>
							
							<tr>
								<td align="center" colspan="4">&nbsp;</td>
							</tr>
							
							<tr>
								<td align="right"><h:outputText value="Apellido Paterno" /></td>
								<td align="left">
									<h:inputText id="ap" value="#{beanExtraPrima.apellidoPaterno}">
										<a4j:support event="onblur" reRender="ap"/>
									</h:inputText>
								</td>			     							     		
								<td align="right"><h:outputText value="Apellido Materno:" /></td>
								<td align="left">
									<h:inputText id="am" value="#{beanExtraPrima.apellidoMaterno}">
										<a4j:support event="onblur" reRender="am"/>
									</h:inputText>	
							</tr>
									
							<tr>
								<td align="center" colspan="4">&nbsp;</td>
							</tr>
							
            				<tr>
								<td align="right"><h:outputText value="Nombre:" /></td>  
								<td align="left">
									<h:inputText id="nombre"  value="#{beanExtraPrima.nombre}">
										<a4j:support event="onblur" reRender="nombre"/> 
									</h:inputText>	
								</td>	 
								<td align="right"><h:outputText value="Suma Asegurada:" /></td>
								<td align="left">
									<h:inputText id="sumaaseg" value="#{beanExtraPrima.suma}">
									 	<a4j:support event="onblur" reRender="suma"/>
									</h:inputText>	
								</td>	  	 
							</tr>
							
							<tr>
								<td align="center" colspan="4">&nbsp;</td>	
							</tr>
							
							<tr>
								<td align="right"><h:outputText value="Observaciones:" /></td>
								<td align="left" >
									<h:inputText id="observ" value="#{beanExtraPrima.observaciones}" style=" width:200px; height:40px">
										<a4j:support event="onblur" reRender="observaciones"/>
									</h:inputText>
								</td>	
								<td align="right"><h:outputText value="Porcentaje:" /></td>
								<td align="left" >
									<h:inputText maxlength="3" id="porcentaje" value="#{beanExtraPrima.porcentaje}" style=" width :35px;">
										<a4j:support event="onblur" reRender="porcentaje"/>
									</h:inputText>
								</td>	
							</tr>
							
							<tr>
								<td align="center" colspan="4">&nbsp;</td>
							</tr>
							
							<tr>
								<td align="center" colspan="4" bgcolor="aaaaaa">
								<table width="80%" border="1" bordercolor="777777">
								<tr>
									<td colspan="6" align="center" bgcolor="red">
										<h:outputText value="Afecciones:" />													
								 	</td>
								</tr>
								<tr>
									<td width="33%">
										<h:selectBooleanCheckbox id="bh1" value="#{beanExtraPrima.edad}">																					
											<a4j:support event="onblur" reRender="bh1"/>
										</h:selectBooleanCheckbox>
										<h:outputText value="Edad Avanzada" />	
									</td>
									<td width="34%">
										<h:selectBooleanCheckbox id="bh2" value="#{beanExtraPrima.cancer}">		
											<a4j:support event="onblur" reRender="bh2"/>
										</h:selectBooleanCheckbox>																			
										<h:outputText value="Cancer" />
									</td>
									<td width="33%">
										<h:selectBooleanCheckbox id="bh3" value="#{beanExtraPrima.diabetes}">
											<a4j:support event="onblur" reRender="bh3"/>
										</h:selectBooleanCheckbox>																					
										<h:outputText value="Diabetes" />
									</td>
								</tr>
								
								<tr>
									<td>
										<h:selectBooleanCheckbox id="bh4" value="#{beanExtraPrima.cardio}">
											<a4j:support event="onblur" reRender="bh4"/>
										</h:selectBooleanCheckbox>																					
										<h:outputText value="Cardiacas" />
									</td>
									<td>
										<h:selectBooleanCheckbox id="bh5" value="#{beanExtraPrima.vih}">
											<a4j:support event="onblur" reRender="bh5"/>
										</h:selectBooleanCheckbox>																					
										<h:outputText value="Sida" />
									</td>
									<td>
										<h:selectBooleanCheckbox id="bh6" value="#{beanExtraPrima.otros}">																					
											<a4j:support event="onblur" reRender="bh6"/>
										</h:selectBooleanCheckbox>
										<h:outputText value="Otros" />
									</td>
								</tr>
								
								<tr>
									<td colspan="6" align="center">
										<h:outputText value="Otros? Por favor, especifique:" />  
										<h:inputText id="otros" value="#{beanExtraPrima.causa}" style=" width:190px">	
											<a4j:support event="onblur" reRender="otros"/>
										</h:inputText>			     		
									</td>
								</tr>
							    </table>
							    </td>
						    </tr>
						    
						        <tr>
									<td align="center" colspan="4">&nbsp;</td>
								</tr>
								
								<tr>
									<td align="center" colspan="4"> 
			      						<a4j:commandButton styleClass="boton" value="Guardar"
			      							action="#{beanExtraPrima.extraprimas}" 
											onclick="this.disabled=true" oncomplete="this.disabled=false"
											title="header=[Guardar] body=[Proceso Inserta Informacion] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" ajaxSingle="true"/> 
									</td>  
								</tr>
							    </table>
								</td>
						</tr>
					</table>

				<table>
				   	<tbody>
				    		<tr>
				    			<td align="center">
					    			<a4j:status for="consulta" stopText=" ">
										<f:facet name="start">
											<h:graphicImage value="/images/ajax-loader.gif" />
										</f:facet>
									</a4j:status>
								</td>
							</tr>
					</tbody>
				</table>
					
					</td>
					    <td class="frameCR"></td>
				  </tr>
				    
				  <tr>
					  <td class="frameBL"></td>
					  <td class="frameBC"></td>
					  <td class="frameBR"></td>
				  </tr>
		    </tbody>
		</table>
		</a4j:region>
	        <br/>				    						
	     </div>
	  </h:form>
	</td>
   </tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div align="center">
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>