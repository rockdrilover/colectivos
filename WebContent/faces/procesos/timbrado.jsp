<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Timbrado
- Fecha: 09/10/2020
- Descripcion: Pantalla para Timbrar
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view locale="es_MX">
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallaxvii}" /></title>
	</head>
	
	<body onload="exibirPanelTimbrado(4);">
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaxvii}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallaxvii}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmTimbrado">
										<h:inputHidden value="#{beanTimbrado.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanTimbrado.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right15">
																			<h:outputText value="#{msgs.timbradotipobus}:" />
																		</td>
																		<td class="left30">
																			<h:selectOneMenu id="inTipoProc" value="#{beanTimbrado.tipoBusqueda}" 
																					title="header=[Tipo Busqueda] body=[Seleccione un tipo busqueda] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																				<f:selectItem itemValue="0" itemLabel="#{msgs.sistemaseleccione}" />
																				<f:selectItem itemValue="1" itemLabel="#{msgs.sistemacerti}" />
																				<f:selectItem itemValue="2" itemLabel="#{msgs.timbradorecibo}" />
																				<f:selectItem itemValue="3" itemLabel="#{msgs.timbradouuid}" />
																				<a4j:support oncomplete="javascript:exibirPanelTimbrado(#{beanTimbrado.tipoBusqueda});" event="onchange" action="#{beanTimbrado.limpiaResultados}"/>
																			</h:selectOneMenu>	
																		</td>
																	</tr>
																	
																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	
																	<tr class="texto_normal" id="trCertificado">
																		<td colspan="2">
																			<table class="tablaCen100">
																				<caption></caption>
																				<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.sistemaramo}" />:
																					</td>
									                   								<td class=left80>
									                   									<h:selectOneMenu id="inRamo" 
									                   													value="#{beanTimbrado.ramo}" 
									                   													binding="#{beanListaParametros.inRamo}">
																							<f:selectItem itemValue="0" itemLabel=" --- Selecione un ramo --- " />
																							<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																							<a4j:support event="onchange" 																		
																									action="#{beanListaParametros.cargaPolizas}" 
																									ajaxSingle="true" 
																									reRender="polizas" /> 
																						</h:selectOneMenu>
									                   								</td>
									                   							</tr>
									                   							<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.sistemapoliza}"/>:
																					</td>
									                   								<td class="left80">
									                   									<a4j:outputPanel id="polizas" ajaxRendered="true">
																							<h:selectOneMenu id="inPoliza" 
																										value="#{beanTimbrado.poliza}" 
																										binding="#{beanListaParametros.inPoliza}">
																								<f:selectItem itemValue="0" itemLabel=" --- Seleccione una póliza --- "/>
																								<f:selectItem itemValue="-1" itemLabel="Prima única"/>
																								<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																								<a4j:support event="onchange" 
																										action="#{beanListaParametros.cargaIdVenta}" 
																										ajaxSingle="true" 
																										reRender="idVenta" />
																							</h:selectOneMenu>
																						</a4j:outputPanel>
									                   								</td>
									                   							</tr>
									                   							<tr class="texto_normal">
									                   								<td class="right-div">
																						<h:outputText value="#{msgs.sistemaidventa}"/>:
																					</td>
									                   								<td class="left80">
									                   									<a4j:outputPanel id="idVenta" ajaxRendered="true">
																							<h:selectOneMenu id="inVenta"  
																										value="#{beanTimbrado.idVenta}"
																										disabled="#{beanListaParametros.habilitaComboIdVenta}">
																								<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																								<a4j:support event="onchange" ajaxSingle="true" reRender="respuestas"/>
																							</h:selectOneMenu>
																						</a4j:outputPanel>
									                   								</td>
									                   							</tr>
									                   							
									                   							<tr class="texto_normal">
									                   								<td class="right-div">
																						<h:outputText value="#{msgs.sistemapolizae}"/>:
																					</td>
									                   								<td class="left80">
									                   									<h:inputText id="polEspecifica" value="#{beanTimbrado.polEsp}" />
									                   								</td>
									                   							</tr>
									                   							
									                   							<tr class="texto_normal">
									                   								<td class="right-div">
																						<h:outputText value="#{msgs.sistemacerti}"/>:
																					</td>
									                   								<td class="left80">
									                   									<h:inputText id="certificado" value="#{beanTimbrado.certificado}" />
									                   								</td>
									                   							</tr>
									                   							
									                   							<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.sistemaEst}:" />
																					</td>
																					<td class="left80">
																						<h:inputText id="estatus" value="#{beanTimbrado.estatus}"  styleClass="wid100" />
																					</td>
																				</tr>
									                   							
									                   							<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.sistemafechad}:" />
																					</td>
																					<td class="left80">
																						<rich:calendar id="fechIni" popup="true"
																								value="#{beanTimbrado.feDesde}"
																								datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
																					</td>
																				</tr>
																				<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.sistemafechah}:" />
																					</td>
																					<td class="left80">
																						<rich:calendar id="fechFin" popup="true"
																								value="#{beanTimbrado.feHasta}"
																								datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>	
																	
																	<tr class="texto_normal" id="trRecibo">
																		<td colspan="2">
																			<table class="tablaCen100">
																				<caption></caption>
																				<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.timbradorecibo}:" />
																					</td>
																					<td class="left80">
																						<h:inputText id="recibo" value="#{beanTimbrado.recibo}" styleClass="wid100" />
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	
																	<tr class="texto_normal" id="trUuid" >
																		<td colspan="2">
																			<table class="tablaCen100">
																				<caption></caption>
																				<tr class="texto_normal">
																					<td class="right-div">
																						<h:outputText value="#{msgs.timbradouuid}:" />
																					</td>
																					<td class="left80">
																						<h:inputText id="uuid" value="#{beanTimbrado.uuid}" styleClass="wid200" />
																					</td>
																				</tr>
																			</table>		
																		</td>
																	</tr>	
																	
																	<tr class="texto_normal">
																		<td colspan="2">
																			<table class="right-div">
																				<caption></caption>
																				<tr class="texto_normal">
																					<td class="left80"><h:outputText value="#{msgs.sistemaespacio}" /></td>
																					<td class="right-div">
																						<a4j:commandButton id="btnConsulta"
																							styleClass="boton" value="#{msgs.botonconsultar}"
																							action="#{beanTimbrado.consulta}"
																							onclick="this.disabled=true;bloqueaPantalla();"
																							oncomplete="this.disabled=false;desbloqueaPantalla();muestraOcultaSeccion('resultado', 0);"
																							reRender="mensaje,secResultados,regTabla"/>
																						<rich:toolTip for="btnConsulta" value="Consulta Timbrados" />
																					</td>
																					<td class="right-div">
																						<a4j:commandButton 
																							ajaxSingle="true" 
																							value="#{msgs.botonnuevo}" 
																							styleClass="boton" 
																							id="btnNuevo" 
																							oncomplete="#{rich:component('newPanel')}.show();muestraOcultaSeccion('resultado', 0)" />
                    																	<rich:toolTip for="btnNuevo" value="Nuevo Timbrado" />
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	
																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen100">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<tr class="texto_normal" id="tr_sec_resultado">
																		<td> 
																			<c:if
																				test="${beanTimbrado.listaTimbrado == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanTimbrado.listaTimbrado != null}">
																				<rich:dataTable id="listaTimbrado" value="#{beanTimbrado.listaTimbrado}"
																						columnsWidth="12px,80px,5px,5px,8px,8px,5px,5px,5px,5px"
																						var="objTimbrado" rows="20" width="100%">
																					<f:facet name="header">
																						<h:outputText value="#{msgs.timbrados}" />
																					</f:facet>

																					<rich:column styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.sistemapoliza}" /></f:facet>
																						<h:outputText value="#{objTimbrado.factura.cofaCampov4}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.timbradouuid}" /></f:facet>
																						<h:outputText value="#{objTimbrado.factura.cofaCampov3}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.timbradorecibo}" /></f:facet>
																						<h:outputText value="#{objTimbrado.factura.cofaNuReciboFiscal}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.sistemaEst}" /></f:facet>
																						<h:outputText value="#{objTimbrado.factura.cofaStRecibo}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.timbradofecha}" /></f:facet>
																						<h:outputText value="#{objTimbrado.factura.cofaFeFacturacion}">
																							<f:convertDateTime pattern="dd/MM/yyyy"/>
																						</h:outputText>
																					</rich:column>
																					<rich:column styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.timbradofechac}" /></f:facet>
																						<h:outputText value="#{objTimbrado.recibo.coreFeStatus}" >
																							<f:convertDateTime pattern="dd/MM/yyyy"/>
																						</h:outputText>
																					</rich:column>
																					
																					<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" 
					                    															id="detallelink" 
					                    															reRender="listaDetalle" 
					                    															action="#{beanTimbrado.getDetalle}"
					                    															onclick="bloqueaPantalla();" 
					                    															oncomplete="#{rich:component('detPanel')}.show();desbloqueaPantalla();muestraOcultaSeccion('resultado', 1)">
					                        												<h:graphicImage value="../../images/certificados.png" styleClass="bor0" />                        												
					                        												<f:setPropertyActionListener value="#{objTimbrado}" target="#{beanTimbrado.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="detallelink" value="Ver Detalle" />
																                	</rich:column>
																					
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaTimbrado" maxPages="10" page="#{beanTimbrado.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<rich:spacer height="15px"></rich:spacer>
	
		<%@include file="/WEB-INF/includes/parametrizacion/modalDetalleTimbrado.jsp"%>
		
		<%@include file="/WEB-INF/includes/parametrizacion/modalNuevoTimbrado.jsp"%>

		<%@include file="/WEB-INF/includes/parametrizacion/modalDatosTimbrar.jsp"%>		
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
    	
		<div>
			<%@include file="../footer.jsp"%>
		</div>
	</body>
</html>
</f:view>

