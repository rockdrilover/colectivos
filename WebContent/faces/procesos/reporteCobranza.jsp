<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Cobranza - Reportes
- Fecha: 05/12/2022
- Descripcion: Pantalla para Generacion de reportes Cobranza
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>


<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.cobranzareportestitulo}" /></title>
	</head>

	<body>
		<h:form id="frmReporteador">
		<div class="center_div"><%@include file="../header.jsp" %></div> 
		<div class="center_div">
			<table class="tablaPrincipal">			
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div class="center_div">
									<table class="encabezadoTabla" >
										<caption></caption>
										<tr>
											<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
											<td class="encabezado2"><h:outputText value="#{msgs.sistemax}" /></td>
											<td class="encabezado3"><h:outputText value="#{msgs.cobranzareportestitulo}" /></td>
											<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
											<td class="encabezado5">
												<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" />
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div class="center_div">
				    					<h:inputHidden value="#{beanReporteCobranza.beanGen.bean}" id="hidLoad" ></h:inputHidden>
					    				<table class="tablaCen90">
					    					<caption></caption>
						    				<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											<tr>
												<td>
													<h:outputText id="mensaje" value="#{beanReporteCobranza.beanGen.strRespuesta}" styleClass="error"/>
												</td>
											</tr>	
											<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
					    				</table>
				    						    								    				
					    				<a4j:outputPanel id="consultaDatos">
					    				<table class="tablaCen90">
										<tr>
											<td>
												<fieldset class="borfieldset">
													<legend>
														<span id="img_sec_consulta_m">
															<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
														</span>
														<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
													</legend>
													
													<table class="tablaCen90" border="0">	
														<caption></caption>
														<tr class="texto_normal">
															<td class="right-div">
																<h:outputText value="#{msgs.tuiiotprep}:" />
															</td>
															<td class="left80">
																<h:selectOneMenu id="tipo" onchange="muestraFiltrosRepCob(this.value);" value="#{beanReporteCobranza.tipo}" title="header=[Tipo de Reporte] body=[Seleccione una tipo de REPORTE.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																	<f:selectItem itemValue="0"	itemLabel="Seleccione un tipo reporte" />
																	<f:selectItem itemValue="1"	itemLabel="Estado Cuenta" />
																	<f:selectItem itemValue="2"	itemLabel="Estatus Cobranza" />
																	<f:selectItem itemValue="3"	itemLabel="Estatus Cobro" />
																	<f:selectItem itemValue="4"	itemLabel="Flujo Efectivo" />
																	<f:selectItem itemValue="5"	itemLabel="Pago Comisiones" />
																	<f:selectItem itemValue="6"	itemLabel="Provisi�n Emisi�n" />
																	<f:selectItem itemValue="7"	itemLabel="Provisi�n Emisi�n Contable"/>
																	<f:selectItem itemValue="8"	itemLabel="Recibos Rehabilitados" />
																</h:selectOneMenu>
															</td>
														</tr>
														
														<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
														
							                    		<tr class="texto_normal" style="display:none;" id="trRamo">
															<td class="right15">
																<h:outputText value="#{msgs.sistemaramo}:" />
															</td>
															<td class="left30">
																<h:selectOneMenu id="inRamo" value="#{beanReporteCobranza.beanGen.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
												    				<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
												    				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
												    				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas"/>
												    			</h:selectOneMenu>
															</td>
														</tr>
														<tr class="texto_normal" style="display:none;" id="trPoliza">
															<td class="right15">
																<h:outputText value="#{msgs.sistemapoliza}:" />
															</td>
															<td class="left30">
																<a4j:outputPanel id="polizas">
																	<h:selectOneMenu id="inPoliza"  value="#{beanReporteCobranza.beanGen.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" style=" width : 190px;">
																		<f:selectItem itemValue="0" itemLabel="Seleccione una p�liza"/>
																		<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																	</h:selectOneMenu>
																</a4j:outputPanel>
															</td>
														</tr>
													
														<tr class="texto_normal" style="display:none;" id="trEstatus">
															<td class="right15">
																<h:outputText value="#{msgs.sistemaEst}:" />
															</td>
															<td class="left30">
																<h:selectOneMenu id="estatus" value="#{beanReporteCobranza.estatus}" title="header=[Id venta] body=[Seleccione una ESTATUS.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																	<f:selectItem itemValue="0"	itemLabel="Seleccione el Estatus" />
																	<f:selectItem itemValue="1"	itemLabel="Pendiente" />
																	<f:selectItem itemValue="2"	itemLabel="Cobrado" />
																	<f:selectItem itemValue="3"	itemLabel="Cancelado" />
																</h:selectOneMenu>
															</td>
														</tr>
														
														<tr class="texto_normal" style="display:none;" id="trFeIni">
															<td class="right15">
																<h:outputText value="#{msgs.sistemafechad}:" />
															</td>
															<td class="left30">
																<rich:calendar id="fechIni" popup="true"
																		value="#{beanReporteCobranza.beanGen.feDesde}"
																		datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
															</td>
														</tr>
														<tr class="texto_normal" style="display:none;" id="trFeFin">
															<td class="right15">
																<h:outputText value="#{msgs.sistemafechah}:" />
															</td>
															<td class="left30">
																<rich:calendar id="fechFin" popup="true"
																		value="#{beanReporteCobranza.beanGen.feHasta}"
																		datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
															</td>
														</tr>	
														
							                    		<tr class="texto_normal">
							                    			<td class="right-div" colspan="2">	
																<h:commandButton id="btnReporte" 
                                                            			action="#{beanReporteCobranza.consultaReporteComisiones}"
                                                            			onclick="bloqueaPantalla();" onblur="desbloqueaPantalla();"
                                                                		value="Extraer Reporte">
                                                                	<a4j:support event="onclick"
                                                                			ajaxSingle="true"                                                                		
                                                                    		reRender="mensaje" />
                                                            	</h:commandButton>															
																<rich:toolTip for="btnReporte" value="Extrae Reporte" />													
															</td>
							                    		</tr>
							                    		
							                    		<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
							                    		
							                    	</table>
							                    </fieldset>
											</td>
										</tr>
										<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
					    				</table>
					    				</a4j:outputPanel>
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
    	
		<div><%@include file="../footer.jsp" %></div>
    	
	</body>

</html>
</f:view>