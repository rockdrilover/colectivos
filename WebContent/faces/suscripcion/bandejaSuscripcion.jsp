<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/colectivos.js"></script>
<title>Bandeja Suscripci&oacute;n</title>
</head>

<body
	onload="document.getElementById('frmFiltroBandejaSuscripcion:trickyButton').click();">
	<f:view>
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center"><%@include file="../header.jsp"%>
		</div>

		<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>
						<td align="center">
							<table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<table class="encabezadoTabla">
												<tbody>
													<tr>
														<td width="10%" align="center">Procesos</td>
														<td width="4%" align="center" bgcolor="#d90909">||||</td>
														<td align="left" width="64%">Bandeja
															Suscripci&oacute;n</td>
														<td align="right" width="14%">Fecha:</td>
														<td align="left" width="10%"><input name="fecha"
															readonly="readonly"
															value="<%=request.getSession().getAttribute("fecha")%>"
															size="7"></td>
													</tr>
												</tbody>
											</table> <br>
											<div align="center">
												<rich:spacer height="15" />
												<table class="botonera">

													<tr>
														<td class="frameTL"></td>
														<td class="frameTC"></td>
														<td class="frameTR"></td>
													</tr>
													<tr>
														<td class="frameCL"></td>
														<td class="frameC"><h:form
																id="frmFiltroBandejaSuscripcion">
																<a4j:region id="filtroCarga">
																	<!-- INICIO CAMPOS FILTRO -->

																	<table border="0">
																		<a4j:outputPanel id="mensajes" ajaxRendered="true">

																			<tr>
																				<td><h:outputText
																						value="#{beanSuscripcion.message}"
																						styleClass="error" /></td>
																			</tr>
																		</a4j:outputPanel>
																		<tr>
																			<td colspan="1">&nbsp;</td>
																		</tr>
																		<tr id="trBandejaSuscripcion">
																			<td align="center" colspan="3"><rich:panel
																					id="paBandejaSuscripcion" styleClass="paneles">
																					<f:facet name="header">
																						<h:outputText value="Area" />
																					</f:facet>
																					<table width="100%" border="0" cellpadding="0"
																						cellspacing="3" align="center">
																						<tr>
																							<td align="right" width="10%"><h:outputText
																									value="Estatus:" /></td>
																							<td width="40%"><h:selectOneMenu
																									id="dictamen"
																									value="#{beanSuscripcion.filtroSuscripcionDTO.dictamen}">
																									<f:selectItem itemValue="" itemLabel="Todos" />
																									<f:selectItem itemValue="P"
																										itemLabel="Pendiente" />
																									<f:selectItem itemValue="A"
																										itemLabel="Aceptados" />
																									<f:selectItem itemValue="R"
																										itemLabel="Rechazados" />
																									<f:selectItem itemValue="E"
																										itemLabel="En Evaluacion" />
																									<f:selectItem itemValue="S"
																										itemLabel="Suspendido" />
																									<f:selectItem itemValue="T"
																										itemLabel="Completado" />
																								</h:selectOneMenu></td>
																							<td align="right" width="10%"><h:outputText
																									value="Fecha Carga:" /></td>
																							<td width="40%"><rich:calendar
																									id="fechaCarga" popup="true"
																									value="#{beanSuscripcion.filtroSuscripcionDTO.fechaCarga}"
																									datePattern="dd/MM/yyyy"
																									styleClass="calendario" /></td>
																						</tr>
																						<tr>
																							<td align="right" width="10%"><h:outputText
																									value="Folio CH:" /></td>
																							<td width="40%"><h:inputText id="folioCH"
																									value="#{beanSuscripcion.filtroSuscripcionDTO.folioCH}" /></td>
																							<td align="right" width="10%"><h:outputText
																									value="Fecha Solicitud:" /></td>
																							<td width="40%"><rich:calendar
																									id="fechaSolicitud" popup="true"
																									value="#{beanSuscripcion.filtroSuscripcionDTO.fechaSolicitud}"
																									datePattern="dd/MM/yyyy"
																									styleClass="calendario" /></td>
																						</tr>
																						<tr>

																							<td align="right" colspan="4"><a4j:commandButton
																									styleClass="boton" value="Consultar"
																									action="#{beanSuscripcion.consultaSuscripciones}"
																									onclick="this.disabled=true"
																									oncomplete="this.disabled=false"
																									reRender="mensaje,regTabla,divRespuesta" />
																								<div style="display: none;">
																									<a4j:commandButton id="trickyButton"
																										action="#{beanSuscripcion.inicializaCarga}"
																										reRender="mensaje,divRespuesta, regTabla, regSeguimiento" />
																								</div></td>
																						</tr>
																					</table>

																				</rich:panel></td>

																		</tr>

																		<tr class="espacio_15">
																			<td>&nbsp;</td>
																		</tr>
																		<tr>
																			<td align="center"><a4j:status for="filtroCarga"
																					stopText=" ">
																					<f:facet name="start">
																						<h:graphicImage value="/images/ajax-loader.gif" />
																					</f:facet>
																				</a4j:status></td>
																		</tr>
																		<tr>
																			<td colspan="3"><a4j:outputPanel id="regTabla"
																					ajaxRendered="true">
																					<div style="overflow: auto; width: 700px;">

																						<rich:dataTable id="suscripcion"
																							value="#{beanSuscripcion.suscripciones}"
																							var="registro" columns="14" rows="5" width="100%">
																							<f:facet name="header">
																								<h:outputText value="Suscripciones" />
																							</f:facet>
																							<rich:column>
																								<h:selectBooleanCheckbox id="checkboxId"
																									value="#{registro.selected}">
																									<a4j:support
																										action="#{beanSuscripcion.consultaSeguimientoAreas}"
																										event="onclick"
																										reRender="regSeguimiento,divRespuesta">
																										<a4j:actionparam name="actionParemId"
																											value="#{registro.cosuId}"
																											assignTo="#{beanSuscripcion.selectID}" />

																										<a4j:actionparam name="actionParemSelectId"
																											value="#{registro.selected}"
																											assignTo="#{beanSuscripcion.checking}" />
																									</a4j:support>
																								</h:selectBooleanCheckbox>
																							</rich:column>

																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="Estatus" />
																								</f:facet>
																								<h:outputText value="#{registro.cosuDictamen}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="Folio Riesgos" />
																								</f:facet>
																								<h:outputText
																									value="#{registro.cosuFolioRiesgos}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="Folio CH" />
																								</f:facet>
																								<h:outputText value="#{registro.cosuFolioCh}" />
																							</rich:column>

																							<rich:column width="200px;">
																								<f:facet name="header">
																									<h:outputText value="Nombre" />
																								</f:facet>
																								<div style="overflow: hidden; width: 200px">
																									<h:outputText value="#{registro.cosuNombre}" />
																								</div>
																							</rich:column>

																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="Participacion" />
																								</f:facet>
																								<h:outputText
																									value="#{registro.cosuParticipacion}" />
																							</rich:column>

																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="F. R. Aseguradora" />
																								</f:facet>
																								<h:outputText
																									value="#{registro.cosuFechaRecAseg}">
																									<f:convertDateTime pattern="dd/MM/yyyy" />
																								</h:outputText>
																							</rich:column>

																							<rich:column width="200px;">
																								<f:facet name="header">
																									<h:outputText value="Plaza" />
																								</f:facet>
																								<div style="overflow: hidden; width: 200px">
																									<h:outputText value="#{registro.cosuPlaza}" />
																								</div>
																							</rich:column>

																							<rich:column width="250px;">
																								<f:facet name="header">
																									<h:outputText value="Suc. / Ejec." />
																								</f:facet>
																								<div style="overflow: hidden; width: 250px">
																									<h:outputText value="#{registro.cosuSucEjec}" />
																								</div>
																							</rich:column>
																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="F. Nacimiento" />
																								</f:facet>
																								<h:outputText value="#{registro.cosuFechaNac}">
																									<f:convertDateTime pattern="dd/MM/yyyy" />
																								</h:outputText>
																							</rich:column>
																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="Monto" />
																								</f:facet>
																								<h:outputText value="#{registro.cosuMonto}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header">
																									<h:outputText value="F. Sol Cred" />
																								</f:facet>
																								<h:outputText
																									value="#{registro.cosuFechaSolCred}">
																									<f:convertDateTime pattern="dd/MM/yyyy" />
																								</h:outputText>
																							</rich:column>
																							<rich:column width="10px;">

																								<f:facet name="header">

																									<h:outputText value="Local/Foraneo" />


																								</f:facet>


																								<h:outputText value="#{registro.cosuLocal}" />

																							</rich:column>
																							<rich:column width="30px;">
																								<f:facet name="header">
																									<h:outputText value="Exam. Medico" />
																								</f:facet>

																								<h:outputText value="#{registro.cosuExamMedico}" />

																							</rich:column>
																							<f:facet name="footer">
																								<rich:datascroller align="right"
																									for="suscripcion" maxPages="10"
																									page="#{beanSuscripcion.numPagina}" />
																							</f:facet>
																						</rich:dataTable>
																					</div>
																				</a4j:outputPanel></td>
																		</tr>

																	</table>
																	<!-- FIN CAMPOS FILTRO -->
																</a4j:region>

															</h:form></td>
													</tr>
													<tr>
														<td colspan="3" align="center"><h:form
																id="frmOperaciones">
																<rich:messages style="color:red;"></rich:messages>

																<table width="80%" border="0" cellpadding="0"
																	cellspacing="0" align="right">
																	<tr class="espacio_5">
																		<td>&nbsp;</td>
																	</tr>
																	<tr class="texto_normal">
																		<td><a4j:commandButton ajaxSingle="true"
																				value="Enviar Tarea" styleClass="boton"
																				id="btnEnviarTarea"
																				action="#{beanSuscripcion.consultaAreaAsignar}"
																				oncomplete="if (#{beanSuscripcion.permiso}) {#{rich:component('panelEnviarTarea')}.show()}"
																				reRender="regTablaGrupos" /> <rich:toolTip
																				for="btnEnviarTarea" value="Enviar Tarea" /></td>
																		<td></td>
																		<td><a4j:commandButton ajaxSingle="true"
																				value="Completar" styleClass="boton"
																				id="btnCompletar"
																				oncomplete="if (#{beanSuscripcion.permiso}) {#{rich:component('panelCompletarRegistro')}.show()}"
																				action="#{beanSuscripcion.seleccionaModificar}" />
																			<rich:toolTip for="btnCompletar" value="Completar" /></td>
																		<td><a4j:commandButton ajaxSingle="true"
																				value="Dictamen" styleClass="boton" id="btnDictamen"
																				oncomplete="if (#{beanSuscripcion.permiso}) {#{rich:component('panelDictamen')}.show()}"
																				action="#{beanSuscripcion.seleccionaModificar}" />
																			<rich:toolTip for="btnDictamen" value="Dictamen" /></td>

																		<td><a4j:commandButton ajaxSingle="true"
																				value="Suspender" styleClass="boton"
																				id="btnSuspender" reRender="regTabla"
																				action="#{beanSuscripcion.suspenderSuscripcion}" />
																			<rich:toolTip for="btnDictamen" value="Dictamen" /></td>
																	</tr>
																</table>
															</h:form></td>
													</tr>
													<tr>
														<td colspan="3"><a4j:outputPanel id="regSeguimiento"
																ajaxRendered="true">
																<h:form id="frmTablaSeguimiento">

																	<rich:dataTable id="tableSeguimiento"
																		value="#{beanSuscripcion.seguimientos}"
																		var="seguimiento" columns="10"
																		columnsWidth="10px, 10px, 10px, 10px, 10px, 10px"
																		width="90%">
																		<f:facet name="header">
																			<h:outputText value="Seguimiento con areas" />
																		</f:facet>
																		<rich:column>
																			<f:facet name="header">
																				<h:outputText value="Area" />
																			</f:facet>
																			<h:outputText value="#{seguimiento.coseArea}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header">
																				<h:outputText value="F. Asignacion" />
																			</f:facet>
																			<h:outputText
																				value="#{seguimiento.coseFechaAsignacion}">
																				<f:convertDateTime pattern="dd/MM/yyyy" />
																			</h:outputText>
																		</rich:column>
																		<rich:column>
																			<f:facet name="header">
																				<h:outputText value="Usuario" />
																			</f:facet>
																			<h:outputText
																				value="#{seguimiento.coseUsuarioAtendio}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header">
																				<h:outputText value="Fecha" />
																			</f:facet>
																			<h:outputText
																				value="#{seguimiento.coseFechaAtencion}">
																				<f:convertDateTime pattern="dd/MM/yyyy" />
																			</h:outputText>
																		</rich:column>
																		<rich:column>
																			<f:facet name="header">
																				<h:outputText value="Obs Asignacion" />
																			</f:facet>
																			<h:outputText
																				value="#{seguimiento.coseObsAsig}" />
																		</rich:column>
																		<rich:column>
																			<f:facet name="header">
																				<h:outputText value="Obs Area" />
																			</f:facet>
																			<h:outputText
																				value="#{seguimiento.coseObservaciones}" />
																		</rich:column>

																		<rich:column title="Atender" width="5%"
																			styleClass="texto_centro">
																			<f:facet name="header">
																				<h:outputText value=" Atender " />
																			</f:facet>
																			<a4j:commandLink ajaxSingle="true" id="editlink"
																				oncomplete="#{rich:component('editPanelSeguimiento')}.show()">
																				<h:graphicImage value="../../images/edit_page.png"
																					style="border:0" />
																				<f:setPropertyActionListener value="#{seguimiento}"
																					target="#{beanSuscripcion.seguimientoActual}" />
																			</a4j:commandLink>
																			<rich:toolTip for="editlink" value="Atender" />
																		</rich:column>

																		<rich:column title="Archivos" width="5%"
																			styleClass="texto_centro">
																			<f:facet name="header">
																				<h:outputText value=" Archivos " />
																			</f:facet>
																			<a4j:commandLink ajaxSingle="true" id="editArchivos"
																				oncomplete="#{rich:component('panelCargarArchivos')}.show()"
																				action="#{beanSuscripcion.consultarArchivos}"
																				reRender="regTablaArchivos">
																				<h:graphicImage value="../../images/componentes.png"
																					style="border:0" />
																				<f:setPropertyActionListener value="#{seguimiento}"
																					target="#{beanSuscripcion.seguimientoActual}" />

																			</a4j:commandLink>
																			<rich:toolTip for="editlink" value="Archivos" />
																		</rich:column>
																	</rich:dataTable>
																</h:form>
															</a4j:outputPanel></td>
													</tr>

													<tr>
														<td class="frameBL"></td>
														<td class="frameBC"></td>
														<td class="frameBR"></td>
													</tr>
												</table>

												<br />
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div align="center">
			<br />
			<%@ include file="../footer.jsp"%></div>

		<rich:modalPanel id="editPanelSeguimiento" autosized="true"
			width="500">
			<f:facet name="header">
				<h:outputText value="Hacer Tarea" />
			</f:facet>
			<f:facet name="controls">
				<h:panelGroup>
					<h:graphicImage value="../../images/cerrarModal.png"
						id="hidelinkNewpanelCompletarSeguimiento" styleClass="hidelink" />
					<rich:componentControl for="editPanelSeguimiento"
						attachTo="hidelinkNewpanelCompletarSeguimiento" operation="hide"
						event="onclick" />
				</h:panelGroup>
			</f:facet>
			<h:form id="frmEditarSeguimiento">
				<rich:messages style="color:red;"></rich:messages>
				<h:panelGrid columns="1">
					<a4j:outputPanel ajaxRendered="true">
						<h:panelGrid columns="2">
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double" />

							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<h:outputText value="Folio CH: " />
							<h:inputText id="cosuFolioCh" readonly="true"
								value="#{beanSuscripcion.modifica.cosuFolioCh}"
								style="width: 250px" required="true"
								requiredMessage="* Folio CH - Campo Requerido" />

							<h:outputText value="Nombre: " />
							<h:inputText id="cosuNombre" readonly="true"
								value="#{beanSuscripcion.modifica.cosuNombre}"
								style="width: 250px" required="true" />

							<h:outputText value="Area: " />
							<h:inputText id="coseArea" readonly="true"
								value="#{beanSuscripcion.seguimientoActual.coseArea}"
								style="width: 250px" required="true" />
							
							
							<h:outputText value="Obs Asignacion: " />
							<h:inputText id="coseObsAsig" readonly="true"
								value="#{beanSuscripcion.seguimientoActual.coseObsAsig}"
								style="width: 350px" required="true" />
								
							<h:outputText value="Obs Area: " />
							<h:inputText id="coseObservaciones"
								value="#{beanSuscripcion.seguimientoActual.coseObservaciones}"
								style="width: 250px" />


						</h:panelGrid>
					</a4j:outputPanel>
					<a4j:commandButton value="Enviar"
						action="#{beanSuscripcion.atenderTarea}" reRender="regSeguimiento"
						oncomplete="#{rich:component('editPanelSeguimiento')}.hide()" />

				</h:panelGrid>
			</h:form>
		</rich:modalPanel>

		<!-- Pop UP DICTAMEN -->

		<rich:modalPanel id="panelDictamen" autosized="true" width="400">
			<f:facet name="header">
				<h:outputText value="Dictamen" />
			</f:facet>
			<f:facet name="controls">
				<h:panelGroup>
					<h:graphicImage value="../../images/cerrarModal.png"
						id="hidelinkNew" styleClass="hidelink" />
					<rich:componentControl for="panelDictamen" attachTo="hidelinkNew"
						operation="hide" event="onclick" />
				</h:panelGroup>
			</f:facet>
			<h:form id="frmDictamen">
				<rich:messages style="color:red;"></rich:messages>
				<h:panelGrid columns="1">
					<a4j:outputPanel ajaxRendered="true">
						<h:panelGrid columns="2">
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double" />

							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<h:outputText value="Folio CH: " />
							<h:inputText id="cosuFolioCh" readonly="true"
								value="#{beanSuscripcion.modifica.cosuFolioCh}"
								style="width: 250px" required="true"
								requiredMessage="* Folio CH - Campo Requerido" />

							<h:outputText value="Nombre: " />
							<h:inputText id="cosuNombre" readonly="true"
								value="#{beanSuscripcion.modifica.cosuNombre}"
								style="width: 250px" required="true"
								requiredMessage="* Folio CH - Campo Requerido" />
							<h:outputText value="Dictamen: " />
							<h:selectOneMenu id="dictamen"
								value="#{beanSuscripcion.modifica.cosuDictamen}">
								<f:selectItem itemValue="A" itemLabel="Aceptado" />
								<f:selectItem itemValue="R" itemLabel="Rechazado" />
							</h:selectOneMenu>
							<h:outputText value="Fecha Respuesta a Sucursal:" />
							<rich:calendar id="cosuFechaRespSuc" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaRespSuc}"
								datePattern="dd/MM/yyyy" styleClass="calendario" />
							<h:outputText value="Fecha  Notificacion de dictamen:" />
							<rich:calendar id="cosuFechaNotDic" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaNotDic}"
								datePattern="dd/MM/yyyy" styleClass="calendario" />

						</h:panelGrid>
					</a4j:outputPanel>
					<a4j:commandButton value="Guardar"
						action="#{beanSuscripcion.actualizaEstatusDictamen}"
						reRender="mensaje,regTabla"
						oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('panelDictamen')}.hide();" />
				</h:panelGrid>



			</h:form>
		</rich:modalPanel>

		<!-- FIN Pop UP DICTAMEN -->


		<!-- Pop UP COMPLETAR REGISTRO -->
		<rich:modalPanel id="panelCompletarRegistro" autosized="true"
			width="400">
			<f:facet name="header">
				<h:outputText value="Completar Registro" />
			</f:facet>
			<f:facet name="controls">
				<h:panelGroup>
					<h:graphicImage value="../../images/cerrarModal.png"
						id="hidelinkNewpanelCompletarRegistro" styleClass="hidelink" />
					<rich:componentControl for="panelCompletarRegistro"
						attachTo="hidelinkNewpanelCompletarRegistro" operation="hide"
						event="onclick" />
				</h:panelGroup>
			</f:facet>
			<h:form id="frmCompletarRegistro">
				<rich:messages style="color:red;"></rich:messages>
				<h:panelGrid columns="1">
					<a4j:outputPanel ajaxRendered="true">
						<h:panelGrid columns="2">
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double" />

							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<h:outputText value="Folio CH: " />
							<h:inputText id="cosuFolioCh" readonly="true"
								value="#{beanSuscripcion.modifica.cosuFolioCh}"
								style="width: 250px" required="true"
								requiredMessage="* Folio CH - Campo Requerido" />

							<h:outputText value="Nombre: " />
							<h:inputText id="cosuNombre" readonly="true"
								value="#{beanSuscripcion.modifica.cosuNombre}"
								style="width: 250px" required="true" />
							<h:outputText value="Aplica Examen medico: " />
							<h:selectOneMenu id="cosuExamMedico"
								value="#{beanSuscripcion.modifica.cosuExamMedico}">

								<f:selectItem itemValue="S" itemLabel="SI" />
								<f:selectItem itemValue="N" itemLabel="NO" />
								<a4j:support event="onchange"
									action="#{beanSuscripcion.habilitaExamenMedico}"
									reRender="cosuFechaSolExam,cosuFechaCitExam,cosuMotExamMed,cosuFechaAplExam,cosuFechaResult,cosuFechaResDoc,cosuFechaResult" />

							</h:selectOneMenu>
							<h:outputText value="Fecha Solicitud de Examen :" />
							<rich:calendar id="cosuFechaSolExam" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaSolExam}"
								datePattern="dd/MM/yyyy" styleClass="calendario"
								disabled="#{beanSuscripcion.bExamMedico}" />
							<h:outputText value="Fecha  Cita de Examen:" />
							<rich:calendar id="cosuFechaCitExam" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaCitExam}"
								datePattern="dd/MM/yyyy" styleClass="calendario"
								disabled="#{beanSuscripcion.bExamMedico}" />

							<h:outputText value="Motivo Examen Medico: " />
							<h:inputText id="cosuMotExamMed"
								value="#{beanSuscripcion.modifica.cosuMotExamMed}"
								style="width: 250px" disabled="#{beanSuscripcion.bExamMedico}" />

							<h:outputText value="Fecha  Aplicacion Examen:" />
							<rich:calendar id="cosuFechaAplExam" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaAplExam}"
								datePattern="dd/MM/yyyy" styleClass="calendario"
								disabled="#{beanSuscripcion.bExamMedico}" />


							<h:outputText value="Fecha  Resultados:" />
							<rich:calendar id="cosuFechaResult" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaResult}"
								datePattern="dd/MM/yyyy" styleClass="calendario"
								disabled="#{beanSuscripcion.bExamMedico}" />


							<h:outputText value="Fecha  Respuesta Doctor:" />
							<rich:calendar id="cosuFechaResDoc" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaResDoc}"
								datePattern="dd/MM/yyyy" styleClass="calendario"
								disabled="#{beanSuscripcion.bExamMedico}" />


							<h:outputText value="Fecha  Firma:" />
							<rich:calendar id="cosuFechaFirm" popup="true"
								value="#{beanSuscripcion.modifica.cosuFechaFirm}"
								datePattern="dd/MM/yyyy" styleClass="calendario" />

							<h:outputText value="Monto Firmado: " />
							<h:inputText id="cosuMontoFirmado"
								value="#{beanSuscripcion.modifica.cosuMontoFirmado}"
								style="width: 250px" />

							<h:outputText value="Numero de Creditro: " />
							<h:inputText id="cosuNumCredito"
								value="#{beanSuscripcion.modifica.cosuNumCredito}"
								style="width: 250px" />


							<h:outputText value="Extra Prima: " />
							<h:inputText id="cosuExtraFirma"
								value="#{beanSuscripcion.modifica.cosuExtraFirma}"
								style="width: 250px" />

							<h:outputText value="Observaciones: " />
							<h:inputText id="cosuObsWf"
								value="#{beanSuscripcion.modifica.cosuObsWf}"
								style="width: 250px" />



						</h:panelGrid>
					</a4j:outputPanel>
					<a4j:commandButton value="Guardar"
						action="#{beanSuscripcion.actualizaDictamen}" reRender="regTabla" />
				</h:panelGrid>



			</h:form>
		</rich:modalPanel>

		<!-- FIN Pop UP COMPLETAR REGISTRO -->
		<!-- Pop UP ENVIAR TAREA -->

		<rich:modalPanel id="panelEnviarTarea" autosized="true" width="500">
			<f:facet name="header">
				<h:outputText value="Enviar Tarea" />
			</f:facet>
			<f:facet name="controls">
				<h:panelGroup>
					<h:graphicImage value="../../images/cerrarModal.png"
						id="hidelinkNewpanelEnviarRegistro" styleClass="hidelink" />
					<rich:componentControl for="panelEnviarTarea"
						attachTo="hidelinkNewpanelEnviarRegistro" operation="hide"
						event="onclick" />
				</h:panelGroup>
			</f:facet>
			<h:form id="frmEnviarTarea">
				<rich:messages style="color:red;"></rich:messages>
				<h:panelGrid columns="1">
					<a4j:outputPanel ajaxRendered="true">
						<h:panelGrid columns="2">
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double" />

							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<h:outputText value="Folio CH: " />
							<h:inputText id="cosuFolioCh" readonly="true"
								value="#{beanSuscripcion.modifica.cosuFolioCh}"
								style="width: 250px" required="true"
								requiredMessage="* Folio CH - Campo Requerido" />

							<h:outputText value="Nombre: " />
							<h:inputText id="cosuNombre" readonly="true"
								value="#{beanSuscripcion.modifica.cosuNombre}"
								style="width: 250px" required="true" />
						</h:panelGrid>
						<a4j:outputPanel id="regTablaGrupos" ajaxRendered="true">
							<rich:dataTable id="tablaGrupos"
								value="#{beanSuscripcion.grupos}" var="grupo" columns="10"
								columnsWidth="10px, 10px" width="500">
								<rich:column>
									<h:selectBooleanCheckbox id="checkboxId"
										value="#{grupo.selected}">
										<a4j:support event="onclick" reRender="dataTableID">
											<a4j:actionparam name="actionParemId" value="#{grupo.id}" />
										</a4j:support>
									</h:selectBooleanCheckbox>
								</rich:column>
								<rich:column>
									<f:facet name="header">
										<h:outputText value="Area" />
									</f:facet>
									<h:outputText value="#{grupo.descripcion}" />
								</rich:column>

								<rich:column>
									<f:facet name="header">
										<h:outputText value="Observaciones" />
									</f:facet>
									<h:inputText value="#{grupo.observaciones}"
										style="width: 350px" />
								</rich:column>


							</rich:dataTable>
						</a4j:outputPanel>
					</a4j:outputPanel>
					<a4j:commandButton value="Asignar"
						action="#{beanSuscripcion.enviaTareas}" reRender="mensaje"
						oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('panelEnviarTarea')}.hide();" />

				</h:panelGrid>

			</h:form>
			<!-- FIN Pop UP ENVIAR TAREA -->

			<!-- Pop UP Cargar archivos -->
			<rich:modalPanel id="panelCargarArchivos" autosized="true"
				width="600">
				<f:facet name="header">
					<h:outputText value="Cargar Archivos" />
				</f:facet>
				<f:facet name="controls">
					<h:panelGroup>
						<h:graphicImage value="../../images/cerrarModal.png"
							id="hidelinkNewpanelCargarArchivos" styleClass="hidelink" />
						<rich:componentControl for="panelCargarArchivos"
							attachTo="hidelinkNewpanelCargarArchivos" operation="hide"
							event="onclick" />
					</h:panelGroup>
				</f:facet>
				<h:form id="frmCargaArchivosSeguimiento">
					<rich:messages style="color:red;"></rich:messages>
					<table width="90%">
						<tbody>
							<tr>

								<td align="right" width="24%" height="30"><h:outputText
										value="Cargar archivo:" /></td>
								<td align="left" width="76%" height="30"
									title="header=[Archivo] body=[Archivo a cargar extensi�n CSV, cualquier otra extensi�n el sistema no lo tomar� en cuenta).] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
									<rich:fileUpload
										fileUploadListener="#{beanSuscripcion.listener}"
										addControlLabel="Examinar" uploadControlLabel="Cargar"
										maxFilesQuantity="5" stopControlLabel="Detener"
										immediateUpload="true" clearAllControlLabel="Borrar todo"
										clearControlLabel="Borrar" styleClass="archivo"
										acceptedTypes="*" listHeight="60px"
										addButtonClass="botonArchivo" listWidth="90%"
										id="componenteSubirArchivo">
										<a4j:support event="onuploadcomplete" reRender="mensaje" />
										<f:facet name="label">
											<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
										</f:facet>
									</rich:fileUpload>
								</td>
							</tr>
						</tbody>
					</table>
					<table>
						<tbody>
							<tr>
								<td align="center"><a4j:commandButton value="Agregar"
										action="#{beanSuscripcion.cargaArchivoSeguimiento}"
										onclick="this.disabled=true" reRender="mensaje"
										oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('panelCargarArchivos')}.show();this.disabled=false;document.getElementById('frmCargaArchivosSeguimiento:componenteSubirArchivo:clean1').click();" />
								</td>

								<td align="center"><h:commandLink id="btnDescarga"
										action="#{beanSuscripcion.getFileZip}">
										<h:graphicImage value="../../images/zip.jpg" />

									</h:commandLink></td>
							</tr>

							<tr>
								<td></td>
							</tr>

						</tbody>
					</table>
					<rich:spacer height="30px"></rich:spacer>

					<a4j:outputPanel id="regTablaArchivos" ajaxRendered="true">
						<rich:dataTable id="tableArchivos"
							value="#{beanSuscripcion.archivosSeguimiento}" var="archivo"
							columns="2" columnsWidth="20px, 10px" width="100%">
							<f:facet name="header">
								<h:outputText value="Archivos" />
							</f:facet>
							<rich:column>
								<f:facet name="header">
									<h:outputText value="Archivo" />
								</f:facet>
								<h:outputText value="#{archivo.coseNomDocto}" />
							</rich:column>
							<rich:column style="align: center;">
								<f:facet name="header">
									<h:outputText value="Descargar Archivo" />
								</f:facet>
								<h:commandLink id="btnDescarga"
									action="#{beanSuscripcion.getFileSuscripcion}">
									<h:graphicImage value="../../images/componentes.png" />
									<f:setPropertyActionListener value="#{archivo}"
										target="#{beanSuscripcion.archivoActual}" />
								</h:commandLink>
							</rich:column>
						</rich:dataTable>
					</a4j:outputPanel>
				</h:form>
			</rich:modalPanel>


			<!-- FIN Pop UP Cargar archivos -->

		</rich:modalPanel>

	</f:view>
</body>


</html>