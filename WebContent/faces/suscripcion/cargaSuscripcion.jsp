<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Carga archivos Suscripci&oacute;n</title>
</head>
<body>
	<f:view>
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center"><%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>
						<td align="center">
							<table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<div align="center">
												<table class="encabezadoTabla">
													<tbody>
														<tr>
															<td width="10%" align="center">Secci�n</td>
															<td width="4%" align="center" bgcolor="#d90909">||||</td>
															<td align="left" width="64%">Carga archivo
																Suscripci&oacute;n</td>
															<td align="right" width="12%">Fecha</td>
															<td align="left" width="10%"><input name="fecha"
																readonly="readonly"
																value="<%=request.getSession().getAttribute("fecha")%>"
																size="7"></td>
														</tr>
													</tbody>
												</table>
											</div> <rich:spacer height="2px"></rich:spacer> <h:form
												id="frmCargaSuscripcion">
												<rich:messages style="color:red;"></rich:messages>

												<div align="center">
													<table class="botonera">
														<tbody>
															<tr>
																<td class="frameTL"></td>
																<td class="frameTC"></td>
																<td class="frameTR"></td>
															</tr>
															<tr>
																<td class="frameCL"></td>
																<td class="frameC" width="600px">
																	<table>
																		<tbody>
																			<tr>

																				<td align="right" width="24%" height="30"><h:outputText
																						value="Cargar archivo:" /></td>
																				<td align="left" width="76%" height="30"
																					title="header=[Archivo] body=[Archivo a cargar extensi�n CSV, cualquier otra extensi�n el sistema no lo tomar� en cuenta).] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																					<rich:fileUpload
																						fileUploadListener="#{beanSuscripcion.listener}"
																						addControlLabel="Examinar"
																						uploadControlLabel="Cargar" maxFilesQuantity="5"
																						stopControlLabel="Detener" immediateUpload="true"
																						clearAllControlLabel="Borrar todo"
																						clearControlLabel="Borrar" styleClass="archivo"
																						acceptedTypes="csv" listHeight="65px"
																						addButtonClass="botonArchivo" listWidth="90%">
																						<a4j:support event="onuploadcomplete"
																							reRender="mensaje" />
																						<f:facet name="label">
																							<h:outputText
																								value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
																						</f:facet>
																					</rich:fileUpload>
																				</td>
																			</tr>
																		</tbody>
																	</table> <rich:spacer height="30px"></rich:spacer> <a4j:region
																		id="filtroCarga">

																		<table>
																			<tbody>
																				<tr>
																					<td align="center" width="60%"><a4j:status
																							for="filtroCarga" stopText=" ">
																							<f:facet name="start">
																								<h:graphicImage value="/images/ajax-loader.gif" />
																							</f:facet>
																						</a4j:status></td>
																				</tr>

																				<tr>
																					<td align="center"><a4j:commandButton
																							action="#{beanSuscripcion.cargaArchivoSuscripcion}"
																							onclick="this.disabled=true"
																							oncomplete="this.disabled=false;document.getElementById('frmDescargaArchivo:btnDescarga').click();"
																							value="Cargar Suscripciones">

																						</a4j:commandButton></td>

																				</tr>
																			</tbody>
																		</table>
																		<br />
																	</a4j:region>
																</td>
																<td class="frameCR"></td>
															<tr>
																<td class="frameBL"></td>
																<td class="frameBC"></td>
																<td class="frameBR"></td>
															</tr>
														</tbody>
													</table>
												</div>
											</h:form> <h:form id="frmDescargaArchivo">
												<h:commandLink id="btnDescarga" value="  "
													action="#{beanSuscripcion.descargarArchivoResultados}">
												</h:commandLink>
											</h:form>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<rich:spacer height="10px" />
		<div align="center"><%@ include file="../footer.jsp"%>
		</div>
	</f:view>
</body>
</html>