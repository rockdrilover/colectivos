<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/colectivos.js"></script>
<link rel="stylesheet" type="text/css"
	href="../../css/estilosReporteador.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<script type="text/javascript" src="../../js/boxoverReporteador.js"></script>

<title>Reporteador Suscripci&oacute;n</title>
</head>

<body
	onload="document.getElementById('frmFiltroBandejaSuscripcion:trickyButton').click();">
	<f:view>
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center"><%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>
						<td align="center">
							<table class="tablaSecundaria">
								<tr>
									<td align="center">
										<table class="encabezadoTabla">
											<tbody>
												<tr>
													<td width="10%" align="center">Procesos</td>
													<td width="4%" align="center" bgcolor="#d90909">||||</td>
													<td align="left" width="64%">Reporteador
														Suscripci&oacute;n</td>
													<td align="right" width="14%">Fecha:</td>
													<td align="left" width="10%"><input name="fecha"
														readonly="readonly"
														value="<%=request.getSession().getAttribute("fecha")%>"
														size="7"></td>
												</tr>
											</tbody>
										</table> <br>
										<div align="center">
											<rich:spacer height="15" />
											<h:form id="frmFiltroBandejaSuscripcion">
												<a4j:region id="filtroCarga">
													<rich:messages style="color:red;"></rich:messages>

													<table class="botonera">

														<tr>
															<td class="frameTL"></td>
															<td class="frameTC"></td>
															<td class="frameTR"></td>
														</tr>
														<tr>
															<td class="frameCL"></td>
															<td class="frameC">
																<!-- INICIO CAMPOS FILTRO -->

																<table border="0">

																	<tr>
																		<td colspan="1">&nbsp;</td>
																	</tr>
																	<tr id="trBandejaSuscripcion">
																		<td align="center" colspan="3"><rich:panel
																				id="paBandejaSuscripcion" styleClass="paneles">
																				<f:facet name="header">
																					<h:outputText value="Area" />
																				</f:facet>
																				<table width="100%" border="0" cellpadding="0"
																					cellspacing="3" align="center">
																					<tr>
																						<td align="right" width="10%"><h:outputText
																								value="Estatus:" /></td>
																						<td width="40%"><h:selectOneMenu
																								id="dictamen"
																								value="#{beanSuscripcionReporteador.filtroSuscripcionDTO.dictamen}">
																								<f:selectItem itemValue="" itemLabel="Todos" />
																								<f:selectItem itemValue="P"
																										itemLabel="Pendiente" />
																									<f:selectItem itemValue="A"
																										itemLabel="Aceptados" />
																									<f:selectItem itemValue="R"
																										itemLabel="Rechazados" />
																									<f:selectItem itemValue="E"
																										itemLabel="En Evaluacion" />
																									<f:selectItem itemValue="S"
																										itemLabel="Suspendido" />
																									<f:selectItem itemValue="T"
																										itemLabel="Completado" />


																							</h:selectOneMenu></td>
																						<td align="right" width="10%"><h:outputText
																								value="Fecha Carga:" /></td>
																						<td width="40%"><rich:calendar
																								id="fechaCarga" popup="true"
																								value="#{beanSuscripcionReporteador.filtroSuscripcionDTO.fechaCarga}"
																								datePattern="dd/MM/yyyy" styleClass="calendario" /></td>
																					</tr>
																					<tr>
																						<td align="right" width="10%"><h:outputText
																								value="Folio CH:" /></td>
																						<td width="40%"><h:inputText id="folioCH"
																								value="#{beanSuscripcionReporteador.filtroSuscripcionDTO.folioCH}" /></td>
																						<td align="right" width="10%"><h:outputText
																								value="Fecha Solicitud:" /></td>
																						<td width="40%"><rich:calendar
																								id="fechaSolicitud" popup="true"
																								value="#{beanSuscripcionReporteador.filtroSuscripcionDTO.fechaSolicitud}"
																								datePattern="dd/MM/yyyy" styleClass="calendario" /></td>
																					</tr>
																					<tr>

																						<td align="right" colspan="4"><a4j:commandButton
																								styleClass="boton" value="Consultar"
																								action="#{beanSuscripcionReporteador.consultaSuscripciones}"
																								onclick="this.disabled=true"
																								oncomplete="this.disabled=false"
																								reRender="mensaje,regTabla,divRespuesta" />
																							<div style="display: none;">
																								<a4j:commandButton id="trickyButton"
																									action="#{beanSuscripcionReporteador.inicializaPantalla}"
																									reRender="mensaje,divRespuesta, regTabla, regSeguimiento" />
																							</div></td>
																					</tr>
																				</table>

																			</rich:panel></td>

																	</tr>

																	<tr class="espacio_15">
																		<td>&nbsp;</td>
																	</tr>
																	<tr>
																		<td align="center"><a4j:status for="filtroCarga"
																				stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status></td>
																	</tr>


																</table> <!-- FIN CAMPOS FILTRO -->


															</td>
														</tr>

														<tr>
															<td colspan="2" align="center">
																<table width="100%">
																	<tr>
																		<td width="90%" align="center"><a4j:outputPanel
																				id="nombreColumnas" ajaxRendered="true">
																				<rich:panelBar width="100%"
																					contentClass="texto_normal">
																					<rich:panelBarItem label="   Ocultar"
																						headerStyle="text-align: left;"></rich:panelBarItem>
																					<rich:panelBarItem
																						label="   Seleccion de campos para generar el reporte."
																						headerStyle="text-align: left;">
																						<h:panelGrid columns="3" width="100%">
																							<h:selectManyCheckbox
																								value="#{beanSuscripcionReporteador.columnasSeleccionadas}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanSuscripcionReporteador.listaComboEstatus}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanSuscripcionReporteador.columnasSeleccionadasC2}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanSuscripcionReporteador.listaComboEstatusC2}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanSuscripcionReporteador.columnasSeleccionadasC3}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanSuscripcionReporteador.listaComboEstatusC3}" />
																							</h:selectManyCheckbox>
																						</h:panelGrid>
																					</rich:panelBarItem>

																					<c:if
																						test="${fn:length(beanSuscripcionReporteador.suscripciones) > 0}">
																						<rich:panelBarItem label=" Resultados"
																							headerStyle="text-align: left;">
																							<table width="100%">
																								<tr>
																									<td title="Total registros" width="80%"><h:graphicImage
																											value="../../images/imgEmitir.png" />&nbsp;
																										<strong> <h:outputText id="lblEmitir"
																												value="Total: " />&nbsp;
																											${fn:length(beanSuscripcionReporteador.suscripciones)}
																									</strong></td>
																									<td><h:commandButton
																											action="#{beanSuscripcionReporteador.generarReporteCSV}"
																											value="Generar Reporte"
																											title="header=[Descargar] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																									</td>
																								</tr>
																								<rich:spacer height="10px"></rich:spacer>
																								<tr>
																									<td colspan="2">
																										<div>
																											<table cellpadding="0" cellspacing="0"
																												class="tablaConsultaReporteador"
																												align="center">
																												<tr class="encabezadoTablaReporteador">
																													<c:forEach var="nombreCol"
																														items="#{beanSuscripcionReporteador.listaColumnasSeleccionadas}">
																														<td align="center"><strong>&nbsp;&nbsp;${nombreCol.headerColumn}&nbsp;&nbsp;</strong></td>
																													</c:forEach>
																												</tr>

																												<c:set var="campos"
																													value="${fn:length(beanSuscripcionReporteador.listaColumnasSeleccionadas)}" />
																												<c:forEach var="registro"
																													items="#{beanSuscripcionReporteador.suscripciones}"
																													end="30">
																													<tr class="contenidoTablaReporteador">

																														<c:forEach var="nombreCol"
																															items="#{beanSuscripcionReporteador.listaColumnasSeleccionadas}">
																															<td align="center"
																																style="padding-left: 20px; padding-right: 20px">${registro[nombreCol.nombreColumna]}</td>
																														</c:forEach>

																													</tr>
																												</c:forEach>
																											</table>
																										</div>
																									</td>
																								</tr>
																							</table>
																						</rich:panelBarItem>
																					</c:if>
																				</rich:panelBar>
																			</a4j:outputPanel></td>
																	</tr>
																</table>
															</td>
														</tr>

														<tr>
															<td class="frameBL"></td>
															<td class="frameBC"></td>
															<td class="frameBR"></td>
														</tr>
													</table>
												</a4j:region>
											</h:form>
											<br />
										</div>


									</td>
								</tr>
							</table>

						</td>
					</tr>
				</tbody>
			</table>

		</div>
		<div align="center">
			<br />
			<%@ include file="../footer.jsp"%></div>
	</f:view>

</body>