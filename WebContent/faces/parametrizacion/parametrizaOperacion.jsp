<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>

<f:view>
	<!-- Desde aqui comienzo -->
	<fmt:setBundle 	basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" />

	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script src="../../static/js/colectivos.js"></script>
<title>Lista de parametros de Operacion</title>
</head>

<body>
	<h:form id="frmParametrizaOper">
		<div align="center"><%@include file="../header.jsp"%></div>
		<div align="center">
			<table class="tablaPrincipal">
				<tr>
					<td align="center">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div align="center">
										<table class="encabezadoTabla">
											<tr>
												<td width="10%" align="center">Secci�n</td>
												<td width="4%" align="center" bgcolor="#d90909">I</td>
												<td align="left" width="64%">Parametriza Operacion</td>
												<td align="right" width="12%">Fecha</td>
												<td align="left" width="10%"><input name="fecha"
													readonly="readonly"
													value="<%=request.getSession().getAttribute("fecha")%>"
													size="7"></td>
											</tr>
										</table>
									</div> <rich:spacer height="10px"></rich:spacer>

									<div>
										<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
											<tr class="espacio_15"> <td>&nbsp;</td> </tr>
											<tr><td><h:outputText id="mensaje"  value="#{beanParamOperacion.strRespuesta}" styleClass="error" /></td>
											</tr><tr class="espacio_15"><td>&nbsp;</td>
											</tr>
										</table>

										<!-- Inicio ACCIONES -->
										<a4j:region id="consultaDatos">
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_consulta_m"> 
																   <h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																</span> 
																<span class="titulos_secciones">Acciones</span>
															</legend>

															<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">
																	<tr class="texto_normal">
																		<td width="70%">
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																	
																	
																<%-- base --%>	
																<tr>
																		<td align="right" width="24%" height="30"><h:outputText value=" Ramo :" /></td>
																		<td align="left" width="76%" height="30">
																			<h:selectOneMenu id="inRamo" value="#{beanParamOperacion.copaNvalor2}" >
																				<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
																				<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
																				<a4j:support event="onchange"  action="#{beanParamOperacion.cargaPolizas}" reRender="inPoliza" />	
																			</h:selectOneMenu>
																			<rich:message for="inRamo" styleClass="errorMessage" />																						
																	</td>
																</tr>
				
															<tr>
																<td align="right" width="24%" height="30"><h:outputText value="Poliza :"/></td>
																<td align="left" width="76%" height="30">
																	<h:selectOneMenu id="inPoliza"  value="#{beanParamOperacion.copaNvalor3}">
																		<f:selectItem itemValue="-2" itemLabel="Seleccione una p�liza"/>
																		<f:selectItems value="#{beanParamOperacion.listaComboPolizas}"/>
																	<a4j:support event="onchange"  action="#{beanParamOperacion.cargaIdVenta}" reRender="idVenta"/>
																	</h:selectOneMenu>
																	<rich:message for="inPoliza" styleClass="errorMessage" />
																</td>
															</tr>
														<tr>
															<td align="right" width="24%" height="30"><h:outputText value="Canal de Venta:"/></td>
															<td align="left" width="76%" height="30">
																<h:selectOneMenu id="idVenta"  value="#{beanParamOperacion.valorIdVenta}" binding="#{beanListaParametros.inIdVenta}" disabled="#{beanParamOperacion.habilitaComboIdVenta}" >
																	<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																	<f:selectItems value="#{beanParamOperacion.listaComboIdVenta}"/>
																	<a4j:support event="onchange"  action="#{beanParamOperacion.consultaProducto}" reRender="idProducto"/>
																</h:selectOneMenu>
																<rich:message for="idVenta" styleClass="errorMessage" />
															</td>
														</tr>
														<tr>
															<td align="right" width="24%" height="30"><h:outputText value="Producto:"/></td>
															<td align="left" width="76%" height="30">
																<h:selectOneMenu id="idProducto"  value="#{beanParamOperacion.valorProducto}">
																	<f:selectItem itemValue="-1" itemLabel="Seleccione un producto"/>
																	<f:selectItems value="#{beanParamOperacion.listaComboProducto}"/>
																	<a4j:support event="onchange"  action="#{beanParamOperacion.consultaPlanes}" reRender="idPlan"/>
																</h:selectOneMenu>
																<rich:message for="idProducto" styleClass="errorMessage" />
															</td>
														</tr>
														<tr>
															<td align="right" width="24%" height="30"><h:outputText value="Plan:"/></td>
															<td align="left" width="76%" height="30">
																<h:selectOneMenu id="idPlan"  value="#{beanParamOperacion.valorPlan}">
																	<f:selectItem itemValue="-1" itemLabel="Seleccione un plan"/>
																	<f:selectItems value="#{beanParamOperacion.listaComboPlanes}"/>
																</h:selectOneMenu>
																<rich:message for="idPlan" styleClass="errorMessage" />
															</td>
														</tr>
																<%--fn base --%>
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr class="texto_normal">
																	<td align="center"><a4j:commandButton
																			styleClass="boton" id="btnConsultar" action="#{beanParamOperacion.disparaConceptos}" onclick="this.disabled=true"
																			oncomplete="this.disabled=false" reRender="regTablaRehabilita,mensaje,regTablaEndoso,regTablaRestitucion,regTablaSaldo,regTablaDevolucion" 
																			value="Consultar" /> 
																			<rich:toolTip for="btnConsultar" value="Consultar Parametrizacion " />
																	</td>
																</tr>
																<tr class="espacio_15">
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td align="center" colspan="2"><a4j:status
																			for="consultaDatos" stopText=" ">
																			<f:facet name="start">
																				<h:graphicImage value="/images/ajax-loader.gif" />
																			</f:facet>
																		</a4j:status></td>
																</tr>
															</table>
														</fieldset>
													</td>
												</tr>
												<tr class="espacio_15">
													<td>&nbsp;</td>
												</tr>
											</table>
										</a4j:region>
											
										<!-- Inicio RESULTADOS Endoso -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_resultados_m" style="display: none;">
																	<h:graphicImage value="../../images/mostrar_seccion.png"  onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta" />&nbsp;
																</span> 
																<span id="img_sec_resultados_o">
																    <h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra" />&nbsp;
																</span>
																<span class="titulos_secciones">Endoso</span>
															</legend>

															<a4j:outputPanel id="regTablaEndoso" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_resultados"	style="display: none;">
																		<td>
																		<%-- cambiar  --%>
																		<c:choose>
																			<c:when test="${fn:length(beanParamOperacion.listaParametrosEndoso) == 0}">
																				<table width="90%" align="center" border="0">
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																					
																					<tr>
																				    <td width="4">&nbsp;</td>
																					<td width="13%" align="right">
																					<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevoEndoso" oncomplete="#{rich:component('newPanel')}.show()" />
				                    									   			<rich:toolTip for="btnNuevoEndoso" value="Nuevo Parametro Endoso" />
			                    													</td>
			                    													</tr>
																					
																				</table>
																			</c:when>
																			<c:otherwise>															
																				<rich:contextMenu attached="false" id="menu1" submitMode="ajax" 
																						oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																		            <rich:menuItem value="Edit Record" ajaxSingle="true"
																		                	oncomplete="#{rich:component('editPanel')}.show()"
																		                	actionListener="#{beanParamOperacion.buscarFilaActualE}">
																		                <a4j:actionparam name="clave" value="{clave}" />
																		                <a4j:actionparam name="row" value="{filaActual}" />
																		            </rich:menuItem>
																		        </rich:contextMenu>
													        					<%-- termina el cambio  --%>
													        					
																				<rich:datascroller for="dsEndoso" maxPages="10" />
																				<rich:dataTable value="#{beanParamOperacion.mapValuesEndoso}" id="dsEndoso" var="registroE" rows="10" 
																				               rowKeyVar="row" ajaxKeys="#{beanParamOperacion.keysEndoso}" columns="10" width="90%" align="center">
																					<f:facet name="header"><h:outputText value="Parametrizacion " /></f:facet>

																					<rich:column title="Ramo" width="5%" styleClass="texto_centro" sortBy="#{registroE.copaNvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Ramo " title="Ramo" />
																						</f:facet>
																						<h:outputText value="#{registroE.copaNvalor2}" />
																					</rich:column>

																					<rich:column title="Poliza" width="5%"  styleClass="texto_centro"	sortBy="#{registroE.copaNvalor3}">
																						<f:facet name="header"> <h:outputText value=" Poliza " title="Poliza" /> </f:facet>
																						<h:outputText value="#{registroE.copaNvalor3}" />
																					</rich:column>

																					<rich:column title="Concepto" width="30%" styleClass="texto_centro" sortBy="#{registroE.copaIdParametro}">
																						<f:facet name="header"><h:outputText value=" Concepto " title="Concepto" /></f:facet>
																						<h:outputText value="#{registroE.copaIdParametro}" />
																					</rich:column>

																					<rich:column title="Endoso en Emsion" width="5%" styleClass="texto_centro" sortBy="#{registroE.copaVvalor3}">
																						<f:facet name="header"><h:outputText value=" Endoso en Emsion " title="Afecta Calculo de Prima" /> </f:facet>
																						<h:outputText value="#{registroE.copaVvalor3}" />
																					</rich:column>

																					<rich:column title="Endoso en Renovacion" width="5%" styleClass="texto_centro" sortBy="#{registroE.copaVvalor4}">
																						<f:facet name="header"><h:outputText value=" Endoso en Renovacion " title="Afecta Calculo de Prima" /></f:facet>
																						<h:outputText value="#{registroE.copaVvalor4}" />
																					</rich:column>
																					
																					<rich:column title="Endoso en Cancelacion" width="5%" styleClass="texto_centro" sortBy="#{registroE.copaVvalor5}">
																						<f:facet name="header"><h:outputText value=" Endoso en Cancelacion " title="Afecta Calculo de Prima" /></f:facet>
																						<h:outputText value="#{registroE.copaVvalor5}" />
																					</rich:column>
																					
																					<rich:column title="Tipo de calculo " width="5%" styleClass="texto_centro" sortBy="#{registroE.copaNvalor5}">
																						<f:facet name="header"><h:outputText value=" Tipo de calculo " title="Afecta Calculo de Prima" /></f:facet>
																						<h:outputText value="#{registroE.copaNvalor5}" />
																					</rich:column>
																					
																					<rich:column title="Modificar" width="5%" 	styleClass="texto_centro">
																						<f:facet name="header">
																							<h:outputText value=" Modificar " />
																						</f:facet>
																						<a4j:commandLink ajaxSingle="true" id="editlink1" oncomplete="#{rich:component('editPanelEndoso')}.show()">
																							<h:graphicImage 	value="../../images/edit_page.png"	style="border:0" />
																							<f:setPropertyActionListener value="#{registroE}"  target="#{beanParamOperacion.objActualE}" />
																							<f:setPropertyActionListener value="#{row}"  target="#{beanParamOperacion.filaActualE}" />
																						</a4j:commandLink>
																						<rich:toolTip for="editlink" value="Editar" />
																					</rich:column>
																				</rich:dataTable>
																				</c:otherwise>	
																					</c:choose>
																			</td>
																	</tr>
																																     	
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											<%-- Termina Resulktados  --%>
											

			
											
										<!-- Inicio Rehabltacion -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_resultados1_m" style="display: none;">
																	<h:graphicImage value="../../images/mostrar_seccion.png"  onclick="muestraOcultaSeccion('resultados1', 0);" title="Oculta" />&nbsp;
																</span> 
																<span id="img_sec_resultados1_o">
																    <h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados1', 1);" title="Muestra" />&nbsp;
																</span>
																<span class="titulos_secciones">Rehabilitacion</span>
															</legend>

															<a4j:outputPanel id="regTablaRehabilita" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_resultados1"	style="display: none;">
																		<td>
																		<%-- cambiar  --%>
																		<c:choose>
																			<c:when test="${fn:length(beanParamOperacion.listaParametrosRehabilitacion) == 0}">
																				<table width="90%" align="center" border="0">
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																					<tr>
																						<td width="4">&nbsp;</td>
																						<td width="13%" align="right">
																							<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevoRehabilita" oncomplete="#{rich:component('newPanelRehabilita')}.show()" />
								                    										<rich:toolTip for="btnNuevoRehabilta" value="Nuevo Parametro Endoso" />									
																						</td>
																				     </tr>
																				</table>
																			</c:when>
																			<c:otherwise>															
																				<rich:contextMenu attached="false" id="menu2" submitMode="ajax" 
																						oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																		            <rich:menuItem value="Edit Record" ajaxSingle="true"
																		                	oncomplete="#{rich:component('editPanel')}.show()"
																		                	actionListener="#{beanParamOperacion.buscarFilaActual}">
																		                <a4j:actionparam name="clave" value="{clave}" />
																		                <a4j:actionparam name="row" value="{filaActual}" />
																		            </rich:menuItem>
																		        </rich:contextMenu>
													        					<%-- termina el cambio  --%>
													        					
																				<rich:datascroller for="dsRehabilita" maxPages="10" />
																				<rich:dataTable value="#{beanParamOperacion.mapValues}" id="dsRehabilita" var="registro" rows="10" 
																				               rowKeyVar="row" ajaxKeys="#{beanParamOperacion.keys}" columns="10" width="90%" align="center">
																					<f:facet name="header"><h:outputText value="Parametrizacion " /></f:facet>

																					<rich:column title="Ramo" width="5%" styleClass="texto_centro" sortBy="#{registro.copaNvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Ramo " title="Ramo" />
																						</f:facet>
																						<h:outputText value="#{registro.copaNvalor2}" />
																					</rich:column>

																					<rich:column title="Poliza" width="5%"  styleClass="texto_centro"	sortBy="#{registro.copaNvalor3}">
																						<f:facet name="header">
																							<h:outputText value=" Poliza " title="Poliza" />
																						</f:facet>
																						<h:outputText value="#{registro.copaNvalor3}" />
																					</rich:column>

																					<rich:column title="Concepto" width="30%" styleClass="texto_centro" sortBy="#{registro.copaIdParametro}">
																						<f:facet name="header">
																							<h:outputText value=" Concepto " title="Concepto" />
																						</f:facet>
																						<h:outputText value="#{registro.copaIdParametro}" />
																					</rich:column>

																					<rich:column title="copaNvalor4" width="5%" styleClass="texto_centro" sortBy="#{registro.copaNvalor4}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor4 " title="copaNvalor4" />
																						</f:facet>
																						<h:outputText value="#{registro.copaNvalor4}" />
																					</rich:column>

																					<rich:column title="copaNvalor5" width="5%" styleClass="texto_centro" sortBy="#{registro.copaNvalor5}">
																						<f:facet name="header">
																							<h:outputText value="copaNvalor5 "
																								title="copaNvalor5" />
																						</f:facet>
																						<h:outputText value="#{registro.copaNvalor5}" />
																					</rich:column>
																					
																					<rich:column title="copaNvalor6" width="5%" styleClass="texto_centro" sortBy="#{registro.copaNvalor6}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor6 "
																								title="copaNvalor6" />
																						</f:facet>
																						<h:outputText value="#{registro.copaNvalor6}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor1" width="5%" styleClass="texto_centro" sortBy="#{registro.copaVvalor1}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor1 "
																								title="copaVvalor1" />
																						</f:facet>
																						<h:outputText value="#{registro.copaVvalor3}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor4" width="5%" styleClass="texto_centro" sortBy="#{registro.copaVvalor4}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor4 "
																								title="copaVvalor4" />
																						</f:facet>
																						<h:outputText value="#{registro.copaVvalor4}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor5" width="5%" styleClass="texto_centro" sortBy="#{registro.copaVvalor5}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor5 "
																								title="copaVvalor5" />
																						</f:facet>
																						<h:outputText value="#{registro.copaVvalor5}" />
																					</rich:column>
																					
																						<rich:column title="copaVvalor6" width="5%" styleClass="texto_centro" sortBy="#{registro.copaVvalor6}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor6 "
																								title="copaVvalor6" />
																						</f:facet>
																						<h:outputText value="#{registro.copaVvalor6}" />
																					</rich:column>
																					
																					<rich:column title="Modificar" width="5%"
																						styleClass="texto_centro">
																						<f:facet name="header">
																							<h:outputText value=" Modificar " />
																						</f:facet>
																						<a4j:commandLink ajaxSingle="true" id="editlinkR" oncomplete="#{rich:component('editPanel')}.show()">
																							<h:graphicImage 	value="../../images/edit_page.png"	style="border:0" />
																							<f:setPropertyActionListener value="#{registro}"  target="#{beanParamOperacion.objActual}" />
																							<f:setPropertyActionListener value="#{row}"  target="#{beanParamOperacion.filaActual}" />
																						</a4j:commandLink>
																						<rich:toolTip for="editlinkR" value="Editar" />
																					</rich:column>
																				</rich:dataTable>
																				</c:otherwise>	
																					</c:choose>
																			</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											<%-- termina rehabilita --%>
									
										    <!-- Inicio Restitucion -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_resultados2_m" style="display: none;">
																	<h:graphicImage value="../../images/mostrar_seccion.png"  onclick="muestraOcultaSeccion('resultados2', 0);" title="Oculta" />&nbsp;
																</span> 
																<span id="img_sec_resultados2_o">
																    <h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados2', 1);" title="Muestra" />&nbsp;
																</span>
																<span class="titulos_secciones">Restitucion</span>
															</legend>

															<a4j:outputPanel id="regTablaRestitucion" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_resultados2"	style="display: none;">
																		<td>
																		<%-- cambiar  --%>
																		<c:choose>
																			<c:when test="${fn:length(beanParamOperacion.listaParametrosRestitucion) == 0}">
																				<table width="90%" align="center" border="0">
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																				
																					<tr>
																						<td width="4">&nbsp;</td>
																						<td width="13%" align="right">
																						<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevoResti" oncomplete="#{rich:component('newPanelRestitucion')}.show()" />
			                    														<rich:toolTip for="btnNuevoResti" value="Nuevo Parametro Restitucion" />
			                    													</td>
			                    													</tr>									
																				
																				</table>
																			</c:when>
																			<c:otherwise>															
																				<rich:contextMenu attached="false" id="menu3" submitMode="ajax" 
																						oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																		            <rich:menuItem value="Edit Record" ajaxSingle="true"
																		                	oncomplete="#{rich:component('editPanel')}.show()"
																		                	actionListener="#{beanParamOperacion.buscarFilaActualRestitucion}">
																		                <a4j:actionparam name="clave" value="{clave}" />
																		                <a4j:actionparam name="row" value="{filaActual}" />
																		            </rich:menuItem>
																		        </rich:contextMenu>
													        					<%-- termina el cambio  --%>
													        					
																				<rich:datascroller for="dsRestitucion" maxPages="10" />
																				<rich:dataTable value="#{beanParamOperacion.mapValuesRestitucion}" id="dsRestitucion" var="registroR" rows="10" 
																				               rowKeyVar="row" ajaxKeys="#{beanParamOperacion.keysResti}" columns="10" width="90%" align="center">
																					<f:facet name="header"><h:outputText value="Parametrizacion " /></f:facet>

																					<rich:column title="Ramo" width="5%" styleClass="texto_centro" sortBy="#{registroR.copaNvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Ramo " title="Ramo" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaNvalor2}" />
																					</rich:column>

																					<rich:column title="Poliza" width="5%"  styleClass="texto_centro"	sortBy="#{registroR.copaNvalor3}">
																						<f:facet name="header">
																							<h:outputText value=" Poliza " title="Poliza" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaNvalor3}" />
																					</rich:column>

																					<rich:column title="Concepto" width="30%" styleClass="texto_centro" sortBy="#{registroR.copaIdParametro}">
																						<f:facet name="header">
																							<h:outputText value=" Concepto " title="Concepto" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaIdParametro}" />
																					</rich:column>

																					<rich:column title="copaNvalor4" width="5%" styleClass="texto_centro" sortBy="#{registroR.copaNvalor4}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor4 "
																								title="copaNvalor4" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaNvalor4}" />
																					</rich:column>

																					<rich:column title="copaNvalor5" width="5%" styleClass="texto_centro" sortBy="#{registroR.copaNvalor5}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor5 " title="copaNvalor5" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaNvalor5}" />
																					</rich:column>
																					
																					<rich:column title="copaNvalor6" width="5%" styleClass="texto_centro" sortBy="#{registroR.copaNvalor6}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor6 " title="copaNvalor6" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaNvalor6}" />
																					</rich:column>
																					
																					<rich:column title="copaNvalor7" width="5%" styleClass="texto_centro" sortBy="#{registroR.copaNvalor7}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor7 " title="copaNvalor7" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaNvalor7}" />
																					</rich:column>
																					
																					<rich:column title="copaNvalor8" width="5%" styleClass="texto_centro" sortBy="#{registroR.copaNvalor8}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor8 " title="copaNvalor8" />
																						</f:facet>
																						<h:outputText value="#{registroR.copaNvalor8}" />
																					</rich:column>
																					
																					<rich:column title="Modificar" width="5%" styleClass="texto_centro">
																						<f:facet name="header">
																							<h:outputText value=" Modificar " />
																						</f:facet>
																						<a4j:commandLink ajaxSingle="true" id="editlinkResti" oncomplete="#{rich:component('editPanelRestitucion')}.show()">
																							<h:graphicImage 	value="../../images/edit_page.png"	style="border:0" />
																							<f:setPropertyActionListener value="#{registroR}"  target="#{beanParamOperacion.objActualResti}" />
																							<f:setPropertyActionListener value="#{row}"  target="#{beanParamOperacion.filaActualResti}" />
																						</a4j:commandLink>
																						<rich:toolTip for="editlink" value="Editar" />
																					</rich:column>
																				</rich:dataTable>
																				</c:otherwise>	
																					</c:choose>
																			</td>
																	</tr>
																	
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											<%-- termina Restitucion --%>
									
									
											  <!-- Inicio Saldo -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_resultados3_m" style="display: none;">
																	<h:graphicImage value="../../images/mostrar_seccion.png"  onclick="muestraOcultaSeccion('resultados3', 0);" title="Oculta" />&nbsp;
																</span> 
																<span id="img_sec_resultados3_o">
																    <h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados3', 1);" title="Muestra" />&nbsp;
																</span>
																<span class="titulos_secciones">Saldo Insoluto </span>
															</legend>

															<a4j:outputPanel id="regTablaSaldo" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_resultados3"	style="display: none;">
																		<td>
																		<%-- cambiar  --%>
																		<c:choose>
																			<c:when test="${fn:length(beanParamOperacion.listaParametrosSaldo) == 0}">
																				<table width="90%" align="center" border="0">
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																					
																				    <tr>
																				    <td width="4">&nbsp;</td>
																					<td width="13%" align="right">
																					<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevoSaldo" oncomplete="#{rich:component('newPanelSaldo')}.show()" />
			                    													<rich:toolTip for="btnNuevoSaldo" value="Nuevo Parametro Saldo" />
			                    													</td>
			                    													</tr>
			                    													
																				</table>
																			</c:when>
																			<c:otherwise>															
																				<rich:contextMenu attached="false" id="menu4" submitMode="ajax" 
																						oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																		            <rich:menuItem value="Edit Record" ajaxSingle="true"
																		                	oncomplete="#{rich:component('editPanel')}.show()"
																		                	actionListener="#{beanParamOperacion.buscarFilaActualSaldo}">
																		                <a4j:actionparam name="clave" value="{clave}" />
																		                <a4j:actionparam name="row" value="{filaActual}" />
																		            </rich:menuItem>
																		        </rich:contextMenu>
													        					<%-- termina el cambio  --%>
													        					
																				<rich:datascroller for="dsSaldo" maxPages="10" />
																				<rich:dataTable value="#{beanParamOperacion.mapValuesSaldo}" id="dsSaldo" var="registroS" rows="10" 
																				               rowKeyVar="row" ajaxKeys="#{beanParamOperacion.keysSaldo}" columns="10" width="90%" align="center">
																					<f:facet name="header"><h:outputText value="Parametrizacion " /></f:facet>

																					<rich:column title="Ramo" width="5%" styleClass="texto_centro" sortBy="#{registroS.copaNvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Ramo " title="Ramo" />
																						</f:facet>
																						<h:outputText value="#{registroS.copaNvalor2}" />
																					</rich:column>

																					<rich:column title="Poliza" width="5%"  styleClass="texto_centro"	sortBy="#{registroS.copaNvalor3}">
																						<f:facet name="header">
																							<h:outputText value=" Poliza " title="Poliza" />
																						</f:facet>
																						<h:outputText value="#{registroS.copaNvalor3}" />
																					</rich:column>

																					<rich:column title="Concepto" width="30%" styleClass="texto_centro" sortBy="#{registroS.copaIdParametro}">
																						<f:facet name="header">
																							<h:outputText value=" Concepto " title="Concepto" />
																						</f:facet>
																						<h:outputText value="#{registroS.copaIdParametro}" />
																					</rich:column>

																					<rich:column title="copaVvalor1" width="5%" styleClass="texto_centro" sortBy="#{registroS.copaVvalor1}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor1"
																								title="copaVvalor1" />
																						</f:facet>
																						<h:outputText value="#{registroS.copaVvalor1}" />
																					</rich:column>

																					<rich:column title="copaVvalor2" width="5%" styleClass="texto_centro" sortBy="#{registroS.copaVvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor2 "
																								title="copaVvalor2" />
																						</f:facet>
																						<h:outputText value="#{registroS.copaVvalor2}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor4" width="5%" styleClass="texto_centro" sortBy="#{registroS.copaVvalor4}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor4 "
																								title="copaVvalor4" />
																						</f:facet>
																						<h:outputText value="#{registroS.copaVvalor4}" />
																					</rich:column>
																					
																						<rich:column title="copaVvalor6" width="5%" styleClass="texto_centro" sortBy="#{registroS.copaVvalor6}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor6 "
																								title="copaVvalor6" />
																						</f:facet>
																						<h:outputText value="#{registroS.copaVvalor6}" />
																					</rich:column>
																					
																					
																					<rich:column title="Modificar" width="5%"
																						styleClass="texto_centro">
																						<f:facet name="header">
																							<h:outputText value=" Modificar " />
																						</f:facet>
																						<a4j:commandLink ajaxSingle="true" id="editlinkSaldo" oncomplete="#{rich:component('editPanelSaldo')}.show()">
																							<h:graphicImage 	value="../../images/edit_page.png"	style="border:0" />
																							<f:setPropertyActionListener value="#{registroS}"  target="#{beanParamOperacion.objActualSaldo}" />
																							<f:setPropertyActionListener value="#{row}"  target="#{beanParamOperacion.filaActualSaldo}" />
																						</a4j:commandLink>
																						<rich:toolTip for="editlinkSaldo" value="Editar" />
																					</rich:column>
																				</rich:dataTable>
																				</c:otherwise>	
																					</c:choose>
																			</td>
																	</tr>
																	
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											<%-- termina Saldo --%>
									
									
											
											  <!-- Inicio Devolucion -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_resultados4_m" style="display: none;">
																	<h:graphicImage value="../../images/mostrar_seccion.png"  onclick="muestraOcultaSeccion('resultados4', 0);" title="Oculta" />&nbsp;
																</span> 
																<span id="img_sec_resultados4_o">
																    <h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados4', 1);" title="Muestra" />&nbsp;
																</span>
																<span class="titulos_secciones">Diferencia en primas emitidas y Devolucion </span>
															</legend>

															<a4j:outputPanel id="regTablaDevolucion" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_resultados4"	style="display: none;">
																		<td>
																		<%-- cambiar  --%>
																		<c:choose>
																			<c:when test="${fn:length(beanParamOperacion.listaParametrosDevolucion) == 0}">
																				<table width="90%" align="center" border="0">
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																					<tr>
																						<td width="4">&nbsp;</td>
																						<td width="13%" align="right">
																						<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevoDev" oncomplete="#{rich:component('newPanelDevolucion')}.show()" />
				                    													<rich:toolTip for="btnNuevoDev" value="Nuevo Parametro Devolucion" />
				                    													</td>
			                    													</tr>
			                    												                    													
																				</table>
																			</c:when>
																			<c:otherwise>															
																				<rich:contextMenu attached="false" id="menu5" submitMode="ajax" 
																						oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																		            <rich:menuItem value="Edit Record" ajaxSingle="true"
																		                	oncomplete="#{rich:component('editPanelDevolucion')}.show()"
																		                	actionListener="#{beanParamOperacion.buscarFilaActualDevolucion}">
																		                <a4j:actionparam name="clave" value="{clave}" />
																		                <a4j:actionparam name="row" value="{filaActual}" />
																		            </rich:menuItem>
																		        </rich:contextMenu>
													        					<%-- termina el cambio  --%>
													        					
																				<rich:datascroller for="dsDevolucion" maxPages="10" />
																				<rich:dataTable value="#{beanParamOperacion.mapValuesDevolucion}" id="dsDevolucion" var="registroD" rows="10" 
																				               rowKeyVar="row" ajaxKeys="#{beanParamOperacion.keysDev}" columns="10" width="90%" align="center">
																					<f:facet name="header"><h:outputText value="Parametrizacion " /></f:facet>

																					<rich:column title="Ramo" width="5%" styleClass="texto_centro" sortBy="#{registroD.copaNvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Ramo " title="Ramo" />
																						</f:facet>
																						<h:outputText value="#{registroD.copaNvalor2}" />
																					</rich:column>

																					<rich:column title="Poliza" width="5%"  styleClass="texto_centro"	sortBy="#{registroD.copaNvalor3}">
																						<f:facet name="header">
																							<h:outputText value=" Poliza " title="Poliza" />
																						</f:facet>
																						<h:outputText value="#{registroD.copaNvalor3}" />
																					</rich:column>

																					<rich:column title="Concepto" width="30%" styleClass="texto_centro" sortBy="#{registroD.copaIdParametro}">
																						<f:facet name="header">
																							<h:outputText value=" Concepto " title="Concepto" />
																						</f:facet>
																						<h:outputText value="#{registroD.copaIdParametro}" />
																					</rich:column>

																					<rich:column title="copaNvalor4" width="5%" styleClass="texto_centro" sortBy="#{registroD.copaNvalor4}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor4 "
																								title="copaNvalor4" />
																						</f:facet>
																						<h:outputText value="#{registroD.copaNvalor4}" />
																					</rich:column>

																					<rich:column title="copaNvalor5" width="5%" styleClass="texto_centro" sortBy="#{registroD.copaNvalor5}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor5 "
																								title="copaNvalor5" />
																						</f:facet>
																						<h:outputText value="#{registroD.copaNvalor5}" />
																					</rich:column>
																					
																					<rich:column title="copaNvalor6" width="5%" styleClass="texto_centro" sortBy="#{registroD.copaNvalor6}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor6 "
																								title="copaNvalor6" />
																						</f:facet>
																						<h:outputText value="#{registroD.copaNvalor6}" />
																					</rich:column>
																					
																					<rich:column title="Modificar" width="5%"
																						styleClass="texto_centro">
																						<f:facet name="header">
																							<h:outputText value=" Modificar " />
																						</f:facet>
																						<a4j:commandLink ajaxSingle="true" id="editlinkDev" oncomplete="#{rich:component('editPanelDevolucion')}.show()">
																							<h:graphicImage 	value="../../images/edit_page.png"	style="border:0" />
																							<f:setPropertyActionListener value="#{registroD}"  target="#{beanParamOperacion.objActualDev}" />
																							<f:setPropertyActionListener value="#{row}"  target="#{beanParamOperacion.filaActualDev}" />
																						</a4j:commandLink>
																						<rich:toolTip for="editlinkDev" value="Editar" />
																					</rich:column>
																				</rich:dataTable>
																				</c:otherwise>	
																					</c:choose>
																			</td>
																	</tr>
																												
																	
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											<%-- termina Devolucion --%>
									
											 <!-- Inicio Facturacion -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_resultados5_m" style="display: none;">
																	<h:graphicImage value="../../images/mostrar_seccion.png"  onclick="muestraOcultaSeccion('resultados5', 0);" title="Oculta" />&nbsp;
																</span> 
																<span id="img_sec_resultados5_o">
																    <h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados5', 1);" title="Muestra" />&nbsp;
																</span>
																<span class="titulos_secciones">Facturacion </span>
															</legend>

															<a4j:outputPanel id="regTablaDFacturaion" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_resultados5"	style="display: none;">
																		<td>
																		<%-- cambiar  --%>
																		<c:choose>
																			<c:when test="${fn:length(beanParamOperacion.listaParametrosFacturacion) == 0}">
																				<table width="90%" align="center" border="0">
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																					<tr>
																						<td width="4">&nbsp;</td>
																						<td width="13%" align="right">
																							<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevoFac" oncomplete="#{rich:component('newPanelFacturacion')}.show()" />
								                    										<rich:toolTip for="btnNuevoFac" value="Nuevo Parametro Facturacion" />									
																						</td>
																				     </tr>
																				</table>
																			</c:when>
																			<c:otherwise>															
																				<rich:contextMenu attached="false" id="menu7" submitMode="ajax" 
																						oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																		            <rich:menuItem value="Edit Record" ajaxSingle="true"
																		                	oncomplete="#{rich:component('editPanel')}.show()"
																		                	actionListener="#{beanParamOperacion.buscarFilaActualFacturacion}">
																		                <a4j:actionparam name="clave" value="{clave}" />
																		                <a4j:actionparam name="row" value="{filaActual}" />
																		            </rich:menuItem>
																		        </rich:contextMenu>
													        					<%-- termina el cambio  --%>
													        					
																				<rich:datascroller for="dsFacturacion" maxPages="10" />
																				<rich:dataTable value="#{beanParamOperacion.mapValuesFactura}" id="dsFacturacion" var="registroF" rows="10" 
																				               rowKeyVar="row" ajaxKeys="#{beanParamOperacion.keysFac}" columns="10" width="90%" align="center">
																					<f:facet name="header"><h:outputText value="Parametrizacion " /></f:facet>

																					<rich:column title="Ramo" width="5%" styleClass="texto_centro" sortBy="#{registroF.copaNvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Ramo " title="Ramo" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaNvalor2}" />
																					</rich:column>

																					<rich:column title="Poliza" width="5%"  styleClass="texto_centro"	sortBy="#{registroF.copaNvalor3}">
																						<f:facet name="header">
																							<h:outputText value=" Poliza " title="Poliza" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaNvalor3}" />
																					</rich:column>

																					<rich:column title="Concepto" width="30%" styleClass="texto_centro" sortBy="#{registroF.copaIdParametro}">
																						<f:facet name="header">
																							<h:outputText value=" Concepto " title="Concepto" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaIdParametro}" />
																					</rich:column>

																					<rich:column title="copaVvalor4" width="5%" styleClass="texto_centro" sortBy="#{registroF.copaVvalor4}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor4 "
																								title="copaVvalor4" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaVvalor4}" />
																					</rich:column>

																					<rich:column title="copaVvalor5" width="5%" styleClass="texto_centro" sortBy="#{registroF.copaVvalor5}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor5 "
																								title="copaVvalor5" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaVvalor5}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor6" width="5%" styleClass="texto_centro" sortBy="#{registroF.copaVvalor6}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor6 "
																								title="copaVvalor6" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaVvalor6}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor7" width="5%" styleClass="texto_centro" sortBy="#{registroF.copaVvalor7}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor7 "
																								title="copaVvalor7" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaVvalor7}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor8" width="5%" styleClass="texto_centro" sortBy="#{registroF.copaVvalor8}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor8 "
																								title="copaVvalor8" />
																						</f:facet>
																						<h:outputText value="#{registroF.copaVvalor8}" />
																					</rich:column>
																					
																					<rich:column title="Modificar" width="5%"
																						styleClass="texto_centro">
																						<f:facet name="header">
																							<h:outputText value=" Modificar " />
																						</f:facet>
																						<a4j:commandLink ajaxSingle="true" id="editlinkF" oncomplete="#{rich:component('editPanelFacturacion')}.show()">
																							<h:graphicImage 	value="../../images/edit_page.png"	style="border:0" />
																							<f:setPropertyActionListener value="#{registroF}"  target="#{beanParamOperacion.objActualFac}" />
																							<f:setPropertyActionListener value="#{row}"  target="#{beanParamOperacion.filaActualFac}" />
																						</a4j:commandLink>
																						<rich:toolTip for="editlinkFac" value="Editar" />
																					</rich:column>
																				</rich:dataTable>
																				</c:otherwise>	
																					</c:choose>
																			</td>
																	</tr>
																	
																	
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											<%-- termina Facturacion --%>
											
											
											<!-- Inicio Obligados -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<span id="img_sec_resultados6_m" style="display: none;">
																	<h:graphicImage value="../../images/mostrar_seccion.png"  onclick="muestraOcultaSeccion('resultados6', 0);" title="Oculta" />&nbsp;
																</span> 
																<span id="img_sec_resultados6_o">
																    <h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados6', 1);" title="Muestra" />&nbsp;
																</span>
																<span class="titulos_secciones">Obligados </span>
															</legend>

															<a4j:outputPanel id="regTablaObligados" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_resultados6"	style="display: none;">
																		<td>
																		<%-- cambiar  --%>
																		<c:choose>
																			<c:when test="${fn:length(beanParamOperacion.listaParametrosObligados) == 0}">
																				<table width="90%" align="center" border="0">
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																				</table>
																			</c:when>
																			<c:otherwise>															
																				<rich:contextMenu attached="false" id="menu6" submitMode="ajax" 
																						oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																		            <rich:menuItem value="Edit Record" ajaxSingle="true"
																		                	oncomplete="#{rich:component('editPanel')}.show()"
																		                	actionListener="#{beanParamOperacion.buscarFilaActualObligado}">
																		                <a4j:actionparam name="clave" value="{clave}" />
																		                <a4j:actionparam name="row" value="{filaActual}" />
																		            </rich:menuItem>
																		        </rich:contextMenu>
													        					<%-- termina el cambio  --%>
													        					
																				<rich:datascroller for="dsObligado" maxPages="10" />
																				<rich:dataTable value="#{beanParamOperacion.mapValuesObligado}" id="dsObligado" var="registroO" rows="10" 
																				               rowKeyVar="row" ajaxKeys="#{beanParamOperacion.keysObligado}" columns="10" width="90%" align="center">
																					<f:facet name="header"><h:outputText value="Parametrizacion " /></f:facet>

																					<rich:column title="Ramo" width="5%" styleClass="texto_centro" sortBy="#{registroO.copaNvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Ramo " title="Ramo" />
																						</f:facet>
																						<h:outputText value="#{registroO.copaNvalor2}" />
																					</rich:column>

																					<rich:column title="Poliza" width="5%"  styleClass="texto_centro"	sortBy="#{registroO.copaNvalor3}">
																						<f:facet name="header">
																							<h:outputText value=" Poliza " title="Poliza" />
																						</f:facet>
																						<h:outputText value="#{registroO.copaNvalor3}" />
																					</rich:column>

																					<rich:column title="Concepto" width="30%" styleClass="texto_centro" sortBy="#{registroO.copaIdParametro}">
																						<f:facet name="header">
																							<h:outputText value=" Concepto " title="Concepto" />
																						</f:facet>
																						<h:outputText value="#{registroO.copaIdParametro}" />
																					</rich:column>

																					<rich:column title="copaVvalor1" width="5%" styleClass="texto_centro" sortBy="#{registroO.copaVvalor1}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor1 "
																								title="copaVvalor1" />
																						</f:facet>
																						<h:outputText value="#{registroO.copaVvalor1}" />
																					</rich:column>

																					<rich:column title="Consecutivo" width="5%" styleClass="texto_centro" sortBy="#{registroO.copaVvalor2}">
																						<f:facet name="header">
																							<h:outputText value=" Consecutivo "
																								title="Consecutivo" />
																						</f:facet>
																						<h:outputText value="#{registroO.copaVvalor2}" />
																					</rich:column>
																					
																					<rich:column title="copaVvalor3" width="5%" styleClass="texto_centro" sortBy="#{registroO.copaVvalor3}">
																						<f:facet name="header">
																							<h:outputText value=" copaVvalor3 "
																								title="copaVvalor3" />
																						</f:facet>
																						<h:outputText value="#{registroO.copaVvalor3}" />
																					</rich:column>
																					
																						<rich:column title="copaNvalor4" width="5%" styleClass="texto_centro" sortBy="#{registroO.copaNvalor4}">
																						<f:facet name="header">
																							<h:outputText value=" copaNvalor4 "
																								title="copaNvalor4" />
																						</f:facet>
																						<h:outputText value="#{registroO.copaNvalor4}" />
																					</rich:column>
																					
																					
																					
																					<rich:column title="Modificar" width="5%"
																						styleClass="texto_centro">
																						<f:facet name="header">
																							<h:outputText value=" Modificar " />
																						</f:facet>
																						<a4j:commandLink ajaxSingle="true" id="editlinkO" oncomplete="#{rich:component('editPanelObligado')}.show()">
																							<h:graphicImage 	value="../../images/edit_page.png"	style="border:0" />
																							<f:setPropertyActionListener value="#{registroO}"  target="#{beanParamOperacion.objActualObligado}" />
																							<f:setPropertyActionListener value="#{row}"  target="#{beanParamOperacion.filaActualObligado}" />
																						</a4j:commandLink>
																						<rich:toolTip for="editlinkO" value="Editar" />
																					</rich:column>
																				</rich:dataTable>
																				</c:otherwise>	
																					</c:choose>
																			</td>
																	</tr>
																	
																	<tr>
																	<td width="4">&nbsp;</td>
																	<td width="13%" align="right">
																		<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevoObligado" oncomplete="#{rich:component('newPanelObligados')}.show()" />
			                    										<rich:toolTip for="btnNuevoObligado" value="Nuevo Parametro Obligados" />									
																	</td>
															     	</tr>
																	
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											<%-- termina Obligados --%>
											
								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>
		
		</div>
	</h:form>
	<rich:spacer height="10px"></rich:spacer>

	<div><%@include file="../footer.jsp"%></div>



	
	<%-- modal editar ENDOSO --%>
	
		<rich:modalPanel id="editPanelEndoso" autosized="true" width="350">
		<f:facet name="header">
			<h:outputText value="Editar Endoso" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
				<rich:componentControl for="editPanelEndoso" attachTo="hidelink" operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="editarEndoso">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo : " styleClass="texto_mediano" />
						<h:outputText
							value="#{beanParamOperacion.objActualE.copaNvalor2}"
							styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
							<h:outputText value="Poliza	: " styleClass="texto_mediano" />
						<h:outputText
							value="#{beanParamOperacion.objActualE.copaNvalor3}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="Concepto : "  styleClass="texto_mediano"/>
						<h:outputText value="#{beanParamOperacion.objActualE.copaIdParametro}" styleClass="texto_mediano"/>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						
					

						<h:outputText value="Endoso en Emision : " />
						<h:inputText
							value="#{beanParamOperacion.objActualE.copaVvalor3}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="Endoso en Renovacion " />
						<h:inputText
							value="#{beanParamOperacion.objActualE.copaVvalor4}" size="5"/>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="Endoso en Cancelacion: " />
						<h:inputText
							value="#{beanParamOperacion.objActualE.copaVvalor5}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Tipo de calculo : " />
						<h:inputText
							value="#{beanParamOperacion.objActualE.copaNvalor5}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Modificar"
					action="#{beanParamOperacion.modificar}"
					reRender="mensaje,regTabla,dsProducto, dsEndoso, regTablaEndoso"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanelEndoso')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>

	<%--Fin panel editar ENDOSO --%>
	
	<%-- modal editar REHABILITACION --%>

	<rich:modalPanel id="editPanel" autosized="true" width="350">
		<f:facet name="header">
			<h:outputText value="Editar Parametro" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png" id="hidelinkR"
					styleClass="hidelinkR" />
				<rich:componentControl for="editPanel" attachTo="hidelinkR"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="editarRehabilitacion">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: "  styleClass="texto_mediano"/>
						<h:outputText value="#{beanParamOperacion.objActual.copaNvalor2}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Poliza: "  styleClass="texto_mediano"/>
						<h:outputText value="#{beanParamOperacion.objActual.copaNvalor3}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Concepto: " styleClass="texto_mediano" />
						<h:outputText value="#{beanParamOperacion.objActual.copaIdParametro}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />

						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						
						<h:outputText value="copaNvalor4: " />
						<h:inputText value="#{beanParamOperacion.objActual.copaNvalor4}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaNvalor5: " />
						<h:inputText value="#{beanParamOperacion.objActual.copaNvalor5}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaNvalor6: " />
						<h:inputText value="#{beanParamOperacion.objActual.copaNvalor6}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Estatus valdo para rehabilitar: " />
						<h:inputText value="#{beanParamOperacion.objActual.copaVvalor1}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
					
						<h:outputText value="copaVvalor3: " />
						<h:inputText  value="#{beanParamOperacion.objActual.copaVvalor3}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor4: " />
						<h:inputText  value="#{beanParamOperacion.objActual.copaVvalor4}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor5: " />
						<h:inputText  value="#{beanParamOperacion.objActual.copaVvalor5}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor6: " />
						<h:inputText value="#{beanParamOperacion.objActual.copaVvalor6}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Modificar"
					action="#{beanParamOperacion.modificarRehabilita}"
					reRender="mensaje,regTabla,dsProducto, dsEndoso"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%-- fin editar REHABILITACION --%>
	
	<%-- modal editar RESTITUCION --%>

	<rich:modalPanel id="editPanelRestitucion" autosized="true" width="350">
		<f:facet name="header">
			<h:outputText value="Editar parametro de Restitucion" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png" id="hidelinkRest"
					styleClass="hidelinkRest" />
				<rich:componentControl for="editPanelRestitucion" attachTo="hidelinkRest"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="editarRestitucion">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo : " styleClass="texto_mediano"/>
						<h:outputText value="#{beanParamOperacion.objActualResti.copaNvalor2}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Poliza : " styleClass="texto_mediano" />
						<h:outputText value="#{beanParamOperacion.objActualResti.copaNvalor3}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Concepto : " styleClass="texto_mediano"/>
						<h:outputText value="#{beanParamOperacion.objActualResti.copaIdParametro}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />

						<h:outputText value="copaNvalor4: " />
						<h:inputText  value="#{beanParamOperacion.objActualResti.copaNvalor4}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaNvalor5: " />
						<h:inputText value="#{beanParamOperacion.objActualResti.copaNvalor5}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaNvalor6 : " />
						<h:inputText value="#{beanParamOperacion.objActualResti.copaNvalor6}"  size="5"/>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaNvalor7: " />
						<h:inputText  value="#{beanParamOperacion.objActualResti.copaNvalor7}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaNvalor8: " />
						<h:inputText  value="#{beanParamOperacion.objActualResti.copaNvalor8}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Modificar"
					action="#{beanParamOperacion.modificarRestitucion}"
					reRender="mensaje,regTabla,dsProducto, dsEndoso"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanelRestitucion')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%-- fin editar RESTTUCON --%>
	
	<%-- modal editar SALDO --%>

	<rich:modalPanel id="editPanelSaldo" autosized="true" width="350">
		<f:facet name="header">
			<h:outputText value="Editar parametro de Saldo" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png" id="hidelinkSaldo"
					styleClass="hidelinkSaldo" />
				<rich:componentControl for="editPanelSaldo" attachTo="hidelinkSaldo"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="editarSaldo">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo : "  styleClass="texto_mediano"  />
						<h:outputText value="#{beanParamOperacion.objActualSaldo.copaNvalor2}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="Poliza : "  styleClass="texto_mediano" />
						<h:outputText value="#{beanParamOperacion.objActualSaldo.copaNvalor3}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Concepto : "  styleClass="texto_mediano"  />
						<h:outputText value="#{beanParamOperacion.objActualSaldo.copaIdParametro}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
							
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />

						<h:outputText value="copaVvalor1: " />
						<h:inputText value="#{beanParamOperacion.objActualSaldo.copaVvalor1}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor2: " />
						<h:inputText value="#{beanParamOperacion.objActualSaldo.copaVvalor2}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor4: " />
						<h:inputText value="#{beanParamOperacion.objActualSaldo.copaVvalor4}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor6: " />
						<h:inputText value="#{beanParamOperacion.objActualSaldo.copaVvalor6}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Modificar"
					action="#{beanParamOperacion.modificarSaldo}"
					reRender="mensaje,regTabla,dsProducto, dsEndoso"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanelSaldo')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%-- fin editar SALDO --%>
	
		<%-- modal editar DEVOLUCION --%>

	<rich:modalPanel id="editPanelDevolucion" autosized="true" width="350">
		<f:facet name="header">
			<h:outputText value="Editar parametro de Devolucion" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png" id="hidelinkDev"
					styleClass="hidelinkDev" />
				<rich:componentControl for="editPanelDevolucion" attachTo="hidelinkDev"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="editarDevolucion">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo : " styleClass="texto_mediano" />
						<h:outputText value="#{beanParamOperacion.objActualDev.copaNvalor2}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Polza : " styleClass="texto_mediano" />
						<h:outputText value="#{beanParamOperacion.objActualDev.copaNvalor3}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Concepto  : " />
						<h:outputText value="#{beanParamOperacion.objActualDev.copaDesParametro}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />

						<h:outputText value="copaNvalor4: " />
						<h:inputText value="#{beanParamOperacion.objActualDev.copaNvalor4}" styleClass="texto_mediano" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />

						<h:outputText value="copaNvalor5 : " />
						<h:inputText value="#{beanParamOperacion.objActualDev.copaNvalor5}"	styleClass="texto_mediano" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

												
					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Modificar"
					action="#{beanParamOperacion.modificarDevolucion}"
					reRender="mensaje,regTabla,dsProducto, dsEndoso"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanelDevolucion')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%-- fin editar DEVOLUCION --%>
	
	<%-- modal editar FACTURACION --%>

	<rich:modalPanel id="editPanelFacturacion" autosized="true" width="350">
		<f:facet name="header">
			<h:outputText value="Editar Parametro Facturacion" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png" id="hidelinkF"
					styleClass="hidelinkF" />
				<rich:componentControl for="editPanelFacturacion" attachTo="hidelinkF"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="editarFacturacion">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo : " />
						<h:outputText value="#{beanParamOperacion.objActualFac.copaNvalor2}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Poliza : " />
						<h:outputText value="#{beanParamOperacion.objActualFac.copaNvalor3}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Concepto : " />
						<h:outputText value="#{beanParamOperacion.objActualFac.copaIdParametro}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />

						<h:outputText value="copaVvalor4: " />
						<h:inputText value="#{beanParamOperacion.objActualFac.copaVvalor4}" styleClass="texto_mediano" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor5: " />
						<h:inputText  value="#{beanParamOperacion.objActualFac.copaVvalor5}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor6: " />
						<h:inputText  value="#{beanParamOperacion.objActualFac.copaVvalor6}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaVvalor7: " />
						<h:inputText  value="#{beanParamOperacion.objActualFac.copaVvalor7}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaVvalor8: " />
						<h:inputText  value="#{beanParamOperacion.objActualFac.copaVvalor8}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
			

					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Modificar"
					action="#{beanParamOperacion.modificarFacturacion}"
					reRender="mensaje,regTabla,dsProducto, dsEndoso"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanelFacturacion')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%-- fin editar FACTURACION --%>
	
	<%-- modal editar Obligados --%>

	<rich:modalPanel id="editPanelObligado" autosized="true" width="350">
		<f:facet name="header">
			<h:outputText value="Editar Parametro Facturacion" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png" id="hidelinkO"
					styleClass="hidelinkO" />
				<rich:componentControl for="editPanelObligado" attachTo="hidelinkO"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="editarObligado">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: " />
						<h:outputText
							value="#{beanParamOperacion.objActualObligado.copaNvalor2}"
							 />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Poliza : " />
						<h:inputText
							value="#{beanParamOperacion.objActualObligado.copaNvalor3}"
							size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Concepto: " />
						<h:outputText
							value="#{beanParamOperacion.objActualObligado.copaIdParametro}"
							 />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />

						<h:outputText value="copaVvalor1: " />
						<h:inputText
							value="#{beanParamOperacion.objActualObligado.copaVvalor1}"
							 size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />

						<h:outputText value="Consecutivo : " />
						<h:inputText
							value="#{beanParamOperacion.objActualObligado.copaVvalor2}"
							 />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor3 : " />
						<h:inputText
							value="#{beanParamOperacion.objActualObligado.copaVvalor3}"
							 />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
											
						
						<h:outputText value="copaNvalor4 : " />
						<h:inputText
							value="#{beanParamOperacion.objActualObligado.copaNvalor4}" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Modificar"
					action="#{beanParamOperacion.modificarObligado}"
					reRender="mensaje,regTabla,dsProducto, dsEndoso"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanelObligado')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%-- fin editar Obligados --%>
	
	
    <%-- Boton NUEVOS parametros--%>
    
    <%-- Nuevo Endoso --%>
	<rich:modalPanel id="newPanel" autosized="true" width="450">
		<f:facet name="header">
			<h:outputText value="Nuevo Endoso" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png"
					id="hidelinkNew" styleClass="hidelink" />
				<rich:componentControl for="newPanel" attachTo="hidelinkNew"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="nuevoEndoso">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: " />
						<h:selectOneMenu id="cmbNewRamo" value="#{beanParamOperacion.copaNvalor2}" required="true" >
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaPolizas}" reRender="cmbNewPol" />
						</h:selectOneMenu>	
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Poliza	: " />
						<h:selectOneMenu id="cmbNewPol" value="#{beanParamOperacion.copaNvalor3}"  >
	                        	<f:selectItem itemValue="-2" itemLabel="--SELECCIONAR--"/>
	                        	<f:selectItems value="#{beanParamOperacion.listaComboPolizas}" />
	                        	<a4j:support event="onchange"  action="#{beanParamOperacion.cargaIdVenta}" reRender="cmbNewIdventa" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
										
						<h:outputText value="Id venta	: " />
						<h:selectOneMenu id="cmbNewIdventa" value="#{beanParamOperacion.copaNvalor4}"  >
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboIdVenta}" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />

						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						
						
						<h:outputText value="Endoso en Emision : " />
						<h:selectOneMenu id="endosoEmi"
							value="#{beanParamOperacion.copaVvalor3}" >
							<f:selectItem itemValue="" itemLabel="--SELECCIONAR--"/>
							<f:selectItem itemValue="100" itemLabel=" Permitiir endoso de Emision " />
							<f:selectItem itemValue="0" itemLabel="Sin permiso de Emisi�n " />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						

						<h:outputText value="Endoso en Renovacion  " />
						<h:selectOneMenu id="endosoRen"
							value="#{beanParamOperacion.copaVvalor4}" >
							<f:selectItem itemValue="" itemLabel="--SELECCIONAR--"/>
							<f:selectItem itemValue="300" itemLabel=" Permite Endoso Renovaci�n " />
							<f:selectItem itemValue="0" itemLabel=" No permite  de Endoso en Renovaci�n " />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="Endoso en Cancelacion:   " />
						<h:selectOneMenu id="endosoCanc"
							value="#{beanParamOperacion.copaVvalor5}" >
							<f:selectItem itemValue="" itemLabel="--SELECCIONAR--"/>
							<f:selectItem itemValue="200" itemLabel=" Permite Endoso  en Cancelacion " />
							<f:selectItem itemValue="0" itemLabel=" No permite  de Endoso en Cancelacion " />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="Tipo de Calculo: " />
						<h:selectOneMenu id="endosoTC"
							value="#{beanParamOperacion.copaNvalor5}">
							<f:selectItem itemValue="" itemLabel="--SELECCIONAR--"/>
							<f:selectItem itemValue="1" itemLabel="Recibo directo" />
							<f:selectItem itemValue="2" itemLabel="Calculo sobre anualidad" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						
						
						
					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Guardar"
					action="#{beanParamOperacion.altaParamEndoso}"
					reRender="mensaje,regTabla,dsProducto"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%--fn endoso --%>
	
	<%-- Nuevo Rehabilita   btnNuevoRehabilita--%>  
	<rich:modalPanel id="newPanelRehabilita" autosized="true" width="450">
		<f:facet name="header">
			<h:outputText value="Nuevo Parametro" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png"
					id="hidelinkNewReha" styleClass="hidelink" />
				<rich:componentControl for="newPanelRehabilita" attachTo="hidelinkNewReha"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="nuevoParametroRehabilita">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">
								
						<h:outputText value="Ramo: " />
						<h:selectOneMenu id="cmbNewRamo" value="#{beanParamOperacion.copaNvalor2}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaPolizas}" reRender="cmbNewPol" />
						</h:selectOneMenu>	
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Poliza	: " />
						<h:selectOneMenu id="cmbNewPol" value="#{beanParamOperacion.copaNvalor3}" >
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboPolizas}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaIdVenta}" reRender="cmbNewIdventa" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Id venta	: " />
						<h:selectOneMenu id="cmbNewIdventa" value="#{beanParamOperacion.copaNvalor4}"  >
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboIdVenta}" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
									
						<h:outputText value="copaNvalor5: " />
						<h:inputText value="#{beanParamOperacion.copaNvalor5}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaNvalor6: " />
						<h:inputText value="#{beanParamOperacion.copaNvalor6}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="Estatus valdo para rehabilitar: " />
						<h:inputText value="#{beanParamOperacion.copaVvalor1}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
					
						<h:outputText value="copaVvalor3: " />
						<h:inputText  value="#{beanParamOperacion.copaVvalor3}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor4: " />
						<h:inputText  value="#{beanParamOperacion.copaVvalor4}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor5: " />
						<h:inputText  value="#{beanParamOperacion.copaVvalor5}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor6: " />
						<h:inputText value="#{beanParamOperacion.copaVvalor6}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						
					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Guardar"
					action="#{beanParamOperacion.altaParamRehabilicion}"
					reRender="mensaje,regTabla,dsProducto, regTablaRehabilita"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('nuevoParametroRehabilita')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	<%--  fin Nuevo Rehabilita --%>
	
	
	   <%--Restitucion --%>
	<rich:modalPanel id="newPanelRestitucion" autosized="true" width="450">
		<f:facet name="header">
			<h:outputText value="Nuevo Parametro de Restitucion " />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png"
					id="hidelinkNewResti" styleClass="hidelinkResti" />
				<rich:componentControl for="newPanelRestitucion" attachTo="hidelinkNewResti"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="nuevoParametroRestitucion">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: "/>
	                    <h:selectOneMenu id="cmbNewRamo" value="#{beanParamOperacion.copaNvalor2}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaPolizas}" reRender="cmbNewPol" />		
						</h:selectOneMenu>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						
						<h:outputText value="Poliza: "/>
	                    <h:selectOneMenu id="cmbNewPol" value="#{beanParamOperacion.copaNvalor3}" required="true" requiredMessage="* POLIZA - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboPolizas}" />
						</h:selectOneMenu>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						<h:outputText value="      "/>

						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double" />

						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />

						<h:outputText value="copaNvalor4: " />
						<h:inputText  value="#{beanParamOperacion.copaNvalor4}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaNvalor5: " />
						<h:inputText value="#{beanParamOperacion.copaNvalor5}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaNvalor6 : " />
						<h:inputText value="#{beanParamOperacion.copaNvalor6}"  size="5"/>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaNvalor7: " />
						<h:inputText  value="#{beanParamOperacion.copaNvalor7}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaNvalor8: " />
						<h:inputText  value="#{beanParamOperacion.copaNvalor8}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />


					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Guardar"
					action="#{beanParamOperacion.altaParamRestitucion}"
					reRender="mensaje,regTabla,dsProducto"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanelRestitucion')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	
	<%--fn Restitucion --%>
	
	
	
		<%-- Nuevo Saldo   --%>  
	<rich:modalPanel id="newPanelSaldo" autosized="true" width="450">
		<f:facet name="header">
			<h:outputText value="Nuevo Parametro Saldo" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png"
					id="hidelinkNewSaldo" styleClass="hidelinkSaldo" />
				<rich:componentControl for="newPanelSaldo" attachTo="hidelinkNewSaldo"
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="nuevoParametroSaldo">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: "/>
	                    <h:selectOneMenu id="cmbNewRamo" value="#{beanParamOperacion.copaNvalor2}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaPolizas}" reRender="cmbNewPol" />
						</h:selectOneMenu>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						
						<h:outputText value="Poliza: "  />
						<h:selectOneMenu id="cmbNewPol" value="#{beanParamOperacion.copaNvalor3}" required="true" requiredMessage="* POLIZA - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboPolizas}" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
									
						<h:outputText value="copaVvalor1: " />
						<h:inputText value="#{beanParamOperacion.copaVvalor1}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor2: " />
						<h:inputText value="#{beanParamOperacion.copaVvalor2}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor4: " />
						<h:inputText value="#{beanParamOperacion.copaVvalor4}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor6: " />
						<h:inputText value="#{beanParamOperacion.copaVvalor6}" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Guardar"
					action="#{beanParamOperacion.altaParamSaldo}"
					reRender="mensaje,regTabla,dsProducto"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanelSaldo')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	<%--  fin Nuevo Saldo --%>
	
	
		<%-- Nuevo devolucion   --%>  
	<rich:modalPanel id="newPanelDevolucion" autosized="true" width="450">
		<f:facet name="header">
			<h:outputText value="Nuevo Parametro Devolucion" />
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png"
					id="hidelinkNewDevolucion" styleClass="hidelinkDevolucion" />
				<rich:componentControl for="newPanelDevolucion" attachTo="hidelinkNewDevolucion" 
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="nuevoParametroDevolucion">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: "/>
	                    <h:selectOneMenu id="cmbNewRamo" value="#{beanParamOperacion.copaNvalor2}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaPolizas}" reRender="cmbNewPol" />
						</h:selectOneMenu>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						
						<h:outputText value="Poliza: " />
						<h:selectOneMenu id="cmbNewPol" value="#{beanParamOperacion.copaNvalor3}" required="true" requiredMessage="* Pol - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboPolizas}" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
									
						<h:outputText value="copaNvalor4: " />
						<h:inputText value="#{beanParamOperacion.copaNvalor4}" styleClass="texto_mediano" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />

						<h:outputText value="copaNvalor5 : " />
						<h:inputText value="#{beanParamOperacion.copaNvalor5}"	styleClass="texto_mediano" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
			
						
					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Guardar"
					action="#{beanParamOperacion.altaParamDevolucion}"
					reRender="mensaje,regTabla,dsProducto"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanelDevolucion')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	<%--  fin Nuevo Devolucion --%>
	
	
	
		<%-- Nuevo Facturacion   --%>  
	<rich:modalPanel id="newPanelFacturacion" autosized="true" width="450">
		<f:facet name="header">
			<h:outputText value="Nuevo Parametro Facturacion" /> 
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png"
					id="hidelinkNewFacturacion" styleClass="hidelinkFacturacion" />
				<rich:componentControl for="newPanelFacturacion" attachTo="hidelinkNewFacturacion" 
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="nuevoParametroFacturacion">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: "/>
	                    <h:selectOneMenu id="cmbNewRamo" value="#{beanParamOperacion.copaNvalor2}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaPolizas}" reRender="cmbNewPol" />
						</h:selectOneMenu>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						
						<h:outputText value="Poliza: "  />
						<h:selectOneMenu id="cmbNewPol" value="#{beanParamOperacion.copaNvalor3}" required="true" requiredMessage="* Pol - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboPolizas}" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
									
						
						<h:outputText value="copaVvalor4: " />
						<h:inputText value="#{beanParamOperacion.copaVvalor4}" styleClass="texto_mediano" size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor5: " />
						<h:inputText  value="#{beanParamOperacion.copaVvalor5}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor6: " />
						<h:inputText  value="#{beanParamOperacion.copaVvalor6}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaVvalor7: " />
						<h:inputText  value="#{beanParamOperacion.copaVvalor7}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
						<h:outputText value="copaVvalor8: " />
						<h:inputText  value="#{beanParamOperacion.copaVvalor8}" styleClass="texto_mediano" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
						
					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Guardar"
					action="#{beanParamOperacion.altaParamFacturacion}"
					reRender="mensaje,regTabla,dsProducto"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanelFacturacion')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	<%--  fin Nuevo Facturacion --%>
	
	
	
		
		<%-- Nuevo Obligados   --%>  
	<rich:modalPanel id="newPanelObligados" autosized="true" width="450">
		<f:facet name="header">
			<h:outputText value="Nuevo Parametro Obligados" /> 
		</f:facet>
		<f:facet name="controls">
			<h:panelGroup>
				<h:graphicImage value="../../images/cerrarModal.png"
					id="hidelinkNewObligados" styleClass="hidelinkObligados" />
				<rich:componentControl for="newPanelObligados" attachTo="hidelinkNewObligados" 
					operation="hide" event="onclick" />
			</h:panelGroup>
		</f:facet>
		<h:form id="nuevoParametroObligados">
			<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
				<a4j:outputPanel ajaxRendered="true">
					<h:panelGrid columns="5">

						<h:outputText value="Ramo: "/>
	                    <h:selectOneMenu id="cmbNewRamo" value="#{beanParamOperacion.copaNvalor2}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}"  reRender="cmbNewPol" />
						</h:selectOneMenu>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						
						
						<h:outputText value="Poliza: "/>
	                	<h:selectOneMenu id="cmbNewPol" value="#{beanParamOperacion.copaNvalor3}" >
	                        	<f:selectItem itemValue="-2" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboPolizas}" />
  								<a4j:support event="onchange"  action="#{beanParamOperacion.cargaIdVenta}" reRender="cmbNewIdventa"/>
						</h:selectOneMenu>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						<h:outputText value="      "/>
						
						
						<h:outputText value="Id venta	: " />
						<h:selectOneMenu id="cmbNewIdventa" value="#{beanParamOperacion.copaNvalor4}"  >
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanParamOperacion.listaComboIdVenta}" />
						</h:selectOneMenu>
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
							
						<h:outputText value="copaVvalor1: " />
						<h:inputText  value="#{beanParamOperacion.objActualObligado.copaVvalor1}"  size="5" />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
									
						
						<h:outputText value="Consecutivo : " />
						<h:inputText
							value="#{beanParamOperacion.objActualObligado.copaVvalor2}"
							 />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />

						<h:outputText value="copaVvalor3 : " />
						<h:inputText
							value="#{beanParamOperacion.objActualObligado.copaVvalor3}"
							 />
						<h:outputText value="      " />
						<h:outputText value="      " />
						<h:outputText value="      " />
											
					
						
					</h:panelGrid>
				</a4j:outputPanel>
				<a4j:commandButton value="Guardar"
					action="#{beanParamOperacion.altaParamObligado}"
					reRender="mensaje,regTabla,dsProducto"
					oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanelObligados')}.hide();" />
			</h:panelGrid>
		</h:form>
	</rich:modalPanel>
	<%--  fin Nuevo Facturacion --%>
		
	
	
	<a4j:status onstart="#{rich:component('wait')}.show()"
		onstop="#{rich:component('wait')}.hide()" />
	<rich:modalPanel id="wait" autosized="true" width="160" height="60"
		moveable="false" resizeable="false">
		<f:facet name="header">
			<h:outputText value="Procesando" />
		</f:facet>
		<h:graphicImage value="../../images/reloj.png" />
		<h:outputText value="    Por favor espere..." />
	</rich:modalPanel>
</body>
	</html>
</f:view>
