<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>

	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
			<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
			<title>Parametrizacion</title>
		</head>
		<body>
		<h:form id="frmCatalogoProductos">
			<div align="center"><%@include file="../header.jsp" %></div> 
			<div align="center">
				<table class="tablaPrincipal">
					<tbody>
						<tr>
							<td align="center">
								<table class="tablaSecundaria">
									<tbody>
										<tr>
											<td>
												<%@ include file="/WEB-INF/includes/tituloParametrizacion.jsp" %>
							    				<div align="center">
								    				<table width="90%" cellpadding="0" cellspacing="0" align="center">
									    				<tr class="espacio_15"><td>&nbsp;</td></tr>
														<tr><td>
																<h:outputText id="mensaje" value="#{beanParametrizacion.strRespuesta}" styleClass="error"/><br>
																<h:outputText id="mensaje1" value="#{beanCoberturas.strRespuesta}" 	   styleClass="error"/>
															</td>
														</tr>	
														<tr class="espacio_15"><td>&nbsp;</td></tr>
								    				</table>
				
													<%@ include file="/WEB-INF/includes/parametrizacion/paramConsulta.jsp" %>
													<%@ include file="/WEB-INF/includes/parametrizacion/paramListaCoberturas.jsp" %>
													<%@ include file="/WEB-INF/includes/parametrizacion/paramListaComponentes.jsp" %>
													
													<rich:spacer height="10px"></rich:spacer>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
		
							<%--@include file="/WEB-INF/includes/componentesView.jsp" --%>
							<%--@include file="/WEB-INF/includes/homologaView.jsp"    --%>
								
							</td>
						</tr>
					</tbody>
				</table>   	
			</div>
			</h:form>
			
			<rich:spacer height="15px"></rich:spacer>
			
			<div><%@include file="../footer.jsp" %></div>
		
			<%@include file="/WEB-INF/includes/parametrizacion/modalCoberturas.jsp"%>
			<%@include file="/WEB-INF/includes/parametrizacion/modalCoberturasAsociar.jsp"%>
			<%@include file="/WEB-INF/includes/parametrizacion/modalCoberturasNuevo.jsp"%>
			<%@include file="/WEB-INF/includes/parametrizacion/modalComponentesAsociar.jsp"%>
		
		   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
		   	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
		       	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
		       	<h:graphicImage value="../../images/reloj.png"/>
		       	<h:outputText value="    Por favor espere..." />
		   	</rich:modalPanel>
		
			
		</body>
	</html>
</f:view>	
