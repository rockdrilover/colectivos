<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Centro de Costos</title>
	</head>

	<body>
		<h:form id="frmCatalogoCentroCosto">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> I </td>
											<td align="left" width="64%">Cat�logo Centro de Costos</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanCentroCosto.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    						    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
														<td width="70%">
															Ramo:&nbsp;&nbsp;
							                    			<h:selectOneMenu value="#{beanCentroCosto.copaNvalor2}">
											     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
											     				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
											     			</h:selectOneMenu>												     		
		                   								</td>
						                    			<td width="13%" align="right">						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanCentroCosto.consultaCentroCostos}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consulta Centro de Costos" />													
														</td>
														<td width="4">&nbsp;</td>
														<td width="13%" align="right">
															<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nuevo Centro de Costos" />									
														</td>
						                    		</tr>
						                    		
						                    		<tr class="espacio_15"><td>&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanCentroCosto.hmResultados == null}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanCentroCosto.hmResultados != null}">
															<rich:contextMenu attached="false" id="menu" submitMode="ajax" 
																	oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
													            <rich:menuItem value="Edit Record" ajaxSingle="true"
													                	oncomplete="#{rich:component('editPanel')}.show()"
													                	actionListener="#{beanCentroCosto.buscarFilaActual}">
													                <a4j:actionparam name="clave" value="{clave}" />
													                <a4j:actionparam name="row" value="{filaActual}" />
													            </rich:menuItem>
													        </rich:contextMenu>
													        
															<rich:datascroller for="dsCentroCosto" maxPages="10"/>
															<rich:dataTable value="#{beanCentroCosto.mapValues}" id="dsCentroCosto" var="registro" 
																	rows="10" rowKeyVar="row" ajaxKeys="#{beanCentroCosto.keys}" 
																	columns="10" width="90%" align="center">
									      						<f:facet name="header"><h:outputText value="Centro costos"/></f:facet>
									      						<rich:column title="Ramo" width="10%" styleClass="texto_centro" sortBy="#{registro.copaNvalor2}" >
									      							<f:facet name="header"><h:outputText value=" Ramo " title="Ramo" /></f:facet>
																	<h:outputText value="#{registro.copaNvalor2}"/>  
									      						</rich:column>
														      	<rich:column title="CentroCosto" width="15%" styleClass="texto_centro" sortBy="#{registro.copaNvalor5}">
														      		<f:facet name="header"><h:outputText value=" Centro Costos"  title="CentroCosto"/></f:facet>
														      		<h:outputText value="#{registro.copaNvalor5}"/>
														      	</rich:column>
														      	<rich:column title="Descripcion" width="40%" styleClass="texto_centro" sortBy="#{registro.copaVvalor1}">
														      		<f:facet name="header"><h:outputText value=" Descripcion " title="Descripcion" /></f:facet>
																	<h:outputText value="#{registro.copaVvalor1}"/>  
														      	</rich:column>
														      	<rich:column title="IniVigencia" width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Inicio de Vigencia"  title="IniVigencia"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor1}">
														      		<f:convertDateTime pattern="dd-MM-yyyy" />
														      		</h:outputText>
														      	</rich:column>
														      	<rich:column title="FinVigencia" width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="Fin de Vigencia"  title="FinVigencia"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor2}">
														      		<f:convertDateTime pattern="dd-MM-yyyy" />
														      		</h:outputText>
														      	</rich:column>
													      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />                        												
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanCentroCosto.objActual}" /> 
                        												<f:setPropertyActionListener value="#{row}" target="#{beanCentroCosto.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    				<%--  fn resultados--%>			    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Centro de Costo" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
   	
    		<h:form id="editarCentroCostos">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	<h:outputText value="Ramo: "/>
	                    	<h:outputText value="#{beanCentroCosto.objActual.auxDato}" styleClass="texto_mediano"/>
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="Descripcion: "/>
							<h:inputText value="#{beanCentroCosto.objActual.copaVvalor1}" style="width: 250px" maxlength="80" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>
							
							<h:outputText value="Centro Costos: "/>
	                        <h:inputText value="#{beanCentroCosto.objActual.copaNvalor5}" style="width: 50px" maxlength="5" required="true" requiredMessage="* CENTRO COSTOS - Campo Requerido" />
							
	                        <h:outputText value="Inicio Vigencia: "/>
	                        <rich:calendar id="fecha_ini" value="#{beanCentroCosto.objActual.copaFvalor1}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* INICIO VIGENCIA - Campo Requerido"/>
	                        
	                        <h:outputText value="Fin Vigencia: "/>
	                        <rich:calendar id="fecha_fin" value="#{beanCentroCosto.objActual.copaFvalor2}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* FIN VIGENCIA - Campo Requerido"/>
	                        	                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanCentroCosto.modificar}" reRender="mensaje,regTabla,dsCentroCosto"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
	     </rich:modalPanel>
    		
    	 <rich:modalPanel id="newPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Nuevo Centro de Costos" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevoCentroCostos">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />
	                    	
	                    	<h:outputText value="Ramo: "/>
	                    	<h:selectOneMenu id="cmbNewRamo" value="#{beanCentroCosto.objNuevo.copaNvalor2}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
							</h:selectOneMenu>
							
	                    	<h:outputText value="Descripcion: "/>
	                    	<h:inputText value="#{beanCentroCosto.objNuevo.copaVvalor1}" style="width: 250px" maxlength="80" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>
							
							<h:outputText value="Centro Costos: "/>
	                    	<h:inputText value="#{beanCentroCosto.objNuevo.copaNvalor5}" style="width: 50px" maxlength="5" required="true" requiredMessage="* CENTRO COSTOS - Campo Requerido"/>
							
							<h:outputText value="Inicio Vigencia :"/>
							<rich:calendar id="fechaInicial" value="#{beanCentroCosto.objNuevo.copaFvalor1}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* INICIO VIGENCIA - Campo Requerido"/>
	                        
							<h:outputText value="Fin Vigencia:"/>
							<rich:calendar id="fechaFinal" value="#{beanCentroCosto.objNuevo.copaFvalor2}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* FIN VIGENCIA - Campo Requerido"/>
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanCentroCosto.guardar}" reRender="mensaje,regTabla,dsCentroCosto"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    		
    		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>