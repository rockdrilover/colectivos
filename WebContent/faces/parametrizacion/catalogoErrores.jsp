<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Catalogo de Errores</title>
	</head>

	<body>
		<h:form id="frmCatalogoErrores">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> II </td>
											<td align="left" width="64%">Cat�logo Errores</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanCatalogoErrores.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    						    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
									
						                    			<td width="13%" align="center">						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanCatalogoErrores.consultar}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consulta Catalogo Errores" />													
														</td>												
						                    		</tr>						                    								                  						                    		
						                    		<tr class="espacio_15"><td>&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanCatalogoErrores.listaCatalogoError == null}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanCatalogoErrores.listaCatalogoError != null}">
															 <rich:contextMenu attached="false" id="menu" submitMode="ajax" 
																	oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
													            <rich:menuItem value="Edit Record" ajaxSingle="true"
													                	oncomplete="#{rich:component('editPanel')}.show()"
													                	actionListener="#{beanCatalogoErrores.buscarFilaActual}">
													                <a4j:actionparam name="clave" value="{clave}" />
													                <a4j:actionparam name="row" value="{filaActual}" />
													            </rich:menuItem>
													        </rich:contextMenu> 
											
															<rich:datascroller for="dsCatErrores" maxPages="15"/>		        													
															<rich:dataTable value="#{beanCatalogoErrores.mapValues}" id="dsCatErrores" var="registro" 
																	rows="15" rowKeyVar="row" ajaxKeys="#{beanCatalogoErrores.keys}" 
																	columns="3" width="90%" align="center">
									      						<f:facet name="header"><h:outputText value="Errores"/></f:facet>
									      						<rich:column title="C�digo Error" width="10%" styleClass="texto_centro">
									      							<f:facet name="header"><h:outputText value=" C�digo Error " title="Error" /></f:facet>
																	<h:outputText value="#{registro.copaNvalor1}"/>  
									      						</rich:column>
														      	<rich:column title="Descripci�n" width="80%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Descripci�n"  title="Descripci�n"/></f:facet>
														      		<h:outputText value="#{registro.copaRegistro}"/>
														      	</rich:column>
													      		<rich:column title="Modificar" width="10%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />                        												
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanCatalogoErrores.objActual}" /> 
                        												<f:setPropertyActionListener value="#{row}" target="#{beanCatalogoErrores.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    				<%--  fn resultados--%>			    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Catalogo de Errores" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
   	
    		<h:form id="catErrores">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1" id="gridEdit1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2" id="gridEdit2">
	                    	<h:outputText value="Codigo Error: " />	                    	
							<h:outputText value="#{beanCatalogoErrores.objActual.copaNvalor1}" styleClass="texto_mediano"/>
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>						
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />							
							
							<h:outputText value="Descripci�n de Error: " /> 							
							<h:inputTextarea  id="Desc" value="#{beanCatalogoErrores.objActual.copaRegistro}" style="width: 250px; height: 120px" required="true" requiredMessage="* Descripcion de error - Campo Requerido"/>
	                    	<rich:toolTip for="Desc" value="No ingresar mas de 500 caracteres ;" />							
				        </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanCatalogoErrores.modificar}" reRender="mensaje,regTabla,dsCatErrores"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
	     </rich:modalPanel>    		    	     	  	    		    		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>