<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title> Coberturas </title>
	</head>

	<body>
		<h:form id="frmCatalogocoberturas">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> I </td>
											<td align="left" width="64%">Cat�logo Coberturas</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanCatalogoCoberturas.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    						    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
														<td width="70%">
															Ramo:&nbsp;&nbsp;
							                    			<h:selectOneMenu value="#{beanCatalogoCoberturas.cartRamosPolizas.carpCdRamo}">
											     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
											     				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
											     			</h:selectOneMenu>												     		
		                   								</td>
						                    			<td width="13%" align="right">						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanCatalogoCoberturas.consultarCartCoberturas}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consulta Centro de Costos" />													
														</td>
						                    		</tr>
						                    		
						                    		<tr class="espacio_15"><td>&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				
				    				<!-- Inicio RESULTADOS -->
				    				
				    			<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
													<c:choose>
														<c:when test="${fn:length(beanCatalogoCoberturas.listaCartCoberturas) == 0}">
															<table width="90%" align="center" border="0">  
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr> 
											                </table> 
													     </c:when>
														<c:otherwise>
																<rich:contextMenu attached="false" id="menu" submitMode="ajax" 
																	oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
													            <rich:menuItem value="Edit Record" ajaxSingle="true"
													                	oncomplete="#{rich:component('editPanel')}.show()"
													                	actionListener="#{beanListaProducto.buscarFilaActual}">
													                <a4j:actionparam name="clave" value="{clave}" />
													                <a4j:actionparam name="row" value="{filaActual}" />
													            </rich:menuItem>
													        </rich:contextMenu>														
														
															<rich:datascroller for="dsCoberturas" maxPages="10"/>
															<rich:dataTable value="#{beanCatalogoCoberturas.mapValues}" id="dsCoberturas" var="registro" 
																	rows="10" rowKeyVar="row" ajaxKeys="#{beanCatalogoCoberturas.keys}" 
																	columns="10" width="90%" align="center">
									      						<f:facet name="header"><h:outputText value="Coberturas "/></f:facet>
									      						
									      						<rich:column title="Ramo Poliza" width="20%" styleClass="texto_centro" sortBy="#{registro.cartRamosPolizas.carpCdRamo}" >
									      							<f:facet name="header"><h:outputText value=" Ramo Poliza" title="Ramo Poliza" /></f:facet>
 																	<h:outputText value="#{registro.cartRamosPolizas.carpCdRamo}"/>  
									      						</rich:column>
									      						
														      	<rich:column title="Clave Cobertura" width="5%" styleClass="texto_centro" sortBy="#{registro.id.cacbCdCobertura}">
														      		<f:facet name="header"><h:outputText value=" Clave Cobertura " title="Descripcion" /></f:facet>
																	<h:outputText value="#{registro.id.cacbCdCobertura}"/>  
														      	</rich:column>
														      	
														      	<rich:column title=" Ramo Contable " width="20%">
														      		<f:facet name="header"><h:outputText value=" Ramo Contable"  title="CentroCosto"/></f:facet>
														      		<h:outputText value="#{registro.id.cacbCarbCdRamo}"/>
														      	</rich:column>
														      	
														      	<rich:column title="Descripcion Cobertura " width="40%">
														      		<f:facet name="header"><h:outputText value="Descripcion Cobertura"  title="Descrbe cobertura"/></f:facet>
														      		<h:outputText value="#{registro.cacbDeCobertura}"/>
																</rich:column>
			   
										     		  			<rich:column title="Modificar " width="5%" styleClass="texto_centro" rendered="#{registro.cacbMtMaximoDolares == 1 }">
										     		  			  <f:facet name="header"><h:outputText value=" Modificar "/></f:facet> 
																   	<c:choose>
																	   	<c:when test="${registro.cacbMtMaximoDolares == 1 }">   
	                    													   No se permite la edicion
			                    										</c:when>
																		<c:otherwise>
																				  <a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
					                        										<h:graphicImage value="../../images/edit_page.png" style="border:0" />
					                        										<f:setPropertyActionListener value="#{registro}" target="#{beanCatalogoCoberturas.objActual}" /> 
					                        										<f:setPropertyActionListener value="#{row}" target="#{beanCatalogoCoberturas.filaActual}" />
					                    										  </a4j:commandLink>
			                    												 
																		</c:otherwise> 	
																	</c:choose>
																	 <rich:toolTip for="editlink" value="Editar" />
		                    									 </rich:column>
																							                	
															</rich:dataTable>
															</c:otherwise>	
														</c:choose>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    					    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="450">
        	<f:facet name="header"><h:outputText value="Editar Cobertura" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	      
   	
    	 <h:form id="editarCobertura">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="5">
	                    	<h:outputText value="Ramo Poliza: "/>
	                    	<h:outputText value="#{beanCatalogoCoberturas.objActual.cartRamosPolizas.carpCdRamo}" styleClass="texto_mediano"/>
							<h:outputText value="      "/>
							<h:outputText value="      "/>
							<h:outputText value="      "/>
							
							<h:outputText value= "Clave Cobertura"/>
							<h:outputText value="#{beanCatalogoCoberturas.objActual.id.cacbCdCobertura}" styleClass="texto_mediano"/>
							<h:outputText value="      "/>
							<h:outputText value="      "/>
							<h:outputText value="      "/>
							
							<h:outputText value="Ramo contable: "/>
	                        <h:outputText value="#{beanCatalogoCoberturas.objActual.cacbCaroCdRamo}" styleClass="texto_mediano"/>
	                        <h:outputText value="      "/>
							<h:outputText value="      "/>
							<h:outputText value="      "/>
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="Descripcion de la cobertura: "/>
	                        <h:inputText value="#{beanCatalogoCoberturas.objActual.cacbDeCobertura}" size="80"/>
	                        <h:outputText value="      "/>
							<h:outputText value="      "/>
							<h:outputText value="      "/>
	                                             	                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanCatalogoCoberturas.modificar}" reRender="mensaje,regTabla,dsCoberturas"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
	     </rich:modalPanel>
    		
      	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>