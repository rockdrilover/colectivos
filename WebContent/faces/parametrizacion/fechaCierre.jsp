<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Fecha de Cierre</title>
	</head>

	<body>
		<h:form id="frmCatalogoFechaCierre">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> II </td>
											<td align="left" width="64%">Cat�logo Fecha de Cierre</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanFechaCierre.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    						    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
														<td>
															<h:outputText id="Anios" value="A�o:" />&nbsp; 
															<h:selectOneMenu value="#{beanFechaCierre.anio}"  required="false" >
																<f:selectItem itemValue="0" itemLabel="Seleccione un a�o"/>
																<f:selectItems value="#{beanFechaCierre.listaComboAnios}"/>														
															</h:selectOneMenu>
														</td>
		                   								<td>
															<h:outputText value="Mes:" />&nbsp; 
															<h:selectOneMenu value="#{beanFechaCierre.mes}">
																<f:selectItem itemValue="0" itemLabel="- Seleccione un mes -" />
																<f:selectItem itemValue="Enero" itemLabel="Enero" />
																<f:selectItem itemValue="Febrero" itemLabel="Febrero" />
																<f:selectItem itemValue="Marzo" itemLabel="Marzo" />
																<f:selectItem itemValue="Abril" itemLabel="Abril" />
																<f:selectItem itemValue="Mayo" itemLabel="Mayo" />
																<f:selectItem itemValue="Junio" itemLabel="Junio" />
																<f:selectItem itemValue="Julio" itemLabel="Julio" />
																<f:selectItem itemValue="Agosto" itemLabel="Agosto" />
																<f:selectItem itemValue="Septiembre" itemLabel="Septiembre" />
																<f:selectItem itemValue="Octubre" itemLabel="Octubre" />
																<f:selectItem itemValue="Noviembre" itemLabel="Noviembre" />
																<f:selectItem itemValue="Diciembre" itemLabel="Diciembre" />
															</h:selectOneMenu>
														</td>
						                    			<td width="13%" align="right">						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanFechaCierre.consultaFechaCierre}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consulta Fecha de Cierre" />													
														</td>
														<td width="4">&nbsp;</td>
														<td width="13%" align="right">
															<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nueva Fecha de Cierre" />									
														</td>
						                    		</tr>						                    								                  						                    		
						                    		<tr class="espacio_15"><td>&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanFechaCierre.listaFechasCierre == null}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanFechaCierre.listaFechasCierre != null}">
															 <rich:contextMenu attached="false" id="menu" submitMode="ajax" 
																	oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
													            <rich:menuItem value="Edit Record" ajaxSingle="true"
													                	oncomplete="#{rich:component('editPanel')}.show()"
													                	actionListener="#{beanFechaCierre.buscarFilaActual}">
													                <a4j:actionparam name="clave" value="{clave}" />
													                <a4j:actionparam name="row" value="{filaActual}" />
													            </rich:menuItem>
													        </rich:contextMenu> 
													        
													        <rich:datascroller for="dsFechasCierre" maxPages="3"/>	
															<rich:dataTable value="#{beanFechaCierre.mapValues}" id="dsFechasCierre" var="registro" 
																	rows="10" rowKeyVar="row" ajaxKeys="#{beanFechaCierre.keys}" 
																	columns="6" width="90%" align="center">
									      						<f:facet name="header"><h:outputText value="Fechas de Cierre"/></f:facet>
									      						<rich:column title="A�o" width="8%" styleClass="texto_centro">
									      							<f:facet name="header"><h:outputText value=" A�o " title="A�o" /></f:facet>
																	<h:outputText value="#{registro.copaNvalor1}"/>  
									      						</rich:column>
														      	<rich:column title="Mes" width="14%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Mes"  title="Mes"/></f:facet>
														      		<h:outputText value="#{registro.copaVvalor2}"/>
														      	</rich:column>
														      	<rich:column title="F.Inicio" width="14%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" F.Inicio " title="F.Inicio" /></f:facet>
																	<h:outputText value="#{registro.copaVvalor3}"/>  
														      	</rich:column>
														      	<rich:column title="F.Fin" width="14%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" F.Fin"  title="F.Fin"/></f:facet>
														      		<h:outputText value="#{registro.copaVvalor4}">														      		
														      		</h:outputText>
														      	</rich:column>
														      	<rich:column title="Estatus" width="40%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="Estatus"  title="Estatus"/></f:facet>
														      		<h:outputText value="#{registro.copaVvalor8}">														      		
														      		</h:outputText>
														      	</rich:column>
														      	
														      	
														      	<rich:column title="Modificar" width="5%" styleClass="texto_centro">
													      			<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()" rendered="#{registro.blnModificar}">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />                        												
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanFechaCierre.objActual}" /> 
                        												<f:setPropertyActionListener value="#{row}" target="#{beanFechaCierre.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />													    			
											                	</rich:column>
														      	
														      	
														      	
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    				<%--  fn resultados--%>			    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Fechas Cierre" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
   	
    		<h:form id="fechaCierre">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1" id="gridEdit1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2" id="gridEdit2">
	                    	<h:outputText value="A�o: " />
	                    	<h:outputText value="#{beanFechaCierre.objActual.copaNvalor1}" styleClass="texto_mediano"/>	                    	
							<h:outputText value="Mes: " />
							<h:outputText value="#{beanFechaCierre.objActual.copaVvalor2}" styleClass="texto_mediano"/>
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />														
							
							<h:outputText value="Estatus: "/>
	                        <h:selectOneMenu id="status" value="#{beanFechaCierre.objActual.copaNvalor2}">	
								<f:selectItem itemValue="-1" itemLabel="Selecione un estatus"/>
								<f:selectItem itemValue="0" itemLabel="Cancelado"/>
								<f:selectItem itemValue="1" itemLabel="Por Ejecutar Cierre Mensual"/>
								<f:selectItem itemValue="2" itemLabel="Cerrado - Ejecuto Cierre Mensual"/>
							</h:selectOneMenu>
							
	                        <h:outputText value="Fecha Inicio : "/>
	                        <rich:calendar id="fecha_ini" value="#{beanFechaCierre.objActual.copaFvalor1}" 
	                        		cellWidth="24px" cellHeight="22px" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true"  styleClass="calendario"/>
	                        
	                        <h:outputText value="Fecha Fin : "/>
	                        <rich:calendar id="fecha_fin" value="#{beanFechaCierre.objActual.copaFvalor2}" 
	                        		cellWidth="24px" cellHeight="22px" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true"  styleClass="calendario"/>
	                        	                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanFechaCierre.modificar}" reRender="mensaje,regTabla,dsFechasCierre"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
	     </rich:modalPanel>
    		
    	 <rich:modalPanel id="newPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Nueva Fecha de Cierre" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevaFechaCierre">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />	                    		                    	
							
	 						<h:outputText value="A�o:" /> 
							<h:selectOneMenu id="nuevoAnio"  value="#{beanFechaCierre.objNuevo.copaNvalor1}"  required="false" >
								<f:selectItem itemValue="0" itemLabel="Seleccione un a�o"/>
								<f:selectItems value="#{beanFechaCierre.listaComboAnios}"/>														
							</h:selectOneMenu>
							
							<h:outputText value="Mes:" /> 
							<h:selectOneMenu  id="mesnuevo" value="#{beanFechaCierre.objNuevo.copaVvalor2}">
								<f:selectItem itemValue="0" itemLabel="- Seleccione un mes -" />
								<f:selectItem itemValue="Enero" itemLabel="Enero" />
								<f:selectItem itemValue="Febrero" itemLabel="Febrero" />
								<f:selectItem itemValue="Marzo" itemLabel="Marzo" />
								<f:selectItem itemValue="Abril" itemLabel="Abril" />
								<f:selectItem itemValue="Mayo" itemLabel="Mayo" />
								<f:selectItem itemValue="Junio" itemLabel="Junio" />
								<f:selectItem itemValue="Julio" itemLabel="Julio" />
								<f:selectItem itemValue="Agosto" itemLabel="Agosto" />
								<f:selectItem itemValue="Septiembre" itemLabel="Septiembre" />
								<f:selectItem itemValue="Octubre" itemLabel="Octubre" />
								<f:selectItem itemValue="Noviembre" itemLabel="Noviembre" />
								<f:selectItem itemValue="Diciembre" itemLabel="Diciembre" />
							</h:selectOneMenu>
							
	                        <h:outputText value="Fecha Inicio : "/>
	                        <rich:calendar id="fecha_ini" value="#{beanFechaCierre.objNuevo.copaFvalor1}" 
	                        		cellWidth="24px" cellHeight="22px" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true"  styleClass="calendario"/>
	                        
	                        <h:outputText value="Fecha Fin : "/>
	                        <rich:calendar id="fecha_fin" value="#{beanFechaCierre.objNuevo.copaFvalor2}" 
	                        		cellWidth="24px" cellHeight="22px" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true"  styleClass="calendario"/>	                        			
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanFechaCierre.guardar}" reRender="mensaje,regTabla,dsFechasCierre"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    		
    		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>