<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 

<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Planes</title>
	</head>

	<body>
		<h:form id="frmCatalogoPlanes">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> I </td>
											<td align="left" width="64%">Cat�logo de Planes</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanPlan.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
														<td width="60%">
															<table width="100%" cellpadding="0" cellspacing="0" align="center">
																<tr class="texto_normal">
																	<td>Ramo:</td>
																	<td width="80%">
										                    			<h:selectOneMenu value="#{beanPlan.id.alplCdRamo}">
														     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
														     				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														     				<a4j:support event="onchange"  action="#{beanPlan.consultarProductos}" ajaxSingle="true" reRender="comboProductos" />
														     			</h:selectOneMenu>												     		
					                   								</td>
						                    					</tr>
						                    					<tr class="espacio_5"><td>&nbsp;</td></tr>
																<tr class="texto_normal">
																	<td>Producto:</td>
																	<td width="80%">
										                    			<h:selectOneMenu value="#{beanPlan.id.alplCdProducto}" id="comboProductos">
														     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
														     				<f:selectItems value="#{beanPlan.listaComboProductos}"/>
														     			</h:selectOneMenu>												     		
					                   								</td>
									                    		</tr>
															</table>
														</td>
														<td width="5%">&nbsp;</td>
														<td width="35%">
															<table width="100%" cellpadding="0" cellspacing="0" align="center">
									                    		<tr class="texto_normal">
									                  				<td>						                    										                    			
																		<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanPlan.consultaPlanes}" 
																						onclick="this.disabled=true" oncomplete="this.disabled=false" 
																					   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
																		<rich:toolTip for="btnConsultar" value="Consultar Planes" />													
																	</td>
																	<td width="4">&nbsp;</td>
																	<td>
																		<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" 
																						 oncomplete="#{rich:component('newPanel')}.show()" />
			                    										<rich:toolTip for="btnNuevo" value="Nuevo Plan" />									
																	</td>
									                    		</tr>
									                    	</table>
									                    </td>
						                    		</tr>
						                    		<tr class="espacio_5"><td>&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center" colspan="2">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:choose>
															<c:when test="${fn:length(beanPlan.listaPlanes) == 0}">
																<table width="90%" align="center" border="0">
																	<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																</table>
															</c:when>
														
															<c:otherwise>															
																<rich:contextMenu attached="false" id="menu" submitMode="ajax" 
																		oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
														            <rich:menuItem value="Edit Record" ajaxSingle="true"
														                	oncomplete="#{rich:component('editPanel')}.show()"
														                	actionListener="#{beanListaProducto.buscarFilaActual}">
														                <a4j:actionparam name="clave" value="{clave}" />
														                <a4j:actionparam name="row" value="{filaActual}" />
														            </rich:menuItem>
														        </rich:contextMenu>
																
																<rich:datascroller for="dsPlan" maxPages="10"/>
																<rich:dataTable value="#{beanPlan.listaPlanes}" id="dsPlan" var="plan" 
																		rows="10" rowKeyVar="row" 
																		columns="10" width="90%" align="center">
										      						<f:facet name="header"><h:outputText value="Planes"/></f:facet>
										      						
															      	<rich:column title="Plan" width="5%" styleClass="texto_centro" sortBy="#{plan.id.alplCdPlan}">
															      		<f:facet name="header"><h:outputText value=" Plan " title="Plan" /></f:facet>
																		<h:outputText value="#{plan.id.alplCdPlan}"/>  
															      	</rich:column>
															      	
															      	<rich:column title="Descripcion" width="40%">
															      		<f:facet name="header"><h:outputText value=" Descripcion "  title="Descripcion"/></f:facet>
															      		<h:outputText value="#{plan.alplDePlan}"/>
															      	</rich:column>
	
															      	<rich:column title="Centro Costos" width="8%">
															      		<f:facet name="header"><h:outputText value=" Centro Costos "  title="Centro de Costos"/></f:facet>
															      		<h:outputText value="#{plan.alplDato3}"/>
															      	</rich:column>
															      	
															      	<rich:column title="Segmento" width="10%">
															      		<f:facet name="header"><h:outputText value=" Segmento "  title="Segmento"/></f:facet>
															      		<h:outputText value="#{plan.alplDato2}"/>
															      	</rich:column>
															      	
															      	<rich:column title="Plazo" width="5%">
															      		<f:facet name="header"><h:outputText value=" Plazo "  title="Plazo"/></f:facet>
															      		<h:outputText value="#{plan.alplDato1}"/>
															      	</rich:column>
															      	
															      	<rich:column title="Num Recibos" width="5%">
															      		<f:facet name="header"><h:outputText value=" Numero Recibos "  title="Numero Recibos"/></f:facet>
															      		<h:outputText value="#{plan.alplDato4}"/>
															      	</rich:column>
					
					
														      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
	                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
	                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()" action="#{beanPlan.cargaCentroDeCostos}">
	                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
	                        												<f:setPropertyActionListener value="#{plan}" target="#{beanPlan.plan}" />
	                        												<f:setPropertyActionListener value="#{row}" target="#{beanListaProducto.filaActual}" />
	                    												</a4j:commandLink>
	                    												<rich:toolTip for="editlink" value="Editar" />
												                	</rich:column>
																</rich:dataTable>
															</c:otherwise>	
														</c:choose>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>				    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="450">
        	<f:facet name="header"><h:outputText value="Editar Plan" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="editarProducto">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	<h:outputText value="Ramo: "/>
	                    	<h:outputText value="#{beanPlan.plan.ramo}" styleClass="texto_mediano"/>
							
							<h:outputText value="Producto: "/>
							<h:outputText value="#{beanPlan.plan.producto}" styleClass="texto_mediano"/>
							
							<h:outputText value="Plan: "/>
							<h:outputText value="#{beanPlan.plan.id.alplCdPlan}" styleClass="texto_mediano"/>
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
	                        <h:outputText value="Descripcion: "/>
	                        <h:inputText value="#{beanPlan.plan.alplDePlan}" style="width: 250px" maxlength="100" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>

	                        <h:outputText value="Centro de Costos: "/>
							<h:selectOneMenu id="cc" value="#{beanPlan.plan.alplDato3}" >
								<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--" />
								<f:selectItems value="#{beanPlan.listaComboCentroCostos}" />
							</h:selectOneMenu>

	                    	<h:outputText value="Segmento: "/>
							<h:selectOneMenu id="segmento" value="#{beanPlan.plan.alplDato2}" >
								<f:selectItem itemValue="" itemLabel="--SELECCIONAR--" />
								<f:selectItems value="#{beanListaParametros.listaComboSegmento}" />
							</h:selectOneMenu>
							
							<h:outputText value="Plazo: "/>
	                        <h:inputText value="#{beanPlan.plan.alplDato1}" style="width: 50px" maxlength="15" required="true" requiredMessage="* PLAZO - Campo Requerido"/>
	                        
	                        <h:outputText value="Numero Recibos (A�o): "/>
	                        <h:inputText value="#{beanPlan.plan.alplDato4}" style="width: 50px" maxlength="15" required="true" requiredMessage="* NUMERO RECIBOS - Campo Requerido"/>
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanPlan.modificar}" reRender="mensaje,regTabla,dsPlan"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	
    	<rich:modalPanel id="newPanel" autosized="true" width="450">
        	<f:facet name="header"><h:outputText value="Nuevo Plan" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevoPlan">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	<h:outputText value="Clave: "/>
	                    	<h:outputText value="#{beanPlan.newPlan.id.alplCdPlan}" styleClass="texto_mediano"/>
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />
	                    
	                    	<h:outputText value="Ramo: "/>
	                    	<h:selectOneMenu id="cmbNewRamo" value="#{beanPlan.id.alplCdRamo}" required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								<a4j:support event="onchange"  action="#{beanPlan.cargaParametros}" ajaxSingle="true" reRender="newComboProductos, cc" />
							</h:selectOneMenu>

	                    	<h:outputText value="Producto: "/>
							<h:selectOneMenu value="#{beanPlan.newPlan.id.alplCdProducto}" id="newComboProductos" required="true" requiredMessage="* PRODUCTO - Campo Requerido">
							<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
							<f:selectItems value="#{beanPlan.listaComboProductos}"/>
							</h:selectOneMenu>												     		
	                    	
	                    	<h:outputText value="Descripcion: "/>
	                    	<h:inputText value="#{beanPlan.newPlan.alplDePlan}" style="width: 250px" maxlength="100" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>
	                        
	                        <h:outputText value="Centro de Costos: "/>
							<h:selectOneMenu id="cc" value="#{beanPlan.newPlan.alplDato3}" >
								<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--" />
								<f:selectItems value="#{beanPlan.listaComboCentroCostos}" />
							</h:selectOneMenu>

							<h:outputText value="Segmento: "/>
							<h:selectOneMenu id="segmento" value="#{beanPlan.newPlan.alplDato2}" >
								<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--" />
								<f:selectItems value="#{beanListaParametros.listaComboSegmento}" />
							</h:selectOneMenu>
	                        
	                        <h:outputText value="Plazo: "/>
	                    	<h:inputText value="#{beanPlan.newPlan.alplDato1}" style="width: 50px" maxlength="15" required="true" requiredMessage="* PLAZO - Campo Requerido"/>
	                    	
	                    	<h:outputText value="Numero Recibos (A�o): "/>
	                        <h:inputText value="#{beanPlan.newPlan.alplDato4}" style="width: 50px" maxlength="15" required="true" requiredMessage="* NUMERO RECIBOS - Campo Requerido"/>
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanPlan.altaPlan}" reRender="mensaje,regTabla,dsPlan"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>