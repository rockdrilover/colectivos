<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Parametrizacion
- Fecha: 09/10/2020
- Descripcion: Pantalla para Configurar Procesos
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />
 		
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallaxii}" /></title>
	</head>
	
	<body onload="muestraOcultaSeccion('confproceso', 1);">	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaxii}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallaxii}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" />
												</td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmConfiguracionProcesos">
									<h:inputHidden value="#{beanConfProcesos.bean}" id="hidLoad" ></h:inputHidden>
									<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td>
														<h:outputText id="mensaje" value="#{beanConfProcesos.strRespuesta}" styleClass="error" />
													</td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<table class="tablaCen90">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secConfProceso" ajaxRendered="true">
																	<span id="img_sec_confproceso_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('confproceso', 0);" title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_confproceso_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('confproceso', 1);" title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemapantallaxii}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<tr class="texto_normal" id="tr_sec_confproceso">
																		<td>
																		<c:if test="${beanConfProcesos.lstProcesos == null}">
																			<table class="tablaCen100">
																				<caption></caption>
																				<tr>
																					<td>NO SE ENCONTRARON PROCESOS CONFIGURADOS.</td>
																				</tr>
																			</table>
																			</c:if> 
																			<c:if test="${beanConfProcesos.lstProcesos != null}">
																				<rich:dataTable value="#{beanConfProcesos.lstProcesos}" id="listapProcesos" var="beanProceso" rows="15" width="100%">
																	      			<f:facet name="header">
																	      				<h:outputText value="#{msgs.confprocesoid}" />																      			
																	      			</f:facet>
																	      		
																		       		<rich:column width="5%" styleClass="texto_centro">
																		       			<f:facet name="header"><h:outputText value="#{msg.confprocesono}" /></f:facet>
																						<h:outputText value="#{beanProceso.copaNvalor1}" />
																					</rich:column>
																		      		<rich:column width="10%" styleClass="texto_centro">
																		      			<f:facet name="header"><h:outputText value="#{msgs.confprocesoid}" /></f:facet>
																						<h:outputText value="#{beanProceso.copaIdParametro}" />																				
																					</rich:column>
																					<rich:column width="15%" styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.confproceso}" /></f:facet>
																						<h:outputText value="#{beanProceso.copaVvalor1}" />																				
																					</rich:column>
																					<rich:column width="15%" styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.confprocesofrecuencia}" /></f:facet>
																						<h:outputText value="#{beanProceso.copaVvalor2}" />
																					</rich:column>
																					<rich:column width="10%" styleClass="texto_centro">
																		      			<f:facet name="header"><h:outputText value="#{msgs.confprocesoejecucion}" /></f:facet>
																						<h:outputText value="#{beanProceso.copaVvalor4}" />																					
																					</rich:column>
																					<rich:column width="35%" styleClass="texto_centro">
																		      			<f:facet name="header"><h:outputText value="#{msgs.confprocesogrupo}" /></f:facet>
																						<h:outputText value="#{beanProceso.copaVvalor33}" />																					
																					</rich:column>
																					<rich:column width="10%" styleClass="texto_centro">
																						<f:facet name="header"><h:outputText value="#{msgs.sistemaeditar}" /></f:facet>																					
																						<a4j:commandLink ajaxSingle="true" 
																								id="editlink" 
																								oncomplete="#{rich:component('editPanel')}.show();muestraOcultaSeccion('confproceso', 1);">
					                        												<h:graphicImage value="../../images/edit_page.png" styleClass="bor0"/>
					                        												<f:setPropertyActionListener value="#{beanProceso}" target="#{beanConfProcesos.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="editlink" value="Editar" />
																					</rich:column>
																					
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listapProcesos" maxPages="10" />
																					</f:facet>
																		    	</rich:dataTable>																			
																			</c:if>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>											
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Configuracion Proceso" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
   	
    		<h:form id="editarConfiguracionProceso">
	            <rich:messages styleClass="errorMessage"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	<h:outputText value="#{msgs.confprocesoid}"/>
	                    	<h:outputText value="#{beanConfProcesos.seleccionado.copaIdParametro}" styleClass="texto_mediano"/>
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="#{msgs.confprocesohora}:"/>
							<h:panelGrid columns="3">
								<h:selectOneMenu id="hora" value="#{beanConfProcesos.seleccionado.copaVvalor31}" >
									<f:selectItems value="#{beanConfProcesos.cmbHoras}" />
								</h:selectOneMenu>
								<h:outputText value=":"/>
								<h:selectOneMenu id="minu" value="#{beanConfProcesos.seleccionado.copaVvalor32}" >
									<f:selectItems value="#{beanConfProcesos.cmbMinutos}" />
								</h:selectOneMenu>
							</h:panelGrid>	

							<h:outputText value="#{msgs.confprocesogrupo}:"/>
	                        <h:selectOneMenu id="grupo" value="#{beanConfProcesos.seleccionado.copaNvalor2}" >
								<f:selectItems value="#{beanConfProcesos.cmbGrupos}" />
							</h:selectOneMenu>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanConfProcesos.modificar}" reRender="mensaje,regTabla,listapProcesos"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();muestraOcultaSeccion('confproceso', 1);" />
	            </h:panelGrid>
	        </h:form>
	     </rich:modalPanel>
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>	
</body>
</html>
</f:view>