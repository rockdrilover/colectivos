<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Carga Cobranza
- Fecha: 10/11/2022
- Descripcion: Pantalla para Carga de cobranza
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.comisionproducto}" /></title>
	</head>		
	
	<body>	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaxv}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.comisionproducto}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmCargaCobranza">
										<h:inputHidden value="#{beanNombreProducto.beanGen.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanNombreProducto.beanGen.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
										                  				<td>						                    										                    			
																			<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanNombreProducto.consultarProducto}" 
																							onclick="this.disabled=true;bloqueaPantalla();" oncomplete="this.disabled=false;desbloqueaPantalla();" 
																						   	reRender="secResultados,regTabla,mensaje" value="Consultar" />																
																			<rich:toolTip for="btnConsultar" value="Consultar Productos Comision" />													
																		</td>
																		<td>
																			<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
				                    										<rich:toolTip for="btnNuevo" value="Nuevo Producto Comision" />									
																		</td>
										                    		</tr>
																	
																	<tr class="espacio_10"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	
																	<tr class="espacio_5"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2" id="loaderProducto"><a4j:status for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen100">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m" style="display: none;">
																		<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultado', 0);" title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultado', 1);" title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones" id="seccionesProducto"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<tr class="texto_normal" id="tr_sec_resultado" style="display:none;">
																		<td>
																			<c:if test="${beanNombreProducto.hmResultados == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																				</table>
																			</c:if>
																			
																			<c:if test="${beanNombreProducto.hmResultados != null}">
																				<rich:datascroller for="dsProducto" maxPages="10"/>
																				<rich:dataTable value="#{beanNombreProducto.mapValues}" id="dsProducto" var="registro" 
																						columnsWidth="10px,50px,15px,15px,10px"
																						rows="10" rowKeyVar="row" ajaxKeys="#{beanNombreProducto.keys}" 
																						columns="5" width="90%" align="center">
														      						<f:facet name="header"><h:outputText value="Nombre Productos"/></f:facet>
														      						
																			      	<rich:column styleClass="texto_centro" sortBy="#{registro.copaIdParametro}" >
																			      		<f:facet name="header"><h:outputText value="#{msgs.sistemaclave}"/></f:facet>
																			      		<h:outputText value="#{registro.copaCdParametro}"/>
																			      	</rich:column>
					
																			      	<rich:column styleClass="texto_centro" sortBy="#{registro.copaVvalor1}">
																			      		<f:facet name="header"><h:outputText value="#{msgs.timbradodesc}"/></f:facet>
																			      		<h:outputText value="#{registro.copaVvalor1}"/>
																			      	</rich:column>
																			      	
																			      	<rich:column styleClass="texto_centro">
																			      		<f:facet name="header"><h:outputText value="#{msgs.sistemafechad}"/></f:facet>
																			      		<h:outputText value="#{registro.copaFvalor1}">
																			      			<f:convertDateTime pattern="dd-MM-yyyy" />
																			      		</h:outputText>
																			      	</rich:column>
																			      	
																			      	<rich:column styleClass="texto_centro">
																			      		<f:facet name="header"><h:outputText value="#{msgs.sistemafechah}"/></f:facet>
																			      		<h:outputText value="#{registro.copaFvalor2}">
																			      			<f:convertDateTime pattern="dd-MM-yyyy" />
																			      		</h:outputText>
																			      	</rich:column>
																			      	
																		      		<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemaeditar}"/></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
					                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
					                        												<f:setPropertyActionListener value="#{registro}" target="#{beanNombreProducto.objActual}" />
					                        											    <f:setPropertyActionListener value="#{row}" target="#{beanListaProducto.filaActual}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="editlink" value="#{msgs.sistemaeditar}" />
																                	</rich:column>
																				</rich:dataTable>
																			</c:if>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Segmento" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="editarSegmento">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	<h:outputText value="Clave: "/>
	                    	<h:outputText value="#{beanNombreProducto.objActual.copaCdParametro}" styleClass="texto_mediano"/>
							
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="Descripcion : "/>
							<h:inputText value="#{beanNombreProducto.objActual.copaVvalor1}" style="width: 250px" maxlength="80" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>

							<h:outputText value="Inicio de Vigencia: "/>
							<rich:calendar id="fechaIni" value="#{beanNombreProducto.objActual.copaFvalor1}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* INICIO VIGENCIA - Campo Requerido"/>
							
	                        <h:outputText value="Fin Vigencia: "/>
	                        <rich:calendar id="fechaFinal" value="#{beanNombreProducto.objActual.copaFvalor2}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* FIN VIGENCIA - Campo Requerido"/>
		                  	                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="#{msgs.sistemaeditar}"
	                    action="#{beanNombreProducto.modificarReg}" reRender="mensaje,regTabla,dsProducto"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	
    	<rich:modalPanel id="newPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Nuevo Nombre Producto" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevoSegmento">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    
	                    	<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />
	                    	
	                    	<h:outputText value="Descripcion: "/>
	                    	<h:inputText value="#{beanNombreProducto.objNuevo.copaVvalor1}" style="width: 250px" maxlength="50" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>
							
							<h:outputText value="Inicio Vigencia :"/>
							<rich:calendar id="fechaInicial" value="#{beanNombreProducto.objNuevo.copaFvalor1}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* INICIO VIGENCIA - Campo Requerido"/>
	                        
							<h:outputText value="Fin Vigencia:"/>
							<rich:calendar id="fechaFinal" value="#{beanNombreProducto.objNuevo.copaFvalor2}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* FIN VIGENCIA - Campo Requerido"/>
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanNombreProducto.altaProducto}" reRender="mensaje,regTabla,dsProducto"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>