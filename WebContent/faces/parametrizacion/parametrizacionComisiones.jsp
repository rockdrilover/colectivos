<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view> 
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallav}" /></title>
	</head>

	<body>
		<h:form id="frmParamComision">
		<div class="center_div"><%@include file="../header.jsp" %></div> 
		<div class="center_div">
			<table class="tablaPrincipal">			
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div class="center_div">
									<table class="encabezadoTabla" >
										<caption></caption>
										<tr>
											<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
											<td class="encabezado2"><h:outputText value="#{msgs.sistemav}" /></td>
											<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallav}" /></td>
											<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
											<td class="encabezado5">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div class="center_div">
				    				<h:inputHidden value="#{beanParamComision.bean}" id="hidLoad" ></h:inputHidden>
				    				<table class="tablaCen90">
				    					<caption></caption>
					    				<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
										<tr>
											<td>
												<h:outputText id="mensaje" value="#{beanParamComision.strRespuesta}" styleClass="error"/>
											</td>
										</tr>	
										<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
				    				</table>
				    						    								    				
				    				<a4j:region id="consultaDatos">
				    				<table class="tablaCen90">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
												</legend>
												
												<table class="tablaCen90">	
													<caption></caption>
													<tr class="texto_normal">
														<td class="right-div">
															<h:outputText value="#{msgs.comisiontp}:" />
														</td>
														<td class="left80">
															<h:selectOneMenu id="inTipoParam" value="#{beanParamComision.tipoParam}">
																<f:selectItem itemValue="0" itemLabel="#{msgs.sistemaseleccione}" />
																<f:selectItem itemValue="1" itemLabel="#{msgs.sistemapoliza}" />
																<f:selectItem itemValue="2" itemLabel="#{msgs.sistemaproducto}" />
																<f:selectItem itemValue="3" itemLabel="#{msgs.sistematarifa}" />
															</h:selectOneMenu>	
														</td>
													</tr>
													<tr class="texto_normal">	
														<td class="right-div">
															<h:outputText value="#{msgs.comisiondesc}" />:
														</td>												
														<td class="left80">
							                    			<h:inputText id="descripcion" value="#{beanParamComision.descripcion}" style="width: 250px" />
		                   								</td>
						                    		</tr>	
						                    		<tr class="texto_normal">
														<td class="right15">
															<h:outputText value="#{msgs.sistemafechad}:" />
														</td>
														<td class="left30">
															<rich:calendar id="fechIni" style="width:100%;" popup="true"
																	value="#{beanParamComision.feDesde}"
																	datePattern="dd/MM/yyyy" styleClass="calendario" />
														</td>
													</tr>
													<tr class="texto_normal">
														<td class="right15">
															<h:outputText value="#{msgs.sistemafechah}:" />
														</td>
														<td class="left30">
															<rich:calendar id="fechFin" style="width:100%;" popup="true"
																	value="#{beanParamComision.feHasta}"
																	datePattern="dd/MM/yyyy" styleClass="calendario" />
														</td>
													</tr>	
						                    		<tr class="texto_normal">
						                    			<td class="right-div" colspan="2">	
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanParamComision.consulta}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="#{msgs.botonconsultar}">
																<f:setPropertyActionListener value="0" target="#{beanParamComision.nAccion}" />
															</a4j:commandButton>																
															<rich:toolTip for="btnConsultar" value="Consulta Reglas" />													
															<a4j:commandButton ajaxSingle="true" value="#{msgs.botonnuevo}" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nueva Regla" />
                    										<a4j:commandButton ajaxSingle="true" value="#{msgs.botoncargar}" styleClass="boton" id="btnCargar" oncomplete="#{rich:component('cargaPanel')}.show()" />
                    										<rich:toolTip for="btnCargar" value="Carga Archivo" />										
														</td>
						                    		</tr>
						                    		
						                    		<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
						                    		<tr>
										    			<td class="center_div" colspan="2">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
				    				</table>
				    				</a4j:region>
				    								    				
				    				<table class="tablaCen90">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table class="tablaCen100">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanParamComision.lstReglas == null}">
															<table class="tablaCen100">
																<caption></caption>
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanParamComision.lstReglas != null}">
															<rich:dataTable value="#{beanParamComision.lstReglas}" id="dsReglas" var="registro" rows="20" width="100%">
									      						<f:facet name="header"><h:outputText value="Reglas Comisiones"/></f:facet>
									      						
														      	<rich:column width="45%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="#{msgs.comisiondesc}"/></f:facet>
														      		<h:outputText value="#{registro.copaVvalor8}"/>
														      	</rich:column>
														      	<rich:column width="10%" styleClass="texto_centro"  title="Comision Total Tecnica Emision">
														      		<f:facet name="header"><h:outputText value="#{msgs.comisionte}"/></f:facet>
																	<h:outputText value="#{registro.copaVvalor31}"/>  
														      	</rich:column>
														      	<rich:column width="10%" styleClass="texto_centro" title="Comision Total Tecnica Renovacion">
														      		<f:facet name="header"><h:outputText value="#{msgs.comisiontr}"/></f:facet>
																	<h:outputText value="#{registro.copaVvalor32}"/>  
														      	</rich:column>
														      	<rich:column width="10%" styleClass="texto_centro" title="Comision Total Comercial Emision">
									      							<f:facet name="header"><h:outputText value="#{msgs.comisionce}"/></f:facet>
																	<h:outputText value="#{registro.copaVvalor33}"/>  
									      						</rich:column>
														      	<rich:column width="10%" styleClass="texto_centro" title="Comision Total Comercial Renovacion">
														      		<f:facet name="header"><h:outputText value="#{msgs.comisioncr}"/></f:facet>
																	<h:outputText value="#{registro.copaRegistro1}"/>  
														      	</rich:column>
														      	<rich:column width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="#{msgs.sistemafechad}"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor1}">
														      			<f:convertDateTime pattern="dd-MM-yyyy" />
														      		</h:outputText>
														      	</rich:column>														      	
													      		<rich:column width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="#{msgs.sistemafechah}"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor2}">
														      			<f:convertDateTime pattern="dd-MM-yyyy" />
														      		</h:outputText>
														      	</rich:column>
													      		<rich:column width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value="#{msgs.sistemaeditar}" /></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" action="#{beanParamComision.setDatosEditar}" oncomplete="#{rich:component('editPanel')}.show()">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />                        												
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanParamComision.objActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
											                	
																<f:facet name="footer">
																	<rich:datascroller align="center" for="dsReglas" maxPages="10" />
																</f:facet>
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    				<%--  fn resultados--%>			    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<%@include file="/WEB-INF/includes/parametrizacion/modalNewParamComision.jsp"%>
		
		<%@include file="/WEB-INF/includes/parametrizacion/modalCargaParamComision.jsp"%>
		
		<%@include file="/WEB-INF/includes/parametrizacion/modalEditParamComision.jsp"%>
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
    	
		<div><%@include file="../footer.jsp" %></div>
    	
	</body>
</html>
</f:view>