<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Timbrado
- Fecha: 09/10/2020
- Descripcion: Pantalla para parametrizar opciones de timbrado por polizas
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%> 


<f:view> 
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallaxvi}" /></title>
	</head>

	<body onload="muestraOcultaSeccion('resultados', 0);">
		<h:form id="frmParamTimbrado">
		<div class="center_div"><%@include file="../header.jsp" %></div> 
		<div class="center_div">
			<table class="tablaPrincipal">			
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div class="center_div">
									<table class="encabezadoTabla" >
										<caption></caption>
										<tr>
											<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
											<td class="encabezado2"><h:outputText value="#{msgs.sistemaxvi}" /></td>
											<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallaxvi}" /></td>
											<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
											<td class="encabezado5">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div class="center_div">
				    				<h:inputHidden value="#{beanParamTimbrado.beanGen.bean}" id="hidLoad" ></h:inputHidden>
				    				<table class="tablaCen90">
				    					<caption></caption>
					    				<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
										<tr>
											<td>
												<h:outputText id="mensaje" value="#{beanParamTimbrado.beanGen.strRespuesta}" styleClass="error"/>
											</td>
										</tr>	
										<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
				    				</table>
				    						    								    				
				    				<a4j:region id="consultaDatos">
				    				<table class="tablaCen90">
									<tr>
										<td>
											<fieldset class="borfieldset">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
												</legend>
												
												<table class="tablaCen90">	
													<caption></caption>
													
		                   							<tr class="texto_normal">
		                   								<td class=left80>
		                   									<h:outputText value="#{msgs.sistemaramo}" />:
		                   									<h:selectOneMenu id="inRamo" 
		                   													value="#{beanParamTimbrado.beanGen.ramo}" 
		                   													binding="#{beanListaParametros.inRamo}">
																<f:selectItem itemValue="0" itemLabel=" --- Selecione un ramo --- " />
																<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																<a4j:support event="onchange" 																		
																		action="#{beanListaParametros.cargaPolizas}" 
																		ajaxSingle="true" 
																		reRender="polizas" 
																		oncomplete="muestraOcultaSeccion('resultados', 0);"/> 
															</h:selectOneMenu>
		                   								</td>
		                   							</tr>
		                   							<tr class="texto_normal">
		                   								<td class="left80">
		                   									<h:outputText value="#{msgs.sistemapoliza}"/>:
		                   									<a4j:outputPanel id="polizas" ajaxRendered="true">
																<h:selectOneMenu id="inPoliza" 
																			value="#{beanParamTimbrado.beanGen.poliza}" 
																			binding="#{beanListaParametros.inPoliza}">
																	<f:selectItem itemValue="-1" itemLabel=" --- Seleccione una póliza --- "/>
																	<f:selectItem itemValue="0" itemLabel="Prima única"/>
																	<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																	<a4j:support event="onchange" 
																			action="#{beanListaParametros.cargaIdVenta}" 
																			ajaxSingle="true" 
																			reRender="idVenta" 
																			oncomplete="muestraOcultaSeccion('resultados', 0);"/>
																</h:selectOneMenu>
															</a4j:outputPanel>
		                   								</td>
		                   							</tr>
		                   							<tr class="texto_normal">
		                   								<td class="left80">
		                   									<h:outputText value="#{msgs.sistemaidventa}"/>:
		                   									<a4j:outputPanel id="idVenta" ajaxRendered="true">
																<h:selectOneMenu id="inVenta"  
																			value="#{beanParamTimbrado.beanGen.idVenta}"
																			disabled="#{beanListaParametros.habilitaComboIdVenta}">
																	<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																	<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																	<a4j:support event="onchange" ajaxSingle="true" reRender="respuestas" oncomplete="muestraOcultaSeccion('resultados', 0);"/>
																</h:selectOneMenu>
															</a4j:outputPanel>
		                   								</td>
		                   							</tr>
		                   							<tr class="texto_normal">
						                    			<td class="encabezado4">						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanParamTimbrado.consulta}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false;muestraOcultaSeccion('resultados', 0);" 
																		   	reRender="secRes,regTabla,mensaje" value="#{msgs.botonconsultar}">
																<f:setPropertyActionListener value="0" target="#{beanParamTimbrado.beanGen.nAccion}" />
															</a4j:commandButton>																
															<rich:toolTip for="btnConsultar" value="Consulta Parametrizacion" />													
														</td>
						                    		</tr>
						                    		
						                    		<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
						                    		<tr>
										    			<td class="center_div">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
				    				</table>
				    				</a4j:region>
				    								    				
				    				<table class="tablaCen90">
									<tr>
										<td>
											<fieldset class="borfieldset">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table class="tablaCen100">	
													<tr class="texto_normal" id="tr_sec_resultados">
														<td>
														<c:if test="${beanParamTimbrado.lstPolizas == null}">
															<table class="tablaCen100">
																<caption></caption>
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanParamTimbrado.lstPolizas != null}">
															<rich:dataTable value="#{beanParamTimbrado.lstPolizas}" id="dsParam" var="registro" rows="15" width="100%">
									      						<f:facet name="header"><h:outputText value="Polizas Parametrizadas"/></f:facet>
									      						
									      						<rich:column width="5%" styleClass="texto_centro">
									      							<f:facet name="header"><h:outputText value="#{msgs.sistemacanal}"/></f:facet>
																	<h:outputText value="#{registro.objParam.copaNvalor1}"/>  
									      						</rich:column>
														      	<rich:column width="5%" styleClass="texto_centro">
									      							<f:facet name="header"><h:outputText value="#{msgs.sistemaramo}"/></f:facet>
																	<h:outputText value="#{registro.objParam.copaNvalor2}"/>  
									      						</rich:column>
									      						<rich:column width="10%" styleClass="texto_centro">
									      							<f:facet name="header"><h:outputText value="#{msgs.sistemapoliza}"/></f:facet>
																	<h:outputText value="#{registro.objParam.copaNvalor3}"/>  
									      						</rich:column>
														      	<rich:column width="5%" styleClass="texto_centro">
									      							<f:facet name="header"><h:outputText value="#{msgs.sistemaidventa}"/></f:facet>
																	<h:outputText value="#{registro.objParam.copaNvalor4}"/>  
									      						</rich:column>
														      	<rich:column width="25%" styleClass="texto_centro"  title="#{registro.razonSocial}">
														      		<f:facet name="header"><h:outputText value="#{msgs.timbradorfc}"/></f:facet>
														      		<h:outputText value="#{registro.rfc}"/>
														      	</rich:column>
														      	<rich:column width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="#{msgs.timbradometpag}"/></f:facet>
																	<h:outputText value="#{registro.objParam.copaVvalor6}"/>  
														      	</rich:column>														      															      
													      		<rich:column width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value="#{msgs.sistemaeditar}" /></f:facet>
                    												<a4j:commandLink ajaxSingle="true" 
                    														id="editlink" 
                    														oncomplete="#{rich:component('editPanel')}.show();muestraOcultaSeccion('resultados', 1);">
                        												<h:graphicImage value="../../images/edit_page.png" styleClass="bor0"/>                        												
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanParamTimbrado.objActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
											                	
																<f:facet name="footer">
																	<rich:datascroller align="center" for="dsParam" maxPages="10" />
																</f:facet>
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    				<%--  fn resultados--%>			    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<%@include file="/WEB-INF/includes/parametrizacion/modalEditParamTimbrado.jsp"%>
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
    	
		<div><%@include file="../footer.jsp" %></div>
    	
	</body>
</html>
</f:view>