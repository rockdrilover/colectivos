<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Parametrizaci�n Beneficiario</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla">
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">|||</td>
							<td align="left" width="64%">Parametrizaci�n de Obligado</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7" />
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmParamObligado">
				    <div align="center">
					    <rich:spacer height="15"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    <a4j:region id="consultaDatos">
								    <table>
								    <tbody>
								    	<tr>
								    		<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
								    		<td align="left" width="80%">
												<h:selectOneMenu id="inRamo" value="#{beanObligado.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														<a4j:support event="onchange"  action="#{beanListaParametros.cargaProductos}" ajaxSingle="true" reRender="producto" />
													</h:selectOneMenu>
												<h:message for="inRamo" styleClass="errorMessage" />
											</td>
							     		</tr>
							     		
							     		
							     			<tr>
							     			<td align="right" width="20%"><h:outputText value="Producto:"/></td>
											<td align="left" width="80%">
												<a4j:outputPanel id="producto" ajaxRendered="true">
													<h:selectOneMenu id="inProducto"  value="#{beanObligado.inProducto}"  binding="#{beanListaParametros.inProducto}" title="header=[Producto] body=[Seleccione un producto] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
															<f:selectItem itemValue="" itemLabel="Seleccione un producto"/>
															<f:selectItems value="#{beanListaParametros.listaComboProductos}"/>
															<a4j:support event="onchange"  action="#{beanListaParametros.cargaPlanes}" ajaxSingle="true" reRender="plan" /> 
													</h:selectOneMenu>
													<h:message for="inProducto" styleClass="errorMessage" />
												</a4j:outputPanel>
											</td>
							     		</tr> 
							     		
							     		
							     		<tr>
							     			<td align="right" width="20%"><h:outputText value="Plan:"/></td>
											<td align="left" width="80%">
												<a4j:outputPanel id="plan" ajaxRendered="true">
													<h:selectOneMenu id="inPlan"  value="#{beanObligado.inPlan}"  binding="#{beanListaParametros.inPlan}" title="header=[Plan] body=[Seleccione un plan] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
															<f:selectItem itemValue="" itemLabel="Seleccione un plan"/>
															<f:selectItems value="#{beanListaParametros.listaComboPlan}"/>
															<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" /> 
													</h:selectOneMenu>
													<h:message for="inPlan" styleClass="errorMessage" />
												</a4j:outputPanel>
											</td>
							     		</tr> 
						     		
							     		
							     		<tr>
							     			<td align="right" width="20%"><h:outputText value="Poliza:"/></td>
											<td align="left" width="80%">
												<a4j:outputPanel id="polizas" ajaxRendered="true">
													<h:selectOneMenu id="inPoliza"  value="#{beanObligado.inPoliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
															<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
															<f:selectItem itemValue="0" itemLabel="Prima �nica carga"/>
															<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
															<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta" />
													</h:selectOneMenu>
													<h:message for="inPoliza" styleClass="errorMessage" />
												</a4j:outputPanel>
											</td>
							     		</tr>
									
							     		<tr>
								    		<td align="right" width="20%" ><h:outputText value="Canal de Venta:"/></td>
											<td align="left" width="50%" >
												<a4j:outputPanel id="idVenta" ajaxRendered="true">
													<h:selectOneMenu id="inVenta"  value="#{beanObligado.inIdVenta}" binding="#{beanListaParametros.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
													         disabled="#{beanListaParametros.habilitaComboIdVenta}">
															<f:selectItem itemValue="1" itemLabel="Prima Recurrente"/>
															<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
													</h:selectOneMenu>
												</a4j:outputPanel>
											</td>
								    	</tr>    	
								    	
								    	<tr> 
									        <td align="right" width="20%" height="35"><h:outputText value="Total de Obligados:"/></td>
									         <td align="left" width="50%" height="30">
								     			<h:inputText id="obligados"  value="#{beanObligado.inNumObligados}" required="true" />
								     			<h:message for="obligados" styleClass="errorMessage" />		 
									        </td>
							     		  </tr>
								    	
								    	<tr>
											<td align="right" width="20%">
											<td align="left" width="50%" height="30">
										       <h:commandButton value="Guardar" action="#{beanObligado.guardarObligado}" style=" width : 162px;"/>
									        </td>
										</tr>	
										
										<tr>                                                                                                                                                                                                                             
											<td align="center" colspan="2" >                                                                                                                                                                                               
			                                    	<h:outputText id="res"  value="#{beanObligado.respuesta}" styleClass="importante1"/>									                                                                                                   
											</td>                                                                                                                                                                                                                    
										</tr>	        	
 
							     	</tbody>     		         		    		
								    </table> 
								    
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>	
								</td>							    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="15px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>