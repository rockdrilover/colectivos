<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Configuración de Conciliación</title>
</head>
<body>
<f:view>
	<!-- Desde aqui comienzo -->
	<f:loadBundle
		basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
		var="msgs" />
	<div align="center"><%@include file="../header.jsp"%>
	</div>
	<div align="center">
	<table class="tablaPrincipal">
		<tbody>
			<tr>
				<td align="center">
				<table class="tablaSecundaria">


					<tbody>
						<tr>
							<td>
							<div align="center">
							<table class="encabezadoTabla">
								<tbody>
									<tr>
										<td width="10%" align="center">Secci&oacute;n</td>
										<td width="4%" align="center" bgcolor="#d90909">||||</td>
										<td align="left" width="64%">Configuración de Conciliación</td>
										<td align="right" width="12%">Fecha</td>
										<td align="left" width="10%"><input name="fecha"
											readonly="readonly"
											value="<%=request.getSession().getAttribute("fecha")%>"
											size="7"></td>
									</tr>
								</tbody>
							</table>
							</div>
							<rich:spacer height="10px"></rich:spacer> 
							<rich:dragIndicator id="indicador" /> 
							
								<div align="center">
								
								<a4j:region id="consulta">
									
<!-- --------------------- INICIO BLOQUE CON CONTENIDO 1 --------------------- -->
									<table class="botonera">
										<tbody>
											<!-- Rows superiores para formato Top Left, Top Center, Top Right -->
											<tr>
												<td class="frameTL"></td>
												<td class="frameTC"></td>
												<td class="frameTR"></td>
											</tr>
											
											<!-- ---------- INICIO ROWS CENTRALES donde va el contenido Center Left, Center, Center Right ---------- -->
											<tr>
												<td class="frameCL"></td>
												<td class="frameC">
							
												<%@ include file="/WEB-INF/includes/parametrizacion/paramConciliacion.jsp"%>
                                           
												</td>
												<td class="frameCR"></td>
											</tr>
											<!-- --------- FIN ROWS CENTRALES ---------- -->
											
											<!-- Rows inferiores para formato Buttom Left, Buttom Center, Buttom Right -->
											<tr>
												<td class="frameBL"></td>
												<td class="frameBC"></td>
												<td class="frameBR"></td>
											</tr>
										</tbody>
									</table>
<!-- --------------------- FIN BLOQUE CON CONTENIDO 1 --------------------- -->
									
									<br />
									

									
									


									<br />

								</a4j:region>
							</div>
							
							</td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	<rich:spacer height="10px"></rich:spacer>
	<div align="center"><%@include file="../footer.jsp"%>
	</div>
</f:view>
</body>
</html>