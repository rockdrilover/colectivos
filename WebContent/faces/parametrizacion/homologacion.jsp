<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 

<f:view>  <!-- Desde aqui comienzo -->
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Homologacion Banco-Seguros</title>
	</head>
	
	<body>
	<h:form id="frmHomologacion">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> I </td>
											<td align="left" width="64%">Homologacion Banco Seguros</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanHomologacion.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
														<td width="60%">
															<table width="100%" cellpadding="0" cellspacing="0" align="center">
																<tr class="texto_normal">
																	<td>Ramo:</td>
																	<td width="80%">
										                    			<h:selectOneMenu value="#{beanHomologacion.inputRamo}">
														     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
														     				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														     				<a4j:support event="onchange"  action="#{beanHomologacion.consultarProductos}" ajaxSingle="true" reRender="comboProductos" />
														     			</h:selectOneMenu>												     		
					                   								</td>
						                    					</tr>
						                    					<tr class="espacio_5"><td>&nbsp;</td></tr>
																<tr class="texto_normal">
																	<td>Producto:</td>
																	<td width="80%">
										                    			<h:selectOneMenu value="#{beanHomologacion.inputProducto}" id="comboProductos">
														     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
														     				<f:selectItems value="#{beanHomologacion.listaComboProductos}"/>
														     				<a4j:support event="onchange"  action="#{beanHomologacion.consultaPlanes}" ajaxSingle="true" reRender="comboPlanes" />
														     			</h:selectOneMenu>												     		
					                   								</td>
									                    		</tr>
									                    		<tr class="espacio_5"><td>&nbsp;</td></tr>
																<tr class="texto_normal">
																	<td>Plan:</td>
																	<td width="80%">
										                    			<h:selectOneMenu value="#{beanHomologacion.inputPlan}" id="comboPlanes">
														     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
														     				<f:selectItems value="#{beanHomologacion.listaComboPlanes}"/>
														     			</h:selectOneMenu>												     		
					                   								</td>
									                    		</tr>
									                    		<tr class="espacio_5"><td>&nbsp;</td></tr>
																<tr class="texto_normal">
																	<td>Subpro Banco:</td>
																	<td width="80%">
										                    			<h:inputText value="#{beanHomologacion.inputSubproBanco}" style="width: 60px" maxlength="6" />											     		
					                   								</td>
									                    		</tr>
															</table>
														</td>
														<td width="5%">&nbsp;</td>
														<td width="35%">
															<table width="100%" cellpadding="0" cellspacing="0" align="center">
									                    		<tr class="texto_normal">
									                    			<td>						                    										                    			
																		<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanHomologacion.consultar}" 
																						onclick="this.disabled=true" oncomplete="this.disabled=false" 
																					   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
																		<rich:toolTip for="btnConsultar" value="Consultar Homologacion" />													
																	</td>
									                    		</tr>
									                  			<tr class="espacio_5"><td>&nbsp;</td></tr>	
									                  			<tr class="texto_normal">
									                  				<td>
																		<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" 
																						 oncomplete="#{rich:component('newPanel')}.show()" />
			                    										<rich:toolTip for="btnNuevo" value="Nueva Homologacion" />									
																	</td>
									                  			</tr>
									                  			<tr class="espacio_5"><td>&nbsp;</td></tr>
									                  			<tr class="texto_normal">
									                  				<td>
																		<a4j:commandButton ajaxSingle="true" value="Carga Lay Out" styleClass="boton" id="btnLayOut" 
																						 oncomplete="#{rich:component('newPanelArchivo')}.show()" />
			                    										<rich:toolTip for="btnLayOut" value="Carga Lay Out" />									
																	</td>
									                  			</tr>	
									                    	</table>
									                    </td>
						                    		</tr>
						                    		
						                    		<tr class="espacio_15"><td>&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>

												<a4j:outputPanel id="regTabla" ajaxRendered="true">												
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanHomologacion.hmResultados == null}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanHomologacion.hmResultados != null}">
															<rich:contextMenu attached="false" id="menu" submitMode="ajax" 
																	oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
													            <rich:menuItem value="Edit Record" ajaxSingle="true"
													                	oncomplete="#{rich:component('editPanel')}.show()"
													                	actionListener="#{beanHomologacion.buscarFilaActual}">
													                <a4j:actionparam name="clave" value="{clave}" />
													                <a4j:actionparam name="row" value="{filaActual}" />
													            </rich:menuItem>
													        </rich:contextMenu>
															
															<rich:datascroller for="dsHomologacion" maxPages="10"/>
															<rich:dataTable value="#{beanHomologacion.mapValues}" id="dsHomologacion" var="registro" 
																	rows="20" rowKeyVar="row" ajaxKeys="#{beanHomologacion.keys}" 
																	columns="7" width="90%" align="center">
									      						<f:facet name="header"><h:outputText value="Productos"/></f:facet>
									      						
									      						<rich:column title="Plan Seguros" width="20%" styleClass="texto_centro" >
									      							<f:facet name="header"><h:outputText value=" P. Seguros " title="Plan Seguros" /></f:facet>
																	<h:outputText value="#{registro.plan.alplDePlan}"/>  
									      						</rich:column>
									      						
									      						<rich:column title="Producto Banco" width="4%" styleClass="texto_centro" sortBy="#{registro.id.hoppProdBancoRector}" >
									      							<f:facet name="header"><h:outputText value=" P. Banco " title="Producto Banco" /></f:facet>
																	<h:outputText value="#{registro.id.hoppProdBancoRector}"/>  
									      						</rich:column>
									      						
														      	<rich:column title="Subproducto Banco" width="4%" styleClass="texto_centro" sortBy="#{registro.id.hoppPlanBancoRector}">
														      		<f:facet name="header"><h:outputText value=" S. Banco " title="Subproducto Banco" /></f:facet>
																	<h:outputText value="#{registro.id.hoppPlanBancoRector}"/>  
														      	</rich:column>
														      	
														      	<rich:column title="Descripcion" width="40%">
														      		<f:facet name="header"><h:outputText value=" Descripcion "  title="Descripcion"/></f:facet>
														      		<h:outputText value="#{registro.hoppDeProducto}"/>
														      	</rich:column>
														      	
														      	<rich:column title="Tarifa" width="8%">
														      		<f:facet name="header"><h:outputText value=" Tarifa "  title="Tarifa"/></f:facet>
														      		<h:outputText value="#{registro.hoppDato1}"/>
														      	</rich:column>
														      	
														      	<rich:column title="Id Venta" width="4%">
														      		<f:facet name="header"><h:outputText value=" Id Venta "  title="Id Venta"/></f:facet>
														      		<h:outputText value="#{registro.hoppDato3}"/>
														      	</rich:column>
														      	
													      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanHomologacion.objActual}" />
                        												<f:setPropertyActionListener value="#{row}" target="#{beanHomologacion.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>												
											</fieldset>
										</td>
									</tr>
				    				</table>				    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
	</h:form>
	<rich:spacer height="10px"></rich:spacer>

	<div><%@include file="../footer.jsp" %></div>
		
	<rich:modalPanel id="editPanel" autosized="true" width="550">
       	<f:facet name="header"><h:outputText value="Editar Homologacion" /></f:facet>
        <f:facet name="controls">
            <h:panelGroup>
                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
            </h:panelGroup>
        </f:facet>
        <h:form id="editarHomologacion">
            <rich:messages style="color:red;"></rich:messages>
            <h:panelGrid columns="1">
                <a4j:outputPanel ajaxRendered="true">
                    <h:panelGrid columns="2">
                    	<h:outputText value="Ramo: "/>
                    	<h:outputText value="#{beanHomologacion.objActual.plan.ramo}" styleClass="texto_mediano"/>
                    	
                    	<h:outputText value="Producto: "/>
						<h:outputText value="#{beanHomologacion.objActual.plan.producto}" styleClass="texto_mediano"/>
						
						<h:outputText value="Plan: "/>
						<h:outputText value="#{beanHomologacion.objActual.plan}" styleClass="texto_mediano"/>
						
						<h:outputText value="Prod Banco: "/>
						<h:outputText value="#{beanHomologacion.objActual.id.hoppProdBancoRector}" styleClass="texto_mediano"/>
						
						<h:outputText value="SubProd Banco: "/>
						<h:outputText value="#{beanHomologacion.objActual.id.hoppPlanBancoRector}" styleClass="texto_mediano"/>
						
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double"/>
						
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						
                    	<h:outputText value="Descripcion: "/>
                    	<h:inputText value="#{beanHomologacion.objActual.hoppDeProducto}" style="width: 250px" maxlength="100" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>
                    	
                    	<h:outputText value="Tarifa: "/>
                    	<h:inputText value="#{beanHomologacion.objActual.hoppDato1}" style="width: 50px" maxlength="10" required="true" requiredMessage="* TARIFA - Campo Requerido"/>
                    	
                    </h:panelGrid>
                </a4j:outputPanel>
                <a4j:commandButton 
                	value="Modificar"
                    action="#{beanHomologacion.modificar}" reRender="mensaje,regTabla,dsHomologacion"	                    
                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
            </h:panelGrid>
        </h:form>
   	</rich:modalPanel>
    	
   	<rich:modalPanel id="newPanel" autosized="true" width="450">
       	<f:facet name="header"><h:outputText value="Nuevo Homologacion" /></f:facet>
        <f:facet name="controls">
            <h:panelGroup>
                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
            </h:panelGroup>
        </f:facet>
        <h:form id="nuevoHomologacion">
            <rich:messages style="color:red;"></rich:messages>
            <h:panelGrid columns="1" >
                <a4j:outputPanel id="panelNew" ajaxRendered="true">
					<h:panelGrid columns="2">
                    	<h:outputText value="Clave: "/>
                    	<h:outputText value=" "/>
						
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double"/>
						
                    	<rich:spacer height="10px" />
						<rich:spacer height="10px" />
                    	
                    	<h:outputText value="Ramo: "/>
                    	<h:selectOneMenu id="cmbNewRamo" value="#{beanHomologacion.objNuevo.id.hoppCdRamo}" required="true" requiredMessage="* RAMO - Campo Requerido">
		     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
		     				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
		     				<a4j:support oncomplete="javascript:exibirPanel(11);" event="onchange"  action="#{beanHomologacion.consultarProductos}" ajaxSingle="true" reRender="comboProductos, panelNew" />
		     			</h:selectOneMenu>	
                    	
                    	<h:outputText value="Producto: "/>
                    	<h:selectOneMenu value="#{beanHomologacion.objNuevo.id.hoppProdColectivo}" required="true" requiredMessage="* PRODUCTO - Campo Requerido" id="comboProductos">
		     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
		     				<f:selectItems value="#{beanHomologacion.listaComboProductos}"/>
		     				<a4j:support oncomplete="javascript:exibirPanel(#{beanHomologacion.nTipoCarga});" event="onchange"  action="#{beanHomologacion.consultaPlanes}" ajaxSingle="true" reRender="comboPlanes, panelNew" />
		     			</h:selectOneMenu>
														
						<h:outputText value="Plan: "/>														     				
		     			<h:selectOneMenu value="#{beanHomologacion.objNuevo.id.hoppPlanColectivo}" required="true" requiredMessage="* PLAN - Campo Requerido" id="comboPlanes">
		     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
		     				<f:selectItems value="#{beanHomologacion.listaComboPlanes}"/>
		     			</h:selectOneMenu>
		     			
		     			<h:outputText value="Prod Banco: "/>
                    	<h:inputText value="#{beanHomologacion.objNuevo.id.hoppProdBancoRector}" style="width: 40px" maxlength="6" required="true" requiredMessage="* PROD BANCARIO - Campo Requerido"/>
                    	
                    	<h:outputText value="Subprod Banco: "/>
                    	<h:inputText value="#{beanHomologacion.objNuevo.id.hoppPlanBancoRector}" style="width: 40px" maxlength="6" required="true" requiredMessage="* SUBPROD BANCARIO - Campo Requerido"/>
                    	
                    	<h:outputText value="Descripcion: "/>
                    	<h:inputText value="#{beanHomologacion.objNuevo.hoppDeProducto}" style="width: 250px" maxlength="100" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>
                    	
                    	<h:outputText value="Tarifa: "/>
                    	<h:inputText value="#{beanHomologacion.objNuevo.hoppDato1}" style="width: 50px" maxlength="10" required="true" requiredMessage="* TARIFA - Campo Requerido"/>
                    	
                    	</h:panelGrid>
                </a4j:outputPanel>
                <a4j:commandButton 
                	value="Guardar"
				    action="#{beanHomologacion.guardar}" reRender="mensaje,regTabla,dsHomologacion"	                    
				    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
            </h:panelGrid>
        </h:form>
   	</rich:modalPanel>
   	
   	<rich:modalPanel id="newPanelArchivo" autosized="true" width="450">
       	<f:facet name="header"><h:outputText value="Carga Lay Out" /></f:facet>
        <f:facet name="controls">
            <h:panelGroup>
                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNewL" styleClass="hidelink" />
                <rich:componentControl for="newPanelArchivo" attachTo="hidelinkNewL" operation="hide" event="onclick" />
            </h:panelGroup>
        </f:facet>
        <h:form id="nuevoHomologacionArchivo">
            <rich:messages style="color:red;"></rich:messages>
           	<h:panelGrid columns="1">
              	<rich:panel id="paVentas" styleClass="paneles">
		        	<f:facet name="header"><h:outputText value="Seleccione archivo a carga" /></f:facet>
		            
		            <h:panelGrid columns="1">																							          
						<rich:fileUpload fileUploadListener="#{beanHomologacion.listener}"
		    					maxFilesQuantity="5" id="loadArchivo"  
	   							addControlLabel="Examinar" uploadControlLabel="Cargar"  
	   							stopControlLabel="Detener" immediateUpload="true" 
	   							clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
	   							styleClass="archivo" acceptedTypes="csv,txt"
	   							listHeight="100px" addButtonClass="botonArchivo" listWidth="250px" >
	   					<a4j:support event="onuploadcomplete" reRender="mensaje" />
		    			<f:facet name="label">
		    				<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
		    			</f:facet>                      
		    			</rich:fileUpload>
					</h:panelGrid>
				</rich:panel>
			</h:panelGrid>
            <a4j:commandButton 
             	value="Guardar"
	    		action="#{beanHomologacion.guardarArchivo}" reRender="mensaje,regTabla,dsHomologacion"	                    
	    		oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanelArchivo')}.hide();" />
        </h:form>
   	</rich:modalPanel>
   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
   	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
       	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
       	<h:graphicImage value="../../images/reloj.png"/>
       	<h:outputText value="    Por favor espere..." />
   	</rich:modalPanel>
	</body>
</html>
</f:view>