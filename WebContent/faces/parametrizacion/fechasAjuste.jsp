<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Fecha de Ajuste</title>
	</head>

	<body>
		<h:form id="frmFechasAjuste">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> II </td>
											<td align="left" width="64%">Cat�logo Fecha de Ajuste</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanFechasAjuste.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    						    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
														<td width="60%">
															<table width="100%" cellpadding="0" cellspacing="0" align="center">
																<tr class="texto_normal">
																	<td>Ramo:</td>
																	<td width="80%">
																		<h:selectOneMenu value="#{beanFechasAjuste.inRamo}" binding="#{beanListaParametros.inRamo}">
																			<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
																			<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																			<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" /> 
																			
																		</h:selectOneMenu>
																		<h:message for="inRamo" styleClass="errorMessage" />
																	</td>
																</tr>
						                    					<tr class="espacio_5"><td>&nbsp;</td></tr>
																<tr class="texto_normal">
																	<td>Poliza:</td>
																	<td width="80%">
																		<a4j:outputPanel id="polizas" ajaxRendered="true">
																			<h:selectOneMenu value="#{beanFechasAjuste.inPoliza}" binding="#{beanListaParametros.inPoliza}" >
																					<f:selectItem itemValue="-1" itemLabel="Seleccione una p�liza"/>
																					<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																					
																			</h:selectOneMenu>
																			<h:message for="inPoliza" styleClass="errorMessage" />
																		</a4j:outputPanel>
																	</td>
																</tr>
															</table>
														</td>
														<td width="5%">&nbsp;</td>
														<td width="35%">
															<table width="100%" cellpadding="0" cellspacing="0" align="center">
																<tr class="texto_normal">
									                    			<td>						                    										                    			
																		<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanFechasAjuste.consultaFechasAjuste}" 
																						onclick="this.disabled=true" oncomplete="this.disabled=false" 
																					   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
																		<rich:toolTip for="btnConsultar" value="Consulta Fecha de Ajuste" />													
																	</td>
																</tr>
																<tr class="espacio_5"><td>&nbsp;</td></tr>
																<tr class="texto_normal">
																	<td>
																		<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
			                    										<rich:toolTip for="btnNuevo" value="Nueva Fecha de Ajuste" />									
																	</td>
																</tr>
															</table>
														</td>
													</tr>						                    								                  						                    		
						                    		<tr class="espacio_15"><td>&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanFechasAjuste.listaCentroCosto == null}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanFechasAjuste.listaCentroCosto != null}">
															 <rich:contextMenu attached="false" id="menu" submitMode="ajax" 
																	oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
													            <rich:menuItem value="Edit Record" ajaxSingle="true"
													                	oncomplete="#{rich:component('editPanel')}.show()"
													                	actionListener="#{beanFechasAjuste.buscarFilaActual}">
													                <a4j:actionparam name="clave" value="{clave}" />
													                <a4j:actionparam name="row" value="{filaActual}" />
													            </rich:menuItem>
													        </rich:contextMenu> 
													        													
															<rich:dataTable value="#{beanFechasAjuste.mapValues}" id="dsCentroCosto" var="registro" 
																	rows="20" rowKeyVar="row" ajaxKeys="#{beanFechasAjuste.keys}" 
																	columns="10" width="90%" align="center">
									      						<f:facet name="header"><h:outputText value="Catalogo Fecha de Ajuste"/></f:facet>
									      						<rich:column title="Canal" width="10%" styleClass="texto_centro">
									      							<f:facet name="header"><h:outputText value=" Canal " title="Canal" /></f:facet>
																	<h:outputText value="#{registro.copaNvalor1}"/>  
									      						</rich:column>
														      	<rich:column title="Ramo" width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Ramo"  title="Ramo"/></f:facet>
														      		<h:outputText value="#{registro.copaNvalor2}"/>
														      	</rich:column>
														      	<rich:column title="Poliza" width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Poliza " title="Poliza" /></f:facet>
																	<h:outputText value="#{registro.copaNvalor3}"/>  
														      	</rich:column>
														      	<rich:column title="DiasGracia" width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Dias de Gracia"  title="Dias de Gracia"/></f:facet>
														      		<h:outputText value="#{registro.copaNvalor4}">														      		
														      		</h:outputText>
														      	</rich:column>
														      	<rich:column title="ProxEjecProceso" width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Proxima Ejecucion Proceso de Ajuste"  title="Proxima Ejecucion Proceso de Ajuste"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor1}">
														      			<f:convertDateTime pattern="dd/MM/yyyy"/>														      		
														      		</h:outputText>
														      	</rich:column>
														      	<rich:column title="DifPrima" width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="Diferencia Prima"  title="Diferencia Prima"/></f:facet>
														      		<h:outputText value="#{registro.copaNvalor6}">														      		
														      		</h:outputText>
														      	</rich:column>
													      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />                        												
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanFechasAjuste.objActual}" /> 
                        												<f:setPropertyActionListener value="#{row}" target="#{beanFechasAjuste.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    				<%--  fn resultados--%>			    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Catalogo" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
   	
    		<h:form id="fechasAjuste">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1" id="gridEdit1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2" id="gridEdit2">
	                    	<h:outputText value="Ramo: " />	
							<h:outputText value="#{beanFechasAjuste.objActual.copaNvalor2}" styleClass="texto_mediano"/>														
              	
              				<h:outputText value="Poliza: "/>
              				<h:outputText value="#{beanFechasAjuste.objActual.copaNvalor3}" styleClass="texto_mediano"/>
              				
              				<h:outputText value="Ejecucion Proceso de Ajuste: "/>
              				<h:outputText value="#{beanFechasAjuste.objActual.copaFvalor1}" styleClass="texto_mediano"/>
              				
							
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
						
							<h:outputText value="Dias de Gracia: "/>
							<h:inputText id="inDiaGracia" value="#{beanFechasAjuste.objActual.copaNvalor4}" />
							<h:outputText value="Diferencia Prima: "/>
							<h:inputText id="inDifPrima" value="#{beanFechasAjuste.objActual.copaNvalor6}"/>
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanFechasAjuste.modificar}" reRender="mensaje,regTabla,dsCentroCosto"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
	     </rich:modalPanel>
    		
    	     	 <rich:modalPanel id="newPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Nuevo Ajuste" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        
	        <h:form id="nuevaFechaAjuste">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />	                    		                    	
							
	 						<h:outputText value="Ramo:" /> 

							<h:selectOneMenu value="#{beanFechasAjuste.newRamo}" >
								<f:selectItem itemValue="0" itemLabel="Seleccione un Ramo"/>
								<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>	
								<a4j:support event="onchange"  action="#{beanFechasAjuste.cargaPolizas}" ajaxSingle="true" reRender="polizas" />													
							</h:selectOneMenu>

							<h:outputText value="Poliza:" />

							<h:selectOneMenu value="#{beanFechasAjuste.newPoliza}"  > 
								<f:selectItem itemValue="-1" itemLabel="Selecione una poliza"/>
								<f:selectItems value="#{beanFechasAjuste.listaComboPolizas}"/>
							</h:selectOneMenu> 
							
							<h:outputText value="Dias de Gracia: "/>
							<h:inputText id="inDiaGracia"  value="#{beanFechasAjuste.objNuevo.copaNvalor4}" required="true"/>
	                        
	                        <h:outputText value="Diferencia Prima : "/>
	                        <h:inputText id="inDifPrima"  value="#{beanFechasAjuste.objNuevo.copaNvalor6}" required="true"/> 
	                        		
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
 	                	value="Guardar"
 	                    action="#{beanFechasAjuste.guardar}" reRender="mensaje,regTabla,dsCentroCosto"	                     
 	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" /> 
 	            </h:panelGrid> 
 	        </h:form> 
	        
    	</rich:modalPanel>
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    		
    		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>