<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Lista de Contratantes</title>
	</head>

	<body>
		<h:form id="frmCatalogoContratantes">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> I </td>
											<td align="left" width="64%">Cat�logo de Contratantes</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanContratante.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="texto_normal">
														<td width="80%" colspan="3">
															Raz&oacute;n Social :&nbsp;&nbsp;
							                    			<h:selectOneMenu value="#{beanContratante.id.cacnNuCedulaRif}" id="comboContra">
											     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
											     				<f:selectItems value="#{beanContratante.listaComboContratantes}"/>
											     			</h:selectOneMenu>												     		
		                   								</td>
		                   							</tr>
		                   							<tr class="espacio_10"><td>&nbsp;</td></tr>
		                   							<tr class="texto_normal">	
		                   								<td width="60%">&nbsp;</td>	                   							
						                  				<td width="13%" align="right">						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanContratante.consultar}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consultar Contratante" />													
														</td>
														<td width="13%" align="right">
															<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nuevo Plan" />									
														</td>
						                    		</tr>
													<tr class="espacio_15"><td colspan="3">&nbsp;</td></tr>
						                    		<tr>
										    			<td align="center" colspan="3">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:choose>
														<c:when test="${fn:length(beanContratante.listaContratantes) == 0}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:when>
														
														<c:otherwise>															
															<rich:datascroller for="dsContratante" maxPages="10"/>
															<rich:dataTable value="#{beanContratante.listaContratantes}" id="dsContratante" var="contratante" 
																	rows="10" rowKeyVar="row" 
																	columns="10" width="100%" align="center">
									      						<f:facet name="header"><h:outputText value="Contratantes"/></f:facet>
									      						
									      						<rich:column title="Raz&oaccute;n Social" width="20%" styleClass="texto_centro" sortBy="#{contratante.cartCliente.cacnNmApellidoRazon}" >
									      							<f:facet name="header"><h:outputText value=" Razon Social " title="Razon Social" /></f:facet>
																	<h:outputText value="#{contratante.cartCliente.cacnNmApellidoRazon}"/>  
									      						</rich:column>
 														      	  
														      	<rich:column title="RFC" width="5%" styleClass="texto_centro" sortBy="#{contratante.cartCliente.cacnRfc}">
														      		<f:facet name="header"><h:outputText value=" RFC " title="RFC" /></f:facet>
																	<h:outputText value="#{contratante.cartCliente.cacnRfc}"/>  
														      	</rich:column>
														      	
														      	<rich:column title="Constitucion" width="8%" styleClass="texto_centro" sortBy="#{contratante.cartCliente.cacnFeNacimiento}">
														      		<f:facet name="header"><h:outputText value=" Constitucion " title="Fecha Constitucion" /></f:facet>
																	<h:outputText value="#{contratante.cartCliente.cacnFeNacimiento}"/>  
														      	</rich:column>
														      	
														      	<rich:column title="Direccion" width="30%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Direccion "  title="Direccion"/></f:facet>
														      		<h:outputText value="#{contratante.cartCliente.cacnDiCobro1}"/>
														      	</rich:column>

														      	<rich:column title="Colonia" width="10%">
														      		<f:facet name="header"><h:outputText value=" Colonia "  title="Colonia"/></f:facet>
														      		<h:outputText value="#{contratante.cartCliente.cacnCazpColoniaCob}"/>
														      	</rich:column>

														      	<rich:column title="CP" width="10%">
														      		<f:facet name="header"><h:outputText value=" CP "  title="CP"/></f:facet>
														      		<h:outputText value="#{contratante.cartCliente.cacnZnPostalCobro}"/>
														      	</rich:column>
														      	
														      	<rich:column title="Ciudad" width="10%">
														      		<f:facet name="header"><h:outputText value=" Ciudad "  title="Ciudad"/></f:facet>
														      		<h:outputText value="#{contratante.ciudad}"/>
														      	</rich:column>
														      	
														      	<rich:column title="Estado" width="10%">
														      		<f:facet name="header"><h:outputText value=" Estado "  title="Estado"/></f:facet>
														      		<h:outputText value="#{contratante.estado}"/>
														      	</rich:column>
														      	
													      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()" action="#{beanContratante.consultaCiudad}" reRender="editarContratante">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
                        												<f:setPropertyActionListener value="#{contratante.cartCliente}" target="#{beanContratante.cartCliente}" />
                        												<f:setPropertyActionListener value="#{row}" target="#{beanListaProducto.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>

															</rich:dataTable>
														</c:otherwise>	
														</c:choose>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>				    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="500">
        	<f:facet name="header"><h:outputText value="Editar Contratante" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="editarContratante">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	
	                    	<h:outputText value="Nacionalidad: "/>
	                    	<h:outputText value="#{beanContratante.cartCliente.id.cacnCdNacionalidad}" styleClass="texto_mediano"/>
	 						
	 						<h:outputText value="RIF: "/>
	                    	<h:outputText value="#{beanContratante.cartCliente.id.cacnNuCedulaRif}" styleClass="texto_mediano"/>
	                    	
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="Raz�n Social: "/>
	                    	<h:inputText value="#{beanContratante.cartCliente.cacnNmApellidoRazon}" style="width: 350px" maxlength="100" required="true" requiredMessage="* RAZON SOCIAL - Campo Requerido"/>
	                    	
							<h:outputText value="RFC: "/>
							<h:inputText value="#{beanContratante.cartCliente.cacnRfc}" style="width: 80px" maxlength="15" required="true" requiredMessage="* RFC - Campo Requerido"/>
							
	                        <h:outputText value="Fecha de Constitucion: "/>
	                        <rich:calendar id="fecha_cons" value="#{beanContratante.cartCliente.cacnFeNacimiento}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* FECHA CONSTITUCION - Campo Requerido"/>
	                        
	                        <h:outputText value="Direccion: "/>
	                        <h:inputText value="#{beanContratante.cartCliente.cacnDiCobro1}" style="width: 350px" maxlength="140" required="true" requiredMessage="* DIRECCION - Campo Requerido"/>

	                        <h:outputText value="Colonia: "/>
	                        <h:inputText value="#{beanContratante.cartCliente.cacnCazpColoniaCob}" style="width: 150px" maxlength="150" required="true" requiredMessage="* COLONIA - Campo Requerido"/>

	                        <h:outputText value="CP: "/>
	                        <h:inputText value="#{beanContratante.cartCliente.cacnZnPostalCobro}" style="width: 50px" maxlength="6" required="true" requiredMessage="* COLONIA - Campo Requerido"/>

							<h:outputText value="Estado: "/>
	                        <h:selectOneMenu id="inEstado"  value="#{beanContratante.cartCliente.cacnCaesCdEstadoHab}" required="true" requiredMessage="* ESTADO - Campo Requerido">
								<f:selectItem itemValue="" itemLabel="<Seleccione Estado>"/>
								<f:selectItems value="#{beanListaEstado.listaComboEstados}"/>
								<a4j:support event="onchange"  action="#{beanContratante.consultaCiudad}" reRender="inCiudad"/>
							</h:selectOneMenu>
							
	                        <h:outputText value="Ciudad: "/>
							<h:selectOneMenu id="inCiudad"  value="#{beanContratante.cartCliente.cacnCaciCdCiudadHab}" required="false" >
								<f:selectItem itemValue="" itemLabel="<Seleccione Ciudad>"/>
								<f:selectItems value="#{beanContratante.listaComboCiudades}"/>
							</h:selectOneMenu>
	                        	                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanContratante.modificar}" reRender="mensaje,regTabla,dsContratante"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	
    	<rich:modalPanel id="newPanel" autosized="true" width="450">
        	<f:facet name="header"><h:outputText value="Nuevo Contratante" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevoContratante">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
							<h:outputText value="Nacionalidad: "/>
	                    	<h:outputText value="#{beanContratante.newCliente.id.cacnCdNacionalidad}" styleClass="texto_mediano"/>
	 						
	 						<h:outputText value="RIF: "/>
	                    	<h:outputText value="#{beanContratante.newCliente.id.cacnNuCedulaRif}" styleClass="texto_mediano"/>
	 						
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="Raz�n Social: "/>
	                    	<h:inputText value="#{beanContratante.newCliente.cacnNmApellidoRazon}" style="width: 350px" maxlength="100" required="true" requiredMessage="* RAZON SOCIAL - Campo Requerido"/>
	                    	
							<h:outputText value="RFC: "/>
							<h:inputText value="#{beanContratante.newCliente.cacnRfc}" style="width: 80px" maxlength="15" required="true" requiredMessage="* RFC - Campo Requerido"/>
							
	                        <h:outputText value="Fecha de Constitucion: "/>
	                        <rich:calendar id="fecha_cons2" value="#{beanContratante.newCliente.cacnFeNacimiento}" 
	                        		cellWidth="24px" cellHeight="22px" style="width:200px" required="true" datePattern="dd/MM/yyyy"
	                        		inputSize="10" popup="true" requiredMessage="* FECHA CONSTITUCION - Campo Requerido"/>
	                        
	                        <h:outputText value="Direccion: "/>
	                        <h:inputText value="#{beanContratante.newCliente.cacnDiCobro1}" style="width: 350px" maxlength="140" required="true" requiredMessage="* DIRECCION - Campo Requerido"/>

	                        <h:outputText value="Colonia: "/>
	                        <h:inputText value="#{beanContratante.newCliente.cacnCazpColoniaCob}" style="width: 150px" maxlength="150" required="true" requiredMessage="* COLONIA - Campo Requerido"/>

	                        <h:outputText value="CP: "/>
	                        <h:inputText value="#{beanContratante.newCliente.cacnZnPostalCobro}" style="width: 50px" maxlength="6" required="true" requiredMessage="* COLONIA - Campo Requerido"/>

	                        <h:outputText value="Estado: "/>
	                        <h:selectOneMenu id="inEstado2"  value="#{beanContratante.newCliente.cacnCaesCdEstadoHab}" required="true" requiredMessage="* ESTADO - Campo Requerido">
								<f:selectItem itemValue="" itemLabel="<Seleccione Estado>"/>
								<f:selectItems value="#{beanListaEstado.listaComboEstados}"/>
								<a4j:support event="onchange"  action="#{beanContratante.consultaCiudad}" reRender="inCiudad2"/>
							</h:selectOneMenu>
							
							<h:outputText value="Ciudad: "/>
							<h:selectOneMenu id="inCiudad2"  value="#{beanContratante.newCliente.cacnCaciCdCiudadHab}" required="false" >
								<f:selectItem itemValue="" itemLabel="<Seleccione Ciudad>"/>
								<f:selectItems value="#{beanContratante.listaComboCiudades}"/>
							</h:selectOneMenu>								             
	                       
						</h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanContratante.guardar}" reRender="mensaje,regTabla,dsContratante,comboContra"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>