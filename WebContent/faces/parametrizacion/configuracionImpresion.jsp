<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<script type="text/javascript" src="../../js/boxover.js"></script>
	<title>Configuraci�n de Impresi�n</title>
</head>
<body>
	<div align="center">
		<%@include file="../header.jsp" %>
	</div>
	<div align="center">
		<table class="tablaPrincipal">
			<tbody>
				<tr>
					<td align="center">
						<table class="tablaSecundaria">
							<tbody>
								<tr>
									<td align="center">
										<div align="center">
											<table class="encabezadoTabla" >
												<tbody>
													<tr>
														<td width="10%" align="center">Secci�n</td>
														<td width="4%" align="center" bgcolor="#d90909">||||</td>
														<td align="left" width="64%">Configuraci�n de impresi�n</td>
														<td align="right" width="12%" >Fecha</td>
														<td align="left" width="10%">
															<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div align="center">
											<rich:spacer height="15"/>
											<h:form id="frmImpre">
												<table class="botonera">
													<tbody>
														<tr>
															<td class="frameTL"></td>
															<td class="frameTC"></td>
															<td class="frameTR"></td>
														</tr>
														<tr>
															<td class="frameCL"/>
															<td class="frameC" >
																<a4j:region id="filtroCarga">
																	<table border="0" align="center">
																		<tbody>
																			<tr><td align="left" colspan="3" >&nbsp;</td></tr>
																			<tr>
																				<td align="center" colspan="3">
																					<h:outputText id="respuestas" value="#{beanConfiguracionImpresion.strRespuesta}" styleClass="respuesta" />
																				</td>
																			</tr>
																			<tr><td align="left" colspan="3" >&nbsp;</td></tr>
																			<tr>
																				<td align="left">
																					<h:outputText value="Ramo:" />
																				</td>
																				<td align="left">
																					<h:selectOneMenu id="inRamo" value="#{beanConfiguracionImpresion.ramo}" binding="#{beanListaParametros.inRamo}"
																						title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
																						<f:selectItem itemValue="0" itemLabel=" --- Selecione un ramo --- " />
																						<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																						<a4j:support event="onchange" oncomplete="exibirPanelesImpresion(0)" action="#{beanListaParametros.cargaPolizas}" 
																								ajaxSingle="true" reRender="polizas,inProducto,respuestas" /> 
																					</h:selectOneMenu>
																				</td>
																				<td align="left" >&nbsp;</td>
																			</tr>
																			<tr>
																				<td align="left">
																					<h:outputText value="Poliza:"/>
																				</td>
																				<td align="left">
																					<a4j:outputPanel id="polizas" ajaxRendered="true">
																						<h:selectOneMenu id="inPoliza"  value="#{beanConfiguracionImpresion.poliza}" binding="#{beanListaParametros.inPoliza}"
																							title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																							<f:selectItem itemValue="-1" itemLabel=" --- Seleccione una p�liza --- "/>
																							<f:selectItem itemValue="0" itemLabel="Prima �nica"/>
																							<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																							<a4j:support event="onchange" action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta,respuestas" />
																						</h:selectOneMenu>
																					</a4j:outputPanel>
																				</td>
																				<td align="left" >
																					<a4j:commandButton value="Consultar" id="idConsultar" oncomplete="javascript:exibirPanelesImpresion(1, #{beanConfiguracionImpresion.muestra});"
																						action="#{beanConfiguracionImpresion.consultaPlantilla}" reRender="resultado1,respuestas"
																						title="header=[Consultar] body=[Consulta plantillas existentes.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																				</td>
																			</tr>
																			<tr>
																				<td align="left" >
																					<h:outputText value="Canal de Venta:"/>
																				</td>
																				<td align="left" >
																					<a4j:outputPanel id="idVenta" ajaxRendered="true">
																						<h:selectOneMenu id="inVenta"  value="#{beanConfiguracionImpresion.idVenta}"
																							disabled="#{beanListaParametros.habilitaComboIdVenta}"
																							title="header=[Id venta] body=[Seleccione un canal de venta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
																							<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																							<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																							<a4j:support event="onchange" ajaxSingle="true" reRender="respuestas"/>
																						</h:selectOneMenu>
																					</a4j:outputPanel>
																				</td>
																				<td align="left" >
																					<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="idNuevo" oncomplete="javascript:validaDatos(#{rich:component('newPanel')})" 
																									action="#{beanConfiguracionImpresion.getCamposBase}" reRender="idCampos,respuestas,archi,btoCerrarNewPanel, editarEditarNombrePlatilla"	 />
                    																<rich:toolTip for="btnNuevo" value="Nueva Fecha de Cierre" />
																				</td>
																			</tr>
																			<tr align="left" >
								                   						   	    <td align="left">
								                   						   	    	<h:outputText value="Producto: "/>
								                   						   	    </td>
																			    <td align="left">
																			    	<a4j:outputPanel id="inProducto" ajaxRendered="true">
																				    	<h:selectOneMenu id="idProducto" value="#{beanConfiguracionImpresion.nProducto}" 
																				    		title="header=[Producto] body=[Seleccione un Producto.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
																					 		<f:selectItem itemValue="0" itemLabel="--- Selecione un producto ---"/>
																					    	<f:selectItems value="#{beanConfiguracionImpresion.listaProductos}" />
																							<a4j:support event="onchange"  action="#{beanConfiguracionImpresion.consultaPlanes}" ajaxSingle="true" reRender="idPlan,respuestas"/>
																						</h:selectOneMenu>
																			    	</a4j:outputPanel>
																			    </td>
																			    <td align="left" >&nbsp;</td>
																			</tr>
																		    <tr align="left">
																			    <td align="left">
																			    	<h:outputText value="Plan: "/>
																			    </td>
																				<td align="left">
																					<a4j:outputPanel id="idPlan" ajaxRendered="true">
																						<h:selectOneMenu id="inPlan" value="#{beanConfiguracionImpresion.nPlan}"
																							title="header=[Plan] body=[Seleccione un Plan.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
																							<f:selectItem itemValue="0" itemLabel=" --- Seleccione un plan ---"/>
																							<f:selectItems value="#{beanConfiguracionImpresion.listaComboPlanes}"/>
																							<a4j:support event="onchange" ajaxSingle="true" action="#{beanConfiguracionImpresion.limpiaRespuestas}" reRender="respuestas"/>
																						</h:selectOneMenu>
																					</a4j:outputPanel>
																			    </td>
																			    <td align="left" >&nbsp;</td>
																			</tr>
																			<tr><td align="left" colspan="3" >&nbsp;</td></tr>
																			<tr><td align="left" colspan="3" >&nbsp;</td></tr>
																		</tbody>
																	</table>
																	<table id="panelRespuesta" >
																		<tr id="panel1" style="display: none;">
																			<td align="center" colspan="3">
																				<a4j:outputPanel id="resultado1" ajaxRendered="true">
																						<rich:contextMenu attached="false" id="menu1" submitMode="ajax" 
																								oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
																				            <rich:menuItem value="Edit Record" ajaxSingle="true"
																				                	oncomplete="#{rich:component('editPanel')}.show()"
																				                	actionListener="#{beanConfiguracionImpresion.buscarFilaActual}">
																				                <a4j:actionparam name="clave" value="{clave}" />
																				                <a4j:actionparam name="row" value="{filaActual}" />
																				            </rich:menuItem>
																				        </rich:contextMenu>
																						<rich:dataTable id="resultadoConsulta" value="#{beanConfiguracionImpresion.listaConsulta}" var="registro" columns="4" width="90px"
																							rows="50" rowKeyVar="row" ajaxKeys="#{registro.copaVvalor1}">
																							<f:facet name="header"><h:outputText value="Plantillas" /></f:facet>
																							<rich:column width="35%">
																								<f:facet name="header"><h:outputText value="Nombres de Campos del Formulario" /></f:facet>
																								<h:outputText value="#{registro.copaVvalor6}" />
																							</rich:column>
																							<rich:column width="35%">
																								<f:facet name="header"><h:outputText value="Campos de Base de Datos" /> </f:facet>
																								<h:outputText value="#{registro.copaVvalor7}" />
																							</rich:column>
																							<rich:column width="10%">
																								<f:facet name="header"><h:outputText value="Id Campo" /> </f:facet>
																								<h:outputText value="#{registro.copaIdParametro}" />
																							</rich:column>
																							<rich:column width="10%">
																								<f:facet name="header"><h:outputText value="Modificar" /> </f:facet>
																								<a4j:commandLink ajaxSingle="true" id="editlin" oncomplete="#{rich:component('editPanel')}.show()">
							                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
							                        												<f:setPropertyActionListener value="#{registro}" target="#{beanConfiguracionImpresion.objActual}" />
							                        												<f:setPropertyActionListener value="#{row}" target="#{beanConfiguracionImpresion.filaActual}" />
							                    												</a4j:commandLink>
							                    												<rich:toolTip for="editlin" value="Editar" />
																							</rich:column>
																							<f:facet name="footer">
																								<rich:datascroller align="center" for="resultadoConsulta" maxPages="10" page="#{beanConfiguracionImpresion.numPagina}" />
																							</f:facet>
																						</rich:dataTable>
																				</a4j:outputPanel>
																			</td>
																		</tr>
																	</table>
																</a4j:region>
															</td>
															<td class="frameCR" />
														</tr>
														<tr>
															<td class="frameBL"></td>
															<td class="frameBC"></td>
															<td class="frameBR"></td>
														</tr>
													</tbody>
												</table>
												<br/>
											</h:form>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<rich:spacer height="10px"/>
	<div align="center">
		<%@ include file="../footer.jsp" %>
	</div>
	<rich:modalPanel id="editPanel" autosized="true" width="400">
       	<f:facet name="header"><h:outputText value="Editar Campos" /></f:facet>
        <f:facet name="controls">
            <h:panelGroup>
                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
            </h:panelGroup>
        </f:facet>
  	
   		<h:form id="configModificar">
            <rich:messages style="color:red;"></rich:messages>
            <h:panelGrid columns="1" id="gridEdit1">
                <a4j:outputPanel ajaxRendered="true">
                    <h:panelGrid columns="2" id="gridEdit2">
                    	<h:outputText value="Configuracion Impresion: " />
						<h:outputText />
						<rich:separator height="4" lineType="double" />
						<rich:separator height="4" lineType="double"/>
						
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						
						<h:outputText value="Campo del formulario:" />
						<h:inputText value="#{beanConfiguracionImpresion.objActual.copaVvalor2}" />
						
						<h:outputText value="Campo de la base: "/>
                        <h:selectOneMenu id="status" value="#{beanConfiguracionImpresion.objActual.copaVvalor1}">
							<f:selectItem itemValue="0" itemLabel="--- Selecione un producto ---"/>
							<f:selectItems value="#{beanConfiguracionImpresion.listaCampos}" />
						</h:selectOneMenu>
                    </h:panelGrid>
                </a4j:outputPanel>
                <a4j:commandButton
                	value="Modificar"
                    action="#{beanConfiguracionImpresion.modificar}" reRender="resultado2,resultado1,respuestas"
                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
            </h:panelGrid>
        </h:form>
     </rich:modalPanel>
     
     <rich:modalPanel id="newPanel" autosized="true" width="400">
       	<f:facet name="header"><h:outputText value="Nueva Configuracion" /></f:facet>
        <f:facet name="controls">
            <h:panelGroup id="btoCerrarNewPanel">
                <h:graphicImage value="../../images/cerrarModal.png" id="hide" styleClass="hidelink"  />
                <rich:componentControl for="newPanel" attachTo="hide" operation="hide" event="onclick"  />
            </h:panelGroup>
        </f:facet>
  	
   		<h:form id="newModif">
            <rich:messages style="color:red;"></rich:messages>
            <h:panelGrid id="gridNew1">
                <a4j:outputPanel ajaxRendered="true">
                    <h:panelGrid  id="gridNew2">
                    	<h:outputText id="msjPlantilla" value="#{beanConfiguracionImpresion.msjPlantilla}" style="font-size: 10px; font: : Verdana, Helvetica;color: #d90909;margin-bottom:50px" />
                    	<rich:spacer height="10px" />
						<h:outputText value="Nombre del Campo del Formulario:" />
						<h:inputText id="campFormu" value="#{beanConfiguracionImpresion.campoFormulario}" maxlength="100"/>
						<rich:spacer height="10px" />
						<h:outputText value="Campo de Base de Datos:" />
						<a4j:outputPanel id="idCampos" ajaxRendered="true">
					    	<h:selectOneMenu  value="#{beanConfiguracionImpresion.campoBase}" onchange="muestraMsg(this.value);"
					    		title="header=[Campo] body=[Seleccione un Campo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
						 		<f:selectItem itemValue="0" itemLabel="--- Selecione un producto ---"/>
						    	<f:selectItems value="#{beanConfiguracionImpresion.listaCampos}" />
							</h:selectOneMenu>
				    	</a4j:outputPanel>
						<rich:spacer height="10px" />
						<a4j:commandButton value="Agregar" id="idAgregar" disabled="#{!beanConfiguracionImpresion.plantillaExistente}"
							action="#{beanConfiguracionImpresion.agregaTabla}" reRender="resultadoNuevo,respuestas,idTabla,panelAgrega,campFormu,msjPlantilla"
							title="header=[Agregar] body=[Agrega los campos correspondientes a la plantilla.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						<a4j:outputPanel id="resultado2" ajaxRendered="true">
								<rich:dataTable id="resultadoNuevo" value="#{beanConfiguracionImpresion.listaNuevos}" var="registro2" columns="3" width="90px"
									rows="5" rowKeyVar="row" >
									<f:facet name="header"><h:outputText value="Plantilla" /></f:facet>
									<rich:column width="35%">
										<f:facet name="header"><h:outputText value="Nombres de Campos del Formulario" /></f:facet>
										<h:outputText value="#{registro2.copaVvalor6}" />
									</rich:column>
									<rich:column width="35%">
										<f:facet name="header"><h:outputText value="Campos de Base de Datos" /> </f:facet>
										<h:outputText value="#{registro2.copaVvalor7}" />
									</rich:column>
									<rich:column width="10%">
										<f:facet name="header"><h:outputText value="Id Campo" /> </f:facet>
										<h:outputText value="#{registro2.copaIdParametro}" />
									</rich:column>
									<f:facet name="footer">
										<rich:datascroller align="center" for="resultadoNuevo" maxPages="10" page="#{beanConfiguracionImpresion.numPagina}" />
									</f:facet>
								</rich:dataTable>
						</a4j:outputPanel>
						<rich:spacer height="10px" />
						<rich:separator height="4" lineType="double" />
						<h:outputText value="CONFIGURACION DE PLANTILLA" />
						<rich:separator height="4" lineType="double"/>
						<rich:spacer height="10px" />	
					</h:panelGrid>
					<h:panelGrid id="gridNew3" columns="5"  style="display:#{beanConfiguracionImpresion.display1};">
						<h:outputText value="Nombre de Plantilla:" />
						<h:outputText value="" />
						<h:outputText id="archi" value="#{beanConfiguracionImpresion.nombreArchivo}" />
						<a4j:commandButton value="Modificar" id="btnModificarPlantillaModal" 
								oncomplete="#{rich:component('editarEditarNombrePlatilla')}.show();" reRender="editarEditarNombrePlatilla">
								<f:setPropertyActionListener value="#{beanConfiguracionImpresion.nombreArchivo}" 
									target="#{beanConfiguracionImpresion.nuevoNombreArchivo}" />
						</a4j:commandButton>
						<a4j:commandButton value="Borrar" id="btnBorrarPlantilla" 
								action="#{beanConfiguracionImpresion.borrarPlantilla}" 
								reRender="respuestas, idAgregar, gridNew3, gridNew4, btoCerrarNewPanel,msjPlantilla" />
                    </h:panelGrid>
					<h:panelGrid id="gridNew4" columns="5" style="display:#{beanConfiguracionImpresion.display2};">
						<h:outputText value="Nombre de Plantilla:" />
						<h:outputText value="" />
						<h:inputText value="#{beanConfiguracionImpresion.nombreArchivo}" />
						<h:outputText value="" />
						<a4j:commandButton value="Guardar" id="idPlantilla" 
								action="#{beanConfiguracionImpresion.guardarPlantilla}" 
								reRender="respuestas, idAgregar, gridNew3, gridNew4, btoCerrarNewPanel,msjPlantilla" /> 
                    </h:panelGrid>
                </a4j:outputPanel>
            </h:panelGrid>
        </h:form>
     </rich:modalPanel>
     
     
     <rich:modalPanel id="editarEditarNombrePlatilla" autosized="true" >
		       	<f:facet name="header"><h:outputText value="Modificar nombre platilla" /></f:facet>
		        <f:facet name="controls">
		            <h:panelGroup >
		                <h:graphicImage value="../../images/cerrarModal.png" id="hide2" styleClass="hidelink"  />
		                <rich:componentControl for="editarEditarNombrePlatilla" attachTo="hide2" operation="hide" event="onclick"  />
		            </h:panelGroup>
		        </f:facet>
		        <h:form id="plantillaModif">
		        <h:panelGrid id="gridModificaPlantilla" columns="3">
						<h:outputText value="Nombre de Plantilla:" />
						<h:inputText value="#{beanConfiguracionImpresion.nuevoNombreArchivo}" />
						<a4j:commandButton value="Modificar" id="btnModificarPlantilla" 
								action="#{beanConfiguracionImpresion.editarNombrePlantilla}"
								oncomplete="#{rich:component('editarEditarNombrePlatilla')}.hide();" 
								reRender="respuestas, idAgregar, gridNew3, gridNew4, btoCerrarNewPanel,msjPlantilla" />
                    </h:panelGrid>
                    </h:form>
		    </rich:modalPanel>
		    
</body>
</html>
</f:view>