<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Grupos Correo
- Fecha: 09/10/2020
- Descripcion: Pantalla para Configurar Grupos
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%> 


<f:view> 
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallaxiii}" /></title>
	</head>

	<body onload="muestraOcultaSeccion('resultados', 0);">
		<h:form id="frmGrupos">
		<div class="center_div"><%@include file="../header.jsp" %></div> 
		<div class="center_div">
			<table class="tablaPrincipal">			
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div class="center_div">
									<table class="encabezadoTabla" >
										<caption></caption>
										<tr>
											<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
											<td class="encabezado2"><h:outputText value="#{msgs.sistemaxiii}" /></td>
											<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallaxiii}" /></td>
											<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
											<td class="encabezado5">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div class="center_div">
				    				<h:inputHidden value="#{beanGrupos.bean}" id="hidLoad" ></h:inputHidden>
				    				<table class="tablaCen90">
				    					<caption></caption>
					    				<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
										<tr>
											<td>
												<h:outputText id="mensaje" value="#{beanGrupos.strRespuesta}" styleClass="error"/>
											</td>
										</tr>	
										<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
				    				</table>
				    						    								    				
				    				<a4j:region id="consultaDatos">
				    				<table class="tablaCen90">
									<tr>
										<td>
											<fieldset class="borfieldset">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
												</legend>
												
												<table class="tablaCen90">	
													<caption></caption>
													<tr class="texto_normal">
														<td class="wid70por">
															<h:outputText value="#{msgs.gruposnombre}" />:
							                    			<h:inputText id="nombreGpo" value="#{beanGrupos.nombreGrupo}" styleClass="wid250"/>
		                   								</td>
						                    			<td class="encabezado4">						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanGrupos.consultaGrupos}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false;muestraOcultaSeccion('resultados', 0)" 
																		   	reRender="secRes,regTabla,mensaje" value="#{msgs.botonconsultar}">
																<f:setPropertyActionListener value="0" target="#{beanGrupos.nAccion}" />
															</a4j:commandButton>																
															<rich:toolTip for="btnConsultar" value="Consulta Grupos" />													
														</td>
														<td class="wid5por"><h:outputText value="#{msgs.sistemaespacio}" /></td> 
														<td class="encabezado4">
															<a4j:commandButton ajaxSingle="true" value="#{msgs.botonnuevo}" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nuevo Grupo" />									
														</td>
						                    		</tr>
						                    		
						                    		<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
						                    		<tr>
										    			<td class="center_div">
											    			<a4j:status for="consultaDatos" stopText=" "> 
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
														    </a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
				    				</table>
				    				</a4j:region>
				    								    				
				    				<table class="tablaCen90">
									<tr>
										<td>
											<fieldset class="borfieldset">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table class="tablaCen100">	
													<tr class="texto_normal" id="tr_sec_resultados">
														<td>
														<c:if test="${beanGrupos.lstGrupos == null}">
															<table class="tablaCen100">
																<caption></caption>
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
															
														<c:if test="${beanGrupos.lstGrupos != null}">
															<rich:dataTable value="#{beanGrupos.lstGrupos}" id="dsGrupos" var="registro" rows="15" width="100%">
									      						<f:facet name="header"><h:outputText value="Grupos de Correos"/></f:facet>
									      						
									      						<rich:column width="5%" styleClass="texto_centro" >
									      							<f:facet name="header"><h:outputText value="#{msgs.confprocesono}"/></f:facet>
																	<h:outputText value="#{registro.copaNvalor1}"/>  
									      						</rich:column>
														      	<rich:column width="30%" styleClass="texto_centro" sortBy="#{registro.copaVvalor8}">
														      		<f:facet name="header"><h:outputText value="#{msgs.gruposnombre}" title="#{msgs.gruposnombre}"/></f:facet>
														      		<h:outputText value="#{registro.copaVvalor8}"/>
														      	</rich:column>
														      	<rich:column width="50%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="#{msgs.gruposcorreos}" title="#{msgs.gruposcorreos}"/></f:facet>
																	<h:outputText value="#{registro.copaVvalor6}"/>  
														      	</rich:column>
														      	<rich:column width="15%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="#{msgs.gruposfecha}" title="#{msgs.gruposfecha}"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor1}">
														      		<f:convertDateTime pattern="dd-MM-yyyy" />
														      		</h:outputText>
														      	</rich:column>														      	
													      		<rich:column width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value="#{msgs.sistemaeditar}" /></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" action="#{beanGrupos.setListaCorreos}" oncomplete="#{rich:component('editPanel')}.show();muestraOcultaSeccion('resultados', 1)">
                        												<h:graphicImage value="../../images/edit_page.png" styleClass="bor0" />                        												
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanGrupos.objActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
											                	
																<f:facet name="footer">
																	<rich:datascroller align="center" for="dsGrupos" maxPages="10" />
																</f:facet>
															</rich:dataTable>
														</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>	
				    				<%--  fn resultados--%>			    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<%@include file="/WEB-INF/includes/parametrizacion/modalEditGrupo.jsp"%>
		
		<%@include file="/WEB-INF/includes/parametrizacion/modalNewGrupo.jsp"%>
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
    	
		<div><%@include file="../footer.jsp" %></div>
    	
	</body>
</html>
</f:view>