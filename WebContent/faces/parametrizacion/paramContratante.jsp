<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<%@ page import="mx.com.santander.aseguradora.colectivos.view.bean.BeanListaClienteCertif" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:c="http://java.sun.com/jsp/jstl/core">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
    <script type="text/javascript" src="../../js/boxover.js"></script>
    <script type="text/javascript" src="../../js/colectivos.js"></script>
	<title>Parametrizacion Contratante</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>				
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Parametrizacion Contratante</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmconscred">
					    <div align="center">
						    <rich:spacer height="15px"/>	
						    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">
										    <a4j:region id="consultaDatos">
											     <table cellpadding="2" cellspacing="0" border="0">
											    	<tr>
										     			<td align="right" width="40%"><h:outputText value="Cedula:" /></td>
										     			<td align="left" width="40%" colspan="3">
											     		<h:inputText id="ap"  value="#{beanListaContratante.cedula}" />
											     		</td>
										     		</tr>
										     		<tr>
											     		<td align="right" width="40%"><h:outputText value="Raz�n Social:" /></td>
											     		<td align="left" width="40%" colspan="3">
											     		<h:inputText id="nombre"  value="#{beanListaContratante.razonSocial}" />
											     		</td>
										     		</tr>
										     		<tr>
										     			<td align="center" width="80%" colspan="3">
										     				<a4j:commandButton styleClass="boton" value="Consultar" action="#{beanListaContratante.consulta}" onclick="this.disabled=true" oncomplete="this.disabled=false" reRender="listaCreditos" title="header=[Consulta certificados] body=[Consulta los contratantes dependiendo del filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
										     			</td>
										     		<tr>
										     		 <td>
										     		  <rich:spacer height="40px"/>
										     		 </td>
										     		</tr>
										     		<tr>
											    		<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
											    		<td align="left" width="85%" colspan="3">
															<h:selectOneMenu id="inRamo" value="#{beanListaContratante.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione ramo para asociar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
																<f:selectItem itemValue="0" itemLabel="Selecione ramo para asociar" />
																<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVentaRamo}" ajaxSingle="true" reRender="inPoliza" />
															</h:selectOneMenu>
															<h:message for="inRamo" styleClass="errorMessage" />
														</td>
										     		</tr>
										   
										      		<tr>
											    		<td align="right" width="40%"><h:outputText value="Tipo Prima:" /></td>
											    		<td align="left" width="40%" colspan="3">
															<h:selectOneMenu id="inPoliza" value="#{beanListaContratante.tpPoliza}" binding="#{beanListaParametros.inPoliza}" title="header=[Poliza] body=[SeleccioneTipo Poliza para asociar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
																<f:selectItem itemValue="0" itemLabel="Prima Unica" />
																<f:selectItem itemValue="1" itemLabel="Prima Recurrente" /> 
																 <%--<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>--%>
																 <a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVentaRamo}" ajaxSingle="true" reRender="inIdVenta" /> 
															</h:selectOneMenu>
															<h:message for="inPoliza" styleClass="errorMessage" />
														</td>
										     		</tr>
										     		<tr>
											    		<td align="right" width="20%"><h:outputText value="Canal de Venta:" /></td>
											    		<td align="left" width="85%" colspan="3">
															<h:selectOneMenu id="inIdVenta" value="#{beanListaContratante.idVenta}" binding="#{beanListaParametros.inIdVenta}" title="header=[IdVenta] body=[Seleccione Canal de Venta para asociar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
																<f:selectItem itemValue="0" itemLabel="Selecione Canal de Venta para asociar" />
																<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																<%-- <a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="polizas" /> --%>
															</h:selectOneMenu>
															<h:message for="inIdVenta" styleClass="errorMessage" />
														</td>
										     		</tr>
										     		
										     		<tr>
										     			<td></td>
										     			<td></td>
										     			<td align="left">
										     			    <h:commandButton action="#{beanListaContratante.guardar}" value="Guardar" title="header=[Consulta certificados] body=[Guarda la asignacion contratante ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
										     				<%-- <a4j:commandButton styleClass="boton" value="Guardar" action="#{beanListaContratante.guardar}" onclick="this.disabled=true" oncomplete="this.disabled=false" reRender="listaCreditos" title="header=[Consulta certificados] body=[Guarda la asignacion contratante ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" /> --%>
										     			</td>
										     		</tr>	
										     		
					                                <tr>
					                                     <td colspan="3" align="center">
					                                           <br/>
					                                           <h:outputText id="mensaje" value="#{beanListaContratante.respuesta}" styleClass="respuesta"/>
					                                           <br/>
					                                           <br/>
					                                     </td>
					                                </tr>
											    </table>
											    <rich:spacer height="15px"></rich:spacer>
											    <table>
										    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
												</table>
											</a4j:region>								    
									    <td class="frameCR"></td>
									</tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">
									     	<table>
										     	<tr>
										     		<td align="center">
												      	<rich:dataTable id="listaCreditos" value="#{beanListaContratante.listaContratantes}" columns="5" var="beanClienteCertif" rows="20" rowKeyVar="valor">
												      		<f:facet name="header">
												      			<rich:columnGroup>
												      			    <rich:column width="5%"><h:outputText value="Chk" /></rich:column>
												      				<rich:column width="15%"><h:outputText value="Nacionalidad" /></rich:column>
												      				<rich:column width="20%"><h:outputText value="Cedula" /></rich:column>
												      				<rich:column width="40%"><h:outputText value="Raz�n Social" /></rich:column>
												      				<rich:column width="40%" colspan="2"><h:outputText value="Datos Generales" /></rich:column>
												      			</rich:columnGroup>
												      		</f:facet>
												      		<rich:column width="5%" style="text-align:center">
																	<h:selectBooleanCheckbox value="#{beanListaContratante.chkUpdate}" />
															</rich:column>
												      		<rich:column width="15%" style="text-align:center">
												      				<h:outputText value="#{beanListaContratante.nacionalidad}" />
															</rich:column>
												      		<rich:column width="20%" style="text-align:center">
												      				<h:outputText value="#{beanListaContratante.cedula}" />
															</rich:column>
												      		 <rich:column width="40%" style="text-align:center">
												      			<h:outputText value="#{beanListaContratante.razonSocial} " />
															</rich:column > 

		                                                    <rich:column width="20%" style="text-align:center ">
		                                                    	<h:graphicImage onclick="verCteCert(1, #{valor}, #{fn:length(beanListaContratante.listaContratantes)}, #{fn:length(beanListaContratante.listaContratantes)});" value="/images/clientes.png" title="header=[Clientes] body=[Dar click para mostrar detalle del contratante.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
		                                                     </rich:column>
		                                                     
		                                                     
		                                                     
		                                                    <%-- 
		                                                    <rich:column width="20%" style="text-align:center">
		                                                    	<h:graphicImage onclick="verCteCert(2, #{valor}, #{fn:length(beanListaClienteCertif.listaCreditos)}, #{fn:length(beanListaClienteCertif.listaCertificados)});" value="/images/certificados.png" title="header=[Certificados] body=[Dar click para mostrar detalle de los certificados.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
		                                                    </rich:column> --%>
		                                                    
		                                                    <rich:column breakBefore="true" colspan="7" id="colClientes" style="display: none">
																<table align="center" width=100% border="0">
																	<tr>
																		<td colspan="4"  bgcolor="#d90909" align="center">
																			<h:outputText value="Datos Contratante" style="font-weight:bold; color: white" />
																		</td>
																	</tr>
																	<tr>
																		<td colspan="4" width="20">
																		</td>
																	</tr>
																	<tr> 
																		<%-- <td align="left">
																			<h:outputText value="BUC:" style="font-weight:bold; color: black" />
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnBucCliente}" />      		  			
																		</td> --%>
																		<td align="left">
																			<h:outputText value="Direccion:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanListaContratante.direccion}" />
																		</td>
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="Cp:" style="font-weight:bold; color: black" />
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanListaContratante.cp}" >
																			</h:outputText>      		  			
																		</td>
																		<td align="left">
																			<h:outputText value="RFC:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanListaContratante.rfc}" />      		  			
																		</td>
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="Fecha Constitucion:" style="font-weight:bold; color: black" />
							
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanListaContratante.fecha_cons}" />      		  			
																		</td>
																		<td align="left">
																			<h:outputText value="Del/Mun:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanListaContratante.ciudad}" />
																    </td>  		  			
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="Estado:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanListaContratante.estado}" />      		  			
																		</td>
																	</tr>	  			
																</table>
		                                                    </rich:column> 
					
												      		<f:facet name="footer">
												      			<rich:datascroller align="right" for="listaCreditos" maxPages="10" page="#{beanListaContratantes.numPagina}"/>
												      		</f:facet>
												      	</rich:dataTable> 
												    </td>
											     </tr>
											</table>
										<td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
						    </table>
						</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>
