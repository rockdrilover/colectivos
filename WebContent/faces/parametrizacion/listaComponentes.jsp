<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 


<f:view>  <!-- Desde aqui comienzo --> 
<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<title>Componentes</title>
	</head>

	<body>
		<h:form id="frmCatalogoComponentes">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> I </td>
											<td align="left" width="64%">Cat�logo de Componentes</td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div align="center">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanListaComponentes.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				
				    				<!-- Inicio ACCIONES -->
				    				<a4j:region id="consultaDatos">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
						                    		<tr class="espacio_5"><td>&nbsp;</td></tr>
						                    		<tr class="texto_normal">
						                  				<td>						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanListaComponentes.consultar}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consultar Planes" />													
														</td>
														<td>
															<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nuevo Componente" />									
														</td>
						                    		</tr>
						                    		
													<tr class="espacio_15"><td>&nbsp;</td></tr>						                    		
						                    		<tr>
										    			<td align="center" colspan="2">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    				</a4j:region>
				    				
				    				
				    				<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:choose>
														<c:when test="${fn:length(beanListaComponentes.listaComponentes) == 0}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:when>
														<c:otherwise>
															<rich:datascroller for="dsComponente" maxPages="10"/>
															<rich:dataTable value="#{beanListaComponentes.listaComponentes}" id="dsComponente" var="compo" 
																	rows="10" rowKeyVar="row" 
																	columns="10" width="90%" align="center">
									      						<f:facet name="header"><h:outputText value="Componentes"/></f:facet>
									      						
									      						<rich:column title="Orden" width="5%" styleClass="texto_centro" sortBy="#{compo.copaVvalor2}" >
									      							<f:facet name="header"><h:outputText value=" Orden " title="Orden" /></f:facet>
																	<h:outputText value="#{compo.copaNvalor2}"/>  
									      						</rich:column>
									      						
									      						<rich:column title="Componente" width="10%" styleClass="texto_centro" >
									      							<f:facet name="header"><h:outputText value=" Componente " title="Componente" /></f:facet>
																	<h:outputText value="#{compo.copaVvalor2}"/>  
									      						</rich:column>
														      	  
														      	<rich:column title="Descripcion" width="40%" styleClass="texto_centro" sortBy="#{compo.copaVvalor1}">
														      		<f:facet name="header"><h:outputText value=" Descripcion " title="Descripcion" /></f:facet>
																	<h:outputText value="#{compo.copaVvalor1}"/>  
														      	</rich:column>
														      	
														      	<rich:column title="Porcentaje" width="5%">
														      		<f:facet name="header"><h:outputText value=" Porcentaje "  title="Porcentaje"/></f:facet>
														      		<h:outputText value="#{compo.copaVvalor6}">
														      			<f:convertNumber pattern="#0.00" />
														      		</h:outputText>
														      	</rich:column>

													      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
                        												<f:setPropertyActionListener value="#{compo}" target="#{beanListaComponentes.editComponente}" />
                        												<f:setPropertyActionListener value="#{row}" target="#{beanListaProducto.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
															</rich:dataTable>
														</c:otherwise>	
														</c:choose>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    				</table>				    		
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>

		<div><%@include file="../footer.jsp" %></div>
		
		<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Componente" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="editarComponente">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                    	<h:outputText value="Clave: "/>
	                    	<h:outputText value="#{beanListaComponentes.editComponente.copaCdParametro}" styleClass="texto_mediano"/>
	                    	
	                    	<h:outputText value="Componente: "/>
	                    	<h:outputText value="#{beanListaComponentes.editComponente.copaVvalor2}" styleClass="texto_mediano"/>
	                    	
	                    	<h:outputText value="Orden: "/>
	                    	<h:outputText value="#{beanListaComponentes.editComponente.copaNvalor2}" styleClass="texto_mediano"/>

							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
	                    
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="Descripcion: "/>
							<h:inputText value="#{beanListaComponentes.editComponente.copaVvalor1}" style="width: 250px" maxlength="80" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>

							<h:outputText value="Afecta Calculo de Prima: "/>
							<h:selectOneMenu id="cc" value="#{beanListaComponentes.editComponente.copaNvalor1}" >
								<f:selectItem itemValue="0" itemLabel="0 - No" />
								<f:selectItem itemValue="1" itemLabel="1 - Si" />
							</h:selectOneMenu>
							
	                        <h:outputText value="Porcentaje: "/>
	                        <h:inputText value="#{beanListaComponentes.editComponente.copaVvalor6}" style="width: 50px" maxlength="10" required="true" requiredMessage="* PORCENTAJE - Campo Requerido"/>
	                        	                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanListaComponentes.modificar}" reRender="mensaje,regTabla,dsComponente"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	
    	<rich:modalPanel id="newPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Nuevo Componente" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevoComponente">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />

							<h:outputText value="Componente: "/>
							<h:selectOneMenu id="nComp" value="#{beanListaComponentes.newComponente.copaVvalor2}" >
								<f:selectItem itemValue="1|DPO" itemLabel="1 - DPO" />
								<f:selectItem itemValue="2|RFI" itemLabel="2 - RFI" />
								<f:selectItem itemValue="3|IVA" itemLabel="3 - IVA" />
								<f:selectItem itemValue="4|COM" itemLabel="4 - COM" />
								<f:selectItem itemValue="5|CON" itemLabel="5 - CON" />
							</h:selectOneMenu>

<%--
	                    	<h:inputText id="nComp" value="#{beanListaComponentes.newComponente.copaVvalor2}" style="width: 150px" maxlength="10" required="true" requiredMessage="* COMPONENTE - Campo Requerido"/>
 
	                    	<h:outputText value="Orden: "/>
	                    	<h:inputText id="nOrden" value="#{beanListaComponentes.newComponente.copaNvalor2}" style="width: 50px" maxlength="10" required="true" requiredMessage="* ORDEN - Campo Requerido"/>
--%>
							<h:outputText value="Descripcion: "/>
							<h:inputText id="nDesc" value="#{beanListaComponentes.newComponente.copaVvalor1}" style="width: 250px" maxlength="80" required="true" requiredMessage="* DESCRIPCION - Campo Requerido"/>

<%-- 
							<h:outputText value="Afecta Calculo de Prima: "/>
							<h:selectOneMenu id="cc" value="#{beanListaComponentes.newComponente.copaNvalor1}">
								<f:selectItem itemValue="0" itemLabel="0 - No" />
								<f:selectItem itemValue="1" itemLabel="1 - Si" />
							</h:selectOneMenu>
--%>
	                        <h:outputText value="Porcentaje: "/>
	                        <h:inputText id="nPor" value="#{beanListaComponentes.newComponente.copaVvalor6}" style="width: 50px" maxlength="6" required="true" requiredMessage="* PORCENTAJE - Campo Requerido" validatorMessage="El porcentaje maximo permitido es 9">
	                        	<f:validateDoubleRange maximum="9" />
	                        </h:inputText>

	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanListaComponentes.alta}" reRender="mensaje,regTabla,dsComponente"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
    	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>