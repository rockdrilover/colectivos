<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<title>Consulta de Certificados </title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<table width="800px" bgcolor="#fefefe" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td align="left"><img src="../images/logo.gif" width="245" height="40"></td>
			<td align="right"><img src="../images/logo2.gif" width="211" height="56"></td>
		</tr>
	</tbody>
</table>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
	    <table class="tablaSecundaria">
		<tbody>
			<tr>
				<td align="center">
				<div align="center">
				<table bgcolor="#555242" cellpadding="0" cellspacing="0" class="encabezado">
				<tbody>
					<tr>
						<td bgcolor="#8F8B70" width="10%" align="center">Secci�n</td>
						<td width="4%" align="center" bgcolor="#d90909">|</td>
						<td align="left" width="64%">Conteo Certificados</td>
						<td align="right" width="14%" >Fecha</td>
						<td align="left" width="10%">
							<input name="fecha" readonly="readonly" size="10">
						</td>
					</tr>
				</tbody>
				</table>
			    </div>
				<br>
				<h:form id="frmConsultaCertif">
				<div align="center">
				<table class="tabla" cellpadding="2" cellspacing="0">
				<tbody>
					<tr>
						<td align="right" width="30%"><h:outputText value="Ramo:" /></td>
						<td align="left">
							<h:selectOneMenu id="ramo" value="#{beanCertificadosLista.ramo}" binding="#{beanListaRamo.inRamo}" required="true">
								<f:selectItem itemValue="0" itemLabel="Ramo"/>
								<f:selectItems value="#{beanListaRamo.beansRamos}"/>
								<f:param name="ramo"/>
								<a4j:support event="onchange" action="#{beanListaRamo.getCertificadoH}" ajaxSingle="true" reRender="polizas"/>
						 	</h:selectOneMenu>
							<h:message for="ramo" styleClass="errorMessage"/>
						</td>  
						<td></td>
					</tr>
					<tr>
						<td align="right"><h:outputText value="Poliza:"/></td>
						<td align="left">
							<a4j:outputPanel id="polizas" ajaxRendered="true">
								<h:selectOneMenu id="poliza"  value="#{beanCertificadosLista.poliza}" required="true">
									<f:selectItem itemValue="" itemLabel="Seleccione una poliza"/>
									<f:selectItems value="#{beanListaRamo.listaCertificadoH}"/>
								</h:selectOneMenu>
								<h:message for="poliza" styleClass="errorMessage"/>
							</a4j:outputPanel>
						</td>
						<td align="left">
						    <h:commandButton action="#{beanCertificadosLista.getCuenta}" value="Consulta" />
						</td>
					</tr>														
				</tbody>
				</table>
				</div>											
				<div align="center">
				<table class="tabla" cellpadding="10" cellspacing="0" border="0">
				<tbody>
				<tr>
					<td align="center">
					<rich:dataTable value="#{beanCertificadosLista.listaCertificadosCount}" var="beanCertificadosCount">
						<f:facet name="header">
							<h:outputText value="Consulta de Certificados por estatus" /> 
						</f:facet>
						<rich:column>
						<f:facet name="header">
							<h:outputText value="Total certificados" /> 
						</f:facet>    
						<h:outputText value="#{beanCertificadosCount.cuentaCertif}" />  			
						</rich:column>
						<rich:column>
							<f:facet name="header">
								<h:outputText value="Estatus" /> 
							</f:facet>    
							<h:outputText value="#{beanCertificadosCount.descripcion}" />      		  			
						</rich:column>      		
					</rich:dataTable>
					</td>
				</tr>
				</tbody>
				</table>
				</div>
				</h:form>
				<h:messages styleClass="errorMessage" globalOnly="true" style="COLOR: #000000;"/>			
				</td>
			</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
</div>
<div align="center">
<%@ include file="footer.jsp" %>
</div>
</f:view>
</body>
 </html>