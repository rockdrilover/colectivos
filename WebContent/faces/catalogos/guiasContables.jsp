<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<title>Guias Contables</title>
		<script type="text/javascript">
	    function muestraGuia(){
 
	    	document.getElementById("tablaCoberturas").style.display = "block";
 
	    }
	    
	    function ocultaGuia(){
	    	 
	    	document.getElementById("tablaCoberturas").style.display = "none";
 
	    }
	    
	    
	  </script>
	</head>

	<body>
	<f:view>  <!-- Desde aqui comienzo --> 
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
		<table class="tablaPrincipal">
		<tbody>
			<tr>
				<td align="center">
					<table class="tablaSecundaria">
					<tbody>
						<tr>
							<td>
								<div align="center">
								<table class="encabezadoTabla" >
								<tbody>
									<tr>
										<td width="10%" align="center">Sección</td>
										<td width="4%" align="center" bgcolor="#d90909">||||</td>
										<td align="left" width="64%">Configuración guias contables</td>
										<td align="right" width="12%" >Fecha</td>
										<td align="left" width="10%">
											<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
										</td>
									</tr>
								</tbody>
								</table>
				    			</div>	
				    
				    			<rich:spacer height="10px"></rich:spacer>
				    			<h:form id="frmGuiaContable">
				    				<div align="center">
					    			<rich:spacer height="15"/>	
					    			<table class="botonera">
					    			<tbody>
						    			<tr>
										    <td class="frameTL"></td>
										    <td class="frameTC"></td>
										    <td class="frameTR"></td>
									    </tr>
						    			<tr>
										    <td class="frameCL"></td>
										    <td class="frameC" width="600px">
							   					<a4j:region id="consultaDatos">
												    <table cellpadding="0" cellspacing="0">
												    	<tr>
												    		<td width="80%">
												    			<table width="100%">
												    			<tr><td>&nbsp;</td></tr>
												    				<tr>
															    		<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
															    		<td align="left" width="80%">
																			<h:selectOneMenu id="inRamo" value="#{beanGuiasContables.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
																				<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
																				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																				<a4j:support event="onchange"  action="#{beanGuiasContables.cargaProductos}" ajaxSingle="true" reRender="selProducto, inPlanc" />
																			</h:selectOneMenu>
																			<h:message for="inRamo" styleClass="errorMessage" />
																		</td>
														     		</tr>
														     		
														     		<tr><td>&nbsp;</td></tr>
														     		
														     		<tr>
															    		<td align="right" width="20%"><h:outputText value="Producto:" /></td>
															    		<td align="left" width="80%">
																    		<%-- <a4j:outputPanel id="productos" ajaxRendered="true"> --%>
																    			<h:selectOneMenu id="selProducto"  value="#{beanGuiasContables.producto}" binding="#{beanListaParametros.inProducto}" title="header=[Producto] body=[Seleccione un producto.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																					<f:selectItem itemValue="0" itemLabel="Selecione un producto" />
																					<f:selectItems value="#{beanGuiasContables.listaProductos}" />
																					<a4j:support event="onchange"  action="#{beanGuiasContables.cargarPlanes}" ajaxSingle="true" reRender="inPlan" />
																				</h:selectOneMenu>
																			   <h:message for="inProducto" styleClass="errorMessage" />	
															    			<%-- </a4j:outputPanel> --%>
																		</td>
														     		</tr>
														     		
														     		<tr><td>&nbsp;</td></tr>
														     		
														     		<tr>
															    		<td align="right" width="20%"><h:outputText value="Plan:" /></td>
															    		<td align="left" width="80%">
																    		<%-- <a4j:outputPanel id="plan" ajaxRendered="true">--%>
																    			<h:selectOneMenu id="inPlan" value="#{beanGuiasContables.plan}" required="true" title="header=[Plan] body=[Seleccione un plan.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																					<f:selectItem itemValue="0" itemLabel="Selecione un plan" />
																					<f:selectItems value="#{beanGuiasContables.listaPlanes}" />
																				</h:selectOneMenu>
																			   <h:message for="inPlan" styleClass="errorMessage" />	
															    			<%--</a4j:outputPanel>--%>
																		</td>
														     		</tr>
														     		
														     		<tr><td>&nbsp;</td></tr>
														     		
														     		<tr>							     		
										     							<td align="center" colspan="2">
										     								<a4j:commandButton styleClass="boton" value="Mostrar Coberturas" id="botonCoberturas"
																			   action="#{beanGuiasContables.consultar}" 
 																			   onclick="ocultaGuia()" oncomplete="this.disabled=false" 
																			   reRender="listaCoberturas, total, botonGuia2"
																			   title="header=[Mostrar Coberturas] body=[Muestra las Coberturas.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">																			   
																			</a4j:commandButton>
										     							</td>
										     						</tr>
										     						<tr>
										     							<td align="center"colspan="2">
										     								<a4j:commandButton styleClass="boton" value="Guia Contable"	 id="botonGuia2"																	   
																			   onclick="muestraGuia()" oncomplete="this.disabled=false"
																			   disabled="#{!beanGuiasContables.disable }" 
																			   reRender="listaGuias"
																			   title="header=[Mostrar Guias Contables] body=[Muestra las Guias Contables.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">																			   
																			</a4j:commandButton>
										     							</td>
										     						</tr>
												    			</table>
												    		</td>	
												    												    	
												    	</tr>
								    				</table>
								    				
								    				<rich:spacer height="15px"></rich:spacer>
								    				<table>
							    					<tbody>
											    		<tr>
											    			<td align="center">
												    			<a4j:status for="consultaDatos" stopText=" ">
																	<f:facet name="start">
																		<h:graphicImage value="/images/ajax-loader.gif" />
																	</f:facet>
																</a4j:status>
															</td>
														</tr>
													</tbody>
													</table>
												</a4j:region>
											</td>								    
							    			<td class="frameCR"></td>
										</tr>
									    <tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
						    		</tbody>
						    		</table>
						    		<rich:spacer height="10px"></rich:spacer>
						    		<table class="botonera">
						    		<tbody>
									    <tr>
										    <td class="frameTL"></td>
										    <td class="frameTC"></td>
										    <td class="frameTR"></td>
									    </tr>
									   
							    		<tr>
										    <td class="frameCL"></td>
										    <td class="frameC" width="600px">
									     		<table> 
									     		<tbody>
										     		<tr>
										     			<td align="center">
													      	<rich:dataTable value="#{beanGuiasContables.listaCoberturas}" id="listaCoberturas" var="beanCoberturas" rows="10" width="100%" rowKeyVar="valor">																
																<f:facet name="header">																	      	
																	<rich:columnGroup>
																		<rich:column colspan="10"><h:outputText value="Informacion Coberturas" /></rich:column>																																																																
																		<rich:column breakBefore="true"><h:outputText value="Numero Poliza" /></rich:column>
																		<rich:column><h:outputText value="Ramo Poliza" /></rich:column>
																		<rich:column><h:outputText value="Ramo Reactor" /></rich:column>
																		<rich:column><h:outputText value="Ramo Contable" /></rich:column>
																		<rich:column><h:outputText value="CD Cobertura" /></rich:column>
																		<rich:column><h:outputText value="Cobertura" /></rich:column>
																		<rich:column><h:outputText value="Centro de Costos" /></rich:column>
																		<rich:column><h:outputText value="Tarifa" /></rich:column>
																		
																											
																	</rich:columnGroup>
																</f:facet>
																
																<rich:column style="border: 0px;"><h:outputText style="font-size:9px" value="#{beanCoberturas.numeroPoliza}" /></rich:column>
																<rich:column ><h:outputText style="font-size:9px" value="#{beanCoberturas.ramoPoliza}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanCoberturas.ramoReactor}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanCoberturas.ramoContable}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanCoberturas.cdCobertura}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanCoberturas.cobertura}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanCoberturas.centroCostos}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanCoberturas.tarifa}" /></rich:column>
																
																<tr>
															    		<td align="right" width="100%"><h:outputText value="Total:" /></td>
															    		<td align="right" width="100%"><h:outputText value="#{beanCoberturas.tarifa}" /></td>
															    </tr>
															    
																<f:facet name="footer">										
																	<rich:datascroller align="right" for="listaCoberturas" maxPages="10" page="1"/>
																</f:facet>
																
															</rich:dataTable>
															
															<rich:spacer height="10px"></rich:spacer>
						    		
														</td>
													</tr>
												</tbody>
												</table>
											</td>
											<td class="frameCR"></td>
								    	</tr>
									    <tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
							    	</tbody>
							    	</table>
							    	
<rich:spacer height="10px"></rich:spacer>
						    		<table class="botonera">
						    		<tbody>
									    <tr>
										    <td class="frameTL"></td>
										    <td class="frameTC"></td>
										    <td class="frameTR"></td>
									    </tr>
									   
							    		<tr>
										    <td class="frameCL"></td>
										    <td class="frameC" width="600px">
									     <table>
									     	<tr>
													<td align=center><h:outputText value="Total Tarifa" styleClass="importante2" style="color:#000000;font-size: 20px;"/>&nbsp;&nbsp;</td>
													<td align=center><h:outputText id="total" value="#{beanGuiasContables.totalTarifa}" styleClass="importante2" style="color:#000000;font-size: 20px;"/>&nbsp;&nbsp;</td>
											</tr>
									     </table>
											</td>
											<td class="frameCR"></td>
								    	</tr>
									    <tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
							    	</tbody>
							    	</table>
							    	<rich:spacer height="10px"></rich:spacer>
							    	<table class="botonera" id="tablaCoberturas" style="display: none">
						    		<tbody>
									    <tr>
										    <td class="frameTL"></td>
										    <td class="frameTC"></td>
										    <td class="frameTR"></td>
									    </tr>
									   
							    		<tr>
										    <td class="frameCL"></td>
										    <td class="frameC" width="600px">
									     		<table> 
									     		<tbody>
										     		<tr>
										     			<td align="center">
													      	<rich:dataTable value="#{beanGuiasContables.listaGuiasContables}" id="listaGuias" var="lstGuiasContables" rows="10" width="100%" rowKeyVar="valor">																
																<f:facet name="header">																	      	
																	<rich:columnGroup>
																		<rich:column colspan="10"><h:outputText value="Informacion Guias Contables" /></rich:column>																																																																
																		<rich:column breakBefore="true"><h:outputText value="Operacion" /></rich:column>
																		<rich:column><h:outputText value="Componente" /></rich:column>
																		<rich:column><h:outputText value="Cuenta" /></rich:column>
																		<rich:column><h:outputText value="Debe/Haber" /></rich:column>
																		<rich:column><h:outputText value="Fecha Inicio" /></rich:column>
																		<rich:column><h:outputText value="Fecha Fin" /></rich:column>																	
																											
																	</rich:columnGroup>
																</f:facet>
																
																<rich:column style="border: 0px;"><h:outputText style="font-size:9px" value="#{lstGuiasContables.cdOperacion}" /></rich:column>
																<rich:column ><h:outputText style="font-size:9px" value="#{lstGuiasContables.cdComponente}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{lstGuiasContables.deCuenta}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{lstGuiasContables.inDebhab}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{lstGuiasContables.fechaInicio}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{lstGuiasContables.fechaFin}" /></rich:column>
																																<f:facet name="footer">										
																	<rich:datascroller align="right" for="listaGuias" maxPages="10" page="1"/>
																</f:facet>
															</rich:dataTable>
															
															<rich:spacer height="10px"></rich:spacer>
						    		
														</td>
													</tr>
												</tbody>
												</table>
											</td>
											<td class="frameCR"></td>
								    	</tr>
									    <tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
							    	</tbody>
							    	</table>
								    	
						    					    	
									</div>
			     				</h:form>
							</td>
						</tr>
					</tbody>
			   		</table>
   				</td>
   			</tr>
		</tbody>
		</table>   	
		</div>
		<rich:spacer height="15px"></rich:spacer>
		<div><%@include file="../footer.jsp" %></div>
	</f:view>
	</body>
</html>