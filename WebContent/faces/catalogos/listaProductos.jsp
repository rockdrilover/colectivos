<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Lista de productos</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Cat�logo de productos</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form>
				    <div align="center">
					    <rich:spacer height="15"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="600px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
								    	<tr>
								    		<td width="30%"></td>
								     		<td align="right" width="10%"><h:outputText value="Ramo:" /></td>
								     		<td align="left" width="45%">
								     			<h:selectOneMenu value="#{beanListaProducto.inputRamo}">
								     				<f:selectItem itemValue="" itemLabel="<Seleccione una opcion>"/>
								     				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
								     			</h:selectOneMenu>
								     		<td align="left" width="5%"></td>
							     		</tr>
							     		<tr>
							     			<td align="center" colspan="2">
							     				<h:commandButton action="agregarProducto" value="Agregar" styleClass="boton"/>
							     			</td>
							     			<td align="right" colspan="2">
							     			<a4j:commandButton styleClass="boton" value="Consultar" 
															   action="#{beanListaProducto.consultarProductos}" 
															   onclick="this.disabled=true" oncomplete="this.disabled=false" 
															   reRender="lista"
															   title="header=[Consulta productos] body=[Consulta los productos dependiendo del ramo seleccionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
							     			</td>
							     		</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    <table class="botonera">
						    <tbody>
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="600px">
							     	<table>
							     	<tbody>
								     	<tr>
								     		<td align="center">
								     		<a4j:region>
										      	<rich:dataTable id="lista" value="#{beanListaProducto.listaProductos}" columns="5" columnsWidth="150px,10px,150px,12px,12px" var="beanProducto" rows="10">
										      		<f:facet name="header">
										      			<rich:columnGroup>
										      				<rich:column colspan="5"><h:outputText value="Lista de productos" /></rich:column>
										      				<rich:column breakBefore="true"><h:outputText value="Ramo" /></rich:column>
										      				<rich:column width="5px" ><h:outputText value="Producto" /></rich:column>
										      				<rich:column><h:outputText value="Descripci�n" /></rich:column>
										      				<rich:column><h:outputText value="Modificar" /></rich:column>
										      				<rich:column><h:outputText value="Eliminar" /></rich:column>
										      			</rich:columnGroup>
										      		</f:facet>
										      		<rich:column filterBy="#{beanProducto.ramo}" filterEvent="onkeyup">
											      		<h:outputText value="#{beanProducto.ramo}" />		      		  			
										      		</rich:column>
										      		<rich:column filterBy="#{beanProducto.id.alprCdProducto}" filterEvent="onkeyup">
										      			<h:outputText value="#{beanProducto.id.alprCdProducto}" />
										      		</rich:column>
										      		<rich:column filterBy="#{beanProducto.alprDeProducto}" filterEvent="onkeyup">
										      			<h:outputText value="#{beanProducto.alprDeProducto}" />
										      		</rich:column>
										      		<rich:column>
										      			<a4j:commandLink action="editarProducto">
										      				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
										      				<f:setPropertyActionListener value="#{beanProducto}" target="#{beanSesion.currentBeanProducto}" />
										      			</a4j:commandLink>
										      		</rich:column>
										      		<rich:column>
										      			<a4j:commandLink action="#{beanProducto.borrarProducto}" reRender="lista">
										      				<h:graphicImage value="../../images/delete.gif"  width="20" height="20"/>
										      				<f:setPropertyActionListener value="#{beanProducto}" target="#{beanSesion.currentBeanProducto}" />
										      			</a4j:commandLink>
										      		</rich:column>
										      		<f:facet name="footer">
										      			<rich:datascroller align="center" for="lista" maxPages="10" page="#{beanListaProducto.numPagina}"/>
										      		</f:facet>
										      	 </rich:dataTable>
									      	 </a4j:region>
										     </td>
										</tr>
									</tbody>
									</table>
									<td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
					</div>
			     	</h:form>
			     	<rich:spacer height="10px"></rich:spacer>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="15px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>