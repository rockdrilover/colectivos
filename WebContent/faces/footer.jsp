<%@include file="include.jsp"  %>
<html>
<head>
<script src="../../js/colectivos.js" language="javascript"></script>
</head>
<body>
<f:subview id="footer">
	<h:form id="frmFooter">
	<div align="center">
			<table class=tablaPrincipal cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
					<td align="center">
					<table class="tablaSecundaria">
					<tbody>
					<tr>
					<td>
						<table class="botonera">
						<tbody>
						<tr>
						<td class="frameTL"></td>
						<td class="frameTC"></td>
						<td class="frameTR"></td>
						</tr>
						<tr>
							<td class="frameCL"></td>
							<td class="frameC">
						<table>
							<tbody>
								<tr height="25">
									<td align="center" colspan="2" >
										<h:commandButton styleClass="boton" action="#{beanSesion.salir}" value="Salir" title="header=[Salir] body=[Cierra la sesi�n del usuario.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
									</td>	
									<td align="center">
										<h:commandButton styleClass="boton" value="Menu" action="#{beanSesion.menu}" title="header=[Men�] body=[Regresa a la pantalla principal de opciones.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
									</td>
									<td>
										<h:commandButton styleClass="boton" value="Limpiar" action="#{beanSesion.limpia}" title="header=[Limpiar] body=[Limpia la pantalla actual.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
									</td>
									<td align="center">
										<h:commandButton styleClass="boton" value="Imprimir" onclick="javascript:imprimir();" title="header=[Imprimir] body=[Imprime la pantalla actual.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
									</td>	
								</tr>
								
							</tbody>
						</table>
						</td>
						<td class="frameCR"></td>
						</tr>
						<tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
						</tbody>
						</table>
						<rich:spacer height="15" />
						<table align="center" bgcolor="#424242" width="785px" cellpadding="0" cellspacing="0">
						<tbody>
							<tr height="25" class="encabezado">
								<td align="right"><h:outputText value="Usuario:" /></td>
								<td align="left">&nbsp;<h:outputText value="#{beanSesion.userName}" /></td>
								<td align="right"><h:outputText value="Nombre:" /></td>
								<td align="left">&nbsp;<h:outputText value="#{beanSesion.nombreUsuario}" /> </td>						
							</tr>
						</tbody>
						</table>
					</td>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
				</tbody>
			</table>
	</div>
	</h:form>
</f:subview>	
</body>
</html>