<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<title>Consulta Facturaci�n</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
			<table class="tablaSecundaria">
				<tbody>
					<tr>
						<td align="center">
							<div align="center">
							<table bgcolor="#555242" cellpadding="0" cellspacing="0" class="encabezado">
								<tbody>
									<tr>
										<td bgcolor="#8F8B70" width="10%" align="center">Secci�n</td>
										<td width="4%" align="center" bgcolor="#d90909">|</td>
										<td align="left" width="64%">Consulta Facturaci�n</td>
										<td align="right" width="14%" >Fecha</td>
										<td align="left" width="10%">
											<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="10">
										</td>
									</tr>
								</tbody>
							</table>
						    </div>
						    <br/>
						    <h:form id="frmCargaEmision">
							    <div align="center">	
							     <h:panelGrid columns="1" width="600">
							    	<rich:panelMenu iconExpandedGroup="disc" iconCollapsedGroup="disc"
							    					iconExpandedTopGroup="chevronUp" iconGroupTopPosition="right"
							    					iconCollapsedTopGroup="chevronDown" iconGroupPosition="right">
							    		<rich:panelMenuGroup label="Consulta por recibo">
							    			<rich:panelMenuItem disabled="true" >
							    				<div align="center">
								                 <table class="tabla" align="center" width="60%">
								                 	<tbody>
								                 		<tr>
								                 			<td align="right" width="40%">
																<h:outputText value="Recibo:"/>										                 			
								                 			</td>
								                 			<td align="left">
								                 				<h:inputText value="#{beanListaFacturacion.numRecibo}" />
								                 			</td>
								                 		</tr>
								                 		<tr>
								                 			<td>
								                 			</td>
								                 			<td>
															   <h:commandButton action="#{beanListaFacturacion.consultaFactura}" value="Consultar" styleClass="boton" />												                 			
								                 			</td>
								                 		</tr>
								                 	</tbody>
								                 </table>
								                </div>
								                
								              <div align="center">
												<rich:dataTable id="datospoliza" value="#{beanListaFacturacion.listaFacturacion}" var="beanFacturacion" rows="10" columns="6" width="650px" columnsWidth="5px">
														<f:facet name="header">
														<h:outputText value="Datos de la p�liza" />
														</f:facet>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="Canal" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.certificado.id.coceCasuCdSucursal}">
															</h:outputText>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="Ramo" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.certificado.id.coceCarpCdRamo}">
															</h:outputText>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="P�liza" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.certificado.id.coceCapoNuPoliza}">
															</h:outputText>
															</rich:column>
															<rich:column >
																<f:facet name="header">
																<h:outputText value="Fecha Inicio" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.certificado.coceFeDesde}">
															<f:convertDateTime pattern="dd/MM/yyyy"/>  <!-- Para mostrar el mes en la fecha: MMM ene, MMMM enero -->
															</h:outputText>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="Fecha Fin" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.certificado.coceFeHasta}">
															<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="Estatus" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.certificado.alternaEstatus.alesCdEstatus}">
															</h:outputText>
															</rich:column>
													</rich:dataTable>
								                </div> 
								                
								                 <div align="center">
												<rich:dataTable id="datosrecibo" value="#{beanListaFacturacion.listaFacturacion}" var="beanFacturacion" rows="10" columns="10" width="650px">
														<f:facet name="header">
														<h:outputText value="Datos del recibo" />
														</f:facet>
															<rich:column>
															<f:facet name="header">
																<h:outputText value="Consecutivo" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaCuotaRecibo}">
															</h:outputText>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="Fecha Desde" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.recibo.careFeDesde}">
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>
															</rich:column>
															<rich:column>
															<f:facet name="header">
																<h:outputText value="Fecha Hasta" >
																	<f:convertDateTime pattern="dd/MM/yyyy"/>
																</h:outputText>	
															</f:facet>
															<h:outputText value="#{beanFacturacion.recibo.careFeHasta}">
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>
															</rich:column>
															<rich:column >
																<f:facet name="header">
																<h:outputText value="Suma Asegurada" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaMtTotSua}"/>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="Prima pura" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaMtTotPma}"/>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="IVA" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaMtTotIva}">
															</h:outputText>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="RFI" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaMtTotRfi}">
															</h:outputText>
															</rich:column>
															<rich:column >
																<f:facet name="header">
																<h:outputText value="DPO" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaMtTotDpo}"/>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="COM" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaMtTotCom}"/>
															</rich:column>
															<rich:column>
																<f:facet name="header">
																<h:outputText value="Prima total" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaMtRecibo}">
															</h:outputText>
															</rich:column>
													</rich:dataTable>
								                </div> 
				                        </rich:panelMenuItem>
							    		</rich:panelMenuGroup>

							    		<rich:panelMenuGroup label="Consulta por p�liza">
							    			<rich:panelMenuItem disabled="true">
							    			  <div align="center">
							    				<table class="tabla">
							    					<tbody>
													    <tr>
													    	<td align="right" width="40%">
													    		<h:outputText value="Ramo:"/>
													    	</td>
													    	<td align="left">
													    		<h:selectOneMenu id="inRamo" value="#{beanListaFacturacion.ramo}" binding="#{beanListaRamo.inRamo}">
																	<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
																	<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																	<a4j:support event="onchange" action="#{beanListaRamo.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
																</h:selectOneMenu>
													    	</td>
													    </tr>
													    <tr>
													    	<td align="right">
													    		<h:outputText value="Poliza:" />
													    	</td>
													    	<td align="left">
													    		<a4j:outputPanel id="polizas" ajaxRendered="true">
																	<h:selectOneMenu id="inPoliza"  value="#{beanListaFacturacion.numPoliza}">
																			<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																			<f:selectItems value="#{beanListaRamo.listaPolizas}"/>
																	</h:selectOneMenu>
																</a4j:outputPanel>
													    	</td>
													    </tr>
													    <tr>
													    	<td>
													    	</td>
													    	<td>
													    		<h:commandButton action="#{beanListaFacturacion.consultaRecibo}" value="Consultar" styleClass="boton"/>
													    	</td>
													    </tr>					
							    					</tbody>
							    				</table>
							    		     </div>
	
											<div align="center">
												<rich:dataTable id="recibos" value="#{beanListaFacturacion.listaFacturacion}" var="beanFacturacion" rows="10" columns="10" width="650px">
														<f:facet name="header">
															<h:outputText value="Recibos de la p�liza" />
														</f:facet>
														<rich:column>
															<f:facet name="header">
																<h:outputText value="Recibo" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaNuReciboFiscal}">
															</h:outputText>
														</rich:column>
														<rich:column>
															<f:facet name="header">
																<h:outputText value="Fecha Desde" />
															</f:facet>
														<h:outputText value="#{beanFacturacion.recibo.careFeDesde}">
															<f:convertDateTime pattern="dd/MM/yyyy"/>
														</h:outputText>
														</rich:column>
														<rich:column>
															<f:facet name="header">
																<h:outputText value="Fecha Hasta" >
																	<f:convertDateTime pattern="dd/MM/yyyy"/>
																</h:outputText>	
															</f:facet>
															<h:outputText value="#{beanFacturacion.recibo.careFeHasta}">
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>
														</rich:column>
														<rich:column >
															<f:facet name="header">
																<h:outputText value="Consecutivo" />
															</f:facet>
															<h:outputText value="#{beanFacturacion.cofaCuotaRecibo}"/>
														</rich:column>
													</rich:dataTable>
								                </div> 
	
						    			</rich:panelMenuItem>
							    		</rich:panelMenuGroup>
							    	</rich:panelMenu>
							    </h:panelGrid>
						    	<br/>	
							    														   
								</div>
							
								<h:messages styleClass="errorMessage" globalOnly="true"/>
						    </h:form>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<div align="center">
	<%@include file="footer.jsp"%>
		
</div>
</f:view>
</body>
</html>