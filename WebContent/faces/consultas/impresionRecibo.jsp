<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta por Recibo</title>
</head>
<body>
	<f:view>
		<!-- Desde aqui comienzo -->
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center">
			<%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tr>
					<td align="center">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div align="center">
										<table class="encabezadoTabla">
											<tr>
												<td width="10%" align="center">Secci�n</td>
												<td width="4%" align="center" bgcolor="#d90909">|||</td>
												<td align="left" width="64%">Impresion de Recibo</td>
												<td align="right" width="12%">Fecha</td>
												<td align="left" width="10%"><input name="fecha"
													readonly="readonly"
													value="<%=request.getSession().getAttribute("fecha")%>"
													size="7" /></td>
											</tr>
										</table>
									</div> <h:form id="frmRecibos">
										<div align="center">
											<table width="90%" border="0" cellpadding="0" cellspacing="0"
												align="center">
												<tr class="espacio_15">
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td><h:outputText id="mensaje"
															value="#{beanListaRecibo.respuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15">
													<td>&nbsp;</td>
												</tr>
											</table>

											<a4j:region id="consultaDatos">
												<table width="90%" border="0" cellpadding="0"
													cellspacing="0" align="center">
													<tr>
														<td>
															<fieldset style="border: 1px solid white">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> <h:graphicImage
																				value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones">Filtros</span>
																	</a4j:outputPanel>
																</legend>

																<table width="100%" border="0" cellpadding="0"
																	cellspacing="3" align="center">
																	<tr>
																		<td align="right" width="20%"><h:outputText
																				value="Ramo:" /></td>
																		<td align="left" width="80%" colspan="3"><h:selectOneMenu
																				id="inRamo" value="#{beanListaRecibo.ramo}"
																				binding="#{beanListaParametros.inRamo}"
																				title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																				required="true">
																				<f:selectItem itemValue="0"
																					itemLabel="Selecione un ramo" />
																				<f:selectItems
																					value="#{beanListaRamo.listaComboRamos}" />
																				<a4j:support event="onchange"
																					action="#{beanListaParametros.cargaPolizas}"
																					ajaxSingle="true" reRender="polizas" />
																			</h:selectOneMenu> <h:message for="inRamo" styleClass="errorMessage" />
																		</td>
																	</tr>
																	<tr>
																		<td align="right" width="20%"><h:outputText
																				value="Poliza:" /></td>
																		<td align="left" width="80%" colspan="3"><a4j:outputPanel
																				id="polizas" ajaxRendered="true">
																				<h:selectOneMenu id="inPoliza"
																					value="#{beanListaRecibo.poliza}"
																					binding="#{beanListaParametros.inPoliza}"
																					title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																					required="true">
																					<f:selectItem itemValue="-1"
																						itemLabel="Seleccione una p�liza" />
																					<f:selectItem itemValue="0" itemLabel="Prima �nica" />
																					<f:selectItems
																						value="#{beanListaParametros.listaComboPolizas}" />
																					<a4j:support event="onchange"
																						action="#{beanListaParametros.cargaIdVenta}"
																						ajaxSingle="true" reRender="idVenta" />
																				</h:selectOneMenu>
																				<h:message for="inPoliza" styleClass="errorMessage" />
																			</a4j:outputPanel></td>
																	</tr>
																	<tr>
																		<td align="right" width="20%"><h:outputText
																				value="Canal de Venta:" /></td>
																		<td align="left" width="80%" colspan="3"><a4j:outputPanel
																				id="idVenta" ajaxRendered="true">
																				<h:selectOneMenu id="inVenta"
																					value="#{beanListaRecibo.idVenta}"
																					title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																					disabled="#{beanListaParametros.habilitaComboIdVenta}">
																					<f:selectItem itemValue="0"
																						itemLabel="Seleccione una canal de venta" />
																					<f:selectItems
																						value="#{beanListaParametros.listaComboIdVenta}" />
																				</h:selectOneMenu>
																			</a4j:outputPanel></td>
																	</tr>
																	<tr>
																		<td align="right" width="15%"><h:outputText
																				value="Fecha Desde:" /></td>
																		<td align="left" width="30%"><rich:calendar
																				id="fechIni" style="width:100%;" popup="true"
																				value="#{beanListaRecibo.feDesde}"
																				datePattern="dd/MM/yyyy" styleClass="calendario" />
																		</td>
																		<td align="right" width="15%"><h:outputText
																				value="Fecha Hasta:" /></td>
																		<td align="left" width="30%"><rich:calendar
																				id="fechFin" style="width:100%;" popup="true"
																				value="#{beanListaRecibo.feHasta}"
																				datePattern="dd/MM/yyyy" styleClass="calendario" />
																		</td>
																	</tr>
																	<tr>
																		<td align="right" width="20%"><h:outputText
																				value="No. de Recibo:" /></td>
																		<td align="left" width="80%" colspan="3"><h:inputText
																				id="noRecibo" value="#{beanListaRecibo.noRecibo}" />
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3"></td>
																		<td align="left"><a4j:commandButton
																				styleClass="boton" value="Consultar"
																				action="#{beanListaRecibo.consulta2}"
																				onclick="this.disabled=true"
																				oncomplete="this.disabled=false"
																				reRender="mensaje,secRecibos,regTabla"
																				title="header=[Consulta recibos] body=[Consulta los recibos dependiendo del filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</td>
																	</tr>

																	<tr class="espacio_15">
																		<td>&nbsp;</td>
																	</tr>
																	<tr>
																		<td align="center" colspan="4"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status></td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<!-- Inicio REECIBOS -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0"
												align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<a4j:outputPanel id="secRecibos" ajaxRendered="true">
																	<span id="img_sec_recibos_m" style="display: none;">
																		<h:graphicImage
																			value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('recibos', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_recibos_o"> <h:graphicImage
																			value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('recibos', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones">Recibos</span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0"
																	cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_recibos"
																		style="display: none;">
																		<td><c:if
																				test="${beanListaRecibo.listaRecibos == null}">
																				<table width="90%" align="center" border="0">
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO
																							SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> <c:if test="${beanListaRecibo.listaRecibos != null}">
																				<rich:dataTable id="listaRecibos"
																					value="#{beanListaRecibo.listaRecibos}"
																					
																					columnsWidth="8px,8px,15px,15px,15px,20px,20px,5px"
																					var="beanRecibo" rows="10">
																					<f:facet name="header">
																						<h:outputText value="Datos del Recibo" />
																					</f:facet>

																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Canal" />
																						</f:facet>
																						<h:outputText
																							value="#{beanRecibo.id.careCasuCdSucursal}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Ramo" />
																						</f:facet>
																						<h:outputText value="#{beanRecibo.careCarpCdRamo}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="P�liza" />
																						</f:facet>
																						<h:outputText
																							value="#{beanRecibo.careCapoNuPoliza}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Recibo" />
																						</f:facet>
																						<h:commandLink
																							value="#{beanRecibo.id.careNuRecibo}"
																							action="#{beanRecibo.imprimeRecibo}" />
																						<br />
																						<h:commandLink
																							value="#{beanRecibo.reciboAnterior}"
																							action="#{beanRecibo.imprimeRecibo2}"
																							rendered="#{not empty beanRecibo.reciboAnterior}" />
																					</rich:column>																					
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Estatus Recibo" />
																						</f:facet>
																						<h:outputText value="#{beanRecibo.careStRecibo}" /> -
																						<h:outputText value="#{beanRecibo.estatus.id.rvMeaning}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Estatus Cobranza" />
																						</f:facet>
																						<h:outputText
																							value="#{beanRecibo.stCobranza}" />
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Emisi�n" />
																						</f:facet>
																						<h:outputText value="#{beanRecibo.careFeEmision}">
																							<f:convertDateTime pattern="dd/MM/yyyy" />
																						</h:outputText>
																					</rich:column>
																					<rich:column>
																						<f:facet name="header">
																							<h:outputText value="Detalle" />
																						</f:facet>
																						<a4j:commandLink styleClass="boton"
																							id="btnConsCoberCom"
																							action="#{beanListaRecibo.mostrarCoberComp}"
																							onclick="this.disabled=true"
																							oncomplete="this.disabled=false"
																							reRender="secCoberturas,regTablaCoberturas,secComponentes,regTablaComponentes">
																							<f:setPropertyActionListener
																								value="#{beanRecibo.id.careNuRecibo}"
																								target="#{beanListaRecibo.noReciboConsulta}" />
																							<h:graphicImage
																								value="../../images/certificados.png"
																								style="border:0" />
																						</a4j:commandLink>
																					</rich:column>
																					<rich:column
																						>
																						<f:facet name="header" >
																							<h:outputText value="XML"  />
																						</f:facet>

																						<h:commandLink value="#{beanRecibo.UUID}"
																							action="#{beanRecibo.descargaCFDIXML}" rendered="#{beanRecibo.blnReciboTimbrado && !beanRecibo.blnReciboCancelado}" />



																					</rich:column>
																					<rich:column
																						>
																						<f:facet name="header">
																							<h:outputText value="PDF" />
																						</f:facet>
																						<h:commandLink value="#{beanRecibo.UUID}"
																							action="#{beanRecibo.descargaCFDIPDF}" rendered="#{beanRecibo.blnReciboTimbrado && !beanRecibo.blnReciboCancelado}" >
																							
																							</h:commandLink>
																					</rich:column>
																					<rich:column >
																						<f:facet name="header">
																							<h:outputText value="Timbrar" />
																						</f:facet>
																						<a4j:commandLink styleClass="boton"
																							id="btnTimbrar"
																							rendered="#{(!beanRecibo.blnReciboTimbrado && !beanRecibo.blnReciboCancelado)}"
																							action="#{beanListaRecibo.sellarRecibos}"
																							onclick="this.disabled=true"
																							oncomplete="this.disabled=false"
																							reRender="mensaje,secRecibos,regTabla">
																							<f:setPropertyActionListener
																								value="#{beanRecibo}"
																								target="#{beanListaRecibo.seleccionado}" />
																							<h:graphicImage
																								value="../../images/certificados.png"
																								style="border:0" />
																						</a4j:commandLink>
																					</rich:column>
																					<rich:column >
																						<f:facet name="header">
																							<h:outputText value="Cancelar" />
																						</f:facet>
																						<a4j:commandLink styleClass="boton"
																							id="btnCancelar"
																							rendered="#{beanRecibo.blnReciboTimbrado && !beanRecibo.blnReciboCancelado}"
																							action="#{beanListaRecibo.cancelarRecibos}"
																							onclick="this.disabled=true"
																							oncomplete="this.disabled=false"
																							reRender="mensaje,	secCoberturas,regTablaCoberturas,secComponentes,regTablaComponentes">
																							<f:setPropertyActionListener
																								value="#{beanRecibo}"
																								target="#{beanListaRecibo.seleccionado}" />
																							<h:graphicImage
																								value="../../images/certificados.png"
																								style="border:0" />
																						</a4j:commandLink>
																					</rich:column>
																					
																					
																					<f:facet name="footer">
																						<rich:datascroller align="right"
																							for="listaRecibos" maxPages="10"
																							page="#{beanListaRecibo.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>

											<!-- Inicio Movimientos Recibos -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0"
												align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<a4j:outputPanel id="secCoberturas" ajaxRendered="true">
																	<span id="img_sec_coberturas_m" style="display: none;">
																		<h:graphicImage
																			value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('coberturas', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_coberturas_o"> <h:graphicImage
																			value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('coberturas', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones">Movimientos
																		Recibos</span>
																</a4j:outputPanel>
															</legend>

															<a4j:outputPanel id="regTablaCoberturas"
																ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0"
																	cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_coberturas"
																		style="display: none;">
																		<td><c:if
																				test="${beanListaRecibo.listaCoberturas == null}">
																				<table width="90%" align="center" border="0">
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL RECIBO
																							SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> <c:if
																				test="${beanListaRecibo.listaCoberturas != null}">
																				<rich:dataTable id="listaCober"
																					value="#{beanListaRecibo.listaCoberturas}"
																					columns="2" var="cobert" rows="10"
																					rowKeyVar="valor">
																					<f:facet name="header">
																						<rich:columnGroup>
																							<rich:column width="70%">
																								<h:outputText value="Descripcion" />
																							</rich:column>
																							<rich:column width="30%">
																								<h:outputText value="Prima" />
																							</rich:column>
																						</rich:columnGroup>
																					</f:facet>

																					<rich:column width="70%" style="text-align:center">
																						<h:outputText value="#{cobert.cober_desc}" />
																					</rich:column>
																					<rich:column width="30%" style="text-align:center">
																						<h:outputText value="#{cobert.monto_cober}" />
																					</rich:column>
																					<f:facet name="footer">
																						<rich:datascroller align="right" for="listaCober"
																							maxPages="10" page="#{beanListaRecibo.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>

											<!-- Inicio Componentes Recibos -->
											<table width="90%" border="0" cellpadding="0" cellspacing="0"
												align="center">
												<tr>
													<td>
														<fieldset style="border: 1px solid white">
															<legend>
																<a4j:outputPanel id="secComponentes" ajaxRendered="true">
																	<span id="img_sec_componentes_m" style="display: none;">
																		<h:graphicImage
																			value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('componentes', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_componentes_o"> <h:graphicImage
																			value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('componentes', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones">Componentes</span>
																</a4j:outputPanel>
															</legend>

															<a4j:outputPanel id="regTablaComponentes"
																ajaxRendered="true">
																<table width="100%" border="0" cellpadding="0"
																	cellspacing="0">
																	<tr class="texto_normal" id="tr_sec_componentes"
																		style="display: none;">
																		<td><c:if
																				test="${beanListaRecibo.listaComponentes == null}">
																				<table width="90%" align="center" border="0">
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL RECIBO
																							SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> <c:if
																				test="${beanListaRecibo.listaComponentes != null}">
																				<rich:dataTable id="listacompo"
																					value="#{beanListaRecibo.listaComponentes}"
																					columns="3" var="componente" rows="10"
																					rowKeyVar="valor">
																					<f:facet name="header">
																						<rich:columnGroup>

																							<rich:column width="50%">
																								<h:outputText value="Descripcion" />
																							</rich:column>
																							<rich:column width="300%">
																								<h:outputText value="Monto" />
																							</rich:column>
																							<rich:column width="20%">
																								<h:outputText value="Tarifa" />
																							</rich:column>
																						</rich:columnGroup>
																					</f:facet>

																					<rich:column width="50%" style="text-align:center">
																						<h:outputText value="#{componente.compo_desc}" />
																					</rich:column>
																					<rich:column width="30%" style="text-align:center">
																						<h:outputText value="#{componente.monto_compo}" />
																					</rich:column>
																					<rich:column width="20%" style="text-align:center">
																						<h:outputText value="#{componente.tarif}" />
																					</rich:column>
																					<f:facet name="footer">
																						<rich:datascroller align="right" for="listacompo"
																							maxPages="10" page="#{beanListaRecibo.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>

										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
	</f:view>
</body>
</html>