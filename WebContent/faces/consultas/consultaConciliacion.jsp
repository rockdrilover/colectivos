<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title>Consulta Conciliacion </title>
		
		<style>
			.filtroR {text-align: right;}
			.filtroL {text-align: left;}
			.paneles {width: 90%;}
			.texto_normal {font-weight: bold; font-size: 11px}
		</style>
	</head>

	<body>
	<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
	<div align="center"><%@include file="../header.jsp" %></div>
	
	<div align="center">
	<table class="tablaPrincipal">
		<tr>	
			<td align="center">
	    		<table class="tablaSecundaria">
					<tr>
						<td align="center">
							<table class="encabezadoTabla" >
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Consulta Conciliacion</td>
									<td align="right" width="12%" >Fecha</td>
									<td align="left" width="10%"><input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7"></td>
								</tr>
							</table>
							
				    		<rich:spacer height="10px"></rich:spacer>
							<h:form>
						    <rich:spacer height="15px"/>	
						    <table class="botonera">
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
					    		<tr>
						    		<td class="frameCL"></td>
						    		<td class="frameC">
							    		<a4j:region id="consulta">
							    		<table width="80%">
									    	<tr>
												<td align="center" width="60%">
													<h:outputText id="mensaje" value="#{beanConsultaConciliacion.message}" styleClass="respuesta"/>
												</td>
												<td align="left" width="20%">
													<a4j:commandButton styleClass="boton" value="Consultar" 
															action="#{beanConsultaConciliacion.consulta}" 
															onclick="this.disabled=true" oncomplete="this.disabled=false;" 
															reRender="mensaje"
															title="header=[Consulta] body=[Ejecuta la consulta con el filtro especificado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
															<a4j:support event="onclick"  action="#{beanConsultaConciliacion.consulta}" ajaxSingle="true" reRender="resultados" />
													</a4j:commandButton>
												</td>
											</tr>
											
											<tr><td colspan="2">&nbsp;</td></tr>
								    		<tr>
								    			<td colspan="2">
								    				<rich:panel styleClass="paneles">
								               			<f:facet name="header"><h:outputText value="Opciones de consulta" /></f:facet>
								               			<h:panelGrid columns="2" columnClasses="filtroR, filtroL">																							          
											        		<h:outputText value="Origen:   " styleClass="texto_normal"/>														        	
															<h:selectOneMenu id="origen" value="#{beanConsultaConciliacion.nOrigen}" 
																	title="header=[Origen] body=[Seleccione el origen] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
																	required="true">
																<f:selectItem itemValue="0" itemLabel="Seleccione una opcion" />
																<f:selectItems value="#{beanConsultaConciliacion.lstComboOrigen}"/>
															</h:selectOneMenu>	
														
															<h:outputText value="Tipo Reporte:   " styleClass="texto_normal"/>														        	
															<h:selectOneMenu id="tipoRep" value="#{beanConsultaConciliacion.nTipoRep}" 
																	title="header=[Tipo Reporte] body=[Seleccione un tipo de reporte] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
																	required="true">
																<f:selectItem itemValue="0" itemLabel="Seleccione una opcion" />
																<f:selectItems value="#{beanConsultaConciliacion.lstComboTipoRep}"/>
															</h:selectOneMenu>	
														
															<h:outputText id="lblFecha" value="Fecha Inicial:   " styleClass="texto_normal"/>
								    						<rich:calendar id="fecha" value="#{beanConsultaConciliacion.fecha}" datePattern="dd/MM/yyyy"
								    								enableManualInput="false" inputStyle="width:130px;"/>
								    					
								    						<h:outputText id="lblFechaFin" value="Fecha Final:   " styleClass="texto_normal"/>
								    						<rich:calendar id="fechaFin" value="#{beanConsultaConciliacion.fechaFin}" datePattern="dd/MM/yyyy"
								    								enableManualInput="false" inputStyle="width:130px;"/>
											        	</h:panelGrid>
								               		</rich:panel>
												</td>
								    		</tr>
								    		<tr><td colspan="2">&nbsp;</td></tr>
										</table>
							    		
							    		<table width="80%">
							    			<tr>
							    				<td>
							    					<a4j:outputPanel id="resultados" ajaxRendered="true">
								    				<rich:panelBar width="90%" styleClass="panelRich" contentClass="texto_normal_blanco">
														<rich:panelBarItem label="   Ocultar" headerStyle="text-align: left;"></rich:panelBarItem>
														
														<c:if test="${beanConsultaConciliacion.lstResultados != null}">
															<rich:panelBarItem label="   Resultados - Acumulado." headerStyle="text-align: left;">														
																<c:if test="${beanConsultaConciliacion.nTipoRep == 3 || beanConsultaConciliacion.nTipoRep == 8 || beanConsultaConciliacion.nTipoRep == 10}">
																	<table width="100%">
																		<c:set var="nRegistro" value="<%= 0%>" />
																		<c:forEach var="resultado" items="#{beanConsultaConciliacion.lstResultados}"  >
																			<tr>
																				<c:if test="${nRegistro == 0}">
																					<td colspan="2" align="left">
																						&nbsp;&nbsp;
																						<img src="../../images/${resultado.imagen}" title="${resultado.descripcion}">
																						<strong><h:outputText value="Total: "/>&nbsp;&nbsp;${resultado.totalregistros}&nbsp;&nbsp;&nbsp;&nbsp;<h:outputText value="Prima Real: "/>&nbsp;&nbsp;${resultado.prima}&nbsp;&nbsp;&nbsp;&nbsp;<h:outputText value="Prima Pura: "/>&nbsp;&nbsp;${resultado.primapura}</strong>
																					</td>
																				</c:if>
																				<c:if test="${nRegistro > 0}">
																					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
																					<td>
																						<img src="../../images/${resultado.imagen}" title="${resultado.descripcion}">
																						<strong><h:outputText value="Total: "/>&nbsp;&nbsp;${resultado.totalregistros}&nbsp;&nbsp;&nbsp;&nbsp;<h:outputText value="Prima Real: "/>&nbsp;&nbsp;${resultado.prima}&nbsp;&nbsp;&nbsp;&nbsp;<h:outputText value="Prima Pura: "/>&nbsp;&nbsp;${resultado.primapura}</strong>
																					</td>
																				</c:if>
																			</tr>	
																			<c:set var="nRegistro" value="${nRegistro + 1}" />																	
																		</c:forEach>
																	</table>
																</c:if>
																<c:if test="${beanConsultaConciliacion.nTipoRep != 3 && beanConsultaConciliacion.nTipoRep != 8 && beanConsultaConciliacion.nTipoRep != 10}">
																<table width="100%">
																	<tr>
																		<td width="80%">
																			<h:graphicImage value="../../images/total.png" title="Total"/>&nbsp;
																			<strong><h:outputText id="lblTotal" value="Total: " />&nbsp;&nbsp;${fn:length(beanConsultaConciliacion.lstResultados)}</strong>
																		</td>
																	</tr>
																</table>	
																</c:if>
															</rich:panelBarItem>
														
															<rich:panelBarItem label="   Resultados - Detalle" headerStyle="text-align: left;">
																<table width="100%">
																	<tr>
																		<td align="right">	
																			<h:commandButton action="#{beanConsultaConciliacion.descargar}" value="Descargar" 
																				title="header=[Descargar] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />  
							    										</td>
																	</tr>
																	<tr><td>&nbsp;</td></tr>
																				
																	<tr>																
																		<td>
																			<c:if test="${beanConsultaConciliacion.nTipoRep != 3 && beanConsultaConciliacion.nTipoRep != 8 && beanConsultaConciliacion.nTipoRep != 10}">
																			<rich:dataTable value="#{beanConsultaConciliacion.lstResultados}" id="taRespuesta" var="resultado" columns="8" rows="10" width="80%">
																				<f:facet name="header">
																	      			<rich:columnGroup>
																	      				<rich:column><h:outputText value="Numero Credito" /></rich:column>
																	      				<rich:column><h:outputText value="Prima" /></rich:column>
																	      				
																	      				<c:if test="${beanConsultaConciliacion.nTipoRep == 5 || beanConsultaConciliacion.nTipoRep == 9 || beanConsultaConciliacion.nTipoRep == 7}">			
																	      					<rich:column><h:outputText value="Suma Asegurada" /></rich:column>
																	      					<rich:column><h:outputText value="Cod. Error" /></rich:column>
																	      					<rich:column><h:outputText value="Desc. Error" /></rich:column>
																	      					<c:if test="${beanConsultaConciliacion.nTipoRep == 5}">
																	      						<rich:column><h:outputText value="Tarifa" /></rich:column>
																	      					</c:if>
																	      					<c:if test="${beanConsultaConciliacion.nTipoRep == 7}">
																	      						<rich:column><h:outputText value="Edad" /></rich:column>
																	      					</c:if>
																	      				</c:if>
																	      				
																	      				<c:if test="${beanConsultaConciliacion.nTipoRep == 4}">	
																	      					<rich:column><h:outputText value="Cuenta" /></rich:column>
																	      					<rich:column><h:outputText value="Descripcion" /></rich:column>
																	      				</c:if>																			      																					      				
																	      			</rich:columnGroup>
																	      		</f:facet>
																	      		
																	      		<rich:column><h:outputText value="#{resultado.numcredito}"/></rich:column>																				
																	      		<rich:column><h:outputText value="#{resultado.prima}" /></rich:column>
																	      		
																	      		<c:if test="${beanConsultaConciliacion.nTipoRep == 5 || beanConsultaConciliacion.nTipoRep == 9 || beanConsultaConciliacion.nTipoRep == 7}">			
																	      			<rich:column><h:outputText value="#{resultado.sumaasegurada}" /></rich:column>
																	      			<rich:column><h:outputText value="#{resultado.coderror}" /></rich:column>
																	      			<rich:column><h:outputText value="#{resultado.descerror}" /></rich:column>
																	      			<c:if test="${beanConsultaConciliacion.nTipoRep == 5}">
															      						<rich:column><h:outputText value="#{resultado.tarifa}" /></rich:column>
															      					</c:if>
															      					<c:if test="${beanConsultaConciliacion.nTipoRep == 7}">
															      						<rich:column><h:outputText value="#{resultado.edad}" /></rich:column>
															      					</c:if>
															      				</c:if>
															      				
															      				<c:if test="${beanConsultaConciliacion.nTipoRep == 4}">	
															      					<rich:column><h:outputText value="#{resultado.cuenta}" /></rich:column>
															      					<rich:column><h:outputText value="#{resultado.descripcion}" /></rich:column>
															      				</c:if>		
															      				
															      				<f:facet name="footer">
																	      			<rich:datascroller for="taRespuesta" maxPages="10"/>
																	      		</f:facet>
																			</rich:dataTable>
																			</c:if>	
																			
																			<c:if test="${beanConsultaConciliacion.nTipoRep == 3 || beanConsultaConciliacion.nTipoRep == 8 || beanConsultaConciliacion.nTipoRep == 10}">
																			<rich:dataTable value="#{beanConsultaConciliacion.lstEmitir}" id="taRespuesta1" var="res" columns="8" rows="10" width="80%">
																				<f:facet name="header">
																	      			<rich:columnGroup>
																	      				<rich:column><h:outputText value="Numero Poliza" /></rich:column>
																	      				<rich:column><h:outputText value="Numero Credito" /></rich:column>
																	      				<rich:column><h:outputText value="Prima" /></rich:column>
																      					<rich:column><h:outputText value="Suma Asegurada" /></rich:column>
																      					<c:if test="${beanConsultaConciliacion.nTipoRep == 8}">
																	      					<rich:column><h:outputText value="Prima Pura" /></rich:column>
																      						<rich:column><h:outputText value="Suma Asegurada SI" /></rich:column>
																	      				</c:if>
																	      				<rich:column><h:outputText value="Prima Vida" /></rich:column>
																	      				<rich:column>
																	      					<h:outputText value="Prima Desempleo" rendered="#{beanConsultaConciliacion.nOrigen != 6}"/>
																	      					<h:outputText value="Prima Gap" rendered="#{beanConsultaConciliacion.nOrigen == 6}"/>
																	      				</rich:column>
																	      			</rich:columnGroup>
																	      		</f:facet>
																	      		
																	      		<rich:column><h:outputText value="#{res.poliza}"/></rich:column>
																	      		<rich:column><h:outputText value="#{res.numcredito}"/></rich:column>																				
																	      		<rich:column><h:outputText value="#{res.prima}" /></rich:column>
																	      		<rich:column><h:outputText value="#{res.sumaasegurada}" /></rich:column>
																	      		<c:if test="${beanConsultaConciliacion.nTipoRep == 8}">
															      					<rich:column><h:outputText value="#{res.primapura}" /></rich:column>
																	      			<rich:column><h:outputText value="#{res.sumaaseguradasi}" /></rich:column>
																	      		</c:if>
																	      		<rich:column><h:outputText value="#{res.primavida}" /></rich:column>
																	      		<rich:column><h:outputText value="#{res.primadesempleo}" /></rich:column>
																	      		
																	      		<f:facet name="footer">
																	      			<rich:datascroller for="taRespuesta1" maxPages="10"/>
																	      		</f:facet>
																			</rich:dataTable>
																			
																			</c:if>															
																		</td>																												
																	</tr>
																</table>
															</rich:panelBarItem>
														</c:if>
													</rich:panelBar>
													</a4j:outputPanel>
							    				</td>
							    			</tr>
							    		</table>
							    		
										<rich:spacer height="15px"></rich:spacer>
							   			
							   			<table>
								    		<tr>
								    			<td align="center">
									    			<a4j:status for="consulta" stopText=" ">
														<f:facet name="start"><h:graphicImage value="/images/ajax-loader.gif" /></f:facet>
													</a4j:status>
												</td>
											</tr>
										</table>
								    	</a4j:region>
								    </td>
						    		<td class="frameCR"></td>
						    	</tr>
						    	<tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    <rich:spacer height="10px"></rich:spacer>	
							</h:form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	
	<rich:spacer height="10px"/>
	<div align="center"><%@ include file="../footer.jsp" %></div>
	</f:view>
	</body>
 </html>