<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<%@ page import="mx.com.santander.aseguradora.colectivos.view.bean.BeanListaClienteCertif" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:c="http://java.sun.com/jsp/jstl/core">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
    <script type="text/javascript" src="../../js/boxover.js"></script>
    <script type="text/javascript" src="../../js/colectivos.js"></script>
	<title>Consulta Detalle por Certificado</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>				
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Consulta Detalle por Certificado</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmconscred">
					    <div align="center">
						    <rich:spacer height="15px"/>	
						    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">
										    <a4j:region id="consultaDatos">
											     <table cellpadding="2" cellspacing="0">
											    	<tr>
											    		<td></td>
											     		<td align="right"><h:outputText value="Numero Identificador:" /></td>
											     		<td align="left"><h:inputText value="#{beanListaClienteCertif.idCertificado}" />
											     		</td>
										     		</tr>
										     		<tr>
										     			<td></td>
											     		<td align="right"><h:outputText value="Nombre:" /></td>
											     		<td align="left"><h:inputText id="nombre"  value="#{beanListaClienteCertif.nombre}" />
											     		</td>
										     		</tr>
										     		<tr>
										     			<td></td>
											     		<td align="right"><h:outputText value="Apellido Paterno:" /></td>
											     		<td align="left"><h:inputText id="ap"  value="#{beanListaClienteCertif.apellidoPaterno}" />
											     		</td>
										     		</tr>
							                        <tr>
							                        	<td></td>
											     		<td align="right"><h:outputText value="Apellido Materno:" /></td>
											     		<td align="left"><h:inputText id="am"  value="#{beanListaClienteCertif.apellidoMaterno}" />
											     		</td>
										     		</tr>
										     		<tr>
										     			<td></td>
										     			<td></td>
										     			<td align="right">
										     				<a4j:commandButton styleClass="boton" value="Consultar" action="#{beanListaClienteCertif.consultaCred}" onclick="this.disabled=true" oncomplete="this.disabled=false" reRender="listaCreditos" title="header=[Consulta certificados] body=[Consulta los certificados dependiendo del filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
										     			</td>
										     		</tr>
					                                <tr>
					                                     <td colspan="3" align="center">
					                                           <br/>
					                                           <h:outputText id="mensaje" value="#{beanListaClienteCertif.respuesta}" styleClass="respuesta"/>
					                                     </td>
					                                </tr>
											    </table>
											    <rich:spacer height="15px"></rich:spacer>
											    <table>
										    		<tr>
										    			<td align="center">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
												</table>
											</a4j:region>								    
									    <td class="frameCR"></td>
									</tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">
									     	<table>
										     	<tr>
										     		<td align="center">
												      	<rich:dataTable id="listaCreditos" value="#{beanListaClienteCertif.listaCreditos}" columns="5" var="beanClienteCertif" rows="20" rowKeyVar="valor">
												      		<f:facet name="header">
												      			<rich:columnGroup>
												      				<rich:column width="10%"><h:outputText value="ID" /></rich:column>
												      				<rich:column width="10%"><h:outputText value="ID2" /></rich:column>
												      				<rich:column width="40%"><h:outputText value="Nombre" /></rich:column>
												      				<rich:column width="40%" colspan="2"><h:outputText value="Datos Generales" /></rich:column>
												      			</rich:columnGroup>
												      		</f:facet>
												      		<rich:column width="10%" style="text-align:center">
												      				<h:outputText value="#{beanClienteCertif.certificado.coceNuCredito}" />
															</rich:column>
												      		<rich:column width="10%" style="text-align:center">
												      				<h:outputText value="#{beanClienteCertif.certificado.coceBucEmpresa}" />
															</rich:column>
												      		<rich:column width="40%" style="text-align:center">
												      			<h:outputText value="#{beanClienteCertif.cliente.cocnNombre} #{beanClienteCertif.cliente.cocnApellidoPat} #{beanClienteCertif.cliente.cocnApellidoMat}" />
															</rich:column>
		                                                    <rich:column width="20%" style="text-align:center ">
		                                                    	<h:graphicImage onclick="verCteCert(1, #{valor}, #{fn:length(beanListaClienteCertif.listaCreditos)}, #{fn:length(beanListaClienteCertif.listaCertificados)});" value="/images/clientes.png" title="header=[Clientes] body=[Dar click para mostrar detalle de los clientes.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
		                                                     </rich:column>
		                                                    <rich:column width="20%" style="text-align:center">
		                                                    	<h:graphicImage onclick="verCteCert(2, #{valor}, #{fn:length(beanListaClienteCertif.listaCreditos)}, #{fn:length(beanListaClienteCertif.listaCertificados)});" value="/images/certificados.png" title="header=[Certificados] body=[Dar click para mostrar detalle de los certificados.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
		                                                    </rich:column>
		                                                    
		                                                    <rich:column breakBefore="true" colspan="7" id="colClientes" style="display: none">
																<table align="center" width=100% border="0">
																	<tr>
																		<td colspan="4"  bgcolor="#d90909" align="center">
																			<h:outputText value="Datos Cliente" style="font-weight:bold; color: white" />
																		</td>
																	</tr>
																	<tr>
																		<td colspan="4" width="20">
																		</td>
																	</tr>
																	<tr> 
																		<td align="left">
																			<h:outputText value="BUC:" style="font-weight:bold; color: black" />
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnBucCliente}" />      		  			
																		</td>
																		<td align="left">
																			<h:outputText value="Colonia:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnColonia}" />
																		</td>
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="No. Cliente:" style="font-weight:bold; color: black" />
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnNuCliente}" >
																			</h:outputText>      		  			
																		</td>
																		<td align="left">
																			<h:outputText value="Deleg/Mun:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnDelegmunic}" />      		  			
																		</td>
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="No. Cuenta:" style="font-weight:bold; color: black" />
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.certificado.coceNuCuenta}" />      		  			
																		</td>
																		<td align="left">
																			<h:outputText value="CP:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnCdPostal}" />
																    </td>  		  			
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="Sexo:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnCdSexo}" />      		  			
																		</td>
																		<td align="left">
																			<h:outputText value="Estado:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnCampov1}" />      		  			
																		</td>
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="RFC:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnRfc}" />      		  			
																		</td>
																		<td align="left">
																			<h:outputText value="Lada:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnNuLada}" />  
																		</td>
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="Fecha Nac:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnFeNacimiento}" >
																				<f:convertDateTime pattern="dd-MM-yyyy"/>
																			</h:outputText>
																    	</td>  		  			
																		<td align="left">
																			<h:outputText value="Telefono" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnNuTelefono}" />      		  			
																		</td>
																	</tr>
																	<tr>
																		<td align="left">
																			<h:outputText value="Calle y No:" style="font-weight:bold; color: black"/>
																		</td>
																		<td align="left">
																			<h:outputText value="#{beanClienteCertif.cliente.cocnCalleNum}" />      		  			
																		</td>
																		<td align="left">
																			&nbsp;
																		</td>
																		<td align="left">
																			&nbsp;
																	   </td>
																	</tr>  		  			
																</table>
		                                                    </rich:column>
		                                                    
		                                                    <rich:column breakBefore="true" colspan="7" id="colCertificados" style="display: none">
		                                                    	  <c:set var="nTama�o" value="#{fn:length(beanListaClienteCertif.listaCertificados)}"></c:set>
		                                                          <rich:dataTable value="#{beanListaClienteCertif.listaCertificados}" id="listaCertificados" var="beanCertif" rows="20" headerClass="detalleEncabezado2" styleClass="detalleEncabezado" rowKeyVar="valor1" >
		                                                                <rich:column style="text-align:center">
																			<table align="center" width=100% border="0">
																				<tr>
																					<td colspan="9"  bgcolor="#d90909" align="left" onclick="verDetalleCert(<h:outputText value="#{valor1}" />, ${nTama�o}, <h:outputText value="#{valor}" />);">
																						&nbsp;&nbsp;&nbsp;
																						<h:outputText value="DATOS CERTIFICADO: " style="font-weight:bold; color: white" />
																						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																						<h:outputText value="Ramo: " style="font-weight:bold; color: white" />
																						<h:outputText value="#{beanCertif.certificado.id.coceCarpCdRamo}" style="font-weight:bold; color: white" />&nbsp;&nbsp;&nbsp;
																						<h:outputText value="#{beanCertif.certificado.coceCampov4}" style="font-weight:bold; color: white" />&nbsp;&nbsp;&nbsp;					
																						<h:outputText value="Poliza: " style="font-weight:bold; color: white" />
																						<%--<h:outputText value="#{beanCertif.certificado.id.coceCapoNuPoliza}" style="font-weight:bold; color: white" />&nbsp;&nbsp;&nbsp; --%>
																						<h:commandLink value="#{beanCertif.certificado.id.coceCapoNuPoliza}" action="#{beanCertif.impresionPoliza}" style="font-weight:bold; color: white" />&nbsp;
																						<h:outputText value="Certificado: " style="font-weight:bold; color: white" />
																						<%-- <h:outputText value="#{beanCertif.certificado.id.coceNuCertificado}" style="font-weight:bold; color: white" /> --%>
																						<h:commandLink value="#{beanCertif.certificado.id.coceNuCertificado}" action="#{beanCertif.getReportePlatilla}" style="font-weight:bold; color: white" />&nbsp; 
																						<%--<h:outputText id="existe"  value="#{beanCertif.existe}" />--%>	   
																					</td>
																				</tr>
																				<tr id="trDetalleCert_<h:outputText value="#{valor1}" />" style="display: none">
																					<td>
																						<table>
																							<tr>
																								<td colspan="9" width="20">
																								</td>
																							</tr>
																							<tr> 
																								<td align="left">
																									<h:outputText value="Status Colectivos" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceCampov1}" />
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Fecha Carga" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceFeCarga}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																							    </td>  		  			
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Ult. Recibo" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceNoRecibo}" />
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td align="left">
																									<h:outputText value="Status Tecnico" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceCampov2}" />
																						    	</td>  		  			
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Fecha Emision" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceFeSuscripcion}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Fec. Inicio Vigencia" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceFeDesde}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td align="left">
																									<h:outputText value="Motivo Anulaci�n" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceCampov3}" />
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Suma Asegurada" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceMtSumaAsegurada}" />
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Fec. Hasta Vigencia" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceFeHasta}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td align="left">
																									<h:outputText value="Fecha Anulaci�n" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceFeAnulacionCol}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Saldo P/Base de C�lculo" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceMtSumaAsegSi}" />
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Fec. Inicio Credito" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceFeIniCredito}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td align="left">
																									<h:outputText value="Producto" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceTpProductoBco}" />
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Prima Total" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceMtPrimaSubsecuente}" />
																							  	</td>  		  			
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Fec. Fin Credito" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceFeFinCredito}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td align="left">
																									<h:outputText value="Plan" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceTpSubprodBco}" />
																								</td>
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Prima Neta" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceMtPrimaPura}" />
																							  	</td>  		  			
																								<td width="10px">
																									&nbsp;
																								</td>
																								<td align="left">
																									<h:outputText value="Sucursal" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="right">
																									<h:outputText value="#{beanCertif.certificado.coceCazbCdSucursal}" />
																								</td>
																							</tr>
																							<tr>
																							    <td align="left">
																									<h:outputText value="M�s Informaci�n" style="font-weight:bold; color: black"/>
																								</td>
																								<td align="left" colspan="6">
																									<h:outputText value="#{beanCertif.certificado.coceEmpresa}"/>
																								</td>																							
																							</tr>
																							<tr> 
																								<td colspan="5">
																									<table align="center" width=100% border="0">
																										<tr>
																											<td>
														                                                    	<h:graphicImage onclick="verCobAsis(21, #{valor1}, #{fn:length(beanListaClienteCertif.listaCertificados)}, #{valor});" value="/images/coberturas.png" title="header=[Coberturas] body=[Dar click para mostrar detalle de las coberturas.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																											</td>
																											<td>
					                                    									                	<h:graphicImage onclick="verCobAsis(22, #{valor1}, #{fn:length(beanListaClienteCertif.listaCertificados)}, #{valor});" value="/images/asistencias.png" title="header=[Asistencias] body=[Dar click para mostrar detalle de las asistencias.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																											</td>
																											<td>
					                                    									                	<h:graphicImage rendered="#{beanCertif.complemento.id.cocoCaceCarpCdRamo == 89}" onclick="verCobAsis(23, #{valor1}, #{fn:length(beanListaClienteCertif.listaCertificados)}, #{valor});" value="/images/gap.png" title="header=[Complemento] body=[Dar click para mostrar detalle del los datos complementarios.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																											</td>
																										    <td>
					                                    									                	<h:graphicImage rendered="#{beanCertif.numAseguradosAd != 0}" onclick="verCobAsis(24, #{valor1}, #{fn:length(beanListaClienteCertif.listaCertificados)}, #{valor});" value="/images/clientes adicionales.png" title="header=[Asegurados Adicionales] body=[Dar click para mostrar detalle de los asegurados adicionales.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																											</td>
																											<td>
														                                                    	<h:graphicImage onclick="verCobAsis(25, #{valor1}, #{fn:length(beanListaClienteCertif.listaCertificados)}, #{valor});" value="/images/beneficiarios.png" title="header=[Beneficiarios] body=[Dar click para mostrar beneficiarios.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																											</td>
																											<td>
					                                    									                	<h:graphicImage onclick="verCobAsis(26, #{valor1}, #{fn:length(beanListaClienteCertif.listaCertificados)}, #{valor});" value="/images/endosos.jpg" title="header=[Endoso] body=[Dar click para mostrar detalle de endosos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																											</td>																										
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
		                                                                </rich:column> 
		                                                                <rich:column breakBefore="true" colspan="7" id="colAsistencias" style="display: none">
		    	                                                        	<rich:dataTable value="#{beanCertif.lstAsistencias}" id="listaAsistencias" var="beanAsistencias" rows="10" headerClass="detalleEncabezado2" styleClass="detalleEncabezado">
																		      		<f:facet name="header">
																		      			<rich:columnGroup>
																		      				<rich:column width="40%" style="border:0"><h:outputText value="Descripci�n Asistencia" /></rich:column>
																		      			</rich:columnGroup>
																		      		</f:facet>
																		      		<rich:column width="40%" style="text-align:center; border:0">
																								<h:outputText value="#{beanAsistencias.certificado.coceCampov2}" />
																					</rich:column>
																	    	</rich:dataTable>
																		</rich:column>
		                                                                <rich:column breakBefore="true" colspan="7" id="colCoberturas" style="display: none">
		    	                                                        	<rich:dataTable value="#{beanCertif.lstCoberturas}" id="listaCoberturas" var="beanCoberturas1" rows="10" headerClass="detalleEncabezado2" styleClass="detalleEncabezado">
																		      		<f:facet name="header">
																		      			<rich:columnGroup>
																		      				<rich:column width="8%" style="border:0"><h:outputText value="Cobertura" /></rich:column>
																		      				<rich:column width="40%" style="border:0"><h:outputText value="Descripci�n Cobertura" /></rich:column>
																		      				<rich:column width="20%" style="border:0"><h:outputText value="Tarifa" /></rich:column>
																		      			</rich:columnGroup>
																		      		</f:facet>
																		       		<rich:column width="8%" style="text-align:center; border:0">
																								<h:outputText value="#{beanCoberturas1.certificado.coceCampov1}" />
																					</rich:column>
																		      		<rich:column width="40%" style="text-align:center; border:0">
																								<h:outputText value="#{beanCoberturas1.certificado.coceCampov2}" />
																								<f:facet name="footer">         										
										                											<h:outputText value="TARIFA TOTAL" style="text-align:center; FONT-WEIGHT: bold"/>
										                								    	</f:facet>
																					</rich:column>
																		      		<rich:column width="20%" style="text-align:center; border:0">
																								<h:outputText value="#{beanCoberturas1.certificado.coceCampon4}" />
																								<f:facet name="footer">         										
										                											<h:outputText value="#{beanCertif.certificado.coceTpPma}" style="text-align:center; FONT-WEIGHT: bold" />
										                								    	</f:facet>
																					</rich:column>
																	    	</rich:dataTable>
																		</rich:column>
																		
																		<rich:column breakBefore="true" colspan="7" id="colComplemento" style="display: none">
		    	                                                        	<rich:panel styleClass="detalleEncabezado">
		    	                                                        		<f:facet name="header">
																		            <h:outputText value="Datos Autos"/>
																		      	</f:facet>
																		      	<table>
																		      	<tr>
																		      		<td align="left">
																						<h:outputText value="Tipo Veh�culo" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoHogDelegmunic }" />
																				  	</td>
																				  	<td width="10px">
																						&nbsp;
																					</td>
																					<td align="left">
																						<h:outputText value="Serie" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoHogVcampo2 }" />
																				  	</td>
																				  	<td width="10px">
																						&nbsp;
																					</td>
																					<td align="left">
																						<h:outputText value="Ramo" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoVidtVcampo2 }" />
																				  	</td>																								
																		      	</tr>
																		      	<tr>
																		      		<td align="left">
																						<h:outputText value="Marca" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
 																						<h:outputText value="#{beanCertif.complemento.cocoHogColonia}" />
																				  	</td>
																				  	<td width="10px">
																						&nbsp;
																					</td>
																					<td align="left">
																						<h:outputText value="Motor" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoVidtVcampo4}" />
																				  	</td>
																				  	<td width="10px">
																						&nbsp;
																					</td>
																					<td align="left">
																						<h:outputText value="Prima Seguro Auto" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoVidtVcampo3}" />
																				  	</td>
																		      	</tr>
																		      	<tr>
																		      		<td align="left">
																						<h:outputText value="Modelo" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoHogCalleNum}" />
																				  	</td>
																				  	<td width="10px">
																						&nbsp;
																					</td>
																					<td align="left">
																						<h:outputText value="Placas" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoHogVcampo3}" />
																				  	</td>
																				  	<td width="10px">
																						&nbsp;
																					</td>
																					<td align="left">
																						<h:outputText value="Tipo Cobertura" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanCertif.complemento.cocoVidtVcampo1}" />
																				  	</td>
																		      	</tr>
																		      	<tr>
																		      		<td align="left">
																						<h:outputText value="Poliza Autos" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="right">
																						<h:outputText value="#{beanClienteCertif.certificado.coceBucEmpresa}" />
																				  	</td>
																					<td align="left">
																						
																					</td>
																					<td align="right">
																						
																				  	</td>
																					<td align="left">
																						
																					</td>
																					<td align="right">
																						
																				  	</td>
																		      	</tr>
																		      	<tr>
																		      		<td align="left">
																						<h:outputText value="Descripci�n" style="font-weight:bold; color: black"/>
																					</td>
																					<td align="left" colspan="6">
																						<h:outputText value="#{beanCertif.complemento.cocoHogVcampo1}" />
																				  	</td>
																		      	</tr>
																		      	</table>
		    	                                                        	</rich:panel>
																		</rich:column>
																		
																		<rich:column breakBefore="true" colspan="7" id="colAsegurados" >
		    	                                                        	<rich:dataTable value="#{beanCertif.lstAsegurados}" id="listaAsegurados" var="beanAseg" rows="10" headerClass="detalleEncabezado2" styleClass="detalleEncabezado">
																	      		<f:facet name="header">
																	      			<h:outputText value="Asegurados" />																		      			
																		      	</f:facet>
																		      	
																	       		<rich:column width="20%" style="text-align:center; border:0">
																	       			<f:facet name="header"><h:outputText value="Nombre" /></f:facet>
																					<h:outputText value="#{beanAseg.cliente.cocnNombre}" />
																				</rich:column>
																				<rich:column width="20%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Apellido Paterno" /></f:facet>
																					<h:outputText value="#{beanAseg.cliente.cocnApellidoPat}" />
																				</rich:column>
																				<rich:column width="20%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Apellido Materno" /></f:facet>
																					<h:outputText value="#{beanAseg.cliente.cocnApellidoMat}" />
																				</rich:column>
																				<rich:column width="15%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Fecha Nacimiento" /></f:facet>
																					<h:outputText value="#{beanAseg.cliente.cocnFeNacimiento}">
																						<f:convertDateTime pattern="dd-MM-yyyy"/>
																					</h:outputText>
																				</rich:column>
																				<rich:column width="10%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="RFC" /></f:facet>
																					<h:outputText value="#{beanAseg.cliente.cocnRfc}" />
																				</rich:column>
																				<rich:column width="5%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Sexo" /></f:facet>
																					<h:outputText value="#{beanAseg.cliente.cocnCdSexo}" />
																				</rich:column>
																				<rich:column width="20%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Tipo Asegurado" /></f:facet>
																					<h:outputText value="#{beanAseg.cliente.cocnCampov1}" />
																				</rich:column>
																		      		
																	    	</rich:dataTable>
																		</rich:column>
																		
																		<rich:column breakBefore="true" colspan="7" id="colBeneficiarios" style="display: none">
		    	                                                        	<rich:dataTable value="#{beanCertif.lstBeneficiarios}" id="listaBeneficiarios" var="beanBene" rows="10" headerClass="detalleEncabezado2" styleClass="detalleEncabezado">
																	      		<f:facet name="header">
																	      			<h:outputText value="Beneficiarios" />																	      			
																	      		</f:facet>
																	      		
																	       		<rich:column width="8%" style="text-align:center; border:0">
																	       			<f:facet name="header"><h:outputText value="Numero" /></f:facet>
																					<h:outputText value="#{beanBene.id.cobeNuBeneficiario}" />
																				</rich:column>
																	      		<rich:column width="40%" style="text-align:center; border:0">
																	      			<f:facet name="header"><h:outputText value="Nombre" /></f:facet>
																					<h:outputText value="#{beanBene.cobeNombre}" />&nbsp;&nbsp;
																					<h:outputText value="#{beanBene.cobeApellidoPat}" />&nbsp;&nbsp;
																					<h:outputText value="#{beanBene.cobeApellidoMat}" />																							
																				</rich:column>
																	      		<rich:column width="20%" style="text-align:center; border:0">
																	      			<f:facet name="header"><h:outputText value="Relacion" /></f:facet>
																					<h:outputText value="#{beanBene.cobeRelacionBenef}" />&nbsp;-&nbsp;
																					<h:outputText value="#{beanBene.cobeVcampo2}" />																							
																				</rich:column>
																				<rich:column width="20%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="%" /></f:facet>
																					<h:outputText value="#{beanBene.cobePoParticipacion}" />																						
																				</rich:column>
																				<rich:column width="20%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Fecha Alta" /></f:facet>
																					<h:outputText value="#{beanBene.cobeFeDesde}">
																						<f:convertDateTime pattern="dd/MM/yyyy" />
																					</h:outputText>																					
																				</rich:column>
																	    	</rich:dataTable>
																		</rich:column>
																		
																		<rich:column breakBefore="true" colspan="7" id="colEndosos" style="display: none">
		    	                                                        	<rich:dataTable value="#{beanCertif.lstEndosos}" id="listaEndoso" var="beanEndoso" rows="10" headerClass="detalleEncabezado2" styleClass="detalleEncabezado">
																	      		<f:facet name="header">
																	      			<h:outputText value="Endosos" />																	      			
																	      		</f:facet>
																	      		
																	       		<rich:column width="10%" style="text-align:center; border:0">
																	       			<f:facet name="header"><h:outputText value="No." /></f:facet>
	 																				<h:outputText value="#{beanEndoso.id.cedaNuEndosoDetalle}" />
																				</rich:column>
																	      		<rich:column width="15%" style="text-align:center; border:0">
																	      			<f:facet name="header"><h:outputText value="Tipo" /></f:facet>
																					<h:outputText value="#{beanEndoso.cedaCampov3}" />																					
																				</rich:column>
																	      		<rich:column width="15%" style="text-align:center; border:0">
																	      			<f:facet name="header"><h:outputText value="Folio" /></f:facet>
<%-- 																					<h:outputText value="#{beanEndoso.cedaNuFolioEntrada}" /> --%>
																					<h:outputText value="#{beanEndoso.cedaCampov1}" />																				
																				</rich:column>
																				<rich:column width="20%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Usuario" /></f:facet>
																					<h:outputText value="#{beanEndoso.cedaCdUsuarioAplica}" />
																				</rich:column>
																				<rich:column width="20%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Aplicacion" /></f:facet>
																					<h:outputText value="#{beanEndoso.cedaFeAplica}" >
																						<f:convertDateTime pattern="dd/MM/yyyy HH:mm" />
																					</h:outputText>																				
																				</rich:column>
																				<rich:column width="10%" style="text-align:center; border:0">
																					<f:facet name="header"><h:outputText value="Detalle" /></f:facet>																					
																					<a4j:commandLink ajaxSingle="true" id="detallelink" oncomplete="#{rich:component('detPanel')}.show()">
				                        												<h:graphicImage value="../../images/certificados.png" style="border:0" />
				                        												<f:setPropertyActionListener value="#{beanEndoso}" target="#{beanListaClienteCertif.seleccionado}" />
				                    												</a4j:commandLink>
				                    												<rich:toolTip for="editlink" value="Ver Detalle Endoso" />
																				</rich:column>
																	    	</rich:dataTable>
																		</rich:column>
		                                                          </rich:dataTable>
		                                                    </rich:column>
												      		<f:facet name="footer">
												      			<rich:datascroller align="right" for="listaCreditos" maxPages="10" page="#{beanListaClienteCertif.numPagina}"/>
												      		</f:facet>
												      	</rich:dataTable>
												    </td>
											     </tr>
											</table>
										<td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
						    </table>
						</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
		<rich:modalPanel id="detPanel" autosized="true" width="550">
	       	<f:facet name="header"><h:outputText value="Detalle Endoso" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="detPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="detalleEndoso">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="6">
	                    	
	                    	<h:outputText value="Folio: "/>
	                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaCampov1}" styleClass="textoModal"/>
	                    	<h:outputText value="      "/>
	                    	<h:outputText value="      "/>
	                    	<h:outputText value="Tipo: "/>
	                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaCampov3}" styleClass="textoModal"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
	                    	
	                    	<rich:separator height="2" lineType="double" />
							<rich:separator height="2" lineType="double"/>
							<rich:separator height="2" lineType="double" />
							<rich:separator height="2" lineType="double"/>
							<rich:separator height="2" lineType="double" />
							<rich:separator height="2" lineType="double"/>
	                    	
	                    	<h:outputText value="Datos "/>
	                    	<h:outputText value="Nuevos "/>
	                    	<h:outputText value="      "/>
	                    	<h:outputText value="      "/>
	                    	<h:outputText value="Datos "/>
	                    	<h:outputText value="Anteriores "/>
	                    	
	                    	<rich:separator height="2" lineType="double" />
							<rich:separator height="2" lineType="double"/>
							<rich:separator height="2" lineType="double" />
							<rich:separator height="2" lineType="double"/>
							<rich:separator height="2" lineType="double" />
							<rich:separator height="2" lineType="double"/>
							
							<c:if test="${beanListaClienteCertif.seleccionado.cedaCampov3 == 'CREDITO'}">
								<h:outputText value="Credito: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaNombreNvo}" styleClass="textoModal"/>
		                    	<h:outputText value="      "/>
		                    	<h:outputText value="      "/>	                    	
		                    	<h:outputText value="Credito: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaNombreAnt}" styleClass="textoModal"/>
							</c:if> 
							<c:if test="${beanListaClienteCertif.seleccionado.cedaCampov3 != 'CREDITO'}">
		                    	<h:outputText value="Nombre: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaNombreNvo}" styleClass="textoModal"/>
		                    	<h:outputText value="      "/>
		                    	<h:outputText value="      "/>	                    	
		                    	<h:outputText value="Nombre: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaNombreAnt}" styleClass="textoModal"/>
		                    	
		                    	<h:outputText value="Apellido Paterno: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaApNvo}" styleClass="textoModal"/>
		                    	<h:outputText value="      "/>
		                    	<h:outputText value="      "/>
								<h:outputText value="Apellido Paterno: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaApAnt}" styleClass="textoModal"/>
								
								<h:outputText value="Apellido Materno: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaAmNvo}" styleClass="textoModal"/>
		                    	<h:outputText value="      "/>
		                    	<h:outputText value="      "/>
				     			<h:outputText value="Apellido Materno: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaAmAnt}" styleClass="textoModal"/>
		                    	
				     			<h:outputText value="Parentesco: "/>
								<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaPjParticipaNvo}" styleClass="textoModal"/>
								<h:outputText value="      "/>
		                    	<h:outputText value="      "/>	                    	
								<h:outputText value="Parentesco: "/>
								<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaPjParticipaAnt}" styleClass="textoModal"/>
										                    	
		                    	<h:outputText value="% Participacion: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaParentescoNvo}" styleClass="textoModal"/>
		                    	<h:outputText value="      "/>
		                    	<h:outputText value="      "/>
								<h:outputText value="% Participacion: "/>
		                    	<h:outputText value="#{beanListaClienteCertif.seleccionado.cedaParentescoAnt}" styleClass="textoModal"/>
		                    </c:if>	                    	
	                    </h:panelGrid>
	                </a4j:outputPanel>	               
	            </h:panelGrid>
	        </h:form>
   		</rich:modalPanel>
</f:view>
</body>
</html>
