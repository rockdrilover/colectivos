<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta por Recibo</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla">
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">|||</td>
							<td align="left" width="64%">Impresion y Reporte de Endosos</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7" />
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmEndoso">
				    <div align="center">
					    <rich:spacer height="15"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    <a4j:region id="consultaDatos">
								    <table>
								    <tbody>
								    	<tr>
											<td colspan="2">
												<h:outputText id="mensaje" value="#{beanListaEndoso.strRespuesta}" styleClass="error"/>
											</td>
										</tr>
								    	<tr>
								    		<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
								    		<td align="left" width="80%">
												<h:selectOneMenu id="inRamo" value="#{beanListaEndoso.inRamo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
														<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
														<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
													</h:selectOneMenu>
												<h:message for="inRamo" styleClass="errorMessage" />
											</td>
							     		</tr>
							     		<tr>
							     			<td align="right" width="20%"><h:outputText value="Poliza:"/></td>
											<td align="left" width="80%">
												<a4j:outputPanel id="polizas" ajaxRendered="true">
													<h:selectOneMenu id="inPoliza"  value="#{beanListaEndoso.inPoliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
															<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
															<f:selectItem itemValue="0" itemLabel="Prima �nica carga"/>
															<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
															<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta" />
													</h:selectOneMenu>
													<h:message for="inPoliza" styleClass="errorMessage" />
												</a4j:outputPanel>
											</td>
							     		</tr>
							     		<tr>
								    		<td align="right" width="24%" height="30"><h:outputText value="Canal de Venta:"/></td>
											<td align="left" width="76%" height="30">
												<a4j:outputPanel id="idVenta" ajaxRendered="true">
													<h:selectOneMenu id="inVenta"  value="#{beanListaEndoso.inIdVenta}" binding="#{beanListaParametros.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
													         disabled="#{beanListaParametros.habilitaComboIdVenta}">
															<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
															<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
													</h:selectOneMenu>
												</a4j:outputPanel>
											</td>
								    	</tr>
							     	   <tr>  
						    				<td align="right">
						    					<a4j:commandButton styleClass="boton" value="Tipos de Endoso" 
													   action="#{beanListaParametros.cargaTipoEndoso}" 
													   reRender="inOperEndoso"
													   title="header=[Endosos] body=[Tipos de endosos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>	   
											</td>
											<td align="left" width="76%" height="30">
												<h:selectOneMenu id="inOperEndoso" value="#{beanListaEndoso.inOperEndoso}" binding="#{beanListaParametros.inOperEndoso}" 
												     title="header=[Ramo] body=[Seleccione operacion para endosar.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
												     required="true">
													<f:selectItem itemValue="0" itemLabel="Seleccione el tipo de Endoso" />
													<f:selectItems value="#{beanListaParametros.listaTipoEndoso}"/>
												</h:selectOneMenu>
												<h:message for="inOperEndoso" styleClass="errorMessage" />
											</td>
							    		</tr>
				                        <tr>
				                        	<td align="right" width="15%"><h:outputText value="Fecha Desde:" /></td>
								     		<td align="left" width="25%">
                                              <rich:calendar  id="fechIni" style ="width:70%;" popup="true" value="#{beanListaEndoso.feDesdeReporte}"  
                                              datePattern="dd/MM/yyyy" styleClass="calendario" />
                                            </td>
							     		</tr>
							     		<tr>
							     			<td align="right" width="15%"><h:outputText value="Fecha Hasta:" /></td>
				                        	<td align="left" width="25%">
                                              <rich:calendar  id="fechFin" style ="width:70%;" popup="true" value="#{beanListaEndoso.feHastaReporte}"  
                                              datePattern="dd/MM/yyyy" styleClass="calendario" />
                                            </td>
							     		</tr>
							     		
							     		<tr>
							     			<td align="center" colspan="4">
							     			    <br/>
							     				<h:commandButton styleClass="boton" style ="width:20%;" value="Lista Endosos" 
						    					       id="listaEndoso"
													   action="#{beanListaEndoso.consultaEndoso}"  
													   title="header=[Endosos] body=[Lista de Endosos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
							     				&nbsp
							     				<h:commandButton styleClass="boton" style ="width:20%;" value="Reporte de Endosos" 
						    					       id="reporteEndoso"
													   action="#{beanListaEndoso.reporteEndosos}"  
													   title="header=[Endosos] body=[Reporte de Endosos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
							     			
								     			<br/>
									    		<br/>
									    		<br/>
							     			</td>

							     		</tr>
							     		
							     		<tr>
									    <td class="frameC" colspan="3">
									    	<div align="center">
												<rich:dataTable id="listaEndosos" value="#{beanListaEndoso.listaEndosos}" columns="8" rowKeyVar="row" 
										     		       columnsWidth="10px,10px,14px,14px,14px,14px,20px, 4px" var="endoso" rows="10">
											      		<f:facet name="header"><h:outputText value="Datos del Endoso" /></f:facet>
											      		<rich:column>  
											      	        <f:facet name="header"><h:outputText value="Canal" /></f:facet>
															<h:outputText value="#{endoso.endoso.id.coedCasuCdSucursal}" />      		  			
											      		</rich:column>
											      		<rich:column> 
											      		    <f:facet name="header"><h:outputText value="Ramo" /></f:facet> 
															<h:outputText value="#{endoso.endoso.id.coedCarpCdRamo}" />      		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="P�liza" /></f:facet>
															<h:outputText value="#{endoso.endoso.id.coedCapoNuPoliza}" />      		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Recibo" /></f:facet>
															<h:outputText value="#{endoso.endoso.coedNuRecibo}" /> 
														</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="No. Endoso" /></f:facet>
															<h:outputText  />  
															<h:commandLink value="#{endoso.endoso.coedCampov1}" action="#{endoso.imprimeEndoso}"/>     		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Desde Endoso" /></f:facet>
															<h:outputText value="#{endoso.endoso.coedFechaInicio}" >
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>     		  			
											      		</rich:column>
											      		<rich:column>  
											      		    <f:facet name="header"><h:outputText value="Hasta Endoso" /></f:facet>
															<h:outputText value="#{endoso.endoso.coedFechaFin}" >
																<f:convertDateTime pattern="dd/MM/yyyy"/>
															</h:outputText>      		  			
											      		</rich:column>
											      		<rich:column width="10%">
											      			<f:facet name="header"><h:outputText value="Detalle" /></f:facet>
															<h:commandLink id="btnDescarga"
																action="#{beanListaEndoso.descargaDetalle}">
																<h:graphicImage value="../../images/certificados.png" />
																<f:setPropertyActionListener value="#{endoso}" target="#{beanListaEndoso.objActual}" />
															</h:commandLink>
														</rich:column>
											      		<f:facet name="footer">
											      			<rich:datascroller align="right" for="listaEndosos" maxPages="10" page="#{beanListaEndoso.numPagina}"/>
											      		</f:facet>
											      	</rich:dataTable>
												<br/>
											</div>
									    </td>
										
								    	</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>	
								</td>							    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="15px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>