<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Cedula
- Fecha: 09/10/2020
- Descripcion: Pantalla Cedula Control
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view locale="es_MX">
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">		
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallaxviii}" /></title>
	</head>
	
	<body onload="muestraOcultaSeccion('actual', 0);muestraOcultaSeccion('historico', 0);">
	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaxviii}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallaxviii}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmCedula">
										<h:inputHidden value="#{beanCedula.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanCedula.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<caption></caption>
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemafiltro}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.cedulatipo}:" />
																		</td>
																		<td class="left80">
																			<a4j:outputPanel id="tipoce" ajaxRendered="true">
																				<h:selectOneMenu id="inTipoce" value="#{beanCedula.tipocedula}">
																					<f:selectItem itemValue="0" itemLabel=" --- Seleccione un tipo cedula --- "/>
																					<f:selectItem itemValue="1" itemLabel="Cedula Emision"/>
																					<f:selectItem itemValue="2" itemLabel="Cedula Cancelacion"/>
																					<a4j:support event="onchange" 
																							action="#{beanListaParametros.cargaIdVenta}" 
																							ajaxSingle="true" 
																							reRender="idVenta" 
																							oncomplete="muestraOcultaSeccion('actual', 0);muestraOcultaSeccion('historico', 0);">
																						<f:setPropertyActionListener value="#{beanCedula.inPoliza}" target="#{beanListaParametros.inPoliza}" />
																						<f:setPropertyActionListener value="#{beanCedula.inRamo}" target="#{beanListaParametros.inRamo}" />
																					</a4j:support>
																				</h:selectOneMenu>
																			</a4j:outputPanel>
																		</td>
																	</tr>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemaidventa}:" />
																		</td>
																		<td class="left80">
																			<a4j:outputPanel id="idVenta" ajaxRendered="true">
																				<h:selectOneMenu id="inVenta" value="#{beanCedula.idVenta}">
																					<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																				</h:selectOneMenu>
																			</a4j:outputPanel>
																		</td>
																	</tr>
																	<tr class="texto_normal">
																		<td class="right15">
																			<h:outputText value="#{msgs.sistemafechad}:" />
																		</td>
																		<td class="left30">
																			<rich:calendar id="fechIni" popup="true"
																					value="#{beanCedula.feDesde}"
																					datePattern="dd/MM/yyyy" styleClass="calendario,wid100" />
																		</td>
																	</tr>
																	<tr class="texto_normal">
																		<td class="right15">
																			<h:outputText value="#{msgs.sistemafechah}:" />
																		</td>
																		<td class="left30">
																			<rich:calendar id="fechFin" popup="true"
																					value="#{beanCedula.feHasta}"
																					datePattern="dd/MM/yyyy" styleClass="calendario,wid100" />
																		</td>
																	</tr>																	
																	<tr class="texto_normal">
																		<td class="right-div" colspan="2"> 
																			<a4j:commandButton id="btnConsulta"
																							styleClass="boton" value="#{msgs.botonconsultar}"
																							action="#{beanCedula.consulta}"
																							onclick="this.disabled=true;bloqueaPantalla();"
																							oncomplete="this.disabled=false;
																										desbloqueaPantalla();
																										muestraOcultaSeccion('actual', 0);
																										muestraOcultaSeccion('historico', 0);"
																							reRender="mensaje,regTabla, regTablaP"/>
																			<rich:toolTip for="btnConsulta" value="Consulta Cedulas" />
																		</td>
																	</tr>

																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen90">
												<caption></caption>
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secActual" ajaxRendered="true">
																	<span id="img_sec_actual_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('actual', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_actual_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('actual', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.cedulaactual}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<caption></caption>
																	<tr class="texto_normal" id="tr_sec_actual" >
																		<td>
																			<c:if
																				test="${beanCedula.listaActual == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanCedula.listaActual != null}">
																				<rich:dataTable id="listaActual" value="#{beanCedula.listaActual}"
																						columnsWidth="10px,8px,8px,8px,8px,8px,8px,8px,8px,8px"
																						var="cedula" rows="15" width="100%">
																					<f:facet name="header">
																						<h:outputText value="#{msgs.cedulaactual}" />
																					</f:facet>

																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularemesa}" /></f:facet>
																						<h:outputText value="#{cedula.coccRemesaCedula}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" title="Total de Registros">
																						<f:facet name="header"><h:outputText value="#{msgs.cedular}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegistros}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro"  title="Total de Registros PND">
																						<f:facet name="header"><h:outputText value="#{msgs.cedularp}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegPnd}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" title="Total de Registros Emitidos o Cancelados">
																						<f:facet name="header"><h:outputText value="#{msgs.cedularo}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegOk}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" title="Total de Prima Emitida o Cancelada">
																						<f:facet name="header"><h:outputText value="#{msgs.cedulapo}" /></f:facet>
																						<h:outputText value="#{cedula.coccMtPrimaOk}" >
																							<f:convertNumber currencySymbol="$" type="currency"/>
																						</h:outputText>
																					</rich:column>
																					<rich:column styleClass="texto_centro" title="Total de Registros No Emitidos o No Cancelados">
																						<f:facet name="header"><h:outputText value="#{msgs.cedularno}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegNook}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedulapno}" /></f:facet>
																						<h:outputText value="#{cedula.coccMtPrimaNook}">
																							<f:convertNumber currencySymbol="$" type="currency"/>
																						</h:outputText>
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularsp}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegSinpnd}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularspo}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegSinpndOk}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularspno}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegSinpndNook}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" 
					                    																id="detallelink" 
					                    																reRender="detalleCedula,listaDetalle,detPanel" 
					                    																action="#{beanCedula.getDetalle}" 
					                    																oncomplete="#{rich:component('detPanel')}.show();
					                    																			muestraOcultaSeccion('actual', 1);
					                    																			muestraOcultaSeccion('historico', 0);">
					                        												<h:graphicImage value="../../images/certificados.png" styleClass="bor0" />                        												
					                        												<f:setPropertyActionListener value="#{cedula}" target="#{beanCedula.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="detallelink" value="Ver Detalle" />
																                	</rich:column>
																					
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaActual" maxPages="10" page="#{beanCedula.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
											
											<table class="tablaCen90">
												<caption></caption>
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secHistorico" ajaxRendered="true">
																	<span id="img_sec_historico_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('historico', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_historico_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('historico', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.cedulahistorico}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTablaP" ajaxRendered="true">
																<table class="tablaCen100">
																	<caption></caption>
																	<tr class="texto_normal" id="tr_sec_historico">
																		<td>
																			<c:if
																				test="${beanCedula.listaHist == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanCedula.listaHist != null}">
																				<rich:dataTable id="listaHist" value="#{beanCedula.listaHist}"
																						columnsWidth="10px,8px,8px,8px,8px,8px,8px,8px,8px,8px"
																						var="cedula" rows="15" width="100%">
																					<f:facet name="header">
																						<h:outputText value="#{msgs.cedulaactual}" />
																					</f:facet>

																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularemesa}" /></f:facet>
																						<h:outputText value="#{cedula.coccRemesaCedula}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" title="Total de Registros">
																						<f:facet name="header"><h:outputText value="#{msgs.cedular}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegistros}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro"  title="Total de Registros PND">
																						<f:facet name="header"><h:outputText value="#{msgs.cedularp}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegPnd}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" title="Total de Registros Emitidos o Cancelados">
																						<f:facet name="header"><h:outputText value="#{msgs.cedularo}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegOk}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" title="Total de Prima Emitida o Cancelada">
																						<f:facet name="header"><h:outputText value="#{msgs.cedulapo}" /></f:facet>
																						<h:outputText value="#{cedula.coccMtPrimaOk}" >
																							<f:convertNumber currencySymbol="$" type="currency"/>
																						</h:outputText>
																					</rich:column>
																					<rich:column styleClass="texto_centro" title="Total de Registros No Emitidos o No Cancelados">
																						<f:facet name="header"><h:outputText value="#{msgs.cedularno}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegNook}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedulapno}" /></f:facet>
																						<h:outputText value="#{cedula.coccMtPrimaNook}">
																							<f:convertNumber currencySymbol="$" type="currency"/>
																						</h:outputText>
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularsp}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegSinpnd}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularspo}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegSinpndOk}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.cedularspno}" /></f:facet>
																						<h:outputText value="#{cedula.coccNuRegSinpndNook}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" 
					                    																id="detallelink" 
					                    																reRender="detalleCedula,listaDetalle,detPanel" 
					                    																action="#{beanCedula.getDetalle}" 
					                    																oncomplete="#{rich:component('detPanel')}.show();
					                    																		muestraOcultaSeccion('actual', 0);
					                    																		muestraOcultaSeccion('historico', 1);">
					                        												<h:graphicImage value="../../images/certificados.png" styleClass="bor0"  />                        												
					                        												<f:setPropertyActionListener value="#{cedula}" target="#{beanCedula.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="detallelink" value="Ver Detalle" />
																                	</rich:column>
																					
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaHist" maxPages="10" page="#{beanCedula.numPagina1}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if>
																		</td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		
		<%@include file="/WEB-INF/includes/modalDetalleCedula.jsp"%>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>