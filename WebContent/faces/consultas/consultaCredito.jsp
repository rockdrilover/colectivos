<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta por Cr�dito</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Consulta de certificados</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form>
				    <div align="center">
					    <rich:spacer height="15px"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="600px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
								    	<tr>
								    		<td></td>
								     		<td align="right"><h:outputText value="Numero Identificador:" /></td>
								     		<td align="left"><h:inputText value="#{beanListaClienteCertif.idCertificado}" />
								     		<td></td>
							     		</tr>
							     		<tr>
							     			<td></td>
								     		<td align="right"><h:outputText value="Nombre:" /></td>
								     		<td align="left"><h:inputText id="nombre"  value="#{beanListaClienteCertif.nombre}"/>
								     		<td></td>
							     		</tr>
							     		<tr>
							     			<td></td>
								     		<td align="right"><h:outputText value="Apellido Paterno:" /></td>
								     		<td align="left"><h:inputText id="ap"  value="#{beanListaClienteCertif.apellidoPaterno}"/>
								     		<td></td>
							     		</tr>
				                        <tr>
				                        	<td></td>
								     		<td align="right"><h:outputText value="Apellido Materno:" /></td>
								     		<td align="left"><h:inputText id="am"  value="#{beanListaClienteCertif.apellidoMaterno}"/>
								     		<td></td>
							     		</tr>
							     		<tr>
							     			<td></td>
							     			<td></td>
							     			<td align="right">
							     				<a4j:commandButton styleClass="boton" value="Consultar" 
															   action="#{beanListaClienteCertif.consulta}" 
															   onclick="this.disabled=true" oncomplete="this.disabled=false" 
															   reRender="listaCertificados"
															   title="header=[Consulta certificados] body=[Consulta los certificados dependiendo del filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
							     			<td></td>
							     		</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    <table class="botonera">
						    <tbody>
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="600px">
								     	<table>
								     	<tbody>
									     	<tr>
									     		<td align="center">
									     		
										      	<rich:dataTable id="listaCertificados" value="#{beanListaClienteCertif.listaClientesCertificados}" columns="15" columnsWidth="10px,10px,200px,8px,8px,15px,15px,20px,20px,8px,8px,8px,8px,8px" var="beanClienteCertif" rows="10">
										      		<f:facet name="header">
										      			<rich:columnGroup>
										      				<rich:column colspan="1" rowspan="2"><h:outputText value="ID"/></rich:column>
										      				<rich:column colspan="1" rowspan="2"><h:outputText value="ID2"/></rich:column>
										      				<rich:column colspan="1" rowspan="2"><h:outputText value="Cliente" /></rich:column>
										      				<rich:column colspan="6"><h:outputText value="Datos certificado" /></rich:column>
										      				<rich:column colspan="1" rowspan="2"><h:outputText value="Emisi�n" /></rich:column>
										      				<rich:column colspan="2"><h:outputText value="Vigencia" /></rich:column>
										      				<rich:column colspan="1" rowspan="2"><h:outputText value="Monto Devoluci�n Calculado" /></rich:column>
										      				<rich:column colspan="1" rowspan="2"><h:outputText value="Monto Devoluci�n Reportado" /></rich:column>
										      				<rich:column colspan="1" rowspan="2"><h:outputText value="Fecha Anulaci�n" /></rich:column>
										      				<rich:column breakBefore="true"><h:outputText value="Canal" /></rich:column>
										      				<rich:column><h:outputText value="Ramo" /></rich:column>
										      				<rich:column><h:outputText value="P�liza" /></rich:column>
										      				<rich:column><h:outputText value="Certificado" /></rich:column>
										      				<rich:column><h:outputText value="Estatus Colectivo" /></rich:column>
										      				<rich:column><h:outputText value="Estatus General" /></rich:column>
										      				<rich:column><h:outputText value="Desde" /></rich:column>
										      				<rich:column><h:outputText value="Hasta" /></rich:column>
										      			</rich:columnGroup>
										      		</f:facet>
										      		<rich:column>
											      		<h:outputText value="#{beanClienteCertif.id.coccIdCertificado}" />		      		  			
										      		</rich:column>
										      		<rich:column>
											      		<h:outputText value="#{beanClienteCertif.certificado.coceBucEmpresa}" />		      		  			
										      		</rich:column>
										      		<rich:column>
										      			<h:outputText value="#{beanClienteCertif.cliente.cocnNombre} #{beanClienteCertif.cliente.cocnApellidoPat} #{beanClienteCertif.cliente.cocnApellidoMat}" />		      		  			
										      		</rich:column>
										      	    <rich:column>  
														<h:outputText value="#{beanClienteCertif.id.coccCasuCdSucursal}" />      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.id.coccCarpCdRamo}" />      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.id.coccCapoNuPoliza}" />      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.id.coccNuCertificado}" />      		  			
										      		</rich:column>
										      		<rich:column>  
										      			<h:outputText value="#{beanClienteCertif.certificado.estatus.alesDescripcion}" />      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.certificado.estatus.alesCampo1}" />      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.certificado.coceFeEmision}" >
															<f:convertDateTime pattern="dd/MM/yyyy"/>
														</h:outputText>      		  			
										      		</rich:column> 
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.certificado.coceFeDesde}" >
															<f:convertDateTime pattern="dd/MM/yyyy"/>
														</h:outputText>      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.certificado.coceFeHasta}" >
															<f:convertDateTime pattern="dd/MM/yyyy"/>
														</h:outputText>      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.certificado.coceMtDevolucion}" />      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.certificado.coceMtBcoDevolucion}" />      		  			
										      		</rich:column>
										      		<rich:column>  
														<h:outputText value="#{beanClienteCertif.certificado.coceFeAnulacionCol}" >
															<f:convertDateTime pattern="dd/MM/yyyy"/>
														</h:outputText>      		  			
										      		</rich:column>
										      		<f:facet name="footer">
										      			<rich:datascroller align="right" for="listaCertificados" maxPages="10" page="#{beanListaClienteCertif.numPagina}"/>
										      		</f:facet>
										      	</rich:dataTable>
										      	
											      </td>
											     </tr>
										</tbody>
										</table>
									<td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>