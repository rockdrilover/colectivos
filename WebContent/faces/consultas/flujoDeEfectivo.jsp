<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<script type="text/javascript" src="../../js/colectivos.js"></script>
<title>Flujo de Efectivo</title>
</head>

<body>
	<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
		<div align="center">
			<%@include file="../header.jsp" %>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>	
						<td align="center">
						    <table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<table class="encabezadoTabla" >
												<tbody>
													<tr>
														<td width="10%" align="center">Secci�n</td>
														<td width="4%" align="center" bgcolor="#d90909">||||</td>
														<td align="left" width="64%">Pantalla de Flujo de Efectivo</td>
														<td align="right" width="14%" >Fecha:</td>
														<td align="left" width="10%">
															<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
														</td>
													</tr>
												</tbody>
											</table>
											<div align="center">
												<rich:spacer height="15"/>	
												<table class="botonera">
													<h:form id="Efectivo">
														<a4j:region id="filtroCarga">
															<tr>
																<td colspan="1" align="left">
																	<a4j:outputPanel id="filtro" ajaxRendered="true">
																	<fieldset style="border: 1px solid white">
																		<table  border="0" align="center">
																			<tr><td colspan="6">&nbsp;</td></tr>
																			<tr><td align="center" colspan="6">
																					<h:outputText id="respuestas" value="#{beanFlujoDeEfectivo.strRespuesta}" styleClass="respuesta" />
																				</td>
																			</tr>
																			<tr><td colspan="6">&nbsp;</td></tr>
																			<tr>
																				<td align="left">
																					<h:outputText value="Operaci�n : " />
																				</td>	 
																				<td align="left">
													                    			<h:selectOneMenu value="#{beanFlujoDeEfectivo.operacion}" >
																	     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
																	     				<f:selectItem itemValue="1" itemLabel="Cargos"/>
																	     				<f:selectItem itemValue="2" itemLabel="Abonos"/>
																	     			</h:selectOneMenu>											     		
								                   								</td>
																				<td align="left">
																					<h:outputText value="Cuenta : " />
																				</td>	 
																				<td align="left">
													                    			<h:selectOneMenu value="#{beanFlujoDeEfectivo.cuenta}" >
																	     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
																	     				<f:selectItems value="#{beanFlujoDeEfectivo.listaComboCuentas}"/>
																	     			</h:selectOneMenu>											     		
								                   								</td>
								                   								<td align="left">
																					<h:outputText value="Mes : " />
																				</td>
																				<td align="left" >
																					<h:selectOneMenu value="#{beanFlujoDeEfectivo.mes}">
																	     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
																	     				<f:selectItems value="#{beanFlujoDeEfectivo.listaComboMes}"/>
																	     			</h:selectOneMenu>
																				</td>
																			</tr>
																			<tr><td colspan="6">&nbsp;</td></tr>
																			<tr>
																				<td align="center" colspan="6">
																					<a4j:commandButton styleClass="boton" value="Consultar" action="#{beanFlujoDeEfectivo.sacarReportes}" 
																						onclick="this.disabled=true" oncomplete="this.disabled=false"
																					 	reRender="filtro, regTabla,respuestas" />
																				</td>
																			</tr>
																		</table>
																	</fieldset>
																	</a4j:outputPanel>
																	
																	<fieldset style="border: 1px solid white">
																		<legend>
																		<a4j:outputPanel id="secRes" ajaxRendered="true">
																			<span id="img_sec_resultados_m" style="display: none;">
																				<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
																			</span>
																			<span id="img_sec_resultados_o">
																				<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
																			</span>
																			<span class="titulos_secciones">Resultados</span>
																			</a4j:outputPanel>
																		</legend>
																		
																		<a4j:outputPanel id="regTabla" ajaxRendered="true">												
																			<table width="100%" border="0" cellpadding="0" cellspacing="0">	
																				<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
																					<td>
																					<c:if test="${beanFlujoDeEfectivo.listaCerti == null}">
																						<table width="90%" align="center" border="0">
																							<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
																						</table>
																					</c:if>
																						
																					<c:if test="${beanFlujoDeEfectivo.listaCerti != null}">
																						<rich:dataTable id="repTareas1" value="#{beanFlujoDeEfectivo.listaCerti}" var="registro" columns="5" columnsWidth="20px, 20px, 20px, 20px, 20px" width="100px">
																							<f:facet name="header"><h:outputText value="#{beanFlujoDeEfectivo.strTarea}" /></f:facet>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Cuenta" /></f:facet>
																								<h:outputText value="#{registro.coceCampov1}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Ingresos" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov2}" ><f:convertNumber   pattern="$####.00"  /></h:outputText>
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Registrado Contablemente" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov3}" ><f:convertNumber   pattern="$####.00"  /></h:outputText>
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Prima en Dep�sito" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov4}" ><f:convertNumber   pattern="$####.00"  /></h:outputText>
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Emisi�n y Cancelaci�n Mismo Mes" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov5}" ><f:convertNumber   pattern="$####.00"  /></h:outputText>
																							</rich:column>
																							<f:facet name="footer">
																		                        <rich:columnGroup>
																		                            <rich:column>Totales</rich:column>
																		                            <rich:column>
																		                                <h:outputText value="#{beanFlujoDeEfectivo.nEgresos}"><f:convertNumber   pattern="$####.00"  /></h:outputText>
																		                            </rich:column>
																		                            <rich:column>
																		                                <h:outputText value="#{beanFlujoDeEfectivo.nContable}"><f:convertNumber   pattern="$####.00"  /></h:outputText>
																		                            </rich:column>
																		                            <rich:column>
																		                                <h:outputText value="#{beanFlujoDeEfectivo.nCargo}"><f:convertNumber   pattern="$####.00"  /></h:outputText>
																		                            </rich:column>
																		                            <rich:column>
																		                                <h:outputText value="#{beanFlujoDeEfectivo.nEmiCan}"><f:convertNumber   pattern="$####.00"  /></h:outputText>
																		                            </rich:column>
																		                        </rich:columnGroup>
                    																		</f:facet>
																						</rich:dataTable>
																					</c:if>
																					<br/>
																					<h:commandButton action="#{beanFlujoDeEfectivo.reporteCargoAbono}" value="Reporte" />																					
																					</td>
																				</tr>
																			</table>
																		</a4j:outputPanel>
																	</fieldset>
																</td>
															</tr>
														</a4j:region>											
													</h:form>
												</table>
											</div>
											<h:messages styleClass="errorMessage" globalOnly="true"/>			
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div align="center">
			<%@ include file="../footer.jsp" %>
		</div>
	</f:view>
</body>
</html>