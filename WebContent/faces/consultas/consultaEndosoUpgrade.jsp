<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta Mensual Emisi�n</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
						<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Consulta Endosos Upgrade</td>
									<td align="right" width="12%">Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
						</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmEndososUpgrade">
				      <div align="center">
					    <rich:spacer height="15px"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="700px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
								     	<tr>
								     		<td width="15%"></td>
								     		<td align="center">
	     	                                	<h:outputText value="Fecha inicial" />
											</td>
								     		<td align="center">
	                                        	<h:outputText value="Fecha final" />
											</td>
											<td width="15%"></td>
								     	</tr>
						                <tr>
						                	<td></td>
											<td align="center">
		                                        <rich:calendar id="fechaInicial" popup="false" datePattern="dd/MM/yyyy" required="true" value="#{beanListaCertificado.fecini}"  />
		                                        <h:message for="fechaInicial" styleClass="errorMessage" />
	     	                                </td>
	     	                                <td align="left">
		                                        <rich:calendar id="fechaFinal" popup="false" datePattern="dd/MM/yyyy" required="true" value="#{beanListaCertificado.fecfin}" />
		                                        <h:message for="fechaFinal" styleClass="errorMessage" />
	                                        </td>
	                                        <td></td>
	                                    </tr>
	                                    <tr>
	                                    	<td colspan="2" align="right">
	                                    		<h:outputText value="Nombre Empresa:" />
	                                    	</td>
	                                    	<td colspan="2" align="left">
	                                    		<h:inputText id="empresa" value="#{beanListaCertificado.empresa}"></h:inputText>
	                                    	</td>
	                                    </tr>
	                                    <tr>
	                                    	<td colspan="2" align="right">
	                                    		<h:outputText value="Contrato Enlace:" />
	                                    	</td>
	                                    	<td colspan="2" align="left">
	                                    		<h:inputText id="contratoEnlace" value="#{beanListaCertificado.contratoEnlace}"></h:inputText>
	                                    	</td>
	                                    </tr>
	                                    <tr>
	                                    	<td colspan="2" align="right">
	                                    		<h:outputText value="Folio Endoso:" />
	                                    	</td>
	                                    	<td colspan="2" align="left">
	                                    		<h:inputText id="folioEndoso" value="#{beanListaCertificado.folioEndoso}" title="header=[Folio endoso] body=[Folio formado por {poliza}{buc empresa}] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"></h:inputText>
	                                    	</td>
	                                    </tr>
										<tr height="10px">
											<td colspan="4" ></td>
										</tr>
	           	                		<tr>
	           	                			<td>
	           	                			</td>
	           	                		  	<td colspan="2" align="right">
	           	                		  	<h:commandButton action="#{beanListaCertificado.generarReporteEndosoUpgrade}" value="Generar Endoso y Soporte" />  
											</td>
											<td></td>
										</tr>	
										<tr>
											<td colspan="4" align="center">
												<br/>
												
												<h:outputText id="mensaje" value="#{beanListaCertificado.mensaje}" styleClass="respuesta"/>
												<br/>
												<br/>
											</td>
										</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
					    </table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>