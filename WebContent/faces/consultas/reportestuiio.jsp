<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Reportes
- Fecha: 09/10/2020
- Descripcion: Pantalla para Reportes de TUIIO
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%> 


<f:view> 
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallavi}" /></title>
	</head>

	<body>
		<h:form id="frmRepTuiio">
		<div class="center_div"><%@include file="../header.jsp" %></div> 
		<div class="center_div">
			<table class="tablaPrincipal">			
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div class="center_div">
									<table class="encabezadoTabla" >
										<caption></caption>
										<tr>
											<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
											<td class="encabezado2"><h:outputText value="#{msgs.sistemavi}" /></td>
											<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallavi}" /></td>
											<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
											<td class="encabezado5">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    				<rich:spacer height="10px"></rich:spacer>
				    
				    				<div class="center_div">
				    					<h:inputHidden value="#{beanReportesTuiio.bean}" id="hidLoad" ></h:inputHidden>
					    				<table class="tablaCen90">
					    					<caption></caption>
						    				<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											<tr>
												<td>
													<h:outputText id="mensaje" value="#{beanReportesTuiio.strRespuesta}" styleClass="error"/>
												</td>
											</tr>	
											<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
					    				</table>
				    						    								    				
					    				<a4j:region id="consultaDatos">
					    				<table class="tablaCen90">
										<tr>
											<td>
												<fieldset class="borfieldset">
													<legend>
														<span id="img_sec_consulta_m">
															<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
														</span>
														<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaacciones}" /></span>
													</legend>
													
													<table class="tablaCen90">	
														<caption></caption>
														<tr class="texto_normal">
															<td class="right-div">
																<h:outputText value="#{msgs.tuiiotprep}:" />
															</td>
															<td class="left80">
																<h:selectOneMenu id="inTipoParam" value="#{beanReportesTuiio.tipoReporte}">
																	<f:selectItem itemValue="0" itemLabel="#{msgs.sistemaseleccione}" />
																	<f:selectItem itemValue="1" itemLabel="#{msgs.tuiiorep1}" />
																	<f:selectItem itemValue="2" itemLabel="#{msgs.tuiiorep2}" />
																</h:selectOneMenu>	
															</td>
														</tr>
							                    		<tr class="texto_normal">
															<td class="right15">
																<h:outputText value="#{msgs.sistemafechad}:" />
															</td>
															<td class="left30">
																<rich:calendar id="fechIni" popup="true"
																		value="#{beanReportesTuiio.feDesde}"
																		datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
															</td>
														</tr>
														<tr class="texto_normal">
															<td class="right15">
																<h:outputText value="#{msgs.sistemafechah}:" />
															</td>
															<td class="left30">
																<rich:calendar id="fechFin" popup="true"
																		value="#{beanReportesTuiio.feHasta}"
																		datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
															</td>
														</tr>	
							                    		<tr class="texto_normal">
							                    			<td class="right-div" colspan="2">	
																<h:commandButton id="btnReporte" 
                                                            			action="#{beanReportesTuiio.consulta}"
                                                            			onclick="bloqueaPantalla();" onblur="desbloqueaPantalla();"
                                                                		value="Extraer Reporte">
                                                                	<a4j:support event="onclick"
                                                                			ajaxSingle="true"                                                                		
                                                                    		reRender="mensaje" />
                                                            	</h:commandButton>															
																<rich:toolTip for="btnReporte" value="Extrae Reporte" />													
															</td>
							                    		</tr>
							                    		
							                    		<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
							                    		
							                    	</table>
							                    </fieldset>
											</td>
										</tr>
										<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
					    				</table>
					    				</a4j:region>
									</div>
								</td>
							</tr>
   						</table>
   					</td>
   				</tr>
			</table>   	
		</div>
		</h:form>
		<rich:spacer height="10px"></rich:spacer>
		
		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
    	
		<div><%@include file="../footer.jsp" %></div>
    	
	</body>
</html>
</f:view>