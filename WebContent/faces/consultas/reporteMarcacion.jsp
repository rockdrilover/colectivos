<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title>Reporte Calificador </title>
	</head>

	<body>
	<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
		<div align="center"><%@include file="../header.jsp" %></div>
		
		<div align="center">
			<table class="tablaPrincipal" >
				<tr>
					<td>
						<table class="tablaSecundaria">
							<tr>
								<td align="center">
									<div align="center">
										<table class="encabezadoTabla">
											<tr>
												<td width="10%" align="center">Secci�n</td>
												<td width="4%" align="center" bgcolor="#d90909">-</td>
												<td align="left" width="64%"> Reporte Calificador </td>	
												<td align="right" width="12%" >Fecha</td>
												<td align="left" width="10%">
													<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
												</td>
											</tr>
										</table>
						    		</div>
					    		
					    			<rich:spacer height="10px"></rich:spacer>
					    		
					    			<h:form id="frmReporteMarcacion">
					    			<div align="center">
						    			<rich:spacer height="15"/>	
						    			<table class="botonera">
									    	<tr>
											    <td class="frameTL"></td>
											    <td class="frameTC"></td>
											    <td class="frameTR"></td>
										    </tr>
							    			<tr>
								    			<td class="frameCL"></td>
								    			<td class="frameC" width="700px">
												   <a4j:region id="Marcacion">
													<table>
									    				<tr>
									    					<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
									    					<td align="left" width="80%" >
									    						<h:selectOneMenu id="inRamo" value="#{beanMarcacion.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
												    				<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
												    				<f:selectItem itemValue="-1" itemLabel="0 - TODOS LOS RAMOS"/>
												    				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
												    				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas"/>
												    			</h:selectOneMenu>
									    					</td>
									    				</tr>
									    	
												    	<tr><td>&nbsp;</td></tr>
									    		
											    		<tr>
												    		<td align="right" width="20%"><h:outputText value="Poliza:"/></td>
															<td align="left" width="80%" >
																<a4j:outputPanel id="polizas">
																	<h:selectOneMenu id="inPoliza"  value="#{beanMarcacion.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" style=" width : 190px;">
																		<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																		<f:selectItem itemValue="-1" itemLabel="0 - TODAS LAS POLIZAS"/>
																		<!--f:selectItem itemValue="0" itemLabel="Prima �nica"/-->
																		<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																	</h:selectOneMenu>
																</a4j:outputPanel>
															</td>				
									    				</tr>
									    	
												    	<tr><td>&nbsp;</td></tr>
									    	
												    	<tr>
												    		<td align="right" width="20%"><h:outputText value="Status:"/></td>
															<td align="left" width="80%" >
																<a4j:outputPanel id="statuss">
																	<h:selectOneMenu id="status"  value="#{beanMarcacion.status}" binding="#{beanListaParametros.inStatus}" title="header=[Status] body=[Seleccione un status.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" style=" width : 190px;">
																		<f:selectItem itemValue=""   itemLabel="Seleccione un status"/>
																		<f:selectItem itemValue="-1" itemLabel="0 - TODOS LOS STATUS"/>
																		<f:selectItem itemValue="1"  itemLabel="Vigente"/>
																    	<f:selectItem itemValue="2"  itemLabel="Cancelado"  /> 
																	</h:selectOneMenu>
																</a4j:outputPanel>
															</td>				
												    	</tr>
									    			</table>
									                </a4j:region>
									    			<rich:spacer height="15px"></rich:spacer>									    
									    			<table>
										    			<tr>
										    				<td align="right" width="20%" height="30"><h:outputText value="Cargar archivo:" /></td>
										    				<td align="left" width="80%" height="30" title="header=[Archivo] body=[Archivo a cargar extensi�n CSV, cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
										    					<rich:fileUpload id="uploadMarcacion" fileUploadListener="#{beanMarcacion.listener}"
								    								addControlLabel="Examinar" uploadControlLabel="Cargar" maxFilesQuantity="5"   
								    								stopControlLabel="Detener" immediateUpload="true" 
								    								clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" styleClass="archivo" acceptedTypes="csv"
								    								listHeight="65px" addButtonClass="botonArchivo" listWidth="70%">
								    								
										    						<f:facet name="label">
										    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
										    						</f:facet>
										    					</rich:fileUpload>
										    				</td>
										    			</tr>
													</table>
									    			
									    			<rich:spacer height="15px"></rich:spacer>										
													<table>
										    		    <tr>
										    		        <td align="center">
										    			        <h:commandButton action="#{beanMarcacion.marcacion}" value="Consulta Masiva" id="uno"
			  															title="header=[Consulta] body=[Descarga el detalle del archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
															</td>
										    			</tr>
													</table>
												</td>
												<td class="frameCR"></td>
											</tr>
											<tr>
												<td class="frameBL"></td>
												<td class="frameBC"></td>
												<td class="frameBR"></td>
											</tr>
										</table>
						    			
						    			<table class="botonera"></table>
						   			</div>
					    			</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		
		<rich:spacer height="10px"></rich:spacer>
		
		<div align="center"><%@ include file="../footer.jsp" %></div>
	</f:view>
	</body>
</html>