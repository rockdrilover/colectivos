<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<title>Consulta por Recibo</title>
	</head>

	<body>
	<f:view>  <!-- Desde aqui comienzo --> 
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
		<table class="tablaPrincipal">
		<tbody>
			<tr>
				<td align="center">
					<table class="tablaSecundaria">
					<tbody>
						<tr>
							<td>
								<div align="center">
								<table class="encabezadoTabla" >
								<tbody>
									<tr>
										<td width="10%" align="center">Secci�n</td>
										<td width="4%" align="center" bgcolor="#d90909">-</td>
										<td align="left" width="64%">Consulta Tarifas</td>
										<td align="right" width="12%" >Fecha</td>
										<td align="left" width="10%">
											<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
										</td>
									</tr>
								</tbody>
								</table>
				    			</div>	
				    
				    			<rich:spacer height="10px"></rich:spacer>
				    			<h:form id="frmConsultaTarifas">
				    				<div align="center">
					    			<rich:spacer height="15"/>	
					    			<table class="botonera">
					    			<tbody>
						    			<tr>
										    <td class="frameTL"></td>
										    <td class="frameTC"></td>
										    <td class="frameTR"></td>
									    </tr>
						    			<tr>
										    <td class="frameCL"></td>
										    <td class="frameC" width="600px">
							   					<a4j:region id="consultaDatos">
												    <table cellpadding="0" cellspacing="0">
												    	<tr>
												    		<td width="80%">
												    			<table width="100%">
												    			<tr><td>&nbsp;</td></tr>
												    				<tr>
															    		<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
															    		<td align="left" width="80%">
																			<h:selectOneMenu id="inRamo" value="#{beanListaTarifas.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
																				<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
																				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																				<a4j:support event="onchange"  action="#{beanListaTarifas.cargaProductos}" ajaxSingle="true" reRender="productos" />
																			</h:selectOneMenu>
																			<h:message for="inRamo" styleClass="errorMessage" />
																		</td>
														     		</tr>
														     		
														     		<tr><td>&nbsp;</td></tr>
														     		
														     		<tr>
															    		<td align="right" width="20%"><h:outputText value="Producto:" /></td>
															    		<td align="left" width="80%">
																    		<a4j:outputPanel id="productos" ajaxRendered="true">
																    			<h:selectOneMenu id="inProducto" value="#{beanListaTarifas.valorProducto}" required="true" title="header=[Producto] body=[Seleccione un producto.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																					<f:selectItem itemValue="0" itemLabel="Selecione un producto" />
																					<f:selectItems value="#{beanListaTarifas.listaProductos}" />
																				</h:selectOneMenu>
																			   <h:message for="inProducto" styleClass="errorMessage" />	
															    			</a4j:outputPanel>
																		</td>
														     		</tr>
												    			</table>
												    		</td>	
												    		<td width="20%">
												    			<table width="100%">
												    			<tr><td>&nbsp;</td></tr>
												    				<tr>							     		
										     							<td align="center">
										     								<a4j:commandButton styleClass="boton" value="Consultar"
																			   action="#{beanListaTarifas.consultar}" 
																			   onclick="this.disabled=true" oncomplete="this.disabled=false" 
																			   reRender="listaTarifas"
																			   title="header=[Consulta Tarifas] body=[Consulta de tarifas.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">																			   
																			</a4j:commandButton>
										     							</td>
										     						</tr>
										     						
										     						<tr><td>&nbsp;</td></tr>
										     						
										     						<tr>
										     							<td align="center">
										     								<h:commandButton styleClass="boton" value="Genera Reporte" 
																			   action="#{beanListaTarifas.generaReporte}" 
																			   title="header=[Reporte Tarifas] body=[Reporte tarifas.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
										     							</td>
							     									</tr>
												    			</table>
												    		</td>											    	
												    	</tr>
								    				</table>
								    				
								    				<rich:spacer height="15px"></rich:spacer>
								    				<table>
							    					<tbody>
											    		<tr>
											    			<td align="center">
												    			<a4j:status for="consultaDatos" stopText=" ">
																	<f:facet name="start">
																		<h:graphicImage value="/images/ajax-loader.gif" />
																	</f:facet>
																</a4j:status>
															</td>
														</tr>
													</tbody>
													</table>
												</a4j:region>
											</td>								    
							    			<td class="frameCR"></td>
										</tr>
									    <tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
						    		</tbody>
						    		</table>
						    		<rich:spacer height="10px"></rich:spacer>
						    		<table class="botonera">
						    		<tbody>
									    <tr>
										    <td class="frameTL"></td>
										    <td class="frameTC"></td>
										    <td class="frameTR"></td>
									    </tr>
									   
							    		<tr>
										    <td class="frameCL"></td>
										    <td class="frameC" width="600px">
									     		<table> 
									     		<tbody>
										     		<tr>
										     			<td align="center">
													      	<rich:dataTable value="#{beanListaTarifas.listaTarifas}" id="listaTarifas" var="beanTarifas" rows="10" width="100%" rowKeyVar="valor">																
																<f:facet name="header">																	      	
																	<rich:columnGroup>
																		<rich:column colspan="10"><h:outputText value="Informacion Tarifas" /></rich:column>																																																																
																		<rich:column breakBefore="true" colspan="2" width="50%"><h:outputText value="Descripcion Plan" /></rich:column>
																		<rich:column><h:outputText value="Tarifa Total Cobertura" /></rich:column>												
																		<rich:column><h:outputText value="Tarifa Total Operativa" /></rich:column>
																		<rich:column><h:outputText value="Centro de Costos" /></rich:column>
																		<rich:column><h:outputText value="Desde" /></rich:column>
																		<rich:column><h:outputText value="Hasta" /></rich:column>
																		<rich:column colspan="3"><h:outputText value="Detalle" /></rich:column>
																											
																	</rich:columnGroup>
																</f:facet>
																
																<rich:column width="30%"  style="border: 0px;"><h:outputText style="font-size:9px" value="#{beanTarifas.plan} - #{beanTarifas.descplan}" /></rich:column>
																<rich:column width="20%"><h:outputText style="font-size:9px" value="#{beanTarifas.descnegocio}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanTarifas.tarifa1}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanTarifas.tarifa2}" /></rich:column>
																<rich:column><h:outputText style="font-size:9px" value="#{beanTarifas.centro}" /></rich:column>
																<rich:column>
																	<h:outputText style="font-size:9px" value="#{beanTarifas.fecini}">
																		<f:convertDateTime pattern="dd/MM/yyyy"/>
																	</h:outputText>
																</rich:column>
																<rich:column>
																	<h:outputText style="font-size:9px" value="#{beanTarifas.fecfin}">
																		<f:convertDateTime pattern="dd/MM/yyyy"/>
																	</h:outputText>
																</rich:column>
																<rich:column>
																	<h:graphicImage onclick="verDetalle(1, #{valor}, #{fn:length(beanListaTarifas.listaTarifas)});" value="/images/coberturas.png" title="header=[Coberturas] body=[Dar click para mostrar detalle de las coberturas.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																</rich:column>
													      		<rich:column>
													      			<h:graphicImage onclick="verDetalle(2, #{valor}, #{fn:length(beanListaTarifas.listaTarifas)});" value="/images/componentes.png" title="header=[Componentes] body=[Dar click para mostrar detalle de los componentes.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
													      		</rich:column>
													      		<rich:column>
													      			<h:graphicImage onclick="verDetalle(3, #{valor}, #{fn:length(beanListaTarifas.listaTarifas)});" value="/images/prodplanes.png" title="header=[Productos/Planes] body=[Dar click para mostrar detalle de los productos y planes.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
													      		</rich:column>
																																
																<rich:column breakBefore="true" colspan="11" id="colCoberturas" style="display: none" >
																	<rich:dataTable value="#{beanTarifas.lstCoberturas}" id="listaCoberturas" var="cobertura" headerClass="detalleEncabezado2" styleClass="detalleEncabezado">
																		<f:facet name="header">																	      	
																			<rich:columnGroup>
																			<rich:column colspan="10" styleClass="detalleInfo"><h:outputText value="Coberturas" /></rich:column>																																						
																				<rich:column breakBefore="true"><h:outputText value="Codigo" /></rich:column>											
																				<rich:column><h:outputText  value="Descripci�n" /></rich:column>													
																				<rich:column><h:outputText value="Tarifa" /></rich:column>											
																				<rich:column><h:outputText value="Status" /></rich:column>												
																				<rich:column><h:outputText value="Desde" /></rich:column>
																				<rich:column><h:outputText value="Hasta" /></rich:column>
																				<rich:column><h:outputText value="Edad Min" /></rich:column>
																				<rich:column><h:outputText value="Edad Max" /></rich:column>
																				<rich:column><h:outputText value="% Suma" /></rich:column>
																				<rich:column><h:outputText value="Set Coberturas" /></rich:column>
																			</rich:columnGroup>
																		</f:facet>

																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.cdcobertura}"/></rich:column>										
																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.desccobertura}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.tarifa1}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.status}" /></rich:column>
																		<rich:column>
																		    <h:outputText style="font-size:9px" value="#{cobertura.fecini}">
																		    	<f:convertDateTime pattern="dd/MM/yyyy"/>
																		    </h:outputText>
																		</rich:column>
																		<rich:column>
																		     <h:outputText style="font-size:9px" value="#{cobertura.fecfin}">
																		     	<f:convertDateTime pattern="dd/MM/yyyy"/>
																		     </h:outputText>
																		</rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.edadmin}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.edadmax}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.porcentaje}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{cobertura.setcoberturas}" /></rich:column>
																	</rich:dataTable>
																	
																</rich:column>
															
																<rich:column breakBefore="true" colspan="11" id="colComponentes" style="display: none" >	
																	<rich:dataTable value="#{beanTarifas.lstComponentes}" id="listaComponentes" var="componente" headerClass="detalleEncabezado2" styleClass="detalleEncabezado" >
																		<f:facet name="header">																	      	
																			<rich:columnGroup>
																			<rich:column colspan="6" styleClass="detalleInfo"><h:outputText value="Componentes" /></rich:column>
																				<rich:column breakBefore="true"><h:outputText value="Componente" /></rich:column>											
																				<rich:column><h:outputText value="Tasa" /></rich:column>													
																				<rich:column><h:outputText value="Status" /></rich:column>												
																				<rich:column><h:outputText value="Desde" /></rich:column>
																				<rich:column><h:outputText value="Hasta" /></rich:column>
																				<rich:column><h:outputText value="Set Componentes		" /></rich:column>
																			</rich:columnGroup>
																		</f:facet>
																		
																		<rich:column><h:outputText style="font-size:9px" value="#{componente.cdcomponente}"/></rich:column>										
																		<rich:column><h:outputText style="font-size:9px" value="#{componente.tasacomponente}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{componente.status}" /></rich:column>
																		<rich:column>
																		    <h:outputText style="font-size:9px" value="#{componente.fecini}">
																		    	<f:convertDateTime pattern="dd/MM/yyyy"/>
																		    </h:outputText>
																		</rich:column>
																		<rich:column>
																		     <h:outputText style="font-size:9px" value="#{componente.fecfin}">
																		     	<f:convertDateTime pattern="dd/MM/yyyy"/>
																		     </h:outputText>
																		</rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{componente.setcoberturas}" /></rich:column>
																																			
																	</rich:dataTable>
																</rich:column>
																
																
																<rich:column breakBefore="true" colspan="11" style="display: none" id="colProdPlan">
																<rich:dataTable value="#{beanTarifas.lstProdPlanes}" id="listaProdPlanes" var="beanProdPlanes" headerClass="detalleEncabezado2" styleClass="detalleEncabezado">
																		<f:facet name="header">																	      	
																			<rich:columnGroup>
																				<rich:column colspan="6" styleClass="detalleInfo"><h:outputText value="Productos / Planes" /></rich:column>																																						
																				<rich:column breakBefore="true"><h:outputText value="Producto Banco" /></rich:column>													
																				<rich:column><h:outputText value="Plan Banco" /></rich:column>											
																				<rich:column><h:outputText value="Desc. Producto" /></rich:column>												
																				<rich:column><h:outputText value="Identificador" /></rich:column>
																			</rich:columnGroup>
																		</f:facet>
																		
																		<rich:column><h:outputText style="font-size:9px" value="#{beanProdPlanes.subproducto}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{beanProdPlanes.tipo}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px; text-transform: uppercase;" value="#{beanProdPlanes.descproducto}" /></rich:column>
																		<rich:column><h:outputText style="font-size:9px" value="#{beanProdPlanes.identificador}" /></rich:column>
																		
																</rich:dataTable>
																</rich:column>
																
																<f:facet name="footer">										
																	<rich:datascroller align="right" for="listaTarifas" maxPages="10" page="1"/>
																</f:facet>
																
															</rich:dataTable>
														</td>
													</tr>
												</tbody>
												</table>
											</td>
											<td class="frameCR"></td>
								    	</tr>
									    <tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
							    	</tbody>
							    	</table>
									</div>
			     				</h:form>
							</td>
						</tr>
					</tbody>
			   		</table>
   				</td>
   			</tr>
		</tbody>
		</table>   	
		</div>
		<rich:spacer height="15px"></rich:spacer>
		<div><%@include file="../footer.jsp" %></div>
	</f:view>
	</body>
</html>