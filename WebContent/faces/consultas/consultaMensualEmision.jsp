<%--
 - Autor:
 - Aplicaci�n:
 - Modulo:
 - Fecha: 
 
 - Co-Autor: JJFM Juan Jose Flores.
 - Fecha Modificaci�n: 22/10/2019
 - Descripci�n: Muestra reportes mensuales.
 --%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="../../css/estilos.css">
    <script type="text/javascript" src="../../js/boxover.js"></script>
    <script type="text/javascript" src="../../js/colectivos.js"></script>
    <title>Consulta Mensual Emisi�n</title>
</head>

<body>
<f:view>
	<!-- Desde aqui comienzo -->
    <f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />
    <!-- ----------------------------------------------------------------------------------------------------------- -->
    <!-- Header -->
    <!-- ----------------------------------------------------------------------------------------------------------- -->
    <div align="center">
    	<%@include file="../header.jsp"%>
   	</div>
   
    <!-- ---------------------------------------------------------------------------------------------------------- -->
    <!-- Tabla Principal -->
    <!-- ---------------------------------------------------------------------------------------------------------- -->
    <div align="center">
    <table class="tablaPrincipal">
    <tbody>
       <tr>
      	  <td align="center">
             <table class="tablaSecundaria">
             <tbody>
                <tr>
                   <td>
                     <!--Encabezado de la tabla (Secci�n)-->
                     <div align="center">
                      	<table class="encabezadoTabla">
                        <tbody>
                           <tr>
                              <td width="10%" align="center">Secci�n</td>
                              <td width="4%" align="center" bgcolor="#d90909">||||</td>
                              <td align="left" width="64%">Consulta Mensual Emisi�n</td>
                              <td align="right" width="12%">Fecha</td>
                              <td align="left" width="10%">
                              	<input name="fecha"readonly="readonly"
                                       value="<%=request.getSession().getAttribute("fecha")%>"
                                       size="7"/>
                              </td>
                           </tr>
                       </tbody>
                       </table>
                     </div>
                     <!--FIN Encabezado de la tabla (Secci�n)-->

                     <rich:spacer height="25px"></rich:spacer>
					
					 <div align="center">
                       <h:form id="frmConsultarEmision">
                         <table>
                         <tbody>                       
                            <tr>
                               <td class="frameCL"></td>
                               <td class="frameC" width="800px">
                               		<a4j:region id="consultaDatos">
                                    	<table cellpadding="2" cellspacing="0">
                                        <tbody>
                                           <tr>
                                               <td width="30%"></td>
                                               <td width="40%"></td>
                                               <td width="30%"></td>
                                           </tr>

                                           <!-- Tipo Reporte -->
                                           <tr>
                                              <td colspan="3" align="center" height="25">
                                               	 <a4j:region id="regionTpReporte">
                                                 <rich:panel id="tpReporte"
                                                 	header="Tipo de Reporte"
                                                    style=" align:center; background-color:#E5E0EC"
                                                    styleClass="tablaSecundaria">
	                                                  <f:facet name="header">
	                                                     <h:outputText value="Tipo Reporte" />
	                                                  </f:facet>
                                                      <h:selectOneMenu id="rep"
	                                                       required="true"
	                                                       value="#{beanListaCertificado.rreporte}"
	                                                       binding="#{beanListaParametros.rreporte}">
	                                                      
	                                                       <f:selectItems value="#{beanListaParametros.darrsiTipoReporte}"/>
                                                      
                                                           <a4j:support
                                                               event="onchange"
                                                               oncomplete="boton_reporte();"
                                                               action="#{beanListaCertificado.subCleanReports}"
                                                               ajaxSingle="false"
                                                               reRender="opciones, btn_extraer_reporte, dxp_recibos, endosos" />
                                                      </h:selectOneMenu>
                                                      <h:message for="rep"
                                                          styleClass="errorMessage" />
                                                 </rich:panel>
                                                 </a4j:region>
                                              </td>
                                           </tr>
                                           <!-- Fin Tipo Reporte-->

                                           <tr>
                                               <td>
                                               <rich:spacer height="10px"></rich:spacer>
                                               </td>
                                           </tr>
                                           
                                           <!-- Opciones -->
                                           <tr>
                                         	  <td colspan="3" align="center">
                                                 <a4j:region id="regionOpciones">
                                                     <rich:panel id="opciones"
                                                     	header="Opciones"
                                                        style=" width: 100%; align:center; background-color:#E5E0EC"
                                                        styleClass="tablaSecundaria">
                                                        <f:facet name="header">
                                                        	<h:outputText value="Opciones del Reporte" />
                                                        </f:facet>
                                                        <table>
														<tbody>
													   	  <!-- OPCIONES: Seleccion de Ramo -->
                                                             <tr>
                                                                <td align="right" height="25">
                                                                   <h:outputText value="Ramo:" />
                                                                </td>
                                                                <td colspan="2" align="left" height="25">
                                                                   <h:selectOneMenu id="ramo"
                                                                       value="#{beanListaCertificado.ramo}"
                                                                       binding="#{beanListaParametros.inRamo}"
                                                                       title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
                                                                       required="true"
                                                                    >
                                                                       <f:selectItem 
                                                                       	itemValue=""
                                                                           itemLabel="Selecciona un ramo" />
                                                                       <f:selectItems 
                                                                       	value="#{beanListaRamo.listaComboRamos}" />
                                                                       <a4j:support
                                                                           event="onchange"
                                                                           action="#{beanListaParametros.cargaPolizas}"
                                                                           ajaxSingle="true"
                                                                           reRender="polizas" />
                                                                   </h:selectOneMenu>
																<h:message for="ramo" styleClass="errorMessage" />
                                                                </td>
                                                             </tr>
                                                             <!-- Termina Seleccion de Ramo -->
                                                             
                                                             <!-- OPCIONES: Seleccion de poliza -->
                                                             <tr>
                                                                <td align="right" height="25">
                                                                   <h:outputText value="Poliza:" />
                                                                </td>
                                                                <td colspan="2" align="left" height="25">
                                                                   <a4j:outputPanel id="polizas"
                                                                   	ajaxRendered="true">
                                                                       <h:selectOneMenu id="poliza"
                                                                           value="#{beanListaCertificado.poliza}"
                                                                           binding="#{beanListaParametros.inPoliza}"
                                                                           title="header=[Poliza] body=[Seleccione una poliza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
                                                                           required="true">
                                                                           <f:selectItem
                                                                           	itemValue=""
                                                                              	itemLabel="Seleccione una poliza" />
                                                                           <f:selectItem
                                                                               itemValue="0"
                                                                               itemLabel="Prima �nica" />
                                                                           <f:selectItems
                                                                               value="#{beanListaParametros.listaComboPolizas}" />
                                                                           <a4j:support
                                                                               event="onchange"
                                                                               action="#{beanListaParametros.comboPolizaOnChangeListener}"
                                                                               ajaxSingle="true"
                                                                               reRender="opciones" />
                                                                       </h:selectOneMenu>
                                                                       <h:message for="poliza" styleClass="errorMessage" />
                                                                   </a4j:outputPanel>
                                                                </td>
                                                             </tr>
														  <!-- Termina seccion Seleccion de poliza -->
                                                             
                                                             <!-- OPCIONES: Seleccion de Canal de Venta -->
                                                             <c:if test="${ beanListaCertificado.poliza == 0}">
                                                             <tr>
                                                                <td align="right" height="25">
                                                                	<h:outputText value="Canal de Venta:" />
                                                                </td>
                                                                <td colspan="2" align="left" height="25">
                                                                	<a4j:outputPanel id="idVenta"
                                                                   	ajaxRendered="true">
                                                                   	<h:selectOneMenu id="inVenta"
                                                                       	value="#{beanListaCertificado.inIdVenta}"
                                                                           title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
                                                                           required="false"
                                                                           disabled="#{beanListaParametros.habilitaComboIdVenta}">
                                                                        <f:selectItem
                                                                            itemValue=""
                                                                            itemLabel="Seleccione un canal de venta" />
                                                                        <f:selectItems
                                                                           	value="#{beanListaParametros.listaComboIdVenta}" />
                                                                        <a4j:support
                                                                            event="onchange"
                                                                            ajaxSingle="true" />
                                                                       </h:selectOneMenu>
                                                                       <h:message for="inVenta" styleClass="errorMessage" />
                                                                   </a4j:outputPanel>
                                                                </td>
                                                             </tr>
                                                            </c:if>
                                                          <!-- Termina seccion Seleccion de Canal de Venta -->
                                                          
                                                             <!-- OPCIONES: Seleccion de Vigencia -->
                                                             <c:if test="${(beanListaCertificado.rreporte == 10 || beanListaCertificado.rreporte == 11) && beanListaCertificado.poliza > 0}">
                                                             <tr>
                                                                <td align="right" height="25">
                                                                	<h:outputText value="Vigencia: " />
                                                                </td>
                                                                <td colspan="2" align="left" height="25">
                                                                	<a4j:outputPanel id="comboVigenciasPanel"
                                                                   	ajaxRendered="true">
                                                                   	<h:selectOneMenu id="comboVigencias"
                                                                           value="#{beanListaCertificado.strVigencia}"
                                                                           binding="#{beanListaParametros.comboVigencia}"
                                                                           title="header=[Vigencia] body=[Seleccione una Vigencia.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
                                                                           required="true"
                                                                          >
                                                                          <f:selectItem
                                                                          	value=""
                                                                          	itemLabel="Selecciona una vigencia"
                                                                          />
                                                                        <f:selectItems
                                                                           	value="#{beanListaParametros.darrsiVigencias}" />
                                                                        <a4j:support
                                                                            event="onchange"
                                                                            ajaxSingle="true" />
                                                                       </h:selectOneMenu>
                                                                       <h:message for="comboVigencias" styleClass="errorMessage" />
                                                                   </a4j:outputPanel>
                                                                </td>
                                                             </tr>
                                                             
                                                             <tr>
                                                               <td align="right" height="25">
                                                               		<h:outputText value="Fecha: " />
                                                               </td>
                                                               <td colspan="2" align="left" height="25">
                                                               		<rich:calendar id="fechaFinal_vig"
                                                                   		datePattern="dd/MM/yyyy"
                                                                    	value="#{beanListaCertificado.dateFechaReporteDxp}" 
                                                                    />
                                                               </td>
                                                             </tr>
                                                            </c:if>
                                                          <!-- Termina seccion Seleccion de Vigencia -->
                                                          
                                                             <!-- OPCIONES: Seleccion Poliza Especifica -->                                      
                                                         	  <c:if test="${beanListaCertificado.poliza == 0 && beanListaCertificado.rreporte != 10  && beanListaCertificado.rreporte != 11 && beanListaCertificado.rreporte != 9}">
                                                          <tr>
                                                             <td align="right" height="25">
                                                             	<h:outputText value="Poliza Especifica:" />
                                                             </td>
                                                           	 <td colspan="2" align="left" height="25">
                                                             	<a4j:outputPanel id="idPoliza"
                                                                	ajaxRendered="true">
                                                                    <h:inputText id="poliza1"
                                                                     	value="#{beanListaCertificado.poliza1}"
                                                                        required="false"
                                                                        disabled="#{beanListaParametros.habilitaPoliza}"/>
                                                                    <h:message for="poliza1" styleClass="errorMessage" />
                                                                </a4j:outputPanel>
                                                             </td>
                                                          </tr>
                                                          </c:if>
												          <!-- Termina Seleccion de Poliza Especifica --> 
												      
														  <!-- OPCIONES: Seleccion de Sstatus -->
														  <c:if test="${beanListaCertificado.rreporte == 2 or beanListaCertificado.rreporte == 3 or beanListaCertificado.rreporte == 9}">
                                                             <tr>
                                                                <td align="right" height="25">
                                                                	<h:outputText value="Status:"/>
                                                                </td>
                                                                <td colspan="2" align="left" height="25">
                                                                	<h:outputText
                                                                   	value="Facturado"
                                                                   	rendered="#{beanListaCertificado.rreporte == 2}" />
                                                               	<h:outputText
                                                                    value="Vigente"
                                                                    rendered="#{beanListaCertificado.rreporte == 3}" />
                                                               	<h:outputText
                                                                    value="Cobrado"
                                                                    rendered="#{beanListaCertificado.rreporte == 9}" />
                                                               	<h:selectBooleanCheckbox id="c1"
                                                                    value="#{beanListaCertificado.boolFiltrarFacturadoVigente}"
                                                                    rendered="#{beanListaCertificado.rreporte == 2 or beanListaCertificado.rreporte == 3 or beanListaCertificado.rreporte == 9}"/>
																<h:outputText
																	value="Cancelado"
                                                                       rendered="#{beanListaCertificado.rreporte == 2 || beanListaCertificado.rreporte == 3}" />
                                                                   <h:outputText
                                                                       value="Pendiente de Cobro"
                                                                       rendered="#{beanListaCertificado.rreporte == 9}" />
                                                                   <h:selectBooleanCheckbox id="c2"
                                                                       value="#{beanListaCertificado.boolFiltrarCancelado}"
                                                                       rendered="#{beanListaCertificado.rreporte == 2 or beanListaCertificado.rreporte == 3 or beanListaCertificado.rreporte == 9 }" />
                                                                   <h:outputText
                                                                       value="Emitido"
                                                                       rendered="#{beanListaCertificado.rreporte == 2}" />
                                                                   <h:outputText
                                                                       value="Todos"
                                                                       rendered="#{beanListaCertificado.rreporte == 3}" />
                                                                   <h:selectBooleanCheckbox
                                                                       id="c3"
                                                                       value="#{beanListaCertificado.boolFiltrarEmitidoTodos}"
                                                                       rendered="#{beanListaCertificado.rreporte == 2 or beanListaCertificado.rreporte == 3 }"/>
                                                                </td>
                                                             </tr>
                                                             </c:if>
                                                             <!-- Termina Seleccion de Status -->
                                                             
                                                             <!-- OPCIONES: Seleccion de Filtro Fecha -->
                                                             <c:if  test="${((beanListaCertificado.rreporte == 10 || beanListaCertificado.rreporte == 11) && beanListaCertificado.poliza == 0) || beanListaCertificado.rreporte != 10 && beanListaCertificado.rreporte != 11 && beanListaCertificado.rreporte != 3 && beanListaCertificado.rreporte != 5 && beanListaCertificado.rreporte != 3 && beanListaCertificado.rreporte != 6 }">
                                                             <tr>
                                                             	 <td align="right" width="24%" height="30">
                                                                	<h:outputText
                                                                   	value="Filtro fechas: " />
                                                                </td>
                                                                <td colspan="2" align="left" width="76%" height="30">
                                                                	<a4j:outputPanel id="idFiltroFechas"
                                                                   	ajaxRendered="true">
                                                                       <h:selectOneMenu id="inFiltroFechas"
                                                                       	required="true"                                                                                                
                                                                           value="#{beanListaCertificado.inFiltroFechas}">
                                                                           <f:selectItem
                                                                           	itemValue="0"
                                                                               itemLabel="Seleccione opcion" />
                                                                           <f:selectItem
                                                                               itemValue="1"
                                                                               itemLabel="Por periodo contable" />
                                                                           <f:selectItem
                                                                               itemValue="2"
                                                                               itemLabel="Por fechas" />
                                                                           <a4j:support
                                                                           	event="onchange"
                                                                               ajaxSingle="true"
                                                                               reRender="opciones" />
                                                                       </h:selectOneMenu>
                                                                   </a4j:outputPanel>
                                                                </td>
                                                             </tr>
       													  </c:if>
       													  
														  <c:if test="${((beanListaCertificado.rreporte == 10 || beanListaCertificado.rreporte == 11) && beanListaCertificado.poliza == 0 && beanListaCertificado.inFiltroFechas == 1)
														   || beanListaCertificado.rreporte != 10 && beanListaCertificado.rreporte != 11 && beanListaCertificado.rreporte != 3 && beanListaCertificado.rreporte != 5  && beanListaCertificado.rreporte != 6  && beanListaCertificado.inFiltroFechas == 1}">
														  <tr id="tdMes2">
                                                             	 <td align="right" width="24%" height="30">
                                                               	<h:outputText
                                                               		value="Mes: "/>
                                                                </td>
                                                                <td colspan="2" align="left" width="76%" height="30">
                                                                	<h:selectOneMenu id="inMes2"
                                                                   	value="#{beanListaCertificado.mes}"
                                                                       style="width: 100px"
                                                                       title="header=[Mes] body=[Seleccione un mes para realizar la consulta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
                                                                		<f:selectItems
                                                                       	value="#{beanListaCertificado.listaMes}" />
                                                                       </h:selectOneMenu>
                                                                </td>
                                                             </tr>

                                                             <tr id="tdAnio2">
                                                                <td align="right" width="24%" height="30">
                                                                	<h:outputText 
                                                                		value="Anio: " />
                                                                </td>
                                                                <td colspan="2" align="left" width="76%" height="30">
                                                                	<h:selectOneMenu id="inAnio2"
                                                                   	value="#{beanListaCertificado.anio}"
                                                                       style="width: 100px"
                                                                       title="header=[A�o] body=[Seleccione un a�o para realizar la consulta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
                                                                   	<f:selectItems
                                                                       value="#{beanListaCertificado.listaComboAnios}" />
                                                                   </h:selectOneMenu>
                                                                </td>
                                                             </tr>
														  </c:if>
														  
														  <c:if test="${(beanListaCertificado.poliza == 0 && (beanListaCertificado.rreporte == 10  || beanListaCertificado.rreporte == 11) && beanListaCertificado.inFiltroFechas == 2) ||
														  (beanListaCertificado.rreporte != 3 && beanListaCertificado.rreporte != 5  
														  && beanListaCertificado.rreporte != 6&& beanListaCertificado.inFiltroFechas == 2) 
												
														  }">
                                                             <tr id="tdFechasIni2">
                                                             	 <td align="right">
                                                             		<h:outputText value="Fecha inicial:" />
                                                                </td>
                                                                <td  align="left">
                                                                	<rich:calendar id="fechaInicial"
                                                                   	datePattern="dd/MM/yyyy"
                                                                    value="#{beanListaCertificado.fecini}" />
                                                                     
                                                                </td>
                                                                  <h:message for="fechaInicial" styleClass="errorMessage" />
                                                             </tr>
                                                             
                                                             <tr id="tdFechasFin2">
                                                                <td align="right">
                                                                	<h:outputText value="Fecha final:" />
                                                                </td>
                                                                <td align="left">
                                                                	<rich:calendar id="fechaFinal"
                                                                   		datePattern="dd/MM/yyyy"
                                                                    	value="#{beanListaCertificado.fecfin}" 
                                                                    />
                                                                   
                                                                </td>
                                                                <h:message for="fechaFinal" styleClass="errorMessage" />
                                                             </tr>
                                                             </c:if>
														  	 <!-- Fin Seleccion filtros fecha -->
														  	 
															<%-- Check para Programar Reporte --%>
														  	<c:if test="${beanListaCertificado.rreporte == 2}">
														  	<tr>
														  		<td colspan="2" class="left_div">
														  			<h:outputText value="#{msgs.reporteprogramar}: "/>                                                               	
                                                               		<h:selectBooleanCheckbox 
                                                               			id="chkProgramar"
                                                                    	value="#{beanListaCertificado.blnProgramaRep}"/>
																</td>
														  	</tr>														  	
														  	</c:if>
														  	
														  	<%-- Fin Check para Programar Reporte --%> 	
                                                        </tbody>
                                                        </table>   
														                                            
                                                     </rich:panel>
                                                 </a4j:region>
                                              </td>
                                           </tr>
                                           <!-- FIN Opciones -->
                                       
                                           <tr>
                                             <td colspan="3">
                                             	<rich:spacer height="10px"></rich:spacer>
                                             </td>
                                           </tr>
                                                 
                                           <!-- Botones -->                                     
                                           <tr>
                                              <td colspan="3" align="center">
                                              	<!--btn Extraer Reporte-->
			                                    <a4j:region id="btn_extraer_panel">
                                                	<rich:panel id="btn_extraer_reporte"
                                                    	style=" align:center; background-color:#a4a4a4">
			                                     		<c:if test="${ beanListaCertificado.rreporte != 10 && beanListaCertificado.rreporte != 11}">
                                                        	<h:commandButton
                                                            	action="#{beanListaCertificado.reportes}"
                                                                value="Extraer Reporte">
                                                                <a4j:support event="onclick"
                                                                	ajaxSingle="true"
                                                                    reRender="barra,mensaje" />
                                                            </h:commandButton>
                                                        </c:if>
                                                         <c:if test="${beanListaCertificado.rreporte == 10 || beanListaCertificado.rreporte == 11}">
															<h:commandButton id="btn_consulta"
                                                            	action="#{beanListaCertificado.consultaDxP}"
                                                                value="Consulta">
                                                            	<a4j:support
                                                                	event="onclick"
                                                                    ajaxSingle="true"
                                                                    reRender="dxp_recibos" />
                                                            </h:commandButton>
                                                         </c:if>
                                                         
                                                          <c:if test="${beanListaCertificado.rreporte == 7 || beanListaCertificado.rreporte == 8 }">
														  <h:commandButton 
														   		action="#{beanListaCertificado.recibos}"
															    value="Consulta Recibos">
															    <a4j:support
															     	event="onclick"
															        ajaxSingle="true"
															        reRender="barra,mensaje,tr_sec_recibos" />
														  </h:commandButton>
														  <h:commandButton
														   		action="#{beanListaCertificado.limpiaRecibos}"
														        value="Limpiar Consulta">
														        <a4j:support
														        	event="onclick"
														            ajaxSingle="true"
														            reRender="barra,mensaje,tr_sec_recibos" />
														  </h:commandButton>
														  </c:if>
                                                    </rich:panel>
                                                </a4j:region>
                                           	  </td>
                                           </tr>
                                           <!-- Fin Botones -->                                           

                                         <!-- Mensaje -->
                                         <tr>
                                             <td colspan="3" align="center">
                                                 <br />
                                                 <h:outputText id="mensaje"
                                                     value="#{beanListaCertificado.respuesta}"
                                                     styleClass="respuesta" />                                                
                                                 <br />
                                             </td>
                                         </tr>
                                         <!-- Fin Mensaje-->
                                        </tbody>
                                        </table>
                                        
                                        <rich:spacer height="15px"></rich:spacer>

                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <rich:progressBar
                                                            value="#{beanListaCertificado.progressValue}"
                                                            id="barra"
                                                            enabled="#{beanListaCertificado.progressHabilitar}"
                                                            style=" width : 250px;"
                                                            interval="500"
                                                            rendered="true"
                                                            reRenderAfterComplete="frmConsultarEmision">
                                                            <h:outputText
                                                                value="Generando Reporte" />
                                                            <f:facet name="complete">
                                                                <h:outputText
                                                                    value="Proceso Completado"
                                                                    style=" color : #FFFFFF; font-size: 15px; font-weight: bold;" />
                                                            </f:facet>
                                                        </rich:progressBar>
                                                    </td>
                                                    <td align="center">
                                                        <rich:progressBar
                                                            value="#{beanListaCertificado.progressValue}"
                                                            id="barra_programar"
                                                            enabled="#{beanListaCertificado.progressHabilitar}"
                                                            style=" width : 250px;"
                                                            interval="500"
                                                            rendered="true"
                                                            reRenderAfterComplete="frmConsultarEmision">
                                                            <h:outputText
                                                                value="Programando Reporte" />
                                                            <f:facet name="complete">
                                                                <h:outputText
                                                                    value="Proceso Completado"
                                                                    style=" color : #FFFFFF; font-size: 15px; font-weight: bold;" />
                                                            </f:facet>
                                                        </rich:progressBar>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </a4j:region>
                                        
                               </td>
                            </tr>

                            <tr>
                                <td class="frameBL">
                                </td>
                                <td class="frameBC">
                                </td>
                                <td class="frameBR">
                                </td>
                            </tr>
                         </tbody>
                         </table>
                                               
                       </h:form>
                     </div>
                   </td>
                </tr>
             </tbody>
             </table>
          </td>
       </tr>
    </tbody>
    </table>
    <!-- Final Tabla Principal -->           

    <rich:spacer height="10px"></rich:spacer>
    <!-- JJFM: Seccion para resultado de reportes -->
    <div align="center">
    
    <!--IBB: Seccion para reporte de endosos - Recibos	-->
    <h:form id="endosos">
    <c:if test="${(beanListaCertificado.rreporte == 7 || beanListaCertificado.rreporte == 8) && beanListaCertificado.listaRecibos != null}">
    <table class="tablaPrincipal">
    <tbody>    
        <tr class="texto_normal" id="tr_sec_recibos">
            <td colspan="3">
                <c:if test="${beanListaCertificado.listaRecibosSize <= 0}">
                    <div>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO. </div> 
                </c:if>
                
                <c:if test="${beanListaCertificado.listaRecibos != null and beanListaCertificado.listaRecibosSize > 0 }">
                    <rich:dataTable id="listaRecibos"
                        width="100%"
                        value="#{beanListaCertificado.listaRecibos}"
                        columnsWidth="5px,12px,5px,15px,12px,12px,20px,20px"
                        var="beanRecibo"
                        rows="10">
                        <f:facet name="header">
                            <h:outputText value="Recibos" />
                        </f:facet>
                        <rich:column>
                            <f:facet name="header">
                                <h:outputText value="Sel." />
                            </f:facet>
                            <h:selectBooleanCheckbox
                                value="#{beanRecibo.chk}"
                                onclick="javascript:checkOneSelect(this);" />
                        </rich:column>
                        <rich:column>
                            <f:facet name="header">
                                <h:outputText value="Fe Fac" />
                            </f:facet>
                            <h:outputText value="#{beanRecibo.cofaFeFacturacion}">
                                <f:convertDateTime pattern="dd/MM/yyyy" />
                            </h:outputText>
                        </rich:column>
                        <rich:column>
                            <f:facet name="header">
                                <h:outputText value="Cons" />
                            </f:facet>
                            <h:outputText value="#{beanRecibo.cofaCuotaRecibo}" />
                        </rich:column>
                        <rich:column>
                            <f:facet name="header">
                                <h:outputText value="Recibo" />
                            </f:facet>
                            <h:outputText value="#{beanRecibo.cofaCampon5}" />
                        </rich:column>
                        <rich:column>
                            <f:facet name="header">
                                <h:outputText value="Desde" />
                            </f:facet>
                            <h:outputText value="#{beanRecibo.cofaCampof2}">
                                <f:convertDateTime pattern="dd/MM/yyyy" />
                            </h:outputText>
                        </rich:column>
                        <rich:column>
                        <f:facet name="header">
                            <h:outputText value="Hasta" />
                        </f:facet>
                        <h:outputText value="#{beanRecibo.cofaCampof3}">
                            <f:convertDateTime pattern="dd/MM/yyyy" />
                        </h:outputText>
                        </rich:column>
                        <rich:column>
                            <f:facet name="header">
                                <h:outputText value="Pma Neta" />
                            </f:facet>
                            <h:outputText value="#{beanRecibo.cofaMtTotPma}" />
                        </rich:column>
                        <rich:column>
                            <f:facet name="header">
                                <h:outputText value="Certificados" />
                            </f:facet>
                            <h:outputText value="#{beanRecibo.cofaTotalCertificados}" />
                        </rich:column>
                    </rich:dataTable>
                </c:if>
            </td>
        </tr>
    </tbody>
    </table>    
    </c:if>
    </h:form>
    <!--IBB: Fin Seccion para reporte de endosos - Recibos	--> 

    <!-- JJFM: Seccion para reporte DxP y Recibos -->
    <h:form id="dxp_recibos">
    <c:if test="${ (beanListaCertificado.rreporte == 10 || beanListaCertificado.rreporte == 11) && beanListaCertificado.reporteDxP != null}">
  	<table class="tablaPrincipal">
    <tbody>
        <tr>
        <td>
            <a4j:region id="regionDxPRecibos">
                <rich:panel id="dxp_recibos_panel"
                    style=" align:center; background-color:#a4a4a4">
                    <rich:panel 
                        style=" align:center; background-color:#E5E0EC"
                        styleClass="tablaSecundaria">
                            <div id="tr_rep_dxp" class="texto_normal">
                                <div>
                                    <c:if test="${beanListaCertificado.isArrDxPNullorEmpty}">
                                    <div>NO SE ENCONTRO INFORMACION.</div>
                                    </c:if>
                                    <c:if test="${ (beanListaCertificado.rreporte == 10 || beanListaCertificado.rreporte == 11) && !beanListaCertificado.isArrDxPNullorEmpty }">
                                        <!-- TABLA RESUMEN REPORTE DxP -->
                                       <rich:dataTable
                                        id="ReporteDxP"
                                            width="100%"
                                            value="#{beanListaCertificado.reporteDxP}"
                                            var="reporteDxP"
                                            rows="1"
                                            columnsWidth="20px,75px,75px,75px,75px,75px,75px,75px,75px,75px,75px,75px"
                                            columns="1">
                                       
                                       <rich:column>
                                       		 <rich:dataTable
                                            id="resumenDxP"
                                            width="100%"
                                            value="#{reporteDxP}"
                                            var="resumen"
                                            columnsWidth="20px,75px,75px,75px,75px,75px,75px,75px,75px,75px,75px,75px"
                                            columns="12"
                                            >
                                                 
                                            <!-- HEADER --> 
                                            <f:facet name="header">
                                                <h:outputText
                                                    value="Resumen deudor por prima de la Poliza No. #{resumen.bdecimalIdPoliza}" />
                                            </f:facet>

                                            <!-- COLUMNS --> 
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText  value="IdPoliza"
                                                    	title="header=[Poliza.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                            <h:outputText value="#{resumen.bdecimalIdPoliza}"/>
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value=" EPNA" 
                                                    	title="header=[Emisi�n de prima neta anualizada.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalEPNA}"/>  
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="FAPN" 
                                                    	title="header=[Factura acumulada prima neta.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalFAPN}" />
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="DxP EPN" 
                                                    	title="header=[Deudor por prima exigible (Prima Neta).] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalDxPEPN}" />
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="DxP NEPN" 
                                                    	title="header=[Deudor por prima no exigible (Prima Neta).] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalDxPNEPN}">  
                                                </h:outputText>
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText  value="DT" 
                                                    	title="header=[Deudor total (Prima Neta).] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalDT}"/>
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                <h:outputText value="EPTA" 
                                                	title="header=[Emisi�n de prima total anualizada.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalEPTA}" />
                                            </rich:column>
                                            <rich:column>
                                                <f:facet  name="header">
                                                    <h:outputText value="FAPT" 
                                                    	title="header=[Factura anualizada prima total.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalFAPT}" />
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="MCPT"
                                                    	title="header=[Monto cobrado prima total.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalMCPT}" />
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="DxP EPT"
                                                    	title="header=[Deudor por prima exigible (Prima Total).] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalDxPEPT}" />
                                            </rich:column>
                                            <rich:column>
                                                <f:facet  name="header">
                                                    <h:outputText  value="DxP NEPT" 
                                                    	title="header=[Deudor por prima no exigible (Prima Total).] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalDxPNEPT}" />
                                            </rich:column>
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="DXP EM" 
                                                    	title="header=[Deudor por prima exigible del mes.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{resumen.bdecimalDxPEM}" />
                                            </rich:column>
                                        </rich:dataTable>
                                            
                                        <rich:spacer height="20px"></rich:spacer>
                                         
                                        <!-- TABLA DETALLE REPORTE DxP -->
                                       
                                        <rich:dataTable
                                            id="detalleDxP" 
                                         	width="100%"
                                         	value="#{reporteDxP.arrDetalleDXP}"
                                         	var="detalleDxP"
                                         	columnsWidth="20px,75px,75px,75px,75px,75px,75px,75px,75px,75px,75px,75px"
                                            rows="12">
                                            <!-- HEADER --> 
                                            <f:facet name="header">
                                            <h:outputText value="Detalle deudor por prima por mes" />
                                            </f:facet>
                                            
                                            <!-- COLUMNAS --> 
                                            <!-- 1.- MES -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="Mes" 
                                                    	title="header=[Mes.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.strMes}"/>       
                                            </rich:column>
                                            <!-- 2.- Cuota -->
                                           <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="Cuota" 
                                                    	title="header=[Cuota.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.shortCuota}"/>
                                            </rich:column>
                                            <!-- 3.- FMPN -->
                                           <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="FMPN"
                                                    	title="header=[Factura del mes prima neta.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalFMPN}"/>
                                            </rich:column>
                                           <!-- 4.- MCPN -->
                                           <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="MCPN" 
                                                    	title="header=[Monto cobrado prima neta.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalMCPN}"/>
                                            </rich:column>
                                            <!-- 5.- EVN -->
                                           <rich:column>
                                               <f:facet name="header">
                                                    <h:outputText  value="EVN"
                                                    	title="header=[Endoso venta nueva.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalEVN}"/>
                                            </rich:column>
                                            <!-- 6.- EDS -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="EDS"
                                                    	title="header=[Endoso disminuci�n de stock.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalEDS}"/>
                                            </rich:column>
                                            <!-- 7.- ECPN -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="ECPN" 
                                                    	title="header=[Endoso cancelaci�n prima neta.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalECPN}"/>
                                            </rich:column>
                                            <!-- 8.- EPA -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="EPA"
                                                    	title="header=[Emisi�n de prima anualizada.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalEPA}"/>
                                            </rich:column>
                                             <!-- 9.- EP -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="EP" 
                                                    	title="header=[Emisi�n de prima.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalEP}"/>
                                            </rich:column>
                                             <!-- 10.- DxP EM -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="DXP EM" 
                                                    	title="header=[Deudor por prima exigible del mes.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalDxPEM}"/>
                                            </rich:column>
                                             <!-- 11.- DxP NEM -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="DXP NEM"
                                                    	title="header=[Deudor por prima no exigible del mes.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalDxPNEM}"/>
                                            </rich:column>
                                             <!-- 12.- Total DxP M -->
                                            <rich:column>
                                                <f:facet name="header">
                                                    <h:outputText value="TOTAL DXP M" 
                                                    	title="header=[Total deudor por prima del mes.] body=[] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
                                                </f:facet>
                                                <h:outputText value="#{detalleDxP.bdecimalTotalDXP}"/>
                                            </rich:column>
                                        </rich:dataTable>
                                       	
                                       </rich:column>
                                       <f:facet name="footer">
							      			<rich:datascroller  align="right" for="ReporteDxP" maxPages="10" page="#{beanListaCertificado.numPagina}"/>
							      		</f:facet>
                                       </rich:dataTable>
                                        </c:if>
                                        <rich:spacer height="20px"></rich:spacer>
                                        <div align="center">
                                            <h:commandButton id="dxpExtraerReporte"
                                                action="#{beanListaCertificado.reportes}"
                                                value="Extraer Reporte">
                                                <a4j:support event="onclick"
                                                ajaxSingle="true"
                                                reRender="barra,mensaje" />
                                                </h:commandButton>
                                        </div>
                                        </div>
                                   
                                </div>
                       
                    </rich:panel>
                </rich:panel>
            </a4j:region>
        </td>
        </tr>   
    </tbody>
    </table>
    </c:if>
    </h:form>
    <!-- JJFM: Fin Seccion para reporte DxP y Recibos -->
    </div>
    <!-- JJFM: Fin Seccion para resultados de Reportes -->
    <rich:spacer height="10px"></rich:spacer>
    </div>

    <!-- ---------------------------------------------------------------------------------------------------------- -->
    <!-- Footer -->
    <!-- ---------------------------------------------------------------------------------------------------------- -->
    <div>
        <%@include file="../footer.jsp"%>
    </div>
</f:view>
</body>
</html>