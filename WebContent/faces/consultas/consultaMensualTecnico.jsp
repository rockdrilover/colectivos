<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta Mensual Tecnico</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
						<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Secci�n</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Consulta Mensual Tecnico</td>
									<td align="right" width="12%">Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
						</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmConsultarCertif">
				      <div align="center">
					    <rich:spacer height="15px"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="700px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
										<tr>
											<td width="40%"></td>
											<td width="30%"></td>
											<td width="30%"></td>
										</tr>
										<tr>
											<td align="right">
												<h:outputText value="Ramo:" />
											</td>
											<td colspan="2" align="left">
											 	<h:selectOneMenu id="ramo" value="#{beanListaCertificado.ramo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
													<f:selectItem itemValue="" itemLabel="Ramo" />
													<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
											    </h:selectOneMenu>
												<h:message for="ramo" styleClass="errorMessage"/>
											</td>  
										</tr>
										<tr>
											<td align="right">
												<h:outputText value="Poliza Inicial:" />
											</td>
											<td colspan="2" align="left">
												<h:inputText id="poliza1"  value="#{beanListaCertificado.poliza1}" required="true" />
								     			<h:message for="poliza1" styleClass="errorMessage" />
											</td>
										</tr>
										<tr>
											<td align="right">
												<h:outputText value="Poliza Final:" />
											</td>
											<td colspan="2" align="left">
												<h:inputText id="poliza2"  value="#{beanListaCertificado.poliza2}" required="true" />
								     			<h:message for="poliza2" styleClass="errorMessage" />
											</td>
										</tr>
						                <tr>
											<td align="right">
	     	                                	<h:outputText value="Fecha inicial:" />
											</td>
											<td colspan="2" align="left">
		                                        <rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy" required="true" value="#{beanListaCertificado.fecini}" />
		                                        <h:message for="fechaInicial" styleClass="errorMessage" />
	     	                                </td>
	                                    </tr>
	
	                                    <tr>
											<td align="right">
	                                        	<h:outputText value="Fecha final:" />
											</td>
											<td colspan="2" align="left">
		                                        <rich:calendar id="fechaFinal" datePattern="dd/MM/yyyy" required="true" value="#{beanListaCertificado.fecfin}" />
		                                        <h:message for="fechaFinal" styleClass="errorMessage" />
	                                        </td>
	                                    </tr>    
										<tr height="10px">
											<td colspan="3" ></td>
										</tr>
	           	                		<tr>
	           	                		  	<td colspan="3" align="center">
	           	                		  	<h:commandButton action="#{beanListaCertificado.consultaMensualTecnico}" value="Consultar" />  
											</td>
										</tr>	
										<tr>
											<td colspan="3" align="center">
												<br/>
												<h:outputText id="mensaje" value="#{beanListaCertificado.mensaje}" styleClass="respuesta"/>
												<br/>
												<br/>
											</td>
										</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
					    </table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>