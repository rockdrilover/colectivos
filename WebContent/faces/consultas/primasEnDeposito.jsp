<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta Mensual Emisión</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
						<table class="encabezadoTabla" >
							<tbody>
								<tr>
									<td width="10%" align="center">Sección</td>
									<td width="4%" align="center" bgcolor="#d90909">||||</td>
									<td align="left" width="64%">Primas en Depósito</td>
									<td align="right" width="12%">Fecha</td>
									<td align="left" width="10%">
										<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
									</td>
								</tr>
							</tbody>
						</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmConsultarEmision">
				      <div align="center">
					    <rich:spacer height="15px"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="700px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
										<tr>
											<td width="30%"></td>
											<td width="40%"></td>
											<td width="30%"></td>
										</tr>
										<a4j:outputPanel id="mensajes" ajaxRendered="true">																														
					    						<tr class="espacio_15"><td>&nbsp;</td></tr>												
												<tr><td><h:outputText value="#{beanPrimasEnDeposito.respuesta}" styleClass="error"/></td></tr>	
												<tr class="espacio_15"><td>&nbsp;</tr>																								
										</a4j:outputPanel>
										<tr>
								    		<td align="right" height="25">														
												<h:outputText value="Acción:" />&nbsp;</td>
												<td align="left" height="25">
												<h:selectOneMenu value="#{beanPrimasEnDeposito.opcion}">
													<f:selectItem itemValue="0" itemLabel="- Seleccione una opción -" />
												    <f:selectItem itemValue="1" itemLabel="--Tiempo Real-- " />
												    <f:selectItem itemValue="2" itemLabel="--Guardados--" />	
												    <a4j:support oncomplete="javascript:exibirPanelTRG(#{beanPrimasEnDeposito.opcion});" event="onchange" 
												    action="#{beanPrimasEnDeposito.limpia}"/>																		     											   																	     
												</h:selectOneMenu>	
											</td>
										</tr>
				     					<tr>
											<td align="right" height="25"><h:outputText value="Ramo:" /></td>
											<td align="left" height="25">
											 	<h:selectOneMenu id="ramo" value="#{beanPrimasEnDeposito.ramo}" binding="#{beanListaParametros.inRamo}">
											 		<f:selectItem itemValue="" itemLabel="Ramo" />
													<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
													<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
											 	</h:selectOneMenu>
											 </td>
										</tr>
								    	<tr>
								    		<td align="right" height="25"><h:outputText value="Poliza:"/></td>
											<td align="left" height="25">
												<a4j:outputPanel id="polizas" ajaxRendered="true">
													<h:selectOneMenu id="poliza"  value="#{beanPrimasEnDeposito.poliza}" binding="#{beanListaParametros.inPoliza}">
													<f:selectItem itemValue="-1" itemLabel="Seleccione una póliza"/>
													<f:selectItem itemValue="0" itemLabel="Prima única"/>
													<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
													<a4j:support event="onchange" action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta, idPoliza" />
													</h:selectOneMenu>
												</a4j:outputPanel>
											</td>
								    	</tr> 
								    	<tr>
								    		<td align="right" height="25"><h:outputText value="Canal de Venta:"/></td>
											<td align="left" height="25">
												<a4j:outputPanel id="idVenta" ajaxRendered="true">
													<h:selectOneMenu id="inVenta"  value="#{beanPrimasEnDeposito.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisión.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
													         disabled="#{beanListaParametros.habilitaComboIdVenta}">
															<f:selectItem itemValue="-1" itemLabel="Seleccione un canal de ventas"/>
															<f:selectItem itemValue="0" itemLabel="Todos"/>																																							
															<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>																																		
													</h:selectOneMenu>
												</a4j:outputPanel>
											</td>
								    	</tr>
								    	<tr>
                   						<td align="right" width="24%" height="30"><h:outputText value="Mes"/></td>
										<td align="left" width="76%" height="30">														                   															                   																																																											 
											<h:selectOneMenu value="#{beanPrimasEnDeposito.mes}" style="width: 150px"  title="Seleccione un mes">
												<f:selectItem itemValue="0" itemLabel="- Seleccione un mes -"/>
												<f:selectItem itemValue="1" itemLabel="Enero" />
												<f:selectItem itemValue="2" itemLabel="Febrero" />
												<f:selectItem itemValue="3" itemLabel="Marzo" />
												<f:selectItem itemValue="4" itemLabel="Abril" />
												<f:selectItem itemValue="5" itemLabel="Mayo" />
												<f:selectItem itemValue="6" itemLabel="Junio" />
												<f:selectItem itemValue="7" itemLabel="Julio" />
												<f:selectItem itemValue="8" itemLabel="Agosto" />
												<f:selectItem itemValue="9" itemLabel="Septiembre" />
												<f:selectItem itemValue="10" itemLabel="Octubre" />
												<f:selectItem itemValue="11" itemLabel="Noviembre" />
												<f:selectItem itemValue="12" itemLabel="Diciembre" />
												 <a4j:support event="onchange"
															action="#{beanPrimasEnDeposito.muestraMesPrima}"
															reRender="panelEnero,panelFebrero,panelMarzo,panelAbril,panelMayo,panelJunio,panelJulio,panelAgosto
															,panelSeptiembre,panelOctubre,panelNoviembre,panelDiciembre,panelSumas" /> 
											</h:selectOneMenu>
										</td>	
									</tr>
									<tr><td colspan="2">&nbsp;</td></tr>
									<tr><td colspan="2">&nbsp;</td></tr>
									<tr id="idTmpReal"  style="display:none;">
							     			<td colspan="3" align="center">
							     			    <a4j:region >
								     				<table>
								     					
											     		<tr height="10px">
															<td colspan="2" ></td>
														</tr>
														<tr>
														<td colspan="2" align="left">
															<a4j:commandButton  value="Guardar Foto"
																action="#{beanPrimasEnDeposito.guardaFoto}" 
																onclick="this.disabled=false" reRender="resultado" >
															</a4j:commandButton>																		
														</td>
													</tr>
													<tr><td colspan="2">&nbsp;</td></tr>
													<tr>
														<td colspan="4" >
															<rich:dataTable>
																<f:facet name="header">														
																	<h:outputText value="Consulta de Certificados" />
																</f:facet>		
															</rich:dataTable>						
														</td>
													</tr>
														<tr>	
														<td>																																											
															<h:panelGrid id="panelEnero"
																		style="display:#{beanPrimasEnDeposito.display1};" >
																<a4j:outputPanel  ajaxRendered="true" id="panelprod">
																	<rich:dataTable value="#{beanPrimasEnDeposito.listaPrimasEne}" var="registro" 
																	 id="listaCertificados" columns="3" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row" >
																		<f:facet name="header">
																			<rich:columnGroup>																																																								
																				<rich:column breakBefore="true" rowspan="2">
																					<h:outputText value="PRODUCTOS" />																																														
																				</rich:column>																
																				<rich:column colspan="4">
																					<h:outputText value="Enero" />
																				</rich:column>																																										
																				<rich:column  style="min-width:80px" breakBefore="true">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column style="min-width:80px" >
																					<h:outputText value="Recuperación de prima" />																								
																				</rich:column>																																																												
																			</rich:columnGroup>												
																		</f:facet>																		
																		<rich:column >												
																			 <h:outputText value="#{registro.desc1}" />																						  																						
																		</rich:column>												
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesEne}"  >
																					<h:outputText value="#{registro.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesEne}"  >
																					<h:outputText value="#{registro.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>																						
																	</rich:dataTable>
																</a4j:outputPanel>
															</h:panelGrid>																	
														</td>																	 																																			
														<td>	
															<h:panelGrid id="panelFebrero"
																		style="display:#{beanPrimasEnDeposito.display2};" >
																<a4j:outputPanel  ajaxRendered="true" id="panelprod2">
																	<rich:dataTable id="listaCertificados2" value="#{beanPrimasEnDeposito.listaPrimasFeb}" 
																	var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row" >
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Febrero" />
																				</rich:column>																								
																				<rich:column  style="min-width:80px" breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column  style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>																																														
																			<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesFeb}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesFeb}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																	</rich:dataTable>
																</a4j:outputPanel>	
															</h:panelGrid>
														</td>
														<td>	
															<h:panelGrid id="panelMarzo"
																		style="display:#{beanPrimasEnDeposito.display3};" >
																<a4j:outputPanel  ajaxRendered="true" id="panelprod3">
																	<rich:dataTable id="listaCertificados3" value="#{beanPrimasEnDeposito.listaPrimasMar}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Marzo" />
																				</rich:column>																								
																				<rich:column style="min-width:80px"  breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column  style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>													
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesMar}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesMar}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																	</rich:dataTable>
																</a4j:outputPanel>	
															</h:panelGrid>
														</td>
														<td>	
															<h:panelGrid id="panelAbril"
																		style="display:#{beanPrimasEnDeposito.display4};" >
																<a4j:outputPanel  ajaxRendered="true">																			
																	<rich:dataTable id="listaCertificados4" value="#{beanPrimasEnDeposito.listaPrimasAbr}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Abril" />
																				</rich:column>																								
																				<rich:column style="min-width:80px"  breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column  style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>																																			
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesAbr}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesAbr}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																	</rich:dataTable>
																</a4j:outputPanel>	
															</h:panelGrid>
														</td>
													</tr>
													<tr>																	
														<td>	
															<h:panelGrid id="panelMayo"
																		style="display:#{beanPrimasEnDeposito.display5};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados5" value="#{beanPrimasEnDeposito.listaPrimasMay}" var="registro1" columns="3" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" rowspan="2">
																					<h:outputText value="PRODUCTOS" />
																				</rich:column>																
																				<rich:column colspan="4">
																					<h:outputText value="Mayo" />
																				</rich:column>																																										
																				<rich:column style="min-width:80px"  breakBefore="true">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column style="min-width:80px" >
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																												
																			</rich:columnGroup>												
																		</f:facet>	
																		<rich:column >	
																			 <h:outputText value="#{registro1.desc1}" />																											
																		</rich:column>												
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesMay}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesMay}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																	</rich:dataTable>
																</a4j:outputPanel>
															</h:panelGrid>
														</td>																						
														<td>	
															<h:panelGrid id="panelJunio"
																		style="display:#{beanPrimasEnDeposito.display6};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados6" value="#{beanPrimasEnDeposito.listaPrimasJun}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Junio" />
																				</rich:column>																								
																				<rich:column style="min-width:80px" breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>	
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesJun}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesJun}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>																																
																	</rich:dataTable>
																</a4j:outputPanel>
															</h:panelGrid>
														</td>
														<td>	
															<h:panelGrid id="panelJulio"
																		style="display:#{beanPrimasEnDeposito.display7};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados7" value="#{beanPrimasEnDeposito.listaPrimasJul}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Julio" />
																				</rich:column>																								
																				<rich:column style="min-width:80px" breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>													
																		<f:facet name="footer">														
																		</f:facet>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesJul}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesJul}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>				
																	</rich:dataTable>
																</a4j:outputPanel>	
															</h:panelGrid>
														</td>
														<td>	
															<h:panelGrid id="panelAgosto"
																		style="display:#{beanPrimasEnDeposito.display8};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados8" value="#{beanPrimasEnDeposito.listaPrimasAgo}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column  style="min-width:80px" breakBefore="true" colspan="2">
																					<h:outputText value="Agosto" />
																				</rich:column>																								
																				<rich:column  style="min-width:80px" breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>													
																			<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesAgo}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesAgo}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>				
																	</rich:dataTable>
																</a4j:outputPanel>	
															</h:panelGrid>
														</td>
													</tr>
													<tr>																
														<td>	
															<h:panelGrid id="panelSeptiembre"
																		style="display:#{beanPrimasEnDeposito.display9};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados9" value="#{beanPrimasEnDeposito.listaPrimasSep}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" rowspan="2">
																					<h:outputText value="PRODUCTOS" />
																				</rich:column>																
																				<rich:column colspan="4">
																					<h:outputText value="Septiembre" />
																				</rich:column>																																										
																				<rich:column  style="min-width:80px" breakBefore="true">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column style="min-width:80px" >
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																														
																			</rich:columnGroup>												
																		</f:facet>
																		<rich:column >												
																			 <h:outputText value="#{registro1.desc1}" />
																		</rich:column>												
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesSep}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesSep}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>																																	
																	</rich:dataTable>
																</a4j:outputPanel>	
															</h:panelGrid>
														</td>																									
														<td>	
															<h:panelGrid id="panelOctubre"
																		style="display:#{beanPrimasEnDeposito.display10};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados10" value="#{beanPrimasEnDeposito.listaPrimasOct}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Octubre" />
																				</rich:column>																								
																				<rich:column  style="min-width:80px" breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column  style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>																																											
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesOct}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesOct}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																	</rich:dataTable>
																</a4j:outputPanel>
															</h:panelGrid>
														</td>
														<td>	
															<h:panelGrid id="panelNoviembre"
																		style="display:#{beanPrimasEnDeposito.display11};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados11" value="#{beanPrimasEnDeposito.listaPrimasNov}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Noviembre" />
																				</rich:column>																								
																				<rich:column  style="min-width:80px" breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column  style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>													
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesNov}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesNov}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>																				
																	</rich:dataTable>
																</a4j:outputPanel>	
															</h:panelGrid>
														</td>
														<td>	
															<h:panelGrid id="panelDiciembre"
																		style="display:#{beanPrimasEnDeposito.display12};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados12" value="#{beanPrimasEnDeposito.listaPrimasDic}" var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row">
																		<f:facet name="header">
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" colspan="2">
																					<h:outputText value="Diciembre" />
																				</rich:column>																								
																				<rich:column  style="min-width:80px" breakBefore="true" colspan="1">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column  style="min-width:80px" colspan="1">
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																													
																			</rich:columnGroup>												
																		</f:facet>													
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesDic}"  >
																					<h:outputText value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:commandLink  action="#{beanPrimasEnDeposito.regresaMesDic}"  >
																					<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																					</h:outputText>
																				</h:commandLink>
																		</rich:column>
																	</rich:dataTable>
																</a4j:outputPanel>
															</h:panelGrid>																	
														</td>
													</tr>
													<tr>	
														<td>	
															<h:panelGrid id="panelSumas"
																		style="display:#{beanPrimasEnDeposito.display15};" >
																<a4j:outputPanel  ajaxRendered="true">
																	<rich:dataTable id="listaCertificados13" value="#{beanPrimasEnDeposito.listaGlobal}" 
																	var="registro1" columns="2" width="700px"
																			columnsWidth="40px,30px,30px"
																			rows="15" rowKeyVar="row" align="left">
																		<f:facet name="header">																	
																			<rich:columnGroup>																											
																				<rich:column breakBefore="true" rowspan="2">
																					<h:outputText value="PRODUCTOS" />
																				</rich:column>																
																				<rich:column colspan="4">
																					<h:outputText value="Sumas totales" />
																				</rich:column>																																										
																				<rich:column style="min-width:80px"  breakBefore="true">
																					<h:outputText value="Prima en deposito" />
																				</rich:column>
																				<rich:column style="min-width:80px" >
																					<h:outputText value="Recuperación de prima" />
																				</rich:column>																																														
																			</rich:columnGroup>														
																		</f:facet>	
																		<rich:column >												
																			 <h:outputText value="#{registro1.desc1}" />
																		</rich:column>											
																		<rich:column style="text-align: right;">
																				<h:outputText  value="#{registro1.coceCampon7BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																				</h:outputText>
																		</rich:column>
																		<rich:column style="text-align: right;">
																				<h:outputText value="#{registro1.coceCampon8BD}" >
																						 <f:convertNumber pattern="#0.00" locale="es_MX" />
																				</h:outputText>
																		</rich:column>
																	</rich:dataTable>
																</a4j:outputPanel>
															</h:panelGrid>																	
														</td>
													</tr>											 			
											     </table>
							     				</a4j:region>
							     			</td>
							     		</tr>
							     		<tr id="idGuarda" style="display:none;">
							     			<td colspan="3" align="center">
							     			    <a4j:region >
								     				<table>
													<tr>
														<td  align="left">
															<a4j:commandButton  value="Revisar Fechas"
																action="#{beanPrimasEnDeposito.cargaFechas}" 
																onclick="this.disabled=false" reRender="resultado" >
															</a4j:commandButton>																		
														</td>
													
														<td  align="center">
															<a4j:commandButton  value="Mostrar Detalles"
																onclick="javascript:exibirPanelTRG(1);" 
																action="#{beanPrimasEnDeposito.limpia}" >
															</a4j:commandButton>																		
														</td>
													</tr>
													<tr><td colspan="2">&nbsp;</td></tr>
													<tr>
														<td>		
															<a4j:outputPanel id="resultados" ajaxRendered="true" >
																<rich:dataTable id="fot" value="#{beanPrimasEnDeposito.listaTraeFotos}" var="registro" columns="2" width="90px" >
																	<f:facet name="header">
																		<h:outputText value="Fechas Guardadas" />
																	</f:facet>																	
																	<rich:column width="20%">
																		<f:facet name="header"><h:outputText value="Id Venta" /> </f:facet>																		
																		<h:outputText value="#{registro.desc1}" />
																	</rich:column>
																	<rich:column width="20px%">
																		<f:facet name="header">
																			<h:outputText value="Fecha de Carga " /> 
																		</f:facet>												
																		<h:outputText  value="#{registro.desc2}" />
																	</rich:column>
																	
																	<br/><br/>
																</rich:dataTable>
															</a4j:outputPanel>	
														</td>	
													</tr>	
													
											     	</table>
							     				</a4j:region>
							     			</td>
							     		</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
					    </table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>