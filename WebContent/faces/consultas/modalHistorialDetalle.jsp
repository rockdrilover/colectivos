<%@include file="../include.jsp"%>
<style type="text/css">
#global {
	height: 600px;
	weight: 80px;
	overflow-y: scroll;
}
</style>
<div id="global">
	<h:panelGrid columns="8" cellspacing="1" cellpadding="1"
		id="datosPoliza">

		<h:outputText value="CANAL: "  />
		<h:outputText value="#{beanHistorialCredito.objActual.cd_sucursal}"
			style="font-weight:bold; width: 206px;" />
		<h:outputText value="RAMO: " />
		<h:outputText value="#{beanHistorialCredito.objActual.ramo_nom}"
			style="font-weight:bold;" />
		<h:outputText value="POLIZA: " />
<%-- 		<h:outputText value="#{beanHistorialCredito.objActual.poliza}" --%>
<%-- 			style="font-weight:bold;" /> --%>
			<h:commandLink value="#{beanHistorialCredito.objActual.poliza}" action="#{beanHistorialCredito.impresionPoliza}" style="font-weight:bold; color:black" />
		<h:outputText value="CERTIFICADO: " />
<%-- 		<h:outputText value="#{beanHistorialCredito.objActual.certificado}" --%>
<%-- 			style="font-weight:bold;" /> --%>
		<h:commandLink id="inCertif" value="#{beanHistorialCredito.objActual.certificado}" action="#{beanHistorialCredito.getReportePlatilla}" style="font-weight:bold; color: black" />
		<rich:toolTip for="inCertif" value="Introduce el numero de certificado(Solo numeros)" />
	</h:panelGrid>


	<fieldset style="border: 1px solid black">
		<legend>
			<a4j:outputPanel id="secRes" ajaxRendered="true">
				<span id="img_sec_contratante_m" style="display: none;"> <h:graphicImage
						value="../../images/mostrar_seccion.png"
						onclick="muestraOcultaSeccion('contratante', 0);" title="Oculta" />&nbsp;
				</span>
				<span id="img_sec_contratante_o"> <h:graphicImage
						value="../../images/ocultar_seccion.png"
						onclick="muestraOcultaSeccion('contratante', 1);" title="Muestra" />&nbsp;
				</span>
				<span class="titulos_secciones">Datos Contratante</span>
			</a4j:outputPanel>
		</legend>
		
		<a4j:outputPanel id="regTablaCon" ajaxRendered="true">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">	
				<tr class="texto_normal" id="tr_sec_contratante" style="display:none;">
					<td>
						<h:panelGrid columns="5" cellspacing="1" cellpadding="1"
							id="datosGenerales">
							<h:outputText value="" style="font-weight:bold; color: black" />
							<h:outputText value="" />
							<h:outputText value="      " />
							<h:outputText value="      " style="font-weight:bold; color: black" />
							<h:outputText value="" />
							<h:outputText value="NOMBRE: " />
							<h:outputText value="#{beanHistorialCredito.objActual.nom_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="" />
							<h:outputText value="" style="font-weight:bold;" />
							<h:outputText value="RFC:" />
							<h:outputText value="#{beanHistorialCredito.objActual.rfc_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="" />
							<h:outputText value="" style="font-weight:bold;" />
							<h:outputText value="TELEFONO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.tel_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="" />
							<h:outputText value="" style="font-weight:bold;" />
							<h:outputText value="DOMICILIO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.dir_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="" />
							<h:outputText value="" style="font-weight:bold;" />
							<h:outputText value="COLONIA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.col_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="" />
							<h:outputText value="" style="font-weight:bold;" />
							<h:outputText value="DELEGACION: " />
							<h:outputText value="#{beanHistorialCredito.objActual.del_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="" />
							<h:outputText value="" style="font-weight:bold;" />
							<h:outputText value="CIUDAD: " />
							<h:outputText value="#{beanHistorialCredito.objActual.edo_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="      " />
							<h:outputText value="      " style="font-weight:bold;" />
							<h:outputText value="CODIGO POSTAL: " />
							<h:outputText value="#{beanHistorialCredito.objActual.cp_cont}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="      " />
							<h:outputText value="      " style="font-weight:bold;" />
				
						</h:panelGrid>
					</td>
				</tr>
			</table>
		</a4j:outputPanel>
	</fieldset>

	<fieldset style="border: 1px solid black">
		<legend>
			<a4j:outputPanel id="secAseg" ajaxRendered="true">
				<span id="img_sec_asegurado_m" style="display: none;"> <h:graphicImage
						value="../../images/mostrar_seccion.png"
						onclick="muestraOcultaSeccion('asegurado', 0);" title="Oculta" />&nbsp;
				</span>
				<span id="img_sec_asegurado_o"> <h:graphicImage
						value="../../images/ocultar_seccion.png"
						onclick="muestraOcultaSeccion('asegurado', 1);" title="Muestra" />&nbsp;
				</span>
				<span class="titulos_secciones">Datos Asegurado</span>
			</a4j:outputPanel>
		</legend>
		
		<a4j:outputPanel id="regTablaAse" ajaxRendered="true">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">	
				<tr class="texto_normal" id="tr_sec_asegurado" style="display:none;">
					<td>
						<h:panelGrid columns="5" cellspacing="1" cellpadding="1"
							id="datosAsegurado">
							<h:outputText value="" style="font-weight:bold; color: black" />
							<h:outputText value="" />
							<h:outputText value="      " />
							<h:outputText value="      " style="font-weight:bold; color: black" />
							<h:outputText value="" />
							<h:outputText value="NOMBRE: " />
							<h:outputText value="#{beanHistorialCredito.objActual.nombre_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="CALLE: " />
							<h:outputText value="#{beanHistorialCredito.objActual.dom_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="BUC: " />
							<h:outputText value="#{beanHistorialCredito.objActual.buc_cliente}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="COLONIA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.col_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="No. CLIENTE:" />
							<h:outputText value="#{beanHistorialCredito.objActual.nu_cliente}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="DELEGACION: " />
							<h:outputText value="#{beanHistorialCredito.objActual.del_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="BUC: " />
							<h:outputText value="#{beanHistorialCredito.objActual.buc_cliente}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="CODIGO POSTAL: " />
							<h:outputText value="#{beanHistorialCredito.objActual.cp_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="SEXO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.cd_sexo}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="ESTADO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.edo_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="RFC: " />
							<h:outputText value="#{beanHistorialCredito.objActual.rfc_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="TELEFONO\LADA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.tel_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="CURP: " />
							<h:outputText value="#{beanHistorialCredito.objActual.curp_aseg}"
								style="font-weight:bold;" />
							<h:outputText value="      " />
							<h:outputText value="FECHA NACIMIENTO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.fena_aseg}"
								style="font-weight:bold;" >
								<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="      " />
							<h:outputText value="      " />
							<h:outputText value="      " style="font-weight:bold;" />
							<h:outputText value="      " />
				
							<h:outputText value="      " />
						</h:panelGrid>
					</td>
				</tr>
			</table>
		</a4j:outputPanel>						
	</fieldset>
	
	<fieldset style="border: 1px solid black">
		<legend>
			<a4j:outputPanel id="secCred" ajaxRendered="true">
				<span id="img_sec_credito_m" style="display: none;"> <h:graphicImage
						value="../../images/mostrar_seccion.png"
						onclick="muestraOcultaSeccion('credito', 0);" title="Oculta" />&nbsp;
				</span>
				<span id="img_sec_credito_o"> <h:graphicImage
						value="../../images/ocultar_seccion.png"
						onclick="muestraOcultaSeccion('credito', 1);" title="Muestra" />&nbsp;
				</span>
				<span class="titulos_secciones">Dato Credito</span>
			</a4j:outputPanel>
		</legend>
		
		<a4j:outputPanel id="regTablaCre" ajaxRendered="true">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">	
				<tr class="texto_normal" id="tr_sec_credito" style="display:none;">
					<td>
						<h:panelGrid columns="5" cellspacing="1" cellpadding="1"
							id="datosVigencia">
							<h:outputText value="   " />
							<h:outputText value="   " />
							<h:outputText value="   " />
							<h:outputText value="   " />
							<h:outputText value="   " />
							<h:outputText value="FECHA CARGA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.fec_carga}"
								style="font-weight:bold;" />
							<h:outputText value="   " />
							<h:outputText value="SUMA ASEGURADA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.suma_aseg}" style="font-weight:bold;"/>
							<h:outputText value="FECHA EMISION: " />
							<h:outputText value="#{beanHistorialCredito.objActual.fec_emision}"
								style="font-weight:bold;" >
								<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="   " />
							<h:outputText value="SALDO BASE:" />
							<h:outputText value="#{beanHistorialCredito.objActual.saldo_base}" style="font-weight:bold; "/>
							<h:outputText value="FECHA DE INGRESO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.fechaingreso}"
								style="font-weight:bold;" >
								<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="   " />
							<h:outputText value="PRIMA TOTAL: " />
							<h:outputText value="#{beanHistorialCredito.objActual.prima_total}" style="font-weight:bold;"/>
							<h:outputText value="" />
							<h:outputText value=""
								style="font-weight:bold;" />
							<h:outputText value="   " />
							<h:outputText value="PRIMA NETA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.pmaneta}" style="font-weight:bold;"/>
							<h:outputText value="FEC_INI_POLIZA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.feinivigpol}"
								style="font-weight:bold;" >
								<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="   " />
							<h:outputText value="FEC_FIN_POLIZA: " />
							<h:outputText value="#{beanHistorialCredito.objActual.fefinvigpol}"
								style="font-weight:bold;" >
								<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="FEC_INI_CERTIFICADO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.feinivigcer}"
								style="font-weight:bold;" >
								<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="   " />
							<h:outputText value="FEC_FIN_CERTIFICADO: " />
							<h:outputText value="#{beanHistorialCredito.objActual.fefincre}"
								style="font-weight:bold;" >
								<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="ESTATUS: " />
							<h:outputText value="#{beanHistorialCredito.objActual.id_estatus}"
								style="font-weight:bold; color: black" />
							<h:outputText value="   " />
							<h:outputText value=" ESTATUS TECNICO:" />
							<h:outputText value="#{beanHistorialCredito.objActual.estat_tec}" style="font-weight:bold;"/>
							<h:outputText value="FECHA CANCELACION: " />
							<h:outputText value="#{beanHistorialCredito.objActual.fecha_anu} "
							style="font-weight:bold; color: black " >
							<f:convertDateTime pattern="dd/MM/yyyy"/>
							</h:outputText>
							<h:outputText value="   " />
							<h:outputText value=" MOTIVO CANCELACION: " />
							<h:outputText value="#{beanHistorialCredito.objActual.motivo_anulacion} " 
							style="font-weight:bold; color: black "/>
							<rich:separator lineType="none" align="right" style="width: 200px;  " />
							<rich:separator lineType="none" align="left" />
							<rich:separator lineType="none" align="right" style="width: 65px; " />
							<rich:separator lineType="none" align="left" />
							<rich:separator lineType="none" align="left" style="width: 65px;"/>
						</h:panelGrid>
					</td>
				</tr>
			</table>
		</a4j:outputPanel>
	</fieldset>

	<fieldset style="border: 1px solid black">
		<legend>
			<a4j:outputPanel id="secCob" ajaxRendered="true">
				<span id="img_sec_coberturas_m" style="display: none;"> <h:graphicImage
						value="../../images/mostrar_seccion.png"
						onclick="muestraOcultaSeccion('coberturas', 0);" title="Oculta" />&nbsp;
				</span>
				<span id="img_sec_coberturas_o"> <h:graphicImage
						value="../../images/ocultar_seccion.png"
						onclick="muestraOcultaSeccion('coberturas', 1);" title="Muestra" />&nbsp;
				</span>
				<span class="titulos_secciones">Coberturas</span>
			</a4j:outputPanel>
		</legend>
		
		<a4j:outputPanel id="regTablaReci" ajaxRendered="true">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">	
				<tr class="texto_normal" id="tr_sec_coberturas" style="display:none;">
					<td>
						<rich:dataTable id="listaHistorialDetalle"
							value="#{beanHistorialCredito.objActual.lstCoberturas}"
							columns="4" var="resultadoHistorial" rows="10" rowKeyVar="valor">
							<f:facet name="header">
								<rich:columnGroup>
									<rich:column width="10%">
										<h:outputText value="Ramo Contable" />
									</rich:column>
									<rich:column width="10%">
										<h:outputText value="Cobertura" />
									</rich:column>
									<rich:column width="10%">
										<h:outputText value="Descripcion" />
									</rich:column>
									<rich:column width="10%">
										<h:outputText value="Tarifa" />
									</rich:column>
			
			
								</rich:columnGroup>
							</f:facet>
							<rich:column width="10%" style="text-align:center">
								<h:outputText value="#{resultadoHistorial.ramo_cont}" />
							</rich:column>
							<rich:column width="10%" style="text-align:center">
								<h:outputText value="#{resultadoHistorial.cober}" />
							</rich:column>
							<rich:column width="10%" style="text-align:center">
								<h:outputText value="#{resultadoHistorial.cober_desc}" />
							</rich:column>
							<rich:column width="20%" style="text-align:center">
								<h:outputText value="#{resultadoHistorial.tarif}" />
							</rich:column>
			
							<f:facet name="footer">
								<rich:datascroller align="center" for="listaHistorialDetalle" maxPages="10"
									page="#{beanHistorialCredito.numPagina}" />
							</f:facet>
						</rich:dataTable>
					</td>
				</tr>
			</table>
		</a4j:outputPanel>

	</fieldset>
	

	<!-- inicio componente -->
		<fieldset style="border: 1px solid black">
		<legend>
			<a4j:outputPanel id="secCompo" ajaxRendered="true">
				<span id="img_sec_componentes_m" style="display: none;"> <h:graphicImage
						value="../../images/mostrar_seccion.png"
						onclick="muestraOcultaSeccion('componentes', 0);" title="Oculta" />&nbsp;
				</span>
				<span id="img_sec_componentes_o"> <h:graphicImage
						value="../../images/ocultar_seccion.png"
						onclick="muestraOcultaSeccion('componentes', 1);" title="Muestra" />&nbsp;
				</span>
				<span class="titulos_secciones">Componentes</span>
			</a4j:outputPanel>
		</legend>
				<a4j:outputPanel id="regTablaCub" ajaxRendered="true">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">	
					<tr class="texto_normal" id="tr_sec_componentes" style="display:none;">
						<td>
					<rich:dataTable id="listacompo"	value="#{beanHistorialCredito.objActual.lstComponentes}"
						columns="3" var="componente" rows="10"
						rowKeyVar="valor">
						<f:facet name="header">
							<rich:columnGroup>
								
								<rich:column width="10%">
									<h:outputText value="Descripcion" />
								</rich:column>
								<rich:column width="10%">
									<h:outputText value="Tarifa" />
								</rich:column>
								<rich:column width="10%">
									<h:outputText value="Monto" />
								</rich:column>
							</rich:columnGroup>
						</f:facet>
						
						<rich:column width="10%" style="text-align:center">
							<h:outputText value="#{componente.compo_desc}" />
						</rich:column>
						<rich:column width="10%" style="text-align:center">
							<h:outputText value="#{componente.tarif}" />
						</rich:column>
						<rich:column width="10%" style="text-align:center">
							<h:outputText value="#{componente.monto_compo}" />
						</rich:column>
						<f:facet name="footer">
							<rich:datascroller align="center" for="listacompo" maxPages="10"							/>
						</f:facet>
					</rich:dataTable>
						</td>
				</tr>
			</table>
				</a4j:outputPanel>
		
		
	
		</fieldset>
	<!-- fin componente -->


	<fieldset style="border: 1px solid black">
		<legend>
			<a4j:outputPanel id="secRecibos" ajaxRendered="true">
				<span id="img_sec_recibos_m" style="display: none;"> <h:graphicImage
						value="../../images/mostrar_seccion.png"
						onclick="muestraOcultaSeccion('recibos', 0);" title="Oculta" />&nbsp;
				</span>
				<span id="img_sec_recibos_o"> <h:graphicImage
						value="../../images/ocultar_seccion.png"
						onclick="muestraOcultaSeccion('recibos', 1);" title="Muestra" />&nbsp;
				</span>
				<span class="titulos_secciones">Recibos</span>
			</a4j:outputPanel>
		</legend>
		<a4j:outputPanel id="regTabla" ajaxRendered="true">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">	
					<tr class="texto_normal" id="tr_sec_recibos" style="display:none;">
						<td>
			<rich:dataTable id="resultadoRecibo"
				value="#{beanHistorialCredito.objActual.lstrecibos}"
				columns="6" var="resultadoRecibo" rows="10" rowKeyVar="valor">
				<f:facet name="header">
					<rich:columnGroup>
						<rich:column width="10%">
							<h:outputText value="Consecutivo" />
						</rich:column>
						<rich:column width="10%">
							<h:outputText value="Recibo" />
						</rich:column>
						<rich:column width="10%">
							<h:outputText value="Monto" />
						</rich:column>
						<rich:column width="10%">
							<h:outputText value="Estatus" />
						</rich:column>
						<rich:column width="10%">
							<h:outputText value="Fecha Desde" />
						</rich:column>
						<rich:column width="10%">
							<h:outputText value="Fecha Hasta" />
						</rich:column>

					</rich:columnGroup>
				</f:facet>
				<rich:column width="10%" style="text-align:center">
					<h:outputText value="#{resultadoRecibo.consecutivo}" />
				</rich:column>
				<rich:column width="10%" style="text-align:center">
					<h:outputText value="#{resultadoRecibo.recibo}" />
				</rich:column>
				<rich:column width="10%" style="text-align:center">
					<h:outputText value="#{resultadoRecibo.monto}" />
				</rich:column>
				<rich:column width="20%" style="text-align:center">
					<h:outputText value="#{resultadoRecibo.estatus_rec}" />
				</rich:column>
				<rich:column width="20%" style="text-align:center">
					<h:outputText value="#{resultadoRecibo.fech_des}" />
				</rich:column>
				<rich:column width="20%" style="text-align:center">
					<h:outputText value="#{resultadoRecibo.fech_has}" />
				</rich:column>
				<f:facet name="footer">
					<rich:datascroller align="center" for="resultadoRecibo" maxPages="10" />
				</f:facet>
			</rich:dataTable>
			</td>
				</tr>
			</table>
		</a4j:outputPanel>


	</fieldset>

</div>

