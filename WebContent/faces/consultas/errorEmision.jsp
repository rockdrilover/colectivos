<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Errores
- Fecha: 09/10/2020
- Descripcion: Pantalla Errores Emision
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallax}" /></title>
	</head>
	
	<body onload="muestraOcultaSeccion('resultado', 0);">
	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemax}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallax}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmErrorEmision">
										<h:inputHidden value="#{beanErrorEmision.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanErrorEmision.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemafiltro}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right15">
																			<h:outputText value="#{msgs.sistemafechad}:" />
																		</td>
																		<td class="left30">
																			<rich:calendar id="fechIni" popup="true"
																					value="#{beanErrorEmision.feDesde}"
																					datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
																		</td>
																	</tr>
																	<tr class="texto_normal">
																		<td class="right15">
																			<h:outputText value="#{msgs.sistemafechah}:" />
																		</td>
																		<td class="left30">
																			<rich:calendar id="fechFin" popup="true"
																					value="#{beanErrorEmision.feHasta}"
																					datePattern="dd/MM/yyyy" styleClass="calendario,wid100por" />
																		</td>
																	</tr>
																	
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.erroreslayout}:" />
																		</td>
																		<td class="left80">
																			<h:inputText id="nombreGpo" value="#{beanErrorEmision.nombreLayout}" styleClass="wid250"  />
																		</td>
																	</tr>
																	
																	<tr class="texto_normal">
																		<td class="right-div" colspan="2"> 
																			<a4j:commandButton
																				styleClass="boton" value="Consultar"
																				action="#{beanErrorEmision.consulta}"
																				onclick="this.disabled=true"
																				oncomplete="this.disabled=false;
																							muestraOcultaSeccion('resultado', 0);"
																				reRender="mensaje,secResultados,regTabla"
																				title="header=[Consulta Errores] body=[Consulta el agrupado de errores por LayOut.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</td>
																	</tr>

																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen90">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<tr class="texto_normal" id="tr_sec_resultado">
																		<td>
																			<c:if
																				test="${beanErrorEmision.listaArchivos == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanErrorEmision.listaArchivos != null}">
																				<rich:dataTable id="listaArchivos" value="#{beanErrorEmision.listaArchivos}"
																						columnsWidth="5px,50px,10px,10px,10px,5px"
																						var="beanArchivo" rows="15" width="100%">
																					<f:facet name="header">
																						<h:outputText value="#{msgs.errores}" />
																					</f:facet>

																					<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.erroresno}" /></f:facet>
																						<h:outputText value="#{beanArchivo.cociCdControl}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" sortBy="#{registro.cociNombreArchivo}">
																						<f:facet name="header"><h:outputText value="#{msgs.erroresarchivo}" /></f:facet>
																						<h:outputText value="#{beanArchivo.cociNombreArchivo}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.erroresemi}" /></f:facet>
																						<h:outputText value="#{beanArchivo.cociNuEmitidos}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.erroresrec}" /></f:facet>
																						<h:outputText value="#{beanArchivo.cociNuRechazados}" />
																					</rich:column>
																						<rich:column styleClass="texto_centro" >
																						<f:facet name="header"><h:outputText value="#{msgs.errorestot}" /></f:facet>
																						<h:outputText value="#{beanArchivo.cociNuTotal}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro">
					                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
					                    												<a4j:commandLink ajaxSingle="true" 
					                    													id="detallelink" 
					                    													reRender="listaDetalle" 
					                    													action="#{beanErrorEmision.getDetalle}" 
					                    													oncomplete="#{rich:component('detPanel')}.show();
					                    															muestraOcultaSeccion('resultado', 0);">
					                        													<h:graphicImage value="../../images/certificados.png" styleClass="bor0" />                        												
					                        													<f:setPropertyActionListener value="#{beanArchivo}" target="#{beanErrorEmision.seleccionado}" />
					                    												</a4j:commandLink>
					                    												<rich:toolTip for="detallelink" value="Ver Detalle" />
																                	</rich:column>
																					
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaArchivos" maxPages="10" page="#{beanErrorEmision.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>


										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
		
		<rich:modalPanel id="detPanel" autosized="true" width="700" height="500">
			<div class="scroll-div">
	   		<rich:toolTip id="toolTip" mode="ajax" value="Dar clic en Emitir o Cancelar." direction="top-right" />
	   		
	       	<f:facet name="header"><h:outputText value="Detalle Errores" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl attachTo="hidelinkNew" operation="show" for="toolTip" event="onmouseover" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="detalleErrores">
	            <rich:messages styleClass="errorMessage"></rich:messages>
	            <h:panelGrid columns="1" width="95%">
	                <a4j:outputPanel id="panelDet" ajaxRendered="true">
					<h:panelGrid columns="2">
	                    	<h:outputText value="#{msgs.erroresemi}:   "/>
	                    	<h:outputText value="#{beanErrorEmision.seleccionado.cociNuEmitidos}" styleClass="texto_mediano"/>
							
							<h:outputText value="#{msgs.erroresrec}:   "/>
	                    	<h:outputText value="#{beanErrorEmision.seleccionado.cociNuRechazados}" styleClass="texto_mediano"/>
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                
	                <rich:spacer height="10px" />
	                
	                <h:panelGrid columns="2">
	                	<a4j:commandButton value="  Emitir  "
	                		action="#{beanErrorEmision.emiteErrores}"
						    reRender="mensaje,regTabla,listaArchivos"
						    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } muestraOcultaSeccion('resultado', 0);" />
						
						<a4j:commandButton value="Cancelar"
						    reRender="mensaje,regTabla,listaArchivos"
						    onclick="#{rich:component('detPanel')}.hide();"	                    
						    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } muestraOcultaSeccion('resultado', 1);" />
	                </h:panelGrid>
	                	               
					<rich:dataTable id="listaDetalle" value="#{beanErrorEmision.lstDetalleArchivo}" var="beanDetalle" rows="20" width="100%">
						<f:facet name="header">
							<h:outputText value="Detalle Errores: #{beanErrorEmision.seleccionado.cociNombreArchivo}" />							 						
						</f:facet>
										
						<rich:column width="3%">
							<h:selectBooleanCheckbox value="#{beanDetalle.check}" disabled="#{beanDetalle.checkHabilita}" />			      		
			      		</rich:column>
						<rich:column width="13%">
							<f:facet name="header"><h:outputText value="#{msgs.sistemacre}" /></f:facet>
							<h:outputText value="#{beanDetalle.coerNuCredito}" />
						</rich:column>														
						<rich:column width="13%">
							<f:facet name="header"><h:outputText value="#{msgs.sistemaEst}" /></f:facet>
							<h:outputText value="#{beanDetalle.coerTipoError}" />
						</rich:column>
						<rich:column width="10%">
							<f:facet name="header"><h:outputText value="#{msgs.sistemafecar}"/></f:facet>
							<h:outputText value="#{beanDetalle.coerFechaCarga}">
								<f:convertDateTime pattern="dd-MM-yyyy" />
							</h:outputText>
						</rich:column>
						<rich:column width="61%">
							<f:facet name="header"><h:outputText value="#{msgs.erroredesc}" /></f:facet>
							<h:outputText value="#{beanDetalle.coerCdError}" />-
							<h:outputText value="#{beanDetalle.coerDescError}" />
						</rich:column>		
						
						<f:facet name="footer">
							<rich:datascroller align="center" for="listaDetalle" maxPages="10" page="#{beanErrorEmision.numPagina1}" />
						</f:facet>																							
					</rich:dataTable>
	            </h:panelGrid>
	        </h:form>
	        </div>
	   	</rich:modalPanel>
	   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>