<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/colectivos.js"></script>
<title>Reportes Cierre</title>

<style>
.filtroR {
	text-align: right;
}

.filtroL {
	text-align: left;
}

.paneles {
	width: 90%;
}

.texto_normal {
	font-weight: bold;
	font-size: 11px
}
</style>
</head>

<body>
	<f:view>
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center"><%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<table class="tablaPrincipal">
				<tr>
					<td align="center">
						<table class="tablaSecundaria">
							<tr>
								<td align="center">
									<table class="encabezadoTabla">
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909">||||</td>
											<td align="left" width="64%">Reportes Cierre</td>
											<td align="right" width="14%">Fecha:</td>
											<td align="left" width="10%"><input name="fecha"
												readonly="readonly"
												value="<%=request.getSession().getAttribute("fecha")%>"
												size="7"></td>
										</tr>
										</tbody>
									</table> <br>

									<div align="center">
										<rich:spacer height="15" />
										<table class="botonera">
											<tr>
												<td class="frameTL"></td>
												<td class="frameTC"></td>
												<td class="frameTR"></td>
											</tr>

											<tr>
												<td class="frameCL"></td>
												<td class="frameC"><h:form id="frmReportes">
														<a4j:region id="filtroCarga">
															<table border="0" align="center">
																<tr>
																	<td colspan="3">&nbsp;</td>
																</tr>

																<tr>
																	<td><h:outputText value="Accion:" />&nbsp; <h:selectOneMenu
																			value="#{beanReportesCierre.opcion}">
																			<f:selectItem itemValue="0"
																				itemLabel="- Seleccione una opcion -" />
																			<f:selectItem itemValue="1"
																				itemLabel="ExtraeComComer" />
																			<f:selectItem itemValue="2"
																				itemLabel="Montos Totales RFI" />
																			<f:selectItem itemValue="3" itemLabel="Rector Cob" />
																			<f:selectItem itemValue="4" itemLabel="Rector sn Cob" />
																			<f:selectItem itemValue="5"
																				itemLabel="Reporte Cifras Comerciales" />
																			<f:selectItem itemValue="6"
																				itemLabel="Reporte Cifras ComercialesU" />
																			<f:selectItem itemValue="7"
																				itemLabel="Reporte Cifras Tecnicas" />
																			<f:selectItem itemValue="8"
																				itemLabel="Reporte Detalle RFI" />
																			<f:selectItem itemValue="9"
																				itemLabel="ExtraeComTecnicas" />
																			<f:selectItem itemValue="10" itemLabel="ExtraeDosOP" />
																			<f:selectItem itemValue="11"
																				itemLabel="Reporte CuatroOP" />
																			<%--  <f:selectItem itemValue="12" itemLabel="Reporte Total Coaseguro" /> --%>
																			<f:selectItem itemValue="13" itemLabel="Reporte Emi" />
																			<f:selectItem itemValue="14" itemLabel="Reporte Consulta Reservas" />
																			<f:selectItem itemValue="15" itemLabel="Reporte Consulta Suma Asegurada" />
																			<a4j:support
																				oncomplete="javascript:exibirPanelReportes(#{beanReportesCierre.opcion},15);"
																				event="onchange"
																				action="#{beanReportesCierre.getOpcion}" />
																		</h:selectOneMenu></td>
																</tr>

																<tr>
																	<td colspan="3">&nbsp;</td>
																</tr>
																<tr>
																	<td colspan="3">&nbsp;</td>
																</tr>

																<tr id="trReportes_1" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paComComer" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeComComer" />
																			</f:facet>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_2" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paMontoRfi" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraerMontoRfi" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio1}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini1" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin1}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin1" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_3" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paRecCob" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraerRecCob" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio2}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini2" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin2}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin2" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_4" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paRecSnCob" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeRecSnCob" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio3}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini3" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin3}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin3" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_5" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paCifCom" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeCifCom" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio4}"
																				inputSize="10" popup="true" cellHeight="20px" style="width:145px"
																				id="Ini4" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin4}"
																				inputSize="10" popup="true" cellHeight="20px" style="width:145px"
																				id="fin4" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_6" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paCifComU" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeCifComU" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio5}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini5" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin5}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin5" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_7" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paCifrasTecnicas" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeCifrasTecnicas" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio6}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini6" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin6}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin6" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Operaci�n:" />
																			<h:selectOneMenu
																				value="#{beanReportesCierre.operacion}">
																				<f:selectItem itemValue="0"
																					itemLabel="- Seleccione una opcion -" />
																				<f:selectItem itemValue="6001" itemLabel="Emision" />
																				<f:selectItem itemValue="2003"
																					itemLabel="Rehabilitacion" />
																				<f:selectItem itemValue="6002"
																					itemLabel="Devolucion" />
																				<f:selectItem itemValue="2002"
																					itemLabel="Cancelacion" />
																			</h:selectOneMenu>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_8" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paDetaRfi" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeDetaRfi" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio7}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini7" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin7}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin7" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_9" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paComTec" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeComTec" />
																			</f:facet>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_10" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paDosOp" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeDosOp" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio8}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini8" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin8}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin8" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_11" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paCuatroOP" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeCuatroOP" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio9}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini9" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin9}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin9" cellWidth="100px" datePattern="dd/MM/yyyy"
																				styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_12" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paRepTotCoa" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeRepTotCoa" />
																			</f:facet>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio10}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini10" cellWidth="100px"
																				datePattern="dd/MM/yyyy" styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin10}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin10" cellWidth="100px"
																				datePattern="dd/MM/yyyy" styleClass="calendario">
																			</rich:calendar>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>
																<tr id="trReportes_13" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paRepEmi" styleClass="paneles">
																			<f:facet name="header">
																				<h:outputText value="ExtraeRepEmi" />
																			</f:facet>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</rich:panel></td>
																</tr>																
																<tr id="trReportes_14" style="display: none;">
																	<td align="center" colspan="3">
																		<rich:panel id="paRepRes" styleClass="paneles">																			
																			<f:facet name="header">
																				<h:outputText value="Reporte de Reservas" />
																			</f:facet>
																			<%-- <h:outputText id="ramos" value="Ramo:" />
																			<h:selectOneMenu value="#{beanReportesCierre.inRamo}">
																				<f:selectItem itemValue="0" itemLabel="--Selecione un ramo--"/>
																				<f:selectItem itemValue="57" itemLabel="57--VIDAEMPRESARIAL-SERFIN"/>
																				<f:selectItem itemValue="58" itemLabel="58--VIDAEMPRESARIAL-SANTANDER"/>
																		 	</h:selectOneMenu> --%>
																		 	
															<h:outputText value="Ramo:"/>
														
								                    			<h:selectOneMenu value="#{beanReportesCierre.inRamo}"  style="width: 250px" id="inRamo" title="Seleccione ramo" required="false" requiredMessage="* RAMO - Campo Requerido">  
														        	<f:selectItem itemLabel=" --- Seleccione un Ramo ---" itemValue="0" />  
														            <f:selectItems value="#{beanListaRamo.listaComboRamos}"/>											            
														           	<a4j:support event="onchange"  action="#{beanAltaContratanteR57.cargaProductos}" ajaxSingle="true" reRender="inProducto,mensaje" />
													        	</h:selectOneMenu>
			                   							
			                   							
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio14}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini14" cellWidth="100px"
																				datePattern="dd/MM/yyyy" styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin14}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin14" cellWidth="100px"
																				datePattern="dd/MM/yyyy" styleClass="calendario">
																			</rich:calendar>
																			<br/>
																			<br/>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />																			
																		</rich:panel>
																	</td>
																</tr>
																<tr id="trReportes_15" style="display: none;">
																	<td align="center" colspan="3"><rich:panel
																			id="paRepSumAseg" styleClass="paneles">																			
																			<f:facet name="header">
																					<h:outputText value="Reporte de Sumas Aseguradas" />
																			</f:facet>
																			<h:outputText value="Ramo:" />
																			<h:selectOneMenu value="#{beanReportesCierre.inRamo2}">
																				<f:selectItem itemValue="0" itemLabel="--Selecione un ramo--"/>
																				<f:selectItem itemValue="57" itemLabel="57--VIDAEMPRESARIAL-SERFIN"/>
																				<f:selectItem itemValue="58" itemLabel="58--VIDAEMPRESARIAL-SANTANDER"/>
																			</h:selectOneMenu>
																			<h:outputText value="Fecha Inicio:" />
																			<rich:calendar
																				value="#{beanReportesCierre.f_inicio15}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="Ini15" cellWidth="100px"
																				datePattern="dd/MM/yyyy" styleClass="calendario">
																			</rich:calendar>
																			<h:outputText value="Fecha Fin:" />
																			<rich:calendar value="#{beanReportesCierre.f_fin15}"
																				inputSize="10" popup="true" cellHeight="20px"
																				id="fin15" cellWidth="100px"
																				datePattern="dd/MM/yyyy" styleClass="calendario">
																			</rich:calendar>
																			<br>
																			<br>
																			<h:commandButton value="Reporte"
																				action="#{beanReportesCierre.sacarReportes}"
																				title="header=[Reporte] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />																			
																		</rich:panel></td>
																</tr> 
																
															</table>
														</a4j:region>
													</h:form></td>
												<td class="frameCR"></td>
											</tr>
											<tr>
												<td class="frameBL"></td>
												<td class="frameBC"></td>
												<td class="frameBR"></td>
											</tr>
										</table>
										<rich:spacer height="10px"></rich:spacer>
										<rich:spacer height="10px"></rich:spacer>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="10px" />
		<div align="center"><%@ include file="../footer.jsp"%></div>
	</f:view>
</body>
</html>