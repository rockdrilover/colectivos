<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<title>Consulta por Cuenta/Cr�dito</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
						<table class="encabezadoTabla" >
						<tbody>
							<tr>
								<td width="10%" align="center">Secci�n</td>
								<td width="4%" align="center" bgcolor="#d90909">||||</td>
								<td align="left" width="64%">Consulta de creditos</td>
								<td align="right" width="12%" >Fecha</td>
								<td align="left" width="10%">
									<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
								</td>
							</tr>
						</tbody>
						</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form>
					    <div align="center">
						    <rich:spacer height="15px"/>	
						    <table class="botonera">
						    <tbody>
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="600px">
								    <a4j:region id="consultaDatos">
									     <table cellpadding="2" cellspacing="0">
									     <tbody>
									    	<tr>
									     		<td align="right" colspan="2"><h:outputText value="Numero de cuenta:" /></td>
									     		<td align="left" colspan="2"><h:inputText value="#{beanListaTraspasoAptc.idCertificado}" />							     	
								     		</tr>
								     		<tr>
									    		<td colspan="4">
									    			<table>
									    				<tbody>
									    					<tr align="center">
									    						<td colspan="2">
									    							<h:outputText value="Fecha Carga" />
									    						</td>
									    					</tr>
									    					<tr>
									    						<td>
									    							<h:outputText value="Inicial" />
																 	 <rich:calendar id="fechaInicial"  value="#{beanListaTraspasoAptc.fechaInicial}" inputSize="10" inputStyle="width:50px"
											    									popup="false" cellHeight="20px"  cellWidth="20px" datePattern="dd/MM/yyyy" 
											    									styleClass="calendario" required="true"  />  	
					     											 <h:message for="fechaInicial" styleClass="errorMessage"/>
									    						</td>
									    						<td>
									    							<h:outputText value="Final" />
																 	 <rich:calendar id="fechaFinal"  value="#{beanListaTraspasoAptc.fechaFinal}" inputSize="10" 
											    									popup="false" cellHeight="20px"  cellWidth="20px" datePattern="dd/MM/yyyy" 
											    									styleClass="calendario" required="true" requiredMessage="Proporcione na fecha final"   />  	
					     											 <h:message for="fechaFinal" styleClass="errorMessage"/>
									    						</td>
									    					</tr>
									    				</tbody>
									    			</table>
									    		</td>	     	
								     		</tr>
								     		<tr>
								     			<td align="left">
								     				<h:commandButton styleClass="boton" value="Exportar" 
																   action="#{beanListaTraspasoAptc.erroresEmision}" 
																   title="header=[Consulta certificados] body=[Exportar cuentas con errores de emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
								     			</td>
								     			<td></td>
								     			<td align="right">
								     				<a4j:commandButton styleClass="boton" value="Consultar" 
																   action="#{beanListaTraspasoAptc.consultarErrores}" 
																   onclick="this.disabled=true" oncomplete="this.disabled=false" 
																   reRender="listaTraspaso"
																   title="header=[Consulta certificados] body=[Consulta de cuentas con errores en la emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
								     			<td></td>
								     		</tr>
								     	</tbody>     		         		    		
									    </table>
									    <rich:spacer height="15px"></rich:spacer>
									    <table>
								    	<tbody>
								    		<tr>
								    			<td align="center">
									    			<a4j:status for="consultaDatos" stopText=" ">
														<f:facet name="start">
															<h:graphicImage value="/images/ajax-loader.gif" />
														</f:facet>
													</a4j:status>
												</td>
											</tr>
										</tbody>
										</table>
									</a4j:region>								    
								    <td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							</tbody>
							</table>
						    <rich:spacer height="10px"></rich:spacer>
						    <table class="botonera">
							    <tbody>
								    <tr>
									    <td class="frameTL"></td>
									    <td class="frameTC"></td>
									    <td class="frameTR"></td>
								    </tr>
								    <tr>
									    <td class="frameCL"></td>
									    <td class="frameC" width="600px">
									     	<table>
									     	<tbody>
										     	<tr>
										     		<td align="center">
										     		
											      	<rich:dataTable id="listaTraspaso" value="#{beanListaTraspasoAptc.listaTraspaso}" columns="4" columnsWidth="10px,10px,10px,200px" var="beanClienteCertif" rows="10">
											      		<f:facet name="header">
											      			<rich:columnGroup>
											      				<rich:column colspan="1" rowspan="2"><h:outputText value="Cuenta"/></rich:column>
											      				<rich:column colspan="1" rowspan="2"><h:outputText value="Producto"/></rich:column>
											      				<rich:column colspan="1" rowspan="2"><h:outputText value="Fecha carga" /></rich:column>
											      				<rich:column colspan="1"><h:outputText value="Error" /></rich:column>
											      			</rich:columnGroup>
											      		</f:facet>
											      		<rich:column>
												      		<h:outputText value="#{beanClienteCertif.trseCuenta}" />		      		  			
											      		</rich:column>
											      	    <rich:column>
											      	    	<h:outputText value="#{beanClienteCertif.id.trseClaveProducto}" />       		  			
											      		</rich:column>
											      		<rich:column>
											      			<h:outputText value="#{beanClienteCertif.trseFechaCarga}" >   
											      				<f:convertDateTime pattern="dd/MM/yyyy"/>  
											      			</h:outputText>  		  			
											      		</rich:column>
											      		<rich:column>
											      			<h:outputText value="#{beanClienteCertif.trseRegistro}" />       		  			
											      		</rich:column>
											      		<f:facet name="footer">
											      			<rich:datascroller align="right" for="listaTraspaso" maxPages="10" page="#{beanListaTraspasoAptc.numPagina}"/>
											      		</f:facet>
											      	</rich:dataTable>
											      	
												      </td>
												     </tr>
											</tbody>
											</table>
										<td class="frameCR"></td>
								    </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
							    </table>
						</div>
				     	<h:messages styleClass="errorMessage" globalOnly="true"/>
			     	</h:form>
			     	
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>