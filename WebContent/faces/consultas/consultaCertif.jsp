<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta Certificados </title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
	    <table class="tablaSecundaria">
		<tbody>
			<tr>
				<td align="center">
				<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Consulta Certificado</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
					<h:form>
					<div align="center">
				    <rich:spacer height="15px"/>	
				    <table class="botonera">
				    <tbody>
					    <tr>
						    <td class="frameTL"></td>
						    <td class="frameTC"></td>
						    <td class="frameTR"></td>
					    </tr>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC" width="600px">
							    <a4j:region id="consulta">
							    <table>
								<tbody>
								
									<tr>
										<td align="right" width="35%"><h:outputText value="Ramo:" /></td>
										<td align="left" width="50%">
											<h:selectOneMenu id="ramo" value="#{beanListaCertificado.ramo}" binding="#{beanListaRamo.inRamo}" required="true" style="width : 95%; height : 23px;">
												<f:selectItem itemValue="" itemLabel="<Seleccione un ramo>"/>
												<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
										 	</h:selectOneMenu>
										 <h:message for="ramo" styleClass="errorMessage"/>	
										</td>  
										<td width="15%" align="left"></td>
									</tr>
									

									<tr>
										<td align="right"><br/><h:outputText value="P�liza:" /></td>
						                <td align="left">
						                <br/><h:inputText id="Poliza" value="#{beanListaCertificado.poliza}" required="true">
						                <f:validateLength  maximum="10"/>
			     							</h:inputText>	
			     						<h:message for="Poliza" styleClass="errorMessage"/>
						          
						                <td></td>
					                </tr>
					                 
					                <tr>
     	                                <td align="right"><br/><h:outputText value="Fecha inicial:" /></td>
                                        <td align="left"><br/>
                                        <rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy" required="false" value="#{beanListaCertificado.fecini}"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="right"><br/><h:outputText value="Fecha final:" /></td>
                                        <td align="left"><br/>
                                        <rich:calendar id="fechaFinal" datePattern="dd/MM/yyyy" required="false" value="#{beanListaCertificado.fecfin}"/>
                                        </td>
                                    </tr>    
           	                		<tr>
           	                		  	<td colspan="4" align="right">
           	                		  	<h:commandButton action="#{beanListaCertificado.ConsultaCertif}" value="Consultar" />  
										</td>
									</tr>	
                                    <tr>
                                         <td colspan="4" align="center">
                                               <br/>
                                               <h:outputText id="mensaje" value="#{beanListaCertificado.mensaje}" styleClass="respuesta"/>
                                               <br/>
                                               <br/>
                                         </td>
                                    </tr>
								</tbody>
								</table>
								<rich:spacer height="15px"></rich:spacer>
							    <table>
						    	<tbody>
						    		<tr>
						    			<td align="center">
							    			<a4j:status for="consulta" stopText=" ">
												<f:facet name="start">
													<h:graphicImage value="/images/ajax-loader.gif" />
												</f:facet>
											</a4j:status>
										</td>
									</tr>
								</tbody>
								</table>
							    </a4j:region>
						    <td class="frameCR"></td>
						    </tr>
						    <tr>
								<td class="frameBL"></td>
								<td class="frameBC"></td>
								<td class="frameBR"></td>
							</tr>
					    </tbody>
					    </table>
					    <rich:spacer height="10px"></rich:spacer>
					    <rich:spacer height="10px"></rich:spacer>	
					</div>
					</h:form>
				</td>
			</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %> 
</div>
</f:view>
</body>
 </html>