<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Consultas
- Fecha: 08/03/2021
- Descripcion: Pantalla para consulta detalle credito para area de Siniestros
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include.jsp"%>

<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs" />

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<title><h:outputText value="#{msgs.sistemapantallaiii}" /></title>
	</head>
	
	<body onload="muestraOcultaSeccion('resultado', 0);">
	
		<div class="center_div"><%@include file="../header.jsp"%></div>
		<div class="center_div">
			<table class="tablaPrincipal">
				<tr>
					<td class="center_div">
						<table class="tablaSecundaria">
							<tr>
								<td>
									<div class="center_div">
										<table class="encabezadoTabla">
											<caption></caption>
											<tr>
												<td class="encabezado1"><h:outputText value="#{msgs.sistemasecc}" /></td>
												<td class="encabezado2"><h:outputText value="#{msgs.sistemaiii}" /></td>
												<td class="encabezado3"><h:outputText value="#{msgs.sistemapantallaiii}" /></td>
												<td class="encabezado4"><h:outputText value="#{msgs.sistemafecha}" /></td>
												<td class="encabezado5">
													<input name="fecha" readonly="readonly" value="<%=request.getSession().getAttribute("fecha")%>" size="7" /></td>
											</tr>
										</table>
									</div> 
									
									<h:form id="frmconscred">
										<h:inputHidden value="#{beanConsDetalleCreSin.bean}" id="hidLoad" ></h:inputHidden>
										<div class="center_div">
											<table class="tablaCen90">
												<caption></caption>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
												<tr>
													<td><h:outputText id="mensaje" value="#{beanConsDetalleCreSin.strRespuesta}" styleClass="error" /></td>
												</tr>
												<tr class="espacio_15"><td><h:outputText value="" /></td></tr>
											</table>

											<a4j:region id="consultaDatos">
												<table class="tablaCen90">
													<caption></caption>
													<tr>
														<td>
															<fieldset class="borfieldset">
																<legend>
																	<a4j:outputPanel id="secAcciones" ajaxRendered="true">
																		<span id="img_sec_resultados_m"> 
																			<h:graphicImage value="../../images/mostrar_seccion.png" />&nbsp;
																		</span>
																		<span class="titulos_secciones"><h:outputText value="#{msgs.sistemafiltro}" /></span>
																	</a4j:outputPanel>
																</legend>

																<table class="tablacen80">
																	<caption></caption>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemaidentificador}:" />
																		</td>
																		<td class="left80">
																			<h:inputText id="identificador" value="#{beanConsDetalleCreSin.credito}" styleClass="wid250"  />
																		</td>
																	</tr>
																	
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.loginnombre}:" />
																		</td>
																		<td class="left80">
																			<h:inputText id="nombre" value="#{beanConsDetalleCreSin.nombre}" styleClass="wid250"  />
																		</td>
																	</tr>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemaappaterno}:" />
																		</td>
																		<td class="left80">
																			<h:inputText id="appaterno" value="#{beanConsDetalleCreSin.apellidoPaterno}" styleClass="wid250"  />
																		</td>
																	</tr>
																	<tr class="texto_normal">
																		<td class="right-div">
																			<h:outputText value="#{msgs.sistemaapmaterno}:" />
																		</td>
																		<td class="left80">
																			<h:inputText id="apmaterno" value="#{beanConsDetalleCreSin.apellidoMaterno}" styleClass="wid250"  />
																		</td>
																	</tr>
																	
																	<tr class="texto_normal">
																		<td class="right-div" colspan="2"> 
																			<a4j:commandButton
																				styleClass="boton" value="Consultar"
																				action="#{beanConsDetalleCreSin.consulta}"
																				onclick="this.disabled=true"
																				oncomplete="this.disabled=false;
																							muestraOcultaSeccion('resultado', 0);"
																				reRender="mensaje,secResultados,regTabla"
																				title="header=[Consulta Informacion] body=[Consulta los registros de acuerdo al filtro seleccionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																		</td>
																	</tr>

																	<tr class="espacio_15"><td><h:outputText value="#{msgs.sistemaespacio}" /></td></tr>
																	<tr>
																		<td class="center_div" colspan="2"><a4j:status
																				for="consultaDatos" stopText=" ">
																				<f:facet name="start">
																					<h:graphicImage value="/images/ajax-loader.gif" />
																				</f:facet>
																			</a4j:status>
																		</td>
																	</tr>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</a4j:region>

											<table class="tablaCen90">
												<tr>
													<td>
														<fieldset class="borfieldset">
															<legend>
																<a4j:outputPanel id="secResultados" ajaxRendered="true">
																	<span id="img_sec_resultado_m">
																		<h:graphicImage value="../../images/mostrar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 0);"
																			title="Oculta" />&nbsp;
																	</span>
																	<span id="img_sec_resultado_o"> 
																		<h:graphicImage value="../../images/ocultar_seccion.png"
																			onclick="muestraOcultaSeccion('resultado', 1);"
																			title="Muestra" />&nbsp;
																	</span>
																	<span class="titulos_secciones"><h:outputText value="#{msgs.sistemaresultados}" /></span>
																</a4j:outputPanel>
															</legend>
																	
															<a4j:outputPanel id="regTabla" ajaxRendered="true">
																<table class="tablaCen100">
																	<caption></caption>
																	<tr class="texto_normal" id="tr_sec_resultado">
																		<td>
																			<c:if
																				test="${beanConsDetalleCreSin.lstCertificados == null}">
																				<table class="tablaCen90">
																					<caption></caption>
																					<tr>
																						<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td>
																					</tr>
																				</table>
																			</c:if> 
																			<c:if test="${beanConsDetalleCreSin.lstCertificados != null}">
																				<rich:dataTable id="listaCreditos" 
																						value="#{beanConsDetalleCreSin.lstCertificados}"
																						columns="5" columnsWidth="10%,10%,50%,5%,5%" 
																						var="beanRegistro" rows="20" width="100%"
																						rowKeyVar="valor">
																						
																					<f:facet name="header">
																		      			<rich:columnGroup>
																		      				<rich:column><h:outputText value="#{msgs.cambiocreditoid1}" /></rich:column>
																		      				<rich:column><h:outputText value="#{msgs.cambiocreditoid2}" /></rich:column>
																		      				<rich:column><h:outputText value="#{msgs.loginnombre}" /></rich:column>
																		      				<rich:column><h:outputText value="#{msgs.detcrecliente}" /></rich:column>
																		      				<rich:column><h:outputText value="#{msgs.detcrecertificados}" /></rich:column>
																		      			</rich:columnGroup>
																		      		</f:facet>
																					
																					<rich:column styleClass="texto_centro" >
																						<h:outputText value="#{beanRegistro.cliente.cocnCampov2}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro">
																						<h:outputText value="#{beanRegistro.cliente.cocnCampov3}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<h:outputText value="#{beanRegistro.cliente.cocnNuCliente} - #{beanRegistro.cliente.cocnNombre} #{beanRegistro.cliente.cocnApellidoPat} #{beanRegistro.cliente.cocnApellidoMat}" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<h:graphicImage onclick="verCteCert(1, #{valor}, #{fn:length(beanConsDetalleCreSin.lstCertificados)}, 0);" 
																							value="/images/clientes.png" 
																							title="header=[Clientes] body=[Dar click para mostrar detalle de los clientes.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																					</rich:column>
																					<rich:column styleClass="texto_centro" >
																						<h:graphicImage onclick="verCteCert(2, #{valor}, #{fn:length(beanConsDetalleCreSin.lstCertificados)}, 0);" 
																							value="/images/certificados.png" 
																							title="header=[Certificados] body=[Dar click para mostrar detalle de los certificados.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																					</rich:column>
																					
																					<rich:column breakBefore="true" colspan="5" id="colClientes" style="display: none">
																						<table class="tablacen100b">
																							<caption></caption>
																							<tr class="cellred">
																								<td colspan="4" >
																									<h:outputText value="#{msgs.detcreclidatos}" />
																								</td>
																							</tr>
																							<tr class="espacio_5"><td colspan="4"></td></tr>
																							<tr class="left_div"> 
																								<td><h:outputText value="#{msgs.detcreclibuc}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnBucCliente}"/></td>
																								
																								<td><h:outputText value="#{msgs.detcreclicol}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnColonia}"/></td>
																							</tr>
																							<tr class="left_div">
																								<td><h:outputText value="#{msgs.detcreclinum}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnNuCliente}"></h:outputText></td>
																								
																								<td><h:outputText value="#{msgs.detcreclidel}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnDelegmunic}"/></td>
																							</tr>
																							<tr class="left_div">
																								<td><h:outputText value="#{msgs.detcreclicuenta}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnCampov4}"/></td>
																								
																								<td><h:outputText value="#{msgs.detcreclicp}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnCdPostal}"/></td>  		  			
																							</tr>
																							<tr class="left_div">
																								<td><h:outputText value="#{msgs.detcreclisexo}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnCdSexo}"/></td>
																								
																								<td><h:outputText value="#{msgs.detcrecliestado}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnCampov1}"/></td>
																							</tr>
																							<tr class="left_div">
																								<td><h:outputText value="#{msgs.timbradorfc}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnRfc}"/></td>
																								
																								<td><h:outputText value="#{msgs.detcreclilada}:"/></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnNuLada}"/></td>
																							</tr>
																							<tr class="left_div">
																								<td><h:outputText value="#{msgs.detcreclifena}:"/></td>
																								<td>
																									<h:outputText value="#{beanRegistro.cliente.cocnFeNacimiento}" >
																										<f:convertDateTime pattern="dd-MM-yyyy"/>
																									</h:outputText>
																						    	</td>
																						    	  		  			
																								<td><h:outputText value="#{msgs.detcreclitel}" /></td>
																								<td><h:outputText value="#{beanRegistro.cliente.cocnNuTelefono}"/></td>
																							</tr>
																							<tr class="left_div">
																								<td><h:outputText value="#{msgs.detcreclicalle}:" /></td>
																								<td colspan="3"><h:outputText value="#{beanRegistro.cliente.cocnCalleNum}" /></td>																								
																							</tr>  		  			
																						</table>
								                                                    </rich:column>
		                                                    						
		                                                    						<rich:column breakBefore="true" colspan="5" id="colCertificados" style="display: none">
		                                                    							<rich:dataTable id="listaCertificados" value="#{beanRegistro.lstCertificados }"
																							columnsWidth="20%,5%,5%,5%,5%,5%,5%,5%"
																							var="beanCert" rows="15" width="90%" styleClass="detalleEncabezado">
																							<f:facet name="header">
																								<h:outputText value="#{msgs.cambiocredito}" />
																							</f:facet>
		
																							<rich:column styleClass="texto_centro" >
																								<f:facet name="header"><h:outputText value="#{msgs.sistemacerti}" /></f:facet>
																								<h:outputText value="#{beanCert.id.coceCasuCdSucursal}-#{beanCert.id.coceCarpCdRamo}-#{beanCert.id.coceCapoNuPoliza}-#{beanCert.id.coceNuCertificado}" />
																							</rich:column>
																							<rich:column styleClass="texto_centro">
							                    												<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
							                    												<a4j:commandLink ajaxSingle="true" 
							                    													id="detallelink" 
							                    													reRender="detPanel,panelDet" 							                    													 			                    												
							                    													oncomplete="#{rich:component('detPanel')}.show();
							                    															muestraOcultaSeccion('resultado', 0);">
							                        													<h:graphicImage value="../../images/certificados.png" styleClass="bor0" />                        												
							                        													<f:setPropertyActionListener value="#{beanCert}" target="#{beanConsDetalleCreSin.seleccionado}" />
							                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanConsDetalleCreSin.selCliente}" />
							                        													<f:setPropertyActionListener value="#{valor}" target="#{beanConsDetalleCreSin.nEjecuta}" />
							                    												</a4j:commandLink>
							                    												<rich:toolTip for="detallelink" value="Ver Detalle" />
																		                	</rich:column>
																		                	<rich:column styleClass="texto_centro">
							                    												<f:facet name="header"><h:outputText value="#{msgs.timbradocober}" /></f:facet>
							                    												<a4j:commandLink ajaxSingle="true" 
							                    													id="coberlink" 
							                    													reRender="cobPanel,panelCob" 
							                    													action="#{beanConsDetalleCreSin.getCoberturas}" 
							                    													oncomplete="#{rich:component('cobPanel')}.show();
							                    															muestraOcultaSeccion('resultado', 0);">
							                        													<h:graphicImage value="../../images/coberturas.png" styleClass="bor0" />                        												
							                        													<f:setPropertyActionListener value="#{beanCert}" target="#{beanConsDetalleCreSin.seleccionado}" />
							                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanConsDetalleCreSin.selCliente}" />
							                        													<f:setPropertyActionListener value="#{valor}" target="#{beanConsDetalleCreSin.nEjecuta}" />
							                    												</a4j:commandLink>
							                    												<rich:toolTip for="coberlink" value="Ver Coberturas" />
																		                	</rich:column>
																							<rich:column styleClass="texto_centro">
							                    												<f:facet name="header"><h:outputText value="#{msgs.detcreasis}" /></f:facet>
							                    												<a4j:commandLink ajaxSingle="true" 
							                    													id="asislink" 
							                    													reRender="asisPanel,panelAsis" 
							                    													action="#{beanConsDetalleCreSin.getAsistencias}" 
							                    													oncomplete="#{rich:component('asisPanel')}.show();
							                    															muestraOcultaSeccion('resultado', 0);">
							                        													<h:graphicImage value="../../images/asistencias.png" styleClass="bor0" />                        												
							                        													<f:setPropertyActionListener value="#{beanCert}" target="#{beanConsDetalleCreSin.seleccionado}" />
							                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanConsDetalleCreSin.selCliente}" />
							                        													<f:setPropertyActionListener value="#{valor}" target="#{beanConsDetalleCreSin.nEjecuta}" />							                        													
							                    												</a4j:commandLink>
							                    												<rich:toolTip for="asislink" value="Ver Asistencias" />
																		                	</rich:column>
																		                	<rich:column styleClass="texto_centro">
							                    												<f:facet name="header"><h:outputText value="#{msgs.detcreaseg}" /></f:facet>
							                    												<a4j:commandLink ajaxSingle="true" 
							                    													id="aseglink" 
							                    													reRender="asegPanel,panelAseg" 
							                    													action="#{beanConsDetalleCreSin.getAsegurados}" 
							                    													oncomplete="#{rich:component('asegPanel')}.show();
							                    															muestraOcultaSeccion('resultado', 0);">
							                        													<h:graphicImage value="../../images/clientes adicionales.png" styleClass="bor0" />                        												
							                        													<f:setPropertyActionListener value="#{beanCert}" target="#{beanConsDetalleCreSin.seleccionado}" />
							                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanConsDetalleCreSin.selCliente}" />
							                        													<f:setPropertyActionListener value="#{valor}" target="#{beanConsDetalleCreSin.nEjecuta}" />
							                    												</a4j:commandLink>
							                    												<rich:toolTip for="aseglink" value="Ver Asegurados Adicionales" />
																		                	</rich:column>
																		                	<rich:column styleClass="texto_centro">
							                    												<f:facet name="header"><h:outputText value="#{msgs.detcreben}" /></f:facet>
							                    												<a4j:commandLink ajaxSingle="true" 
							                    													id="benlink" 
							                    													reRender="benPanel,panelBen" 
							                    													action="#{beanConsDetalleCreSin.getBeneficiarios}" 
							                    													oncomplete="#{rich:component('benPanel')}.show();
							                    															muestraOcultaSeccion('resultado', 0);">
							                        													<h:graphicImage value="../../images/beneficiarios.png" styleClass="bor0" />                        												
							                        													<f:setPropertyActionListener value="#{beanCert}" target="#{beanConsDetalleCreSin.seleccionado}" />
							                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanConsDetalleCreSin.selCliente}" />
							                        													<f:setPropertyActionListener value="#{valor}" target="#{beanConsDetalleCreSin.nEjecuta}" />
							                    												</a4j:commandLink>
							                    												<rich:toolTip for="benlink" value="Ver Beneficiarios" />
																		                	</rich:column>
																		                	<rich:column styleClass="texto_centro">
							                    												<f:facet name="header"><h:outputText value="#{msgs.detcreend}" /></f:facet>
							                    												<a4j:commandLink ajaxSingle="true" 
							                    													id="endlink" 
							                    													reRender="endPanel,panelEnd" 
							                    													action="#{beanConsDetalleCreSin.getEndosos}" 
							                    													oncomplete="#{rich:component('endPanel')}.show();
							                    															muestraOcultaSeccion('resultado', 0);">
							                        													<h:graphicImage value="../../images/endosos.jpg" styleClass="bor0" />                        												
							                        													<f:setPropertyActionListener value="#{beanCert}" target="#{beanConsDetalleCreSin.seleccionado}" />
							                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanConsDetalleCreSin.selCliente}" />
							                        													<f:setPropertyActionListener value="#{valor}" target="#{beanConsDetalleCreSin.nEjecuta}" />
							                    												</a4j:commandLink>
							                    												<rich:toolTip for="endlink" value="Ver Endosos" />
																		                	</rich:column>
																		                	<rich:column styleClass="texto_centro">
																		                		<f:facet name="header"><h:outputText value="#{msgs.detcremig}" /></f:facet>
							                    												<h:commandLink styleClass="boton"
							                    															rendered="#{beanCert.coceSubCampana == 4}"  
																											id="btnMigra"
																											onclick="bloqueaPantalla();" onblur="alert('Certificado Migrado.');desbloqueaPantalla();"
																											action="#{beanConsDetalleCreSin.migraInfo}">																		
																										<h:graphicImage value="../../images/migracion.png" style="border:0" />
																										<f:setPropertyActionListener value="#{beanCert}" target="#{beanConsDetalleCreSin.seleccionado}" />
							                        													<f:setPropertyActionListener value="#{beanRegistro}" target="#{beanConsDetalleCreSin.selCliente}" />
																								</h:commandLink>                		
																								<rich:toolTip for="btnMigra" value="Migra Informacion a Rector" />
																		                	</rich:column>
																		                	
																						</rich:dataTable>				
		                                                    						</rich:column>
																																										
																					<f:facet name="footer">
																						<rich:datascroller align="center" for="listaCreditos" maxPages="10" page="#{beanConsDetalleCreSin.numPagina}" />
																					</f:facet>
																				</rich:dataTable>
																			</c:if></td>
																	</tr>
																</table>
															</a4j:outputPanel>
														</fieldset>
													</td>
												</tr>
											</table>
										</div>
									</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<rich:spacer height="15px"></rich:spacer>
		<div>
			<%@include file="../footer.jsp"%>
		</div>
	   	
	   	<%@include file="/WEB-INF/includes/detalleCredito/modaldetallecre.jsp"%>
	   	
	   	<%@include file="/WEB-INF/includes/detalleCredito/modalasistenciascre.jsp"%>
	   	
	   	<%@include file="/WEB-INF/includes/detalleCredito/modalaseguradoscre.jsp"%>
	   	
	   	<%@include file="/WEB-INF/includes/detalleCredito/modalbeneficiarioscre.jsp"%>
	   	
	   	<%@include file="/WEB-INF/includes/detalleCredito/modalendososcre.jsp"%>
	   	
	   	<%@include file="/WEB-INF/includes/detalleCredito/modalcoberturascre.jsp"%>
	   	
	   	<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="#{msgs.sistemaprocesando}" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
    	</rich:modalPanel>
	</body>
</html>
</f:view>