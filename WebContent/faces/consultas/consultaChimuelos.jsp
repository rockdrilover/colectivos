<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Consulta Chimuelos</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">-</td>
							<td align="left" width="64%">Consulta Chimuelos</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form>
				    <div align="center">
					    <rich:spacer height="15"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="600px">
							    <a4j:region id="consultaDatos">
								    <table cellpadding="2" cellspacing="0">
								    <tbody>
								    	<tr>
									    	<td align="right" width="24%" height="30"><h:outputText value="Ramo:" /></td>
									    	<td align="left" width="76%" height="30">
												<h:selectOneMenu id="inRamo" value="#{beanChimuelos.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" style=" width : 258px;">
													<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
													<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
													<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
												</h:selectOneMenu>
												<h:message for="inRamo" styleClass="errorMessage" />
											</td>
									    </tr>
							     		<tr>
									    	<td align="right" width="24%" height="30"><h:outputText value="Poliza:"/></td>
											<td align="left" width="76%" height="30">
												<a4j:outputPanel id="polizas" ajaxRendered="true">
													<h:selectOneMenu id="inPoliza"  value="#{beanChimuelos.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" style=" width : 258px;">
															<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
															<f:selectItem itemValue="0" itemLabel="Prima �nica"/>
															<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
													</h:selectOneMenu>
													<h:message for="inPoliza" styleClass="errorMessage" />
												</a4j:outputPanel>
											</td>
									    </tr>
							     	   
							     	   <tr>
							     		</tr>
							     		
							     		<tr>
							     			<td align="center" colspan="4">
												<h:commandButton action="#{beanChimuelos.reporteChimuelos}" value="Consultar" 
 	  															 title="header=[Consulta] body=[Descarga el detalle del archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />			   
							     			</td>
							     		</tr>
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>	
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
					    </table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>