<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
 <% response.setHeader("Cache-Control","no-cache"); 
 response.setHeader("Pragma","no-cache");  
 response.setDateHeader ("Expires", 0);   
%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script type="text/javascript" src="../../js/boxover.js"></script>
		<script type="text/javascript" src="../../js/colectivos.js"></script>
		<title>Consulta Masiva por Certificado</title>
	</head>

	<body>
	<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
		<div align="center"><%@include file="../header.jsp" %></div>
		
		<div align="center">
			<table class="tablaPrincipal" >
				<tr>
					<td>
						<table class="tablaSecundaria">
							<tr>
								<td align="center">
									<div align="center">
										<table class="encabezadoTabla">
											<tr>
												<td width="10%" align="center">Secci�n</td>
												<td width="4%" align="center" bgcolor="#d90909">-</td>
												<td align="left" width="64%"> Comprimir Archivos</td>	
												<td align="right" width="12%" >Fecha</td>
												<td align="left" width="10%">
													<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
												</td>
											</tr>
										</table>
						    		</div>
					    		
					    			<rich:spacer height="10px"></rich:spacer>
					    		
					    			<h:form id="frmCosultaMasiva">
					    			<div align="center">
						    			<rich:spacer height="15"/>	
						    			<table class="botonera">
									    	<tr>
											    <td class="frameTL"></td>
											    <td class="frameTC"></td>
											    <td class="frameTR"></td>
										    </tr>
							    			<tr>
								    			<td class="frameCL"></td>
								    			<td class="frameC" width="700px">												   
									    			<rich:spacer height="15px"></rich:spacer>									    
									    			<table>
									    				<tr>													    	
															<td align="center" colspan="2">
																<h:outputText id="mensaje" value="#{beanConsultaFacturacionI.respuesta}" styleClass="respuesta"/>
															</td>															
														</tr>
									    			
									    				<tr><td colspan="3">&nbsp;</td></tr>
									    					
										    			<tr>
										    				<td align="right" width="20%" height="30"><h:outputText value="Cargar archivo:" /></td>
										    				<td align="left" width="80%" height="30" title="header=[Archivo] body=[Archivo a cargar extensi�n CSV, cualquier otra extensi�n el sistema no lo tomar� en cuenta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
										    					
												    		<rich:fileUpload id="uploadCancelacion" fileUploadListener="#{beanConsultaFacturacionI.listener}"
						    								 addControlLabel="Examinar" uploadControlLabel="Cargar"  
						    								 stopControlLabel="Detener" maxFilesQuantity="10"  immediateUpload="true" 
						    								 clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" styleClass="archivo" acceptedTypes="csv"
						    								 listHeight="65px" addButtonClass="botonArchivo" listWidth="90%">
						    								<a4j:support event="onuploadcomplete" reRender="mensaje,uno" />
								    						<f:facet name="label">
								    							<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
								    						</f:facet>
								    					    </rich:fileUpload>
										    						
												    			
										    				</td>
										    			</tr>
													</table>
									    			
									    			<rich:spacer height="15px"></rich:spacer>										
													<table>
										    		    <tr>
										    		        <td align="center">
										    		       
										    		        
										    			       <h:commandButton action="#{beanConsultaFacturacionI.consultaMasiva}" value="Consulta Masiva" id="uno"
 			  															title="header=[Consulta] body=[Descarga el detalle del archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
 			  															disabled="#{beanConsultaFacturacionI.disable}" > 
 			  													<a4j:support event="onComplete" reRender="mensaje,uno"  /> 
 			  													</h:commandButton> 
<%-- 																<a4j:commandLink action="#{beanConsultaFacturacionI.consultaMasiva}" --%>
<%-- 																          	     disabled="#{beanConsultaFacturacionI.disable}" --%>
<%-- 																          	     > --%>
<%-- 																<h:graphicImage value="../../images/edit_page.png" style="border:0" />																	 --%>
																
<%-- 																</a4j:commandLink> --%>
			  												</td>
										    			</tr>
													</table>
												</td>
												<td class="frameCR"></td>
											</tr>
											<tr>
												<td class="frameBL"></td>
												<td class="frameBC"></td>
												<td class="frameBR"></td>
											</tr>
										</table>
						    			
						    			<table class="botonera"></table>
						   			</div>
					    			</h:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		
		<rich:spacer height="10px"></rich:spacer>
		
		<div align="center"><%@ include file="../footer.jsp" %></div>
	</f:view>
	</body>
</html>