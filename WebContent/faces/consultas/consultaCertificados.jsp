<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Consulta de Certificados</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Consulta de certificados</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmConsultarCertif">
				      <div align="center">
					    <rich:spacer height="15px"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="700px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
										<tr>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="12%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
										</tr>
										<tr>
											<td align="center" colspan="9">
												<h:outputText value="Ramo:" />
											 	<h:selectOneMenu id="ramo" value="#{beanListaCertificado.ramo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
													<f:selectItem itemValue="" itemLabel="Ramo" />
													<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
											    </h:selectOneMenu>
												<h:message for="ramo" styleClass="errorMessage"/>
											</td>  
										</tr>
										<tr height="10px">
											<td colspan="9"></td>
										</tr>
							     		<tr>
								     		<td align="center" colspan="3"><h:outputText value="Tipo de Prima" /></td>
								     		<td align="center" colspan="3"><h:outputText value="Tipo de Reporte" /></td>
								     		<td align="center" colspan="3"><h:outputText value="Status Certificado" /></td>
							     		</tr>
							     		<tr>
							     			<td></td>
								     		<td align="left" colspan="2">
								     			<h:selectOneRadio id="tp" value="#{beanListaCertificado.rprima}" title="Selecciona una opci�n" layout="pageDirection" required="false">
													<f:selectItem itemLabel="Prima Unica" itemValue="1" />
													<f:selectItem itemLabel="Prima Recurrente" itemValue="2" />
												</h:selectOneRadio>
												<h:message for="tp" styleClass="errorMessage"/>
											</td>
							     			<td></td>
								     		<td align="left" colspan="2">
								     			<h:selectOneRadio id="tr" value="#{beanListaCertificado.rreporte}" title="Selecciona una opci�n" layout="pageDirection" required="true">
													<f:selectItem itemLabel="Detalle" itemValue="1" />
													<f:selectItem itemLabel="Consolidado" itemValue="2" />
												</h:selectOneRadio>
												<h:message for="tr" styleClass="errorMessage"/>
											</td>
							     			<td></td>
								     		<td align="left" colspan="2">
								     			<h:selectOneRadio id="st" value="#{beanListaCertificado.rstatus}" title="Selecciona una opci�n" layout="pageDirection" required="true">
													<f:selectItem id="st1" itemLabel="Vigente" itemValue="1" />
													<f:selectItem id="st2" itemLabel="Cancelado" itemValue="2" />
													<f:selectItem id="st3" itemLabel="Todos" itemValue="3" />
												</h:selectOneRadio>
												<h:message for="st" styleClass="errorMessage"/>
											</td>
							     		</tr>
										<tr height="10px">
											<td colspan="9"></td>
										</tr>
							     		<tr>
								     		<td align="center" colspan="3">
								     			<h:outputText value="Poliza:" />
								     			<h:inputText id="poliza"  value="#{beanListaCertificado.poliza}" required="true" />
								     			<h:message for="poliza" styleClass="errorMessage" />
									        <td align="center" colspan="3" rowspan="4">
									        		<h:outputText value="Fecha Inicial:" />
												 	 <rich:calendar id="fecha_ini"  value="#{beanListaCertificado.inFechaIni}" inputSize="10" 
							    									popup="true" cellHeight="20px"  cellWidth="20px" datePattern="dd/MM/yyyy" 
							    									styleClass="calendario" required="false" />  	
	     											 <h:message for="fecha_ini" styleClass="errorMessage"/>
									        </td>
									        <td align="center" colspan="3" rowspan="4">
									        		<h:outputText value="Fecha Final:" />
												 	 <rich:calendar id="fecha_fin"  value="#{beanListaCertificado.inFechaFin}" inputSize="10" 
							    									popup="true" cellHeight="20px"  cellWidth="20px" datePattern="dd/MM/yyyy" 
							    									styleClass="calendario" required="false" />  	
	     											 <h:message for="fecha_fin" styleClass="errorMessage"/>
									        </td>
							     		</tr>
										<tr>
											<td colspan="9" ></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>
										<tr>
											<td colspan="9" height="5"></td>
										</tr>
										<tr>
											<td align="center" colspan="9">
											    <a4j:commandButton styleClass="boton" 
											    action="#{beanListaCertificado.consultaCertificados}" 
											    reRender="certificados"
											    value="Consultar" />
											</td>
										</tr>							
							     	</tbody>     		         		    		
								    </table>
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
										<tr>
											<td align="center">
												<br/>
												<h:outputText id="mensaje" value="#{beanListaCertificado.mensaje}" styleClass="respuesta"/>
												<br/>
												<br/>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
					    </table>
					    <rich:spacer height="10px"></rich:spacer>
						<table  class="botonera">
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    	<div align="center">
							    	    <h:form>
										<rich:dataTable id="certificados" value="#{beanListaCertificado.listaCertificadosFec}" var="registro" columns="9" columnsWidth="15px, 10px, 15px, 25px,15px, 15px, 15px, 15px, 15px" width="135px" rows="50">
											<f:facet name="header"><h:outputText value="Reporte de Certificados" /></f:facet>
											<rich:column>
												<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
												<h:outputText value="#{registro.id.coceCapoNuPoliza}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Estatus Colectivos" /> </f:facet>
											<h:outputText value="#{registro.estatus.alesDescripcion}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Estatus" /> </f:facet>				
											<h:outputText value="#{registro.coceCampov1}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Causa" /> </f:facet>
											<h:outputText value="#{registro.coceCampov4}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Certificado" /></f:facet>
												<h:outputText value="#{registro.id.coceNuCertificado}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Suma Asegurada" /> </f:facet>
												<h:outputText value="#{registro.coceMtSumaAsegurada}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Prima Anual" /> </f:facet>
												<h:outputText value="#{registro.coceMtPrimaAnual}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Prima Real" /></f:facet>
												<h:outputText value="#{registro.coceMtPrimaReal}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Prima Pura" /> </f:facet>
												<h:outputText value="#{registro.coceMtPrimaPura}" />
											</rich:column>
								      		<f:facet name="footer">
								      			<rich:datascroller align="right" for="certificados" maxPages="10" page="#{beanListaCertificado.numPagina}"/>
								      		</f:facet>
										</rich:dataTable>
										<br/>
										</h:form>
										<br/>
									</div>
							    </td>
								<td class="frameCR"></td>
					    	</tr>								    	
					    	<tr>
								<td class="frameBL"></td>
								<td class="frameBC"></td>
								<td class="frameBR"></td>
							</tr>
						</table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>