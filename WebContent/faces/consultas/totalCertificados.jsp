<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Consulta de Certificados </title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
	    <table class="tablaSecundaria">
		<tbody>
			<tr>
				<td align="center">
				<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Total certificados</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
					<h:form>
					<div align="center">
				    <rich:spacer height="15px"/>	
				    <table class="botonera">
				    <tbody>
					    <tr>
						    <td class="frameTL"></td>
						    <td class="frameTC"></td>
						    <td class="frameTR"></td>
					    </tr>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC" width="600px">
							    <a4j:region id="consulta">
							    <table>
								<tbody>
									<tr>
										<td align="right" width="35%"><h:outputText value="Ramo:" /></td>
										<td align="left" width="50%">
											<h:selectOneMenu id="ramo" value="#{beanListaCertificado.ramo}" binding="#{beanListaParametros.inRamo}">
												<f:selectItem itemValue="0" itemLabel="Selecione un ramo"/>
												<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
												<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="comboPolizas"/>
										 	</h:selectOneMenu>
										</td>  
										<td width="15%" align="left"></td>
									</tr>
									<tr>
										<td align="right"><h:outputText value="Poliza:"/></td>
										<td align="left">
											<a4j:outputPanel id="comboPolizas" ajaxRendered="true">
											<h:selectOneMenu id="poliza" value="#{beanListaCertificado.poliza}" >
												<f:selectItem itemValue="" itemLabel="<Seleccione una poliza>"/>
												<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
											</h:selectOneMenu>
											</a4j:outputPanel>
										</td>
										<td align="left"></td>
									</tr>
									<tr>
										<td colspan="4" align="right">
											<a4j:commandButton styleClass="boton" value="Consultar" 
															   action="#{beanListaCertificado.totalCertificados}" 
															   onclick="this.disabled=true" oncomplete="this.disabled=false" 
															   reRender="listaCertificados,mensaje"
															   title="header=[Consulta certificados] body=[Consulta los certificados agrupados por estatus.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
										</td>
									</tr>														
								</tbody>
								</table>
								<rich:spacer height="15px"></rich:spacer>
							    <table>
						    	<tbody>
						    		<tr>
						    			<td align="center">
							    			<a4j:status for="consulta" stopText=" ">
												<f:facet name="start">
													<h:graphicImage value="/images/ajax-loader.gif" />
												</f:facet>
											</a4j:status>
										</td>
									</tr>
									<tr>
										<td align="center"><h:outputText id="mensaje" value="#{beanListaCertificado.respuesta}" styleClass="respuesta" /></td>
									</tr>
								</tbody>
								</table>
							    </a4j:region>
						    <td class="frameCR"></td>
						    </tr>
						    <tr>
								<td class="frameBL"></td>
								<td class="frameBC"></td>
								<td class="frameBR"></td>
							</tr>
					    </tbody>
					    </table>
					    <rich:spacer height="10px"></rich:spacer>
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    	<rich:dataTable id="listaCertificados" value="#{beanListaCertificado.listaTotalCertificados}" var="registro" width="400px">
										<f:facet name="header">
											<rich:columnGroup>
												<rich:column colspan="3">
													<h:outputText value="Consulta de certificados por estatus" />
												</rich:column>
												<rich:column breakBefore="true"><h:outputText value="Estatus Operativo" /></rich:column>
												<rich:column><h:outputText value="Estatus General" /></rich:column>
												<rich:column><h:outputText value="Certificados" /></rich:column>
											</rich:columnGroup> 
										</f:facet>
										<rich:column>   
										<h:outputText value="#{registro[1]}" />  			
										</rich:column>
										<rich:column>    
											<h:outputText value="#{registro[2]}" />      		  			
										</rich:column>
										<rich:column>    
											<h:outputText value="#{registro[0]}" />      		  			
										</rich:column>      		
									</rich:dataTable>
								<td class="frameCR"></td>
						    </tr>
						    <tr>
								<td class="frameBL"></td>
								<td class="frameBC"></td>
								<td class="frameBR"></td>
							</tr>
					    </tbody>
					    </table>
					    <rich:spacer height="10px"></rich:spacer>	
					</div>
					</h:form>
				    <h:messages styleClass="errorMessage" globalOnly="true" style="error"/>			
				</td>
			</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
</body>
 </html>