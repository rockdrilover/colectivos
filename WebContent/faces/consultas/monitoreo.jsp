<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<script type="text/javascript" src="../../js/colectivos.js"></script>
<title>Monitoreo</title>
</head>

<body>
	<f:view>
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
		<div align="center">
			<%@include file="../header.jsp" %>
		</div>
		<div align="center">
			<h:inputHidden value="#{beanMonitoreo.init}" />
			<table class="tablaPrincipal">
				<tbody>
					<tr>	
						<td align="center">
						    <table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<table class="encabezadoTabla" >
												<tbody>
													<tr>
														<td width="10%" align="center">Secci�n</td>
														<td width="4%" align="center" bgcolor="#d90909">||||</td>
														<td align="left" width="64%">Pantalla de Monitoreo</td>
														<td align="right" width="14%" >Fecha:</td>
														<td align="left" width="10%">
															<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
														</td>
													</tr>
												</tbody>
											</table>
											<div align="center">
												<rich:spacer height="15"/>	
												<h:form id="Monitoreo">
												<a4j:region id="filtroCarga">
												<table class="botonera">
													
															<tr>
																<td colspan="1" align="left">
																	<fieldset style="border: 1px solid white">
																		<table  border="0" align="center">
																			<tr><td colspan="4">&nbsp;</td></tr>
																			<tr>
																				<td align="center" colspan="4">
																					<h:outputText id="respuestas" value="#{beanMonitoreo.strRespuesta}" styleClass="respuesta" />
																				</td>
																			</tr>
																			<tr><td colspan="4">&nbsp;</td></tr>
																			<tr>
																				<td align="left">
																					<h:outputText value="Proceso : " />
																				</td>	 
																				<td align="left">
													                    			<h:selectOneMenu value="#{beanMonitoreo.opcion}" >
																	     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
																	     				<f:selectItem itemValue="1" itemLabel="Prima Unica"/>
																	     				<f:selectItem itemValue="2" itemLabel="Prima Recurrente"/>
																	     				<f:selectItem itemValue="3" itemLabel="Otros Procesos"/>
																	     				<a4j:support event="onchange"  action="#{beanMonitoreo.llenaTarea}" reRender="tarea"
																	     					oncomplete="javascript:exibirPanelesTareas(2,#{beanMonitoreo.tarea});"/>
																	     			</h:selectOneMenu>											     		
								                   								</td>
								                   								<td align="left" >
																				</td>
																				<td align="left" >
																				</td>
																			</tr>
																			<tr><td colspan="4">&nbsp;</td></tr>
																			<tr id="cero" style="display: true">
																				<td align="left">
																					<h:outputText value="Tarea : " />
																				</td>
																				<td align="left" >
																					<h:selectOneMenu id="tarea" value="#{beanMonitoreo.tarea}">
																	     				<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
																	     				<f:selectItems value="#{beanMonitoreo.listaComboTareas}"/>
																	     				<a4j:support event="onchange" oncomplete="javascript:exibirPanelesTareas(2,#{beanMonitoreo.tarea});" ajaxSingle="true"/>
																	     			</h:selectOneMenu>
																				</td>
																				<td align="left" >
																				</td>
																				<td align="left" >
																				</td>
																			</tr>
																			<tr><td colspan="4">&nbsp;</td></tr>
																			<tr id="productos" >
																				<td align="left" >
																					<h:outputText value="Ramo : " />
																				</td>
																				<td align="left" >
																	     			<h:selectOneMenu id="ramos" value="#{beanMonitoreo.inRamo}" binding="#{beanListaParametros.inRamo}" required="true" >
																	    				<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
																	    				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																	    				<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas"
																	    					oncomplete="javascript:exibirPanelesTareas(2,#{beanMonitoreo.tarea});" />
																	    			</h:selectOneMenu>
																				</td>
																				<td align="left">
																					<h:outputText value="Poliza : " />
																				</td>
																				<td align="left" >
																					<h:selectOneMenu id="polizas"  value="#{beanMonitoreo.inPoliza}" binding="#{beanListaParametros.inPoliza}" required="true" >
																						<f:selectItem itemValue="-1" itemLabel="Seleccione una p�liza"/>
																						<f:selectItem itemValue="0" itemLabel="Prima unica"/>
																						<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																						<a4j:support event="onchange" action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="inVenta" 
																							oncomplete="javascript:exibirPanelesTareas(2,#{beanMonitoreo.tarea});"/>
																					</h:selectOneMenu>
																				</td> 
																			</tr>
																			<tr><td colspan="4">&nbsp;</td></tr>
																			<tr id="cero" style="display: true">
																				<td align="left">
																					<h:outputText value="Id venta : " />
																				</td>
																				<td align="left" >
																					<%-- <a4j:outputPanel id="idVenta" ajaxRendered="true"> --%>
																						<h:selectOneMenu id="inVenta"  value="#{beanMonitoreo.inIdVenta}" disabled="#{beanListaParametros.habilitaComboIdVenta}" required="true">
																							<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																							<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																							<a4j:support event="onchange" oncomplete="javascript:exibirPanelesTareas(2,#{beanMonitoreo.tarea});" ajaxSingle="true" />
																						</h:selectOneMenu>
																					<%-- </a4j:outputPanel> --%>
																				</td>
																				<td align="left" >
																				</td>
																				<td align="left" >
																				</td>
																			</tr>
																			<tr><td colspan="4">&nbsp;</td></tr>
																			<tr id="fechas" >
																				<td align="left">
																					<h:outputText value="Fecha Inicio : " />
																				</td>
																				<td align="left" >
																					<rich:calendar
																						value="#{beanMonitoreo.fInicio}"
																						inputSize="10" popup="true" cellHeight="20px"
																						id="Ini1" cellWidth="100px" datePattern="dd/MM/yyyy"
																						styleClass="calendario">
																					</rich:calendar>
																				</td>
																				<td align="left">
																					<h:outputText value="Fecha Fin : " />
																				</td>
																				<td align="left" >
																					<rich:calendar value="#{beanMonitoreo.fFin}"
																						inputSize="10" popup="true" cellHeight="20px"
																						id="fin1" cellWidth="100px" datePattern="dd/MM/yyyy"
																						styleClass="calendario">
																					</rich:calendar>
																				</td>
																			</tr>
																			<tr><td colspan="4">&nbsp;</td></tr>
																			<tr>
																				<td align="center" colspan="4">
																					<a4j:commandButton id="botonConsultar"
																					  styleClass="boton"
																						value="Consultar" action="#{beanMonitoreo.sacarReportes}"  process="@this" 
																					 	oncomplete="exibirPanelesTareas(1,#{beanMonitoreo.tarea});" reRender="resultado1,resultado2,respuestas"/>
																				</td>
																			</tr>
																		</table>
																	</fieldset>
																	<fieldset style="border: 1px solid white">
																		<legend>
																			<a4j:outputPanel id="secRes1" ajaxRendered="true" >
																				<span id="muestras" >
																					<h:graphicImage value="../../images/ocultar_seccion.png" onclick="exibirPanelesTareas(1,#{beanMonitoreo.tarea});" title="Muestra" />&nbsp;
																				</span>
																				<span id="ocultas" style="display: none;"> 
																					<h:graphicImage value="../../images/mostrar_seccion.png" onclick="exibirPanelesTareas(2,#{beanMonitoreo.tarea});" title="Oculta" />&nbsp;
																				</span>
																				<span class="titulos_secciones">Resultados</span>
																			</a4j:outputPanel>
																		</legend>
																		<table id="panelRespuesta" >
																			<tr id="panel1" style="display: none;">		
																				<td align="center" colspan="3">
																					<a4j:outputPanel id="resultado1" ajaxRendered="true">
																						<rich:dataTable id="repTareas1" value="#{beanMonitoreo.listaCerti}" var="registro" columns="3" columnsWidth="30px, 30px, 30px" width="90px">
																							<f:facet name="header"><h:outputText value="#{beanMonitoreo.strTarea}" /></f:facet>
																							<rich:column>
																								<f:facet name="header"><h:outputText value=" " /></f:facet>
																								<h:outputText value="#{registro.desc1}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Total de registros" /> </f:facet>
																								<h:outputText value="#{registro.coceNuMovimiento}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Total de prima" /> </f:facet>
																								<h:outputText value="#{registro.coceMtPrimaReal}" />
																							</rich:column>
																						</rich:dataTable>
																						<br/>
																					</a4j:outputPanel>
																				</td>
																			</tr>
																			<tr id="panel2" style="display: none;">		
																				<td align="center" colspan="10">
																					<a4j:outputPanel id="resultado2" ajaxRendered="true">
																						<rich:dataTable id="repTareas2" value="#{beanMonitoreo.listaCerti}" var="registro" columns="10" 
																							rows="5" rowKeyVar="row" columnsWidth="10px, 10px, 50px, 10px, 10px, 10px, 10px, 10px, 10px, 10px" width="140px">
																							<f:facet name="header"><h:outputText value="#{beanMonitoreo.strTarea}" /></f:facet>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="No. P�liza" /></f:facet>
																								<h:outputText value="#{registro.coceCampon5}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Tipo de Endoso" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov1}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="No. Endoso" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov2}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Desde" /></f:facet>
																								<h:outputText value="#{registro.coceCampov3}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Hasta" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov4}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Prima neta" /> </f:facet>
																								<h:outputText value="#{registro.coceMtPrimaPura}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="DPO" /></f:facet>
																								<h:outputText value="#{registro.coceCampov5}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Recargo" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov6}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Impuesto" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov7}" />
																							</rich:column>
																							<rich:column>
																								<f:facet name="header"><h:outputText value="Total" /> </f:facet>
																								<h:outputText value="#{registro.coceCampov8}" />
																							</rich:column>
																							<f:facet name="footer">
																								<rich:datascroller align="center" for="repTareas2" maxPages="10" page="#{beanEndosoDatos.numPagina}" />
																							</f:facet>
																						</rich:dataTable>
																						<br/>
																					</a4j:outputPanel>
																				</td>
																			</tr>
																			<tr id="panel3" style="display: none;">		
																				<td align="center" colspan="3">
																					<a href="http://localhost:8080/Colectivos/faces/principal.com" >Click aqui para revisar la pantalla de Cedula de Emisi�n</a>
																				</td>
																			</tr>
																		</table>
																	</fieldset>
																</td>
															</tr>
												</table>
												</a4j:region>											
											</h:form>
											</div>
											<h:messages styleClass="errorMessage" globalOnly="true"/>			
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div align="center">
			<%@ include file="../footer.jsp" %>
		</div>
	</f:view>
</body>
</html>