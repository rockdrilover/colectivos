<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<link rel="stylesheet" type="text/css"
	href="../../css/estilosReporteador.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Reporteador</title>
</head>

<body>
	<f:view>
		<f:loadBundle
			basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"
			var="msgs" />
		<div align="center">
			<%@include file="../header.jsp"%>
		</div>
		<div align="center">
			<h:form id="frmReporteador">
				<table class="tablaPrincipal">
					<tr>
						<td align="center">
							<table class="tablaSecundaria">
								<tr>
									<td align="center">
										<div align="center">
											<table class="encabezadoTabla">
												<tr>
													<td width="10%" align="center">Secci�n</td>
													<td width="4%" align="center" bgcolor="#d90909">I</td>
													<td align="left" width="64%">Reporteador</td>
													<td align="right" width="12%">Fecha</td>
													<td align="left" width="10%"><input name="fecha"
														readonly="readonly"
														value="<%=request.getSession().getAttribute("fecha")%>"
														size="7" /></td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td align="center">
							<table>
								<tr>
									<td width="25%"><h:outputText value="Eliga una opcion: " />
									</td>
									<td width="50%"><h:selectOneRadio
											value="#{beanReporteador.inOpcionRep}" id="inOpcionRep"
											onclick="habilitaPreguntas(this.value)">
											<f:selectItem itemValue="1" itemLabel="Reporteador" />
											<f:selectItem itemValue="2" itemLabel="ReportesEspeciales" />
											<f:selectItem itemValue="3" itemLabel="Reportes Comisiones" />
											<a4j:support event="onclick"
												action="#{beanReporteador.limpiaDatos}" ajaxSingle="true"
												reRender="filtroRepor" />
										</h:selectOneRadio></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td colspan="2"><a4j:outputPanel id="mensaje"
								ajaxRendered="true">
								<h:outputText value="#{beanReporteador.respuesta}"
									id="strMensaje" styleClass="error" />
							</a4j:outputPanel></td>
					</tr>

					<tr>
						<td align="center" id="respCual1" style="display: none"><rich:spacer
								height="2px"></rich:spacer>
							<div align="center">
								<table class="botonera">
									<tr>
										<td class="frameTL"></td>
										<td class="frameTC"></td>
										<td class="frameTR"></td>
									</tr>
									<tr>
										<td class="frameCL"></td>
										<td class="frameC" width="700px"><a4j:region
												id="reporteador">
												<a4j:outputPanel id="filtroRepor" ajaxRendered="true">
													<table width="100%" align="center" border="0"
														cellpadding="1" cellspacing="1">
														<tr>
															<td align="right" width="35%" height="30"><h:outputText
																	value="Ramo:" /></td>
															<td align="left" width="65%" height="30"><h:selectOneMenu
																	id="inRamo" value="#{beanReporteador.inRamo}"
																	binding="#{beanListaParametros.inRamo}"
																	title="header=[Ramo] body=[Seleccione un ramo para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																	required="false">
																	<f:selectItem itemValue="0"
																		itemLabel="Selecione un ramo" />
																	<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
																	<a4j:support event="onchange"
																		action="#{beanListaParametros.cargaPolizas}"
																		ajaxSingle="true" reRender="polizas" />
																</h:selectOneMenu> <h:message for="inRamo" styleClass="errorMessage" /></td>
														</tr>
														<tr>
															<td align="right" width="35%" height="30"><h:outputText
																	value="Poliza:" /></td>
															<td align="left" width="65%" height="30"><a4j:outputPanel
																	id="polizas" ajaxRendered="true">
																	<h:selectOneMenu id="inPoliza"
																		value="#{beanReporteador.inPoliza}"
																		binding="#{beanListaParametros.inPoliza}"
																		title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																		required="false">
																		<f:selectItem itemValue="-1"
																			itemLabel="Seleccione una p�liza" />
																		<f:selectItem itemValue="0"
																			itemLabel="Prima �nica carga" />
																		<f:selectItems
																			value="#{beanListaParametros.listaComboPolizas}" />
																		<a4j:support event="onchange"
																			action="#{beanListaParametros.cargaIdVenta}"
																			ajaxSingle="true" reRender="idVenta" />
																	</h:selectOneMenu>
																	<h:message for="inPoliza" styleClass="errorMessage" />
																</a4j:outputPanel></td>
														</tr>
														<tr>
															<td align="right" width="35%" height="30"><h:outputText
																	value="Canal de Venta:" /></td>
															<td align="left" width="65%" height="30"><a4j:outputPanel
																	id="idVenta" ajaxRendered="true">
																	<h:selectOneMenu id="inIdVenta"
																		value="#{beanReporteador.inIdVenta}"
																		title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																		disabled="#{beanListaParametros.habilitaComboIdVenta}">
																		<f:selectItem itemValue="0"
																			itemLabel="Seleccione un canal de venta" />
																		<f:selectItems
																			value="#{beanListaParametros.listaComboIdVenta}" />
																		<a4j:support event="onchange"
																			action="#{beanListaPreCarga.setValor}"
																			ajaxSingle="true" reRender="idVenta" />
																	</h:selectOneMenu>
																</a4j:outputPanel></td>
														</tr>
														<tr>
															<td align="right" width="35%" height="30"><h:outputText
																	value="Status:" /></td>
															<td align="left" width="65%" height="30"><a4j:outputPanel
																	id="idEstatus" ajaxRendered="true">
																	<h:selectOneMenu id="estatus"
																		value="#{beanReporteador.estatus}"
																		binding="#{beanReporteador.hsomEstatus}"
																		title="header=[Estatus] body=[Seleccione un estatus para poder realizar la consulta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																		required="false" style="width:160px;">
																		<f:selectItem itemValue="0"
																			itemLabel="Seleccione un estatus" />
																		<f:selectItems
																			value="#{beanReporteador.listaComboEstatus}" />
																		<a4j:support id="suppEstatusS" event="onchange"
																			action="#{beanReporteador.setColumnas}"
																			ajaxSingle="true" reRender="nombreColumnas" />
																	</h:selectOneMenu>
																</a4j:outputPanel></td>
														</tr>
														<tr>
															<td align="right" width="35%" height="30"><h:outputText
																	value="Fecha Inicial:" /></td>
															<td align="left" width="65%" height="30"><rich:calendar
																	id="fechainicio" datePattern="dd/MM/yyyy"
																	required="false" style="width:170px"
																	value="#{beanReporteador.fechainicio}" /> <h:message
																	for="fechaInicial" styleClass="errorMessage" /></td>
														</tr>
														<tr>
															<td align="right" width="35%" height="30"><h:outputText
																	value="Fecha Final:" /></td>
															<td align="left" width="65%" height="30"><rich:calendar
																	id="fechafin" datePattern="dd/MM/yyyy" required="false"
																	style="width:170px" value="#{beanReporteador.fechafin}" />
																<h:message for="fechaFinal" styleClass="errorMessage" />
															</td>
														</tr>
														<tr>
															<td align="right" width="35%" height="30"><h:outputText
																	value="Tipo Archivo:" /></td>
															<td align="left" width="65%" height="30"><a4j:outputPanel
																	id="idTipoArchivo" ajaxRendered="true">
																	<h:selectOneMenu id="tipoarchivo"
																		value="#{beanReporteador.tipoarchivo}"
																		title="header=[Tipo Archivo] body=[Seleccione el tipo de archivo en el cual se descargaran los resultados de la consulta.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																		required="false" style="width:160px;">
																		<f:selectItem itemValue="0"
																			itemLabel="Seleccione tipo de archivo" />
																		<f:selectItems id="tipoArchivoItems"
																			value="#{beanReporteador.listaComboTipoArchivo}" />
																		<a4j:support id="suppTipoArchivo" event="onchange"
																			ajaxSingle="true" reRender="tipoarchivo" />
																	</h:selectOneMenu>
																</a4j:outputPanel></td>
														</tr>
														<tr>
															<td colspan="2" align="right"><a4j:commandButton
																	styleClass="boton" id="btnConsultar"
																	action="#{beanReporteador.consultar}"
																	onclick="this.disabled=true"
																	oncomplete="this.disabled=false"
																	reRender="secRes,regTabla,mensaje" value="Consultar" />
																<rich:toolTip for="btnConsultar"
																	value="Consultar Reporte" />
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
														</tr>

														<tr class="espacio_5">
															<td colspan="2">&nbsp;</td>
														</tr>

														<tr>
															<td colspan="2" align="center">
																<table width="100%">
																	<tr>
																		<td width="90%" align="center"><a4j:outputPanel
																				id="nombreColumnas" ajaxRendered="true">
																				<rich:panelBar width="100%"
																					contentClass="texto_normal">
																					<rich:panelBarItem label="   Ocultar"
																						headerStyle="text-align: left;"></rich:panelBarItem>
																					<rich:panelBarItem
																						label="   Seleccion de campos para generar el reporte."
																						headerStyle="text-align: left;">
																						<h:panelGrid columns="3" width="100%">
																							<h:selectManyCheckbox
																								value="#{beanReporteador.campos1}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanReporteador.listaCampos1}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanReporteador.campos2}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanReporteador.listaCampos2}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanReporteador.campos3}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanReporteador.listaCampos3}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanReporteador.campos4}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanReporteador.listaCampos4}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanReporteador.campos5}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanReporteador.listaCampos5}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanReporteador.campos6}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanReporteador.listaCampos6}" />
																							</h:selectManyCheckbox>
																							<h:selectManyCheckbox
																								value="#{beanReporteador.campos7}"
																								layout="pageDirection">
																								<f:selectItems
																									value="#{beanReporteador.listaCampos7}" />
																							</h:selectManyCheckbox>
																						</h:panelGrid>
																					</rich:panelBarItem>

																					<c:if
																						test="${beanReporteador.lstResultados != null}">
																						<rich:panelBarItem label=" Resultados"
																							headerStyle="text-align: left;">
																							<table width="100%">
																								<tr>
																									<td title="Total registros" width="80%"><h:graphicImage
																											value="../../images/imgEmitir.png" />&nbsp; <strong>
																											<h:outputText id="lblEmitir" value="Total: " />&nbsp;
																											${fn:length(beanReporteador.lstResultados)}
																									</strong></td>
																									<td><h:commandButton
																											action="#{beanReporteador.descargar}"
																											value="Generar Reporte"
																											title="header=[Descargar] body=[Descarga el detalle en un archivo .csv.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
																									</td>
																								</tr>
																								<rich:spacer height="10px"></rich:spacer>
																								<tr>
																									<td colspan="2">
																										<div>
																											<table cellpadding="0" cellspacing="0"
																												class="tablaConsultaReporteador"
																												align="center">
																												<tr class="encabezadoTablaReporteador">
																													<c:forEach var="nombreCol"
																														items="#{beanReporteador.lstColumnas}">
																														<td align="center"><strong>&nbsp;&nbsp;${nombreCol}&nbsp;&nbsp;</strong></td>
																													</c:forEach>
																												</tr>

																												<c:set var="campos"
																													value="${fn:length(beanReporteador.lstColumnas)}" />
																												<c:forEach var="registro"
																													items="#{beanReporteador.lstResultados}"
																													end="30">
																													<tr class="contenidoTablaReporteador">
																														<c:forEach var="i" begin="0" step="1"
																															end="${campos - 1}">
																															<c:if test="${i % 2 == 0}">
																																<td align="center"
																																	style="padding-left: 20px; padding-right: 20px">${registro[i]}</td>
																															</c:if>
																															<c:if test="${i % 2 != 0}">
																																<td align="center"
																																	style="padding-left: 20px; padding-right: 20px">${registro[i]}</td>
																															</c:if>
																														</c:forEach>
																													</tr>
																												</c:forEach>
																											</table>
																										</div>
																									</td>
																								</tr>
																							</table>
																						</rich:panelBarItem>
																					</c:if>
																				</rich:panelBar>
																			</a4j:outputPanel></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>

													<rich:spacer height="15px"></rich:spacer>

													<table>
														<tr>
															<td align="center"><a4j:status for="reporteador"
																	stopText=" ">
																	<f:facet name="start">
																		<h:graphicImage value="/images/ajax-loader.gif" />
																	</f:facet>
																</a4j:status></td>
														</tr>
													</table>
												</a4j:outputPanel>
											</a4j:region></td>
										<td class="frameCR"></td>
									</tr>
									<tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
								</table>
							</div></td>
					</tr>

					<tr>
						<td align="center" id="respCual2" style="display: none"><rich:spacer
								height="2px"></rich:spacer>
							<div align="center">
								<table class="botonera">
									<tr>
										<td class="frameTL"></td>
										<td class="frameTC"></td>
										<td class="frameTR"></td>
									</tr>
									<tr>
										<td class="frameCL"></td>
										<td class="frameC" width="700px"><a4j:region
												id="reportesEspeciales">
												<table width="100%" align="center" border="0"
													cellpadding="1" cellspacing="1">
													<tr>
														<td align="left" colspan="2"><h:outputText
																value="Seleccione un tipo reporte: " /></td>
														<td align="left"><h:selectOneMenu
																id="inReporteEspecial"
																value="#{beanReporteador.inReporteEspecial}">
																<f:selectItem itemValue="0"
																	itemLabel="Seleccione una opcion" />
																<f:selectItem itemValue="1" itemLabel="Reporte Cobranza" />
																<f:selectItem itemValue="2"
																	itemLabel="Reporte Emision Corte Contable" />
																<f:selectItem itemValue="3" itemLabel="Reporte Fiscal" />
																<f:selectItem itemValue="4"
																	itemLabel="Reporte Estimacion" />
																<f:selectItem itemValue="5"
																	itemLabel="Reporte Base Final" />
																<f:selectItem itemValue="6"
																	itemLabel="Reporte Integracion de Cuentas" />
																<a4j:support event="onchange"
																	action="#{beanReporteador.limpiaDatos}"
																	ajaxSingle="true" reRender="filtroRE" />
															</h:selectOneMenu></td>

														<td align="right"><h:commandButton
																action="#{beanReporteador.consultaReporteEspecial}"
																value="Consultar Reporte"
																title="header=[Descargar] body=[Descargar reporte.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
														</td>
													</tr>

													<tr class="espacio_10">
														<td colspan="4">&nbsp;</td>
													</tr>
													<tr class="espacio_10">
														<td colspan="4">&nbsp;</td>
													</tr>

													<tr>
														<td colspan="4" width="90%"><a4j:outputPanel
																id="filtroRE" ajaxRendered="true">
																<table width=100% " border="0" cellpadding="1"
																	cellspacing="1">
																	<c:if test="${beanReporteador.inReporteEspecial == 3}">
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Ramo:" /></td>
																			<td align="left" width="76%" height="30"><h:selectOneMenu
																					id="inRamo2" value="#{beanReporteador.inRamo}"
																					title="header=[Ramo] body=[Seleccione un ramo para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																					required="false">
																					<f:selectItem itemValue="0"
																						itemLabel="Selecione un ramo" />
																					<f:selectItems
																						value="#{beanListaRamo.listaComboRamos}" />
																				</h:selectOneMenu> <h:message for="inRamo2" styleClass="errorMessage" />
																			</td>
																		</tr>
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Poliza:" /></td>
																			<td align="left" width="76%" height="30"><a4j:outputPanel
																					id="polizas2" ajaxRendered="true">
																					<h:selectOneMenu id="inPoliza2"
																						value="#{beanReporteador.inPoliza}"
																						title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"
																						required="false">
																						<f:selectItem itemValue="-1"
																							itemLabel="Seleccione una p�liza" />
																						<f:selectItem itemValue="0"
																							itemLabel="Prima �nica carga" />
																					</h:selectOneMenu>
																					<h:message for="inPoliza2"
																						styleClass="errorMessage" />
																				</a4j:outputPanel></td>
																		</tr>
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Canal de Venta:" /></td>
																			<td align="left" width="76%" height="30"><a4j:outputPanel
																					id="idVenta2" ajaxRendered="true">
																					<h:selectOneMenu id="inIdVenta2"
																						value="#{beanReporteador.inIdVenta}"
																						title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																						<f:selectItem itemValue="0"
																							itemLabel="Seleccione un canal de venta" />
																						<f:selectItems
																							value="#{beanReporteador.listaComboIdVenta}" />
																					</h:selectOneMenu>
																				</a4j:outputPanel></td>
																		</tr>
																	</c:if>

																	<c:if
																		test="${beanReporteador.inReporteEspecial == 1 || beanReporteador.inReporteEspecial == 2 || beanReporteador.inReporteEspecial == 3 || beanReporteador.inReporteEspecial == 6}">
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Filtro fechas: " /></td>
																			<td align="left" width="76%" height="30"><h:selectOneMenu
																					id="inFiltroFechas"
																					onchange="showOptFechas(this.value, 2)"
																					value="#{beanReporteador.inFiltroFechas}">
																					<f:selectItem itemValue="0"
																						itemLabel="Seleccione opcion" />
																					<f:selectItem itemValue="1" itemLabel="Por mes" />
																					<f:selectItem itemValue="2" itemLabel="Por fechas" />
																				</h:selectOneMenu></td>
																		</tr>
																		<tr class="espacio_5">
																			<td colspan="2">&nbsp;</td>
																		</tr>
																		<tr id="tdMes2" style="display: none">
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Mes del Reporte: " /></td>
																			<td align="left" width="76%" height="30"><h:selectOneMenu
																					id="inMes2" value="#{beanReporteador.mes}"
																					style="width: 150px" title="Seleccione un mes">
																					<f:selectItems value="#{beanReporteador.listaMes}" />
																				</h:selectOneMenu></td>
																		</tr>
																		<tr id="tdFechasIni2" style="display: none">
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Fecha Inicial:" /></td>
																			<td align="left" width="76%" height="30"><rich:calendar
																					id="fechainicio2" datePattern="dd/MM/yyyy"
																					required="false" style="width:120px"
																					value="#{beanReporteador.fechainicio}" /></td>
																		</tr>
																		<tr id="tdFechasFin2" style="display: none">
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Fecha Final:" /></td>
																			<td align="left" width="76%" height="30"><rich:calendar
																					id="fechafin2" datePattern="dd/MM/yyyy"
																					required="false" style="width:120px"
																					value="#{beanReporteador.fechafin}" /></td>
																		</tr>
																	</c:if>
																</table>
															</a4j:outputPanel></td>
													</tr>
												</table>

												<rich:spacer height="15px"></rich:spacer>

												<table>
													<tr>
														<td align="center"><a4j:status
																for="reportesEspeciales" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status></td>
													</tr>
												</table>
											</a4j:region></td>
										<td class="frameCR"></td>
									</tr>
									<tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
								</table>
							</div></td>
					</tr>


					<tr>
						<td align="center" id="respCual3" style="display: none"><rich:spacer
								height="2px"></rich:spacer>
							<div align="center">
								<table class="botonera">
									<tr>
										<td class="frameTL"></td>
										<td class="frameTC"></td>
										<td class="frameTR"></td>
									</tr>
									<tr>
										<td class="frameCL"></td>
										<td class="frameC" width="700px"><a4j:region
												id="reportesComisiones">
												<table width="100%" align="center" border="0"
													cellpadding="1" cellspacing="1">
													<tr>
														<td align="left" colspan="2"><h:outputText
																value="Seleccione un tipo reporte: " /></td>
														<td align="left"><h:selectOneMenu
																id="inReporteComisiones"
																value="#{beanReporteador.inReporteComisiones}">
																<f:selectItem itemValue="0"
																	itemLabel="#{msgs.sistemaseleccione}" />
																<f:selectItem itemValue="7"
																	itemLabel="#{msgs.repcomisiones1}" />
																<f:selectItem itemValue="8"
																	itemLabel="#{msgs.repcomisiones2}" />
																<f:selectItem itemValue="9"
																	itemLabel="#{msgs.repcomisiones3}" />
																<f:selectItem itemValue="10"
																	itemLabel="#{msgs.repcomisiones4}" />
																<f:selectItem itemValue="11" 
																	itemLabel="#{msgs.repcomisiones5}" />
																<f:selectItem itemValue="12"
																	itemLabel="#{msgs.repcomisiones6}" />																
																<a4j:support event="onchange"
																	action="#{beanReporteador.limpiaDatos}"
																	ajaxSingle="true" reRender="filtroRE" />
															</h:selectOneMenu></td>

														<td align="right"><h:commandButton
																action="#{beanReporteador.consultaReporteComisiones}"
																value="Consultar Reporte"
																title="header=[Descargar] body=[Descargar reporte.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
														</td>
													</tr>

													<tr class="espacio_10">
														<td colspan="4">&nbsp;</td>
													</tr>
													<tr class="espacio_10">
														<td colspan="4">&nbsp;</td>
													</tr>

													<tr>
														<td colspan="4" width="90%"><a4j:outputPanel
																id="filtroRC" ajaxRendered="true">
																<table width="100%" border="0" cellpadding="1"
																	cellspacing="1">


																	
																		<c:if
																		test="${beanReporteador.inReporteComisiones == 7 || beanReporteador.inReporteComisiones == 8 || beanReporteador.inReporteComisiones == 9 || beanReporteador.inReporteComisiones == 10 || beanReporteador.inReporteComisiones == 11 || beanReporteador.inReporteComisiones == 12 || beanReporteador.inReporteComisiones == 13 || beanReporteador.inReporteComisiones == 14}">
											
																		<tr>
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Filtro fechas: " /></td>
																			<td align="left" width="76%" height="30"><h:selectOneMenu
																					id="inFiltroFechasComisiones"
																					onchange="showOptFechas(this.value, 3)"
																					value="#{beanReporteador.inFiltroFechasRC}">
																					<f:selectItem itemValue="0"
																						itemLabel="Seleccione opcion" />
																					<f:selectItem itemValue="1" itemLabel="Por mes" />
																					<f:selectItem itemValue="2" itemLabel="Por fechas" />
																				</h:selectOneMenu></td>
																		</tr>
																		<tr class="espacio_5">
																			<td colspan="2">&nbsp;</td>
																		</tr>
																		<tr id="tdMes3" style="display: none">
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Mes del Reporte: " /></td>
																			<td align="left" width="76%" height="30"><h:selectOneMenu
																					id="inMes3" value="#{beanReporteador.mesRC}"
																					style="width: 150px" title="Seleccione un mes">
																					<f:selectItems value="#{beanReporteador.listaMes}" />
																				</h:selectOneMenu></td>
																		</tr>
																		<tr id="tdFechasIni3" style="display: none">
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Fecha Inicial:" /></td>
																			<td align="left" width="76%" height="30"><rich:calendar
																					id="fechainicio3" datePattern="dd/MM/yyyy"
																					required="false" style="width:120px"
																					value="#{beanReporteador.fechainicioRC}" /></td>
																		</tr>
																		<tr id="tdFechasFin3" style="display: none">
																			<td align="right" width="24%" height="30"><h:outputText
																					value="Fecha Final:" /></td>
																			<td align="left" width="76%" height="30"><rich:calendar
																					id="fechafin3" datePattern="dd/MM/yyyy"
																					required="false" style="width:120px"
																					value="#{beanReporteador.fechafinRC}" /></td>
																		</tr>
																		</c:if>
																</table>
															</a4j:outputPanel></td>
													</tr>
												</table>

												<rich:spacer height="15px"></rich:spacer>

												<table>
													<tr>
														<td align="center"><a4j:status
																for="reportesComisiones" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status></td>
													</tr>
												</table>
											</a4j:region></td>
										<td class="frameCR"></td>
									</tr>
									<tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
								</table>
							</div></td>
					</tr>
				</table>
			</h:form>
		</div>
		<rich:spacer height="10px" />
		<div align="center">
			<%@ include file="../footer.jsp"%>
		</div>
	</f:view>
	<script>
		if (document.getElementById('frmReporteador:inOpcionRep:0').checked == true) {
			habilitaPreguntas('1');
		}
		if (document.getElementById('frmReporteador:inOpcionRep:1').checked == true) {
			habilitaPreguntas('2');
		}
		if (document.getElementById('frmReporteador:inOpcionRep:2').checked == true) {
			habilitaPreguntas('3');
		}
		showOptFechas(
				document.getElementById('frmReporteador:inFiltroFechas').value,
				2);
	</script>
</body>
</html>
