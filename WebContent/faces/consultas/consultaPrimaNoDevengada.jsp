<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>C�lculo de Devoluci�n de Prima</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">C�lculo de Devoluci�n de Prima</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>
				    <rich:spacer height="10px"></rich:spacer>
				    <rich:dragIndicator id="indicador" />
				    <h:form>
				    <div align="center">
				    <a4j:region id="consulta">
				    <table class="botonera" align="center">
				    <tbody align="center">
					    <tr>
						    <td class="frameTL"></td>
						    <td class="frameTC"></td>
						    <td class="frameTR"></td>
					    </tr>
					    <tr align="center">
						    <td class="frameCL"></td>
						    <td class="frameC" align="center">
						    <rich:panel id="opciones" header="C�lculo de Prima de Seguros a Devolver por Cancelaci�n de P�liza" style=" width: 65%; align:center; background-color:#E5E0EC" styleClass="tablaSecundaria">
							    <table align="center">
					            <tbody>
					            	<tr>
					            		<td width="50%">
					            			<table>
									     		<tr>
									     			<td align="center" width="50%">
									     			    <br/>
									     				<table>
									     					<tr>
										    	        		<td colspan="2">
										    	        			<table border="1">
										    	        				<tr>
													    	        		<td align="center" width="90%" style="font-size:15px; font-weight: bold;">
													    	        			 <h:graphicImage id="logo1" value="../../images/tasa.png" height="3%" width="3%"/> Informacion del Cr�dito
													    	        		</td>
												    	        		</tr>
												    	        	</table>
												    	        	<br/>
										    	        		</td>
										    	        	</tr>
									     					<tr>
													     		<td align="left" width="60%">
													     			<h:outputText value="Tasa Anualizada Amortizaci�n del Cr�dito: " />
													     		</td>
													     		<td align="left" width="40%">				     						     		
													     			 <h:inputText id="tasa"  value="#{beanCancelacionIndividual.tasaAmortiza.copaNvalor7}">
													     			 	<f:validateDoubleRange minimum="0"/>
													     			 </h:inputText>%
													     			 <rich:message for="tasa" />
													     			 
													     		</td>		
													     	</tr>
												     	</table>
												     	<br/>
											     	</td>	     							     		
									     		</tr>
									     		<tr>
									     			<td align="center" width="50%">
									     			<rich:separator width="60%"/>
									     			<br/>
										    	        <table>
										    	        	<tr>
										    	        		<td colspan="2">
										    	        			<table border="1">
										    	        				<tr>
													    	        		<td align="center" width="90%" style="font-size:15px; font-weight: bold;">
													    	        			 <h:graphicImage id="logo2" value="../../images/Calender.png" height="3%" width="3%"/> Vigencia del Seguro
													    	        		</td>
												    	        		</tr>
												    	        	</table>
												    	        	<br/>
										    	        		</td>
										    	        	</tr>
												     		<tr>
											     		   		<td align="left" width="60%">
											     		   			<h:outputText value="Temporalidad del Seguro en meses: " />
											     		   		</td>
											     		   		<td align="left" width="40%">
											     					<h:inputText id="palzo"  value="#{beanCancelacionIndividual.certifCancelado.certificado.coceCdPlazo}" >
											     						<f:validateDoubleRange minimum="1"/>
											     						<f:convertNumber type="number" maxFractionDigits="0" locale="es_mx"/>
											     						<a4j:support event="onchange"  action="#{beanCancelacionIndividual.determinaFechaFinCredito}" ajaxSingle="true" reRender="fefin" />				     		
											     					</h:inputText>
											     				</td>
										     				</tr>		     		
												     		<tr>
													     		<td align="left"><h:outputText value="Inicio de Vigencia del Seguro: " /></td>
													     		<td align="left">
													     		<rich:calendar binding="#{beanCancelacionIndividual.cal}" id="feini" value="#{beanCancelacionIndividual.certifCancelado.certificado.coceFeIniCredito}" inputSize="20" 
												    				popup="true" cellHeight="20px" cellWidth="20px" datePattern="dd/MM/yyyy" styleClass="calendario">
												    				<a4j:support event="onchanged" action="#{beanCancelacionIndividual.determinaFechaFinCredito}" reRender="fefin" />
												    			</rich:calendar>
													     		</td>
												     		</tr>
									                        <tr>
													     		<td align="left"><h:outputText value="Fin de Vigencia Seguro: " /></td>
													     		<td align="left" style="font-size:12px; background:#C2D3E8; font-weight: bold;">
													     			<h:outputText id="fefin" value="#{beanCancelacionIndividual.certifCancelado.certificado.coceFeFinCredito}">
													     				<f:convertDateTime pattern="dd/MM/yyyy" dateStyle="short"/>
													     			</h:outputText>
													     		</td>
												     		</tr>
												     		<tr>
													     		<td align="left"><h:outputText value="Fecha de Cancelaci�n: " /></td>
													     		<td align="left">
														     		<rich:calendar value="#{beanCancelacionIndividual.fechaAnulacion}" inputSize="20" 
													    				popup="true" cellHeight="20px"  
													    				cellWidth="20px" datePattern="dd/MM/yyyy" styleClass="calendario">
													    			</rich:calendar>
													     		</td>
												     		</tr>
												     	</table>
												     	<br/>
										     		</td>
									     		</tr>	
									     		
									     		<tr>
									     			<td align="center" width="50%">
									     			<rich:separator width="60%"/>
									     			<br/>
									     				<table>
									     					<tr>
										    	        		<td colspan="2">
										    	        			<table border="1">
										    	        				<tr>
													    	        		<td align="center" width="90%" style="font-size:15px; font-weight: bold;">
													    	        			 <h:graphicImage id="logo3" value="../../images/kreversi.png" height="3%" width="3%"/> Primas del Seguro
													    	        		</td>
												    	        		</tr>
												    	        	</table>
												    	        	<br/>
										    	        		</td>
										    	        	</tr>
									     					<tr>
													     		<td align="left" width="60%"><h:outputText value="Monto Pagado de Prima �nica:  " /></td>
													     		<td align="left" width="40%">
													     			$<h:inputText id="pu"  value="#{beanCancelacionIndividual.certifCancelado.certificado.coceMtPrimaSubsecuente}" >
													     				<f:convertNumber type="number" maxFractionDigits="2" locale="es_mx"/>
													     				<f:validateDoubleRange minimum="0"/>
													     			</h:inputText>	
													     		</td>
													     	</tr>
													     	<tr>
													     		<td>
													     			<rich:separator width="0%"/>
													     		</td>
													     	</tr>
													     	<tr style="font-size:15px; background:#C2D3E8; font-weight: bold;">
								            					<td align="left"> <h:outputText value="Prima a Devolver por Cancelaci�n:    " /> </td>
								            					<td align="left">
									            					<h:outputText id="primaDevo"  value="#{beanCancelacionIndividual.primaDevuelta}">
									            						<f:convertNumber type="currency" currencySymbol="$" locale="es_mx"/> 
									            					</h:outputText>
								            					</td>
								            				</tr>
												     	</table>
												     	<br/>
										     		</td>
									     		</tr>
									     		<tr>
									    			<td align="center">
														<rich:spacer height="5px"></rich:spacer>
														<div align="center" >
															<h:outputText id="respuesta2" value="#{beanCancelacionIndividual.respuesta}" styleClass="respuesta" />
														</div>
													</td>
												</tr>
					            				<tr>
					            					<td colspan="2">
						            					<a4j:commandButton styleClass="boton" 
									     					action="#{beanCancelacionIndividual.calculaDevolucion}" value="Calcular" reRender="primaDevo,respuesta2"
									     					onclick="this.disabled=true" oncomplete="this.disabled=false" 
															title="header=[Calcula Devolucion] body=[Calcula la devoluci�n con base a los datos registrados] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
														</a4j:commandButton>
					            					</td>
					            				</tr>

					            			</table>
					            			
					            		</td>		   
					            	</tr>
					            </table>
					            </rich:panel>
				            </td>
						    <td class="frameCR"></td>
					    </tr>
					    <tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
				    </tbody>
				    </table>
				    </a4j:region>
				    				
			     	</h:form>
					</div>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div align="center">
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>