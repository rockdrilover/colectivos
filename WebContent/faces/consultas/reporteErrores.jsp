<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<title>Reporte Errores</title>
	</head>
	
	<body>
		<f:view>  <!-- Desde aqui comienzo --> 
		<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
		<table class="tablaPrincipal">
			<tr>
				<td align="center">
					<table class="tablaSecundaria">
						<tr>
							<td>
								<div align="center">
								<table class="encabezadoTabla" >
									<tr>
										<td width="10%" align="center">Secci�n</td>
										<td width="4%" align="center" bgcolor="#d90909">X</td>
										<td align="left" width="64%">Reporte Errores</td>
										<td align="right" width="12%">Fecha</td>
										<td align="left" width="10%">
											<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
										</td>
									</tr>
								</table>
			    				</div>	
				    			
				    			<rich:spacer height="10px"></rich:spacer>
				    			<h:form id="frmReporteErrores">
				      				<div align="center">
					    			<rich:spacer height="15px"/>	
					    			<table class="botonera">
									    <tr>
										    <td class="frameTL"></td>
										    <td class="frameTC"></td>
										    <td class="frameTR"></td>
									    </tr>
						    			<tr>
							    			<td class="frameCL"></td>
							    			<td class="frameC" width="700px">
							    			<a4j:region id="consultaDatos">
								     			<table cellpadding="2" cellspacing="0">
													<tr>
														<td width="30%"></td>
														<td width="40%"></td>
														<td width="30%"></td>
													</tr>
										     		<tr height="10px"><td colspan="3" ></td></tr>
							     					<tr>
							     						<td colspan="3" align="center">
							     			    			<a4j:region id="regionOpciones">
										    				<rich:panel id="opciones" header="Opciones" style=" width: 100%; align:center; background-color:#E5E0EC" styleClass="tablaSecundaria">
										    	 				<f:facet name="header"><h:outputText value="Opciones del Reporte" /></f:facet>       
								     							<table>
											     					<tr>
																		<td align="right" height="25"><h:outputText value="Ramo:" /></td>
																		<td colspan="2" align="left" height="25">
																		 	<h:selectOneMenu id="ramo" value="#{beanReporteErrores.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
																				<f:selectItem itemValue="-1" itemLabel="Seleccione un Ramo" />
																				<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																				<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
																		    </h:selectOneMenu>
																			<h:message for="ramo" styleClass="errorMessage"/>
																		</td>  
																	</tr>
															    	<tr>
															    		<td align="right" height="25"><h:outputText value="Poliza:"/></td>
																		<td colspan="2" align="left" height="25">
																			<a4j:outputPanel id="polizas" ajaxRendered="true">
																				<h:selectOneMenu id="poliza"  value="#{beanReporteErrores.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																						<f:selectItem itemValue="-1" itemLabel="Seleccione una p�liza"/>
																						<f:selectItem itemValue="0" itemLabel="Prima �nica"/>
																						<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																						<a4j:support event="onchange" action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta, idPoliza" />
																				</h:selectOneMenu>
																				<h:message for="poliza" styleClass="errorMessage" />
																			</a4j:outputPanel>
																		</td>
															    	</tr>
															    	<tr>
															    		<td align="right" height="25"><h:outputText value="Canal de Venta:"/></td>
																		<td colspan="2" align="left" height="25">
																			<a4j:outputPanel id="idVenta" ajaxRendered="true">
																				<h:selectOneMenu id="inVenta"  value="#{beanReporteErrores.inIdVenta}" title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
																				         disabled="#{beanListaParametros.habilitaComboIdVenta}">
																						<f:selectItem itemValue="-1" itemLabel="Seleccione una canal de venta"/>
																						<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
																						<a4j:support event="onchange" ajaxSingle="true" />
																				</h:selectOneMenu>
																			</a4j:outputPanel>
																		</td>
															    	</tr>
															    	<tr>
															    		<td align="right" height="25"><h:outputText value="Tipo Error:"/></td>
																		<td colspan="2" align="left" height="25">
																			<a4j:outputPanel id="tipoError" ajaxRendered="true">
																				<h:selectOneMenu id="inTipoError"  value="#{beanReporteErrores.inTipoError}" title="header=[Tipo Error] body=[Seleccione un tipo de error.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]">
																						<f:selectItem itemValue="-1" itemLabel="Seleccione una canal de venta"/>
																						<f:selectItem itemValue="1" itemLabel="Error de Conciliacion"/>
																						<f:selectItem itemValue="2" itemLabel="Error de Validacion"/>
																						<f:selectItem itemValue="3" itemLabel="Error de Carga"/>
																						<f:selectItem itemValue="4" itemLabel="Error de Emision"/>
																						<a4j:support event="onchange" ajaxSingle="true" />
																				</h:selectOneMenu>
																			</a4j:outputPanel>
																		</td>
															    	</tr>
													                <tr>
																		<td align="right">
								     	                                	<h:outputText value="Fecha inicial:"/>
																		</td>
																		<td colspan="2" align="left">
									                                        <rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy" required="true" value="#{beanReporteErrores.fecini}" />
									                                        <h:message for="fechaInicial" styleClass="errorMessage" />
								     	                                </td>
								                                    </tr>
								                                    <tr>
																		<td align="right">
								                                        	<h:outputText value="Fecha final:"/>
																		</td>
																		<td colspan="2" align="left">
									                                        <rich:calendar id="fechaFinal" datePattern="dd/MM/yyyy" required="true" value="#{beanReporteErrores.fecfin}" />
									                                        <h:message for="fechaFinal" styleClass="errorMessage" />
								                                        </td>
								                                    </tr> 
								     							</table>
							     							</rich:panel>
							     							</a4j:region>
										     			</td>
										     		</tr>		
										     		
													<tr height="10px"><td colspan="3" ></td></tr>
				           	                		<tr>
				           	                		  	<td colspan="3" align="center">
				           	                		  		<h:commandButton action="#{beanReporteErrores.generaReporte}" value="Generar Reporte" >
															 	<a4j:support event="onclick" ajaxSingle="true" reRender="barra,mensaje"/>
															</h:commandButton>  
														</td>
													</tr>	
													<tr>
														<td colspan="3" align="center">
															<br/>
															<h:outputText id="mensaje" value="#{beanReporteErrores.mensaje}" styleClass="respuesta"/>
															<br/>
														</td>
													</tr>
								    			</table>
								    			<rich:spacer height="15px"></rich:spacer>
								    		
								    			<table>
										    		<tr>
							    						<td align="center">
											    			<rich:progressBar value="#{beanReporteErrores.progressValue}" id="barra"  
															     enabled= "#{beanReporteErrores.progressHabilitar}" style=" width : 250px;" interval="500" rendered="true" reRenderAfterComplete="frmReporteStock">
																<h:outputText value="Generando Reporte" />
																<f:facet name="complete">
																	<h:outputText value="Proceso Completado" style=" color : #FFFFFF; font-size: 15px; font-weight: bold;"/>
																</f:facet>
															</rich:progressBar>
														</td>
													</tr>
												</table>
											</a4j:region>								    
							    			<td class="frameCR"></td>
						    			</tr>
									    <tr>
											<td class="frameBL"></td>
											<td class="frameBC"></td>
											<td class="frameBR"></td>
										</tr>
								    </table>
									</div>
			     				</h:form>
							</td>
						</tr>
   					</table>
   				</td>
   			</tr>
		</table>   	
		</div>
		
		<rich:spacer height="10px"></rich:spacer>
	
		<div><%@include file="../footer.jsp" %></div>
		
		</f:view>
	</body>
</html>