<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Historial de Cr�ditos </title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>	
		<td align="center">
		    <table class="tablaSecundaria">
				<tbody>
					<tr>
						<td align="center">
						<div align="center">
							<table class="encabezadoTabla" >
								<tbody>
									<tr>
										<td width="10%" align="center">Secci�n</td>
										<td width="4%" align="center" bgcolor="#d90909">||||</td>
										<td align="left" width="64%">Historial de Cr�ditos</td>
										<td align="right" width="12%" >Fecha</td>
										<td align="left" width="10%">
											<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
										</td>
									</tr>
								</tbody>
							</table>
					  </div>	
					  <rich:spacer height="10px"></rich:spacer>
					  <h:form>
							<div align="center">
						    <rich:spacer height="15px"/>	
						    <table class="botonera">
						    <tbody>
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="700px">
								 	<a4j:region id="consulta">
									   <div align="center" > 
									    <table>
											<tbody>
												<tr>
													<td align="center"><h:outputText id="mensaje" value="#{beanHistorialCredito.message}" styleClass="respuesta" /></td>
												</tr>
												<tr>
													<td align="center"><h:outputText value="Seleccionar un filtro para la b�squeda:" /></td>
													<td align="left"><h:selectOneRadio id="RadioHistorial" value="#{beanHistorialCredito.tipoBusqueda}" >  
													   <f:selectItem itemValue="0" itemLabel="P�liza"/>   
													   <f:selectItem itemValue="1" itemLabel="Cr�dito"/>   
													   <f:selectItem itemValue="2" itemLabel="Nombre"/> 
													   <a4j:support event="onclick"  action="#{beanHistorialCredito.muestraCriterio}" reRender="panelCriterios3,panelCriterios1,panelCriterios2,regTabla"/>
													</h:selectOneRadio></td>
							                    </tr>
							                  </tbody>
							           </table> 
							             </div>
							            <div align="center">  
							              <table>
											<tbody>	
								                    <tr>
													<td width="5%">&nbsp;</td>
													<td colspan="2" align="left">
													<h:panelGrid id="panelCriterios1" columns="3" style="display:#{beanHistorialCredito.display1};" >
														<h:outputText value=" "/> 
														<h:outputText value="Ramo:" />
														<h:selectOneMenu id="ramo" value="#{beanHistorialCredito.ramo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
															<f:selectItem itemValue="0" itemLabel="Ramo" />
															<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
															<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
														</h:selectOneMenu>
													
														<h:outputText value=" "/> 
														<h:outputText value="Poliza:"/>
														<h:selectOneMenu id="polizas"  value="#{beanHistorialCredito.poliza}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" >
																<f:selectItem itemValue="0" itemLabel="Seleccione una p�liza"/>
																<f:selectItem itemValue="1" itemLabel="Prima �nica"/>
																<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																<a4j:support event="onchange" action="#{beanCancelacionIndividual.muestraPE}" ajaxSingle="true"></a4j:support>
														</h:selectOneMenu>
														
														<h:outputText value=" "/> 
														<h:outputText value="Poliza Especifica:"/>
														<h:inputText id="polEspecifica"  value="#{beanHistorialCredito.polizaEspecifica}"/>
														
														<h:outputText value=" "/>
														<h:outputText value="Certificado:" />
														<h:inputText id="certificado"  value="#{beanHistorialCredito.certificado}"/>
													</h:panelGrid>
														
														<h:panelGrid id="panelCriterios2" columns="2" style="display:#{beanHistorialCredito.display2};">
															<h:outputText value="Credito:"/>
															<h:inputText id="inCredito" value="#{beanHistorialCredito.credito}"/>
															<rich:toolTip for="inCredito" value="Introduce el numero de credito (Solo numeros)" />
														</h:panelGrid>
														<h:panelGrid id="panelCriterios3" columns="2" style="display:#{beanHistorialCredito.display3};">
															<h:outputText value="Nombre:" />
															<h:inputText value="#{beanHistorialCredito.nombre}" />		
															<h:outputText value="Apellido Paterno:" />
															<h:inputText value="#{beanHistorialCredito.paterno}" />
															<h:outputText value="Apellido Materno:" />
															<h:inputText value="#{beanHistorialCredito.materno}" />
														</h:panelGrid>
													</td>
													<td width="5%">&nbsp;</td>
												</tr>	
												<tr>
													<td colspan="4" align="right">
														<a4j:commandButton styleClass="boton" value="Consultar" 
																		   action="#{beanHistorialCredito.historialCredito}" 
																		   onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   reRender="listaHistorial,mensaje"
																		   title="header=[Historial de Cr�ditos] body=[Consulta Historial de Cr�ditos.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>
													</td>
												</tr>	
											</tbody>
										</table>
									 </div>
										<rich:spacer height="15px"></rich:spacer>
									    <table>
											<tbody>
												<tr>
													<td align="center">
														<a4j:status for="consulta" stopText=" ">
															<f:facet name="start">
																<h:graphicImage value="/images/ajax-loader.gif" />
															</f:facet>
														</a4j:status>
													</td>
												</tr>
											</tbody>
										</table>
									    </a4j:region>
								    <td class="frameCR"></td>
								 </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
							    </tbody>
							    </table>
							    <rich:spacer height="10px"></rich:spacer>
								    <table class="botonera">
									    <tbody>
										    <tr>
											    <td class="frameTL"></td>
											    <td class="frameTC"></td>
											    <td class="frameTR"></td>
										    </tr>
										    <tr>
											    <td class="frameCL"></td>
											    <td class="frameC" width="700px">
											     	<table>
												     	<tr>
												     		<td align="center">
												     		<a4j:outputPanel id="regTabla" ajaxRendered="true">
														      	<rich:dataTable id="listaHistorial" value="#{beanHistorialCredito.listaHistorial}" columns="6" 
														      	var="resultadoHistorial" rows="10" rowKeyVar="valor">
														      		<f:facet name="header">
														      			<rich:columnGroup>
														      				<rich:column width="10%"><h:outputText value="Ramo" /></rich:column>
														      				<rich:column width="10%"><h:outputText value="Poliza" /></rich:column>
		     												      			<rich:column width="10%"><h:outputText value="Certificado" /></rich:column>
														      				<rich:column width="10%"><h:outputText value="Cr�dito" /></rich:column>
														      				<rich:column width="40%"><h:outputText value="Nombre" /></rich:column>
														      				<rich:column width="40%" colspan="2"><h:outputText value="Detalle" /></rich:column>
														      			</rich:columnGroup>
														      		</f:facet>
																	<rich:column width="10%" style="text-align:center"> 
														      			<h:outputText value="#{resultadoHistorial.ramo}" />
																	</rich:column> 
																	<rich:column width="10%" style="text-align:center">
														      			<h:outputText value="#{resultadoHistorial.poliza}" />
																	</rich:column>
														      		<rich:column width="10%" style="text-align:center">
														      			<h:outputText value="#{resultadoHistorial.certificado}" />
																	</rich:column>
		 															<rich:column width="20%" style="text-align:center">
														      			<h:outputText value="#{resultadoHistorial.credito}" />
		 															</rich:column> 
														      		<rich:column width="40%" style="text-align:center">
														      			<h:outputText value="#{resultadoHistorial.nombre_aseg}" />
																	</rich:column>
				                                                    <rich:column title="Detalle" width="5%" styleClass="texto_centro">
		                    											<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('detalleHistorial')}.show()">
		                        											<h:graphicImage value="../../images/certificados.png" style="border:0" />   
		                        											<f:setPropertyActionListener value="#{resultadoHistorial}" target="#{beanHistorialCredito.objActual}" />
                        													<f:setPropertyActionListener value="#{valor}" target="#{beanHistorialCredito.filaActual}" />                     												
		                    											</a4j:commandLink>
													                </rich:column>	       
														      		<f:facet name="footer">
														      			<rich:datascroller align="center" for="listaHistorial" maxPages="10" page="#{beanHistorialCredito.numPagina}"/>
														      		</f:facet>
														      	</rich:dataTable>
														      </a4j:outputPanel>
														    </td>
													     </tr>
													</table>
												<td class="frameCR"></td>
										    </tr>
										    <tr>
												<td class="frameBL"></td>
												<td class="frameBC"></td>
												<td class="frameBR"></td>
											</tr>
									    </tbody>
								    </table>
								</div>
					     	</h:form>
						</td>
					</tr>
				</tbody>
	   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>

<rich:modalPanel id="detalleHistorial" height="300" width="800" onmove="true" autosized="true" visualOptions="">
	<f:facet name="header">
        <h:panelGroup>
            <h:outputText value="Detalle del Historial"></h:outputText>
        </h:panelGroup>
    </f:facet>
	<f:facet name="controls">
	        <h:panelGroup>
	         	<h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	         	<rich:componentControl for="detalleHistorial" attachTo="hidelink" operation="hide" event="onclick"/>	             	
	         </h:panelGroup>
	</f:facet>
		<h:form id="detalleHistorialCredito">
	    	<rich:messages style="color:red;"></rich:messages>
			<h:panelGrid columns="1">
	        	 <a4j:outputPanel ajaxRendered="true">
						<%@include file="../consultas/modalHistorialDetalle.jsp" %>
	        	 </a4j:outputPanel>
	        </h:panelGrid>
	  	</h:form>
</rich:modalPanel>

<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>
