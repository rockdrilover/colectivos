<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<title>Alta Cobertura</title>
</head>
<body>
	<f:view>
			<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
			<div align="center">
			<%@include file="../header.jsp" %>
			</div>
			<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>	
						<td align="center">
							<table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<div align="center">
												<table bgcolor="#555242" cellpadding="0" cellspacing="0" class="encabezado">
													<tbody>
														<tr>
															<td bgcolor="#8F8B70" width="10%" align="center">Secci�n</td>
															<td width="4%" align="center" bgcolor="#d90909">|</td>
															<td align="left" width="64%">Alta Cobertura</td>
															<td align="right" width="14%" >Fecha</td>
															<td align="left" width="10%">
																<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="10">
															</td>
														</tr>
													</tbody>
												</table>
										    </div>
											<br>
											<h:form id="frmCombos">
											<div align="center">
												<table class="tabla" cellpadding="0" cellspacing="0" width="95%">
													<tbody>
														<tr>
															<td></td>
															
															<td align="right"><h:outputText value="Ramo:" /></td>
															<td align="left">
																  <h:selectOneMenu id="ramo" value="#{beanAltaCobertura.inRamo}" binding="#{beanListaRamo.inRamo}" required="true" style="width : 242px; height : 23px;">
																		<f:selectItem itemValue="0" itemLabel="Ramo"/>
																		<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																		<a4j:support event="onchange"  ajaxSingle="true" reRender="ramoContable"/>
																	</h:selectOneMenu>
																	<h:message for="ramo" styleClass="errorMessage"/>	
															</td>  									
													     
															</tr>
																																						
														
														<tr>
															<td></td>
															<td align="right"><h:outputText value="Ramo Contable:" /></td>
															<td align="left">
                                                                   <h:selectOneMenu id="ramoContable" value="#{beanAltaCobertura.inRamoContable}" binding="#{beanListaRamoContable.inRamoContable}" required="true">
																		<f:selectItem itemValue="0" itemLabel="Ramo_Contable"/>
																		<f:selectItems value="#{beanListaRamoContable.beansRamosContables}"/>
																		<a4j:support event="onchange"  ajaxSingle="true" reRender="descripCober"/>																	
																        </h:selectOneMenu>
																	<h:message for="ramoContable" styleClass="errorMessage"/>
															</td>  
	
	
														<tr>
																														
															<td></td>
															<td align="right"><h:outputText value="Descripci�n Cobertura:" /></td>
															<td align="left">
                                                                        <h:inputText id="descripCober"  value="#{beanAltaCobertura.inDescCobertura}" required="true" style=" width : 240px;" >  
                                                                        <f:validateLength  maximum="100"/>
                                                                        </h:inputText>		
          											    		<h:message for="descripCober" errorStyle="errorMessage"/>										
													        </td>																								
															
														</tr>
														
													
													</tbody>
												</table>
											</div>											
																						
											<div align="center">
												<table class="tabla" cellpadding="0" cellspacing="0" align="center">
													<tbody>
													<tr>
														<td align="right" width="20%">
															<h:commandButton value="Alta" action="#{beanAltaCobertura.iniciaAlta}" style=" width : 77px;"/>
														</td>
													</tr>
													</tbody>
												</table>
											</div>
											</h:form>
											<h:messages styleClass="errorMessage" globalOnly="true"/>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			</div>
		<%@ include file="../footer.jsp" %>
	</f:view>
<br>
</body>
</html>