<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<script type="text/javascript" src="../../js/boxover.js"></script>
<title>Datos del Contratante</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla" >
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">||||</td>
							<td align="left" width="64%">Datos del Contratante</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmConsultarCertif">
				      <div align="center">
					    <rich:spacer height="15px"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC" width="700px">
							    <a4j:region id="consultaDatos">
								     <table cellpadding="2" cellspacing="0">
								     <tbody>
										<tr>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="12%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
											<td width="11%"></td>
										</tr>
	
										<tr height="10px">
											<td colspan="9"></td>
										</tr>

										<tr height="10px">
											<td colspan="9"></td>
										</tr>
							     		<tr>
								     		<td align="center" colspan="1" rowspan="3">
								     			<h:outputText value="Raz�n Social:" />
				 				     			<h:inputText id="razonSocial"  value="#{beanAlta.inRazonSocial}" required="true" />
								     			<h:message for="razonSocial" styleClass="errorMessage" />
									        <td align="center" colspan="2" rowspan="3">
									        		<h:outputText value="RFC:" />
								      			    <h:inputText id="rfc"  value="#{beanAlta.inRfc}" required="true" maxlength="10"/>
								     			     <h:message for="rfc" styleClass="errorMessage" />
									        </td>
									       </tr> 
									      <tr>
											<td colspan="9" ></td>
										  </tr>
										  <tr>
											<td colspan="9"></td>
										  </tr>
										  <tr>
											<td colspan="9"></td>
										  </tr>  
									        
									       <tr> 
									        <td align="center" colspan="1" rowspan="3">
									        		<h:outputText value="Fecha de Constituci�n:" />
												 	 <rich:calendar id="fecha_con"  value="#{beanAlta.inFechaCons}" inputSize="10" 
							    									popup="true" cellHeight="20px"  cellWidth="5px" datePattern="dd/MM/yyyy" 
							    									styleClass="calendario" required="false" />  	
	     											 <h:message for="fecha_con" styleClass="errorMessage"/>
	     									
	     									 <td align="center" colspan="2" rowspan="3">
								     			<h:outputText value="Calle/Numero :" />
				 				     			<h:inputText id="calle"  value="#{beanAlta.inCalleNum}" required="true" />
								     			<h:message for="calle" styleClass="errorMessage" />		 
									        </td>
							     		  </tr>
							     		<tr>
											<td colspan="9" ></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>	
										<tr>
								     		
									        <td align="center" colspan="1" rowspan="3">
									        		<h:outputText value="Colonia:" />
								      			    <h:inputText id="colonia"  value="#{beanAlta.inColonia}" required="true" />
								     			    <h:message for="colonia" styleClass="errorMessage" />
								     			    <a4j:support event="onchange"  action="#{beanListaEstado.consultarEstados}" ajaxSingle="true" reRender="estado" />
									        </td>
									        
									        <tr>
									    		<td align="right" width="24%" height="30"><h:outputText value="Estado:" /></td>
									    		<td align="left" width="40%" height="30">
													<h:selectOneMenu id="estado" value="#{beanAlta.inPoblacion}" title="header=[Estado] body=[Seleccione un estado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
														<f:selectItem itemValue="0" itemLabel="Selecione Estado" />
														<f:selectItem itemValue="1" itemLabel="AGUASCALIENTES" />
														<f:selectItem itemValue="2" itemLabel="BAJA CALIFORNIA NORTE" />
														<f:selectItem itemValue="3" itemLabel="BAJA CALIFORNIA SUR" />
														<f:selectItem itemValue="4" itemLabel="CAMPECHE" />
														<f:selectItem itemValue="5" itemLabel="COAHUILA" />
														<f:selectItem itemValue="6" itemLabel="COLIMA" />
														<f:selectItem itemValue="7" itemLabel="CHIAPAS" />
														<f:selectItem itemValue="8" itemLabel="CHIHUAHUA" />
														<f:selectItem itemValue="9" itemLabel="DISTRITO FEDERAL" />
														<f:selectItem itemValue="10" itemLabel="DURANGO" />
														<f:selectItems value="#{beanListaEstado.listaEstados}"/>
														<%-- <a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" /> --%>
													</h:selectOneMenu>
													<h:message for="inRamo" styleClass="errorMessage" />
												</td>
							     		</tr>
										<tr>
											<td colspan="9" ></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>		
										<tr>
								     		<td align="center" colspan="1" rowspan="3">
									        		<h:outputText value="CP:" />
								      			    <h:inputText id="cp"  value="#{beanAlta.inCp}" required="true" />
								     			    <h:message for="cp" styleClass="errorMessage" />
									        </td>
							     		</tr>
										<tr>
											<td colspan="9" ></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>
										<tr>
											<td colspan="9"></td>
										</tr>
										<tr>
											<td colspan="9" height="5"></td>
										</tr>
										
										<tr>
											<td align="center" colspan="4"><h:outputText value="Cedula Generada" styleClass="importante2" style="color:#000000;font-size: 20px;"/>&nbsp;&nbsp;</td>
										</tr>
											<tr>
												<td align="center" colspan="4" >
			                                        	<h:outputText id="cedula"  value="#{beanAlta.cedulaRif}" styleClass="importante2"/>									
											        </td> 
												</tr>	
												<tr height="10"></tr>	
										<tr>
											<td align="center" width="100%">
										       <h:commandButton value="Guardar" action="#{beanAlta.iniciaAltaContratante}" style=" width : 162px;"/>
									        </td>
										</tr>							
							     	</tbody>     		         		    		
								    </table>
								     <rich:spacer height="15px"></rich:spacer>
								   <%--  <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
										<tr>
											<td align="center">
												<br/>
												<h:outputText id="mensaje" value="#{beanListaCertificado.mensaje}" styleClass="respuesta"/>
												<br/>
												<br/>
											</td>
										</tr>
									</tbody>
									</table>
																
								
								----aqui termina --%>
								</a4j:region>								    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
					    </table>
					    <rich:spacer height="10px"></rich:spacer>
						<table  class="botonera">
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						   <%--  <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    	<div align="center">
							    	    <h:form>
										<rich:dataTable id="certificados" value="#{beanListaCertificado.listaCertificadosFec}" var="registro" columns="9" columnsWidth="15px, 10px, 15px, 25px,15px, 15px, 15px, 15px, 15px" width="135px" rows="50">
											<f:facet name="header"><h:outputText value="Reporte de Certificados" /></f:facet>
											<rich:column>
												<f:facet name="header"><h:outputText value="Poliza" /> </f:facet>
												<h:outputText value="#{registro.id.coceCapoNuPoliza}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Estatus Colectivos" /> </f:facet>
											<h:outputText value="#{registro.estatus.alesDescripcion}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Estatus" /> </f:facet>				
											<h:outputText value="#{registro.coceCampov1}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Causa" /> </f:facet>
											<h:outputText value="#{registro.coceCampov4}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Certificado" /></f:facet>
												<h:outputText value="#{registro.id.coceNuCertificado}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Suma Asegurada" /> </f:facet>
												<h:outputText value="#{registro.coceMtSumaAsegurada}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Prima Anual" /> </f:facet>
												<h:outputText value="#{registro.coceMtPrimaAnual}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Prima Real" /></f:facet>
												<h:outputText value="#{registro.coceMtPrimaReal}" />
											</rich:column>
											<rich:column>
												<f:facet name="header"><h:outputText value="Prima Pura" /> </f:facet>
												<h:outputText value="#{registro.coceMtPrimaPura}" />
											</rich:column>
								      		<f:facet name="footer">
								      			<rich:datascroller align="right" for="certificados" maxPages="10" page="#{beanListaCertificado.numPagina}"/>
								      		</f:facet>
										</rich:dataTable>
										<br/>
										</h:form>
										<br/>
									</div>
							    </td>
								<td class="frameCR"></td>
					    	</tr> 								    	
					    	
					    	---- aqui termina --%>
					    	<tr>
								<td class="frameBL"></td>
								<td class="frameBC"></td>
								<td class="frameBR"></td>
							</tr>
						</table>
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="10px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>