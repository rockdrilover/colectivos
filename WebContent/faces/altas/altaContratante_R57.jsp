<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
		<script src="../../static/js/colectivos.js"></script>
		
		<title> Alta Poliza 57-58 </title>
	</head>

	<body>
	<f:view>  <!-- Desde aqui comienzo --> 
		<fmt:setBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages"/>
		<h:form id="frmCContratante">
		<div align="center"><%@include file="../header.jsp" %></div> 
		<div align="center">
			<table class="tablaPrincipal">			
				<tr>
					<td align="center">
						<table class="tablaSecundaria">			
							<tr>
								<td>
									<div align="center">
									<table class="encabezadoTabla" >
										<tr>
											<td width="10%" align="center">Secci�n</td>
											<td width="4%" align="center" bgcolor="#d90909"> I </td>
											<td align="left" width="64%"> Alta Poliza 57-58 </td>
											<td align="right" width="12%" >Fecha</td>
											<td align="left" width="10%">
												<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
											</td>
										</tr>
									</table>
				    				</div>	
				    
				    				<div align="center">
				    				<a4j:region id="capturaDatos">
				    				<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
					    				<tr>
					    					<td align="center">
												<a4j:status for="capturaDatos" stopText=" "> 
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>												
											</td>
					    					<td width="13%" align="right">						                    										                    			
												<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanAltaContratanteR57.alta}" 
													onclick="this.disabled=false" oncomplete="#{beanAltaContratanteR57.viewButton}" reRender="mensaje" value="Guardar"/>	
											</td>
					    				</tr>
					    				
										<tr><td colspan="2">
												<a4j:outputPanel id="mensaje" ajaxRendered="true">
													<h:outputText  value="#{beanAltaContratanteR57.strRespuesta}" styleClass="error"/>
												</a4j:outputPanel>
											</td></tr>	
				    				</table>
				    						    				
				    				<!-- Inicio ACCIONES -->
				    				<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
										<tr>
											<td>
												<fieldset style="border:1px solid white">
													<legend>
														<span class="titulos_secciones"><h:outputText value="Datos de Poliza"/></span>
													</legend>
													
													<table width="100%" border="0" cellpadding="0" cellspacing="4" align="center">	
														<tr><td colspan="6">&nbsp;</td></tr>
														<tr class="texto_normal">
															<td align="left" width="12%">&nbsp;&nbsp;<h:outputText value="Ramo:"/></td>
															<td colspan="5"	align="left">
								                    			<h:selectOneMenu value="#{beanAltaContratanteR57.inRamo}"  style="width: 250px" id="inRamo" title="Seleccione ramo" required="false" requiredMessage="* RAMO - Campo Requerido">  
														        	<f:selectItem itemLabel=" --- Seleccione un Ramo ---" itemValue="0" />  
														            <f:selectItems value="#{beanListaRamo.listaCmbRamos5758}"/>											            
														           	<a4j:support event="onchange"  action="#{beanAltaContratanteR57.cargaProductos}" ajaxSingle="true" reRender="inProducto,mensaje" />
													        	</h:selectOneMenu>
			                   								</td>
			                   							</tr>
			                   							<tr class="texto_normal">
			                   						   	    <td align="left" width="12%">&nbsp;&nbsp;<h:outputText value="Producto: "/></td>
														    <td colspan="2" align="left"> 
														    	<a4j:outputPanel id="inProducto" ajaxRendered="true">
															    	<h:selectOneMenu  value="#{beanAltaContratanteR57.nProducto}" style="width: 250px" title="Seleccione producto" required="false" requiredMessage="* PRODUCTO - Campo Requerido" >
																 		<f:selectItem itemValue="0" itemLabel="--- Selecione un producto ---"/>
																    	<f:selectItems value="#{beanAltaContratanteR57.listaProductos}" />
																		<a4j:support event="onchange"  action="#{beanAltaContratanteR57.consultaPlanes}" ajaxSingle="true" reRender="idPlan,mensaje" />  
																	</h:selectOneMenu>
														    	</a4j:outputPanel>
														    </td>
														    <td align="left" width="12%"><h:outputText value="Plan: "/></td>	
															<td colspan="2" align="left"> 
																<a4j:outputPanel id="idPlan" ajaxRendered="true">
																	<h:selectOneMenu value="#{beanAltaContratanteR57.nPlan}" style="width: 250px" title="Seleccione un plan." required="false" requiredMessage="* PLAN - Campo Requerido" >
																		<f:selectItem itemValue="0" itemLabel=" --- Seleccione un plan ---"/>
																		<f:selectItems value="#{beanAltaContratanteR57.listaComboPlanes}"/>
																		<a4j:support event="onchange"  action="#{beanAltaContratanteR57.getSumaAsegurada}" ajaxSingle="true" reRender="datosAdiciSuma,datosAdiciPma,mensaje" />
																	</h:selectOneMenu>
																</a4j:outputPanel>
														    </td>
														</tr>
														<tr class="texto_normal">
			                   						   	    <td align="left" width="12%">&nbsp;&nbsp;<h:outputText value="Cond. Cobro: "/></td>
														    <td colspan="2" align="left"> 
																<h:selectOneMenu  value="#{beanAltaContratanteR57.contratante.strTpCuenta}" style="width: 250px" id="inCobro" title="Seleccione conducto de cobro" required="false" requiredMessage="* CONDUCTO COBRO - Campo Requerido" >
															 		<f:selectItem itemValue="0" itemLabel="--- Selecione ---"/>
															    	<f:selectItem itemLabel="CHEQUES" itemValue="C" />  
														            <f:selectItem itemLabel="TARJETA" itemValue="T" />
														            <f:selectItem itemLabel="AMERICAN EXPRESS" itemValue="A" />
																</h:selectOneMenu>
														    </td>
														    <td align="left" width="12%"><h:outputText value="Cuenta:"/>&nbsp;&nbsp;</td>
								            				<td colspan="2" align="left" width="21%">  
								            					<h:inputText  value="#{beanAltaContratanteR57.contratante.strCuenta}" style="width: 160px" maxlength="20" required="false" requiredMessage="* Cuenta - Campo Requerido" title="Introduzca Cuenta"/>
								            				</td>
														</tr>
														<tr class="texto_normal">
			                   						   	    <td align="left" width="12%">&nbsp;&nbsp;<h:outputText value="Funcionario: "/></td>
														    <td colspan="5" align="left"> 
																<h:inputText value="#{beanAltaContratanteR57.strCdAnalista}" style="width: 70px" maxlength="10" required="false" requiredMessage="* Funcionario - Campo Requerido" title="Introduzca Clave Funcionario">
																	<a4j:support event="onchange"  action="#{beanAltaContratanteR57.getAnalista}" ajaxSingle="true" reRender="descAnalista,mensaje" />
																</h:inputText>
																&nbsp;
																<a4j:outputPanel id="descAnalista" ajaxRendered="true">
																	<h:inputText value="#{beanAltaContratanteR57.strAnalista}" style="width: 400px" disabled="true"/>	
																</a4j:outputPanel>
								            				</td>
														</tr>
														<tr class="texto_normal">
			                   						   	    <td align="left" width="12%">&nbsp;&nbsp;<h:outputText value="Sucursal: "/></td>
														    <td colspan="5" align="left"> 
														    	<h:inputText value="#{beanAltaContratanteR57.nCdSucursal}" style="width: 70px" maxlength="7" required="false" requiredMessage="* Sucursal - Campo Requerido" title="Introduzca Clave Sucursal">
																	<a4j:support event="onchange"  action="#{beanAltaContratanteR57.getSucursal}" ajaxSingle="true" reRender="descSucursal,mensaje"/>
														    	</h:inputText>
																&nbsp;
																<a4j:outputPanel id="descSucursal" ajaxRendered="true">
								            						<h:inputText value="#{beanAltaContratanteR57.strSucursal}" style="width: 400px" disabled="true"/>
								            					 </a4j:outputPanel>
								            				</td>
														</tr>
														<tr class="espacio_5"><td>&nbsp;</td></tr>				
														<tr class="texto_normal">
															
															<td colspan="6" align="center">
																<table border="0">
																	<tr>
																		<td align="right" width="12%"><h:outputText value="Suma Asegurada: "/></td>
																		<td align="left" width="12%">&nbsp;&nbsp;
																			<a4j:outputPanel id="datosAdiciSuma" ajaxRendered="true">
																				<h:outputText value="#{beanAltaContratanteR57.strSumaAseg}"/>
																			</a4j:outputPanel>
																		</td>
																		
																		<td align="right" width="12%"><h:outputText value="Prima: "/></td>
																		<td align="left" width="12%">&nbsp;&nbsp;
																			<a4j:outputPanel id="datosAdiciPma" ajaxRendered="true">
																				<h:outputText value="#{beanAltaContratanteR57.strPrima}"/>
																			</a4j:outputPanel>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>		
			                   					</fieldset>
											</td>		
										</tr>
									
										<tr class="espacio_5"><td>&nbsp;</td></tr>
									
										<tr>
											<td>
												<fieldset style="border:1px solid white">
													<legend>
														<span class="titulos_secciones">  Datos de Contratante </span>
													</legend>
												
													<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center">
		                   								<tr><td colspan="6">&nbsp;</td></tr>
		                   								<tr class="texto_normal">	
		                   									<td align="right" width="13%"><h:outputText value="Cve. Cliente:"/>&nbsp;&nbsp;</td>
								            				<td align="left" width="21%"> 
								            					<h:inputText value="#{beanAltaContratanteR57.strCveCliente}" style="width: 150px" maxlength="60" required="false" disabled="false" />
								            				</td>
								            				<td align="right" width="12%"><h:outputText value="Persona:"/>&nbsp;&nbsp;</td>
															<td align="left" width="21%"> 
																<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.cacnTpCliente}" style="width: 152px" required="false" requiredMessage="* Tipo de persona - Campo Requerido" title="Seleccione Tipo de persona">  
														            <f:selectItem itemLabel=" --- Seleccione ---" itemValue="0"/>  
														            <f:selectItem itemLabel="FISICA " itemValue="F" />  
														            <f:selectItem itemLabel="MORAL" itemValue="M" />
														            <f:selectItem itemLabel="FISICA CON ACTIVIDAD EMPRESARIAL" itemValue="N" />    
													        	</h:selectOneMenu>
											        		</td>						
											        		<td colspan="2"></td>       	
											        	</tr>
											        	
											        	<tr class="texto_normal">
													    	<td align="right" width="13%"><h:outputText value="Nombre:"/>&nbsp;&nbsp;</td>
												         	<td align="left" width="21%">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnNombre}"  style="width: 150px" maxlength="100" required="false" requiredMessage="* Nombre - Campo Requerido"  title="Introduzca Nombre"/>
												         	</td>
													    	<td align="right" width="12%"><h:outputText value="A. Paterno:"/>&nbsp;&nbsp;</td>
												         	<td align="left" width="21%">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnApellidoPat}"  style="width: 150px" maxlength="60" required="false" title="Introduzca Apellido Paterno"/>
												         	</td>
												         	<td align="right" width="12%"><h:outputText value="A. Materno:"/>&nbsp;&nbsp;</td>
												         	<td align="left" width="21%">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnApellidoMat}"  style="width: 160px" maxlength="60" required="false" title="Introduzca Apellido Paterno" /> 
												         	</td>
												    	</tr>
												    
												    	<tr class="texto_normal">
												         	<td align="right"><h:outputText value="Fecha Nac.:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<rich:calendar id="fecha_Nac" value="#{beanAltaContratanteR57.contratante.cacnFeNacimiento}" 
									                        		cellWidth="24px" cellHeight="22px" style="width:145px"  datePattern="dd/MM/yyyy"
									                        		inputSize="10" popup="true"/>
									                     	</td>
												         	<td align="right"><h:outputText value="Edo. Civil:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.cacnCdEdoCivil}" style="width: 152px" required="false" requiredMessage="* Edo. Civil - Campo Requerido" title="Selecciona Estado Civil">  
													            	<f:selectItem itemLabel="--- Seleccione ---" itemValue="0" />  
													            	<f:selectItem itemLabel="SOLTERO" itemValue="S" />  
													            	<f:selectItem itemLabel="CASADO" itemValue="C" />  
													            	<f:selectItem itemLabel="DIVORCIADO" itemValue="D" />
													            	<f:selectItem itemLabel="VIUDO" itemValue="V" />  
													            	<f:selectItem itemLabel="OTRO" itemValue="O" />  
													            	  
												        		</h:selectOneMenu> 	
												         	</td>
												         	<td align="right"><h:outputText value="Sexo:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
													        	<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.cacnCdSexo}" style="width: 162px" required="false" requiredMessage="* Sexo - Campo Requerido" title="Seleccione el sexo">  
													            	<f:selectItem itemLabel="--- Selecccione ---" itemValue="0" />  
													            	<f:selectItem itemLabel="FEMENINO" itemValue="F" />  
													            	<f:selectItem itemLabel="MASCULINO" itemValue="M" />
													            	<f:selectItem itemLabel="OTROS" itemValue="I" />  
												        	  	</h:selectOneMenu>
												         	</td>
														</tr>	

								       			      	<tr class="texto_normal">
												         	<td align="right"><h:outputText value="RFC:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnRfc}" style="width:150px" maxlength="15" required="false" requiredMessage="* RFC - Campo Requerido" title="Ingrese el RFC"/>
									                     	</td>
												         	<td align="right"><h:outputText value="CURP:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnNmPersonaNatural}" style="width: 150px" maxlength="20" required="false" requiredMessage="* CURP  - Campo Requerido" title="Ingrese el CURP"/> 	
												         	</td>
												         	<td colspan="2"></td>
								       			      	</tr>

													  	<tr class="texto_normal">										
												         	<td align="right"><h:outputText value="Direccion:"/>&nbsp;&nbsp;</td>
												         	<td align="left" colspan="3">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnDiHabitacion1}"  style="width: 393px" maxlength="100" required="false"  requiredMessage="* DIRECCION - Campo Requerido" title="Ingrese la direccion con formato (Calle Numero Exterior Numero interior)"/>
												         	</td>
												         	<td align="right"><h:outputText value="C.P.:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnZnPostalHabitacion}" style="width: 70px" maxlength="6" required="false" requiredMessage="* CODIGO POSTAL- Campo Requerido" title="Ingrese Codigo Postal">
										                         	<a4j:support event="onchange" action="#{beanAltaContratanteR57.consultaCodigoPostal}"  ajaxSingle="true" reRender="cmbColonia,cmbPoblacion,txtEstado">
										                         		<f:setPropertyActionListener value="0" target="#{beanAltaContratanteR57.nTipoConsCP}" />
										                         	</a4j:support>
									                         	</h:inputText>
									                     	</td>
														</tr>
														
													  	<tr class="texto_normal">
												         	<td align="right"><h:outputText value="Colonia:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<a4j:outputPanel id="cmbColonia" ajaxRendered="true">
													         		<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.cacnDiCobro2}" style="width: 152px" required="false" requiredMessage="* COLONIA - Campo Requerido" title="Seleccione Colonia">
																		<f:selectItem itemValue="SIN VALOR" itemLabel=" --- Seleccione --- "/>
																		<f:selectItems value="#{beanAltaContratanteR57.listaCmbColonia}"/>
																		<a4j:support event="onchange" />
																    </h:selectOneMenu> 	
																</a4j:outputPanel>
												         	</td>
															<td align="right"><h:outputText value="Poblacion:"/>&nbsp;&nbsp;</td>
									                    	<td align="left">
									                    		<a4j:outputPanel id="cmbPoblacion" ajaxRendered="true">
										                    		<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.cacnDatosReg1}" style="width: 152px" required="false" requiredMessage="* DEL/POB/MUN - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
																		<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
																		<f:selectItems value="#{beanAltaContratanteR57.listaCmbCiudades}"/>
																		<a4j:support event="onchange" />
																    </h:selectOneMenu>
																</a4j:outputPanel>
													    	</td>
									                   		<td align="right"><h:outputText value="Estado:"/>&nbsp;&nbsp;</td>
									                    	<td align="left">
									                    		<a4j:outputPanel id="txtEstado" ajaxRendered="true"> 
									                    			<h:inputText value="#{beanAltaContratanteR57.strDescEstadoCont}" style="width: 160px" disabled="true" required="false" requiredMessage="* COLONIA - Campo Requerido" />
									                    		</a4j:outputPanel>
															</td>
										               	</tr>

														<tr class="texto_normal">
												         	<td align="right"><h:outputText value="Tel. Domicilio:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnNuTelefonoCobro}" style="width: 150px" maxlength="15" required="false" requiredMessage="* TELEFONO DOMICILIO - Campo Requerido" title="Ingrese telefono"/>
									                     	</td>												         	
												         	<td align="right"><h:outputText value="Tel. Oficina:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnNuTelefonoHabitacion}" style="width: 150px" maxlength="15" required="false" requiredMessage="* TELEFONO OFICINA - Campo Requerido" title="Ingrese telefono oficina"/>
												         	</td>
												         	<td align="right"><h:outputText value="Email:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnDiHabitacion2}" style="width: 160px" maxlength="50" required="false" requiredMessage="* EMAIL - Campo Requerido" title="Ingrese correo electronico"/>
									                     	</td>
								       			      	</tr>

														<tr class="texto_normal">
												         	<td align="right"><h:outputText value="Num. Serie:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.strNumeroSerie}" style="width: 150px" maxlength="20" required="false" requiredMessage="* NUMERO DE SERIE - Campo Requerido" title="Ingrese Numero de Serie"/> 	
												         	</td>
												         	<td align="right"><h:outputText value="F. Mercantil:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.strFolioMercantil}" style="width: 150px" maxlength="20" required="false" requiredMessage="* FOLIO MERCANTIL - Campo Requerido" title="Ingrese Folio Mercantil"/>
									                     	</td>
												         	<td align="right"><h:outputText value="G. Mercantil:"/>&nbsp;&nbsp;</td>
												         	<td align="left"> 
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.strGiroMercantil}" style="width: 160px" maxlength="20" required="false" requiredMessage="* GIRO MERCANTIL - Campo Requerido" title="Ingrese Giro Mercantil"/> 	
												         	</td>
								       			      	</tr>
													</table>		
			                   					</fieldset>
											</td>		
										</tr>

										<tr class="espacio_5"><td>&nbsp;</td></tr>
									
										<tr>
											<td>
												<fieldset style="border:1px solid white">
													<legend>
														<span class="titulos_secciones">  Direccion de Envio </span>
													</legend>
												
													<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center">
		                   								<tr class="texto_normal">										
												         	<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Direccion: "/> </td>
												         	<td align="left" colspan="4">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.strDireccionEnvio}" style="width: 600px" maxlength="50" required="false" requiredMessage="* DIRECCION ENVIO - Campo Requerido" title="Ingrese la direccion con formato (Calle Numero Exterior Numero interior)"/>
												         	</td>
														</tr>
														
													  	<tr class="texto_normal">
												         	<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="C.P.:"/></td>
												         	<td align="left" width="33%">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.strCPEnvio}" style="width: 70px" maxlength="6" required="false" requiredMessage="* C.P. ENVIO - Campo Requerido" title="Ingrese Codigo Postal">
																	<a4j:support event="onchange" action="#{beanAltaContratanteR57.consultaCodigoPostal}"  ajaxSingle="true" reRender="cmbColoniaEnv,cmbPoblacionEnv,txtEstadoEnv">
										                         		<f:setPropertyActionListener value="1" target="#{beanAltaContratanteR57.nTipoConsCP}" />
										                         	</a4j:support>
									                         	</h:inputText>
									                     	</td>
												         	<td width="4%">&nbsp;</td>
												         	<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Colonia:"/></td>
												         	<td align="left" width="33%"> 
												         		<a4j:outputPanel id="cmbColoniaEnv" ajaxRendered="true">
													         		<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.strColoniaEnvio}" style="width: 230px" required="false" requiredMessage="* COLONIA ENVIO - Campo Requerido" title="Seleccione Colonia">
																		<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
																		<f:selectItems value="#{beanAltaContratanteR57.listaCmbColoniaEnvio}"/>
																		<a4j:support event="onchange" />
																    </h:selectOneMenu> 	
																</a4j:outputPanel>
												         	</td>
								       			      	</tr>

												      	<tr class="texto_normal">	    
															<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Poblacion/Delegacion :"/></td>
									                    	<td align="left" width="33%">
									                    		<a4j:outputPanel id="cmbPoblacionEnv" ajaxRendered="true">
										                    		<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.strPoblacionEnvio}" style="width: 230px" required="false" requiredMessage="* DEL/POB/MUN - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
																		<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
																		<f:selectItems value="#{beanAltaContratanteR57.listaCmbCiudadesEnvio}"/>
																		<a4j:support event="onchange" />
																    </h:selectOneMenu>
																</a4j:outputPanel>
													    	</td>
															<td width="4%">&nbsp;</td>
									                   		<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Estado: "/></td>
									                    	<td align="left" width="33%"> 
																<a4j:outputPanel id="txtEstadoEnv" ajaxRendered="true"> 
									                    			<h:inputText value="#{beanAltaContratanteR57.strDescEstadoEnvio}" style="width: 228px" disabled="true" required="false" requiredMessage="* ESTADO ENVIO - Campo Requerido" />
									                    		</a4j:outputPanel>
															</td>
										               	</tr>
													</table>		
			                   					</fieldset>
											</td>		
										</tr>

										<tr class="espacio_5"><td>&nbsp;</td></tr>
									
										<tr>
											<td>
												<fieldset style="border:1px solid white">
													<legend>
														<span class="titulos_secciones">  Direccion de Fiscal </span>
													</legend>
													
													<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center">
		                   								<tr class="texto_normal">										
												         	<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Direccion: "/> </td>
												         	<td align="left" colspan="4">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.cacnDiCobro1}" style="width: 600px" maxlength="50" required="false" requiredMessage="* DIRECCION FISCAL - Campo Requerido" title="Ingrese la direccion con formato (Calle Numero Exterior Numero interior)"/>
												         	</td>
														</tr>
														
													  	<tr class="texto_normal">
												         	<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="C.P.:"/></td>
												         	<td align="left" width="33%"> 
																<h:inputText value="#{beanAltaContratanteR57.contratante.cacnZnPostalCobro}" style="width: 70px" maxlength="6" required="false" requiredMessage="* C.P. ENVIO - Campo Requerido" title="Ingrese Codigo Postal">
																	<a4j:support event="onchange" action="#{beanAltaContratanteR57.consultaCodigoPostal}"  ajaxSingle="true" reRender="cmbColoniaFis,cmbPoblacionFis,txtEstadoFis">
										                         		<f:setPropertyActionListener value="2" target="#{beanAltaContratanteR57.nTipoConsCP}" />
										                         	</a4j:support>
									                         	</h:inputText>
									                     	</td>
												         	<td width="4%">&nbsp;</td>
												         	<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Colonia:"/></td>
												         	<td align="left" width="33%">
												         		<a4j:outputPanel id="cmbColoniaFis" ajaxRendered="true">
													         		<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.cacnCazpColoniaCob}" style="width: 230px" required="false" requiredMessage="* COLONIA FISCAL - Campo Requerido" title="Seleccione Colonia">
																		<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
																		<f:selectItems value="#{beanAltaContratanteR57.listaCmbColoniaFiscal}"/>
																		<a4j:support event="onchange" />
																    </h:selectOneMenu> 	
																</a4j:outputPanel>
												         	</td>
								       			      	</tr>

												      	<tr class="texto_normal">	    
															<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Poblacion/Delegacion :"/></td>
									                    	<td align="left" width="33%">
									                    		<a4j:outputPanel id="cmbPoblacionFis" ajaxRendered="true">
										                    		<h:selectOneMenu value="#{beanAltaContratanteR57.contratante.cacnCazpPoblacCob}" style="width: 230px" required="false" requiredMessage="* DEL/POB/MUN FISCAL - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
																		<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
																		<f:selectItems value="#{beanAltaContratanteR57.listaCmbCiudadesFiscal}"/>
																		<a4j:support event="onchange" />
																    </h:selectOneMenu> 	
																</a4j:outputPanel>
													    	</td>
															<td width="4%">&nbsp;</td>
									                   		<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Estado: "/></td>
									                    	<td align="left" width="33%">
									                    		<a4j:outputPanel id="txtEstadoFis" ajaxRendered="true"> 
									                    			<h:inputText value="#{beanAltaContratanteR57.strDescEstadoFiscal}" style="width: 228px" disabled="true" required="false" requiredMessage="* ESTADO FISCAL - Campo Requerido" />
									                    		</a4j:outputPanel>
															</td>
										               	</tr>
													</table>		
			                   					</fieldset>
											</td>		
										</tr>
										
										<tr class="espacio_5"><td>&nbsp;</td></tr>
									
										<tr>
											<td>
												<fieldset style="border:1px solid white">
													<legend>
														<span class="titulos_secciones">  Representante Legal </span>
													</legend>
												
													<table width="100%" border="1" cellpadding="0" cellspacing="0" align="center">
		                   								<tr class="texto_normal">										
												         	<td align="left" width="20%">&nbsp;&nbsp;<h:outputText value="Nombre: "/> </td>
												         	<td align="left" colspan="4">
												         		<h:inputText value="#{beanAltaContratanteR57.contratante.strNombreRepLegal}" style="width: 600px" maxlength="50" required="false" requiredMessage="* NOMBRE - Campo Requerido" title="Ingrese Nombre Completo"/>
												         	</td>
														</tr>
		                   								
		                   								<tr class="texto_normal">	
		                   									<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Nacionalidad:"/></td>
								            				<td align="left" width="33%"> 
								            					<h:inputText value="#{beanAltaContratanteR57.contratante.strNacionalidad}" style="width: 228px" maxlength="50" required="false" requiredMessage="* NACIONALIDAD - Campo Requerido" title="Ingrese Nacionalidad"/>
								            				</td>
								            				<td width="4%">&nbsp;</td>
								            				<td align="left" width="15%">&nbsp;&nbsp;<h:outputText value="Puesto:"/></td>
															<td align="left" width="33%"> 
																<h:inputText value="#{beanAltaContratanteR57.contratante.strPuestoRepLegal}" style="width: 228px" maxlength="50" required="false" requiredMessage="* PUESTO - Campo Requerido" title="Ingrese Puesto"/>
											        		</td>											        	
											        	</tr>
													</table>		
			                   					</fieldset>
											</td>		
										</tr>
				    				</table>
			    					</a4j:region>		 										
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>   	
		</div>
		</h:form>

		<rich:spacer height="10px"></rich:spacer>
		<div><%@include file="../footer.jsp" %></div>
<%--    		<a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />  		 --%>
	</f:view>    	
	</body>
</html>
