<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<title>Alta Producto - Plan</title>
</head>
<body>
	<f:view>
			<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
			<div align="center">
			<table width="800px" bgcolor="#fefefe">
				<tbody>
					<tr>
						<td align="left"><img src="../images/logo.gif" width="245" height="40"></td>
						<td align="right"><img src="../images/cargas.gif" width="205" height="50"></td>
					</tr>
				</tbody>
			</table>
			</div>
			<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>	
						<td align="center">
							<table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<div align="center">
												<table bgcolor="#555242" cellpadding="0" cellspacing="0" class="encabezado">
													<tbody>
														<tr>
															<td bgcolor="#8F8B70" width="10%" align="center">Secci�n</td>
															<td width="4%" align="center" bgcolor="#d90909">|</td>
															<td align="left" width="64%">Alta Producto - Plan</td>
															<td align="right" width="14%" >Fecha</td>
															<td align="left" width="10%">
																<input name="fecha" readonly="readonly" size="10">
															</td>
														</tr>
													</tbody>
												</table>
										    </div>
											<br>
											<h:form id="frmAltaProductoPlan">
											<div align="center">
												<table bgcolor="#8F8B70" cellpadding="0" cellspacing="0" width="95%" border="2">
													<tbody>
														<tr>
															<td></td>
															
															<td align="right"><h:outputText value="Ramo:" /></td>
															<td align="left">
																   <h:selectOneMenu id="ramo" value="#{beanAltaProd.inRamo}" binding="#{beanListaRamo.inRamo}" required="true" style="width : 242px; height : 23px;">
																		<f:selectItem itemValue="0" itemLabel="Ramo"/>
																		<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																		<a4j:support event="onchange"  ajaxSingle="true" reRender="producto"/>																	
																        </h:selectOneMenu>
																	<h:message for="ramo" styleClass="errorMessage"/>	
															</td>  									
													     
															</tr>
	
														<tr>
																														
															<td></td>
															<td align="right"><h:outputText value="Producto:" /></td>
															<td align="left">
                                                                        <h:inputText id="producto"  value="#{beanAltaProd.inDescProducto}" required="true" style=" width : 240px;" >  
                                                                        <f:validateLength  maximum="100"/>
                                                                        </h:inputText>		
          											    		<h:message for="producto" errorStyle="errorMessage"/>										
													        </td>																								
															
														</tr>
														

														<tr>
															<td></td>
															<td align="right"><h:outputText value="Plan:" /></td>
															<td align="left">
                                                                        <h:inputText id="plan"  value="#{beanAltaProd.inDescPlan}" required="true" style=" width : 240px;">  
                                                                        <f:validateLength  maximum="100"/>
          											    		</h:inputText>		
          											    		<h:message for="plan" errorStyle="errorMessage"/>										
													        </td>		
																													
															<td></td>
														
														</tr>														
													</tbody>
												</table>
											</div>											
																						
											<div align="center">
												<table bgcolor="#8F8B70" cellpadding="0" cellspacing="0" width="95%" border="2" align="center">
													<tbody>
													 <tr>
														<td>
																<td align="right" width="20%">
																<h:commandButton styleClass="boton" value="Alta" action="#{beanAltaProd.iniciaAlta}" />
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											</h:form>
											<h:messages styleClass="errorMessage" globalOnly="true" />
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			</div>
		<%@ include file="../footer.jsp" %>
	</f:view>
<br>
</body>
</html>