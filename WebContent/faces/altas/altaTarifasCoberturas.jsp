<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<title>Alta Tarifas-Coberturas</title>
</head>
<body>
	<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
			<div align="center">
			<table class="tablaPrincipal">
				<tbody>
					<tr>	
						<td align="center">
							<table class="tablaSecundaria">
								<tbody>
									<tr>
										<td align="center">
											<div align="center">
												<table bgcolor="#555242" cellpadding="0" cellspacing="0" class="encabezado">
													<tbody>
														<tr>
															<td bgcolor="#8F8B70" width="10%" align="center">Secci�n</td>
															<td width="4%" align="center" bgcolor="#d90909">|</td>
															<td align="left" width="64%">Alta Tarifa-Cobertura</td>
															<td align="right" width="14%" >Fecha</td>
															<td align="left" width="10%">
																<input name="fecha" readonly="readonly" size="10">
															</td>
														</tr>
													</tbody>
												</table>
										    </div>
											<br>
											<h:form id="frmCombos" enctype="multipart/form-data">
											<div align="center">
												<table bgcolor="#8F8B70" cellpadding="0" cellspacing="0" width="95%" border="2">
													<tbody>
														<tr>
															<td></td>
															<td align="right"><h:outputText value="Ramo:" /></td>
															<td align="left">
                                                                         <h:selectOneMenu id="ramo" value="#{beanAltaTarifaCobertura.inRamo}"  binding="#{beanListaRamo.inRamo}" required="true" style="width : 227px; height : 21px;">
																		<f:selectItem itemValue="0" itemLabel="Ramo"/>
																		<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																		<a4j:support event="onchange"  action="#{beanListaRamo.ambos}" ajaxSingle="true" reRender="producto,polizas"/>
																	</h:selectOneMenu>
																	<h:message for="ramo" styleClass="errorMessage"/>
													        </td>	


                                               
															<td align="right"><h:outputText value="Poliza:" /></td>
															<td align="left">
																 <a4j:outputPanel id="polizas" ajaxRendered="true" >
												                  <h:selectOneMenu id="poliza"  value="#{beanAltaTarifaCobertura.inPoliza}" >
																	<f:selectItem itemValue="" itemLabel="Poliza"/>
																	<f:selectItems value="#{beanListaRamo.listaPolizas}" />
															      </h:selectOneMenu>
														         </a4j:outputPanel>
														         
															<td></td>
														</tr>
																											
														
														
														<tr>
															<td></td>
															<td align="right"><h:outputText value="Producto:" /></td>
															<td align="left">
                                                                   <a4j:outputPanel id="productos" ajaxRendered="true">
																		<h:selectOneMenu id="producto" value="#{beanAltaTarifaCobertura.inProducto}" binding="#{beanListaRamo.inProducto}" >
																			<f:selectItem itemValue="" itemLabel="Producto"/>
																			<f:selectItems value="#{beanListaRamo.listaProductos}"/>
																			<a4j:support event="onchange"  action="#{beanListaRamo.cargaPlanes}" ajaxSingle="true" reRender="planes"/>
																		</h:selectOneMenu>
																		<h:message for="producto" styleClass="errorMessage"/>
																</a4j:outputPanel>
															</td>  
															
															
														
															<td align="right"><h:outputText value="Plan:" /></td>
															<td align="left">
																<a4j:outputPanel id="planes" ajaxRendered="true">
																		<h:selectOneMenu id="plan" value="#{beanAltaTarifaCobertura.inPlan}" style=" width : 214px; height : 22px;">
																			<f:selectItem itemValue="" itemLabel="Plan"/>
																			<f:selectItems value="#{beanListaRamo.listaPlanes}"/>
																		</h:selectOneMenu>
																		<h:message for="plan" styleClass="errorMessage"/>
																</a4j:outputPanel>
															
															
															
															<td></td>
														
														</tr>
														
						
														
                                                           <tr>
															<td></td>
                                                                 <td align="right"><h:outputText value="Ramo Contable:" /></td>
															<td align="left">
                                                                   <h:selectOneMenu id="ramoContable" value="#{beanAltaTarifaCobertura.inRamoContable}" binding="#{beanListaRamoContable.inRamoContable}" required="true" style=" width : 230px;">
																		<f:selectItem itemValue="0" itemLabel="Ramo Contable"/>
																		<f:selectItems value="#{beanListaRamoContable.beansRamosContables}"/>
																		<a4j:support event="onchange"  action="#{beanListaRamoContable.cargaCoberturas}" ajaxSingle="true" reRender="coberturas"/>																	
																        </h:selectOneMenu>
																	<h:message for="ramoContable" styleClass="errorMessage"/>
															</td>  
															
	
															<td align="right"><h:outputText value="Cobertura:" /></td>
															<td align="left">
																<a4j:outputPanel id="coberturas" ajaxRendered="true">
																		<h:selectOneMenu id="cobertura" value="#{beanAltaTarifaCobertura.inCobertura}" required="true" style="width : 214px; height : 24px;">
																			<f:selectItem itemValue="" itemLabel="Cobertura"/>
																			<f:selectItems value="#{beanListaRamoContable.listaCoberturas}"/>
																		</h:selectOneMenu>
																		<h:message for="cobertura" styleClass="errorMessage"/>
																</a4j:outputPanel>
	                                                           </td>      
															<td></td>
															
															
															
															
															<tr>
															<td></td>
															<td align="right"><h:outputText value="Monto:" /></td>
															<td align="left">
                                                                    <h:inputText id="montoCom"  value="#{beanAltaTarifaCobertura.inMonto}" style=" width : 226px;" >  
                                                                        <f:validateLength  maximum="99999999"/>
                                                                        </h:inputText>		
																	<h:message for="montoCom" styleClass="errorMessage"/>
													        </td>	



                                               
															<td align="right"><h:outputText value="Porcentaje:" /></td>
															<td align="left">
																 <h:inputText id="porcentajeCom"  value="#{beanAltaTarifaCobertura.inPorcentaje}" style=" width : 211px;"  >  
                                                                        <f:validateLength  maximum="100"/>
                                                                        </h:inputText>		
																	<h:message for="porcentajeCom" styleClass="errorMessage"/>
																 </td>  
															<td></td>
														</tr>
														
														<tr>
																														
															<td></td>
															<td align="right"><h:outputText value="Tarifa:" /></td>
															<td align="left">
                                                                        <h:inputText id="descripCober"  value="#{beanAltaTarifaCobertura.inTaRiesgo}"  style=" width : 227px;" >  
                                                                        <f:validateLength  maximum="100"/>
                                                                        </h:inputText>		
          											    		<h:message for="descripCober" errorStyle="errorMessage"/>										
													        </td>																								
															
														</tr>
														

														
														<tr>
															<td></td>
															<td align="right"><h:outputText value="Edad Min:" /></td>
															<td align="left">
                                                                    <h:inputText id="edadMin"  value="#{beanAltaTarifaCobertura.inEdadMin}" style=" width : 71px;" >  
                                                                        <f:validateLength  maximum="3"/>
                                                                        </h:inputText>		
																	<h:message for="edadMin" styleClass="errorMessage"/>
													       </td>	



                                               
															<td align="right"><h:outputText value="Edad Max:" /></td>
															<td align="left">
																 <h:inputText id="edadMax"  value="#{beanAltaTarifaCobertura.inEdadMax}" style=" width : 71px;" >  
                                                                        <f:validateLength  maximum="3"/>
                                                                        </h:inputText>		
																	<h:message for="porcentajeCom" styleClass="errorMessage"/>
																 </td>  
															<td></td>
														</tr>
														
			
																									
															<tr>
															<td></td>
															<td align="right"><h:outputText value="Fecha Inicio:" /></td>
															<td align="left">
                                                                         <a4j:outputPanel id="fechaIni" ajaxRendered="true">
                                                                            <rich:calendar  id="fechIni" value="#{beanAltaTarifaCobertura.inFechIni}" required="true" datePattern="dd/MM/yyyy" /> 
																		    <h:message for="fechIni" styleClass="errorMessage"/>
																         </a4j:outputPanel>
													        </td>	


															<td align="right"><h:outputText value="Fecha Fin:" /></td>
															<td align="left">
																 <a4j:outputPanel id="fechaFin" ajaxRendered="true">
                                                                            <rich:calendar  id="fechFin" value="#{beanAltaTarifaCobertura.inFechFin}" required="true" datePattern="dd/MM/yyyy"/>
                                                                            <h:message for="fechIni" styleClass="errorMessage"/>
																</a4j:outputPanel>
																 </td>  
															<td></td>
														</tr>													
													</tbody>
												</table>
											</div>											
																						
											<div align="center">
												<table bgcolor="#8F8B70" cellpadding="0" cellspacing="0" width="95%" border="2" align="center">
													<tbody>
													<tr>
														   <td align="left" width="20%">
																<h:commandButton styleClass="boton" value="Limpia" type="reset"/>
															</td>
															<td align="right" width="20%">
																<h:commandButton styleClass="boton" value="Alta" action="#{beanAltaTarifaCobertura.iniciaAlta}" style=" width : 77px;"/>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											</h:form>
											<h:messages styleClass="errorMessage" globalOnly="true" style="COLOR: #000000;"/>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			</div>
		<%@ include file="../footer.jsp" %>
	</f:view>
  <br>
 </body>
</html>