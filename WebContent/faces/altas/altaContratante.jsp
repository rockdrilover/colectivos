<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Alta Contratante</title>
</head>
<body>
<f:view>  <!-- Desde aqui comienzo --> 
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div> 
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td align="center">
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
					<div align="center">
					<table class="encabezadoTabla">
					<tbody>
						<tr>
							<td width="10%" align="center">Secci�n</td>
							<td width="4%" align="center" bgcolor="#d90909">|||</td>
							<td align="left" width="64%">Alta de Contratante</td>
							<td align="right" width="12%" >Fecha</td>
							<td align="left" width="10%">
								<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7" />
							</td>
						</tr>
					</tbody>
					</table>
				    </div>	
				    <rich:spacer height="10px"></rich:spacer>
				    <h:form id="frmParamObligado">
				    <div align="center">
					    <rich:spacer height="15"/>	
					    <table class="botonera">
					    <tbody>
						    <tr>
							    <td class="frameTL"></td>
							    <td class="frameTC"></td>
							    <td class="frameTR"></td>
						    </tr>
						    <tr>
							    <td class="frameCL"></td>
							    <td class="frameC">
							    <a4j:region id="consultaDatos">
								    <table>
								    <tbody>
								    	<tr> 
									        <td align="right" width="20%" height="35"><h:outputText value="Raz�n Social:"/></td>
									         <td align="left" width="50%" height="30">
								     			<h:inputText id="razonSocial"  value="#{beanAlta.inRazonSocial}" required="true" />
								     			<h:message for="razonSocial" styleClass="errorMessage" />		 
									        </td>
							     		  </tr>
							     		 <tr> 
									        <td align="right" width="20%" height="35"><h:outputText value="Rfc:"/></td>
									         <td align="left" width="50%" height="30">
								     			<h:inputText id="rfc"  value="#{beanAlta.inRfc}" required="true" />
								     			<h:message for="rfc" styleClass="errorMessage" />		 
									        </td>
							     		  </tr>
							     		 <tr> 
									        <td align="right" width="20%" height="35"><h:outputText value="Fecha Constitucion:"/></td>
									         <td align="left" width="50%" height="25">
								     			<rich:calendar id="fecha_con"  value="#{beanAlta.inFechaCons}" style ="width:15%;" popup="true"     
														styleClass="calendario" required="false" datePattern="dd/MM/yyyy" />  	                            
										        <h:message for="fecha_con" styleClass="errorMessage"/>                           
	 								        </td>
							     		  </tr>
						     			 <tr> 
									        <td align="right" width="20%" height="35"><h:outputText value="Direcci�n:"/></td>
									         <td align="left" width="50%" height="30">
								     			<h:inputText id="calle" value="#{beanAlta.inCalleNum}" required="true" />
								     			<h:message for="calle" styleClass="errorMessage" />		 
									        </td>
							     		  </tr>
							     		<tr> 
									        <td align="right" width="20%" height="35"><h:outputText value="Colonia:"/></td>
									         <td align="left" width="50%" height="30">
								     			<h:inputText id="colonia" value="#{beanAlta.inColonia}" required="true" />
								     			<h:message for="colonia" styleClass="errorMessage" />		 
									        </td>
							     		  </tr>	
								    	
								    	<tr> 
									        <td align="right" width="20%" height="35"><h:outputText value="Cp:"/></td>
									         <td align="left" width="50%" height="30">
								     			<h:inputText id="cp"  value="#{beanAlta.inCp}" required="true" />
								     			<h:message for="cp" styleClass="errorMessage" />		 
									        </td>
							     		  </tr>
							     		  
							     		 <tr>
							     			<td align="right" width="20%"><h:outputText value="Estado:"/></td>
											<td align="left" width="80%">
												<a4j:outputPanel id="estado" ajaxRendered="true">
													<h:selectOneMenu id="inEstado"  value="#{beanAlta.inPoblacion}"  binding="#{beanListaParametros.inEstado}" title="header=[Estado] body=[Seleccione un estado] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
															<f:selectItem itemValue="" itemLabel="<Seleccione Estado>"/>
															<f:selectItems value="#{beanListaEstado.listaComboEstados}"/>
																	
													</h:selectOneMenu>
													<h:message for="inEstado" styleClass="errorMessage" />
												</a4j:outputPanel>
											</td>
							     		  </tr>  
		  
							     		 <tr>                                                                                                                                                                                                                               
											 <td align="center" colspan="4"><h:outputText value="Cedula Generada" styleClass="importante2" style="color:#000000;font-size: 20px;"/>&nbsp;&nbsp;</td>                                                                          
										 </tr>                                                                                                                                                                                                                              
											<tr>                                                                                                                                                                                                                             
												<td align="center" colspan="4" >                                                                                                                                                                                               
			                                        	<h:outputText id="cedula"  value="#{beanAlta.cedulaRif}" styleClass="importante2"/>									                                                                                                   
											        </td>                                                                                                                                                                                                                    
												</tr>	                                                                                                                                                                                                                         
												<tr height="10"></tr>  
								    	
								    	<tr>
											<td align="right" width="20%">
											<td align="left" width="50%" height="30">
										       <h:commandButton value="Guardar" action="#{beanAlta.iniciaAltaContratante}" style=" width : 162px;"/>
									        </td>
										</tr>		
							     	</tbody>     		         		    		
								    </table> 
								    
								    <rich:spacer height="15px"></rich:spacer>
								    <table>
							    	<tbody>
							    		<tr>
							    			<td align="center">
								    			<a4j:status for="consultaDatos" stopText=" ">
													<f:facet name="start">
														<h:graphicImage value="/images/ajax-loader.gif" />
													</f:facet>
												</a4j:status>
											</td>
										</tr>
									</tbody>
									</table>
								</a4j:region>	
								</td>							    
							    <td class="frameCR"></td>
							    </tr>
							    <tr>
									<td class="frameBL"></td>
									<td class="frameBC"></td>
									<td class="frameBR"></td>
								</tr>
						    </tbody>
						    </table>
						    <rich:spacer height="10px"></rich:spacer>
						    
					</div>
			     	</h:form>
				</td>
			</tr>
		</tbody>
   		</table>
   		</td>
   	</tr>
</tbody>
</table>   	
</div>
<rich:spacer height="15px"></rich:spacer>
<div>
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>