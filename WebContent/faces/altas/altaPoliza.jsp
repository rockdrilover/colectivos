<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
	<title>Alta de Poliza</title>
</head>
<body>
<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
	<tbody>
		<tr>	
			<td align="center">
				<table class="tablaSecundaria">
					<tbody>
						<tr>
							<td align="center">
								<div align="center">
									<table bgcolor="#555242" cellpadding="0" cellspacing="0" class="encabezado">
										<tbody>
											<tr>
												<td width="10%" align="center">Secci�n</td>
												<td width="4%" align="center" bgcolor="#d90909">|</td>
												<td align="left" width="64%">Alta de Poliza</td>
												<td align="right" width="14%" >Fecha</td>
												<td align="left" width="10%">
													<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
												</td>
											</tr>
										</tbody>
									</table>
							    </div>
								<br>
								<h:form id="frmCombos">
						<div align="center">
						<table class="botonera">
						    <tbody>
							    <tr>
								    <td class="frameTL"></td>
								    <td class="frameTC"></td>
								    <td class="frameTR"></td>
							    </tr>
							    <tr>
								    <td class="frameCL"></td>
								    <td class="frameC" width="650px">
										<table>
											<tbody>
												<tr>
												    <td align="right"><br/><h:outputText value="Ramo:" />&nbsp;&nbsp;</td>
													<td align="left" colspan="3"><br/>
														   <h:selectOneMenu id="ramo" value="#{beanAlta.inRamo}" binding="#{beanListaRamo.inRamo}" required="true" style="width : 95%; height : 23px;">
																<f:selectItem itemValue="0" itemLabel="Ramo"/>
																<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
																<a4j:support event="onchange"  ajaxSingle="true" reRender="productos"/>
															</h:selectOneMenu>
															<h:message for="ramo" styleClass="errorMessage"/>	
													</td> 
													<td align="center"><h:outputText value="Fecha de Inicio de Vigencia:" /></td>	
													
												</tr>
												<tr>
													<td align="right"><h:outputText value="Sucursal:" />&nbsp;&nbsp;</td>
													<td align="left">
			                                        	<h:inputText id="sucursal"  value="#{beanAlta.inSucursal}" required="true">  
			                                                <f:validateLength  maximum="4"/>
			     										</h:inputText>		
			     										<h:message for="sucursal" styleClass="errorMessage"/>										
											        </td> 
											        <td align="right"><h:outputText value="Analista:" />&nbsp;&nbsp;</td>
													<td align="left">
														 	 <h:inputText id="analista"  value="#{beanAlta.inAnalista}" required="true">  
			                                                                   <f:validateLength  maximum="10"/>
			     											    		</h:inputText>		
			     											 <h:message for="analista" styleClass="errorMessage"/>
											        </td> 
											        <td align="center" rowspan="4">
														 	 <rich:calendar id="fecha_ini"  value="#{beanAlta.inFechaIni}" inputSize="10" 
									    									popup="false" cellHeight="20px"  cellWidth="20px" datePattern="dd/MM/yyyy" 
									    									styleClass="calendario" required="true" />  	
			     											 <h:message for="fecha_ini" styleClass="errorMessage"/>
											        </td>
												</tr>	
												<tr>
													<td align="center" colspan="4"><h:outputText value="Poliza Generada" styleClass="importante2" style="color:#000000;font-size: 20px;"/>&nbsp;&nbsp;</td>
												</tr>
												<tr>
													<td align="center" colspan="4" >
			                                        	<h:outputText id="poliza"  value="#{beanAlta.poliza}" styleClass="importante2"/>									
											        </td> 
												</tr>	
												<tr height="10"></tr>													
											</tbody>
										</table>
									</td>
									<td class="frameCR"></td>
								   </tr>
								    <tr>
										<td class="frameBL"></td>
										<td class="frameBC"></td>
										<td class="frameBR"></td>
									</tr>
								 </tbody>
							</table>
						</div>											
						<br/>											
						<div align="center">
							<table align="center">
								<tbody>
								  <tr>
									<td align="center" width="100%">
										<h:commandButton value="Generar P�liza" action="#{beanAlta.iniciaAlta}" style=" width : 162px;"/>
									</td>
									</tr>
								</tbody>
							</table>
						</div>
						</h:form>
						<h:messages styleClass="importante" globalOnly="true"/>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<rich:spacer height="10px"/>
<div align="center">
<%@ include file="../footer.jsp" %>
</div>
</f:view>
<br>
</body>
</html>