<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="../include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../../css/estilos.css">
<title>Agregar producto</title>
</head>
<body>
<f:view>
<div align="center">
<%@include file="../header.jsp" %>
</div>
<div align="center">
<table class="tablaPrincipal">
<tbody>
	<tr>
		<td>
		<table class="tablaSecundaria">
		<tbody>
			<tr>
				<td>
				<div align="center">
				<table class="encabezadoTabla" >
				<tbody>
					<tr>
						<td width="10%" align="center">Secci�n</td>
						<td width="4%" align="center" bgcolor="#d90909">||||</td>
						<td align="left" width="64%">Agregar producto</td>
						<td align="right" width="12%" >Fecha</td>
						<td align="left" width="10%">
							<input name="fecha" readonly="readonly" value="<%= request.getSession().getAttribute("fecha") %>" size="7">
						</td>
					</tr>
				</tbody>
				</table>
			    </div>
			    <rich:spacer height="10px"></rich:spacer>
			    <h:form>
			    <div align="center">
			    	<rich:spacer height="15"/>	
				    <table class="botonera">
				    <tbody>
					    <tr>
						    <td class="frameTL"></td>
						    <td class="frameTC"></td>
						    <td class="frameTR"></td>
					    </tr>
					    <tr>
						    <td class="frameCL"></td>
						    <td class="frameC">
						    	<table>
						    	<tbody>
						    		<tr>
						    			<td align="right" width="20%"><h:outputText value="Ramo:" /></td>
						    			<td align="left" width="60%">
											<h:selectOneMenu id="inRamo" value="#{beanProducto.selectedRamo}" binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
												<f:selectItem itemValue="0" itemLabel="Selecione un ramo" />
												<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
											</h:selectOneMenu>
										</td>
										<td align="left" width="20%"><h:message for="inRamo" styleClass="errorMessage" /></td>
						    		</tr>
						    		<tr>
						    			<td align="right"><h:outputText value="Descripci�n producto:" /> </td>
						    			<td align="left">
						    				<h:inputTextarea id="descripcionProducto" value="#{beanProducto.alprDeProducto}" rows="2" cols="40" >
						    					<f:validateLength minimum="10" maximum="100"/>
						    				</h:inputTextarea>	 
						    			</td>
						    			<td align="left"><h:message for="descripcionProducto" styleClass="errorMessage"/> </td>
						    		</tr>
						    	</tbody>
						    	</table>
						    	<rich:spacer height="10px"></rich:spacer>
						    	<table>
						    	<tbody>
						    		<tr>
						    			<td align="center"><h:commandButton action="#{beanProducto.cancelar}" styleClass="boton" value="Cancelar" immediate="true" /> </td>
						    			<td></td>
						    			<td align="center"><h:commandButton action="#{beanProducto.guardarProducto}" value="Aceptar" styleClass="boton"/> </td>
						    		</tr>
						    	</tbody>
						    	</table>
						    </td>
						    <td class="frameCR"></td>
					    </tr>
					    <tr>
							<td class="frameBL"></td>
							<td class="frameBC"></td>
							<td class="frameBR"></td>
						</tr>
				    </tbody>
				    </table>		    
			    </div>
			    </h:form>
				</td>
			</tr>
		</tbody>
		</table>
		</td>
	</tr>
</tbody>
</table>
</div>
<div align="center">
<%@include file="../footer.jsp" %>
</div>
</f:view>
</body>
</html>