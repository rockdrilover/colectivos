<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="include.jsp" %>

<f:view>
	<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
	
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="../css/estilos.css">
		<title><h:outputText value="#{msgs.sistema}" /></title>
	</head>

<body>
	<%-- DO NOT MOVE! The following AllWebMenus linking code section must always be placed right AFTER the BODY tag --%>
	<%-- ******** BEGIN ALLWEBMENUS CODE FOR menu ******** --%>
	<script type="text/javascript">var MenuLinkedBy="AllWebMenus [4]",awmMenuName="menu",awmBN="754";awmAltUrl="";</script>
	<script src="../menu/menu.js" type="text/javascript"></script>
	<script type="text/javascript" language="JavaScript" src="../menu/js/awmlib2.js"></script>
	<script type="text/javascript" language="JavaScript" src="../menu/js/awmlib1.js"></script>
	<script type="text/javascript">awmBuildMenu();</script>
	<%-- ******** END ALLWEBMENUS CODE FOR menu ******** --%>
	
	 
	<div class="center_div">
		<table class="taprincipal1">
			<caption></caption>
			<tr>
			    <td class="left_div">
			    	<img src="../images/logo.gif" width="245" height="40" alt="Logo Santander">
			    </td>
				<td class="right-div">
					<img src="../images/logoNew.png" width="120" height="111" alt="Logo Colectivos">								
				</td>		
			</tr>
			<tr>
			    <td/>
				<td class="right-div">
					<h:outputText value="#{beanSesion.nombreUsuario}" style="COLOR: #800000; font-weight:bold; font-size:12px; font-family:Verdana;"/>
				</td>					
			</tr>
		</table>
	</div>

	<div class="center_div">
		<br></br>
		
		<table class="taprincipal2">
			<caption></caption> 
			<tr>
				<td id="awmAnchor-menu" class="tdmenu"></td>
				<td class="encabezado4"></td>
				<td class="left_div"rowspan="3">
				    <img src="../images/logo2.gif" width="455" height="380" alt="Bienvenidos a Colectivos">
				</td>			
			</tr>
		</table>
		
	</div>
</body>
</html>
</f:view>