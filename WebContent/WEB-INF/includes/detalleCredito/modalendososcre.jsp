<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Detalle Credito Siniestros
- Fecha: 10/03/2021
- Descripcion: Modal para mostrar ENDOSOS de un credito
--%>
<%@include file="../include.jsp"%>

<rich:modalPanel id="endPanel" autosized="true" width="600">
  	<f:facet name="header"><h:outputText value="#{msgs.detcreend}" /></f:facet>
   	<f:facet name="controls">
       <h:panelGroup>
           <h:graphicImage value="../../images/cerrarModal.png" 
           		id="hidelinkEnd" 
           		styleClass="hidelink"
           		onclick="muestraOcultaSeccion('resultado', 1);verCteCert(2,#{beanConsDetalleCreSin.nEjecuta},0,0);"/>
           <rich:componentControl for="endPanel" attachTo="hidelinkEnd" operation="hide" event="onclick" />          
       </h:panelGroup>
   </f:facet>
   
   <h:form id="endosos">
       <rich:messages styleClass="errorMessage"></rich:messages>
       <h:panelGrid columns="1" width="90%">
       		<a4j:outputPanel id="panelEnd" ajaxRendered="true">
            	<h:panelGrid columns="6">	            		
            		<h:outputText value="#{msgs.sistemacerti}:   " styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCasuCdSucursal} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCarpCdRamo} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCapoNuPoliza} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceNuCertificado} (" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceNuCredito} )" styleClass="texto_mediano"/>
            	</h:panelGrid>
            	
            	<rich:separator height="4" lineType="double" />
                
                <rich:spacer height="10px" />
                
                <rich:dataTable value="#{beanConsDetalleCreSin.selCliente.lstEndosos}" 
                	id="listaEndoso" 
                	var="beanEndoso" 
                	rows="10" 
                	columns="5" 
                	styleClass="tablacen80"
                	width="100%">
		      		
		      		<f:facet name="header">
		      			<h:outputText value="#{msgs.detcreend}" />																	      			
		      		</f:facet>
		      		
		       		<rich:column>
		       			<f:facet name="header"><h:outputText value="#{msgs.confprocesono}" /></f:facet>
							<h:outputText value="#{beanEndoso.id.cedaNuEndosoDetalle}" />
					</rich:column>
		      		<rich:column>
		      			<f:facet name="header"><h:outputText value="#{msgs.sistematipo}" /></f:facet>
						<h:outputText value="#{beanEndoso.cedaCampov3}" />																					
					</rich:column>
		      		<rich:column>
		      			<f:facet name="header"><h:outputText value="#{msgs.timbradofol}" /></f:facet>
						<h:outputText value="#{beanEndoso.cedaCampov1}" />																				
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.loginusuario}" /></f:facet>
						<h:outputText value="#{beanEndoso.cedaCdUsuarioAplica}" />
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.detcreendapli}" /></f:facet>
						<h:outputText value="#{beanEndoso.cedaFeAplica}" >
							<f:convertDateTime pattern="dd/MM/yyyy HH:mm" />
						</h:outputText>																				
					</rich:column>					
		    	</rich:dataTable>
       		</a4j:outputPanel>
       		
			<rich:spacer height="10px" />
       </h:panelGrid>
   </h:form>
</rich:modalPanel>