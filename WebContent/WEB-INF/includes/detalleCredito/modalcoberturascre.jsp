<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Detalle Credito Siniestros
- Fecha: 10/03/2021
- Descripcion: Modal para mostrar COBERTURAS de un credito
--%>
<%@include file="../include.jsp"%>

<rich:modalPanel id="cobPanel" autosized="true" width="600">
  	<f:facet name="header"><h:outputText value="#{msgs.detcrecob}" /></f:facet>
   	<f:facet name="controls">
       <h:panelGroup>
           <h:graphicImage value="../../images/cerrarModal.png" 
           		id="hidelinkCob" 
           		styleClass="hidelink" 
           		onclick="muestraOcultaSeccion('resultado', 1);verCteCert(2,#{beanConsDetalleCreSin.nEjecuta},0,0);"/>
           <rich:componentControl for="cobPanel" attachTo="hidelinkCob" operation="hide" event="onclick" />           
       </h:panelGroup>
   </f:facet>
   
   <h:form id="coberturas">
       <rich:messages styleClass="errorMessage"></rich:messages>
       <h:panelGrid columns="1" width="90%">
       		<a4j:outputPanel id="panelCob" ajaxRendered="true">
            	<h:panelGrid columns="6">	            		
            		<h:outputText value="#{msgs.sistemacerti}:   " styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCasuCdSucursal} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCarpCdRamo} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCapoNuPoliza} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceNuCertificado} (" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceNuCredito} )" styleClass="texto_mediano"/>
            	</h:panelGrid>
            	
            	<rich:separator height="4" lineType="double" />
                
                <rich:spacer height="10px" />
                
                <rich:dataTable value="#{beanConsDetalleCreSin.selCliente.lstCoberturas}"
               		id="listaCoberturas" 
                	var="regCob" 
                	rows="15" 
                	columnsWidth="10%,60%,10%,20%"
                	columns="4" 
                	styleClass="tablaCen100">
		      		
		      		<f:facet name="header">
		      			<rich:columnGroup>
		      				<rich:column styleClass="wid10"><h:outputText value="#{msgs.comisioncob}" /></rich:column>
		      				<rich:column styleClass="wid60"><h:outputText value="#{msgs.timbradodesc} #{msgs.comisioncob}" /></rich:column> 
		      				<rich:column styleClass="wid10"><h:outputText value="#{msgs.sistematarifa}" /></rich:column>
		      				<rich:column styleClass="wid20"><h:outputText value="#{msgs.sistemasumaseg}" /></rich:column>
		      			</rich:columnGroup>
		      		</f:facet>
		       		<rich:column styleClass="wid10">
						<h:outputText value="#{regCob.id.cocbCarbCdRamo} - #{regCob.id.cocbCacbCdCobertura}" />
					</rich:column>
		      		<rich:column styleClass="wid60">
						<h:outputText value="#{regCob.cocbCampov3}" />
						<f:facet name="footer">         										
							<h:outputText value="TARIFA TOTAL" style="text-align:center; FONT-WEIGHT: bold"/>
				    	</f:facet>
					</rich:column>
		      		<rich:column styleClass="wid10">
						<h:outputText value="#{regCob.cocbTaRiesgo}" />
						<f:facet name="footer">         										
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceTpPma}" style="text-align:center; FONT-WEIGHT: bold" />
				    	</f:facet>
					</rich:column>
					<rich:column styleClass="wid20">
						<h:outputText value="#{regCob.cocbMtFijo}" />
					</rich:column>
		    	</rich:dataTable>
       		</a4j:outputPanel>
       		
			<rich:spacer height="10px" />
       </h:panelGrid>
   </h:form>
</rich:modalPanel>