<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Detalle Credito Siniestros
- Fecha: 10/03/2021
- Descripcion: Modal para mostrar ASISTENCIAS de un credito
--%>
<%@include file="../include.jsp"%>

<rich:modalPanel id="asisPanel" autosized="true" width="600">
  	<f:facet name="header"><h:outputText value="#{msgs.detcreasis}" /></f:facet>
   	<f:facet name="controls">
       <h:panelGroup>
           <h:graphicImage value="../../images/cerrarModal.png" 
           		id="hidelinkAsis" 
           		styleClass="hidelink" 
           		onclick="muestraOcultaSeccion('resultado', 1);verCteCert(2,#{beanConsDetalleCreSin.nEjecuta},0,0);"/>
           <rich:componentControl for="asisPanel" attachTo="hidelinkAsis" operation="hide" event="onclick" />
       </h:panelGroup>
   </f:facet>
   
   <h:form id="asistencias">
       <rich:messages styleClass="errorMessage"></rich:messages>
       <h:panelGrid columns="1" width="90%">
       		<a4j:outputPanel id="panelAsis" ajaxRendered="true">
            	<h:panelGrid columns="6">	            		
            		<h:outputText value="#{msgs.sistemacerti}:   " styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCasuCdSucursal} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCarpCdRamo} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCapoNuPoliza} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceNuCertificado} (" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceNuCredito} )" styleClass="texto_mediano"/>
            	</h:panelGrid>
            	
            	<rich:separator height="4" lineType="double" />
                
                <rich:spacer height="10px" />
                
				<rich:dataTable value="#{beanConsDetalleCreSin.selCliente.lstAsistencias}" 
						id="listaAsistencias" 
						var="beanAsistencias" 
						rows="10" 						
						styleClass="tablacen80">
		      		<f:facet name="header">
		      			<rich:columnGroup>
		      				<rich:column>
		      					<h:outputText value="#{msgs.timbradodesc} #{msgs.detcreasis}" />
		      				</rich:column>
		      			</rich:columnGroup>
		      		</f:facet>
		      		<rich:column>
						<h:outputText value="#{beanAsistencias.certificado.coceCampov2}" />
					</rich:column>
		    	</rich:dataTable>       
                
       		</a4j:outputPanel>
       		
			<rich:spacer height="10px" />
       </h:panelGrid>
   </h:form>
</rich:modalPanel>