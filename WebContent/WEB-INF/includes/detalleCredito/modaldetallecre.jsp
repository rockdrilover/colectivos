<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Detalle Credito Siniestros
- Fecha: 09/03/2021
- Descripcion: Modal para mostrar detalle credito
--%>
<%@include file="../include.jsp"%>

<rich:modalPanel id="detPanel" autosized="true" width="780">
   	<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle} #{msgs.timbradocert}" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" 
            	id="hidelink" 
            	styleClass="hidelink" 
            	onclick="muestraOcultaSeccion('resultado', 1);verCteCert(2,#{beanConsDetalleCreSin.nEjecuta},0,0);"/>            
            <rich:componentControl for="detPanel" attachTo="hidelink" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>
    
    <h:form id="detalleCertificado">
        <rich:messages style="color:red;"></rich:messages>
        <h:panelGrid columns="1">
            <a4j:outputPanel id="panelDet" ajaxRendered="true">
            	<h:panelGrid columns="6">	            		
            		<h:outputText value="#{msgs.sistemacerti}:   " styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCasuCdSucursal} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCarpCdRamo} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCapoNuPoliza} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceNuCertificado} (" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceNuCredito} )" styleClass="texto_mediano"/>
            	</h:panelGrid>
            	
            	<rich:separator height="4" lineType="double" />
                
                <rich:spacer height="10px" />
                
                <table class="tablacen100b">
                	<caption></caption>
					
					<tr> 
						<td class="leftdivbo1"><h:outputText value="#{msgs.timbradoestsis}:" /></td>
						<td class="right-div2"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceCampov1}" /></td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo1"><h:outputText value="#{msgs.sistemafecar}:"/></td>
						<td class="right-div1">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeCarga}" >
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
					    </td>  		  			
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>
						
						<td class="leftdivbo1"><h:outputText value="#{msgs.detcrecertrecibo}:"/></td>
						<td class="right-div1"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceNoRecibo}" /></td>
					</tr>
					<tr>
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemaEst}:" /></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceCampov2}" /></td>  		  			
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.timbradofeemi}:"/></td>
						<td class="right-div">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeSuscripcion}" >
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
						</td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemafechad}:"/></td>
						<td class="right-div">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeDesde}" >
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
						</td>						
					</tr>
					<tr>
						<td class="leftdivbo"><h:outputText value="#{msgs.detcrecertmotanu}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceCampov3}" /></td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemasumaseg}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceMtSumaAsegurada}" /></td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemafechah}:"/></td>
						<td class="right-div">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeHasta}" >
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
						</td>
					</tr>
					<tr>
						<td class="leftdivbo"><h:outputText value="#{msgs.detcrecertfeanu}:"/></td>
						<td class="right-div">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeAnulacionCol}" >
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
						</td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.detcrecertbasecal}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceMtSumaAsegSi}" /></td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.detcrecertinicre}:"/></td>
						<td class="right-div">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeIniCredito}" >
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
						</td>						
					</tr>
					<tr>
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemaproducto}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceTpProductoBco}" /></td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemapmatot}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceMtPrimaSubsecuente}" /></td>  		  			
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.detcrecertfincre}:"/></td>
						<td class="right-div">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeFinCredito}" >
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
						</td>						
					</tr>
					<tr>
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemaplan}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceTpSubprodBco}" /></td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemapmaneta}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceMtPrimaPura}" /></td>  		  			
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
						<td class="leftdivbo"><h:outputText value="#{msgs.sistemasuc}:"/></td>
						<td class="right-div"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceCazbCdSucursal}" /></td>
					</tr>
					<tr>
						<td class="leftdivbo"><h:outputText value="#{msgs.detcrecertfeven}:"/></td>
						<td class="right-div">
							<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceFeAnulacion}">
								<f:convertDateTime pattern="dd-MM-yyyy"/>
							</h:outputText>
						</td>
						<td class="wid1por"><h:outputText value="#{msgs.sistemaespacio}" /></td>  
						
					    <td class="leftdivbo"><h:outputText value="#{msgs.detcrecertinfo}:"/></td>
						<td class="left_div" colspan="4"><h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceEmpresa}" /></td>																							
					</tr>
                </table>
            </a4j:outputPanel>	                	              
        </h:panelGrid>
    </h:form>
</rich:modalPanel>