<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Detalle Credito Siniestros
- Fecha: 10/03/2021
- Descripcion: Modal para mostrar ASEGURADOS de un credito
--%>
<%@include file="../include.jsp"%>

<rich:modalPanel id="asegPanel" autosized="true" width="700">
  	<f:facet name="header"><h:outputText value="#{msgs.detcreaseg}" /></f:facet>
   	<f:facet name="controls">
       <h:panelGroup>
           <h:graphicImage value="../../images/cerrarModal.png" 
           		id="hidelinkAseg" 
           		styleClass="hidelink"
           		onclick="muestraOcultaSeccion('resultado', 1);verCteCert(2,#{beanConsDetalleCreSin.nEjecuta},0,0);"/>
           <rich:componentControl for="asegPanel" attachTo="hidelinkAseg" operation="hide" event="onclick" />           
       </h:panelGroup>
   </f:facet>
   
   <h:form id="asegurados">
       <rich:messages styleClass="errorMessage"></rich:messages>
       <h:panelGrid columns="1" width="90%">
       		<a4j:outputPanel id="panelAseg" ajaxRendered="true">
            	<h:panelGrid columns="6">	            		
            		<h:outputText value="#{msgs.sistemacerti}:   " styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCasuCdSucursal} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCarpCdRamo} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceCapoNuPoliza} -" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.id.coceNuCertificado} (" styleClass="texto_mediano"/>
            		<h:outputText value="#{beanConsDetalleCreSin.seleccionado.coceNuCredito} )" styleClass="texto_mediano"/>
            	</h:panelGrid>
            	
            	<rich:separator height="4" lineType="double" />
                
                <rich:spacer height="10px" />
                
				<rich:dataTable value="#{beanConsDetalleCreSin.selCliente.lstAsegurados}" 
					id="listaAsegurados" 
					var="beanAseg" 
					rows="10" 
					columns="7" 
                	styleClass="tablacen80"
                	width="100%">
		      		
		      		<f:facet name="header">
		      			<h:outputText value="#{msgs.detcreaseg}" />																		      			
			      	</f:facet>
			      	
		       		<rich:column>
		       			<f:facet name="header"><h:outputText value="#{msgs.loginnombre}" /></f:facet>
						<h:outputText value="#{beanAseg.cliente.cocnNombre}" />
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.sistemaappaterno}" /></f:facet>
						<h:outputText value="#{beanAseg.cliente.cocnApellidoPat}" />
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.sistemaapmaterno}" /></f:facet>
						<h:outputText value="#{beanAseg.cliente.cocnApellidoMat}" />
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.detcreclifena}" /></f:facet>
						<h:outputText value="#{beanAseg.cliente.cocnFeNacimiento}">
							<f:convertDateTime pattern="dd-MM-yyyy"/>
						</h:outputText>
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.timbradorfc}" /></f:facet>
						<h:outputText value="#{beanAseg.cliente.cocnRfc}" />
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.detcreclisexo}" /></f:facet>
						<h:outputText value="#{beanAseg.cliente.cocnCdSexo}" />
					</rich:column>
					<rich:column>
						<f:facet name="header"><h:outputText value="#{msgs.sistematipo} #{msgs.detcreaseg}" /></f:facet> 
						<h:outputText value="#{beanAseg.cliente.cocnCampov1}" />
					</rich:column>
			      		
		    	</rich:dataTable>
				
                
       		</a4j:outputPanel>
       		
			<rich:spacer height="10px" />
       </h:panelGrid>
   </h:form>
</rich:modalPanel>