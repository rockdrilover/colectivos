<%@ include file="include.jsp"%>
<%--<a4j:region id="planesRegion">--%> 
	<table cellpadding="2" cellspacing="0">
	
		<tbody>
													
														<tr>
															<td align="right" width="24%" height="30"><h:outputText value="Ramo :" /></td>
															<td align="left" width="76%" height="30">
																<h:selectOneMenu id="inRamo" value="#{beanExtraPrimaConfig.id.alplCdRamo}" binding="#{beanListaParametros.inRamo}" required="true">
																	<f:selectItem itemValue=""   itemLabel="Selecione un ramo" />
																	<f:selectItem itemValue="61" itemLabel="61  VIDA COLECTIVOS" />
																<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="inPoliza" />	
																</h:selectOneMenu>
																<rich:message for="inRamo" styleClass="errorMessage" />																						
														</td>
														</tr>
														<tr>
															<td align="right" width="24%" height="30"><h:outputText value="Poliza :"/></td>
															<td align="left" width="76%" height="30">
															<h:selectOneMenu id="inPoliza"  value="#{beanListaPreCarga.inPoliza}" binding="#{beanListaParametros.inPoliza}">
																<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																<f:selectItem itemValue="0" itemLabel="PRIMA UNICA"/>
																<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
															</h:selectOneMenu>
															<rich:message for="inPoliza" styleClass="errorMessage" />
															</td>
														</tr>
														
													
														
														<tr>
															
															<td align="right" height="25"> <h:outputLabel value="Producto: " style="font-weight:bold" /></td>
        													<td align="left" height="25">  <h:inputText value="#{beanExtraPrimaConfig.id.alplCdProducto}"  readonly="true"/></td>
														</tr>
														
														<tr>
															
															<td align="right" height="25"><h:outputLabel value="Plan: " style="font-weight:bold" /></td>
        													<td align="left" height="25"><h:inputText value="#{beanExtraPrimaConfig.id.alplCdPlan}"   readonly="true"/></td>
														</tr>
														
														
														<tr>
															
															<td align="right" height="25"><h:outputLabel value="Descripcion del Plan: " style="font-weight:bold" /></td>
        													<td align="left" height="25"><h:inputText value="#{beanExtraPrimaConfig.alplDePlan}"   readonly="true"/></td>
														</tr>
														
														
														<tr>
														<td align="right" width="24%" height="30"><h:outputText value="Centro de Costos :" /></td>
														<td align="left" width="76%" height="30">
															<h:inputText id="cCostos" value="#{beanExtraPrimaConfig.alplDato3}" maxlength="6" size="15"/>
															<h:message for="cCostos" styleClass="errorMessage" />																			
														</td>
														</tr>
														
														
															<tr>
															<td align="right" width="24%" height="30"></td>
															<td align="left" width="76%" height="30">
																<a4j:commandButton styleClass="boton" value="Guardar Plan" id="bPlan"
																   action="#{beanExtraPrimaConfig.altaPlan}" 
																   onclick="this.disabled=true" oncomplete="this.disabled=false" 
																   reRender="barra, listaExtraPma" 
																   title="header=[Guarda plan con extraprima]  cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>	   
															</td>
															</tr>
															
	  	     <tr>
	  	      <td>  </td>
	  	     </tr>
	  	
		</tbody>
	</table>
<%--</a4j:region>--%>