<%@include file="include.jsp"%>

<rich:modalPanel id="homologaPopup"  width="400" height="500" resizeable="true" onmaskclick="#{rich:component('homologaPopup')}.hide()" >
	<f:facet name="header">
		<h:outputText value="Alta de Homologacion" />
	</f:facet>
	<f:facet name="controls">
		<h:outputLink value="#" onclick="#{rich:component('homologaPopup')}.hide(); return false;">
			Cerrar X
		</h:outputLink>
	</f:facet>

<a4j:form id="modalForm3">

			<h:panelGrid columns="3" id="h_panel_1" styleClass="center">	
				
				<h:outputLabel value="Plan:" />    		<h:inputText id="plan"     	  value="#{beanHomologa.descPlan}" 		 	   size="25" readonly="true"/> <h:outputLabel value=""/>				
				<h:outputLabel value="Producto:"/> 		<h:inputText id="producto" 	  value="#{beanHomologa.hoppDeProducto}" 	   size="25" readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Ramo:"/>     		<h:inputText id="cobRamo"  	  value="#{beanHomologa.id.hoppCdRamo}"  	   size="4"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Id Producto:"/> 	<h:inputText id="idProducto"  value="#{beanHomologa.id.hoppProdColectivo}" size="4"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Plazo:"/>	   		<h:inputText id="plazo" 	  value="#{beanHomologa.plazo}"     		   size="4"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Id Venta:"/>	   	<h:inputText id="idVenta" 	  value="#{beanHomologa.hoppDato3}"     	   size="4"  readonly="true"/><h:outputLabel value=""/>

				<h:outputLabel value="Producto Banco:"/><h:inputText id="eMin"    value="#{beanHomologa.id.hoppProdBancoRector}" size="4" required="true"/> <rich:message for="eMin" 	 styleClass="errorMessage"/>
				<h:outputLabel value="Plan Banco:"/>	<h:inputText id="eMax"    value="#{beanHomologa.id.hoppPlanBancoRector}" size="4" required="true"/> <rich:message for="eMax" 	 styleClass="errorMessage"/>
				<h:outputLabel value="Tarifa:"/>  		<h:inputText id="tase"    value="#{beanHomologa.hoppDato1}"   			 size="4" required="true"/> <rich:message for="tase" 	 styleClass="errorMessage"/>
			
			</h:panelGrid>
			
			<h:panelGrid columns="2" id="h_panel_3" styleClass="center">
				<a4j:commandButton action="#{beanHomologa.guardaHomologa}" value="Guardar"  id="Guardar" reRender="modalForm3" />
			</h:panelGrid>
			<rich:spacer height="10px"/>
			
			<rich:dataTable id="listaHomologa" value="#{beanHomologa.listaHomologa}" columns="6" var="beanHomo" rows="10" styleClass="center">
					<f:facet name="header">
						<rich:columnGroup>
							<rich:column colspan="6"><h:outputText value="Homologacion" /></rich:column>
							<rich:column breakBefore="true"><h:outputText value="Ramo" /></rich:column>
							<rich:column><h:outputText value="Producto" /></rich:column>
							<rich:column><h:outputText value="Plan" /></rich:column>
							<rich:column><h:outputText value="Producto Banco" /></rich:column>
							<rich:column><h:outputText value="Plan Banco" /></rich:column>
							<rich:column><h:outputText value="Tarifa" /></rich:column>
						</rich:columnGroup>
					</f:facet>
					<rich:column>
						<h:outputText value="#{beanHomo.id.hoppCdRamo}" />		      		  			
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanHomo.id.hoppProdColectivo}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanHomo.id.hoppPlanColectivo}"/>
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanHomo.id.hoppProdBancoRector}"/>
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanHomo.id.hoppPlanBancoRector}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanHomo.hoppDato1}" />
					</rich:column>
			</rich:dataTable>
			
</a4j:form>			
</rich:modalPanel>

