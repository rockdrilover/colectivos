<fieldset>
	<legend>
		<a4j:outputPanel id="secRes" ajaxRendered="true">
			<span id="img_sec_resultados_m" style="display: none;"> <h:graphicImage
					value="../../images/mostrar_seccion.png" />&nbsp;
			</span>
		</a4j:outputPanel>
	</legend>

	<table class="tabla">
		<tr>
			<td width="5%">&nbsp;</td>
			<td>
				<fieldset style="border: 1px solid white">
					<legend>
						<span class="titulos_secciones"> Datos del Asegurado </span>
					</legend>
					<table>
						<tr>
							<td><h:outputText value="Num. Cliente:" />&nbsp;&nbsp;</td>
							<td><h:inputText id="numCliente"
									value="#{beanAsegurado.asegurado.copcNumCliente}" /></td>
							<td colspan="4"></td>
						</tr>
						<tr>
							<td><h:outputText value="Nombre:" /></td>
							<td><h:inputText id="nombre"
									value="#{beanAsegurado.asegurado.copcNombre}" /></td>
							<td><h:outputText value="Apellido Paterno:" /></td>
							<td><h:inputText id="ap"
									value="#{beanAsegurado.asegurado.copcApPaterno}" /></td>
							<td><h:outputText value="Apellido Materno:" /></td>
							<td><h:inputText id="am"
									value="#{beanAsegurado.asegurado.copcApMaterno}" /></td>
						</tr>
						<tr>
							<td><h:outputText value="Fecha de Nacimiento:" /></td>
							<td><rich:calendar
									value="#{beanAsegurado.asegurado.copcFeNac}" inputSize="10"
									popup="true" cellHeight="20px" id="FECHA_NACIMIENTO_ASEG"
									cellWidth="100px" datePattern="dd/MM/yyyy"
									styleClass="calendario">
								</rich:calendar></td>
							<td><h:outputText value="Sexo:" /></td>
							<td><h:selectOneMenu id="sexo"
									value="#{beanAsegurado.asegurado.copcSexo}">
									<f:selectItem itemLabel=" --- Seleccciona ---" itemValue="" />
									<f:selectItem itemLabel="    FEMENINO   " itemValue="FE" />
									<f:selectItem itemLabel="    MASCULINO  " itemValue="MA" />
									<f:selectItem itemLabel="    OTROS  " itemValue="IN" />
								</h:selectOneMenu></td>
							<td><h:outputText value="Estado Civil:" /></td>
							<td><h:selectOneMenu id="edoCivil"
									value="#{beanAsegurado.asegurado.copcNuCredito}">
									<f:selectItem itemLabel=" --- Seleccciona ---" itemValue="" />
									<f:selectItem itemLabel="  SOLTERO  " itemValue="S" />
									<f:selectItem itemLabel="  CASADO  " itemValue="C" />
									<f:selectItem itemLabel="  CONCUBINATO  " itemValue="O" />
								</h:selectOneMenu></td>
						</tr>
						<tr>
							<td><h:outputText value="RFC:" /></td>
							<td><h:inputText id="rfc"
									value="#{beanAsegurado.asegurado.copcRfc}" /></td>
							<td><h:outputText value="Curp:" /></td>
							<td><h:inputText id="curp"
									value="#{beanAsegurado.asegurado.copcDato20}" /></td>
							<td><h:outputText value="Ocupaci�n:" /></td>
							<td><h:inputText id="ocup"
									value="#{beanAsegurado.asegurado.copcDato40}" /></td>
						</tr>


						<tr>
							<td><h:outputText value="Direccion:" /></td>
							<td colspan="3"><h:inputText id="direccion" value="#{beanAsegurado.asegurado.copcCalle}" style="width: 393px" maxlength="100" required="false" /></td>
							<td><h:outputText value="C�digo Postal:" /></td>
							<td><h:inputText id="cp" value="#{beanAsegurado.asegurado.copcCp}">
									<a4j:support event="onchange" action="#{beanAsegurado.consultaCodigoPostal}" ajaxSingle="true" reRender="cmbColonia,cmbPoblacion,txtEstado">
										<f:setPropertyActionListener value="0" target="#{beanAsegurado.nTipoConsCP}" />
									</a4j:support>
								</h:inputText></td>
							</td>
						</tr>
						<tr>
							<td><h:outputText value="Colonia:" /></td>
							<td>
								<a4j:outputPanel id="cmbColonia" ajaxRendered="true">
					         		<h:selectOneMenu value="#{beanAsegurado.asegurado.copcColonia}" required="false" requiredMessage="* COLONIA ASEGURADO - Campo Requerido" title="Seleccione Colonia">
										<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
										<f:selectItems value="#{beanAsegurado.listaCmbColoniaAseg}"/>
										<a4j:support event="onchange" />
								    </h:selectOneMenu> 	
								</a4j:outputPanel>
							<td><h:outputText value="Poblaci�n/Delegaci�n:" /></td>
							<td>
								<a4j:outputPanel id="cmbPoblacion" ajaxRendered="true">
		                    		<h:selectOneMenu value="#{beanAsegurado.asegurado.copcDelmunic}" required="false" requiredMessage="* DEL/POB/MUN ASEGURADO - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
										<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
										<f:selectItems value="#{beanAsegurado.listaCmbCiudadesAseg}"/>
										<a4j:support event="onchange" />
								    </h:selectOneMenu> 	
								</a4j:outputPanel>
							<td><h:outputText value="Estado:" /></td>
							<td>
								<a4j:outputPanel id="txtEstado" ajaxRendered="true"> 
	                    			<h:inputText value="#{beanAsegurado.strDescEstadoAseg}" disabled="true" required="false" requiredMessage="* ESTADO ASEGURADO - Campo Requerido" />
	                    		</a4j:outputPanel>
							</td>
						</tr>


						<tr>
							<td><h:outputText value="Correo Electr�nico:" /></td>
							<td><h:inputText id="correoElectronico"
									value="#{beanAsegurado.asegurado.copcDato21}" /></td>
							<td><h:outputText value="Lada:" /></td>
							<td><h:inputText id="lada"
									value="#{beanAsegurado.asegurado.copcDato27}" size="3"
									maxlength="3" /></td>
							<td><h:outputText value="Telefono" /></td>
							<td><h:inputText id="tel"
									value="#{beanAsegurado.asegurado.copcTelefono}" size="8"
									maxlength="8" /></td>
						</tr>
					</table>
				</fieldset>
			</td>
			<td width="5%">&nbsp;</td>
		</tr>

		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
	</table>
</fieldset>
