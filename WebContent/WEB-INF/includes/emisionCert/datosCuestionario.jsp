<fieldset style="border: 1px solid white">
	<legend>
		<a4j:outputPanel id="secRes6" ajaxRendered="true">
			<span id="img_sec_resultados_m" style="display: none;"> <h:graphicImage
					value="../../images/mostrar_seccion.png" />&nbsp;
			</span>
			<span class="titulos_secciones">Cuestionario</span>
		</a4j:outputPanel>
	</legend>

	<table class="tabla">
		<tr>
			<td width="5%">&nbsp;</td>
			<td>
				<table>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr>
						<td width="5%">&nbsp;</td>
						<td colspan="2" align="left"><h:panelGrid
								id="panelCuestionario" columns="2">
								<h:outputText
									value="1) � Padece o ha padecido C�ncer, Leucemia o tumores de cualqier clase, enfermedades de la sangre,
																			enfermedades relacionadas con el coraz�n, infarto al Miocardio, Aparato Circulatorio, Afecciones de arterias coronarias que
																			requieran puente coronario, Hipertensi�n arterial, Trastornos neurol�gicos, Accidente Cerebrovascular, mentales, depresi�n nerviosa,
																			alteraciones de las gl�ndulas, diabetes, problemas renales, del Sistema Genitourinario, enfermedades del h�gado, p�ncreas, pulmonares,
																			asma cr�nica, tuberculosis o enfermedades graves del Aparato Digestivo ?" />
								<h:selectOneMenu id="pregunta1"
									value="#{beanAsegurado.asegurado.copcPlazoTiempo}">
									<f:selectItem itemValue="NO" itemLabel="NO" />
									<f:selectItem itemValue="SI" itemLabel="SI" />
								</h:selectOneMenu>

								<h:outputText
									value="2) � Le han diagnosticado SIDA o es portador del Virus de Inmunodeficiencia Adquirida?" />
								<h:selectOneMenu id="pregunta2"
									value="#{beanAsegurado.asegurado.copcDato5}">
									<f:selectItem itemValue="NO" itemLabel="NO" />
									<f:selectItem itemValue="SI" itemLabel="SI" />
								</h:selectOneMenu>

								<h:outputText
									value="3) � En los �ltimos dos a�os ha sido internado en un hospital y/o tiene pendiente alguna intervenci�n quir�rgica ?" />
								<h:selectOneMenu id="pregunta3"
									value="#{beanAsegurado.asegurado.copcDato4}">
									<f:selectItem itemValue="NO" itemLabel="NO" />
									<f:selectItem itemValue="SI" itemLabel="SI" />
								</h:selectOneMenu>

								<h:outputText
									value="4) � Le han extirpado alg�n �rgano o parte de �l ?" />
								<h:selectOneMenu id="pregunta4"
									value="#{beanAsegurado.asegurado.copcDato41}">
									<f:selectItem itemValue="NO" itemLabel="NO" />
									<f:selectItem itemValue="SI" itemLabel="SI" />
								</h:selectOneMenu>
							</h:panelGrid></td>
						<td width="5%">&nbsp;</td>
					</tr>
				</table>
			</td>
			<td width="5%">&nbsp;</td>
		</tr>

		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>

		<%-- 
													<tr>
														<td width="5%">&nbsp;</td>
														<td><a4j:commandButton styleClass="boton"
															id="btnConultar"
															action="#{beanEndosoDatos.consultaCertificados}"
															value="Consultar" reRender="listaCertificados,respuesta1,tipoEndoso,panelCriteriosE1,panelCriteriosE2,panelCriteriosE3,panelCriteriosE4,panelCriteriosE5,panelCriteriosE6"
															onclick="this.disabled=true"
															oncomplete="this.disabled=false; mostrar();"
															title="header=[Consulta certificados] body=[Consulta los criterios con base al filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"></a4j:commandButton>
														</td>
														<td width="5%">&nbsp;</td>
													</tr>
--%>
	</table>


</fieldset>
