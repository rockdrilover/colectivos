												<table>
													<tbody>
														<tr>
															<td width="5%">&nbsp;</td>
															<td align="center">
																
																<a4j:status for="consulta" stopText=" " id="estatusCon"
																	onstart="#{rich:component('sra')}.show(); return false;"
    																onstop="#{rich:component('sra')}.hide(); return false;">
																	
																	<%--  
																	<f:facet name="start">
																		<h:graphicImage value="/images/ajax-loader.gif" />
																	</f:facet>
																	--%>
																	
																</a4j:status> 
																
																<rich:spacer height="20px"></rich:spacer>

																<div align="center">
																	<h:outputText id="respuesta1" value="#{beanAsegurado.respuesta}" styleClass="respuesta" />
																</div>
																
																<rich:spacer height="20px"></rich:spacer>
																
																<rich:dataTable id="listaResultados" value="#{beanAsegurado.lstRespuesta}" columns="4"  var="res">
																	<f:facet name="header">
																		<rich:columnGroup>
																			<rich:column><h:outputText value="Tipo" /></rich:column>
																			<rich:column><h:outputText value="Resultado" /></rich:column>
																			<rich:column><h:outputText value="Poliza" /></rich:column>
																			<rich:column><h:outputText value="Subtotal" /></rich:column>
																		</rich:columnGroup>
																	</f:facet>
																	<rich:column>
																		<h:outputText value="#{res.tipo}" />		      		  			
																	</rich:column>
																	<rich:column>
																		<h:outputText value="#{res.descripcion}" />
																	</rich:column>
																	<rich:column>
																		<h:outputText value="#{res.poliza}" />
																	</rich:column>
																	<rich:column>
																		<h:outputText value="#{res.subtotal}" />
																	</rich:column>
																		
																</rich:dataTable>
															</td>
															<td width="5%">&nbsp;</td>
														</tr>
														<tr>
															<td colspan="3">&nbsp;</td>
														</tr>
														<tr>
															<td width="5%">&nbsp;</td>
															<td>
																<h:commandButton styleClass="boton" value="Errores" action="#{beanAsegurado.erroresEmision}"/>
															</td>
															<td width="5%">&nbsp;</td>
														</tr>
														
													</tbody>
												</table>
