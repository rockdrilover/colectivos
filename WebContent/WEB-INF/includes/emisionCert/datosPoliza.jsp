							    <table>
								<tbody>
									<tr>
										<td align="right" width="35%"><h:outputText value="Ramo:" /></td>
										<td align="left" width="50%">
											
											<h:selectOneMenu id="ramo" value="#{beanAsegurado.id.copcCdRamo}" binding="#{beanListaParametros.inRamo}">
											
												<f:selectItem itemValue="0" itemLabel="Selecione un ramo"/>
												<f:selectItem itemValue="57" itemLabel="57--VIDAEMPRESARIAL-SERFIN"/>
												<f:selectItem itemValue="58" itemLabel="58--VIDAEMPRESARIAL-SANTANDER"/>
												
												<a4j:support event="onchange" action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="comboPolizas"/>
										 	</h:selectOneMenu>
										 	
										</td>  
										<td width="15%" align="left"></td>
									</tr>
									<tr>
										<td align="right"><h:outputText value="Poliza:"/></td>
										<td align="left">
											<a4j:outputPanel id="comboPolizas" ajaxRendered="true">
											<h:selectOneMenu id="poliza" value="#{beanAsegurado.id.copcNumPoliza}" >
												<f:selectItem itemValue="0" itemLabel="<Seleccione una poliza>"/>
												<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
												<a4j:support event="onchange" />
											</h:selectOneMenu>
											</a4j:outputPanel>
										</td>
										<td align="left"></td>
									</tr>
								</tbody>
								</table>
								
								<rich:spacer height="15px"></rich:spacer>