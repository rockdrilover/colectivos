<fieldset>
	<legend>
		<a4j:outputPanel id="secciRes" ajaxRendered="true">
			<span id="img_sec_resultados_m" style="display: none;"> <h:graphicImage
					value="../../images/mostrar_seccion.png" />&nbsp;
			</span>
		</a4j:outputPanel>
	</legend>
	<table class="tabla">
		<tr>
			<td width="5%">&nbsp;</td>
			<td>
				<fieldset style="border: 1px solid white">
					<legend>
						<span class="titulos_secciones"> Datos del Beneficiario </span>
					</legend>
					<table class="tabla">
						<tr>
							<td width="5%">&nbsp;</td>
							<td colspan="2" align="left">

								<fieldset style="border: 1px solid white">
									<legend>
										<a4j:outputPanel id="secRes1" ajaxRendered="true" >
											<span id="img_sec_panelBene1_m">
												<h:graphicImage value="../../images/mostrar_seccion.png" onclick="exibirBeneficiarios(1, 1);" title="Muestra" />&nbsp;
											</span>
											<span id="img_sec_panelBene1_o" style="display: none;"> 
												<h:graphicImage value="../../images/ocultar_seccion.png" onclick="exibirBeneficiarios(0, 1);" title="Oculta" />&nbsp;
											</span>
											<span class="titulos_secciones">Beneficiario 1</span>
										</a4j:outputPanel>
									</legend>


									<table id="panelBene1" columns="2" style="display: none;">
										<tr>
											<td><h:outputText value="Nombre:" /></td>
											<td><h:inputText id="nombreBeneficiario1" value="#{beanAsegurado.beneficiario1.cobeNombre}"/>												
											</td>
											<td><h:outputText value="Apellido Paterno:" /></td>
											<td><h:inputText id="patBeneficiario1"
													value="#{beanAsegurado.beneficiario1.cobeApellidoPat}" /></td>
											<td><h:outputText value="Apellido Materno:" /></td>
											<td><h:inputText id="matBeneficiario1"
													value="#{beanAsegurado.beneficiario1.cobeApellidoMat}" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Fecha de Nacimiento:" /></td>
											<td><rich:calendar
													value="#{beanAsegurado.beneficiario1.cobeFeNacimiento}"
													inputSize="10" popup="true" cellHeight="20px"
													id="fecNacBene1" cellWidth="100px" datePattern="dd/MM/yyyy"
													styleClass="calendario">
												</rich:calendar></td>
											<td><h:outputText value="Parentesco:" /></td>
											<td>
								         		<h:selectOneMenu value="#{beanAsegurado.beneficiario1.cobeRelacionBenef}" required="false" requiredMessage="* PARENTESCO - Campo Requerido" title="Seleccione Parentesco">
													<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
													<f:selectItems value="#{beanAsegurado.listaCmbParentescoBen1}"/>
											    </h:selectOneMenu>
											</td>
											<td><h:outputText value="Porcentaje:" /></td>
											<td><h:inputText id="porcentaje1"
													value="#{beanAsegurado.beneficiario1.cobePoParticipacion}"
													maxlength="13" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Direcci�n:" /></td>
											<td colspan="3"><h:inputText id="CALLE_Y_NUMERO1" value="#{beanAsegurado.beneficiario1.cobeVcampo4}" style="width: 393px" maxlength="100" /></td>
											<td><h:outputText value="C�digo Postal:" /></td>
											<td><h:inputText id="CODIGO_POSTAL1" value="#{beanAsegurado.beneficiario1.cobeNcampo1}" maxlength="5">
													<a4j:support event="onchange" action="#{beanAsegurado.consultaCodigoPostal}" ajaxSingle="true" reRender="COLONIA1,CIUDAD1,ESTADO1">
														<f:setPropertyActionListener value="1" target="#{beanAsegurado.nTipoConsCP}" />
													</a4j:support>
												</h:inputText></td>
										</tr>
										<tr>
											<td><h:outputText value="Colonia:" /></td>
											<td>
												<a4j:outputPanel id="COLONIA1" ajaxRendered="true">
									         		<h:selectOneMenu value="#{beanAsegurado.beneficiario1.cobeVcampo3}" required="false" requiredMessage="* COLONIA - Campo Requerido" title="Seleccione Colonia">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbColoniaBen1}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Ciudad:" /></td>
											<td>
												<a4j:outputPanel id="CIUDAD1" ajaxRendered="true">
						                    		<h:selectOneMenu value="#{beanAsegurado.beneficiario1.cobeVcampo2}" required="false" requiredMessage="* DEL/POB/MUN - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbCiudadesBen1}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Estado:" /></td>
											<td>
												<a4j:outputPanel id="ESTADO1" ajaxRendered="true"> 
					                    			<h:inputText value="#{beanAsegurado.strDescEstadoBen1}" disabled="true" required="false" requiredMessage="* ESTADO - Campo Requerido" />
					                    		</a4j:outputPanel>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
							<td width="5%">&nbsp;</td>
						</tr>

						<tr>
							<td width="5%">&nbsp;</td>
							<td colspan="2" align="left">

								<fieldset style="border: 1px solid white">
									<legend>
										<a4j:outputPanel id="secRes2" ajaxRendered="true">
											<span id="img_sec_panelBene2_m" >
												<h:graphicImage value="../../images/mostrar_seccion.png"
													onclick="exibirBeneficiarios(1, 2);" title="Muestra" />&nbsp;
											</span>
											<span id="img_sec_panelBene2_o" style="display: none;"> <h:graphicImage
													value="../../images/ocultar_seccion.png"
													onclick="exibirBeneficiarios(0, 2);" title="Oculta" />&nbsp;
											</span>
											<span class="titulos_secciones">Beneficiario 2</span>
										</a4j:outputPanel>
									</legend>
									<table id="panelBene2" columns="2" style="display: none;">
										<tr>
											<td><h:outputText value="Nombre:" /></td>
											<td><h:inputText id="nombreBeneficiario2"
													value="#{beanAsegurado.beneficiario2.cobeNombre}" /></td>
											<td><h:outputText value="Apellido Paterno:" /></td>
											<td><h:inputText id="patBeneficiario2"
													value="#{beanAsegurado.beneficiario2.cobeApellidoPat}" /></td>
											<td><h:outputText value="Apellido Materno:" /></td>
											<td><h:inputText id="matBeneficiario2"
													value="#{beanAsegurado.beneficiario2.cobeApellidoMat}" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Fecha de Nacimiento:" /></td>
											<td><rich:calendar
													value="#{beanAsegurado.beneficiario2.cobeFeNacimiento}"
													inputSize="10" popup="true" cellHeight="20px"
													id="fecNacBene2" cellWidth="100px" datePattern="dd/MM/yyyy"
													styleClass="calendario">
												</rich:calendar></td>
											<td><h:outputText value="Parentesco:" /></td>
											<td>
								         		<h:selectOneMenu value="#{beanAsegurado.beneficiario2.cobeRelacionBenef}" required="false" requiredMessage="* PARENTESCO - Campo Requerido" title="Seleccione Parentesco">
													<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
													<f:selectItems value="#{beanAsegurado.listaCmbParentescoBen2}"/>
											    </h:selectOneMenu>
											</td>
											<td><h:outputText value="Porcentaje:" /></td>
											<td><h:inputText id="porcentaje2"
													value="#{beanAsegurado.beneficiario2.cobePoParticipacion}"
													maxlength="13" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Direcci�n:" /></td>
											<td colspan="3"><h:inputText id="CALLE_Y_NUMERO2"
													value="#{beanAsegurado.beneficiario2.cobeVcampo4}"
													style="width: 393px" maxlength="100" /></td>
											<td><h:outputText value="C�digo Postal:" /></td>
											<td><h:inputText id="CODIGO_POSTAL2" value="#{beanAsegurado.beneficiario2.cobeNcampo1}" maxlength="5">
													<a4j:support event="onchange" action="#{beanAsegurado.consultaCodigoPostal}" ajaxSingle="true" reRender="COLONIA2,CIUDAD2,ESTADO2">
														<f:setPropertyActionListener value="2" target="#{beanAsegurado.nTipoConsCP}" />
													</a4j:support>
												</h:inputText></td>
										</tr>
										<tr>
											<td><h:outputText value="Colonia:" /></td>
											<td>
												<a4j:outputPanel id="COLONIA2" ajaxRendered="true">
									         		<h:selectOneMenu value="#{beanAsegurado.beneficiario2.cobeVcampo3}" required="false" requiredMessage="* COLONIA - Campo Requerido" title="Seleccione Colonia">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbColoniaBen2}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Ciudad:" /></td>
											<td>
												<a4j:outputPanel id="CIUDAD2" ajaxRendered="true">
						                    		<h:selectOneMenu value="#{beanAsegurado.beneficiario2.cobeVcampo2}" required="false" requiredMessage="* DEL/POB/MUN - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbCiudadesBen2}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Estado:" /></td>
											<td>
												<a4j:outputPanel id="ESTADO2" ajaxRendered="true"> 
					                    			<h:inputText value="#{beanAsegurado.strDescEstadoBen2}" disabled="true" required="false" requiredMessage="* ESTADO - Campo Requerido" />
					                    		</a4j:outputPanel>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
							<td width="5%">&nbsp;</td>
						</tr>

						<tr>
							<td width="5%">&nbsp;</td>
							<td colspan="2" align="left">

								<fieldset style="border: 1px solid white">
									<legend>
										<a4j:outputPanel id="secRes3" ajaxRendered="true">
											<span id="img_sec_panelBene3_m" >
												<h:graphicImage value="../../images/mostrar_seccion.png"
													onclick="exibirBeneficiarios(1, 3);" title="Muestra" />&nbsp;
											</span>
											<span id="img_sec_panelBene3_o" style="display: none;"> <h:graphicImage
													value="../../images/ocultar_seccion.png"
													onclick="exibirBeneficiarios(0, 3);" title="Oculta" />&nbsp;
											</span>
											<span class="titulos_secciones">Beneficiario 3</span>
										</a4j:outputPanel>
									</legend>
									<table id="panelBene3" columns="2" style="display: none;">
										<tr>
											<td><h:outputText value="Nombre:" /></td>
											<td><h:inputText id="nombreBeneficiario3"
													value="#{beanAsegurado.beneficiario3.cobeNombre}" /></td>
											<td><h:outputText value="Apellido Paterno:" /></td>
											<td><h:inputText id="patBeneficiario3"
													value="#{beanAsegurado.beneficiario3.cobeApellidoPat}" /></td>
											<td><h:outputText value="Apellido Materno:" /></td>
											<td><h:inputText id="matBeneficiario3"
													value="#{beanAsegurado.beneficiario3.cobeApellidoMat}" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Fecha de Nacimiento:" /></td>
											<td><rich:calendar
													value="#{beanAsegurado.beneficiario3.cobeFeNacimiento}"
													inputSize="10" popup="true" cellHeight="20px"
													id="fecNacBene3" cellWidth="100px" datePattern="dd/MM/yyyy"
													styleClass="calendario">
												</rich:calendar></td>
											<td><h:outputText value="Parentesco:" /></td>
											<td>
								         		<h:selectOneMenu value="#{beanAsegurado.beneficiario3.cobeRelacionBenef}" required="false" requiredMessage="* PARENTESCO - Campo Requerido" title="Seleccione Parentesco">
													<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
													<f:selectItems value="#{beanAsegurado.listaCmbParentescoBen3}"/>
											    </h:selectOneMenu>
											</td>
											<td><h:outputText value="Porcentaje:" /></td>
											<td><h:inputText id="porcentaje3"
													value="#{beanAsegurado.beneficiario3.cobePoParticipacion}"
													maxlength="13" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Direcci�n:" /></td>
											<td colspan="3"><h:inputText id="CALLE_Y_NUMERO3"
													value="#{beanAsegurado.beneficiario3.cobeVcampo4}"
													style="width: 393px" maxlength="100" /></td>
											<td><h:outputText value="C�digo Postal:" /></td>
											<td><h:inputText id="CODIGO_POSTAL3" value="#{beanAsegurado.beneficiario3.cobeNcampo1}" maxlength="5">
													<a4j:support event="onchange" action="#{beanAsegurado.consultaCodigoPostal}" ajaxSingle="true" reRender="COLONIA3,CIUDAD3,ESTADO3">
														<f:setPropertyActionListener value="3" target="#{beanAsegurado.nTipoConsCP}" />
													</a4j:support>
												</h:inputText></td>
										</tr>
										<tr>
											<td><h:outputText value="Colonia:" /></td>
											<td>
												<a4j:outputPanel id="COLONIA3" ajaxRendered="true">
									         		<h:selectOneMenu value="#{beanAsegurado.beneficiario3.cobeVcampo3}" required="false" requiredMessage="* COLONIA - Campo Requerido" title="Seleccione Colonia">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbColoniaBen3}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Ciudad:" /></td>
											<td>
												<a4j:outputPanel id="CIUDAD3" ajaxRendered="true">
						                    		<h:selectOneMenu value="#{beanAsegurado.beneficiario3.cobeVcampo2}" required="false" requiredMessage="* DEL/POB/MUN - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbCiudadesBen3}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Estado:" /></td>
											<td>
												<a4j:outputPanel id="ESTADO3" ajaxRendered="true"> 
					                    			<h:inputText value="#{beanAsegurado.strDescEstadoBen3}" disabled="true" required="false" requiredMessage="* ESTADO - Campo Requerido" />
					                    		</a4j:outputPanel>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
							<td width="5%">&nbsp;</td>
						</tr>

						<tr>
							<td width="5%">&nbsp;</td>
							<td colspan="2" align="left">

								<fieldset style="border: 1px solid white">
									<legend>
										<a4j:outputPanel id="secRes4" ajaxRendered="true">
											<span id="img_sec_panelBene4_m" >
												<h:graphicImage value="../../images/mostrar_seccion.png"
													onclick="exibirBeneficiarios(1, 4);" title="Muestra" />&nbsp;
											</span>
											<span id="img_sec_panelBene4_o" style="display: none;"> <h:graphicImage
													value="../../images/ocultar_seccion.png"
													onclick="exibirBeneficiarios(0, 4);" title="Oculta" />&nbsp;
											</span>
											<span class="titulos_secciones">Beneficiario 4</span>
										</a4j:outputPanel>
									</legend>
									<table id="panelBene4" columns="2" style="display: none;">
										<tr>
											<td><h:outputText value="Nombre:" /></td>
											<td><h:inputText id="nombreBeneficiario4"
													value="#{beanAsegurado.beneficiario4.cobeNombre}" /></td>
											<td><h:outputText value="Apellido Paterno:" /></td>
											<td><h:inputText id="patBeneficiario4"
													value="#{beanAsegurado.beneficiario4.cobeApellidoPat}" /></td>
											<td><h:outputText value="Apellido Materno:" /></td>
											<td><h:inputText id="matBeneficiario4"
													value="#{beanAsegurado.beneficiario4.cobeApellidoMat}" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Fecha de Nacimiento:" /></td>
											<td><rich:calendar
													value="#{beanAsegurado.beneficiario4.cobeFeNacimiento}"
													inputSize="10" popup="true" cellHeight="20px"
													id="fecNacBene4" cellWidth="100px" datePattern="dd/MM/yyyy"
													styleClass="calendario">
												</rich:calendar></td>
											<td><h:outputText value="Parentesco:" /></td>
											<td>
								         		<h:selectOneMenu value="#{beanAsegurado.beneficiario4.cobeRelacionBenef}" required="false" requiredMessage="* PARENTESCO - Campo Requerido" title="Seleccione Parentesco">
													<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
													<f:selectItems value="#{beanAsegurado.listaCmbParentescoBen4}"/>
											    </h:selectOneMenu>
											</td>
											<td><h:outputText value="Porcentaje:" /></td>
											<td><h:inputText id="porcentaje4"
													value="#{beanAsegurado.beneficiario4.cobePoParticipacion}"
													maxlength="13" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Direcci�n:" /></td>
											<td colspan="3"><h:inputText id="CALLE_Y_NUMERO4"
													value="#{beanAsegurado.beneficiario4.cobeVcampo4}"
													style="width: 393px" maxlength="100" /></td>
											<td><h:outputText value="C�digo Postal:" /></td>
											<td><h:inputText id="CODIGO_POSTAL4" value="#{beanAsegurado.beneficiario4.cobeNcampo1}" maxlength="5">
													<a4j:support event="onchange" action="#{beanAsegurado.consultaCodigoPostal}" ajaxSingle="true" reRender="COLONIA4,CIUDAD4,ESTADO4">
														<f:setPropertyActionListener value="4" target="#{beanAsegurado.nTipoConsCP}" />
													</a4j:support>
												</h:inputText></td>
										</tr>
										<tr>
											<td><h:outputText value="Colonia:" /></td>
											<td>
												<a4j:outputPanel id="COLONIA4" ajaxRendered="true">
									         		<h:selectOneMenu value="#{beanAsegurado.beneficiario4.cobeVcampo3}" required="false" requiredMessage="* COLONIA - Campo Requerido" title="Seleccione Colonia">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbColoniaBen4}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Ciudad:" /></td>
											<td>
												<a4j:outputPanel id="CIUDAD4" ajaxRendered="true">
						                    		<h:selectOneMenu value="#{beanAsegurado.beneficiario4.cobeVcampo2}" required="false" requiredMessage="* DEL/POB/MUN - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbCiudadesBen4}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Estado:" /></td>
											<td>
												<a4j:outputPanel id="ESTADO4" ajaxRendered="true"> 
					                    			<h:inputText value="#{beanAsegurado.strDescEstadoBen4}" disabled="true" required="false" requiredMessage="* ESTADO - Campo Requerido" />
					                    		</a4j:outputPanel>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
							<td width="5%">&nbsp;</td>
						</tr>

						<tr>
							<td width="5%">&nbsp;</td>
							<td colspan="2" align="left">

								<fieldset style="border: 1px solid white">
									<legend>
										<a4j:outputPanel id="secRes5" ajaxRendered="true">
											<span id="img_sec_panelBene5_m">
												<h:graphicImage value="../../images/mostrar_seccion.png"
													onclick="exibirBeneficiarios(1, 5);" title="Muestra" />&nbsp;
											</span>
											<span id="img_sec_panelBene5_o" style="display: none;"> <h:graphicImage
													value="../../images/ocultar_seccion.png"
													onclick="exibirBeneficiarios(0, 5);" title="Oculta" />&nbsp;
											</span>
											<span class="titulos_secciones">Beneficiario 5</span>
										</a4j:outputPanel>
									</legend>
									<table id="panelBene5" columns="2" style="display: none;">
										<tr>
											<td><h:outputText value="Nombre:" /></td>
											<td><h:inputText id="nombreBeneficiario5"
													value="#{beanAsegurado.beneficiario5.cobeNombre}" /></td>
											<td><h:outputText value="Apellido Paterno:" /></td>
											<td><h:inputText id="patBeneficiario5"
													value="#{beanAsegurado.beneficiario5.cobeApellidoPat}" /></td>
											<td><h:outputText value="Apellido Materno:" /></td>
											<td><h:inputText id="matBeneficiario5"
													value="#{beanAsegurado.beneficiario5.cobeApellidoMat}" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Fecha de Nacimiento:" /></td>
											<td><rich:calendar
													value="#{beanAsegurado.beneficiario5.cobeFeNacimiento}"
													inputSize="10" popup="true" cellHeight="20px"
													id="fecNacBene5" cellWidth="100px" datePattern="dd/MM/yyyy"
													styleClass="calendario">
												</rich:calendar></td>
											<td><h:outputText value="Parentesco:" /></td>
											<td>
								         		<h:selectOneMenu value="#{beanAsegurado.beneficiario5.cobeRelacionBenef}" required="false" requiredMessage="* PARENTESCO - Campo Requerido" title="Seleccione Parentesco">
													<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
													<f:selectItems value="#{beanAsegurado.listaCmbParentescoBen5}"/>
											    </h:selectOneMenu>
											</td>
											<td><h:outputText value="Porcentaje:" /></td>
											<td><h:inputText id="porcentaje5"
													value="#{beanAsegurado.beneficiario5.cobePoParticipacion}"
													maxlength="13" /></td>
										</tr>
										<tr>
											<td><h:outputText value="Direcci�n:" /></td>
											<td colspan="3"><h:inputText id="CALLE_Y_NUMERO5"
													value="#{beanAsegurado.beneficiario5.cobeVcampo4}"
													style="width: 393px" maxlength="100" /></td>
											<td><h:outputText value="C�digo Postal:" /></td>
											<td><h:inputText id="CODIGO_POSTAL5" value="#{beanAsegurado.beneficiario5.cobeNcampo1}" maxlength="5">
													<a4j:support event="onchange" action="#{beanAsegurado.consultaCodigoPostal}" ajaxSingle="true" reRender="COLONIA5,CIUDAD5,ESTADO5">
														<f:setPropertyActionListener value="5" target="#{beanAsegurado.nTipoConsCP}" />
													</a4j:support>
												</h:inputText></td>
										</tr>
										<tr>
											<td><h:outputText value="Colonia:" /></td>
											<td>
												<a4j:outputPanel id="COLONIA5" ajaxRendered="true">
									         		<h:selectOneMenu value="#{beanAsegurado.beneficiario5.cobeVcampo3}" required="false" requiredMessage="* COLONIA - Campo Requerido" title="Seleccione Colonia">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbColoniaBen5}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Ciudad:" /></td>
											<td>
												<a4j:outputPanel id="CIUDAD5" ajaxRendered="true">
						                    		<h:selectOneMenu value="#{beanAsegurado.beneficiario5.cobeVcampo2}" required="false" requiredMessage="* DEL/POB/MUN - Campo Requerido" title="Seleccione Deleg/Mun/Pobl">
														<f:selectItem itemValue="0" itemLabel=" --- Seleccione --- "/>
														<f:selectItems value="#{beanAsegurado.listaCmbCiudadesBen5}"/>
														<a4j:support event="onchange" />
												    </h:selectOneMenu> 	
												</a4j:outputPanel>
											</td>
											<td><h:outputText value="Estado:" /></td>
											<td>
												<a4j:outputPanel id="ESTADO5" ajaxRendered="true"> 
					                    			<h:inputText value="#{beanAsegurado.strDescEstadoBen5}" disabled="true" required="false" requiredMessage="* ESTADO - Campo Requerido" />
					                    		</a4j:outputPanel>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
							<td width="5%">&nbsp;</td>
						</tr>

						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>

						<tr>
							<td width="5%">&nbsp;</td>
							<td><a4j:commandButton styleClass="boton" id="btnConultar"
									action="#{beanAsegurado.emitir}" value="Emitir"
									reRender="listaCertificados,respuesta,respuesta1,tipoEndoso,listaResultados"
									onclick="this.disabled=true" oncomplete="this.disabled=false;"
									title="header=[Consulta certificados] body=[Consulta los criterios con base al filtro proporcionado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"></a4j:commandButton>
							</td>
							<td width="5%">&nbsp;</td>
						</tr>
					</table>
				</fieldset>
			</td>
			<td width="5%">&nbsp;</td>
		</tr>

		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
	</table>
</fieldset>