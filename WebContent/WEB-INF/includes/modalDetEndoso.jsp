<%@include file="include.jsp"%>

<rich:modalPanel id="detPanel" autosized="true" width="550">
   	<f:facet name="header"><h:outputText value="Detalle Endoso" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
            <rich:componentControl for="detPanel" attachTo="hidelink" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>
    
    <h:form id="detalleEndoso">
        <rich:messages style="color:red;"></rich:messages>
        <h:panelGrid columns="1">
            <a4j:outputPanel ajaxRendered="true">
                <h:panelGrid columns="6">
                	
                	<h:outputText value="Folio: "/>
                	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaCampov1}" styleClass="textoModal"/>
                	<h:outputText value="      "/>
                	<h:outputText value="      "/>
                	<h:outputText value="Tipo: "/>
                	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaCampov3}" styleClass="textoModal"/>
		
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
                	
                	<rich:separator height="2" lineType="double" />
					<rich:separator height="2" lineType="double"/>
					<rich:separator height="2" lineType="double" />
					<rich:separator height="2" lineType="double"/>
					<rich:separator height="2" lineType="double" />
					<rich:separator height="2" lineType="double"/>
			                	
                	<h:outputText value="#{msgs.endosodatosdatos} "/>
                	<h:outputText value="#{msgs.endosodatosnue} "/>
                	<h:outputText value="      "/>
                	<h:outputText value="      "/>
                	<h:outputText value="#{msgs.endosodatosdatos} "/>
                	<h:outputText value="#{msgs.endosodatosante} "/>
                	
                	<rich:separator height="2" lineType="double" />
					<rich:separator height="2" lineType="double"/>
					<rich:separator height="2" lineType="double" />
					<rich:separator height="2" lineType="double"/>
					<rich:separator height="2" lineType="double" />
					<rich:separator height="2" lineType="double"/>
		
					<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 1}">
						<h:outputText value="Nombre: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaNombreNvo}" styleClass="textoModal"/>
	                 	<h:outputText value="      "/>
	                 	<h:outputText value="      "/>	                    	
	                 	<h:outputText value="Nombre: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaNombreAnt}" styleClass="textoModal"/>
	                 	
	                 	<h:outputText value="Apellido Paterno: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaApNvo}" styleClass="textoModal"/>
	                 	<h:outputText value="      "/>
	                 	<h:outputText value="      "/>
						<h:outputText value="Apellido Paterno: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaApAnt}" styleClass="textoModal"/>
			
						<h:outputText value="Apellido Materno: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaAmNvo}" styleClass="textoModal"/>
	                 	<h:outputText value="      "/>
	                 	<h:outputText value="      "/>
    					<h:outputText value="Apellido Materno: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaAmAnt}" styleClass="textoModal"/>
					</c:if>
					
					<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 7}">
						<h:outputText value="Nombre: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaNombreNvo}" styleClass="textoModal"/>
	                 	<h:outputText value="      "/>
	                 	<h:outputText value="      "/>	                    	
	                 	<h:outputText value="Nombre: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaNombreAnt}" styleClass="textoModal"/>
	                 	
	                 	<h:outputText value="Apellido Paterno: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaApNvo}" styleClass="textoModal"/>
	                 	<h:outputText value="      "/>
	                 	<h:outputText value="      "/>
						<h:outputText value="Apellido Paterno: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaApAnt}" styleClass="textoModal"/>
			
						<h:outputText value="Apellido Materno: "/>
	                 	<h:outputText value="#{beanEndososPendientes.seleccionado.cedaAmNvo}" styleClass="textoModal"/>
	                 	<h:outputText value="      "/>
	                 	<h:outputText value="      "/>
    					<h:outputText value="Apellido Materno: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaAmAnt}" styleClass="textoModal"/>
						
						<h:outputText value="Parentesco: "/>
						<h:outputText value="#{beanEndososPendientes.seleccionado.cedaParentescoNvo}" styleClass="textoModal"/>
						<h:outputText value="      "/>
                 		<h:outputText value="      "/>	                    	
						<h:outputText value="Parentesco: "/>
						<h:outputText value="#{beanEndososPendientes.seleccionado.cedaParentescoAnt}" styleClass="textoModal"/>
					                    	
                 		<h:outputText value="% Participacion: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaPjParticipaNvo}" styleClass="textoModal"/>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="% Participacion: "/>
               			<h:outputText value="#{beanEndososPendientes.seleccionado.cedaPjParticipaAnt}" styleClass="textoModal"/>
					</c:if>
					
                	
                	<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 3}">
                 		<h:outputText value="Fecha Nacimiento: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFechaNvo}" styleClass="textoModal">
                 		<f:convertDateTime pattern="dd-MM-yyyy"/>
                 		</h:outputText>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="Fecha Nacimiento: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFechaAnt}" styleClass="textoModal">
                 			<f:convertDateTime pattern="dd-MM-yyyy"/>
                 		</h:outputText>
                	</c:if>
                	
                	<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 4}">
                 		<h:outputText value="RFC: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaRfcNvo}" styleClass="textoModal"/>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="RFC: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaRfcAnt}" styleClass="textoModal"/>
                 	</c:if>
                 
                 	<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 2}">
                 		<h:outputText value="Direccion: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaDireccionNvo}" styleClass="textoModal"/>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="Direccion: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaDireccionAnt}" styleClass="textoModal"/>
					</c:if>
		
					<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 5}">		                    	
                 		<h:outputText value="Sexo: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedasexoNvo}" styleClass="textoModal"/>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="Sexo: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedasexoAnt}" styleClass="textoModal"/>
                	</c:if>
                	
                	<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 6}">
                 		<h:outputText value="Correo: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaCorreoNvo}" styleClass="textoModal"/>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="Correo: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaCorreoAnt}" styleClass="textoModal"/>
                 	</c:if>
                 
                 	<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 10}">
                 		<h:outputText value="Suma Asegurada: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaSumaNvo}" styleClass="textoModal"/>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="Suma Asegurada: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaSumaAnt}" styleClass="textoModal"/>
                	</c:if>
                	
                	<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 9}">
                 		<h:outputText value="Fecha Suscripcion: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFeSusNvo}" styleClass="textoModal">
	                 		<f:convertDateTime pattern="dd-MM-yyyy"/>
    	             	</h:outputText>
        	         	<h:outputText value="      "/>
            	     	<h:outputText value="      "/>
						<h:outputText value="Fecha Suscripcion: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFeSusAnt}" styleClass="textoModal">
                 			<f:convertDateTime pattern="dd-MM-yyyy"/>
                 		</h:outputText>
                 	</c:if>
                 
                 	<c:if test="${beanEndososPendientes.seleccionado.cedatpEndoso == 8}">
                 		<h:outputText value="Fecha Desde: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFeDesNvo}" styleClass="textoModal">
                 			<f:convertDateTime pattern="dd-MM-yyyy"/>
                 		</h:outputText>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="Fecha Desde: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFeDesAnt}" styleClass="textoModal">
	                 		<f:convertDateTime pattern="dd-MM-yyyy"/>
    	             	</h:outputText>
                 	
                 		<h:outputText value="Fecha Hasta: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFeHasNvo}" styleClass="textoModal">
                 			<f:convertDateTime pattern="dd-MM-yyyy"/>
                 		</h:outputText>
                 		<h:outputText value="      "/>
                 		<h:outputText value="      "/>
						<h:outputText value="Fecha Hasta: "/>
                 		<h:outputText value="#{beanEndososPendientes.seleccionado.cedaFeHasAnt}" styleClass="textoModal">
                 			<f:convertDateTime pattern="dd-MM-yyyy"/>
                 		</h:outputText>
                 	</c:if>
                 
                 	<a4j:commandButton 
              			value="Autorizar"
                  		action="#{beanEndososPendientes.autorizar}" reRender="mensaje,regTabla"	   
                  		onclick="#{rich:component('detPanel')}.hide();bloqueaPantalla();"	                    
	    				oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } desbloqueaPantalla();" />
                	<h:outputText value="      "/>
                	<h:outputText value="      "/>
                	<h:outputText value="      "/>
                	<a4j:commandButton 
              			value="Rechazar"
                  		action="#{beanEndososPendientes.rechazar}" reRender="mensaje,regTabla"	   
                  		onclick="#{rich:component('detPanel')}.hide();bloqueaPantalla();"	                    
	    				oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('detPanel')}.hide(); } else { #{rich:component('detPanel')}.show(); } desbloqueaPantalla();" />
                	<h:outputText value="      "/>
                </h:panelGrid>
            </a4j:outputPanel>	                	              
        </h:panelGrid>
    </h:form>
</rich:modalPanel>