<%@ include file="include.jsp"%> 
<%-- <a4j:region id="paramEndoso"> --%>
<table cellpadding="2" cellspacing="0">
	<tbody>
										<td colspan="2" >
								    				<rich:panel styleClass="paneles">
								              	 <f:facet name="header"><h:outputText value="Parametrizacion de Endosos:" /></f:facet>
								             	
								              	    <h:outputText value="Endoso" style="direction: ltr; text-align: right"/>
								               		
										      		<h:selectBooleanCheckbox id="opcion60" 
										        				value="#{beanParamOperacion.nEndosoChk}"
																title="Selecciona parametrizar Endoso" 
																style="direction: ltr; text-align: right"
																binding="#{beanParamOperacion.endosoChk}">
													  </span> <H: outputLabel value = "# {messages.rememberMeLabel}" for = "recordar" />
													</h:selectBooleanCheckbox>
																
								               	 <h:panelGrid columns="2" columnClasses="filtroR, filtroL" >									              	 			
								               		   <tr>
								               				<td>
															<h:outputLabel value="Endoso en Emision:"></h:outputLabel>
															</td>
															<td>
															<h:selectOneRadio id="endEmi"
																 value="#{beanParamOperacion.copaVvalor3}"  
																 title="Selecciona una opci�n" 
																 layout="lineDirection">
																 <f:selectItem itemLabel="Permitir Endoso en Emision " itemValue="100" />
																 <f:selectItem itemLabel="Sin permiso de Endoso en Emision " itemValue="0" />
															</h:selectOneRadio>
															</td>
														</tr>
														<tr>
															<td>
															<h:outputLabel value="Endoso en Renovacion:"></h:outputLabel>
															</td>
															<td>
															<h:selectOneRadio id="endRen"
															    value="#{beanParamOperacion.copaVvalor4}"  
																 title="Selecciona una opci�n" 
																 layout="lineDirection">
																 <f:selectItem id="item3" itemLabel="Permitir endoso en Renovacion" itemValue="300" />
																 <f:selectItem id="item4" itemLabel="No permite Endoso en Renovacion" itemValue="0" />
															</h:selectOneRadio>
															</td>
														</tr>
															
														<tr>
															<td>
															<h:outputLabel value="Endoso de cancelacion:"></h:outputLabel>
															</td>
															<td>
															<h:selectOneRadio id="endCan"
																 value="#{beanParamOperacion.copaVvalor5}"  
																 title="Selecciona una opci�n" 
																 layout="lineDirection">
																 <f:selectItem id="item5" itemLabel="Permitir endoso de Cancelacion" itemValue="200" />
																 <f:selectItem id="item6" itemLabel="No permitir endoso de Cancelacion" itemValue="0" />
															</h:selectOneRadio>
															</td>
														</tr>	
															
														<tr>
															<td>
															<h:outputLabel value="Tipo de calculo:"></h:outputLabel>
															</td>
															<td>
															<h:selectOneRadio id="tipoCal"
																 value="#{beanParamOperacion.copaNvalor5}"  
																 title="Selecciona una opci�n" 
																 layout="lineDirection"
																	>
																 <f:selectItem id="item7" itemLabel="Recibo directo " itemValue="1" />
																 <f:selectItem id="item8" itemLabel="Calculo sobre anualidad " itemValue="2" />
															</h:selectOneRadio>
															</td>
														</tr>				
																						        																
											        	</h:panelGrid>
											        	
											       
											        	
								               		</rich:panel>
												</td>

	</tbody>
</table>
<%--</a4j:region> --%>    
