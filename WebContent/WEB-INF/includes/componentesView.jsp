<%@include file="include.jsp"%>

<rich:modalPanel id="componentesPopup"  width="400" height="500" resizeable="true" onmaskclick="#{rich:component('componentesPopup')}.hide()" >
	<f:facet name="header">
		<h:outputText value="Alta de Componentes" />
	</f:facet>
	<f:facet name="controls">
		<h:outputLink value="#" onclick="#{rich:component('componentesPopup')}.hide(); return false;">
			Cerrar X
		</h:outputLink>
	</f:facet>

<a4j:form id="modalForm2" ajaxSubmit="true">
			
			<rich:spacer height="20px"/>
			
			<h:panelGrid columns="3" id="panel_1" styleClass="center">	
				
				<h:outputLabel value="Plan:" />     <h:inputText id="plan"     value="#{beanComponentes.descPlan}" 			    size="25" readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Ramo:"/>      <h:inputText id="cobRamo"  value="#{beanComponentes.id.coctCarpCdRamo}"     size="4"  readonly="true"/><h:outputLabel value=""/>				
				<h:outputLabel value="P�liza:"/>    <h:inputText id="poliza"   value="#{beanComponentes.id.coctCapoNuPoliza}"   size="8"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Producto:"/>  <h:inputText id="producto" value="#{beanComponentes.id.coctCapuCdProducto}" size="4"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Plazo:"/>	    <h:inputText id="plazo"    value="#{beanComponentes.id.coctCapbCdPlan}"     size="4"  readonly="true"/><h:outputLabel value=""/>
			
			</h:panelGrid>

			<rich:spacer height="20px"/>
			
			<h:panelGrid columns="3" id="panel_2" styleClass="center">
				<h:outputLabel value="Componente:"/>

				<h:selectOneMenu id="comboComp" value="#{beanComponentes.valComboCobertura}" required="true">
					<f:selectItem itemValue="" itemLabel="Seleccione un componente"/>
					<f:selectItems value="#{beanComponentes.listaComboComponentes}"/>
				</h:selectOneMenu>

				<rich:message for="comboComp" styleClass="errorMessage"/>
				
				<h:outputLabel value="Fecha Desde:"/><rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy" required="true" value="#{beanComponentes.coctFeDesde}" inputSize="8"/> <rich:message for="fechaInicial" styleClass="errorMessage"/>
				<h:outputLabel value="Fecha Hasta:"/><rich:calendar id="fechaFinal"   datePattern="dd/MM/yyyy" required="true" value="#{beanComponentes.coctFeHasta}" inputSize="8" /><rich:message for="fechaFinal"   styleClass="errorMessage"/>
				<h:outputLabel value="Tase de Riesgo:"/><h:inputText id="tase" value="#{beanComponentes.coctTaComponente}" size="4" required="true"/> <rich:message for="tase" 	 styleClass="errorMessage"/>
			</h:panelGrid>

			<rich:spacer height="20px"/>

			<h:panelGrid columns="2" id="panel_6" styleClass="center">
				<a4j:commandButton action="#{beanComponentes.guardaComponentes}" value="Guardar"  type="submit" id="Guardar" reRender="modalForm2,listaComponentes"/>
			</h:panelGrid>
			<rich:spacer height="10px"/>
			
			<rich:dataTable id="listaComponentes" value="#{beanComponentes.listaComponentes}" columns="4" var="beanComponente" rows="10" styleClass="center">
					<f:facet name="header">
						<rich:columnGroup>
							<rich:column colspan="4"><h:outputText value="Lista de Componentes" /></rich:column>
							<rich:column breakBefore="true"><h:outputText value="Componente" /></rich:column>
							<rich:column><h:outputText value="Fecha Inicio" /></rich:column>
							<rich:column><h:outputText value="Fecha Fin" /></rich:column>
							<rich:column><h:outputText value="Tase" /></rich:column>
						</rich:columnGroup>
					</f:facet>
					<rich:column>
						<h:outputText value="#{beanComponente.id.coctCappCdComponente}" />		      		  			
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanComponente.coctFeDesde}"><f:convertDateTime type="date" pattern="dd/MM/yyyy"/></h:outputText>
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanComponente.coctFeHasta}"><f:convertDateTime type="date" pattern="dd/MM/yyyy"/></h:outputText>
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanComponente.coctTaComponente}"/>
					</rich:column>

			</rich:dataTable>
			
			<%-- 
			<a4j:commandLink action="#{beanCoberturas.consultaPropiedadesCoberturas}" >
				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
				<a4j:actionparam name="coberturaSingle" assignTo="#{beanCoberturas.valComboCobertura}" value="10"/>
			</a4j:commandLink>
			--%>
</a4j:form>			
</rich:modalPanel>

