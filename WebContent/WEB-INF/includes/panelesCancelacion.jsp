<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Cancelacion
- Fecha: 09/10/2020
- Descripcion: Paneles de mensajes
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="panelNotSin">
	<f:facet name="header">
		<h:outputText value="Siniestro en Proceso" />
	</f:facet>
 
	Este certicado que intenta cancelar tiene en proceso una Notificacion o Siniestro con numero:
	<strong><h:outputText value="#{beanCancelacionIndividual.tasaAmortiza.copaVvalor2}" id="sinpro"/></strong>. 
	�Esta seguro de continuar con la operaci�n?
	
	<br/>
	<br/>

	<table class="tablaCen100">
		<caption></caption>
		<tr>
			<td class="center_div">
				<a onclick="Richfaces.hideModalPanel('panelNotSin');Richfaces.showModalPanel('panelCancela',{height:'120px'});" href="#1">
			    	<a4j:commandButton styleClass="boton" value="Si"/>
				</a>
			</td>
			<td class="encabezado1"/>
			<td class="center_div">
				<a onclick="Richfaces.hideModalPanel('panelNotSin');" href="#1">
					<a4j:commandButton styleClass="boton" value="No"/>
				</a>
			</td>
		</tr>
	</table>

	<br/>
	<br/>
    <br/>										    	
</rich:modalPanel>


<rich:modalPanel id="panelCancela">
	<f:facet name="header">
		<h:outputText value="#{msgs.sistemaconfirmar}" />
	</f:facet>
 
	Se cancelar�n <strong><h:outputText value="#{beanCancelacionIndividual.qty}" id="tQty"/></strong> certificado(s). 
	La suma de la(s) prima(s) es : <strong><h:outputText value="#{beanCancelacionIndividual.total}" id="tTotal"/></strong>
	, todo de acuerdo con las condiciones establecidas.
	�Esta seguro de continuar con la operaci�n?
	
	<br/>
	<br/>

	<table class="tablaCen100">
		<caption></caption>
		<tr>
			<td class="center_div">
				<a4j:commandButton 
						styleClass="boton" 
						action="#{beanCancelacionIndividual.cancelaCertificados2}" 
						value="Si"
						onclick="this.disabled=true" 
						oncomplete="this.disabled=false" 
						reRender="listaCertificadosCancel,respuesta"/> 		  			

			</td>
			<td class="encabezado1"/>
			<td class="center_div">
				<a onclick="Richfaces.hideModalPanel('panelCancela');" href="#1">
					<a4j:commandButton styleClass="boton" value="No"/>
				</a>
			</td>
		</tr>
	</table>

	<br/>
	<br/>
    <br/>										    	
</rich:modalPanel>