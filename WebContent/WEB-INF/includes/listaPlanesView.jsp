<%@ include file="include.jsp"%>
<table>
	<tbody>
		<tr>
			<td align="center">
				<rich:dataTable id="lista" value="#{beanParametrizacion.listaPlanes}" columns="7"  var="beanPlan" rows="10">
					<f:facet name="header">
						<rich:columnGroup>
							<rich:column colspan="7"><h:outputText value="Lista de Planes" /></rich:column>
							<rich:column width="4px" breakBefore="true"><h:outputText value="Ramo" /></rich:column>
							<rich:column width="5px" ><h:outputText value="Plan" /></rich:column>
							<rich:column><h:outputText value="Descripción" /></rich:column>
							<rich:column><h:outputText value="Centro de Costos" /></rich:column>
							<rich:column><h:outputText value="Coberturas" /></rich:column>
							<rich:column><h:outputText value="Componentes" /></rich:column>
							<rich:column><h:outputText value="Homologación" /></rich:column>
						</rich:columnGroup>
					</f:facet>
					<rich:column>
						<h:outputText value="#{beanPlan.id.alplCdRamo}" />		      		  			
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanPlan.id.alplCdPlan}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanPlan.alplDePlan}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanPlan.alplDato3}" />
					</rich:column>
					<rich:column>
		      			<a4j:commandLink ajaxSingle="true" reRender="modalForm,listaCoberturas" oncomplete="Richfaces.showModalPanel('popup');" action="#{beanCoberturas.consultaCoberturas}">
		      				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
        					<a4j:actionparam name="cobRamo" 	assignTo="#{beanCoberturas.id.cocbCarpCdRamo}" 		value="#{beanPlan.id.alplCdRamo}"/>
        					<a4j:actionparam name="cobProd" 	assignTo="#{beanCoberturas.id.cocbCapuCdProducto}" 	value="#{beanParametrizacion.id.alprCdProducto}"/>
        					<a4j:actionparam name="cobPlan" 	assignTo="#{beanCoberturas.id.cocbCapbCdPlan}" 		value="#{beanPlan.id.alplCdPlan}"/>
        					<a4j:actionparam name="descPlan" 	assignTo="#{beanCoberturas.descPlan}" 				value="#{beanPlan.alplDePlan}"/>
  																															       					
        					<a4j:actionparam name="cobIdVenta" 	assignTo="#{beanCoberturas.idVenta}" 				value="#{beanParametrizacion.alprDato3}"/>
        					<a4j:actionparam name="cobPlazo" 	assignTo="#{beanCoberturas.plazo}" 					value="#{beanParametrizacion.plazo}"/>
        					<a4j:actionparam name="poliza" 		assignTo="#{beanCoberturas.poliza}" 				value="#{beanListaPreCarga.inPoliza}"/>
        					
        					<%-- <a4j:actionparam name="lstCob" assignTo="#{beanCoberturas.listaComboCoberturas}" 	value="#{beanParametrizacion.listaComboCoberturas}"/>--%>
		      			</a4j:commandLink>
					</rich:column>

					<rich:column>
		      			<a4j:commandLink ajaxSingle="true" reRender="modalForm2,listaCoberturas" oncomplete="Richfaces.showModalPanel('componentesPopup');" action="#{beanComponentes.consultaComponentes}">
		      				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
        					<a4j:actionparam name="cobRamo" 	assignTo="#{beanComponentes.id.coctCarpCdRamo}" 	 value="#{beanPlan.id.alplCdRamo}"/>
        					<a4j:actionparam name="cobProd" 	assignTo="#{beanComponentes.id.coctCapuCdProducto}"  value="#{beanParametrizacion.id.alprCdProducto}"/>
        					<a4j:actionparam name="cobPlan" 	assignTo="#{beanComponentes.id.coctCapbCdPlan}" 	 value="#{beanPlan.id.alplCdPlan}"/>
        					<a4j:actionparam name="descPlan" 	assignTo="#{beanComponentes.descPlan}" 				 value="#{beanPlan.alplDePlan}"/>
  																															       					
        					<a4j:actionparam name="cobIdVenta" 	assignTo="#{beanComponentes.idVenta}" 				value="#{beanParametrizacion.alprDato3}"/>
        					<a4j:actionparam name="cobPlazo" 	assignTo="#{beanComponentes.plazo}" 				value="#{beanParametrizacion.plazo}"/>
        					<a4j:actionparam name="poliza" 		assignTo="#{beanComponentes.poliza}" 				value="#{beanListaPreCarga.inPoliza}"/>
		      			</a4j:commandLink>
					</rich:column>

					<rich:column>
		      			<a4j:commandLink ajaxSingle="true" reRender="modalForm3,listaHomologa" oncomplete="Richfaces.showModalPanel('homologaPopup');" action="#{beanHomologa.consultaHomologa}">
		      				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
        					<a4j:actionparam name="cobRamo" 	 assignTo="#{beanHomologa.id.hoppCdRamo}" 		value="#{beanPlan.id.alplCdRamo}"/>
        					<a4j:actionparam name="cobProd" 	 assignTo="#{beanHomologa.id.hoppProdColectivo}" value="#{beanParametrizacion.id.alprCdProducto}"/>
        					<a4j:actionparam name="cobPlan" 	 assignTo="#{beanHomologa.id.hoppPlanColectivo}" value="#{beanPlan.id.alplCdPlan}"/>
        					<a4j:actionparam name="descPlan" 	 assignTo="#{beanHomologa.descPlan}" 			value="#{beanPlan.alplDePlan}"/>
        					<a4j:actionparam name="descProducto" assignTo="#{beanHomologa.hoppDeProducto}"			value="#{beanParametrizacion.alprDeProducto}"/>
        					
  																															       					
        					<a4j:actionparam name="cobIdVenta" 	assignTo="#{beanHomologa.hoppDato3}" value="#{beanParametrizacion.alprDato3}"/>
        					<a4j:actionparam name="cobPlazo" 	assignTo="#{beanHomologa.plazo}"   value="#{beanParametrizacion.plazo}"/>
		      			</a4j:commandLink>
					</rich:column>

				</rich:dataTable>
			</td>
		</tr>
	</tbody>
</table>
