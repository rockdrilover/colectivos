<%@include file="include.jsp"%>

<rich:modalPanel id="popup" autosized="true"  onmaskclick="#{rich:component('popup')}.hide()" >
	<f:facet name="header">
		<h:outputText value="Alta de Coberturas" />
	</f:facet>
	<f:facet name="controls">
		<h:outputLink value="#" onclick="#{rich:component('popup')}.hide(); return false;">
			Cerrar X
		</h:outputLink>
	</f:facet>

<a4j:form id="modalForm" ajaxSubmit="true">
			<h:panelGrid columns="2" id="panel_1" styleClass="center">
			<a4j:outputPanel layout="block">
				<a4j:outputPanel layout="block">
					<rich:message for="comboCob" styleClass="errorMessage"/>
				</a4j:outputPanel>

				<a4j:outputPanel layout="block">
					<h:outputLabel value="Coberturas:"/>
					<h:selectOneMenu id="comboCob" value="#{beanCoberturas.valComboCobertura}" required="true">
						<f:selectItem itemValue="" itemLabel="Seleccione una cobertura"/>
						<f:selectItems value="#{beanCoberturas.listaComboCoberturas}"/>
					</h:selectOneMenu>
				</a4j:outputPanel>
			</a4j:outputPanel>
			</h:panelGrid>
			
			<rich:spacer height="20px"/>
			
			<h:panelGrid columns="3" id="panel_2" styleClass="center">	
				
				<h:outputLabel value="Plan:" />    <h:inputText id="plan"     value="#{beanCoberturas.descPlan}" 			  size="25" readonly="true"/> <h:outputLabel value=""/>				
				<h:outputLabel value="P�liza:"/>   <h:inputText id="poliza"   value="#{beanCoberturas.id.cocbCapoNuPoliza}"   size="8"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Ramo:"/>     <h:inputText id="cobRamo"  value="#{beanCoberturas.id.cocbCarpCdRamo}"     size="4"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Producto:"/> <h:inputText id="producto" value="#{beanCoberturas.id.cocbCapuCdProducto}" size="4"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Plazo:"/>	   <h:inputText id="plazo" 	  value="#{beanCoberturas.id.cocbCapbCdPlan}"     size="4"  readonly="true"/><h:outputLabel value=""/>
				<h:outputLabel value="Inicio Vigencia:"/> <rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy" required="true" value="#{beanCoberturas.cocbFeDesde}" inputSize="8"/> <rich:message for="fechaInicial" styleClass="errorMessage"/>
				<h:outputLabel value="Fin Vigencia:"/>    <rich:calendar id="fechaFinal"   datePattern="dd/MM/yyyy" required="true" value="#{beanCoberturas.cocbFeHasta}" inputSize="8" /><rich:message for="fechaFinal"   styleClass="errorMessage"/>

				<h:outputLabel value="Edad M�nima:"/>	  <h:inputText id="eMin"    value="#{beanCoberturas.cocbEdadMinima}" size="4" required="true"/> <rich:message for="eMin" 	 styleClass="errorMessage"/>
				<h:outputLabel value="Edad M�xima:"/>	  <h:inputText id="eMax"    value="#{beanCoberturas.cocbEdadMaxima}" size="4" required="true"/> <rich:message for="eMax" 	 styleClass="errorMessage"/>
				<h:outputLabel value="Tase de Riesgo:"/>  <h:inputText id="tase"    value="#{beanCoberturas.cocbTaRiesgo}"   size="4" required="true"/> <rich:message for="tase" 	 styleClass="errorMessage"/>
				<h:outputLabel value="Valor al milla:"/>  <h:inputText id="vMilla"  value="#{beanCoberturas.cocbCampon1}"    size="4" required="true"/> <rich:message for="vMilla"  styleClass="errorMessage"/>
				<h:outputLabel value="Porcentaje:"/>	  <h:inputText id="percent" value="#{beanCoberturas.cocbCampov1}"    size="4" required="true"/> <rich:message for="percent" styleClass="errorMessage"/>
			
			</h:panelGrid>
			
			<h:panelGrid columns="2" id="panel_3" styleClass="center">
				<a4j:commandButton action="#{beanCoberturas.guardaCoberturas}" value="Guardar"  type="submit" id="Guardar" reRender="modalForm,listaCoberturas"/>
			</h:panelGrid>
			<rich:spacer height="10px"/>
			
			<rich:dataTable id="listaCoberturas" value="#{beanCoberturas.listaCoberturas}" columns="9" var="beanCobertura" rows="10" styleClass="center">
					<f:facet name="header">
						<rich:columnGroup>
							<rich:column colspan="9"><h:outputText value="Lista de Coberturas" /></rich:column>
							<rich:column width="4px" breakBefore="true"><h:outputText value="Ramo Contable" /></rich:column>
							<rich:column width="5px" ><h:outputText value="Cobertura" /></rich:column>
							<rich:column><h:outputText value="Inicio Vigencia" /></rich:column>
							<rich:column><h:outputText value="Fin Vigencia" /></rich:column>
							<rich:column><h:outputText value="Edad Minima" /></rich:column>
							<rich:column><h:outputText value="Edad Maxima" /></rich:column>
							<rich:column><h:outputText value="Tase de Riesgo" /></rich:column>
							<rich:column><h:outputText value="Valor al milla" /></rich:column>
							<rich:column><h:outputText value="Porcentaje" /></rich:column>
						</rich:columnGroup>
					</f:facet>
					<rich:column>
						<h:outputText value="#{beanCobertura.id.cocbCarbCdRamo}" />		      		  			
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.id.cocbCacbCdCobertura}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.cocbFeDesde}"><f:convertDateTime type="date" pattern="dd/MM/yyyy"/></h:outputText>
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.cocbFeHasta}"><f:convertDateTime type="date" pattern="dd/MM/yyyy"/></h:outputText>
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.cocbEdadMinima}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.cocbEdadMaxima}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.cocbTaRiesgo}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.cocbCampon1}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanCobertura.cocbCampov1}" />
					</rich:column>

			</rich:dataTable>
			
			<%-- 
			<a4j:commandLink action="#{beanCoberturas.consultaPropiedadesCoberturas}" >
				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
				<a4j:actionparam name="coberturaSingle" assignTo="#{beanCoberturas.valComboCobertura}" value="10"/>
			</a4j:commandLink>
			--%>
</a4j:form>			
</rich:modalPanel>

