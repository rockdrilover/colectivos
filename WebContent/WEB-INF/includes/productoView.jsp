<%@ include file="include.jsp"%> 
<%-- <a4j:region id="productoRegion"> --%>
<table cellpadding="2" cellspacing="0">
	<a4j:region id="r1">
	<tbody>
		<tr>
			<td align="right" width="24%" height="30"><h:outputText value="Ramo :" /></td>
			<td align="left" width="76%" height="30">
				<h:selectOneMenu id="inRamo" value="#{beanParametrizacion.ramo.carpCdRamo}" binding="#{beanListaParametros.inRamo}" required="true">
					<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
					<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
				<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="inPoliza" />	
				</h:selectOneMenu>
				<rich:message for="inRamo" styleClass="errorMessage" />																						
		</td>
	</tr>
	<tr>
		<td align="right" width="24%" height="30"><h:outputText value="Poliza :"/></td>
		<td align="left" width="76%" height="30">
			<h:selectOneMenu id="inPoliza"  value="#{beanListaPreCarga.inPoliza}" binding="#{beanListaParametros.inPoliza}" required="true">
				<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
				<f:selectItem itemValue="0" itemLabel="PRIMA UNICA"/>
				<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
			<a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" reRender="idVenta"/>
			</h:selectOneMenu>
			<rich:message for="inPoliza" styleClass="errorMessage" />
		</td>
	</tr>
	<tr>
		<td align="right" width="24%" height="30"><h:outputText value="Canal de Venta:"/></td>
		<td align="left" width="76%" height="30">
			<h:selectOneMenu id="idVenta"  value="#{beanParametrizacion.alprDato3}" binding="#{beanListaParametros.inIdVenta}" disabled="#{beanListaParametros.habilitaComboIdVenta}" >
				<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
				<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
			</h:selectOneMenu>
			<rich:message for="idVenta" styleClass="errorMessage" />
		</td>
	</tr>
	</a4j:region>
	<tr>
		<td align="right" width="24%" height="30"><h:outputText value="Producto:"/></td>
		<td align="left" width="76%" height="30">
			<h:inputText id="producto" value="#{beanParametrizacion.alprDeProducto}" maxlength="100" size="50" required="true"/>
			<rich:message for="producto" styleClass="errorMessage" />
		</td>
	</tr>
	<tr>
		<td align="right" width="24%" height="30"></td>
		<td align="left" width="76%" height="30">
			<a4j:commandButton styleClass="boton" value="Guardar" id="Guardar"
			   action="#{beanParametrizacion.altaProducto}" 
			   onclick="this.disabled=true" oncomplete="this.disabled=false" 
			   reRender="test, idProducto" />	   
		</td>
	</tr>

	<tr>
		<td align="right" width="24%" height="30">
			<h:outputText value=""/>
		</td>
		<td align="left" width="76%" height="30">
			<h:panelGroup id="test">
				<h:outputText id="txtProducto"  value="#{beanParametrizacion.id.alprCdProducto}"/>-
				<h:outputText id="descProducto" value="#{beanParametrizacion.alprDeProducto}"/>
			</h:panelGroup>
		</td>
	</tr>

	</tbody>
</table>
<%--</a4j:region> --%>    
