<%@ include file="include.jsp"%> 
<a4j:region id="regioncomun">
<table cellpadding="2" cellspacing="0">
	<tbody>
		<tr>		
					<td align="right" height="25"><h:outputText value="Ramo:" /></td>
					<td colspan="2" align="left" height="25">
						<h:selectOneMenu id="ramo" value="#{beanParamOperacion.copaNvalor2}"  binding="#{beanListaParametros.inRamo}" title="header=[Ramo] body=[Seleccione un ramo.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true" >
							<f:selectItem itemValue="" itemLabel="Ramo" />
							<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
							<a4j:support event="onchange"  action="#{beanListaParametros.cargaPolizas}" ajaxSingle="true" reRender="polizas" />
					     </h:selectOneMenu>
					   <h:message for="ramo" styleClass="errorMessage"/>
					</td>  
		</tr>
												    	<tr>
												    		<td align="right" height="25"><h:outputText value="Poliza:"/></td>
															<td colspan="2" align="left" height="25">
																<a4j:outputPanel id="polizas" ajaxRendered="true">
																	<h:selectOneMenu id="poliza"  value="#{beanParamOperacion.copaNvalor3}" binding="#{beanListaParametros.inPoliza}" title="header=[P�liza] body=[Seleccione una p�liza para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" required="true">
																			<f:selectItem itemValue="" itemLabel="Seleccione una p�liza"/>
																			<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
																			<a4j:support event="onchange" action="#{beanParamOperacion.cargaIdVenta}"  ajaxSingle="true" reRender="idVenta, idPoliza, opcion20, opcion60, opcion30, opcion40, opcion5, opcion6" />
																	</h:selectOneMenu>
																	<h:message for="poliza" styleClass="errorMessage" />
																</a4j:outputPanel>
															</td>
												    	</tr>
												    	<tr>
												    		<td align="right" height="25"><h:outputText value="Canal de Venta:"/></td>
															<td colspan="2" align="left" height="25">
																<a4j:outputPanel id="idVenta" ajaxRendered="true">
																	<h:selectOneMenu id="inVenta"  value="#{beanParamOperacion.copaNvalor4}" title="header=[Id venta] body=[Seleccione una canal de venta para la carga y emisi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" 
																	         disabled="#{beanParamOperacion.habilitaComboIdVenta}">
																			<f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
																			<f:selectItems value="#{beanParamOperacion.listaComboIdVenta}"/>
																			<a4j:support event="onchange" ajaxSingle="true" />
																	</h:selectOneMenu>
																</a4j:outputPanel>
															</td>
												    	</tr>
	
		
	</tbody>
</table>
</a4j:region>    
