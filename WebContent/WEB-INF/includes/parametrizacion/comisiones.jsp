<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Comisiones
- Fecha: 09/10/2020
- Descripcion: Modal para porcentajes de comisiones
--%>
<%@include file="include.jsp"%>

<rich:separator height="4" lineType="double" />
<h:outputText value="#{msgs.comisiontec} " styleClass="texto_mediano"/>					
<h:panelGrid columns="6">
	<h:outputText value="#{msgs.comisionac} "/>
	<h:outputText value="#{msgs.comisiontec} "/>
	<h:outputText value="#{msgs.comisionccn} "/>
	<h:outputText value="#{msgs.comisionccm} "/>
	<h:outputText value="#{msgs.comisionr} "/>
	<h:outputText value="#{msgs.comisiont} "/>
	
	<h:outputText value="#{msgs.comisionemi} "/>
	<h:inputText value="#{beanParamComision.tecnicaEmi.comTecnica}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecEmi" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.tecnicaEmi.comContingente}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecEmi" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.tecnicaEmi.comComercial}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecEmi" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.tecnicaEmi.comRepartos}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecEmi" />
	</h:inputText>
	<a4j:outputPanel id="totTecEmi" ajaxRendered="true">
		<h:outputText value="#{beanParamComision.tecnicaEmi.comTotal}%" styleClass="texto_mediano"/>
	</a4j:outputPanel>
	
	
	<h:outputText value="#{msgs.comisionren} "/>
	<h:inputText value="#{beanParamComision.tecnicaReno.comTecnica}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecRen" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.tecnicaReno.comContingente}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecRen" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.tecnicaReno.comComercial}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecRen" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.tecnicaReno.comRepartos}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totTecRen" />
	</h:inputText>
	<a4j:outputPanel id="totTecRen" ajaxRendered="true">
		<h:outputText value="#{beanParamComision.tecnicaReno.comTotal}%" styleClass="texto_mediano"/>
	</a4j:outputPanel>
</h:panelGrid>

<rich:separator height="4" lineType="double" />
<h:outputText value="#{msgs.comisioncom} " styleClass="texto_mediano"/>					
<h:panelGrid columns="6">
	<h:outputText value="#{msgs.comisionac} "/>
	<h:outputText value="#{msgs.comisiontec} "/>
	<h:outputText value="#{msgs.comisionccn} "/>
	<h:outputText value="#{msgs.comisionccm} "/>
	<h:outputText value="#{msgs.comisionr} "/>
	<h:outputText value="#{msgs.comisiont} "/>
	
	<h:outputText value="#{msgs.comisionemi} "/>
	<h:inputText value="#{beanParamComision.comercialEmi.comTecnica}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComEmi" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.comercialEmi.comContingente}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComEmi" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.comercialEmi.comComercial}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComEmi" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.comercialEmi.comRepartos}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComEmi" />
	</h:inputText>
	<a4j:outputPanel id="totComEmi" ajaxRendered="true">
		<h:outputText value="#{beanParamComision.comercialEmi.comTotal}%" styleClass="texto_mediano"/>
	</a4j:outputPanel>
	
	<h:outputText value="#{msgs.comisionren} "/>
	<h:inputText value="#{beanParamComision.comercialReno.comTecnica}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComRen" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.comercialReno.comContingente}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComRen" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.comercialReno.comComercial}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComRen" />
	</h:inputText>
	<h:inputText value="#{beanParamComision.comercialReno.comRepartos}" styleClass="wid30">
		<a4j:support event="onchange" ajaxSingle="true" reRender="totComRen" />
	</h:inputText>
	<a4j:outputPanel id="totComRen" ajaxRendered="true">
		<h:outputText value="#{beanParamComision.comercialReno.comTotal}%" styleClass="texto_mediano"/>
	</a4j:outputPanel>
</h:panelGrid>