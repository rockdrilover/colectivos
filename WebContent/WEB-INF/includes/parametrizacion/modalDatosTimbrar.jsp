<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Timbrado
- Fecha: 09/10/2020
- Descripcion: Modal para datos de Timbrado
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="datPanel" autosized="true" height="600" width="600">
	<div class="scroll-div">
   	<f:facet name="header"><h:outputText value="Nuevo Timbrado" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="datlinkN" styleClass="hidelink" />
            <rich:componentControl for="datPanel" attachTo="datlinkN" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>

	<h:form id="datosTimbrar">
        <rich:messages styleClass="errorMessage"></rich:messages>
        <h:panelGrid columns="1">
            <a4j:outputPanel ajaxRendered="true">
                <h:panelGrid columns="1">
                	<h:panelGrid columns="10">
                		<h:outputText value="#{msgs.endosodatosdatos}"/>
                		<h:outputText value="#{beanTimbrado.nuevo.recibo.coreCarpCdRamo}" styleClass="texto_mediano"/>
                		<h:outputText value="#{msgs.sistemaguion}" styleClass="texto_mediano"/>
                		<h:outputText value="#{beanTimbrado.nuevo.recibo.coreCapoNuPoliza}" styleClass="texto_mediano"/>
                		<h:outputText value="#{msgs.sistemaguion}" styleClass="texto_mediano"/>
                		<h:outputText value="#{beanTimbrado.nuevo.recibo.coreCaceNuCertificado}" styleClass="texto_mediano"/>
                		<h:outputText value="#{msgs.sistemaguion}" styleClass="texto_mediano"/>
                		<h:outputText value="#{beanTimbrado.nuevo.factura.cofaNuReciboFiscal}" styleClass="texto_mediano"/>
                		
                		<h:graphicImage value="../../images/espacio.png"/>
						<a4j:commandButton id="btnTimbrar"
								styleClass="boton" value="#{msgs.botontimbrar}"
								action="#{beanTimbrado.timbrar}"
								onclick="this.disabled=true;bloqueaPantalla();"
								oncomplete="this.disabled=false;desbloqueaPantalla();muestraOcultaSeccion('resultado', 1)"
								reRender="panPrincipal"/>
                	</h:panelGrid>
					
					<h:outputText value="#{msgs.timbradode} " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double" />
					<h:panelGrid columns="2">
						<h:outputText value="#{msgs.timbradors}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreNuReciboAceptado}" styleClass="texto-mediano2, wid350" disabled="true"/>
					</h:panelGrid>
					<h:panelGrid columns="4">
						<h:outputText value="#{msgs.timbradorfc}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCacnCdNacionalidad}" styleClass="texto-mediano2, wid150" disabled="true"/>
						<h:outputText value="#{msgs.timbradorf}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreInDevolucion}" styleClass="texto-mediano2, wid170"/>
					</h:panelGrid>
		           	<rich:spacer height="10px" />
		           	
		           	<h:outputText value="#{msgs.timbradodc}: " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double" />
					<h:panelGrid columns="4">
						<h:outputText value="#{msgs.timbradotipdoc}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreTpTramite}" styleClass="texto-mediano2, wid170"/>
						<h:outputText value="#{msgs.timbradotipcom}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreTpTramite}" styleClass="texto-mediano2, wid170"/>
						
						<h:outputText value="#{msgs.timbradoluex}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCaloCdLocalidad}" styleClass="texto-mediano2, wid170"/>
						<h:outputText value="#{msgs.timbradomon}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCamoCdMoneda}" styleClass="texto-mediano2, wid170"/>
						
						<h:outputText value="#{msgs.timbradoser}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaNuCedulaRif}" styleClass="texto-mediano2, wid170"/>
						<h:outputText value="#{msgs.timbradofol}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaNuTrasfer}" styleClass="texto-mediano2, wid170"/>
						
						<h:outputText value="#{msgs.timbradoforpa}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaNuCuenta}" styleClass="texto-mediano2, wid170"/>
						<h:outputText value="#{msgs.timbradometpa}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCdMotivo}" styleClass="texto-mediano2, wid170"/>
					</h:panelGrid>
					<h:panelGrid columns="2">
						<h:outputText value="#{msgs.timbradoconpa}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaStRecibo}" styleClass="texto-mediano2, wid300"/>
					</h:panelGrid>	
					<rich:spacer height="10px" />
					
					<h:outputText value="#{msgs.timbradodr}: " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double" />
					<h:panelGrid columns="4">
						<h:outputText value="#{msgs.timbradorfc}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCacnNuCedulaRif}" styleClass="texto-mediano2, wid150"/>
						<h:outputText value="#{msgs.timbradocuso}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreInFinanciamiento}" styleClass="texto-mediano2, wid180"/>
					</h:panelGrid>
					<h:panelGrid columns="2">
						<h:outputText value="#{msgs.loginnombre}: "/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreInEmision}" styleClass="texto-mediano2, wid350"/>
					</h:panelGrid>		
					<rich:spacer height="10px" />
					
					<h:outputText value="#{msgs.timbradocon}: " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double" />
					<h:panelGrid columns="7">
						<h:outputText value="#{msgs.timbradoproser} "/>
						<h:outputText value="#{msgs.timbradocanti} "/>
						<h:outputText value="#{msgs.timbradouni} "/>
						<h:outputText value="#{msgs.timbradodesc} "/>
						<h:outputText value="#{msgs.timbradovu} "/>
						<h:outputText value="#{msgs.timbradoimpo} "/>
						<h:outputText value="#{msgs.timbradodescu} "/>
						
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCjegNuEgreso}" styleClass="texto-mediano2, wid60"/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCapoCdProvisional}" styleClass="texto-mediano2, wid20"/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreInPagoCoaseguroCedido}" styleClass="texto-mediano2, wid50"/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaCampov3}" styleClass="texto-mediano2, wid150"/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaMtRecibo}" styleClass="texto-mediano2, wid80">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:inputText>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaMtRecibo}" styleClass="texto-mediano2, wid80">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:inputText>
						<h:inputText value="0" styleClass="texto-mediano2, wid50" disabled="true"/>
					</h:panelGrid>
					<rich:spacer height="10px" />
					
					<h:outputText value="#{msgs.timbradoimp}: " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double" />
					<h:panelGrid columns="4">
						<h:outputText value="#{msgs.timbradoimpu} "/>
						<h:outputText value="#{msgs.timbradotipfac} "/>
						<h:outputText value="#{msgs.timbradotasa} "/>
						<h:outputText value="#{msgs.timbradoimpo} "/>
						
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreInPagoComision}" styleClass="texto-mediano2, wid80"/>
						<h:inputText value="Tasa" styleClass="texto-mediano2, wid50" disabled="true"/>
						<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCataTaTasa}" styleClass="texto-mediano2, wid50" disabled="true"/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaMtTotIva}" styleClass="texto-mediano2, wid80">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:inputText>
					</h:panelGrid>
					<rich:spacer height="10px" />					
					
					<h:outputText value="#{msgs.timbradotot}: " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double" />
					<h:panelGrid columns="4">
						<h:outputText value="#{msgs.timbradosub} "/>
						<h:outputText value="#{msgs.timbradodescu} "/>
						<h:outputText value="#{msgs.timbradoimp} "/>
						<h:outputText value="#{msgs.timbradototal} "/>
						
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaMtRecibo}" styleClass="texto-mediano2, wid80">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:inputText>
						<h:inputText value="0" styleClass="texto-mediano2, wid50" disabled="true"/>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaMtTotIva}" styleClass="texto-mediano2, wid80">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:inputText>
						<h:inputText value="#{beanTimbrado.nuevo.factura.cofaMtTotPma}" styleClass="texto-mediano2, wid80">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:inputText>
					</h:panelGrid>
					<rich:spacer height="10px" />
                </h:panelGrid>
            </a4j:outputPanel>          
        </h:panelGrid>    
    </h:form>    
    </div>
 </rich:modalPanel>



					