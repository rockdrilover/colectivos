<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Timbrado
- Fecha: 09/10/2020
- Descripcion: Modal para editar parametrizacion de timbrado
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="editPanel" autosized="true" width="450">
   	<f:facet name="header"><h:outputText value="Editar Parametrizacion" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
            <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>

	<h:form id="editarGrupo">
        <rich:messages styleClass="errorMessage"></rich:messages>
        
        <h:panelGrid columns="1">
        <a4j:outputPanel ajaxRendered="true">
           	<h:outputText value="#{beanParamTimbrado.objActual.objParam.auxDato}" styleClass="texto_mediano"/>
			<rich:separator height="4" lineType="double" />
			<rich:spacer height="10px" />
			
			<rich:tabPanel switchType="ajax">
	        	<rich:tab label="#{msgs.timbrado}">
	        		<h:panelGrid columns="2">
						<h:outputText value="#{msgs.timbradors}: "/>
						<h:outputText value="#{beanParamTimbrado.objActual.razonSocial} - #{msgs.sistemadefautl}" styleClass="wid290"/>
						
						<h:outputText value="#{msgs.sistemaespacio} "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objParam.copaRegistro}" styleClass="wid290" maxlength="120"/>
						
						<h:outputText value="#{msgs.timbradomp}: "/>
	                    <h:outputText value="#{beanParamTimbrado.metodoPago} - #{msgs.sistemadefautl}" styleClass="wid290"/>
	                    
	                    <h:outputText value="#{msgs.sistemaespacio} "/>
						<h:selectOneMenu id="mepa" value="#{beanParamTimbrado.objActual.objParam.copaVvalor6}" >
							<f:selectItems value="#{beanParamTimbrado.cmbMetodosPago}" />
						</h:selectOneMenu>
						
						<h:outputText value="#{msgs.timbradoca}: "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objParam.copaVvalor1}" styleClass="wid280" maxlength="80"/>
						
						<h:outputText value="#{msgs.timbradons}: "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objParam.copaVvalor2}" styleClass="wid280" maxlength="80"/>
						
						<h:outputText value="#{msgs.timbradocg}: "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objParam.copaVvalor3}" styleClass="wid280" maxlength="80"/>
						
						<h:outputText value="#{msgs.timbradocc}: "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objParam.copaVvalor4}" styleClass="wid280" maxlength="80"/>
            		</h:panelGrid>
	        	</rich:tab>
	        	
	        	<rich:tab label="#{msgs.timbradoCobranza}">
	        		<h:panelGrid columns="2">
	        			<h:outputText value="#{msgs.timbradoTCobranza}" id="msgTipo"/>
	                    <h:selectOneRadio id ="timbradoTCobranza" value="#{beanParamTimbrado.objActual.objCobranza.copaVvalor1}" >
		                    <f:selectItem itemValue="1" itemLabel="Por P�liza" />
		                    <f:selectItem itemValue="2" itemLabel="Por Credito" />
	                    </h:selectOneRadio>	 
						
						<h:outputText value="#{msgs.timbradoPgracia} "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objCobranza.copaNvalor5}" styleClass="wid290" maxlength="120"/>
							
						<h:outputText value="#{msgs.timbradoTemision} "/>
						<h:selectOneRadio id ="timbradoTemision" value="#{beanParamTimbrado.objActual.objCobranza.copaVvalor2}" >
		                    <f:selectItem itemValue="1" itemLabel="Sin Desfase" />
		                    <f:selectItem itemValue="2" itemLabel="Con Desfase" />
	                    </h:selectOneRadio>	
						
						<h:outputText value="#{msgs.timbradoTipoPoliza} "/>
						<h:selectOneMenu value="#{beanParamTimbrado.objActual.objCobranza.copaVvalor3}">
							<f:selectItem itemValue="0" itemLabel="-- Seleccionar" />
							<f:selectItem itemValue="1" itemLabel="Normal" />
							<f:selectItem itemValue="2" itemLabel="Covid" />
						</h:selectOneMenu>
						
					</h:panelGrid>
	        	</rich:tab>
	        	
	        	
	        	<rich:tab label="#{msgs.timbradoComision}">
	        		<h:panelGrid columns="2">
	        			
	        			<h:outputText value="#{msgs.comisioncob} "/>	
						<h:selectOneRadio value="#{beanParamTimbrado.objActual.objCobranza.copaNvalor4}" id="tipoCober">
							<f:selectItem itemValue="1" itemLabel="Normal (Vida)" />
							<f:selectItem itemValue="2" itemLabel="Aplica Ambos (Vida y Desempleo)" />						
						</h:selectOneRadio>
					
						<h:outputText value="#{msgs.timbradoNombreProducto} "/>
						<h:selectOneMenu value="#{beanParamTimbrado.objActual.objCobranza.copaVvalor4}" id="proVida">
							<f:selectItem itemValue="0" itemLabel="-- Seleccionar --" />
							<f:selectItems value="#{beanParamTimbrado.lstNombreProducto}"/>
						</h:selectOneMenu>
						
						<h:outputText value="#{msgs.timbradoPorcentaje} "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objCobranza.copaVvalor5}" id="porVida" styleClass="wid290" maxlength="120"/>
						
						
						<h:outputText value="#{msgs.timbradoNombreProductoDes} "/>
						<h:selectOneMenu value="#{beanParamTimbrado.objActual.objCobranza.copaVvalor7}" id="proDes">
							<f:selectItem itemValue="0" itemLabel="-- Seleccionar --" />
							<f:selectItems value="#{beanParamTimbrado.lstNombreProducto}"/>
						</h:selectOneMenu>
						
						<h:outputText value="#{msgs.timbradoPorcentajeDes} "/>
						<h:inputText value="#{beanParamTimbrado.objActual.objCobranza.copaVvalor6}" id="porDes" styleClass="wid290" maxlength="120"/>
						
					</h:panelGrid>
	        	</rich:tab>
	        	
	        	<rich:tab label="#{msgs.timbradootros}">
	        		<h:panelGrid columns="1">
		        		<h:outputText value="#{msgs.timbradops}: " styleClass="texto_mediano"/>					
						<rich:separator height="4" lineType="double" />
						<rich:spacer height="10px" />
			
						<h:panelGrid columns="2">
	                     	<h:outputText value="#{msgs.timbradoips}: "/>
							<h:selectOneMenu id="prostock" value="#{beanParamTimbrado.objActual.objParam.copaNvalor6}" >
								<f:selectItem itemValue="0" itemLabel="NO" />
								<f:selectItem itemValue="1" itemLabel="SI" />
							</h:selectOneMenu>											
	                    </h:panelGrid>             
	                    
	                    <rich:spacer height="10px" />
						
						<h:outputText value="#{msgs.timbradocp}: " styleClass="texto_mediano"/>					
						<rich:separator height="4" lineType="double" />
						<rich:spacer height="10px" />
			
						<h:panelGrid columns="2">
	                     	<h:outputText value="#{msgs.timbradoce}: "/>
							<h:inputText value="#{beanParamTimbrado.objActual.objParam.copaNvalor5}" styleClass="wid280" maxlength="80"/>
	                    </h:panelGrid>             
	                    
	                    <rich:spacer height="10px" />
														
						<h:outputText value="#{msgs.timbradofe}: " styleClass="texto_mediano"/>					
						<rich:separator height="4" lineType="double"/>	
						<rich:spacer height="10px" />
			
						<h:panelGrid columns="2">
	                    	<h:outputText value="#{msgs.timbradofi}: "/>
	                    	<rich:calendar id="fecha_ini" value="#{beanParamTimbrado.objActual.objParam.copaFvalor1}" 
		                        		cellWidth="24px" cellHeight="22px" datePattern="dd/MM/yyyy"
		                        		inputSize="10" popup="true"  styleClass="calendario,wid100"/>
							
							<h:outputText value="#{msgs.timbradoff}: "/>
							<rich:calendar id="fecha_fin" value="#{beanParamTimbrado.objActual.objParam.copaFvalor2}" 
		                        		cellWidth="24px" cellHeight="22px" datePattern="dd/MM/yyyy"
		                        		inputSize="10" popup="true"  styleClass="calendario,wid100"/>
						</h:panelGrid>
					</h:panelGrid>
	        	</rich:tab>
	        </rich:tabPanel>
	        
	        <rich:spacer height="10px" />
	        
	        <h:panelGrid columns="2">
				<a4j:commandButton 
	              		value="Modificar"
	                  	action="#{beanParamTimbrado.modificar}" 
	                  	reRender="mensaje,regTabla,dsParam"	                    
	                  	oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();muestraOcultaSeccion('resultados', 0);" />	
	                 
	             <a4j:commandButton 
	               	value="Cancelar"
				    action="#{beanParamTimbrado.cancelar}"
				    reRender="mensaje,regTabla,dsParam"
				   	onclick="#{rich:component('editPanel')}.hide();"	                    
				    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('editPanel')}.hide(); } else { #{rich:component('editPanel')}.show(); }muestraOcultaSeccion('resultados', 1);" />
	             	</h:panelGrid>
	    </a4j:outputPanel>
		</h:panelGrid>
    </h:form>
 </rich:modalPanel>