<%@ include file="../include.jsp"%>
				    				
				    		<div align="center">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanConciliacion.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    		</div>		
				    				
				  	
				    				<!-- Inicio ACCIONES -->
				    	<a4j:region id="consultaDatos">
				    		  <h:form id="consultaDatos2">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="espacio_5"><td>&nbsp;</td></tr>						                    		
						                    		<tr class="texto_normal">
						                  				<td>						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanConciliacion.consultarConciliacion}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consultar Conciliaciones" />													
														</td>
														<td>
															<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nueva Conciliacion" />									
														</td>
						                    		</tr>
						                    		
													<tr class="espacio_15"><td>&nbsp;</td></tr>						                    		
						                    		<tr>
										    			<td align="center" colspan="2">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				                </table>
				                
				           	<!-- Inicio RESULTADOS -->
				                  
				    		<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													 <a4j:outputPanel id="secRes" ajaxRendered="true"> 
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
									
											   <a4j:outputPanel id="regTabla" ajaxRendered="true">
											
												<table width="40%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanConciliacion.hmResultados == null}">
															<table width="90%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
														
														  <c:if test="${beanConciliacion.hmResultados != null}">
															<rich:dataTable value="#{beanConciliacion.mapValues}" id="dsConciliacion" var="registro" 
																	rows="10" rowKeyVar="row" ajaxKeys="#{beanConciliacion.keys}" 
																	columns="10"  align="center" >
									      						<f:facet name="header"><h:outputText value="Configuracion"/></f:facet>
									      						
														      	<rich:column title="Canal" width="10%" styleClass="texto_centro" sortBy="#{registro.copaNvalor6}" >
														      		<f:facet name="header">
                                                                    <h:outputText value=" Canal "  title="Canal"/></f:facet>
                                                                     <h:outputText value="#{registro.copaNvalor8}">
                                                                        <f:convertNumber  integerOnly="true" />
                                                                     </h:outputText>
														      	</rich:column>

														      	<rich:column title="Ramo" width="20%" styleClass="texto_centro" sortBy="#{registro.copaNvalor8}">
														      		<f:facet name="header"><h:outputText value=" Ramo "  title="Ramo"/></f:facet>
														      		<h:outputText value="#{registro.copaNvalor6}"/>
                                                                </rich:column>
                                                                
                                                                <rich:column title="Id de Venta" width="20%" styleClass="texto_left">
														      		<f:facet name="header"><h:outputText value=" Id de Venta "  title="Id de Venta"/></f:facet>
														      		<h:outputText value="#{registro.copaNvalor4} - #{registro.copaIdParametro}"/>
                                                                </rich:column>
                                                                
                                                                <rich:column title="Tipo Carga" width="20%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Tipo Carga "  title="Tipo Carga"/></f:facet>
														      		<h:outputText value="#{registro.copaVvalor3}"/>
                                                                </rich:column>
                                                                
														      	
														      	<rich:column title="Ult. Carga Ventas" width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value="Ult. Carga Ventas "  title="Ult. Carga Ventas"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor1}">
														      			<f:convertDateTime pattern="dd/MM/yyyy" />
														      		</h:outputText>
														      	</rich:column>
														      	
														      	<rich:column title="Ultima Carga de Arch Canc" width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Ult Carga Canc "  title="Ult Carga Canc"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor2}">
														      			<f:convertDateTime pattern="dd/MM/yyyy" />
														      		</h:outputText>
														      	</rich:column>
														      	
														        <rich:column title="Ult. Emision" width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Ult. Emision "  title="Ult. Emision"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor3}">
														      			<f:convertDateTime pattern="dd/MM/yyyy" />
														      		</h:outputText>
														      	</rich:column>
														      	
														        <rich:column title="Ult. Cancelacion" width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Ult. Cancelacion "  title="Ult. Cancelacion"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor4}">
														      			<f:convertDateTime pattern="dd/MM/yyyy" />
														      		</h:outputText>
														      	</rich:column>
                                                                
                                                                <rich:column title="Ultima Carga Workflow" width="10%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Ult Carga Workf "  title="Ult Carga Workf"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor5}">
														      			<f:convertDateTime pattern="dd/MM/yyyy" />
														      		</h:outputText>
														      	</rich:column>
														      	
													      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()" action="#{beanConciliacion.consultarRutas}">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanConciliacion.objActual}" />
                        											    <f:setPropertyActionListener value="#{row}" target="#{beanListaProducto.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>

													      		<f:facet name="footer">
													      			<rich:datascroller align="right" for="dsConciliacion" maxPages="10" page="#{beanListaRecibo.numPagina}"/>
													      		</f:facet>
											                	
															</rich:dataTable>
															</c:if>
														</td>
													</tr>
												</table>
												
												
												</a4j:outputPanel>
												
											</fieldset>

										</td>
									</tr>
				    		</table> 
			    		   
				        </h:form>
				        
</a4j:region>


<rich:modalPanel id="newPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Nueva Configuración" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevaConciliacion">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                   
                      <h:panelGrid columns="2">
	                    
                           <rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							
							<h:outputText value="Ramo: "/>
	                    	<h:selectOneMenu id="cmbRamo" value="#{beanConciliacion.objNuevo.copaNvalor6}"  binding="#{beanListaParametros.inRamo}"  required="true" requiredMessage="* RAMO - Campo Requerido">
	                        	     <f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
  								    <f:selectItems value="#{beanListaRamo.listaComboRamos}" />
  								     <a4j:support event="onchange"  action="#{beanListaParametros.cargaIdVenta}" ajaxSingle="true" reRender="idVenta" />
							</h:selectOneMenu>
							
							
							<h:outputText value=" "/>
                            <h:selectOneMenu id="inpoliza"   value="0"  binding = "#{beanListaParametros.inPoliza}" required="true" requiredMessage="*  Tipo de Carga- Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="Prima Unica"/>
                           </h:selectOneMenu>
							
							

						    <h:outputText value="Canal de Venta: "/>
			                <h:selectOneMenu id="idVenta"  value="#{beanConciliacion.objNuevo.copaNvalor4}"  
							             disabled="#{beanListaParametros.habilitaComboIdVenta}">
						                <f:selectItem itemValue="0" itemLabel="Seleccione una canal de venta"/>
							            <f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
							
						    </h:selectOneMenu>
							
                            <h:outputText value="Sucursal: "/>
	                    	<h:inputText  id="Canal" value="#{beanConciliacion.objNuevo.copaNvalor8}" style="width: 90px" maxlength="3" required="true" requiredMessage="* CANAL DE VENTA - Campo Requerido"/>
							
							<h:outputText value="Fecha valida: "/>
                            <h:selectOneRadio id ="Fechavalida" value="#{beanConciliacion.objNuevo.copaVvalor2}" >
                            <f:selectItem itemValue="1" itemLabel="Valida" />
                            <f:selectItem itemValue="2" itemLabel="Invalida" />
                            </h:selectOneRadio>	   		
							
							<h:outputText value="Tipo de Carga: "/>
                            <h:selectOneMenu id="tpoCarga"   value="#{beanConciliacion.objNuevo.copaVvalor3}" required="true" requiredMessage="*  Tipo de Carga- Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
	                        	<f:selectItem itemValue="CARGA_AUT" itemLabel="   CARGA_AUTOMATICA  "/>
  								<f:selectItem itemValue="CARGA_MAN" itemLabel="   CARGA_MANUAL  "/>
							</h:selectOneMenu>
	                    	
	                    	<h:outputText value=" Correo 1: "/>
	                    	<h:inputText  id="Correo1" value="#{beanConciliacion.objNuevo.copaVvalor5}" style="width: 250px" maxlength="75" required="true" requiredMessage="* CORREO 1 - Campo Requerido"/>
	                    	<rich:toolTip for="Correo1" value="Si quieres mas de un Correo Separar con ;" />
	                    	<h:outputText value=" "/>
	                    	<h:outputText value="Correo 2: "/>
                            <h:inputText  id="Correo2" value="#{beanConciliacion.objNuevo.copaVvalor7}" style="width: 250px" maxlength="75" required="true" requiredMessage="* CORREO 2 - Campo Requerido"/>
	                    	<rich:toolTip for="Correo2" value="Si Requieres mas de un Correo Separar con ;" />
	                    	
	                    	<h:outputText value=" "/>
							<h:outputText value=" Mas - Menos "/>
	                    	<h:inputText   id="MoM" value="#{beanConciliacion.objNuevo.copaNvalor1}" style="width: 90px" maxlength="50" required="true" requiredMessage="*  PASSWORD - Campo Requerido"/>
							
						    <h:outputText value="Tipo de Conciliacion: "/>
                            <h:selectOneMenu id="tpoConciliacion"   value="#{beanConciliacion.objNuevo.copaNvalor2}" required="true" requiredMessage="*  Tipo de Conciliacion- Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
	                        	<f:selectItem itemValue="1" itemLabel="   CREDITO "/>
  								<f:selectItem itemValue="2" itemLabel="   CONCILIACION X MONTO  "/>
							</h:selectOneMenu>
							
							<h:outputText value="Proceso de Emision: "/>
                             <h:selectOneRadio  id="Ejecucion" value="#{beanConciliacion.objNuevo.copaNvalor3}" >
                               <f:selectItem itemValue="1" itemLabel=" Si Ejecutar " />
                               <f:selectItem itemValue="0" itemLabel=" No Ejecutar" />
                           </h:selectOneRadio>	  
                          <!--  
                           <h:outputText value=" ID "/>
	                    	<h:inputText  id="ID" value="#{beanConciliacion.objNuevo.copaNvalor4}" style="width: 90px" maxlength="50" required="true" requiredMessage="*  PASSWORD - Campo Requerido"/>
							
							<h:outputText value=" NVALOR5 "/>
	                    	<h:inputText  id="NValor5" value="#{beanConciliacion.objNuevo.copaNvalor5}" style="width: 90px" maxlength="50" required="true" requiredMessage="*  PASSWORD - Campo Requerido"/>
							--> 
                            <h:outputText value="Proceso de Cancelación : "/>
                               <h:selectOneRadio id="procCanc" value="#{beanConciliacion.objNuevo.copaNvalor7}" >
                                  <f:selectItem itemValue="1" itemLabel="Si Ejecutar " />
                                  <f:selectItem itemValue="2" itemLabel="No Ejecutar" />
                            </h:selectOneRadio>	 
                            
                            <h:outputText value="Ejecuta Reproceso: "/>
                               <h:selectOneRadio id="EjecRepro" value="#{beanConciliacion.objNuevo.copaNvalor8}" >
                                  <f:selectItem itemValue="1" itemLabel="Si Ejecutar " />
                                  <f:selectItem itemValue="2" itemLabel="No Ejecutar" />
                            </h:selectOneRadio>	
   
                          <rich:separator height="4" lineType="double" />
						  <rich:separator height="4" lineType="double"/>
							
	                      <rich:spacer height="10px" />
						  <rich:spacer height="10px" />
                          
                          
                          <h:outputText value=" Archivos a Cargar: "/>
	                      <h:inputText value="#{beanConciliacion.objNuevo.copaRegistro}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Archivos a Cargar - Campo Requerido"/> 
                          
                          <h:outputText value="Usuario|Password: "/>
	                      <h:inputText  id="User" value= "#{beanConciliacion.objNuevo.copaVvalor6}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                          <h:outputText value=" "/> 
                          <rich:toolTip for="User" value="Usuario Y Password separar PIPE |" />
                          
                          
                           <h:outputText value=" Para Copiar : "/>
                           <h:inputText  id="RutaArch"   value="#{beanConciliacion.objNuevo.copaRegistro1}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Para Copiar - Campo Requerido"/>
                          
                          <h:outputText value="Usuario|Password: "/>
	                      <h:inputText  id="User1" value= "#{beanConciliacion.objNuevo.usuario1}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                          <h:outputText value=" "/> 
                          <rich:toolTip for="User1" value="Usuario y Password separar PIPE |" />
                          
                          <h:outputText value=" Para Reproceso : "/>
			              <h:inputText value="#{beanConciliacion.objNuevo.copaRegistro2}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Para Reproceso - Campo Requerido"/> 
                          
                          <h:outputText value="Usuario|Password: "/>
	                      <h:inputText  id="User2" value= "#{beanConciliacion.objNuevo.usuario2}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                          <h:outputText value=" "/> 
                          <rich:toolTip for="User2" value="Usuario y Password separar PIPE |" />
                          
                          <h:outputText value=" Para Errores : "/>
			              <h:inputText value="#{beanConciliacion.objNuevo.copaRegistro3}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Para Errores - Campo Requerido"/> 
                           
                           <h:outputText value="Usuario|Password: "/>
	                       <h:inputText  id="User3" value= "#{beanConciliacion.objNuevo.usuario3}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                           <h:outputText value=" "/> 
                           <rich:toolTip for="User3" value="Usuario y Password separar PIPE |" />
                          
                     </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanConciliacion.altaConciliacion}" reRender="mensaje,regTabla,dsConciliacion"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
 </rich:modalPanel>
 
 <rich:modalPanel id="editPanel"  width="400" height="700">
        	<f:facet name="header"><h:outputText value="Editar Configuración" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick"  />
	               
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="editarConciliacion">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                        <h:outputText value="Clave: "/>
	                    	<h:outputText value="#{beanConciliacion.objActual.copaCdParametro}" styleClass="texto_mediano"/>
	                    	
	                    	<h:outputText value="Canal: "/>
	                    	<h:outputText value="#{beanConciliacion.objActual.copaNvalor8}" styleClass="texto_mediano">
                              <f:convertNumber  integerOnly="true" />
                            </h:outputText>
	                    	
	                    	<h:outputText value="Ramo: "/>
	                    	<h:outputText value="#{beanConciliacion.objActual.copaNvalor6}" styleClass="texto_mediano"/>
	                    	
	                    	<h:outputText value="Canal de Venta: "/>
	                    	<h:outputText value="#{beanConciliacion.objActual.copaNvalor4} - #{beanConciliacion.objActual.copaIdParametro}" styleClass="texto_mediano"/>
	                    	
	        
							
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							<h:outputText value="Fecha valida: "/>
                            <h:selectOneRadio id ="Fechavalida" value="#{beanConciliacion.objActual.copaVvalor2}" >
                            <f:selectItem itemValue="1" itemLabel="Valida" />
                            <f:selectItem itemValue="2" itemLabel="Invalida" />
                            </h:selectOneRadio>	   		
							
							<h:outputText value="Tipo de Carga: "/>
                            <h:selectOneMenu id="tpoCarga"   value="#{beanConciliacion.objActual.copaVvalor3}" required="true" requiredMessage="*  Tipo de Carga- Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
	                        	<f:selectItem itemValue="CARGA_AUT" itemLabel="   CARGA_AUTOMATICA  "/>
  								<f:selectItem itemValue="CARGA_MAN" itemLabel="   CARGA_MANUAL  "/>
							</h:selectOneMenu>
	                    	
	                    	<h:outputText value=" Correo 1: "/>
	                    	<h:inputText  id="Correo1" value="#{beanConciliacion.objActual.copaVvalor5}" style="width: 250px" maxlength="75" required="true" requiredMessage="* CORREO 1 - Campo Requerido"/>
	                    	<rich:toolTip for="Correo1" value="Si quieres mas de un Correo Separar con ;" />
	                    	<h:outputText value=" "/>
	                    	<h:outputText value="Correo 2: "/>
                            <h:inputText  id="Correo2" value="#{beanConciliacion.objActual.copaVvalor7}" style="width: 250px" maxlength="75" required="true" requiredMessage="* CORREO 2 - Campo Requerido"/>
	                    	<rich:toolTip for="Correo2" value="Si Requieres mas de un Correo Separar con ;" />
	                    	
	                    	<h:outputText value=" "/>
							<h:outputText value=" Mas - Menos "/>
	                    	<h:inputText   id="MoM" value="#{beanConciliacion.objActual.copaNvalor1}" style="width: 90px" maxlength="50" required="true" requiredMessage="*  Mas - Menos - Campo Requerido"/>
							
						    <h:outputText value="Tipo de Conciliacion: "/>
                            <h:selectOneMenu id="tpoConciliacion"   value="#{beanConciliacion.objActual.copaNvalor2}" required="true" requiredMessage="*  Tipo de Conciliacion- Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
	                        	<f:selectItem itemValue="1" itemLabel="   CREDITO "/>
  								<f:selectItem itemValue="2" itemLabel="   CONCILIACION X MONTO  "/>
							</h:selectOneMenu>
							
							<h:outputText value="Proceso de Emision: "/>
                             <h:selectOneRadio  id="Ejecucion" value="#{beanConciliacion.objActual.copaNvalor3}" >
                               <f:selectItem itemValue="1" itemLabel=" Si Ejecutar " />
                               <f:selectItem itemValue="0" itemLabel=" No Ejecutar" />
                           </h:selectOneRadio>	  

                            <h:outputText value="Proceso de Cancelación : "/>
                               <h:selectOneRadio id="procCanc" value="#{beanConciliacion.objActual.copaNvalor7}" >
                                   <f:convertNumber  integerOnly="true" />
                                  <f:selectItem itemValue="1" itemLabel="Si Ejecutar " />
                                  <f:selectItem itemValue="2" itemLabel="No Ejecutar" />
                            </h:selectOneRadio>	 
                            
                            <h:outputText value="Ejecuta Reproceso: "/>
                               <h:selectOneRadio id="EjecRepro" value="#{beanConciliacion.objActual.copaNvalor8}" >
                                   <f:convertNumber  integerOnly="true" />
                                  <f:selectItem itemValue="1" itemLabel="Si Ejecutar " />
                                  <f:selectItem itemValue="2" itemLabel="No Ejecutar" />
                            </h:selectOneRadio>	
							
						  <rich:separator height="4" lineType="double" />
						  <rich:separator height="4" lineType="double"/>
							
	                      <rich:spacer height="10px" />
						  <rich:spacer height="10px" />	
							
						  <h:outputText value=" Archivos a Cargar: "/>
	                      <h:inputText value="#{beanConciliacion.objActual.copaRegistro}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Archivos a Cargar - Campo Requerido"/> 
                          
                          <h:outputText value="Usuario|Password: "/>
	                      <h:inputText  id="User" value= "#{beanConciliacion.objActual.copaVvalor6}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                          <h:outputText value=" "/> 
                          <rich:toolTip for="User" value="Usuario Y Password separar PIPE |" />
                          
                          <h:outputText value=" Para Copiar : "/>
                          <h:inputText  id="RutaArch"   value="#{beanConciliacion.objActual.copaRegistro1}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Para Copiar - Campo Requerido"/>
                          
                          <h:outputText value="Usuario|Password: "/>
	                      <h:inputText  id="User1" value= "#{beanConciliacion.objActual.usuario1}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                          <h:outputText value=" "/> 
                          <rich:toolTip for="User1" value="Usuario y Password separar PIPE |" />
                          
                          <h:outputText value=" Para Reproceso : "/>
			              <h:inputText value="#{beanConciliacion.objActual.copaRegistro2}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Para Reproceso - Campo Requerido"/> 
                          
                          <h:outputText value="Usuario|Password: "/>
	                      <h:inputText  id="User2" value= "#{beanConciliacion.objActual.usuario2}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                          <h:outputText value=" "/> 
                          <rich:toolTip for="User2" value="Usuario y Password separar PIPE |" />
                          
                          <h:outputText value=" Para Errores : "/>
			              <h:inputText value="#{beanConciliacion.objActual.copaRegistro3}" style="width: 250px" maxlength="50" required="true" requiredMessage="*  Para Errores - Campo Requerido"/> 
                           
                           <h:outputText value="Usuario|Password: "/>
	                       <h:inputText  id="User3" value= "#{beanConciliacion.objActual.usuario3}" style="width: 150px" maxlength="50" required="true" requiredMessage="* USUARIO- Campo Requerido"/>
                           <h:outputText value=" "/> 
                           <rich:toolTip for="User3" value="Usuario y Password separar PIPE |" />
                          
                          
							
							
                   	                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanConciliacion.modificar}" reRender="mensaje,regTabla,dsConciliacion"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
 
 
 
 

 
 
 <a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
  </rich:modalPanel>
 



