<%@ include file="include.jsp"%>
	<table cellpadding="2" cellspacing="0">
		<h:inputHidden id="idProducto" value="#{beanParametrizacion.id.alprCdProducto}" />
		<tbody>
			<tr>
				<td align="right" width="24%" height="30"><h:outputText value="Descripcion Plan :" /></td>
				<td align="left" width="76%" height="30">
					<h:inputText id="alplDePlan" value="#{beanParametrizacion.alplDePlan}" maxlength="100" size="50"/>
					<h:message for="alplDePlan" styleClass="errorMessage" />																			
				</td>
			</tr>
			<tr>
				<td align="right" width="24%" height="30"><h:outputText value="Plazo :"/></td>
				<td align="left" width="76%" height="30">	
					<h:selectOneMenu id="plazo" value="#{beanParametrizacion.idPlan.alplCdPlan}" >
						<f:selectItem itemValue="" itemLabel="Selecione un plan" value=""/>
						<f:selectItems value="#{beanListaPlanes.listaComboPlanes}" />
					</h:selectOneMenu>
				</td>
			</tr>
			<tr>
				<td align="right" width="24%" height="30"><h:outputText value="Centro de Costos :" /></td>
				<td align="left" width="76%" height="30">
					<h:inputText id="cCostos" value="#{beanParametrizacion.alplDato3}" maxlength="10" size="15"/>
					<h:message for="cCostos" styleClass="errorMessage" />																			
				</td>
			</tr>
			<tr>																							
			<tr>
				<td align="right" width="24%" height="30"></td>
				<td align="left" width="76%" height="30">
					<a4j:commandButton styleClass="boton" value="Guardar" id="bPlan"
					   action="#{beanParametrizacion.altaPlan}" 
					   onclick="this.disabled=true" oncomplete="this.disabled=false" 
					   reRender="barra, lista"
					   title="header=[Carga y emisi�n masiva] body=[Ejecuta el proceso de carga y emisi�n, el tiempo de procesamiento depender� del archivo cargado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>	   
				</td>
			</tr>
		</tbody>
	</table>
