<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Timbrado
- Fecha: 09/10/2020
- Descripcion: Modal para nuevo timbrado
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="newPanel" autosized="true" height="400" width="500">
	<div class="scroll-div">
   	<f:facet name="header"><h:outputText value="Nuevo Timbrado" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkN" styleClass="hidelink" />
            <rich:componentControl for="newPanel" attachTo="hidelinkN" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>

	<h:form id="nuevoTimbrado">
        <rich:messages styleClass="errorMessage"></rich:messages>
        <h:panelGrid columns="1" id="panPrincipal">
            <a4j:outputPanel ajaxRendered="true">
               	<h:panelGrid columns="10">
               		<h:outputText value="#{msgs.sistemaramo}:"/>
               		<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCarpCdRamo}" styleClass="wid30"/>
					<h:outputText value="#{msgs.sistemapoliza}:"/>
					<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCapoNuPoliza}" styleClass="wid80"/>
					<h:outputText value="#{msgs.sistemacerti}:"/>
					<h:inputText value="#{beanTimbrado.nuevo.recibo.coreCaceNuCertificado}" styleClass="wid80"/>
					<h:outputText value="#{msgs.timbradorecibo}:"/>
					<h:inputText value="#{beanTimbrado.nuevo.factura.cofaNuReciboFiscal}" styleClass="wid100"/>
					<h:outputText value="#{msgs.timbradoan}:"/>
					<h:inputText value="#{beanTimbrado.nuevo.factura.cofaCampon2}" styleClass="wid50"/>
					
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<h:graphicImage value="../../images/espacio.png"/>
					<a4j:commandButton id="btnConsultaN"
							styleClass="boton" value="#{msgs.botonconsultar}"
							action="#{beanTimbrado.consultaHistorial}"
							onclick="this.disabled=true;bloqueaPantalla();"
							oncomplete="this.disabled=false;desbloqueaPantalla();muestraOcultaSeccion('resultado', 1)"
							reRender="panPrincipal"/>
               	</h:panelGrid>
					
				<rich:spacer height="10px" />
				<rich:dataTable id="listaRecibos" value="#{beanTimbrado.listaHist}"
						var="historial" rows="20" width="100%">
					<f:facet name="header">
						<h:outputText value="#{msgs.timbradolr}" />
					</f:facet>
	
					<rich:column styleClass="texto_centro" >
						<f:facet name="header"><h:outputText value="#{msgs.timbradorecibo}" /></f:facet>
						<h:outputText value="#{historial.factura.cofaNuReciboFiscal}" />
					</rich:column>
					<rich:column styleClass="texto_centro" >
						<f:facet name="header"><h:outputText value="#{msgs.timbradocuota}" /></f:facet>
						<h:outputText value="#{historial.recibo.coreNuConsecutivoCuota}" />
					</rich:column>							
					<rich:column styleClass="texto_centro" >
						<f:facet name="header"><h:outputText value="#{msgs.timbradofeemi}" /></f:facet>
						<h:outputText value="#{historial.recibo.coreFeEmision}" >
							<f:convertDateTime pattern="dd/MM/yyyy" />
						</h:outputText>
					</rich:column>
					<rich:column styleClass="texto_centro" >
						<f:facet name="header"><h:outputText value="#{msgs.sistemafechad}" /></f:facet>
						<h:outputText value="#{historial.recibo.coreFeDesde}" >
							<f:convertDateTime pattern="dd/MM/yyyy" />
						</h:outputText>
					</rich:column>
					<rich:column styleClass="texto_centro" >
						<f:facet name="header"><h:outputText value="#{msgs.sistemafechah}" /></f:facet>
						<h:outputText value="#{historial.recibo.coreFeHasta}" >
							<f:convertDateTime pattern="dd/MM/yyyy" />
						</h:outputText>
					</rich:column>
					<rich:column styleClass="texto_centro" >
						<f:facet name="header"><h:outputText value="#{msgs.timbradoestsis}" /></f:facet>
						<h:outputText value="#{historial.factura.cofaNuCuenta}" />									
					</rich:column>
					<rich:column styleClass="texto_centro">
						<f:facet name="header"><h:outputText value="#{msgs.timbradoestcob}" /></f:facet>
						<h:outputText value="#{historial.factura.cofaNuCuentaDep}" />
					</rich:column>
					<rich:column styleClass="texto_centro">
						<f:facet name="header"><h:outputText value="#{msgs.sistemapmatot}" /></f:facet>
						<h:outputText value="#{historial.recibo.coreMtPrima}" >
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:outputText>
					</rich:column>
					<rich:column styleClass="texto_centro">
						<f:facet name="header"><h:outputText value="#{msgs.sistemadetalle}" /></f:facet>
						<a4j:commandLink ajaxSingle="true" 
									id="detallelink" 
									reRender="panPrincipal" 
									action="#{beanTimbrado.consultaDatosTimbrado}"
									onclick="bloqueaPantalla();" 
									oncomplete="#{rich:component('datPanel')}.show();desbloqueaPantalla();muestraOcultaSeccion('resultado', 1)">
							<h:graphicImage value="../../images/certificados.png" styleClass="bor0"/>                        												
							<f:setPropertyActionListener value="#{historial}" target="#{beanTimbrado.nuevo}" />
						</a4j:commandLink>
						<rich:toolTip for="detallelink" value="Ver Detalle" />
                	</rich:column>
				</rich:dataTable>					
            </a4j:outputPanel>          
        </h:panelGrid>    
    </h:form>    
    </div>
 </rich:modalPanel>