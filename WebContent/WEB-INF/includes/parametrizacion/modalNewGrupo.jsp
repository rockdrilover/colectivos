<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Grupos Correo
- Fecha: 09/10/2020
- Descripcion: Modal para nuevo grupo
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="newPanel" autosized="true" width="400">
   	<f:facet name="header"><h:outputText value="Nuevo Grupo" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
            <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>
    <h:form id="nuevoGrupo">
        <rich:messages styleClass="errorMessage"></rich:messages>
        <h:panelGrid columns="1">
            <a4j:outputPanel ajaxRendered="true">
                <h:panelGrid columns="1">
                	<h:panelGrid columns="2">
			<rich:separator height="4" lineType="double" />
			<rich:separator height="4" lineType="double"/>
			
			<rich:spacer height="10px" />
			<rich:spacer height="10px" />
		
			<h:outputText value="#{msgs.grupos}: "/>
			<h:inputText value="#{beanGrupos.objNuevo.copaVvalor8}" styleClass="wid290" maxlength="80"/>
			
			<h:outputText value="#{msgs.gruposcorreo}: "/>
                     <h:inputText value="#{beanGrupos.correo}" styleClass="wid290" required="false" />
			
			<rich:spacer height="10px" />	                
             		<a4j:commandButton value="Agregar" action="#{beanGrupos.agregar}" reRender="listaAddCorreos" onclick="this.disabled=true" oncomplete="this.disabled=false;muestraOcultaSeccion('resultados', 1)" />
            		</h:panelGrid>
		
		<rich:spacer height="10px" />

		<rich:dataTable id="listaAddCorreos" value="#{beanGrupos.lstCorreos}"																					
				columnsWidth="90px,10px" headerClass="detalleEncabezado2" styleClass="detalleEncabezado"
				var="beanCorreo" rows="10" width="400">
			<f:facet name="header">
				<h:outputText value="Correos Agregados" />
			</f:facet>
																					
			<rich:column>								
				<h:outputText value="#{beanCorreo}" />
			</rich:column>
																												
			<rich:column title="Quitar" width="5%" styleClass="texto_centro">								
				<a4j:commandLink styleClass="boton" id="btnQuitar"
					action="#{beanGrupos.quitar}"
					onclick="this.disabled=true" oncomplete="this.disabled=false;muestraOcultaSeccion('resultados', 1)"
					reRender="listaAddCorreos">
					<f:setPropertyActionListener
						value="#{beanCorreo}"
						target="#{beanGrupos.correo}" />
					<h:graphicImage value="../../images/obligado_eliminar.png" styleClass="bor0" />
				</a4j:commandLink>
				<rich:toolTip for="editlink" value="Quitar" />
              	</rich:column>																								
		</rich:dataTable>
             
             	<rich:spacer height="10px" />
		
		<h:panelGrid columns="2">
            <h:outputText value="#{msgs.gruposasunto}: "/>
			<h:inputText value="#{beanGrupos.objNuevo.copaVvalor1}" styleClass="wid280" maxlength="80"/>
                     	                       
            <h:outputText value="#{msgs.gruposmsg}: "/>
			<h:inputTextarea value="#{beanGrupos.objNuevo.copaVvalor7}" styleClass="text-area"/>
			
			<rich:spacer height="10px" />
			<rich:spacer height="10px" />
			
			<a4j:commandButton 
               	value="Guardar"
                   action="#{beanGrupos.guardar}" 
                   reRender="mensaje,regTabla,dsGrupos"	                    
                   oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();muestraOcultaSeccion('resultados', 0)" />
			    
               <a4j:commandButton 
               	value="Cancelar"
			    action="#{beanGrupos.cancelar}"
			    reRender="mensaje,regTabla,dsGrupos"
			   	onclick="#{rich:component('newPanel')}.hide();"	                    
			    oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('newPanel')}.hide(); } else { #{rich:component('newPanel')}.show(); }muestraOcultaSeccion('resultados', 0)" />
                	</h:panelGrid>
                </h:panelGrid>
            </a4j:outputPanel>
        </h:panelGrid>
    </h:form>
</rich:modalPanel>