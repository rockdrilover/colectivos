<%@ include file="include.jsp"%>
<!-- Inicio RESULTADOS -->
<table width="95%" border="0" cellpadding="0" cellspacing="0"
	align="center">
	<tr>
		<td>
			<fieldset style="border: 1px solid white">
				<legend>
					<a4j:outputPanel id="secRes" ajaxRendered="true">
						<span id="img_sec_resultados1_m" style="display: none;"> <h:graphicImage
								value="../../images/mostrar_seccion.png"
								onclick="muestraOcultaSeccion('resultados1', 0);" title="Oculta" />&nbsp;
						</span>
						<span id="img_sec_resultados1_o"> <h:graphicImage
								value="../../images/ocultar_seccion.png"
								onclick="muestraOcultaSeccion('resultados1', 1);"
								title="Muestra" />&nbsp;
						</span>
						<span class="titulos_secciones">Resultados Componentes</span>
					</a4j:outputPanel>
				</legend>

				<a4j:outputPanel id="regTabla1" ajaxRendered="true">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr class="texto_normal" id="tr_sec_resultados1"
							style="display: none;">
							<td><c:choose>
									<c:when
										test="${fn:length(beanParametrizacion.listaColComponentes) == 0}">
										<table width="90%" align="center" border="0">
											<tr>
												<td>NO SE ENCONTRO INFORMACION PARA EL FILTRO
													SELECCIONADO.</td>
											</tr>
										</table>
									</c:when>
									<c:otherwise>
										<rich:contextMenu attached="false" id="menu1"
											submitMode="ajax"
											oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
											<rich:menuItem value="Edit Record" ajaxSingle="true"
												oncomplete="#{rich:component('editPanel')}.show()"
												actionListener="#{beanListaProducto.buscarFilaActual}">
												<a4j:actionparam name="clave" value="{clave}" />
												<a4j:actionparam name="row" value="{filaActual}" />
											</rich:menuItem>
										</rich:contextMenu>

										<rich:datascroller for="dsProducto1" maxPages="10" />
										<rich:dataTable
											value="#{beanParametrizacion.listaColComponentes}"
											id="dsProducto1" var="componente" rows="10" rowKeyVar="row"
											columns="10" width="90%" align="center">

											<f:facet name="header">
												<h:outputText value="Componentes" />
											</f:facet>


											<rich:column title="Componente" width="20%"
												styleClass="texto_centro"
												sortBy="#{componente.id.coctCappCdComponente}">
												<f:facet name="header">
													<h:outputText value=" Componente " title="Componente" />
												</f:facet>
												<h:outputText value="#{componente.id.coctCappCdComponente}" />
											</rich:column>


											<rich:column title="%" width="5%" styleClass="texto_centro"
												sortBy="#{componente.coctTaComponente}">
												<f:facet name="header">
													<h:outputText value=" % " title="%" />
												</f:facet>
												<h:outputText value="#{componente.coctTaComponente}" />
											</rich:column>

											<rich:column title=" Fecha Desde " width="20%">
												<f:facet name="header">
													<h:outputText value=" Fecha Desde " title="Fecha Desde" />
												</f:facet>
												<h:outputText value="#{componente.coctFeDesde}">
													<f:convertDateTime type="date" pattern="dd/MM/yyyy" />
												</h:outputText>
											</rich:column>

											<rich:column title=" Fecha Hasta " width="20%">
												<f:facet name="header">
													<h:outputText value=" Fecha Hasta " title="Fecha Hasta" />
												</f:facet>
												<h:outputText value="#{componente.coctFeHasta}">
													<f:convertDateTime type="date" pattern="dd/MM/yyyy" />
												</h:outputText>
											</rich:column>

											<rich:column title=" Estatus " width="20%">
												<f:facet name="header">
													<h:outputText value=" Estatus " title="Estatus" />
												</f:facet>
												<h:outputText value="#{componente.id.coctEstatus}" />
											</rich:column>

											<rich:column title="Modificar" width="5%"
												styleClass="texto_centro">
												<f:facet name="header">
													<h:outputText value=" Modificar " />
												</f:facet>
												<a4j:commandLink ajaxSingle="true" id="editlink"
													oncomplete="#{rich:component('componentePanel')}.show()"
													reRender="mensaje, mensaje1, regTabla3"
													action="#{beanComponentes.consultaEdicion}">
													<h:graphicImage value="../../images/edit_page.png"
														style="border:0" />
													<f:setPropertyActionListener value="#{beanParametrizacion}"
														target="#{beanComponentes.bean}" />

													<f:setPropertyActionListener value="#{componente}"
														target="#{beanComponentes.editar}" />
												</a4j:commandLink>
												<rich:toolTip for="editlink" value="Editar" />
											</rich:column>
										</rich:dataTable>

									</c:otherwise>
								</c:choose> <a4j:commandButton ajaxSingle="true" id="editlink3"
									oncomplete="#{rich:component('componentePanel')}.show()"
									action="#{beanComponentes.consulta}" styleClass="boton"
									value="Asociar">
									<f:setPropertyActionListener value="#{beanParametrizacion}"
										target="#{beanComponentes.bean}" />
								</a4j:commandButton></td>
						</tr>
					</table>
				</a4j:outputPanel>
			</fieldset>
		</td>
	</tr>
</table>