<%@include file="include.jsp"%>

<rich:modalPanel id="newPanel" autosized="true" width="450">
	<f:facet name="header">
		<h:outputText value="Nueva Cobertura" />
	</f:facet>
	<f:facet name="controls">
		<h:panelGroup>
			<h:graphicImage value="../../images/cerrarModal.png" id="hidelink_2"
				styleClass="hidelink" />
			<rich:componentControl for="newPanel" attachTo="hidelink_2"
				operation="hide" event="onclick" />
		</h:panelGroup>
	</f:facet>
	<h:form id="newCobertura">
		<rich:messages style="color:red;"></rich:messages>

		<h:panelGrid columns="1" width="100%">
			<a4j:outputPanel ajaxRendered="true">
				<h:panelGrid columns="5" width="100%" cellspacing="0"
					cellpadding="1">
					<h:outputText value="Ramo: " />
					<h:outputText value="#{beanCoberturas.bean.ramo.carpCdRamo}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="Poliza: " />
					<h:outputText value="#{beanCoberturas.bean.polizaUnica}"
						styleClass="texto_mediano" />

					<h:outputText value="Producto: " />
					<h:outputText value="#{beanCoberturas.bean.id.alprCdProducto}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="Plan: " />
					<h:outputText value="#{beanCoberturas.bean.idPlan.alplCdPlan}"
						styleClass="texto_mediano" />

					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />

					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />

				</h:panelGrid>

				<h:panelGrid columns="1" width="100%" cellspacing="0"
					cellpadding="1">
					<h:outputText value=" Datos para Catalogo "
						styleClass="texto_mediano" />

					<rich:spacer height="7px" />
				</h:panelGrid>

				<h:panelGrid columns="3" width="100%" cellspacing="0"
					cellpadding="1">

					<h:outputText value="Ramo Contable: " />
					<h:selectOneMenu id="comboCob"
						value="#{beanCoberturas.idCartCoberturas.cacbCarbCdRamo}"
						required="true" requiredMessage="RAMO CONTABLE: Valor Requerido">
						<f:selectItem itemValue="" itemLabel="Seleccione un ramo contable" />
						<f:selectItems value="#{beanListaRamoContable.comboRamoContable}" />
					</h:selectOneMenu>
					<h:outputText value="      " />
				</h:panelGrid>

				<h:panelGrid columns="3" width="100%" cellspacing="0"
					cellpadding="1">

					<h:outputText value="Clave Cobertura: " />
					<h:inputText
						value="#{beanCoberturas.idCartCoberturas.cacbCdCobertura}"
						styleClass="texto_mediano" required="true"
						requiredMessage="CLAVE COBERTURA: Valor Requerido" />
					<h:outputText value="      " />

					<h:outputText value="Descripcion: " />
					<h:inputText
						value="#{beanCoberturas.cartCoberturas.cacbDeCobertura}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />

					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />

					<rich:spacer height="7px" />
					<rich:spacer height="7px" />
					<rich:spacer height="7px" />
				</h:panelGrid>

				<h:panelGrid columns="1" width="100%" cellspacing="0"
					cellpadding="1">
					<h:outputText value=" Datos para Colectivos "
						styleClass="texto_mediano" />

					<rich:spacer height="7px" />
				</h:panelGrid>

				<h:panelGrid columns="3" width="100%" cellspacing="0"
					cellpadding="1">
					<!--  -->
					<h:outputText value="Fecha Inicial ++: " />
					<rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy"
						required="true" value="#{beanCoberturas.nwCoberturas.cocbFeDesde}"
						inputSize="8"
						onchanged="setFecha('newCobertura:fechaFinalInputDate',this.value);">
						<%-- a4j:support event="onchanged" oncomplete="setFecha('newCobertura:fechaFinalInputDate','#{beanCoberturas.nwCoberturas.cocbFeDesde}');"  reRender="fechaFinal" ajaxSingle="true"/--%>
					</rich:calendar>
					<h:outputText value="      " />

					<h:outputText value="Fecha Final: " />
					<rich:calendar id="fechaFinal" datePattern="dd/MM/yyyy"
						value="#{beanCoberturas.nwCoberturas.cocbFeHasta}" inputSize="8" />
					<h:outputText value="      " />

					<h:outputText value="Edad Minima: " />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbEdadMinima}"
						styleClass="texto_mediano" required="true"
						requiredMessage="EDAD MINIMA: Valor requerido" />
					<h:outputText value="      " />

					<h:outputText value="Edad Maxima: " />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbEdadMaxima}"
						styleClass="texto_mediano" required="true"
						requiredMessage="EDAD MAXIMA: Valor requerido" />
					<h:outputText value="      " />

					<h:outputText value="Edad Minima Renovacion: " />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbCampon2}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />

					<h:outputText value="Edad Maxima Renovacion: " />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbCampon3}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />

					<h:outputText value="T. Riesgo: " />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbTaRiesgo}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />

					<h:outputText value="Tipo Comision: " />

					<h:selectOneMenu id="cosuExamMedico"
						value="#{beanCoberturas.nwCoberturas.tipoComision}"
						onchange="ocultaComisiones(this.value, 'newCobertura:panelComisiones', 'newCobertura:txtComTecnica', 'newCobertura:txtComContigente');">

						<f:selectItem itemValue="P" itemLabel="Producto / Plan" />
						<f:selectItem itemValue="C" itemLabel="Cobertura" />


					</h:selectOneMenu>

					<h:outputText value="      " />
				</h:panelGrid>
				<h:panelGrid columns="3" width="100%" cellspacing="0"
					cellpadding="1" style="display:none;" id='panelComisiones'>
					<h:outputText value="Com. Tecnica: " id="lblComTecnica" />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbPoComision}"
						styleClass="texto_mediano" id="txtComTecnica" />
					<h:outputText value="      " />

					<h:outputText value="Com. Contingente: " id="lblComContigente" />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbMtFijo}"
						styleClass="texto_mediano" id="txtComContigente" />
					<h:outputText value="      " />
				</h:panelGrid>
				<h:panelGrid columns="3" width="100%" cellspacing="0"
					cellpadding="1">
					<h:outputText value="Tipo Cobertura: " />
					<h:selectOneMenu id="tipoCob"
						value="#{beanCoberturas.nwCoberturas.cocbSexo}">
						<f:selectItem itemValue="" itemLabel="<Seleccione una opcion>" />
						<f:selectItems value="#{beanListaParametros.listaTipoCobertura}" />
					</h:selectOneMenu>
					<h:outputText value="      " />
						

					<h:outputText value="#{msgs.cobersumafija}:" />
					<h:inputText value="#{beanCoberturas.nwCoberturas.cocbCampov2}" 
								styleClass="texto_mediano"
								title="#{msgs.cobersumamsg}" />
					<h:outputText value="      " />				
				</h:panelGrid>
			</a4j:outputPanel>
			<a4j:commandButton value="Guardar"
				action="#{beanCoberturas.guardaNuevaCobertura}"
				reRender="mensaje1,regTabla,dsProducto"
				oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />

		</h:panelGrid>
	</h:form>
</rich:modalPanel>