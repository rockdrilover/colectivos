<%@ include file="include.jsp"%>
<!-- Inicio RESULTADOS -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>
			<fieldset style="border:1px solid white">
				<legend>
					<span id="img_sec_resultados_m" style="display: none;">
						<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
					</span>
					<span id="img_sec_resultados_o">
						<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
					</span>
					<span class="titulos_secciones">Resultados</span>
				</legend>
				
				<a4j:outputPanel id="regTabla" ajaxRendered="true">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">	
						<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
							<td>
								<c:choose>
									<c:when test="${fn:length(beanParametrizacion.listaColCoberturas) == 0}">
										<table width="90%" align="center" border="0">
											<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
										</table>
									</c:when>
									<c:otherwise>															
										<rich:contextMenu attached="false" id="menu" submitMode="ajax" oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
											<rich:menuItem value="Edit Record" ajaxSingle="true" oncomplete="#{rich:component('editPanel')}.show()" actionListener="#{beanListaProducto.buscarFilaActual}">
												<a4j:actionparam name="clave" value="{clave}" />
												<a4j:actionparam name="row" value="{filaActual}" />
											</rich:menuItem>
										</rich:contextMenu>
										
										<rich:datascroller for="dsProducto" maxPages="10"/>
										<rich:dataTable value="#{beanParametrizacion.listaColCoberturas}" id="dsProducto" var="cobertura" 
																						rows="10" rowKeyVar="row" 
																						columns="10" width="90%" align="center">
										
											<f:facet name="header"><h:outputText value="Coberturas"/></f:facet>
											
											
				      						<rich:column title="Ramo Contable" width="20%" styleClass="texto_centro" sortBy="#{cobertura.id.cocbCarbCdRamo}" >
				      							<f:facet name="header"><h:outputText value=" Ramo Contable " title="Ramo Contable" /></f:facet>
													<h:outputText value="#{cobertura.id.cocbCarbCdRamo}"/>  
				      						</rich:column>
				      						
									      	<rich:column title="Clave Cobertura" width="5%" styleClass="texto_centro" sortBy="#{cobertura.id.cocbCacbCdCobertura}">
									      		<f:facet name="header"><h:outputText value=" Clave Cobertura " title="Descripcion" /></f:facet>
												<h:outputText value="#{cobertura.id.cocbCacbCdCobertura}"/>  
									      	</rich:column>
									      	
									      	<rich:column title=" Inicio Vigencia " width="20%">
									      		<f:facet name="header"><h:outputText value=" Inicio Vigencia "  title="Inicio Vigencia"/></f:facet>
									      		<h:outputText value="#{cobertura.cocbFeDesde}"><f:convertDateTime type="date" pattern="dd/MM/yyyy"/></h:outputText>
									      	</rich:column>

									      	<rich:column title=" Fin Vigencia " width="20%">
									      		<f:facet name="header"><h:outputText value=" Fin Vigencia "  title="Fin Vigencia"/></f:facet>
									      		<h:outputText value="#{cobertura.cocbFeHasta}"><f:convertDateTime type="date" pattern="dd/MM/yyyy"/></h:outputText>
									      	</rich:column>

									      	<rich:column title=" Edad Minima " width="20%">
									      		<f:facet name="header"><h:outputText value=" Edad Minima "  title="Edad Minima"/></f:facet>
									      		<h:outputText value="#{cobertura.cocbEdadMinima}"/>
									      	</rich:column>

									      	<rich:column title=" Edad Maxima " width="20%">
									      		<f:facet name="header"><h:outputText value=" Edad Maxima "  title="Edad Maxima"/></f:facet>
									      		<h:outputText value="#{cobertura.cocbEdadMaxima}"/>
									      	</rich:column>

									      	
									      	<rich:column title=" Riesgo " width="20%">
									      		<f:facet name="header"><h:outputText value=" Riesgo "  title="Riesgo"/></f:facet>
									      		<h:outputText value="#{cobertura.cocbTaRiesgo}"/>
									      	</rich:column>

									      	<rich:column title=" V. Millar " width="20%">
									      		<f:facet name="header"><h:outputText value=" V. Millar "  title="V. Millar"/></f:facet>
									      		<h:outputText value="#{cobertura.cocbCampon1}"/>
									      	</rich:column>

									      	<rich:column title=" Porcentaje " width="20%">
									      		<f:facet name="header"><h:outputText value=" Porcentaje "  title="Porcentaje"/></f:facet>
									      		<h:outputText value="#{cobertura.cocbCampov1}"/>
									      	</rich:column>

								      		<rich:column title="Modificar1" width="5%" styleClass="texto_centro">
   												<f:facet name="header"><h:outputText value=" Modificar1 "/></f:facet>
   												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()" action="#{beanCoberturas.consultaCartCoberturas}">
       												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
       												<f:setPropertyActionListener value="#{cobertura}" target="#{beanCoberturas.coberturas}" />
       												<f:setPropertyActionListener value="#{row}" target="#{beanListaProducto.filaActual}" />
   												</a4j:commandLink>
   												<rich:toolTip for="editlink" value="Editar" />
						                	</rich:column>
										</rich:dataTable>
									</c:otherwise>	
								</c:choose>
							</td>
						</tr>
					</table>
				</a4j:outputPanel>
			</fieldset>
		</td>
	</tr>
</table>				    		


<%-- 
<table>
	<tbody>
		<tr>
			<td align="center">
				<rich:dataTable id="lista" value="#{beanParametrizacion.listaPlanes}" columns="7"  var="beanPlan" rows="10">
					<f:facet name="header">
						<rich:columnGroup>
							<rich:column colspan="7"><h:outputText value="Lista de Planes" /></rich:column>
							<rich:column width="4px" breakBefore="true"><h:outputText value="Ramo" /></rich:column>
							<rich:column width="5px" ><h:outputText value="Plan" /></rich:column>
							<rich:column><h:outputText value="Descripción" /></rich:column>
							<rich:column><h:outputText value="Centro de Costos" /></rich:column>
							<rich:column><h:outputText value="Coberturas" /></rich:column>
							<rich:column><h:outputText value="Componentes" /></rich:column>
							<rich:column><h:outputText value="Homologación" /></rich:column>
						</rich:columnGroup>
					</f:facet>
					<rich:column>
						<h:outputText value="#{beanPlan.id.alplCdRamo}" />		      		  			
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanPlan.id.alplCdPlan}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanPlan.alplDePlan}" />
					</rich:column>
					<rich:column>
						<h:outputText value="#{beanPlan.alplDato3}" />
					</rich:column>
					<rich:column>
		      			<a4j:commandLink ajaxSingle="true" reRender="modalForm,listaCoberturas" oncomplete="Richfaces.showModalPanel('popup');" action="#{beanCoberturas.consultaCoberturas}">
		      				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
        					<a4j:actionparam name="cobRamo" 	assignTo="#{beanCoberturas.id.cocbCarpCdRamo}" 		value="#{beanPlan.id.alplCdRamo}"/>
        					<a4j:actionparam name="cobProd" 	assignTo="#{beanCoberturas.id.cocbCapuCdProducto}" 	value="#{beanParametrizacion.id.alprCdProducto}"/>
        					<a4j:actionparam name="cobPlan" 	assignTo="#{beanCoberturas.id.cocbCapbCdPlan}" 		value="#{beanPlan.id.alplCdPlan}"/>
        					<a4j:actionparam name="descPlan" 	assignTo="#{beanCoberturas.descPlan}" 				value="#{beanPlan.alplDePlan}"/>
  																															       					
        					<a4j:actionparam name="cobIdVenta" 	assignTo="#{beanCoberturas.idVenta}" 				value="#{beanParametrizacion.alprDato3}"/>
        					<a4j:actionparam name="cobPlazo" 	assignTo="#{beanCoberturas.plazo}" 					value="#{beanParametrizacion.plazo}"/>
        					<a4j:actionparam name="poliza" 		assignTo="#{beanCoberturas.poliza}" 				value="#{beanListaPreCarga.inPoliza}"/>
--%>
        					<%-- <a4j:actionparam name="lstCob" assignTo="#{beanCoberturas.listaComboCoberturas}" 	value="#{beanParametrizacion.listaComboCoberturas}"/>--%>
<%-- 
		      			</a4j:commandLink>
					</rich:column>

					<rich:column>
		      			<a4j:commandLink ajaxSingle="true" reRender="modalForm2,listaCoberturas" oncomplete="Richfaces.showModalPanel('componentesPopup');" action="#{beanComponentes.consultaComponentes}">
		      				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
        					<a4j:actionparam name="cobRamo" 	assignTo="#{beanComponentes.id.coctCarpCdRamo}" 	 value="#{beanPlan.id.alplCdRamo}"/>
        					<a4j:actionparam name="cobProd" 	assignTo="#{beanComponentes.id.coctCapuCdProducto}"  value="#{beanParametrizacion.id.alprCdProducto}"/>
        					<a4j:actionparam name="cobPlan" 	assignTo="#{beanComponentes.id.coctCapbCdPlan}" 	 value="#{beanPlan.id.alplCdPlan}"/>
        					<a4j:actionparam name="descPlan" 	assignTo="#{beanComponentes.descPlan}" 				 value="#{beanPlan.alplDePlan}"/>
  																															       					
        					<a4j:actionparam name="cobIdVenta" 	assignTo="#{beanComponentes.idVenta}" 				value="#{beanParametrizacion.alprDato3}"/>
        					<a4j:actionparam name="cobPlazo" 	assignTo="#{beanComponentes.plazo}" 				value="#{beanParametrizacion.plazo}"/>
        					<a4j:actionparam name="poliza" 		assignTo="#{beanComponentes.poliza}" 				value="#{beanListaPreCarga.inPoliza}"/>
		      			</a4j:commandLink>
					</rich:column>

					<rich:column>
		      			<a4j:commandLink ajaxSingle="true" reRender="modalForm3,listaHomologa" oncomplete="Richfaces.showModalPanel('homologaPopup');" action="#{beanHomologa.consultaHomologa}">
		      				<h:graphicImage value="../../images/edit.gif" width="20" height="20" />
        					<a4j:actionparam name="cobRamo" 	 assignTo="#{beanHomologa.id.hoppCdRamo}" 		value="#{beanPlan.id.alplCdRamo}"/>
        					<a4j:actionparam name="cobProd" 	 assignTo="#{beanHomologa.id.hoppProdColectivo}" value="#{beanParametrizacion.id.alprCdProducto}"/>
        					<a4j:actionparam name="cobPlan" 	 assignTo="#{beanHomologa.id.hoppPlanColectivo}" value="#{beanPlan.id.alplCdPlan}"/>
        					<a4j:actionparam name="descPlan" 	 assignTo="#{beanHomologa.descPlan}" 			value="#{beanPlan.alplDePlan}"/>
        					<a4j:actionparam name="descProducto" assignTo="#{beanHomologa.hoppDeProducto}"			value="#{beanParametrizacion.alprDeProducto}"/>
        					
  																															       					
        					<a4j:actionparam name="cobIdVenta" 	assignTo="#{beanHomologa.hoppDato3}" value="#{beanParametrizacion.alprDato3}"/>
        					<a4j:actionparam name="cobPlazo" 	assignTo="#{beanHomologa.plazo}"   value="#{beanParametrizacion.plazo}"/>
		      			</a4j:commandLink>
					</rich:column>

				</rich:dataTable>
			</td>
		</tr>
	</tbody>
</table>
--%>