<%@ include file="../include.jsp"%>
				    				
				    		<div align="center">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
					    				<tr class="espacio_15"><td>&nbsp;</td></tr>
										<tr><td><h:outputText id="mensaje" value="#{beanCtaConciliacion.strRespuesta}" styleClass="error"/></td></tr>	
										<tr class="espacio_15"><td>&nbsp;</td></tr>
				    				</table>
				    		</div>		
				    				
				  	
				    <!-- Inicio ACCIONES -->
				    	  <a4j:region id="consultaDatos">
				    		  <h:form id="consultaDatosCtas">
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<span id="img_sec_consulta_m">
														<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
													</span>
													<span class="titulos_secciones">Acciones</span>
												</legend>
												
												<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
													<tr class="espacio_5"><td>&nbsp;</td></tr>						                    		
						                    		<tr class="texto_normal">
						                  				<td>						                    										                    			
															<a4j:commandButton styleClass="boton" id="btnConsultar" action="#{beanCtaConciliacion.consultarCtasConc}" 
																			onclick="this.disabled=true" oncomplete="this.disabled=false" 
																		   	reRender="secRes,regTabla,mensaje" value="Consultar" />																
															<rich:toolTip for="btnConsultar" value="Consultar Ctas Conciliaciones" />													
														</td>
														<td>
															<a4j:commandButton ajaxSingle="true" value="Nuevo" styleClass="boton" id="btnNuevo" oncomplete="#{rich:component('newPanel')}.show()" />
                    										<rich:toolTip for="btnNuevo" value="Nueva Cuenta" />									
														</td>
						                    		</tr>
						                    		
													<tr class="espacio_15"><td>&nbsp;</td></tr>						                    		
						                    		<tr>
										    			<td align="center" colspan="2">
											    			<a4j:status for="consultaDatos" stopText=" ">
																<f:facet name="start">
																	<h:graphicImage value="/images/ajax-loader.gif" />
																</f:facet>
															</a4j:status>
														</td>
													</tr>
						                    	</table>
						                    </fieldset>
										</td>
									</tr>
									<tr class="espacio_15"><td>&nbsp;</td></tr>
				                </table>
				                
				           	<!-- Inicio RESULTADOS -->
				    				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td>
											<fieldset style="border:1px solid white">
												<legend>
													<a4j:outputPanel id="secRes" ajaxRendered="true">
													<span id="img_sec_resultados_m" style="display: none;">
														<h:graphicImage value="../../images/mostrar_seccion.png" onclick="muestraOcultaSeccion('resultados', 0);" title="Oculta"/>&nbsp;
													</span>
													<span id="img_sec_resultados_o">
														<h:graphicImage value="../../images/ocultar_seccion.png" onclick="muestraOcultaSeccion('resultados', 1);" title="Muestra"/>&nbsp;
													</span>
													<span class="titulos_secciones">Resultados</span>
													</a4j:outputPanel>
												</legend>
												
												<a4j:outputPanel id="regTabla" ajaxRendered="true">
												<table width="80%" border="0" cellpadding="0" cellspacing="0">	
													<tr class="texto_normal" id="tr_sec_resultados" style="display:none;">
														<td>
														<c:if test="${beanCtaConciliacion.hmResultados == null}">
															<table width="60%" align="center" border="0">
																<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
															</table>
														</c:if>
														
														<c:if test="${beanCtaConciliacion.hmResultados != null}">
														<rich:datascroller for="dsCuentas" maxPages="10"/>
															<rich:dataTable value="#{beanCtaConciliacion.mapValues}" id="dsCuentas" var="registro" 
																	rows="10" rowKeyVar="row" ajaxKeys="#{beanCtaConciliacion.keys}" 
																	columns="10" width="10%" align="center">
									      						<f:facet name="header"><h:outputText value="Configuraci�n"/></f:facet>
									      						
														      	<rich:column title="Numero de Cuenta" width="5%" styleClass="texto_centro" sortBy="#{registro.copaVvalor1}" >
														      		<f:facet name="header"><h:outputText value=" Cuenta "  title="Cuenta"/></f:facet>
                                                                    <h:outputText value="#{registro.copaVvalor1}"/>
														      	</rich:column>

														      	<rich:column title="Tipo de Ejecucion" width="10%" styleClass="texto_centro" sortBy="#{registro.copaVvalor3}">
														      		<f:facet name="header"><h:outputText value=" Tipo de Ejecucion "  title="Tipo de Ejecucion"/></f:facet>
														      		<h:outputText value="#{registro.copaVvalor3}"/>
                                                                </rich:column>
														      	
														      	<rich:column title="Fecha " width="5%" styleClass="texto_centro">
														      		<f:facet name="header"><h:outputText value=" Fecha "  title="Ultima Carga"/></f:facet>
														      		<h:outputText value="#{registro.copaFvalor1}">
														      			<f:convertDateTime pattern="dd-MM-yyyy" />
														      		</h:outputText>
														      	</rich:column>


														      	
													      		<rich:column title="Modificar" width="5%" styleClass="texto_centro">
                    												<f:facet name="header"><h:outputText value=" Modificar "/></f:facet>
                    												<a4j:commandLink ajaxSingle="true" id="editlink" oncomplete="#{rich:component('editPanel')}.show()">
                        												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
                        												<f:setPropertyActionListener value="#{registro}" target="#{beanCtaConciliacion.objActual}" />
                        											    <f:setPropertyActionListener value="#{row}" target="#{beanCtaConciliacion.filaActual}" />
                    												</a4j:commandLink>
                    												<rich:toolTip for="editlink" value="Editar" />
											                	</rich:column>
															</rich:dataTable>
															</c:if>
														</td>
													</tr>
												</table>
												</a4j:outputPanel>
											</fieldset>
										</td>
									</tr>
				    		</table>     
				        </h:form>
</a4j:region>

<rich:modalPanel id="newPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Nueva Conciliacion" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
	                <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="nuevaCtasConciliacion">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                   
                      <h:panelGrid columns="2">
	                    
                           <rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							
	                    	<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							
							<h:outputText value="Cuenta: "/>
                            <h:inputText  id= "Cuenta" value="#{beanCtaConciliacion.objNuevo.copaVvalor1}" style="width: 90px" maxlength="50" required="true" requiredMessage="* Cuenta - Campo Requerido"/>
							
							 <h:outputText value="Tipo de Ejecucion: "/>
                            <h:selectOneMenu id="tpoCarga"   value="#{beanCtaConciliacion.objNuevo.copaVvalor3}" required="true" requiredMessage="*  Tipo de Ejecucion- Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
	                        	<f:selectItem itemValue="CARGA_AUT" itemLabel="   CARGA_AUTOMATICA  "/>
  								<f:selectItem itemValue="CARGA_MAN" itemLabel="   CARGA_MANUAL  "/>
							</h:selectOneMenu>
							
							
							 <h:outputText value="Concepto del mov tiene el N�mero de Cr�dito: "/>
	                    	 <h:inputText id ="ConcptoNCredito"  value="#{beanCtaConciliacion.objNuevo.copaNvalor1}" />
	                    	
	                         <h:outputText value=" Concepto empieza con, solo No. Cr�dito   "/>
	                    	 <h:inputText  id="credito" value="#{beanCtaConciliacion.objNuevo.copaVvalor4}" style="width: 90px" maxlength="50" requiredMessage="* Concepto empieza con, Solo No. Credito:- Campo Requerido"/>
			
                            <h:outputText value=" Concepto empieza con, quitar caracteres  "/>
	                    	<h:inputText  id="concepto1" value="#{beanCtaConciliacion.objNuevo.copaVvalor6}" style="width: 250px" maxlength="50" requiredMessage="* Concepto empieza con,quitar caracteres Concepto Alfanumerico - Campo Requerido"/>
                            
                            <h:outputText value=" Concepto empieza con   "/>
	                    	<h:inputText  id="credito2" value="#{beanCtaConciliacion.objNuevo.copaVvalor2}" style="width: 90px" maxlength="50"  requiredMessage="* Concepto empieza con- Campo Requerido"/>
							
                            <h:outputText value="Concepto No Tiene #Credito: "/>
	                        <h:outputText value="" styleClass="texto_mediano"/>
	                        <h:outputText value=" Empieza: "/>
	                    	<h:inputText  id="concepto2" value="#{beanCtaConciliacion.objNuevo.copaVvalor5}" style="width: 250px" maxlength="50" requiredMessage="*  Concepto No  Tiene # Credito - Campo Requerido"/>
							
							<h:outputText value=" Concepto contiene  "/>
							<h:inputText  id="credito3" value="#{beanCtaConciliacion.objNuevo.copaVvalor8}" style="width: 90px" maxlength="50"  requiredMessage="*  Concepto contiene- Campo Requerido"/>
							

    
                     </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Guardar"
	                    action="#{beanCtaConciliacion.altaCtaConciliacion}" reRender="mensaje,regTabla,dsConciliacion"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
 </rich:modalPanel>

<rich:modalPanel id="editPanel" autosized="true" width="400">
        	<f:facet name="header"><h:outputText value="Editar Configuraci�n" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
	                <rich:componentControl for="editPanel" attachTo="hidelink" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="editarConciliacion">
	            <rich:messages style="color:red;"></rich:messages>
	            <h:panelGrid columns="1">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="2">
	                        <h:outputText value="Clave: "/>
	                    	<h:outputText value="#{beanCtaConciliacion.objActual.copaCdParametro}" styleClass="texto_mediano"/>
	                    	
                             <h:outputText value="Cuenta: "/>
	                    	<h:outputText value="#{beanCtaConciliacion.objActual.copaVvalor1}" styleClass="texto_mediano"/>
							
							
							<h:outputText value="Fecha Ultima Carga: : "/>
	                    	<h:outputFormat value="{0, date, dd/MM/yyyy}"  styleClass="texto_mediano">
                                <f:param value="#{beanCtaConciliacion.objActual.copaFvalor1}" />
                           </h:outputFormat>
	                    	 
                            <rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							
							 		 <h:outputText value="Tipo de Ejecucion: "/>
                            <h:selectOneMenu id="tpoCarga"   value="#{beanCtaConciliacion.objActual.copaVvalor3}" required="true" requiredMessage="*  Tipo de Ejecucion- Campo Requerido">
	                        	<f:selectItem itemValue="0" itemLabel="--SELECCIONAR--"/>
	                        	<f:selectItem itemValue="CARGA_AUT" itemLabel="   CARGA_AUTOMATICA  "/>
  								<f:selectItem itemValue="CARGA_MAN" itemLabel="   CARGA_MANUAL  "/>
							</h:selectOneMenu>
							
							
							 <h:outputText value="Concepto del mov tiene el N�mero de Cr�dito: "/>
	                    	 <h:inputText id ="ConcptoNCredito"  value="#{beanCtaConciliacion.objActual.copaNvalor1}" />
	                    	
	                         <h:outputText value=" Concepto empieza con, solo No. Cr�dito   "/>
	                    	 <h:inputText  id="credito" value="#{beanCtaConciliacion.objActual.copaVvalor4}" style="width: 90px" maxlength="50" requiredMessage="* Solo Numero Credito:- Campo Requerido"/>
			
                            <h:outputText value=" Concepto empieza con, quitar caracteres  "/>
	                    	<h:inputText  id="concepto1" value="#{beanCtaConciliacion.objActual.copaVvalor6}" style="width: 250px" maxlength="50"  requiredMessage="* Concepto Alfanumerico - Campo Requerido"/>
                            
                            <h:outputText value=" Concepto empieza con   "/>
	                    	<h:inputText  id="credito2" value="#{beanCtaConciliacion.objActual.copaVvalor2}" style="width: 90px" maxlength="50" requiredMessage="* No Manejamos N# de Creedito- Campo Requerido"/>
							
                            <h:outputText value="Concepto No  Tiene #Credito: "/>
	                        <h:outputText value="" styleClass="texto_mediano"/>
	                        <h:outputText value=" Empieza: "/>
	                    	<h:inputText  id="concepto2" value="#{beanCtaConciliacion.objActual.copaVvalor5}" style="width: 250px" maxlength="50" requiredMessage="*  Contiene - Campo Requerido"/>
							
							<h:outputText value=" Concepto contiene  "/>
							<h:inputText  id="credito3" value="#{beanCtaConciliacion.objActual.copaVvalor8}" style="width: 90px" maxlength="50"  requiredMessage="* Empieza- Campo Requerido"/>
							

							
                       
	                    </h:panelGrid>
	                </a4j:outputPanel>
	                <a4j:commandButton 
	                	value="Modificar"
	                    action="#{beanCtaConciliacion.modificar}" reRender="mensaje,regTabla,dsConciliacion"	                    
	                    oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
	            </h:panelGrid>
	        </h:form>
    	</rich:modalPanel>
 
 
 
 
 <a4j:status onstart="#{rich:component('wait')}.show()" onstop="#{rich:component('wait')}.hide()" />
    	<rich:modalPanel id="wait" autosized="true" width="160" height="60" moveable="false" resizeable="false">
        	<f:facet name="header"><h:outputText value="Procesando" /></f:facet>
        	<h:graphicImage value="../../images/reloj.png"/>
        	<h:outputText value="    Por favor espere..." />
  </rich:modalPanel>





