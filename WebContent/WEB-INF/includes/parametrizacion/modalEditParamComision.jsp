<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Comisiones
- Fecha: 09/10/2020
- Descripcion: Modal para editar Parametrizacion de Comision
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="editPanel" autosized="true" width="400">
   	<f:facet name="header"><h:outputText value="Editar Regla" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkEdit" styleClass="hidelink" />
            <rich:componentControl for="editPanel" attachTo="hidelinkEdit" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>
    <h:form id="editarRegla">
        <rich:messages styleClass="errorMessage"></rich:messages>
        <h:panelGrid columns="1">
        	<a4j:outputPanel ajaxRendered="true">
			<h:panelGrid columns="2">
				<h:outputText value="#{msgs.comisionre} "/>
				<h:outputText value="#{beanParamComision.objActual.auxDato}" styleClass="texto_mediano"/>
				
				<h:outputText value="#{msgs.sistemafecha} "/>
				<h:outputText value="#{beanParamComision.objActual.copaRegistro3}" styleClass="texto_mediano"/>
				
				<h:outputText value="#{msgs.comisioncob} "/>	
				<h:outputText value="#{beanParamComision.objActual.copaRegistro2}" styleClass="texto_mediano"/>
			</h:panelGrid>
				
			<rich:separator height="4" lineType="double" />

			<rich:spacer height="10px" />
            
            <h:panelGrid columns="2">
				<h:outputText value="#{msgs.comisiondesc} "/>
				<h:inputText value="#{beanParamComision.objActual.copaVvalor8}" styleClass="wid200" size="80" />
			</h:panelGrid>						
             	
            <rich:spacer height="10px" />
            
			<%@include file="/WEB-INF/includes/parametrizacion/comisiones.jsp"%>				
				
			<h:panelGrid columns="2">
				<a4j:commandButton 
               		value="Modificar"
                   	action="#{beanParamComision.modificar}" 
                   	reRender="mensaje,regTabla"	                    
                   	oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />
			    
               	<a4j:commandButton 
               		value="Cancelar"
			    	action="#{beanParamComision.cancelar}"
			    	reRender="mensaje,regTabla"
			   		onclick="#{rich:component('editPanel')}.hide();"	                    
			    	oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('editPanel')}.hide(); } else { #{rich:component('editPanel')}.show(); }" />
			</h:panelGrid>
			</a4j:outputPanel>
        </h:panelGrid>
    </h:form>
</rich:modalPanel>