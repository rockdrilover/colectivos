<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Comisiones
- Fecha: 09/10/2020
- Descripcion: Modal para nueva Parametrizacion de Comision
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="newPanel" autosized="true" width="500">
   	<f:facet name="header"><h:outputText value="Nueva Regla" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
            <rich:componentControl for="newPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>
    <h:form id="nuevaRegla">
        <rich:messages styleClass="errorMessage"></rich:messages>
        <h:panelGrid columns="1">
            <a4j:outputPanel ajaxRendered="true" id="campos">
               	<h:panelGrid columns="2">
					<h:outputText value="#{msgs.comisiontp}" />
					<h:selectOneMenu id="inTipoProc" value="#{beanParamComision.objActual.copaNvalor5}">
						<f:selectItem itemValue="0" itemLabel="#{msgs.sistemaseleccione}" />
						<f:selectItem itemValue="1" itemLabel="#{msgs.sistemapoliza}" />
						<f:selectItem itemValue="2" itemLabel="#{msgs.sistemaproducto}" />
						<f:selectItem itemValue="3" itemLabel="#{msgs.sistematarifa}" />
						<f:selectItem itemValue="4" itemLabel="#{msgs.timbradoan}" />
						<a4j:support event="onchange"
									action="#{beanParamComision.limpiaCombos}"
									reRender="campos"/>
					</h:selectOneMenu>
					<rich:spacer height="10px" />
             		<rich:spacer height="10px" />
             		
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double"/>
					
					<rich:spacer height="10px" />
             		<rich:spacer height="10px" />
					
					<h:outputText value="#{msgs.sistemaramo} "/>
					<h:selectOneMenu id="inRamo" 
								value="#{beanParamComision.ramo}" 
								binding="#{beanListaParametros.inRamo}">
						<f:selectItem itemValue="0" itemLabel=" --- Selecione un ramo --- " />
						<f:selectItems value="#{beanListaRamo.listaComboRamos}"/>
						<a4j:support event="onchange" 																		
								action="#{beanListaParametros.cargaPolizas}" 
								ajaxSingle="true" 
								reRender="polizas" /> 
					</h:selectOneMenu>
					
					<h:outputText value="#{msgs.sistemapoliza} "/>
                    <a4j:outputPanel id="polizas" ajaxRendered="true">
						<h:selectOneMenu id="inPoliza" 
								value="#{beanParamComision.poliza}" 
								binding="#{beanListaParametros.inPoliza}">
							<f:selectItem itemValue="0" itemLabel=" --- Seleccione una p�liza --- "/>
							<f:selectItem itemValue="-1" itemLabel="Prima �nica"/>
							<f:selectItems value="#{beanListaParametros.listaComboPolizas}"/>
							<a4j:support event="onchange" 
								action="#{beanListaParametros.cargaIdVenta}" 
								ajaxSingle="true" 
								reRender="idVenta" />
						</h:selectOneMenu>
					</a4j:outputPanel>
					
					<h:outputText value="#{msgs.sistemaidventa} "/>
					<a4j:outputPanel id="idVenta" ajaxRendered="true">
						<h:selectOneMenu id="inVenta"  
								value="#{beanParamComision.idVenta}"
								disabled="#{beanListaParametros.habilitaComboIdVenta}">
							<f:selectItem itemValue="0" itemLabel=" --- Seleccione un id venta --- "/>
							<f:selectItems value="#{beanListaParametros.listaComboIdVenta}"/>
							<a4j:support event="onchange" 
								action="#{beanParamComision.cargaProductos}"
								ajaxSingle="true" 
								reRender="idProductos"/>
						</h:selectOneMenu>
					</a4j:outputPanel>

					<c:if test="${beanParamComision.objActual.copaNvalor5 == 2 or beanParamComision.objActual.copaNvalor5 == 3 or beanParamComision.objActual.copaNvalor5 == 4}">					
					<h:outputText value="#{msgs.sistemaproducto} "/>
					<a4j:outputPanel id="idProductos" ajaxRendered="true">
						<h:selectOneMenu id="inProducto"  
								value="#{beanParamComision.objActual.copaVvalor1}">
							<f:selectItem itemValue="0" itemLabel=" --- Seleccione un producto --- "/>
							<f:selectItems value="#{beanParamComision.cmbProductos}"/>
							<a4j:support event="onchange" 
								action="#{beanParamComision.cargaTarifas}" 
								ajaxSingle="true" 
								reRender="idTarifas" />
						</h:selectOneMenu>
					</a4j:outputPanel>
					</c:if>
					
					<c:if test="${beanParamComision.objActual.copaNvalor5 == 3}">
					<h:outputText value="#{msgs.sistematarifa} "/>
					<a4j:outputPanel id="idTarifas" ajaxRendered="true">				
						<h:selectOneMenu id="inTarifa"
								value="#{beanParamComision.objActual.copaVvalor2}">
							<f:selectItem itemValue="0" itemLabel=" --- Seleccione una tarifa --- "/>
							<f:selectItems value="#{beanParamComision.cmbTarifas}"/>
						</h:selectOneMenu>
					</a4j:outputPanel>
					</c:if>					
					
					<c:if test="${beanParamComision.objActual.copaNvalor5 == 4}">
					<h:outputText value="#{msgs.timbradoan} "/>
					<h:selectOneMenu id="inAnios"
							value="#{beanParamComision.objActual.copaNvalor6}">
						<f:selectItem itemValue="0" itemLabel=" --- Seleccione un a�o --- "/>
						<f:selectItem itemValue="1" itemLabel=" A�o 0 "/>
						<f:selectItem itemValue="2" itemLabel=" A�o 1 "/>
						<f:selectItem itemValue="3" itemLabel=" A�o 2 "/>
						<f:selectItem itemValue="4" itemLabel=" A�o 3 "/>
						<f:selectItem itemValue="5" itemLabel=" A�o mayor igual a 4 "/>
					</h:selectOneMenu>
					</c:if>				
					
					<h:outputText value="#{msgs.comisiondesc} "/>
					<h:inputText value="#{beanParamComision.objActual.copaVvalor8}" styleClass="wid200" size="80"/>
					
					<h:outputText value="#{msgs.sistemafechad} "/>
					<rich:calendar id="fechIni" popup="true"
						value="#{beanParamComision.objActual.copaFvalor1}"
						datePattern="dd/MM/yyyy" styleClass="calendario,wid100" />
						
					<h:outputText value="#{msgs.sistemafechah} "/>
					<rich:calendar id="fechFin" popup="true"
						value="#{beanParamComision.objActual.copaFvalor2}"
						datePattern="dd/MM/yyyy" styleClass="calendario,wid100" />
					
					<h:outputText value="#{msgs.comisioncob} "/>	
					<h:selectOneRadio value="#{beanParamComision.tipoCobertura}" id="tipoCober">
						<f:selectItem itemValue="1" itemLabel="Normal" />
						<f:selectItem itemValue="2" itemLabel="Desempleo" />
						<f:selectItem itemValue="3" itemLabel="Aplica Ambos" />						
					</h:selectOneRadio>
						
					<a4j:commandButton value="Agregar Comisiones" action="#{beanParamComision.setValores}" reRender="campos" onclick="this.disabled=true" oncomplete="this.disabled=false;" />
				</h:panelGrid>
								
				<rich:spacer height="10px" />
             	<rich:spacer height="10px" />
					
				<c:if test="${beanParamComision.showComisiones == 1}">
					<%@include file="/WEB-INF/includes/parametrizacion/comisiones.jsp"%>
					
					<rich:spacer height="10px" />
				
					<h:panelGrid columns="2">
						<a4j:commandButton 
		               		value="Guardar"
		                   	action="#{beanParamComision.guardar}" 
		                   	reRender="mensaje,regTabla"	                    
		                   	oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('newPanel')}.hide();" />
					    
		               	<a4j:commandButton 
		               		value="Cancelar"
					    	action="#{beanParamComision.cancelar}"
					    	reRender="mensaje,regTabla"
					   		onclick="#{rich:component('newPanel')}.hide();"	                    
					    	oncomplete="if (#{facesContext.maximumSeverity==null}) { #{rich:component('newPanel')}.hide(); } else { #{rich:component('newPanel')}.show(); }" />
					</h:panelGrid>				
				</c:if>
				
            </a4j:outputPanel>
        </h:panelGrid>
    </h:form>
</rich:modalPanel>