<%@ include file="include.jsp"%> 
<a4j:region id="consultaDatos">
<table width="90%">
	<tr>
		<td>
	
			<fieldset style="border:1px solid white">
			<legend>
				<span id="img_sec_consulta_m">
					<h:graphicImage value="../../images/mostrar_seccion.png"/>&nbsp;
				</span>
				<span class="titulos_secciones">Acciones</span>
			</legend>
			
			<table cellpadding="2" cellspacing="0">
				
				<tbody>
					<tr>
						<td>
						    <h:selectOneRadio id="configuracion" value="#{beanParametrizacion.radio}">
						        <f:selectItem id="item1" itemLabel="Configurar Nuevo Producto" itemValue="1" />
						        <f:selectItem id="item2" itemLabel="Clonar Producto Existente" itemValue="2" />
						        <a4j:support event="onclick" action="#{beanParametrizacion.show}" reRender="frmCatalogoProductos"/>
						    </h:selectOneRadio>							
						</td>
					</tr>
				</tbody>
			</table>
					
			<table cellpadding="2" cellspacing="0">
				
				<tbody>
					<tr>
						<td align="right" width="24%" height="30"><h:outputText value=" Ramos :" /></td>
						<td align="left" width="76%" height="30">
							<h:selectOneMenu id="inRamo" value="#{beanParametrizacion.ramo.carpCdRamo}" >
								<f:selectItem itemValue="" itemLabel="Selecione un ramo" />
								<f:selectItems value="#{beanListaRamo.listaComboRamos}" />
								<a4j:support event="onchange"  action="#{beanParametrizacion.cargaPolizas}" reRender="inPoliza" />	
							</h:selectOneMenu>
							<rich:message for="inRamo" styleClass="errorMessage" />																						
					</td>
				</tr>
				
				<tr>
					<td align="right" width="24%" height="30"><h:outputText value="Poliza :"/></td>
					<td align="left" width="76%" height="30">
						<h:selectOneMenu id="inPoliza"  value="#{beanParametrizacion.poliza.poliza}">
							<f:selectItem itemValue="-2" itemLabel="Seleccione una p�liza"/>
							<f:selectItems value="#{beanParametrizacion.listaComboPolizas}"/>
						<a4j:support event="onchange"  action="#{beanParametrizacion.cargaIdVenta}" reRender="idVenta"/>
						</h:selectOneMenu>
						<rich:message for="inPoliza" styleClass="errorMessage" />
					</td>
				</tr>
				<tr>
					<td align="right" width="24%" height="30"><h:outputText value="Canal de Venta:"/></td>
					<td align="left" width="76%" height="30">
						<h:selectOneMenu id="idVenta"  value="#{beanParametrizacion.alprDato3}" disabled="#{beanParametrizacion.habilitaComboIdVenta}" >
							<f:selectItem itemValue="" itemLabel="Seleccione una canal de venta"/>
							<f:selectItems value="#{beanParametrizacion.listaComboIdVenta}"/>
							<a4j:support event="onchange"  action="#{beanParametrizacion.consultaProducto}" reRender="idProducto"/>
						</h:selectOneMenu>
						<rich:message for="idVenta" styleClass="errorMessage" />
					</td>
				</tr>
				<tr>
					<td align="right" width="24%" height="30"><h:outputText value="Producto:"/></td>
					<td align="left" width="76%" height="30">
						<h:selectOneMenu id="idProducto"  value="#{beanParametrizacion.id.alprCdProducto}">
							<f:selectItem itemValue="-1" itemLabel="Seleccione un producto"/>
							<f:selectItems value="#{beanParametrizacion.listaComboProducto}"/>
							<a4j:support event="onchange"  action="#{beanParametrizacion.consultaPlanes}" reRender="idPlan"/>
						</h:selectOneMenu>
						<rich:message for="idProducto" styleClass="errorMessage" />
					</td>
				</tr>
				<tr>
					<td align="right" width="24%" height="30"><h:outputText value="Plan:" rendered="#{beanParametrizacion.renderConfig}"/></td>
					<td align="left" width="76%" height="30">
						<h:selectOneMenu id="idPlan"  value="#{beanParametrizacion.idPlan.alplCdPlan}" rendered="#{beanParametrizacion.renderConfig}">
							<f:selectItem itemValue="-1" itemLabel="Seleccione un plan"/>
							<f:selectItems value="#{beanParametrizacion.listaComboPlanes}"/>
						</h:selectOneMenu>
						<rich:message for="idPlan" styleClass="errorMessage" />
					</td>
				</tr>
			
				<tr>
					<td align="right" width="24%" height="30"></td>
					<td align="left" width="76%" height="30">
						<a4j:commandButton styleClass="boton" value="Consultar" id="Consultar"
						   rendered="#{beanParametrizacion.renderConfig}"
						   action="#{beanParametrizacion.consulta}" 
						   onclick="this.disabled=true" oncomplete="this.disabled=false" 
						   reRender="test, idProducto, mensaje, mensaje1" />	   

						<a4j:commandButton styleClass="boton" value="Clonar" id="Clonar"
						   rendered="#{beanParametrizacion.renderClon}"
						   action="#{beanParametrizacion.clonar}" 
						   onclick="this.disabled=true" oncomplete="this.disabled=false" 
						   reRender="test, idProducto, mensaje, mensaje1" />	   
					</td>
				</tr>
			
				</tbody>
			</table>
			</fieldset>
		</td>
	</tr>	
</table>
</a4j:region>    
