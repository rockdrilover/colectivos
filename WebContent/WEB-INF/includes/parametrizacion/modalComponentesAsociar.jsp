<%@include file="include.jsp"%>

		<rich:modalPanel id="componentePanel" autosized="true" width="450">
        	<f:facet name="header"><h:outputText value="Asociar Componentes" /></f:facet>
	        <f:facet name="controls">
	            <h:panelGroup>
	                <h:graphicImage value="../../images/cerrarModal.png" id="hidelink_3" styleClass="hidelink" />
	                <rich:componentControl for="componentePanel" attachTo="hidelink_3" operation="hide" event="onclick" />
	            </h:panelGroup>
	        </f:facet>
	        <h:form id="asociarComponente">
	            <rich:messages style="color:red;"></rich:messages>

	            <h:panelGrid columns="1" width="100%">
	                <a4j:outputPanel ajaxRendered="true">
	                    <h:panelGrid columns="5" width="100%" cellspacing="0" cellpadding="1">
	                    	<h:outputText value="Ramo: "/>
	                    	<h:outputText value="#{beanComponentes.bean.ramo.carpCdRamo}" styleClass="texto_mediano"/>
							<h:outputText value="      "/>
							<h:outputText value="Poliza: "/>
							<h:outputText value="#{beanComponentes.bean.polizaUnica}" styleClass="texto_mediano"/>

	                    	<h:outputText value="Producto: "/>
	                    	<h:outputText value="#{beanComponentes.bean.id.alprCdProducto}" styleClass="texto_mediano"/>
							<h:outputText value="      "/>
	                    	<h:outputText value="Plan: "/>
	                    	<h:outputText value="#{beanComponentes.bean.idPlan.alplCdPlan}" styleClass="texto_mediano"/>

							<rich:separator height="4" lineType="double" />
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							<rich:separator height="4" lineType="double"/>
							
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
							<rich:spacer height="10px" />
						</h:panelGrid>
						
						<h:panelGrid columns="3" width="100%" cellspacing="0" cellpadding="1">

							<a4j:outputPanel id="regTabla3" ajaxRendered="true">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">	
									<tr class="texto_normal" id="tr_sec_resultados3" style="display:block;">
										<td>
											<c:choose>
												<c:when test="${fn:length(beanComponentes.listaParamComponentes) == 0}">
													<table width="90%" align="center" border="0">
														<tr><td>NO SE ENCONTRO INFORMACION PARA EL FILTRO SELECCIONADO.</td></tr>
													</table>
												</c:when>
												<c:otherwise>															
													<rich:contextMenu attached="false" id="menu2" submitMode="ajax" oncollapse="row.style.backgroundColor='#{a4jSkin.tableBackgroundColor}'">
														<rich:menuItem value="Edit Record" ajaxSingle="true" oncomplete="#{rich:component('editPanel')}.show()" actionListener="#{beanListaProducto.buscarFilaActual}">
															<a4j:actionparam name="clave" value="{clave}" />
															<a4j:actionparam name="row" value="{filaActual}" />
														</rich:menuItem>
													</rich:contextMenu>
													
													<rich:datascroller for="dsProducto2" maxPages="10"/>
													<rich:dataTable value="#{beanComponentes.listaParamComponentes}" id="dsProducto2" var="componente" 
																									rows="10" rowKeyVar="row" 
																									columns="10" width="90%" align="center">
													
														<f:facet name="header"><h:outputText value="Componentes"/></f:facet>
														
														
							      						<rich:column title="Componente" width="20%" styleClass="texto_centro" sortBy="#{componente.copaVvalor2}" >
							      							<f:facet name="header"><h:outputText value=" Componente " title="Componente" /></f:facet>
																<h:outputText value="#{componente.copaVvalor2}"/>  
							      						</rich:column>
							      									  
												      	<rich:column title="%" width="5%" styleClass="texto_centro" sortBy="#{componente.copaVvalor6}">
												      		<f:facet name="header"><h:outputText value=" % " title="%" /></f:facet>
															<h:outputText value="#{componente.copaVvalor6}"/>  
												      	</rich:column>
												      	
												      	<rich:column title=" Fecha Desde " width="20%">
												      		<f:facet name="header"><h:outputText value=" Fecha Desde "  title="Fecha Desde"/></f:facet>
															<%-- <h:inputText value="#{componente.copaFvalor1}" id="fDesde"/>--%>
															<rich:calendar id="fechaDesde" datePattern="dd/MM/yyyy" value="#{componente.copaFvalor1}" inputSize="8" onchanged="setFecha('asociarComponente:dsProducto2:#{row}:fechaHastaInputDate',this.value);"/>															 
												      	</rich:column>
			
												      	<rich:column title=" Fecha Hasta * " width="20%">
												      		<f:facet name="header"><h:outputText value=" Fecha Hasta "  title="Fecha Hasta-"/></f:facet>
												      		<rich:calendar id="fechaHasta" datePattern="dd/MM/yyyy" value="#{componente.copaFvalor2}" inputSize="8"/>
												      	</rich:column>
			
												      	<rich:column title=" Afecta Calculo " width="20%">
												      		<f:facet name="header"><h:outputText value=" Afecta Calculo "  title="Afecta Calculo"/></f:facet>
												      		<h:outputText value="#{componente.copaNvalor1}"/>
												      	</rich:column>

											      		<rich:column title="Asociar" width="5%" styleClass="texto_centro">
			   												<f:facet name="header"><h:outputText value=" Asociar "/></f:facet>
			   												<a4j:commandLink ajaxSingle="false" id="asocLink" reRender="mensaje, mensaje1, dsProducto2, error_c" action="#{beanComponentes.asociar}">
			       												<h:graphicImage value="../../images/edit_page.png" style="border:0" />
			       												<f:setPropertyActionListener value="#{componente}" target="#{beanComponentes.componentes}" />
			       												<f:setPropertyActionListener value="#{row}" target="#{beanListaProducto.filaActual}" />
			   												</a4j:commandLink>
			   												<rich:toolTip for="asocLink" value="Asociar" />
									                	</rich:column>

													</rich:dataTable>
												</c:otherwise>	
											</c:choose>
										</td>
									</tr>
								</table>
							</a4j:outputPanel>

	                    </h:panelGrid>
	                </a4j:outputPanel>

	            </h:panelGrid>
	        
	        	<h:outputText id="error_c" value="#{beanComponentes.strRespuesta}" styleClass="error"/><br>
	        
	        </h:form>
	        
    	</rich:modalPanel>