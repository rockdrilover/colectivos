<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Comisiones
- Fecha: 09/10/2020
- Descripcion: Modal para cargar archivo de comisiones
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="cargaPanel" autosized="true" width="400">
   	<f:facet name="header"><h:outputText value="Carga Lay Out" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkCargar" styleClass="hidelink" />
            <rich:componentControl for="cargaPanel" attachTo="hidelinkCargar" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>
    <h:form id="cargaReglas">
        <rich:messages  styleClass="errorMessage"></rich:messages>
		<h:panelGrid columns="1"> 
			<a4j:commandButton 
				styleClass="boton" 
				value="#{msgs.botoncargar}" 
				action="#{beanParamComision.cargaMasiva}" 
				onclick="this.disabled=true" 
				oncomplete="this.disabled=false; if (#{facesContext.maximumSeverity==null}) #{rich:component('cargaPanel')}.hide();"
				reRender="mensaje,regTabla" />
								
			<rich:fileUpload fileUploadListener="#{beanParamComision.listener}"
					maxFilesQuantity="5" id="loadArchivo"  
					addControlLabel="Examinar" uploadControlLabel="Cargar"  
					stopControlLabel="Detener" immediateUpload="true" 
					clearAllControlLabel="Borrar todo" clearControlLabel="Borrar" 
					styleClass="archivo" acceptedTypes="csv"
					listHeight="80px" addButtonClass="botonArchivo" >
				<a4j:support event="onuploadcomplete" reRender="mensaje" />
				<f:facet name="label">
					<h:outputText value="{_KB}KB de {KB}KB cargados -- {mm}:{ss}" />
				</f:facet>                      
			</rich:fileUpload>
		</h:panelGrid>
    </h:form>
</rich:modalPanel>