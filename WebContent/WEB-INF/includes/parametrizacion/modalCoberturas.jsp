<%@include file="include.jsp"%>

<rich:modalPanel id="editPanel" autosized="true" width="450" onshow="inicializaEdicionCobertura();">
	<f:facet name="header">
		<h:outputText value="Editar Cobertura" />
	</f:facet>
	<f:facet name="controls">
		<h:panelGroup>
			<h:graphicImage value="../../images/cerrarModal.png" id="hidelink"
				styleClass="hidelink" />
			<rich:componentControl for="editPanel" attachTo="hidelink"
				operation="hide" event="onclick" />
		</h:panelGroup>
	</f:facet>
	<h:form id="editarProducto">
		<rich:messages style="color:red;"></rich:messages>

		<h:panelGrid columns="1" width="100%">
			<a4j:outputPanel ajaxRendered="true">
				<h:panelGrid columns="5" width="100%" cellspacing="0"
					cellpadding="1">
					<h:outputText value="Ramo: " />
					<h:outputText
						value="#{beanCoberturas.coberturas.id.cocbCarpCdRamo}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="Poliza: " />
					<h:outputText
						value="#{beanCoberturas.coberturas.id.cocbCapoNuPoliza}"
						styleClass="texto_mediano" />

					<h:outputText value="Producto: " />
					<h:outputText
						value="#{beanCoberturas.coberturas.id.cocbCapuCdProducto}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="Plan: " />
					<h:outputText
						value="#{beanCoberturas.coberturas.id.cocbCapbCdPlan}"
						styleClass="texto_mediano" />

					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />
					<rich:separator height="4" lineType="double" />

					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />
					<rich:spacer height="10px" />

					<h:outputText value="Fecha Inicial: " />
					<rich:calendar id="fechaInicial" datePattern="dd/MM/yyyy"
						required="true" value="#{beanCoberturas.coberturas.cocbFeDesde}"
						inputSize="8" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />

					<h:outputText value="Fecha Final: " />
					<rich:calendar id="fechaFinal" datePattern="dd/MM/yyyy"
						required="true" value="#{beanCoberturas.coberturas.cocbFeHasta}"
						inputSize="8" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />

					<h:outputText value="Edad Minima: " />
					<h:inputText value="#{beanCoberturas.coberturas.cocbEdadMinima}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />

					<h:outputText value="Edad Maxima: " />
					<h:inputText value="#{beanCoberturas.coberturas.cocbEdadMaxima}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />

					<h:outputText value="Edad Minima Renovacion: " />
					<h:inputText value="#{beanCoberturas.coberturas.cocbCampon2}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />

					<h:outputText value="Edad Maxima Renovacion: " />
					<h:inputText value="#{beanCoberturas.coberturas.cocbCampon3}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />

					<h:outputText value="T. Riesgo: " />
					<h:inputText value="#{beanCoberturas.coberturas.cocbTaRiesgo}"
						styleClass="texto_mediano" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="Tipo Comision: " />
					<h:selectOneMenu id="cosuExamMedico"
						value="#{beanCoberturas.coberturas.tipoComision}"
						onchange="ocultaComisiones(this.value, 'editarProducto:panelComisiones', 'editarProducto:txtComTecnica', 'editarProducto:txtComContigente');">

						<f:selectItem itemValue="P" itemLabel="Producto / Plan" />
						<f:selectItem itemValue="C" itemLabel="Cobertura" />


					</h:selectOneMenu>

					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />
				</h:panelGrid>
				<h:panelGrid columns="5" width="100%" cellspacing="0"
					cellpadding="1" style="display:none;" id='panelComisiones'>
					<h:outputText value="Com. Tecnica: " />
					<h:inputText value="#{beanCoberturas.coberturas.cocbPoComision}"
						styleClass="texto_mediano"  id="txtComTecnica"/>
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />

					<h:outputText value="Com. Contingente: " />
					<h:inputText value="#{beanCoberturas.coberturas.cocbMtFijo}"
						styleClass="texto_mediano" id="txtComContigente"/>
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />
				</h:panelGrid>
				
				<h:panelGrid columns="5" width="100%" cellspacing="0"
					cellpadding="1">
					<h:outputText value="Tipo Cobertura: " />
					<h:selectOneMenu id="tipoCob"
						value="#{beanCoberturas.coberturas.cocbSexo}">
						<f:selectItem itemValue="" itemLabel="<Seleccione una opcion>" />
						<f:selectItems value="#{beanListaParametros.listaTipoCobertura}" />
					</h:selectOneMenu>
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />
					
					<h:outputText value="#{msgs.cobersumafija}:" />
					<h:inputText value="#{beanCoberturas.coberturas.cocbCampov2}" 
									styleClass="texto_mediano"
									title="#{msgs.cobersumamsg}" />
					<h:outputText value="      " />
					<h:outputText value="      " />
					<h:outputText value="      " />
				</h:panelGrid>
			</a4j:outputPanel>
			<a4j:commandButton value="Modificar"
				action="#{beanCoberturas.modificar}"
				reRender="mensaje1,regTabla,dsProducto"
				oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('editPanel')}.hide();" />

		</h:panelGrid>
	</h:form>
</rich:modalPanel>