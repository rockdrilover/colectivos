<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Timbrado
- Fecha: 09/10/2020
- Descripcion: Modal para mostrar detalle timbrado
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="detPanel" autosized="true" height="600" width="550">
	<div class="scroll-div">
   	<f:facet name="header"><h:outputText value="Detalle Timbrado" /></f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="../../images/cerrarModal.png" id="hidelink" styleClass="hidelink" />
            <rich:componentControl for="detPanel" attachTo="hidelink" operation="hide" event="onclick" />
        </h:panelGroup>
    </f:facet>

	<h:form id="detalle">
        <rich:messages styleClass="errorMessage"></rich:messages>
        <h:panelGrid columns="1">
            <a4j:outputPanel ajaxRendered="true">
                <h:panelGrid columns="1">
                	<h:panelGrid columns="10">
                		<h:outputText value="#{msgs.sistemaacciones}:"/>
						<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						
						<h:commandLink styleClass="boton"
									id="btnPDF"
									action="#{beanTimbrado.descargaCFDI}">																		
								<h:graphicImage value="../../images/type_pdf.png" styleClass="bor0" />
								<f:setPropertyActionListener value="pdf" target="#{beanTimbrado.tipoDoc}" />
						</h:commandLink>                		
						<rich:toolTip for="btnPDF" value="Descarga PDF" />
                		<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
                		
                		<a4j:commandLink styleClass="boton"
									id="btnXML"						
									action="#{beanTimbrado.descargaCFDI}"
									onclick="this.disabled=true"
									oncomplete="this.disabled=false"
									reRender="mensaje,	secCoberturas,regTablaCoberturas,secComponentes,regTablaComponentes">
							<h:graphicImage value="../../images/type_xml.png" styleClass="bor0" />
							<f:setPropertyActionListener value="xml" target="#{beanTimbrado.tipoDoc}" />
						</a4j:commandLink>
                		<rich:toolTip for="btnXML" value="Descarga XML" />
                		<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
                		
                		<a4j:commandLink styleClass="boton"
									id="btnCan"						
									action="#{beanTimbrado.cancelarRecibos}"
									onclick="this.disabled=true"
									oncomplete="this.disabled=false"
									reRender="mensaje,	secCoberturas,regTablaCoberturas,secComponentes,regTablaComponentes">
							<h:graphicImage value="../../images/type_cancel.png" styleClass="bor0" />
						</a4j:commandLink>
						<rich:toolTip for="btnCan" value="Cancelar CFDI" />                		
                	</h:panelGrid>
                	
                	<h:panelGrid columns="4">
                		<h:outputText value="#{msgs.sistemapoliza}:"/>
                		<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaCampov4}" styleClass="texto-mediano2"/>
						<h:outputText value="#{msgs.sistemaEst}:"/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaStRecibo}" styleClass="texto-mediano2"/>
						
						<h:outputText value="#{msgs.timbradouuid}:"/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaCampov3}" styleClass="texto-mediano2"/>
						<h:outputText value="#{msgs.timbradofecha}:"/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaFeFacturacion}" styleClass="texto-mediano2">
							<f:convertDateTime pattern="dd/MM/yyyy" />
						</h:outputText>
						
						<h:outputText value="#{msgs.timbradocert}:"/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaCampov1}" styleClass="texto-mediano2"/>
						<h:outputText value="#{msgs.timbradofechac}:"/>
						<h:outputText value="#{beanTimbrado.seleccionado.recibo.coreFeStatus}" styleClass="texto-mediano2">
							<f:convertDateTime pattern="dd/MM/yyyy" />
						</h:outputText>
                	</h:panelGrid>
                 	
					<rich:separator height="4" lineType="double" />
                	
                	<h:panelGrid columns="2">
						<rich:spacer height="10px" />
						<rich:spacer height="10px" />
						
						<h:outputText value="#{msgs.timbradors}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaCdUsuario}" styleClass="texto-mediano2, wid290"/>
					</h:panelGrid>

                	<h:panelGrid columns="5">
						<h:outputText value="#{msgs.timbradorfc}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaCampov2}" styleClass="texto-mediano2"/>
						<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						<h:outputText value="#{msgs.timbradorecibo} "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaNuReciboFiscal}" styleClass="texto-mediano2"/>
						
						<h:outputText value="#{msgs.sistemafechad}: "/>
	                    <h:outputText value="#{beanTimbrado.seleccionado.recibo.coreFeDesde}" styleClass="texto-mediano2">
	                    	<f:convertDateTime pattern="dd/MM/yyyy" />
	                    </h:outputText>
	                    <h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
	                    <h:outputText value="#{msgs.sistemafechah} "/>
						<h:outputText value="#{beanTimbrado.seleccionado.recibo.coreFeHasta}" styleClass="texto-mediano2">
	                    	<f:convertDateTime pattern="dd/MM/yyyy" />
	                    </h:outputText>
						
						<h:outputText value="#{msgs.timbradofeemi}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.recibo.coreFeEmision}" styleClass="texto-mediano2">
	                    	<f:convertDateTime pattern="dd/MM/yyyy" />
	                    </h:outputText>
	                    <h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						<h:outputText value="#{msgs.timbradofefac}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaCampof2}" styleClass="texto-mediano2">
	                    	<f:convertDateTime pattern="dd/MM/yyyy" />
	                    </h:outputText>
						
						<h:outputText value="#{msgs.timbradoestsis}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaNuCuenta}" styleClass="texto-mediano2"/>
						<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						<h:outputText value="#{msgs.timbradoestcob}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaNuCuentaDep}" styleClass="texto-mediano2"/>
						
						<h:outputText value="#{msgs.timbradototcer}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaTotalCertificados}" styleClass="texto-mediano2"/>
						<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						<h:outputText value="#{msgs.timbradocuota}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.recibo.coreNuConsecutivoCuota}" styleClass="texto-mediano2"/>
						
						<h:outputText value="#{msgs.sistemapmaneta}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.recibo.coreMtPrimaPura}" styleClass="texto-mediano2">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:outputText>
						<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						<h:outputText value="#{msgs.sistemapmatot}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.recibo.coreMtPrima}" styleClass="texto-mediano2">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:outputText>
						
						<h:outputText value="#{msgs.sistemaiva}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaMtTotIva}" styleClass="texto-mediano2">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:outputText>
						<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						<h:outputText value="#{msgs.sistemarfi}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaMtTotRfi}" styleClass="texto-mediano2">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:outputText>
						
						<h:outputText value="#{msgs.sistemadpo}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaMtTotDpo}" styleClass="texto-mediano2">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:outputText>
						<h:graphicImage value="../../images/espacio.png" styleClass="wid10"/>
						<h:outputText value="#{msgs.sistemasumaseg}: "/>
						<h:outputText value="#{beanTimbrado.seleccionado.factura.cofaMtTotSua}" styleClass="texto-mediano2">
							<f:convertNumber currencySymbol="$" type="currency"/>
						</h:outputText>
            		</h:panelGrid>
					
					<rich:spacer height="10px" />
					
					<h:outputText value="#{msgs.timbradocober}: " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double" />
					<rich:spacer height="10px" />
		
					<rich:dataTable id="lstCober"
						value="#{beanTimbrado.seleccionado.lstCobe}"
						columns="5" var="cobert" rows="15"
						rowKeyVar="valor">
						<f:facet name="header">
							<rich:columnGroup>
								<rich:column width="15%">
									<h:outputText value="#{msgs.sistemaclave}" />
								</rich:column>
								<rich:column width="50%">
									<h:outputText value="#{msgs.timbradodesc}" />
								</rich:column>
								<rich:column width="20%">
									<h:outputText value="#{msgs.sistemapmaneta}" />
								</rich:column>
								<rich:column width="20%">
									<h:outputText value="#{msgs.sistemapmatot}" />
								</rich:column>
							</rich:columnGroup>
						</f:facet>

						<rich:column width="15%" styleClass="center_div">
							<h:outputText value="#{cobert.cocbCampov1}" />
						</rich:column>
						<rich:column width="50%" styleClass="center_div">
							<h:outputText value="#{cobert.cocbCampov2}" />
						</rich:column>
						<rich:column width="20%" styleClass="center_div">
							<h:outputText value="#{cobert.cocbMtFijo}" >
								<f:convertNumber currencySymbol="$" type="currency"/>
							</h:outputText>
						</rich:column>
						<rich:column width="20%" styleClass="center_div">
							<h:outputText value="#{cobert.cocbCampon3}" >
								<f:convertNumber currencySymbol="$" type="currency"/>
							</h:outputText>
						</rich:column>
					</rich:dataTable>
                                 
                    <rich:spacer height="10px" />
													
					<h:outputText value="#{msgs.timbradocompo}: " styleClass="texto_mediano"/>					
					<rich:separator height="4" lineType="double"/>	
					<rich:spacer height="10px" />
		
					<rich:dataTable id="lstCompo"
						value="#{beanTimbrado.seleccionado.lstComp}"
						columns="3" var="componente" rows="5"
						rowKeyVar="valor">
						<f:facet name="header">
							<rich:columnGroup>
								<rich:column width="50%">
									<h:outputText value="#{msgs.timbradodesc}" />
								</rich:column>
								<rich:column width="30%">
									<h:outputText value="#{msgs.sistemamonto}" />
								</rich:column>
								<rich:column width="20%">
									<h:outputText value="#{msgs.sistematarifa}" />
								</rich:column>
							</rich:columnGroup>
						</f:facet>

						<rich:column width="50%" styleClass="center_div">
							<h:outputText value="#{componente.coctCampov1}" />
						</rich:column>
						<rich:column width="30%" styleClass="center_div">
							<h:outputText value="#{componente.coctCampon2}" >
								<f:convertNumber currencySymbol="$" type="currency"/>
							</h:outputText>
						</rich:column>
						<rich:column width="20%" styleClass="center_div">
							<h:outputText value="#{componente.coctTaComponente}" />
						</rich:column>
					</rich:dataTable>             
                </h:panelGrid>
            </a4j:outputPanel>          
        </h:panelGrid>    
    </h:form>    
    </div>
 </rich:modalPanel>