<%@ include file="include.jsp"%>
<%--<a4j:region id="paramEndoso">--%> 
	<table cellpadding="2" cellspacing="0">
	
		<tbody>

			<h:panelGrid columns="3" id="panel" >
			
				<h:outputLabel value="Endoso en Emision :"></h:outputLabel>
				<h:selectOneRadio id="endEmi">
					 <f:selectItem id="item1" itemLabel="Permitir Endoso en emision " itemValue="1" />
					 <f:selectItem id="item2" itemLabel="Sin permiso de Emision " itemValue="2" />
				</h:selectOneRadio>
				
				
				<h:outputLabel value="Endoso en Renovacion:"></h:outputLabel>
				<h:selectOneRadio id="endRen">
					 <f:selectItem id="item2" itemLabel="Permitir endoso en Renovacion" itemValue="2" />
					 <f:selectItem id="item3" itemLabel="No permite Endoso en Renovacion" itemValue="3" />
				</h:selectOneRadio>
				
				
				
				<h:outputLabel value="Endoso de cancelacion:"></h:outputLabel>
				<h:selectOneRadio id="endCan">
					 <f:selectItem id="item4" itemLabel="Permitir endoso de Cancelacion" itemValue="4" />
					 <f:selectItem id="item5" itemLabel="No permitir endoso de Cancelacion" itemValue="5" />
				</h:selectOneRadio>
				
			
					<h:outputLabel value="Tipo de calculo:"></h:outputLabel>
				<h:selectOneRadio id="tipoCal">
					 <f:selectItem id="item6" itemLabel="Recibo directo " itemValue="6" />x
					 <f:selectItem id="item7" itemLabel="Calculo sobre anualidad " itemValue="7" />
				</h:selectOneRadio>
				
				
				
				<tr>
				<td align="right" width="24%" height="30"></td>
				<td align="left" width="76%" height="30">
					<a4j:commandButton styleClass="boton" value="Guardar" id="bPlan"
					   action="#{BeanParamOperacion.altaParamEndoso}" 
					   onclick="this.disabled=true" oncomplete="this.disabled=false" 
					   reRender="barra, lista"
					   title="header=[Carga y emisi�n masiva] body=[Ejecuta el proceso de carga y emisi�n, el tiempo de procesamiento depender� del archivo cargado.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"/>	   
				</td>
				</tr>
							
			</h:panelGrid>
				<%-- 
			<h:commandButton action="#{temperatureConvertor.celsiusToFahrenheit}" value="Calculate"></h:commandButton>
			<h:commandButton action="#{temperatureConvertor.reset}" value="Reset"></h:commandButton>
			<h:messages layout="table"></h:messages>
			--%>
</tbody>
	</table>
<%--</a4j:region>--%>