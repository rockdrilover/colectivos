<%-- 
- Autor: Ing. Issac Bautista
- Aplicacion: WEB Colectivos
- Modulo: Cedula
- Fecha: 09/10/2020
- Descripcion: Modal detalle para cedula
--%>
<%@include file="include.jsp"%>

<rich:modalPanel id="detPanel" autosized="true" width="800">
	<rich:toolTip id="toolTip" mode="ajax" value="Dar clic en Aceptar, Cancelar o Salir." direction="top-right" />

  	<f:facet name="header"><h:outputText value="Detalle Cedula - #{beanCedula.seleccionado.coccRemesaCedula}" /></f:facet>
   	<f:facet name="controls">
       <h:panelGroup>
           <h:graphicImage value="../../images/cerrarModal.png" id="hidelinkNew" styleClass="hidelink" />
           <rich:componentControl for="detPanel" attachTo="hidelinkNew" operation="hide" event="onclick" />
           <rich:componentControl attachTo="hidelinkNew" operation="show" for="toolTip" event="onmouseover" />
       </h:panelGroup>
   </f:facet>
   
   <h:form id="detalleCedula">
       <rich:messages styleClass="errorMessage"></rich:messages>
       <h:panelGrid columns="1" width="100%">			
			<h:panelGrid columns="5" width="30%">
               	<h:outputText value="#{msgs.sistemaacciones}:"/>
				<h:commandLink styleClass="boton"
							id="btnXLS"
							action="#{beanCedula.descargaDetalle}">																		
					<h:graphicImage value="../../images/type_xls.png" styleClass="bor0" />
				</h:commandLink>                		
				<rich:toolTip for="btnXLS" value="Descarga detalle de creditos" />
              		
           		<a4j:commandLink styleClass="boton"
							id="btnFoto"						
							action="#{beanCedula.saveFoto}"
							onclick="this.disabled=true"
							oncomplete="if (#{facesContext.maximumSeverity==null}) #{rich:component('detPanel')}.hide();
										muestraOcultaSeccion('actual', 0);
										muestraOcultaSeccion('historico', 0);"
							reRender="mensaje,regTabla,regTablaP">
					<h:graphicImage value="../../images/type_file.png" styleClass="bor0"/>
				</a4j:commandLink>
              	<rich:toolTip for="btnFoto" value="Guarda foto de cedula" />
			</h:panelGrid>			
           	               
			<rich:dataTable id="listaDetalle" value="#{beanCedula.listaDetalle}"
					columnsWidth="10px,8px,8px,8px,8px,8px,8px,8px,8px,8px"
					var="detalle" rows="35" width="100%">
				<f:facet name="header">
					<h:outputText value="#{msgs.deculadetalle} - #{beanCedula.seleccionado.coccRemesaCedula}" />
				</f:facet>
			
				<rich:column styleClass="texto_centro" >
					<f:facet name="header"><h:outputText value="#{msgs.deculafeen}" /></f:facet>
					<h:outputText value="#{detalle.coccFeEntrega}" >
						<f:convertDateTime pattern="dd/MM/yyyy" />
					</h:outputText>
				</rich:column>
				<rich:column styleClass="texto_centro" >
					<f:facet name="header"><h:outputText value="#{msgs.deculafepr}" /></f:facet>
					<h:outputText value="#{detalle.coccFeProceso}" >
						<f:convertDateTime pattern="dd/MM/yyyy" />
					</h:outputText>
				</rich:column>
				<rich:column styleClass="texto_centro" >
					<f:facet name="header"><h:outputText value="#{msgs.deculafeco}" /></f:facet>
					<h:outputText value="#{detalle.coccFeComer}" >
						<f:convertDateTime pattern="dd/MM/yyyy" />
					</h:outputText>
				</rich:column>
				<rich:column styleClass="texto_centro" title="Total de Registros">
					<f:facet name="header"><h:outputText value="#{msgs.cedular}" /></f:facet>
					<h:outputText value="#{detalle.coccNuRegistros}" />
				</rich:column>
					<rich:column styleClass="texto_centro"  title="Total de Registros PND">
					<f:facet name="header"><h:outputText value="#{msgs.cedularp}" /></f:facet>
					<h:outputText value="#{detalle.coccNuRegPnd}" />
				</rich:column>
					<rich:column styleClass="texto_centro" title="Total de Registros Emitidos o Cancelados">
					<f:facet name="header"><h:outputText value="#{msgs.cedularo}" /></f:facet>
					<h:outputText value="#{detalle.coccNuRegOk}" />
				</rich:column>
					<rich:column styleClass="texto_centro" title="Total de Prima Emitida o Cancelada">
					<f:facet name="header"><h:outputText value="#{msgs.cedulapo}" /></f:facet>
					<h:outputText value="#{detalle.coccMtPrimaOk}" >
						<f:convertNumber currencySymbol="$" type="currency"/>
					</h:outputText>
				</rich:column>
				<rich:column styleClass="texto_centro" title="Total de Registros No Emitidos o No Cancelados">
					<f:facet name="header"><h:outputText value="#{msgs.cedularno}" /></f:facet>
					<h:outputText value="#{detalle.coccNuRegNook}" />
				</rich:column>
				<rich:column styleClass="texto_centro" >
					<f:facet name="header"><h:outputText value="#{msgs.cedulapno}" /></f:facet>
					<h:outputText value="#{detalle.coccMtPrimaNook}">
						<f:convertNumber currencySymbol="$" type="currency"/>
					</h:outputText>
				</rich:column>
					<rich:column styleClass="texto_centro" >
					<f:facet name="header"><h:outputText value="#{msgs.cedularsp}" /></f:facet>
					<h:outputText value="#{detalle.coccNuRegSinpnd}" />
				</rich:column>
					<rich:column styleClass="texto_centro" >
					<f:facet name="header"><h:outputText value="#{msgs.cedularspo}" /></f:facet>
					<h:outputText value="#{detalle.coccNuRegSinpndOk}" />
				</rich:column>
				<rich:column styleClass="texto_centro" >
					<f:facet name="header"><h:outputText value="#{msgs.cedularspno}" /></f:facet>
					<h:outputText value="#{detalle.coccNuRegSinpndNook}" />
				</rich:column>				
				
				<f:facet name="footer">
		            <rich:columnGroup>
		                <rich:column colspan="3"><h:outputText value="#{msgs.errorestot}" /></rich:column>
		                <rich:column styleClass="texto_centro" ><h:outputText value="#{beanCedula.seleccionado.coccNuRegistros}" /></rich:column>
		                <rich:column styleClass="texto_centro" ><h:outputText value="#{beanCedula.seleccionado.coccNuRegPnd}" /></rich:column>
		                <rich:column styleClass="texto_centro" ><h:outputText value="#{beanCedula.seleccionado.coccNuRegOk}" /></rich:column>
		                <rich:column styleClass="texto_centro" >
		                    <h:outputText value="#{beanCedula.seleccionado.coccMtPrima}" >
								<f:convertNumber currencySymbol="$" type="currency"/>
							</h:outputText>
		                </rich:column>
		                <rich:column styleClass="texto_centro" ><h:outputText value="#{beanCedula.seleccionado.coccNuRegNook}" /></rich:column>
		                <rich:column styleClass="texto_centro" >
		                    <h:outputText value="#{beanCedula.seleccionado.coccMtPrimaNook}">
								<f:convertNumber currencySymbol="$" type="currency"/>
							</h:outputText>
		                </rich:column>
		                <rich:column styleClass="texto_centro" ><h:outputText value="#{beanCedula.seleccionado.coccNuRegSinpnd}" /></rich:column>
		                <rich:column styleClass="texto_centro" ><h:outputText value="#{beanCedula.seleccionado.coccNuRegSinpndOk}" /></rich:column>
		                <rich:column styleClass="texto_centro" ><h:outputText value="#{beanCedula.seleccionado.coccNuRegSinpndNook}" /></rich:column>
		            </rich:columnGroup>
		        </f:facet>
			</rich:dataTable>
			<rich:spacer height="10px" />
       </h:panelGrid>
   </h:form>
</rich:modalPanel>