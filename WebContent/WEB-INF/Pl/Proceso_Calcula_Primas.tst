DECLARE 
	paramCanal  number := ?;
	paramRamo   number := ?;
	paramPoliza	number := ?;
	paramCerti  number := ?;
	paramSuma	number(11,2) := ?;
  	
	carga 		COLECTIVOS_CARGA%rowtype;		
	cobertura 	COLECTIVOS.t_coberturaCertificado;
	componentes COLECTIVOS.tablaComponentes;
	primaMensual	number(15,6);
	primaReal 	number(15,6);
	primaAnualReal 	number(15,6);
	primaPuraMensualReal	number(15,6);
	componentesCertificado 	varchar2(200);
	 
BEGIN
   
	primaMensual := 0;
	primaAnualReal := 0;
	primaReal := 0;
	primaPuraMensualReal := 0;
	componentesCertificado := '';

	dbms_output.put_line('1. Get registro carga');
	SELECT CR.* INTO carga
		FROM COLECTIVOS_CARGA CR
	WHERE CR.COCR_CD_SUCURSAL		= paramCanal
		AND CR.COCR_CD_RAMO     	= paramRamo
		AND CR.COCR_NUM_POLIZA  	= paramPoliza
		AND CR.COCR_ID_CERTIFICADO  = paramCerti
		AND CR.COCR_TIPO_REGISTRO   = 'END'
		AND CR.COCR_SUMA_ASEGURADA	= paramSuma; 

	dbms_output.put_line('2. Funcion Prima Pura');	
	cobertura := COLECTIVOS.primaPura(paramCanal => carga.cocr_cd_sucursal,
						paramRamo => carga.cocr_cd_ramo,
						paramPoliza => carga.cocr_num_poliza,
						paramProducto => carga.cocr_cd_producto,
						paramPlan => carga.cocr_cd_plan,
						paramSumaAsegurada => carga.cocr_suma_asegurada);
	
	dbms_output.put_line('3. Funcion Prima Neta');		
	componentes := COLECTIVOS.primaNeta(paramCanal => carga.cocr_cd_sucursal,
							paramRamo => carga.cocr_cd_ramo,
							paramPoliza => carga.cocr_num_poliza,
							paramProducto => carga.cocr_cd_producto,
							paramPlan => carga.cocr_cd_plan,
							cobertura => cobertura);
	
	dbms_output.put_line('4. Calcula Prima Mensual');		
	FOR i IN componentes.first .. componentes.last
	LOOP
		IF(i <> 4 AND i <> 5) THEN -- componentes COM no tomar en cuenta
			primaMensual := primaMensual + NVL(componentes(i).monto,0);
		END IF;
		componentesCertificado := componentesCertificado||componentes(i).componente||componentes(i).monto||'|';
	END LOOP;

	dbms_output.put_line('5. Asignando Valores');		
	componentesCertificado := componentesCertificado||'PMAP'||cobertura.primaPura||'|';	
	primaMensual := NVL(primaMensual,0) + NVL(cobertura.primaPura,0);
	
	dbms_output.put_line('6. Calculando Prima REAL y ANUAL REAL');		
	primaReal := COLECTIVOS.primaRealMensual(paramPrima => primaMensual, paramFecha => carga.cocr_fe_ingreso);
	primaPuraMensualReal := COLECTIVOS.primaPuraMensualPro(paramPrimaPura => primaMensual,paramFechaEmision => carga.cocr_fe_ingreso);
	primaAnualReal := COLECTIVOS.primaRealAnual(paramPrimaMensual => primaMensual, paramPrimaReal =>  primaReal, paramPlazo => carga.COCR_NU_PLAZO);

	dbms_output.put_line('7. Update registro');		
	UPDATE COLECTIVOS_CARGA CC SET 
			CC.COCR_PRIMA_REAL = primaReal,								   
			CC.COCR_DATO28	= primaMensual * carga.COCR_NU_PLAZO, -- prima anual
			CC.COCR_DATO29 	= cobertura.primaPura, -- prima pura mensual
			CC.COCR_DATO30 	= primaAnualReal,  -- prima anual real
			CC.COCR_DATO26 	= primaPuraMensualReal, -- prima pura prorateada
			CC.COCR_DATO24 	= componentesCertificado, -- componentes del certificado								   								   
			CC.COCR_COMISIONES = COLECTIVOS.esDatosComisiones,
			CC.COCR_PRIMA 	= primaMensual
	where CC.COCR_CD_SUCURSAL	= carga.cocr_cd_sucursal
		and CC.COCR_CD_RAMO 	= carga.cocr_cd_ramo
		and CC.COCR_NUM_POLIZA 	= carga.cocr_num_poliza
		and CC.COCR_ID_CERTIFICADO	= carga.cocr_id_certificado
		and CC.COCR_TIPO_REGISTRO 	= carga.cocr_tipo_registro
		and CC.COCR_SUMA_ASEGURADA 	= carga.cocr_suma_asegurada;
	
END;