WITH 
	dxp_total AS (
		SELECT T.*, D.CODX_MT_DIFERENCIA_PNETA, D.CODX_MT_DIFERENCIA_PTOTAL, D.CODX_NU_TIPO_DXP, D.CODX_MT_PRIMA_NETA, D.CODX_MT_PRIMA_TOTAL
			FROM (
				SELECT CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO, MAX(CODX_NU_PK) CODX_NU_PK, MAX(CODX_NU_MOVIMIENTO) CODX_NU_MOVIMIENTO, SUM(CODX_MT_NETO) CODX_MT_NETO, SUM(CODX_MT_TOTAL) CODX_MT_TOTAL
					FROM COLECTIVOS_DXP C
				WHERE C.CODX_CASU_CD_SUCURSAL 		= pjSucursal
					AND C.CODX_CARP_CD_RAMO 		= pjRamo
					AND C.CODX_CAPO_NU_POLIZA 		= pjPoliza
					AND C.CODX_COCE_NU_CERTIFICADO 	> 0			
					AND C.CODX_NU_TIPO_DXP 			NOT IN(6,8)
					AND C.CODX_FE_CONTABLE_REG 		<= TO_DATE('pjFeHasta', 'DD/MM/YYYY')
					AND C.CODX_NU_PK = (
							SELECT MAX (CODX_NU_PK) FROM COLECTIVOS_DXP CD
							WHERE CD.CODX_CASU_CD_SUCURSAL 		= C.CODX_CASU_CD_SUCURSAL
								AND CD.CODX_CARP_CD_RAMO 		= C.CODX_CARP_CD_RAMO
								AND CD.CODX_CAPO_NU_POLIZA 		= C.CODX_CAPO_NU_POLIZA
								AND CD.CODX_COCE_NU_CERTIFICADO	= C.CODX_COCE_NU_CERTIFICADO
								AND CD.CODX_FE_CONTABLE_REG 	<= TO_DATE('pjFeHasta', 'DD/MM/YYYY')
								AND CD.CODX_NU_TIPO_DXP NOT IN(6,8)
								AND CD.CODX_FE_INICIO_VIGENCIA  = C.CODX_FE_INICIO_VIGENCIA
							)
				GROUP BY CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO
			) T
			INNER JOIN COLECTIVOS_DXP D ON(D.CODX_NU_PK = T.CODX_NU_PK)
    ),
	dxp_exi AS (
        SELECT CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO, MAX(CODX_NU_MOVIMIENTO) CODX_NU_MOVIMIENTO, SUM(CODX_MT_NETO) CODX_MT_NETO, SUM(CODX_MT_TOTAL) CODX_MT_TOTAL
            FROM COLECTIVOS_DXP C
        WHERE C.CODX_CASU_CD_SUCURSAL 		= pjSucursal
            AND C.CODX_CARP_CD_RAMO 		= pjRamo
            AND C.CODX_CAPO_NU_POLIZA 		= pjPoliza
            AND C.CODX_COCE_NU_CERTIFICADO 	> 0     
            AND C.CODX_NU_TIPO_DXP 			= 8
            AND C.CODX_FE_CONTABLE_REG 		<= TO_DATE('pjFeHasta', 'DD/MM/YYYY')
            AND C.CODX_NU_PK = (
					SELECT MAX (CODX_NU_PK) FROM COLECTIVOS_DXP CD
					WHERE CD.CODX_CASU_CD_SUCURSAL 		= C.CODX_CASU_CD_SUCURSAL
						AND CD.CODX_CARP_CD_RAMO 		= C.CODX_CARP_CD_RAMO
						AND CD.CODX_CAPO_NU_POLIZA 		= C.CODX_CAPO_NU_POLIZA
						AND CD.CODX_COCE_NU_CERTIFICADO	= C.CODX_COCE_NU_CERTIFICADO
						AND CD.CODX_FE_CONTABLE_REG 	<= TO_DATE('pjFeHasta', 'DD/MM/YYYY')
                        AND CD.CODX_NU_TIPO_DXP 		= 8 
						AND CD.CODX_FE_INICIO_VIGENCIA  = C.CODX_FE_INICIO_VIGENCIA
                    )
		GROUP BY CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO
    ), 
	cobranza AS ( 
		SELECT C.CODX_CASU_CD_SUCURSAL, C.CODX_CARP_CD_RAMO, C.CODX_CAPO_NU_POLIZA, C.CODX_COCE_NU_CERTIFICADO, 
                (SUM(CODX_MT_AJUSTE_NETO)* -1) MCPN, (SUM(CODX_MT_AJUSTE_TOTAL)* -1) MCPT                
            FROM COLECTIVOS_DXP C 
		WHERE C.CODX_CASU_CD_SUCURSAL 		= pjSucursal 
			AND C.CODX_CARP_CD_RAMO 		= pjRamo
			AND C.CODX_CAPO_NU_POLIZA 		= pjPoliza
			AND C.CODX_COCE_NU_CERTIFICADO	> 0			
			AND C.CODX_FE_CONTABLE_REG 		<= TO_DATE('pjFeHasta', 'DD/MM/YYYY')
			AND C.CODX_NU_TIPO_DXP 			= 6 
        GROUP BY C.CODX_CASU_CD_SUCURSAL, C.CODX_CARP_CD_RAMO, C.CODX_CAPO_NU_POLIZA, C.CODX_COCE_NU_CERTIFICADO 
    ),
	facturacion AS ( 
        SELECT DXP.CODX_CASU_CD_SUCURSAL, DXP.CODX_CARP_CD_RAMO, DXP.CODX_CAPO_NU_POLIZA, DXP.CODX_COCE_NU_CERTIFICADO, 
                SUM(DXP.CODX_MT_AJUSTE_NETO) FAPN, SUM(DXP.CODX_MT_AJUSTE_TOTAL) FAPT 
			FROM COLECTIVOS_DXP DXP 
		WHERE DXP.CODX_CASU_CD_SUCURSAL 		= pjSucursal 
			AND DXP.CODX_CARP_CD_RAMO 			= pjRamo
			AND DXP.CODX_CAPO_NU_POLIZA 		= pjPoliza
			AND DXP.CODX_COCE_NU_CERTIFICADO	> 0			
			AND DXP.CODX_FE_CONTABLE_REG 		<=  TO_DATE('pjFeHasta', 'DD/MM/YYYY')
			AND DXP.CODX_NU_TIPO_DXP 			= 8 
			AND DXP.CODX_MT_AJUSTE_TOTAL 		> 0 
        GROUP BY  CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO
	)			
SELECT (
            SELECT CC.COCE_CARP_CD_RAMO	|| 
						'|' || PL.ALPL_DATO3  	|| 
						'|' || CC.COCE_CAPO_NU_POLIZA || 
						'|' || CC.COCE_NO_RECIBO || 
						'|' || CCC.COCC_ID_CERTIFICADO || 
						'|' || CC.COCE_NU_CERTIFICADO || 				
						'|' || NVL(PL.ALPL_DATO2, PL.ALPL_DE_PLAN) || 							
						'|' || EST.ALES_CAMPO1	||
						'|' || 'ALTA' ||						
						'|' || CC.COCE_NU_CUENTA	||                                                                                                                                     
						'|' || CC.COCE_TP_PRODUCTO_BCO	||                                                                                                                        
						'|' || CC.COCE_TP_SUBPROD_BCO   ||   
						'|' || CC.COCE_CAZB_CD_SUCURSAL ||   						
						'|' || CL.COCN_APELLIDO_PAT     ||
						'|' || CL.COCN_APELLIDO_MAT     ||
						'|' || CL.COCN_NOMBRE           ||
						'|' || CL.COCN_CD_SEXO          ||
						'|' || CL.COCN_FE_NACIMIENTO    ||
						'|' || CL.COCN_BUC_CLIENTE      ||
						'|' || CL.COCN_RFC              ||                                                                                                                                                       
						'|' || CC.COCE_CD_PLAZO  		||	   						
						'|' || HEAD.COCE_FE_DESDE 		||
						'|' || HEAD.COCE_FE_HASTA		||
						'|' || CC.COCE_FE_SUSCRIPCION 	||
						'|' || CC.COCE_FE_DESDE 		||
						'|' || CC.COCE_FE_HASTA			||
						'|' || CC.COCE_FE_INI_CREDITO 	||
						'|' || CC.COCE_FE_FIN_CREDITO  	||
						'|' || REPLACE(CC.COCE_CAMPOV6, '|', '-') || 
						'|' || CC.COCE_FE_ANULACION || 							
						'|' || CC.COCE_FE_ANULACION_COL 		||				
						'|' || TRIM(TO_CHAR(DECODE(CC.COCE_SUB_CAMPANA,'7',TO_NUMBER(CC.COCE_MT_SUMA_ASEG_SI),CC.COCE_MT_SUMA_ASEGURADA),'999999999.99')) ||
						'|' || DECODE(CC.COCE_SUB_CAMPANA,'7',CC.COCE_MT_SUMA_ASEGURADA,CC.COCE_MT_SUMA_ASEG_SI) ||										
						'|' || CASE WHEN PF.COPA_NVALOR5 = 0 THEN 
										NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'P',1,3) + 1,INSTR(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'P',1,3)+1),'|',1,1)-1)),0) 
									WHEN PF.COPA_NVALOR5 = 1 THEN 
										CC.COCE_MT_PRIMA_PURA                                                                         
									ELSE 0 END  ||
						'|' || TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'O',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,1) -INSTR(CC.COCE_DI_COBRO1, 'O',1,1)-1)) ||            
						'|' || TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'I',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,2) -INSTR(CC.COCE_DI_COBRO1, 'I',1,1)-1)) ||
						'|' || TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'A',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,3)-INSTR(CC.COCE_DI_COBRO1, 'A',1,1) -1)) ||
						'|' || CASE WHEN PF.COPA_NVALOR5 = 0 THEN 
										NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'O',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,1)-INSTR(CC.COCE_DI_COBRO1, 'O',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'I',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,2)-INSTR(CC.COCE_DI_COBRO1, 'I',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'A',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,3)-INSTR(CC.COCE_DI_COBRO1, 'A',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'P',1,3) + 1,INSTR(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'P',1,3)+1),'|',1,1)-1)),0) 
									WHEN PF.COPA_NVALOR5 = 1 THEN 
										NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'O',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,1)-INSTR(CC.COCE_DI_COBRO1, 'O',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'I',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,2)-INSTR(CC.COCE_DI_COBRO1, 'I',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CC.COCE_DI_COBRO1,INSTR(CC.COCE_DI_COBRO1, 'A',1,1) + 1,INSTR(CC.COCE_DI_COBRO1, '|',1,3)-INSTR(CC.COCE_DI_COBRO1, 'A',1,1) -1)),0) 
										+ NVL(CC.COCE_MT_PRIMA_PURA,0) 
									ELSE 0 END || 
						'|' || ROUND(CC.COCE_MT_PRIMA_PURA*1000/DECODE(CC.COCE_SUB_CAMPANA,'7',DECODE(CC.COCE_MT_SUMA_ASEGURADA,0,1,CC.COCE_MT_SUMA_ASEGURADA),DECODE(CC.COCE_MT_SUMA_ASEG_SI,0,1,CC.COCE_MT_SUMA_ASEG_SI)),4) ||
						'|' || ROUND(CASE WHEN CC.COCE_CARP_CD_RAMO IN (61,65) THEN (DECODE(CC.COCE_SUB_CAMPANA,'7',CC.COCE_MT_SUMA_ASEGURADA,CC.COCE_MT_SUMA_ASEG_SI)/1000) * 
									(SELECT SUM (COB.COCB_TA_RIESGO) TASAVIDA                                                                           
									  FROM COLECTIVOS_COBERTURAS COB                                                                                                              
									 WHERE COB.COCB_CASU_CD_SUCURSAL 	=  1                                                                                  
									   AND COB.COCB_CARP_CD_RAMO     	= CC.COCE_CARP_CD_RAMO                             
									   AND COB.COCB_CARB_CD_RAMO     	= 14                                                                                     
									 AND COB.COCB_CAPO_NU_POLIZA     	= CC.COCE_CAPO_NU_POLIZA   
									   AND COB.COCB_CACB_CD_COBERTURA 	<> '003'                                                                       
									   AND COB.COCB_CAPU_CD_PRODUCTO   	= CC.COCE_CAPU_CD_PRODUCTO 
									   AND COB.COCB_CAPB_CD_PLAN       	= CC.COCE_CAPB_CD_PLAN              
									   AND COB.COCB_CER_NU_COBERTURA   	= CC.COCE_NU_COBERTURA)         
									END,2) ||
						'|' || ROUND(CASE WHEN CC.COCE_CARP_CD_RAMO = 61 THEN (DECODE(CC.COCE_SUB_CAMPANA,'7',CC.COCE_MT_SUMA_ASEGURADA,CC.COCE_MT_SUMA_ASEG_SI)/1000) * 
									(SELECT SUM (COB.COCB_TA_RIESGO) TASADES                                                                             
									  FROM COLECTIVOS_COBERTURAS COB                                                                                                              
									 WHERE COB.COCB_CASU_CD_SUCURSAL 	=  1                                                                                  
									   AND COB.COCB_CARP_CD_RAMO     	= 61                                                                                     
									   AND COB.COCB_CARB_CD_RAMO     	= 14                                                                                     
									 AND COB.COCB_CAPO_NU_POLIZA     	= CC.COCE_CAPO_NU_POLIZA   
									   AND COB.COCB_CACB_CD_COBERTURA 	= '003'                                                                          
									   AND COB.COCB_CAPU_CD_PRODUCTO   	= CC.COCE_CAPU_CD_PRODUCTO 
									   AND COB.COCB_CAPB_CD_PLAN       	= CC.COCE_CAPB_CD_PLAN              
									   AND COB.COCB_CER_NU_COBERTURA   	= CC.COCE_NU_COBERTURA)         
									END,2) ||
						'|' || CL.COCN_CD_ESTADO ||		
						'|' || ES.CAES_DE_ESTADO ||
						'|' || CL.COCN_DELEGMUNIC ||
						'|' || CL.COCN_CD_POSTAL || 				
						'|' || NVL(CC.COCE_MT_BCO_DEVOLUCION,0) || 
						'|' || NVL(CC.COCE_MT_DEVOLUCION,0) ||
						'|' || ABS( NVL(CC.COCE_MT_DEVOLUCION ,0) - NVL(CC.COCE_MT_BCO_DEVOLUCION ,0) )  ||
						'|' || NVL(CC.COCE_BUC_EMPRESA,0) ||
						'|' || CC.COCE_CAPU_CD_PRODUCTO ||
						'|' || CC.COCE_CAPB_CD_PLAN ||				
						'|' || (SELECT SUM (COB.COCB_TA_RIESGO)                         
									FROM COLECTIVOS_COBERTURAS COB                    
								WHERE COB.COCB_CASU_CD_SUCURSAL =  1               
									AND COB.COCB_CARP_CD_RAMO     = 61               
									AND COB.COCB_CARB_CD_RAMO     = 14               
									AND COB.COCB_CACB_CD_COBERTURA  <> '003'         
									AND COB.COCB_CAPU_CD_PRODUCTO   = CC.COCE_CAPU_CD_PRODUCTO           
									AND COB.COCB_CAPB_CD_PLAN       = CC.COCE_CAPB_CD_PLAN               
									AND COB.COCB_CER_NU_COBERTURA   = CC.COCE_NU_COBERTURA) ||
						'|' || (SELECT SUM (COB.COCB_TA_RIESGO)                                            
									FROM COLECTIVOS_COBERTURAS COB                                       
								WHERE COB.COCB_CASU_CD_SUCURSAL =  1                                  
									AND COB.COCB_CARP_CD_RAMO     = 61                                  
									AND COB.COCB_CARB_CD_RAMO     = 14                                  
									AND COB.COCB_CACB_CD_COBERTURA  = '003'                             
									AND COB.COCB_CAPU_CD_PRODUCTO   = CC.COCE_CAPU_CD_PRODUCTO           
									AND COB.COCB_CAPB_CD_PLAN       = CC.COCE_CAPB_CD_PLAN               
									AND COB.COCB_CER_NU_COBERTURA   = CC.COCE_NU_COBERTURA) ||				
						'|' ||
						'|' ||
						'|' ||
						'|' ||
						'|' LINEA                       
				FROM COLECTIVOS_CERTIFICADOS CC 
				INNER JOIN COLECTIVOS_CLIENTE_CERTIF CCC ON(CCC.COCC_CASU_CD_SUCURSAL = CC.COCE_CASU_CD_SUCURSAL                                        
													AND CCC.COCC_CARP_CD_RAMO     = CC.COCE_CARP_CD_RAMO                                                             
													AND CCC.COCC_CAPO_NU_POLIZA   = CC.COCE_CAPO_NU_POLIZA                                                  
													AND CCC.COCC_NU_CERTIFICADO   = CC.COCE_NU_CERTIFICADO                                                      
													AND NVL(CCC.COCC_TP_CLIENTE,1)= 1)           
				INNER JOIN COLECTIVOS_CLIENTES CL ON(CL.COCN_NU_CLIENTE = CCC.COCC_NU_CLIENTE) 							
				INNER JOIN COLECTIVOS_CERTIFICADOS HEAD ON(HEAD.COCE_CASU_CD_SUCURSAL = CC.COCE_CASU_CD_SUCURSAL 				
													AND HEAD.COCE_CARP_CD_RAMO     = CC.COCE_CARP_CD_RAMO 					
													AND HEAD.COCE_CAPO_NU_POLIZA   = CC.COCE_CAPO_NU_POLIZA 				
													AND HEAD.COCE_NU_CERTIFICADO   = 0) 
				INNER JOIN ALTERNA_ESTATUS EST ON(EST.ALES_CD_ESTATUS = CC.COCE_ST_CERTIFICADO)
				LEFT JOIN COLECTIVOS_PARAMETROS PF ON(PF.COPA_DES_PARAMETRO 	= 'POLIZA'                                                                                       
													AND PF.COPA_ID_PARAMETRO	= 'GEPREFAC'                                                                                              
													AND PF.COPA_NVALOR1         = CC.COCE_CASU_CD_SUCURSAL 
													AND PF.COPA_NVALOR2         = CC.COCE_CARP_CD_RAMO
													AND PF.COPA_NVALOR3         = DECODE(PF.COPA_NVALOR4, 0, CC.COCE_CAPO_NU_POLIZA, 1, SUBSTR(CC.COCE_CAPO_NU_POLIZA,0,3), SUBSTR(CC.COCE_CAPO_NU_POLIZA,0,2)) 
													AND PF.COPA_NVALOR6         = TO_NUMBER(NVL(CC.COCE_SUB_CAMPANA,'0')))
				LEFT JOIN ALTERNA_PLANES PL	ON(PL.ALPL_CD_RAMO	= CC.COCE_CARP_CD_RAMO                                                                       
													AND PL.ALPL_CD_PRODUCTO	= CC.COCE_CAPU_CD_PRODUCTO                                                           
													AND PL.ALPL_CD_PLAN       = CC.COCE_CAPB_CD_PLAN)  				 
				LEFT JOIN CART_ESTADOS ES ON(ES.CAES_CD_ESTADO = CL.COCN_CD_ESTADO)            
			WHERE CC.COCE_CASU_CD_SUCURSAL	= d.CODX_CASU_CD_SUCURSAL
				AND CC.COCE_CARP_CD_RAMO	= d.CODX_CARP_CD_RAMO
				AND CC.COCE_CAPO_NU_POLIZA  = d.CODX_CAPO_NU_POLIZA 
				AND CC.COCE_NU_CERTIFICADO  = d.CODX_COCE_NU_CERTIFICADO
                AND CC.COCE_NU_MOVIMIENTO   = DECODE(d.CODX_NU_TIPO_DXP, 3, e.CODX_NU_MOVIMIENTO, d.CODX_NU_MOVIMIENTO) 	                                                                                            
			UNION
			SELECT CM.COCM_CARP_CD_RAMO	|| 
						'|' || PL.ALPL_DATO3  	|| 
						'|' || CM.COCM_CAPO_NU_POLIZA || 
						'|' || CM.COCM_NU_RECIBO || 
						'|' || CCC.COCC_ID_CERTIFICADO || 
						'|' || CM.COCM_NU_CERTIFICADO || 				
						'|' || NVL(PL.ALPL_DATO2, PL.ALPL_DE_PLAN) || 							
						'|' || EST.ALES_CAMPO1	||
						'|' || 'ALTA' ||						
						'|' || CM.COCM_NU_CUENTA	||                                                                                                                                     
						'|' || CM.COCM_TP_PRODUCTO_BCO	||                                                                                                                        
						'|' || CM.COCM_TP_SUBPROD_BCO   ||   
						'|' || CM.COCM_CAZB_CD_SUCURSAL ||   						
						'|' || CL.COCN_APELLIDO_PAT     ||
						'|' || CL.COCN_APELLIDO_MAT     ||
						'|' || CL.COCN_NOMBRE           ||
						'|' || CL.COCN_CD_SEXO          ||
						'|' || CL.COCN_FE_NACIMIENTO    ||
						'|' || CL.COCN_BUC_CLIENTE      ||
						'|' || CL.COCN_RFC              ||                                                                                                                                                       
						'|' || CM.COCM_CD_PLAZO  		||	   						
						'|' || HEAD.COCE_FE_DESDE 		||
						'|' || HEAD.COCE_FE_HASTA		||
						'|' || CM.COCM_FE_SUSCRIPCION 	||
						'|' || CM.COCM_FE_DESDE 		||
						'|' || CM.COCM_FE_HASTA			||
						'|' || CM.COCM_FE_INI_CREDITO 	||
						'|' || CM.COCM_FE_FIN_CREDITO  	||
						'|' || REPLACE(CM.COCM_CAMPOV6, '|', '-') || 
						'|' || CM.COCM_FE_ANULACION_REAL || 							
						'|' || CM.COCM_FE_ANULACION 		||				
						'|' || TRIM(TO_CHAR(DECODE(CM.COCM_SUB_CAMPANA,'7',TO_NUMBER(CM.COCM_MT_SUMA_ASEG_SI),CM.COCM_MT_SUMA_ASEGURADA),'999999999.99')) ||
						'|' || DECODE(CM.COCM_SUB_CAMPANA,'7',CM.COCM_MT_SUMA_ASEGURADA,CM.COCM_MT_SUMA_ASEG_SI) ||										
						'|' || CASE WHEN PF.COPA_NVALOR5 = 0 THEN 
										NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'P',1,3) + 1,INSTR(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'P',1,3)+1),'|',1,1)-1)),0) 
									WHEN PF.COPA_NVALOR5 = 1 THEN 
										CM.COCM_MT_PRIMA_PURA                                                                         
									ELSE 0 END  ||
						'|' || TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'O',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,1) -INSTR(CM.COCM_DI_COBRO1, 'O',1,1)-1)) ||            
						'|' || TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'I',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,2) -INSTR(CM.COCM_DI_COBRO1, 'I',1,1)-1)) ||
						'|' || TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'A',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,3)-INSTR(CM.COCM_DI_COBRO1, 'A',1,1) -1)) ||
						'|' || CASE WHEN PF.COPA_NVALOR5 = 0 THEN 
										NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'O',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,1)-INSTR(CM.COCM_DI_COBRO1, 'O',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'I',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,2)-INSTR(CM.COCM_DI_COBRO1, 'I',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'A',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,3)-INSTR(CM.COCM_DI_COBRO1, 'A',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'P',1,3) + 1,INSTR(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'P',1,3)+1),'|',1,1)-1)),0) 
									WHEN PF.COPA_NVALOR5 = 1 THEN 
										NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'O',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,1)-INSTR(CM.COCM_DI_COBRO1, 'O',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'I',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,2)-INSTR(CM.COCM_DI_COBRO1, 'I',1,1) -1)),0) 
										+ NVL(TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'A',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,3)-INSTR(CM.COCM_DI_COBRO1, 'A',1,1) -1)),0) 
										+ NVL(CM.COCM_MT_PRIMA_PURA,0) 
									ELSE 0 END || 
						'|' || ROUND(CM.COCM_MT_PRIMA_PURA*1000/DECODE(CM.COCM_SUB_CAMPANA,'7',DECODE(CM.COCM_MT_SUMA_ASEGURADA,0,1,CM.COCM_MT_SUMA_ASEGURADA),DECODE(CM.COCM_MT_SUMA_ASEG_SI,0,1,CM.COCM_MT_SUMA_ASEG_SI)),4) ||
						'|' || ROUND(CASE WHEN CM.COCM_CARP_CD_RAMO IN (61,65) THEN (DECODE(CM.COCM_SUB_CAMPANA,'7',CM.COCM_MT_SUMA_ASEGURADA,CM.COCM_MT_SUMA_ASEG_SI)/1000) * 
									(SELECT SUM (COB.COCB_TA_RIESGO) TASAVIDA                                                                           
									  FROM COLECTIVOS_COBERTURAS COB                                                                                                              
									 WHERE COB.COCB_CASU_CD_SUCURSAL 	=  1                                                                                  
									   AND COB.COCB_CARP_CD_RAMO     	= CM.COCM_CARP_CD_RAMO                             
									   AND COB.COCB_CARB_CD_RAMO     	= 14                                                                                     
									 AND COB.COCB_CAPO_NU_POLIZA     	= CM.COCM_CAPO_NU_POLIZA   
									   AND COB.COCB_CACB_CD_COBERTURA 	<> '003'                                                                       
									   AND COB.COCB_CAPU_CD_PRODUCTO   	= CM.COCM_CAPU_CD_PRODUCTO 
									   AND COB.COCB_CAPB_CD_PLAN       	= CM.COCM_CAPB_CD_PLAN              
									   AND COB.COCB_CER_NU_COBERTURA   	= CM.COCM_NU_COBERTURA)         
									END,2) ||
						'|' || ROUND(CASE WHEN CM.COCM_CARP_CD_RAMO = 61 THEN (DECODE(CM.COCM_SUB_CAMPANA,'7',CM.COCM_MT_SUMA_ASEGURADA,CM.COCM_MT_SUMA_ASEG_SI)/1000) * 
									(SELECT SUM (COB.COCB_TA_RIESGO) TASADES                                                                             
									  FROM COLECTIVOS_COBERTURAS COB                                                                                                              
									 WHERE COB.COCB_CASU_CD_SUCURSAL 	=  1                                                                                  
									   AND COB.COCB_CARP_CD_RAMO     	= 61                                                                                     
									   AND COB.COCB_CARB_CD_RAMO     	= 14                                                                                     
									 AND COB.COCB_CAPO_NU_POLIZA     	= CM.COCM_CAPO_NU_POLIZA   
									   AND COB.COCB_CACB_CD_COBERTURA 	= '003'                                                                          
									   AND COB.COCB_CAPU_CD_PRODUCTO   	= CM.COCM_CAPU_CD_PRODUCTO 
									   AND COB.COCB_CAPB_CD_PLAN       	= CM.COCM_CAPB_CD_PLAN              
									   AND COB.COCB_CER_NU_COBERTURA   	= CM.COCM_NU_COBERTURA)         
									END,2) ||
						'|' || CL.COCN_CD_ESTADO ||		
						'|' || ES.CAES_DE_ESTADO ||
						'|' || CL.COCN_DELEGMUNIC ||
						'|' || CL.COCN_CD_POSTAL || 				
						'|' || NVL(CM.COCM_MT_BCO_DEVOLUCION,0) || 
						'|' || NVL(CM.COCM_MT_DEVOLUCION,0) ||
						'|' || ABS( NVL(CM.COCM_MT_DEVOLUCION ,0) - NVL(CM.COCM_MT_BCO_DEVOLUCION ,0) )  ||
						'|' || NVL(CM.COCM_BUC_EMPRESA,0) ||
						'|' || CM.COCM_CAPU_CD_PRODUCTO ||
						'|' || CM.COCM_CAPB_CD_PLAN ||				
						'|' || (SELECT SUM (COB.COCB_TA_RIESGO)                         
									FROM COLECTIVOS_COBERTURAS COB                    
								WHERE COB.COCB_CASU_CD_SUCURSAL =  1               
									AND COB.COCB_CARP_CD_RAMO     = 61               
									AND COB.COCB_CARB_CD_RAMO     = 14               
									AND COB.COCB_CACB_CD_COBERTURA  <> '003'         
									AND COB.COCB_CAPU_CD_PRODUCTO   = CM.COCM_CAPU_CD_PRODUCTO           
									AND COB.COCB_CAPB_CD_PLAN       = CM.COCM_CAPB_CD_PLAN               
									AND COB.COCB_CER_NU_COBERTURA   = CM.COCM_NU_COBERTURA) ||
						'|' || (SELECT SUM (COB.COCB_TA_RIESGO)                                            
									FROM COLECTIVOS_COBERTURAS COB                                       
								WHERE COB.COCB_CASU_CD_SUCURSAL =  1                                  
									AND COB.COCB_CARP_CD_RAMO     = 61                                  
									AND COB.COCB_CARB_CD_RAMO     = 14                                  
									AND COB.COCB_CACB_CD_COBERTURA  = '003'                             
									AND COB.COCB_CAPU_CD_PRODUCTO   = CM.COCM_CAPU_CD_PRODUCTO           
									AND COB.COCB_CAPB_CD_PLAN       = CM.COCM_CAPB_CD_PLAN               
									AND COB.COCB_CER_NU_COBERTURA   = CM.COCM_NU_COBERTURA) ||				
						'|' ||
						'|' ||
						'|' ||
						'|' ||
						'|' LINEA       
				FROM COLECTIVOS_CERTIFICADOS_MOV CM 				                
				INNER JOIN COLECTIVOS_CLIENTE_CERTIF CCC ON(CCC.COCC_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL                                        
													AND CCC.COCC_CARP_CD_RAMO     = CM.COCM_CARP_CD_RAMO                                                             
													AND CCC.COCC_CAPO_NU_POLIZA   = CM.COCM_CAPO_NU_POLIZA                                                  
													AND CCC.COCC_NU_CERTIFICADO   = CM.COCM_NU_CERTIFICADO                                                      
													AND NVL(CCC.COCC_TP_CLIENTE,1)= 1)           
				INNER JOIN COLECTIVOS_CLIENTES CL ON(CL.COCN_NU_CLIENTE = CCC.COCC_NU_CLIENTE) 							
				INNER JOIN COLECTIVOS_CERTIFICADOS HEAD ON(HEAD.COCE_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL 				
													AND HEAD.COCE_CARP_CD_RAMO     = CM.COCM_CARP_CD_RAMO 					
													AND HEAD.COCE_CAPO_NU_POLIZA   = CM.COCM_CAPO_NU_POLIZA 				
													AND HEAD.COCE_NU_CERTIFICADO   = 0) 
				INNER JOIN ALTERNA_ESTATUS EST ON(EST.ALES_CD_ESTATUS = CM.COCM_ST_CERTIFICADO)
				LEFT JOIN COLECTIVOS_PARAMETROS PF ON(PF.COPA_DES_PARAMETRO 	= 'POLIZA'                                                                                       
													AND PF.COPA_ID_PARAMETRO	= 'GEPREFAC'                                                                                              
													AND PF.COPA_NVALOR1         = CM.COCM_CASU_CD_SUCURSAL 
													AND PF.COPA_NVALOR2         = CM.COCM_CARP_CD_RAMO
													AND PF.COPA_NVALOR3         = DECODE(PF.COPA_NVALOR4, 0, CM.COCM_CAPO_NU_POLIZA, 1, SUBSTR(CM.COCM_CAPO_NU_POLIZA,0,3), SUBSTR(CM.COCM_CAPO_NU_POLIZA,0,2)) 
													AND PF.COPA_NVALOR6         = TO_NUMBER(NVL(CM.COCM_SUB_CAMPANA,'0')))
				LEFT JOIN ALTERNA_PLANES PL	ON(PL.ALPL_CD_RAMO	= CM.COCM_CARP_CD_RAMO                                                                       
													AND PL.ALPL_CD_PRODUCTO	= CM.COCM_CAPU_CD_PRODUCTO                                                           
													AND PL.ALPL_CD_PLAN       = CM.COCM_CAPB_CD_PLAN)  				 
				LEFT JOIN CART_ESTADOS ES ON(ES.CAES_CD_ESTADO = CL.COCN_CD_ESTADO)
			WHERE CM.COCM_CASU_CD_SUCURSAL 	= d.CODX_CASU_CD_SUCURSAL
				AND CM.COCM_CARP_CD_RAMO	= d.CODX_CARP_CD_RAMO 
				AND CM.COCM_CAPO_NU_POLIZA  = d.CODX_CAPO_NU_POLIZA 
				AND CM.COCM_NU_CERTIFICADO  = d.CODX_COCE_NU_CERTIFICADO
                AND CM.COCM_NU_MOVIMIENTO   = DECODE(d.CODX_NU_TIPO_DXP, 3, e.CODX_NU_MOVIMIENTO, d.CODX_NU_MOVIMIENTO)
        ) || '|' ||
        D.CODX_MT_DIFERENCIA_PNETA || '|' ||
        D.CODX_MT_DIFERENCIA_PTOTAL || '|' ||
        CASE 
            WHEN D.CODX_NU_TIPO_DXP = 1 then 'Emision'
            WHEN D.CODX_NU_TIPO_DXP = 2 then 'Stock'
            WHEN D.CODX_NU_TIPO_DXP = 3 then 'Cancelacion'                             
            WHEN D.CODX_NU_TIPO_DXP = 4 then 'Rehabilitacion'  
            ELSE 'Renovacion' end || '|' ||
        D.CODX_MT_PRIMA_NETA || '|' ||
        D.CODX_MT_PRIMA_TOTAL || '|' ||
        COB.MCPN || '|' ||
        COB.MCPT || '|' ||
        FAC.FAPN || '|' ||
        FAC.FAPT || '|' ||
		(E.CODX_MT_NETO - NVL(COB.MCPN, 0)) || '|' ||
        (E.CODX_MT_TOTAL - NVL(COB.MCPT, 0)) || '|' ||        
        (D.CODX_MT_NETO - E.CODX_MT_NETO)  || '|' ||
        (D.CODX_MT_TOTAL - E.CODX_MT_TOTAL) || '|' ||
        (D.CODX_MT_NETO - NVL(COB.MCPN, 0)) || '|' ||
        (D.CODX_MT_TOTAL - NVL(COB.MCPT, 0)) || '|' ||    
        NULL
    FROM dxp_total d
	INNER JOIN dxp_exi e ON(d.CODX_CASU_CD_SUCURSAL = e.CODX_CASU_CD_SUCURSAL 
				AND d.CODX_CARP_CD_RAMO 		= e.CODX_CARP_CD_RAMO  
				AND d.CODX_CAPO_NU_POLIZA 		= e.CODX_CAPO_NU_POLIZA  
				AND d.CODX_COCE_NU_CERTIFICADO	= e.CODX_COCE_NU_CERTIFICADO )
	LEFT JOIN cobranza cob ON(d.CODX_CASU_CD_SUCURSAL = cob.CODX_CASU_CD_SUCURSAL 
				AND d.CODX_CARP_CD_RAMO 		= cob.CODX_CARP_CD_RAMO
				AND d.CODX_CAPO_NU_POLIZA 		= cob.CODX_CAPO_NU_POLIZA
				AND d.CODX_COCE_NU_CERTIFICADO	= cob.CODX_COCE_NU_CERTIFICADO) 
	LEFT JOIN facturacion fac ON(d.CODX_CASU_CD_SUCURSAL = fac.CODX_CASU_CD_SUCURSAL 
				AND d.CODX_CARP_CD_RAMO 		= fac.CODX_CARP_CD_RAMO
				AND d.CODX_CAPO_NU_POLIZA 		= fac.CODX_CAPO_NU_POLIZA
				AND d.CODX_COCE_NU_CERTIFICADO	= fac.CODX_COCE_NU_CERTIFICADO) 	
WHERE ((D.CODX_MT_TOTAL - NVL(COB.MCPT, 0)) > 0 OR (FAC.FAPN - NVL(COB.MCPT, 0)) > 0)
ORDER BY D.CODX_COCE_NU_CERTIFICADO