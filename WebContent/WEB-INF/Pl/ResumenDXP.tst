WITH 
dxp_total
        AS (   
            select CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO,CODX_MT_NETO, CODX_MT_TOTAL, CODX_FE_CONTABLE_REG
            FROM colectivos_dxp c
                   where 1 = 1 
            and c.CODX_CASU_CD_SUCURSAL = pjSucursal
            AND c.CODX_CARP_CD_RAMO = pjRamo
            AND c.CODX_CAPO_NU_POLIZA = pjPoliza
           -- and c.CODX_FE_INICIO_VIGENCIA =  to_date('pjFechaVigencia', 'dd/mm/yyyy')
            and c.CODX_NU_TIPO_DXP NOT IN(6,8)
            and c.CODX_FE_CONTABLE_REG <=  to_date('pjFechaBusqueda', 'dd/mm/yyyy')
            and c.CODX_NU_PK = (
                                select max (CODX_NU_PK) from colectivos_dxp cd
                                WHERE cd.CODX_CASU_CD_SUCURSAL = c.CODX_CASU_CD_SUCURSAL
                                AND cd.CODX_CARP_CD_RAMO = c.CODX_CARP_CD_RAMO
                                AND cd.CODX_CAPO_NU_POLIZA = c.CODX_CAPO_NU_POLIZA
                                and cd.CODX_COCE_NU_CERTIFICADO = c.CODX_COCE_NU_CERTIFICADO
                                and cD.CODX_FE_CONTABLE_REG <=  to_date('pjFechaBusqueda', 'dd/mm/yyyy')
                                and  cd.CODX_NU_TIPO_DXP NOT IN(6,8)
                                )
            ),
dxp_exi
        AS (
            select CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO,CODX_MT_NETO, CODX_MT_TOTAL 
            FROM colectivos_dxp c
                   where 1 = 1 
            and c.CODX_CASU_CD_SUCURSAL = pjSucursal
            AND c.CODX_CARP_CD_RAMO = pjRamo
            AND c.CODX_CAPO_NU_POLIZA = pjPoliza
           -- and c.CODX_FE_INICIO_VIGENCIA =  to_date('pjFechaVigencia', 'dd/mm/yyyy')
            and c.CODX_NU_TIPO_DXP = 8
            and c.CODX_FE_CONTABLE_REG <=  to_date('pjFechaBusqueda', 'dd/mm/yyyy')
            and c.CODX_NU_PK = (
                                select max (CODX_NU_PK) from colectivos_dxp cd
                                WHERE cd.CODX_CASU_CD_SUCURSAL = c.CODX_CASU_CD_SUCURSAL
                                AND cd.CODX_CARP_CD_RAMO = c.CODX_CARP_CD_RAMO
                                AND cd.CODX_CAPO_NU_POLIZA = c.CODX_CAPO_NU_POLIZA
                                and cd.CODX_COCE_NU_CERTIFICADO = c.CODX_COCE_NU_CERTIFICADO
                                and cD.CODX_FE_CONTABLE_REG <=  to_date('pjFechaBusqueda', 'dd/mm/yyyy')
                                and  cd.CODX_NU_TIPO_DXP = 8 
                                )
            ), 
cobranza 
            AS ( 
                SELECT c.CODX_CASU_CD_SUCURSAL, c.CODX_CARP_CD_RAMO, c.CODX_CAPO_NU_POLIZA, c.CODX_COCE_NU_CERTIFICADO, 
                (SUM(CODX_MT_AJUSTE_NETO)* -1) MCPN, (SUM(CODX_MT_AJUSTE_TOTAL)* -1) MCPT                
                FROM colectivos_dxp c 
                   where 1 = 1 
                AND c.CODX_CASU_CD_SUCURSAL = pjSucursal 
            	AND c.CODX_CARP_CD_RAMO = pjRamo
            	AND c.CODX_CAPO_NU_POLIZA = pjPoliza
            --	and c.CODX_FE_INICIO_VIGENCIA =  to_date('pjFechaVigencia', 'dd/mm/yyyy')
	            and c.CODX_FE_CONTABLE_REG <=  to_date('pjFechaBusqueda', 'dd/mm/yyyy')
                AND c.CODX_NU_TIPO_DXP = 6 
                GROUP BY c.CODX_CASU_CD_SUCURSAL, c.CODX_CARP_CD_RAMO, c.CODX_CAPO_NU_POLIZA, c.CODX_COCE_NU_CERTIFICADO 
                ), 
facturacion 
            AS ( 
                SELECT  CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO, 
                SUM(dxp.CODX_MT_AJUSTE_NETO) FAPN, 
                SUM(dxp.CODX_MT_AJUSTE_TOTAL) FAPT 
                FROM colectivos_dxp dxp 
                   where 1 = 1 
                AND dxp.CODX_CASU_CD_SUCURSAL = pjSucursal 
            	AND dxp.CODX_CARP_CD_RAMO = pjRamo
            	AND dxp.CODX_CAPO_NU_POLIZA = pjPoliza
            	--and dxp.CODX_FE_INICIO_VIGENCIA =  to_date('pjFechaVigencia', 'dd/mm/yyyy')
	            and dxp.CODX_FE_CONTABLE_REG <=  to_date('pjFechaBusqueda', 'dd/mm/yyyy')
                AND dxp.CODX_NU_TIPO_DXP = 8 
                AND dxp.CODX_MT_AJUSTE_TOTAL > 0 
                GROUP BY  CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO )
select 
    nvl(d.CODX_CAPO_NU_POLIZA, 0) IdPoliza, 
    0.0 EPNA, 
    nvl(sum(f.FAPN), 0) "FacAcuPN", 
    nvl(SUM(e.CODX_MT_NETO), 0) - nvl(sum(cob.MCPN), 0) "DxP_ExiPN", 
    nvl(SUM((d.CODX_MT_NETO - e.CODX_MT_NETO)), 0)  "DxP_NoExiPN",     
    nvl(SUM(d.CODX_MT_NETO), 0) - nvl(sum(cob.MCPN), 0) "DxP_Tot", 
    nvl(sum(cob.MCPN), 0) "MonCobPN",
    0.0 EPTA, 
    nvl(sum(f.FAPT), 0) "FacAcuPT", 
    nvl(SUM(e.CODX_MT_TOTAL), 0) - nvl(sum(cob.MCPT), 0) "DxP_ExiPT", 
    nvl(SUM((d.CODX_MT_TOTAL - e.CODX_MT_TOTAL)), 0) "DxP_NoExiPT",     
    nvl(SUM(d.CODX_MT_TOTAL), 0) - nvl(sum(cob.MCPT), 0) "DxT_PT" ,
    nvl(sum(cob.MCPT), 0) "MonCobPT"        
from dxp_total d , dxp_exi e, facturacion f, cobranza cob 
where d.CODX_CASU_CD_SUCURSAL = e.CODX_CASU_CD_SUCURSAL 
AND d.CODX_CARP_CD_RAMO = e.CODX_CARP_CD_RAMO  
AND d.CODX_CAPO_NU_POLIZA = e.CODX_CAPO_NU_POLIZA  
AND d.CODX_COCE_NU_CERTIFICADO = e.CODX_COCE_NU_CERTIFICADO 
 
and d.CODX_CASU_CD_SUCURSAL = f.CODX_CASU_CD_SUCURSAL (+) 
AND d.CODX_CARP_CD_RAMO = f.CODX_CARP_CD_RAMO (+) 
AND d.CODX_CAPO_NU_POLIZA = f.CODX_CAPO_NU_POLIZA (+) 
AND d.CODX_COCE_NU_CERTIFICADO = f.CODX_COCE_NU_CERTIFICADO (+) 
 
and d.CODX_CASU_CD_SUCURSAL = cob.CODX_CASU_CD_SUCURSAL (+) 
AND d.CODX_CARP_CD_RAMO = cob.CODX_CARP_CD_RAMO (+) 
AND d.CODX_CAPO_NU_POLIZA = cob.CODX_CAPO_NU_POLIZA (+) 
AND d.CODX_COCE_NU_CERTIFICADO = cob.CODX_COCE_NU_CERTIFICADO (+) 
GROUP BY d.CODX_CAPO_NU_POLIZA, 0.0 