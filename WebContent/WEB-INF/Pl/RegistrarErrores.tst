declare 

  
  canal   number := ?;
  ramo    number := ?;
  poliza  number := ?;
  idVenta number := ?;
  archivo varchar2(2000) := ?;
  
  contador     number;
  noErrores    number;
  consecutivo  number;
  totalErrores number;
  
  error tlmk_traspaso_aptc%rowtype;
  
  cursor errores is
  
    select CP.COPC_CD_SUCURSAL       CANAL
          ,CP.COPC_CD_RAMO           RAMO
          ,CP.COPC_NUM_POLIZA        POLIZA
          ,CP.COPC_ID_CERTIFICADO    ID
          ,CP.COPC_TIPO_REGISTRO     TIPO_REG
          ,to_date(CP.COPC_FE_INGRESO,'dd/mm/yyyy')        FECHA_INGRESO
          ,to_number(CP.COPC_SUMA_ASEGURADA)               SUMA_ASEGURADA
          ,CP.COPC_DATO6                                   SALDO_INSOLUTO
          ,'PC-'||decode(CP.COPC_NUM_POLIZA,0, 'Producto,plan o tarifa no parametrizada'
                       ,CP.COPC_REGISTRO)    ESTATUS
          ,CP.COPC_DATO11       ARCHIVO
        from colectivos_precarga CP
       where CP.COPC_CD_SUCURSAL   = canal
         and CP.COPC_CD_RAMO       = ramo
         and CP.COPC_NUM_POLIZA    = decode(idVenta,0,poliza,CP.COPC_NUM_POLIZA)
         and (CP.COPC_DATO11       = archivo or archivo = '*')
         and (idVenta = 0 or CP.COPC_DATO16 = idVenta)
               
        union all

    select CC.COCR_CD_SUCURSAL     CANAL
          ,CC.COCR_CD_RAMO         RAMO
          ,CC.COCR_NUM_POLIZA      POLIZA
          ,CC.COCR_ID_CERTIFICADO  ID
          ,CC.COCR_TIPO_REGISTRO   TIPO_REG
          ,CC.COCR_FE_INGRESO      FECHA_INGRESO
          ,CC.COCR_SUMA_ASEGURADA  SUMA_ASEGURADA
          ,CC.COCR_DATO6           SALDO_INSOLUTO
          ,'CC-'||CC.COCR_REGISTRO ESTATUS
          ,CC.COCR_DATO11          ARCHIVO
       from colectivos_carga CC
      where CC.COCR_CD_SUCURSAL   = canal
        and CC.COCR_CD_RAMO       = ramo
        and CC.COCR_NUM_POLIZA    = decode(idVenta,0,poliza,CC.COCR_NUM_POLIZA)
        and (CC.COCR_DATO11       = archivo or archivo = '*')
        and (idVenta = 0 or CC.COCR_DATO16 = idVenta)
      order by 1,4;


begin
  -- Test statements here
  contador := 0;
  noErrores  := 0;
  consecutivo := 0;
  
    
  select sum (total.cantidad)
    into totalErrores
    from (
          select count(1) cantidad
            from colectivos_precarga CP
           where CP.COPC_CD_SUCURSAL   = canal
             and CP.COPC_CD_RAMO       = ramo
             and CP.COPC_NUM_POLIZA    = decode(idVenta,0,poliza,CP.COPC_NUM_POLIZA)
             and (CP.COPC_DATO11       = archivo or archivo = '*')
             and (idVenta = 0 or CP.COPC_DATO16 = idVenta)
                       
          union all

          select count(1)
             from colectivos_carga CC
            where CC.COCR_CD_SUCURSAL   = canal
              and CC.COCR_CD_RAMO       = ramo
              and CC.COCR_NUM_POLIZA    = decode(idVenta,0,poliza,CC.COCR_NUM_POLIZA)
              and (CC.COCR_DATO11       = archivo or archivo = '*')
              and (idVenta = 0 or CC.COCR_DATO16 = idVenta)
           )total;
  
  
  begin 
    select nvl(max(this.trse_numero_poliza),0)+1
      into consecutivo
      from TLMK_TRASPASO_APTC this
     where this.trse_sucursal       = to_number(to_char(sysdate,'yyyy'))
       and this.trse_clave_producto = to_number(to_char(sysdate,'mm'))
       and this.trse_sub_campana    = idVenta;
          
  exception when others then
    consecutivo := 1;
  end;
  
  consecutivo := consecutivo + totalErrores;
  
  for e in errores loop
    begin     
    
          error.trse_Tipo_Registro    := '4';                                 -- TipoError    PK
          error.trse_Sucursal         := to_number(to_char(sysdate,'yyyy'));  -- Anio         PK
          error.trse_Clave_Producto   := to_number(to_char(sysdate,'mm'));    -- Mes          PK
          error.trse_Numero_Poliza    := consecutivo;                         -- Consecutivo  PK  
          error.Trse_Cargada          := 0;                                   -- Activado
          error.trse_Sub_Campana      := idVenta;                             -- IdVenta
          error.trse_fecha_carga      := trunc(sysdate);                      -- Fecha_Carga
          error.trse_Registro         := e.ESTATUS;                           -- Descripcion
          error.trse_Cuenta           := e.ID;                                -- ID_cert
          error.trse_Fecha_Pago       := e.FECHA_INGRESO;                     -- FechaIngreso
          error.trse_Usuario          := e.SUMA_ASEGURADA;                    -- Suma_Asegurada
          error.trse_Remesa           := e.SALDO_INSOLUTO;                    -- Saldo_Insoluto
          error.trse_tipo_producto    := e.RAMO;                              -- Ramo
          error.trse_numero_solicitud := e.poliza;                            -- Poliza
          error.trse_campana          := 0;                                   -- not null
          error.trse_nom_base         := e.CANAL||'-'||e.RAMO||'-'||e.POLIZA||'-'||
                                         e.ID||'-'||e.Suma_Asegurada||'-'||e.TIPO_REG;
          insert into tlmk_traspaso_aptc values error;
            
          commit;
              
          consecutivo := consecutivo - 1;
          contador := contador + 1;  
    
    exception when others then
      noErrores:= noErrores +1;
    end; 
  
  end loop;
  
  dbms_output.put_line('Total: '||contador);
  dbms_output.put_line('Errores: '||noErrores);
  
end;

