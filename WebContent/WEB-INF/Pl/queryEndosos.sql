SELECT T.RAMO, T.POLIZA, T.CERTIFICADO, T.NUM, MIN(T.LINEA) LINEA 
	FROM ( 
		SELECT NVL(c.coce_carp_cd_ramo, ce.coed_carp_cd_ramo)	RAMO,  
				NVL(c.coce_capo_nu_poliza, ce.coed_capo_nu_poliza) POLIZA,  
				NVL(c.coce_nu_certificado, ce.coed_nu_certificado) CERTIFICADO, 
				ce.coed_campon2 NUM, 
				NVL(c.coce_carp_cd_ramo, ce.coed_carp_cd_ramo)	|| '�' ||  
				NVL(pl.alpl_dato3, ce.coed_capj_cd_sucursal)             || '�' ||  
				NVL(c.coce_capo_nu_poliza, ce.coed_capo_nu_poliza) || '�' ||  
				NVL(c.coce_no_recibo, ce.coed_nu_recibo) || '�' ||  
				NVL(cc.cocc_id_certificado, ce.coed_id_certificado)    || '�' ||  
				NVL(c.coce_nu_certificado, ce.coed_nu_certificado)     || '�' ||  
				nvl(pl.alpl_dato2,pl.alpl_de_plan)  || '�' ||  
				est.ales_campo1              || '�' ||  
				'ALTA'                    || '�' ||  
				c.coce_nu_cuenta          || '�' ||  
				c.coce_tp_producto_bco    || '�' ||  
				c.coce_tp_subprod_bco     || '�' ||  
				c.coce_cazb_cd_sucursal   || '�' ||  
				cl.cocn_apellido_pat      || '�' ||  
				cl.cocn_apellido_mat      || '�' ||  
				cl.cocn_nombre            || '�' ||  
				cl.cocn_cd_sexo           || '�' ||  
				TO_CHAR(cl.cocn_fe_nacimiento, 'dd/MM/yyyy')     || '�' ||  
				cl.cocn_buc_cliente       || '�' ||  
				cl.cocn_rfc               || '�' ||  
				c.coce_cd_plazo           || '�' ||  
				
				TO_CHAR(NVL((select min(re.core_fe_desde)  
					from colectivos_recibos re  
				where re.core_casu_cd_sucursal            = r.core_casu_cd_sucursal  
					and re.core_carp_cd_ramo            = r.core_carp_cd_ramo  
					and re.core_capo_nu_poliza          = r.core_capo_nu_poliza  
					and re.core_cace_nu_certificado     = 0  
					and re.core_st_recibo               in (1,2,4)  
					and re.core_nu_consecutivo_cuota    = 1  
					and re.core_fe_desde                <= r.core_fe_desde  
					and add_months(re.core_fe_desde,12) > r.core_fe_desde), ce.coed_fecha_nvo), 'dd/MM/yyyy')    || '�' ||  
				
				TO_CHAR(NVL((select add_months(min(re.core_fe_desde), 12) 
					from colectivos_recibos re   
				where re.core_casu_cd_sucursal          = r.core_casu_cd_sucursal  
					and re.core_carp_cd_ramo            = r.core_carp_cd_ramo  
					and re.core_capo_nu_poliza          = r.core_capo_nu_poliza  
					and re.core_cace_nu_certificado     = 0  
					and re.core_st_recibo               in (1,2,4)                      
					and re.core_nu_consecutivo_cuota    = 1                          
					and re.core_fe_desde                <= r.core_fe_desde  
					and add_months(re.core_fe_desde,12) > r.core_fe_desde), ce.coed_fecha_ant), 'dd/MM/yyyy')    || '�' ||  
					
				TO_CHAR(c.coce_fe_suscripcion, 'dd/MM/yyyy')    || '�' ||  
				TO_CHAR(c.coce_fe_desde, 'dd/MM/yyyy')          || '�' ||  
				TO_CHAR(c.coce_fe_hasta, 'dd/MM/yyyy')          || '�' ||  
				TO_CHAR(c.coce_fe_ini_credito, 'dd/MM/yyyy')    || '�' ||  
				TO_CHAR(c.coce_fe_fin_credito, 'dd/MM/yyyy')    || '�' ||  
				c.coce_campov6           || '�' ||  
				TO_CHAR(c.coce_fe_anulacion, 'dd/MM/yyyy')      || '�' ||  
				TO_CHAR(c.coce_fe_anulacion_col, 'dd/MM/yyyy')  || '�' ||  
				trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) || '�' ||    
				nvl(c.coce_mt_bco_devolucion,0) || '�' ||  
				nvl(c.coce_mt_devolucion,0)     || '�' ||  
				decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) || '�' ||  
				case   
					when pf.copa_nvalor5 = 0 then  
						nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)   
					when pf.copa_nvalor5 = 1 then  
						c.coce_mt_prima_pura  
					else 0 end || '�' ||  
				to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) || '�' ||  
				to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) || '�' ||  
				to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) || '�' ||  
				case   
					when pf.copa_nvalor5 = 0 then  
						nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)  
						   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)  
						   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)  
						   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)  
					when pf.copa_nvalor5 = 1 then  
						nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)  
						   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)  
						   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)  
						   + nvl(c.coce_mt_prima_pura,0)  
					else 0 end || '�' ||  
				round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) || '�' ||  
				round(case  
					when c.coce_carp_cd_ramo in (61,65) then  
						(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *  
							(select sum (cob.cocb_ta_riesgo) tasaVida  
								from colectivos_coberturas cob  
							where cob.cocb_casu_cd_sucursal =  1  
								and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo  
								and cob.cocb_carb_cd_ramo     = 14  
								and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza  
								and cob.cocb_cacb_cd_cobertura  <> '003'  
								and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto  
								and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan  
								and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)  
					end,2) || '�' ||  
				round(case  
					when c.coce_carp_cd_ramo = 61 then  
						(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *  
							(select sum (cob.cocb_ta_riesgo) tasaDes  
								from colectivos_coberturas cob  
							where cob.cocb_casu_cd_sucursal =  1  
								and cob.cocb_carp_cd_ramo     = 61  
								and cob.cocb_carb_cd_ramo     = 14  
								and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza  
								and cob.cocb_cacb_cd_cobertura  = '003'  
								and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto  
								and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan  
								and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)  
					end,2) || '�' ||  
				cl.cocn_cd_estado    || '�' ||  
				es.caes_de_estado   || '�' ||  
				cl.cocn_cd_postal   || '�' ||  
				c.coce_capu_cd_producto    || '�' ||  
				c.coce_capb_cd_plan        || '�' ||  
				(select sum (cob.cocb_ta_riesgo)  
					from colectivos_coberturas cob  
				where cob.cocb_casu_cd_sucursal     =  1  
					and cob.cocb_carp_cd_ramo         = 61  
					and cob.cocb_carb_cd_ramo         = 14  
					and cob.cocb_cacb_cd_cobertura  <> '003'  
					and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto  
					and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan  
					and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) || '�' ||  
				(select sum (cob.cocb_ta_riesgo)  
					from colectivos_coberturas cob  
				where cob.cocb_casu_cd_sucursal     =  1  
					and cob.cocb_carp_cd_ramo         = 61  
					and cob.cocb_carb_cd_ramo         = 14  
					and cob.cocb_cacb_cd_cobertura  = '003'  
					and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto  
					and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan  
					and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) || '�' ||  
				abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) || '�' ||  
				clo.cocn_apellido_pat || '/' ||  
				clo.cocn_apellido_mat || '/' ||  
				clo.cocn_nombre        || '�' ||  
				clo.cocn_rfc        || '�' ||  
				TO_CHAR(clo.cocn_fe_nacimiento, 'dd/MM/yyyy') || '�' ||  
				clo.cocn_cd_sexo    || '�' ||  
				cco.cocc_tp_cliente    || '�' ||  
				ce.coed_campon3 || '�' ||  
				ce.coed_nombre_ant || '�' ||  
				ce.coed_nombre_nvo || '�' ||  
				ce.coed_nu_recibo || '�' ||  
				CE.coed_nu_solicitud || '�' ||  
				TO_CHAR(ce.coed_fecha_inicio, 'dd/MM/yyyy') || '�' ||  
				TO_CHAR(ce.coed_fecha_fin, 'dd/MM/yyyy')  || '�' ||  				
				TO_CHAR(ce.coed_fe_aplica, 'dd/MM/yyyy')  || '�' ||  
				
				NVL(c.coce_buc_empresa,0)  || '�' ||  																
				NVL((select cln.cocn_fe_nacimiento   ||'/'||                                    
						cln.cocn_cd_sexo         ||'/'||                                    
						clc.cocc_tp_cliente      ||'/'||                                    
						p.copa_vvalor1 
					from colectivos_cliente_certif clc                                                      
						,colectivos_clientes       cln 
						,colectivos_parametros     p 
				where clc.cocc_casu_cd_sucursal   = c.coce_casu_cd_sucursal 
					and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 
					and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza 
					and clc.cocc_nu_certificado   = c.coce_nu_certificado 
					and nvl(clc.cocc_tp_cliente,1) > 1 
					and cln.cocn_nu_cliente       = clc.cocc_nu_cliente 
					and p.copa_des_parametro(+)   = 'ASEGURADO' 
					and p.copa_id_parametro(+)    = 'TIPO' 				                       				 								
					and p.copa_nvalor1(+)         = clc.cocc_tp_cliente                      				 				
					and ROWNUM = 1 ), ' ') || '�' || 
					
				DECODE(INSTR(c.coce_empresa,'|OBLIGADO CON INGRESOS: '), 0, ' ', SUBSTR(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)) linea 											
				 
			from colectivos_endosos ce  
			inner join colectivos_certificados c on(c.coce_casu_cd_sucursal = ce.coed_casu_cd_sucursal  
					and c.coce_carp_cd_ramo = ce.coed_carp_cd_ramo  
					and c.coce_capo_nu_poliza = ce.coed_capo_nu_poliza  
					and c.coce_nu_certificado = ce.coed_nu_certificado  
					and c.coce_nu_movimiento = ce.coed_campon2)  				 
			inner join colectivos_cliente_certif cc ON(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal and cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo and cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza and cc.cocc_nu_certificado = c.coce_nu_certificado and nvl(cc.cocc_tp_cliente,1) = 1)  
			inner join colectivos_clientes cl ON(cl.cocn_nu_cliente = cc.cocc_nu_cliente)  
			left join colectivos_cliente_certif cco ON(cco.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal and cco.cocc_carp_cd_ramo = c.coce_carp_cd_ramo and cco.cocc_capo_nu_poliza = c.coce_capo_nu_poliza and cco.cocc_nu_certificado = c.coce_nu_certificado and cco.cocc_tp_cliente = 2)  
			left join colectivos_clientes clo ON(clo.cocn_nu_cliente = cco.cocc_nu_cliente)  
			inner join colectivos_recibos r ON(r.core_casu_cd_sucursal = c.coce_casu_cd_sucursal and r.core_nu_recibo = c.coce_no_recibo)  
			inner join alterna_planes pl ON(pl.alpl_cd_ramo = c.coce_carp_cd_ramo and pl.alpl_cd_producto = c.coce_capu_cd_producto and pl.alpl_cd_plan = c.coce_capb_cd_plan)  
			inner join colectivos_parametros pf ON(pf.copa_des_parametro = 'POLIZA'  
					and pf.copa_id_parametro = 'GEPREFAC'  
					and pf.copa_nvalor1		= c.coce_casu_cd_sucursal  
					and pf.copa_nvalor2     = c.coce_carp_cd_ramo  
					and pf.copa_nvalor3     = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)))  
			left join cart_estados es ON(es.caes_cd_estado = cl.cocn_cd_estado)  
			inner join alterna_estatus est ON(est.ales_cd_estatus = c.coce_st_certificado)  					
		where ce.coed_casu_cd_sucursal 	= @canal  
			and ce.coed_carp_cd_ramo 	= @ramo  
			and ce.coed_capo_nu_poliza between @poliza1 and @poliza2 			
			and ce.coed_nu_certificado 	> 0  
			and ce.coed_cd_endoso IN(500, 600)  
			
			@filtro			  
				
		union all      
			
		select NVL(cm.cocm_carp_cd_ramo, ce.coed_carp_cd_ramo)	RAMO,  
				NVL(cm.cocm_capo_nu_poliza, ce.coed_capo_nu_poliza) POLIZA,  
				NVL(cm.cocm_nu_certificado, ce.coed_nu_certificado) CERTIFICADO,  
				ce.coed_campon2 NUM,  
				NVL(cm.cocm_carp_cd_ramo, ce.coed_carp_cd_ramo)        || '�' ||  
				NVL(pl.alpl_dato3, ce.coed_capj_cd_sucursal)            || '�' ||  
				NVL(cm.cocm_capo_nu_poliza, ce.coed_capo_nu_poliza)    || '�' ||  
				NVL(cm.cocm_nu_recibo, ce.coed_nu_recibo) || '�' ||  
				NVL(cc.cocc_id_certificado, ce.coed_id_certificado)   || '�' ||  
				NVL(cm.cocm_nu_certificado, ce.coed_nu_certificado)    || '�' ||  
				nvl(pl.alpl_dato2,pl.alpl_de_plan) || '�' ||  
				est.ales_campo1             || '�' ||  
				'ALTA'                   || '�' ||  
				cm.cocm_nu_cuenta         || '�' ||  
				cm.cocm_tp_producto_bco   || '�' ||  
				cm.cocm_tp_subprod_bco    || '�' ||  
				cm.cocm_cazb_cd_sucursal  || '�' ||  
				cl.cocn_apellido_pat     || '�' ||  
				cl.cocn_apellido_mat     || '�' ||  
				cl.cocn_nombre           || '�' ||  
				cl.cocn_cd_sexo          || '�' ||  
				TO_CHAR(cl.cocn_fe_nacimiento, 'dd/MM/yyyy')    || '�' ||  
				cl.cocn_buc_cliente      || '�' ||  
				cl.cocn_rfc              || '�' ||  
				cm.cocm_cd_plazo          || '�' ||  
				
				TO_CHAR(nvl((select min(re.core_fe_desde)                                          
					from colectivos_recibos re                                                  
				where re.core_casu_cd_sucursal            = r.core_casu_cd_sucursal  
					and re.core_carp_cd_ramo            = r.core_carp_cd_ramo 
					and re.core_capo_nu_poliza          = r.core_capo_nu_poliza  
					and re.core_cace_nu_certificado     = 0  
					and re.core_st_recibo               in (1,2,4)  
					and re.core_nu_consecutivo_cuota    = 1  
					and re.core_fe_desde                <= r.core_fe_desde  
					and add_months(re.core_fe_desde,12) > r.core_fe_desde), ce.coed_fecha_nvo), 'dd/MM/yyyy')    || '�' ||  
					
				TO_CHAR(nvl((select add_months(min(re.core_fe_desde), 12)  
					from colectivos_recibos re  
				where re.core_casu_cd_sucursal          = r.core_casu_cd_sucursal  
					and re.core_carp_cd_ramo            = r.core_carp_cd_ramo  
					and re.core_capo_nu_poliza          = r.core_capo_nu_poliza  
					and re.core_cace_nu_certificado     = 0  
					and re.core_st_recibo               in (1,2,4)  
					and re.core_nu_consecutivo_cuota    = 1  
					and re.core_fe_desde                <= r.core_fe_desde  
					and add_months(re.core_fe_desde,12) > r.core_fe_desde), ce.coed_fecha_ant), 'dd/MM/yyyy')    || '�' ||  
					
				TO_CHAR(cm.cocm_fe_suscripcion, 'dd/MM/yyyy')   || '�' ||  
				TO_CHAR(cm.cocm_fe_desde, 'dd/MM/yyyy')          || '�' ||  
				TO_CHAR(cm.cocm_fe_hasta, 'dd/MM/yyyy')          || '�' ||  
				TO_CHAR(cm.cocm_fe_ini_credito, 'dd/MM/yyyy')    || '�' ||  
				TO_CHAR(cm.cocm_fe_fin_credito, 'dd/MM/yyyy')    || '�' ||  
				cm.cocm_campov6           || '�' ||  
				TO_CHAR(cm.cocm_fe_anulacion_real, 'dd/MM/yyyy')      || '�' ||  
				TO_CHAR(cm.cocm_fe_anulacion, 'dd/MM/yyyy')  || '�' ||  
				trim(to_char(decode(cm.cocm_sub_campana,'7',to_number(cm.cocm_mt_suma_aseg_si),cm.cocm_mt_suma_asegurada),'999999999.99')) || '�' ||  
				nvl(cm.cocm_mt_bco_devolucion,0) || '�' ||  
				nvl(cm.cocm_mt_devolucion,0)     || '�' ||  
				decode(cm.cocm_sub_campana,'7',cm.cocm_mt_suma_asegurada,cm.cocm_mt_suma_aseg_si) || '�' ||  
				case   
					when pf.copa_nvalor5 = 0 then  
						nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)  
					when pf.copa_nvalor5 = 1 then  
						cm.cocm_mt_prima_pura  
					else 0 end || '�' ||  
				to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'O',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,1) -instr(cm.cocm_di_cobro1, 'O',1,1)-1)) || '�' ||  
				to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'I',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,2) -instr(cm.cocm_di_cobro1, 'I',1,1)-1)) || '�' ||  
				to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'A',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,3)-instr(cm.cocm_di_cobro1, 'A',1,1) -1)) || '�' ||  
				case  
					when pf.copa_nvalor5 = 0 then  
						nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'O',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,1)-instr(cm.cocm_di_cobro1, 'O',1,1) -1)),0)  
						   + nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'I',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,2)-instr(cm.cocm_di_cobro1, 'I',1,1) -1)),0)  
						   + nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'A',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,3)-instr(cm.cocm_di_cobro1, 'A',1,1) -1)),0)  
						   + nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)  
					when pf.copa_nvalor5 = 1 then  
						nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'O',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,1)-instr(cm.cocm_di_cobro1, 'O',1,1) -1)),0)  
						   + nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'I',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,2)-instr(cm.cocm_di_cobro1, 'I',1,1) -1)),0)  
						   + nvl(to_number(substr(cm.cocm_di_cobro1,instr(cm.cocm_di_cobro1, 'A',1,1) + 1,instr(cm.cocm_di_cobro1, '|',1,3)-instr(cm.cocm_di_cobro1, 'A',1,1) -1)),0)  
						   + nvl(cm.cocm_mt_prima_pura,0)  
					else 0 end || '�' ||  
				round(cm.cocm_mt_prima_pura*1000/decode(cm.cocm_sub_campana,'7',decode(cm.cocm_mt_suma_asegurada,0,1,cm.cocm_mt_suma_asegurada),decode(cm.cocm_mt_suma_aseg_si,0,1,cm.cocm_mt_suma_aseg_si)),4) || '�' ||  
				round(case  
					when cm.cocm_carp_cd_ramo in (61,65) then  
						(decode(cm.cocm_sub_campana,'7',cm.cocm_mt_suma_asegurada,cm.cocm_mt_suma_aseg_si)/1000) *  
							(select sum (cob.cocb_ta_riesgo) tasaVida  
								from colectivos_coberturas cob  
							where cob.cocb_casu_cd_sucursal =  1  
								and cob.cocb_carp_cd_ramo     = cm.cocm_carp_cd_ramo  
								and cob.cocb_carb_cd_ramo     = 14  
								and cob.cocb_capo_nu_poliza     = cm.cocm_capo_nu_poliza  
								and cob.cocb_cacb_cd_cobertura  <> '003'  
								and cob.cocb_capu_cd_producto   = cm.cocm_capu_cd_producto  
								and cob.cocb_capb_cd_plan       = cm.cocm_capb_cd_plan  
								and cob.cocb_cer_nu_cobertura   = cm.cocm_nu_cobertura)  
					end,2) || '�' ||  
				round(case  
					when cm.cocm_carp_cd_ramo = 61 then  
						(decode(cm.cocm_sub_campana,'7',cm.cocm_mt_suma_asegurada,cm.cocm_mt_suma_aseg_si)/1000) *  
							(select sum (cob.cocb_ta_riesgo) tasaDes  
								from colectivos_coberturas cob  
							where cob.cocb_casu_cd_sucursal =  1 
								and cob.cocb_carp_cd_ramo     = 61  
								and cob.cocb_carb_cd_ramo     = 14  
								and cob.cocb_capo_nu_poliza     = cm.cocm_capo_nu_poliza  
								and cob.cocb_cacb_cd_cobertura  = '003'  
								and cob.cocb_capu_cd_producto   = cm.cocm_capu_cd_producto  
								and cob.cocb_capb_cd_plan       = cm.cocm_capb_cd_plan  
								and cob.cocb_cer_nu_cobertura   = cm.cocm_nu_cobertura)  
					end,2) || '�' ||  
				cl.cocn_cd_estado    || '�' ||  
				es.caes_de_estado   || '�' ||  
				cl.cocn_cd_postal   || '�' ||  
				cm.cocm_capu_cd_producto    || '�' ||  
				cm.cocm_capb_cd_plan        || '�' ||  
				(select sum (cob.cocb_ta_riesgo)  
					from colectivos_coberturas cob  
				where cob.cocb_casu_cd_sucursal     =  1  
					and cob.cocb_carp_cd_ramo         = 61  
					and cob.cocb_carb_cd_ramo         = 14  
					and cob.cocb_cacb_cd_cobertura  <> '003'  
					and cob.cocb_capu_cd_producto   = cm.cocm_capu_cd_producto  
					and cob.cocb_capb_cd_plan       = cm.cocm_capb_cd_plan  
					and cob.cocb_cer_nu_cobertura   = cm.cocm_nu_cobertura) || '�' ||  
				(select sum (cob.cocb_ta_riesgo)  
					from colectivos_coberturas cob  
				where cob.cocb_casu_cd_sucursal     =  1  
					and cob.cocb_carp_cd_ramo         = 61  
					and cob.cocb_carb_cd_ramo         = 14  
					and cob.cocb_cacb_cd_cobertura  = '003'  
					and cob.cocb_capu_cd_producto   = cm.cocm_capu_cd_producto  
					and cob.cocb_capb_cd_plan       = cm.cocm_capb_cd_plan  
					and cob.cocb_cer_nu_cobertura   = cm.cocm_nu_cobertura) || '�' ||  
				abs( nvl(cm.cocm_mt_devolucion,0) - nvl(cm.cocm_mt_bco_devolucion,0) ) || '�' ||  
				clo.cocn_apellido_pat || '/' ||  
				clo.cocn_apellido_mat || '/' ||  
				clo.cocn_nombre        || '�' ||  
				clo.cocn_rfc        || '�' ||  
				TO_CHAR(clo.cocn_fe_nacimiento, 'dd/MM/yyyy') || '�' ||  
				clo.cocn_cd_sexo    || '�' ||  
				cco.cocc_tp_cliente    || '�' ||  
				ce.coed_campon3 || '�' ||  
				ce.coed_nombre_ant || '�' ||  
				ce.coed_nombre_nvo || '�' ||  
				ce.coed_nu_recibo || '�' ||  
				CE.coed_nu_solicitud || '�' ||  
				TO_CHAR(ce.coed_fecha_inicio, 'dd/MM/yyyy') || '�' ||  
				TO_CHAR(ce.coed_fecha_fin, 'dd/MM/yyyy') || '�' ||  
				TO_CHAR(ce.coed_fe_aplica, 'dd/MM/yyyy') || '�' ||  
				
				NVL(cm.cocm_buc_empresa,0)  || '�' ||  		
				
				NVL((select cln.cocn_fe_nacimiento   ||'/'||                                    
						cln.cocn_cd_sexo         ||'/'||                                    
						clc.cocc_tp_cliente      ||'/'||                                    
						p.copa_vvalor1 
					from colectivos_cliente_certif clc                                                      
						,colectivos_clientes       cln 
						,colectivos_parametros     p 
				where clc.cocc_casu_cd_sucursal   = cm.cocm_casu_cd_sucursal 
					and clc.cocc_carp_cd_ramo     = cm.cocm_carp_cd_ramo 
					and clc.cocc_capo_nu_poliza   = cm.cocm_capo_nu_poliza 
					and clc.cocc_capo_nu_poliza   = cm.cocm_capo_nu_poliza 
					and clc.cocc_nu_certificado   = cm.cocm_nu_certificado 
					and nvl(clc.cocc_tp_cliente,1) > 1 
					and cln.cocn_nu_cliente       = clc.cocc_nu_cliente 
					and p.copa_des_parametro(+)   = 'ASEGURADO' 
					and p.copa_id_parametro(+)    = 'TIPO' 
					and p.copa_nvalor1(+)         = clc.cocc_tp_cliente                      				 				
					and ROWNUM = 1 ), ' ') || '�' || 
					
				DECODE(INSTR(cm.cocm_empresa,'|OBLIGADO CON INGRESOS: '), 0, ' ', SUBSTR(cm.cocm_empresa,instr(cm.cocm_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)) linea 
												
			from colectivos_endosos ce  
			left join colectivos_certificados_mov cm on(cm.cocm_casu_cd_sucursal = ce.coed_casu_cd_sucursal  
					and cm.cocm_carp_cd_ramo = ce.coed_carp_cd_ramo  
					and cm.cocm_capo_nu_poliza = ce.coed_capo_nu_poliza  
					and cm.cocm_nu_certificado = ce.coed_nu_certificado  
					and cm.cocm_nu_movimiento = ce.coed_campon2)  											
			left join colectivos_cliente_certif cc ON(cc.cocc_casu_cd_sucursal = cm.cocm_casu_cd_sucursal and cc.cocc_carp_cd_ramo = cm.cocm_carp_cd_ramo and cc.cocc_capo_nu_poliza = cm.cocm_capo_nu_poliza and cc.cocc_nu_certificado = cm.cocm_nu_certificado and nvl(cc.cocc_tp_cliente,1) = 1)  
			left join colectivos_clientes cl ON(cl.cocn_nu_cliente = cc.cocc_nu_cliente)  
			left join colectivos_cliente_certif cco ON(cco.cocc_casu_cd_sucursal = cm.cocm_casu_cd_sucursal and cco.cocc_carp_cd_ramo = cm.cocm_carp_cd_ramo and cco.cocc_capo_nu_poliza = cm.cocm_capo_nu_poliza and cco.cocc_nu_certificado = cm.cocm_nu_certificado and cco.cocc_tp_cliente = 2)  
			left join colectivos_clientes clo ON(clo.cocn_nu_cliente = cco.cocc_nu_cliente)  
			left join colectivos_recibos r ON(r.core_casu_cd_sucursal = cm.cocm_casu_cd_sucursal and r.core_nu_recibo = cm.cocm_nu_recibo)  
			left join alterna_planes pl ON(pl.alpl_cd_ramo = cm.cocm_carp_cd_ramo and pl.alpl_cd_producto = cm.cocm_capu_cd_producto and pl.alpl_cd_plan = cm.cocm_capb_cd_plan)  
			left join colectivos_parametros pf ON(pf.copa_des_parametro = 'POLIZA'  
					and pf.copa_id_parametro = 'GEPREFAC'  
					and pf.copa_nvalor1		= cm.cocm_casu_cd_sucursal  
					and pf.copa_nvalor2     = cm.cocm_carp_cd_ramo  
					and pf.copa_nvalor3     = decode(pf.copa_nvalor4, 0, cm.cocm_capo_nu_poliza, 1, substr(cm.cocm_capo_nu_poliza,0,3), substr(cm.cocm_capo_nu_poliza,0,2)) )  
			left join cart_estados es ON(es.caes_cd_estado = cl.cocn_cd_estado)  
			left join alterna_estatus est ON(est.ales_cd_estatus = cm.cocm_st_certificado)  
			
		where ce.coed_casu_cd_sucursal 	= @canal 
			and ce.coed_carp_cd_ramo 	= @ramo   
			and ce.coed_capo_nu_poliza between @poliza1 and @poliza2  										
			and ce.coed_nu_certificado 	> 0  
			and ce.coed_cd_endoso IN(500, 600)  
			
			@filtro
			
	) T  
			
GROUP BY T.RAMO, T.POLIZA, T.CERTIFICADO, T.NUM  
ORDER BY 1,2, 3, 4  