declare 

  paramCanal  number := ?;
  paramRamo   number := ?;
  paramPoliza number := ?;
  
  existeRest  number;
  
  cursor curPrecarga is
      select *
        from colectivos_precarga pre
       where pre.copc_cd_sucursal = paramCanal
         and pre.copc_cd_ramo     = paramRamo
         and pre.copc_num_poliza  = paramPoliza
         and pre.copc_cargada     = 0
         and pre.copc_dato9       = '8'; 

begin

  existeRest := 0;
  
  for p in curPrecarga loop
  
      select count(1)
        into existeRest
        from colectivos_cliente_certif clc
            ,colectivos_certificados   c
       where clc.cocc_casu_cd_sucursal = paramCanal
         and clc.cocc_carp_cd_ramo     = paramRamo
         and clc.cocc_id_certificado   = p.copc_id_certificado
         --
         and c.coce_casu_cd_sucursal   = clc.cocc_casu_cd_sucursal
         and c.coce_carp_cd_ramo       = clc.cocc_carp_cd_ramo
         and c.coce_capo_nu_poliza     = clc.cocc_capo_nu_poliza
         and c.coce_nu_certificado     = clc.cocc_nu_certificado
         and c.coce_tp_cuenta          = 7;
         
         
         
      if existeRest > 0 then
        update colectivos_precarga pr
           set pr.copc_cargada         = '3'
              ,pr.copc_registro        = 'El credito ya cuenta con una restitucion, y se intenta emitir una multidisposicion'     
         where pr.copc_cd_sucursal     = p.copc_cd_sucursal
           and pr.copc_cd_ramo         = p.copc_cd_ramo
           and pr.copc_num_poliza      = p.copc_num_poliza
           and pr.copc_id_certificado  = p.copc_id_certificado
           and pr.copc_tipo_registro   = p.copc_tipo_registro
	         and pr.copc_cargada         = p.copc_cargada;
           
      end if;
    
  end loop;
   
  
end;