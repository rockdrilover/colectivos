WITH  
    dxp_total AS ( 
            select CODX_NU_CUOTA_POLIZA, 
                    CODX_FE_INICIO_VIGENCIA, 
                    sum(CODX_MT_NETO) CODX_MT_NETO, 
                    sum(CODX_MT_TOTAL) CODX_MT_TOTAL 
                from colectivos_dxp c 
            where 1 = 1 
                and c.CODX_CASU_CD_SUCURSAL = pjSucursal 
                and  c.CODX_CARP_CD_RAMO = pjRamo
                and  c.CODX_CAPO_NU_POLIZA = pjPoliza
               -- AND c.codx_fe_inicio_vigencia = to_date('pjFechaVigencia', 'dd/mm/yyyy') 
                AND c.CODX_FE_CONTABLE_REG <= to_date('pjFechaBusqueda', 'dd/mm/yyyy') 
                and CODX_NU_TIPO_DXP NOT IN(8,6) 
                and CODX_NU_PK = ( 
                                select max(CODX_NU_PK) from colectivos_dxp cd 
                                where cd.CODX_CASU_CD_SUCURSAL       = c.CODX_CASU_CD_SUCURSAL 
                                    and cd.CODX_CARP_CD_RAMO         = c.CODX_CARP_CD_RAMO 
                                    and cd.CODX_CAPO_NU_POLIZA       = c.CODX_CAPO_NU_POLIZA 
                                    and cd.CODX_COCE_NU_CERTIFICADO  = c.CODX_COCE_NU_CERTIFICADO                                     
                                    and cd.CODX_NU_CUOTA_POLIZA      = c.CODX_NU_CUOTA_POLIZA 
									AND cd.CODX_FE_CONTABLE_REG     <= to_date('pjFechaBusqueda', 'dd/mm/yyyy')                 
                                    and cd.CODX_NU_TIPO_DXP          NOT IN(8,6) 
                                 ) 
            group by CODX_NU_CUOTA_POLIZA, CODX_FE_INICIO_VIGENCIA ), 
    dxp_exi AS ( 
            select CODX_NU_CUOTA_POLIZA, 
                    CODX_FE_INICIO_VIGENCIA, 
                    sum(CODX_MT_NETO) CODX_MT_NETO, 
                    sum(CODX_MT_TOTAL) CODX_MT_TOTAL 
                from colectivos_dxp c 
            where 1 = 1 
                and c.CODX_CASU_CD_SUCURSAL = pjSucursal 
                and c.CODX_CARP_CD_RAMO = pjRamo 
                and c.CODX_CAPO_NU_POLIZA = pjPoliza
                --AND c.codx_fe_inicio_vigencia = to_date('pjFechaVigencia', 'dd/mm/yyyy') 
                AND c.CODX_FE_CONTABLE_REG <= to_date('pjFechaBusqueda', 'dd/mm/yyyy') 
                and c.CODX_NU_TIPO_DXP = 8 
                and c.CODX_NU_PK = ( 
                                select max(CODX_NU_PK) from colectivos_dxp cd 
                                where cd.CODX_CASU_CD_SUCURSAL      = c.CODX_CASU_CD_SUCURSAL 
                                    and cd.CODX_CARP_CD_RAMO        = c.CODX_CARP_CD_RAMO 
                                    and cd.CODX_CAPO_NU_POLIZA      = c.CODX_CAPO_NU_POLIZA 
                                    and cd.CODX_COCE_NU_CERTIFICADO = c.CODX_COCE_NU_CERTIFICADO 
                                    and cd.CODX_NU_CUOTA_POLIZA     = c.CODX_NU_CUOTA_POLIZA 
									AND cd.CODX_FE_CONTABLE_REG     <= to_date('pjFechaBusqueda', 'dd/mm/yyyy') 
                                    and cd.CODX_NU_TIPO_DXP         = 8 
                                  ) 
            group by CODX_NU_CUOTA_POLIZA, CODX_FE_INICIO_VIGENCIA ), 
	endosos AS ( 
			select CODX_NU_CUOTA_POLIZA, 
                    sum(case when CODX_NU_TIPO_DXP=1 then CODX_MT_AJUSTE_NETO else 0 end )   emitido , 
                    sum(case when CODX_NU_TIPO_DXP=1 then 1 else 0 end )   canemitido ,
                    sum(case when CODX_NU_TIPO_DXP=2 then CODX_MT_AJUSTE_NETO else 0 end )   stock, 
                    sum(case when CODX_NU_TIPO_DXP=2 then 1 else 0 end )   canstock ,
                    sum(case when CODX_NU_TIPO_DXP=3 then CODX_MT_AJUSTE_NETO else 0  end )  cancelacion, 
                    sum(case when CODX_NU_TIPO_DXP=3 then 1 else 0 end )   cancance ,
                    sum(case when CODX_NU_TIPO_DXP=6 then CODX_MT_AJUSTE_NETO * -1 else 0  end )  cobranza, 
                    sum(case when CODX_NU_TIPO_DXP=6 then 1 else 0 end )   cancob ,
                    sum(case when CODX_NU_TIPO_DXP=1 then CODX_MT_AJUSTE_TOTAL else 0 end )   emitido_total , 
                    sum(case when CODX_NU_TIPO_DXP=2 then CODX_MT_AJUSTE_TOTAL else 0 end )   stock_total, 
                    sum(case when CODX_NU_TIPO_DXP=3 then CODX_MT_AJUSTE_TOTAL else 0  end )  cancelacion_total, 
                    sum(case when CODX_NU_TIPO_DXP=6 then CODX_MT_AJUSTE_TOTAL * -1 else 0  end ) cobranza_total, 
                    sum(case when CODX_NU_TIPO_DXP=8 and CODX_MT_AJUSTE_TOTAL >0 then CODX_MT_AJUSTE_NETO else 0  end )  facturado 
                from colectivos_dxp c 
            where 1 = 1 
				and c.CODX_CASU_CD_SUCURSAL = pjSucursal
				and  c.CODX_CARP_CD_RAMO = pjRamo 
				and  c.CODX_CAPO_NU_POLIZA = pjPoliza
				--AND c.codx_fe_inicio_vigencia = to_date('pjFechaVigencia', 'dd/mm/yyyy') 
				AND c.CODX_FE_CONTABLE_REG <= to_date('pjFechaBusqueda', 'dd/mm/yyyy') 
            group by  CODX_NU_CUOTA_POLIZA ) 
select 
		TRIM(to_char( ADD_MONTHS(t.CODX_FE_INICIO_VIGENCIA,t.CODX_NU_CUOTA_POLIZA -1 ), 'Month'))|| '-' || extract (year from ADD_MONTHS(t.CODX_FE_INICIO_VIGENCIA,t.CODX_NU_CUOTA_POLIZA-1 )) "Mes", 
		t.CODX_NU_CUOTA_POLIZA "Cuota", 
		en.FACTURADO  "FacMesPN", 
		en.COBRANZA "MonCobPN", 
		en.EMITIDO "EndVenNue", 
		en.STOCK "EndDisSto", 
		en.CANCELACION "EndCanPN", 
		(en.EMITIDO + en.STOCK + en.CANCELACION) "EndPmaAnu", 
		(en.EMITIDO_TOTAL + en.STOCK_TOTAL + en.CANCELACION_TOTAL) "EmiPma_TOTAL", 
		en.COBRANZA_TOTAL MCPT, 
		nvl(e.CODX_MT_TOTAL,0) - en.COBRANZA_TOTAL  "DxP_ExiMes", 
		nvl(t.CODX_MT_TOTAL,0) - nvl(e.CODX_MT_TOTAL,0)  "DxP_NoExiMes", 
		nvl(t.CODX_MT_TOTAL, 0) - en.COBRANZA_TOTAL "Total_DxP_Mes" 
	from dxp_exi e, dxp_total t, endosos en 
where t.CODX_NU_CUOTA_POLIZA = e.CODX_NU_CUOTA_POLIZA (+) 
	AND t.CODX_NU_CUOTA_POLIZA = en.CODX_NU_CUOTA_POLIZA 
ORDER BY 2