declare 
  -- Local variables here
  i integer;  
  
  error varchar2(20000);
  
  registroVida colectivos_precarga%rowtype;
  registroGap  colectivos_precarga%rowtype;
  
   type t_poliza_auto is record
  (
     canal        cart_certificados.cace_casu_cd_sucursal%type,
     ramo         cart_certificados.cace_carp_cd_ramo%type,
     poliza       cart_certificados.cace_capo_nu_poliza%type,
     estatus      cart_certificados.cace_st_certificado%type,
     fechaAnula   cart_certificados.cace_fe_anulacion%type,
     causaAnula   cart_causas_anulacion_recibos.cana_de_causa_anulacion%type,
     credito      varchar2(30),
     prima_anual  cart_certificados.cace_mt_prima_anual%type,
     serie        auth_inspecciones_riesgo.auir_auve_nu_serial_carroceria%type,
     modelo       varchar2(500),
     placas       auth_inspecciones_riesgo.auir_de_accesorios%type,
     motor        auth_inspecciones_riesgo.auir_de_faros%type,
     marca        varchar2(500),
     tipo_veh     auth_inspecciones_riesgo.auir_de_tapiceria%type,
     descripcion  auth_inspecciones_riesgo.auir_de_modelo_radio%type
  );
  
  infoAuto t_poliza_auto;
  
  -- Primas y Sumas
  primaVida number(10,4);
  primaGap  number(10,4);
  
  capital     number(15,4);
  comisionAp  number(15,4);
  sumaVida    number(15,4);
  sumaGap     number(15,4);
  
  tarifaVida number(8,5);
  tarifaGap  number(8,5);
  
  cursor carga is 
    select *
      from vta_precarga  v
     where v.vtap_cd_sucursal    = 1
       and v.vtap_cd_ramo        = 95
       and v.vtap_cargada        = '1'
       and v.vtap_sistema_origen = 'GAP';
         
begin
  
  i :=0;
  
  for v in carga loop
    
   error := null;
     
   begin 
    
     infoAuto     := null;
     registroVida := null;
     registroGap  := null;
     
     primaVida    := null;
     primaGap     := null;
  
     sumaVida     := null;
     sumaGap      := null;
     comisionAp   := null;
     capital	  := null;
  
     tarifaVida   := null;
     tarifaGap    := null;
           
      begin
         select --POLIZA AUTO--
               pc.aupc_cace_casu_cd_sucursal                      CANAL
              ,pc.aupc_cace_carp_cd_ramo                          RAMO
              ,pc.aupc_cace_capo_nu_poliza                        POLIZA
              ,c.cace_st_certificado                              ESTATUS
              ,c.cace_fe_anulacion                                FECHA_ANULACION
              ,ca.cana_de_causa_anulacion                         CAUSA_ANULACION
              ,pc.aupc_folio_credito                              CREDITO
              ,c.cace_mt_prima_anual                              PRIMA_ANUAL
              --AUTO--
              ,F.AUIR_AUVE_NU_SERIAL_CARROCERIA                   SERIE
              ,F.AUIR_CAPS_CD_PROVEEDOR||'-'||F.AUIR_DE_VIDRIOS   MODELO
              ,F.AUIR_DE_ACCESORIOS                               PLACAS
              ,F.AUIR_DE_FAROS                                    MOTOR
              ,F.AUIR_DE_LATONERIA||'-'||F.AUIR_DE_MARCA_RADIO    MARCA 
              ,F.AUIR_DE_TAPICERIA                                TIPO_VEH 
              ,F.AUIR_DE_MODELO_RADIO                             DESCRIPCIONAUT
          into infoAuto
          from cart_certificados c
              ,cart_causas_anulacion_recibos ca
              ,autt_pol_cred     pc
              ,auth_inspecciones_riesgo f
         where pc.aupc_cace_casu_cd_sucursal = 6
           and pc.aupc_cace_carp_cd_ramo     = to_number(substr(v.VTAP_NU_CONTRATO,1,instr(v.VTAP_NU_CONTRATO,'-')-1))
           and pc.aupc_cace_capo_nu_poliza   = to_number(substr(v.VTAP_NU_CONTRATO,instr(v.VTAP_NU_CONTRATO,'-')+1))
           and pc.aupc_cace_nu_certificado   = 0
           and pc.aupc_id_origen_poliza      = 'C'
           --
           and c.cace_casu_cd_sucursal  = pc.aupc_cace_casu_cd_sucursal
           and c.cace_carp_cd_ramo      = pc.aupc_cace_carp_cd_ramo
           and c.cace_capo_nu_poliza    = pc.aupc_cace_capo_nu_poliza
           and c.cace_nu_certificado    = pc.aupc_cace_nu_certificado
           --
           AND f.AUIR_AUCE_CASU_CD_SUCURSAL(+)     = c.cace_casu_cd_sucursal
           AND f.AUIR_AUCE_CARP_CD_RAMO(+)         = c.cace_carp_cd_ramo
           AND f.AUIR_AUCE_CAPO_NU_POLIZA(+)       = c.cace_capo_nu_poliza
           AND f.AUIR_AUCE_CACE_NU_CERTIFICADO (+) = c.cace_nu_certificado
           --
           AND ca.cana_cd_causa_anulacion(+)       = c.cace_cd_causa_anulacion;
           
           if infoAuto.estatus <> 1 then
             error := error || case when infoAuto.estatus = 11 then 'P�liza cancelada de Automovil: '|| v.VTAP_NU_CONTRATO||
                                                           ' ;Fecha Anulaci�n: '||infoAuto.fechaAnula||' ;Causa: '|| infoAuto.causaAnula
                                    when infoAuto.estatus = 12 then 'P�liza terminada de Autom�vil'
                                    else 'P�liza de Autom�vil no Viegnte. Estatus actual: '||infoAuto.estatus end;
           end if;
           
      exception when no_data_found then
                     error := error||'Informaci�n de Autos no encotrada. ';
                when others then
                     error := error||'Error al buscar la informaci�n de Autos: '||sqlerrm;
      end;
      
      if error is null or length(error) <= 0 then
        
          -- Cargar informacion del auto a la tabla de carga
          v.VTAP_VDATO1   := infoAuto.TIPO_VEH;
          v.VTAP_VDATO10  := infoAuto.SERIE;
          v.VTAP_VDATO11  := infoAuto.MOTOR;
          v.VTAP_VDATO12  := infoAuto.DESCRIPCION;
          v.VTAP_VDATO8   := infoAuto.MARCA;
          v.VTAP_VDATO9   := infoAuto.MODELO;
          v.VTAP_SEXO_BEN := infoAuto.PLACAS;	
          v.VTAP_NOMBRE_BEN := infoAuto.PRIMA_ANUAL;
          
          select infoAuto.RAMO||'-'||ramo.carp_de_ramo
            into v.VTAP_AP_PATERNO_BEN
            from cart_ramos_polizas ramo
           where ramo.carp_cd_ramo = infoAuto.RAMO;
          
          -- Obtener Tarifas --
          begin 
            
           select ppv.hopp_dato1     TARIFA_VIDA
                 ,ppg.hopp_dato1     TARIFA_GAP
             into tarifaVida
                 ,tarifaGap
             from homologa_prodplanes ppv
                 ,homologa_prodplanes ppg
            where ppv.hopp_cd_ramo             = 61
              and ppv.hopp_prod_banco_rector   = v.vtap_vdato3
              and ppv.hopp_plan_banco_rector   = v.vtap_vdato4
              and ppv.hopp_dato4               = to_number(v.vtap_vdato5)
              --
              and ppg.hopp_cd_ramo             = 89
              and ppg.hopp_prod_banco_rector   = v.vtap_vdato3
              and ppg.hopp_plan_banco_rector   = v.vtap_vdato4
              and ppg.hopp_dato4               = to_number(v.vtap_vdato5);
              
              if tarifaGap+tarifaVida <> v.VTAP_NDATO8 then
                error := error||'Diferencia en la tarifa total (Vida+Gap). Archivo: '||v.VTAP_NDATO8
                        ||' Sistema: '||(tarifaGap+tarifaVida);
              end if;
          
          exception when no_data_found then
                          error := error||'Tarifas no encontradas. Producto, Subproducto � plazo no encontrado';  
                    when others then
                          error := error||'Error al buscar tarifas. ';          
          end;

          if error is null or length(error) <= 0 then 
          
            -- Calculamos Primas Y Sumas Aseguradas --
            begin 
              
              capital      := to_number(v.vtap_suma_asegurada);
              comisionAp   := to_number(v.vtap_ap_materno_ben);
                 
              sumaVida  := capital+comisionAp+infoAuto.PRIMA_ANUAL;
              sumaGap   := capital+comisionAp+infoAuto.PRIMA_ANUAL;
              
              primaVida := (sumaVida*tarifaVida)/1000;
              primaGAP  := (sumaGap*tarifaGAP)/1000;
              
              v.VTAP_REGISTRO := tarifaVida||'|'||sumaVida||'|'||primaVida||'/'||
                                 tarifaGap ||'|'||sumaGap ||'|'||primaGap;
              
              if abs(round(primaVida+primaGap,2)- round(to_number(v.VTAP_PRIMA),2)) > 2 then
                error := error||'Error en la prima Vida+Gap. Sistema: '||round(primaVida+primaGap,2)
                                    ||' Archivo: '||round(to_number(v.VTAP_PRIMA),2);  
              else
                  -- Registro de Vida --
                  registroVida.Copc_Cargada  := '0';
                  registroVida.Copc_Registro := '';
                  registroVida.Copc_Fe_Carga := v.VTAP_FE_CARGA;
      							
                  registroVida.Copc_Cd_Sucursal    := 1;
                  registroVida.Copc_Cd_Ramo        := 61;
                  registroVida.Copc_Num_Poliza     := 0;
                  registroVida.Copc_Id_Certificado := v.VTAP_ID_CERTIFICADO;
                  registroVida.Copc_Dato22         := v.VTAP_NU_CONTRATO;
                  registroVida.Copc_Tipo_Registro  := 'VID';
                  registroVida.Copc_Suma_Asegurada := v.VTAP_VDATO6;
          									
                  registroVida.Copc_Cuenta       := v.VTAP_CUENTA;
                  registroVida.Copc_Nu_Credito   := v.VTAP_ID_CERTIFICADO;
                  registroVida.Copc_Ap_Paterno   := v.VTAP_AP_PATERNO;
                  registroVida.Copc_Ap_Materno   := v.VTAP_AP_MATERNO;
                  registroVida.Copc_Nombre       := v.VTAP_NOMBRE;
                  registroVida.Copc_Fe_Nac       := v.VTAP_FE_NAC;
                  
                  registroVida.Copc_Sexo         := CASE WHEN v.VTAP_SEXO = 'F' THEN 'FE' 
                                                         WHEN v.VTAP_SEXO = 'M' THEN 'MA'
                                                         ELSE v.VTAP_SEXO END;		
                  
                  registroVida.Copc_Num_Cliente  := v.VTAP_NUM_CLIENTE;
                  registroVida.Copc_Rfc          := v.VTAP_RFC;
                  registroVida.Copc_Calle        := v.VTAP_CALLE;
                  registroVida.Copc_Colonia      := v.VTAP_COLONIA;
                  registroVida.Copc_Delmunic     := v.VTAP_DELMUNIC;
                  registroVida.Copc_Cd_Estado    := v.VTAP_CD_ESTADO;
                  registroVida.Copc_Cp           := v.VTAP_CP;
                  registroVida.Copc_Dato27       := v.VTAP_LADA;
                  registroVida.Copc_Telefono     := v.VTAP_TELEFONO;
                  registroVida.Copc_Tipo_Cliente := v.VTAP_NDATO1;
                  registroVida.Copc_Fe_Ingreso   := v.VTAP_FE_INGRESO;
                  registroVida.Copc_Prima        := primaVida;
      							
                  registroVida.Copc_Dato6        := sumaVida;
                  registroVida.Copc_Dato8        := v.VTAP_VDATO6; 
      		
                  registroVida.Copc_Dato7        := tarifaVida;							
                  registroVida.Copc_Bco_Producto := v.VTAP_VDATO3;
                  registroVida.Copc_Sub_Producto := v.VTAP_VDATO4;
      							
                  registroVida.Copc_Dato33       := to_char(v.VTAP_DDATO1,'dd/mm/yyyy');
                  registroVida.Copc_Dato34       := to_char(v.VTAP_DDATO2,'dd/mm/yyyy');
                  registroVida.Copc_Nu_Plazo     := v.VTAP_VDATO5;
      							
                  registroVida.Copc_Dato49       := 'PRIMA AUTOCREDITO BCO: '||v.VTAP_PRIMA||'|'||
                                                    'COMISION x APERTURA: '||comisionAp;
                  registroVida.Copc_Dato16       := v.VTAP_TIPO_VENTA;	
                  registroVida.Copc_Dato11       := v.VTAP_ARCHIVO;
                  
                  -- Registro de GAP --
                  registroGap.Copc_Cargada  := '0';
                  registroGap.Copc_Registro := '';
                  registroGap.Copc_Fe_Carga := v.VTAP_FE_CARGA;
      							
                  registroGap.Copc_Cd_Sucursal    := 1;
                  registroGap.Copc_Cd_Ramo        := 89;
                  registroGap.Copc_Num_Poliza     := 0;
                  registroGap.Copc_Id_Certificado := v.VTAP_ID_CERTIFICADO;
                  registroGap.Copc_Dato22         := v.VTAP_NU_CONTRATO;
                  registroGap.Copc_Tipo_Registro  := 'GAP';
                  registroGap.Copc_Suma_Asegurada := v.VTAP_VDATO6;
          									
                  registroGap.Copc_Cuenta       := v.VTAP_CUENTA;
                  registroGap.Copc_Nu_Credito   := v.VTAP_ID_CERTIFICADO;
                  registroGap.Copc_Ap_Paterno   := v.VTAP_AP_PATERNO;
                  registroGap.Copc_Ap_Materno   := v.VTAP_AP_MATERNO;
                  registroGap.Copc_Nombre       := v.VTAP_NOMBRE;
                  registroGap.Copc_Fe_Nac       := v.VTAP_FE_NAC;
                  
                  registroGap.Copc_Sexo         := CASE WHEN v.VTAP_SEXO = 'F' THEN 'FE' 
                                                        WHEN v.VTAP_SEXO = 'M' THEN 'MA'
                                                        ELSE v.VTAP_SEXO END;		
                  
                  registroGap.Copc_Num_Cliente  := v.VTAP_NUM_CLIENTE;
                  registroGap.Copc_Rfc          := v.VTAP_RFC;
                  registroGap.Copc_Calle        := v.VTAP_CALLE;
                  registroGap.Copc_Colonia      := v.VTAP_COLONIA;
                  registroGap.Copc_Delmunic     := v.VTAP_DELMUNIC;
                  registroGap.Copc_Cd_Estado    := v.VTAP_CD_ESTADO;
                  registroGap.Copc_Cp           := v.VTAP_CP;
                  registroGap.Copc_Dato27       := v.VTAP_LADA;
                  registroGap.Copc_Telefono     := v.VTAP_TELEFONO;
                  registroGap.Copc_Tipo_Cliente := v.VTAP_NDATO1;
                  registroGap.Copc_Fe_Ingreso   := v.VTAP_FE_INGRESO;
                  registroGap.Copc_Prima        := primaGap;
      							
                  registroGap.Copc_Dato6        := sumaGap;
                  registroGap.Copc_Dato8        := v.VTAP_VDATO6; 
      		
                  registroGap.Copc_Dato7        := tarifaGap;							
                  registroGap.Copc_Bco_Producto := v.VTAP_VDATO3;
                  registroGap.Copc_Sub_Producto := v.VTAP_VDATO4;
      							
                  registroGap.Copc_Dato33       := to_char(v.VTAP_DDATO1,'dd/mm/yyyy');
                  registroGap.Copc_Dato34       := to_char(v.VTAP_DDATO2,'dd/mm/yyyy');
                  registroGap.Copc_Nu_Plazo     := v.VTAP_VDATO5;
                  
                  registroGap.Copc_Dato2        := infoAuto.TIPO_VEH;
                  registroGap.Copc_Dato3        := infoAuto.DESCRIPCION;
                  registroGap.Copc_Dato4        := infoAuto.MODELO;       
                  registroGap.Copc_Dato5        := infoAuto.SERIE;
                  registroGap.Copc_Dato19       := infoAuto.PLACAS;
                  registroGap.Copc_Dato17       := infoAuto.MARCA;
                  registroGap.Copc_Dato20       := 'AMPLIA';
                  registroGap.Copc_Dato13       := v.VTAP_AP_PATERNO_BEN;
                  registroGap.Copc_Dato14       := infoAuto.PRIMA_ANUAL;
                  registroGap.Copc_Dato15       := infoAuto.MOTOR;
                  
                  registroGap.Copc_Dato49       := 'PRIMA AUTOCREDITO BCO: '||v.VTAP_PRIMA;	
                  registroGap.Copc_Dato49       := 'PRIMA AUTOCREDITO BCO: '||v.VTAP_PRIMA||'|'||
                                                   'COMISION x APERTURA: '||comisionAp;						
                  registroGap.Copc_Dato16       := v.VTAP_TIPO_VENTA;	
                  registroGap.Copc_Dato11       := v.VTAP_ARCHIVO;
                  
                  -- Inserci�n --
                  insert into colectivos_precarga values registroVida;
                  insert into colectivos_precarga values registroGap;
                  
                  commit;
              end if; -- Diferencia en Primas

            exception when others then
              error := error||'Error general al calcular primas: '||sqlerrm;
            end;       
         end if; -- Tarifas no encontradas o error en update de info de autos 
       end if; -- info de auto no encontrada
     exception when others then
      error := error||'Error: '||sqlerrm; 
     end; 
       
       if error is null or length(error) <= 0 then
                            
          -- Listos para emitir
          v.VTAP_CARGADA   := '2';
          
          update vta_precarga vp
         set row = v
           where vp.VTAP_CD_RAMO        = v.VTAP_CD_RAMO
             and vp.VTAP_CD_SUCURSAL    = v.VTAP_CD_SUCURSAL
             and vp.VTAP_ID_CERTIFICADO = v.VTAP_ID_CERTIFICADO 
             and vp.VTAP_NUM_POLIZA     = v.VTAP_NUM_POLIZA
             and vp.VTAP_SISTEMA_ORIGEN = v.VTAP_SISTEMA_ORIGEN;
        
       else
         
          -- Actualizamos errores
          v.VTAP_DES_ERROR := error;
          v.VTAP_CARGADA   := '3';
          
          update vta_precarga vp
         set row = v
           where vp.VTAP_CD_RAMO        = v.VTAP_CD_RAMO
             and vp.VTAP_CD_SUCURSAL    = v.VTAP_CD_SUCURSAL
             and vp.VTAP_ID_CERTIFICADO = v.VTAP_ID_CERTIFICADO 
             and vp.VTAP_NUM_POLIZA     = v.VTAP_NUM_POLIZA
             and vp.VTAP_SISTEMA_ORIGEN = v.VTAP_SISTEMA_ORIGEN;
          
       end if;
       
       commit;
       
      i:= i+1;

  end loop;
  
  dbms_output.put_line ('Procesados: '||i);
  
end;