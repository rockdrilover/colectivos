DECLARE 
	paramCanal  number := ?;
	paramRamo   number := ?;
	paramPoliza	number := ?;
	paramCerti  number := ?;	
  	
	cliente 	COLECTIVOS_CLIENTES%rowtype;		
	w_nu_cliente colectivos_clientes.cocn_nu_cliente%Type;
	v_nac       varchar2(3);
    v_rif       varchar2(11);
	v_mensaje	varchar2(1000) := null;	
	
BEGIN
   
	dbms_output.put_line('1. Get Numero Cliente');
	SELECT CCC.COCC_NU_CLIENTE INTO w_nu_cliente
		FROM COLECTIVOS_CLIENTE_CERTIF CCC
	WHERE CCC.COCC_CASU_CD_SUCURSAL 	= paramCanal
		AND CCC.COCC_CARP_CD_RAMO       = paramRamo
		AND CCC.COCC_CAPO_NU_POLIZA     = paramPoliza
		AND CCC.COCC_NU_CERTIFICADO     = paramCerti                		
		AND NVL(CCC.COCC_TP_CLIENTE, 1)	= 1;

	dbms_output.put_line('2. Get CLIENTE');
	SELECT CC.* INTO cliente
		FROM COLECTIVOS_CLIENTES  CC
	WHERE CC.COCN_NU_CLIENTE	= w_nu_cliente; 
		
	dbms_output.put_line('3. Get RIF y NUM DOM');
	SELECT CACE_CACN_CD_NACIONALIDAD, CACE_CACN_NU_CEDULA_RIF 
				INTO v_nac, v_rif
		FROM CART_CERTIFICADOS
	WHERE CACE_CASU_CD_SUCURSAL	= paramCanal
		AND CACE_CARP_CD_RAMO	= paramRamo
		AND CACE_CAPO_NU_POLIZA	= paramPoliza
		AND CACE_NU_CERTIFICADO	= paramCerti;
	
	dbms_output.put_line('4. Migra Benediciarios');
	PACK_MIGRACOL_SIN.P_Migra_Beneficiarios(paramCanal, paramRamo, paramPoliza, paramCerti, cliente, v_nac, v_rif, v_mensaje);				
	COMMIT;
	
END;