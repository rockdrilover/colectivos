declare 

  canal number   := 1;
  ramo  number   := 9;
  poliza number  := 0;
  idVenta number := 6;
  
  -- VENTAS --
  cursor ventasBT is
      
      select pv.vtap_id_certificado               ID
            ,pv.vtap_cd_sucursal                  CANAL
            ,pv.vtap_cd_ramo                      RAMO
            ,pv.vtap_num_poliza                   POLIZA
            ,pv.vtap_sistema_origen               TIPO
            ,pv.vtap_cargada                      CARGADA
            ,pv.vtap_estatus                      ESTATUS
            ,pv.vtap_nombre                       NOMBRE
            ,pv.vtap_ap_paterno                   AP_PATERNO
            ,pv.vtap_ap_materno                   AP_MATERNO
            ,to_date(pv.vtap_fe_nac,'dd/mm/yyyy') FECHA_NACIMIENTO
            ,pv.vtap_num_cliente                  BUC
            ,to_number(pv.vtap_suma_asegurada)    SUMA_ASEGURADA
            ,to_number(pv.vtap_vdato6)            BASE_CALULO
            ,to_number(pv.vtap_vdato5)            PLAZO 
            ,pv.vtap_vdato3                       PRODUCTO_BCO
            ,pv.vtap_vdato4                       SUBPROD_BCO
            ,to_char(pv.vtap_ndato8)              TARIFA
            ,to_number(pv.vtap_tipo_venta)        IDVENTA
        from vta_precarga pv
       where pv.vtap_cd_sucursal       = 1
         and pv.vtap_cd_ramo           = 94
         and pv.vtap_estatus           = '0'
         and pv.vtap_sistema_origen    = 'BT';
    
              
  limite     number;
  limiteSuma number;
  cumulo     number; 
  
  
  cobertura gioseg.colectivos.t_coberturaCertificado;
  componentes gioseg.colectivos.tablaComponentes;
  
  sumaAsegNueva number;
  primaNueva    number;
  
  producto     number;
  plan         number;
  polizaPU     number;
  
begin
  
  begin 
    select p.copa_nvalor8
      into limite
      from colectivos_parametros p
     where p.copa_des_parametro = 'POLIZA'
       and p.copa_id_parametro  = 'CUMULO'
       and p.copa_nvalor1       = canal
       and p.copa_nvalor2       = ramo
       and p.copa_nvalor3       = poliza
       and p.copa_nvalor4       = idVenta;
  exception when others then
    limite := 350000;
  end;

  begin 
    select to_number(p.copa_vvalor2)
      into limiteSuma
      from colectivos_parametros p
     where p.copa_des_parametro = 'POLIZA'
       and p.copa_id_parametro  = 'SALDOINSOLUTO'
       and p.copa_nvalor1       = canal
       and p.copa_nvalor2       = ramo
       and p.copa_nvalor3       = 612
       and p.copa_nvalor6       = idVenta;
  exception when others then
    limite := 237500;
  end;
  
  for vtBt in ventasBT loop

       if vtBt.Suma_Asegurada >= limiteSuma then
         update vta_precarga pv
            set pv.vtap_estatus           = '3'    
               ,pv.vtap_des_error         = 'La Suma Asegurada para este asegurado se ha superado. Maximo: '||limiteSuma    
          where pv.vtap_cd_sucursal       = vtBt.Canal
            and pv.vtap_cd_ramo           = vtBt.Ramo
            and pv.vtap_num_poliza        = vtBt.Poliza
            and pv.vtap_id_certificado    = vtBt.Id
            and pv.vtap_cargada           = vtBt.Cargada
            and pv.vtap_estatus           = vtBT.Estatus    
            and pv.vtap_sistema_origen    = vtBt.Tipo;
      end if;
      -- Busca certificados BT
      begin 
        select sum(ce.coce_mt_suma_asegurada) SUMA
          into cumulo
          from colectivos_clientes cl
              ,colectivos_cliente_certif clc
              ,colectivos_certificados   ce
         where cl.cocn_nombre         =  vtBt.Nombre
           and cl.cocn_apellido_pat   =  vtBt.Ap_Paterno
           and cl.cocn_apellido_mat   =  vtBt.Ap_Materno
           and cl.cocn_fe_nacimiento  =  vtBt.Fecha_Nacimiento
           and cl.cocn_buc_cliente    =  vtBt.Buc
           --
           and clc.cocc_nu_cliente          = cl.cocn_nu_cliente
           and nvl(clc.cocc_tp_cliente,1)   = 1 -- S�lo Titular
           --
           and ce.coce_casu_cd_sucursal     = clc.cocc_casu_cd_sucursal
           and ce.coce_carp_cd_ramo         = clc.cocc_carp_cd_ramo
           and ce.coce_capo_nu_poliza       = clc.cocc_capo_nu_poliza
           and ce.coce_nu_certificado       = clc.cocc_nu_certificado
           and ce.coce_carp_cd_ramo         = ramo
           and ce.coce_sub_campana          = to_char(idVenta)
           and ce.coce_st_certificado       in (1,2) -- Vigente
         group by cl.cocn_nombre
                 ,cl.cocn_apellido_pat
                 ,cl.cocn_apellido_mat
                 ,cl.cocn_fe_nacimiento
                 ,cl.cocn_buc_cliente;
      exception when others then
         cumulo := 0;  
      end;  
      
      if (cumulo+vtBt.Suma_Asegurada > limite) then
        sumaAsegNueva := Round(limite - cumulo,2);
        
        if sumaAsegNueva > 0 then
          
            -- Calculamos la prima mensual
           
            select HP.HOPP_PROD_COLECTIVO newProducto, HP.HOPP_PLAN_COLECTIVO newPlan,
                   PA.COPA_NVALOR3||decode(length(HP.HOPP_PROD_COLECTIVO),1,'0'||HP.HOPP_PROD_COLECTIVO,HP.HOPP_PROD_COLECTIVO) newPoliza
            into producto, plan, polizaPU
            from colectivos_parametros PA
                ,homologa_prodplanes HP
            where PA.COPA_NVALOR1            = canal
              and PA.COPA_NVALOR2            = ramo
              and PA.COPA_NVALOR5            = vtBt.Plazo
              and PA.COPA_NVALOR6            = idVenta
              and PA.COPA_DES_PARAMETRO      = 'POLIZA'
              and PA.COPA_ID_PARAMETRO       = 'GEP'
              and PA.COPA_NVALOR7            = 1
              and PA.COPA_NVALOR4            = 1
              --
              and HP.HOPP_CD_RAMO            = ramo
              and HP.HOPP_PROD_BANCO_RECTOR  = vtBt.Producto_Bco
              and HP.HOPP_PLAN_BANCO_RECTOR  = vtBt.Subprod_Bco
              and to_number(HP.HOPP_DATO1)   = to_number(vtBt.Tarifa)
              and HP.HOPP_DATO3              = idVenta
              and HP.HOPP_DATO4              = vtBt.Plazo;       
              
             cobertura := gioseg.colectivos.primaPura
                                   (canal
                                   ,ramo
                                   ,polizaPU
                                   ,producto
                                   ,plan
                                   ,sumaAsegNueva);
                                                                            
             componentes := gioseg.colectivos.primaNeta
                                   (canal,
                                    ramo,
                                    polizaPU,
                                    producto,
                                    plan,
                                    cobertura.primaPura); 
                                    
                                    
             primaNueva := 0;
             
             for i in componentes.first .. componentes.last
             loop
                 if(i <> 4) then 
                      primaNueva := primaNueva + NVL(componentes(i).monto,0);
                 end if;
             end loop;

             primaNueva := round(NVL(primaNueva,0) + NVL(cobertura.primaPura,0),2);
             ----
             
             begin 
               
                update vta_precarga pv
                   set pv.vtap_suma_asegurada    = sumaAsegNueva
                      ,pv.vtap_prima             = primaNueva
                 where pv.vtap_cd_sucursal       = vtBt.Canal
                   and pv.vtap_cd_ramo           = vtBt.Ramo
                   and pv.vtap_num_poliza        = vtBt.Poliza
                   and pv.vtap_id_certificado    = vtBt.Id
                   and pv.vtap_cargada           = vtBt.Cargada
                   and pv.vtap_estatus           = vtBt.Estatus
                   and pv.vtap_sistema_origen    = vtBt.Tipo;
             end;

        else 
              
              update vta_precarga pv
                 set pv.vtap_estatus           = '3'    
                    ,pv.vtap_des_error         = 'El cumulo para este asegurado se ha superado. Maximo: '||limite
               where pv.vtap_cd_sucursal       = vtBt.Canal
                 and pv.vtap_cd_ramo           = vtBt.Ramo
                 and pv.vtap_num_poliza        = vtBt.Poliza
                 and pv.vtap_id_certificado    = vtBt.Id
                 and pv.vtap_cargada           = vtBt.Cargada
                 and pv.vtap_estatus           = vtBT.Estatus    
                 and pv.vtap_sistema_origen    = vtBt.Tipo;
                   
        end if;
        
        commit;
        
      end if;
         
  end loop;

end;