SELECT CL.COCN_BUC_CLIENTE  BUC, 
        CE.COCE_CAPO_NU_POLIZA POLIZA,  
        CE.COCE_NU_CERTIFICADO CERTIFICADO,  
        COCO_PO_TOTAL PRC_COMISION,  
        CE.COCE_CARP_CD_RAMO RAMO, 
        PL.ALPL_DE_PLAN DESC_PRODUCTO,         
        CE.COCE_CASU_CD_SUCURSAL CANAL,         
        CE.COCE_CAZB_CD_SUCURSAL SUCURSAL, 
        DECODE(COCO_TP_MOVIMIENTO, 1, 'VENTA_NUEVA', 
                                2, 'STOCK', 
                                3, 'CANCELACION',
                                4, 'REHABILITACION',
                                5, 'RENOVACION',
                                6, 'COBRABZA',
                                TO_CHAR(COCO_TP_MOVIMIENTO)) TIPO_OPERACION,  
        COCO_MT_PRIMA PRIMA_NETA_EMITIDA,  
        TO_NUMBER(SUBSTR(CE.COCE_DI_COBRO1,INSTR(CE.COCE_DI_COBRO1, 'O',1,1) + 1,INSTR(CE.COCE_DI_COBRO1, '|',1,1) -INSTR(CE.COCE_DI_COBRO1, 'O',1,1)-1)) DERECHOS_P_EMITIDA,  
        COCO_NVALOR3 RECARGOS_P_EMITIDA, 
        0  ASISTENCIA_EMITIDA, 
        TO_NUMBER(SUBSTR(CE.COCE_DI_COBRO1,INSTR(CE.COCE_DI_COBRO1, 'A',1,1) + 1,INSTR(CE.COCE_DI_COBRO1, '|',1,3)-INSTR(CE.COCE_DI_COBRO1, 'A',1,1) -1)) IVA_P_EMITIDA,      
        (COCO_MT_PRIMA +
            TO_NUMBER(SUBSTR(CE.COCE_DI_COBRO1,INSTR(CE.COCE_DI_COBRO1, 'O',1,1) + 1,INSTR(CE.COCE_DI_COBRO1, '|',1,1) -INSTR(CE.COCE_DI_COBRO1, 'O',1,1)-1)) +
            COCO_NVALOR3 +
            TO_NUMBER(SUBSTR(CE.COCE_DI_COBRO1,INSTR(CE.COCE_DI_COBRO1, 'A',1,1) + 1,INSTR(CE.COCE_DI_COBRO1, '|',1,3)-INSTR(CE.COCE_DI_COBRO1, 'A',1,1) -1))) PRIMA_TOTAL,    
        0,0,0,0,0,0,
        COCO_MT_TOTAL MONTO_COMISION_TECNICA, 
        CE.COCE_CAPU_CD_PRODUCTO CLAVE_PRODUCTO, 
        COCO_FE_INGRESO FECHA_REG_CONTABLE, 
        P.CAPO_CD_FR_PAGO_NORMAL FORMA_PAGO, 
        '' EJECUTIVO, 
        '' CONVENIO, 
        '' ID_CONVENIO,
        PL.ALPL_DATO3 CENTRO_COSTOS,
        CE.COCE_NO_RECIBO RECIBO,
        CC.COCC_ID_CERTIFICADO CREDITO,
        0 CRENUE,
        EST.ALES_CAMPO1 ESTATUS,
        CE.COCE_NU_CUENTA CUENTA,
        CE.COCE_TP_PRODUCTO_BCO PRODUCTO,
        CE.COCE_TP_SUBPROD_BCO SUBPRODUCTO,
        CL.COCN_APELLIDO_PAT APELLIDO_PATERNO,
        CL.COCN_APELLIDO_MAT APELLIDO_MATERNO,
        CL.COCN_NOMBRE NOMBRE,
        CL.COCN_CD_SEXO SEXO,
        CL.COCN_FE_NACIMIENTO FECHA_NACIMIENTO,
        CL.COCN_RFC RFC,
        CE.COCE_CD_PLAZO PLAZO,
        HEAD.COCE_FE_DESDE FECHA_INICIO_POLIZA,
        HEAD.COCE_FE_HASTA FECHA_FIN_POLIZA,
        CE.COCE_FE_SUSCRIPCION FECHA_INGRESO,
        CE.COCE_FE_DESDE FECHA_DESDE,        
        CE.COCE_FE_ANULACION FECHA_ANULACION,
        CE.COCE_FE_ANULACION_COL FECHA_CANCELACION,
        TRIM(TO_CHAR(DECODE(CE.COCE_SUB_CAMPANA,'7',TO_NUMBER(CE.COCE_MT_SUMA_ASEG_SI),CE.COCE_MT_SUMA_ASEGURADA),'999999999.99')) SUMA_ASEGURADA,
        NVL(CE.COCE_MT_BCO_DEVOLUCION,0) MONTO_DEVOLUCION,
        DECODE(CE.COCE_SUB_CAMPANA,'7',CE.COCE_MT_SUMA_ASEGURADA,CE.COCE_MT_SUMA_ASEG_SI) BASE_CALCULO,
        ROUND(CE.COCE_MT_PRIMA_PURA*1000/DECODE(CE.COCE_SUB_CAMPANA,'7',DECODE(CE.COCE_MT_SUMA_ASEGURADA, 0,1,CE.COCE_MT_SUMA_ASEGURADA),DECODE(CE.COCE_MT_SUMA_ASEG_SI, 0,1,CE.COCE_MT_SUMA_ASEG_SI)),4) TARIFA,
        CL.COCN_CD_ESTADO CD_ESTADO,
        ES.CAES_DE_ESTADO ESTADO,
        CL.COCN_CD_POSTAL CP,
        CE.COCE_CAPU_CD_PRODUCTO PRODUCTO_ASEGURADORA,
        CE.COCE_CAPB_CD_PLAN PLAN_ASEGURADORA,
        (SELECT SUM (COB.COCB_TA_RIESGO) 
            FROM COLECTIVOS_COBERTURAS COB
           WHERE COB.COCB_CASU_CD_SUCURSAL =  1
             AND COB.COCB_CARP_CD_RAMO     = 61
             AND COB.COCB_CARB_CD_RAMO     = 14
             AND COB.COCB_CACB_CD_COBERTURA  <> '003'
             AND COB.COCB_CAPU_CD_PRODUCTO   = CE.COCE_CAPU_CD_PRODUCTO
             AND COB.COCB_CAPB_CD_PLAN       = CE.COCE_CAPB_CD_PLAN
             AND COB.COCB_CER_NU_COBERTURA   = CE.COCE_NU_COBERTURA) CUOTA_BASICA,
        '','','',
        ABS( NVL(CE.COCE_MT_DEVOLUCION,0) - NVL(CE.COCE_MT_BCO_DEVOLUCION,0) ) DIFERENCIA_DEVOLUCION
    FROM COLECTIVOS_COMISIONES 
    INNER JOIN COLECTIVOS_CERTIFICADOS CE 
        ON(COCE_CASU_CD_SUCURSAL = COCO_CASU_CD_SUCURSAL
        AND CE.COCE_CARP_CD_RAMO = COCO_CARP_CD_RAMO
        AND CE.COCE_CAPO_NU_POLIZA = COCO_CAPO_NU_POLIZA
        AND CE.COCE_NU_CERTIFICADO = COCO_COCE_NU_CERTIFICADO 
        AND CE.COCE_NU_MOVIMIENTO = COCO_NU_MOVIMIENTO)
    INNER JOIN COLECTIVOS_CERTIFICADOS HEAD 
        ON(HEAD.COCE_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL 				
        AND HEAD.COCE_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO 					
        AND HEAD.COCE_CAPO_NU_POLIZA = CE.COCE_CAPO_NU_POLIZA 				
        AND HEAD.COCE_NU_CERTIFICADO = 0)
    INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC 
        ON (CC.COCC_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL 
        AND CC.COCC_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO 
        AND CC.COCC_CAPO_NU_POLIZA = CE.COCE_CAPO_NU_POLIZA 
        AND CC.COCC_NU_CERTIFICADO = CE.COCE_NU_CERTIFICADO
        AND NVL(CC.COCC_TP_CLIENTE, 1) = 1) 				                        
    INNER JOIN COLECTIVOS_CLIENTES CL 
        ON(CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE)
    INNER JOIN CART_POLIZAS P 
        ON (P.CAPO_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL 
        AND P.CAPO_CARP_CD_RAMO     = CE.COCE_CARP_CD_RAMO 
        AND P.CAPO_NU_POLIZA        = CE.COCE_CAPO_NU_POLIZA) 			
    INNER JOIN ALTERNA_PLANES PL ON(PL.ALPL_CD_RAMO = COCO_CARP_CD_RAMO
                    AND PL.ALPL_CD_PRODUCTO = COCO_CAPU_CD_PRODUCTO         
                    AND PL.ALPL_CD_PLAN = COCO_CAPB_CD_PLAN)                    
    INNER JOIN ALTERNA_ESTATUS EST ON(EST.ALES_CD_ESTATUS = CE.COCE_ST_CERTIFICADO)    
    LEFT JOIN CART_ESTADOS ES ON(ES.CAES_CD_ESTADO = CL.COCN_CD_ESTADO)
WHERE COCO_FE_CARGA BETWEEN TO_DATE('---fecha_ini---', 'dd/MM/yyyy') AND TO_DATE('---fecha_fin---', 'dd/MM/yyyy') 
    AND COCO_TP_COMISION = 1 
UNION ALL 
SELECT CL.COCN_BUC_CLIENTE  BUC, 
        CM.COCM_CAPO_NU_POLIZA POLIZA,  
        CM.COCM_NU_CERTIFICADO CERTIFICADO,  
        COCO_PO_TOTAL PRC_COMISION,  
        CM.COCM_CARP_CD_RAMO RAMO, 
        PL.ALPL_DE_PLAN DESC_PRODUCTO,         
        CM.COCM_CASU_CD_SUCURSAL CANAL,         
        CM.COCM_CAZB_CD_SUCURSAL SUCURSAL, 
        DECODE(COCO_TP_MOVIMIENTO, 1, 'VENTA_NUEVA', 
                                2, 'STOCK', 
                                3, 'CANCELACION',
                                4, 'REHABILITACION',
                                5, 'RENOVACION',
                                6, 'COBRABZA',
                                TO_CHAR(COCO_TP_MOVIMIENTO)) TIPO_OPERACION,
        COCO_MT_PRIMA PRIMA_NETA_EMITIDA,  
        TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'O',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,1) -INSTR(CM.COCM_DI_COBRO1, 'O',1,1)-1)) DERECHOS_P_EMITIDA,  
        COCO_NVALOR3 RECARGOS_P_EMITIDA, 
        0  ASISTENCIA_EMITIDA, 
        TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'A',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,3)-INSTR(CM.COCM_DI_COBRO1, 'A',1,1) -1)) IVA_P_EMITIDA,      
        (COCO_MT_PRIMA +
            TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'O',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,1) -INSTR(CM.COCM_DI_COBRO1, 'O',1,1)-1)) +
            COCO_NVALOR3 +
            TO_NUMBER(SUBSTR(CM.COCM_DI_COBRO1,INSTR(CM.COCM_DI_COBRO1, 'A',1,1) + 1,INSTR(CM.COCM_DI_COBRO1, '|',1,3)-INSTR(CM.COCM_DI_COBRO1, 'A',1,1) -1))) PRIMA_TOTAL,    
        0,0,0,0,0,0,
        COCO_MT_TOTAL MONTO_COMISION_TECNICA, 
        CM.COCM_CAPU_CD_PRODUCTO CLAVE_PRODUCTO, 
        COCO_FE_INGRESO FECHA_REG_CONTABLE, 
        P.CAPO_CD_FR_PAGO_NORMAL FORMA_PAGO, 
        '' EJECUTIVO, 
        '' CONVENIO, 
        '' ID_CONVENIO,
        PL.ALPL_DATO3 CENTRO_COSTOS,
        CM.COCM_NU_RECIBO RECIBO,
        CC.COCC_ID_CERTIFICADO CREDITO,
        0 CRENUE,
        EST.ALES_CAMPO1 ESTATUS,
        CM.COCM_NU_CUENTA CUENTA,
        CM.COCM_TP_PRODUCTO_BCO PRODUCTO,
        CM.COCM_TP_SUBPROD_BCO SUBPRODUCTO,
        CL.COCN_APELLIDO_PAT APELLIDO_PATERNO,
        CL.COCN_APELLIDO_MAT APELLIDO_MATERNO,
        CL.COCN_NOMBRE NOMBRE,
        CL.COCN_CD_SEXO SEXO,
        CL.COCN_FE_NACIMIENTO FECHA_NACIMIENTO,
        CL.COCN_RFC RFC,
        CM.COCM_CD_PLAZO PLAZO,
        HEAD.COCE_FE_DESDE FECHA_INICIO_POLIZA,
        HEAD.COCE_FE_HASTA FECHA_FIN_POLIZA,
        CM.COCM_FE_SUSCRIPCION FECHA_INGRESO,
        CM.COCM_FE_DESDE FECHA_DESDE,        
        CM.COCM_FE_ANULACION_REAL FECHA_ANULACION,
        CM.COCM_FE_ANULACION FECHA_CANCELACION,
        TRIM(TO_CHAR(DECODE(CM.COCM_SUB_CAMPANA,'7',TO_NUMBER(CM.COCM_MT_SUMA_ASEG_SI),CM.COCM_MT_SUMA_ASEGURADA),'999999999.99')) SUMA_ASEGURADA,
        NVL(CM.COCM_MT_BCO_DEVOLUCION,0) MONTO_DEVOLUCION,
        DECODE(CM.COCM_SUB_CAMPANA,'7',CM.COCM_MT_SUMA_ASEGURADA,CM.COCM_MT_SUMA_ASEG_SI) BASE_CALCULO,
        ROUND(CM.COCM_MT_PRIMA_PURA*1000/DECODE(CM.COCM_SUB_CAMPANA,'7',DECODE(CM.COCM_MT_SUMA_ASEGURADA, 0,1,CM.COCM_MT_SUMA_ASEGURADA),DECODE(CM.COCM_MT_SUMA_ASEG_SI, 0,1,CM.COCM_MT_SUMA_ASEG_SI)),4) TARIFA,
        CL.COCN_CD_ESTADO CD_ESTADO,
        ES.CAES_DE_ESTADO ESTADO,
        CL.COCN_CD_POSTAL CP,
        CM.COCM_CAPU_CD_PRODUCTO PRODUCTO_ASEGURADORA,
        CM.COCM_CAPB_CD_PLAN PLAN_ASEGURADORA,
        (SELECT SUM (COB.COCB_TA_RIESGO) 
            FROM COLECTIVOS_COBERTURAS COB
           WHERE COB.COCB_CASU_CD_SUCURSAL =  1
             AND COB.COCB_CARP_CD_RAMO     = 61
             AND COB.COCB_CARB_CD_RAMO     = 14
             AND COB.COCB_CACB_CD_COBERTURA  <> '003'
             AND COB.COCB_CAPU_CD_PRODUCTO   = CM.COCM_CAPU_CD_PRODUCTO
             AND COB.COCB_CAPB_CD_PLAN       = CM.COCM_CAPB_CD_PLAN
             AND COB.COCB_CER_NU_COBERTURA   = CM.COCM_NU_COBERTURA) CUOTA_BASICA,
        '','','',
        ABS( NVL(CM.COCM_MT_DEVOLUCION,0) - NVL(CM.COCM_MT_BCO_DEVOLUCION,0) ) DIFERENCIA_DEVOLUCION        
    FROM COLECTIVOS_COMISIONES 
    INNER JOIN COLECTIVOS_CERTIFICADOS_MOV CM 
        ON(CM.COCM_CASU_CD_SUCURSAL = COCO_CASU_CD_SUCURSAL
        AND CM.COCM_CARP_CD_RAMO = COCO_CARP_CD_RAMO
        AND CM.COCM_CAPO_NU_POLIZA = COCO_CAPO_NU_POLIZA
        AND CM.COCM_NU_CERTIFICADO = COCO_COCE_NU_CERTIFICADO 
        AND CM.COCM_NU_MOVIMIENTO = COCO_NU_MOVIMIENTO)
    INNER JOIN COLECTIVOS_CERTIFICADOS HEAD 
        ON(HEAD.COCE_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL 				
        AND HEAD.COCE_CARP_CD_RAMO = CM.COCM_CARP_CD_RAMO 					
        AND HEAD.COCE_CAPO_NU_POLIZA = CM.COCM_CAPO_NU_POLIZA 				
        AND HEAD.COCE_NU_CERTIFICADO = 0)
    INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC 
        ON (CC.COCC_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL 
        AND CC.COCC_CARP_CD_RAMO = CM.COCM_CARP_CD_RAMO 
        AND CC.COCC_CAPO_NU_POLIZA = CM.COCM_CAPO_NU_POLIZA 
        AND CC.COCC_NU_CERTIFICADO = CM.COCM_NU_CERTIFICADO
        AND NVL(CC.COCC_TP_CLIENTE, 1) = 1) 				                        
    INNER JOIN COLECTIVOS_CLIENTES CL 
        ON(CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE)
    INNER JOIN CART_POLIZAS P 
        ON (P.CAPO_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL 
        AND P.CAPO_CARP_CD_RAMO     = CM.COCM_CARP_CD_RAMO 
        AND P.CAPO_NU_POLIZA        = CM.COCM_CAPO_NU_POLIZA) 			
    INNER JOIN ALTERNA_PLANES PL ON(PL.ALPL_CD_RAMO = COCO_CARP_CD_RAMO
                    AND PL.ALPL_CD_PRODUCTO = COCO_CAPU_CD_PRODUCTO         
                    AND PL.ALPL_CD_PLAN = COCO_CAPB_CD_PLAN)                    
    INNER JOIN ALTERNA_ESTATUS EST ON(EST.ALES_CD_ESTATUS = CM.COCM_ST_CERTIFICADO)    
    LEFT JOIN CART_ESTADOS ES ON(ES.CAES_CD_ESTADO = CL.COCN_CD_ESTADO)
WHERE COCO_FE_CARGA BETWEEN TO_DATE('---fecha_ini---', 'dd/MM/yyyy') AND TO_DATE('---fecha_fin---', 'dd/MM/yyyy') 
    AND COCO_TP_COMISION = 1