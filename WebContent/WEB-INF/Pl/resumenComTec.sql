SELECT T.COCO_CARP_CD_RAMO, T.POLIZA, T.FRPAGO, T.TIPO_PMA, T.ALPL_DATO3, T.COCO_VVALOR1,
        SUM(T.PMANET) PMANET, SUM(T.PMANETCAN) PMANETCAN, 0, 0, 0, 0,
        (SUM(T.PMANET) + SUM(T.PMANETCAN)) TOTAL, T.COCO_PO_TOTAL, SUM(T.COCO_MT_TOTAL) 
    FROM (
        SELECT COCO_CARP_CD_RAMO, 
                DECODE(NVL(COCE_SUB_CAMPANA, 0), 0, TO_CHAR(COCO_CAPO_NU_POLIZA), NVL(COCE_SUB_CAMPANA, 0) || ' - ' || NVL(COPA_VVALOR1, '')) POLIZA, 
                DECODE(NVL(COCE_SUB_CAMPANA, 0), 0, 'MENSUAL', 'UNICA') FRPAGO,
                DECODE(COCE_NU_RENOVACION,NULL,'PRIMER A�O','RENOVACION') TIPO_PMA, 
                PL.ALPL_DATO3, 
                COCO_VVALOR1,
                DECODE(COCO_TP_MOVIMIENTO, 3, 0, COCO_MT_PRIMA) PMANET, 
                DECODE(COCO_TP_MOVIMIENTO, 3, COCO_MT_PRIMA, 0) PMANETCAN, 
                COCO_MT_TOTAL, COCO_PO_TOTAL
            FROM COLECTIVOS_COMISIONES 
            INNER JOIN COLECTIVOS_CERTIFICADOS ON(COCE_CASU_CD_SUCURSAL = COCO_CASU_CD_SUCURSAL
							AND COCE_CARP_CD_RAMO = COCO_CARP_CD_RAMO
							AND COCE_CAPO_NU_POLIZA = COCO_CAPO_NU_POLIZA
							AND COCE_NU_CERTIFICADO = COCO_COCE_NU_CERTIFICADO 
							AND COCE_NU_MOVIMIENTO = COCO_NU_MOVIMIENTO)
            INNER JOIN ALTERNA_PLANES PL ON(PL.ALPL_CD_RAMO = COCO_CARP_CD_RAMO
                            AND PL.ALPL_CD_PRODUCTO = COCO_CAPU_CD_PRODUCTO         
                            AND PL.ALPL_CD_PLAN = COCO_CAPB_CD_PLAN)                    
            LEFT JOIN COLECTIVOS_PARAMETROS ON(COPA_DES_PARAMETRO = 'PRIMAUNICA'
                            AND COPA_ID_PARAMETRO = 'IDVENTA'
                            AND COPA_NVALOR1 = COCE_CASU_CD_SUCURSAL
                            AND COPA_NVALOR2 = COCE_CARP_CD_RAMO
                            AND COPA_NVALOR4 = NVL(COCE_SUB_CAMPANA, 0))
        WHERE COCO_FE_CARGA BETWEEN TO_DATE('---fecha_ini---', 'dd/MM/yyyy') AND TO_DATE('---fecha_fin---', 'dd/MM/yyyy') 
            AND COCO_TP_COMISION = 1 
        UNION ALL 
        SELECT COCO_CARP_CD_RAMO, 
                DECODE(NVL(COCM_SUB_CAMPANA, 0), 0, TO_CHAR(COCO_CAPO_NU_POLIZA), NVL(COCM_SUB_CAMPANA, 0) || ' - ' || NVL(COPA_VVALOR1, '')) POLIZA,         
                DECODE(NVL(COCM_SUB_CAMPANA, 0), 0, 'MENSUAL', 'UNICA'),
                DECODE(COCM_NU_RENOVACION,NULL,'PRIMER A�O','RENOVACION'), 
                PL.ALPL_DATO3, 
                COCO_VVALOR1,         
                DECODE(COCO_TP_MOVIMIENTO, 3, 0, COCO_MT_PRIMA), 
                DECODE(COCO_TP_MOVIMIENTO, 3, COCO_MT_PRIMA, 0), 
                COCO_MT_TOTAL, COCO_PO_TOTAL
            FROM COLECTIVOS_COMISIONES 
            INNER JOIN COLECTIVOS_CERTIFICADOS_MOV ON(COCM_CASU_CD_SUCURSAL = COCO_CASU_CD_SUCURSAL
							AND COCM_CARP_CD_RAMO = COCO_CARP_CD_RAMO
							AND COCM_CAPO_NU_POLIZA = COCO_CAPO_NU_POLIZA
							AND COCM_NU_CERTIFICADO = COCO_COCE_NU_CERTIFICADO 
							AND COCM_NU_MOVIMIENTO = COCO_NU_MOVIMIENTO)
            INNER JOIN ALTERNA_PLANES PL ON(PL.ALPL_CD_RAMO = COCO_CARP_CD_RAMO
                            AND PL.ALPL_CD_PRODUCTO = COCO_CAPU_CD_PRODUCTO         
                            AND PL.ALPL_CD_PLAN = COCO_CAPB_CD_PLAN)                    
            LEFT JOIN COLECTIVOS_PARAMETROS ON(COPA_DES_PARAMETRO = 'PRIMAUNICA'
                            AND COPA_ID_PARAMETRO = 'IDVENTA'
                            AND COPA_NVALOR1 = COCM_CASU_CD_SUCURSAL
                            AND COPA_NVALOR2 = COCM_CARP_CD_RAMO
                            AND COPA_NVALOR4 = NVL(COCM_SUB_CAMPANA, 0))
        WHERE COCO_FE_CARGA BETWEEN TO_DATE('---fecha_ini---', 'dd/MM/yyyy') AND TO_DATE('---fecha_fin---', 'dd/MM/yyyy') 
            AND COCO_TP_COMISION = 1
    ) T 
GROUP BY T.COCO_CARP_CD_RAMO, T.POLIZA, T.FRPAGO, T.TIPO_PMA, T.ALPL_DATO3, T.COCO_VVALOR1, T.COCO_PO_TOTAL 
ORDER BY 1, 2, 4, 5