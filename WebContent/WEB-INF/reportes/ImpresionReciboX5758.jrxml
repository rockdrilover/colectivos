<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="report name" pageWidth="600" pageHeight="750" whenNoDataType="BlankPage" columnWidth="560" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="81"/>
	<subDataset name="dataset1"/>
	<parameter name="CANAL" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{CANAL}]]></defaultValueExpression>
	</parameter>
	<parameter name="RAMO" class="java.lang.String"/>
	<parameter name="POLIZA" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{POLIZA}]]></defaultValueExpression>
	</parameter>
	<parameter name="RECIBO" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{RECIBO}]]></defaultValueExpression>
	</parameter>
	<parameter name="RUTAIMG" class="java.lang.String"/>
	<parameter name="PU" class="java.lang.Short"/>
	<queryString>
		<![CDATA[SELECT a.CARE_CARP_CD_RAMO  ramo,
       nvl(j.caem_serie||j.caem_foliocruzado,'NO DIGITAL')       nu_recibo,
       a.CARE_CAPO_NU_POLIZA    poliza,
       nvl(h.copa_vvalor2,k.carp_de_ramo) ds_ramo,
       nvl(h.copa_vvalor6,'')             ds_ramocol,
       a.care_fe_status         fe_hoy,
       trunc(sysdate)           fe_imp ,
       trunc(a.CARE_FE_DESDE)   desde,
       trunc(a.CARE_FE_HASTA)   hasta,
       a.CARE_FE_HASTA          hasta2,
       trunc(i.COCE_FE_DESDE)   desde_pol,
       trunc(i.COCE_FE_HASTA)   hasta_pol,
       a.CARE_MT_SUMA_ASEGURADA suma_pol,
       a.CARE_MT_PRIMA          prima,
       a.CARE_MT_PRIMA_PURA     pura,
       p.CAFP_DE_FR_PAGO        fr_pago,
       a.CARE_NU_CONSECUTIVO_CUOTA   cuota,
       p.CAFP_NU_PAGOS_ANO           nu_recibos,
       replace(b.CACN_NM_APELLIDO_RAZON,'/',' ') nombre,
       b.CACN_RFC      rfc,
       f.CAES_DE_ESTADO                    estado,
       b.CACN_CACI_CD_CIUDAD_COB           ciudad,
       b.CACN_DI_COBRO1                    direcc,
       'C.P. ' || b.CACN_ZN_POSTAL_COBRO   cp,
       b.cacn_cazp_poblac_cob              poblacion,
       b.cacn_cazp_colonia_cob             colonia,
       nvl(c.CARG_MT_COMPONENTE,0)         mt_RFI,
       nvl(c.CARG_TA_COMPONENTE*100,0) ||' %'  ta_RFI,
       nvl(d.CARG_MT_COMPONENTE,0)         mt_DPO,
       nvl(d.CARG_TA_COMPONENTE,0)         ta_DPO,
       nvl(e.CARG_MT_COMPONENTE,0)         mt_IVA,
       nvl(e.CARG_TA_COMPONENTE,0)         ta_IVA,
       j.caem_anioaprobacion||' '||j.caem_numaprobacion anionumaprob,
       j.caem_numcertificado               certif,
       j.caem_cadena                       cadena,
       j.caem_sello                        sello,
       h.copa_vvalor4                      rfc_seguros,
       h.copa_vvalor3                      rfc_imagen,
       h.copa_vvalor1                      razon_seguros,
       l.copa_vvalor8                      leyenda_digital
  FROM CART_RECIBOS             a,
       CART_CLIENTES            b,
       CART_RECIBOS_COMPONENTES c,
       CART_RECIBOS_COMPONENTES d,
       CART_RECIBOS_COMPONENTES e,
       CART_ESTADOS             f,
       CART_ZONAS_POSTALES      g,
       COLECTIVOS_PARAMETROS    h,
       COLECTIVOS_PARAMETROS    l,
       CART_FR_PAGOS            p,
       COLECTIVOS_CERTIFICADOS  i,
       CART_EMISIONCFDI         j,
       CART_RAMOS_POLIZAS       k
 WHERE a.CARE_CASU_CD_SUCURSAL  = $P{CANAL}
   and a.CARE_CARP_CD_RAMO      = $P{RAMO}
   and a.CARE_CAPO_NU_POLIZA    = $P{POLIZA}
   and (a.CARE_NU_RECIBO        = $P{RECIBO}
         OR a.CARE_NU_RECIBO_ANTERIOR = $P{RECIBO})
    --
   and b.CACN_CD_NACIONALIDAD       = a.CARE_CACN_CD_NACIONALIDAD
   and b.CACN_NU_CEDULA_RIF         = a.CARE_CACN_NU_CEDULA_RIF
    --
   and c.CARG_CASU_CD_SUCURSAL(+)   = a.CARE_CASU_CD_SUCURSAL
   and c.CARG_CARE_NU_RECIBO(+)     = a.CARE_NU_RECIBO
   and c.CARG_CAPP_CD_COMPONENTE(+) = 'RFI'
    --
   and d.CARG_CASU_CD_SUCURSAL(+)   = a.CARE_CASU_CD_SUCURSAL
   and d.CARG_CARE_NU_RECIBO(+)     = a.CARE_NU_RECIBO
   and d.CARG_CAPP_CD_COMPONENTE(+) = 'DPO'
    --
   and e.CARG_CASU_CD_SUCURSAL(+)   = a.CARE_CASU_CD_SUCURSAL
   and e.CARG_CARE_NU_RECIBO(+)     = a.CARE_NU_RECIBO
   and e.CARG_CAPP_CD_COMPONENTE(+) = 'IVA'
   --
   and f.CAES_CD_ESTADO(+)          = b.CACN_CAES_CD_ESTADO_COB
   and g.CAZP_CD_POSTAL(+)          = b.CACN_ZN_POSTAL_COBRO
   --
   and h.Copa_Des_Parametro         = 'RECIBO'
   and h.copa_id_parametro          = 'IMPRESION'
   and h.copa_nvalor1               = a.care_casu_cd_sucursal
   and h.copa_nvalor2               = a.care_carp_cd_ramo
   and h.copa_nvalor3               = CASE WHEN $P{PU} = 1 THEN
                                              to_number(substr(a.care_capo_nu_poliza,1,5))
                                           ELSE
                                              a.care_capo_nu_poliza
                                      END
   and a.care_fe_emision           >= h.copa_fvalor1
   and a.care_fe_emision           <= h.copa_fvalor2
   and k.CARP_CD_RAMO               = h.copa_nvalor4
   --
   and l.copa_des_parametro         = 'RECIBO'
   and l.copa_id_parametro          = 'LEYENDAS'
   and a.care_fe_emision            between l.copa_fvalor1 and l.copa_fvalor2
   --
   and i.COCE_CASU_CD_SUCURSAL      = a.CARE_CASU_CD_SUCURSAL
   and i.COCE_CARP_CD_RAMO          = a.CARE_CARP_CD_RAMO
   and i.COCE_CAPO_NU_POLIZA        = a.CARE_CAPO_NU_POLIZA
   and i.COCE_NU_CERTIFICADO        = 0
   --
   and p.CAFP_CD_FR_PAGO            = a.CARE_CAPO_CD_FR_PAGO
   --
   and j.caem_casu_cd_sucursal(+)  = a.care_casu_cd_sucursal
   and j.caem_carp_cd_ramo(+)      = a.care_carp_cd_ramo
   and j.caem_nu_poliza(+)         = a.care_capo_nu_poliza
   and j.caem_nu_recibo(+)         = $P{RECIBO}]]>
	</queryString>
	<field name="nu_recibo" class="java.lang.String"/>
	<field name="poliza" class="java.lang.Integer"/>
	<field name="ds_ramo" class="java.lang.String"/>
	<field name="ds_ramocol" class="java.lang.String"/>
	<field name="fe_hoy" class="java.util.Date"/>
	<field name="fe_imp" class="java.util.Date"/>
	<field name="desde" class="java.util.Date"/>
	<field name="hasta" class="java.util.Date"/>
	<field name="hasta2" class="java.util.Date"/>
	<field name="desde_pol" class="java.util.Date"/>
	<field name="hasta_pol" class="java.util.Date"/>
	<field name="suma_pol" class="java.lang.Double"/>
	<field name="prima" class="java.lang.Double"/>
	<field name="pura" class="java.lang.Double"/>
	<field name="fr_pago" class="java.lang.String"/>
	<field name="cuota" class="java.lang.String"/>
	<field name="nu_recibos" class="java.lang.Integer"/>
	<field name="nombre" class="java.lang.String"/>
	<field name="rfc" class="java.lang.String"/>
	<field name="estado" class="java.lang.String"/>
	<field name="ciudad" class="java.lang.String"/>
	<field name="direcc" class="java.lang.String"/>
	<field name="cp" class="java.lang.String"/>
	<field name="poblacion" class="java.lang.String"/>
	<field name="colonia" class="java.lang.String"/>
	<field name="mt_RFI" class="java.lang.Double"/>
	<field name="ta_RFI" class="java.lang.String"/>
	<field name="mt_DPO" class="java.lang.Double"/>
	<field name="ta_DPO" class="java.lang.Double"/>
	<field name="mt_IVA" class="java.lang.Double"/>
	<field name="ta_IVA" class="java.lang.Double"/>
	<field name="anionumaprob" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="certif" class="java.lang.String"/>
	<field name="cadena" class="java.lang.String"/>
	<field name="sello" class="java.lang.String"/>
	<field name="rfc_seguros" class="java.lang.String"/>
	<field name="razon_seguros" class="java.lang.String"/>
	<field name="rfc_imagen" class="java.lang.String"/>
	<field name="leyenda_digital" class="java.lang.String"/>
	<title>
		<band height="174">
			<rectangle radius="10">
				<reportElement x="155" y="79" width="262" height="15" forecolor="#333333" backcolor="#333333"/>
			</rectangle>
			<staticText>
				<reportElement x="428" y="80" width="46" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[R.F.C. ]]></text>
			</staticText>
			<textField>
				<reportElement x="474" y="80" width="86" height="15"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{rfc}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="158" y="97" width="59" height="13"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[NOMBRE :]]></text>
			</staticText>
			<staticText>
				<reportElement x="158" y="128" width="59" height="15"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[DOMICILIO :]]></text>
			</staticText>
			<textField>
				<reportElement x="217" y="97" width="198" height="31"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nombre}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="217" y="128" width="198" height="40"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{direcc} + ", " + $F{colonia} + ". " + $F{cp} + ". " + $F{poblacion} + ", " + $F{estado}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="428" y="113" width="132" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[  VIGENCIA DE POLIZA]]></text>
			</staticText>
			<staticText>
				<reportElement x="428" y="128" width="66" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[DESDE]]></text>
			</staticText>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="428" y="141" width="66" height="12"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{desde_pol}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="494" y="140" width="66" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{hasta_pol}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="157" y="37" width="38" height="15"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="9"/>
				</textElement>
				<text><![CDATA[ RAMO:]]></text>
			</staticText>
			<textField>
				<reportElement x="474" y="0" width="86" height="23" forecolor="#000099"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{nu_recibo}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="195" y="37" width="242" height="15"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ds_ramo}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="202" y="0" width="171" height="19"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="10" isBold="true" isUnderline="false"/>
				</textElement>
				<text><![CDATA[RECIBO DE PAGO DE PRIMAS]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="195" y="52" width="242" height="15"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ds_ramocol}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="415" y="0" width="59" height="23"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="11" isBold="true" isUnderline="true"/>
				</textElement>
				<text><![CDATA[FOLIO No.]]></text>
			</staticText>
			<textField pattern="&apos;México D.F.,  a  &apos; dd &apos; de &apos; MMMM &apos; de &apos;yyyy" isBlankWhenNull="false">
				<reportElement x="183" y="19" width="220" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fe_hoy}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="155" y="72" width="405" height="1" forecolor="#660000"/>
				<graphicElement>
					<pen lineWidth="2.0"/>
				</graphicElement>
			</line>
			<image scaleImage="RealHeight" hAlign="Center" vAlign="Middle">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="69" width="149" height="83"/>
				<imageExpression><![CDATA[$P{RUTAIMG} + "RFCs/" + $F{rfc_imagen}]]></imageExpression>
			</image>
			<staticText>
				<reportElement x="494" y="128" width="66" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[HASTA]]></text>
			</staticText>
			<image scaleImage="RealSize" hAlign="Center" vAlign="Middle">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="0" width="158" height="69"/>
				<imageExpression><![CDATA[$P{RUTAIMG} + "logo.gif"]]></imageExpression>
			</image>
			<staticText>
				<reportElement mode="Transparent" x="159" y="79" width="258" height="14" forecolor="#FFFFFF" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[CONTRATANTE]]></text>
			</staticText>
			<line>
				<reportElement x="203" y="17" width="168" height="1" forecolor="#660000"/>
			</line>
		</band>
	</title>
	<pageHeader>
		<band height="369">
			<rectangle radius="10">
				<reportElement x="2" y="276" width="166" height="15" forecolor="#333333" backcolor="#333333"/>
			</rectangle>
			<rectangle radius="10">
				<reportElement mode="Opaque" x="0" y="11" width="560" height="94" forecolor="#FFFFFF" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineWidth="2.0"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="10">
				<reportElement mode="Transparent" x="131" y="148" width="88" height="15" forecolor="#000066" backcolor="#333333"/>
				<graphicElement>
					<pen lineStyle="Dotted"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="10">
				<reportElement mode="Transparent" x="423" y="148" width="132" height="15" forecolor="#000066" backcolor="#333333"/>
				<graphicElement>
					<pen lineStyle="Dotted"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="10">
				<reportElement x="2" y="186" width="166" height="15" forecolor="#333333" backcolor="#333333"/>
			</rectangle>
			<textField>
				<reportElement mode="Opaque" x="2" y="27" width="107" height="17"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Calibri" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{poliza}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="109" y="12" width="236" height="15" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[PLAN CONTRATADO]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="345" y="12" width="94" height="15" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[PRIMA TOTAL ANUAL]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="439" y="12" width="117" height="15" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[ SUMA ASEGURADA]]></text>
			</staticText>
			<textField>
				<reportElement mode="Opaque" x="109" y="27" width="236" height="17"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ds_ramo}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement mode="Opaque" x="438" y="27" width="118" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{suma_pol}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="2" y="43" width="106" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Calibri" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[FORMA DE PAGO]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="109" y="43" width="107" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[MONEDA]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="216" y="43" width="101" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[ESTE RECIBO AMPARA]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="317" y="43" width="56" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[DESDE]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="373" y="43" width="64" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[HASTA]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="437" y="44" width="119" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[TASA DE FINANCIAMIENTO]]></text>
			</staticText>
			<textField>
				<reportElement mode="Opaque" x="2" y="60" width="106" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Calibri" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fr_pago}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="108" y="60" width="104" height="14"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[PESOS M.N]]></text>
			</staticText>
			<textField>
				<reportElement mode="Opaque" x="213" y="60" width="101" height="14"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cuota} + " / " + $F{nu_recibos}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement mode="Opaque" x="315" y="60" width="57" height="14"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{desde}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement mode="Opaque" x="373" y="60" width="64" height="14"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{hasta}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="439" y="61" width="117" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ta_RFI}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="2" y="74" width="106" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Calibri" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[PRIMA NETA]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="109" y="74" width="154" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[RECARGO POR PAGO FRACCIONADO]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="264" y="74" width="108" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[GASTOS DE EXPEDICION]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="373" y="74" width="64" height="17" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[IVA]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="437" y="75" width="119" height="15" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[TOTAL A PAGAR]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="4" y="90" width="104" height="15"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Calibri" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pura}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="109" y="91" width="154" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{mt_RFI}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="264" y="91" width="108" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{mt_DPO}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="373" y="91" width="64" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{mt_IVA}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="437" y="91" width="119" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{prima}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="147" width="128" height="16"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Calibri" size="8"/>
				</textElement>
				<text><![CDATA[Año y No. de Aprobación de Folios]]></text>
			</staticText>
			<textField>
				<reportElement x="131" y="147" width="87" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{anionumaprob}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="333" y="147" width="90" height="16"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Número de Certificado]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="423" y="147" width="132" height="16"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{certif}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="2" y="12" width="107" height="15" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Calibri" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[NUMERO DE POLIZA]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="-1" y="292" width="556" height="69"/>
				<textElement textAlignment="Justified">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{sello}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="1" y="202" width="554" height="74"/>
				<textElement>
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{cadena}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="3" y="27" width="553" height="1"/>
			</line>
			<line>
				<reportElement x="3" y="43" width="553" height="1"/>
			</line>
			<line>
				<reportElement x="3" y="60" width="554" height="1"/>
			</line>
			<line>
				<reportElement x="2" y="74" width="554" height="1"/>
			</line>
			<line>
				<reportElement x="2" y="90" width="555" height="1"/>
			</line>
			<line>
				<reportElement x="108" y="12" width="1" height="92"/>
			</line>
			<line>
				<reportElement x="437" y="12" width="1" height="92"/>
			</line>
			<line>
				<reportElement x="345" y="11" width="1" height="32"/>
			</line>
			<line>
				<reportElement x="212" y="44" width="1" height="30"/>
			</line>
			<line>
				<reportElement x="263" y="74" width="1" height="31"/>
			</line>
			<line>
				<reportElement x="372" y="74" width="1" height="31"/>
			</line>
			<line>
				<reportElement x="314" y="43" width="1" height="30"/>
			</line>
			<line>
				<reportElement x="7" y="126" width="134" height="1" forecolor="#660000"/>
			</line>
			<line>
				<reportElement x="415" y="125" width="134" height="1" forecolor="#660000"/>
			</line>
			<staticText>
				<reportElement mode="Transparent" x="7" y="275" width="161" height="16" forecolor="#FFFFFF" backcolor="#CCCCCC"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[SELLO DIGITAL]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="6" y="185" width="162" height="16" forecolor="#FFFFFF" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[CADENA ORIGINAL]]></text>
			</staticText>
			<rectangle radius="10">
				<reportElement mode="Transparent" x="2" y="11" width="555" height="94" forecolor="#000000" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineWidth="2.0"/>
				</graphicElement>
			</rectangle>
			<textField>
				<reportElement x="141" y="119" width="274" height="14"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{leyenda_digital}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<pageFooter>
		<band height="33">
			<textField>
				<reportElement x="0" y="8" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{razon_seguros} +", R.F.C. " + $F{rfc_seguros}  +  " Sevilla 40, Pisos 1,2,3 y 4 Col. Juarez, C.P 06600, Mexico D.F. Tel 51694300 en el D.F."]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
