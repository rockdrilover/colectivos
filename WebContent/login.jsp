<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="faces/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
<f:loadBundle basename="mx.com.santander.aseguradora.colectivos.view.bundle.Messages" var="msgs"/>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<head>
		<title><h:outputText value="#{msgs.login}"/> - <h:outputText value="#{msgs.sistema}"/> </title>
		<link rel="stylesheet"  type="text/css" href="css/estilos.css" />
		<link rel="stylesheet"  type="text/css" href="css/botones.css" />
		<script type="text/javascript" src="js/boxover.js"></script>
	</head>

	<body class="bodyLogin">
	<h:form id="frmLogin" prependId="false">
		<br><br>
		<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" bordercolor="#FFFFFF">
			<tr>
				<td width="15%"></td>
				
				<c:if test="${beanLogin.nOpcion == 0}">
				<td class="centroLogin" id="login">
					<table border="0" width="100%">
						<tr>
							<td width="50%">
								<table border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td><h:graphicImage value="/images/login.png"/></td>
									</tr>
									<tr>
										<td class="fondoImagenLogin">
											<div class="textoImagen1"><h:outputText value="#{msgs.sistemasubtitulo}"/></div>
											<div class="espacio_5">&nbsp;&nbsp;</div>
											<div class="textoImagen2"><h:outputText value="#{msgs.sistemadesc}"/></div>
										</td>
									</tr>
								</table>
							</td>
							<td width="5%">&nbsp;&nbsp;</td>
							<td width="40%">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">									
									<tr>
										<td width="20%">&nbsp;&nbsp;<h:graphicImage value="/images/logoNew.png" /></td>
										<td align="left"><div class="tituloLogin"><h:outputText value="#{msgs.sistema}"/></div></td>
									</tr>
									<tr class="espacio_15"><td colspan="2">&nbsp;&nbsp;</td></tr>

									<tr>
										<td colspan="2" class="texto_normal"><h:outputText value="#{msgs.loginusuario}"/>:</td>
									</tr>
									<tr>
										<td colspan="2">
											<h:inputText id="j_username" value="#{beanLogin.username}" size="10" styleClass="estilotextLogin" 
												title="header=[Usuario] body=[Ingresa tu nombre de usuario sin dejar espacios.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]"  />					                       			    		                    	
			    		                </td>
			    		            </tr>
			    		            <tr class="espacio_10"><td colspan="2">&nbsp;&nbsp;</td></tr>

									<tr>
										<td colspan="2" class="texto_normal"><h:outputText value="#{msgs.loginpassword}"/>:</td>
									</tr>
									<tr>
										<td colspan="2">
											<h:inputSecret id="j_password" value="#{beanLogin.password}" styleClass="estilotextLogin"
												title="header=[Contrase�a] body=[Ingresa tu contrase�a para iniciar sesi�n.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />					                        
					                    </td>
					                </tr>
					                <tr class="espacio_20"><td colspan="2">&nbsp;&nbsp;</td></tr>
					                <tr class="espacio_20"><td colspan="2">&nbsp;&nbsp;</td></tr>
					                
					                <tr>
					                	<td colspan="2">
					                	<table width="90%" border="0">
					                		<tr> 
					                			<td width="55%">
							                		<h:commandLink styleClass="button large red" id="btnLogin" action="#{beanLogin.login}" type="submit">
														<span><h:graphicImage value="/images/imgBtnLogin.png" />&nbsp;
																<h:outputText value="#{msgs.botonlogin}"/>&nbsp;&nbsp;&nbsp;</span>
													</h:commandLink>											
							                	</td>
							                	<td width="45%" align="left">
							                		&nbsp;&nbsp;
							                		<h:commandLink id="btnCambio" action="#{beanLogin.verPantalla}" type="submit">
														<h:outputText value="Cambiar Contrase�a"/>&nbsp;&nbsp;&nbsp;
													</h:commandLink>											
							                	</td>
					                		</tr>
					                	</table>
					                	</td>
					                </tr>
					                <tr class="espacio_20"><td colspan="2">&nbsp;&nbsp;</td></tr>
					                <tr class="espacio_20"><td colspan="2">&nbsp;&nbsp;</td></tr>
					                
					                <tr><td colspan="2" align="left"><h:outputText id="mensaje" value="#{beanLogin.respuesta}" styleClass="errorMessage"/></td></tr>
								</table>
							</td>
						</tr>
						
						<tr class="espacio_20"><td colspan="3">&nbsp;&nbsp;</td></tr>
						<tr class="espacio_20"><td colspan="3">&nbsp;&nbsp;</td></tr>
						<tr><td colspan="3"><hr>&nbsp;Version: Febrero-2023</td></tr>
					</table>													
				</td>
				</c:if>
				
				<c:if test="${beanLogin.nOpcion == 1}">
				<td  class="centroLogin" id="cambio" align="center">
					<table width="50%" border="0">
						<tr>
							<td>
								<fieldset>
									<legend>
										<h:graphicImage value="/images/imgCambioPass.png"/>&nbsp;&nbsp;&nbsp;
										<span class="titulos_secciones"><h:outputText value="#{msgs.cambiopassword}"/></span>
									</legend>
															
									<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">	
										<tr><td class="texto_normal"><h:outputText value="#{msgs.loginusuario}"/>:</td></tr>
										<tr>
											<td>
												<h:inputText value="#{beanLogin.usuario.username}" styleClass="estilotextLogin" disabled="true"/>					                       	
				    		                </td>
				    		            </tr>
				    		            <tr class="espacio_5"><td>&nbsp;&nbsp;</td></tr>
				    		            
										<tr><td class="texto_normal"><h:outputText value="#{msgs.loginnombre}"/>:</td></tr>
										<tr>
											<td>
												<h:inputText value="#{beanLogin.usuario.nombre}" styleClass="estilotextLogin" disabled="true"/>&nbsp;
												<h:inputText value="#{beanLogin.usuario.apellidoPaterno}" styleClass="estilotextLogin" disabled="true"/>&nbsp;
												<h:inputText value="#{beanLogin.usuario.apellidoMaterno}" styleClass="estilotextLogin" disabled="true"/>						                       	
				    		                </td>
				    		            </tr>
				    		            <tr class="espacio_5"><td>&nbsp;&nbsp;</td></tr>
										
				    		            <tr><td class="texto_normal"><h:outputText value="#{msgs.loginpassword}"/>:</td></tr>
										<tr>
											<td>
												<h:inputSecret id="j_newpassword" value="#{beanLogin.passwordnew}" styleClass="estilotextLogin"
													title="header=[Contrase�a] body=[Ingresa tu nueva contrase�a.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
								               	<h:message for="j_newpassword" styleClass="error"/>						                       	
				    		                </td>
				    		            </tr>
				    		            <tr class="espacio_5"><td>&nbsp;&nbsp;</td></tr>
				    		            
			                    		<tr><td class="texto_normal"><h:outputText value="#{msgs.loginpasswordrepite}"/>:</td></tr>
										<tr>
											<td>
												<h:inputSecret id="j_newpasswordrep" value="#{beanLogin.passwordrep}" styleClass="estilotextLogin"
													title="header=[Contrase�a] body=[Repite tu nueva contrase�a.] cssheader=[tooltipH] cssbody=[tooltipB] fade=[on]" />
								               	<h:message for="j_newpasswordrep" styleClass="error"/>						                       	
				    		                </td>
				    		            </tr>
				    		            <tr class="espacio_20"><td>&nbsp;&nbsp;</td></tr>
										
										<tr><td>
					                		<table width="90%" border="0">
					                			<tr> 
					                				<td width="50">
								                		<h:commandLink styleClass="button large red" id="btnCambiar" action="#{beanLogin.resetPass}" type="submit">
															<span><h:graphicImage value="/images/imgBtnAceptar.png" />&nbsp;
																	<h:outputText value="#{msgs.botonguardar}"/>&nbsp;&nbsp;&nbsp;</span>
														</h:commandLink>											
								                	</td>
								                	<td width="50%">
								                		<h:commandLink styleClass="button large red" id="btnCancelar" action="#{beanLogin.cancelar}" type="submit">
															<span><h:graphicImage value="/images/imgBtnCancelar.png" />&nbsp;
																	<h:outputText value="#{msgs.botoncancelar}"/>&nbsp;&nbsp;&nbsp;</span>
														</h:commandLink>										
								                	</td>
						                		</tr>
						                	</table>
					                	</td></tr>
					                	<tr class="espacio_20"><td>&nbsp;&nbsp;</td></tr>
					                	<tr class="espacio_20"><td>&nbsp;&nbsp;</td></tr>
					                	
					                	<tr><td align="left"><h:outputText id="mensaje" value="#{beanLogin.respuesta}" styleClass="errorMessage"/></td></tr>
			                    	</table>
			                    </fieldset>
							</td>
						</tr>
						
						<tr class="espacio_20"><td>&nbsp;&nbsp;</td></tr>
						<tr><td><hr>&nbsp;</td></tr>
					</table>
				</td>
				</c:if>
				
				<td width="15%"></td>
			</tr>
   		</table>
	</h:form>
	</body>
</html>
</f:view>