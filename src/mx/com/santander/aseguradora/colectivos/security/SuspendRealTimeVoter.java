/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.Authentication;
import org.springframework.security.ConfigAttribute;
import org.springframework.security.ConfigAttributeDefinition;
import org.springframework.security.vote.AccessDecisionVoter;

/**
 * @author Sergio Plata
 *
 */
public class SuspendRealTimeVoter implements AccessDecisionVoter{

	private Set<String> revokedUsers = new HashSet<String>();
	
	public boolean supports(ConfigAttribute attribute) {
		// TODO Auto-generated method stub
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean supports(Class clazz) {
		// TODO Auto-generated method stub
		return true;
	}

	public int vote(Authentication authentication, Object object, ConfigAttributeDefinition config){
		// TODO Auto-generated method stub
		String username = authentication.getName();
		
		return revokedUsers.contains(username)? ACCESS_DENIED: ACCESS_GRANTED;
	}
	
	/**
	 * 
	 * @param username
	 */
	public void suspend(String username)
	{
		revokedUsers.add(username);
	}
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public Boolean isSuspended(String user)
	{
		return revokedUsers.contains(user);
	}

	/**
	 * 
	 * @param user
	 */
	public void grant(String user)
	{
		revokedUsers.remove(user);
	}
}
