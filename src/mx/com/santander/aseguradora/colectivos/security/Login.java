/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.security;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

import org.springframework.security.BadCredentialsException;
import org.springframework.security.ui.AbstractProcessingFilter;

/**
 * @author Sergio
 *
 */
public class Login implements PhaseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -140330636438522507L;

	/* (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
	 */
	public void afterPhase(PhaseEvent event) {
		// TODO Auto-generated method stub

		Exception e = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(
					   AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY);
		
		
		if(e instanceof BadCredentialsException){
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(
					   AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY, null);
			
			FacesUtils.addErrorMessage("Nombre de usuario o password incorrecto.");
		}	
		
	}

	/* (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
	 */
	public void beforePhase(PhaseEvent event) {
		// TODO Auto-generated method stub
		
		
	}

	/* (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#getPhaseId()
	 */
	public PhaseId getPhaseId() {
		// TODO Auto-generated method stub
		return PhaseId.RENDER_RESPONSE;
	}

}
