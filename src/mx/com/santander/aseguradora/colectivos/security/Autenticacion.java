/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.security.providers.AuthenticationProvider;
import org.springframework.security.providers.ProviderManager;

/**
 * @author Sergio Plata
 *
 */
public class Autenticacion extends ProviderManager {

	protected String provider;
	private ApplicationContext context;

	
	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}
	/**
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Autenticacion() {
		// TODO Auto-generated constructor stub
		this.context = new FileSystemXmlApplicationContext("applicationContext.xml");
	}
	public void propertiesSet()throws Exception{
		
		if(this.provider != null){
			
			List<String> providers = new ArrayList<String>();
			String[] names = provider.split(",");
			
			for(String providerUnit: names){
				
				AuthenticationProvider provider = (AuthenticationProvider)context.getBean(providerUnit.trim());

				if(provider == null){
					throw new Exception("Authentication " + providerUnit + " does not exist.");
				}
				
				providers.add(providerUnit);
			}
			
			super.setProviders(providers);
		}
		super.afterPropertiesSet();
		
	}
}
