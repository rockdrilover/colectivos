/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;


import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;




import mx.com.santander.aseguradora.colectivos.model.dao.AltaTarifaCoberturaDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


/**
 * @author dflores
 *
 */
@SuppressWarnings("unchecked")

public class AltaTarifaCoberturaDaoHibernate extends HibernateDaoSupport implements AltaTarifaCoberturaDao {

	
	/**
	 * 
	 * @author dflores
	 *
	 */
	private class IniciaAlta extends StoredProcedure{

		public IniciaAlta(DataSource ds) {
			// TODO Auto-generated constructor stub
			super.setDataSource(ds);
			super.setSql(Colectivos.PROCESO_ALTATARIFACOBERTURA);
			super.declareParameter(new SqlOutParameter("V_TAR", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", OracleTypes.NUMBER));
			super.declareParameter(new SqlParameter("paramRamoCont", OracleTypes.NUMBER));
			super.declareParameter(new SqlParameter("paramCdCobertura", OracleTypes.VARCHAR));
			super.declareParameter(new SqlParameter("paramCdProducto", OracleTypes.NUMBER));
			super.declareParameter(new SqlParameter("paramCdPlan", OracleTypes.NUMBER));		
			super.declareParameter(new SqlParameter("paramFeIni", OracleTypes.DATE));
			super.declareParameter(new SqlParameter("paramFeFin", OracleTypes.DATE));
			super.declareParameter(new SqlParameter("paramMonto", OracleTypes.NUMBER));
			super.declareParameter(new SqlParameter("paramComision", OracleTypes.NUMBER));
			super.declareParameter(new SqlParameter("paramTaRiesgo",OracleTypes.NUMBER));
			super.declareParameter(new SqlParameter("paramEdadMin", OracleTypes.NUMBER));
			super.declareParameter(new SqlParameter("paramEdadMax", OracleTypes.NUMBER));
			
			super.compile();
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.StoredProcedure#execute(java.util.Map)
		 */
		

		public Map execute( Integer canal, Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, 
				Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, 
				Float taRiesgo, Integer edadMin, Integer edadMax)
			// TODO Auto-generated method stub
			
			throws DataAccessException {
				// TODO Auto-generated method stub
				Map<String, Object> inParams = new HashMap<String, Object>();
				
				inParams.put("paramCanal", canal);
				inParams.put("paramRamo", ramo);
				inParams.put("paramPoliza", poliza);
				inParams.put("paramRamoCont", ramoCont);
				inParams.put("paramCdCobertura", cdCobertura);
				inParams.put("paramCdProducto", cdProducto);
				inParams.put("paramCdPlan", cdPlan);
				inParams.put("paramFeIni", fechIni);
				inParams.put("paramFeFin", fechFin);
				inParams.put("paramMonto", monto);
				inParams.put("paramComision", comision);
				inParams.put("paramTaRiesgo",taRiesgo);
				inParams.put("paramEdadMin",edadMin);
				inParams.put("paramEdadMax",edadMax);
			    return super.execute(inParams);
				
			}

			
		
	}


	public List getListaAltaTarifaCober() {
		// TODO Auto-generated method stub
		return null;
	}



	public void saveListaAltaTarifaCober(List lista)  {
		// TODO Auto-generated method stub
		
	}


	public Object saveAltaTarifaCober(Object altaTarifaCober)  {
		// TODO Auto-generated method stub
		return null;
	}


	public List getListaAltaTarifaCober(Integer canal, Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, 
			Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, 
			Float taRiesgo, Integer edadMin, Integer edadMax) {
		// TODO Auto-generated method stub
		return null;
	}


	public Integer iniciaAlta( Integer canal, Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, 
			Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, 
			Float taRiesgo, Integer edadMin, Integer edadMax) {
		// TODO Auto-generated method stub
		 Integer pol = 0;
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			IniciaAlta alta = new IniciaAlta(ds);
			
			pol = (Integer) ((Map)alta.execute(  canal, ramo,  poliza, ramoCont,  cdCobertura, 
					 cdProducto,  cdPlan , fechIni,  fechFin,  monto,  comision, 
					 taRiesgo,  edadMin,  edadMax)).get("V_TAR");
			
			return pol;
	}


 public Integer iniciaAltaTarifaCober ( Integer canal, Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, 
			Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, 
			Float taRiesgo, Integer edadMin, Integer edadMax) {
		// TODO Auto-generated method stub
		 Integer pol = 0;
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			IniciaAlta alta = new IniciaAlta(ds);
			
			pol = (Integer) ((Map)alta.execute(  canal, ramo,  poliza, ramoCont,  cdCobertura, 
					 cdProducto,  cdPlan , fechIni,  fechFin,  monto,  comision, 
					 taRiesgo,  edadMin,  edadMax)).get("V_TAR");
			
			return pol;
 }		

}
