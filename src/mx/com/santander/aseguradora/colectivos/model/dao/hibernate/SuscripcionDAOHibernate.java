package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSuscripcion;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Grupo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.SuscripcionDAO;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author CJPV - vectormx
 * @version 07-09-2017
 */
@Repository
public class SuscripcionDAOHibernate extends HibernateDaoSupport implements SuscripcionDAO {

	/**
	 * Constructor de sesion hibernate.
	 * 
	 * @param sessionFactory
	 *            Session factory hibernate
	 */
	@Autowired
	public SuscripcionDAOHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = { "ObjetoDuplicado", "Excepciones" })
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		try {
			T producto = (T) this.getHibernateTemplate().get(objeto, id);
			return producto;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Override
	public ColectivosSuscripcion consultaObjeto(long id) throws Excepciones {
		try {
			ColectivosSuscripcion res = this.getHibernateTemplate().get(ColectivosSuscripcion.class, id);
			return res;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().update(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);

		}

	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.SuscripcionDAO#
	 * obtieneSuscripcionFolioRiesgos (mx.com.santander.aseguradora.colectivos.model
	 * .bussinessObject.Suscripcion)
	 */
	@Override
	public ColectivosSuscripcion obtieneSuscripcionFolioRiesgos(ColectivosSuscripcion filtro) throws Excepciones {
		StringBuilder consulta = new StringBuilder();
		ColectivosSuscripcion res = null;
		logger.info("Folio Riesgos" + filtro.getCosuFolioCh());

		consulta.append("from ColectivosSuscripcion sus ");
		consulta.append("where sus.cosuFolioCh = :pFolioCH ");
		consulta.append(" and sus.cosuParticipacion = :pcosuParticipacion ");
		consulta.append(" and sus.cosuNumInter = :pcosuNumInter ");
		

		try {
			Query qry = this.getSession().createQuery(consulta.toString());
			qry.setParameter("pFolioCH", filtro.getCosuFolioCh());
			qry.setParameter("pcosuParticipacion", filtro.getCosuParticipacion());
			qry.setParameter("pcosuNumInter", filtro.getCosuNumInter());

			res = (ColectivosSuscripcion) qry.uniqueResult();
		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error consulta suscripcion por folio riesgo", e);
		}
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.santander.aseguradora.colectivos.model.dao.SuscripcionDAO#gruposTareas
	 * ()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Grupo> gruposTareas() throws Excepciones {
		StringBuilder consulta = new StringBuilder();
		List<Grupo> res;
		consulta.append("from Parametros p ");
		consulta.append("where p.copaDesParametro = 'SUSCRIPCION' ");
		consulta.append("and p.copaIdParametro = 'AREAS_TAREAS' ");

		try {
			Query qry = this.getSession().createQuery(consulta.toString());

			Parametros param = (Parametros) qry.uniqueResult();
			consulta = new StringBuilder();
			consulta.append("from Grupo g ");
			consulta.append("where g.id in (");
			consulta.append(param.getCopaVvalor1());
			consulta.append(")");
			qry = this.getSession().createQuery(consulta.toString());

			res = (List<Grupo>) qry.list();
			return res;
		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error consulta suscripcion por folio riesgo", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.SuscripcionDAO#
	 * archivoParametros()
	 */
	@Override
	public Parametros archivoParametros() throws Excepciones {
		StringBuilder consulta = new StringBuilder();
		consulta.append("from Parametros p ");
		consulta.append("where p.copaDesParametro = 'SUSCRIPCION' ");
		consulta.append("and p.copaIdParametro = 'AREAS_TAREAS' ");

		Query qry = this.getSession().createQuery(consulta.toString());

		Parametros param = (Parametros) qry.uniqueResult();
		return param;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.SuscripcionDAO#
	 * consultaSuscripciones
	 * (mx.com.santander.aseguradora.colectivos.model.bussinessObject .Suscripcion)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ColectivosSuscripcion> consultaSuscripciones(ColectivosSuscripcion filtro) throws Excepciones {
		/*
		 * return this.getHibernateTemplate().executeFind(new HibernateCallback(){
		 * 
		 * public Object doInHibernate(Session sesion) throws HibernateException,
		 * SQLException { // TODO Auto-generated method stub
		 * 
		 * StringBuffer consulta = new StringBuffer(150);
		 * consulta.append("from Parametros P \n"); consulta.append("where 1=1 \n");
		 * consulta.append(filtro);
		 * 
		 * Query qry = sesion.createQuery(consulta.toString());
		 * 
		 * 
		 * List<T> lista = qry.list();
		 * 
		 * return lista; }
		 * 
		 * });
		 */

		List<ColectivosSuscripcion> res = null;

		StringBuilder consulta = new StringBuilder();

		consulta.append("from ColectivosSuscripcion  ");
		consulta.append(" where 1 = 1 ");

		if (!"".equals(filtro.getCosuDictamen())) {
			consulta.append(" and cosuDictamen = :pcosuDictamen ");
			// consulta.append(filtro.getCosuDictamen());
			// consulta.append("' ");
		}

		if (filtro.getCosuFolioCh() != null && filtro.getCosuFolioCh().intValue() != 0) {
			consulta.append(" and cosuFolioCh = :pcosuFolioCh");
			// consulta.append(filtro.getCosuFolioCh());
		}

		if (filtro.getCosuFechaSolCred() != null) {
			consulta.append(" and cosuFechaSolCred = :pcosuFechaSolCred");
			// consulta.append(filtro.getCosuFechaSolCred());
			// consulta.append("' ");

		}

		if (filtro.getCosuFechaCarga() != null) {
			consulta.append(" and cosuFechaCarga = :pcosuFechaCarga");
			// consulta.append(filtro.getCosuFechaCarga());
			// consulta.append("' ");

		}

		consulta.append(" order by cosuFechaCarga ");

		logger.info(consulta.toString());

		try {
			Query qry = this.getSession().createQuery(consulta.toString());
			if (!"".equals(filtro.getCosuDictamen())) {
				qry.setParameter("pcosuDictamen", filtro.getCosuDictamen());

			}

			if (filtro.getCosuFolioCh() != null && filtro.getCosuFolioCh().intValue() != 0) {
				qry.setParameter("pcosuFolioCh", filtro.getCosuFolioCh());
				// consulta.append(filtro.getCosuFolioCh());
			}

			if (filtro.getCosuFechaSolCred() != null) {
				qry.setParameter("pcosuFechaSolCred", filtro.getCosuFechaSolCred());
				// consulta.append(filtro.getCosuFechaSolCred());
				// consulta.append("' ");

			}

			if (filtro.getCosuFechaCarga() != null) {
				qry.setParameter("pcosuFechaCarga", filtro.getCosuFechaCarga());
				// consulta.append(filtro.getCosuFechaCarga());
				// consulta.append("' ");

			}
			res = (List<ColectivosSuscripcion>) qry.list();

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new Excepciones("Error consulta suscripcion por folio riesgo", e);
		}

		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.SuscripcionDAO#
	 * consecutivoSeguimiento(mx.com.santander.aseguradora.colectivos.model.
	 * bussinessObject.ColectivosSuscripcion)
	 */
	@Override
	public long tareaAtendida(ColectivosSuscripcion filtro) throws Excepciones {

		StringBuilder sbConsulta = new StringBuilder();
		Query qry;

		sbConsulta.append("SELECT COUNT(COSE_COSU_ID)  \n");
		sbConsulta.append("	FROM COLECTIVOS_SEGUIMIENTO SEG \n");
		sbConsulta.append("WHERE COSE_COSU_ID 	= :cosuId \n");
		sbConsulta.append("and COSE_ESTATUS = 'P'  \n");

		try {
			qry = this.getSession().createSQLQuery(sbConsulta.toString());
			qry.setParameter("cosuId", filtro.getCosuId());

			BigDecimal res = (BigDecimal) qry.uniqueResult();
			return res.longValue();
		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error consulta consecutivo seguimiento", e);
		}
	}

}
