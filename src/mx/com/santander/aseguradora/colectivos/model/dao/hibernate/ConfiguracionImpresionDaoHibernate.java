package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.model.dao.ConfiguracionImpresionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

@Repository
public class ConfiguracionImpresionDaoHibernate extends HibernateDaoSupport implements ConfiguracionImpresionDao{
	
	@Autowired
	public ConfiguracionImpresionDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Override
	public <T> List<T> getCamposBase()throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select * ");
			sb.append("from colectivos_parametros c ");
			sb.append("where c.copa_des_parametro = 'IMPRESION_QUERY_DINAMICO' ");
			sb.append("order by c.copa_cd_parametro ");
	
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}

	@Override
	public String insertConfigImpresion(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto,Integer nPlan, String formulario, String base) throws Exception {
		String carga = "";
		
		try {
			Query qry;
			StringBuilder consulta = new StringBuilder(); 
			consulta.append("insert into colectivos_conf_impresion(" );
			consulta.append("coim_canal, ");
			consulta.append("coim_ramo, ");
			consulta.append("coim_poliza, ");
			consulta.append("coim_idVenta, ");
			consulta.append("coim_producto, ");
			consulta.append("coim_plan, ");
			consulta.append("coim_campo_formulario, ");
			consulta.append("coim_campo_base) ");
			consulta.append("values( "); 
			consulta.append(canal).append(", "); 
			consulta.append(ramo).append(", ");
			consulta.append(poliza).append(", ");
			consulta.append(idVenta).append(", ");
			consulta.append(nProducto).append(", ");
			consulta.append(nPlan).append(", ");
			consulta.append("'").append(formulario).append("', ");
			consulta.append("'").append(base).append("') ");
			
			qry = getSession().createSQLQuery(consulta.toString());
			
			qry.executeUpdate();
			carga = "EXITO";			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return carga;
	}
	
	@Override
	public <T> List<T> consultaPlantilla(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto,Integer nPlan)throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select c.coim_campo_formulario, p.copa_Vvalor7, c.coim_campo_base ");
			sb.append("from COLECTIVOS_CONF_IMPRESION c, colectivos_parametros p ");
			sb.append("where c.coim_canal =  ").append(canal).append(" ");
			sb.append("and c.coim_ramo = ").append(ramo).append(" ");
			sb.append("and c.coim_poliza = ").append(poliza).append(" ");
			sb.append("and c.coim_idventa = ").append(idVenta).append(" ");
			sb.append("and c.coim_producto = ").append(nProducto).append(" ");
			sb.append("and c.coim_plan = ").append(nPlan).append(" ");
			sb.append("and p.copa_des_parametro = 'IMPRESION_QUERY_DINAMICO' ");
			sb.append("and p.copa_id_parametro = c.coim_campo_base ");
			sb.append("order by coim_campo_base asc");
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	@Override
	public String actualizaDatos(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto,Integer nPlan, String formulario, String base, String baseAnte) throws Exception {
	       String proceso="";
	       StringBuffer actualiza = new StringBuffer();
	       try {
	    	   
	           actualiza.append("UPDATE COLECTIVOS_CONF_IMPRESION c  \n");
	           actualiza.append("SET c.coim_campo_formulario = '" + formulario + "', ");
	           actualiza.append("c.coim_campo_base = '" + base + "' ");
	           actualiza.append("where c.coim_canal =  ").append(canal).append(" ");
	           actualiza.append("and c.coim_ramo = ").append(ramo).append(" ");
	           actualiza.append("and c.coim_poliza = ").append(poliza).append(" ");
	           actualiza.append("and c.coim_idventa = ").append(idVenta).append(" ");
	           actualiza.append("and c.coim_producto = ").append(nProducto).append(" ");
	           actualiza.append("and c.coim_plan = ").append(nPlan).append(" ");
	           actualiza.append("and c.coim_campo_base = '").append(baseAnte).append("' ");
	           
	           Query queryAct = getSession().createSQLQuery(actualiza.toString());
	           queryAct.executeUpdate();
	       } catch (Exception e) {
	    	   proceso="Error al modificar";
	           e.printStackTrace();
	       }
	   return proceso;
	   }

	@Override
	public String guardaPlantilla(Long next, String idParam, String desParam, int canal, Short ramo, Integer poliza, Integer nProducto, Integer nPlan, String nombreArchivo) throws Exception {
		String carga = "";
		
		try {
			Session s;
			Query qry;
			StringBuilder consulta = new StringBuilder(); 
			consulta.append("insert into colectivos_parametros(" );
			consulta.append("COPA_CD_PARAMETRO, ");
			consulta.append("COPA_ID_PARAMETRO, ");
			consulta.append("COPA_DES_PARAMETRO, ");
			consulta.append("COPA_NVALOR1, ");
			consulta.append("COPA_NVALOR2, ");
			consulta.append("COPA_NVALOR3, ");
			consulta.append("COPA_VVALOR1, ");
			consulta.append("COPA_VVALOR2, ");
			consulta.append("COPA_FVALOR1, ");
			consulta.append("COPA_FVALOR2, ");
			consulta.append("COPA_VVALOR8) ");
			consulta.append("values( :cd, :id, :des, :canal, :ramo, :poliza, :prod, :plan, to_date(:fedes,'dd/MM/yyyy'), to_date(:fehas,'dd/MM/yyyy'), :nombre)"); 
			
			qry = getSession().createSQLQuery(consulta.toString());
			qry.setLong("cd", next);
			qry.setParameter("id", idParam);
			qry.setParameter("des", desParam);
			qry.setInteger("canal", canal);
			qry.setShort("ramo", ramo);
			qry.setInteger("poliza", poliza);
			qry.setParameter("prod", nProducto);
			qry.setParameter("plan", nPlan);
			qry.setParameter("fedes", "01/01/2000");
			qry.setParameter("fehas", "31/12/2060");
			qry.setParameter("nombre", nombreArchivo);
			
			qry.executeUpdate();
			carga = "EXITO";			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return carga;
	}

	@Override
	public String getPlazo(Short ramo, Integer nProducto, Integer nPlan) throws Exception {
		String plazo = "";
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select c.alpl_dato1 \n");
			sb.append("from alterna_planes c \n");
			sb.append("where c.alpl_cd_ramo = ").append(ramo).append(" \n");
			sb.append("and c.alpl_cd_producto = ").append(nProducto).append(" \n");
			sb.append("and c.alpl_cd_plan = ").append(nPlan).append(" \n");
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			
			plazo = query.list().get(0).toString();
			
			return plazo;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}

	@Override
	public String existe(String filtro) throws Exception {
		String a;
    	StringBuilder query = null;
    	List<Object> lista = null;
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<Object>();
    		
    		query.append(" select CP.COPA_VVALOR8 \n");
    		query.append(" from colectivos_parametros cp   \n");
    		query.append(filtro);
    		
    		lista = getSession().createSQLQuery(query.toString()).list();
    		if(!lista.isEmpty()){
    			a = lista.get(0).toString();
    		} else {
    			a = "0";
    		}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error al recuperar existe en colectivos_parametros", e);
		}
    	return a;
	}
	
	/**
	 * Metodo que se usa para editar plantilla
	 * @param plantilla nombre de plantilla
	 * @param filtro filtro a ejeccutar
	 * @return bandera de ejecucion
	 * @throws Exception con errores en general
	 */
	public Boolean editarNombrePlantilla(String plantilla, Map<String, String> filtro) throws Exception {
    	StringBuilder query =new StringBuilder(String.format("UPDATE COLECTIVOS_PARAMETROS SET COPA_VVALOR8 = '%s' WHERE ", plantilla));
    	try {
    		Iterator<Map.Entry<String, String>> filtros = filtro.entrySet().iterator();
    		Map.Entry<String, String> fltr = filtros.next();
    		query.append(fltr.getKey()).append(String.format("= '%s' ", fltr.getValue()));
    		while (filtros.hasNext()) {
    			fltr = filtros.next();
        		query.append("AND ").append(fltr.getKey()).append(String.format("= '%s' ", fltr.getValue()));
    		}
    		return getSession().createSQLQuery(query.toString()).executeUpdate() == 1;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("0Error al modificar la plantilla -> " + plantilla, e);
		}
	}
	
	/**
	 * Metodo que borra una plantilla
	 * @param plantilla nombre de plantilla
	 * @param hmParams filtro a ejecutar
	 * @return bandera de ejecuion
	 * @throws Exception con errores en general
	 */
	public Boolean borrarPlantilla(String plantilla, Map<String, String> hmParams) throws Exception {
		StringBuilder strQuery = new StringBuilder();
    	
		try {
			hmParams.put("COPA_VVALOR8", plantilla);
			
			strQuery.append("DELETE FROM COLECTIVOS_PARAMETROS \n");
			strQuery.append(" WHERE COPA_DES_PARAMETRO = :COPA_DES_PARAMETRO \n");
			strQuery.append(" AND COPA_ID_PARAMETRO = :COPA_ID_PARAMETRO \n");
			strQuery.append(" AND COPA_NVALOR1 = :COPA_NVALOR1 \n");
			strQuery.append(" AND COPA_NVALOR2 = :COPA_NVALOR2 \n");
			strQuery.append(" AND COPA_NVALOR3 = :COPA_NVALOR3 \n");
			strQuery.append(" AND COPA_VVALOR1 = :COPA_VVALOR1 \n");
			strQuery.append(" AND COPA_VVALOR2 = :COPA_VVALOR2 \n");
			strQuery.append(" AND COPA_VVALOR8 = :COPA_VVALOR8 \n");
			
			Query query = getSession().createSQLQuery(strQuery.toString());
			
			for(Map.Entry<String, String> entry : hmParams.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
    		
    		return query.executeUpdate() == 1;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("0Error al borrar la plantilla -> " + plantilla, e);
		}
	}
}
