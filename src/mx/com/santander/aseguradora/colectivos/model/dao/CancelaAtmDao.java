package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCancelaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Interface que implementa la clase CancelaAtmDaoHibernate.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Local
public interface CancelaAtmDao extends Serializable {
	
	/**
	 * Metodo que consulta el resumen de los creditos a cancelar
	 * @param nombreLayOut nombre de archivo
	 * @param feDesde fecha desde
	 * @param feHasta dehca hasta 
	 * @return lista de datos 
	 * @throws Excepciones con error en general
	 */
	List<Object> getResumenCancela(String nombreLayOut, String feDesde, String feHasta) throws Excepciones;

	/**
	 * Metodo que actualiza creditos a cancelar
	 * @param seleccionado datos para hacer update
	 * @param lista creditos a cancelar
	 * @throws Excepciones con error general
	 */
	void cancelaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones;

	/**
	 * Metodo que elimina creditos por errores o que no se quieren cancelar
	 * @param seleccionado datos para hacer delete
	 * @param lista creditos a eliminar
	 * @throws Excepciones con error general
	 */
	void eliminaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones;

}
