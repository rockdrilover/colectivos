package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author dflores
 *
 */

public interface ObligadoDao extends Serializable {
	
	public abstract void guardaObligado(Long cdParametro,
			String desParametro, String idParametro, Integer inProducto,
			String inPlan, String inPeriodicidad, Short inCanal, Short inRamo,
			Integer inPoliza, Short inNumObligados) throws Excepciones;

}
