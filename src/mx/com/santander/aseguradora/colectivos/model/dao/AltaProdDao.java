package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;


/**
 * @author dflores
 *
 */

public interface AltaProdDao<AltaProd> {
	
	public abstract AltaProd saveAltaProd(AltaProd altaProd);
	/**
	 * 
	 * @param lista
	 */
	public abstract void saveListaAltaProd(List<Object> lista);
	/**
	 * 
	 * @param id
	 * @return
	 */
	
	public abstract List<AltaProd> getListaAltaProd();
	/**
	 * 
	 * @param ramo
	 * @param poliza
	 * @return
	 */
	public abstract List<AltaProd> getListaAltaProd( Short ramo, String descProducto, String descPlan );
	/**
	 * 
	 * @param canal
	 * @param ramo
	 * @param poliza
	 */
	public abstract Integer iniciaAltaProd(   Short ramo, String descProducto, String descPlan );
	

	

}
