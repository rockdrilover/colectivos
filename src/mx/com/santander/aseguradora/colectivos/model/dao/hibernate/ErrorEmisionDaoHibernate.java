package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import javax.ejb.Stateless;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.model.dao.ErrorEmisionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.BitacoraErroresDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase de tipo DAO que tiene acceso a base de datos para 
 * 				el proceso de Errores Emision.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Repository
public class ErrorEmisionDaoHibernate extends HibernateDaoSupport implements ErrorEmisionDao {
	//Implementacion de serializable
	private static final long serialVersionUID = 298075158826438461L;

	/**
	 * Constructor de clase
	 * @param sessionFactory session factory para conexion a DB
	 */
	@Autowired
	public ErrorEmisionDaoHibernate(SessionFactory sessionFactory) {
		//Se inicializa session factory
		super.setSessionFactory(sessionFactory);
	}

	@Override
	public void eliminaError(BitacoraErroresDTO dto) throws Excepciones {
		StringBuilder qryDelete;
		Query qry;
		try {
			qryDelete  = new StringBuilder() ;
			qryDelete.append("DELETE COLECTIVOS_BITACORA_ERRORES WHERE COER_CD_ERROR_BITACORA = ").append(dto.getCoerCdErrorBitacora());
			qry = getSession().createSQLQuery(qryDelete.toString()); 
			qry.executeUpdate();
			
			if(dto.getCoerTipoProceso().equals(Constantes.ESTRUCTURA_PU)) {
				qryDelete  = new StringBuilder() ;
				qryDelete.append("DELETE (SELECT * \n");
					qryDelete.append("FROM COLECTIVOS_CEDULA_DETALLE CCD \n");
					qryDelete.append("INNER JOIN COLECTIVOS_CEDULA CC ON (CC.COCC_CASU_CD_SUCURSAL = ").append(dto.getCoerCasuCdSucursal()).append(" \n");
							qryDelete.append("AND CC.COCC_CARP_CD_RAMO = ").append(dto.getCoerCarpCdRamo()).append(" \n");
							qryDelete.append("AND CC.COCC_UNIDAD_NEGOCIO = ").append(dto.getCoerIdVenta()).append(" \n");
							qryDelete.append("AND CC.COCC_ARCHIVO = '").append(dto.getCoerNombreArchivo()).append("' \n");
							qryDelete.append("AND CC.COCC_CD_CEDULA = CCD.COCD_COCC_CD_CEDULA) \n");
					qryDelete.append("WHERE CCD.COCD_NU_CREDITO = '").append(dto.getCoerNuCredito()).append("') \n");
				qry = getSession().createSQLQuery(qryDelete.toString()); 
				qry.executeUpdate();
			}
		} catch (HibernateException e) {
			throw new Excepciones("Error.", e);
		}
	}

}
