/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.HibernateException;
import WSTimbrado.EnviaTimbrado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCfdiErrorW;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleReporteDxP;
import mx.com.santander.aseguradora.colectivos.view.dto.ReporteDxP;

/**
 * @author Z014058 
 * Modificacion: Sergio Plata
 * 
 */
public interface CertificadoDao extends CatalogoDao {

	public abstract List<Object> totalCertificados(Short canal, Short ramo, Long Poliza)throws Excepciones;
	public abstract void cancelaCertificado(Certificado certificado)throws Excepciones;
	
	public abstract  <T> List<T> getPreFacCertificado (Short canal, Short ramo, Long poliza, Integer inIdVenta) throws Exception;
	public abstract  void actualizaPreFacPoliza(CertificadoId id);
	public abstract  <T> List<T> consultaCertificados(Short canal, Short ramo, long Pol, String tiprep, String tippma, String sts, String fechaini, String fechafin) throws Exception;
	public String totalCertif(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta, String rutaReportes) throws Excepciones;
	public <T> List<T> consultaMensualTecnico(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2) throws Excepciones;
	public <T> List<T> seleccionaDatosAutos() throws Exception;
	public void actualizaDatosAutos(Short canal, Short ramo, Long poliza, Long nucert, String cred, String nomarch, String cred2) throws Excepciones;
	public <T> List<T> generaReporteAutos() throws Excepciones;
	public <T> List<T> cifrasResumenAutos() throws Excepciones;
	public <T> List<T> generaReporteEmisionAutos() throws Excepciones;
	public void borrarDatosAutosPrecarga() throws Excepciones;
	public <T> List<T> consultaPolizaCancela(Short canal, Short ramo, Long poliza1) throws Excepciones;
	public void cancelaCert(CertificadoId id, String fecha1, Integer causa) throws Excepciones;
	public void conteoCertificados(Certificado certificado)throws Excepciones;
	public void conteoCertificadosMasivo(BeanCertificado certificado) throws Excepciones;
	public String consultaMensualEmision(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta
			                 , boolean chk1, boolean chk2, boolean chk3, String rutaReportes) throws Excepciones;
	public String consultaMensualEmision5758(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta
            , boolean chk1, boolean chk2, boolean chk3, String rutaReportes) throws Excepciones;
	public String procesoRenovacionPolizas(Short inRamo,Long inPoliza) throws Excepciones;
	public <T> List<T> consultaPolizasRenovadas() throws Excepciones;
	public <T> List<T> detallePolizasRenovadas() throws Excepciones;
    	public String reporteFacturados5758() throws Excepciones;
	public String reporteErrores5758() throws Excepciones;
	public void borraErrores5758();
	public String renovar5758(String rmes, String anio) throws Excepciones;
	public String generaLayoutRamo5(Short ramo) throws Excepciones;
	public String generaLayoutRamo65(Short ramo) throws Excepciones;
	public String generaLayoutRamo82(Short ramo) throws Excepciones;
	public void borraErrores56582(Short ramo) throws Excepciones;
   	public String generaReporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa, String contratoEnlace, String folioEndoso) throws Excepciones;
	public String generaSoporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa, String contratoEnlace, String folioEndoso) throws Excepciones;
	public abstract String consultaEmisionDesempleo(String fecha1, String fecha2)throws Excepciones;
	public abstract String generaObligados(short canal, Short ramo, Long poliza, Long poliza1, Integer inIdVenta, boolean chk1,	boolean chk2, boolean chk3, String reporte) throws Excepciones;
	public abstract String generaDevoluciones(short canal, Short ramo, Long poliza, Long poliza1, String fecha1, String fecha2, Integer inIdVenta, String reporte) throws Excepciones;
	public abstract Collection<? extends Object> consultarRegistros( String strQuery) throws Excepciones;
	public abstract String getXmlTimbrar(short canal, byte ramo, String reciboFiscal, String strRutaDestino) throws Excepciones;
	public byte[] getXmlTimbrar2(short canal, byte ramo, String reciboFiscal) throws Excepciones;
	public void actualizaDatosDFCI(EnviaTimbrado objTimbrado, short canal, byte ramo, Double reciboFiscal) throws Excepciones, HibernateException, SQLException ;
	public abstract String generaDetallePreFactura(int canal, int ramo,	int poliza);
	public abstract <T> List<T> resumenPreFactura(int canal, int ramo,Long poliza) throws Excepciones;
	public abstract String consultaReporteCancelacion(String fecha1,String fecha2,String causaAnulacion)throws Excepciones;
	public void actualizaObservacionesCan(CertificadoId id, String strObservaciones) throws Excepciones, HibernateException, SQLException;
	public abstract ArrayList<Object> getPolizasaaRenovar5758(String rmes, String anio) throws Exception;
	public abstract void actualizaPolizasCertificados5758(Object canal, Object ramo, Object poliza, Object anioPoliza, String fechaDesde, String fechaHasta) throws Exception;
	public abstract String getReporteRenovacion5758(String rmes, String anio)  throws Exception;;
	
	// obt producto plan 
	public abstract <T> List<T> obtenerProdPlan(String filtro) throws Excepciones;
	//LPV
	public abstract <T> BigDecimal maxNumeroDeCredito(String filtro) throws Excepciones;
	//CArga Polizas
	public abstract <T> List<T> cargaPolizas(int ramo) throws Exception;
	//carga polizas renovar
	public abstract <T> List<T> cargaPolizasRenovar(int ramo,String rmes2, String anio) throws Exception;

	/**
	 * Consulta el ultimo numero de recibo cobrado del certificado 
	 * @param id datos del canal, ramo, poliza, certificado
	 * @param prima prima para validar si es la misma que el recibo encontrado 
	 * @return ultimo recibo generado
	 * @throws Exception
	 */
	Integer consultaNumRecibo(CertificadoId id, BigDecimal prima) throws Exception;
	
	/**
	 * Metodo que regresa los certificados vigentes de una poliza
	 * @param canal canal al que pertenece el certificado
	 * @param ramo  ramo al que pertenece el certificado
	 * @param poliza poliza al que pertenece el certificado
	 * @param inIdVenta inIdVenta al que pertenece el certificado
	 * @param poliza1 
	 * @return certificados vigentes
	 */
	ArrayList<Object> generaReportevigor(short canal, Short ramo, Long poliza, Integer inIdVenta, Long poliza1);
	
	/**
	 * Metodo que inserta la petecion de un reporte de vigores
	 * @param next  Id 
	 * @param nombreVigores nombre de la peticion
	 * @param rutaParam ruta donde se genera archivo
	 * @param strMensaje mensaje
	 * @param strNomArchivo nombre con el que se generara el archico de salida
	 * @param tipoReporte  tipo de reporte es 
	 * @param reporteComisiones para saber que es de reporte Comisiones 
	 */
	void insertaBanderaBatch(Long next, String nombreVigores, String rutaParam, String strMensaje, String strNomArchivo, Long tipoReporte, Long reporteComisiones) throws Excepciones;
	
	/**
	 * Metodo que inserta la petecion de un reporte para endosos
	 * @param objPeticion: Objeto con datos de peticion
	 */
	void insertaBanderaBatch(Parametros objPeticion) throws Excepciones;
	
	/**
	 * Metodo que regresa los datos de la peticion que se genero
	 * @param strTipo tipo de reporte 
	 * @return objeto con datos de peticion
	 * @throws Excepciones
	 */
	Parametros valoresBatch(String strTipo) throws Excepciones;
	
	/**
	 * Metodo que valida si una poliza tiene recios pendientes de cobro
	 * @param canal canal de la poliza
	 * @param ramo ramo de la poliz 
	 * @param poliza numero de poliza
	 * @return verdadero si tiene recibos, falso si no tiene recibos
	 * @throws Exception
	 */
	boolean recibosPendCobro(Short canal, Short ramo, Long poliza)  throws Excepciones;
	
	/**
	 * Recupera Error del filtro WS
	 * @param filtro filtro de consulta
	 * @return Detalle del error del mensaje
	 * 
	 */
	CartCfdiErrorW descripcionErrorMensaje(CartCfdiErrorW filtro) throws Excepciones;
	
	/**
	 * Metodo que actualiza a estatus 7 los recibos 
	 * @param id datos de canal, ramo y poliza
	 * @throws Exception
	 */
	void actualizaEstatusRecibo(CertificadoId id) throws Excepciones;
	
	/**
	 * AUTHOR: TOWA (JJFM)
	 * Metodo que genera un archivo de reporte para Deudor por prima.
	 * 
	 * @param Short canal - Canal
	 * @param Short ramo - Ramo
	 * @param long poliza1 - poliza
	 * @param String fecha1 - fecha desde
	 * @param String fecha2 - fecha hasta
	 * @param Integer inIdVenta - id canal de venta
	 * @param boolean chk1 - filtro check 1
	 * @param boolean chk2 - filtro check 2
	 * @param boolean chk3 - filtro check 3
	 * @param String rutaReportes
	 * 
	 * @return Archivo csv de reporte deudor por prima.
	 * @throws Excepciones
	 * 
	 */
	 File ExportarReporteDxP(Short canal, Short ramo, long poliza1, String fecha1, String fecha2,
			Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String rutaReportes,
			Short cuota, int filtro) throws Excepciones;

	/**
	 * AUTHOR: TOWA (JJFM)
	 * Metodo que regresa informacion del reporte de Recibos.
	 * @param Short canal -  id de la sucursal
	 * @param Short ramo - id del ramo
	 * @param long poliza1 - poliza a buscar para el reporte
	 * @param String fecha1 - fecha filtro desde
	 * @param String fecha2 - fecha filtro hasta
	 * @param Integer inIdVenta - id del canal de venta
	 * @param List<Integer> arrintStatusRecibos - arreglo con los filtros de estatus de recibos
	 * @param String rutaReportes - Ruta para guardar el reporte.
	 * @return Arreglo con la informacion del reporte de Recibos.
	 * @throws Excepciones
	 */
	 ArrayList<String[]> ExportarReporteRecibos(Short canal, Short ramo, long poliza1, String fecha1, String fecha2,
			Integer inIdVenta, List<Integer> arrintStatusRecibos, String rutaReportes) throws Excepciones;

	/**
	 * AUTHOR: TOWA (JJFM)
	 * 22/11/2019
	 *  Metodo que regresa las vigencias disponibles para una poliza
	 *  @param Short shortCanal - Canal
	 *  @param Short shortRamo - Ramo
	 *  @param long longPoliza - Poliza
	 *  @param short shortCert - Certificado
	 *  
	 * @return Arreglo con las vigencias disponibles de la pliza.
	 * @throws Excepciones
	 */
	 ArrayList<String> arrstrObtenerVigencias(Short shortCanal, Short shortRamo, long longPoliza, short shortCert)
			throws Excepciones;

	/**
	 * AUTHOR: TOWA (JJFM)
	 * 25/11/2019
	 * Consulta base de datos para obtener la información de reporteDxP
	 * @param Short shortCanal - Canal
	 * @param Short shortRamo - Ramo
	 * @param long longPoliza - Poliza
	 * @param Short shortCert - Certificado
	 * @param String strFechaVigencia - fecha vigencia
	 * @param String strFechaBusqueda - fecha hasta
	 * @param short idVenta -idVenta
	 * @return Regresa un arreglo del ReporteDxP.
	 * @throws Excepciones
	 */
	 ArrayList<ReporteDxP> rdxpObtenerResumenDxP(Short shortCanal, Short shortRamo, long longPoliza, Short shortCert,
			String strFechaVigencia, String strFechaBusqueda, short idVenta) throws Excepciones;
	
	/**
	 * AUTHOR: TOWA (JJFM)
	 * 26/11/2019
	 * Consulta base de datos para obtener la información del detalle para reporteDxP.
	 * @param Short shortCanal - Canal
	 * @param Short shortRamo - Ramo
	 * @param long longPoliza - Poliza
	 * @param Short shortCert -Certificado
	 * @param String strFechaVigencia - Fecha Vigencia
	 * @param String strFechaBusqueda - Fecha Hasta
	 * @param  short idVenta - Id canal de venta
	 * @return Regresa un arreglo con la informacion del detalle para reporteDxP .
	 * @throws Excepciones
	 */
	 ArrayList<DetalleReporteDxP> arrdetdxpObtenerDetalleDxP(Short shortCanal, Short shortRamo, long longPoliza, 
			Short shortCert,String strFechaVigencia, String strFechaBusqueda, short idVenta) throws Excepciones;
}
