package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Interface que implementa la clase CedulaControlDaoHibernate.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Local
public interface CedulaControlDao extends Serializable {
	
	/**
	 * Metodo que consulta detalle de creditos
	 * @param tipocedula tipo cedula
	 * @param idVenta id venta
	 * @param coccRemesaCedula remesa para hacer consulta
	 * @return lista de creditos
	 * @throws Excepciones con error en general
	 */
	List<Object> getDetalleCreditos(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones;

	/**
	 * Metodo que guarda foto de una cedula control
	 * @param tipocedula tipo de cedula (Emision o Cancelacion)
	 * @param idVenta numero de id venta
	 * @param coccRemesaCedula Remesa a consulta
	 * @param tipoCedula2 tipo cedulas para historicos
	 * @throws Excepciones con error general
	 */
	void saveFotoCedula(Integer tipocedula, Integer idVenta, String coccRemesaCedula, Integer tipoCedula2) throws Excepciones;

	/**
	 * Metodo que consulta detalle de creditos historicos
	 * @param tipocedula tipo cedula
	 * @param idVenta id venta
	 * @param coccRemesaCedula remesa para hacer consulta
	 * @return lista de creditos
	 * @throws Excepciones con error en general
	 */
	List<Object> getDetalleCredHist(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones;

}
