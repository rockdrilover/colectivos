package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author TONY
 *
 */

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

public interface FacturacionIndDao extends CatalogoDao {
	
	public <T> List<T> getDatosCertificado() throws Exception; 
	
	public String getXmlTimbrar(Short canal, Short ramo, Long poliza, String reciboFiscal, String strRutaDestino, String strNombre) throws Excepciones;
    
    public String imprimeRecibo2(String rutaTemporal, String rutaReporte, String nombre,
			Short careCasuCdSucursal, Short careCarpCdRamo,
			Long careCapoNuPoliza, BigDecimal careNuRecibo, Short primaUnica, Long certificado);
	
    public void borrarDatosPrecarga()  throws Excepciones;

}
