/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.UsuarioDao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * @author Sergio Plata
 *
 */
@Repository
public class UsuarioDaoHibernate extends HibernateDaoSupport implements UsuarioDao {

	@Autowired
	public UsuarioDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	public <T> void actualizarObjeto(T objeto) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().update(objeto);
	}

	public <T> void borrarObjeto(T objeto) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().delete(objeto);
	}

	public <T> T guardarObjeto(T objeto) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(objeto);
		return objeto;
	}

	public <T> void guardarObjetos(List<T> lista) {
		// TODO Auto-generated method stub
		
		for(T objeto: lista){
			
			this.guardarObjeto(objeto);
		}
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) {
		// TODO Auto-generated method stub
		return (T) this.getHibernateTemplate().get(objeto, id);
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().loadAll(objeto);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuilder consulta =  new StringBuilder();
				
				consulta.append("from Usuario U\n");
				consulta.append("where 1 = 1\n");
				consulta.append(filtro);
				
				
				Query qry = sesion.createQuery(consulta.toString());
				
				List<T> lista = qry.list();
				
				return lista;
			}
			
		});
	}

	@SuppressWarnings("unchecked")
	public List<Object> consultarMapeo(String strQuery) throws Exception {
		List<Object> lstResultado;
		Query query;
		
		try {
			query = getSession().createQuery(strQuery);
			lstResultado = query.list();
		} catch (Exception e) {
			throw new Exception("Error:: UsuarioDaoHibernate.consultarMapeo:: " + e.getMessage());
		}
		return lstResultado;
	}
}
