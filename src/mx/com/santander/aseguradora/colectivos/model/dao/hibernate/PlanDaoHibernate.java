/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;





import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.dao.PlanDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;




import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dflores
 *
 */
@Repository
public class PlanDaoHibernate extends HibernateDaoSupport implements
		PlanDao {

	@Autowired
	public PlanDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	public Plan getPlan(PlanId id) {
		// TODO Auto-generated method stub
		Plan plan = (Plan) this.getHibernateTemplate().load(Plan.class, id);
		return plan;
	}


	public List<Plan> getPlanes() {
		// TODO Auto-generated method stub
		
		return null;

	}

	@SuppressWarnings("unchecked")
	public List<Plan> getPlanes(final Integer producto) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuffer sb = new StringBuffer(150);
				sb.append("from Plan P\n" +
						  "join fetch P.producto\n" +
						  "where P.id.alplCdProducto =:producto\n" +
						  "order by P.id");
				
								
				Query query = sesion.createQuery(sb.toString());
				query.setParameter("producto", producto);
				
				List<Object> lista = query.list();
				
				
				
				return lista;
			}
		});
		
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().merge(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {

		try {
			
			for(T objeto: lista){
				
				this.guardarObjeto(objeto);
			}
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	
	public <T> List<T> obtenerObjetos(final String filtro)throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer sb = new StringBuffer(150);
					sb.append("from Plan P	\n");
					sb.append("where 1 = 1      \n");
					sb.append(filtro);
					sb.append("order by P.id");
									
					Query query = sesion.createQuery(sb.toString());
					
					List<T> lista = query.list();
					return lista;
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}		
	}
	
	public List<Integer> getIdPlanes() {
		// TODO Auto-generated method stub
		StringBuffer query = new StringBuffer();
		
		query.append(" select ");
		query.append("        distinct( ");
		//query.append("			  plan.id.alplCdPlan");
		query.append("             cast( substr(plan.id.alplCdPlan, length(plan.id.alplCdPlan)-1) as integer) ");
		query.append("        ) as alplCdPlan ");
		query.append(" from  ");
		query.append("        Plan  plan");
		query.append(" order by 1 ");
		
		
		return this.getHibernateTemplate().find(query.toString());

	}
	
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Integer siguentePlanExtraPma() throws Excepciones {
		
		try {
			
			StringBuilder consulta = new StringBuilder(100);
			consulta.append("Select max(nvl(P.id.alplCdPlan,0)) + 1      	\n");
			consulta.append("from Plan P							    		\n");
			consulta.append("where P.id.alplCdRamo = 61						\n");
			consulta.append("and  P.alplDePlan  ='CASOS ESPECIAL 51'		\n");
			
		
			Query qry = this.getSession().createQuery(consulta.toString());
			
			
			Integer valPlanMax = Integer.parseInt(qry.list().get(0).toString());
			
	
			return valPlanMax;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Integer siguentePlan(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			StringBuilder consulta = new StringBuilder(100);
			consulta.append("Select nvl(max(P.id.alplCdPlan),0) + 1 ");
			consulta.append("from Plan P ");
			consulta.append("where 1=1 ");
			consulta.append(filtro);
		
			Query qry = this.getSession().createQuery(consulta.toString());
			
			Integer valPlanMax = Integer.parseInt(qry.list().get(0).toString());
	
			return valPlanMax;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

	}
	
	
	
}
