/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
	 public interface CartCoberturasDao extends CatalogoDao{
	
	 public <T> List<T> obtenerCartCoberturas(String filtro)throws Excepciones;
	 public <T> List<T> obtenerCartCoberturasObj(final String filtro) throws Excepciones;
}
