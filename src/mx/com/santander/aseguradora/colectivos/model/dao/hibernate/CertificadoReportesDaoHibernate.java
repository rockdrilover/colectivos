package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ProgramarReporteDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;


@Repository
public class CertificadoReportesDaoHibernate extends HibernateDaoSupport implements ProgramarReporteDao {

	@Resource
	private ServicioParametros servicioParametros;
	
    @Autowired
	public ProgramaReporteDaoHibernate queryReporteTecnico;

	@Autowired
	public CertificadoReportesDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}

	public CertificadoReportesDaoHibernate() {
		// TODO Auto-generated constructor stub
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String consultaMensualEmision(List<Object> listaReporte) throws Excepciones {
		
		Short canal = (Short) listaReporte.get(0);
		Short ramo = (Short) listaReporte.get(1);
		Long poliza1 =(Long) listaReporte.get(2);
		Long poliza2 = (Long) listaReporte.get(3);
		String fecha1 = (String) listaReporte.get(4);
		String fecha2 = (String) listaReporte.get(5);
		Integer inIdVenta = (Integer) listaReporte.get(6);
		Boolean chk1 = (Boolean) listaReporte.get(7);
		Boolean chk2 = (Boolean) listaReporte.get(8);
		Boolean chk3 = (Boolean) listaReporte.get(9);
		String reporte = (String)listaReporte.get(10);
		String usuario = (String)listaReporte.get(11);
		
		
		// TODO Auto-generated method stub
		ProgramaReporteDaoHibernate queryReporteTecnico = new ProgramaReporteDaoHibernate(getSessionFactory());
		String query2 = null;
		String nulo = null;
		String nombre="";
		String sts = "";
		String fecha_inicio="";
		String fec1="";

		int hora=0;
	    int minuto=0;
	    int segundo=0;

		StringBuffer sb = new StringBuffer();

		@SuppressWarnings("unused")
		int pu = 0;
		String cadenafac = null;
		String cadenacan = null;
		int flag = 0;
		String status = null;
		String coma = ",";

		Integer inicioPoliza = 0;

		StringBuilder filtro = new StringBuilder();
		StringBuilder filtro1 = new StringBuilder();
		try {

			if (poliza1 == 0) {

				filtro.append("  and P.copaDesParametro = 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro  = 'GEP' \n");
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor6 = ").append(inIdVenta).append("\n");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0)
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				else
					inicioPoliza = -1;

				cadenafac = "  and fac.cofa_capo_nu_poliza >= " + inicioPoliza
						+ "00000000 and fac.cofa_capo_nu_poliza <= " + inicioPoliza + "99999999 \n";
				// +" and c.coce_sub_campana = '" + inIdVenta + "' \n";
				cadenacan = "  and c.coce_capo_nu_poliza >= " + inicioPoliza + "00000000 and c.coce_capo_nu_poliza <= "
						+ inicioPoliza + "99999999 \n" + "  and c.coce_sub_campana      = '" + inIdVenta
						+ "' 				        \n";

			} else {
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n");
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza1).append(", 1, substr(")
						.append(poliza1).append(", 0, 3), substr(").append(poliza1).append(", 0, 2)) \n");
				filtro.append("order by P.copaNvalor3");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());

				for (Parametros item : listaPolizas) {

					if (item.getCopaNvalor4().toString().equals("1")) {
						pu = 1;
						if (poliza2 == 0) {
							cadenafac = "  and fac.cofa_capo_nu_poliza like '" + poliza1 + "%'";
							cadenacan = "  and c.coce_capo_nu_poliza like '" + poliza1 + "%'";
						} else {
							cadenafac = "  and fac.cofa_capo_nu_poliza like '" + poliza2 + "%'";
							cadenacan = "  and c.coce_capo_nu_poliza like '" + poliza2 + "%'";
						}
					} else {
						pu = 0;
						cadenafac = "  and fac.cofa_capo_nu_poliza = " + poliza1;
						cadenacan = "  and c.coce_capo_nu_poliza = " + poliza1;
					}
				}
			}
			if (chk2 == true || chk3 == true) {
				// System.out.println("1");
				filtro1.append("  and P.copaDesParametro= 'REPORTES' \n");
				filtro1.append("  and P.copaIdParametro = 'EMISION'    \n");
				filtro1.append("  and P.copaNvalor1 = 2 \n");
				filtro1.append("order by P.copaNvalor2");

				List<Parametros> listastatus = this.servicioParametros.obtenerObjetos(filtro1.toString());

				for (Parametros item : listastatus) {
					if (flag == 0) {
						status = item.getCopaNvalor2().toString();
						flag = 1;
					} else {
						status = status + coma;
						status = status + item.getCopaNvalor2().toString();
					}
				}
			}

			// RAMO 5: SEGURO COLECTIVO ACCIDENTES PERSONALES
			// POLIZA: 10511
			if (ramo == 5 && poliza1 == 10511) {
				
				sb.append("select c.coce_carp_cd_ramo                           RAMO,\n");
						sb.append("pl.alpl_dato3                                        CENTRO_COSTOS,\n"); 
				sb.append("c.coce_capo_nu_poliza                                POLIZA,\n");
				sb.append("c.coce_no_recibo                                     RECIBO,\n");  
				sb.append("cc.cocc_id_certificado                               CREDITO,\n");  
				sb.append("c.coce_nu_certificado                                CERTIFICADO,\n");
				sb.append("nvl(c.coce_buc_empresa,0)                            CREDITONUEVO,\n");
				sb.append("nvl(pl.alpl_dato2,pl.alpl_de_plan)                   ID_CUOTA,\n");  
				sb.append("est.ales_campo1                                      ESTATUS,\n"); 
				sb.append("'ALTA'                                               ESTATUS_MOVIMIENTO,\n");
				sb.append("c.coce_nu_cuenta                                     CUENTA,\n");
				sb.append("c.coce_tp_producto_bco                               PRODUCTO,\n");
				sb.append("c.coce_tp_subprod_bco                                SUBPRODUCTO,\n");
				sb.append("c.coce_cazb_cd_sucursal                              SUCURSAL,\n");
				sb.append("cl.cocn_apellido_pat                                 APELLIDO_PATERNO,\n");
				sb.append("cl.cocn_apellido_mat                                 APELLIDO_MATERNO,\n");
				sb.append("cl.cocn_nombre                                       NOMBRE,\n");
				sb.append("cl.cocn_cd_sexo                                      SEXO,\n");
				sb.append("to_char(cl.cocn_fe_nacimiento ,'dd/mm/yyyy')         FECHA_NACIMIENTO,\n");
				sb.append("cl.cocn_buc_cliente                                  NUMERO_CLIENTE,\n");
				sb.append("cl.cocn_rfc                                          RFC,\n");
				sb.append("c.coce_cd_plazo                                      PLAZO,\n");
				sb.append("(select to_char(min(re.care_fe_desde),'dd/mm/yyyy')\n");   
						sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
						sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
						sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
						sb.append("and re.care_cace_nu_certificado     = 0\n");
						sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
						sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
						sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            FECHA_INICIO_POLIZA,\n");
				sb.append("(select to_char(add_months(min(re.care_fe_desde),12),'dd/mm/yyyy')\n");
						sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
						sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
						sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
						sb.append("and re.care_cace_nu_certificado     = 0\n");
						sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
						sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
						sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            FECHA_FIN_POLIZA,\n");
				sb.append("to_char(c.coce_fe_suscripcion  ,'dd/mm/yyyy')                     FECHA_INGRESO,\n");
				sb.append("to_char(c.coce_fe_desde  ,'dd/mm/yyyy')                           FECHA_DESDE,\n");
				sb.append("to_char(c.coce_fe_hasta  ,'dd/mm/yyyy')                           FECHA_HASTA,\n");
				sb.append("to_char(c.coce_fe_ini_credito ,'dd/mm/yyyy')                      FECHA_INICIO_CREDITO,\n");
				sb.append("to_char(c.coce_fe_fin_credito ,'dd/mm/yyyy')                      FECHA_FIN_CREDITO,\n");
				sb.append("c.coce_campov6                                    				  FECHA_FIN_CREDITO_2,\n");
				sb.append("to_char(c.coce_fe_anulacion  ,'dd/mm/yyyy')                       FECHA_ANULACION,\n");
				sb.append("to_char(c.coce_fe_anulacion_col ,'dd/mm/yyyy')                    FECHA_CANCELACION,\n");
				sb.append("trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99'))  SUMA_ASEGURADA,\n");
				sb.append("nvl(c.coce_mt_bco_devolucion,0)                                   MONTO_DEVOLUCION,\n");
				sb.append("nvl(c.coce_mt_devolucion,0)                                       MONTO_DEVOLUCION_SIS,\n");
				sb.append("decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)   BASE_CALCULO,\n");
				sb.append("case when pf.copa_nvalor5 = 0\n");
						sb.append("then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1\n");
						sb.append("then c.coce_mt_prima_pura\n");
				sb.append("else 0 end   																	 PRIMA_NETA,\n");
				sb.append("--\n");
				sb.append("to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS,\n");
				sb.append("to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS,\n");
				sb.append("to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA,\n");
				sb.append("case when pf.copa_nvalor5 = 0 then\n");
						sb.append("nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1 then\n");
						sb.append("nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append("+ nvl(c.coce_mt_prima_pura,0)\n");
				sb.append("else 0 end   																	PRIMA_TOTAL,\n");
				sb.append("round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),2) TARIFA,\n");
				sb.append("--\n");
				sb.append("round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
						sb.append("--\n");
				sb.append("(select sum (cob.cocb_ta_riesgo) tasaVida\n");
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2) 																			PRIMA_VIDA,\n");
				sb.append("round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
						sb.append("(select sum (cob.cocb_ta_riesgo) tasaDes\n");           
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
						sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2)                                   										PRIMA_DESEMPLEO,\n");
				sb.append("cl.cocn_cd_estado                                  								CD_ESTADO,\n");
				sb.append("es.caes_de_estado                                  								ESTADO,\n");
				sb.append("cl.cocn_delegmunic                                 								MUNICIPIO,\n");
				sb.append("cl.cocn_cd_postal                                  								CP,\n");
				sb.append("c.coce_capu_cd_producto                                 						PRODUCTO_ASEGURADORA,\n");
				sb.append("c.coce_capb_cd_plan                                     						PLAN_ASEGURADORA,\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         					CUOTA_BASICA,\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
						sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         		CUOTA_DESEMPLEO,\n");
				sb.append("case when pf.copa_nvalor4 > 0 then\n");
				sb.append("(select to_char(cln.cocn_fe_nacimiento ,'dd/mm/yyyy')   ||'|'||\n");
						sb.append("cln.cocn_cd_sexo         ||'|'||\n");
				sb.append("clc.cocc_tp_cliente      ||'|'||\n");
				sb.append("p.copa_vvalor1           ||'|'||\n");
				sb.append("substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)\n");
				sb.append("from colectivos_cliente_certif clc\n");
				sb.append(",colectivos_clientes       cln\n");
				sb.append(",colectivos_parametros     p\n");
				sb.append("where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
						sb.append("and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo\n");
						sb.append("and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
						sb.append("and clc.cocc_nu_certificado   = c.coce_nu_certificado\n");
						sb.append("and nvl(clc.cocc_tp_cliente,1)  > 1\n");
				sb.append("and cln.cocn_nu_cliente       = clc.cocc_nu_cliente\n");
						sb.append("and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
						sb.append("and p.copa_id_parametro(+)    = 'TIPO'\n");
						sb.append("and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)\n");
				sb.append("else '' end                                 									DATOS_OBLIGADO_PU,\n");
				sb.append("to_char(c.COCE_FE_SUSCRIPCION ,'dd/mm/yyyy')                            		FE_EMISION,\n");
				sb.append("to_char(c.COCE_FE_CARGA ,'dd/mm/yyyy')                              			FE_CARGA\n"); 
				sb.append("\n");					
				sb.append("from colectivos_certificados   c\n");                     
				  sb.append(",colectivos_cliente_certif cc\n");                     
				      sb.append(",colectivos_clientes       cl\n");                     
				      sb.append(",colectivos_facturacion    fac\n");                   
				      sb.append(",colectivos_recibos         r\n");                       
				      sb.append(",cart_estados              es\n");                     
				      sb.append(",alterna_estatus           est\n");                   
				      sb.append(",colectivos_certificados   head\n");                   
				      sb.append(",alterna_planes            pl\n");                     
				      sb.append(",colectivos_parametros     pf\n");                    
/*				      sb.append("where fac.cofa_casu_cd_sucursal  = 1  -- CANAL\n");             
						sb.append("and fac.cofa_carp_cd_ramo      = 5   -- RAMO\n");              
						  sb.append("and fac.cofa_capo_nu_poliza    = 10511 -- POLIZA\n");
						  sb.append("and fac.cofa_fe_facturacion    >= to_date('28/06/2018','dd/mm/yyyy')\n"); 
						  sb.append("and fac.cofa_fe_facturacion    <= to_date('01/07/2018','dd/mm/yyyy')\n")*/; 
						  sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
							sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
							sb.append("and fac.cofa_capo_nu_poliza    = 10511 -- POLIZA\n");
							sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
							sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
						  sb.append("--\n"); 
				  sb.append("and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");         
						  sb.append("and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo\n");           
						  sb.append("and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza\n");           
						  sb.append("and c.coce_nu_certificado   > 0\n");                     
				  sb.append("and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal\n");         
						  sb.append("and c.coce_campon2          in (0,2008)\n");                         
				  sb.append("--\n"); 
				  sb.append("and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");        
						  sb.append("and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal\n");        
						  sb.append("and r.core_st_recibo        in (1,4)\n");                  
				  sb.append("--\n"); 
				  sb.append("and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");         
						  sb.append("and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo\n");           
						  sb.append("and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");           
						  sb.append("and cc.cocc_nu_certificado   = c.coce_nu_certificado\n");           
						  sb.append("and nvl(cc.cocc_tp_cliente,1)       = 1\n");                   
						  sb.append("--\n"); 
				  sb.append("and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");         
						  sb.append("and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo\n");           
						  sb.append("and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza\n");         
						  sb.append("and head.coce_nu_certificado   = 0\n");                   
						  sb.append("--\n"); 
				  sb.append("and cl.cocn_nu_cliente       = cc.cocc_nu_cliente\n");             
						  sb.append("--\n"); 
				  sb.append("and es.caes_cd_estado(+)     = cl.cocn_cd_estado\n");             
						  sb.append("--\n"); 
				  sb.append("and est.ales_cd_estatus      = c.coce_st_certificado\n");           
						  sb.append("--\n"); 
				  sb.append("and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo\n");           
						  sb.append("and pl.alpl_cd_producto      = c.coce_capu_cd_producto\n");         
						  sb.append("and pl.alpl_cd_plan          = c.coce_capb_cd_plan\n");            
						  sb.append("--\n");                                    
				sb.append("and pf.copa_des_parametro    = 'POLIZA'\n");                
						  sb.append("and pf.copa_id_parametro     = 'GEPREFAC'\n");                
						  sb.append("and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal\n");        
						  sb.append("and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo\n");           
						  sb.append("and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,substr(fac.cofa_capo_nu_poliza,0,3))\n"); 
						  sb.append("and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0'))\n");
			}
			// RAMO 65: SEGURO COLECTIVO DE VIDA
			// POLIZA: 16511
			else if (ramo == 65 && poliza1 == 16511) {
				sb.append("select c.coce_carp_cd_ramo                           RAMO,\n");
				sb.append("pl.alpl_dato3                                        CENTRO_COSTOS,\n"); 
				sb.append("c.coce_capo_nu_poliza                                POLIZA,\n");
				sb.append("c.coce_no_recibo                                     RECIBO,\n");  
				sb.append("cc.cocc_id_certificado                               CREDITO,\n");  
				sb.append("c.coce_nu_certificado                                CERTIFICADO,\n");
				sb.append("nvl(c.coce_buc_empresa,0)                            CREDITONUEVO,\n");
				sb.append("nvl(pl.alpl_dato2,pl.alpl_de_plan)                   ID_CUOTA,\n");  
				sb.append("est.ales_campo1                                      ESTATUS,\n"); 
				sb.append("'ALTA'                                               ESTATUS_MOVIMIENTO,\n");
				sb.append("c.coce_nu_cuenta                                     CUENTA,\n");
				sb.append("c.coce_tp_producto_bco                               PRODUCTO,\n");
				sb.append("c.coce_tp_subprod_bco                                SUBPRODUCTO,\n");
				sb.append("c.coce_cazb_cd_sucursal                              SUCURSAL,\n");
				sb.append("cl.cocn_apellido_pat                                 APELLIDO_PATERNO,\n");
				sb.append("cl.cocn_apellido_mat                                 APELLIDO_MATERNO,\n");
				sb.append("cl.cocn_nombre                                       NOMBRE,\n");
				sb.append("cl.cocn_cd_sexo                                      SEXO,\n");
				sb.append("to_char(cl.cocn_fe_nacimiento ,'dd/mm/yyyy')         FECHA_NACIMIENTO,\n");
				sb.append("cl.cocn_buc_cliente                                  NUMERO_CLIENTE,\n");
				sb.append("cl.cocn_rfc                                          RFC,\n");
				sb.append("c.coce_cd_plazo                                      PLAZO,\n");
				sb.append("(select to_char(min(re.care_fe_desde),'dd/mm/yyyy')\n");   
						sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
						sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
						sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
						sb.append("and re.care_cace_nu_certificado     = 0\n");
						sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
						sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
						sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            FECHA_INICIO_POLIZA,\n");
				sb.append("(select to_char(add_months(min(re.care_fe_desde),12),'dd/mm/yyyy')\n");
						sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
						sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
						sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
						sb.append("and re.care_cace_nu_certificado     = 0\n");
						sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
						sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
						sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            FECHA_FIN_POLIZA,\n");
				sb.append("to_char(c.coce_fe_suscripcion  ,'dd/mm/yyyy')                     FECHA_INGRESO,\n");
				sb.append("to_char(c.coce_fe_desde  ,'dd/mm/yyyy')                           FECHA_DESDE,\n");
				sb.append("to_char(c.coce_fe_hasta  ,'dd/mm/yyyy')                           FECHA_HASTA,\n");
				sb.append("to_char(c.coce_fe_ini_credito ,'dd/mm/yyyy')                      FECHA_INICIO_CREDITO,\n");
				sb.append("to_char(c.coce_fe_fin_credito ,'dd/mm/yyyy')                      FECHA_FIN_CREDITO,\n");
				sb.append("c.coce_campov6                                    				  FECHA_FIN_CREDITO_2,\n");
				sb.append("to_char(c.coce_fe_anulacion  ,'dd/mm/yyyy')                       FECHA_ANULACION,\n");
				sb.append("to_char(c.coce_fe_anulacion_col ,'dd/mm/yyyy')                    FECHA_CANCELACION,\n");
				sb.append("trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99'))  SUMA_ASEGURADA,\n");
				sb.append("nvl(c.coce_mt_bco_devolucion,0)                                   MONTO_DEVOLUCION,\n");
				sb.append("nvl(c.coce_mt_devolucion,0)                                       MONTO_DEVOLUCION_SIS,\n");
				sb.append("decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)   BASE_CALCULO,\n");
				sb.append("case when pf.copa_nvalor5 = 0\n");
						sb.append("then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1\n");
						sb.append("then c.coce_mt_prima_pura\n");
				sb.append("else 0 end   																	 PRIMA_NETA,\n");
				sb.append("--\n");
				sb.append("to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS,\n");
				sb.append("to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS,\n");
				sb.append("to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA,\n");
				sb.append("case when pf.copa_nvalor5 = 0 then\n");
						sb.append("nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1 then\n");
						sb.append("nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append("+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append("+ nvl(c.coce_mt_prima_pura,0)\n");
				sb.append("else 0 end   																	PRIMA_TOTAL,\n");
				sb.append("round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),2) TARIFA,\n");
				sb.append("--\n");
				sb.append("round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
						sb.append("--\n");
				sb.append("(select sum (cob.cocb_ta_riesgo) tasaVida\n");
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2) 																			PRIMA_VIDA,\n");
				sb.append("round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
						sb.append("(select sum (cob.cocb_ta_riesgo) tasaDes\n");           
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
						sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2)                                   										PRIMA_DESEMPLEO,\n");
				sb.append("cl.cocn_cd_estado                                  								CD_ESTADO,\n");
				sb.append("es.caes_de_estado                                  								ESTADO,\n");
				sb.append("cl.cocn_delegmunic                                 								MUNICIPIO,\n");
				sb.append("cl.cocn_cd_postal                                  								CP,\n");
				sb.append("c.coce_capu_cd_producto                                 						PRODUCTO_ASEGURADORA,\n");
				sb.append("c.coce_capb_cd_plan                                     						PLAN_ASEGURADORA,\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         					CUOTA_BASICA,\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
						sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
						sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
						sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
						sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
						sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
						sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
						sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         		CUOTA_DESEMPLEO,\n");
				sb.append("case when pf.copa_nvalor4 > 0 then\n");
				sb.append("(select to_char(cln.cocn_fe_nacimiento ,'dd/mm/yyyy')   ||'|'||\n");
						sb.append("cln.cocn_cd_sexo         ||'|'||\n");
				sb.append("clc.cocc_tp_cliente      ||'|'||\n");
				sb.append("p.copa_vvalor1           ||'|'||\n");
				sb.append("substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)\n");
				sb.append("from colectivos_cliente_certif clc\n");
				sb.append(",colectivos_clientes       cln\n");
				sb.append(",colectivos_parametros     p\n");
				sb.append("where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
						sb.append("and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo\n");
						sb.append("and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
						sb.append("and clc.cocc_nu_certificado   = c.coce_nu_certificado\n");
						sb.append("and nvl(clc.cocc_tp_cliente,1)  > 1\n");
				sb.append("and cln.cocn_nu_cliente       = clc.cocc_nu_cliente\n");
						sb.append("and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
						sb.append("and p.copa_id_parametro(+)    = 'TIPO'\n");
						sb.append("and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)\n");
				sb.append("else '' end                                 									DATOS_OBLIGADO_PU,\n");
				sb.append("to_char(c.COCE_FE_SUSCRIPCION ,'dd/mm/yyyy')                            		FE_EMISION,\n");
				sb.append("to_char(c.COCE_FE_CARGA ,'dd/mm/yyyy')                              			FE_CARGA\n");
				sb.append("from colectivos_certificados   c                     \n");
				  sb.append(",colectivos_cliente_certif cc               \n");      
				      sb.append(",colectivos_clientes       cl      \n");               
				      sb.append(",colectivos_facturacion    fac\n");                   
				      sb.append(",colectivos_recibos         r     \n");                  
				      sb.append(",cart_estados              es\n");                     
				      sb.append(",alterna_estatus           est      \n");             
				      sb.append(",colectivos_certificados   head\n");                   
				      sb.append(",alterna_planes            pl\n");                     
				      sb.append(",colectivos_parametros     pf\n");                    
				     /* sb.append("where fac.cofa_casu_cd_sucursal  = 1  -- CANAL\n");             
						sb.append("and fac.cofa_carp_cd_ramo      = 65   -- RAMO\n");              
						  sb.append("and fac.cofa_capo_nu_poliza    = 16511 -- POLIZA\n");
						  sb.append("and fac.cofa_fe_facturacion    >= to_date('28/06/2018','dd/mm/yyyy')\n"); 
						  sb.append("and fac.cofa_fe_facturacion    <= to_date('01/07/2018','dd/mm/yyyy')\n"); */
				      sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
						sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
						sb.append("and fac.cofa_capo_nu_poliza    = 16511 -- POLIZA\n");
						sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
						sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
						  sb.append("--\n"); 
				  sb.append("and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");         
						  sb.append("and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo\n");           
						  sb.append("and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza\n");           
						  sb.append("and c.coce_nu_certificado   > 0                     \n");
				  sb.append("and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal\n");         
						  sb.append("and c.coce_campon2          in (0,2008)\n");                         
				  sb.append("--\n"); 
				  sb.append("and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");        
						  sb.append("and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal\n");        
						  sb.append("and r.core_st_recibo        in (1,4)\n");                  
				  sb.append("--\n"); 
				  sb.append("and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");         
						  sb.append("and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo\n");           
						  sb.append("and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");           
						  sb.append("and cc.cocc_nu_certificado   = c.coce_nu_certificado\n");           
						  sb.append("and nvl(cc.cocc_tp_cliente,1)       = 1\n");                   
						  sb.append("--\n"); 
				  sb.append("and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");         
						  sb.append("and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo\n");           
						  sb.append("and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza\n");         
						  sb.append("and head.coce_nu_certificado   = 0\n");                   
						  sb.append("--\n"); 
				  sb.append("and cl.cocn_nu_cliente       = cc.cocc_nu_cliente\n");             
						  sb.append("--\n"); 
				  sb.append("and es.caes_cd_estado(+)     = cl.cocn_cd_estado\n");             
						  sb.append("--\n"); 
				  sb.append("and est.ales_cd_estatus      = c.coce_st_certificado\n");           
						  sb.append("--\n"); 
				  sb.append("and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo\n");           
						  sb.append("and pl.alpl_cd_producto      = c.coce_capu_cd_producto\n");         
						  sb.append("and pl.alpl_cd_plan          = c.coce_capb_cd_plan\n");            
						  sb.append("--\n");                                    
				sb.append("and pf.copa_des_parametro    = 'POLIZA'\n");                
						  sb.append("and pf.copa_id_parametro     = 'GEPREFAC'\n");                
						  sb.append("and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal\n");        
						  sb.append("and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo\n");           
						  sb.append("and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,substr(fac.cofa_capo_nu_poliza,0,3))\n"); 
						  sb.append("and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0'))\n");

			} else {

				if (chk1 == true) {
					sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1		     ESTATUS   				 				\n");
					sb.append("      ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n");
					sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
					if (poliza1 != 0) {
						sb.append(",(select min(re.core_fe_desde)										\n");
						sb.append("    from colectivos_recibos re												\n");
						sb.append("    where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	\n");
						sb.append("      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n");
						sb.append("      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n");
						sb.append("      and re.core_cace_nu_certificado     = 0						\n");
						sb.append("      and re.core_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.core_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.core_fe_desde                <= r.core_fe_desde			\n");
						sb.append(
								"      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_INICIO_POLIZA	\n");
						sb.append(",(select add_months(min(re.core_fe_desde),12)						\n");
						sb.append("    from colectivos_recibos re												\n");
						sb.append("    where re.core_casu_cd_sucursal      = r.core_casu_cd_sucursal	\n");
						sb.append("      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n");
						sb.append("      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n");
						sb.append("      and re.core_cace_nu_certificado     = 0						\n");
						sb.append("      and re.core_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.core_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.core_fe_desde                <= r.core_fe_desde			\n");
						sb.append(
								"      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_FIN_POLIZA	\n");

					} else {
						sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
						sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					}
					sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
					sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.coce_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo		\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");
					sb.append("      ,nvl(c.coce_buc_empresa,0)		  CREDITONUEVO			     		\n");
					sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados   c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,colectivos_facturacion    fac 									\n");
					sb.append("      ,colectivos_recibos   			r 									    \n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
					sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
					sb.append(cadenafac);
					sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
					sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
					sb.append("  -- \n");
					sb.append("  and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n");
					sb.append("  and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n");
					sb.append("  and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n");
					sb.append("  and c.coce_nu_certificado   > 0 										\n");
					sb.append("  and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal 				\n");
					sb.append("  and c.coce_campon2          in (0,2008) 				                \n");
					sb.append("  -- \n");
					sb.append("  and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n");
					sb.append("  and r.core_st_recibo        in (1,4)									\n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'								\n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, fac.cofa_capo_nu_poliza, 1, substr(fac.cofa_capo_nu_poliza,0,3), substr(fac.cofa_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");

					sb.append("\n");
					sb.append("union all \n");
					sb.append("\n");

					sb.append("select c.cocm_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.cocm_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.cocm_nu_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.cocm_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1		     ESTATUS   				 				\n");
					sb.append("      ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n");
					sb.append("      ,c.cocm_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.cocm_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.cocm_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.cocm_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.cocm_cd_plazo          PLAZO 									\n");
					if (poliza1 != 0) {
						sb.append(",(select min(re.core_fe_desde)										\n");
						sb.append("    from colectivos_recibos re												\n");
						sb.append("    where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	\n");
						sb.append("      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n");
						sb.append("      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n");
						sb.append("      and re.core_cace_nu_certificado     = 0						\n");
						sb.append("      and re.core_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.core_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.core_fe_desde                <= r.core_fe_desde			\n");
						sb.append(
								"      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_INICIO_POLIZA	\n");
						sb.append(",(select add_months(min(re.core_fe_desde),12)						\n");
						sb.append("    from colectivos_recibos re												\n");
						sb.append("    where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	\n");
						sb.append("      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n");
						sb.append("      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n");
						sb.append("      and re.core_cace_nu_certificado     = 0						\n");
						sb.append("      and re.core_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.core_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.core_fe_desde                <= r.core_fe_desde			\n");
						sb.append(
								"      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_FIN_POLIZA	\n");

					} else {
						sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
						sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					}
					sb.append("      ,c.cocm_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.cocm_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.cocm_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.cocm_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.cocm_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.cocm_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.cocm_fe_anulacion_real FECHA_ANULACION 							\n");
					sb.append("      ,c.cocm_fe_anulacion      FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.cocm_sub_campana,'7',to_number(c.cocm_mt_suma_aseg_si),c.cocm_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.cocm_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1) -instr(c.cocm_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2) -instr(c.cocm_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.cocm_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.cocm_mt_prima_pura*1000/decode(c.cocm_sub_campana,'7',decode(c.cocm_mt_suma_asegurada,0,1,c.cocm_mt_suma_asegurada),decode(c.cocm_mt_suma_aseg_si,0,1,c.cocm_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.cocm_carp_cd_ramo in (61,65) then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.cocm_carp_cd_ramo 		\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.cocm_carp_cd_ramo = 61 then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.cocm_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.cocm_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.cocm_mt_devolucion ,0) - nvl(c.cocm_mt_bco_devolucion ,0) ) DIFERENCIA_DEVOLUCION \n");
					sb.append("      ,nvl(c.cocm_buc_empresa,0)		  CREDITONUEVO			     		\n");
					sb.append(",c.cocm_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.cocm_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.cocm_empresa,instr(c.cocm_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.cocm_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.cocm_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeEn:',1,1) + 5,instr(c.cocm_campoV6, '|',1,6)-instr(c.cocm_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeCar:',1,1) + 6,instr(c.cocm_campoV6, '|',1,8)-instr(c.cocm_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeRe:',1,1) + 5,instr(c.cocm_campoV6, '|',1,7)-instr(c.cocm_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados_mov c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,colectivos_facturacion    fac 									\n");
					sb.append("      ,colectivos_recibos   			r 									    \n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
					sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
					sb.append(cadenafac);
					sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
					sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
					sb.append("  -- \n");
					sb.append("  and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n");
					sb.append("  and r.core_st_recibo        in (1,4)									\n");
					sb.append("  -- \n");
					sb.append("  and c.cocm_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n");
					sb.append("  and c.cocm_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n");
					sb.append("  and c.cocm_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n");
					sb.append("  and c.cocm_nu_certificado   > 0 										\n");
					sb.append("  and c.cocm_nu_recibo        = fac.cofa_nu_recibo_fiscal 				\n");
					sb.append("  and c.cocm_campon2          in (0,2008) 				                \n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.cocm_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.cocm_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.cocm_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.cocm_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.cocm_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.cocm_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'								\n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, fac.cofa_capo_nu_poliza, 1, substr(fac.cofa_capo_nu_poliza,0,3), substr(fac.cofa_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");

				}
				if (chk1 == true && chk2 == true)
					sb.append("  union all \n");
				if (chk2 == true) {
					sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");
					sb.append("      ,'BAJA'                   ESTATUS_MOVIMIENTO						\n");
					sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
					sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
					sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
					sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.coce_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo 		\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("            and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");
					sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
					sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados   c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
					sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
					sb.append(cadenacan);
					sb.append("  and c.coce_nu_certificado   > 0			 							\n");
					sb.append("  and c.coce_st_certificado   in (10,11)  	     				        \n");
					sb.append("  and c.coce_cd_causa_anulacion <> 45			  	     				\n");
					sb.append("  and c.coce_fe_anulacion    >= to_date('" + fecha1 + "','dd/mm/yyyy')   \n");
					sb.append("  and c.coce_fe_anulacion    <= to_date('" + fecha2 + "','dd/mm/yyyy')   \n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
					sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
				}
				if ((chk1 == true || chk2 == true) && chk3 == true)
					sb.append("  union all \n");
				if (chk3 == true) {
					sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");
					sb.append("      ,'EMITIDO'                ESTATUS_MOVIMIENTO						\n");
					sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
					sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
					sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
					sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.coce_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo   	\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION   \n");
					sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
					sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados   c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
					sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
					sb.append(cadenacan);
					sb.append("  and c.coce_nu_certificado   > 0			 							\n");
					// sb.append(" and c.coce_st_certificado in (" + status + ")
					// \n");
					// sb.append(" and c.coce_cd_causa_anulacion <> 45 \n");
					sb.append("  and c.coce_fe_carga    >= to_date('" + fecha1 + "','dd/mm/yyyy')   \n");
					sb.append("  and c.coce_fe_carga    <= to_date('" + fecha2 + "','dd/mm/yyyy')   \n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
					sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
				}
			}


			
			
			
			Format formatter1;
			Format formatter2;
			formatter1 = new SimpleDateFormat("ddMMyyyy");
			formatter2 = new SimpleDateFormat("dd/MM/yyyy");
			
			String fec = null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
			fec = formatter1.format(nowDate);
			fec1 = formatter2.format(nowDate);
			
			
		    
		    
		    now.setTime(nowDate);
			  
		    hora     = now.get(Calendar.HOUR_OF_DAY);
		    minuto   = now.get(Calendar.MINUTE);
		    segundo  = now.get(Calendar.SECOND);
		    

			if (chk1 == true)
				sts += "VIG";
			if (chk2 == true)
				sts += "CAN";
			if (chk3 == true)
				sts += "EMI";
            
			if(minuto < 10){
		    	nombre = "REPEMI_" + sts + "_" + poliza1 + "_" + fec+ "_" + hora+"0"+minuto+segundo+  ".csv" ;
		    	fecha_inicio =fec1+" "+ hora+":0"+minuto+":"+segundo;
		    	Log.info(nombre);
		    }else{
		    	nombre = "REPEMI_" + sts + "_" + poliza1 + "_" + fec+ "_" + hora+minuto+segundo+  ".csv" ;
		    	fecha_inicio =fec1+" "+ hora+":"+minuto+":"+segundo;
		    	Log.info(nombre);
		    } 
			

			Log.info(sb.toString());
			
			Log.info("-------------------------------------------------------------------");
			query2 = queryReporteTecnico.RecibirQueryReporteTecnico(sb.toString());
            if(query2=="Error"){
                nombre = query2;
            }else {
            	Log.info("Nombre del reporte generado: "+nombre);
            	queryReporteTecnico.insertarQueryReporteTecnico(query2, nombre, usuario, fecha_inicio, Constantes.DEFAULT_STRING);
            }
			Log.info("-------------------------------------------------------------------");
			return nombre;

		} catch (Exception e) {
			Log.info("Error al inserta reporte");
			throw new Excepciones("Error.", e);
			
		}

	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String consultaMensualEmision5758(List<Object> listaReporte) throws Excepciones {
	/*public String consultaMensualEmision5758(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1,
			String fecha2, Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String rutaReportes, String usuario)
			throws Excepciones {*/

		ProgramaReporteDaoHibernate queryReporteTecnico = new ProgramaReporteDaoHibernate(getSessionFactory());
		String query2 = null;
		String fecha_inicio ="";
		String fec1="";
		StringBuffer sb = new StringBuffer();
		Short canal = (Short) listaReporte.get(1);
		Short ramo = (Short) listaReporte.get(2);
		Long poliza1 =(Long) listaReporte.get(3);
		Long poliza2 = (Long) listaReporte.get(4);
		String fecha1 = (String) listaReporte.get(5);
		String fecha2 = (String) listaReporte.get(6);
		Integer inIdVenta = (Integer) listaReporte.get(7);
		Boolean chk1 = (Boolean) listaReporte.get(8);
		Boolean chk2 = (Boolean) listaReporte.get(9);
		Boolean chk3 = (Boolean) listaReporte.get(10);
		String reporte = (String)listaReporte.get(11);
		String usuario = (String)listaReporte.get(12);

		@SuppressWarnings("unused")
		int pu = 0;
		String cadenafac = null;
		String cadenacan = null;
		String nulo = null;
		int flag = 0;
		String status = null;
		String coma = ",";
		Integer inicioPoliza = 0;

		StringBuilder filtro = new StringBuilder();
		StringBuilder filtro1 = new StringBuilder();
		try {

			if (poliza1 == 0) {

				filtro.append("  and P.copaDesParametro = 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro  = 'GEP' \n");
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor6 = ").append(inIdVenta).append("\n");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0)
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				else
					inicioPoliza = -1;

				cadenafac = "  and fac.cofa_capo_nu_poliza >= " + inicioPoliza
						+ "00000000 and fac.cofa_capo_nu_poliza <= " + inicioPoliza + "99999999 \n";
				// +" and c.coce_sub_campana = '" + inIdVenta + "' \n";
				cadenacan = "  and c.coce_capo_nu_poliza >= " + inicioPoliza + "00000000 and c.coce_capo_nu_poliza <= "
						+ inicioPoliza + "99999999 \n" + "  and c.coce_sub_campana      = '" + inIdVenta
						+ "' 				        \n";

			} else {
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n");
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza1).append(", 1, ")
						.append(poliza1.toString().substring(0, 3)).append(", 2, ")
						.append(poliza1.toString().substring(0, 2)).append(")\n");
				filtro.append("order by P.copaNvalor3");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());

				for (Parametros item : listaPolizas) {

					if (item.getCopaNvalor4().toString().equals("1")) {
						pu = 1;
						if (poliza2 == 0) {
							cadenafac = "  and fac.cofa_capo_nu_poliza like '" + poliza1 + "%'";
							cadenacan = "  and c.coce_capo_nu_poliza like '" + poliza1 + "%'";
						} else {
							cadenafac = "  and fac.cofa_capo_nu_poliza like '" + poliza2 + "%'";
							cadenacan = "  and c.coce_capo_nu_poliza like '" + poliza2 + "%'";
						}
					} else {
						pu = 0;
						cadenafac = "  and fac.cofa_capo_nu_poliza = " + poliza1;
						cadenacan = "  and c.coce_capo_nu_poliza = " + poliza1;
					}
				}
			}
			if (chk2 == true || chk3 == true) {
				// System.out.println("1");
				filtro1.append("  and P.copaDesParametro= 'REPORTES' \n");
				filtro1.append("  and P.copaIdParametro = 'EMISION'    \n");
				filtro1.append("  and P.copaNvalor1 = 2 \n");
				filtro1.append("order by P.copaNvalor2");

				List<Parametros> listastatus = this.servicioParametros.obtenerObjetos(filtro1.toString());

				for (Parametros item : listastatus) {
					if (flag == 0) {
						status = item.getCopaNvalor2().toString();
						flag = 1;
					} else {
						status = status + coma;
						status = status + item.getCopaNvalor2().toString();
					}
				}
			}

			// RAMO 5: SEGURO COLECTIVO ACCIDENTES PERSONALES
			// POLIZA: 10511
			if (ramo == 5 && poliza1 == 10511) {

				sb.append(
						"select   'RAMO,CENTRO_COSTOS,POLIZA,RECIBO,CREDITO,CERTIFICADO,CREDITONUEVO,ID_CUOTA,ESTATUS,ESTATUS_MOVIMIENTO,CUENTA,PRODUCTO,SUBPRODUCTO,SUCURSAL,APELLIDO_PATERNO,APELLIDO_MATERNO,NOMBRE,SEXO,FECHA_NACIMIENTO,NUMERO_CLIENTE,RFC,PLAZO,FECHA_INICIO_POLIZA,FECHA_FIN_POLIZA,FECHA_INGRESO,FECHA_DESDE,FECHA_HASTA,FECHA_INICIO_CREDITO,FECHA_FIN_CREDITO,FECHA_FIN_CREDITO_2,FECHA_ANULACION,FECHA_CANCELACION,SUMA_ASEGURADA,MONTO_DEVOLUCION,MONTO_DEVOLUCION_SIS,BASE_CALCULO,PRIMA_NETA,DERECHOS,RECARGOS,IVA,PRIMA_TOTAL,TARIFA,PRIMA_VIDA,PRIMA_DESEMPLEO,CD_ESTADO,ESTADO,MUNICIPIO,CP,PRODUCTO_ASEGURADORA,PLAN_ASEGURADORA,CUOTA_BASICA,CUOTA_DESEMPLEO,DATOS_OBLIGADO_PU,FE_EMISION,FE_CARGA' linea1\n");
				sb.append("from dual\n");
				sb.append("union all\n");
				sb.append("select c.coce_carp_cd_ramo                                  ||','||\n");
				sb.append("pl.alpl_dato3                                        ||','||\n");
				sb.append("c.coce_no_recibo                                     ||','||\n");
				sb.append("c.coce_capo_nu_poliza                                ||','||\n");
				sb.append("c.coce_no_recibo                                     ||','||\n");
				sb.append("cc.cocc_id_certificado                               ||','||\n");
				sb.append("c.coce_nu_certificado                                ||','||\n");
				sb.append("nvl(c.coce_buc_empresa,0)                              ||','||\n");
				sb.append("nvl(pl.alpl_dato2,pl.alpl_de_plan)                                             ||','||\n");
				sb.append("est.ales_campo1                                      ||','||\n");
				sb.append("'ALTA'                                               ||','||\n");
				sb.append("c.coce_nu_cuenta                                              ||','||\n");
				sb.append("c.coce_tp_producto_bco                                 ||','||\n");
				sb.append("c.coce_tp_subprod_bco                                ||','||\n");
				sb.append("c.coce_cazb_cd_sucursal                              ||','||\n");
				sb.append("cl.cocn_apellido_pat                                 ||','||\n");
				sb.append("cl.cocn_apellido_mat                                                           ||','||\n");
				sb.append("cl.cocn_nombre                                                                 ||','||\n");
				sb.append("cl.cocn_cd_sexo                                                                ||','||\n");
				sb.append(
						"to_char(cl.cocn_fe_nacimiento ,'dd/mm/yyyy')                                                          ||','||\n");
				sb.append("cl.cocn_buc_cliente                                  ||','||\n");
				sb.append("cl.cocn_rfc                                                                    ||','||\n");
				sb.append("c.coce_cd_plazo                                                                ||','||\n");
				sb.append("(select to_char(min(re.care_fe_desde),'dd/mm/yyyy')\n");
				sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
				sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
				sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
				sb.append("and re.care_cace_nu_certificado     = 0\n");
				sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
				sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
				sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            ||','||\n");
				sb.append("(select to_char(add_months(min(re.care_fe_desde),12),'dd/mm/yyyy')\n");
				sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
				sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
				sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
				sb.append("and re.care_cace_nu_certificado     = 0\n");
				sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
				sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
				sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            ||','||\n");
				sb.append("to_char(c.coce_fe_suscripcion  ,'dd/mm/yyyy')                               ||','||\n");
				sb.append("to_char(c.coce_fe_desde  ,'dd/mm/yyyy')                                       ||','||\n");
				sb.append("to_char(c.coce_fe_hasta  ,'dd/mm/yyyy')                                     ||','||\n");
				sb.append("to_char(c.coce_fe_ini_credito ,'dd/mm/yyyy')                                ||','||\n");
				sb.append(
						"to_char(c.coce_fe_fin_credito ,'dd/mm/yyyy')                                                           ||','||\n");
				sb.append("c.coce_campov6                                    ||','||\n");
				sb.append("to_char(c.coce_fe_anulacion  ,'dd/mm/yyyy')                                 ||','||\n");
				sb.append("to_char(c.coce_fe_anulacion_col ,'dd/mm/yyyy')                              ||','||\n");
				sb.append(
						"trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) ||','||\n");
				sb.append("nvl(c.coce_mt_bco_devolucion,0)                                                  ||','||\n");
				sb.append("nvl(c.coce_mt_devolucion,0)                                                    ||','||\n");
				sb.append(
						"decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)                        ||','||\n");
				sb.append("case when pf.copa_nvalor5 = 0\n");
				sb.append(
						"then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1\n");
				sb.append("then c.coce_mt_prima_pura\n");
				sb.append("else 0 end ||','||\n");
				sb.append("--\n");
				sb.append(
						"to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) ||','||\n");
				sb.append(
						"to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) ||','||\n");
				sb.append(
						"to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) ||','||\n");
				sb.append("case when pf.copa_nvalor5 = 0 then\n");
				sb.append(
						"nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1 then\n");
				sb.append(
						"nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append("+ nvl(c.coce_mt_prima_pura,0)\n");
				sb.append("else 0 end ||','||\n");
				sb.append(
						"round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),2) ||','||\n");
				sb.append("--\n");
				sb.append(
						"round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
				sb.append("--\n");
				sb.append("(select sum (cob.cocb_ta_riesgo) tasaVida\n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2) ||','||\n");
				sb.append(
						"round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
				sb.append("(select sum (cob.cocb_ta_riesgo) tasaDes           \n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2)                                   ||','||\n");
				sb.append("cl.cocn_cd_estado                                  ||','||\n");
				sb.append("es.caes_de_estado                                  ||','||\n");
				sb.append("cl.cocn_delegmunic                                 ||','||\n");
				sb.append("cl.cocn_cd_postal                                  ||','||\n");
				sb.append("c.coce_capu_cd_producto                                 ||','||\n");
				sb.append("c.coce_capb_cd_plan                                     ||','||\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         ||','||\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         ||','||\n");
				sb.append("case when pf.copa_nvalor4 > 0 then\n");
				sb.append("(select to_char(cln.cocn_fe_nacimiento ,'dd/mm/yyyy')   ||'|'||\n");
				sb.append("cln.cocn_cd_sexo         ||'|'||\n");
				sb.append("clc.cocc_tp_cliente      ||'|'||\n");
				sb.append("p.copa_vvalor1           ||'|'||\n");
				sb.append("substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)\n");
				sb.append("from colectivos_cliente_certif clc\n");
				sb.append(",colectivos_clientes       cln\n");
				sb.append(",colectivos_parametros     p\n");
				sb.append("where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
				sb.append("and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo\n");
				sb.append("and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
				sb.append("and clc.cocc_nu_certificado   = c.coce_nu_certificado\n");
				sb.append("and nvl(clc.cocc_tp_cliente,1)  > 1\n");
				sb.append("and cln.cocn_nu_cliente       = clc.cocc_nu_cliente\n");
				sb.append("and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
				sb.append("and p.copa_id_parametro(+)    = 'TIPO'\n");
				sb.append("and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)\n");
				sb.append("else '' end                                 ||','||\n");
				sb.append("to_char(c.COCE_FE_SUSCRIPCION ,'dd/mm/yyyy')                            ||','||\n");
				sb.append("to_char(c.COCE_FE_CARGA ,'dd/mm/yyyy')                              liena2\n");
				sb.append("from colectivos_certificados   c                     \n");
				sb.append(",colectivos_cliente_certif cc\n");
				sb.append(",colectivos_clientes       cl\n");
				sb.append(",colectivos_facturacion    fac\n");
				sb.append(",colectivos_recibos         r\n");
				sb.append(",cart_estados              es\n");
				sb.append(",alterna_estatus           est\n");
				sb.append(",colectivos_certificados   head\n");
				sb.append(",alterna_planes            pl\n");
				sb.append(",colectivos_parametros     pf\n");
				/*
				 * sb.
				 * append("where fac.cofa_casu_cd_sucursal  = 1  -- CANAL     \n"
				 * );
				 * sb.append("and fac.cofa_carp_cd_ramo      = 5   -- RAMO\n");
				 * sb.
				 * append("and fac.cofa_capo_nu_poliza    = 10511 -- POLIZA\n");
				 * sb.
				 * append("and fac.cofa_fe_facturacion    >= to_date('28/06/2018','dd/mm/yyyy')\n"
				 * ); sb.
				 * append("and fac.cofa_fe_facturacion    <= to_date('01/07/2018','dd/mm/yyyy')\n"
				 * );
				 */

				sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
				sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
				sb.append("and fac.cofa_capo_nu_poliza    = 10511 -- POLIZA\n");
				sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
				sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");

				sb.append("--\n");
				sb.append("and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");
				sb.append("and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo\n");
				sb.append("and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza\n");
				sb.append("and c.coce_nu_certificado   > 0\n");
				sb.append("and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal\n");
				sb.append("and c.coce_campon2          in (0,2008)\n");
				sb.append("--\n");
				sb.append("and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");
				sb.append("and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal\n");
				sb.append("and r.core_st_recibo        in (1,4)\n");
				sb.append("--\n");
				sb.append("and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
				sb.append("and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo\n");
				sb.append("and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
				sb.append("and cc.cocc_nu_certificado   = c.coce_nu_certificado\n");
				sb.append("and nvl(cc.cocc_tp_cliente,1)       = 1\n");
				sb.append("--\n");
				sb.append("and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
				sb.append("and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo\n");
				sb.append("and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
				sb.append("and head.coce_nu_certificado   = 0\n");
				sb.append("--\n");
				sb.append("and cl.cocn_nu_cliente       = cc.cocc_nu_cliente\n");
				sb.append("--\n");
				sb.append("and es.caes_cd_estado(+)     = cl.cocn_cd_estado\n");
				sb.append("--\n");
				sb.append("and est.ales_cd_estatus      = c.coce_st_certificado\n");
				sb.append("--\n");
				sb.append("and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo\n");
				sb.append("and pl.alpl_cd_producto      = c.coce_capu_cd_producto\n");
				sb.append("and pl.alpl_cd_plan          = c.coce_capb_cd_plan\n");
				sb.append("--\n");
				sb.append("and pf.copa_des_parametro    = 'POLIZA'\n");
				sb.append("and pf.copa_id_parametro     = 'GEPREFAC'\n");
				sb.append("and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal\n");
				sb.append("and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo\n");
				sb.append(
						"and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,substr(fac.cofa_capo_nu_poliza,0,3))\n");
				sb.append("and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0'))\n");
			}
			// RAMO 65: SEGURO COLECTIVO DE VIDA
			// POLIZA: 16511
			else if (ramo == 65 && poliza1 == 16511) {
				sb.append(
						"select   'RAMO,CENTRO_COSTOS,POLIZA,RECIBO,CREDITO,CERTIFICADO,CREDITONUEVO,ID_CUOTA,ESTATUS,ESTATUS_MOVIMIENTO,CUENTA,PRODUCTO,SUBPRODUCTO,SUCURSAL,APELLIDO_PATERNO,APELLIDO_MATERNO,NOMBRE,SEXO,FECHA_NACIMIENTO,NUMERO_CLIENTE,RFC,PLAZO,FECHA_INICIO_POLIZA,FECHA_FIN_POLIZA,FECHA_INGRESO,FECHA_DESDE,FECHA_HASTA,FECHA_INICIO_CREDITO,FECHA_FIN_CREDITO,FECHA_FIN_CREDITO_2,FECHA_ANULACION,FECHA_CANCELACION,SUMA_ASEGURADA,MONTO_DEVOLUCION,MONTO_DEVOLUCION_SIS,BASE_CALCULO,PRIMA_NETA,DERECHOS,RECARGOS,IVA,PRIMA_TOTAL,TARIFA,PRIMA_VIDA,PRIMA_DESEMPLEO,CD_ESTADO,ESTADO,MUNICIPIO,CP,PRODUCTO_ASEGURADORA,PLAN_ASEGURADORA,CUOTA_BASICA,CUOTA_DESEMPLEO,DATOS_OBLIGADO_PU,FE_EMISION,FE_CARGA' linea1\n");
				sb.append("from dual\n");
				sb.append("union all\n");
				sb.append("select c.coce_carp_cd_ramo                                  ||','||\n");
				sb.append("pl.alpl_dato3                                        ||','||\n");
				sb.append("c.coce_no_recibo                                     ||','||\n");
				sb.append("c.coce_capo_nu_poliza                                ||','||\n");
				sb.append("c.coce_no_recibo                                     ||','||\n");
				sb.append("cc.cocc_id_certificado                               ||','||\n");
				sb.append("c.coce_nu_certificado                                ||','||\n");
				sb.append("nvl(c.coce_buc_empresa,0)                              ||','||\n");
				sb.append("nvl(pl.alpl_dato2,pl.alpl_de_plan)                                             ||','||\n");
				sb.append("est.ales_campo1                                      ||','||\n");
				sb.append("'ALTA'                                               ||','||\n");
				sb.append("c.coce_nu_cuenta                                              ||','||\n");
				sb.append("c.coce_tp_producto_bco                                 ||','||\n");
				sb.append("c.coce_tp_subprod_bco                                ||','||\n");
				sb.append("c.coce_cazb_cd_sucursal                              ||','||\n");
				sb.append("cl.cocn_apellido_pat                                 ||','||\n");
				sb.append("cl.cocn_apellido_mat                                                           ||','||\n");
				sb.append("cl.cocn_nombre                                                                 ||','||\n");
				sb.append("cl.cocn_cd_sexo                                                                ||','||\n");
				sb.append(
						"to_char(cl.cocn_fe_nacimiento ,'dd/mm/yyyy')                                                          ||','||\n");
				sb.append("cl.cocn_buc_cliente                                  ||','||\n");
				sb.append("cl.cocn_rfc                                                                    ||','||\n");
				sb.append("c.coce_cd_plazo                                                                ||','||\n");
				sb.append("(select to_char(min(re.care_fe_desde),'dd/mm/yyyy')\n");
				sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
				sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
				sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
				sb.append("and re.care_cace_nu_certificado     = 0\n");
				sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
				sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
				sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            ||','||\n");
				sb.append("(select to_char(add_months(min(re.care_fe_desde),12),'dd/mm/yyyy')\n");
				sb.append("from cart_recibos re\n");
				sb.append("where re.care_casu_cd_sucursal        = r.core_casu_cd_sucursal\n");
				sb.append("and re.care_carp_cd_ramo            = r.core_carp_cd_ramo\n");
				sb.append("and re.care_capo_nu_poliza          = r.core_capo_nu_poliza\n");
				sb.append("and re.care_cace_nu_certificado     = 0\n");
				sb.append("and re.care_st_recibo               in (1,2,4)\n");
				sb.append("and re.care_nu_consecutivo_cuota    = 1\n");
				sb.append("and re.care_fe_desde                <= r.core_fe_desde\n");
				sb.append("and add_months(re.care_fe_desde,12) > r.core_fe_desde)            ||','||\n");
				sb.append("to_char(c.coce_fe_suscripcion  ,'dd/mm/yyyy')                               ||','||\n");
				sb.append("to_char(c.coce_fe_desde  ,'dd/mm/yyyy')                                       ||','||\n");
				sb.append("to_char(c.coce_fe_hasta  ,'dd/mm/yyyy')                                     ||','||\n");
				sb.append("to_char(c.coce_fe_ini_credito ,'dd/mm/yyyy')                                ||','||\n");
				sb.append(
						"to_char(c.coce_fe_fin_credito ,'dd/mm/yyyy')                                                           ||','||\n");
				sb.append("c.coce_campov6                                    ||','||\n");
				sb.append("to_char(c.coce_fe_anulacion  ,'dd/mm/yyyy')                                 ||','||\n");
				sb.append("to_char(c.coce_fe_anulacion_col ,'dd/mm/yyyy')                              ||','||\n");
				sb.append(
						"trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) ||','||\n");
				sb.append("nvl(c.coce_mt_bco_devolucion,0)                                                  ||','||\n");
				sb.append("nvl(c.coce_mt_devolucion,0)                                                    ||','||\n");
				sb.append(
						"decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)                        ||','||\n");
				sb.append("case when pf.copa_nvalor5 = 0\n");
				sb.append(
						"then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1\n");
				sb.append("then c.coce_mt_prima_pura\n");
				sb.append("else 0 end ||','||\n");
				sb.append("--\n");
				sb.append(
						"to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) ||','||\n");
				sb.append(
						"to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) ||','||\n");
				sb.append(
						"to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) ||','||\n");
				sb.append("case when pf.copa_nvalor5 = 0 then\n");
				sb.append(
						"nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)\n");
				sb.append("when pf.copa_nvalor5 = 1 then\n");
				sb.append(
						"nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)\n");
				sb.append(
						"+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)\n");
				sb.append("+ nvl(c.coce_mt_prima_pura,0)\n");
				sb.append("else 0 end ||','||\n");
				sb.append(
						"round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),2) ||','||\n");
				sb.append("--\n");
				sb.append(
						"round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
				sb.append("--\n");
				sb.append("(select sum (cob.cocb_ta_riesgo) tasaVida\n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2) ||','||\n");
				sb.append(
						"round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) *\n");
				sb.append("(select sum (cob.cocb_ta_riesgo) tasaDes           \n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)\n");
				sb.append("end,2)                                   ||','||\n");
				sb.append("cl.cocn_cd_estado                                  ||','||\n");
				sb.append("es.caes_de_estado                                  ||','||\n");
				sb.append("cl.cocn_delegmunic                                 ||','||\n");
				sb.append("cl.cocn_cd_postal                                  ||','||\n");
				sb.append("c.coce_capu_cd_producto                                 ||','||\n");
				sb.append("c.coce_capb_cd_plan                                     ||','||\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  <> '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         ||','||\n");
				sb.append("(select sum (cob.cocb_ta_riesgo)\n");
				sb.append("from colectivos_coberturas cob\n");
				sb.append("where cob.cocb_casu_cd_sucursal =  1\n");
				sb.append("and cob.cocb_carp_cd_ramo     = 61\n");
				sb.append("and cob.cocb_carb_cd_ramo     = 14\n");
				sb.append("and cob.cocb_cacb_cd_cobertura  = '003'\n");
				sb.append("and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto\n");
				sb.append("and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan\n");
				sb.append("and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)         ||','||\n");
				sb.append("case when pf.copa_nvalor4 > 0 then\n");
				sb.append("(select to_char(cln.cocn_fe_nacimiento ,'dd/mm/yyyy')   ||'|'||\n");
				sb.append("cln.cocn_cd_sexo         ||'|'||\n");
				sb.append("clc.cocc_tp_cliente      ||'|'||\n");
				sb.append("p.copa_vvalor1           ||'|'||\n");
				sb.append("substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)\n");
				sb.append("from colectivos_cliente_certif clc\n");
				sb.append(",colectivos_clientes       cln\n");
				sb.append(",colectivos_parametros     p\n");
				sb.append("where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
				sb.append("and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo\n");
				sb.append("and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
				sb.append("and clc.cocc_nu_certificado   = c.coce_nu_certificado\n");
				sb.append("and nvl(clc.cocc_tp_cliente,1)  > 1\n");
				sb.append("and cln.cocn_nu_cliente       = clc.cocc_nu_cliente\n");
				sb.append("and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
				sb.append("and p.copa_id_parametro(+)    = 'TIPO'\n");
				sb.append("and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)\n");
				sb.append("else '' end                                 ||','||\n");
				sb.append("to_char(c.COCE_FE_SUSCRIPCION ,'dd/mm/yyyy')                            ||','||\n");
				sb.append("to_char(c.COCE_FE_CARGA ,'dd/mm/yyyy')                              liena2\n");
				sb.append("from colectivos_certificados   c                     \n");
				sb.append(",colectivos_cliente_certif cc\n");
				sb.append(",colectivos_clientes       cl\n");
				sb.append(",colectivos_facturacion    fac\n");
				sb.append(",colectivos_recibos         r\n");
				sb.append(",cart_estados              es\n");
				sb.append(",alterna_estatus           est\n");
				sb.append(",colectivos_certificados   head   \n");
				sb.append(",alterna_planes            pl\n");
				sb.append(",colectivos_parametros     pf\n");
				/*
				 * sb.append("where fac.cofa_casu_cd_sucursal  = 1  -- CANAL\n"
				 * );
				 * sb.append("and fac.cofa_carp_cd_ramo      = 65   -- RAMO\n");
				 * sb.
				 * append("and fac.cofa_capo_nu_poliza    = 16511 -- POLIZA\n");
				 * sb.
				 * append("and fac.cofa_fe_facturacion    >= to_date('28/06/2018','dd/mm/yyyy')\n"
				 * ); sb.
				 * append("and fac.cofa_fe_facturacion    <= to_date('01/07/2018','dd/mm/yyyy')\n"
				 * );
				 */

				sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
				sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
				sb.append("and fac.cofa_capo_nu_poliza    = 16511 -- POLIZA\n");
				sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
				sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
				sb.append("--\n");
				sb.append("and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");
				sb.append("and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo\n");
				sb.append("and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza\n");
				sb.append("and c.coce_nu_certificado   > 0\n");
				sb.append("and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal\n");
				sb.append("and c.coce_campon2          in (0,2008)\n");
				sb.append("--\n");
				sb.append("and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal\n");
				sb.append("and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal\n");
				sb.append("and r.core_st_recibo        in (1,4)\n");
				sb.append("--\n");
				sb.append("and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
				sb.append("and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo       \n");
				sb.append("and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
				sb.append("and cc.cocc_nu_certificado   = c.coce_nu_certificado\n");
				sb.append("and nvl(cc.cocc_tp_cliente,1)       = 1\n");
				sb.append("--\n");
				sb.append("and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal\n");
				sb.append("and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo\n");
				sb.append("and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza\n");
				sb.append("and head.coce_nu_certificado   = 0\n");
				sb.append("--\n");
				sb.append("and cl.cocn_nu_cliente       = cc.cocc_nu_cliente\n");
				sb.append("--\n");
				sb.append("and es.caes_cd_estado(+)     = cl.cocn_cd_estado\n");
				sb.append("--\n");
				sb.append("and est.ales_cd_estatus      = c.coce_st_certificado\n");
				sb.append("--\n");
				sb.append("and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo\n");
				sb.append("and pl.alpl_cd_producto      = c.coce_capu_cd_producto\n");
				sb.append("and pl.alpl_cd_plan          = c.coce_capb_cd_plan\n");
				sb.append("--                                    \n");
				sb.append("and pf.copa_des_parametro    = 'POLIZA'                \n");
				sb.append("and pf.copa_id_parametro     = 'GEPREFAC'                \n");
				sb.append("and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal        \n");
				sb.append("and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo\n");
				sb.append(
						"and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,substr(fac.cofa_capo_nu_poliza,0,3))\n");
				sb.append("and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0'))\n");

			} else {

				if (chk1 == true) {
					sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1		     ESTATUS   				 				\n");
					sb.append("      ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n");
					sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
					if (poliza1 != 0) {
						sb.append(",(select min(re.care_fe_desde)										\n");
						sb.append("    from cart_recibos re												\n");
						sb.append("    where re.care_casu_cd_sucursal        = r.care_casu_cd_sucursal	\n");
						sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
						sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
						sb.append("      and re.care_cace_nu_certificado     = 0						\n");
						sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
						sb.append(
								"      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_INICIO_POLIZA	\n");
						sb.append(",(select add_months(min(re.care_fe_desde),12)						\n");
						sb.append("    from cart_recibos re												\n");
						sb.append("    where re.care_casu_cd_sucursal      	 = r.care_casu_cd_sucursal	\n");
						sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
						sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
						sb.append("      and re.care_cace_nu_certificado     = 0						\n");
						sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
						sb.append(
								"      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_FIN_POLIZA	\n");

					} else {
						sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
						sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					}
					sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
					sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.coce_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo		\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");
					sb.append("      ,nvl(c.coce_buc_empresa,0)		  CREDITONUEVO			     		\n");
					sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados   c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,colectivos_facturacion    fac 									\n");
					sb.append("      ,cart_recibos   			r 									    \n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
					sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
					sb.append(cadenafac);
					sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
					sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
					sb.append("  -- \n");
					sb.append("  and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n");
					sb.append("  and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n");
					sb.append("  and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n");
					sb.append("  and c.coce_nu_certificado   > 0 										\n");
					sb.append("  and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal 				\n");
					sb.append("  and c.coce_campon2          in (0,2008) 				                \n");
					sb.append("  -- \n");
					sb.append("  and r.care_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and r.care_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n");
					sb.append("  and r.care_st_recibo        in (1,4)									\n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'								\n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,1,substr(fac.cofa_capo_nu_poliza,0,3),2,substr(fac.cofa_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");

					sb.append("\n");
					sb.append("union all \n");
					sb.append("\n");

					sb.append("select c.cocm_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.cocm_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.cocm_nu_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.cocm_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1		     ESTATUS   				 				\n");
					sb.append("      ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n");
					sb.append("      ,c.cocm_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.cocm_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.cocm_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.cocm_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.cocm_cd_plazo          PLAZO 									\n");
					if (poliza1 != 0) {
						sb.append(",(select min(re.care_fe_desde)										\n");
						sb.append("    from cart_recibos re												\n");
						sb.append("    where re.care_casu_cd_sucursal        = r.care_casu_cd_sucursal	\n");
						sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
						sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
						sb.append("      and re.care_cace_nu_certificado     = 0						\n");
						sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
						sb.append(
								"      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_INICIO_POLIZA	\n");
						sb.append(",(select add_months(min(re.care_fe_desde),12)						\n");
						sb.append("    from cart_recibos re												\n");
						sb.append("    where re.care_casu_cd_sucursal        = r.care_casu_cd_sucursal	\n");
						sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
						sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
						sb.append("      and re.care_cace_nu_certificado     = 0						\n");
						sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
						sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
						sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
						sb.append(
								"      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_FIN_POLIZA	\n");

					} else {
						sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
						sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					}
					sb.append("      ,c.cocm_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.cocm_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.cocm_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.cocm_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.cocm_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.cocm_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.cocm_fe_anulacion_real FECHA_ANULACION 							\n");
					sb.append("      ,c.cocm_fe_anulacion      FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.cocm_sub_campana,'7',to_number(c.cocm_mt_suma_aseg_si),c.cocm_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.cocm_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1) -instr(c.cocm_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2) -instr(c.cocm_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.cocm_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.cocm_mt_prima_pura*1000/decode(c.cocm_sub_campana,'7',decode(c.cocm_mt_suma_asegurada,0,1,c.cocm_mt_suma_asegurada),decode(c.cocm_mt_suma_aseg_si,0,1,c.cocm_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.cocm_carp_cd_ramo in (61,65) then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.cocm_carp_cd_ramo 		\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.cocm_carp_cd_ramo = 61 then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.cocm_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.cocm_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.cocm_mt_devolucion ,0) - nvl(c.cocm_mt_bco_devolucion ,0) ) DIFERENCIA_DEVOLUCION \n");
					sb.append("      ,nvl(c.cocm_buc_empresa,0)		  CREDITONUEVO			     		\n");
					sb.append(",c.cocm_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.cocm_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.cocm_empresa,instr(c.cocm_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.cocm_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.cocm_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeEn:',1,1) + 5,instr(c.cocm_campoV6, '|',1,6)-instr(c.cocm_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeCar:',1,1) + 6,instr(c.cocm_campoV6, '|',1,8)-instr(c.cocm_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeRe:',1,1) + 5,instr(c.cocm_campoV6, '|',1,7)-instr(c.cocm_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados_mov c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,colectivos_facturacion    fac 									\n");
					sb.append("      ,cart_recibos   			r 									    \n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
					sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
					sb.append(cadenafac);
					sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
					sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
					sb.append("  -- \n");
					sb.append("  and r.care_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and r.care_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n");
					sb.append("  and r.care_st_recibo        in (1,4)									\n");
					sb.append("  -- \n");
					sb.append("  and c.cocm_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n");
					sb.append("  and c.cocm_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n");
					sb.append("  and c.cocm_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n");
					sb.append("  and c.cocm_nu_certificado   > 0 										\n");
					sb.append("  and c.cocm_nu_recibo        = fac.cofa_nu_recibo_fiscal 				\n");
					sb.append("  and c.cocm_campon2          in (0,2008) 				                \n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.cocm_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.cocm_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.cocm_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.cocm_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.cocm_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.cocm_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'								\n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n");
					sb.append("  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,1,substr(fac.cofa_capo_nu_poliza,0,3),2,substr(fac.cofa_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");

				}
				if (chk1 == true && chk2 == true)
					sb.append("  union all \n");
				if (chk2 == true) {
					sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");
					sb.append("      ,'BAJA'                   ESTATUS_MOVIMIENTO						\n");
					sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
					sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
					sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
					sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.coce_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo 		\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("            and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");
					sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
					sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados   c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
					sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
					sb.append(cadenacan);
					sb.append("  and c.coce_nu_certificado   > 0			 							\n");
					sb.append("  and c.coce_st_certificado   in (10,11)  	     				        \n");
					sb.append("  and c.coce_cd_causa_anulacion <> 45			  	     				\n");
					sb.append("  and c.coce_fe_anulacion    >= to_date('" + fecha1 + "','dd/mm/yyyy')   \n");
					sb.append("  and c.coce_fe_anulacion    <= to_date('" + fecha2 + "','dd/mm/yyyy')   \n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
					sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,c.coce_capo_nu_poliza,1,substr(c.coce_capo_nu_poliza,0,3),2,substr(c.coce_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
				}
				if ((chk1 == true || chk2 == true) && chk3 == true)
					sb.append("  union all \n");
				if (chk3 == true) {
					sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
					sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
					sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
					sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
					sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
					sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
					sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
					sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");
					sb.append("      ,'EMITIDO'                ESTATUS_MOVIMIENTO						\n");
					sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
					sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
					sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
					sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
					sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
					sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
					sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
					sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
					sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
					sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
					sb.append("      ,cl.cocn_rfc              RFC 										\n");
					sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
					sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
					sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
					sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
					sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
					sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
					sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
					sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
					sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
					sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
					sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
					sb.append(
							"      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
					sb.append(
							"      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 									\n");
					sb.append(
							"                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1									\n");
					sb.append("                      then c.coce_mt_prima_pura							\n");
					sb.append("                else 0 end PRIMA_NETA									\n");
					sb.append("      -- \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
					sb.append(
							"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
					sb.append("      ,case when pf.copa_nvalor5 = 0 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n");
					sb.append("                when pf.copa_nvalor5 = 1 then \n");
					sb.append(
							"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n");
					sb.append(
							"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n");
					sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n");
					sb.append("                else 0 end PRIMA_TOTAL  									 \n");
					sb.append(
							"      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
					sb.append("      -- \n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("      -- \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo   	\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_VIDA 											\n");
					sb.append(
							"      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
					sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
					sb.append("              from colectivos_coberturas cob 							\n");
					sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
					sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
					sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
					if (poliza1 != 0)
						sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
					sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
					sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
					sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
					sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
					sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
					sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
					sb.append("      ,es.caes_de_estado        ESTADO 									\n");
					sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
					sb.append("      ,cl.cocn_cd_postal        CP 										\n");
					sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
					sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
					sb.append(
							"      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION   \n");
					sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
					sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n");
					sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n");
					sb.append("        from colectivos_coberturas cob                    \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n");
					sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n");
					sb.append("        from colectivos_coberturas cob                                       \n");
					sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n");
					sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n");
					sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n");
					sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n");
					sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n");
					sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n");
					sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n");
					sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n");
					sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n");
					sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n");
					sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n");
					sb.append(
							"           p.copa_vvalor1           ||'|'||                                                 \n");
					sb.append(
							"           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n");
					sb.append(
							"      from colectivos_cliente_certif clc                                                    \n");
					sb.append(
							"          ,colectivos_clientes       cln                                                    \n");
					sb.append("          ,colectivos_parametros     p                                       \n");
					sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n");
					sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n");
					sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n");
					sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n");
					sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n");
					sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n");
					sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n");
					sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n");
					sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n");
					sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
					if (ramo == 9 && inIdVenta == 6) { /* APV */
						sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
						sb.append(
								"  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
						sb.append(
								"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
					} else {
						sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
					} /* APV */
					sb.append("  from colectivos_certificados   c 										\n");
					sb.append("      ,colectivos_cliente_certif cc 										\n");
					sb.append("      ,colectivos_clientes       cl 										\n");
					sb.append("      ,cart_estados              es 										\n");
					sb.append("      ,alterna_estatus           est 									\n");
					sb.append("      ,colectivos_certificados   head 									\n");
					sb.append("      ,alterna_planes            pl 										\n");
					sb.append("      ,colectivos_parametros     pf										\n");
					sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
					sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
					sb.append(cadenacan);
					sb.append("  and c.coce_nu_certificado   > 0			 							\n");
					// sb.append(" and c.coce_st_certificado in (" + status + ")
					// \n");
					// sb.append(" and c.coce_cd_causa_anulacion <> 45 \n");
					sb.append("  and c.coce_fe_carga    >= to_date('" + fecha1 + "','dd/mm/yyyy')   \n");
					sb.append("  and c.coce_fe_carga    <= to_date('" + fecha2 + "','dd/mm/yyyy')   \n");
					sb.append("  -- \n");
					sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
					sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
					sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
					sb.append("  -- \n");
					sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
					sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
					sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
					sb.append("  and head.coce_nu_certificado   = 0 									\n");
					sb.append("  -- \n");
					sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
					sb.append("  -- \n");
					sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
					sb.append("  -- \n");
					sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
					sb.append("  -- \n");
					sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
					sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
					sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
					sb.append("--																		\n");
					sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
					sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
					sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
					sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
					sb.append(
							"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,c.coce_capo_nu_poliza,1,substr(c.coce_capo_nu_poliza,0,3),2,substr(c.coce_capo_nu_poliza,0,2)) \n");
					sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
				}
			}

			String nombre="";
			
			Format formatter1;
			Format formatter2;
			formatter1 = new SimpleDateFormat("ddMMyyyy");
			formatter2 = new SimpleDateFormat("dd/MM/yyyy");


			String sts = "";
			String fec = null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
			fec = formatter1.format(nowDate);
			fec1 = formatter2.format(nowDate);
			
			int hora=0;
		    int minuto=0;
		    int segundo=0;
		    
		    now.setTime(nowDate);
			  
		    hora     = now.get(Calendar.HOUR_OF_DAY);
		    minuto   = now.get(Calendar.MINUTE);
		    segundo  = now.get(Calendar.SECOND);

			if (chk1 == true)
				sts += "VIG";
			if (chk2 == true)
				sts += "CAN";
			if (chk3 == true)
				sts += "EMI";
			
			if(minuto < 10){
		    	nombre = "REPEMI_" + sts + "_" + poliza1 + "_" + fec+ "_" + hora+"0"+minuto+segundo+  ".csv" ;
		    	fecha_inicio =fec1+" "+ hora+":0"+minuto+":"+segundo;
		    	Log.info(nombre);
		    }else{
		    	nombre = "REPEMI_" + sts + "_" + poliza1 + "_" + fec+ "_" + hora+minuto+segundo+  ".csv" ;
		    	fecha_inicio =fec1+" "+ hora+":"+minuto+":"+segundo;
		    	Log.info(nombre);
		    } 
			
			// nombre += ".csv";

			Log.info("QUERY");

			Log.info(sb.toString());
			Log.info("-------------------------------------------------------------------");
			query2 = queryReporteTecnico.RecibirQueryReporteTecnico(sb.toString());
            if(query2=="Error"){
                nombre = query2;
            }else {
            	Log.info("Reporte: "+nombre);
            	queryReporteTecnico.insertarQueryReporteTecnico(query2, nombre, usuario,fecha_inicio, Constantes.DEFAULT_STRING);
            }
			Log.info("-------------------------------------------------------------------");
			return nombre;

		} catch (Exception e) {
			Log.info("Error al inserta reporte",e);
			throw new Excepciones("Error.", e);
		}
	}

	

}
