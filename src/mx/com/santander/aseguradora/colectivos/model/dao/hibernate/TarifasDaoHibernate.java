/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.dao.TarifasDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Ing. Hery Tyler
 *
 */
@Repository
public class TarifasDaoHibernate extends HibernateDaoSupport implements TarifasDao {

	@Autowired
	public TarifasDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getConsultaTarifas(Integer canal, Short ramo, Integer producto) throws Excepciones {
		List<Object> lista; 
		
		try {
			StringBuilder consulta1 = new StringBuilder() ;
			
            consulta1.append("select distinct cober.RAMO                    RAMO                \n");
			consulta1.append("     , cober.PROD                             PRODUCTO            \n");
			consulta1.append("     , cober.PLAN                             PLAN                \n");
			consulta1.append("     , pro.alpr_de_producto                   DESC_PROD           \n");
			consulta1.append("     , pl.alpl_de_plan                        DESC_PLAN           \n");
			consulta1.append("     , pl.alpl_dato2                          DESC_NEGOCIO        \n");           
			consulta1.append("     , cober.TARIFA_COB                       TARIFA_TECNICAS     \n");
			consulta1.append("     , prop.hopp_dato1                        TARIFAS_OPERATIVAS  \n");
			consulta1.append("     , nvl(pl.alpl_dato3, 0)                  CENTRO_COSTOS       \n");
			consulta1.append("     , cober.FE_DESDE                         FEC_DESDE           \n");
			consulta1.append("     , cober.FE_HASTA                         FEC_HAST            \n");
			consulta1.append("     , cober.NU_COB                           NUM_COBERTURA       \n");
		    consulta1.append("   from homologa_prodplanes prop                                  \n");
		    consulta1.append("       ,alterna_planes      pl                                    \n");
		    consulta1.append("       ,alterna_productos   pro                                   \n");
		    consulta1.append("       ,(select coberturas.ramo                    RAMO           \n");		    
		    consulta1.append("              , coberturas.nu_cob                  NU_COB         \n");
		    consulta1.append("              , coberturas.prod                    PROD           \n");
		    consulta1.append("              , coberturas.plan                    PLAN           \n");
		    consulta1.append("              , coberturas.fe_desde                FE_DESDE       \n");
		    consulta1.append("              , coberturas.fe_hasta                FE_HASTA       \n");
		    consulta1.append("              , sum(coberturas.cocb_ta_riesgo)     TARIFA_COB     \n");
		    consulta1.append("     from( select distinct col.cocb_carp_cd_ramo   RAMO           \n");
		    consulta1.append("                  ,col.cocb_cer_nu_cobertura       NU_COB         \n");
		    consulta1.append("                  ,col.cocb_capu_cd_producto       PROD           \n");
		    consulta1.append("                  ,col.cocb_capb_cd_plan           PLAN           \n");
		    consulta1.append("                  ,col.cocb_fe_desde               FE_DESDE       \n");
		    consulta1.append("                  ,col.cocb_fe_hasta               FE_HASTA       \n");
		    consulta1.append("                  ,col.cocb_ta_riesgo                             \n");		    
		    consulta1.append("                from colectivos_coberturas col                    \n");
		    consulta1.append("           where col.cocb_casu_cd_sucursal  = :canal              \n");
		    consulta1.append("              and col.cocb_carp_cd_ramo     = :ramo               \n");
		    consulta1.append("              and col.cocb_capu_cd_producto = :producto           \n");
		    consulta1.append("              and col.cocb_fe_hasta         > sysdate             \n");
		    consulta1.append("              and col.cocb_estatus          = 20 ) coberturas     \n");
            consulta1.append("            group by coberturas.ramo                              \n");
			consulta1.append("                   , coberturas.nu_cob                            \n");
			consulta1.append("                   , coberturas.prod                              \n");
			consulta1.append("					 , coberturas.plan                              \n");
			consulta1.append("					 , coberturas.fe_desde                          \n");
			consulta1.append("					 , coberturas.fe_hasta ) cober                  \n");
		    consulta1.append("            where prop.hopp_cd_ramo         = cober.RAMO          \n");
		    consulta1.append("              and prop.hopp_prod_colectivo  = cober.PROD          \n");
		    consulta1.append("              and prop.hopp_plan_colectivo  = cober.PLAN          \n");
		    consulta1.append("              and pl.alpl_cd_ramo           = cober.RAMO          \n");
			consulta1.append("              and pl.alpl_cd_producto       = cober.PROD          \n");
			consulta1.append("              and pl.alpl_cd_plan           = cober.PLAN          \n");
			consulta1.append("			    and pro.alpr_cd_ramo          = cober.RAMO          \n");
			consulta1.append("              and pro.alpr_cd_producto      = cober.PROD          \n");
			consulta1.append("   order by 3             										\n");	
			
			Query qry = this.getSession().createSQLQuery(consulta1.toString());
			
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("producto", producto);
			
			lista = qry.list();
			
			return lista;
		}catch (Exception e){
			throw new Excepciones ("Error .",e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getConsultaCoberturas(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones {
		List<Object> lista; 
		
		try {
			StringBuilder consulta2 = new StringBuilder() ;
			
			consulta2.append("select cb.cocb_cacb_cd_cobertura   COD_COBERTURA          \n");
			consulta2.append("     , cob.cacb_de_cobertura       DESC_COBERTURA         \n");
			consulta2.append("     , cb.cocb_ta_riesgo           TARIFA                 \n");
			consulta2.append("     , ae.ales_descripcion         STATUS                 \n");
			consulta2.append("     , cb.cocb_fe_desde            FEC_INI_VIGENCIA       \n");
			consulta2.append("     , cb.cocb_fe_hasta            FEC_FIN_VIGENCIA       \n");
			consulta2.append("     , cb.cocb_edad_minima         EDAD_MIN               \n");
			consulta2.append("     , cb.cocb_edad_maxima         EDAD_MAX               \n");
			consulta2.append("     , cb.cocb_campov1 * 100       PORCENTAJE             \n");
			consulta2.append("     , cb.cocb_cer_nu_cobertura    SET_COBERTURAS         \n");
			consulta2.append("  from colectivos_coberturas  cb                          \n");
			consulta2.append("     , cart_coberturas        cob                         \n");
			consulta2.append("     , alterna_estatus        ae                          \n");
			consulta2.append(" where cb.cocb_casu_cd_sucursal  = :canal                 \n");
			consulta2.append("   and cb.cocb_carp_cd_ramo      = :ramo                  \n");
			consulta2.append("   and cb.cocb_capu_cd_producto  = :producto              \n");
			consulta2.append("   and cb.cocb_capb_cd_plan      = :plan                  \n");
			consulta2.append("	 and cb.cocb_carb_cd_ramo      = cob.cacb_carb_cd_ramo  \n");
			consulta2.append("   and cb.cocb_cacb_cd_cobertura = cob.cacb_cd_cobertura  \n");
			consulta2.append("   and cb.cocb_estatus  = ae.ales_cd_estatus              \n");
			consulta2.append("group by cb.cocb_cacb_cd_cobertura             			\n");
			consulta2.append("     , cob.cacb_de_cobertura               				\n");
			consulta2.append("     , cb.cocb_ta_riesgo                          	 	\n");
			consulta2.append("     , ae.ales_descripcion                         		\n");
			consulta2.append("     , cb.cocb_fe_desde                   				\n");
			consulta2.append("     , cb.cocb_fe_hasta                   				\n");
			consulta2.append("     , cb.cocb_edad_minima                        		\n");
			consulta2.append("     , cb.cocb_edad_maxima                        		\n");
			consulta2.append("     , (cb.cocb_campov1 * 100)                   			\n");
			consulta2.append("     , cb.cocb_cer_nu_cobertura             				\n");
			consulta2.append(" order by 4, 5 desc										\n");
			
			
            Query qry = this.getSession().createSQLQuery(consulta2.toString());
			
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("producto", producto);
			qry.setParameter("plan", plan);
			
			lista = qry.list();
			
			return lista;
		}catch (Exception e){
			throw new Excepciones ("Error .",e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getConsultaComponentes(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones {
		List<Object> lista; 
		
		try {
			StringBuilder consulta3 = new StringBuilder() ;
			
			consulta3.append("select com.coct_capp_cd_componente  COMPONENTE        \n");
			consulta3.append("     , com.coct_ta_componente       TASA              \n");
			consulta3.append("     , e.ales_descripcion           STATUS            \n");
			consulta3.append("     , com.coct_fe_desde            FEC_INI_VIGENCIA  \n"); 
			consulta3.append("     , com.coct_fe_hasta            FEC_FIN_VIGENCIA  \n");
			consulta3.append("     , com.coct_cer_nu_componente   SET_COBERTURAS    \n");
			consulta3.append("  from colectivos_componentes   com                   \n");
			consulta3.append("     , alterna_estatus          e                     \n");
			consulta3.append(" where com.coct_casu_cd_sucursal = :canal             \n");
			consulta3.append("   and com.coct_carp_cd_ramo     = :ramo              \n");
			consulta3.append("   and com.coct_capu_cd_producto = :producto          \n");
			consulta3.append("   and com.coct_capb_cd_plan     = :plan              \n");
			consulta3.append("   and com.coct_estatus = e.ales_cd_estatus           \n");
			consulta3.append(" group by com.coct_capp_cd_componente     			\n");
			consulta3.append(" , com.coct_ta_componente                     		\n");
			consulta3.append(" , e.ales_descripcion                    				\n");
			consulta3.append(" , com.coct_fe_desde   								\n");
			consulta3.append(" , com.coct_fe_hasta   								\n");
			consulta3.append(" , com.coct_cer_nu_componente 						\n");
			consulta3.append(" order by 3, 4 desc    \n");
			
            Query qry = this.getSession().createSQLQuery(consulta3.toString());
			
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("producto", producto);
			qry.setParameter("plan", plan);
			
			lista = qry.list();
			
			return lista;
		}catch (Exception e){
			throw new Excepciones ("Error .",e);
		}
	}	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getConsultaHomologa(Short ramo, Integer producto, Short plan) throws Excepciones {
		List<Object> lista; 
		
		try {
			StringBuilder consulta4 = new StringBuilder() ;
			
			consulta4.append("select hp.hopp_cd_ramo            RAMO                     \n");
			consulta4.append("     , hp.hopp_prod_banco_rector  SUB_PRODUCTO             \n");
			consulta4.append("     , hp.hopp_plan_banco_rector  SUB_TIPO                 \n");
			consulta4.append("     , hp.hopp_de_producto        DESCRIPCION              \n");
			consulta4.append("     , hp.hopp_dato3              IDENTIFICADOR            \n");
			consulta4.append("  from homologa_prodplanes  hp                             \n"); 
			consulta4.append(" where hp.hopp_cd_ramo        = :ramo                      \n");
			consulta4.append("   and hp.hopp_prod_colectivo = :producto                  \n");
			consulta4.append("   and hp.hopp_plan_colectivo = :plan                      \n");
			
            Query qry = this.getSession().createSQLQuery(consulta4.toString());
			
			qry.setParameter("ramo", ramo);
			qry.setParameter("producto", producto);
			qry.setParameter("plan", plan);
			
			lista = qry.list();
			
			return lista;
		}catch (Exception e){
			throw new Excepciones ("Error .",e);
		}
	}	
			
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		return null;
	}

	@SuppressWarnings("deprecation")
	public void reporteDetalleTarifas(String rutaTemporal, String rutaReporte,
			Map<String, Object> inParams) throws Excepciones {
		// TODO Auto-generated method stub
		JasperReport reporte;
		JasperPrint datos;
		
		try{
			reporte = JasperCompileManager.compileReport(rutaReporte);
			datos = JasperFillManager.fillReport(reporte, inParams, getSession().connection());
			
			JRXlsExporter exporter = new JRXlsExporter();
			File file = new File(rutaTemporal);
			
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, datos);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, file.toString());
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 65536);			
			
			exporter.exportReport();
			
		}catch(Exception e){
			throw new Excepciones("Error.", e);
		}
	}
	
}