package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao;
import mx.com.santander.aseguradora.colectivos.utils.Constantes.OPC_FORMATO_REPORTE;

public interface ReporteadorDao extends CatalogoDao,Serializable {

	void reporteIntegracion(String rutaTemporal, String rutaReporte, Map<String, Object> inParams, OPC_FORMATO_REPORTE opcr) throws Exception;

}
