/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.File;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.dao.ReporteadorDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes.OPC_FORMATO_REPORTE;

/**
 * @author dflores
 *
 */
@Repository
public class ReporteadorDaoHibernate extends HibernateDaoSupport implements ReporteadorDao {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7368402558337693466L;

	@Autowired
	public ReporteadorDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuilder consulta = new StringBuilder();
					consulta.append("from Reporteador  re    \n");
					consulta.append("where 1=1  \n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					List<T> lista = qry.list();
					
					return lista;
				}
				
				
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("deprecation")
	public void reporteIntegracion(String rutaTemporal, String rutaReporte,
			Map<String, Object> inParams, OPC_FORMATO_REPORTE opcr) throws Exception {
		// TODO Auto-generated method stub
		JasperReport reporte;
		JasperPrint datos;
		
		
		try{
			reporte = JasperCompileManager.compileReport(rutaReporte);
			datos = JasperFillManager.fillReport(reporte, inParams, getSession().connection());
			
			File file = new File(rutaTemporal);
			
			if (opcr == OPC_FORMATO_REPORTE.XLS) {
				JRXlsExporter exporter = new JRXlsExporter();
				
				
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, datos);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, file.toString());
				exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
	            exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
	            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
	            exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
	            exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 65536);			
				
				exporter.exportReport();
			}
			
		}catch(Exception e){
			throw new Excepciones("Error.", e);
		}
	}
}
