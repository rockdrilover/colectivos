package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.BitacoraErroresDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Interface que implementa la clase ErrorEmisionDaoHibernate.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
 @Local
public interface ErrorEmisionDao extends Serializable {
	
	/**
	 * Metodo que borra el error en tablas control
	 * @param dto datos para borrar
	 * @throws Excepciones con error generar
	 */
	void eliminaError(BitacoraErroresDTO dto) throws Excepciones;

}
