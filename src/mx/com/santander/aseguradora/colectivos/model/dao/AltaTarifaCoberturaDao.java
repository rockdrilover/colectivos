package mx.com.santander.aseguradora.colectivos.model.dao;


import java.util.Date;
import java.util.List;


/**
 * @author dflores
 *
 */

public interface AltaTarifaCoberturaDao<AltaTarifaCober> {
	
	public abstract AltaTarifaCober saveAltaTarifaCober(AltaTarifaCober altaTarifaCober);
	/**
	 * 
	 * @param lista
	 */
	public abstract void saveListaAltaTarifaCober(List<Object> lista);
	/**
	 * 
	 * @param id
	 * @return
	 */
	
	public abstract List<AltaTarifaCober> getListaAltaTarifaCober();
	/**
	 * 
	 * @param ramo
	 * @param poliza
	 * @return
	 */
	public abstract List<AltaTarifaCober> getListaAltaTarifaCober( Integer canal,Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, Float taRiesgo, Integer edadMin, Integer edadMax);
	/**
	 * 
	 * @param canal
	 * @param ramo
	 * @param poliza
	 */
	public abstract Integer iniciaAlta( Integer canal, Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, Float taRiesgo, Integer edadMin, Integer edadMax);
	

	

}
