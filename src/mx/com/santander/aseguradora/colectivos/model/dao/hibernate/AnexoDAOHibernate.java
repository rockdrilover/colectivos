package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosAnexo;
import mx.com.santander.aseguradora.colectivos.model.dao.AnexoDAO;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
@Repository
public class AnexoDAOHibernate extends HibernateDaoSupport implements
		AnexoDAO {

	
	/**
	 * Constructor de sesion hibernate.
	 * @param sessionFactory Session factory hibernate
	 */
	@Autowired
	public AnexoDAOHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	
	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		
		return (T)this.getHibernateTemplate().get(ColectivosAnexo.class, id);
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().update(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);

		}

	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ColectivosAnexo> consultaAnexoSuscripcion(
			ColectivosAnexo filtro)
			throws Excepciones {
		
		List<ColectivosAnexo> res = null;
		StringBuilder consulta = new StringBuilder();

		consulta.append("from ColectivosAnexo sus \n");
		consulta.append("where  ");
		consulta.append(" sus.colectivosSeguimiento.coseId = ");
		consulta.append(filtro.getColectivosSeguimiento().getCoseId());
		//consulta.append(" and sus.colectivosSuscripcion.coseGrupo = ");
		//consulta.append(filtro.getCoseGrupo());
		//consulta.append(" ");
		consulta.append(" order by sus.coseConsecutivo desc");
		
		logger.info(consulta.toString());
		
		
		try {
			Query qry = this.getSession().createQuery(consulta.toString());
			res = (List<ColectivosAnexo>) qry.list();

		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones(
					"Error consultar seguimiento por suscripcion", e);
		}
		
		return res;
	}


	@Override
	public long consecutivoAnexo(ColectivosAnexo filtro)
			throws Excepciones {
		StringBuilder sbConsulta = new StringBuilder();
		Query qry;
		
		sbConsulta.append("SELECT NVL(MAX(COSE_CONSECUTIVO),0) + 1 \n");
		sbConsulta.append("	FROM COLECTIVOS_ANEXOS SEG \n");
		sbConsulta.append("WHERE COAN_COSE_ID 	= :coseId \n");
		
		
		try {
			qry = this.getSession().createSQLQuery(sbConsulta.toString());
			qry.setParameter("coseId", filtro.getColectivosSeguimiento().getCoseId());
		
			BigDecimal res = (BigDecimal)qry.uniqueResult();
			return res.longValue();
		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones(
					"Error consulta consecutivo seguimiento", e);
		}
	
	
	}

	

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.AnexoDAO#guardarAnexo(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosAnexo)
	 */
	@Override
	public ColectivosAnexo guardarAnexo(ColectivosAnexo guardar) throws Excepciones {
		try {
			guardar.setCoseConsecutivo(new BigDecimal(consecutivoAnexo(guardar)));
			this.getHibernateTemplate().save(guardar);
			return guardar;
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}


	@Override
	public ColectivosAnexo obtieneObjeto(ColectivosAnexo filtro) {
		
		return 	this.getHibernateTemplate().get(ColectivosAnexo.class, filtro.getCoanId());
	}


	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.AnexoDAO#actualizarAnexo(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosAnexo)
	 */
	@Override
	public void actualizarAnexo(ColectivosAnexo guardar) throws Excepciones {
		try {
			this.getHibernateTemplate().update(guardar);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);

		}
	}

	
	

	
	
}
