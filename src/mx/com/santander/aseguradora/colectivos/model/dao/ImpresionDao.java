/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Complementarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.utils.SmbUtil;

/**
 * @author FEBG
 *
 */
public interface ImpresionDao extends CatalogoDao {

	public abstract  <T> List<T> consultaCredCte(String idCertificado, String nombre, String apellidoPaterno, String apellidoMaterno) throws Exception;
	public abstract  <T> List<T> consultaCredCert(String certs) throws Exception;
	public abstract  <T> List<T> consultaCredCobs(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
	public abstract  <T> List<T> consultaCredAsis(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
	public abstract  <T> List<T> consultaCredAsegurados(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
	public abstract  Complementarios consultaCredComplementos(String credito,	Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
    public abstract String impresionCertificado(String rutaTemporal, short canal, short ramo, long poliza, long certificado, Date fecha_impresion ); 	
    public abstract String impresionPoliza(String rutaTemporal, short canal,short ramo, long poliza, Date fecha_emision);
	public abstract String getPlantilla(Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan, Date fechaEmision) throws Exception;
	public abstract ArrayList<Object> getDatosPlantilla(StringBuffer filtro)  throws Exception;
	public abstract Object[] getDatosRecas(Short ramo, String producto) throws Exception;
	public abstract <T> List<T> consultaDatos(Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan) throws Exception;
	public abstract Parametros getRutaPlantilla() throws Exception;
	//carga polizas impresion
	public abstract <T> List<T> cargaPolizasImpresion(int ramo, long poliza) throws Exception;
	//obtenerRegistros
	public abstract <T> List<T> obtenerRegistros(int ramo, long poliza) throws Exception;
	//reporteAsegurado
	public abstract String reporteAsegurado(String rutaTemp, short ramo, long poliza);
   
}

