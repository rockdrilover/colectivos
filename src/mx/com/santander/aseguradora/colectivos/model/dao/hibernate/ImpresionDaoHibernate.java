/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import mx.com.santander.aseguradora.colectivos.model.dao.ImpresionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Complementarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.SmbUtil;

/**
 * @author FEBG
 *
 */
@Repository
public class ImpresionDaoHibernate extends HibernateDaoSupport implements ImpresionDao {	
	@Autowired
	public ImpresionDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuilder consulta = new StringBuilder();
					consulta.append("from ClienteCertif  CC    \n");
					consulta.append("where 1 = 1           \n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					List<T> lista = qry.list();
					
					return lista;
				}
				
				
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaCred(String idCertificado, String nombre, String apellidoPaterno, String apellidoMaterno) throws Exception {
		try {
			System.out.println("Id--"  + idCertificado + "Nombre--" +  nombre  + "Ap_Pat--" + apellidoPaterno + "Ap_Mat--" + apellidoMaterno);
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select \n");
			consulta.append("   -------------------------------- \n");
			consulta.append("   -- INFORMACION DEL CLIENTE       \n");
			consulta.append("   -------------------------------- \n");
			consulta.append("   b.cocc_id_certificado, \n");
			consulta.append("   c.cocn_nombre, \n");
			consulta.append("   c.cocn_apellido_pat, \n");
			consulta.append("	c.cocn_apellido_mat, \n");
			consulta.append("	c.cocn_fe_nacimiento, \n");
			consulta.append("	c.cocn_rfc, \n");
			consulta.append("	c.cocn_buc_cliente, \n");
			consulta.append("   -------------------------------- \n");
			consulta.append("   -- INFORMACION DEL CERTIFICADO   \n");
			consulta.append("   -------------------------------- \n");
			consulta.append("	a.coce_casu_cd_sucursal, 		\n");
			consulta.append("	a.coce_carp_cd_ramo,     		\n");
			consulta.append("	a.coce_capo_nu_poliza,   		\n");
			consulta.append("	a.coce_nu_certificado,   		\n");
			consulta.append("	ap.alpl_de_plan,				\n");
			consulta.append("	a.coce_fe_ini_credito,			\n");
			consulta.append("	a.coce_fe_fin_credito,			\n");
			consulta.append("	a.coce_fe_suscripcion,			\n");
			consulta.append("	a.coce_fe_emision,				\n");
			consulta.append("	a.coce_fe_rehabilitacion,		\n");
			consulta.append("	e.ales_campo1,  				\n");
			consulta.append("	e.ales_descripcion,				\n");
			consulta.append("	a.coce_mt_suma_asegurada,		\n");
			consulta.append("	a.coce_mt_prima_subsecuente,	\n");
			consulta.append("	a.coce_nu_cuenta,				\n");
			consulta.append("	round(round(months_between(a.coce_fe_fin_credito, a.coce_fe_ini_credito))/12), \n");
			consulta.append("   --------------------------------	\n");
			consulta.append("   -- INFORMACION DE ASISTENCIAS   	\n");
			consulta.append("   --------------------------------	\n");
			consulta.append("	  nvl(cts.ctse_de_servicio,'NO TIENE ASISTENCIA'),	\n");
			consulta.append("   --------------------------------	\n");
			consulta.append("   -- INFORMACION DE ASISTENCIAS   	\n");
			consulta.append("   --------------------------------	\n");
			consulta.append("	cacb.cacb_de_cobertura,				\n");
			consulta.append("	cocb.cocb_ta_riesgo,				\n");
			consulta.append("   a.coce_fe_anulacion_col,				\n");
			consulta.append("   nvl(a.coce_buc_empresa,' ')					\n");			
			consulta.append("from \n"); 
			consulta.append("	colectivos_certificados a \n");
			consulta.append("       left join colectivos_parametros par \n");
			consulta.append("            on par.copa_des_parametro = 'POLIZA' \n");
			consulta.append("           and par.copa_id_parametro  = 'GEP' \n");
			consulta.append("           and par.copa_nvalor1       = a.coce_casu_cd_sucursal \n");
			consulta.append("           and par.copa_nvalor2       = a.coce_carp_cd_ramo \n");
			consulta.append("           and par.copa_nvalor3       = DECODE(par.copa_nvalor7, 0, a.coce_capo_nu_poliza, 1, substr(a.coce_capo_nu_poliza,0,3), substr(a.coce_capo_nu_poliza,0,2)) \n");
			consulta.append(" \n");
			consulta.append("		left join alterna_estatus ae on a.coce_cd_causa_anulacion=ae.ales_cd_estatus \n");
			consulta.append("		left join cart_campanias_productos ccp on ccp.ccap_carp_cd_ramo = a.coce_carp_cd_ramo and \n");
			consulta.append("		                                          ccp.ccap_capu_cd_producto  = to_char(a.coce_capu_cd_producto) \n");
			consulta.append("	    left join cart_campanias cc on cc.ccam_cd_campania = ccp.ccap_ccam_cd_campania  \n");
			consulta.append("		left join cart_campanias_serv_prestados ccsp on ccsp.ccse_ccam_cd_campania = cc.ccam_cd_campania \n");
			consulta.append("	    left join cart_tipos_servicios cts on cts.ctse_cd_servicio = ccsp.ccse_ctse_cd_servicio, \n");
			consulta.append("colectivos_cliente_certif b,  \n");
			consulta.append("colectivos_clientes c,  \n");
			consulta.append("alterna_estatus e,  \n");
			consulta.append("alterna_planes ap,  \n");
			consulta.append("colectivos_coberturas cocb,  \n");
			consulta.append("cart_coberturas cacb  \n");
			consulta.append("where 1=1 \n");
			if(idCertificado!= null && idCertificado.length() > 0){
				consulta.append(" and (b.cocc_id_certificado   = '").append(idCertificado).append("' \n");
				consulta.append(" or a.coce_buc_empresa = '").append(idCertificado).append("') \n");
			}
			if(nombre != null && nombre.length() > 0){
				consulta.append(" and c.cocn_nombre       = '").append(nombre.toUpperCase()).append("' \n");
			}
			
			if(apellidoPaterno != null && apellidoPaterno.length() >0){
				consulta.append(" and c.cocn_apellido_pat = '").append(apellidoPaterno.toUpperCase()).append("' \n");
			}
			
			if(apellidoMaterno != null && apellidoMaterno.length() > 0){
				consulta.append(" and c.cocn_apellido_mat ='").append(apellidoMaterno.toUpperCase()).append("' \n");
			}
			consulta.append("\nand b.cocc_casu_cd_sucursal = a.coce_casu_cd_sucursal \n");
			consulta.append("and b.cocc_carp_cd_ramo     = a.coce_carp_cd_ramo \n");
			consulta.append("and b.cocc_capo_nu_poliza   = a.coce_capo_nu_poliza \n");
			consulta.append("and b.cocc_nu_certificado   = a.coce_nu_certificado \n");
			consulta.append("and c.cocn_nu_cliente       = b.cocc_nu_cliente \n");
			consulta.append("and e.ales_cd_estatus          = a.coce_st_certificado \n");
			consulta.append("and ap.alpl_cd_ramo            = a.coce_carp_cd_ramo \n");
			consulta.append("and ap.alpl_cd_producto        = a.coce_capu_cd_producto \n");
			consulta.append("and ap.alpl_cd_plan            = a.coce_capb_cd_plan \n");
			consulta.append("and cocb.cocb_casu_cd_sucursal  = a.coce_casu_cd_sucursal \n");
			consulta.append("and cocb.cocb_carp_cd_ramo      = a.coce_carp_cd_ramo \n");
			consulta.append("and cocb.cocb_capo_nu_poliza    = decode(nvl(par.copa_nvalor7,1), 1, substr(a.coce_capo_nu_poliza,1,5), 0, a.coce_capo_nu_poliza, substr(a.coce_capo_nu_poliza,1,2)) \n");
			consulta.append("and cocb.cocb_capu_cd_producto  = a.coce_capu_cd_producto \n");
			consulta.append("and cocb.cocb_capb_cd_plan      = a.coce_capb_cd_plan \n");
			consulta.append("and cacb.cacb_cd_cobertura     = cocb.cocb_cacb_cd_cobertura \n");
			consulta.append("and cacb.cacb_carb_cd_ramo     = cocb.cocb_carb_cd_ramo \n");
			consulta.append("order by b.cocc_id_certificado, \n");
			consulta.append("		  a.coce_casu_cd_sucursal, \n");
			consulta.append("		  a.coce_carp_cd_ramo, \n");
			consulta.append("		  a.coce_capo_nu_poliza, \n");
			consulta.append("		  a.coce_nu_certificado \n");

			Query qry = this.getSession().createSQLQuery(consulta.toString());

			List<T> lista = qry.list();
			
			return lista;
			}
			catch (Exception e){
				throw new Excepciones ("Error .",e);
			}
	}
	
	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaCredCte(String idCertificado, String nombre, String apellidoPaterno, String apellidoMaterno) throws Exception {
		try {
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select \n");
			consulta.append("   distinct(b.cocc_id_certificado), \n");
			consulta.append("   nvl(a.coce_buc_empresa,'---'),	 \n");
			consulta.append("   c.cocn_nombre,					 \n");
			consulta.append("	c.cocn_apellido_pat, 			 \n");
			consulta.append("	c.cocn_apellido_mat, 			 \n");
			consulta.append("	nvl(c.cocn_cd_sexo,' '), 		 \n");
			consulta.append("	trunc(c.cocn_fe_nacimiento), 	 \n");
			consulta.append("	nvl(c.cocn_rfc,' '), 			 \n");
			consulta.append("	nvl(c.cocn_calle_num,' '),     	 \n");
			consulta.append("	nvl(c.cocn_colonia,' '),   		 \n");
			consulta.append("	nvl(c.cocn_delegmunic,' '),   	 \n");
			consulta.append("	nvl(d.caes_de_estado,' '),		 \n");
			consulta.append("	nvl(c.cocn_cd_postal,' '),		 \n");
			consulta.append("	nvl(c.cocn_nu_lada,'---'),		 \n");
			consulta.append("	nvl(c.cocn_nu_telefono,'---'),	 \n");
			consulta.append("	nvl(c.cocn_buc_cliente,' '),	 \n");
			consulta.append("	c.cocn_nu_cliente,				 \n");
			consulta.append("	nvl(a.coce_nu_cuenta,' ')		 \n");
			consulta.append("from 								 \n"); 
			consulta.append("colectivos_certificados a, 		 \n");
			consulta.append("colectivos_cliente_certif b,  		 \n");
			consulta.append("colectivos_clientes c,  			 \n");
			consulta.append("cart_estados d  					 \n");
			consulta.append("where 1=1 							 \n");
			if(idCertificado!= null && idCertificado.length() > 0){
				consulta.append(" and (b.cocc_id_certificado   = '").append(idCertificado).append("' \n");
				consulta.append(" or a.coce_buc_empresa = '").append(idCertificado).append("') \n");
			}
			if(nombre != null && nombre.length() > 0){
				consulta.append(" and c.cocn_nombre       = '").append(nombre.toUpperCase()).append("' \n");
			}
				
			if(apellidoPaterno != null && apellidoPaterno.length() >0){
				consulta.append(" and c.cocn_apellido_pat = '").append(apellidoPaterno.toUpperCase()).append("' \n");
			}
			
			if(apellidoMaterno != null && apellidoMaterno.length() > 0){
				consulta.append(" and c.cocn_apellido_mat ='").append(apellidoMaterno.toUpperCase()).append("' \n");
			}
			consulta.append("\nand b.cocc_casu_cd_sucursal  = a.coce_casu_cd_sucursal \n");
			consulta.append("and b.cocc_carp_cd_ramo        = a.coce_carp_cd_ramo \n");
			consulta.append("and b.cocc_capo_nu_poliza      = a.coce_capo_nu_poliza \n");
			consulta.append("and b.cocc_nu_certificado      = a.coce_nu_certificado \n");
			consulta.append("and c.cocn_nu_cliente          = b.cocc_nu_cliente \n");
			consulta.append("and nvl(b.cocc_tp_cliente,1)   = 1 \n");
			consulta.append("and d.caes_cd_estado(+)        = c.cocn_cd_estado \n");
			consulta.append("order by b.cocc_id_certificado \n");

			Query qry = this.getSession().createSQLQuery(consulta.toString());

			List<T> lista = qry.list();
			return lista;
			}
			catch (Exception e){
				throw new Excepciones ("Error en DAO consultaCredCte .",e);
			}
	}
	
	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaCredCert(String certs) throws Exception {
		try {
			
			if (certs == null || certs.length() == 0) certs = "'0'";
			
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select b.cocc_id_certificado, ");
			consulta.append("a.coce_casu_cd_sucursal, ");
			consulta.append("a.coce_carp_cd_ramo, ");
			consulta.append("a.coce_capo_nu_poliza, ");
			consulta.append("a.coce_nu_certificado, ");
			consulta.append("a.coce_st_certificado, ");
			consulta.append("e.ales_descripcion, ");
			consulta.append("e.ales_campo1, ");
			consulta.append("nvl(a.coce_cd_causa_anulacion,0), ");
			consulta.append("nvl(ae.ales_descripcion,' '), ");
			consulta.append("a.coce_fe_anulacion_col, ");
			consulta.append("a.coce_capu_cd_producto, ");
			consulta.append("a.coce_capb_cd_plan, ");
			consulta.append("a.coce_fe_carga, ");
			consulta.append("a.coce_fe_suscripcion, ");
			consulta.append("a.coce_mt_suma_asegurada, ");
			consulta.append("a.coce_mt_suma_aseg_si, ");
			//consulta.append("trim(to_char(decode(a.coce_sub_campana,'7',to_number(a.coce_mt_suma_aseg_si),a.coce_mt_suma_asegurada),'999999999.99')), ");
			//consulta.append("decode(a.coce_sub_campana,'7',a.coce_mt_suma_asegurada,a.coce_mt_suma_aseg_si), ");
			consulta.append("a.coce_mt_prima_subsecuente, ");
			consulta.append("a.coce_mt_prima_pura, ");
			consulta.append("a.coce_fe_desde, ");
			consulta.append("a.coce_fe_hasta, ");
			consulta.append("a.coce_fe_ini_credito, ");
			consulta.append("a.coce_fe_fin_credito, ");
			consulta.append("a.coce_no_recibo, \n");
			consulta.append("f.carp_de_ramo, \n");
			consulta.append("a.coce_cazb_cd_sucursal, \n");
			consulta.append("a.coce_fe_emision, \n");
			consulta.append("a.coce_empresa \n");
			consulta.append("from \n");
			consulta.append("colectivos_certificados a \n");
			consulta.append("left join alterna_estatus ae on a.coce_cd_causa_anulacion=ae.ales_cd_estatus, \n");
			consulta.append("colectivos_cliente_certif b, \n");
			consulta.append("colectivos_clientes c, \n");
			consulta.append("alterna_estatus e, \n");
			consulta.append("cart_ramos_polizas f \n");
			consulta.append("where 1=1 \n");
			consulta.append("and b.cocc_id_certificado   in (" + certs + ") \n");
			consulta.append("and b.cocc_casu_cd_sucursal = a.coce_casu_cd_sucursal  \n");
			consulta.append("and b.cocc_carp_cd_ramo     = a.coce_carp_cd_ramo  \n");
			consulta.append("and b.cocc_capo_nu_poliza   = a.coce_capo_nu_poliza  \n");
			consulta.append("and b.cocc_nu_certificado   = a.coce_nu_certificado \n"); 
			consulta.append("and nvl(b.cocc_tp_cliente,1)= 1 \n"); 
			consulta.append("and c.cocn_nu_cliente       = b.cocc_nu_cliente \n"); 
			consulta.append("and e.ales_cd_estatus       = a.coce_st_certificado \n");
			consulta.append("and f.carp_cd_ramo          = a.coce_carp_cd_ramo \n");
			consulta.append("order by b.cocc_id_certificado, \n");
			consulta.append("a.coce_casu_cd_sucursal, \n");
			consulta.append("a.coce_carp_cd_ramo, \n");
			consulta.append("a.coce_capo_nu_poliza, \n");
			consulta.append("a.coce_nu_certificado \n");
			
			
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			
			List<T> lista = qry.list();
			System.out.println(lista);
			return lista;
			}
			catch (Exception e){
				throw new Excepciones ("Error en DAO consultaCredCert.",e);
			}
	}

	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaCredCobs(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception {
		try {
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select b.cocc_id_certificado, ");
			consulta.append("a.coce_casu_cd_sucursal, ");
			consulta.append("a.coce_carp_cd_ramo, ");
			consulta.append("a.coce_capo_nu_poliza, ");
			consulta.append("a.coce_nu_certificado, ");
			consulta.append("cocb.cocb_cacb_cd_cobertura, ");
			consulta.append("cacb.cacb_de_cobertura, ");
			consulta.append("cocb.cocb_ta_riesgo, ");
			consulta.append("cocb.cocb_fe_desde, ");
			consulta.append("cocb.cocb_fe_hasta \n");
			consulta.append("from \n");
			consulta.append("colectivos_certificados a \n");
			consulta.append("left join colectivos_parametros par \n"); 
			consulta.append("on par.copa_des_parametro = 'POLIZA' \n"); 
			consulta.append("and par.copa_id_parametro  = 'GEP' \n"); 
			consulta.append("and par.copa_nvalor1       = a.coce_casu_cd_sucursal \n"); 
			consulta.append("and par.copa_nvalor2       = a.coce_carp_cd_ramo \n"); 
			consulta.append("and par.copa_nvalor3       = DECODE(par.copa_nvalor7, 0, a.coce_capo_nu_poliza, 1, substr(a.coce_capo_nu_poliza,0,3), substr(a.coce_capo_nu_poliza,0,2)), \n");
			consulta.append("colectivos_cliente_certif b, \n");
			consulta.append("colectivos_clientes c, \n");
			consulta.append("colectivos_coberturas cocb, \n");
			consulta.append("cart_coberturas cacb \n");
			consulta.append("where 1=1 \n");
			consulta.append("and b.cocc_id_certificado   = '" + certs + "' \n");
			consulta.append("and b.cocc_casu_cd_sucursal   = " + canal + " \n");
			consulta.append("and b.cocc_carp_cd_ramo   = " + ramo + " \n");
			consulta.append("and b.cocc_capo_nu_poliza  = " + poliza + " \n");
			consulta.append("and b.cocc_nu_certificado   = " + nucerd + " \n");
			consulta.append("and b.cocc_casu_cd_sucursal = a.coce_casu_cd_sucursal \n");
			consulta.append("and b.cocc_carp_cd_ramo     = a.coce_carp_cd_ramo \n");
			consulta.append("and b.cocc_capo_nu_poliza   = a.coce_capo_nu_poliza \n");
			consulta.append("and b.cocc_nu_certificado   = a.coce_nu_certificado \n");
			consulta.append("and nvl(b.cocc_tp_cliente,1)= 1 \n"); 
			consulta.append("and c.cocn_nu_cliente       = b.cocc_nu_cliente \n");
			consulta.append("and cocb.cocb_casu_cd_sucursal  = a.coce_casu_cd_sucursal \n");
			consulta.append("and cocb.cocb_carp_cd_ramo      = a.coce_carp_cd_ramo \n");
			consulta.append("and cocb.cocb_capo_nu_poliza    = decode(nvl(par.copa_nvalor7,1), 1, substr(a.coce_capo_nu_poliza,1,5), 0, a.coce_capo_nu_poliza, substr(a.coce_capo_nu_poliza,1,2)) \n");
			consulta.append("and cocb.cocb_capu_cd_producto  = a.coce_capu_cd_producto \n");
			consulta.append("and cocb.cocb_capb_cd_plan      = a.coce_capb_cd_plan \n");
			consulta.append("and cacb.cacb_cd_cobertura     = cocb.cocb_cacb_cd_cobertura \n");
			consulta.append("and cacb.cacb_carb_cd_ramo     = cocb.cocb_carb_cd_ramo \n");
			//consulta.append("and cocb.cocb_fe_hasta         > trunc(sysdate) \n");
			consulta.append("and cocb.cocb_cer_nu_cobertura = a.coce_nu_cobertura \n");
			consulta.append("order by b.cocc_id_certificado, \n");
			consulta.append("      a.coce_casu_cd_sucursal, \n");
			consulta.append("      a.coce_carp_cd_ramo, \n");
			consulta.append("      a.coce_capo_nu_poliza, \n");
			consulta.append("      a.coce_nu_certificado \n");
			
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			
			List<T> lista = qry.list();

			return lista;
			}
			catch (Exception e){
				throw new Excepciones ("Error en DAO consultaCredCobs.",e);
			}
	}

	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaCredAsis(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception {
		try {
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select b.cocc_id_certificado, ");
			consulta.append("a.coce_casu_cd_sucursal, ");
			consulta.append("a.coce_carp_cd_ramo, ");
			consulta.append("a.coce_capo_nu_poliza, ");
			consulta.append("a.coce_nu_certificado, ");
			consulta.append("nvl(cts.ctse_de_servicio,'NO TIENE ASISTENCIA') ");
			consulta.append("from \n");
			consulta.append("colectivos_certificados a \n");
			consulta.append("left join cart_campanias_productos ccp on ccp.ccap_carp_cd_ramo = a.coce_carp_cd_ramo and \n"); 
			consulta.append("									ccp.ccap_capu_cd_producto  = to_char(a.coce_capu_cd_producto) \n"); 
			consulta.append("left join cart_campanias cc on cc.ccam_cd_campania = ccp.ccap_ccam_cd_campania \n"); 
			consulta.append("left join cart_campanias_serv_prestados ccsp on ccsp.ccse_ccam_cd_campania = cc.ccam_cd_campania \n"); 
			consulta.append("left join cart_tipos_servicios cts on cts.ctse_cd_servicio = ccsp.ccse_ctse_cd_servicio, \n"); 
			consulta.append("colectivos_cliente_certif b \n");
			consulta.append("where 1=1 \n");
			consulta.append("and b.cocc_id_certificado   = '" + certs + "' \n");
			
			consulta.append("and b.cocc_casu_cd_sucursal   = " + canal + " \n");
			consulta.append("and b.cocc_carp_cd_ramo   = " + ramo + " \n");
			consulta.append("and b.cocc_capo_nu_poliza  = " + poliza + " \n");
			consulta.append("and b.cocc_nu_certificado   = " + nucerd + " \n");
			consulta.append("and nvl(b.cocc_tp_cliente,1)= 1 \n"); 
			consulta.append("and b.cocc_casu_cd_sucursal = a.coce_casu_cd_sucursal \n");
			consulta.append("and b.cocc_carp_cd_ramo     = a.coce_carp_cd_ramo \n");
			consulta.append("and b.cocc_capo_nu_poliza   = a.coce_capo_nu_poliza \n");
			consulta.append("and b.cocc_nu_certificado   = a.coce_nu_certificado \n");
			consulta.append("order by b.cocc_id_certificado, \n");
			consulta.append("      a.coce_casu_cd_sucursal, \n");
			consulta.append("      a.coce_carp_cd_ramo, \n");
			consulta.append("      a.coce_capo_nu_poliza, \n");
			consulta.append("      a.coce_nu_certificado \n");
			
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			
			List<T> lista = qry.list();

			return lista;
			}
			catch (Exception e){
				throw new Excepciones ("Error en DAO consultaCredAsis.",e);
			}
	}

	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Complementarios consultaCredComplementos(String credito, Short canal,
			Short ramo, Long poliza, Long nucerd) throws Exception {
		// TODO Auto-generated method stub
		
		try {
			StringBuilder consulta = new StringBuilder() ;
			
			consulta.append("  from Complementarios comp \n");
			consulta.append(" where comp.id.cocoCaceCasuCdSucursal   = " + canal  + "\n");
			consulta.append("   and comp.id.cocoCaceCarpCdRamo       = " + ramo   + "\n");
			consulta.append("   and comp.id.cocoCaceCapoNuPoliza     = " + poliza + "\n");
			consulta.append("   and comp.id.cocoCaceNuCertificado    = " + nucerd + "\n");
			consulta.append("   and comp.id.cocoNuMovimiento         = 0");
			
			Query qry = this.getSession().createQuery(consulta.toString());
			
			List<Complementarios> lista = qry.list();
			
			if (lista.isEmpty()){
				return new Complementarios();
			}else{
				return lista.get(0);
			}
			
		}
		catch (Exception e){
			throw new Excepciones ("Error en DAO consultaCredAsis.",e);
		}
	}
	
	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaCredAsegurados(String credito, Short canal,
			Short ramo, Long poliza, Long nucerd) throws Exception {
		// TODO Auto-generated method stub
		
		try {
			StringBuilder consulta = new StringBuilder() ;
			
			consulta.append("select clc.cocc_id_certificado                          				\n");
			consulta.append("      ,clc.cocc_casu_cd_sucursal                        				\n");
			consulta.append("      ,clc.cocc_carp_cd_ramo                            				\n");
			consulta.append("      ,clc.cocc_capo_nu_poliza                          				\n");
			consulta.append("      ,clc.cocc_nu_certificado                          				\n");
			consulta.append("      ,cl.cocn_nombre                                   				\n");
			consulta.append("      ,cl.cocn_apellido_pat                             				\n");
			consulta.append("      ,cl.cocn_apellido_mat                             				\n");
			consulta.append("      ,cl.cocn_fe_nacimiento                            				\n");
			consulta.append("      ,cl.cocn_rfc                                      				\n");
			consulta.append("      ,cl.cocn_cd_sexo                                  				\n");
			consulta.append("      ,nvl(p.copa_vvalor1,' ')                           				\n");
			consulta.append("  from colectivos_cliente_certif clc                    				\n");
			consulta.append("      ,colectivos_clientes       cl                     				\n");
			consulta.append("      ,colectivos_parametros     p                      				\n");
			consulta.append(" where clc.cocc_casu_cd_sucursal = " + canal  + "       				\n");
			consulta.append("   and clc.cocc_carp_cd_ramo     = " + ramo   + "       				\n");
			consulta.append("   and clc.cocc_capo_nu_poliza   = " + poliza + "       				\n");
			consulta.append("   and clc.cocc_nu_certificado   = " + nucerd + "       				\n");
			consulta.append("   and clc.cocc_tp_cliente       > 1                    				\n");
			consulta.append("   and cl.cocn_nu_cliente        = clc.cocc_nu_cliente  				\n");
			consulta.append("   and p.copa_des_parametro(+)   = 'ASEGURADO'          				\n");
			consulta.append("   and p.copa_id_parametro(+)    = 'TIPO'               				\n");
			consulta.append("   and p.copa_nvalor1(+)         = clc.cocc_tp_cliente  				\n");
			
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			
			List<T> lista = qry.list();
			
			return lista;
			
		}
		catch (Exception e){
			throw new Excepciones ("Error en DAO consultaCredAsis.",e);
		}
	}

	@SuppressWarnings("deprecation")
	public String impresionCertificado(String rutaTemporal, short canal, short ramo, long poliza, long certificado, Date fecha_emision){
    	String reporte = "";
    	
		try {
			
			//Map<String, Object> parametros = new HashMap<String, Object>();	
			Map parametros = new HashMap();
			String rutaImages = FacesUtils.getServletRequest().getRealPath("//WEB-INF//reportes") + File.separator;
									
		    parametros.put("canal", new Short(canal));
		    parametros.put("ramo", new Short(ramo));
		    parametros.put("poliza", new BigDecimal(poliza));
		    parametros.put("certificado", new BigDecimal(certificado));
		    //parametros.put("SUBREPORT_DIR", rutaTemporal);
		    parametros.put("SUBREPORT_DIR", rutaImages);
		  		    
		    
		    JasperReport report = null; 
    		
    		reporte = recuperarReporte(canal, ramo, poliza, certificado, DateFormat.getDateInstance().format(fecha_emision));
    		
    		if(reporte != null ){

            		report = JasperCompileManager.compileReport(rutaTemporal + reporte);
            		
    	        	reporte = ((String[]) reporte.split("_")) [0]+"_"+canal+"_"+ramo+"_"+"_"+poliza+"_"+certificado+".pdf";
    	        	
    	        	reporte = reporte.replace(".jrxml", "");

    	        	JasperPrint print = JasperFillManager.fillReport(report, parametros,getSession().connection());
    	        	
    	        	
    	        	JRPdfExporter exporter = new JRPdfExporter ();
    	        	exporter.setParameter (JRExporterParameter.JASPER_PRINT, print);
    	        	exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + reporte);
    	        	exporter.setParameter (JRPdfExporterParameter.METADATA_AUTHOR, "Santander Seguros");
    	        	exporter.exportReport();  
    	        	
    		}
		} catch (Exception e) {
		    e.printStackTrace();
		} 
		return reporte;
	}
	
	// dfg impresion poliza 
	
	@SuppressWarnings("deprecation")
	public String impresionPoliza(String rutaTemporal, short canal, short ramo, long poliza, Date fecha_emision)
	{
    	String reporte = "";
    	
		try {
			
			//Map<String, Object> parametros = new HashMap<String, Object>();	
			Map parametros = new HashMap();
			String rutaImages = FacesUtils.getServletRequest().getRealPath("//WEB-INF//reportes") + File.separator;				
		    parametros.put("canal", new Short(canal).intValue());
		    parametros.put("ramo", new Short(ramo).intValue());
		    parametros.put("poliza", new BigDecimal(poliza).intValue());
		    //parametros.put("SUBREPORT_DIR", rutaTemporal);
		    parametros.put("SUBREPORT_DIR", rutaImages);
		  		    
		    
		    JasperReport report = null; 
    		
    		reporte = recuperarReportePoliza(canal, ramo, poliza, DateFormat.getDateInstance().format(fecha_emision));
    		
    		if(reporte != null ){
    		//	reporte = "No existe reporte para este producto";
    		//}else{

            		report = JasperCompileManager.compileReport(rutaTemporal + reporte);
            		
    	        	
            		reporte = ((String[]) reporte.split("_")) [0]+"_"+canal+"_"+ramo+"_"+"_"+poliza+".pdf";
            		reporte = reporte.replace(".jrxml", "");
            		
    	        	JasperPrint print = JasperFillManager.fillReport(report, parametros, getSession().connection());
    	        	
    	        	JRPdfExporter exporter = new JRPdfExporter ();
    	        	exporter.setParameter (JRExporterParameter.JASPER_PRINT, print);
    	        	exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + reporte);
    	        	exporter.setParameter (JRPdfExporterParameter.METADATA_AUTHOR, "Santander Seguros");
    	        	exporter.exportReport();  
    		}
		}catch (Exception e) {
		    e.printStackTrace();
		}
		return reporte;
	}
	
	@SuppressWarnings("unchecked")
	private String recuperarReporte(short canal, short ramo, long poliza, long certificado, String fecha_emision )throws Excepciones{
    	String reporte = "";
    	StringBuilder query = null;
    	List<Object> lista = null;
    	
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<Object>();
    		
    		query.append(" select PP.COPA_VVALOR8                \n"); 	                       									  
    		query.append("   from colectivos_certificados C ,    \n");								  
    		query.append("        colectivos_parametros PP       \n");				  
    		query.append("  where C.COCE_CASU_CD_SUCURSAL = ").append(canal).append("      \n");        
    		query.append("    and C.COCE_CARP_CD_RAMO     = ").append(ramo).append("	   \n"); 		  
    		query.append("    and C.COCE_CAPO_NU_POLIZA   = ").append(poliza).append("     \n"); 		  
    		query.append("    and C.COCE_NU_CERTIFICADO   = ").append(certificado).append("  \n");	  
    		query.append("    and PP.COPA_NVALOR1 = C.COCE_CASU_CD_SUCURSAL 	             \n");		  
    		query.append("    and PP.COPA_NVALOR2 = C.COCE_CARP_CD_RAMO 		             \n");		   
    		query.append("    and to_number(PP.COPA_VVALOR1) = C.COCE_CAPU_CD_PRODUCTO       \n"); 
    		query.append("    and to_number(PP.COPA_VVALOR2) = C.COCE_CAPB_CD_PLAN 	         \n");
    		query.append("    and PP.COPA_DES_PARAMETRO = 'IMPRESION CERTIFICADOS' 	         \n");
    		query.append("    and PP.COPA_ID_PARAMETRO = 'TIPO'                              \n");
    		query.append("    and PP.COPA_FVALOR1 <= to_date('").append(fecha_emision).append("','dd/mm/yyyy')       \n");	
    		query.append("    and PP.COPA_FVALOR2 >= to_date('").append(fecha_emision).append("','dd/mm/yyyy')       \n");	
    		lista = getSession().createSQLQuery(query.toString()).list();
    		
    		if(lista != null && lista.size() > 0){
    			reporte = lista.get(0).toString();
    		}else{
    			reporte = null; /* "No existe reporte para este producto ";*/
    		}
    			
    		
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error al recuperar reporte impresion de certificados", e);
		}
    	
    	return reporte;
    }


	// recuperar poliza
	
	@SuppressWarnings("unchecked")
	private String recuperarReportePoliza(short canal, short ramo, long poliza, String fecha_emision )throws Excepciones{
    	String reporte = "";
    	StringBuilder query = null;
    	List<Object> lista = null;
    	
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<Object>();
    		
    		query.append(" select PP.COPA_VVALOR8                \n"); 	                       									  
    		query.append("   from colectivos_certificados C ,    \n");								  
    		query.append("        colectivos_parametros PP       \n");				  
    		query.append("  where C.COCE_CASU_CD_SUCURSAL = ").append(canal).append("      \n");        
    		query.append("    and C.COCE_CARP_CD_RAMO     = ").append(ramo).append("	   \n"); 		  
    		query.append("    and C.COCE_CAPO_NU_POLIZA   = ").append(poliza).append("     \n"); 		  
    		query.append("    and C.COCE_NU_CERTIFICADO   = 0   \n");	  
    		query.append("    and PP.COPA_NVALOR1 = C.COCE_CASU_CD_SUCURSAL 	             \n");		  
    		query.append("    and PP.COPA_NVALOR2 = C.COCE_CARP_CD_RAMO 		             \n");		   
    		query.append("    and PP.COPA_DES_PARAMETRO = 'IMPRESION POLIZA' 	         \n");
    		query.append("    and PP.COPA_ID_PARAMETRO = 'TIPO'                              \n");
    		query.append("    and PP.COPA_FVALOR1 <= to_date('").append(fecha_emision).append("','dd/mm/yyyy')       \n");	
    		query.append("    and PP.COPA_FVALOR2 >= to_date('").append(fecha_emision).append("','dd/mm/yyyy')       \n");	
    		lista = getSession().createSQLQuery(query.toString()).list();
    		
    		if(lista != null && lista.size() > 0){
    			reporte = lista.get(0).toString();
    		}else{
    			reporte = null; /* "No existe reporte para este producto ";*/
    		}
    			
    		
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error al recuperar reporte impresion de poliza", e);
		}
    	
    	return reporte;
    }
	
	@Override
	public Parametros getRutaPlantilla() throws Exception {
    	StringBuilder sbQuery = null;
    	Parametros param;
    	ArrayList<Object> lstParam;
    	Iterator<Object> itParam;
    	Object[] arrDatos;
    	
    	try {
    		sbQuery = new StringBuilder();
    		param = new Parametros();
    		lstParam = new ArrayList<Object>();
    		
    		sbQuery.append("SELECT cp.copa_vvalor6, cp.copa_vvalor7, cp.copa_registro \n");                	                                           
    		sbQuery.append("  FROM COLECTIVOS_PARAMETROS CP \n");
    		sbQuery.append("WHERE CP.COPA_ID_PARAMETRO = 'PLANTILLAS' \n");
    		sbQuery.append("  AND CP.COPA_DES_PARAMETRO = 'RUTAS' \n");
    		       
    		lstParam.addAll(getSession().createSQLQuery(sbQuery.toString()).list());
    		
    		if(!lstParam.isEmpty()){
    			itParam = lstParam.iterator();
    			while(itParam.hasNext()){
    				arrDatos = (Object[]) itParam.next();
    				
    				param.setCopaVvalor6(arrDatos[0].toString());
    				param.setCopaVvalor7(arrDatos[1].toString());
    				param.setCopaRegistro(arrDatos[2].toString());
    			}
    		}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error: ClienteCertifDao.getPlantilla()" + e.getMessage());
		}
    	return param;
    }

	@Override
	public String getPlantilla(Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan, Date fechaEmision) throws Exception {
    	String strPlantilla = Constantes.DEFAULT_STRING;
    	StringBuilder sbQuery = null;
    	ArrayList<Object> arLista = null;
    	
    	try {
    		sbQuery = new StringBuilder();
    		arLista = new ArrayList<Object>();
    		
    		sbQuery.append("SELECT CP.COPA_VVALOR8 \n");                	                                           
    		sbQuery.append("  FROM COLECTIVOS_PARAMETROS CP \n");
    		sbQuery.append("WHERE CP.COPA_ID_PARAMETRO = 'PLANTILLAS' \n");
    		sbQuery.append("  AND CP.COPA_DES_PARAMETRO = 'CERTIFICADO' \n");
    		sbQuery.append("  AND CP.COPA_NVALOR1 = ").append(canal).append(" \n");                    
    		sbQuery.append("  AND CP.COPA_NVALOR2 = ").append(ramo).append(" \n");
    		sbQuery.append("  AND CP.COPA_NVALOR3 = ").append(poliza).append(" \n");      
    		sbQuery.append("  AND CP.COPA_VVALOR1 = '").append(producto.toString()).append("' \n");        
    		sbQuery.append("  AND CP.COPA_VVALOR2 = '").append(plan.toString()).append("' \n");
    		sbQuery.append("  AND TO_DATE('").append(GestorFechas.formatDate(fechaEmision, "dd/MM/yyyy")).append("','dd/MM/yyyy') BETWEEN CP.COPA_FVALOR1 AND CP.COPA_FVALOR2 \n");
    		arLista.addAll(getSession().createSQLQuery(sbQuery.toString()).list());

    		if(!arLista.isEmpty()) {
    			strPlantilla = arLista.get(0).toString();
    		}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error: ClienteCertifDao.getPlantilla()" + e.getMessage());
		}
    	
    	return strPlantilla;
    }

	@Override
	public ArrayList<Object> getDatosPlantilla(StringBuffer filtro) throws Exception {
		ArrayList<Object> arLista = null;
    	
		try {
			arLista = new ArrayList<Object>();
			
    		arLista.addAll(getSession().createSQLQuery(filtro.toString()).list());
    		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error: ClienteCertifDao.getDatosPlantilla()" + e.getMessage());
		}
		
		return arLista;
	}

	@Override
	public Object[] getDatosRecas(Short ramo, String producto) throws Exception {
		Object[] arrRegreso = {"***", "***", "***", GestorFechas.generateDate("01/01/1900", "dd/MM/yyyy")};
		String strPlantilla = Constantes.DEFAULT_STRING;
    	StringBuilder sbQuery = null, sbConsultaRecas = null;
    	ArrayList<Object> arLista = null;
    	Query qry;
    	
    	try {
    		sbQuery = new StringBuilder();
    		sbConsultaRecas = new StringBuilder();
    		arLista = new ArrayList<Object>();
    		
    		sbQuery.append("SELECT CP.COPA_REGISTRO \n");                	                                           
    		sbQuery.append("  FROM COLECTIVOS_PARAMETROS CP \n");
    		sbQuery.append("WHERE CP.COPA_ID_PARAMETRO = 'PLANTILLAS' \n");
    		sbQuery.append("  AND CP.COPA_DES_PARAMETRO = 'RECAS' \n");
    		arLista.addAll(getSession().createSQLQuery(sbQuery.toString()).list());

    		if(!arLista.isEmpty()) {
    			try {
    				sbConsultaRecas.append(arLista.get(0).toString());
        			qry = this.getSession().createSQLQuery(sbConsultaRecas.toString());    			    			
        			qry.setParameter("ramo", ramo);
        			qry.setParameter("producto", producto);
        			arrRegreso = (Object[]) qry.list().get(0);
				} catch (Exception e) {
					System.out.println("No hay datos de RECAS para estos datos: Ramo-" + ramo + ", Producto-" + producto);
					e.printStackTrace();
				}
    		}  		    		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error: ClienteCertifDao.getDatosRecas()" + e.getMessage());
		}
    	
    	return arrRegreso;
	}
	
	@Override
	public <T> List<T> consultaDatos(Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan) throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select p.copa_vvalor1 || p.copa_vvalor2 || p.copa_vvalor8, p.copa_registro, \n"); 
			sb.append("p.copa_nvalor1, P.COPA_VVALOR4, \n");
			sb.append("p.copa_nvalor2,  P.COPA_VVALOR5, \n");
			sb.append("p.copa_nvalor3,  P.COPA_VVALOR6, \n");
			sb.append("p.copa_vvalor7, c.coim_campo_formulario \n");
			sb.append("from COLECTIVOS_CONF_IMPRESION c, colectivos_parametros p \n");
			sb.append("where c.coim_canal =  ").append(canal).append(" \n");
			sb.append("and c.coim_ramo = ").append(ramo).append(" \n");
			sb.append("and c.coim_poliza = ").append(poliza).append(" \n");
			sb.append("and c.coim_producto = ").append(producto).append(" \n");
			sb.append("and c.coim_plan = ").append(plan).append(" \n");
			sb.append("and p.copa_des_parametro = 'IMPRESION_QUERY_DINAMICO' \n");
			sb.append("and p.copa_id_parametro = c.coim_campo_base \n");
			sb.append("order by p.copa_cd_parametro asc \n");
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	//Carga Polizas Impresion
	@SuppressWarnings("unchecked")
	public <T> List<T> cargaPolizasImpresion(final int ramo, final long poliza) throws Exception{
		List<T> polizas = null;
		
		try{
			
			StringBuilder sb = new StringBuilder();
	
			sb.append(" select cc.coce_casu_cd_sucursal, ");
			sb.append(" cc.coce_carp_cd_ramo, ");
			sb.append(" cc.coce_capo_nu_poliza, ");
			sb.append(" count(cd.coce_nu_certificado), ");
			sb.append(" sum(cd.coce_mt_prima_subsecuente) ");
			sb.append(" from colectivos_certificados cc ");
			sb.append(" inner join colectivos_certificados cd ON(cd.coce_casu_cd_sucursal = cc.coce_casu_cd_sucursal and cd.coce_carp_cd_ramo=cc.coce_carp_cd_ramo and cd.coce_capo_nu_poliza=cc.coce_capo_nu_poliza and cd.coce_nu_certificado > 0) ");
			sb.append(" where cc.coce_casu_cd_sucursal = 1 ");
			sb.append(" and cc.coce_carp_cd_ramo = ").append(ramo);
			sb.append(" and cc.coce_capo_nu_poliza = ").append(poliza);
			sb.append(" and cc.coce_nu_certificado = 0 ");
			sb.append(" and cc.coce_st_facturacion = 1 ");
			sb.append(" and cd.coce_st_certificado IN(1, 2) ");
			sb.append(" group by cc.coce_casu_cd_sucursal,  cc.coce_carp_cd_ramo, cc.coce_capo_nu_poliza");
	
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}

	public <T> List<T> obtenerRegistros(final int ramo, final long poliza) throws Exception{
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
			sb.append(" select cc.coce_casu_cd_sucursal, ");
			sb.append(" cc.coce_carp_cd_ramo, ");
			sb.append(" cc.coce_capo_nu_poliza, ");
			sb.append(" cc.coce_nu_certificado, ");
			sb.append(" CC.COCE_CAPU_CD_PRODUCTO, ");
			sb.append(" CC.COCE_CAPB_CD_PLAN, ");
			sb.append(" NVL(cc.COCE_FE_EMISION,SYSDATE) ");
			sb.append(" from colectivos_certificados cc ");
			sb.append(" where cc.coce_casu_cd_sucursal = 1 ");
			sb.append(" and cc.coce_carp_cd_ramo = ").append(ramo);
			sb.append(" and cc.coce_capo_nu_poliza = ").append(poliza);
			sb.append(" and cc.coce_nu_certificado > 0 ");
			sb.append(" and cc.coce_st_certificado IN(1,2) ");
	
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	public String reporteAsegurado(String rutaTemp, short ramo, long poliza){
		String reporte = "";
		
		try {
			Map parametros = new HashMap();
			String rutaImages = FacesUtils.getServletRequest().getRealPath("images") + File.separator;
		    parametros.put("RAMO", new Short(ramo).intValue());
		    parametros.put("POLIZA", new BigDecimal(poliza).intValue());
		    parametros.put("RUTAIMG", rutaImages);
		    parametros.put("IMAGEN", "logoReporte.png");
		    
		    reporte = "impresionAsegurados.jrxml";
		    
		    JasperReport report = null;
		    
		    report = JasperCompileManager.compileReport(rutaTemp + reporte);
		    
		    reporte = "ListaAsegurado_" + String.valueOf(poliza) + ".pdf";
		    reporte = reporte.replace(".jrxml", "");
		    
		    JasperPrint print = JasperFillManager.fillReport(report, parametros, getSession().connection());
					
        	JRPdfExporter exporter = new JRPdfExporter ();
        	exporter.setParameter (JRExporterParameter.JASPER_PRINT, print);
        	exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, rutaTemp + reporte);
        	exporter.setParameter (JRPdfExporterParameter.METADATA_AUTHOR, "Santander Seguros");
        	exporter.exportReport(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reporte;
	}

}
