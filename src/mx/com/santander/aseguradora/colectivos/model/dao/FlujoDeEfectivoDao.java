package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

public interface FlujoDeEfectivoDao {

	public List<Object> consultaCuentas() throws Exception;

	public <T> List<T> cargaReporteCargoAbono(String fInicio, String fFin, String cuenta, Integer operacion) throws Exception;

}
