package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.ArrayList;
import java.util.Date;


public interface ReportesCierreDao {

	public ArrayList<Object> extraeComCom(int canal) throws Exception;
	public ArrayList<Object> extraerMontoRfi(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraerRecCob(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraerRecSnCob(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraerCifCom(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraerCifComU(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraeComTec(int canal) throws Exception;
	public ArrayList<Object> extraeCifTec(String finicio, String ffin, int operacion) throws Exception;
	public ArrayList<Object> extraeDetaRfi(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraeDosOp(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraeCuatroOP(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraeRepTotCoa(String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraeRepEmi() throws Exception;
	public ArrayList<Object> extraeReservas(int canal, int ramo, String finicio, String ffin) throws Exception;
	public ArrayList<Object> extraeSumaAseg(int canal, int ramo, String finicio, String ffin) throws Exception;
	
}
