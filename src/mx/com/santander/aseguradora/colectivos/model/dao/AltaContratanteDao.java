package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.ArrayList;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;


/**
 * @author dflores
 *
 */

public interface AltaContratanteDao extends CatalogoDao {
	
	public boolean  actualizaCliente(ContratanteDTO contratanteDTO, Map<String, Object> resultado) throws Excepciones;
	public void actualizaCertificados(Integer producto, Integer plan, String sumaAseg, String prima, Short sucursal,Short ramo, Long poliza, Integer certificado, String formaPago)throws Excepciones;
	public Integer obtenerFormaPago(Short ramo, Integer producto, Integer plan) throws Excepciones;
	public abstract ArrayList<Object> consultar(String strQuery) throws Exception;
	public void actualizaCartCertificados(short cdSucursal, Short inRamo, Long numeroPoliza, Map<String, Object> resultadoCliente2) throws Exception;
	
}
