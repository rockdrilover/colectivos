/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author dflores
 * Modificacaciom: Sergio Plata
 *
 */
public interface EstadoDao extends CatalogoDao{

	public abstract <T> List<T> obtenerObjetos() throws Excepciones;
	public abstract List<Object> obtenerEstado() throws Excepciones;
	
}
