/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;


import mx.com.santander.aseguradora.colectivos.model.bussinessObject.RamoContable;
import mx.com.santander.aseguradora.colectivos.model.dao.RamoContableDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * @author Dflores
 *
 */
@Repository
public class RamoContableDaoHibernate extends HibernateDaoSupport implements RamoContableDao {
	
	
	@Autowired
	public RamoContableDaoHibernate(SessionFactory sessionFactory) {

		super.setSessionFactory(sessionFactory);
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<RamoContable> getRamosContables() {
		// TODO Auto-generated method stub
		
		return getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuffer sb = new StringBuffer(150);
				sb.append("from RamoContable RC \n" +
						  "where RC.carbCdRamo <> 0  \n" +
						  "order by RC.carbCdRamo ");
				
				Query query = sesion.createQuery(sb.toString());
				
				List<Object> lista = query.list();
												
				return lista;
			}
			
		});
	}

	
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
}
