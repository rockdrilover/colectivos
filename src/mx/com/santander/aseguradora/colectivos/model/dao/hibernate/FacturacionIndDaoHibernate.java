package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.dao.FacturacionIndDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import oracle.sql.BLOB;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.lob.SerializableBlob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author TONY
 *
 */
@Repository
public class FacturacionIndDaoHibernate extends HibernateDaoSupport implements FacturacionIndDao {
	
	private static final long serialVersionUID = 298075158826438461L;

	@Autowired
	public FacturacionIndDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getXmlTimbrar(Short canal, Short ramo, Long poliza,  String reciboFiscal, String strRutaDestino, String strNombre) throws Excepciones {
		StringBuilder consulta;
		Query qry; 
		List lista; 
		InputStream inStream;
		BLOB blobXml;
		File archivoXml;
		FileOutputStream fos;
		int length  = -1;
		byte[] buffer = null;
		String archivo;
		
		try {
			consulta = new StringBuilder();
			archivo = strNombre + ".xml";
			consulta.append(" select C.ARCHIVOXML ");
			consulta.append(" from cart_emisioncfd_i c ");
			consulta.append(" where C.CANAL = :canal ");
			consulta.append(" and C.RAMO = :ramo ");
			consulta.append(" and C.FOLIO = :reciboFiscal ");
			consulta.append(" and C.POLIZA = :poliza "); 

			qry = getSession().createSQLQuery(consulta.toString());
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("reciboFiscal", reciboFiscal);
			qry.setParameter("poliza", poliza);
			
			lista = qry.list();

			if(!lista.isEmpty()){
				strRutaDestino = strRutaDestino + archivo;
				blobXml = (BLOB) ((SerializableBlob) lista.get(0)).getWrappedBlob();
				archivoXml = new File(strRutaDestino ); 
				fos = new FileOutputStream(archivoXml);
				inStream =  blobXml.getBinaryStream();
			    buffer  = new byte[blobXml.getBufferSize()];
			    while((length = inStream.read(buffer)) != -1){
			        fos.write(buffer, 0, length);
			        fos.flush();
			    }

			    inStream.close();
			    fos.close(); 
			} else {
				strRutaDestino = "";
			}
		} catch (Exception e) {
			e.printStackTrace();//jalm
			throw new Excepciones("Error: Al recuperar XML de la Base. ", e);
		}
		
		return archivo;

	}
    
	@SuppressWarnings({ "unchecked", "deprecation" })
	public String imprimeRecibo2(String rutaTemporal,String rutaReporte, String nombre,
			    	             Short coreCasuCdSucursal, Short coreCarpCdRamo,
				                 Long coreCapoNuPoliza, BigDecimal coreNuRecibo, Short primaUnica, Long certificado){
		String archivoPdf = "";
		
		try {
			Map parametros = new HashMap();
			String rutaImages = FacesUtils.getServletRequest().getRealPath("images") + File.separator;		
		    parametros.put("CANAL", coreCasuCdSucursal.toString());
		    parametros.put("RAMO", coreCarpCdRamo.toString());
		    parametros.put("POLIZA", coreCapoNuPoliza.toString());
		    parametros.put("RECIBO",coreNuRecibo.toString());
		    parametros.put("CERTIFICADO", certificado.toString());
		    parametros.put("PU", primaUnica);
		    parametros.put("RUTAIMG", rutaImages);		
		    
		    StringBuilder consulta = new StringBuilder() ;
		    
		    if(coreCarpCdRamo == 57 || coreCarpCdRamo == 58){
		    	consulta.append("select p.copa_vvalor7													     	   \n");
			    consulta.append("from colectivos_parametros p												       \n");
				consulta.append("    ,cart_recibos          r												       \n");
				consulta.append("where p.copa_des_parametro     = 'RECIBO' 										   \n");
				consulta.append(" and p.copa_id_parametro       = 'LEYENDAS'								       \n");
				consulta.append("and r.care_casu_cd_sucursal    = ").append(coreCasuCdSucursal.toString()).append("\n");
				consulta.append("and r.care_carp_cd_ramo        = ").append(coreCarpCdRamo.toString()).append("    \n");
				consulta.append("and r.care_capo_nu_poliza      = ").append(coreCapoNuPoliza.toString()).append("  \n");
				consulta.append("and r.care_cace_nu_certificado = ").append(certificado.toString()).append("       \n");
				consulta.append("and r.care_nu_recibo           = ").append(coreNuRecibo.toString()).append("      \n");
				consulta.append("and r.care_fe_emision        between p.copa_fvalor1 and p.copa_fvalor2		       \n");
		    } else{
		    	consulta.append("select p.copa_vvalor7													     	   \n");
			    consulta.append("from colectivos_parametros p												       \n");
				consulta.append("    ,colectivos_recibos          r												       \n");
				consulta.append("where p.copa_des_parametro     = 'RECIBO' 										   \n");
				consulta.append(" and p.copa_id_parametro       = 'LEYENDAS'								       \n");
				consulta.append("and r.core_casu_cd_sucursal    = ").append(coreCasuCdSucursal.toString()).append("\n");
				consulta.append("and r.core_carp_cd_ramo        = ").append(coreCarpCdRamo.toString()).append("    \n");
				consulta.append("and r.core_capo_nu_poliza      = ").append(coreCapoNuPoliza.toString()).append("  \n");
				consulta.append("and r.core_cace_nu_certificado = ").append(certificado.toString()).append("       \n");
				consulta.append("and r.core_nu_recibo           = ").append(coreNuRecibo.toString()).append("      \n");
				consulta.append("and r.core_fe_emision        between p.copa_fvalor1 and p.copa_fvalor2		       \n");
		    }

			Query qry = getSession().createSQLQuery(consulta.toString());
			
			String version;
			
			if (qry.list().get(0) != null){
				version = (String) qry.list().get(0);		    
			} else{
				 if(coreCarpCdRamo == 57 || coreCarpCdRamo == 58){
					 version = "ImpresionReciboCFDI5758.jrxml";
				 } else {
					 version = "ImpresionReciboCFDI.jrxml";
				 }
			}
			
		    JasperReport report = null;
		    report = JasperCompileManager.compileReport(rutaReporte + version);
		    JasperPrint print = JasperFillManager.fillReport(report, parametros, getSession().connection());
		    archivoPdf = nombre + ".pdf";
		    JRPdfExporter exporter = new JRPdfExporter ();
		    exporter.setParameter (JRExporterParameter.JASPER_PRINT, print);
		    exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + archivoPdf );
		    exporter.setParameter (JRPdfExporterParameter.METADATA_AUTHOR, "Santander Seguros");
		    exporter.exportReport();
		}catch (Exception e) {	
		    e.printStackTrace();
		}
		return archivoPdf;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)	
    public  <T> List<T>  getDatosCertificado() throws Excepciones {
		
		StringBuilder consulta;
		Query qry; 
		List<T> lista; 

		try {
			consulta = new StringBuilder();
			consulta.append(" select cer.coce_casu_cd_sucursal canal                          			  \n");
			consulta.append("       ,cer.coce_carp_cd_ramo     ramo							  			  \n");
			consulta.append("       ,cer.coce_capo_nu_poliza   poliza                        			  \n");
			consulta.append("       ,to_number(pre.copc_dato1) recibo						  			  \n");
			consulta.append(" 		,DECODE(to_number(nvl(cer.coce_sub_campana,'0')), 0, 0, 1) primaUnica \n");
			consulta.append(" 		,cer.coce_nu_certificado   certificado 								  \n");
			consulta.append("   from colectivos_certificados cer										  \n");
			consulta.append("  inner join colectivos_precarga pre									      \n");
			consulta.append("     on (cer.coce_casu_cd_sucursal   = pre.copc_cd_sucursal			      \n");
			consulta.append("     and cer.coce_carp_cd_ramo > 0 	    	   						      \n");
			consulta.append("     and cer.coce_capo_nu_poliza = pre.copc_num_poliza  	    	          \n");
			consulta.append("     and cer.coce_nu_certificado = pre.copc_suma_asegurada)                  \n");
			consulta.append("  where pre.copc_cd_sucursal   = 1    									      \n");
			consulta.append("    and pre.copc_cd_ramo       = 0        								      \n");
			consulta.append("    and pre.copc_tipo_registro = 'CON'       								  \n");
			consulta.append("    and pre.copc_cargada       = '17'         							      \n");
			
			qry = this.getSession().createSQLQuery(consulta.toString());
			
			lista = qry.list();
			
			
		} catch (Exception e) {
			throw new Excepciones("Error: Al recuperar datos. ", e);
		}
		return lista;
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public void borrarDatosPrecarga()  throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			StringBuilder query = new StringBuilder(100);
			Query qry;
			
			// Borramos la precarga
			query.append("delete PreCarga CP	                \n");
			query.append("where CP.id.copcCdSucursal    = 1     \n");
			query.append("  and CP.id.copcCdRamo        = 0     \n");
		    query.append("  and CP.id.copcNumPoliza     > 0     \n");
		    query.append("  and CP.id.copcSumaAsegurada > 0     \n");
		    query.append("  and CP.copcDato1            > 0     \n");
		    query.append("  and CP.copcCargada          = '17'  \n");
			query.append("  and CP.id.copcTipoRegistro  = 'CON' \n");
			
			qry = this.getSession().createQuery(query.toString());
			
			qry.executeUpdate();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}
}
