/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.RamoContable;

/**
 * @author 
 *
 */
public interface RamoContableDao extends CatalogoDao {

	/**
	 * 
	 * @return
	 */
	public abstract List<RamoContable> getRamosContables();

}
