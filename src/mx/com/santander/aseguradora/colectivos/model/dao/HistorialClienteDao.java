package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
public interface HistorialClienteDao extends CatalogoDao{
	
	public abstract  <T> List<T> consultaHistorial (short ramo, Long poliza, String certificado,String credito ,String nombre, String paterno, String materno) throws Excepciones;

	public abstract <T> List<T> consultaHistorialCobertura(short ramo, Long poliza, String certificado, Integer producto,Integer plan) throws Excepciones;

	public abstract <T> List<T> consultaHistorialRecibos(short ramo, Long poliza,
			String certificado) throws Excepciones;
	
	public abstract <T> List<T> consultaHistorialRecibosRamo57_58(short ramo, Long poliza,
			String certificado) throws Excepciones;
	
	public <T> List<T> consultaDetalleComponente(short ramo, String no_recibo,
			String poliza) throws Excepciones;
		
}
