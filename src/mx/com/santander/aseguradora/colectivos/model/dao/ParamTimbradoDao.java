package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Interface que implementa la clase ParamTimbradoDaoHibernate.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Local
public interface ParamTimbradoDao extends Serializable {
	
	/**
	 * Metodo que consulta los datos para PR
	 * @param canal canal para hacer filtro
	 * @param ramo ramo para hacer filtro
	 * @param poliza poliza para hacer filtro
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	List<Object> getDatosPR(Integer canal, Short ramo, Integer poliza) throws Excepciones;
	
	/**
	 * Metodo que consulta los datos para PU
	 * @param canal canal para hacer filtro
	 * @param ramo ramo para hacer filtro
	 * @param idVenta id venta para hacer filtro
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	List<Object> getDatosPU(Integer canal, Short ramo, Integer idVenta) throws Excepciones;
	
	/**
	 * Metodo que consulta objetos de base de datos 
	 * @param <T> tipo de objeto a consultar
	 * @param filtro para aplicar en la consulta
	 * @return lista de resultados
	 * @throws Excepciones con error generar
	 */
	<T> List<T> obtenerObjetos(String filtro) throws Excepciones;
	
	/**
	 * Metodo que consulta objetos de la lista de nombre Productos
	 * @return lista de productos
	 * @throws Excepciones con error en general
	 */
	List<Object> obtenerNombreProductos() throws Excepciones;
	
}
