/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.EstatusDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Sergio Plata
 *
 */
@Repository
public class EstatusDaoHibernate extends HibernateDaoSupport implements
		EstatusDao {

	@Autowired
	public EstatusDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void borrarObjeto(T objeto)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}		
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			return (T) this.getHibernateTemplate().get(objeto, id);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer sb = new StringBuffer(100);
					sb.append("from Estatus E			 \n");
					sb.append("order by E.alesDescripcion\n");
					
					Query qry = sesion.createQuery(sb.toString());
					
					List<T> lista = qry.list();
					
					return lista;
				}
				
			});
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().update(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			for(T objeto: lista){
				
				this.guardarObjeto(objeto);
			}
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
			
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
				
					StringBuilder consulta = new StringBuilder();
					consulta.append("from Estatus E	\n");
					consulta.append("where 1 = 1 \n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					
					List<T> lista = qry.list();
					
					return lista;
				}
				
			});
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

}
