/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;



import mx.com.santander.aseguradora.colectivos.model.dao.AltaProdDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


/**
 * @author dflores
 *
 */
@SuppressWarnings("unchecked")

public class AltaProdDaoHibernate extends HibernateDaoSupport implements AltaProdDao {

	
	/**
	 * 
	 * @author dflores
	 *
	 */
	private class IniciaAlta extends StoredProcedure{

		public IniciaAlta(DataSource ds) {
			// TODO Auto-generated constructor stub
			super.setDataSource(ds);
			super.setSql(Colectivos.PROCESO_ALTAPROD);
			super.declareParameter(new SqlOutParameter("V_COUNT", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramDescProducto", OracleTypes.VARCHAR));
			super.declareParameter(new SqlParameter("paramDescPlan", OracleTypes.VARCHAR));
			super.compile();
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.StoredProcedure#execute(java.util.Map)
		 */
		

		public Map execute( Short ramo, String descProducto,String descPlan) 
			// TODO Auto-generated method stub
			
			throws DataAccessException {
				// TODO Auto-generated method stub
				Map<String, Object> inParams = new HashMap<String, Object>();
				
				
				inParams.put("paramRamo", ramo);
				inParams.put("paramDescProducto", descProducto);
				inParams.put("paramDescPlan", descPlan);			
				return super.execute(inParams);
				
			}

			
		
	}

	public Integer iniciaAlta( Short ramo, String descProducto, String descPlan) {
		// TODO Auto-generated method stub
		Integer poliza = 0;
		
		DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
		IniciaAlta alta = new IniciaAlta(ds);
		
		poliza = (Integer) ((Map)alta.execute(ramo,descProducto,descPlan)).get("V_COUNT");
		
		return poliza;
		
		
	}


	public List getListaAltaProd() {
		// TODO Auto-generated method stub
		return null;
	}

	public List getListaAltaProd(Short ramo, String descProducto, String descPlan) {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer iniciaAltaProd(Short ramo, String descProducto,
			String descPlan) {
		// TODO Auto-generated method stub
Integer poliza = 0;
		
		DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
		IniciaAlta alta = new IniciaAlta(ds);
		
		poliza = (Integer) ((Map)alta.execute(ramo,descProducto,descPlan)).get("V_COUNT");
		
		return poliza;
		
		
	}
	

	public void saveListaAltaProd(List lista) {
		// TODO Auto-generated method stub
		
	}


	public Object saveAltaProd(Object altaProd) {
		// TODO Auto-generated method stub
		return null;
	}







}
