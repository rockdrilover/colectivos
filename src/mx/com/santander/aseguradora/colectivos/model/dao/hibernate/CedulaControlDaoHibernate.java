package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.dao.CedulaControlDao;
import mx.com.santander.aseguradora.colectivos.model.database.ProcesosPL;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase de tipo DAO que tiene acceso a base de datos para 
 * 				el proceso de Cedula Control.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Repository
public class CedulaControlDaoHibernate extends HibernateDaoSupport implements CedulaControlDao {
	//Implementacion de serializable
	private static final long serialVersionUID = 298075158826438461L;
	
	//Constante para from de quuery
	private static final String TABLA = "FROM COLECTIVOS_CEDULA \n";
	//Constante para where de query
	private static final String WHERE = "WHERE COCC_REMESA_CEDULA = '";
	//Constante para filtros de query
	private static final String AND = "AND COCC_UNIDAD_NEGOCIO = '";
	//Constante para filtros de query
	private static final String AND2 = "AND COCC_TIPO_CEDULA = ";
	
	/**
	 * Constructor de clase
	 * @param sessionFactory session factory para conexion a DB
	 */
	@Autowired
	public CedulaControlDaoHibernate(SessionFactory sessionFactory) {
		//Se inicializa session factory
		super.setSessionFactory(sessionFactory);
	}
	
	/**
	 * Metodo que consulta detalle de creditos
	 * @param tipocedula tipo cedula
	 * @param idVenta id venta
	 * @param coccRemesaCedula remesa para hacer consulta
	 * @return lista de creditos
	 * @throws Excepciones con error en general
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getDetalleCreditos(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT COCU_NO_CREDITO, COCU_FECHA_INGRESO, COCU_FECHA_CONCILIA, '', COCU_MONTO_PRIMA, 'EMITIDO', COCU_POLIZA, COCU_CERTIFICADO, '' \n");
				consulta.append("FROM COLECTIVOS_CUENTAS \n");
			consulta.append("WHERE COCU_ESTATUS_SEGURO IN(SELECT COCC_CD_CEDULA \n");
											consulta.append(TABLA);
										consulta.append(WHERE).append(coccRemesaCedula).append("' \n");
											consulta.append(AND).append(idVenta).append("' \n");
											consulta.append(AND2).append(tipocedula).append(") \n");
			consulta.append("UNION ALL \n");
			consulta.append("SELECT COCD_NU_CREDITO, COCD_FE_INGRESO, COCD_FE_PROCESO, COCD_APLICA_PND, COCD_MT_PRIMA, COCD_ESTATUS, COCD_NU_POLIZA, COCD_NU_CERTIFICADO, COCD_MOTIVO_RECHAZO \n");
				consulta.append("FROM  COLECTIVOS_CEDULA_DETALLE \n");
			consulta.append("WHERE COCD_COCC_CD_CEDULA IN(SELECT COCC_CD_CEDULA \n");
											consulta.append(TABLA);
										consulta.append(WHERE).append(coccRemesaCedula).append("' \n");
											consulta.append(AND).append(idVenta).append("' \n");
											consulta.append(AND2).append(tipocedula).append(") \n");
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());

			//se ejecuta la consulta
			return query.list();
		} catch (HibernateException e) {
			throw new Excepciones("Error en CedulaControlDao.getDetalleCreditos(): ", e); 
		}
	}

	
	@Override
	public void saveFotoCedula(Integer tipocedula, Integer idVenta, String coccRemesaCedula, Integer tipoCedula2) throws Excepciones {
		try {
			ProcesosPL proceso = new ProcesosPL(getSession().connection());
			proceso.saveFotoCedula(tipocedula, idVenta, coccRemesaCedula, tipoCedula2); 
		} catch (Excepciones | SQLException e) {
			throw new Excepciones("Error: al guardar foto", e);
		}
	}
	
	/**
	 * Metodo que consulta detalle de creditos historicos
	 * @param tipocedula tipo cedula
	 * @param idVenta id venta
	 * @param coccRemesaCedula remesa para hacer consulta
	 * @return lista de creditos
	 * @throws Excepciones con error en general
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getDetalleCredHist(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones {
		StringBuilder consulta;
		Query query;
		String[] arrRemesa;
		
		try {
			consulta = new StringBuilder();
			
			//Obetenemos remesa y version de historico
			arrRemesa = coccRemesaCedula.split("\\_");
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT COCD_NU_CREDITO, COCD_FE_INGRESO, COCD_FE_PROCESO, COCD_APLICA_PND, COCD_MT_PRIMA, COCD_ESTATUS, COCD_NU_POLIZA, COCD_NU_CERTIFICADO, COCD_MOTIVO_RECHAZO \n");
				consulta.append("FROM  COLECTIVOS_CEDULA_DETALLE_HIST \n");
			consulta.append("WHERE COCD_COCC_CD_CEDULA IN(SELECT COCC_CD_CEDULA \n");
											consulta.append(TABLA);
										consulta.append(WHERE).append(arrRemesa[0]).append("' \n");
											consulta.append(AND).append(idVenta).append("' \n");
											consulta.append(AND2).append(tipocedula).append(" \n");
											consulta.append("AND COCC_PROCESADA = ").append(arrRemesa[1]).append(") \n");
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());

			//se ejecuta la consulta
			return query.list();
		} catch (HibernateException e) {
			throw new Excepciones("Error en CedulaControlDao.getDetalleCreditos(): ", e); 
		}
	}
}
