/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author Sergio Plata
 *
 */
public interface ParametrosDao extends CatalogoDao{
	
	public abstract <T> List<T> obtenerCatalogoEndoso(String filtro)throws Excepciones;

	public Long siguienteCdParam() throws Excepciones;

	public <T> List<T> obtenerProductos(String filtro)throws Excepciones;

    public abstract Long siguente( )throws Excepciones;
     
    public Integer existe(String filtro) throws Excepciones;

    public <T> List<T> obtenerCanalVenta(String filtro) throws Excepciones;
    
    public abstract <T> List<T> obtenerCatalogoFechasAjuste(Class<T> objeto)throws Excepciones;
    
    public abstract <T> List<T> obtenerCatalogoFechasAjuste(String filtro)throws Excepciones;

    /**
     * Consulta configuracion comisiones
     * @param filtro
     * @return
     * @throws Excepciones
     * @author vectormx - CJPV
     */
    List<Parametros> consultarConfiguracionComisiones(Parametros filtro) throws Excepciones;
    
    /**
     * Consult detalle configuraciones
     * @param filtro
     * @return
     * @throws Excepciones
     *  @author vectormx - CJPV
     */
    List<Object> consultarConfiguracionComisionesDetalle(Parametros filtro) throws Excepciones;
    
    /**
	 * Elimina la configuracio	n de parametros de comision
	 * @param parametros Comfiguracion a eliminar
	 * @throws Excepciones Error en ejecucion de BD
	 * @author vectormx - CJPV
	 */
	void eliminaParametrosComision(Parametros parametros) throws Excepciones;
	
	/**
	 * Valida parametros comisiones
	 * @param parametros parametros a validar
	 * @throws Excepciones Error parametro existente
	 * @author vectormx - CJPV
	 */
	void validaParametrosComision(Parametros parametros) throws Excepciones;
    
}
