/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.File;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.dao.CargaDao;
import mx.com.santander.aseguradora.colectivos.model.database.ProcesosPL;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Sergio Plata
 *
 */
@Repository
public class CargaDaoHibernate extends HibernateDaoSupport implements CargaDao {

	@Autowired
	public CargaDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@SuppressWarnings("unchecked")
	public <T> List<T> estatusCancelacion(Short canal, Short ramo, Long poliza, String archivo)throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
				StringBuilder consulta = new StringBuilder();
				consulta.append("select 1 as tipo, 'Errores preCarga' as preCarga,count(CP.COPC_ID_CERTIFICADO) as subtotal \n");
				consulta.append("from colectivos_precarga CP			\n");
				consulta.append("where CP.COPC_CD_SUCURSAL = :canal1	\n");
				consulta.append("  and CP.COPC_CD_RAMO = :ramo1			\n");
				consulta.append("  and (:poliza1 = 0 or CP.COPC_NUM_POLIZA = :poliza1) \n");
				consulta.append("  and CP.COPC_DATO11 =").append(":archivo1").append(" \n");
				consulta.append("  and CP.COPC_CARGADA in (9,10)				\n");
				consulta.append("union all								\n");
				consulta.append("select 2, 'Certificados cancelados por el proceso', count(C.COCE_NU_CERTIFICADO)	\n");
				consulta.append("from colectivos_certificados C			\n");
				consulta.append("where C.COCE_CASU_CD_SUCURSAL = :canal2\n");
				consulta.append("  and C.COCE_CARP_CD_RAMO = :ramo2		\n");
				consulta.append("  and (C.COCE_CAPO_NU_POLIZA      = :poliza2      \n");
				consulta.append("       or (substr(C.COCE_CAPO_NU_POLIZA,1,3) in  \n");
				consulta.append("                  (select pa.COPA_NVALOR3      \n");
				consulta.append("                     from COLECTIVOS_PARAMETROS pa       \n");
				consulta.append("                    where pa.COPA_DES_PARAMETRO = 'POLIZA' \n");
				consulta.append("                      and pa.COPA_ID_PARAMETRO  = 'GEP'    \n");
				consulta.append("                      and pa.COPA_NVALOR7     = 1)       \n");
				consulta.append("       and :poliza2 = 0   \n");
				consulta.append("       )) \n");
				consulta.append("  and C.COCE_ST_CERTIFICADO in (10,11)	 \n");
				//consulta.append("  and (C.COCE_CAMPOV5 = ").append(":archivo2").append(" OR trunc(to_date(C.COCE_FE_ANULACION)) = trunc(sysdate))\n");
				consulta.append("  and trunc(to_date(C.COCE_FE_ANULACION)) = trunc(sysdate) 	 \n");
				consulta.append("group by C.COCE_CAPO_NU_POLIZA		 \n");
				/*consulta.append("union all								\n");
				consulta.append("select 3, 'Certificados cancelados antes del proceso de carga', count(C.COCE_NU_CERTIFICADO) \n");
				consulta.append("from colectivos_certificados C			\n");
				consulta.append("where C.COCE_CASU_CD_SUCURSAL = :canal3\n");
				consulta.append("  and C.COCE_CARP_CD_RAMO = :ramo3		\n");
				consulta.append("  and C.COCE_CAPO_NU_POLIZA = :poliza3	\n");
				consulta.append("  and C.COCE_ST_CERTIFICADO = 11		\n");
				consulta.append("  and C.COCE_CAMPOV5 <> ").append(":archivo3").append("\n");
				consulta.append("union all								\n");
				consulta.append("select 4, 'Total certificados cancelados', count(C.COCE_NU_CERTIFICADO) \n");
				consulta.append("from colectivos_certificados C			\n");
				consulta.append("where C.COCE_CASU_CD_SUCURSAL = :canal4\n");
				consulta.append("  and C.COCE_CARP_CD_RAMO = :ramo4		\n");
				consulta.append("  and C.COCE_CAPO_NU_POLIZA = :poliza4 \n");
				consulta.append("  and C.COCE_ST_CERTIFICADO = 11");*/
	
				Query qry = this.getSession().createSQLQuery(consulta.toString());
				
				qry.setParameter("canal1", canal);
				qry.setParameter("ramo1", ramo);
				qry.setParameter("poliza1", poliza);
				qry.setParameter("archivo1", archivo);
				
				qry.setParameter("canal2", canal);
				qry.setParameter("ramo2", ramo);
				qry.setParameter("poliza2", poliza);
				//qry.setParameter("archivo2", archivo);
				
				/*qry.setParameter("canal3", canal);
				qry.setParameter("ramo3", ramo);
				qry.setParameter("poliza3", poliza);
				qry.setParameter("archivo3", archivo);
				
				qry.setParameter("canal4", canal);
				qry.setParameter("ramo4", ramo);
				qry.setParameter("poliza4", poliza);*/
				
				
				List<T> lista = qry.list();
				
				return lista;

		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().update(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor =Excepciones.class)
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones{
		// TODO Auto-generated method stub
		try {
		
			this.getHibernateTemplate().save(objeto);
			return objeto;
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			return (T) this.getHibernateTemplate().load(objeto, id);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			return this.getHibernateTemplate().loadAll(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					StringBuilder consulta = new StringBuilder();
					consulta.append("from PreCarga PC\n");
					consulta.append("where 1 = 1\n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					
					List<T> lista = qry.list();
					
					return lista;
				}
				
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		try {
			if(lista != null){
				for(T objeto: lista){					
					this.getHibernateTemplate().save(objeto);
				}
			}
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void actualizarObjetos(List<T> lista) throws Excepciones {
		try {
			if(lista != null) {
				for(T objeto: lista){
					this.getHibernateTemplate().update(objeto);
				}
			}
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public void borrarDatosTemporales(final Short canal, final Short ramo, final Long poliza,
			final Integer inIdVenta, final String archivo, final String origen) throws Excepciones {
		// TODO Auto-generated method stub
		
		
		try {
			
			StringBuilder query = new StringBuilder(100);
			Query qry;
			
			
			// Borramos la precarga
			query.append("delete PreCarga CP	                        \n");
			query.append("where CP.id.copcCdSucursal   = :canal         \n");
			query.append("  and CP.id.copcCdRamo       = :ramo          \n");
		    query.append("  and (CP.id.copcNumPoliza   = :poliza        \n");
		    query.append("       or (substr(CP.id.copcNumPoliza,1,3) in \n");
		    query.append("                  (select pa.copaNvalor3     \n");
		    query.append("                     from Parametros pa      \n");
		    query.append("                    where pa.copaDesParametro = 'POLIZA' \n");
		    query.append("                      and pa.copaIdParametro  = 'GEP'    \n");
		    query.append("                      and pa.copaNvalor7      = 1)       \n");
		    query.append("       and :poliza = 0)) \n");            
		    query.append("  and (:idVenta = 0 or CP.copcDato16 = :idVenta)  \n");		    
			query.append("  and (CP.copcDato11        = :archivo \n");
			query.append("       or :archivo          = '*') \n");
			query.append("  and (:origen = '*' or CP.id.copcTipoRegistro = :origen) \n");
			
			qry = this.getSession().createQuery(query.toString());// .createSQLQuery(query.toString());
			
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("poliza", poliza);
			qry.setParameter("idVenta", inIdVenta);
			qry.setParameter("origen", origen);
			qry.setParameter("archivo", archivo);
			
			
			qry.executeUpdate();
			
			// Borramos la carga
			Utilerias.resetMessage(query);
			query.append("delete Carga C  	               \n");
			query.append("where C.id.cocrCdSucursal      = :canal       \n");
			query.append("  and C.id.cocrCdRamo          = :ramo	    \n");
			query.append("  and (C.id.cocrNumPoliza      = :poliza      \n");
			query.append("       or (substr(C.id.cocrNumPoliza,1,3) in  \n");
			query.append("                  (select pa.copaNvalor3      \n");
		    query.append("                     from Parametros pa       \n");
		    query.append("                    where pa.copaDesParametro = 'POLIZA' \n");
		    query.append("                      and pa.copaIdParametro  = 'GEP'    \n");
		    query.append("                      and pa.copaNvalor7      = 1)       \n");
		    query.append("       and :poliza = 0)) \n");              
		    query.append("  and (:idVenta = 0 or C.cocrDato16 = :idVenta) 	   	   \n");
			query.append("  and (C.cocrDato11 		  = :archivo \n");
			query.append("       or :archivo          = '*') \n");
			query.append("  and (:origen = '*' or C.id.cocrTipoRegistro = :origen) \n");
			
			qry = this.getSession().createQuery(query.toString());
			
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("poliza", poliza);
			qry.setParameter("idVenta", inIdVenta);
			qry.setParameter("origen", origen);
			qry.setParameter("archivo", archivo);
			
			qry.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(readOnly =  true, propagation = Propagation.SUPPORTS)
	@SuppressWarnings("unchecked")
	public <T> List<T> estatusEmision(final Short canal, final Short ramo, final Long poliza, final Integer idVenta, final String archivo)throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			StringBuilder consulta = new StringBuilder();
			
			consulta.append("select 1 as tipo, 'Errores preCarga' as preCarga, CP.COPC_NUM_POLIZA as Poliza, count(CP.COPC_ID_CERTIFICADO) as subtotal \n");
			consulta.append("from colectivos_precarga CP \n");
			consulta.append("where CP.COPC_CD_SUCURSAL = :canal1 \n");
			consulta.append("  and CP.COPC_CD_RAMO = :ramo1 \n");
			consulta.append("  and (:poliza1 = 0 or CP.COPC_NUM_POLIZA = :poliza1) \n");
			consulta.append("  and (:idVenta1 = 0 or CP.COPC_DATO16 = :idVenta1) \n");
			consulta.append("  and CP.COPC_DATO11 =").append(":archivo1").append("\n");
			consulta.append(" group by CP.COPC_NUM_POLIZA \n");
			
			consulta.append("union all \n");
			
			consulta.append("select 2, 'Errores carga', CC.COCR_NUM_POLIZA, count(CC.COCR_ID_CERTIFICADO) \n");
			consulta.append("from colectivos_carga CC \n");
			consulta.append("where CC.COCR_CD_SUCURSAL = :canal2 \n");
			consulta.append("  and CC.COCR_CD_RAMO = :ramo2 \n");
			consulta.append("  and (:poliza2 = 0 or CC.COCR_NUM_POLIZA = :poliza2) \n");
			consulta.append("  and (:idVenta2 = 0 or CC.COCR_DATO16 = :idVenta2) \n");
			consulta.append("  and CC.COCR_DATO11 = ").append(":archivo2").append(" \n");
			consulta.append(" group by CC.COCR_NUM_POLIZA \n");
			
			consulta.append("union all \n");
			
			consulta.append("select 3, 'Certificados emitidos', C.COCE_CAPO_NU_POLIZA, count(C.COCE_NU_CERTIFICADO) \n");
			consulta.append("from colectivos_certificados C \n");
			consulta.append("where C.COCE_CASU_CD_SUCURSAL = :canal4 \n");
			consulta.append("  and C.COCE_CARP_CD_RAMO = :ramo4 \n");
			consulta.append("  and (C.COCE_CAPO_NU_POLIZA      = :poliza4      \n");
			consulta.append("       or (substr(C.COCE_CAPO_NU_POLIZA,1,3) in  \n");
			consulta.append("                  (select pa.COPA_NVALOR3      \n");
			consulta.append("                     from COLECTIVOS_PARAMETROS pa       \n");
			consulta.append("                    where pa.COPA_DES_PARAMETRO = 'POLIZA' \n");
			consulta.append("                      and pa.COPA_ID_PARAMETRO  = 'GEP'    \n");
			consulta.append("                      and pa.COPA_NVALOR7     = 1)       \n");
			consulta.append("       and :poliza4 = 0   \n");
			consulta.append("       and C.COCE_SUB_CAMPANA = :idVenta4 \n");
			consulta.append("       )) \n");
			consulta.append("  and C.COCE_ST_CERTIFICADO in (1,10) \n");
			consulta.append("  and C.COCE_NU_CERTIFICADO > 0 \n");
			consulta.append("  and C.COCE_CAMPOV5 = ").append(":archivo4").append(" \n");
			consulta.append(" group by C.COCE_CAPO_NU_POLIZA \n");
			
			consulta.append(" order by 3");
			
			
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			
			//Pasamos parametros.
			qry.setParameter("canal1", canal);
			qry.setParameter("ramo1", ramo);
			qry.setParameter("poliza1", poliza);
			qry.setParameter("idVenta1", idVenta);
			qry.setParameter("archivo1", archivo);
			
			qry.setParameter("canal2", canal);
			qry.setParameter("ramo2", ramo);
			qry.setParameter("poliza2", poliza);
			qry.setParameter("idVenta2", idVenta);
			qry.setParameter("archivo2", archivo);
						
			qry.setParameter("canal4", canal);
			qry.setParameter("ramo4", ramo);
			qry.setParameter("poliza4", poliza);
			qry.setParameter("idVenta4", idVenta);
			qry.setParameter("archivo4", archivo);
			
			
			List<T> lista = qry.list();
			
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

	}

	@SuppressWarnings("unchecked")
	public List<Object> existeCargaConcilia(Short origen) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			StringBuilder consulta = new StringBuilder();
			consulta.append("from TraspasoColectivos con        \n");
			consulta.append("where con.id.trseTipoRegistro  = 'C1'  \n");
			consulta.append("  and con.id.trseClaveProducto = :origen  \n");
			consulta.append("  and rownum < 2					\n");
								
			Query qry = getSession().createQuery(consulta.toString());
			
			qry.setParameter("origen", origen);
			
			List<Object> lista = qry.list();
			
			return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	public Double getCumulo(Short inCanal, Short inRamo, Long inPoliza,
			Integer inIdVenta) throws Excepciones {
		// TODO Auto-generated method stub
		
		Double cumulo;
		
		try {
			StringBuilder consulta = new StringBuilder();
			consulta.append("select par.copaVvalor8 				  \n");
			consulta.append("  from Parametros par        			  \n");
			consulta.append(" where par.copaDesParametro  = 'POLIZA'  \n");
			consulta.append("   and par.copaIdParametro   = 'CUMULOS' \n");
			consulta.append("   and par.copaNvalor1       = :canal 	  \n");
			consulta.append("   and par.copaNvalor2       = :ramo 	  \n");
			consulta.append("   and par.copaNvalor3       = :poliza	  \n");
			consulta.append("   and par.copaNvalor4       = :idVenta  \n");
			
					
			Query qry = getSession().createQuery(consulta.toString());
			
			qry.setShort("canal", inCanal);
			qry.setShort("ramo", inRamo);
			qry.setLong("poliza", inPoliza);
			qry.setInteger("idVenta", inIdVenta);
			
			List<Object> lista = qry.list();
			
			if (lista.isEmpty())
				return -1.0;
			
			cumulo = Double.parseDouble((String)lista.get(0));
			
			return cumulo;
			
		} catch (Exception e) {
			// TODO: handle exception
			return -1.0;
		}
	}

	@SuppressWarnings("deprecation")
	public void reporteErrores(String rutaTemporal, String rutaReporte,
			Map<String, Object> inParams) throws Excepciones {
		// TODO Auto-generated method stub
		JasperReport reporte;
		JasperPrint datos;
		
		
		try{
			reporte = JasperCompileManager.compileReport(rutaReporte);
			datos = JasperFillManager.fillReport(reporte, inParams, getSession().connection());
			
			JRCsvExporter exporter = new JRCsvExporter();
			File file = new File(rutaTemporal);
			
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, datos);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, file.toString());
			
			
			exporter.exportReport();
			
		}catch(Exception e){
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void estatusCancelacion(List<BeanClienteCertif> listaCertificados, List<BeanClienteCertif> listaCertificadosCancelacion)	throws Excepciones {
		
		String descError;
		Query qry;
		
		
		for (BeanClienteCertif o: listaCertificados){
			if (o.isCheck()){
				StringBuilder consulta = new StringBuilder();
				consulta.append("from ClienteCertif  CC    \n");
				consulta.append("where CC.id.coccCasuCdSucursal   = :canal         \n");
				consulta.append("  and CC.id.coccCarpCdRamo       = :ramo 	       \n");
				consulta.append("  and CC.id.coccCapoNuPoliza     = :poliza 	   \n");
				consulta.append("  and CC.id.coccNuCertificado    = :certificado   \n");
				consulta.append("  and CC.id.coccIdCertificado    = :id   		   \n");
				
				
				qry = getSession().createQuery(consulta.toString());
				
				qry.setShort("canal", o.getId().getCoccCasuCdSucursal());
				qry.setShort("ramo", o.getId().getCoccCarpCdRamo());
				qry.setLong("poliza", o.getId().getCoccCapoNuPoliza());
				qry.setLong("certificado", o.getId().getCoccNuCertificado());
				qry.setString("id", o.getId().getCoccIdCertificado());
				
				List<ClienteCertif> l1 = qry.list();
				
				for (ClienteCertif clc:l1){
					
					descError = "";

					consulta = null;
					consulta = new StringBuilder();
					
					consulta.append("select CAN.copcRegistro							  \n");
					consulta.append("from PreCarga CAN									  \n");
					consulta.append("where CAN.id.copcCdSucursal    = :canal			  \n");
					consulta.append("  and CAN.id.copcCdRamo        = :ramo				  \n");
					consulta.append("  and CAN.id.copcIdCertificado = :id				  \n");
					consulta.append("  and CAN.copcCargada          in ('9','10')		  \n");
					consulta.append("  and CAN.id.copcTipoRegistro  = 'CAN'				  \n");
					consulta.append("  and CAN.copcDato11           = 'CANCELACION INDIV' \n");
					
					qry = getSession().createQuery(consulta.toString());
					
					qry.setShort("canal", o.getId().getCoccCasuCdSucursal());
					qry.setShort("ramo", o.getId().getCoccCarpCdRamo());
					qry.setString("id", o.getId().getCoccIdCertificado());
					
					List<Object> l2 = qry.list();
										
					if (l2.size() >0)
						descError = (String) l2.get(0);
					
					clc.getCertificado().setCoceCampov5(descError);
					
					listaCertificadosCancelacion.add((BeanClienteCertif)ConstruirObjeto.crearBean(BeanClienteCertif.class, clc));
					
				}

			}
		}
		
		// Borramos la precarga
		StringBuilder consulta = new StringBuilder(); 
		consulta.append("delete PreCarga CP	                                  \n");
		consulta.append("where CP.copcCargada          in ('9','10')		  \n");
		consulta.append("  and CP.id.copcTipoRegistro  = 'CAN'				  \n");
		consulta.append("  and CP.copcDato11           = 'CANCELACION INDIV'  \n");
		
		qry = getSession().createQuery(consulta.toString());
		
		qry.executeUpdate();
		
	}
		
	@SuppressWarnings("unchecked")
	public Collection<? extends Object> consultarVentasConsumo(long tipoReg, int ramo, String strOrigen, String strConsulta) throws Exception {
		List<Object> lstResultado;
		Query query;
		
		try {
			query = getSession().createQuery(strConsulta);
			query.setLong("tiporeg", tipoReg);
			query.setInteger("ramo", ramo);
			query.setString("origen", strOrigen);
			
			lstResultado = query.list();
		} catch (Exception e) {
			throw new Exception("Error:: CargaDaoHibernate.consultarVentasConsumo:: " + e.getMessage());
		}
		return lstResultado;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<? extends Object> consultar(String strConsulta) throws Exception {
		List<Object> lstResultado;
		Query query;
		
		try {	
			query = getSession().createSQLQuery(strConsulta);
			lstResultado = query.list();
		} catch (Exception e) {
			throw new Exception("Error:: CargaDaoHibernate.consultar:: " + e.getMessage());
		}
		
		return lstResultado;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> consultarMapeo(String strQuery) throws Exception {
		List<Object> lstResultado;
		Query query;
		
		try {
			query = getSession().createQuery(strQuery);
			lstResultado = query.list();
		} catch (Exception e) {
			throw new Exception("Error:: CargaDaoHibernate.consultarMapeo:: " + e.getMessage());
		}
		return lstResultado;
	}
	
	public ArrayList<Object> consultarRamos(int nDatosRamos) throws Exception {
		ArrayList<Object> arlResultado;
		StringBuffer sbQuery;
		
		try {
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT hpp.hopp_prod_colectivo Prod, ");
					sbQuery.append("hpp.hopp_plan_colectivo Plan, ");
					sbQuery.append("hpp.hopp_dato1 Tarifa, ");
					sbQuery.append("hpp.hopp_plan_banco_rector SubProducto, ");
					sbQuery.append("hpp.hopp_cd_ramo Ramo, ");
					sbQuery.append("DECODE(hpp.hopp_cd_ramo, 61, 'VID', 'DES') TipoReg, ");
					sbQuery.append("hpp.hopp_dato2 Comodin, ");
					sbQuery.append("hpp.hopp_prod_banco_rector ProdBanco, ");
					sbQuery.append("hpp.hopp_cd_comision RangoEdad, ");
					sbQuery.append("hpp.hopp_porc_comision RangoEdad2, ");
					sbQuery.append("hpp.hopp_dato4 Plazo ");
				sbQuery.append("FROM homologa_prodplanes hpp ");
			sbQuery.append("WHERE hpp.hopp_cd_ramo IN (9, 61, 89) ");
				sbQuery.append("AND hpp.hopp_dato3 = ").append(nDatosRamos);
			sbQuery.append(" ORDER BY 7, 8 ASC");
			arlResultado.addAll(consultar(sbQuery.toString()));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.consultarRamos::" + e.getMessage());
		}
			
		return arlResultado;
	}

	@SuppressWarnings("deprecation")
	public void divisionAutoCredito() throws Exception {
		// TODO Auto-generated method stub
		
		ProcesosPL proceso= new ProcesosPL (getSession().connection());
		proceso.divisionAutoCredito();
		
	}

	public void actualizarCargasManuales(String idCertificado, String canal, String ramo, String poliza, String sistemaOrigen) throws Exception {
		Query qry;
		StringBuilder sbConsulta;
		
		try {
			sbConsulta = new StringBuilder();
			
			sbConsulta.append("UPDATE VtaPrecarga VP \n");
			sbConsulta.append("	SET VP.cargada = ").append(Constantes.CARGADA_CONCILIADO_MANUAL).append(" \n");
			sbConsulta.append("WHERE VP.id.cdSucursal = ").append(canal).append(" \n");
			sbConsulta.append("	AND VP.id.cdRamo = ").append(ramo).append(" \n");
			sbConsulta.append("	AND VP.id.numPoliza = ").append(poliza).append(" \n");
			sbConsulta.append("	AND VP.id.idCertificado = '").append(idCertificado).append("' \n");
			sbConsulta.append("	AND VP.id.sistemaOrigen = '").append(sistemaOrigen).append("' \n");
			
			qry = getSession().createQuery(sbConsulta.toString());
			qry.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.actualizarCargasManuales::" + e.getMessage());
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class )
	public void respaldarErrores(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta, String nombreArchivo) throws Exception {
		try {
			ProcesosPL proceso= new ProcesosPL (getSession().connection());
			
			proceso.respaldarErrores(inCanal, inRamo,inPoliza, inIdVenta, nombreArchivo); 
					
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	public void actualizarCuentasLE(String strIdCertificado, String strFechaIngreso) throws Exception {
		StringBuilder sbUpdate;
		Query query;
		int nDiasSumar = 1;
		Date dFechaIngreso;
		
		try {
			sbUpdate = new StringBuilder();
			
			dFechaIngreso = GestorFechas.generateDate2(strFechaIngreso, "dd/MM/yyyy");
			if(GestorFechas.getDiadelaSemana(dFechaIngreso) == Calendar.FRIDAY) {
				nDiasSumar = 3;
			}
			strFechaIngreso = GestorFechas.formatDate(GestorFechas.sumarRestasDiasAFecha(dFechaIngreso, nDiasSumar, Constantes.FECHA_SUMA_DIAS), "dd/MM/yyyy");			
			
			sbUpdate.append("UPDATE \n");
			sbUpdate.append("TLMK_FOLIOS_MEDIOS_PAGO MP ");
			sbUpdate.append("SET MP.CARGADA = 0 \n");
			sbUpdate.append("WHERE MP.NU_CUENTA = 65500520154 \n");
			sbUpdate.append("	AND MP.NU_DE_CONTRATO = '" + strIdCertificado + "' \n");
			sbUpdate.append("  	AND MP.FECHA_APROBACION = TO_DATE(:fechaIngreso, 'dd/MM/yyyy')");
			
			query = getSession().createSQLQuery(sbUpdate.toString()); 
			query.setString("fechaIngreso", strFechaIngreso);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.actualizarCuentasLE::" + e.getMessage());
		}
	}

	public void eliminarDatosReprocesoLE(String tipoError, String strFiltro) throws Exception {
		StringBuilder sbDelete;
		Query query;
		String strCargada;
		try {
			sbDelete = new StringBuilder();
			
			if(tipoError.equals(Constantes.CODIGO_ERROR_EMISION)) {
				strCargada = "2";
				sbDelete.append("DELETE \n");
				sbDelete.append("COLECTIVOS_PRECARGA CP \n");			
				sbDelete.append("WHERE CP.COPC_CD_SUCURSAL = 1 \n");
				sbDelete.append("AND CP.COPC_CD_RAMO = 9 \n");
				sbDelete.append("AND CP.COPC_DATO11 " + strFiltro);
				sbDelete.append("AND CP.COPC_DATO16 = '8' \n");
				query = getSession().createSQLQuery(sbDelete.toString()); 
				query.executeUpdate();
				
				sbDelete = new StringBuilder();
				sbDelete.append("DELETE \n");
				sbDelete.append("COLECTIVOS_CARGA CC \n");			
				sbDelete.append("WHERE CC.COCR_CD_SUCURSAL = 1 \n");
				sbDelete.append("AND CC.COCR_CD_RAMO = 9 \n");
				sbDelete.append("AND CC.COCR_DATO11 " + strFiltro);
				sbDelete.append("AND CC.COCR_DATO16 = '8' \n");
				query = getSession().createSQLQuery(sbDelete.toString()); 
				query.executeUpdate();
			} else {
				strCargada = "3";
			}
			
			sbDelete = new StringBuilder();
			sbDelete.append("DELETE \n");
			sbDelete.append("VTA_PRECARGA  VP \n");			
			sbDelete.append("WHERE VP.VTAP_SISTEMA_ORIGEN = 'LE' \n");
			sbDelete.append("	AND VP.VTAP_CARGADA = '" + strCargada + "' \n");
			query = getSession().createSQLQuery(sbDelete.toString()); 
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.actualizarCuentasLE::" + e.getMessage());
		}
	} 

	/**APV**/
	public void updatePampa() throws Excepciones {
		StringBuilder query = new StringBuilder(100);
		Query qry;
		
		try {
			
			query.append("UPDATE VtaPrecarga vt ");
			query.append(" SET vt.estatus='1' ");
			query.append("  WHERE vt.estatus = '0' ");
		    query.append("  and vt.id.sistemaOrigen='BT' ");
			qry = getSession().createQuery(query.toString());
			qry.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	} 
	
	public void updatePampaCancelacion() throws Excepciones {
		StringBuilder query = new StringBuilder();
		Query qry;
	
		try {
		    
			query.append("update COLECTIVOS_CERTIFICADOS C");
		    query.append("  set C.COCE_CAMPOF3  = trunc(sysdate)");
		    query.append("     ,C.COCE_MT_BCO_DEVOLUCION  = C.COCE_MT_DEVOLUCION");
		    query.append("where C.COCE_CASU_CD_SUCURSAL    = 1");
		    query.append("  and C.COCE_CARP_CD_RAMO        = 9");
		    query.append("  and C.COCE_CAPO_NU_POLIZA      > 600000000");
		    query.append("  and C.COCE_CAPO_NU_POLIZA      < 700000000");
		    query.append("  and C.COCE_SUB_CAMPANA         = '6'");
		    query.append("  and C.COCE_FE_ANULACION        > sysdate -1");
		    query.append("  and nvl(C.COCE_MT_DEVOLUCION,-1)  >  0");
		    query.append("  and C.COCE_CAMPOF3              is null");
		    
			qry = getSession().createSQLQuery(query.toString());// .createSQLQuery(query.toString());
			qry.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}
	
	/******* Se hace el update del status, segun sea cobrado o no cobrado*/
	public void buscarCuentaPampa(String[] strCuenta) throws Exception {
		StringBuilder query = new StringBuilder(100);
		Query qry; 
		
		try {
			
			query.append("  UPDATE VtaPrecarga vt ");
			query.append("  SET vt.cargada=0 ,");
			query.append("  vt.estatus= decode(:estatus,'COBRADO','2','3') ");
			query.append("  , vt.ddato3= trunc(sysdate)");
			query.append("  WHERE vt.estatus = '1' ");
		    query.append("  and vt.id.sistemaOrigen='BT' ");
		    query.append("  and vt.cuentaAlterna=:TDC ");
		    
			qry = getSession().createQuery(query.toString());
			qry.setString("estatus",strCuenta[1]);
			qry.setString("TDC",strCuenta[0]);
			qry.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}

	}
	/**End APV**/
	
	public String getCodigoEstado(String strCP) throws Exception {
		String strCdEstado;
		ArrayList<Object> arlResultado;
		StringBuffer sbQuery;
		
		try {
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT E.CAIV_CAES_CD_ESTADO  ");
				sbQuery.append("FROM CART_ESTADOS_IVA  E ");
			sbQuery.append("WHERE E.CAIV_CP_POBLACION = TO_NUMBER('").append(strCP).append("')");
				sbQuery.append("AND ROWNUM = 1");
			arlResultado.addAll(consultar(sbQuery.toString()));
			
			if(arlResultado.isEmpty()) {
				strCdEstado = "33";
			} else {
				strCdEstado = arlResultado.get(0).toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.getCodigoEstado::" + e.getMessage());
		}
			
		return strCdEstado;
	}
}
