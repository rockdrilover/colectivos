package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.Date;


/**
 * @author dflores
 *
 */

public interface AltaDao {
	
	public abstract Long iniciaAlta(int canal, Short ramo,String analista,int sucursal,String domicilio,String cedula_rif,Date fecha_ini);
	
}
