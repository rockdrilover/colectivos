/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ColectivosComponentesDao extends CatalogoDao{

	public void respaldaIVA(BigDecimal tarifa)	throws Excepciones;
	
	/**
	 * Actualiza comision a 0
	 * @param colectivosCoberturas registro modificado
	 * @throws Excepciones Error al registrar 
	 */
	void actualizaProductoPlan(ColectivosCoberturas  colectivosCoberturas) throws Excepciones;
	
	/**
	 * Actualiza comision a 0
	 * @param colectivosCoberturas registro modificado
	 * @throws Excepciones Error al registrar 
	 */
	void actualizaParametrizacionProductoPlan(Parametros parametrosConfiguracion) throws Excepciones;

	
	/**
	 * Actualiza comision a -1
	 * @param colectivosCoberturas registro modificado
	 * @throws Excepciones Error al registrar 
	 */
	void actualizaCobertura(ColectivosCoberturas  colectivosCoberturas) throws Excepciones;
	
	/**
	 * Actualiza comision a -1
	 * @param parametrosConfiguracion
	 * @throws Excepciones
	 */
	void actualizaParametrizacionCobertura(Parametros parametrosConfiguracion) throws Excepciones;
	

}
