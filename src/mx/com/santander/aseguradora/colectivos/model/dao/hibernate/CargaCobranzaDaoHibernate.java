package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.sql.DataSource;
import mx.com.santander.aseguradora.colectivos.model.dao.CargaCobranzaDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import oracle.jdbc.OracleTypes;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		03-11-2022
 * Description: Clase de tipo DAO que tiene acceso a base de datos para 
 * 				el proceso de Carga Cobranza
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:		
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Repository
public class CargaCobranzaDaoHibernate extends HibernateDaoSupport implements CargaCobranzaDao {
	//Implementacion de serializable
	private static final long serialVersionUID = 298075158826438461L;
	
	//Constantes Estaticas
	private static final String CONCEPTO_REVERSO = "";
	private static final Integer PAGADO = 2;
	
	/**
	 * Constructor de clase
	 * @param sessionFactory session factory para conexion a DB
	 */
	@Autowired
	public CargaCobranzaDaoHibernate(SessionFactory sessionFactory) {
		//Se inicializa session factory
		super.setSessionFactory(sessionFactory);
	}

	/**
	 * Metodo que ejecuta proceso de cobranza
	 * @param sucursal canal seleccionado
	 * @param ramo ramo seleccionado
	 * @param poliza poliza seleccionado
	 * @param tipoCarga tipo de carga seleccionado
	 * @param archivo nombre de archivo
	 * @return numero de errores
	 * @throws Excepciones  con errores en general
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Integer cargaCobranza(Short sucursal, Short ramo, Integer poliza, Integer tipoCarga, String archivo) throws Excepciones {
		try {
			//Se inicializa DataSource
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			
			//Se inicializa clase para ejecutar SP
			ProcesoCargaCobranza migra = new ProcesoCargaCobranza(ds);
			
			//Se ejecuta SP
			return (Integer) migra.execute(sucursal, ramo, poliza, tipoCarga, archivo).get("w_mensaje");
		} catch (RuntimeException e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	/**
	 * Clase que nos sirve para ejecutar SP de cobranza
	 * @author Issac
	 *
	 */
	private static class ProcesoCargaCobranza extends StoredProcedure{
		
		/**
		 * Constructor de clase
		 * @param ds datasource
		 */
		public ProcesoCargaCobranza(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.PROCESO_CARGA_COBRANZA);
			super.declareParameter(new SqlParameter("w_cd_sucursal", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_ramo", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_nu_poliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_tipo_carga", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_archivo", Types.VARCHAR));
			super.declareParameter(new SqlOutParameter("w_mensaje", OracleTypes.INTEGER));
			super.compile();
		}
		
		/**
		 * Metodo que manda a ejecutar el SP de Proceso Cobranza
		 * @param sucursal canal seleccionado
		 * @param ramo ramo seleccionado
		 * @param poliza poliza seleccionado
		 * @param tipoCarga tipo de carga seleccionado
		 * @param archivo nombre archivo
		 * @return regresa respuesta del paquete
		 * @throws DataAccessException con error de ejecucion
		 */
		@SuppressWarnings("rawtypes")
		public Map execute(Short sucursal, Short ramo, Integer poliza, Integer tipoCarga, String archivo) {
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("w_cd_sucursal", sucursal);
			inParams.put("w_cd_ramo", ramo);
			inParams.put("w_nu_poliza", poliza);
			inParams.put("w_tipo_carga", tipoCarga);	
			inParams.put("w_archivo", archivo);	
			return super.execute(inParams);
		}

	}

	/**
	 * Metodo que consulta los errores de la carga
	 * @param canal canal seleccionado
	 * @param ramo ramo seleccionado
	 * @param poliza poliza seleccionado
	 * @param tipoCarga tipo de carga seleccionado
	 * @param nombreArchivo nombre archivo cargado
	 * @return lista de errores
	 * @throws Excepciones  con errores en general
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> erroresCargaCobranza(Short canal, Short ramo, Integer poliza, Integer tipoCarga, String nombreArchivo) throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			//Inicializamos propiedades
			consulta = new StringBuilder();
				
			tipoCarga = tipoCarga + 3;

			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT COCC_LINEA \n");
			consulta.append("FROM COLECTIVOS_COBRANZA_CARGA \n");
			consulta.append("WHERE COCC_FAC_CANAL = :canal \n");
			consulta.append("AND COCC_FAC_RAMO = :ramo \n");
			consulta.append("AND COCC_FAC_POLIZA = :poliza \n");
			consulta.append("AND COCC_ARCHIVO = :nombreArchivo \n");
			consulta.append("AND COCC_CARGADA =:tipoCarga \n");
			consulta.append("ORDER BY 1 \n");

			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se agregan parametros necesarios para realizar consulta
			query.setParameter("canal", canal);
			query.setParameter("ramo", ramo);
			query.setParameter("poliza", poliza);
			query.setParameter("nombreArchivo", nombreArchivo);
			query.setParameter("tipoCarga", tipoCarga);
			
			//se ejecuta la consulta
			return query.list();
		} catch (HibernateException e) {
			throw new Excepciones("Error en CargaCobranzaDao.erroresCargaCobranza(): ", e); 
		}
	}

	/**
	 * Metodo que aplica cobranza al registro seleccionado
	 * @param coceCasuCdSucursal canal del recibo
	 * @param coceCarpCdRamo ramo del recibo
	 * @param coceCapoNuPoliza poliza del recibo
	 * @param coceNuCertificado certificado del recibo
	 * @param coceNuCredito numero de credito
	 * @param coceNoRecibo numero de recibo
	 * @throws Excepciones con error en general
	 */
	@Override
	public void actualizaReciboCobranza(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza, Long coceNuCertificado, String coceNuCredito, BigDecimal coceNoRecibo) throws Excepciones {
		String strQuery;
		Query query;
		      
		try {	
			//Se realiza update
			strQuery = "UPDATE COLECTIVOS_COBRANZA_DET det \n" +
					"	set det.COCD_ESTATUS = :estatus \n" +
					"		, det.COCD_CONCEPTO = :concepto \n" +
					"		, det.COCD_MT_NOC_PRIMA_TOTAL = det.COCD_MT_COB_PRIMA_TOTAL \n" +
					"		, det.COCD_FECHA_COBRO = SYSDATE \n" +
					"		, det.COCD_FECHA_NC = SYSDATE \n" +
					"where det.COCD_CASU_CD_SUCURSAL 	= :canal \n" +
					"and det.COCD_CARP_CD_RAMO       	= :ramo\n" +
					"and det.COCD_CAPO_NU_POLIZA      	= :poliza\n" +
					"and det.COCD_COCE_NU_CERTIFICADO	= :certi \n" +
					"and det.COCD_CORE_NU_RECIBO    	= :recibo \n";
			
			//Se crea query
			query = getSession().createSQLQuery(strQuery); 
			
			//Se mandan parametros
			query.setParameter("estatus", PAGADO);
			query.setParameter("concepto", CONCEPTO_REVERSO);
			query.setParameter("canal", coceCasuCdSucursal);
			query.setParameter("ramo", coceCarpCdRamo);
			query.setParameter("poliza", coceCapoNuPoliza);
			query.setParameter("certi", coceNuCertificado);
			query.setParameter("recibo", coceNoRecibo);
			
			//Se ejecuta Query
			query.executeUpdate();
			
		} catch (HibernateException e) {
			throw new Excepciones("Error: Al actualizar recibo . ", e);
		}
	}

}
