/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.CiudadDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * @author Sergio Plata
 *
 */
@Repository
public class CiudadDaoHibernate extends HibernateDaoSupport implements CiudadDao {

	
	/**
	 * Anotacion que le indica a spring que hay que inyectarle el sessionFactory
	 */
	@Autowired
	public CiudadDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	

	@SuppressWarnings("unchecked")
	public  List<Object> obtenerCiudad() throws Excepciones{{
		// TODO Auto-generated method stub
		List<Object> ciudades ;
	    	StringBuilder query = null;
	    	List<Object> lista = null;
	    	try {
	    		
	    		query = new StringBuilder();
	    		lista = new ArrayList<Object>();
	    		query.append("select e.caci_cd_ciudad, \n");
	    		query.append("       e.caci_de_ciudad \n");
	    		query.append("  from cart_ciudades e    \n");
	    		query.append("order by e.caci_cd_ciudad ");
	    		lista = getSession().createSQLQuery(query.toString()).list();
	    		//reporte = lista.get(0).toString();
			} catch (Exception e) {
				// TODO: handle exception
				throw new Excepciones("Error al recuperar reporte impresion de poliza", e);
			}
	    	
	    	return lista;

	    }
		
	}


	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {

		try {
				
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					
					StringBuffer sb = new StringBuffer(150);
					sb.append("from CartCiudades E	\n");
					sb.append("where 1 = 1      \n");
					sb.append(filtro);
					sb.append("order by E.id");
									
					Query query = sesion.createQuery(sb.toString());
					
					List<T> lista = query.list();
					return lista;
				}
			});
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos() throws Excepciones {
		// TODO Auto-generated method stub
     try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer sb = new StringBuffer(150);
					sb.append("from Estado E	\n");
					sb.append("where 1 = 1      \n");
					sb.append("order by E.id");
									
					Query query = sesion.createQuery(sb.toString());
					
					List<T> lista = query.list();
					return lista;
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

	}
}
