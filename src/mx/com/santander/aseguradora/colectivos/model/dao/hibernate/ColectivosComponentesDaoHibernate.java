/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ColectivosComponentesDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Leobardo Preciado
 *
 */
@Repository
public class ColectivosComponentesDaoHibernate extends HibernateDaoSupport implements
		ColectivosComponentesDao {

	@Autowired
	public ColectivosComponentesDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			T producto = (T) this.getHibernateTemplate().get(objeto, id);
			return producto;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			List<T> lista = this.getHibernateTemplate().loadAll(objeto);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro)throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer sb = new StringBuffer(150);
					sb.append("from ColectivosComponentes P	\n");
					sb.append("where 1 = 1      \n");
					sb.append(filtro);
					sb.append("order by P.id");
									
					Query query = sesion.createQuery(sb.toString());
					
					List<T> lista = query.list();
					return lista;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}

		
	}

	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().merge(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			for(T objeto: lista){
				
				this.guardarObjeto(objeto);
			}
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void respaldaIVA(BigDecimal tarifa)	throws Excepciones {
		Query qry;
		// Respaldamos los componentes de IVA
		StringBuilder consulta = new StringBuilder(); 
		consulta.append("insert into colectivos_componentes \n");
		consulta.append("( select  \n");
		consulta.append("         COCT_CASU_CD_SUCURSAL, COCT_CARP_CD_RAMO, COCT_CAPO_NU_POLIZA,       \n");
		consulta.append("         (select max(nvl(coct_cace_nu_certificado,0))+1 \n");
		consulta.append("         from  \n");
		consulta.append("                colectivos_componentes comp \n");
		consulta.append("         where \n");
		consulta.append("                comp.coct_estatus = 20 and \n");
		consulta.append("                comp.coct_ta_componente <> 0 and \n");
		consulta.append("                comp.coct_capp_cd_componente = 'IVA') nu_certificado, \n");
		consulta.append("   \n");
		consulta.append("         COCT_CAPP_CD_COMPONENTE,   \n");
		consulta.append("         COCT_CER_NU_COMPONENTE,    \n");
		consulta.append("         COCT_CAPU_CD_PRODUCTO,     \n");
		consulta.append("         COCT_CAPB_CD_PLAN,         \n");
		consulta.append("         COCT_SEC_NU_COMPONENTE,    \n");
		consulta.append("         COCT_POL_NU_COMPONENTE,    \n");
		consulta.append("         COCT_FE_DESDE,             \n");
		consulta.append("         sysdate,             \n");
		consulta.append("         COCT_TA_COMPONENTE,        \n");
		consulta.append("         21,              \n");
		consulta.append("         COCT_FE_BAJA,              \n");
		consulta.append("         COCT_CAMPON1,              \n");
		consulta.append("         COCT_CAMPON2,              \n");
		consulta.append("         COCT_CAMPOV1,              \n");
		consulta.append("         COCT_CAMPOV2,              \n");
		consulta.append("         COCT_CAMPOF1,              \n");
		consulta.append("         COCT_CAMPOF2 \n");
		consulta.append(" from  \n");
		consulta.append("        colectivos_componentes comp \n");
		consulta.append(" where \n");
		consulta.append("        comp.coct_estatus = 20 and \n");
		consulta.append("        comp.coct_ta_componente <> 0 and \n");
		consulta.append("        comp.coct_capp_cd_componente = 'IVA'   \n");
		//consulta.append("        and comp.coct_capo_nu_poliza = 509040001 \n");
		consulta.append(" )  \n");
		qry = getSession().createSQLQuery(consulta.toString());
		qry.executeUpdate();
		consulta = new StringBuilder();
		consulta.append(" update colectivos_componentes comp  \n"); 
 		consulta.append(" set  \n");
		consulta.append("		comp.coct_fe_desde = sysdate, \n"); 
		consulta.append("		comp.coct_fe_hasta = add_months(sysdate,600), \n"); 
		consulta.append("		comp.coct_ta_componente=:tarifa \n");
		consulta.append("where  \n");
		consulta.append("		comp.coct_estatus = 20 and  \n");
		consulta.append("		comp.coct_ta_componente <> 0 and  \n");
		consulta.append("		comp.coct_capp_cd_componente = 'IVA'  \n");
		//consulta.append("		and comp.coct_capo_nu_poliza = 509040001  \n");

		qry = getSession().createSQLQuery(consulta.toString());
		qry.setBigDecimal("tarifa", tarifa);
		qry.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ColectivosComponentesDao#actualizaProductoPlan(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas)
	 */
	@Override
	public void actualizaProductoPlan(ColectivosCoberturas colectivosCoberturas) throws Excepciones {
		 StringBuffer actualiza = new StringBuffer();
			actualiza.append("UPDATE COLECTIVOS_COMPONENTES SET COCT_CAMPON2 = COCT_TA_COMPONENTE ");
			actualiza.append("WHERE COCT_CASU_CD_SUCURSAL =  ");
			actualiza.append(colectivosCoberturas.getId().getCocbCasuCdSucursal());
			actualiza.append(" AND COCT_CARP_CD_RAMO =  ");
			actualiza.append(colectivosCoberturas.getId().getCocbCarpCdRamo());
			actualiza.append(" AND COCT_CAPO_NU_POLIZA =  ");
			actualiza.append(colectivosCoberturas.getId().getCocbCapoNuPoliza());
			actualiza.append(" AND COCT_CAPP_CD_COMPONENTE in ('COM', 'CON')");
			
			 try {
				 Query queryAct = getSession().createSQLQuery(actualiza.toString());
							 
				 queryAct.executeUpdate();
				 
			 } catch (Exception e) {
				throw new Excepciones(e.getMessage());
			}
		
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ColectivosComponentesDao#actualizaCobertura(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas)
	 */
	@Override
	public void actualizaCobertura(ColectivosCoberturas colectivosCoberturas) throws Excepciones {
		StringBuffer actualiza = new StringBuffer();
		actualiza.append("UPDATE COLECTIVOS_COMPONENTES SET COCT_CAMPON2 = -1 ");
		actualiza.append("WHERE COCT_CASU_CD_SUCURSAL =  ");
		actualiza.append(colectivosCoberturas.getId().getCocbCasuCdSucursal());
		actualiza.append(" AND COCT_CARP_CD_RAMO =  ");
		actualiza.append(colectivosCoberturas.getId().getCocbCarpCdRamo());
		actualiza.append(" AND COCT_CAPO_NU_POLIZA =  ");
		actualiza.append(colectivosCoberturas.getId().getCocbCapoNuPoliza());
		actualiza.append(" AND COCT_CAPP_CD_COMPONENTE in ('COM', 'CON')");

		 try {
			 Query queryAct = getSession().createSQLQuery(actualiza.toString());
						 
			 queryAct.executeUpdate();
			 
		 } catch (Exception e) {
			throw new Excepciones(e.getMessage());
		}
		
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ColectivosComponentesDao#actualizaParametrizacionProductoPlan(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros)
	 */
	@Override
	public void actualizaParametrizacionProductoPlan(Parametros parametrosConfiguracion) throws Excepciones {
		StringBuffer actualiza = new StringBuffer();
		StringBuilder fpoliza = new StringBuilder();
		Query queryAct;
		
		//Actualiza Comisiones Tecnica (COM)
		actualiza.append("UPDATE COLECTIVOS_COMPONENTES SET COCT_TA_COMPONENTE =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor8());
		actualiza.append(" , COCT_CAMPON2 =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor8());
		actualiza.append(" WHERE COCT_CASU_CD_SUCURSAL =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor1());
		actualiza.append(" AND COCT_CARP_CD_RAMO =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor2());
		actualiza.append(" AND COCT_CAPO_NU_POLIZA   ");
		
		fpoliza = new StringBuilder(); 
		if(parametrosConfiguracion.getCopaNvalor3() == 0){
			fpoliza.append(" between ").append(parametrosConfiguracion.getCopaNvalor4()).append("0000 and ").append(parametrosConfiguracion.getCopaNvalor4()).append("9999 \n");			
		} else {
			fpoliza.append(" = ").append(parametrosConfiguracion.getCopaNvalor3()).append(" \n");
		}
		
		actualiza.append(fpoliza.toString());
		actualiza.append(" AND COCT_CAPP_CD_COMPONENTE = 'COM' ");
		actualiza.append(" AND COCT_CAPU_CD_PRODUCTO =  ");
		actualiza.append(parametrosConfiguracion.getCopaVvalor1());
		actualiza.append(" AND COCT_CAPB_CD_PLAN =  ");
		actualiza.append(parametrosConfiguracion.getCopaVvalor2());
		try {
			queryAct = getSession().createSQLQuery(actualiza.toString());
			queryAct.executeUpdate();			
		} catch (Exception e) {
			throw new Excepciones(e.getMessage());
		}
		
		
		//Actualiza Comisiones Contingente (CON)
		actualiza = new StringBuffer();
		actualiza.append("UPDATE COLECTIVOS_COMPONENTES SET COCT_TA_COMPONENTE =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor7());
		actualiza.append(" , COCT_CAMPON2 =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor7());
		actualiza.append(" WHERE COCT_CASU_CD_SUCURSAL =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor1());
		actualiza.append(" AND COCT_CARP_CD_RAMO =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor2());
		actualiza.append(" AND COCT_CAPO_NU_POLIZA   ");
		
		fpoliza = new StringBuilder(); 
		if(parametrosConfiguracion.getCopaNvalor3() == 0){
			fpoliza.append(" between ").append(parametrosConfiguracion.getCopaNvalor4()).append("0000 and ").append(parametrosConfiguracion.getCopaNvalor4()).append("9999 \n");			
		} else {
			fpoliza.append(" = ").append(parametrosConfiguracion.getCopaNvalor3()).append(" \n");
		}
		
		actualiza.append(fpoliza.toString());
		actualiza.append(" AND COCT_CAPP_CD_COMPONENTE = 'CON' ");
		actualiza.append(" AND COCT_CAPU_CD_PRODUCTO =  ");
		actualiza.append(parametrosConfiguracion.getCopaVvalor1());
		actualiza.append(" AND COCT_CAPB_CD_PLAN =  ");
		actualiza.append(parametrosConfiguracion.getCopaVvalor2());
		try {
			queryAct = getSession().createSQLQuery(actualiza.toString());
			queryAct.executeUpdate();			
		} catch (Exception e) {
			throw new Excepciones(e.getMessage());
		}
		
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ColectivosComponentesDao#actualizaParametrizacionCobertura(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros)
	 */
	@Override
	public void actualizaParametrizacionCobertura(Parametros parametrosConfiguracion) throws Excepciones {
		StringBuffer actualiza = new StringBuffer();
		actualiza.append("UPDATE COLECTIVOS_COMPONENTES SET  ");
		
		actualiza.append(" COCT_CAMPON2 = -1 ");
		
		actualiza.append(" WHERE COCT_CASU_CD_SUCURSAL =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor1());
		actualiza.append(" AND COCT_CARP_CD_RAMO =  ");
		actualiza.append(parametrosConfiguracion.getCopaNvalor2());
		actualiza.append(" AND COCT_CAPO_NU_POLIZA   ");
		StringBuilder fpoliza = new StringBuilder(); 
		if(parametrosConfiguracion.getCopaNvalor3() == 0){
			fpoliza.append(" between ").append(parametrosConfiguracion.getCopaNvalor4()).append("0000 and ").append(parametrosConfiguracion.getCopaNvalor4()).append("9999 \n");
				
		 } else {
			 fpoliza.append(" = ").append(parametrosConfiguracion.getCopaNvalor3()).append(" \n");
		 }
		actualiza.append(fpoliza.toString());
		actualiza.append(" AND COCT_CAPP_CD_COMPONENTE in ('COM', 'CON') ");
		actualiza.append(" AND COCT_CAPU_CD_PRODUCTO =  ");
		actualiza.append(parametrosConfiguracion.getCopaVvalor1());
		
		actualiza.append(" AND COCT_CAPB_CD_PLAN =  ");
		actualiza.append(parametrosConfiguracion.getCopaVvalor2());
		 try {
			 Query queryAct = getSession().createSQLQuery(actualiza.toString());
						 
			 queryAct.executeUpdate();
			 
		 } catch (Exception e) {
			throw new Excepciones(e.getMessage());
		}
		
	}
	
	
	
	
	
	
	
	
}
