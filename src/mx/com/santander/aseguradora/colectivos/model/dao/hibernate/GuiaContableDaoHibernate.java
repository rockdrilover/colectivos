package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.dao.GuiaContableDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class GuiaContableDaoHibernate extends HibernateDaoSupport implements GuiaContableDao{
	
	@Autowired
	public GuiaContableDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object> consultarProductos(int nDatosRamos) throws Exception {
		List<Object> arlResultado;
		StringBuffer sbQuery;
		
		try {
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT DISTINCT ap.alpr_cd_producto, ");
			sbQuery.append("ap.alpr_de_producto ");
			sbQuery.append("FROM Colectivos_Coberturas cc ");
			sbQuery.append("INNER JOIN alterna_productos ap ");
			sbQuery.append("on (ap.alpr_cd_ramo = cc.cocb_carp_cd_ramo ");
			sbQuery.append("and ap.alpr_cd_producto = cc.cocb_capu_cd_producto) ");
			sbQuery.append("WHERE cc.cocb_casu_cd_sucursal= 1 ");
			sbQuery.append("AND cc.cocb_carp_cd_ramo = ").append(nDatosRamos);
			sbQuery.append(" AND cc.cocb_estatus = 20 ");
			sbQuery.append("ORDER BY 1");
			System.out.println(sbQuery.toString());
//			arlResultado.add(consultar(sbQuery.toString()));
			Query qry = this.getSession().createSQLQuery(sbQuery.toString());
			arlResultado = qry.list();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.consultarRamos::" + e.getMessage());
		}
			
		return arlResultado;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> consultarPlanes(int nDatosProductos, int nDatosRamos) throws Exception {
		List<Object> arlResultado;
		StringBuffer sbQuery;
		
		try {
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("select distinct app.alpl_cd_plan, ");
			sbQuery.append("app.alpl_de_plan || '-' || app.alpl_dato2 ");
			sbQuery.append("FROM Colectivos_Coberturas cc ");
			sbQuery.append("INNER JOIN alterna_productos ap ");
			sbQuery.append("on (ap.alpr_cd_ramo = cc.cocb_carp_cd_ramo ");
			sbQuery.append("and ap.alpr_cd_producto = cc.cocb_capu_cd_producto) ");
			sbQuery.append("INNER JOIN alterna_planes app ");
			sbQuery.append("on(app.alpl_cd_ramo = cc.cocb_carp_cd_ramo ");
			sbQuery.append("and app.alpl_cd_producto = cc.cocb_capu_cd_producto ");
			sbQuery.append("and app.alpl_cd_plan = cc.cocb_capb_cd_plan) ");
			sbQuery.append("WHERE cc.cocb_casu_cd_sucursal= 1 ");
			sbQuery.append("AND cc.cocb_carp_cd_ramo  = ").append(nDatosRamos);
			sbQuery.append(" AND cc.cocb_capu_cd_producto = ").append(nDatosProductos);
			sbQuery.append(" AND cc.cocb_estatus = 20 ");
			System.out.println(sbQuery.toString());
//			arlResultado.add(consultar(sbQuery.toString()));
			Query qry = this.getSession().createSQLQuery(sbQuery.toString());
			arlResultado = qry.list();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.consultarRamos::" + e.getMessage());
		}
			
		return arlResultado;
	}
	
	
	public void borrarDatosTemporales(Short canal, Short ramo, Long poliza,
			Integer inIdVenta, String archivo, String origen)
			throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> List<T> estatusEmision(Short canal, Short ramo, Long poliza,
			Integer idVenta, String archivo) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> estatusCancelacion(Short canal, Short ramo, Long poliza,
			String archivo) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public List<Object> existeCargaConcilia(Short origen) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public Double getCumulo(Short inCanal, Short inRamo, Long inPoliza,
			Integer inIdVenta) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public void reporteErrores(String rutaTemporal, String rutaReporte,
			Map<String, Object> inParams) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public void estatusCancelacion(List<BeanClienteCertif> listaCertificados,
			List<BeanClienteCertif> listaCertificadosCancelacion)
			throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public Collection<? extends Object> consultarVentasConsumo(long tipoReg,
			int ramo, String strOrigen, String string) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public Producto consultar(String strQuery)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Object> consultarMapeo(String strQuery) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void actualizarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public void divisionAutoCredito() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void actualizarCargasManuales(String idCertificado, String canal,
			String ramo, String poliza, String sistemaOrigen) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void respaldarErrores(Short inCanal, Short inRamo, Long inPoliza,
			Integer inIdVenta, String nombreArchivo) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void actualizarCuentasLE(String strIdCertificado,
			String strFechaIngreso) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void eliminarDatosReprocesoLE(String tipoError) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void updatePampa() throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public void buscarCuentaPampa(String[] strCuenta) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void updatePampaCancelacion() throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public String getCodigoEstado(String strCP) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<Object> consultarRamos(int nDatosRamos) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<Object> consultaProductos(int nDatosRamos) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@SuppressWarnings("unchecked")
	public List<Object> obtenerCoberturas(int nDatosRamos, int nDatosProductos, int nDatosPlanes) throws Exception {
		List<Object> arlResultado;
		StringBuffer sbQuery;
		
		try {
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("select CC.COCB_CAPO_NU_POLIZA, ");
			sbQuery.append("cc.cocb_carp_cd_ramo RAMOPOLIZA, ");
			sbQuery.append("cc.cocb_carb_cd_ramo RAMOCONTABLERECTOR, ");
			sbQuery.append("CC.COCB_IN_SUMARIZACION RAMOCONTABLE, ");
			sbQuery.append("cc.cocb_cacb_cd_cobertura CDCOB, ");
			sbQuery.append("CO.CACB_DE_COBERTURA DESCOB, ");
			sbQuery.append("APP.ALPL_DATO3 CENTROCOSTOS, ");
			sbQuery.append("CC.COCB_TA_RIESGO TARIFA ");
			sbQuery.append("FROM Colectivos_Coberturas cc ");
			sbQuery.append("INNER JOIN alterna_productos ap ");
			sbQuery.append("on (ap.alpr_cd_ramo = cc.cocb_carp_cd_ramo ");
			sbQuery.append("and ap.alpr_cd_producto = cc.cocb_capu_cd_producto) ");
			sbQuery.append("INNER JOIN alterna_planes app ");
			sbQuery.append("on(app.alpl_cd_ramo = cc.cocb_carp_cd_ramo ");
			sbQuery.append("and app.alpl_cd_producto = cc.cocb_capu_cd_producto ");
			sbQuery.append("and app.alpl_cd_plan = cc.cocb_capb_cd_plan) ");
			sbQuery.append("inner join cart_coberturas co ");
			sbQuery.append("on (co.cacb_carb_cd_ramo = cc.cocb_carb_cd_ramo ");
			sbQuery.append("and co.cacb_cd_cobertura = cc.cocb_cacb_cd_cobertura) ");
			sbQuery.append("WHERE cc.cocb_casu_cd_sucursal= 1 ");
			sbQuery.append("AND cc.cocb_carp_cd_ramo  = ").append(nDatosRamos);
			sbQuery.append(" AND cc.cocb_capu_cd_producto = ").append(nDatosProductos);
			sbQuery.append(" AND cc.cocb_capb_cd_plan  = ").append(nDatosPlanes);
			sbQuery.append(" AND cc.cocb_estatus = 20 ");
			System.out.println(sbQuery.toString());
//			arlResultado.add(consultar(sbQuery.toString()));
			Query qry = this.getSession().createSQLQuery(sbQuery.toString());
			arlResultado = qry.list();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.consultarCoberturas::" + e.getMessage());
		}
			
		return arlResultado;
	}


	public List<Object> obtenerGuiasContables(String ramoPoliza,
			String ramoContable) throws Exception {
		List<Object> arlResultado;
		StringBuffer sbQuery;
		
		try {
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("select CO.COOP_CJTO_CD_OPERACION, ");
			sbQuery.append("CO.COOP_CAPP_CD_COMPONENTE, ");
			sbQuery.append("CO.COOP_CD_CUENTA, ");
			sbQuery.append("CO.COOP_IN_DEBHAB, ");
			sbQuery.append("CO.COOP_FE_INICIO, ");
			sbQuery.append("CO.COOP_FE_FIN ");
			sbQuery.append("FROM cont_operaciones_recibos co ");
//			sbQuery.append("WHERE CO.COOP_CARB_CD_RAMO = ").append(ramoContable);
			sbQuery.append("WHERE CO.COOP_CARB_CD_RAMO = 0");
			sbQuery.append(" AND CO.COOP_CARP_CD_RAMO  = ").append(ramoPoliza);
			System.out.println(sbQuery.toString());
//			arlResultado.add(consultar(sbQuery.toString()));
			Query qry = this.getSession().createSQLQuery(sbQuery.toString());
			arlResultado = qry.list();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: CargaDaoHibernate.obtenerGuiasContables::" + e.getMessage());
		}
			
		return arlResultado;

}
	}

