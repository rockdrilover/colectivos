/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.dao.ProductoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dflores
 * Modificacion : Sergio Plata
 */
@Repository
public class ProductoDaoHibernate extends HibernateDaoSupport implements
		ProductoDao {

	@Autowired
	public ProductoDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			T producto = (T) this.getHibernateTemplate().get(objeto, id);
			return producto;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			List<T> lista = this.getHibernateTemplate().loadAll(objeto);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	
	
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ProductoDao#productoPlanes(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> productoPlanes(String filtro)  throws Excepciones{
		StringBuffer sb = new StringBuffer(150);
		sb.append("FROM Producto p	\n");
		sb.append("JOIN FETCH p.planes pl	\n");
		sb.append("where       \n");
		sb.append(filtro);
		sb.append(" order by p.alprDeProducto ");
		Query query = this.getSession().createQuery(sb.toString());
		List<Producto> lista = (List<Producto>) query.list();

		return lista;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro)throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					
					StringBuffer sb = new StringBuffer(150);
					sb.append("from Producto P	\n");
					sb.append("where 1 = 1      \n");
					sb.append(filtro);
					sb.append("order by P.id.alprCdProducto");
									
					Query query = sesion.createQuery(sb.toString());
					
					List<T> lista = query.list();
					return lista;
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

		
	}

	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().merge(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			for(T objeto: lista){
				
				this.guardarObjeto(objeto);
			}
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Integer siguenteProducto(Short ramo) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			StringBuilder consulta = new StringBuilder(100);
			consulta.append("Select max(nvl(P.id.alprCdProducto,0)) + 1 \n");
			consulta.append("from Producto P							\n");
			consulta.append("where P.id.alprCdRamo = :ramo				\n");
			
			Query qry = this.getSession().createQuery(consulta.toString());
			
			qry.setParameter("ramo", ramo);
			
			Integer producto = Integer.parseInt(qry.list().get(0).toString());
			
	
			return producto;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}

	}




}
