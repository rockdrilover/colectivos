/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosBeneficiarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosBeneficiariosId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId;
import mx.com.santander.aseguradora.colectivos.model.database.EndosoDatosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Sergio Plata
 * @modified Ing. Issac Bautista
 */
@Repository
public class EndosoDatosDaoHibernate extends HibernateDaoSupport implements EndosoDatosDao {
	
	@Autowired
	public EndosoDatosDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().merge(objeto);
		} catch (Exception e) {
			throw new Excepciones("Error, actualizarObjeto.", e);
		}
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			throw new Excepciones("Error, borrarObjeto.", e);
		}
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		} catch (DataIntegrityViolationException e) {
			throw new ObjetoDuplicado("Error, guardarObjeto.", e);
		} catch (Exception e) {
			throw new Excepciones("Error, guardarObjeto.", e);
		}
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		try {
			return (T) this.getHibernateTemplate().get(objeto, id);
		} catch (Exception e) {
			throw new Excepciones("Error: EndosoDatosDao.obtenerObjeto.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		List<T> arlResutlados = null;
		
		try {
			arlResutlados = new ArrayList<>();
			arlResutlados.addAll(this.getHibernateTemplate().loadAll(objeto));
		} catch (Exception e) {
			throw new Excepciones("Error: EndosoDatosDao.obtenerObjetos. " + e.getMessage());
		}
		
		return arlResutlados;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		try {
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){
				public Object doInHibernate(Session sesion) throws HibernateException, SQLException {
					Query qry = sesion.createQuery(filtro);
					return qry.list();
				}
			});
		} catch (Exception e) {
			throw new Excepciones("Error, obtenerObjetos.", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.EndosoDatosDao#consultar(String)
	 */
	@Override	
	public List<Object> consultar(String strQuery) throws Excepciones {
		ArrayList<Object> arlResutlados;
		Query query;
		
		try {
			arlResutlados = new ArrayList<>();
			if(!("").equals(strQuery)){
				query = getSession().createSQLQuery(strQuery);
				arlResutlados = (ArrayList<Object>) query.list();
			}
		} catch (Exception e) {
			throw new Excepciones("Error: EndosoDatosDao.consultar:: " + e.getMessage());
		}
		
		return arlResutlados;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.database.EndosoDatosDao#actualizaBeneficiario(mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO)
	 */
	@Override
	public void actualizaBeneficiario(EndosoDatosDTO seleccionado) throws Excepciones {
		ColectivosBeneficiarios objBeneficiario;
		
		try {
			objBeneficiario = new ColectivosBeneficiarios();
			objBeneficiario.setId(new ColectivosBeneficiariosId());
			objBeneficiario.getId().setCobeCaceCasuCdSucursal(seleccionado.getId().getCedaCasuCdSucursal());
			objBeneficiario.getId().setCobeCaceCarpCdRamo(seleccionado.getId().getCedaCarpCdRamo().byteValue());
			objBeneficiario.getId().setCobeCaceCapoNuPoliza(seleccionado.getId().getCedaCapoNuPoliza());
			objBeneficiario.getId().setCobeCaceNuCertificado(seleccionado.getId().getCedaNuCertificado());
			objBeneficiario.getId().setCobeNuBeneficiario(seleccionado.getCedaCampon2().intValue());
			
			objBeneficiario.setCobeFeDesde(seleccionado.getCedaFechaAnt());
			objBeneficiario.setCobeNombre(seleccionado.getCedaNombreNvo());
			objBeneficiario.setCobeApellidoPat(seleccionado.getCedaApNvo());
			objBeneficiario.setCobeApellidoMat(seleccionado.getCedaAmNvo());
			objBeneficiario.setCobeRelacionBenef(seleccionado.getCedaParentescoNvo());
			objBeneficiario.setCobePoParticipacion(new BigDecimal(seleccionado.getCedaPjParticipaNvo()));
			
			if(seleccionado.getCedaCampov3().equals(Constantes.BENEFICIARIO_NUEVO)) {
				objBeneficiario.getId().setCobeNuBeneficiario(getMaxNumBeneficiario(seleccionado.getId()));
				objBeneficiario.setCobeFeDesde(new Date());
				guardarObjeto(objBeneficiario);
			} else if(seleccionado.getCedaCampov3().equals(Constantes.BENEFICIARIO_MODIFICA)) {
				actualizarObjeto(objBeneficiario);
			} else if(seleccionado.getCedaCampov3().equals(Constantes.BENEFICIARIO_ELIMINA)) {
				objBeneficiario.setCobeNombre(seleccionado.getCedaNombreAnt());
				objBeneficiario.setCobeApellidoPat(seleccionado.getCedaApAnt());
				objBeneficiario.setCobeApellidoMat(seleccionado.getCedaAmAnt());
				objBeneficiario.setCobeRelacionBenef(seleccionado.getCedaParentescoAnt());
				objBeneficiario.setCobePoParticipacion(new BigDecimal(seleccionado.getCedaPjParticipaAnt()));
				borrarObjeto(objBeneficiario);
			}
		} catch (Excepciones e) {
			throw new Excepciones("Error: Al actualizar Beneficiario. ", e);
		}
	}

	private int getMaxNumBeneficiario(EndososDatosId id) throws Excepciones {
		StringBuilder sbQuery;
		      
		try {
			sbQuery = new StringBuilder();
			
			sbQuery.append("SELECT NVL(MAX(COBE_NU_BENEFICIARIO), 0) + 1 ");			
				sbQuery.append(" FROM COLECTIVOS_BENEFICIARIOS ");				
			sbQuery.append("  WHERE COBE_CACE_CASU_CD_SUCURSAL = ").append(id.getCedaCasuCdSucursal());
			sbQuery.append(" AND COBE_CACE_CARP_CD_RAMO = ").append(id.getCedaCarpCdRamo());
			sbQuery.append(" AND COBE_CACE_CAPO_NU_POLIZA = ").append(id.getCedaCapoNuPoliza());
			sbQuery.append(" AND COBE_CACE_NU_CERTIFICADO = ").append(id.getCedaNuCertificado());
			
			return ((BigDecimal) consultar(sbQuery.toString()).get(0)).intValue();
		} catch (Excepciones e) {
			throw new Excepciones("Error: Al obtener numero beneficiario. ", e);
		}
		   
	}

}
