/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.PolizaDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Sergio Plata
 *
 */
@Repository
public class PolizaDaoHibernate extends HibernateDaoSupport implements
		PolizaDao {

	@Autowired
	public PolizaDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	public <T> void actualizarObjeto(T objeto) {
		// TODO Auto-generated method stub
		
	}

	public <T> void borrarObjeto(T objeto) {
		// TODO Auto-generated method stub
		
	}

	public <T> T guardarObjeto(T objeto) {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) {
		// TODO Auto-generated method stub
		
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) {
		// TODO Auto-generated method stub
		return (T) this.getHibernateTemplate().get(objeto, id);
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) {
		// TODO Auto-generated method stub
		
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuilder consulta = new StringBuilder();
				consulta.append("from Poliza P\n");
				consulta.append("where 1 = 1\n");
				consulta.append(filtro);
				
				Query qry = sesion.createQuery(consulta.toString());
				
				List<T> lista = qry.list();
				
				return lista;
			}
			
		});
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaPolizasRenovacion() throws Excepciones {
		StringBuilder selectprecarga;
		Query query;
		
		try {
			selectprecarga = new StringBuilder() ;
	
			selectprecarga.append("select distinct cer.coce_carp_cd_ramo, cer.coce_capo_nu_poliza, cer.coce_fe_desde, cer.coce_fe_hasta, nvl(fac.cofa_cuota_recibo,12) \n");  
			selectprecarga.append("		from colectivos_certificados cer \n");
			selectprecarga.append("		inner join colectivos_parametros par  ON(par.copa_des_parametro    = 'POLIZA') \n");
			selectprecarga.append("		inner join colectivos_facturacion fac ON(fac.cofa_casu_cd_sucursal = cer.coce_casu_cd_sucursal and fac.cofa_carp_cd_ramo = cer.coce_carp_cd_ramo and fac.cofa_capo_nu_poliza   = cer.coce_capo_nu_poliza and fac.cofa_nu_certificado   = cer.coce_nu_certificado) \n"); 
			selectprecarga.append("where cer.coce_casu_cd_sucursal	= par.copa_nvalor1  \n");
			selectprecarga.append("	and cer.coce_carp_cd_ramo       = par.copa_nvalor2  \n"); 
			selectprecarga.append("	and cer.coce_capo_nu_poliza     = par.copa_nvalor3  \n");
			selectprecarga.append("	and cer.coce_nu_certificado     = 0 \n");
			selectprecarga.append("	and cer.coce_st_certificado     = 1 \n");          
			selectprecarga.append("	and par.copa_id_parametro       = 'RENOVACION' \n"); 
			selectprecarga.append("	and par.copa_nvalor4            = 1 \n");                                
			selectprecarga.append("	and fac.cofa_cuota_recibo       = 12 \n"); 		                 
			selectprecarga.append("	and cer.coce_fe_hasta           between  trunc(sysdate) - par.copa_nvalor5 and trunc(sysdate) \n");
			selectprecarga.append("	and fac.cofa_fe_facturacion 	>  cer.coce_fe_hasta \n");
			
			query = this.getSession().createSQLQuery(selectprecarga.toString());
			List<T> lista = query.list();
			
			return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}					
	}

}
