/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mx.com.santander.aseguradora.colectivos.model.dao.AltaDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import oracle.jdbc.OracleTypes;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author dflores
 *
 */
@Repository
public class AltaDaoHibernate extends HibernateDaoSupport implements AltaDao {
	
	
	@Autowired
	public AltaDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class )
	public Long iniciaAlta( int canal, Short ramo,String analista,int sucursal,String domicilio,String cedula_rif,Date fecha_ini) {
		Long poliza = 0L;
		try {
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			IniciaAlta alta = new IniciaAlta(ds);
			
			poliza = Long.parseLong(((Map)alta.execute(canal, ramo, analista, sucursal, domicilio, cedula_rif, fecha_ini)).get("V_POLIZA").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return poliza;
		
	}
	
	private class IniciaAlta extends StoredProcedure{

		public IniciaAlta(DataSource ds) {
			// TODO Auto-generated constructor stub
			super.setDataSource(ds);
			super.setSql(Colectivos.PROCESO_ALTA);
			super.declareParameter(new SqlOutParameter("V_POLIZA", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_CANAL", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_RAMO", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_ANALISTA", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_SUCURSAL", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_NACIONAL", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_CEDULA_RIF", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_FECHA_DESDE", OracleTypes.DATE));
			
			super.compile();
			
		}
		
		public Map execute(int canal, Short ramo,String analista,int sucursal,String domicilio,String cedula_rif,Date fecha_ini) throws DataAccessException {
				Map<String, Object> inParams = new HashMap<String, Object>();
				
				inParams.put("P_CANAL", canal);
				inParams.put("P_RAMO",  ramo);
				inParams.put("P_ANALISTA", analista);
				inParams.put("P_SUCURSAL", sucursal);
				inParams.put("P_NACIONAL", domicilio);
				inParams.put("P_CEDULA_RIF", cedula_rif);
				inParams.put("P_FECHA_DESDE", fecha_ini);
				return super.execute(inParams);
				
			}
		
	}

}
