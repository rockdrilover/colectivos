package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import mx.com.santander.aseguradora.colectivos.model.dao.ValidaEmisionDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import org.springframework.stereotype.Repository;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * 
 * @author Santander Tecnologia JEH
 *
 */
@Repository
public class ValidaEmisionDAOHibernate extends HibernateDaoSupport implements ValidaEmisionDao {
	
	/**
	 * Creacion de instancia de log
	 */
	private static final Log LOG = LogFactory.getLog(ValidaEmisionDao.class);
	/**
	 * Metodo constructor de DAO de validacion de emision		
	 * @param sessionFactory  parametro que recibe el constructor para continuar la sesion de la aplicacion
	 */
	@Autowired
	public void ValidaEmisionDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ValidaEmisionDao#validaemiMasiva(java.lang.Short, java.lang.Short, java.lang.Long, java.lang.Integer)
	 */
	@Override
	public Map<String, Object> validaemiMasiva(Short canal, Short ramo, Long poliza, Integer idVenta) throws Excepciones {
		/* Instancia de cadena de respuesta */
		
		try {
				/* Instancia de Data Source */
				DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
				/* Llamado ala clase de validacion */							
				ValidaEmisionMasiva valida = new ValidaEmisionMasiva(ds);
				/* Ejecucion del metodo de validacion */
				Map mapaRespuesta = valida.execute(canal, ramo, poliza, idVenta);
				
				return  mapaRespuesta;
				/* Bloque de excepciones */
			}catch (DataIntegrityViolationException e) {
				/* Bateo de excepcion de integridad de datos */
				LOG.error("Error de integridad de Datos. ", e);
				throw new Excepciones("Error.", e);
			}catch (Exception e) {
				/* Bateo de bloque de excepciones especificas */
				LOG.error("Excepcion "+e.getClass());
				throw new Excepciones("Error.", e);
			}
	
	}
}
/**
 * 
 * @author Santander Tecnologia JEH
 *
 */
final class ValidaEmisionMasiva extends StoredProcedure{

	/**
	 * Constructor de la clase
	 * @param ds DataSource de la clase que la llama
	 */
	public ValidaEmisionMasiva(DataSource ds) {
		/* Seteo de data source */
		super.setDataSource(ds);
		/* Seteo para identificacion de funcion */
		super.setFunction(true);
		/* Nombre de la funcion */
		super.setSql(Colectivos.VALIDA_POLIZA_EMISION);
		/* Parametro de retorno numerico */
		super.declareParameter(new SqlOutParameter("returnname", Types.INTEGER));
		/* Parametros de entrada */
		super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
		super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
		super.declareParameter(new SqlInOutParameter("paramPoliza", Types.INTEGER));
		super.declareParameter(new SqlParameter("paramIdVenta", Types.INTEGER));
		/* Parametros de salida */
		super.declareParameter(new SqlOutParameter("pValor", Types.INTEGER));
		super.declareParameter(new SqlOutParameter("pMensaje", Types.VARCHAR));
		/* Compilacion de las declaraciones */
		super.compile();
	}
	/**
	 * Metodo de ejecucion de la validacion
	 * @param canal al que esta asociado la poliza
	 * @param ramo de la poliza
	 * @param poliza numero de poliza
	 * @param idVenta de la emision que se realiza
	 * @return retorno de mapa de respuesta de la funcion
	 * @throws DataAccessException excepcion especifica de acceso a datos de BD
	 */
	public Map execute(Short canal, Short ramo, Long poliza, Integer idVenta) throws DataAccessException{
		/* Instancia de mapa de entrada ala funcion */
		Map<String, Object> inParams = new HashMap<String, Object>();
		/* Seteo de valores de entrada ala funcion */
		inParams.put("paramCanal", canal);
		inParams.put("paramRamo", ramo);
		inParams.put("paramPoliza", poliza);
		inParams.put("paramIdVenta", idVenta);
		/* Instancia de ejecuciond e la funcion */
		Map<String, Object> ejecucion;
		ejecucion = super.execute(inParams);
		/* Retorno de la ejecucion */
		return ejecucion;
	}
}