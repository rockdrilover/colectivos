/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClientePK;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dfg
 *
 */

public interface ContratanteDao extends CatalogoDao,Serializable{

	public <T> void actualizarObjeto(T objeto) throws Excepciones;
	public <T> void borrarObjeto(T objeto) throws Excepciones;
	public <T> T guardarObjeto(T objeto) throws Excepciones;
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones;
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones;
	public <T> void guardarObjetos(List<T> lista) throws Excepciones;
	public <T> List<T> consulta2 (String strFiltro ) throws Excepciones;
	public <T> List<T> consulta(String nacionalidad, String cedula, String razonSocial,String strFiltro) throws Excepciones;
	public String guarda(Long next,String idParam, String desParam, String nacionalidad,
			String cedula, short canal, short ramo, Integer idVenta,
			Integer idProducto) throws Excepciones;
	
	/**
     * Metodo que realiza consulta mediante objetos con lista de parametros
     * @param <T> objeto a regresar en la lista
     * @param nTipoConsulta tipo de consulta a ejecutar
	 * @param cartClientePK datos del cliente
     * @return resultados 
     * @throws Excepciones con error en general
     */
	<T> List<T> obtenerObjetos(Integer nTipoConsulta, CartClientePK cartClientePK) throws Excepciones;
	
}
