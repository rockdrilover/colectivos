package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.dao.ParamTimbradoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase de tipo DAO que tiene acceso a base de datos para 
 * 				el proceso de Parametrizacion Timbrado
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Repository
public class ParamTimbradoDaoHibernate extends HibernateDaoSupport implements ParamTimbradoDao {
	//Implementacion de serializable
	private static final long serialVersionUID = 298075158826438461L;
	
	/**
	 * Constructor de clase
	 * @param sessionFactory session factory para conexion a DB
	 */
	@Autowired
	public ParamTimbradoDaoHibernate(SessionFactory sessionFactory) {
		//Se inicializa session factory
		super.setSessionFactory(sessionFactory);
	}

	/**
	 * Metodo que consulta los datos para PR
	 * @param canal canal para hacer filtro
	 * @param ramo ramo para hacer filtro
	 * @param poliza poliza para hacer filtro
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getDatosPR(Integer canal, Short ramo, Integer poliza) throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT CAPO_CASU_CD_SUCURSAL, CAPO_CARP_CD_RAMO, CAPO_NU_POLIZA, CACN_RFC, CAPO_CACN_CD_NACIONALIDAD, CAPO_CACN_NU_CEDULA_RIF, REPLACE(CACN_NM_APELLIDO_RAZON, '/', ''), TI.*, \n");
			consulta.append("CO.COPA_CD_PARAMETRO AS CD_PARAMETRO, CO.COPA_DES_PARAMETRO AS DES_PARAMETRO, CO.COPA_ID_PARAMETRO AS ID_PARAMETRO, CO.COPA_VVALOR1 AS VVALOR1, CO.COPA_VVALOR2 AS VVALOR2, CO.COPA_VVALOR5 AS VVALOR5, CO.COPA_NVALOR5 AS NVALOR5, \n");	
			consulta.append("CO.COPA_VVALOR3 AS VVALOR3, CO.COPA_VVALOR4 AS VVALOR4, CO.COPA_VVALOR6 AS VVALOR6, CO.COPA_NVALOR4 AS NVALOR4, CO.COPA_VVALOR7 AS VVALOR7 \n");
			consulta.append("FROM COLECTIVOS_PARAMETROS CP \n");
				consulta.append("INNER JOIN CART_POLIZAS ON(CAPO_CASU_CD_SUCURSAL = CP.COPA_NVALOR1 AND CAPO_CARP_CD_RAMO = CP.COPA_NVALOR2 AND CAPO_NU_POLIZA = CP.COPA_NVALOR3) \n");
				consulta.append("INNER JOIN CART_CLIENTES CACL ON(CACN_CD_NACIONALIDAD = CAPO_CACN_CD_NACIONALIDAD  AND CACN_NU_CEDULA_RIF = CAPO_CACN_NU_CEDULA_RIF) \n");
				consulta.append("LEFT JOIN COLECTIVOS_PARAMETROS TI ON(TI.COPA_DES_PARAMETRO = CP.COPA_DES_PARAMETRO \n");
					consulta.append("AND TI.COPA_ID_PARAMETRO = 'TIMBRADO' \n");
					consulta.append("AND TI.COPA_NVALOR1 = CAPO_CASU_CD_SUCURSAL \n");
					consulta.append("AND TI.COPA_NVALOR2 = CAPO_CARP_CD_RAMO \n");
					consulta.append("AND TI.COPA_NVALOR3 = CAPO_NU_POLIZA \n");
					consulta.append("AND TI.COPA_NVALOR4 = 0) \n");
				consulta.append("LEFT JOIN COLECTIVOS_PARAMETROS CO ON(CO.COPA_DES_PARAMETRO = CP.COPA_DES_PARAMETRO \n");
					consulta.append("AND CO.COPA_ID_PARAMETRO = 'COBRANZA' \n");
					consulta.append("AND CO.COPA_NVALOR1 = CAPO_CASU_CD_SUCURSAL \n");
					consulta.append("AND CO.COPA_NVALOR2 = CAPO_CARP_CD_RAMO \n");
					consulta.append("AND CO.COPA_NVALOR3 = CAPO_NU_POLIZA) \n");
				consulta.append("WHERE CP.COPA_DES_PARAMETRO = 'POLIZA' \n");
				consulta.append("AND CP.COPA_ID_PARAMETRO = 'GEP' \n");
				consulta.append("AND CP.COPA_NVALOR1 = :canal \n");
				consulta.append("AND CP.COPA_NVALOR2 = :ramo \n");
			if(poliza > 0) {
				consulta.append("AND CP.COPA_NVALOR3 = :poliza \n");
			}		
				consulta.append("AND CP.COPA_NVALOR7 = 0 \n");
			consulta.append("ORDER BY CP.COPA_NVALOR3 \n");
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se agregan parametros necesarios para realizar consulta
			query.setParameter("canal", canal);
			query.setParameter("ramo", ramo);
			if(poliza > 0) {
				query.setParameter("poliza", poliza);
			}
			
			//se ejecuta la consulta
			return query.list();
		} catch (HibernateException e) {
			throw new Excepciones("Error en ParamTimbradoDao.getDatosPR(): ", e); 
		}
	}
	
	/**
	 * Metodo que consulta los datos para PU
	 * @param canal canal para hacer filtro
	 * @param ramo ramo para hacer filtro
	 * @param idVenta id venta para hacer filtro
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getDatosPU(Integer canal, Short ramo, Integer idVenta) throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT CG.COPA_NVALOR1 CANAL, CG.COPA_NVALOR2 RAMO, CG.COPA_NVALOR3 IDVENTA, CACN_RFC, CG.COPA_VVALOR1 CDNA, CG.COPA_VVALOR2 RIF, REPLACE(CACN_NM_APELLIDO_RAZON, '/', ' '), TI.* \n");
				consulta.append("FROM COLECTIVOS_PARAMETROS CG \n");
				consulta.append("INNER JOIN CART_CLIENTES ON(CACN_CD_NACIONALIDAD = CG.COPA_VVALOR1  AND CACN_NU_CEDULA_RIF = CG.COPA_VVALOR2) \n");
				consulta.append("LEFT JOIN COLECTIVOS_PARAMETROS TI ON(TI.COPA_DES_PARAMETRO = 'POLIZA' \n");
					consulta.append("AND TI.COPA_ID_PARAMETRO = 'TIMBRADO' \n");
					consulta.append("AND TI.COPA_NVALOR1 = CG.COPA_NVALOR1 \n");
					consulta.append("AND TI.COPA_NVALOR2 = CG.COPA_NVALOR2 \n");
					consulta.append("AND TI.COPA_NVALOR4 = CG.COPA_NVALOR3) \n");
			consulta.append("WHERE CG.COPA_ID_PARAMETRO = 'CLIENTE' \n");
				consulta.append("AND CG.COPA_DES_PARAMETRO = 'CLIENTE GENERICO' \n");
				consulta.append("AND CG.COPA_NVALOR1 = :canal \n");
				consulta.append("AND CG.COPA_NVALOR2 = :ramo \n");
				consulta.append("AND CG.COPA_NVALOR3 = :idventa \n");
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se agregan parametros necesarios para realizar consulta
			query.setParameter("canal", canal);
			query.setParameter("ramo", ramo);
			query.setParameter("idventa", idVenta);
			
			//se ejecuta la consulta
			return query.list();
		} catch (HibernateException e) {
			throw new Excepciones("Error en ParamTimbradoDao.getDatosPU(): ", e); 
		}
	}
	
	/**
	 * Metodo que consulta objetos de base de datos 
	 * @param <T> tipo de objeto a conultar
	 * @param filtro para aplicar en la consulta
	 * @param nTabla para saber de que tabla consultar
	 * @return lista de resultados
	 * @throws Excepciones con error generar
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)	
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		try {
			//Se hace uso de hibernate template
			return this.getHibernateTemplate().executeFind(new HibernateCallback<Object>(){
				public Object doInHibernate(Session sesion) throws HibernateException, SQLException {
					
					StringBuilder consulta = new StringBuilder();
					
					//Se consulta tabla de carga
					consulta.append("from CartCatalogosCfdi  CCC    \n");
					
					consulta.append("where 1=1  \n");
					consulta.append(filtro);
					
					//Se agrega query a la session
					Query qry = sesion.createQuery(consulta.toString());
					
					//se ejecuta query
					return  qry.list();
				}
			});
		} catch (HibernateException e) {
			throw new Excepciones("Error en ParamTimbradoDao.obtenerObjetos(): ", e);
		}
	}

	/**
	 * Metodo que consulta objetos de nombre parametros
	 */
	@Override
	public List<Object> obtenerNombreProductos() throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			consulta = new StringBuilder();
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT cp.COPA_CD_PARAMETRO  ,cp.COPA_VVALOR1 \n");
			consulta.append("	FROM COLECTIVOS_PARAMETROS cp \n");
			consulta.append("	WHERE cp.COPA_ID_PARAMETRO ='NOMBRE_PRODUCTO' \n");
			consulta.append("	AND cp.COPA_DES_PARAMETRO = 'CATALOGO' \n");
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			//se ejecuta la consulta
			return query.list();
		} catch (HibernateException e) {
			throw new Excepciones("Error en ParamTimbradoDao.getDatosPU(): ", e); 
		}
	}
	
	
}
