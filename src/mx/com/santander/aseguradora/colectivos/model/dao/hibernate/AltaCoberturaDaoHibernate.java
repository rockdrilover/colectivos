/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;



import mx.com.santander.aseguradora.colectivos.model.dao.AltaCoberturaDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


/**
 * @author dflores
 *
 */
@SuppressWarnings("unchecked")

public class AltaCoberturaDaoHibernate extends HibernateDaoSupport implements AltaCoberturaDao {

	
	/**
	 * 
	 * @author dflores
	 *
	 */
	private class IniciaAlta extends StoredProcedure{

		public IniciaAlta(DataSource ds) {
			// TODO Auto-generated constructor stub
			super.setDataSource(ds);
			super.setSql(Colectivos.PROCESO_ALTACOBERTURA);
			super.declareParameter(new SqlOutParameter("V_COB", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramDescCobertura", OracleTypes.VARCHAR));
			super.declareParameter(new SqlParameter("paramRamoContable", OracleTypes.INTEGER));
			super.compile();
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.StoredProcedure#execute(java.util.Map)
		 */
		

		public Map execute( Short ramo, String descCober,Integer ramoCont) 
			// TODO Auto-generated method stub
			
			throws DataAccessException {
				// TODO Auto-generated method stub
				Map<String, Object> inParams = new HashMap<String, Object>();
				inParams.put("paramRamo", ramo);
				inParams.put("paramDescCobertura", descCober);
				inParams.put("paramRamoContable", ramoCont);			
				return super.execute(inParams);
				
			}

			
		
	}

	public Integer iniciaAlta( Short ramo, String descCober,Integer ramoCont) {
		// TODO Auto-generated method stub
		Integer poliza = 0;
		
		DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
		IniciaAlta alta = new IniciaAlta(ds);
		
		poliza = (Integer) ((Map)alta.execute(ramo,descCober,ramoCont)).get("V_COB");
		
		return poliza;
		
		
	}


	public List getListaAltaCober() {
		// TODO Auto-generated method stub
		return null;
	}

	public List getListaAltaCober(Short ramo, String descCober,Integer ramoCont) {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer iniciaAltaCober(Short ramo, String descCober,Integer ramoCont) {
		// TODO Auto-generated method stub
        Integer poliza = 0;
		
		DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
		IniciaAlta alta = new IniciaAlta(ds);
		
		poliza = (Integer) ((Map)alta.execute(ramo,descCober,ramoCont)).get("V_COB");
		
		return poliza;
		
		
	}
	

	public void saveListaAltaCober(List lista) {
		// TODO Auto-generated method stub
		
	}


	public Object saveAltaCober(Object altaCober) {
		// TODO Auto-generated method stub
		return null;
	}









}
