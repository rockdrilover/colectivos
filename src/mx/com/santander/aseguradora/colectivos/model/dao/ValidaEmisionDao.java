package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
/**
 * 
 * @author Santander Tecnologia JEH
 *
 */
public interface ValidaEmisionDao {
	
	/**
	 * Metodo de DAO para validacion de emision
	 * @param canal
	 * @param ramo
	 * @param poliza
	 * @param idVenta
	 * @return retorno de cadena con mensaje de validacion
	 * @throws Excepciones
	 */
	Map<String, Object> validaemiMasiva(Short canal, Short ramo, Long poliza, Integer idVenta) throws Excepciones;

}
