package mx.com.santander.aseguradora.colectivos.model.dao;


import java.util.List;


public interface GuiaContableDao{

	public List<Object> consultarProductos(int nDatosRamos) throws Exception;

	public List<Object> consultarPlanes(int nDatosProductos, int nDatosRamos) throws Exception;

	public List<Object> obtenerCoberturas(int nDatosRamos, int nDatosProductos,	int nDatosPlanes) throws Exception;

	public List<Object> obtenerGuiasContables(String ramoPoliza, String ramoContable) throws Exception;
}
