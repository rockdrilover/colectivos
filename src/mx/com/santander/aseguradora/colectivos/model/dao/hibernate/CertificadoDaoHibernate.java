/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.sql.DataSource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCfdiErrorW;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.GeneraMovimiento;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.NoMovimiento;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ReportesJasper;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleReporteDxP;
import mx.com.santander.aseguradora.colectivos.view.dto.ReporteDxP;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import oracle.sql.BLOB;

import org.apache.commons.httpclient.util.DateParseException;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.lob.SerializableBlob;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.opencsv.CSVWriter;

import WSTimbrado.EnviaTimbrado;

/**
 * @author Z014058
 *
 */
@Repository
public class CertificadoDaoHibernate extends HibernateDaoSupport implements CertificadoDao {

	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ProcesosDao procesosDao;
	
	private Integer cuota;
	
	
	public ProcesosDao getProcesosDao() {
		return procesosDao;
	}

	public void setProcesosDao(ProcesosDao procesosDao) {
		this.procesosDao = procesosDao;
	}
	
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	
	@Autowired
	public CertificadoDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public <T> List<T> getCertificados(String filtro) {
		return null;
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		throw new UnsupportedOperationException();
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		throw new UnsupportedOperationException();
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		throw new UnsupportedOperationException();
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public List<Object> getCertificado(short canal, byte ramo) {
		return null;
	}

	public Certificado getCertificado(CertificadoId id) {
		return null;
	}

	public List<ClienteCertif> getIdCertificado(String id, String nombre,
			String ap, String am) {
		return null;
	}

	public List<ClienteCertif> getIdCertificadoSt(String id, String nombre,
			String ap, String am) {
		return null;
	}

	public List<Object> getPolizasRenovacion() {
		return null;
	}

	public void updateCertificado(Certificado certificado) {
		throw new UnsupportedOperationException();
	}
	
	public void actualizaPreFacPoliza(CertificadoId id) {
		StringBuilder actPreFac = new StringBuilder(300);
		actPreFac.append("update Certificado certif \n" +
				"set certif.coceStFacturacion = 1 \n" +
				"where certif.id.coceCasuCdSucursal =:canal \n" +
				"and certif.id.coceCarpCdRamo       =:ramo\n" +
				"and certif.id.coceCapoNuPoliza     =:poliza\n" +
				"and certif.id.coceNuCertificado    =0 \n" );
		Query queryAct = getSession().createQuery(actPreFac.toString()); 
		queryAct.setParameter("canal", id.getCoceCasuCdSucursal());
		queryAct.setParameter("ramo", id.getCoceCarpCdRamo());
		queryAct.setParameter("poliza", id.getCoceCapoNuPoliza());
		queryAct.executeUpdate();		
	}

	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> getPreFacCertificado (Short canal, Short ramo, Long poliza, Integer inIdVenta) throws Excepciones {
		Long lPoliza1 = null, lPoliza2 = null;
		
		try {
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select cc.coce_Casu_Cd_Sucursal 				\n");
			consulta.append("      ,cc.coce_Carp_Cd_Ramo 					\n");
			consulta.append("      ,cc.coce_Capo_Nu_Poliza 					\n");
			
			consulta.append("      ,decode(p.COPA_NVALOR5, 0, \n");
			consulta.append("	   		sum(nvl(to_number(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'O',1,1) + 1,instr(cc.coce_di_cobro1, '|',1,1)-instr(cc.coce_di_cobro1, 'O',1,1) -1)),0)) \n");
			consulta.append("	        	+ sum(nvl(to_number(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'I',1,1) + 1,instr(cc.coce_di_cobro1, '|',1,2)-instr(cc.coce_di_cobro1, 'I',1,1) -1)),0)) \n");
			consulta.append("	            + sum(nvl(to_number(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'A',1,1) + 1,instr(cc.coce_di_cobro1, '|',1,3)-instr(cc.coce_di_cobro1, 'A',1,1) -1)),0)) \n");
			consulta.append("	            + sum(nvl(to_number(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'P',1,3) + 1,instr(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)), \n"); 
			consulta.append("	        sum(nvl(to_number(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'O',1,1) + 1,instr(cc.coce_di_cobro1, '|',1,1)-instr(cc.coce_di_cobro1, 'O',1,1) -1)),0)) \n");
			consulta.append("	        	+ sum(nvl(to_number(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'I',1,1) + 1,instr(cc.coce_di_cobro1, '|',1,2)-instr(cc.coce_di_cobro1, 'I',1,1) -1)),0)) \n");
			consulta.append("	            + sum(nvl(to_number(substr(cc.coce_di_cobro1,instr(cc.coce_di_cobro1, 'A',1,1) + 1,instr(cc.coce_di_cobro1, '|',1,3)-instr(cc.coce_di_cobro1, 'A',1,1) -1)),0)) \n");
			consulta.append("	            + sum(nvl(cc.coce_mt_prima_pura,0)) ) \n");
						
			consulta.append("      ,count(cc.coce_Nu_Certificado)			\n");
			consulta.append("  from colectivos_Certificados cc 				\n");
			consulta.append("      ,colectivos_Parametros   p 				\n");		
			consulta.append(" where cc.coce_Casu_Cd_Sucursal      = :canal 	\n");
			consulta.append("   and cc.coce_Carp_Cd_Ramo          = :ramo 	\n");
			if(poliza > Constantes.DEFAULT_LONG) {
				consulta.append("   and cc.coce_Capo_Nu_Poliza    = :poliza \n");
			} else {
				consulta.append("   and cc.coce_Capo_Nu_Poliza    between :poliza1 and :poliza2 \n");
				lPoliza1 = Long.valueOf(inIdVenta.toString()) * 100000000;
				lPoliza2 = lPoliza1 + 99999999; 
			}
			consulta.append("   and cc.coce_Nu_Certificado        > 0 				\n");
			consulta.append("   and (cc.coce_st_certificado       = p.copa_Vvalor5 	\n");
			consulta.append("        or cc.coce_st_certificado   = p.copa_Vvalor6) 	\n");
			consulta.append("   and p.copa_Des_Parametro          ='POLIZA' 		\n");
			consulta.append("   and p.copa_Id_Parametro           = 'GEPREFAC'  	\n");
			consulta.append("   and p.copa_Nvalor1                = cc.coce_Casu_Cd_Sucursal 		\n");
			consulta.append("   and p.copa_Nvalor2                = cc.coce_Carp_Cd_Ramo 			\n");
			consulta.append("   and p.copa_Nvalor3                = decode(p.copa_nvalor4, 0, cc.coce_capo_nu_poliza, 1, substr(cc.coce_capo_nu_poliza,0,3), substr(cc.coce_capo_nu_poliza,0,2)) \n");

			consulta.append("group by cc.coce_Casu_Cd_Sucursal 	\n");
			consulta.append("        ,cc.coce_Carp_Cd_Ramo  	\n");
			consulta.append("        ,cc.coce_Capo_Nu_Poliza 	\n");
			consulta.append("        ,p.COPA_NVALOR5     		\n");
				
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			if(poliza > Constantes.DEFAULT_LONG) {
				qry.setParameter("poliza", poliza);
			} else {
				qry.setParameter("poliza1", lPoliza1);
				qry.setParameter("poliza2", lPoliza2);
			}
			
			List<T> lista = qry.list();
			
			return lista;
		}catch (Exception e){
			throw new Excepciones ("Error .",e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					
					StringBuilder consulta = new StringBuilder();
					consulta.append("from Certificado C    \n");
					consulta.append("where 1 = 1           \n");
					consulta.append(filtro);
					consulta.append("order by C.id.coceCasuCdSucursal, C.id.coceCapoNuPoliza, C.id.coceNuCertificado");
					
					Query qry = sesion.createQuery(consulta.toString());
					List<T> lista = qry.list();
					
					return lista;
				}
				
				
			});
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> totalCertificados(Short canal, Short ramo, Long poliza)
			throws Excepciones {
		try {
			StringBuffer consulta = new StringBuffer(250);
			
			consulta.append("Select count(C.id.coceNuCertificado) , C.estatus.alesDescripcion, C.estatus.alesCampo1  \n");
			consulta.append("from Certificado C						 		   \n");
			consulta.append("where C.id.coceCasuCdSucursal = :canal			   \n");
			consulta.append("  and C.id.coceCarpCdRamo     = :ramo 			   \n");
			consulta.append("  and C.id.coceCapoNuPoliza   = :poliza		   \n");
			consulta.append("  and C.id.coceNuCertificado  > 0				   \n");
			consulta.append("group by C.estatus.alesCampo1, C.estatus.alesDescripcion \n");
						
			Query query = this.getSession().createQuery(consulta.toString());
			
			query.setParameter("canal",canal);
			query.setParameter("ramo",ramo);
			query.setParameter("poliza",poliza);			

			List<Object> lista = query.list();
			
			return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
		
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public void cancelaCertificado(Certificado certificado) throws Excepciones {
		Long noMovimiento = null;
		DataSource ds = null;
		
		try {
			
			ds = SessionFactoryUtils.getDataSource(this.getSession().getSessionFactory());
			NoMovimiento movimiento = new NoMovimiento(ds);
			noMovimiento = Long.parseLong(((Map)movimiento.execute(certificado.getId().getCoceCasuCdSucursal(), certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza(), certificado.getId().getCoceNuCertificado())).get("returnname").toString());
			
			GeneraMovimiento generaMovimiento = new GeneraMovimiento(ds);
			generaMovimiento.execute(certificado.getId().getCoceCasuCdSucursal(), certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza(), certificado.getId().getCoceNuCertificado());
			
			certificado.setCoceNuMovimiento(noMovimiento);
			
			this.getHibernateTemplate().update(certificado);
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaCertificados(Short canal, Short ramo, long Pol,
			String tiprep, String tippma, String sts, String fechaini,
			String fechafin) throws Exception {
		try {
			//System.out.println("----- " + canal + "----- " + ramo + "----- " + Pol + "----- " +tiprep + "----- " +tippma + "----- " +sts + "----- " +fechaini + "----- " +fechafin);
			StringBuilder consulta = new StringBuilder() ;
			if (tiprep.equals("1")){
				consulta.append("select a.coce_carp_cd_ramo" +
						        ", a.coce_capo_nu_poliza" +
						        ", ae.ales_descripcion" +
						        ", ae.ales_campo1" +
						        ", a.coce_nu_certificado, \n");
				consulta.append("a.coce_mt_suma_asegurada" +
						        ", a.coce_mt_prima_anual" +
						        ", a.coce_mt_prima_real, \n");
				consulta.append("a.coce_mt_prima_pura \n");
				consulta.append(",nvl(a.coce_cd_causa_anulacion, 0)\n");
			}
			if (tiprep.equals("2")){
				consulta.append("select a.coce_carp_cd_ramo" +
						        ", a.coce_capo_nu_poliza" +
						        ", ae.ales_descripcion" +
						        ", ae.ales_campo1" +						     
						        ", count(a.coce_nu_certificado), \n");
				consulta.append("sum(a.coce_mt_suma_asegurada)" +
						        ", sum(a.coce_mt_prima_anual)" +
						        ", sum(a.coce_mt_prima_real), \n");
				consulta.append("sum(a.coce_mt_prima_pura) \n");
			}
			consulta.append("from colectivos_certificados a " +
					        ",alterna_estatus         ae\n");
			consulta.append("where a.coce_casu_cd_sucursal 	= :canal \n");
			consulta.append("and a.coce_carp_cd_ramo     	= :ramo  \n");
			consulta.append("and a.coce_capo_nu_poliza   	= :Pol   \n");
			consulta.append("and a.coce_Nu_Certificado   	> 0 	 \n");
			consulta.append("and a.coce_st_certificado = ae.ales_cd_estatus \n");
			if (sts.equals("1")){
				//System.out.println("Vigentes");
				consulta.append("and a.coce_st_certificado   in(1,2) \n");
			}
			if (sts.equals("2")){
				consulta.append("and a.coce_st_certificado   =11 	 \n");
			}
			if (fechaini!=null && fechafin==null)
				consulta.append("and a.coce_fe_carga         >='" + fechaini + "' \n");
			if (fechaini!=null && fechafin!=null)
				consulta.append("and a.coce_fe_carga between '" + fechaini + "' and '" + fechafin + "' \n");
			if (fechaini==null && fechafin!=null)
				consulta.append("and a.coce_fe_carga         <='" + fechafin + "' \n");
			// ordenado por st_certificado
			//consulta.append("order by a.coce_st_certificado \n");
			if (tiprep.equals("2"))
				consulta.append("group by a.coce_carp_cd_ramo, a.coce_capo_nu_poliza, a.coce_st_certificado, ae.ales_campo1, ae.ales_descripcion \n");
			//
			if (tiprep.equals("1"))
				consulta.append("order by a.coce_st_certificado, a.coce_nu_certificado \n");
			//System.out.println(consulta.toString());		
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("Pol", Pol);
			
			List<T> lista = qry.list();
			
			return lista;
			}
			catch (Exception e){
				throw new Excepciones ("Error .",e);
			}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String totalCertif(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta, String rutaReportes)
			throws Excepciones {

		String cadenacan=null;
		
		StringBuilder filtro = new StringBuilder();

		try {
			
			if(poliza1==0){
				cadenacan="and cc.coce_capo_nu_poliza >= " + inIdVenta + "00000000 and cc.coce_capo_nu_poliza <= " + inIdVenta + "99999999 ";
			}else{
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza1).append(", 1, substr(").append(poliza1).append(", 0, 3), substr(").append(poliza1).append(", 0, 2)) \n");
				filtro.append("order by P.copaNvalor3");
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for(Parametros item: listaPolizas){
										
					if(item.getCopaNvalor4().toString().equals("1")){
						if(poliza2==0){
							cadenacan="and cc.coce_capo_nu_poliza like '" + poliza1 + "%'";
						}else{
							cadenacan="and cc.coce_capo_nu_poliza like '" + poliza2 + "%'";
						}
					}else{
						cadenacan="and cc.coce_capo_nu_poliza = " + poliza1 + " \n";
					}
				}
			}
			JasperReport report = null;
			String rutaTemporal = rutaReportes;
			StringBuffer sb = new StringBuffer();
			
			sb.append("Select ccc.cocc_id_certificado IDCERTIFICADO\n");
			sb.append(", cc.coce_capo_nu_poliza       POLIZA \n");
			sb.append(", cc.coce_nu_certificado       CERTIFICADO \n");
			sb.append(", cocn_buc_cliente             BUC \n");
			sb.append(", cc.coce_casu_cd_sucursal     SUCURSAL \n");
			sb.append(", cc.coce_carp_cd_ramo         RAMO \n");
			sb.append(", nvl(cc.coce_sucursal_banco,0) SUC_BANCO \n");
			sb.append(", cc.coce_mt_suma_asegurada    SUMA_ASEG \n");
			sb.append(", cc.coce_mt_prima_anual       PRIMA_ANUAL \n");
			sb.append(", cc.coce_mt_prima_subsecuente PRIMA_COBRADA \n");
			sb.append(", ae2.ales_descripcion 		  DESC_STAT \n");   
			sb.append(", ae2.ales_campo1 		      DESC_STAT2 \n");  
			sb.append(", cc.coce_fe_emision  FE_EMI \n");
			sb.append(", cc.coce_fe_desde    FE_DESDE         \n");
			sb.append(", cc.coce_fe_hasta    FE_HASTA         \n");
			sb.append(", cc.coce_fe_anulacion   FE_ANUL      \n");
			sb.append(", cc.coce_fe_anulacion_col   FE_ANUL_COL    \n");
			sb.append(", nvl(ae.ales_descripcion,' ') MOTIVO_CANCEL  \n");
			sb.append(", nvl(cc.coce_mt_devolucion,0) MT_DEVOL  \n");
			sb.append(", nvl(cc.coce_mt_bco_devolucion,0) MT_BCO_DEVOL,  \n");
		    if(ramo == 9 && inIdVenta == 6) {	/***APV***/								
				sb.append("	cc.coce_fe_emision FECHAEMISION, 											\n");
				sb.append("	 substr(cc.coce_campoV6,instr(cc.coce_campoV6, 'FeEn:',1,1) + 5,instr(cc.coce_campoV6, '|',1,6)-instr(cc.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
				sb.append("  substr(cc.coce_campoV6,instr(cc.coce_campoV6, 'FeCar:',1,1) + 6,instr(cc.coce_campoV6, '|',1,8)-instr(cc.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
				sb.append("	 substr(cc.coce_campoV6,instr(cc.coce_campoV6, 'FeRe:',1,1) + 5,instr(cc.coce_campoV6, '|',1,7)-instr(cc.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
			} else {																	
				sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,'' FECHARECIBIDO					\n");
			}		/***APV***/
			sb.append("from colectivos_certificados  cc  \n");
			sb.append("      LEFT JOIN Alterna_Estatus ae on cc.coce_cd_causa_anulacion = ae.ales_cd_estatus \n");
			sb.append("      LEFT JOIN Alterna_Estatus ae2 on cc.coce_st_certificado	= ae2.ales_cd_estatus \n");
			sb.append(",colectivos_cliente_certif       ccc \n");
			sb.append(",colectivos_clientes             ccl \n");
			sb.append("where ccc.cocc_casu_cd_sucursal = cc.coce_casu_cd_sucursal \n"); 
			sb.append("and ccc.cocc_carp_cd_ramo       = cc.coce_carp_cd_ramo     \n"); 
			sb.append("and ccc.cocc_capo_nu_poliza     = cc.coce_capo_nu_poliza   \n");
			sb.append("and ccc.cocc_nu_certificado     = cc.coce_nu_certificado   \n");
			sb.append("and ccc.cocc_nu_cliente         = ccl.cocn_nu_cliente      \n");
			sb.append("and nvl(ccc.cocc_tp_cliente,1)  = 1                        \n");
			sb.append("and cc.coce_casu_cd_sucursal = " + canal + " \n");
			sb.append("and cc.coce_carp_cd_ramo     = " + ramo + " \n");
			sb.append(cadenacan);

      	  if (fecha1!=null && fecha2!=null){
        	  sb.append("\nand cc.coce_fe_carga  between '"  + fecha1 + "' and '"  + fecha2 + "' \n");
          }
              
          if (fecha1!=null && fecha2==null) {
              sb.append("\nand cc.coce_fe_carga = '"  + fecha1 + "' \n");
          }
           
           if (fecha1==null && fecha2!=null) {
               sb.append("\nand cc.coce_fe_carga = '"  + fecha2 + "' \n");
          }
			File reporte = new File(rutaTemporal + "InteligenciaComercial.jrxml");
		    JasperDesign jasperDesign = null;
		    String nombre = "InteligenciaComercial_" + new SimpleDateFormat("ddMMyyyy").format(Calendar.getInstance().getTime());
		    nombre += ".csv"; 
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 1024, 1024);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			query.setText(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}	
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaMensualTecnico(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2)
			throws Excepciones {
		try {
			StringBuffer sb = new StringBuffer();
			
			sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
			sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
			sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
			sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
			sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
			sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
			sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
			sb.append("      ,est.ales_descripcion     ESTATUS_COLECTIVOS		 				\n");
			sb.append("      ,est.ales_campo1          ESTATUS   				 				\n");
			sb.append("      ,to_char(CASE WHEN c.coce_st_certificado in (1,2) THEN 'ALTA' ELSE 'BAJA' END) ESTATUS_MOVIMIENTO \n");
			sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
			sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
		    sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
			sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
			sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
			sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
			sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
			sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
			sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
			sb.append("      ,cl.cocn_rfc              RFC 										\n");
			sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
			sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
			sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
			sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
			sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
			sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
			sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
			sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
			sb.append("      ,nvl(c.coce_campov6,' ')  FECHA_FIN_CREDITO_2 						\n");
			sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
			sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
			sb.append("      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
			sb.append("      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
			sb.append("      ,case when pf.copa_nvalor5 = 0 									\n"); 
			sb.append("                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
			sb.append("                when pf.copa_nvalor5 = 1									\n"); 
			sb.append("                      then c.coce_mt_prima_pura							\n"); 
			sb.append("                else 0 end PRIMA_NETA									\n");
			sb.append("      -- \n");
			sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
			sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
			sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
			sb.append("      ,case when pf.copa_nvalor5 = 0 then \n"); 
			sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
			sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
			sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
			sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
			sb.append("                when pf.copa_nvalor5 = 1 then \n"); 
			sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
			sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
			sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
			sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n"); 
			sb.append("                else 0 end PRIMA_TOTAL  									 \n");
			sb.append("      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si),4) TARIFA \n");
			sb.append("      -- \n");
			sb.append("      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
			sb.append("      -- \n");      
			sb.append("            (select nvl(sum(cob.cocb_ta_riesgo),0) tasaVida 				\n");
			sb.append("              from colectivos_coberturas cob 							\n");
			sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
			sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo					\n");
			sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
			sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
			sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
			sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
			sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura 	\n");
			sb.append("               and cob.cocb_fe_hasta           > sysdate) 				\n");
			sb.append("            end,2) PRIMA_VIDA 											\n");
			sb.append("      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
			sb.append("            (select nvl(sum(cob.cocb_ta_riesgo),0) tasaDes 				\n");
			sb.append("              from colectivos_coberturas cob 							\n");
			sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
			sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
			sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
			sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
			sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
			sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
			sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura 	\n");
			sb.append("               and cob.cocb_fe_hasta           > sysdate) 				\n");
			sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
			sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
			sb.append("      ,es.caes_de_estado        ESTADO 									\n");
			//sb.append("      ,(select nvl(MAX(pob.caiv_cd_poblacion),0) 						\n");
			//sb.append("         from cart_estados_iva pob 										\n");
			//sb.append("        where pob.caiv_cp_poblacion = to_number(cl.cocn_cd_postal) 		\n");
			//sb.append("       )                        CD_MUNICIPIO 							\n");
			sb.append("      ,nvl(cl.cocn_delegmunic,' ')       MUNICIPIO 						\n");
			sb.append("      ,nvl(cl.cocn_cd_postal,' ')        CP 								\n");
			sb.append("      ,nvl(c.coce_mt_devolucion,0) MONTO_DEVOLUCION 						\n");
			sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
			sb.append("      ,nvl(c.coce_buc_empresa,' ')	   CREDITONUEVO						\n");
			sb.append("  from colectivos_certificados   c 										\n");
			sb.append("      ,colectivos_cliente_certif cc 										\n");
			sb.append("      ,colectivos_clientes       cl 										\n");
			sb.append("      ,colectivos_facturacion    fac 									\n");
			sb.append("      ,cart_estados              es 										\n");
			sb.append("      ,alterna_estatus           est 									\n");
			sb.append("      ,colectivos_certificados   head 									\n");
			sb.append("      ,alterna_planes            pl 										\n");
			sb.append("      ,colectivos_parametros     pf										\n");
			sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
			sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
			sb.append("  and fac.cofa_capo_nu_poliza    >= " + poliza1 + "  					\n");
			sb.append("  and fac.cofa_capo_nu_poliza    <= " + poliza2 + " 						\n");
			sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
			sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
			sb.append("  -- \n");
			sb.append("  and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n");
			sb.append("  and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n");
			sb.append("  and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n");
			sb.append("  and c.coce_nu_certificado   > 0 										\n");
			sb.append("  and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal 				\n");
			sb.append("  -- \n");
			sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
			sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
			sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
			sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
			sb.append("  and nvl(cc.cocc_tp_cliente,1)= 1  										\n");
			sb.append("  -- \n");
			sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
			sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
			sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
			sb.append("  and head.coce_nu_certificado   = 0 									\n");
			sb.append("  -- \n");
			sb.append("  and cl.cocn_nu_cliente        = cc.cocc_nu_cliente 					\n");
			sb.append("  -- \n");
			sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
			sb.append("  -- \n");
			sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
			sb.append("  -- \n");
			sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
			sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
			sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
			sb.append("--																		\n");
		    sb.append("  and pf.copa_des_parametro    = 'POLIZA'								\n");
		    sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
		    sb.append("  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n");
		    sb.append("  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n");
		    sb.append("  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, fac.cofa_capo_nu_poliza, 1, substr(fac.cofa_capo_nu_poliza,0,3), substr(fac.cofa_capo_nu_poliza,0,2)) \n");
		    sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
	
          Query query = this.getSession().createSQLQuery(sb.toString());

          List<T> lista = query.list();
			
		  return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> seleccionaDatosAutos()
			throws Excepciones {
		try {
			System.out.println("CertificadoDaoHibernate");
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select a.coce_casu_cd_sucursal,\n");
			consulta.append("       a.coce_carp_cd_ramo,\n");
			consulta.append("       a.coce_capo_nu_poliza,\n");
			consulta.append("       a.coce_nu_certificado,\n");
			consulta.append("       d.copc_cuenta,\n");
			consulta.append("       d.copc_dato11,\n");
			consulta.append("       d.copc_id_certificado\n");
			consulta.append("from\n");
			consulta.append("colectivos_certificados a,\n");
			consulta.append("colectivos_cliente_certif b,\n");
			consulta.append("colectivos_clientes c,\n");
			consulta.append("colectivos_precarga d \n");
			consulta.append("where b.cocc_casu_cd_sucursal = a.coce_casu_cd_sucursal\n");
			consulta.append("  and b.cocc_carp_cd_ramo     = a.coce_carp_cd_ramo\n");
			consulta.append("  and b.cocc_capo_nu_poliza   = a.coce_capo_nu_poliza\n");
			consulta.append("  and b.cocc_nu_certificado   = a.coce_nu_certificado\n");
			consulta.append("  and c.cocn_nu_cliente       = b.cocc_nu_cliente\n");
			consulta.append("  and b.cocc_id_certificado   = d.copc_id_certificado\n");
			consulta.append("  and c.cocn_buc_cliente      = d.copc_num_cliente\n");
			consulta.append("  and d.copc_cd_sucursal      = 666\n");
			consulta.append("  and d.copc_cd_ramo          = 666\n");		
			consulta.append("  and d.copc_num_poliza       = 666\n");
			consulta.append("  and d.copc_cargada          = 0\n");
			
			Query qry = this.getSession().createSQLQuery(consulta.toString());
			System.out.println(consulta.toString());
			List<T> lista = qry.list();
			return lista;
		}catch (Exception e){
			throw new Excepciones("Error.", e);
		}
	}

	public void actualizaDatosAutos(Short canal, Short ramo, Long poliza, Long nucert, String cred, String nomarch, String cred2) {
		StringBuilder actcertif = new StringBuilder() ;
		StringBuilder actprecarga = new StringBuilder() ;
		
		actcertif.append("update\n");
		actcertif.append("colectivos_certificados cer\n");
		actcertif.append("set cer.coce_buc_empresa ='" + cred + "',\n");
		actcertif.append("    cer.coce_campov5     ='" + nomarch + "',\n");
		actcertif.append("    cer.coce_campof1     = sysdate\n");
		actcertif.append("where cer.coce_casu_cd_sucursal =" + canal + "\n");
		actcertif.append("  and cer.coce_carp_cd_ramo     =" + ramo + "\n");
		actcertif.append("  and cer.coce_capo_nu_poliza   =" + poliza + "\n");
		actcertif.append("  and cer.coce_nu_certificado   =" + nucert + "\n");
		Query queryActcert = getSession().createSQLQuery(actcertif.toString()); 
		queryActcert.executeUpdate();
		
		actprecarga.append("update\n");
		actprecarga.append("colectivos_precarga pre\n");
		actprecarga.append("set pre.copc_cargada = 6, pre.copc_registro = 'Credito Actualizado'");
		actprecarga.append("where pre.copc_cd_sucursal      = 666");
		actprecarga.append("  and pre.copc_cd_ramo          = 666");
		actprecarga.append("  and pre.copc_num_poliza       = 666");
		actprecarga.append("  and pre.copc_id_certificado   =" + cred2 + "\n");
		Query queryActpre = getSession().createSQLQuery(actprecarga.toString()); 
		queryActpre.executeUpdate();
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> generaReporteAutos()
			throws Excepciones {
		try {
			StringBuilder actprecarga = new StringBuilder() ;
			StringBuilder selectprecarga = new StringBuilder() ;
			
			actprecarga.append("update ");
			actprecarga.append("colectivos_precarga pre ");
			actprecarga.append("set pre.copc_cargada = 7, pre.copc_registro = 'Credito o BUC no encontrado' ");
			actprecarga.append("where pre.copc_cd_sucursal = 666 ");
			actprecarga.append("and pre.copc_cd_ramo = 666 ");
			actprecarga.append("and pre.copc_num_poliza = 666 ");
			actprecarga.append("and pre.copc_cargada = 0");
			Query queryActcert = getSession().createSQLQuery(actprecarga.toString()); 
			queryActcert.executeUpdate();

			selectprecarga.append("select pre.copc_id_certificado, ");
			selectprecarga.append("pre.copc_registro, ");
			selectprecarga.append("pre.copc_dato11 ");
			selectprecarga.append("from colectivos_precarga pre ");
			selectprecarga.append("where pre.copc_cd_sucursal = 666"); 
			selectprecarga.append(" and pre.copc_cd_ramo = 666"); 
			selectprecarga.append(" and pre.copc_num_poliza = 666");
	
          Query query = this.getSession().createSQLQuery(selectprecarga.toString());

          List<T> lista = query.list();
			
		  return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> cifrasResumenAutos()
			throws Excepciones {
		try {
			StringBuilder selectprecarga = new StringBuilder() ;
			
			selectprecarga.append("select prec.copc_registro, count(1) from");
			selectprecarga.append(" colectivos_precarga prec");
			selectprecarga.append(" where prec.copc_cd_sucursal = 666");
			selectprecarga.append(" and prec.copc_cd_ramo = 666");
			selectprecarga.append(" and prec.copc_num_poliza = 666"); 
			selectprecarga.append(" group by prec.copc_registro"); 
			selectprecarga.append(" order by prec.copc_registro");
	
          Query query = this.getSession().createSQLQuery(selectprecarga.toString());

          List<T> lista = query.list();
			
		  return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
		
		
	}
	@SuppressWarnings("unchecked")
	public <T> List<T> generaReporteEmisionAutos() throws Excepciones {
		try {
			StringBuilder selectprecarga = new StringBuilder() ;
			selectprecarga.append("select pre.copc_id_certificado, ");
			selectprecarga.append("pre.copc_num_cliente, ");
			selectprecarga.append("pre.copc_registro, ");
			selectprecarga.append("pre.copc_dato11 ");
			selectprecarga.append("from colectivos_precarga pre ");
			selectprecarga.append("where pre.copc_cd_sucursal = 666"); 
			selectprecarga.append(" and pre.copc_cd_ramo = 666"); 
			selectprecarga.append(" and pre.copc_num_poliza = 666");
			selectprecarga.append(" and pre.copc_cargada <> 6");
		
		  Query query = this.getSession().createSQLQuery(selectprecarga.toString());
		
		  List<T> lista = query.list();
			
		  return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	/**
	 * IBB: 15042020.
	 * Se modifica para agregar campos de Cuotas Pendientes y a la vez bajar porcentaje de Sonar
	 */
	@SuppressWarnings({ "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String consultaMensualEmision(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String rutaReportes) throws Excepciones {
		StringBuilder sb = new StringBuilder();
		StringBuilder cadenafac = null;
		StringBuilder cadenacan = null;
		StringBuilder sbFechasPoliza = null;
		StringBuilder sbPolizaCobertura = null;
		StringBuilder sbPolizaCoberturaMov = null;
		StringBuilder sbCamposFechas = null;
		
		String sts = Constantes.DEFAULT_STRING;
		String strQuery;
		String cuotasPendientes = Constantes.DEFAULT_STRING_ID;
		
		try {
			cadenafac = new StringBuilder();
			cadenacan = new StringBuilder();
			sbFechasPoliza = new StringBuilder();
			sbPolizaCobertura = new StringBuilder();
			sbPolizaCoberturaMov = new StringBuilder();
			sbCamposFechas = new StringBuilder();
			
			if(poliza1 == 0) {
				//Es PU
				cadenafac = GeneratorQuerys.getCadenaFac(poliza1, inIdVenta, Constantes.DEFAULT_INT);
				cadenacan = GeneratorQuerys.getCadenaCan(poliza1, inIdVenta, Constantes.DEFAULT_INT);
				sbFechasPoliza = GeneratorQuerys.fechasPolizaPU;
				sbPolizaCobertura = GeneratorQuerys.polizaCoberPU;
				sbPolizaCoberturaMov = GeneratorQuerys.polizaCoberPU;
				
				if(ramo == 9 && inIdVenta == 6) {								
					sbCamposFechas = GeneratorQuerys.camposFehasPU;
				} else {																		
					sbCamposFechas = GeneratorQuerys.camposFehasPR;
				}
			} else {
				//Es PR
				cadenafac = GeneratorQuerys.getCadenaFac(poliza1, inIdVenta, 1);
				cadenacan = GeneratorQuerys.getCadenaCan(poliza1, inIdVenta, 1);
				sbFechasPoliza = GeneratorQuerys.fechasPolizaPR;
				sbPolizaCobertura = GeneratorQuerys.polizaCoberPR;
				sbPolizaCoberturaMov = GeneratorQuerys.polizaCoberMovPR;
				sbCamposFechas = GeneratorQuerys.camposFehasPR;

				//Obtenemos cuotas pendiebtes
				StringBuilder sbFiltro = GeneratorQuerys.getConsultaCuotasPendientes(canal, ramo, poliza1);
				Query qry = this.getSession().createSQLQuery(sbFiltro.toString());
				cuotasPendientes = ((Object[])qry.list().get(0))[1].toString();
			}
			
			sts = ReportesJasper.setEstatus(chk1, chk2, chk3);
			
			sb.append(ReportesJasper.setQryTecnico(chk1, chk2, chk3));
			
			strQuery = sb.toString();
			strQuery = strQuery.replace("#fechasPoliza", sbFechasPoliza.toString());
			strQuery = strQuery.replace("#polizaCober", sbPolizaCobertura.toString());
			strQuery = strQuery.replace("#movCober", sbPolizaCoberturaMov.toString());
			strQuery = strQuery.replace("#camposFechas", sbCamposFechas.toString());
			strQuery = strQuery.replace("#canal", canal.toString());
			strQuery = strQuery.replace("#ramo", ramo.toString());
			strQuery = strQuery.replace("#cadenafac", cadenafac.toString());
			strQuery = strQuery.replace("#fecha1", fecha1.toString());
			strQuery = strQuery.replace("#fecha2", fecha2.toString());
			strQuery = strQuery.replace("#cadenacan", cadenacan.toString());
			strQuery = strQuery.replace("#cuopend", cuotasPendientes);
			
			//Generamos nombre del reporte
			StringBuilder nombre = new StringBuilder();
			nombre.append(Constantes.SUFIJO_REPEMI)
				.append(sts)
				.append(Constantes.GUION_BAJO)
				.append(poliza1)
				.append(Constantes.GUION_BAJO)
				.append(inIdVenta)
				.append(Constantes.GUION_BAJO)
				.append(GestorFechas.formatDate(new Date(), Constantes.FORMATO_FECHA_REP))
				.append(".csv");
			
			if(rutaReportes.equals(Constantes.DEFAULT_STRING)) {
				//Se manda insertar reporte a tabla de reportes por batch
				insertaBanderaBatch(Constantes.DEFAULT_LONG, strQuery, FacesUtils.getBeanSesion().getUserName(), Constantes.DEFAULT_STRING, nombre.toString(), Constantes.DEFAULT_LONG, Constantes.DEFAULT_LONG);
			} else {
				//Se genera reporte al momento
				ReportesJasper.generaReporte(strQuery, nombre.toString(), rutaReportes, getSession().connection());
			}
		    
		    return nombre.toString();
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String consultaMensualEmision5758(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta
			, boolean chk1, boolean chk2, boolean chk3, String rutaReportes)
			throws Excepciones {

		JasperReport report = null;
		String rutaTemporal = rutaReportes;
		StringBuffer sb = new StringBuffer();

		@SuppressWarnings("unused")
		int pu = 0;
		String cadenafac=null;
		String cadenacan=null;
		int flag = 0;
		String status = null;
		String coma=",";
		
		Integer inicioPoliza = 0;
		
		StringBuilder filtro = new StringBuilder();
		StringBuilder filtro1 = new StringBuilder();
		try {
			
	
			
		    			
			if(poliza1==0){
				
				filtro.append("  and P.copaDesParametro = 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro  = 'GEP' \n");
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor6 = ").append(inIdVenta).append("\n");
			    
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				if (listaPolizas != null && listaPolizas.size()>0)
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				else inicioPoliza = -1;
				
				cadenafac="  and fac.cofa_capo_nu_poliza >= " + inicioPoliza + "00000000 and fac.cofa_capo_nu_poliza <= " + inicioPoliza + "99999999 \n";
				      //   +"  and c.coce_sub_campana      = '" + inIdVenta + "' 				        \n";
				cadenacan="  and c.coce_capo_nu_poliza >= " + inicioPoliza + "00000000 and c.coce_capo_nu_poliza <= " + inicioPoliza + "99999999 \n"
				         +"  and c.coce_sub_campana      = '" + inIdVenta + "' 				        \n";

			}else{
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza1).append(", 1, ").append(poliza1.toString().substring(0,3)).append(", 2, ").append(poliza1.toString().substring(0,2)).append(")\n");
				filtro.append("order by P.copaNvalor3");
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for(Parametros item: listaPolizas){
										
					if(item.getCopaNvalor4().toString().equals("1")){
						pu = 1;
						if(poliza2==0){
							cadenafac="  and fac.cofa_capo_nu_poliza like '" + poliza1 + "%'";
							cadenacan="  and c.coce_capo_nu_poliza like '" + poliza1 + "%'";
						}else{
							cadenafac="  and fac.cofa_capo_nu_poliza like '" + poliza2 + "%'";
							cadenacan="  and c.coce_capo_nu_poliza like '" + poliza2 + "%'";
						}
					}else{
						pu = 0;
						cadenafac="  and fac.cofa_capo_nu_poliza = " + poliza1;
						cadenacan="  and c.coce_capo_nu_poliza = " + poliza1;
					}
				}
			}
			if(chk2==true || chk3==true){
				//System.out.println("1");
				filtro1.append("  and P.copaDesParametro= 'REPORTES' \n");
				filtro1.append("  and P.copaIdParametro = 'EMISION'    \n" );
				filtro1.append("  and P.copaNvalor1 = 2 \n");
				filtro1.append("order by P.copaNvalor2");
				
				List<Parametros> listastatus = this.servicioParametros.obtenerObjetos(filtro1.toString());
				
				for(Parametros item: listastatus){
					if(flag==0){
						status= item.getCopaNvalor2().toString();
						flag=1;
					}
					else{
						status = status + coma;
						status= status + item.getCopaNvalor2().toString();
					}
				}
			}
			
			if(chk1==true){
				sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
				sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
				sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
				sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
				sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
				sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
				sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
				sb.append("      ,est.ales_campo1		     ESTATUS   				 				\n");
				sb.append("      ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n");
				sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
				sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
				sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
				sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
				sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
				sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
				sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
				sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
				sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
				sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
				sb.append("      ,cl.cocn_rfc              RFC 										\n");
				sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
				if (poliza1 != 0){
					sb.append(",(select min(re.care_fe_desde)										\n");
				    sb.append("    from cart_recibos re												\n");
				    sb.append("    where re.care_casu_cd_sucursal        = r.care_casu_cd_sucursal	\n");
				    sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
				    sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
				    sb.append("      and re.care_cace_nu_certificado     = 0						\n");
				    sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
				    sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
				    sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
				    sb.append("      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_INICIO_POLIZA	\n");
				    sb.append(",(select add_months(min(re.care_fe_desde),12)						\n");
				    sb.append("    from cart_recibos re												\n");
				    sb.append("    where re.care_casu_cd_sucursal      	 = r.care_casu_cd_sucursal	\n");
				    sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
				    sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
				    sb.append("      and re.care_cace_nu_certificado     = 0						\n");
				    sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
				    sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
				    sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
				    sb.append("      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_FIN_POLIZA	\n");
					
				}else{
					sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
					sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
				}
				sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
				sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
				sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
				sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
				sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
				sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
				sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
				sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
				sb.append("      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
				sb.append("      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 									\n"); 
				sb.append("                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1									\n"); 
				sb.append("                      then c.coce_mt_prima_pura							\n"); 
				sb.append("                else 0 end PRIMA_NETA									\n");
				sb.append("      -- \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 then \n"); 
				sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1 then \n"); 
				sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n"); 
				sb.append("                else 0 end PRIMA_TOTAL  									 \n");
				sb.append("      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
				sb.append("      -- \n");
				sb.append("      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
				sb.append("      -- \n");      
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo		\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_VIDA 											\n");
				sb.append("      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
				sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
				sb.append("      ,es.caes_de_estado        ESTADO 									\n");
				sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
				sb.append("      ,cl.cocn_cd_postal        CP 										\n");
				sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
				sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
				sb.append("      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");
				sb.append("      ,nvl(c.coce_buc_empresa,0)		  CREDITONUEVO			     		\n");
				sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"); 
				sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n"); 
				sb.append("        from colectivos_coberturas cob                    \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n"); 
				sb.append("        from colectivos_coberturas cob                                       \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n"); 
				sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n"); 
				sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"); 
				sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n"); 
				sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n"); 
				sb.append("           p.copa_vvalor1           ||'|'||                                                 \n"); 
				sb.append("           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"); 
				sb.append("      from colectivos_cliente_certif clc                                                    \n"); 
				sb.append("          ,colectivos_clientes       cln                                                    \n"); 
				sb.append("          ,colectivos_parametros     p                                       \n"); 
				sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n"); 
				sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n"); 
				sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n"); 
				sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n"); 
				sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"); 
				sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"); 
				sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"); 
				sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n"); 
				sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"); 
				sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
				if(ramo == 9 && inIdVenta == 6) {		/*APV*/								
					sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
					sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
					sb.append("  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
					sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
				} else {																		
					sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
				}	 /*APV*/
				sb.append("  from colectivos_certificados   c 										\n");
				sb.append("      ,colectivos_cliente_certif cc 										\n");
				sb.append("      ,colectivos_clientes       cl 										\n");
				sb.append("      ,colectivos_facturacion    fac 									\n");
				sb.append("      ,cart_recibos   			r 									    \n");
				sb.append("      ,cart_estados              es 										\n");
				sb.append("      ,alterna_estatus           est 									\n");
				sb.append("      ,colectivos_certificados   head 									\n");
				sb.append("      ,alterna_planes            pl 										\n");
				sb.append("      ,colectivos_parametros     pf										\n");
				sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
				sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
				sb.append(cadenafac);
				sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
				sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
				sb.append("  -- \n");
				sb.append("  and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n");
				sb.append("  and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n");
				sb.append("  and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n");
				sb.append("  and c.coce_nu_certificado   > 0 										\n");
				sb.append("  and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal 				\n");
				sb.append("  and c.coce_campon2          in (0,2008) 				                \n");
				sb.append("  -- \n");
				sb.append("  and r.care_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n");
			    sb.append("  and r.care_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n");
				sb.append("  and r.care_st_recibo        in (1,4)									\n");
				sb.append("  -- \n");
				sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
				sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
				sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
				sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
				sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
				sb.append("  -- \n");
				sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
				sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
				sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
				sb.append("  and head.coce_nu_certificado   = 0 									\n");
				sb.append("  -- \n");
				sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
				sb.append("  -- \n");
				sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
				sb.append("  -- \n");
				sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
				sb.append("  -- \n");
				sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
				sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
				sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
				sb.append("--																		\n");
			    sb.append("  and pf.copa_des_parametro    = 'POLIZA'								\n");
			    sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
			    sb.append("  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n");
			    sb.append("  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n");
			    sb.append("  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,1,substr(fac.cofa_capo_nu_poliza,0,3),2,substr(fac.cofa_capo_nu_poliza,0,2)) \n");
			    sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
			    
			    sb.append("\n");
			    sb.append("union all \n");
			    sb.append("\n");
			    
			    sb.append("select c.cocm_carp_cd_ramo      RAMO 									\n");
				sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
				sb.append("      ,c.cocm_capo_nu_poliza    POLIZA 									\n");
				sb.append("      ,c.cocm_nu_recibo         RECIBO 									\n");
				sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
				sb.append("      ,c.cocm_nu_certificado    CERTIFICADO 								\n");
				sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
				sb.append("      ,est.ales_campo1		     ESTATUS   				 				\n");
				sb.append("      ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n");
				sb.append("      ,c.cocm_nu_cuenta         CUENTA 									\n");
				sb.append("      ,c.cocm_tp_producto_bco   PRODUCTO 								\n");
				sb.append("      ,c.cocm_tp_subprod_bco    SUBPRODUCTO 								\n");
				sb.append("      ,c.cocm_cazb_cd_sucursal  SUCURSAL 								\n");
				sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
				sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
				sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
				sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
				sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
				sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
				sb.append("      ,cl.cocn_rfc              RFC 										\n");
				sb.append("      ,c.cocm_cd_plazo          PLAZO 									\n");				
				if (poliza1 != 0){
					sb.append(",(select min(re.care_fe_desde)										\n");
				    sb.append("    from cart_recibos re												\n");
				    sb.append("    where re.care_casu_cd_sucursal        = r.care_casu_cd_sucursal	\n");
				    sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
				    sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
				    sb.append("      and re.care_cace_nu_certificado     = 0						\n");
				    sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
				    sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
				    sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
				    sb.append("      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_INICIO_POLIZA	\n");
				    sb.append(",(select add_months(min(re.care_fe_desde),12)						\n");
				    sb.append("    from cart_recibos re												\n");
				    sb.append("    where re.care_casu_cd_sucursal        = r.care_casu_cd_sucursal	\n");
				    sb.append("      and re.care_carp_cd_ramo            = r.care_carp_cd_ramo		\n");
				    sb.append("      and re.care_capo_nu_poliza          = r.care_capo_nu_poliza	\n");
				    sb.append("      and re.care_cace_nu_certificado     = 0						\n");
				    sb.append("      and re.care_st_recibo               in (1,2,4)					\n");
				    sb.append("      and re.care_nu_consecutivo_cuota    = 1						\n");
				    sb.append("      and re.care_fe_desde                <= r.care_fe_desde			\n");
				    sb.append("      and add_months(re.care_fe_desde,12) > r.care_fe_desde)	FECHA_FIN_POLIZA	\n");
					
				}else{
					sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
					sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
				}			
				sb.append("      ,c.cocm_fe_suscripcion    FECHA_INGRESO 							\n");
				sb.append("      ,c.cocm_fe_desde          FECHA_DESDE 								\n");
				sb.append("      ,c.cocm_fe_hasta          FECHA_HASTA 								\n");
				sb.append("      ,c.cocm_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
				sb.append("      ,c.cocm_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
				sb.append("      ,c.cocm_campov6		   FECHA_FIN_CREDITO_2 						\n");
				sb.append("      ,c.cocm_fe_anulacion_real FECHA_ANULACION 							\n");
				sb.append("      ,c.cocm_fe_anulacion      FECHA_CANCELACION 						\n");
				sb.append("      ,trim(to_char(decode(c.cocm_sub_campana,'7',to_number(c.cocm_mt_suma_aseg_si),c.cocm_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
				sb.append("      ,decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si) BASE_CALCULO 							\n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 									\n"); 
				sb.append("                     then nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1									\n"); 
				sb.append("                      then c.cocm_mt_prima_pura							\n"); 
				sb.append("                else 0 end PRIMA_NETA									\n");
				sb.append("      -- \n");
				sb.append("      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1) -instr(c.cocm_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
				sb.append("      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2) -instr(c.cocm_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
				sb.append("      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)) IVA \n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 then \n"); 
				sb.append("                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1 then \n"); 
				sb.append("                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(c.cocm_mt_prima_pura,0) \n"); 
				sb.append("                else 0 end PRIMA_TOTAL  									 \n");
				sb.append("      ,round(c.cocm_mt_prima_pura*1000/decode(c.cocm_sub_campana,'7',decode(c.cocm_mt_suma_asegurada,0,1,c.cocm_mt_suma_asegurada),decode(c.cocm_mt_suma_aseg_si,0,1,c.cocm_mt_suma_aseg_si)),4) TARIFA \n");
				sb.append("      -- \n");
				sb.append("      , round(case when c.cocm_carp_cd_ramo in (61,65) then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n");
				sb.append("      -- \n");      
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = c.cocm_carp_cd_ramo 		\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				  sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_VIDA 											\n");
				sb.append("      , round(case when c.cocm_carp_cd_ramo = 61 then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n");
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				  sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
				sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
				sb.append("      ,es.caes_de_estado        ESTADO 									\n");
				sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
				sb.append("      ,cl.cocn_cd_postal        CP 										\n");
				sb.append("      ,nvl(c.cocm_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
				sb.append("      ,nvl(c.cocm_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
				sb.append("      ,abs( nvl(c.cocm_mt_devolucion ,0) - nvl(c.cocm_mt_bco_devolucion ,0) ) DIFERENCIA_DEVOLUCION \n");
				sb.append("      ,nvl(c.cocm_buc_empresa,0)		  CREDITONUEVO			     		\n");
				sb.append(",c.cocm_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"); 
				sb.append(",c.cocm_capb_cd_plan             PLAN_ASEGURADORA         \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n"); 
				sb.append("        from colectivos_coberturas cob                    \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_BASICA \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n"); 
				sb.append("        from colectivos_coberturas cob                                       \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_DESEMPLEO  \n"); 
				sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n"); 
				sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"); 
				sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n"); 
				sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n"); 
				sb.append("           p.copa_vvalor1           ||'|'||                                                 \n"); 
				sb.append("           substr(c.cocm_empresa,instr(c.cocm_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"); 
				sb.append("      from colectivos_cliente_certif clc                                                    \n"); 
				sb.append("          ,colectivos_clientes       cln                                                    \n"); 
				sb.append("          ,colectivos_parametros     p                                       \n"); 
				sb.append("     where clc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal               \n"); 
				sb.append("       and clc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo                   \n"); 
				sb.append("       and clc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza                 \n"); 
				sb.append("       and clc.cocc_nu_certificado   = c.cocm_nu_certificado                 \n"); 
				sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"); 
				sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"); 
				sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"); 
				sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n"); 
				sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"); 
				sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
				if(ramo == 9 && inIdVenta == 6) {		/*APV*/								
					sb.append("	c.cocm_fe_emision FECHAEMISION, 											\n");
					sb.append("	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeEn:',1,1) + 5,instr(c.cocm_campoV6, '|',1,6)-instr(c.cocm_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
					sb.append("  substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeCar:',1,1) + 6,instr(c.cocm_campoV6, '|',1,8)-instr(c.cocm_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
					sb.append("	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeRe:',1,1) + 5,instr(c.cocm_campoV6, '|',1,7)-instr(c.cocm_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
				} else {																		
					sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
				}	 /*APV*/
				sb.append("  from colectivos_certificados_mov c 										\n");
				sb.append("      ,colectivos_cliente_certif cc 										\n");
				sb.append("      ,colectivos_clientes       cl 										\n");
				sb.append("      ,colectivos_facturacion    fac 									\n");
				sb.append("      ,cart_recibos   			r 									    \n");
				sb.append("      ,cart_estados              es 										\n");
				sb.append("      ,alterna_estatus           est 									\n");
				sb.append("      ,colectivos_certificados   head 									\n");
				sb.append("      ,alterna_planes            pl 										\n");
				sb.append("      ,colectivos_parametros     pf										\n");
				sb.append("where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n");
				sb.append("  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n");
				sb.append(cadenafac);
				sb.append("  and fac.cofa_fe_facturacion    >= to_date('" + fecha1 + "','dd/mm/yyyy') \n");
				sb.append("  and fac.cofa_fe_facturacion    <= to_date('" + fecha2 + "','dd/mm/yyyy') \n");
				sb.append("  -- \n");
				sb.append("  and r.care_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n");
			    sb.append("  and r.care_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n");
				sb.append("  and r.care_st_recibo        in (1,4)									\n");
				sb.append("  -- \n");
				sb.append("  and c.cocm_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n");
				sb.append("  and c.cocm_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n");
				sb.append("  and c.cocm_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n");
				sb.append("  and c.cocm_nu_certificado   > 0 										\n");
				sb.append("  and c.cocm_nu_recibo        = fac.cofa_nu_recibo_fiscal 				\n");
				sb.append("  and c.cocm_campon2          in (0,2008) 				                \n");
				sb.append("  -- \n");
				sb.append("  and cc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n");
				sb.append("  and cc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n");
				sb.append("  and cc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza	 				\n");
				sb.append("  and cc.cocc_nu_certificado   = c.cocm_nu_certificado 					\n");
				sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
				sb.append("  -- \n");
				sb.append("  and head.coce_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n");
				sb.append("  and head.coce_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n");
				sb.append("  and head.coce_capo_nu_poliza   = c.cocm_capo_nu_poliza 				\n");
				sb.append("  and head.coce_nu_certificado   = 0 									\n");
				sb.append("  -- \n");
				sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
				sb.append("  -- \n");
				sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
				sb.append("  -- \n");
				sb.append("  and est.ales_cd_estatus      = c.cocm_st_certificado 					\n");
				sb.append("  -- \n");
				sb.append("  and pl.alpl_cd_ramo          = c.cocm_carp_cd_ramo 					\n");
				sb.append("  and pl.alpl_cd_producto      = c.cocm_capu_cd_producto 				\n");
				sb.append("  and pl.alpl_cd_plan          = c.cocm_capb_cd_plan  					\n");
				sb.append("--																		\n");
			    sb.append("  and pf.copa_des_parametro    = 'POLIZA'								\n");
			    sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
			    sb.append("  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n");
			    sb.append("  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n");
			    sb.append("  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,fac.cofa_capo_nu_poliza,1,substr(fac.cofa_capo_nu_poliza,0,3),2,substr(fac.cofa_capo_nu_poliza,0,2)) \n");
			    sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
			    
			}
			if(chk1==true && chk2==true) sb.append("  union all \n");
			if(chk2==true){
				sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
				sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
				sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
				sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
				sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
				sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
				sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
				sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");
				sb.append("      ,'BAJA'                   ESTATUS_MOVIMIENTO						\n");
				sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
				sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
				sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
				sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
				sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
				sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
				sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
				sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
				sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
				sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
				sb.append("      ,cl.cocn_rfc              RFC 										\n");
				sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
				sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
				sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
				sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
				sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
				sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
				sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
				sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
				sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
				sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
				sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
				sb.append("      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
				sb.append("      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 									\n"); 
				sb.append("                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1									\n"); 
				sb.append("                      then c.coce_mt_prima_pura							\n"); 
				sb.append("                else 0 end PRIMA_NETA									\n");
				sb.append("      -- \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 then \n"); 
				sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1 then \n"); 
				sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n"); 
				sb.append("                else 0 end PRIMA_TOTAL  									 \n");
				sb.append("      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
				sb.append("      -- \n");
				sb.append("      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
				sb.append("      -- \n");      
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo 		\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				   sb.append("            and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_VIDA 											\n");
				sb.append("      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
				sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
				sb.append("      ,es.caes_de_estado        ESTADO 									\n");
				sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
				sb.append("      ,cl.cocn_cd_postal        CP 										\n");
				sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
				sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
				sb.append("      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");
				sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
				sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"); 
				sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n"); 
				sb.append("        from colectivos_coberturas cob                    \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n"); 
				sb.append("        from colectivos_coberturas cob                                       \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n"); 
				sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n"); 
				sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"); 
				sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n"); 
				sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n"); 
				sb.append("           p.copa_vvalor1           ||'|'||                                                 \n"); 
				sb.append("           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"); 
				sb.append("      from colectivos_cliente_certif clc                                                    \n"); 
				sb.append("          ,colectivos_clientes       cln                                                    \n"); 
				sb.append("          ,colectivos_parametros     p                                       \n"); 
				sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n"); 
				sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n"); 
				sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n"); 
				sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n"); 
				sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"); 
				sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"); 
				sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"); 
				sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n"); 
				sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"); 
				sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
				if(ramo == 9 && inIdVenta == 6) {		/*APV*/								
					sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
					sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
					sb.append("  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
					sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
				} else {																		
					sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
				}	 /*APV*/
				sb.append("  from colectivos_certificados   c 										\n");
				sb.append("      ,colectivos_cliente_certif cc 										\n");
				sb.append("      ,colectivos_clientes       cl 										\n");
				sb.append("      ,cart_estados              es 										\n");
				sb.append("      ,alterna_estatus           est 									\n");
				sb.append("      ,colectivos_certificados   head 									\n");
				sb.append("      ,alterna_planes            pl 										\n");
				sb.append("      ,colectivos_parametros     pf										\n");
				sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
				sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
				sb.append(cadenacan);
				sb.append("  and c.coce_nu_certificado   > 0			 							\n");
				sb.append("  and c.coce_st_certificado   in (10,11)  	     				        \n");
				sb.append("  and c.coce_cd_causa_anulacion <> 45			  	     				\n");
				sb.append("  and c.coce_fe_anulacion    >= to_date('" + fecha1 + "','dd/mm/yyyy')   \n");
				sb.append("  and c.coce_fe_anulacion    <= to_date('" + fecha2 + "','dd/mm/yyyy')   \n");
				sb.append("  -- \n");
				sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
				sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
				sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
				sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
				sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
				sb.append("  -- \n");
				sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
				sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
				sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
				sb.append("  and head.coce_nu_certificado   = 0 									\n");
				sb.append("  -- \n");
				sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
				sb.append("  -- \n");
				sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
				sb.append("  -- \n");
				sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
				sb.append("  -- \n");
				sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
				sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
				sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
				sb.append("--																		\n");
			    sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
			    sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
			    sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
			    sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
			    sb.append("  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,c.coce_capo_nu_poliza,1,substr(c.coce_capo_nu_poliza,0,3),2,substr(c.coce_capo_nu_poliza,0,2)) \n");
			    sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
			}
			if((chk1==true || chk2==true) && chk3==true) sb.append("  union all \n");
			if(chk3==true){
				sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
				sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
				sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
				sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
				sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
				sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
				sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
				sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");
				sb.append("      ,'EMITIDO'                ESTATUS_MOVIMIENTO						\n");
				sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
				sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
				sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
				sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
				sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
				sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
				sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
				sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
				sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
				sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
				sb.append("      ,cl.cocn_rfc              RFC 										\n");
				sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
				sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
				sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
				sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
				sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
				sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
				sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
				sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
				sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
				sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
				sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
				sb.append("      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
				sb.append("      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 									\n"); 
				sb.append("                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1									\n"); 
				sb.append("                      then c.coce_mt_prima_pura							\n"); 
				sb.append("                else 0 end PRIMA_NETA									\n");
				sb.append("      -- \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
				sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
				sb.append("      ,case when pf.copa_nvalor5 = 0 then \n"); 
				sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
				sb.append("                when pf.copa_nvalor5 = 1 then \n"); 
				sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
				sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n"); 
				sb.append("                else 0 end PRIMA_TOTAL  									 \n");
				sb.append("      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
				sb.append("      -- \n");
				sb.append("      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
				sb.append("      -- \n");      
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo   	\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_VIDA 											\n");
				sb.append("      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
				sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
				sb.append("              from colectivos_coberturas cob 							\n");
				sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
				sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
				sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
				if (poliza1 != 0)
				  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
				sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
				sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
				sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
				sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
				sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
				sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
				sb.append("      ,es.caes_de_estado        ESTADO 									\n");
				sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
				sb.append("      ,cl.cocn_cd_postal        CP 										\n");
				sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
				sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
				sb.append("      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION   \n");
				sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
				sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"); 
				sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n"); 
				sb.append("        from colectivos_coberturas cob                    \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n"); 
				sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n"); 
				sb.append("        from colectivos_coberturas cob                                       \n"); 
				sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n"); 
				sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n"); 
				sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n"); 
				sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"); 
				sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
				sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
				sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n"); 
				sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n"); 
				sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"); 
				sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n"); 
				sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n"); 
				sb.append("           p.copa_vvalor1           ||'|'||                                                 \n"); 
				sb.append("           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"); 
				sb.append("      from colectivos_cliente_certif clc                                                    \n"); 
				sb.append("          ,colectivos_clientes       cln                                                    \n"); 
				sb.append("          ,colectivos_parametros     p                                       \n"); 
				sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n"); 
				sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n"); 
				sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n"); 
				sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n"); 
				sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"); 
				sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"); 
				sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"); 
				sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n"); 
				sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"); 
				sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
				if(ramo == 9 && inIdVenta == 6) {		/*APV*/								
					sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
					sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
					sb.append("  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
					sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
				} else {																		
					sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
				}	 /*APV*/
				sb.append("  from colectivos_certificados   c 										\n");
				sb.append("      ,colectivos_cliente_certif cc 										\n");
				sb.append("      ,colectivos_clientes       cl 										\n");
				sb.append("      ,cart_estados              es 										\n");
				sb.append("      ,alterna_estatus           est 									\n");
				sb.append("      ,colectivos_certificados   head 									\n");
				sb.append("      ,alterna_planes            pl 										\n");
				sb.append("      ,colectivos_parametros     pf										\n");
				sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
				sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
				sb.append(cadenacan);
				sb.append("  and c.coce_nu_certificado   > 0			 							\n");
				//sb.append("  and c.coce_st_certificado   in (" + status + ")  	   				\n");
				//sb.append("  and c.coce_cd_causa_anulacion <> 45			  	     				\n");
				sb.append("  and c.coce_fe_carga    >= to_date('" + fecha1 + "','dd/mm/yyyy')   \n");
				sb.append("  and c.coce_fe_carga    <= to_date('" + fecha2 + "','dd/mm/yyyy')   \n");
				sb.append("  -- \n");
				sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
				sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
				sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
				sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
				sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
				sb.append("  -- \n");
				sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
				sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
				sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
				sb.append("  and head.coce_nu_certificado   = 0 									\n");
				sb.append("  -- \n");
				sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
				sb.append("  -- \n");
				sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
				sb.append("  -- \n");
				sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
				sb.append("  -- \n");
				sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
				sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
				sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
				sb.append("--																		\n");
				sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
			    sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
			    sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
			    sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
			    sb.append("  and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,c.coce_capo_nu_poliza,1,substr(c.coce_capo_nu_poliza,0,3),2,substr(c.coce_capo_nu_poliza,0,2)) \n");
			    sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
			}
			File reporte = new File(rutaTemporal + "ReporteTecnicoMensual.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

		    String sts			= "";
			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);

		    if(chk1==true)
				sts += "VIG";
			if(chk2==true)
				sts += "CAN";
			if(chk3==true)
				sts += "EMI";

		    String nombre = "REPEMI_" + sts + "_" + poliza1 + "_" + fec +  ".csv" ;
		    //nombre += ".csv"; 
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			Log.info(sb.toString());
			query.setText(sb.toString());
			
			System.out.println(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
		
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public void borrarDatosAutosPrecarga() throws Excepciones {

		try {
			
			StringBuilder query = new StringBuilder(100);
			Query qry;
			
			
			// Borramos la precarga
			query.append("delete PreCarga CP \n");
			query.append("where CP.id.copcCdSucursal = 666\n");
			query.append("  and CP.id.copcCdRamo = 666\n");
		    query.append("  and CP.id.copcNumPoliza = 666\n");

		    qry = this.getSession().createQuery(query.toString());// .createSQLQuery(query.toString());
			
			qry.executeUpdate();
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consultaPolizaCancela(Short canal, Short ramo, Long poliza1)
			throws Excepciones {
		try {
			StringBuffer sb = new StringBuffer();
			
			sb.append("select c.coce_casu_cd_sucursal, ");
			sb.append("c.coce_carp_cd_ramo, ");
			sb.append("c.coce_capo_nu_poliza, ");
			sb.append("c.coce_nu_certificado, ");
			sb.append("c.coce_fe_desde, ");
			sb.append("c.coce_fe_hasta, ");
			sb.append("nvl(c.coce_nu_credito, c.coce_nu_cuenta), ");
			sb.append("c.coce_fe_suscripcion ");
			sb.append("from colectivos_certificados c ");
			sb.append("where c.coce_casu_cd_sucursal = " + canal);
			sb.append(" and c.coce_carp_cd_ramo  = " + ramo);
			sb.append(" and c.coce_capo_nu_poliza = " + poliza1);
			sb.append(" and c.coce_st_certificado   in(1,2) ");
			sb.append(" order by c.coce_nu_certificado desc");
          Query query = this.getSession().createSQLQuery(sb.toString());

          List<T> lista = query.list();
			
		  return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public void cancelaCert(CertificadoId id, String fecha1, Integer causa) throws Excepciones {
		
		Long noMovimiento = null;
		DataSource ds = null;
		
		try {
			
			ds = SessionFactoryUtils.getDataSource(this.getSession().getSessionFactory());
			NoMovimiento movimiento = new NoMovimiento(ds);
			noMovimiento = Long.parseLong(((Map)movimiento.execute(id.getCoceCasuCdSucursal(), id.getCoceCarpCdRamo(), id.getCoceCapoNuPoliza(), id.getCoceNuCertificado())).get("returnname").toString());
			
			GeneraMovimiento generaMovimiento = new GeneraMovimiento(ds);
			generaMovimiento.execute(id.getCoceCasuCdSucursal(), id.getCoceCarpCdRamo(), id.getCoceCapoNuPoliza(), id.getCoceNuCertificado());
			
			//System.out.println("DAO: " + noMovimiento + " Certificado: " + id.getCoceNuCertificado());
			//this.getHibernateTemplate().update(id);
			StringBuilder actcertpoliza = new StringBuilder() ;
			actcertpoliza.append("update ");
			actcertpoliza.append("colectivos_certificados c ");
			actcertpoliza.append("set c.coce_nu_movimiento = " + noMovimiento);
			actcertpoliza.append(", c.coce_fe_anulacion_col = '" + fecha1 +"' ");
			actcertpoliza.append(", c.coce_fe_anulacion = trunc(sysdate)");
			actcertpoliza.append(", c.coce_fe_estatus = trunc(sysdate)");
			actcertpoliza.append(", c.coce_cd_causa_anulacion = " + causa);
			actcertpoliza.append(", c.coce_campon2 = 2002");
			actcertpoliza.append(", c.coce_st_certificado = 11");
			actcertpoliza.append(" where c.coce_casu_cd_sucursal = " + id.getCoceCasuCdSucursal());
			actcertpoliza.append(" and c.coce_carp_cd_ramo = " + id.getCoceCarpCdRamo());
			actcertpoliza.append(" and c.coce_capo_nu_poliza = " + id.getCoceCapoNuPoliza());
			actcertpoliza.append(" and c.coce_nu_certificado =" + id.getCoceNuCertificado());
			Query queryActpre = getSession().createSQLQuery(actcertpoliza.toString()); 
			queryActpre.executeUpdate();
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public void conteoCertificados(Certificado certificado) throws Excepciones {
		
		String fecha1 	    = null;
        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        fecha1 = formatter.format(certificado.getCoceFeAnulacionCol());

		Long noMovimiento = null;
		DataSource ds = null;
		
		try {
			StringBuffer sb = new StringBuffer();
			
			sb.append("select * ");
			sb.append("from colectivos_certificados c ");
			sb.append("where c.coce_casu_cd_sucursal = " + certificado.getId().getCoceCasuCdSucursal());
			sb.append(" and c.coce_carp_cd_ramo  = " + certificado.getId().getCoceCarpCdRamo());
			sb.append(" and c.coce_capo_nu_poliza = " + certificado.getId().getCoceCapoNuPoliza());
			sb.append(" and c.coce_st_certificado   in(1,2) ");
			sb.append(" and c.coce_nu_certificado   > 0");
			Query query = this.getSession().createSQLQuery(sb.toString());

			int num = query.executeUpdate();
            //System.out.println("Conteo: " + num);
			if(num==0){
			certificado.getId().setCoceNuCertificado(new Long(0));
			ds = SessionFactoryUtils.getDataSource(this.getSession().getSessionFactory());
			NoMovimiento movimiento = new NoMovimiento(ds);
			noMovimiento = Long.parseLong(((Map)movimiento.execute(certificado.getId().getCoceCasuCdSucursal(), certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza(), certificado.getId().getCoceNuCertificado())).get("returnname").toString());
			
			GeneraMovimiento generaMovimiento = new GeneraMovimiento(ds);
			generaMovimiento.execute(certificado.getId().getCoceCasuCdSucursal(), certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza(), 0);
			
			//System.out.println("DAO: " + noMovimiento + " Certificado: " + id.getCoceNuCertificado());
			//this.getHibernateTemplate().update(id);
			StringBuilder actcertpoliza = new StringBuilder() ;
			actcertpoliza.append("update ");
			actcertpoliza.append("colectivos_certificados c ");
			actcertpoliza.append("set c.coce_nu_movimiento = " + noMovimiento);
			actcertpoliza.append(", c.coce_fe_anulacion_col = '" + fecha1);
			actcertpoliza.append("', c.coce_fe_anulacion = trunc(sysdate)");
			actcertpoliza.append(", c.coce_fe_estatus = trunc(sysdate)");
			actcertpoliza.append(", c.coce_cd_causa_anulacion = " + certificado.getCoceCdCausaAnulacion());
			actcertpoliza.append(", c.coce_campon2 = 2002");
			actcertpoliza.append(", c.coce_st_certificado = 11");
			actcertpoliza.append(" where c.coce_casu_cd_sucursal = " + certificado.getId().getCoceCasuCdSucursal());
			actcertpoliza.append(" and c.coce_carp_cd_ramo = " + certificado.getId().getCoceCarpCdRamo());
			actcertpoliza.append(" and c.coce_capo_nu_poliza = " + certificado.getId().getCoceCapoNuPoliza());
			actcertpoliza.append(" and c.coce_nu_certificado = " + certificado.getId().getCoceNuCertificado());
			Query queryActpre = getSession().createSQLQuery(actcertpoliza.toString()); 
			queryActpre.executeUpdate();
			}
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public void conteoCertificadosMasivo(BeanCertificado certificado) throws Excepciones {
		
		Long noMovimiento = null;
		DataSource ds = null;
		
		try {
			StringBuffer sb = new StringBuffer();
			
			sb.append("select * ");
			sb.append("from colectivos_certificados c ");
			sb.append("where c.coce_casu_cd_sucursal = " + certificado.getId().getCoceCasuCdSucursal());
			sb.append(" and c.coce_carp_cd_ramo  = " + certificado.getId().getCoceCarpCdRamo());
			sb.append(" and c.coce_capo_nu_poliza = " + certificado.getId().getCoceCapoNuPoliza());
			sb.append(" and c.coce_st_certificado   in(1,2) ");
			sb.append(" and c.coce_nu_certificado   > 0");
			Query query = this.getSession().createSQLQuery(sb.toString());

			int num = query.executeUpdate();
            //System.out.println("Conteo: " + num);
			if(num==0){
			certificado.getId().setCoceNuCertificado(new Long(0));
			ds = SessionFactoryUtils.getDataSource(this.getSession().getSessionFactory());
			NoMovimiento movimiento = new NoMovimiento(ds);
			noMovimiento = Long.parseLong(((Map)movimiento.execute(certificado.getId().getCoceCasuCdSucursal(), certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza(), certificado.getId().getCoceNuCertificado())).get("returnname").toString());
			
			GeneraMovimiento generaMovimiento = new GeneraMovimiento(ds);
			generaMovimiento.execute(certificado.getId().getCoceCasuCdSucursal(), certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza(), 0);
			
			//System.out.println("DAO: " + noMovimiento + " Certificado: " + id.getCoceNuCertificado());
			//this.getHibernateTemplate().update(id);
			StringBuilder actcertpoliza = new StringBuilder() ;
			actcertpoliza.append("update ");
			actcertpoliza.append("colectivos_certificados c ");
			actcertpoliza.append("set c.coce_nu_movimiento = " + noMovimiento);
			actcertpoliza.append(", c.coce_fe_anulacion_col = trunc(sysdate)");
			actcertpoliza.append(", c.coce_fe_anulacion = trunc(sysdate)");
			actcertpoliza.append(", c.coce_fe_estatus = trunc(sysdate)");
			actcertpoliza.append(", c.coce_cd_causa_anulacion = " + certificado.getCoceCdCausaAnulacion());
			actcertpoliza.append(", c.coce_campon2 = 2002");
			actcertpoliza.append(", c.coce_st_certificado = 11");
			actcertpoliza.append(" where c.coce_casu_cd_sucursal = " + certificado.getId().getCoceCasuCdSucursal());
			actcertpoliza.append(" and c.coce_carp_cd_ramo = " + certificado.getId().getCoceCarpCdRamo());
			actcertpoliza.append(" and c.coce_capo_nu_poliza = " + certificado.getId().getCoceCapoNuPoliza());
			actcertpoliza.append(" and c.coce_nu_certificado = " + certificado.getId().getCoceNuCertificado());
			Query queryActpre = getSession().createSQLQuery(actcertpoliza.toString()); 
			queryActpre.executeUpdate();
			}
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public String procesoRenovacionPolizas(Short inRamo, Long inPoliza) throws Excepciones {
		System.out.println("--------------------------------------------");
		
		try {
			String resp="";
			Short canal=1;
			System.out.println("Entre a Cert	ificadoDaoHibernate.procesoRenovacionPolizas");
			resp = this.procesosDao.renovacion(canal, inRamo, inPoliza);		
			System.out.println("Salgo de CertificadoDaoHibernate.procesoRenovacionPolizas");
			return resp;
		}catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> consultaPolizasRenovadas() throws Excepciones {
		try {
			StringBuffer sb = new StringBuffer();
			
			sb.append("select a.coce_casu_cd_sucursal,");
			sb.append(" a.coce_carp_cd_ramo,");
			sb.append(" a.coce_capo_nu_poliza,");
			sb.append(" decode(a.coce_st_certificado,1,'VIGENTE',2,'VIGENTE',4,'NO RENOVADO',11, 'CANCELADO', 12, 'TERMINADO'),");
			sb.append(" count(decode(a.coce_st_certificado,1,'VIGENTE',2,'VIGENTE',4,'NO RENOVADO',11, 'CANCELADO', 12, 'TERMINADO')),");
			sb.append(" sum(a.coce_mt_prima_subsecuente)");
			sb.append(" from colectivos_certificados a, colectivos_certificados b");
			sb.append(" where b.coce_casu_cd_sucursal = a.coce_casu_cd_sucursal");
			sb.append(" and b.coce_carp_cd_ramo = a.coce_carp_cd_ramo");
			sb.append(" and b.coce_capo_nu_poliza   = a.coce_capo_nu_poliza");
			sb.append(" and b.coce_nu_certificado   = 0");
			sb.append(" and b.coce_fe_renovacion > trunc(sysdate)-1");
			sb.append(" and a.coce_nu_certificado > 0");
			sb.append(" group by a.coce_casu_cd_sucursal,"); 
			sb.append(" a.coce_carp_cd_ramo,");
			sb.append(" a.coce_capo_nu_poliza,");
			sb.append(" decode(a.coce_st_certificado,1,'VIGENTE',2,'VIGENTE',4,'NO RENOVADO',11, 'CANCELADO', 12, 'TERMINADO')");
			Query query = this.getSession().createSQLQuery(sb.toString());
		
			List<T> lista = query.list();
			return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> detallePolizasRenovadas() throws Excepciones {
		try {
			StringBuffer sb = new StringBuffer();
			

			sb.append("select a.coce_carp_cd_ramo,");
			sb.append(" a.coce_capo_nu_poliza,");
			sb.append(" a.coce_nu_certificado,");
			sb.append(" decode(a.coce_st_certificado,1,'VIGENTE',2,'VIGENTE',4,'NO RENOVADO',11, 'CANCELADO', 12, 'TERMINADO'),");
			sb.append(" d.cocn_apellido_pat,");
			sb.append(" d.cocn_apellido_mat,");
			sb.append(" d.cocn_nombre,");
			sb.append(" a.coce_nu_credito,");
			sb.append(" a.coce_mt_prima_subsecuente,");
			sb.append(" a.coce_mt_suma_asegurada");
			sb.append(" from colectivos_certificados a, colectivos_certificados b, colectivos_cliente_certif c, colectivos_clientes d");
			sb.append(" where c.cocc_casu_cd_sucursal = a.coce_casu_cd_sucursal");
			sb.append(" and c.cocc_carp_cd_ramo = a.coce_carp_cd_ramo");
			sb.append(" and c.cocc_capo_nu_poliza = a.coce_capo_nu_poliza");
			sb.append(" and c.cocc_nu_certificado = a.coce_nu_certificado");
			sb.append(" and d.cocn_nu_cliente = c.cocc_nu_cliente");
			sb.append(" and b.coce_casu_cd_sucursal = a.coce_casu_cd_sucursal");
			sb.append(" and b.coce_carp_cd_ramo = a.coce_carp_cd_ramo");
			sb.append(" and b.coce_capo_nu_poliza   = a.coce_capo_nu_poliza");
			sb.append(" and b.coce_nu_certificado   = 0");
			sb.append(" and b.coce_fe_renovacion > trunc(sysdate)-1");
			sb.append(" and a.coce_nu_certificado > 0");
			Query query = this.getSession().createSQLQuery(sb.toString());
		
			List<T> lista = query.list();
			return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
    }
    @SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String reporteFacturados5758() throws Excepciones {

		JasperReport report = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		StringBuffer sb = new StringBuffer();

		try {
				sb.append("select re.care_carp_cd_ramo ramo" +
						" ,re.care_capo_nu_poliza poliza" +
						" ,re.care_nu_recibo      recibo" +
						" ,re.care_nu_consecutivo_cuota consecutivo" +
						" ,trunc(re.care_fe_emision) fechaemision" +
						" ,re.care_fe_desde desde" +
						" ,re.care_fe_hasta hasta" +
						" ,re.care_mt_prima_pura primapura" +
						" ,re.care_mt_prima      primatotal" +
						" from cart_recibos re" +
						" where re.care_casu_cd_sucursal= 1" +
						" and re.care_carp_cd_ramo      in (57,58)" +
						" and trunc(re.care_fe_emision) = trunc(sysdate)" +
						" order by 1,5,2");

			File reporte = new File(rutaTemporal + "ReporteFacturados5758.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);

		    String nombre = "REPFAC_5758_" +  fec +  ".csv" ;
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			System.out.println(sb.toString());
			query.setText(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public String reporteErrores5758() throws Excepciones {

		JasperReport report = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		StringBuffer sb = new StringBuffer();

		try {
				sb.append("select ca.cocr_cd_sucursal canal" +
						  " ,ca.cocr_cd_ramo     ramo" +
						  " ,ca.cocr_num_poliza  poliza" +
						  " ,ca.cocr_fe_carga    fecha_proceso" +
						  " ,ca.cocr_registro    motivo_no_facturacion" +
						  " from colectivos_carga ca" +
						  " where ca.cocr_cd_sucursal = 1" +
						  " and ca.cocr_cd_ramo in (57,58)" +
						  " and ca.cocr_registro   is not null");

			File reporte = new File(rutaTemporal + "ReporteErrores5758.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);

		    String nombre = "REP_ERRORES_5758_" +  fec +  ".csv" ;
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 1024, 1024);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			System.out.println(sb.toString());
			query.setText(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}	

	public void borraErrores5758() {
		StringBuilder suprerr5758 = new StringBuilder() ;
		
		suprerr5758.append("delete from colectivos_carga ca" +
						 " where ca.cocr_cd_sucursal = 1" +
						 " and ca.cocr_cd_ramo in (57,58)");
		Query suprerr = getSession().createSQLQuery(suprerr5758.toString()); 
		suprerr.executeUpdate();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String renovar5758(String rmes, String anio) throws Excepciones {
		
		Integer polizas = 0;
		StringBuffer renueva = new StringBuffer();
		Query suprerr;
		
		// POLIZAS HEADER
		renueva.append("update /*+BYPASS_UJVC*/ ( \n");
		renueva.append("select trunc(sysdate)                 Fecha \n");
		renueva.append("      ,cer.cace_carp_cd_ramo          Ramo \n");
		renueva.append("      ,cer.cace_capo_nu_poliza        Poliza \n");
		renueva.append("      ,x.Certificados Total_Asegurados_Vigentes \n");
		renueva.append("      ,'VIGENTE' ESTATUS \n");
		renueva.append("      ,cer.cace_cair_in_causa_renovacion Anio_Poliza \n");
		renueva.append("      ,nvl(cer.cace_cair_in_causa_renovacion,1)+1 Nuevo_Anio_Poliza# \n");
		renueva.append("      ,cer.cace_fe_desde Desde        --actual \n");
		renueva.append("      ,cer.cace_fe_hasta Hasta \n");
		renueva.append("      ,cer.cace_fe_proxima_facturacion   fe_efectiva \n");
		renueva.append("      ,add_months(cer.cace_fe_desde,12) Nueva_Vigencia_Desde#   --Nueva Vig. \n");
		renueva.append("      ,add_months(cer.cace_fe_hasta,12) Nueva_Vigencia_Hasta# \n");
		renueva.append("      ,cer.cace_fe_suscripcion FechaEmision \n");
		renueva.append("      ,cer.cace_mt_prima_anual Prima_Anual \n");
		renueva.append("      ,cer.cace_in_facturacion  in_facturacion \n");
		renueva.append("      ,cer.cace_mt_suma_asegurada SumaAsegurada \n");
		renueva.append("      ,replace(cl.cacn_nm_apellido_razon,'//','') Contratante \n");
		renueva.append("      ,cer.cace_cazb_cd_sucursal Sucursal \n");
		renueva.append("      ,x.conc  \n");
		renueva.append("from ( \n");
		renueva.append("  select cer.cace_carp_cd_ramo ramo,cer.cace_capo_nu_poliza pol, rec.care_nu_consecutivo_cuota conc \n");
		renueva.append("       , count(1) Certificados \n");
		renueva.append("    from Cart_Certificados cer \n");
		renueva.append("        ,Cart_Certificados h \n");
		renueva.append("        ,Cart_Recibos      rec \n");
		renueva.append("   where cer.cace_casu_cd_sucursal = 1 \n");
		renueva.append("     and cer.cace_carp_cd_ramo     in (57,58) \n");
		renueva.append("     and cer.cace_capo_nu_poliza   > 0 \n");
		renueva.append("     and cer.cace_nu_certificado   > 0 \n");
		renueva.append("	 and cer.cace_st_certificado   = 1 \n");
		renueva.append("     -- \n");
		renueva.append("     and h.cace_casu_cd_sucursal   = cer.cace_casu_cd_sucursal \n");
		renueva.append("     and h.cace_carp_cd_ramo       = cer.cace_carp_cd_ramo \n");
		renueva.append("     and h.cace_capo_nu_poliza     = cer.cace_capo_nu_poliza \n");
		renueva.append("     and h.cace_nu_certificado     = 0 \n");
		renueva.append("     and h.cace_st_certificado     = 1 \n");
		renueva.append("     -- \n");
		renueva.append("     and to_char(h.cace_fe_suscripcion,'mm') = '"+ rmes + "' \n");
		renueva.append("     and to_char(h.cace_fe_suscripcion,'yyyy') <> '"+ anio + "' \n");
		renueva.append("     and to_char(h.cace_fe_hasta,'yyyy')     = '" + anio + "' \n");
		renueva.append("     --and to_number(to_char(h.cace_fe_suscripcion,'dd')) < to_number(to_char(sysdate,'dd')) \n");
		renueva.append("     -- \n");
		renueva.append("     and rec.care_casu_cd_sucursal = cer.cace_casu_cd_sucursal \n");
		renueva.append("     and rec.care_carp_cd_ramo     = cer.cace_carp_cd_ramo \n");
		renueva.append("     and rec.care_capo_nu_poliza   = cer.cace_capo_nu_poliza \n");
		renueva.append("     and rec.care_fe_hasta         = (select MAX(maxrec.care_fe_hasta) \n");
		renueva.append("                                        from cart_recibos maxrec \n");
		renueva.append("                                       where maxrec.care_casu_cd_sucursal = cer.cace_casu_cd_sucursal \n");
		renueva.append("                                         and maxrec.care_carp_cd_ramo     = cer.cace_carp_cd_ramo \n");
		renueva.append("                                         and maxrec.care_capo_nu_poliza   = cer.cace_capo_nu_poliza \n");
		renueva.append("                                         and maxrec.care_fe_emision       > sysdate - 60) \n");
		renueva.append("     and rec.care_nu_consecutivo_cuota = 12 \n");
		renueva.append("      \n");
		renueva.append("      \n");
		renueva.append("   group by cer.cace_carp_cd_ramo,cer.cace_capo_nu_poliza, rec.care_nu_consecutivo_cuota \n");
		renueva.append("  having count(1) > 0 \n");
		renueva.append("  )  x \n");
		renueva.append("  ,cart_certificados cer \n");
		renueva.append("  ,cart_clientes cl \n");
		renueva.append("  ,cart_polizas pol \n");
		renueva.append("where cer.cace_casu_cd_sucursal  = 1 \n");
		renueva.append("  and cer.cace_carp_cd_ramo      = x.ramo \n");
		renueva.append("  and cer.cace_capo_nu_poliza    = x.pol \n");
		renueva.append("  and cer.cace_nu_certificado    = 0 \n");
		renueva.append("  and cer.cace_st_certificado    = 1 \n");
		renueva.append("  -- \n");
		renueva.append("  and pol.capo_casu_cd_sucursal = cer.cace_casu_cd_sucursal \n");
		renueva.append("  and pol.capo_carp_cd_ramo     = cer.cace_carp_cd_ramo \n");
		renueva.append("  and pol.capo_nu_poliza        = cer.cace_capo_nu_poliza \n");
		renueva.append("  -- \n");
		renueva.append("  and cl.cacn_cd_nacionalidad   = pol.capo_cacn_cd_nacionalidad \n");
		renueva.append("  and cl.cacn_nu_cedula_rif     = pol.capo_cacn_nu_cedula_rif \n");
		renueva.append("  )re \n");
		renueva.append("set  re.Anio_Poliza = re.Nuevo_Anio_Poliza# \n");
		renueva.append("    ,re.Desde       = re.Nueva_Vigencia_Desde# \n");
		renueva.append("    ,re.Hasta       = re.Nueva_Vigencia_Hasta# \n");
		renueva.append("    ,re.fe_efectiva = re.Nueva_Vigencia_Hasta# \n");
		renueva.append("    ,re.in_facturacion = 'S'" );

		suprerr = getSession().createSQLQuery(renueva.toString()); 
		polizas = suprerr.executeUpdate();
		
		renueva = null;
		renueva = new StringBuffer();
							  
		if (polizas > 0) {    
			// CERTIFICADOS HIJOS			  
			renueva.append("update /*+BYPASS_UJVC*/ \n");
			renueva.append("( select  cer.cace_carp_cd_ramo Ramo \n");
			renueva.append("         ,cer.cace_capo_nu_poliza Poliza \n");
			renueva.append("         ,cer.cace_cair_in_causa_renovacion Anio_Poliza \n");
			renueva.append("        ,nvl(cer.cace_cair_in_causa_renovacion,1)+1 Nuevo_Anio_Poliza# \n");
			renueva.append("         ,cer.cace_fe_desde Desde \n");
			renueva.append("         ,cer.cace_fe_hasta Hasta \n");
			renueva.append("         ,cer.cace_in_facturacion  in_facturacion \n");
			renueva.append("         ,cer.cace_fe_proxima_facturacion  fe_efectiva \n");
			renueva.append("         ,h.cace_fe_desde DesdePol \n");
			renueva.append("         ,h.cace_fe_hasta HastaPol \n");
			renueva.append("    from Cart_Certificados cer \n");
			renueva.append("        ,Cart_Certificados h \n");
			renueva.append("        ,Cart_Recibos      rec \n");
			renueva.append("   where cer.cace_casu_cd_sucursal = 1 \n");
			renueva.append("     and cer.cace_carp_cd_ramo     in (57,58) \n");
			renueva.append("     and cer.cace_capo_nu_poliza   > 0 \n");
			renueva.append("     and cer.cace_nu_certificado   > 0 \n");
			renueva.append("     and cer.cace_st_certificado   = 1 \n");
			renueva.append("     -- \n");
			renueva.append("     and h.cace_casu_cd_sucursal   = cer.cace_casu_cd_sucursal \n");
			renueva.append("     and h.cace_carp_cd_ramo       = cer.cace_carp_cd_ramo \n");
			renueva.append("     and h.cace_capo_nu_poliza     = cer.cace_capo_nu_poliza \n");
			renueva.append("     and h.cace_nu_certificado     = 0 \n");
			renueva.append("     and h.cace_st_certificado     = 1 \n");
			renueva.append("     -- \n");
			renueva.append("     and to_char(h.cace_fe_suscripcion,'mm') = '"+ rmes + "' \n");
			renueva.append("     and to_char(h.cace_fe_suscripcion,'yyyy') <> '"+ anio + "' \n");
			renueva.append("     -- \n");
			renueva.append("     and rec.care_casu_cd_sucursal = h.cace_casu_cd_sucursal \n");
			renueva.append("     and rec.care_carp_cd_ramo     = h.cace_carp_cd_ramo \n");
			renueva.append("     and rec.care_capo_nu_poliza   = h.cace_capo_nu_poliza \n");
			renueva.append("     and rec.care_fe_hasta         = (select MAX(maxrec.care_fe_hasta) \n");
			renueva.append("                                        from cart_recibos maxrec \n");
			renueva.append("                                       where maxrec.care_casu_cd_sucursal = h.cace_casu_cd_sucursal \n");
			renueva.append("                                         and maxrec.care_carp_cd_ramo     = h.cace_carp_cd_ramo \n");
			renueva.append("                                         and maxrec.care_capo_nu_poliza   = h.cace_capo_nu_poliza \n");
			renueva.append("                                         and maxrec.care_fe_emision       > sysdate - 60) \n");
			renueva.append("     and rec.care_nu_consecutivo_cuota = 12 \n");
			renueva.append("   ) cert \n");
			renueva.append(" set cert.Anio_Poliza = cert.Nuevo_Anio_Poliza# \n");
			renueva.append("    ,cert.Desde       = cert.DesdePol \n");
			renueva.append("    ,cert.Hasta       = cert.HastaPol \n");
			renueva.append("    ,cert.fe_efectiva = cert.HastaPol \n");
			renueva.append("    ,cert.in_facturacion = 'S' \n");
			
			suprerr = getSession().createSQLQuery(renueva.toString()); 
			suprerr.executeUpdate();
		}
		JasperReport report = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
	

		try {
			
			String reporte = rutaTemporal + "ReporteRenovacion5758.jrxml";

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);

		    String nombre = "REPREN_5758_" +  fec +  ".csv" ;
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    parametros.put("mes",rmes);
		    parametros.put("anio",anio);
		    

			report = JasperCompileManager.compileReport(reporte);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String generaLayoutRamo82(Short ramo) throws Excepciones {

		JasperReport report = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		StringBuffer sb = new StringBuffer();
		

		try {
				sb.append("select substr(pc.copc_cuenta,10,11) identificador," +
						  " substr(pc.copc_cuenta,10,11) cuenta," +
						  " pc.copc_num_cliente numerocliente," +
						  " '0'                 sucursalbanco," +
						  " pc.copc_ap_paterno apellidopaterno," +
						  " pc.copc_ap_materno apellidomaterno," +
						  " pc.copc_nombre     nombre," +
						  " decode(pc.copc_sexo, 'M', 'MA', 'F', 'FE') sexo," +
						  " substr(pc.copc_rfc,9,2)||'/'||substr(pc.copc_rfc,7,2)||'/19'||substr(pc.copc_rfc,5,2) fechanacimiento," +
						  " pc.copc_rfc             rfc," +
						  " pc.copc_calle           calle," +
						  " (select distinct(ei.caiv_caes_cd_estado)" +
						  "  from cart_estados_iva ei" +
						  "  where ei.caiv_cp_poblacion = pc.copc_cp) estado," +
						  "  pc.copc_delmunic        delgmun," +
						  "  pc.copc_cp              codigopostal," +
						  "  pc.copc_colonia         colonia," +
						  "  pc.copc_telefono        telefono," +
						  "  '0'                     producto," +
						  "  '0'                     subproducto," +
						  "  substr(pc.copc_dato33,1,2)||'/'||substr(pc.copc_dato33,4,2)||'/'||substr(pc.copc_dato33,7,2) fechaingreso," +
						  "  pc.copc_dato22          contratoenlace," +
						  "  pc.copc_dato25          bucempresa," +
						  "  '1'                     tiposeguro," +
						  "  pc.copc_tipo_cliente    tipocliente," +
						  "  ''                      sumaasegurada," +
						  "  ''                      fechafin," +
						  "  ''                      prima," +
						  "  ''                      remesa" +    						  
						  " from" +
						  " colectivos_precarga pc" +
						  " where pc.copc_cd_sucursal   = 1" +
						  " and pc.copc_cd_ramo       = " + ramo +
						  " and pc.copc_num_poliza    = 999");

			File reporte = new File(rutaTemporal + "LayoutSAFE.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);

		    String nombre = "layout_safe_" +  fec +  ".csv" ;
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 1024, 1024);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			System.out.println(sb.toString());
			query.setText(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
			
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String generaLayoutRamo5(Short ramo) throws Excepciones {
		System.out.println("----------------------------------------------------------------");
		JasperReport report = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		StringBuffer sb = new StringBuffer();

		try {
				sb.append("select substr(pc.copc_cuenta,10,11) identificador," +
						  " substr(pc.copc_cuenta,10,11) cuenta," +
						  " substr(pc.copc_cuenta,10,11) prestamo," +
						  " pc.copc_ap_paterno apellidopaterno," +
						  " pc.copc_ap_materno apellidomaterno," +
						  " pc.copc_nombre     nombre," +
						  " substr(pc.copc_rfc,9,2)||'/'||substr(pc.copc_rfc,7,2)||'/19'||substr(pc.copc_rfc,5,2) fechanacimiento," +
						  " decode(pc.copc_sexo, 'M', 'MA', 'F', 'FE') sexo," +
						  " pc.copc_num_cliente numerocliente," +
						  " pc.copc_rfc             rfc," +
						  " pc.copc_calle           calle," +
						  " pc.copc_colonia         colonia," +
						  " pc.copc_delmunic        delgmun," +
						  " (select distinct(ei.caiv_caes_cd_estado)" +
						  " from cart_estados_iva ei" +
						  " where ei.caiv_cp_poblacion = pc.copc_cp) estado," +
						  " pc.copc_cp              codigopostal," +
						  " pc.copc_telefono        telefono," +
						  " pc.copc_tipo_cliente  tipocliente," +
						  " substr(pc.copc_dato33,1,2)||'/'||substr(pc.copc_dato33,4,2)||'/'||substr(pc.copc_dato33,7,2) fechaingreso," +
						  "  (pa.copa_nvalor8*(pa.copa_nvalor6/pa.copa_nvalor7))/1000 prima," +
						  " pa.copa_nvalor8 sumaasegurada," +
						  " pa.copa_vvalor1 producto," +
						  " pa.copa_vvalor2 subproducto," +
						  " pa.copa_nvalor6/pa.copa_nvalor7 tarifa," +
						  " pc.copc_dato22          contratoenlace," +
						  " pc.copc_dato25          bucempresa," +
						  " 1                       tiposeguro" +
						  " from" +
						  " colectivos_precarga pc" +
						  " ,colectivos_parametros pa" +
						  " where pc.copc_cd_sucursal = pa.copa_nvalor1" +
						  " and pc.copc_cd_ramo = pa.copa_nvalor2" +
						  " and pc.copc_cd_sucursal   = 1" +
						  " and pc.copc_cd_ramo       = " + ramo +
						  " and pc.copc_num_poliza    = 999" +
						  " and pa.copa_des_parametro = 'LAYOUTS'" +
						  " and pa.copa_id_parametro  = 'GENERACION'");

			File reporte = new File(rutaTemporal + "LayoutACC.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);

		    String nombre = "layout_acc_" +  fec +  ".csv" ;		    
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 1024, 1024);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			System.out.println(sb.toString());
			query.setText(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public String generaLayoutRamo65(Short ramo) throws Excepciones {

		JasperReport report = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		StringBuffer sb = new StringBuffer();

		try {
				sb.append("select substr(pc.copc_cuenta,10,11) identificador," +
						  " substr(pc.copc_cuenta,10,11) cuenta," +
						  " pc.copc_ap_paterno apellidopaterno," +
						  " pc.copc_ap_materno apellidomaterno," +
						  " pc.copc_nombre     nombre," +
						  " substr(pc.copc_rfc,9,2)||'/'||substr(pc.copc_rfc,7,2)||'/19'||substr(pc.copc_rfc,5,2) fechanacimiento," +
						  " decode(pc.copc_sexo, 'M', 'MA', 'F', 'FE') sexo," +
						  " pc.copc_num_cliente numerocliente," +
						  " pc.copc_rfc             rfc," +
						  " pc.copc_calle           calle," +
						  " pc.copc_colonia         colonia," +
						  " pc.copc_delmunic        delgmun," +
						  " (select distinct(ei.caiv_caes_cd_estado)" +
						  " from cart_estados_iva ei" +
						  " where ei.caiv_cp_poblacion = pc.copc_cp) estado," +
						  " pc.copc_cp              codigopostal," +
						  " pc.copc_telefono        telefono," +
						  " pc.copc_tipo_cliente  tipocliente," +
						  " substr(pc.copc_dato33,1,2)||'/'||substr(pc.copc_dato33,4,2)||'/'||substr(pc.copc_dato33,7,2) fechaingreso," +
						  " 0                     fechafin," +
						  " 0                     prima," +
						  " pc.copc_suma_asegurada sumaasegurada," +
						  " 0                     producto," +
						  " 0                     subproducto," +
						  " 0                     tarifa," +
						  " pc.copc_dato22          contratoenlace," +
						  " pc.copc_dato25          bucempresa," +
						  " 1                       tiposeguro" +
						  " from" +
						  " colectivos_precarga pc" +
						  " where pc.copc_cd_sucursal   = 1" +
						  " and pc.copc_cd_ramo       = " + ramo +
						  " and pc.copc_num_poliza    = 999");

			File reporte = new File(rutaTemporal + "LayoutVIDA.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);

		    String nombre = "layout_vida_" +  fec +  ".csv" ;
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 1024, 1024);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			System.out.println(sb.toString());
			query.setText(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}	
	public void borraErrores56582(Short ramo) {
		
		System.out.println(ramo);
		StringBuilder suprerr56582 = new StringBuilder() ;
		
		suprerr56582.append("delete colectivos_precarga pc" +
							" where pc.copc_cd_sucursal = 1" +
							" and pc.copc_cd_ramo = " + ramo +
							" and pc.copc_num_poliza  = 999");
		Query suprerr = getSession().createSQLQuery(suprerr56582.toString()); 
		suprerr.executeUpdate();
	}
    
	@SuppressWarnings("deprecation")
	public String generaReporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa, String contratoEnlace, String folioEndoso) throws Excepciones {
		
		JasperReport report = null;
		JasperPrint print = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		Map<String, Object> parametros;
		String archivo = "";
		
		try {
			
			parametros = new HashMap<String, Object>();
			parametros.put("fechaInicial", fechaInicial);
			parametros.put("fechaFinal", fechaFinal);
			parametros.put("empresa", empresa);
			parametros.put("contratoEnlace", contratoEnlace);
			parametros.put("folioEndoso", folioEndoso);
			
			report = JasperCompileManager.compileReport(rutaTemporal + "reporteEndosoUpgrade.jrxml");
			
			
			print = JasperFillManager.fillReport(report, parametros, getSession().connection());
		
			archivo = "reporteEndosoUpgrade.pdf";
			
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + archivo);
			exporter.setParameter(JRPdfExporterParameter.METADATA_AUTHOR, "Seguros Santander");
			
			exporter.exportReport();
			
		} catch (Exception e) {			
			logger.error(e.getMessage());
		}		
		
		return archivo;
	}
	
	@SuppressWarnings("deprecation")
	public String generaSoporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa, String contratoEnlace, String folioEndoso) throws Excepciones {
		
		JasperReport report = null;
		JasperPrint print = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		Map<String, Object> parametros;
		String archivo = "";
		
		try {
			
			parametros = new HashMap<String, Object>();
			parametros.put("fechaInicial", fechaInicial);
			parametros.put("fechaFinal", fechaFinal);
			parametros.put("empresa", empresa);
			parametros.put("contratoEnlace", contratoEnlace);
			parametros.put("folioEndoso", folioEndoso);
			
			
			report = JasperCompileManager.compileReport(rutaTemporal + "detalleEndosoUpgrade.jrxml");
			
			
			print = JasperFillManager.fillReport(report, parametros, getSession().connection());
		
			archivo = "detalleEndosoUpgrade.csv";
			
			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + archivo);
			//exporter.setParameter(JRPdfExporterParameter.METADATA_AUTHOR, "Seguros Santander");
			
			exporter.exportReport();
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}		
		
		return archivo;
	}

	public String consultaEmisionDesempleo(String fecha1, String fecha2)
			throws Excepciones {
		JasperReport report = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		StringBuilder sb = new StringBuilder();
		
		try {
				
				sb.append("select substr(C.COCE_CAMPOV6, instr(C.COCE_CAMPOV6,'Folio:',1,1) + 6,instr(C.COCE_CAMPOV6, '|',1,3)-  instr(C.COCE_CAMPOV6,'Folio:',1) - 6) foliotlmk \n");
				sb.append("      ,C.COCE_FE_SUSCRIPCION fechaventa	 		\n");
				sb.append("      ,C.COCE_CASU_CD_SUCURSAL canal				\n");
				sb.append("      ,C.COCE_CARP_CD_RAMO ramo					\n");
				sb.append("      ,C.COCE_CAPO_NU_POLIZA poliza 				\n");
				sb.append("      ,C.COCE_NU_CERTIFICADO certificado			\n");
				sb.append("      ,C.COCE_NU_CREDITO NoTarjeta				\n");
				sb.append("      ,CL.COCN_APELLIDO_PAT AseguradoAP			\n");
				sb.append("      ,CL.COCN_APELLIDO_MAT AseguradoAM			\n");
				sb.append("      ,CL.COCN_NOMBRE AseguradoNombre			\n");
				sb.append("      ,C.COCE_MT_SUMA_ASEGURADA sumaAsegurada	\n");
				sb.append("      ,nvl(C.COCE_FR_PAGO, 'M') frPago			\n");
				sb.append("      ,C.COCE_MT_PRIMA_SUBSECUENTE prima			\n");
				sb.append("      ,'' CausaREchazo							\n");
				sb.append("      ,'' VariacionPrima							\n");
				sb.append("from colectivos_certificados C					\n");
				sb.append("     inner join colectivos_cliente_certif CC on	\n");
				sb.append("           CC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL	\n");
				sb.append("       and CC.COCC_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO			\n");
				sb.append("       and CC.COCC_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA		\n");
				sb.append("       and CC.COCC_NU_CERTIFICADO = C.COCE_NU_CERTIFICADO		\n");
				sb.append("     inner join colectivos_clientes CL on						\n");
				sb.append("           CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE				\n");
				sb.append("where C.COCE_CASU_CD_SUCURSAL = 1								\n");
				sb.append("  and C.COCE_CARP_CD_RAMO = 9									\n");
				sb.append("  and C.COCE_CAPO_NU_POLIZA = 10911								\n");
				sb.append("  and C.COCE_NU_CERTIFICADO > 0									\n");
				sb.append("  and C.COCE_FE_CARGA >= to_date('" + fecha1 + "','dd/mm/yyyy')  \n");
				sb.append("  and C.COCE_FE_CARGA <= to_date('" + fecha2 + "','dd/mm/yyyy') 	\n");
				sb.append("union all														\n");
				sb.append("select VP.VTAP_ID_CERTIFICADO foliotlmk							\n");
				sb.append("      ,to_date(VP.VTAP_FE_INGRESO, 'dd/MM/yyyy')					\n");
				sb.append("      ,VP.VTAP_CD_SUCURSAL										\n");
				sb.append("      ,VP.VTAP_CD_RAMO											\n");
				sb.append("      ,VP.VTAP_NUM_POLIZA										\n");
				sb.append("  	 ,0 														\n");
				sb.append("      ,VP.VTAP_CUENTA											\n");
				sb.append("      ,VP.VTAP_AP_PATERNO										\n");
				sb.append("      ,VP.VTAP_AP_MATERNO										\n");
				sb.append("      ,VP.VTAP_NOMBRE 											\n");
				sb.append("      ,to_number(VP.VTAP_SUMA_ASEGURADA)							\n");
				sb.append("      ,'M'														\n");
				sb.append("      ,to_number(VP.VTAP_PRIMA)									\n");
				sb.append("      ,VP.VTAP_DES_ERROR											\n");
				sb.append("		,''															\n");
				sb.append("from vta_precarga VP												\n");
				sb.append("where VP.VTAP_SISTEMA_ORIGEN = 'DES_TDC'							\n");
				sb.append("  and to_date(VP.VTAP_FE_CARGA, 'dd/MM/yyyy') >=  to_date('" + fecha1 + "','dd/mm/yyyy')	\n");
				sb.append("  and to_date(VP.VTAP_FE_CARGA, 'dd/MM/yyyy') <=  to_date('" + fecha2 + "','dd/mm/yyyy')	\n");
				
			File reporte = new File(rutaTemporal + "ReporteEmisionDesempleo.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

		    String sts			= "";
			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);	 

		    String nombre = "REPEMI_" + sts + "_" + "_" + fec +  ".csv" ;
		    
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			Log.info(sb.toString());
			query.setText(sb.toString());
			
			//System.out.println(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public String generaObligados(short canal, Short ramo, Long poliza,
			Long poliza1, Integer inIdVenta, boolean chk1, boolean chk2,
			boolean chk3, String rutaTemporal) throws Excepciones {
		JasperReport report = null;
		StringBuilder sb = new StringBuilder();
		
		try {
				
			sb.append("select  c.coce_casu_cd_sucursal CANAL                                                        \n");
			sb.append("       ,c.coce_carp_cd_ramo     RAMO                                                         \n");
			sb.append("       ,c.coce_capo_nu_poliza   POLIZA                                                       \n");
			sb.append("       ,c.coce_nu_certificado   CERTIFICADO                                                  \n");
			sb.append("       ,decode(c.coce_sub_campana,'7',c.coce_buc_empresa,'') FOLIO_CH                        \n");
			sb.append("       ,c.coce_nu_cuenta        CUENTA                                                       \n");
			sb.append("       ,clc.cocc_id_certificado CREDITO                                                      \n");
			sb.append("       ,cl.cocn_nombre          NOMBRE                                                       \n");
			sb.append("       ,cl.cocn_apellido_pat    APELLIDO_PATERNO                                             \n");
			sb.append("       ,cl.cocn_apellido_mat    APELLIDO_MATERNO                                             \n");
			sb.append("       ,clc.cocc_tp_cliente     CODIGO_TIPO                                                  \n");
			sb.append("       ,p.copa_vvalor1          TIPO_CLIENTE                                                 \n");
			sb.append("       ,cl.cocn_rfc             RFC                                                          \n");
			sb.append("       ,cl.cocn_fe_nacimiento   FECHA_NACIMIENTO                                             \n");
			sb.append("       ,cl.cocn_cd_sexo         SEXO                                                         \n");
			sb.append("       ,substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) PARTICIPA    \n");
			sb.append("       ,(select sum(cb.cocb_ta_riesgo)  														\n");
			sb.append("           from colectivos_coberturas cb														\n");
			sb.append("          where cb.cocb_casu_cd_sucursal = c.coce_casu_cd_sucursal							\n");
			sb.append("            and cb.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo								\n");
			sb.append("            and cb.cocb_cer_nu_cobertura = c.coce_nu_cobertura								\n");
			sb.append("            and cb.cocb_capu_cd_producto = c.coce_capu_cd_producto							\n");
			sb.append("            and cb.cocb_capb_cd_plan     = c.coce_capb_cd_plan) TARIFA						\n");
			sb.append("       ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
			sb.append("       ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 											  \n");
			sb.append("       ,c.coce_mt_prima_subsecuente PRIMA													\n");
			sb.append("  from colectivos_certificados    c                                                          \n");
			sb.append("       ,colectivos_cliente_certif clc                                                        \n");
			sb.append("       ,colectivos_clientes       cl                                                         \n");
			sb.append("       ,colectivos_parametros     p                                                          \n");
			sb.append(" where c.coce_casu_cd_sucursal       = 1                                                     \n");
			sb.append("   and c.coce_carp_cd_ramo           = " + ramo + "                                          \n");
			
			if (poliza != 0)
				sb.append("   and c.coce_capo_nu_poliza         = " + poliza + "                                    \n");
			else 
				sb.append("   and c.coce_capo_nu_poliza         > 0                                                 \n");
			
			if (inIdVenta > 0)
				sb.append("   and c.coce_sub_campana            = '" + inIdVenta + "'                               \n");
			
			sb.append("   and c.coce_nu_certificado         > 0                                                     \n");
			
			if (!chk3 && !(chk1 && chk2)){
				if (chk1)
					sb.append("   and c.coce_st_certificado         in (1,2)                                        \n");
				else 
					sb.append("   and c.coce_st_certificado         not in (1,2)                                    \n");
			}
			
			sb.append("   and clc.cocc_casu_cd_sucursal     = c.coce_casu_cd_sucursal                               \n"); 
			sb.append("   and clc.cocc_carp_cd_ramo         = c.coce_carp_cd_ramo                                   \n");
			sb.append("   and clc.cocc_capo_nu_poliza       = c.coce_capo_nu_poliza                                 \n");
			sb.append("   and clc.cocc_nu_certificado       = c.coce_nu_certificado                                 \n");
			sb.append("   and nvl(clc.cocc_tp_cliente,1)    > 1                                                     \n");
			sb.append("   and cl.cocn_nu_cliente            = clc.cocc_nu_cliente                                   \n");
			sb.append("   and p.copa_des_parametro(+)       = 'ASEGURADO'                                           \n");
			sb.append("   and p.copa_id_parametro(+)        = 'TIPO'                                                \n");
			sb.append("   and p.copa_nvalor1(+)             = clc.cocc_tp_cliente                                   \n");
			sb.append("order by 1,2,3,4                                                                             \n");

				
			File reporte = new File(rutaTemporal + "ReporteObligados.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

		    String sts			= "";
			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);	 

		    String nombre = "´REPORTE_OBLIGADOS_" + fec +  ".csv" ;
		    
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			Log.info(sb.toString());
			query.setText(sb.toString());
			
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager.fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	public String generaDevoluciones(short canal, Short ramo, Long poliza1, Long poliza2,  String fecha1, String fecha2, Integer inIdVenta, String reporte) throws Excepciones {
		StringBuffer sb = new StringBuffer();
		StringBuilder filtro = new StringBuilder();
		List<Parametros> listaPolizas;
		Integer inicioPoliza = 0;
		String cadenacan = null;
		int pu = 0;
		String rutaTemporal = reporte;
		JasperReport report = null;
		File objReporte;
	    JasperDesign jasperDesign = null;
	    JRSwapFile swapFile = null;
	    JRSwapFileVirtualizer virtualizer = null;
	    JRDesignQuery query;
		JasperPrint print;
	    JRCsvExporter exporter;
	    
		try {
			if(poliza1 == 0) {
				filtro.append("  and P.copaDesParametro = 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro  = 'GEP' \n");
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor6 = ").append(inIdVenta).append("\n");
			    
				listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0) {
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				} else { 
					inicioPoliza = -1;
				}
				
				cadenacan = "  AND C.COCE_CAPO_NU_POLIZA >= " + inicioPoliza + "00000000 AND C.COCE_CAPO_NU_POLIZA <= " + inicioPoliza + "99999999 \n"
					+ "  AND C.COCE_SUB_CAMPANA = '" + inIdVenta + "' 				        \n";
			} else {
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza1).append(", 1, substr(").append(poliza1).append(", 0, 3), substr(").append(poliza1).append(", 0, 2)) \n");
				filtro.append("order by P.copaNvalor3");
				
				listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				for(Parametros item: listaPolizas) {
					if(item.getCopaNvalor4().toString().equals("1")) {
						pu = 1;
						if(poliza2 == 0) {
							cadenacan="  AND C.COCE_CAPO_NU_POLIZA like '" + poliza1 + "%'";
						} else {
							cadenacan="  AND C.COCE_CAPO_NU_POLIZA like '" + poliza2 + "%'";
						}
					} else {
						pu = 0;
						cadenacan="  AND C.COCE_CAPO_NU_POLIZA = " + poliza1;
					}
				}
			}
			
			sb.append("SELECT C.COCE_CARP_CD_RAMO RAMO							\n");
			sb.append("      ,C.COCE_CAPO_NU_POLIZA POLIZA						\n");
			sb.append("      ,CC.COCC_ID_CERTIFICADO CREDITO					\n");
			sb.append("      ,ROUND(C.COCE_MT_PRIMA_PURA * 1000 / DECODE(C.COCE_SUB_CAMPANA, '7', DECODE(C.COCE_MT_SUMA_ASEGURADA, 0, 1, C.COCE_MT_SUMA_ASEGURADA), DECODE(C.COCE_MT_SUMA_ASEG_SI, 0, 1, C.COCE_MT_SUMA_ASEG_SI)), 2) TARIFA	\n");
			sb.append("      ,C.COCE_FE_SUSCRIPCION FECHA_INGRESO				\n");
			sb.append("      ,C.COCE_CD_PLAZO PLAZO								\n");
			sb.append("      ,C.COCE_FE_INI_CREDITO FECHA_INICIO_CREDITO		\n");
			sb.append("      ,C.COCE_FE_FIN_CREDITO FECHA_FIN_CREDITO			\n");
			sb.append("      ,TRIM(TO_CHAR(DECODE(C.COCE_SUB_CAMPANA, '7', TO_NUMBER(C.COCE_MT_SUMA_ASEG_SI), C.COCE_MT_SUMA_ASEGURADA), '999999999.99')) SUMA_ASEGURADA	\n");
			sb.append("      ,CASE WHEN PF.COPA_NVALOR5 = 0 THEN				\n"); 
			sb.append("      	NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 1) - INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) - 1)), 0)	\n"); 
			sb.append("         + NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 2) - INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) - 1)), 0)	\n"); 
			sb.append("         + NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 3) - INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) - 1)), 0)	\n"); 
			sb.append("         + NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'P', 1, 3) + 1, INSTR(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'P', 1, 3) + 1), '|', 1, 1) - 1)), 0)	\n"); 
			sb.append("      WHEN PF.COPA_NVALOR5 = 1 THEN 						\n"); 
			sb.append("         NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 1) - INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) - 1)), 0)	\n"); 
			sb.append("         + NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 2) - INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) - 1)), 0)	\n"); 
			sb.append("         + NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 3) - INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) - 1)), 0)	\n"); 
			sb.append("         + NVL(C.COCE_MT_PRIMA_PURA, 0)					\n"); 
			sb.append("      ELSE 0 END PRIMA_TOTAL								\n");
			sb.append("      ,C.COCE_FE_ANULACION FECHA_ANULACION				\n");
			sb.append("      ,C.COCE_FE_ANULACION_COL FECHA_CANCELACION			\n");
			sb.append("      ,C.COCE_CD_CAUSA_ANULACION MOTIVO_CANCELACION		\n");
		    sb.append("      ,EST.ALES_DESCRIPCION MOTIVO_CANCELACION_DESC		\n");
			sb.append("      ,NVL(C.COCE_MT_DEVOLUCION,0) MONTO_DEVOLUCION_SIS	\n");
			sb.append("      ,NVL(C.COCE_MT_BCO_DEVOLUCION,0) MONTO_DEVOLUCION	\n");
			sb.append("      ,NVL(MP.AGENCIA, 0) MONTO_EDO_CUENTA				\n");	
			sb.append("	FROM COLECTIVOS_CERTIFICADOS C							\n");
			sb.append(" INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC ON (CC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND CC.COCC_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND CC.COCC_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND CC.COCC_NU_CERTIFICADO = C.COCE_NU_CERTIFICADO)	\n");
			sb.append(" INNER JOIN ALTERNA_ESTATUS EST ON EST.ALES_CD_ESTATUS = C.COCE_CD_CAUSA_ANULACION	\n");
			sb.append(" INNER JOIN COLECTIVOS_CERTIFICADOS HEAD ON (HEAD.COCE_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND HEAD.COCE_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND HEAD.COCE_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND HEAD.COCE_NU_CERTIFICADO = 0)	\n");
			sb.append(" INNER JOIN COLECTIVOS_PARAMETROS PF ON PF.COPA_ID_PARAMETRO = 'GEPREFAC'	\n");
			sb.append(" LEFT JOIN TLMK_FOLIOS_MEDIOS_PAGO MP ON (MP.NU_DE_CONTRATO = CC.COCC_ID_CERTIFICADO AND SUBSTR(MP.VENTA_ORIGEN, 8) ='102' AND MP.FECHA_APROBACION >= C.COCE_FE_ANULACION_COL - 15 AND MP.FECHA_APROBACION <= C.COCE_FE_ANULACION_COL + 15 AND ABS(MP.AGENCIA-NVL(C.COCE_MT_BCO_DEVOLUCION,NVL(C.COCE_MT_DEVOLUCION,0))) <= 1)	\n");
			sb.append("WHERE C.COCE_CASU_CD_SUCURSAL = " + canal + "			\n");
			sb.append("	AND C.COCE_CARP_CD_RAMO = " + ramo + " 					\n");
			sb.append(cadenacan);
			sb.append("	AND C.COCE_NU_CERTIFICADO > 0							\n");
			sb.append(" AND C.COCE_ST_CERTIFICADO IN (10,11)					\n");
			sb.append(" AND C.COCE_CD_CAUSA_ANULACION <> 45						\n");
			sb.append(" AND C.COCE_FE_ANULACION >= TO_DATE('" + fecha1 + "', 'dd/MM/yyyy')   \n");
			sb.append(" AND C.COCE_FE_ANULACION <= TO_DATE('" + fecha2 + "', 'dd/MM/yyyy')   \n");
			sb.append(" AND NVL(CC.COCC_TP_CLIENTE,1) = 1						\n");
		    sb.append(" AND PF.COPA_DES_PARAMETRO = 'POLIZA'					\n");
		    sb.append(" AND PF.COPA_NVALOR1 = C.COCE_CASU_CD_SUCURSAL			\n");
		    sb.append(" AND PF.COPA_NVALOR2 = C.COCE_CARP_CD_RAMO				\n");
		    sb.append(" AND PF.COPA_NVALOR3 = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) \n");
		    sb.append(" AND PF.COPA_NVALOR6 = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA, '0'))	\n");
		    
			objReporte = new File(rutaTemporal + "ReporteDevolucion.jrxml");
		    String nombre = "REPORTE_DEVOLUCIONES_" + GestorFechas.formatDate(new Date(), "ddMMyyyy") +  ".csv" ;
		    
		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    query = new JRDesignQuery();
		    query.setText(sb.toString());
			
			jasperDesign = JRXmlLoader.load(objReporte);
			jasperDesign.setQuery(query);
			report = JasperCompileManager.compileReport(jasperDesign);
			
			print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		    return nombre;
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	public Collection<? extends Object> consultarRegistros(String strQuery) throws Excepciones {
		try {
			Query qry = this.getSession().createSQLQuery(strQuery.toString());
			List lista = qry.list();
			
			return lista;
		}catch (Exception e){
			throw new Excepciones ("Error .",e);
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao#getXmlTimbrar(short, byte, java.lang.String, java.lang.String)
	 */
	public String getXmlTimbrar(short canal, byte ramo, String reciboFiscal, String strRutaDestino) throws Excepciones {
		StringBuilder consulta;
		Query qry; 
		List lista; 
		InputStream inStream = null;
		BLOB blobXml; 
		File archivoXml;
		FileOutputStream fos = null;
		int length  = -1;
		byte[] buffer = null;
		
		try {
			consulta = new StringBuilder();
			consulta.append(" select C.ARCHIVOXML ");
			consulta.append(" from cart_emisioncfd_i c ");
			consulta.append(" where C.CANAL = :canal ");
			consulta.append(" and C.RAMO = :ramo ");
			consulta.append(" and C.FOLIO = :reciboFiscal ");
		
			qry = this.getSession().createSQLQuery(consulta.toString());
			
			//Pasamos parametros.
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("reciboFiscal", reciboFiscal);
			
			lista = qry.list();
			if(lista.isEmpty()) {
				strRutaDestino = "";
				return strRutaDestino;
			}
			
			strRutaDestino = strRutaDestino + "/tempXml.xml";
			blobXml = (BLOB) ((SerializableBlob) lista.get(0)).getWrappedBlob();
			inStream =  blobXml.getBinaryStream();
			buffer  = new byte[blobXml.getBufferSize()];
			
			archivoXml = new File(strRutaDestino); 									
			try {
				fos = new FileOutputStream(archivoXml);
				while((length = inStream.read(buffer)) != -1) {
					fos.write(buffer, 0, length);
					fos.flush();
				}
			} finally {					
				if(fos != null) {
					fos.close();
				}
				if(inStream != null) {
					inStream.close();
				}
			}
			
			return strRutaDestino;
		} catch (Exception e) {
			throw new Excepciones("Error: Al recuperar XML de la Base. ", e);
		} 

		
	}
	
	public byte[] getXmlTimbrar2(short canal, byte ramo, String reciboFiscal) throws Excepciones {
		StringBuilder consulta;
		Query qry; 
		List lista; 
		Blob blobXml; 
		byte[] buffer = null;
		
		try {
			consulta = new StringBuilder();
			consulta.append(" SELECT C.archivoxml ");
			consulta.append(" from CartEmisioncfdI C ");
			consulta.append(" where C.id.canal = :canal ");
			consulta.append(" and C.ramo = :ramo ");
			consulta.append(" and C.id.folio = :reciboFiscal ");
		
			qry = getSession().createQuery(consulta.toString());
			
			//Pasamos parametros.
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("reciboFiscal", new BigDecimal(reciboFiscal));
			
			lista = qry.list();
			if(!lista.isEmpty()) {
				blobXml = (Blob) lista.get(0);
				buffer = new String(blobXml.getBytes(1, (int) blobXml.length())).getBytes("UTF-8");
			} 
		} catch (Exception e) {
			throw new Excepciones("Error: Al recuperar XML de la Base. ", e);
		}

		return buffer;
	}
	
	public void actualizaDatosDFCI(EnviaTimbrado objTimbrado, short canal, byte ramo, Double reciboFiscal) throws Excepciones, HibernateException, SQLException {
		StringBuilder sbQuery;
		Query query;
		      
		try {			
			sbQuery = new StringBuilder();
			
			sbQuery.append("UPDATE\n");
			sbQuery.append("CART_EMISIONCFD_I C ");
			sbQuery.append("SET  C.FOLIOSAT 		  = '" + objTimbrado.getCFDI().getUUID() + "', \n");
			sbQuery.append("     C.CERTIFICADOSAT     = '" + objTimbrado.getCFDI().getNoCertificadoSAT() + "', \n");
			sbQuery.append("     C.FECHATIMBRADO      = TO_DATE(:fechaTimbrado, 'DD/MM/YYYY HH24:MI:SS'), \n");
			sbQuery.append("     C.SELLOSAT           = '" + objTimbrado.getCFDI().getSelloSAT() + "', \n");
			sbQuery.append("     C.CADENAORIGINAL     = '" + objTimbrado.getCFDI().getCadenaOriginal() + "', \n");
			sbQuery.append("     C.CBB                = '" + objTimbrado.getCFDI().getCBBString() + "', \n");
			sbQuery.append("     C.ARCHIVOXML         = :archivoXml ");
			sbQuery.append("WHERE C.CANAL   =" + canal + "\n");
			sbQuery.append("  AND C.RAMO    =" + ramo + "\n");
			sbQuery.append("  AND C.FOLIO   =" + reciboFiscal + "\n");
			query = getSession().createSQLQuery(sbQuery.toString()); 
			query.setString("fechaTimbrado", GestorFechas.formatDate(GestorFechas.generateDate(objTimbrado.getCFDI().getFechaTimbrado() , "yyyy-MM-dd'T'HH:mm:ss"), "dd/MM/yyyy HH:mm:ss"));
			query.setBinary("archivoXml", objTimbrado.getCFDI().getXmlString().getBytes());
			query.executeUpdate();

			sbQuery = new StringBuilder();
			sbQuery.append("UPDATE \n");
			sbQuery.append("COLECTIVOS_FACTURACION CF \n");
			sbQuery.append("SET CF.COFA_NU_TRASFER  = 'TIMBRADO' \n");
			sbQuery.append("WHERE CF.COFA_CASU_CD_SUCURSAL = " + canal + " \n");
			sbQuery.append("  AND CF.COFA_CARP_CD_RAMO = " + ramo + " \n");
			sbQuery.append("  AND CF.COFA_NU_RECIBO_FISCAL = " + reciboFiscal + " \n");
			query = getSession().createSQLQuery(sbQuery.toString()); 
			query.executeUpdate();
			
			sbQuery = new StringBuilder();
			sbQuery.append("UPDATE \n");
			sbQuery.append("CART_RESERVA_DIG_I CR \n");
			sbQuery.append("SET CR.CARD_ST_PROCESO  = 'FMA', \n");
			sbQuery.append("    CR.CARD_TIPO_COBRO  = 'L', \n");
			sbQuery.append("    CR.CARD_FE_TIMBRE  = TO_DATE(:fechaTimbrado, 'DD/MM/YYYY HH24:MI:SS') \n");
			sbQuery.append("WHERE CR.CARD_CASU_CD_SUCURSAL = " + canal + " \n");
			sbQuery.append("  AND CR.CARD_CARP_CD_RAMO = " + ramo + " \n");
			sbQuery.append("  AND CR.CARD_CARE_NU_RECIBO = " + reciboFiscal + " \n");
			query = getSession().createSQLQuery(sbQuery.toString()); 
			query.setString("fechaTimbrado", GestorFechas.formatDate(GestorFechas.generateDate(objTimbrado.getCFDI().getFechaTimbrado() , "yyyy-MM-dd'T'HH:mm:ss"), "dd/MM/yyyy HH:mm:ss"));
			query.executeUpdate();
			
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar tablas para Timbrado. ", e);
		}
	}
	
	// dfg Tu Carrera
	
	public String generaDetallePreFactura(int canal, int ramo, int poliza) {
			
			JasperReport report = null;
			JasperPrint print = null;
			String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			Map<String, Object> parametros;
			String archivo = "";
			
			try {
				
				parametros = new HashMap<String, Object>();
				parametros.put("canal", canal);
				parametros.put("ramo", ramo);
				parametros.put("poliza", poliza);

				
				report = JasperCompileManager.compileReport(rutaTemporal + "DetallePreFactura.jrxml");
				
				
				print = JasperFillManager.fillReport(report, parametros, getSession().connection());
			
				archivo = "detallePreFactura"+".csv";
				
				JRCsvExporter exporter = new JRCsvExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + archivo);
				//exporter.setParameter(JRPdfExporterParameter.METADATA_AUTHOR, "Seguros Santander");
				
				exporter.exportReport();
				
			} catch (Exception e) {
				logger.error(e.getMessage());
			}		
			
			return archivo;
		}

	 //dfg Tu Carrera
	public <T> List<T> resumenPreFactura(int canal, int ramo, Long poliza) throws Excepciones{
		try {
			
			StringBuffer consulta = new StringBuffer();
			
			consulta.append(" SELECT CASE WHEN C.COCE_NU_MOVIMIENTO = 1 THEN 'STOCK'            \n");
			consulta.append("       ELSE 'NUEVO' END TIPO_MOV,									\n");
			consulta.append("       COUNT(1) TOTAL,	 											\n");
			consulta.append("       SUM(C.COCE_MT_PRIMA_SUBSECUENTE) Prima						\n");
			consulta.append("    FROM COLECTIVOS_CERTIFICADOS   C,								\n");
			consulta.append("        COLECTIVOS_CLIENTE_CERTIF CC,								\n");
			consulta.append("     COLECTIVOS_CLIENTES       CL,									\n");
			consulta.append("      COLECTIVOS_CERTIFICADOS   HEAD,								\n");
			consulta.append("      COLECTIVOS_PARAMETROS     PF,								\n");
			consulta.append("      COLECTIVOS_FACTURACION    CF									\n");
			consulta.append(" WHERE C.COCE_CASU_CD_SUCURSAL = " + canal + "    					\n");
			consulta.append("  AND C.COCE_CARP_CD_RAMO      = " + ramo + "     					\n");
			consulta.append("  AND C.COCE_CAPO_NU_POLIZA    = " + poliza + "					\n");
			consulta.append("  AND C.COCE_CASU_CD_SUCURSAL  = CC.COCC_CASU_CD_SUCURSAL			\n");
			consulta.append("  AND C.COCE_CARP_CD_RAMO      = CC.COCC_CARP_CD_RAMO              \n");
			consulta.append("  AND C.COCE_CAPO_NU_POLIZA    = CC.COCC_CAPO_NU_POLIZA            \n");
			consulta.append("  AND C.COCE_NU_CERTIFICADO    = CC.COCC_NU_CERTIFICADO			\n");
			consulta.append("  AND NVL(CC.COCC_TP_CLIENTE,1)= 1    --TUTULAR					\n");
			consulta.append("  AND CL.COCN_NU_CLIENTE       = CC.COCC_NU_CLIENTE				\n");
			consulta.append(" -- AND C.COCE_ST_CERTIFICADO    <> 11								\n");
			consulta.append(" --																\n");
			consulta.append("  AND HEAD.COCE_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL			\n");
			consulta.append("  AND HEAD.COCE_CARP_CD_RAMO     = C.COCE_CARP_CD_RAMO				\n");
			consulta.append("  AND HEAD.COCE_CAPO_NU_POLIZA   = C.COCE_CAPO_NU_POLIZA			\n");	
			consulta.append("  AND HEAD.COCE_NU_CERTIFICADO   = 0								\n");
			consulta.append("  --																\n");
			consulta.append("  AND C.COCE_CASU_CD_SUCURSAL  = CF.COFA_CASU_CD_SUCURSAL			\n");
			consulta.append("  AND C.COCE_CARP_CD_RAMO      = CF.COFA_CARP_CD_RAMO				\n");
			consulta.append("  AND C.COCE_CAPO_NU_POLIZA    = CF.COFA_CAPO_NU_POLIZA			\n");
			consulta.append("  AND 0    = CF.COFA_NU_CERTIFICADO								\n");	
			consulta.append("  AND CF.COFA_NU_RECIBO_FISCAL > 0									\n");
			consulta.append("  AND CF.COFA_NU_RECIBO_COL    > 0									\n");
			consulta.append("  --																\n");			
			consulta.append("  AND PF.COPA_DES_PARAMETRO    = 'POLIZA'							\n");
			consulta.append("  AND PF.COPA_ID_PARAMETRO     = 'GEPREFAC'						\n");
			consulta.append("  AND PF.COPA_NVALOR1          = C.COCE_CASU_CD_SUCURSAL			\n");	
			consulta.append("  AND PF.COPA_NVALOR2          = C.COCE_CARP_CD_RAMO				\n");
			consulta.append("  AND PF.COPA_NVALOR3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) \n");
			consulta.append("  AND PF.COPA_NVALOR6          = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA,'0')) \n");
			consulta.append("  GROUP BY C.COCE_NU_MOVIMIENTO \n");
			Query query = this.getSession().createSQLQuery(consulta.toString());
			List<T> lista = query.list();
			return lista;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
		
	}
	
	public String consultaReporteCancelacion(String fecha1, String fecha2,
			String causaAnulacion) throws Excepciones {
			JasperReport report = null;
			String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			StringBuffer sb = new StringBuffer();
		
		try {
				
			 sb.append(" select CL.cocn_nombre||' '||cl.cocn_apellido_pat||' '||cl.cocn_apellido_mat    NombreCliente   		 \n");      
		     sb.append("        ,C.COCE_CASU_CD_SUCURSAL                                                Canal          			 \n");
		     sb.append("        ,C.COCE_CARP_CD_RAMO                                                    Ramo            		 \n");
		     sb.append("        ,C.COCE_CAPO_NU_POLIZA                                                  Poliza           		 \n");
		     sb.append("        ,C.COCE_NU_CERTIFICADO                                                  Certificado				 \n");
		     sb.append("        ,to_char(c.coce_fe_emision,'dd/mm/yyyy')                                FechaEmisionCertificado  \n");
		     sb.append("        ,CL.COCN_BUC_CLIENTE                                                    BUC            			 \n");
		     sb.append("        ,car.cacn_nm_apellido_razon                                             CONTRATANTE 			 \n");
		     sb.append("        ,c.coce_cazb_cd_sucursal                                                NumeroSucursal 			 \n");
		     sb.append("        ,c.coce_sucursal_banco                                                  NombreSucursal 			 \n");
		     sb.append("        ,'numero de ejecutivo bancario'                                         NumeroEjecutivoBancario  \n");
		     sb.append("        ,'nombre Ejecutivo bancario'                                            NombreEjecutivoBancario  \n");
		     sb.append("        ,c.coce_st_certificado||'-'||ae.ales_descripcion                        EstatusCertificado    	 \n");
		     sb.append("        ,to_char(c.coce_fe_anulacion,'dd/mm/yyyy')                              FechaCancelacion  		 \n");
		     sb.append("        ,c.coce_mt_bco_devolucion                                               ImportePrimaDevuelta   	 \n");
		     sb.append("        ,'TDC'                                                                  ConductoDeCobro          \n");
		     sb.append("        ,c.coce_nu_cuenta                                                       NumeroCuenta             \n");
		     sb.append("        ,to_char(c.coce_fe_estatus,'dd/mm/yyyy')                                FechaRecepcion           \n");
		     sb.append("        ,to_char(c.coce_fe_estatus,'dd/mm/yyyy')                                FechaSolicitud           \n");
		     sb.append("        ,to_char(c.coce_fe_estatus,'dd/mm/yyyy')                                FechaAplicacion          \n");   
		     sb.append("        ,SUBSTR(c.coce_observaciones , 1 ,INSTR(c.coce_observaciones , '|', 1,1)-1)  Observaciones1      \n");
		     sb.append("        ,SUBSTR(c.coce_observaciones,INSTR(c.coce_observaciones, '|', 1,1)+1)        Observaciones2      \n");
		     sb.append(" from colectivos_certificados C 											\n");
		     sb.append("        inner join colectivos_cliente_certif CC on    						\n");
		     sb.append("            CC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL  			\n");  
		     sb.append("            and CC.COCC_CARP_CD_RAMO     = C.COCE_CARP_CD_RAMO      		\n");
		     sb.append("            and CC.COCC_CAPO_NU_POLIZA   = C.COCE_CAPO_NU_POLIZA    		\n");
		     sb.append("            and CC.COCC_NU_CERTIFICADO   = C.COCE_NU_CERTIFICADO    		\n");
		     sb.append("        inner join colectivos_clientes CL on            					\n");
		     sb.append("            CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE   						\n");
		     sb.append("        inner join colectivos_certificados head on    						\n");
		     sb.append("            head.COCE_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL   			\n");  
		     sb.append("            and head.COCE_CARP_CD_RAMO     = C.COCE_CARP_CD_RAMO      		\n");
		     sb.append("            and head.COCE_CAPO_NU_POLIZA    = C.COCE_CAPO_NU_POLIZA    		\n");
		     sb.append("            and head.COCE_NU_CERTIFICADO   = 0    							\n");
		     sb.append("        inner join cart_certificados cac on   								\n");
		     sb.append("            cac.cace_casu_cd_sucursal      = head.COCE_CASU_CD_SUCURSAL  	\n"); 
		     sb.append("            and cac.cace_carp_cd_ramo      = head.COCE_CARP_CD_RAMO       	\n");
		     sb.append("            and cac.cace_capo_nu_poliza    = head.COCE_CAPO_NU_POLIZA      	\n");
		     sb.append("            and cac.cace_nu_certificado    = head.COCE_NU_CERTIFICADO  		\n");
		     sb.append("        inner join cart_clientes car on     								\n");
		     sb.append("            car.cacn_cd_nacionalidad    = cac.cace_cacn_cd_nacionalidad 	\n");
		     sb.append("            and car.cacn_nu_cedula_rif      = cac.cace_cacn_nu_cedula_rif  	\n");
		     sb.append("        inner join alterna_estatus ae on   									\n");
		     sb.append("            ae.ales_cd_estatus = c.coce_st_certificado  					\n");
		     sb.append(" where C.COCE_CASU_CD_SUCURSAL = 1     										\n");           
		     sb.append("            and C.COCE_CARP_CD_RAMO >0                  					\n");
		     sb.append("            and C.COCE_CAPO_NU_POLIZA >0              				        \n");
		     sb.append("            and C.COCE_NU_CERTIFICADO >0                  					\n");
             sb.append("            and C.Coce_Fe_Anulacion between to_date('" + fecha1 + "','dd/mm/yyyy') and to_date('" + fecha2 + "','dd/mm/yyyy')\n");
		     sb.append("            and c.coce_cd_causa_anulacion=" + causaAnulacion + " \n");
		    
		     
			File reporte = new File(rutaTemporal + "ReporteCancelacion.jrxml");
		    JasperDesign jasperDesign = null;

		    Format formatter1;
            formatter1 = new SimpleDateFormat("ddMMyyyy");

		    String sts			= "";
			String fec			= null;
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();
	        fec = formatter1.format(nowDate);	 

		    String nombre = "REPCANCELACION_" + sts + "_" + "_" + fec +  ".csv" ;
		    
		    JRSwapFile swapFile = null;
		    JRSwapFileVirtualizer virtualizer = null;

		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    Map parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			JRDesignQuery query = new JRDesignQuery();
			Log.info(sb.toString());
			query.setText(sb.toString());
			
			//System.out.println(sb.toString());
	    
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			JasperPrint print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		    
		  return nombre;
			
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}

	}
	
	public void actualizaObservacionesCan(CertificadoId id, String strObservaciones) throws Excepciones, HibernateException, SQLException { 
		String strQuery;
		Query query;
		      
		try {			
			strQuery = "update Certificado certif \n" +
					"set certif.coceObservaciones = :observa \n" +
					"where certif.id.coceCasuCdSucursal = :canal \n" +
					"and certif.id.coceCarpCdRamo       = :ramo\n" +
					"and certif.id.coceCapoNuPoliza     = :poliza\n" +
					"and certif.id.coceNuCertificado    = :certi \n";
			
			query = getSession().createQuery(strQuery); 
			query.setParameter("observa", strObservaciones);
			query.setParameter("canal", id.getCoceCasuCdSucursal());
			query.setParameter("ramo", id.getCoceCarpCdRamo());
			query.setParameter("poliza", id.getCoceCapoNuPoliza());
			query.setParameter("certi", id.getCoceNuCertificado());
			query.executeUpdate();
			
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar observaciones. ", e);
		}
		
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public ArrayList<Object> getPolizasaaRenovar5758(String rmes, String anio) throws Exception {
		ArrayList<Object> arlResultado;
		StringBuilder sbConsulta;
		Query qry;
		
		try {
			arlResultado = new ArrayList<Object>();
			sbConsulta = new StringBuilder() ;
			sbConsulta.append("SELECT CER.CACE_CASU_CD_SUCURSAL, CER.CACE_CARP_CD_RAMO, CER.CACE_CAPO_NU_POLIZA, REC.CARE_NU_CONSECUTIVO_CUOTA, COUNT(1), \n");
					sbConsulta.append("CERHE.CACE_CAIR_IN_CAUSA_RENOVACION, NVL(CERHE.CACE_CAIR_IN_CAUSA_RENOVACION, 1) + 1, \n"); 
					sbConsulta.append("CERHE.CACE_FE_DESDE, ADD_MONTHS(CERHE.CACE_FE_DESDE, 12), \n");
					sbConsulta.append("CERHE.CACE_FE_HASTA, ADD_MONTHS(CERHE.CACE_FE_HASTA, 12), \n");
					sbConsulta.append("CERHE.CACE_FE_PROXIMA_FACTURACION \n");                   
				sbConsulta.append("FROM CART_CERTIFICADOS CER \n");  
				sbConsulta.append("INNER JOIN CART_RECIBOS REC ON(REC.CARE_CASU_CD_SUCURSAL = CER.CACE_CASU_CD_SUCURSAL AND REC.CARE_CARP_CD_RAMO = CER.CACE_CARP_CD_RAMO AND REC.CARE_CAPO_NU_POLIZA = CER.CACE_CAPO_NU_POLIZA) \n");
				sbConsulta.append("INNER JOIN CART_CERTIFICADOS CERHE ON(CERHE.CACE_CASU_CD_SUCURSAL = CER.CACE_CASU_CD_SUCURSAL AND CERHE.CACE_CARP_CD_RAMO = CER.CACE_CARP_CD_RAMO AND CERHE.CACE_CAPO_NU_POLIZA = CER.CACE_CAPO_NU_POLIZA AND CERHE.CACE_NU_CERTIFICADO = 0)  \n");
			sbConsulta.append("WHERE CER.CACE_CASU_CD_SUCURSAL = 1 \n"); 
				sbConsulta.append("AND CER.CACE_CARP_CD_RAMO     IN (57,58) \n"); 
				sbConsulta.append("AND CER.CACE_CAPO_NU_POLIZA   > 0 \n"); //
				sbConsulta.append("AND CER.CACE_NU_CERTIFICADO   > 0 \n");
				sbConsulta.append("AND CER.CACE_ST_CERTIFICADO   = 1 \n");         
				sbConsulta.append("AND TO_CHAR(CERHE.CACE_FE_SUSCRIPCION,'mm') = '").append(rmes).append("' \n"); 
				sbConsulta.append("AND TO_CHAR(CERHE.CACE_FE_SUSCRIPCION,'yyyy') <> '").append(anio).append("' \n");
				sbConsulta.append("AND TO_CHAR(CERHE.CACE_FE_HASTA,'yyyy')     = '").append(anio).append("' \n");               
				sbConsulta.append("AND REC.CARE_FE_HASTA = (SELECT MAX(MAXREC.CARE_FE_HASTA) \n"); 
											sbConsulta.append("FROM CART_RECIBOS MAXREC \n"); 
										sbConsulta.append("WHERE MAXREC.CARE_CASU_CD_SUCURSAL = CER.CACE_CASU_CD_SUCURSAL \n"); 
											sbConsulta.append("AND MAXREC.CARE_CARP_CD_RAMO = CER.CACE_CARP_CD_RAMO \n");
											sbConsulta.append("AND MAXREC.CARE_CAPO_NU_POLIZA = CER.CACE_CAPO_NU_POLIZA \n"); 
											sbConsulta.append("AND MAXREC.CARE_FE_EMISION > SYSDATE - 60) \n");
											sbConsulta.append("AND REC.CARE_NU_CONSECUTIVO_CUOTA = 12 \n");
											sbConsulta.append("AND CERHE.CACE_ST_CERTIFICADO    = 1 \n");
			sbConsulta.append("GROUP BY CER.CACE_CASU_CD_SUCURSAL, CER.CACE_CARP_CD_RAMO,CER.CACE_CAPO_NU_POLIZA, REC.CARE_NU_CONSECUTIVO_CUOTA, CERHE.CACE_CAIR_IN_CAUSA_RENOVACION, CERHE.CACE_FE_DESDE, CERHE.CACE_FE_HASTA, CERHE.CACE_FE_PROXIMA_FACTURACION \n");  
			sbConsulta.append("ORDER BY 2 \n");
									
			qry = this.getSession().createSQLQuery(sbConsulta.toString());
			arlResultado.addAll(qry.list());
			return arlResultado;
		} catch (Exception e){
			throw new Excepciones ("Error: CertificadoDao.getPolizasaaRenovar:: " + e.getMessage());
		}
	}


	public void actualizaPolizasCertificados5758(Object canal, Object ramo, Object poliza, Object anioPoliza, String fechaDesde, String fechaHasta) throws Exception {
		StringBuilder sbQuery;
		Query query;
		      
		try {			
			sbQuery = new StringBuilder();
			
			System.out.println("Actualizando Header de Poliza: " + ramo.toString() + "-" + poliza.toString());
			sbQuery.append("UPDATE\n");
			sbQuery.append("CART_CERTIFICADOS CE ");
			sbQuery.append("SET  CE.CACE_CAIR_IN_CAUSA_RENOVACION 	= " + anioPoliza + ", \n");
			sbQuery.append("     CE.CACE_FE_DESDE     				= TO_DATE(:fechaDesde, 'DD/MM/YYYY HH24:MI:SS'), \n");
			sbQuery.append("     CE.CACE_FE_HASTA      				= TO_DATE(:fechaHasta, 'DD/MM/YYYY HH24:MI:SS'), \n");
			sbQuery.append("     CE.CACE_FE_PROXIMA_FACTURACION     = TO_DATE(:fechaHasta, 'DD/MM/YYYY HH24:MI:SS'), \n");
			sbQuery.append("     CE.CACE_IN_FACTURACION     		= 'S' \n");
			sbQuery.append("WHERE CE.CACE_CASU_CD_SUCURSAL  = " + canal + "\n");
			sbQuery.append("  AND CE.CACE_CARP_CD_RAMO    	= " + ramo + "\n");
			sbQuery.append("  AND CE.CACE_CAPO_NU_POLIZA   	= " + poliza + "\n");
			sbQuery.append("  AND CE.CACE_NU_CERTIFICADO   	= 0 \n");
			sbQuery.append("  AND CE.CACE_ST_CERTIFICADO   	= 1 \n");
			
			query = getSession().createSQLQuery(sbQuery.toString()); 
			query.setString("fechaDesde", GestorFechas.formatDate(GestorFechas.generateDate(fechaDesde , "yyyy-MM-dd HH:mm:ss"), "dd/MM/yyyy"));
			query.setString("fechaHasta", GestorFechas.formatDate(GestorFechas.generateDate(fechaHasta , "yyyy-MM-dd HH:mm:ss"), "dd/MM/yyyy"));
			query.executeUpdate();
			System.out.println("Termino Actualizar Header de Poliza: " + ramo.toString() + "-" + poliza.toString());
			
			System.out.println("Actualizando certificados de Poliza: " + ramo.toString() + "-" + poliza.toString());
			sbQuery = new StringBuilder();
			sbQuery.append("UPDATE\n");
			sbQuery.append("CART_CERTIFICADOS CE ");
			sbQuery.append("SET  CE.CACE_CAIR_IN_CAUSA_RENOVACION 	= " + anioPoliza + ", \n");
			sbQuery.append("     CE.CACE_FE_DESDE     				= TO_DATE(:fechaDesde, 'DD/MM/YYYY HH24:MI:SS'), \n");
			sbQuery.append("     CE.CACE_FE_HASTA      				= TO_DATE(:fechaHasta, 'DD/MM/YYYY HH24:MI:SS'), \n");
			sbQuery.append("     CE.CACE_FE_PROXIMA_FACTURACION     = TO_DATE(:fechaHasta, 'DD/MM/YYYY HH24:MI:SS'), \n");
			sbQuery.append("     CE.CACE_IN_FACTURACION     		= 'S' \n");
			sbQuery.append("WHERE CE.CACE_CASU_CD_SUCURSAL  = " + canal + "\n");
			sbQuery.append("  AND CE.CACE_CARP_CD_RAMO    	= " + ramo + "\n");
			sbQuery.append("  AND CE.CACE_CAPO_NU_POLIZA   	= " + poliza + "\n");
			sbQuery.append("  AND CE.CACE_NU_CERTIFICADO   	> 0 \n");
			sbQuery.append("  AND CE.CACE_ST_CERTIFICADO   	= 1 \n");
			query = getSession().createSQLQuery(sbQuery.toString()); 
			
			query.setString("fechaDesde", GestorFechas.formatDate(GestorFechas.generateDate(fechaDesde , "yyyy-MM-dd HH:mm:ss"), "dd/MM/yyyy"));
			query.setString("fechaHasta", GestorFechas.formatDate(GestorFechas.generateDate(fechaHasta , "yyyy-MM-dd HH:mm:ss"), "dd/MM/yyyy"));
			query.executeUpdate();
			System.out.println("Terminando Actualizando certificados de Poliza: " + ramo.toString() + "-" + poliza.toString());
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar tablas para renovacion ramos 57y58. ", e);
		}
		
	}

	public String getReporteRenovacion5758(String rmes, String anio) throws Exception {
		JasperReport report = null;
		String rutaTemporal,  nombre, reporte;
		JRSwapFile swapFile = null;
	    JRSwapFileVirtualizer virtualizer = null;
	    Map parametros;
	    JasperPrint print;
	    
		try {
			rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			reporte = rutaTemporal + "ReporteRenovacion5758.jrxml";
		    nombre = "REPREN_5758_" +  GestorFechas.formatDate(new Date(), "ddMMyyyy") +  ".csv" ;

		    swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
			report = JasperCompileManager.compileReport(reporte);
			
		    parametros = new HashMap();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    parametros.put("mes",rmes);
		    parametros.put("anio",anio);
			print = JasperFillManager .fillReport(report, parametros,getSession().connection());
	    
		    JRCsvExporter exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();

		    return nombre;
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerProdPlan(final String filtro) {
		return this.getHibernateTemplate().executeFind(new HibernateCallback<Object>(){
			public Object doInHibernate(final Session sesion) throws HibernateException, SQLException {
				final StringBuffer consulta = new StringBuffer(500);
				consulta.append("from Certificado C	 \n");
				consulta.append("where 1=1      		 \n");
				consulta.append(filtro);
							
				final Query qry = sesion.createQuery(consulta.toString());
				final List<T> lista = qry.list();
					
				return lista;
			}				
		});
	}
	
	public <T> BigDecimal maxNumeroDeCredito(String filtro) throws Excepciones {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" Select max(cer.coce_nu_credito)+1 from colectivos_certificados cer "); 
			sb.append(" where ");
			sb.append(" 1=1 ").append(filtro);

			Query query = this.getSession().createSQLQuery(sb.toString());
			return (BigDecimal)query.uniqueResult();	
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
		
	//Carga Polizas
	public <T> List<T> cargaPolizas(final int ramo) throws Exception{
		List<T> polizas = null;
		
		try{
			
			StringBuilder sb = new StringBuilder();
	
			sb.append(" select cc.coce_casu_cd_sucursal, ");
			sb.append(" cc.coce_carp_cd_ramo, ");
			sb.append(" cc.coce_capo_nu_poliza, ");
			sb.append(" count(cd.coce_nu_certificado), ");
			sb.append(" sum(cd.coce_mt_prima_subsecuente) ");
			sb.append(" from colectivos_certificados cc ");
			sb.append(" inner join colectivos_certificados cd ON(cd.coce_casu_cd_sucursal = cc.coce_casu_cd_sucursal and cd.coce_carp_cd_ramo=cc.coce_carp_cd_ramo and cd.coce_capo_nu_poliza=cc.coce_capo_nu_poliza and cd.coce_nu_certificado > 0) ");
			sb.append(" where cc.coce_casu_cd_sucursal=1 ");
			sb.append(" and cc.coce_carp_cd_ramo = ").append(ramo);
			sb.append(" and cc.coce_capo_nu_poliza > 0 ");
			sb.append(" and cc.coce_nu_certificado = 0 ");
			sb.append(" and cc.coce_st_facturacion = 1 ");
			sb.append(" group by cc.coce_casu_cd_sucursal,  cc.coce_carp_cd_ramo, cc.coce_capo_nu_poliza ");
	
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
		
	public <T> List<T> cargaPolizasRenovar(final int ramo,String rmes2,  String anio) throws Exception {
		List<T> polizas = null;
		StringBuilder sb;
		Query query; 
		
		try{
			sb = new StringBuilder();
			sb.append("select  cer.coce_casu_cd_sucursal, cer.coce_carp_cd_ramo, cer.coce_capo_nu_poliza, cer.coce_fe_desde, cer.coce_fe_hasta \n"); 
			sb.append("	from colectivos_certificados cer, colectivos_parametros par \n"); 
			sb.append("where cer.coce_casu_cd_sucursal = par.copa_nvalor1  \n");
			sb.append("	and cer.coce_carp_cd_ramo = par.copa_nvalor2  \n");
			sb.append("	and cer.coce_carp_cd_ramo = ").append(ramo).append(" \n");
			sb.append(" and cer.Coce_Capo_Nu_Poliza > 0  \n");
			sb.append("	and cer.coce_nu_certificado   = 0  \n");
			sb.append("	and par.copa_des_parametro    = 'POLIZA'  \n");
			sb.append("	and par.copa_id_parametro     = 'GEP'  \n");
			sb.append("	and par.copa_nvalor4          = 0  \n");
			sb.append("	and cer.coce_st_certificado   = 1 \n");
			sb.append("	and to_char(cer.coce_fe_hasta,'mm') = '"+ rmes2 + "' \n");
			sb.append("	and to_char(cer.coce_fe_hasta,'yyyy') = '" + anio + "' \n");  
			sb.append("order by cer.coce_capo_nu_poliza desc ");
	
			query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		} catch (Exception e){
			throw new Exception("Error.", e);
		}
	}

	@Override
	public Integer consultaNumRecibo(CertificadoId id, BigDecimal prima) throws Exception {
		Integer nResultado = Constantes.DEFAULT_INT;
		BigDecimal nPrimaAntRec;
		StringBuilder sbConsulta;
		Query qry; 
		List lista;
		Object[] arrDatos;
				
		try {
			sbConsulta = new StringBuilder();
			sbConsulta.append("SELECT NVL(MAX(CORE_NU_CONSECUTIVO_CUOTA), 0), NVL(MAX(CORE_NU_RECIBO), 0) \n");
			sbConsulta.append("	FROM COLECTIVOS_CERTIFICADOS CE \n");
			sbConsulta.append("	INNER JOIN COLECTIVOS_RECIBOS CR ON(CORE_CASU_CD_SUCURSAL = COCE_CASU_CD_SUCURSAL AND CORE_CARP_CD_RAMO = COCE_CARP_CD_RAMO AND CORE_CAPO_NU_POLIZA = COCE_CAPO_NU_POLIZA) \n");
			sbConsulta.append("WHERE COCE_CASU_CD_SUCURSAL 	= :canal \n");
			sbConsulta.append("	AND COCE_CARP_CD_RAMO   	= :ramo \n");
			sbConsulta.append("	AND COCE_CAPO_NU_POLIZA		= :poliza \n");
			sbConsulta.append(" AND COCE_NU_CERTIFICADO 	= 0 \n");
			sbConsulta.append(" AND COCE_ST_CERTIFICADO 	= 1 \n");
			sbConsulta.append(" AND CORE_FE_DESDE  			>= COCE_FE_DESDE \n");
			sbConsulta.append(" AND CORE_FE_HASTA       	<= COCE_FE_HASTA \n");
			sbConsulta.append(" AND CORE_ST_RECIBO      	IN (4) \n");
			
			qry = this.getSession().createSQLQuery(sbConsulta.toString());
			qry.setParameter("canal", id.getCoceCasuCdSucursal());
			qry.setParameter("ramo", id.getCoceCarpCdRamo());
			qry.setParameter("poliza", id.getCoceCapoNuPoliza());
			lista = qry.list();
			
			if(!lista.isEmpty()) {
				arrDatos = (Object[]) lista.get(0);
				
				lista = new ArrayList();
				sbConsulta = new StringBuilder();
				sbConsulta.append("SELECT NVL(CORE_MT_PRIMA, 0) \n");
				sbConsulta.append("	FROM COLECTIVOS_RECIBOS CR \n");
				sbConsulta.append("WHERE CORE_CASU_CD_SUCURSAL 	= :canal \n");
				sbConsulta.append("	AND CORE_NU_RECIBO   		= :recibo \n");
				
				qry = this.getSession().createSQLQuery(sbConsulta.toString());
				qry.setParameter("canal", id.getCoceCasuCdSucursal());
				qry.setParameter("recibo", arrDatos[1]);
				lista = qry.list();
				
				if(lista.isEmpty()) {
					nResultado = ((BigDecimal) arrDatos[0]).intValue();
				} else {
					nPrimaAntRec = (BigDecimal) lista.get(0);
					
					if(nPrimaAntRec.equals(prima)) {
						nResultado = -1;
					} else {
						nResultado = ((BigDecimal) arrDatos[0]).intValue();
					}
				}
				
			} 
		} catch (Exception e) {
			throw new Excepciones("Error: Al recuperar ultimo recibo. ", e);
		}
		
		return nResultado;
	}
	
	@Override
	public ArrayList<Object> generaReportevigor(short canal, Short ramo, Long poliza,Integer inIdVenta,Long poliza1) {
		ArrayList<Object> resultado = null;
		String cadenaNoPrim="",	cadenaPrim=""; 
		try {
			StringBuilder sb = new StringBuilder();
			
					 
			sb.append(" select  c.coce_carp_cd_ramo      RAMO \n");                  
			sb.append(" ,pl.alpl_dato3            CENTRO_COSTOS \n");               
			sb.append(" ,c.coce_capo_nu_poliza    POLIZA \n");                  
			sb.append(" ,c.coce_no_recibo         RECIBO \n");                  
			sb.append(" ,cc.cocc_id_certificado   CREDITO \n");                   
			sb.append(" ,c.coce_nu_certificado    CERTIFICADO \n");                 
			sb.append(" ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA \n");     
			sb.append(" ,est.ales_campo1         ESTATUS \n");                              
		    sb.append(" ,c.coce_nu_cuenta         CUENTA \n");                  
		    sb.append(" ,c.coce_tp_producto_bco   PRODUCTO \n");                
		    sb.append(" ,c.coce_tp_subprod_bco    SUBPRODUCTO \n");                 
		    sb.append(" ,c.coce_cazb_cd_sucursal  SUCURSAL \n");                
		    sb.append(" ,cl.cocn_apellido_pat     APELLIDO_PATERNO \n");            
		    sb.append(" ,cl.cocn_apellido_mat     APELLIDO_MATERNO \n");            
		    sb.append(" ,cl.cocn_nombre           NOMBRE \n");                  
		    sb.append(" ,cl.cocn_cd_sexo          SEXO \n");                  
		    sb.append(" ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO \n");            
		    sb.append(" ,cl.cocn_buc_cliente      NUMERO_CLIENTE \n");              
		    sb.append(" ,cl.cocn_rfc              RFC \n");                     
		    sb.append(" ,c.coce_cd_plazo          PLAZO \n");                   
		    sb.append(" ,head.coce_fe_desde       FECHA_INICIO_POLIZA \n");             
		    sb.append(" ,head.coce_fe_hasta       FECHA_FIN_POLIZA \n");            
		    sb.append(" ,c.coce_fe_suscripcion    FECHA_INGRESO \n");               
		    sb.append(" ,c.coce_fe_desde          FECHA_DESDE \n");                 
		    sb.append(" ,c.coce_fe_hasta          FECHA_HASTA \n");                 
		    sb.append(" ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO \n");          
		    sb.append(" ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO \n");             
		    sb.append(" ,c.coce_campov6      FECHA_FIN_CREDITO_2 \n");            
		    sb.append(" ,c.coce_fe_anulacion      FECHA_ANULACION \n");               
		    sb.append(" ,c.coce_fe_anulacion_col  FECHA_CANCELACION \n");     
		    sb.append(" ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n"); 
		    sb.append(" ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO \n");
		    sb.append(" ,case when pf.copa_nvalor5 = 0 \n");                  
		    sb.append(" then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
		    sb.append(" when pf.copa_nvalor5 = 1 \n");                  
		    sb.append(" then c.coce_mt_prima_pura \n");             
		    sb.append(" else 0 end PRIMA_NETA \n");
		    sb.append(" ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n"); 
		    sb.append(" ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS \n");  
		    sb.append(" ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n"); 
		    sb.append(" ,case when pf.copa_nvalor5 = 0 then \n"); 
		    sb.append(" nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
		    sb.append(" + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
		    sb.append(" + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
		    sb.append(" + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
		    sb.append(" when pf.copa_nvalor5 = 1 then \n"); 
		    sb.append(" nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
		    sb.append(" + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
		    sb.append(" + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
		    sb.append(" + nvl(c.coce_mt_prima_pura,0) \n"); 
		    sb.append(" else 0 end PRIMA_TOTAL \n");  
		    sb.append(" ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n"); 
		    sb.append(" , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n"); 
		    sb.append(" (select sum (cob.cocb_ta_riesgo) tasaVida \n");           
		    sb.append(" from colectivos_coberturas cob \n");              
		    sb.append(" where cob.cocb_casu_cd_sucursal =  1 \n");             
		    sb.append(" and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo \n");    
		    sb.append(" and cob.cocb_carb_cd_ramo     = 14 \n");             
		    sb.append(" and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza \n");   
		    sb.append(" and cob.cocb_cacb_cd_cobertura  <> '003' \n");           
		    sb.append(" and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n"); 
		    sb.append(" and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan \n");  
		    sb.append(" and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) \n");   
		    sb.append(" end,2) PRIMA_VIDA \n");                       
		    sb.append(" , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n"); 
		    sb.append(" (select sum (cob.cocb_ta_riesgo) tasaDes \n");          
		    sb.append(" from colectivos_coberturas cob \n");              
		    sb.append(" where cob.cocb_casu_cd_sucursal =  1 \n");             
		    sb.append(" and cob.cocb_carp_cd_ramo     = 61 \n");             
		    sb.append(" and cob.cocb_carb_cd_ramo     = 14 \n");             
		    sb.append(" and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza \n");   
		    sb.append(" and cob.cocb_cacb_cd_cobertura  = '003' \n");          
		    sb.append(" and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n"); 
		    sb.append(" and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan \n");  
		    sb.append(" and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) \n");   
		    sb.append(" end,2) PRIMA_DESEMPLEO \n");                    
		    sb.append(" ,cl.cocn_cd_estado        CD_ESTADO \n");                 
		    sb.append(" ,es.caes_de_estado        ESTADO \n");                  
		    sb.append(" ,cl.cocn_delegmunic       MUNICIPIO \n");                 
		    sb.append(" ,cl.cocn_cd_postal        CP \n");                    
		    sb.append(" ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION \n");           
		    sb.append(" ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS \n");           
		    sb.append(" ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");   
		    sb.append(" ,nvl(c.coce_buc_empresa,0)      CREDITONUEVO \n");            
		    sb.append(" ,c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA \n");     
		    sb.append(" ,c.coce_capb_cd_plan             PLAN_ASEGURADORA \n");         
		    sb.append(" ,(select sum (cob.cocb_ta_riesgo) \n");                         
		    sb.append(" from colectivos_coberturas cob \n");                    
		    sb.append(" where cob.cocb_casu_cd_sucursal =  1 \n");               
		    sb.append(" and cob.cocb_carp_cd_ramo     = 61 \n");               
		    sb.append(" and cob.cocb_carb_cd_ramo     = 14 \n");               
		    sb.append(" and cob.cocb_cacb_cd_cobertura  <> '003' \n");         
		    sb.append(" and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");           
		    sb.append(" and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan \n");               
		    sb.append(" and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n"); 
		    sb.append(" ,(select sum (cob.cocb_ta_riesgo) \n");                                            
		    sb.append(" from colectivos_coberturas cob \n");                                       
		    sb.append(" where cob.cocb_casu_cd_sucursal =  1 \n");                                  
		    sb.append(" and cob.cocb_carp_cd_ramo     = 61 \n");                                  
		    sb.append(" and cob.cocb_carb_cd_ramo     = 14 \n");                                  
		    sb.append(" and cob.cocb_cacb_cd_cobertura  = '003' \n");                             
		    sb.append(" and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");           
		    sb.append(" and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan \n");               
		    sb.append(" and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO \n");
		    sb.append(" ,cocn_fe_nacimiento  FECHA_NACIMIENTO_OBLIGATORIO, \n");
		    sb.append(" cocn_cd_sexo SEXO_OBLIGATORIO, \n");
		    sb.append(" cocc_tp_cliente TIPO_INTERVENCION, \n");
		    sb.append(" c.coce_fe_emision FECHA_EMISION, \n");
		    sb.append(" 0 FECH_ENVIO, \n");
		    sb.append(" c.coce_fe_carga FECHA_CARGA, \n");
		    sb.append(" 0 FECHA_RECIBIDO, \n");
		    sb.append(" '' TASA_AMORTIZACION \n");                                                   
		    sb.append(" from  colectivos_certificados   c \n");                    
		    sb.append(" ,colectivos_cliente_certif cc \n");                     
		    sb.append(" ,colectivos_clientes       cl \n");                     
		    sb.append(" ,cart_estados              es \n");                     
		    sb.append(" ,alterna_estatus           est \n");                  
		    sb.append(" ,colectivos_certificados   head \n");                   
		    sb.append(" ,alterna_planes            pl \n");                     
		    sb.append(" ,colectivos_parametros     pf \n");   
		    sb.append(" where c.coce_casu_cd_sucursal =  ").append(canal).append(" \n");;               
		    sb.append(" and c.coce_carp_cd_ramo =  ").append(ramo).append(" \n");;
		    if(poliza==0){
		    	sb.append(" and c.coce_capo_nu_poliza between ").append(inIdVenta).append("00000000 and ").append(inIdVenta).append("99999999 \n");
			}else{
				sb.append(" and c.coce_capo_nu_poliza = ").append(poliza).append(" \n");
			}
		    sb.append(" and cc.cocc_nu_certificado   > 0 \n");
		    sb.append(" and c.COCE_ST_CERTIFICADO in(1,2) \n");
		    sb.append(" and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal \n");        
		    sb.append(" and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo \n");          
		    sb.append(" and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza \n");          
		    sb.append(" and cc.cocc_nu_certificado   = c.coce_nu_certificado \n");          
		    sb.append(" and nvl(cc.cocc_tp_cliente,1)       = 1 \n");                   
		    sb.append(" and cl.cocn_nu_cliente       = cc.cocc_nu_cliente \n");   
		    sb.append(" and es.caes_cd_estado(+)     = cl.cocn_cd_estado \n");
		    sb.append(" and est.ales_cd_estatus      = c.coce_st_certificado \n");
		    sb.append(" and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal \n");        
		    sb.append(" and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo \n");          
		    sb.append(" and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza \n");        
		    sb.append(" and head.coce_nu_certificado   = 0 \n");                  
		    sb.append(" and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo \n");          
		    sb.append(" and pl.alpl_cd_producto      = c.coce_capu_cd_producto \n");        
		    sb.append(" and pl.alpl_cd_plan          = c.coce_capb_cd_plan \n");                                        
		    sb.append(" and pf.copa_des_parametro    = 'POLIZA' \n");
		    sb.append(" and pf.copa_id_parametro     = 'GEPREFAC' \n");		                  
		    sb.append(" and pf.copa_nvalor1          = c.coce_casu_cd_sucursal \n");            
		    sb.append(" and pf.copa_nvalor2          = c.coce_carp_cd_ramo \n");          
		    sb.append(" and pf.copa_nvalor3          = decode(pf.copa_nvalor4,0,c.coce_capo_nu_poliza,1,substr(c.coce_capo_nu_poliza,0,3),2,substr(c.coce_capo_nu_poliza,0,2)) \n"); 
		    sb.append(" and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0'))  \n");

			Query query = this.getSession().createSQLQuery(sb.toString());
			System.out.println("iniciaQuery "+sb);
			resultado = (ArrayList<Object>) query.list();
			
		} catch (Exception e) {			
			logger.error(e.getMessage());
		}
			
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao#insertaBanderaBatch(java.lang.Long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public void insertaBanderaBatch(Long next, String nombreVigores, String rutaParam, String strMensaje, String strNomArchivo, Long tipoReporte, Long reporteComisiones )  throws Excepciones {
		ProgramaReporteDaoHibernate prgReporte; 
		Calendar now;
		StringBuilder sbFechaInicio;
		
		try {		
			//Inicializamos variables
			prgReporte = new ProgramaReporteDaoHibernate(getSessionFactory());
			now = new GregorianCalendar();
			sbFechaInicio = new StringBuilder();
			
			//Creamos fecha con horas minutos y segundos
			now.setTime(now.getTime());
			sbFechaInicio.append(GestorFechas.formatDate(now.getTime(), Constantes.FORMATO_FECHA_UNO)).append(" ");
			sbFechaInicio.append(now.get(Calendar.HOUR_OF_DAY)).append(":");
			sbFechaInicio.append(Utilerias.agregarCaracteres(String.valueOf(now.get(Calendar.MINUTE)), 1, '0', 1) ).append(":");
			sbFechaInicio.append(now.get(Calendar.SECOND));
			
			//se manda programar reporte
			prgReporte.insertarQueryReporteTecnico(nombreVigores, strNomArchivo, rutaParam, sbFechaInicio.toString(), strMensaje);
			                        
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao#insertaBanderaBatch(java.lang.Long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Long, java.lang.Long, java.lang.Double, java.util.Date, java.util.Date)
	 */
	@Override
	public void insertaBanderaBatch(Parametros objPeticion) throws Excepciones {
		StringBuilder sbquery;                                                             
		Query query;  

		try {		
			sbquery = new StringBuilder();
			sbquery.append("INSERT INTO COLECTIVOS_PARAMETROS \n");                 
			sbquery.append(" (COPA_CD_PARAMETRO , \n");                 
			sbquery.append("  COPA_DES_PARAMETRO, \n");                 
			sbquery.append("  COPA_ID_PARAMETRO , \n");
			sbquery.append("  COPA_VVALOR6,  \n");
			sbquery.append("  COPA_VVALOR7,  \n");
			sbquery.append("  COPA_VVALOR8,  \n");
			sbquery.append("  COPA_NVALOR7,  \n");		
			sbquery.append("  COPA_NVALOR8,  \n");		
			sbquery.append("  COPA_NVALOR1,  \n");
			sbquery.append("  COPA_VVALOR5,  \n");		
			sbquery.append("  COPA_FVALOR1,  \n");		
			sbquery.append("  COPA_FVALOR2,  \n");		
			sbquery.append("  COPA_FVALOR3)  \n");		

			sbquery.append("VALUES ( \n");                 
			sbquery.append(objPeticion.getCopaCdParametro()).append(",");                 
			sbquery.append("'" + objPeticion.getCopaDesParametro()).append("' ,");    
			sbquery.append("'" + objPeticion.getCopaIdParametro()).append("' ,");
			sbquery.append("'" + objPeticion.getCopaVvalor6()).append("' ,");
			sbquery.append("'" + objPeticion.getCopaVvalor7()).append("' ,");
			sbquery.append("'" + objPeticion.getCopaVvalor8()).append("' ,");
			sbquery.append(objPeticion.getCopaNvalor7()).append(" ,");
			sbquery.append(objPeticion.getCopaNvalor8()).append(" ,");
			sbquery.append(objPeticion.getCopaNvalor1()).append(",");
			                                       		
			sbquery.append("'" + objPeticion.getCopaVvalor5()).append("' ,");
			sbquery.append("SYSDATE").append(" ,");
			sbquery.append("TO_DATE('" + GestorFechas.formatDate(objPeticion.getCopaFvalor2(), "dd/MM/yyyy") + "', 'dd/MM/yyyy')").append(" ,");			
			sbquery.append("TO_DATE('" + GestorFechas.formatDate(objPeticion.getCopaFvalor3(), "dd/MM/yyyy") + "', 'dd/MM/yyyy')").append(")");
			
			query = getSession().createSQLQuery(sbquery.toString());                     			                                                                        	                          							                                                                
			query.executeUpdate();                        
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao#valoresBatch(java.lang.String)
	 */
	@Override
	public Parametros valoresBatch(String strTipo) throws Excepciones {
		StringBuilder query = null;
    	Parametros param;
    	ArrayList<Object> lstParam;
    	Iterator<Object> itParam;
    	Object[] arrDatos;
    	try {
    		query = new StringBuilder();
    		param = new Parametros();
    		lstParam = new ArrayList<Object>();
    		
    		query.append("SELECT cp.COPA_NVALOR1,cp.COPA_VVALOR2,cp.COPA_VVALOR3, cp.COPA_REGISTRO \n");
    		query.append("  FROM COLECTIVOS_PARAMETROS CP \n");
    		query.append("WHERE CP.COPA_ID_PARAMETRO = '").append(strTipo).append("' \n");
    		query.append("  AND CP.COPA_DES_PARAMETRO = 'PROCESO_BATCH' \n");    		
    		
    		lstParam.addAll(getSession().createSQLQuery(query.toString()).list());
    		if(!lstParam.isEmpty()){
    			itParam = lstParam.iterator();
    			while(itParam.hasNext()){
    				arrDatos = (Object[]) itParam.next();
    				param.setCopaVvalor2(arrDatos[1].toString());
    				param.setCopaVvalor3(arrDatos[2].toString());	
    				param.setCopaNvalor1(((BigDecimal)arrDatos[0]).intValue());
    				param.setCopaRegistro(arrDatos[3].toString());
    			}
    		}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error al recuperar siguiente cd colectivos_parametros", e);
		}
    	
    	return param;
	}

	@Override
	public boolean recibosPendCobro(Short canal, Short ramo, Long poliza)  throws Excepciones {
		boolean bResultado = false;
		Integer nResultado = Constantes.DEFAULT_INT;
		StringBuilder sbConsulta;
		Query qry; 
		List lista; 
		
		try {
			sbConsulta = new StringBuilder();
			sbConsulta.append("SELECT COUNT(*) \n");
			sbConsulta.append("	FROM COLECTIVOS_RECIBOS CR \n");
			sbConsulta.append("WHERE CR.CORE_CASU_CD_SUCURSAL	= :canal \n");
			sbConsulta.append("	AND CR.CORE_CARP_CD_RAMO   		= :ramo \n");
			sbConsulta.append("	AND CR.CORE_CAPO_NU_POLIZA		= :poliza \n");
			sbConsulta.append(" AND CR.CORE_ST_RECIBO      		=  1 \n");
			
			qry = this.getSession().createSQLQuery(sbConsulta.toString());
			qry.setParameter("canal", canal);
			qry.setParameter("ramo", ramo);
			qry.setParameter("poliza", poliza);
			lista = qry.list();
			
			if(!lista.isEmpty()) {
				nResultado = ((BigDecimal) lista.get(0)).intValue();
				if(nResultado > 0) {
					bResultado = true;
				}
			} 
		} catch (Exception e) {
			throw new Excepciones("Error: Al recuperar ultimo recibo. ", e);
		}
		
		return bResultado;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao#descripcionErrorMensaje(mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCfdiErrorW)
	 */
	@Override
	public CartCfdiErrorW descripcionErrorMensaje(CartCfdiErrorW filtro) throws Excepciones {
		
		CartCfdiErrorW res = new CartCfdiErrorW();
		
		StringBuilder sbConsulta = new StringBuilder();
		Query qry;

		sbConsulta.append("SELECT CCEW_DESCRIPCION  \n");
		sbConsulta.append("	FROM CART_CFDI_ERROR_WS  \n");
		sbConsulta.append("WHERE CCEW_CODIGO = :codigo \n");
		sbConsulta.append("and CCEW_TIPO_OPERACION = :tipoOper  \n");

		try {
			qry = this.getSession().createSQLQuery(sbConsulta.toString());
			qry.setParameter("codigo", filtro.getCcewCodigo() );
			qry.setParameter("tipoOper", filtro.getCcewTipoOperacion());

			String resQry = (String) qry.uniqueResult();
			res.setCcewDescripcion(resQry);
			
		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error consulta consecutivo seguimiento", e);
		}
		
		
		
		return res;
	}

	@Override
	public void actualizaEstatusRecibo(CertificadoId id)  throws Excepciones {
		StringBuilder actRec = new StringBuilder(300);
		Query queryAct;

		try {
			actRec.append("update ColectivosRecibo rec \n"); 
			actRec.append("		set rec.coreStRecibo = 7, rec.coreTpTramite = '7' \n");
			actRec.append("where rec.id.coreCasuCdSucursal = :canal \n");
			actRec.append("	and rec.coreCarpCdRamo         = :ramo \n");
			actRec.append("	and rec.coreCapoNuPoliza       = :poliza \n");
			actRec.append("	and rec.coreStRecibo = 1 \n" );
									
			queryAct = getSession().createQuery(actRec.toString()); 
			queryAct.setParameter("canal", id.getCoceCasuCdSucursal());
			queryAct.setParameter("ramo", id.getCoceCarpCdRamo().byteValue());
			queryAct.setParameter("poliza", id.getCoceCapoNuPoliza().intValue());
			queryAct.executeUpdate();		

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error actualizar estatus recibo", e);
		} 
	}

	//------------------------------------------------------------------------------------------------------------------
	/**
	 * AUTHOT: Towa (JJFM)
	 * ExportarReporteDxP
	 * @param Short canal,  canal		
	 * @param Short ramo,  Ramo			
	 * @param long poliza1,  poliza	
	 * @param String fecha1,  Filtro Fecha desde
	 * @param String fecha2, 	 Filtro Fecha Hasta	
	 * @param Integer inIdVenta,	 id venta 
	 * @param boolean boolFiltroVigente,  Filtro Vigente 				
	 * @param boolean boolFiltroCancelado, Filtro Cancelado						
	 * @param boolean boolFiltroEmitido,  Filtro Emitido
	 * @param String rutaReportes		 ruta del reporte
	 * @return File - archivo con el reporte DxP.
	 * @throws Excepciones
	 */
	public File ExportarReporteDxP(
			//												//canal
			Short canal, 
			//												//Ramo
			Short ramo, 
			//												//poliza
			long poliza1, 
			//												//Filtro Fecha desde
			String fecha1, 
			//												//Filtro Fecha Hasta
			String fecha2, 
			//												//id venta
			Integer inIdVenta, 
			//												//Filtro Vigente 
			boolean boolFiltroVigente, 
			//												//Filtro Cancelado
			boolean boolFiltroCancelado,
			//												//Filtro Emitido
			boolean boolFiltroEmitido, 
			//												//ruta del reporte
			String rutaReportes,
			Short pCuota,
			int  filtro
			
	) throws Excepciones {
		
		try {	
			
			String fec = this.strObtenerFechaEnFormato("yyyyMMdd");

		    String strFileName = "DXP_" +  fec +  ".csv" ;
		
		    String strRutaReporte = rutaReportes.concat(strFileName);
			File reporte = new File(strRutaReporte);
			OutputStreamWriter  filew = new OutputStreamWriter (new FileOutputStream(reporte), Charset.defaultCharset());
			CSVWriter writer = new CSVWriter(filew);
			
			//												//Se agrega el encabezado que tendra el archivo separados
			//												//	por pipes(|).
			
			String[] arrstrHeaders = {
					"Ramo|CentroCostos|Poliza|Recibo|Credito|Certificado|IdCuota|Status|StatusMov|"
					+ "Cuenta|Producto|SubProducto|Sucursal|ApellidoPaterno|Apellido Materno|Nombre|"
					+ "Sexo|FechaNacimiento|BUC|RFC|Plazo|FechaInicioPoliza|FechaFinPoliza|FechaIngreso|"
					+ "FechaDesde|FechaHasta|FechaInicioCredito|FechaFinCredito|FechaFinCredito2|FechaAnulacion|"
					+ "FechaCancelacion|SumaAsegurada|BaseCalculo|PrimaNeta|Derechos|Recargos|IVA|PrimaTotal|Tarifa|"
					+ "PrimaVida|PrimaDesempleo|CDEstado|Estado|Del/mun|CP|MontoDevolucion|MontoDevolucionSistema|"
					+ "Diferencia Devolucion|CreditoNuevo|ProductoAseguradora|PlanAseguradora|CuotaBasica|CuotaDesempleo|"
					+ "DatosOS|Fecha de Emision|Fecha de Envio PAMPA|Fecha de Carga VENTA|Fecha de Recibido PAMPA|"
					+ "DIFERENCIA_PNETA|DIFERENCIA_PTOTAL|STATUS_CRUCE|Prima_total_PNETA|Prima_total_PTOTAL|"
					+ "COBRADO_PNETA|COBRADO_TOTAL|Factura_acum_PNETA|Factura_acum_PTOTAL|DXP_Exigible_PNETA|"
					+ "DXP_Exigible_PTOTAL|DXP_No_Exig_PNETA|DXP_No_Exig_PTOTAL|Deudor_Total_PNETA|Deudor_Total_PTOTAL|STATUS_PAGO"
									};
					 
			writer.writeNext(arrstrHeaders, false);
			
			//												//Se hace consulta a base de datos para obtener los datos
			//												//	y agregarlos en el reporte.
			//												//La consulta ya genera el formato adecuado con los datos
			//												//	separados por pipes(|) en cada fila.
			
			String strConsulta = this.strObtenerQuery(canal, ramo, poliza1, 0, fecha1, fecha2, inIdVenta,
					boolFiltroVigente, boolFiltroCancelado, boolFiltroEmitido, pCuota, filtro);

			Log.info(strConsulta);
			Query qry = this.getSession().createSQLQuery(strConsulta);
			
			List darrobj = qry.list();
			
			
			ArrayList<String[]> darrstrResults = new ArrayList<String[]>();
			
			Iterator<Object> it = darrobj.iterator();
			
			while(it.hasNext()) {
				//									//To easy code
				String[] arrDatos = {(String) it.next()};
				darrstrResults.add(arrDatos);
				
			}
			
			writer.writeAll(darrstrResults, false);	
			writer.close();
				
			
			return reporte;
		    
			
		} catch (Excepciones e) {
			throw new Excepciones("Error.", e);
		} catch (DateParseException dateEx) {
			throw new Excepciones("DateParseError", dateEx);
		} catch (IOException e) {
			throw new Excepciones("Error al generar reporte", e);
		}
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	/**
	 * AUTHOT: Towa (JJFM)
	 * Description: Metodo de apoyo para generar query de consulta reporte DXP
	 * @throws Excepciones
	 */
	private String strObtenerQuery(
			//												//canal
			Short canal, 
			//												//Ramo
			Short ramo, 
			//												//Poliza seleccionada, en caso de ser Prima Unica este 
			//												//	valor llegara igual a 0;
			long polizaSeleccionada, 
			//												//Poliza especifica en caso de ser requerido. Puede ser 0.
			long polizaEspecifica, 
			//												//Filtro Fecha desde
			String fecha1, 
			//												//Filtro Fecha Hasta
			String fecha2, 
			//												//id venta
			Integer inIdVenta, 
			//												//Filtro Vigente 
			boolean boolFiltroVigente, 
			//												//Filtro Cancelado
			boolean boolFiltroCancelado,
			//												//Filtro Emitido
			boolean boolFiltroEmitido
			//												//ruta del reporte
			,
			Short pCuota,
			int pFiltro
	) throws Excepciones {
		
		StringBuilder sb = new StringBuilder();
		Archivo objArchivo;
		
		try {
			
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("ReporteDXP.tst");
			
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_SUCURSAL_DXP), String.valueOf(canal));
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_RAMO_DXP), String.valueOf(ramo));
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_POLIZA_DXP), String.valueOf(polizaSeleccionada));
			Utilerias.replaceAll(sb,  Pattern.compile("pjCuota"), String.valueOf(pCuota));
			Utilerias.replaceAll(sb,  Pattern.compile("pjFeDesde"), fecha1);
			Utilerias.replaceAll(sb,  Pattern.compile("pjFeHasta"), fecha2);
			
		}  catch (Exception e) {
			e.printStackTrace();
		} 
		
		return sb.toString();

	}
	
	/**
	 *@author JuanFlores
	 *obtiene una Lista de polizas dependniendo de la poliza seleccionada
	 *@param long polizaSeleccionada - polizaSeleccionada
	 *@param short canal - canal
	 *@param short ramo - ramo
	 *@param  int inIdVentain - IdVenta
	 *@return List<Parametros> lista con polizas
	 *@throws Excepciones
	 * 
	 **/
	private List<Parametros> listObtenerListaPolizas(long polizaSeleccionada, 
			short canal, short ramo, int inIdVenta) throws Excepciones {
	
			StringBuilder filtro = new StringBuilder();
			if (
					polizaSeleccionada  == 0
			) {
				filtro.append("  and P.copaDesParametro = 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro  = 'GEP' \n");
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor6 = ").append(inIdVenta).append("\n");
			} else {
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro.append("  and P.copaNvalor1 = ").append(canal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				filtro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(polizaSeleccionada).append(", 1, substr(").append(polizaSeleccionada).append(", 0, 3), substr(").append(polizaSeleccionada).append(", 0, 2)) \n");
				filtro.append("order by P.copaNvalor3");
				
			}
			List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
			return listaPolizas;

	} 
	
	/**
	 *@author JuanFlores
	 *obtiene cadena para Facturacion
	 *@param long polizaSeleccionada - polizaSeleccionada
	 *@param long polizaEspecifica - polizaEspecifica
	 *@param List<Parametros> listaPolizas - lista de polizas
	 *@returnString con filtro para facturacion
	 * 
	 **/
	private String strObtenerCadenaFac(long polizaSeleccionada, long polizaEspecifica, List<Parametros> listaPolizas) {
		String cadenafac = "";
		if(
				//											//PRIMA UNICA (0)
				polizaSeleccionada == 0 
		){
			
			int inicioPoliza = -1;
			if (
					listaPolizas != null && 
					!listaPolizas.isEmpty()
			) {
				inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
			}
			
			cadenafac = "  and fac.cofa_capo_nu_poliza >= " + inicioPoliza + "00000000 and fac.cofa_capo_nu_poliza <= " + inicioPoliza + "99999999 \n";
			
		} else {
			
			for(Parametros item: listaPolizas){
									
				if (
						item.getCopaNvalor4().toString().equals("1") &&
						polizaEspecifica==0
				){
					cadenafac="  and fac.cofa_capo_nu_poliza like '" + polizaSeleccionada + "%'";
					
					
				}else if (
						item.getCopaNvalor4().toString().equals("1")
				) {
					cadenafac="  and fac.cofa_capo_nu_poliza like '" + polizaEspecifica + "%'";
				} else{
					cadenafac="  and fac.cofa_capo_nu_poliza = " + polizaSeleccionada;
				}
			}
		}
		return cadenafac;
	}
	
	/**
	 *@author JuanFlores
	 *obtiene una Lista de polizas dependniendo de la poliza seleccionada
	 *@param long polizaSeleccionada - polizaSeleccionada
	 *@param long polizaEspecifica - polizaEspecifica
	 *@param List<Parametros> listaPolizas - lista de polizas
	 *@param  int inIdVentain - IdVenta
	 *@return String cadena para filtro de cancelados
	 *@throws Excepciones
	 * 
	 **/
	private String strObtenerCadenaCan(long polizaSeleccionada, long polizaEspecifica, List<Parametros> listaPolizas, int inIdVenta) {
		String cadenacan = "";
		if(
				//											//PRIMA UNICA (0)
				polizaSeleccionada == 0 
		){
			
			int inicioPoliza = -1;
			if (
					listaPolizas != null && 
					!listaPolizas.isEmpty()
			) {
				inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
			}
			
			cadenacan = "  and c.coce_capo_nu_poliza >= " + inicioPoliza + "00000000 and c.coce_capo_nu_poliza <= " + inicioPoliza + "99999999 \n"
			         +"  and c.coce_sub_campana      = '" + inIdVenta + "' 				        \n";
		} else {
			
			for(Parametros item: listaPolizas){
									
				if (
						item.getCopaNvalor4().toString().equals("1") &&
						polizaEspecifica==0
				){
					cadenacan="  and c.coce_capo_nu_poliza like '" + polizaSeleccionada + "%'";
					
				}else if (
						item.getCopaNvalor4().toString().equals("1")
				) {
					cadenacan="  and c.coce_capo_nu_poliza like '" + polizaEspecifica + "%'";
				}else{
					cadenacan="  and c.coce_capo_nu_poliza = " + polizaSeleccionada;
				}
			}
		}
		return cadenacan;
	}
	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * @author: Towa (JJFM)
	 * UnionFiltroVigente
	 * @return strUnionFiltroVigente
	 */
	private String strUnionFiltroVigente(
			long polizaSeleccionada,
			//												//canal
			Short canal, 
			//												//Ramo
			Short ramo, 
			//												//Filtro Fecha desde
			String strFechaFacturacionDesde, 
			//												//Filtro Fecha Hasta
			String strFechaFacturacionHasta,
			//												//id venta
			Integer inIdVenta,
			//												//Filtro Facturacion
			String cadenafac,
			Short pCuota,
			int pFiltro
	) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(this.strQueryFiltroVigente1( pCuota, pFiltro));
		

		//												//poliza1 > 0
		if (
				polizaSeleccionada > 0
		){
			sb.append( strQueryFiltroVigente2());
			
		}else{
			sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
			sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
		}
		
		sb.append( strQueryFiltroVigente3() );
		
		if (
				polizaSeleccionada > 0
		) {
		  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
		}
		
		sb.append(strQueryFiltroVigente4());
		
		if (
				polizaSeleccionada > 0
		) {
			sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
		}
		
		sb.append(strQueryFiltroVigente5());
		
		if ( 
				ramo == 9 && 
				inIdVenta == 6
		) {		/*APV*/								
			sb.append("	c.coce_fe_emision FECHAEMISION, 											\n"
			+"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n"
			+"  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n"
			+"	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
		} else {																		
			sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
		}	 /*APV*/
		
		sb.append(this.strQueryFiltroVigente6( canal,  ramo,  cadenafac,  strFechaFacturacionDesde,
				 strFechaFacturacionHasta, pCuota, pFiltro));				
		if (pFiltro != 1) {
		if (
				polizaSeleccionada != 0
		){
			sb.append(this.strQueryFiltroVigente7() );
			
		}else{
			sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
			sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
		}			
		
		sb.append(this.strQueryFiltroVigente8()); 
		if (polizaSeleccionada != 0) {
		   sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n"); 
		}
		 sb.append(this.strQueryFiltroVigente9());
		if (polizaSeleccionada != 0) {
		   sb.append("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n"); 
		 }
		 sb.append(this.strQueryFiltroVigente10() );
		
		if ( 
				ramo == 9 && 
				inIdVenta == 6
		) {		/*APV*/								
			sb.append("	c.cocm_fe_emision FECHAEMISION, 											\n");
			sb.append("	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeEn:',1,1) + 5,instr(c.cocm_campoV6, '|',1,6)-instr(c.cocm_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
			sb.append("  substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeCar:',1,1) + 6,instr(c.cocm_campoV6, '|',1,8)-instr(c.cocm_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
			sb.append("	 substr(c.cocm_campoV6,instr(c.cocm_campoV6, 'FeRe:',1,1) + 5,instr(c.cocm_campoV6, '|',1,7)-instr(c.cocm_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
		} else {																		
			sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
		}	 /*APV*/
		sb.append(this.strQueryFiltroVigente11( canal,  ramo,  cadenafac,  strFechaFacturacionDesde,
				 strFechaFacturacionHasta));
	    
		}
	    return sb.toString();
	    
	}
	
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente1(Short pCuota,
			int pFiltro) {
		String res = "select c.coce_carp_cd_ramo       RAMO 									\n"
				+"       ,pl.alpl_dato3            CENTRO_COSTOS 							\n"
				+"       ,c.coce_capo_nu_poliza    POLIZA 									\n"
				+"       ,c.coce_no_recibo         RECIBO 									\n"
				+"       ,cc.cocc_id_certificado   CREDITO 									\n"
				+"       ,c.coce_nu_certificado    CERTIFICADO 								\n"
				+"       ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n"
				+"       ,est.ales_campo1		   ESTATUS   				 				\n";
				 if (pFiltro == 1) {
					 res +="       ,CASE WHEN C.COCE_ST_CERTIFICADO = 10 THEN 'BAJA'  WHEN C.COCE_ST_CERTIFICADO = 11 THEN 'BAJA'  ELSE 'ALTA' END ESTATUS_MOVIMIENTO 						\n";
				 } else {
					 res +="       ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n";
				 }
				 res +="       ,c.coce_nu_cuenta         CUENTA 									\n"
				+"       ,c.coce_tp_producto_bco   PRODUCTO 								\n"
				+"       ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n"
				+"       ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n"
				+"       ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n"
				+"       ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n"
				+"       ,cl.cocn_nombre           NOMBRE 									\n"
				+"       ,cl.cocn_cd_sexo          SEXO 									\n"
				+"       ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n"
				+"       ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n"
				+"       ,cl.cocn_rfc              RFC 										\n"
				+"       ,c.coce_cd_plazo          PLAZO 									\n";
				return res;
	}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente2() {
		return ", (select min(re.core_fe_desde)										            \n"
		    +"     from colectivos_recibos re												    \n"
		    +"     where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	            \n"
		    +"       and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		            \n"
		    +"       and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	            \n"
		    +"       and re.core_cace_nu_certificado     = 0						            \n"
		    +"       and re.core_st_recibo               in (1,2,4)					            \n"
		    +"       and re.core_nu_consecutivo_cuota    = 1						            \n"
		    +"       and re.core_fe_desde                <= r.core_fe_desde			            \n"
		    +"       and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_INICIO_POLIZA	\n"
		    +", (select add_months(min(re.core_fe_desde),12)						            \n"
		    +"     from colectivos_recibos re												    \n"
		    +"     where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	            \n"
		    +"       and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		            \n"
		    +"       and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	            \n"
		    +"       and re.core_cace_nu_certificado     = 0						            \n"
		    +"       and re.core_st_recibo               in (1,2,4)					            \n"
		    +"       and re.core_nu_consecutivo_cuota    = 1						            \n"
		    +"       and re.core_fe_desde                <= r.core_fe_desde			            \n"
		    +"       and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_FIN_POLIZA	\n";
	}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente3() { 
		return "      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n" 
			+"      ,c.coce_fe_desde          FECHA_DESDE 								\n" 
			+"      ,c.coce_fe_hasta          FECHA_HASTA 								\n" 
			+"      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n" 
			+"      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n" 
			+"      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n" 
			+"      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n" 
			+"      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n" 
			+"      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n" 
			+"      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n" 
			//												//Case PRIMA NETA
			+"      ,case when pf.copa_nvalor5 = 0 									\n"  
			+"                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"  
			+"            when pf.copa_nvalor5 = 1										\n"  
			+"                     then c.coce_mt_prima_pura							\n"  
			+"            else 0 end 			PRIMA_NETA								\n" 
			+"      -- \n" 
			
			+"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n" 
			+"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n" 
			+"      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n" 
			
			//												//CASE PRIMA TOTAL
			+"      ,case when pf.copa_nvalor5 = 0 then \n"  
			+"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"  
			+"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"  
			+"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"  
			+"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"  
			+"                when pf.copa_nvalor5 = 1 then \n"  
			+"                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"  
			+"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"  
			+"                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"  
			+"                   + nvl(c.coce_mt_prima_pura,0) \n"  
			+"                else 0 end 		PRIMA_TOTAL  							 \n" 
			
			
			+"      , round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n" 
			+"      -- \n" 
			
			//												//PRIMA VIDA
			+"      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n" 
			+"      -- \n"       
			+"            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n" 
			+"              from colectivos_coberturas cob 							\n" 
			+"             where cob.cocb_casu_cd_sucursal =  1 						\n" 
			+"               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo		\n" 
			+"               and cob.cocb_carb_cd_ramo     = 14 						\n";
	}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente4() { 
		return "               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n" 
			 +"               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n" 
			 +"               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n" 
			 +"               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n" 
			 +"            end,2) PRIMA_VIDA 											\n" 
			
			//												//PRIMA DESEMPLEO
			 +"      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n" 
			 +"            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n" 
			 +"              from colectivos_coberturas cob 							\n" 
			 +"             where cob.cocb_casu_cd_sucursal =  1 						\n" 
			 +"               and cob.cocb_carp_cd_ramo     = 61 						\n" 
			 +"               and cob.cocb_carb_cd_ramo     = 14 						\n" ;
		}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente5() { 
		return "               and cob.cocb_cacb_cd_cobertura  = '003' 					\n" 
			 +"               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n" 
			 +"               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n" 
			 +"               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n" 
			 +"            end,2) PRIMA_DESEMPLEO 										\n" 
			
			//												//
			 +"      ,cl.cocn_cd_estado        CD_ESTADO 								\n" 
			 +"      ,es.caes_de_estado        ESTADO 									\n" 
			 +"      ,cl.cocn_delegmunic       MUNICIPIO 								\n" 
			 +"      ,cl.cocn_cd_postal        CP 										\n" 
			 +"      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n" 
			 +"      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n" 
			 +"      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n" 
			 +"      ,nvl(c.coce_buc_empresa,0)		  CREDITONUEVO			     		\n" 
			 +",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"  
			 +",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n"  
			
			//												//CUOTA BASICA
			 +",(select sum (cob.cocb_ta_riesgo)                         \n"  
			 +"        from colectivos_coberturas cob                    \n"  
			 +"       where cob.cocb_casu_cd_sucursal =  1               \n"  
			 +"         and cob.cocb_carp_cd_ramo     = 61               \n"  
			 +"         and cob.cocb_carb_cd_ramo     = 14               \n"  
			 +"         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"  
			 +"         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"  
			 +"         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"  
			 +"         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n"  
			
			//												//CUOTA DESEMPLEO
			 +",(select sum (cob.cocb_ta_riesgo)                                            \n"  
			 +"        from colectivos_coberturas cob                                       \n"  
			 +"       where cob.cocb_casu_cd_sucursal =  1                                  \n"  
			 +"         and cob.cocb_carp_cd_ramo     = 61                                  \n"  
			 +"         and cob.cocb_carb_cd_ramo     = 14                                  \n"  
			 +"         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"  
			 +"         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"  
			 +"         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"  
			 +"         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n"  
			
			//												//DATOS_OBLIGADO_PU
			 +",case when pf.copa_nvalor4 > 0 then                                          \n"  
			 +"   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"  
			 +"           cln.cocn_cd_sexo         ||'|'||                                  \n"  
			 +"           clc.cocc_tp_cliente      ||'|'||                                  \n"  
			 +"           p.copa_vvalor1           ||'|'||                                                 \n"  
			 +"           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"  
			 +"      from colectivos_cliente_certif clc                                                    \n"  
			 +"          ,colectivos_clientes       cln                                                    \n"  
			 +"          ,colectivos_parametros     p                                       \n"  
			 +"     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n"  
			 +"       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n"  
			 +"       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n"  
			 +"       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n"  
			 +"       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"  
			 +"       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"  
			 +"       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"  
			 +"       and p.copa_id_parametro(+)    = 'TIPO'                                \n"  
			 +"       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"  
			 +"   else '' end DATOS_OBLIGADO_PU,                                             \n" ;
		}
	
	/**
	 * @author JuanFlores
	 * @param short canal - canal de venta
	 * @param short ramo - ramo
	 * @param String cadenafac - cadena facturacion
	 * @param String strFechaFacturacionDesde - Fecha facturacion desde
	 * @param String strFechaFacturacionHasta - fecha facturacion hasta
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente6(short canal, short ramo, String cadenafac, String strFechaFacturacionDesde,
			String strFechaFacturacionHasta,
			Short pCuota,
			int pFiltro) { 
		String res =  ", c.COCE_MT_PRIMA_PURA Prima_total_PNETA  \n" 
			 +" , c.COCE_MT_PRIMA_SUBSECUENTE Prima_total_PTOTAL  \n" 
			 +" , c.COCE_NU_CUOTA \n" 

			//												//FROM-JOIN
			 +"  from colectivos_certificados   c 										\n" 
			 +"      ,colectivos_cliente_certif cc 										\n" 
			 +"      ,colectivos_clientes       cl 										\n" 
			 +"      ,colectivos_facturacion    fac 									\n" 
			 +"      ,colectivos_recibos   		r 									    \n" 
			 +"      ,cart_estados              es 										\n" 
			 +"      ,alterna_estatus           est 									\n" 
			 +"      ,colectivos_certificados   head 									\n" 
			 +"      ,alterna_planes            pl 										\n" 
			 +"      ,colectivos_parametros     pf										\n" 
			
			//												//WHERE
			 +"where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n" 
			 +"  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n" 
			 +cadenafac 
			 +"  and fac.cofa_fe_facturacion    >= to_date('" + strFechaFacturacionDesde + "','dd/mm/yyyy') \n" 
			 +"  and fac.cofa_fe_facturacion    <= to_date('" + strFechaFacturacionHasta + "','dd/mm/yyyy') \n" 
			 +"  -- \n" 
			 +"  and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n" 
			 +"  and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n" 
			 +"  and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n" 
			 +"  and c.coce_nu_certificado   > 0 										\n" 
			 +"  and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal 				\n";
			 if (pFiltro == 1) {
				 res += "  -- \n";
			 }	else {
				 res +="  and c.coce_campon2          in (0,2008) 				                \n"; 
						 res +="  -- \n" ;
			 }
			 res += "  and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n" 
		     +"  and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n" 
			 +"  and r.core_st_recibo        in (1,4)									\n" 
			 +"  -- \n" 
			 +"  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n" 
			 +"  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n" 
			 +"  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n" 
			 +"  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n" 
			 +"  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n" 
			 +"  -- \n" 
			 +"  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n" 
			 +"  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n" 
			 +"  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n" 
			 +"  and head.coce_nu_certificado   = 0 									\n" 
			 +"  -- \n" 
			 +"  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n" 
			 +"  -- \n" 
			 +"  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n" 
			 +"  -- \n" 
			 +"  and est.ales_cd_estatus      = c.coce_st_certificado 					\n" 
			 +"  -- \n" 
			 +"  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n" 
			 +"  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n" 
			 +"  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n" 
			 +"--																		\n" 
		     +"  and pf.copa_des_parametro    = 'POLIZA'								\n" 
		     +"  and pf.copa_id_parametro     = 'GEPREFAC'								\n" 
		     +"  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n" 
		     +"  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n" 
		     +"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, fac.cofa_capo_nu_poliza, 1, substr(fac.cofa_capo_nu_poliza,0,3), substr(fac.cofa_capo_nu_poliza,0,2)) \n" 
		     +"  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n" 
		    
		    //												//UNION...
		     +"\n" ;
		      if (pFiltro != 1) {
		    	  res += "union all \n" 
		     +"select c.cocm_carp_cd_ramo 		  	RAMO 									\n"
		    		+"           ,pl.alpl_dato3            CENTRO_COSTOS 							\n"
		    		+"           ,c.cocm_capo_nu_poliza    POLIZA 									\n"
		    		+"           ,c.cocm_nu_recibo         RECIBO 									\n"
		    		+"           ,cc.cocc_id_certificado   CREDITO 									\n"
		    		+"           ,c.cocm_nu_certificado    CERTIFICADO 								\n"
		    		+"           ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n"
		    		+"           ,est.ales_campo1		     ESTATUS   				 				\n"
		    		+"           ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n"
		    		+"           ,c.cocm_nu_cuenta         CUENTA 									\n"
		    		+"           ,c.cocm_tp_producto_bco   PRODUCTO 								\n"
		    		+"           ,c.cocm_tp_subprod_bco    SUBPRODUCTO 								\n"
		    		+"           ,c.cocm_cazb_cd_sucursal  SUCURSAL 								\n"
		    		+"           ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n"
		    		+"           ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n"
		    		+"           ,cl.cocn_nombre           NOMBRE 									\n"
		    		+"           ,cl.cocn_cd_sexo          SEXO 									\n"
		    		+"           ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n"
		    		+"           ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n"
		    		+"           ,cl.cocn_rfc              RFC 										\n"
		    		+"           ,c.cocm_cd_plazo          PLAZO 									\n"		;
		      }
		return res;
		}
	
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente7() { 
		return ",(select min(re.core_fe_desde)										\n" 
		     +"    from colectivos_recibos re												\n" 
		     +"    where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	\n" 
		     +"      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n" 
		     +"      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n" 
		     +"      and re.core_cace_nu_certificado     = 0						\n" 
		     +"      and re.core_st_recibo               in (1,2,4)					\n" 
		     +"      and re.core_nu_consecutivo_cuota    = 1						\n" 
		     +"      and re.core_fe_desde                <= r.core_fe_desde			\n" 
		     +"      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_INICIO_POLIZA	\n" 
		     +",(select add_months(min(re.core_fe_desde),12)						\n" 
		     +"    from colectivos_recibos re												\n" 
		     +"    where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	\n" 
		     +"      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n" 
		     +"      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n" 
		     +"      and re.core_cace_nu_certificado     = 0						\n" 
		     +"      and re.core_st_recibo               in (1,2,4)					\n" 
		     +"      and re.core_nu_consecutivo_cuota    = 1						\n" 
		     +"      and re.core_fe_desde                <= r.core_fe_desde			\n" 
		     +"      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_FIN_POLIZA	\n";
	}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente8() {
		return "      ,c.cocm_fe_suscripcion    FECHA_INGRESO 							\n" 
			 +"      ,c.cocm_fe_desde          FECHA_DESDE 								\n" 
			 +"      ,c.cocm_fe_hasta          FECHA_HASTA 								\n" 
			 +"      ,c.cocm_fe_ini_credito    FECHA_INICIO_CREDITO 					\n" 
			 +"      ,c.cocm_fe_fin_credito    FECHA_FIN_CREDITO 						\n" 
			 +"      ,c.cocm_campov6		   FECHA_FIN_CREDITO_2 						\n" 
			 +"      ,c.cocm_fe_anulacion_real FECHA_ANULACION 							\n" 
			 +"      ,c.cocm_fe_anulacion      FECHA_CANCELACION 						\n" 
			 +"      ,trim(to_char(decode(c.cocm_sub_campana,'7',to_number(c.cocm_mt_suma_aseg_si),c.cocm_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n" 
			 +"      ,decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si) BASE_CALCULO 							\n" 
			 +"      ,case when pf.copa_nvalor5 = 0 									\n"  
			 +"                     then nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"  
			 +"                when pf.copa_nvalor5 = 1									\n"  
			 +"                      then c.cocm_mt_prima_pura							\n"  
			 +"                else 0 end PRIMA_NETA									\n" 
			 +"      -- \n" 
			 +"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1) -instr(c.cocm_di_cobro1, 'O',1,1)-1)) DERECHOS \n" 
			 +"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2) -instr(c.cocm_di_cobro1, 'I',1,1)-1)) RECARGOS  \n" 
			 +"      ,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)) IVA \n" 
			 +"      ,case when pf.copa_nvalor5 = 0 then \n"  
			 +"                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n"  
			 +"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n"  
			 +"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n"  
			 +"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"  
			 +"                when pf.copa_nvalor5 = 1 then \n"  
			 +"                     nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) \n"  
			 +"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) \n"  
			 +"                   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) \n"  
			 +"                   + nvl(c.cocm_mt_prima_pura,0) \n"  
			 +"                else 0 end PRIMA_TOTAL  									 \n" 
			 +"      ,round(c.cocm_mt_prima_pura*1000/decode(c.cocm_sub_campana,'7',decode(c.cocm_mt_suma_asegurada,0,1,c.cocm_mt_suma_asegurada),decode(c.cocm_mt_suma_aseg_si,0,1,c.cocm_mt_suma_aseg_si)),4) TARIFA \n" 
			 +"      -- \n" 
			 +"      , round(case when c.cocm_carp_cd_ramo in (61,65) then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n" 
			 +"      -- \n"       
			 +"            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n" 
			 +"              from colectivos_coberturas cob 							\n" 
			 +"             where cob.cocb_casu_cd_sucursal =  1 						\n" 
			 +"               and cob.cocb_carp_cd_ramo     = c.cocm_carp_cd_ramo 		\n" 
			 +"               and cob.cocb_carb_cd_ramo     = 14 						\n";
		}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente9() {
		return "               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n" 
			 +"               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n" 
			 +"               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n" 
			 +"               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n" 
			 +"            end,2) PRIMA_VIDA 											\n" 
			 +"      , round(case when c.cocm_carp_cd_ramo = 61 then (decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * \n" 
			 +"            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n" 
			 +"              from colectivos_coberturas cob 							\n" 
			 +"             where cob.cocb_casu_cd_sucursal =  1 						\n" 
			 +"               and cob.cocb_carp_cd_ramo     = 61 						\n" 
			 +"               and cob.cocb_carb_cd_ramo     = 14 						\n" ;
		}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente10() { 
		return "               and cob.cocb_cacb_cd_cobertura  = '003' 					\n" 
			 +"               and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto \n" 
			 +"               and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan 	\n" 
			 +"               and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) 	\n" 
			 +"            end,2) PRIMA_DESEMPLEO 										\n" 
			 +"      ,cl.cocn_cd_estado        CD_ESTADO 								\n" 
			 +"      ,es.caes_de_estado        ESTADO 									\n" 
			 +"      ,cl.cocn_delegmunic       MUNICIPIO 								\n" 
			 +"      ,cl.cocn_cd_postal        CP 										\n" 
			 +"      ,nvl(c.cocm_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n" 
			 +"      ,nvl(c.cocm_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n" 
			 +"      ,abs( nvl(c.cocm_mt_devolucion ,0) - nvl(c.cocm_mt_bco_devolucion ,0) ) DIFERENCIA_DEVOLUCION \n" 
			 +"      ,nvl(c.cocm_buc_empresa,0)		  CREDITONUEVO			     		\n" 
			 +",c.cocm_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"  
			 +",c.cocm_capb_cd_plan             PLAN_ASEGURADORA         \n"  
			 +",(select sum (cob.cocb_ta_riesgo)                         \n"  
			 +"        from colectivos_coberturas cob                    \n"  
			 +"       where cob.cocb_casu_cd_sucursal =  1               \n"  
			 +"         and cob.cocb_carp_cd_ramo     = 61               \n"  
			 +"         and cob.cocb_carb_cd_ramo     = 14               \n"  
			 +"         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"  
			 +"         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n"  
			 +"         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n"  
			 +"         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_BASICA \n"  
			 +",(select sum (cob.cocb_ta_riesgo)                                            \n"  
			 +"        from colectivos_coberturas cob                                       \n"  
			 +"       where cob.cocb_casu_cd_sucursal =  1                                  \n"  
			 +"         and cob.cocb_carp_cd_ramo     = 61                                  \n"  
			 +"         and cob.cocb_carb_cd_ramo     = 14                                  \n"  
			 +"         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"  
			 +"         and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto           \n"  
			 +"         and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan               \n"  
			 +"         and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) CUOTA_DESEMPLEO  \n"  
			 +",case when pf.copa_nvalor4 > 0 then                                          \n"  
			 +"   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"  
			 +"           cln.cocn_cd_sexo         ||'|'||                                  \n"  
			 +"           clc.cocc_tp_cliente      ||'|'||                                  \n"  
			 +"           p.copa_vvalor1           ||'|'||                                                 \n"  
			 +"           substr(c.cocm_empresa,instr(c.cocm_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"  
			 +"      from colectivos_cliente_certif clc                                                    \n"  
			 +"          ,colectivos_clientes       cln                                                    \n"  
			 +"          ,colectivos_parametros     p                                       \n"  
			 +"     where clc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal               \n"  
			 +"       and clc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo                   \n"  
			 +"       and clc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza                 \n"  
			 +"       and clc.cocc_nu_certificado   = c.cocm_nu_certificado                 \n"  
			 +"       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"  
			 +"       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"  
			 +"       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"  
			 +"       and p.copa_id_parametro(+)    = 'TIPO'                                \n"  
			 +"       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"  
			 +"   else '' end DATOS_OBLIGADO_PU,                                             \n";
		}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro vigente
	 * 
	 * **/
	private String strQueryFiltroVigente11(
			short canal, short ramo, String cadenafac, String strFechaFacturacionDesde,
			String strFechaFacturacionHasta) { return " -- Prima_total_PNETA  \n" 
			 +", c.COCM_MT_PRIMA_PURA Prima_total_PNETA  \n" 
			 +" -- Prima_total_PTOTAL \n" 
			 +" , c.COCM_MT_PRIMA_SUBSECUENTE Prima_total_PTOTAL  \n" 
			 +" , head.COCE_NU_CUOTA \n" 
			
			 +"  from colectivos_certificados_mov c 										\n" 
			 +"      ,colectivos_cliente_certif cc 										\n" 
			 +"      ,colectivos_clientes       cl 										\n" 
			 +"      ,colectivos_facturacion    fac 									\n" 
			 +"      ,colectivos_recibos   			r 									    \n" 
			 +"      ,cart_estados              es 										\n" 
			 +"      ,alterna_estatus           est 									\n" 
			 +"      ,colectivos_certificados   head 									\n" 
			 +"      ,alterna_planes            pl 										\n" 
			 +"      ,colectivos_parametros     pf										\n" 
			 +"where fac.cofa_casu_cd_sucursal  = " + canal + " 						\n" 
			 +"  and fac.cofa_carp_cd_ramo      = " + ramo + " 							\n" 
			 +cadenafac 
			 +"  and fac.cofa_fe_facturacion    >= to_date('" + strFechaFacturacionDesde + "','dd/mm/yyyy') \n" 
			 +"  and fac.cofa_fe_facturacion    <= to_date('" + strFechaFacturacionHasta + "','dd/mm/yyyy') \n" 
			 +"  -- \n" 
			 +"  and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n" 
		     +"  and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n" 
			 +"  and r.core_st_recibo        in (1,4)									\n" 
			 +"  -- \n" 
			 +"  and c.cocm_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 				\n" 
			 +"  and c.cocm_carp_cd_ramo     = fac.cofa_carp_cd_ramo 					\n" 
			 +"  and c.cocm_capo_nu_poliza   = fac.cofa_capo_nu_poliza 					\n" 
			 +"  and c.cocm_nu_certificado   > 0 										\n" 
			 +"  and c.cocm_nu_recibo        = fac.cofa_nu_recibo_fiscal 				\n" 
			 +"  and c.cocm_campon2          in (0,2008) 				                \n" 
			 +"  -- \n" 
			 +"  and cc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n" 
			 +"  and cc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n" 
			 +"  and cc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza	 				\n" 
			 +"  and cc.cocc_nu_certificado   = c.cocm_nu_certificado 					\n" 
			 +"  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n" 
			 +"  -- \n" 
			 +"  and head.coce_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n" 
			 +"  and head.coce_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n" 
			 +"  and head.coce_capo_nu_poliza   = c.cocm_capo_nu_poliza 				\n" 
			 +"  and head.coce_nu_certificado   = 0 									\n" 
			 +"  -- \n" 
			 +"  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n" 
			 +"  -- \n" 
			 +"  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n" 
			 +"  -- \n" 
			 +"  and est.ales_cd_estatus      = c.cocm_st_certificado 					\n" 
			 +"  -- \n" 
			 +"  and pl.alpl_cd_ramo          = c.cocm_carp_cd_ramo 					\n" 
			 +"  and pl.alpl_cd_producto      = c.cocm_capu_cd_producto 				\n" 
			 +"  and pl.alpl_cd_plan          = c.cocm_capb_cd_plan  					\n" 
			 +"--																		\n" 
		     +"  and pf.copa_des_parametro    = 'POLIZA'								\n" 
		     +"  and pf.copa_id_parametro     = 'GEPREFAC'								\n" 
		     +"  and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal				\n" 
		     +"  and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 					\n" 
		     +"  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, fac.cofa_capo_nu_poliza, 1, substr(fac.cofa_capo_nu_poliza,0,3), substr(fac.cofa_capo_nu_poliza,0,2)) \n" 
		     +"  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n" ;
			}
	
	
	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * @author: Towa (JJFM)
	 * UnionFiltroCancelado
	 * @return strUnionFiltroCancelado
	 */
	private String strUnionFiltroCancelado(
			long polizaSeleccionada,
			//												//canal
			Short canal, 
			//												//Ramo
			Short ramo, 
			//												//Filtro Fecha desde
			String strFechaAnulacionDesde, 
			//												//Filtro Fecha Hasta
			String strFechaAnulacionHasta,
			//												//id venta
			Integer inIdVenta,
			//												//Filtro 
			String cadenacan,Short pCuota,
			int pFiltro
	) {
		
		StringBuilder sb = new StringBuilder();
	
		sb.append(this.strObtenerQueryFiltroCancelado1(pCuota, pFiltro));
		
		if (polizaSeleccionada > 0) {
		   sb.append("            and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
		}
		sb.append(this.strObtenerQueryFiltroCancelado2());
		if (polizaSeleccionada > 0) {
		  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
		}
		sb.append(this.strObtenerQueryFiltroCancelado3());
		if ( 
				ramo == 9 && 
				inIdVenta == 6
		) {		/*APV*/								
			sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
			sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
			sb.append("  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
			sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
		} else {																		
			sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
		}	 /*APV*/

		sb.append(" -- Prima_total_PNETA  \n");
		sb.append(", c.COCE_MT_PRIMA_PURA Prima_total_PNETA  \n");
		sb.append(" -- Prima_total_PTOTAL \n");
		sb.append(" , c.COCE_MT_PRIMA_SUBSECUENTE Prima_total_PTOTAL  \n");
		sb.append(" , c.COCE_NU_CUOTA \n");

		
		sb.append("  from colectivos_certificados   c 										\n");
		sb.append("      ,colectivos_cliente_certif cc 										\n");
		sb.append("      ,colectivos_clientes       cl 										\n");
		sb.append("      ,cart_estados              es 										\n");
		sb.append("      ,alterna_estatus           est 									\n");
		sb.append("      ,colectivos_certificados   head 									\n");
		sb.append("      ,alterna_planes            pl 										\n");
		sb.append("      ,colectivos_parametros     pf										\n");
		sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
		sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
		sb.append(cadenacan);
		sb.append("  and c.coce_nu_certificado   > 0			 							\n");
		if (pFiltro != 1) {
		sb.append("  and c.coce_st_certificado   in (10,11)  	     				        \n");
		sb.append("  and c.coce_cd_causa_anulacion <> 45			  	     				\n");
		sb.append("  and c.coce_fe_anulacion    >= to_date('" + strFechaAnulacionDesde + "','dd/mm/yyyy')   \n");
		sb.append("  and c.coce_fe_anulacion    <= to_date('" + strFechaAnulacionHasta + "','dd/mm/yyyy')   \n");
		}
		sb.append("  -- \n");
		sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
		sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
		sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
		sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
		sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
		sb.append("  -- \n");
		sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
		sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
		sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
		sb.append("  and head.coce_nu_certificado   = 0 									\n");
		sb.append("  -- \n");
		sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
		sb.append("  -- \n");
		sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
		sb.append("  -- \n");
		sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
		sb.append("  -- \n");
		sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
		sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
		sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
		sb.append("--																		\n");
	    sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
	    sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
	    sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
	    sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
	    sb.append("  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) \n");
	    sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
	    
	    return sb.toString();
	}
	
	/**
	 * @author JuanFlores
	 * @return string con query para filtro cancelado
	 * 
	 * **/
	private String strObtenerQueryFiltroCancelado1(Short pCuota,
			int pFiltro) {
		StringBuilder sb = new StringBuilder();
		sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
		sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
		sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
		sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
		sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
		sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
		sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
		sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");

		if (pFiltro == 1) {
			sb.append(" ,CASE WHEN C.COCE_ST_CERTIFICADO = 10 THEN 'BAJA'  WHEN C.COCE_ST_CERTIFICADO = 11 THEN 'BAJA'  ELSE 'ALTA' END ESTATUS_MOVIMIENTO 						\n");
		 } else {
			 sb.append("       ,'ALTA'                   ESTATUS_MOVIMIENTO 						\n");
		 }
		
		
		sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
		sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
		sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
		sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
		sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
		sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
		sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
		sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
		sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
		sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
		sb.append("      ,cl.cocn_rfc              RFC 										\n");
		sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
		sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
		sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
		sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
		sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
		sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
		sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
		sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
		sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
		sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
		sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
		sb.append("      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
		sb.append("      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
		sb.append("      ,case when pf.copa_nvalor5 = 0 									\n"); 
		sb.append("                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
		sb.append("                when pf.copa_nvalor5 = 1									\n"); 
		sb.append("                      then c.coce_mt_prima_pura							\n"); 
		sb.append("                else 0 end PRIMA_NETA									\n");
		sb.append("      -- \n");
		sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
		sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
		sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
		sb.append("      ,case when pf.copa_nvalor5 = 0 then \n"); 
		sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
		sb.append("                when pf.copa_nvalor5 = 1 then \n"); 
		sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n"); 
		sb.append("                else 0 end PRIMA_TOTAL  									 \n");
		sb.append("      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada,0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si,0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
		sb.append("      -- \n");
		sb.append("      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
		sb.append("      -- \n");      
		sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
		sb.append("              from colectivos_coberturas cob 							\n");
		sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
		sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo 		\n");
		sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
		
		return sb.toString();
		
	}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro cancelado
	 * 
	 * **/
	private String strObtenerQueryFiltroCancelado2() {
		StringBuilder sb = new StringBuilder();
		sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
		sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
		sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
		sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
		sb.append("            end,2) PRIMA_VIDA 											\n");
		sb.append("      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
		sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
		sb.append("              from colectivos_coberturas cob 							\n");
		sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
		sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
		sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
		return sb.toString();
	}
	/**
	 * @author JuanFlores
	 * @return string con query para filtro cancelado
	 * 
	 * **/
	private String strObtenerQueryFiltroCancelado3() {
		
		StringBuilder sb = new StringBuilder();
		sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
		sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
		sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
		sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
		sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
		sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
		sb.append("      ,es.caes_de_estado        ESTADO 									\n");
		sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
		sb.append("      ,cl.cocn_cd_postal        CP 										\n");
		sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
		sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
		sb.append("      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION \n");
		sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
		sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"); 
		sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n"); 
		sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n"); 
		sb.append("        from colectivos_coberturas cob                    \n"); 
		sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n"); 
		sb.append("         and cob.cocb_carp_cd_ramo     = 61               \n"); 
		sb.append("         and cob.cocb_carb_cd_ramo     = 14               \n"); 
		sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"); 
		sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
		sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
		sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n"); 
		sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n"); 
		sb.append("        from colectivos_coberturas cob                                       \n"); 
		sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n"); 
		sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n"); 
		sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n"); 
		sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"); 
		sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
		sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
		sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n"); 
		sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n"); 
		sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"); 
		sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n"); 
		sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n"); 
		sb.append("           p.copa_vvalor1           ||'|'||                                                 \n"); 
		sb.append("           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"); 
		sb.append("      from colectivos_cliente_certif clc                                                    \n"); 
		sb.append("          ,colectivos_clientes       cln                                                    \n"); 
		sb.append("          ,colectivos_parametros     p                                       \n"); 
		sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n"); 
		sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n"); 
		sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n"); 
		sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n"); 
		sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"); 
		sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"); 
		sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"); 
		sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n"); 
		sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"); 
		sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
		
		return sb.toString();
	}

	
	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * @author: Towa (JJFM)
	 * UnionFiltroEmitido
	 * @return strUnionFiltroEmitido
	 */
	private String strUnionFiltroEmitido(
			long polizaSeleccionada,
			//												//canal
			Short canal, 
			//												//Ramo
			Short ramo, 
			//												//Filtro Fecha desde
			String strFechaCargaDesde, 
			//												//Filtro Fecha Hasta
			String strFechaCargaHasta,
			//												//id venta
			Integer inIdVenta,
			//												//Filtro
			String cadenacan
	) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(this.strGetQueryFiltroEmitido(polizaSeleccionada));
		sb.append("         and cob.cocb_cacb_cd_cobertura  <> '003'         \n"); 
		sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
		sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
		sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_BASICA \n"); 
		sb.append(",(select sum (cob.cocb_ta_riesgo)                                            \n"); 
		sb.append("        from colectivos_coberturas cob                                       \n"); 
		sb.append("       where cob.cocb_casu_cd_sucursal =  1                                  \n"); 
		sb.append("         and cob.cocb_carp_cd_ramo     = 61                                  \n"); 
		sb.append("         and cob.cocb_carb_cd_ramo     = 14                                  \n"); 
		sb.append("         and cob.cocb_cacb_cd_cobertura  = '003'                             \n"); 
		sb.append("         and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto           \n"); 
		sb.append("         and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan               \n"); 
		sb.append("         and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) CUOTA_DESEMPLEO  \n"); 
		sb.append(",case when pf.copa_nvalor4 > 0 then                                          \n"); 
		sb.append("   (select cln.cocn_fe_nacimiento   ||'|'||                                  \n"); 
		sb.append("           cln.cocn_cd_sexo         ||'|'||                                  \n"); 
		sb.append("           clc.cocc_tp_cliente      ||'|'||                                  \n"); 
		sb.append("           p.copa_vvalor1           ||'|'||                                                 \n"); 
		sb.append("           substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)     \n"); 
		sb.append("      from colectivos_cliente_certif clc                                                    \n"); 
		sb.append("          ,colectivos_clientes       cln                                                    \n"); 
		sb.append("          ,colectivos_parametros     p                                       \n"); 
		sb.append("     where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal               \n"); 
		sb.append("       and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo                   \n"); 
		sb.append("       and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza                 \n"); 
		sb.append("       and clc.cocc_nu_certificado   = c.coce_nu_certificado                 \n"); 
		sb.append("       and nvl(clc.cocc_tp_cliente,1)  > 1                                   \n"); 
		sb.append("       and cln.cocn_nu_cliente       = clc.cocc_nu_cliente                   \n"); 
		sb.append("       and p.copa_des_parametro(+)   = 'ASEGURADO'                           \n"); 
		sb.append("       and p.copa_id_parametro(+)    = 'TIPO'                                \n"); 
		sb.append("       and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)                  \n"); 
		sb.append("   else '' end DATOS_OBLIGADO_PU,                                             \n");
		
		if( 
				ramo == 9 && 
				inIdVenta == 6
		) {		/*APV*/								
			sb.append("	c.coce_fe_emision FECHAEMISION, 											\n");
			sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n");
			sb.append("  substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n");
			sb.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
		} else {																		
			sb.append("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");
		}	 /*APV*/
		
		sb.append(" -- Prima_total_PNETA  \n");
		sb.append(", c.COCE_MT_PRIMA_PURA Prima_total_PNETA  \n");
		sb.append(" -- Prima_total_PTOTAL \n");
		sb.append(" , c.COCE_MT_PRIMA_SUBSECUENTE Prima_total_PTOTAL  \n");
		sb.append(" , c.COCE_NU_CUOTA \n");

		//													//FROM-JOIN
		sb.append("  from colectivos_certificados   c 										\n");
		sb.append("      ,colectivos_cliente_certif cc 										\n");
		sb.append("      ,colectivos_clientes       cl 										\n");
		sb.append("      ,cart_estados              es 										\n");
		sb.append("      ,alterna_estatus           est 									\n");
		sb.append("      ,colectivos_certificados   head 									\n");
		sb.append("      ,alterna_planes            pl 										\n");
		sb.append("      ,colectivos_parametros     pf										\n");
		sb.append("where c.coce_casu_cd_sucursal = " + canal + " 							\n");
		sb.append("  and c.coce_carp_cd_ramo     = " + ramo + " 							\n");
		sb.append(cadenacan);
		sb.append("  and c.coce_nu_certificado   > 0			 							\n");
		//sb.append("  and c.coce_st_certificado   in (" + status + ")  	   				\n");
		//sb.append("  and c.coce_cd_causa_anulacion <> 45			  	     				\n");
		sb.append("  and c.coce_fe_carga    >= to_date('" + strFechaCargaDesde + "','dd/mm/yyyy')   \n");
		sb.append("  and c.coce_fe_carga    <= to_date('" + strFechaCargaHasta + "','dd/mm/yyyy')   \n");
		sb.append("  -- \n");
		sb.append("  and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
		sb.append("  and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
		sb.append("  and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 				\n");
		sb.append("  and cc.cocc_nu_certificado   = c.coce_nu_certificado 					\n");
		sb.append("  and nvl(cc.cocc_tp_cliente,1)       = 1 							    \n");
		sb.append("  -- \n");
		sb.append("  and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 				\n");
		sb.append("  and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 					\n");
		sb.append("  and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 				\n");
		sb.append("  and head.coce_nu_certificado   = 0 									\n");
		sb.append("  -- \n");
		sb.append("  and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 						\n");
		sb.append("  -- \n");
		sb.append("  and es.caes_cd_estado(+)     = cl.cocn_cd_estado 						\n");
		sb.append("  -- \n");
		sb.append("  and est.ales_cd_estatus      = c.coce_st_certificado 					\n");
		sb.append("  -- \n");
		sb.append("  and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 					\n");
		sb.append("  and pl.alpl_cd_producto      = c.coce_capu_cd_producto 				\n");
		sb.append("  and pl.alpl_cd_plan          = c.coce_capb_cd_plan  					\n");
		sb.append("--																		\n");
		sb.append("  and pf.copa_des_parametro    = 'POLIZA'							    \n");
	    sb.append("  and pf.copa_id_parametro     = 'GEPREFAC'								\n");
	    sb.append("  and pf.copa_nvalor1          = c.coce_casu_cd_sucursal				    \n");
	    sb.append("  and pf.copa_nvalor2          = c.coce_carp_cd_ramo 					\n");
	    sb.append("  and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) \n");
	    sb.append("  and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) \n");
	    
	    return sb.toString();
	}
	
	/**
	 * @author: Towa (JJFM)
	 * strGetQueryFiltroEmitido
	 * @return strGetQueryFiltroEmitido
	 */
	private String strGetQueryFiltroEmitido(long polizaSeleccionada) {
		StringBuilder sb = new StringBuilder();
				sb.append("select c.coce_carp_cd_ramo      RAMO 									\n");
		sb.append("      ,pl.alpl_dato3            CENTRO_COSTOS 							\n");
		sb.append("      ,c.coce_capo_nu_poliza    POLIZA 									\n");
		sb.append("      ,c.coce_no_recibo         RECIBO 									\n");
		sb.append("      ,cc.cocc_id_certificado   CREDITO 									\n");
		sb.append("      ,c.coce_nu_certificado    CERTIFICADO 								\n");
		sb.append("      ,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n");
		sb.append("      ,est.ales_campo1	       ESTATUS   				 				\n");
		sb.append("      ,'EMITIDO'                ESTATUS_MOVIMIENTO						\n");
		sb.append("      ,c.coce_nu_cuenta         CUENTA 									\n");
		sb.append("      ,c.coce_tp_producto_bco   PRODUCTO 								\n");
		sb.append("      ,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n");
		sb.append("      ,c.coce_cazb_cd_sucursal  SUCURSAL 								\n");
		sb.append("      ,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n");
		sb.append("      ,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n");
		sb.append("      ,cl.cocn_nombre           NOMBRE 									\n");
		sb.append("      ,cl.cocn_cd_sexo          SEXO 									\n");
		sb.append("      ,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n");
		sb.append("      ,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n");
		sb.append("      ,cl.cocn_rfc              RFC 										\n");
		sb.append("      ,c.coce_cd_plazo          PLAZO 									\n");
		sb.append("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n");
		sb.append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
		sb.append("      ,c.coce_fe_suscripcion    FECHA_INGRESO 							\n");
		sb.append("      ,c.coce_fe_desde          FECHA_DESDE 								\n");
		sb.append("      ,c.coce_fe_hasta          FECHA_HASTA 								\n");
		sb.append("      ,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n");
		sb.append("      ,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n");
		sb.append("      ,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n");
		sb.append("      ,c.coce_fe_anulacion      FECHA_ANULACION 							\n");
		sb.append("      ,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n");
		sb.append("      ,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA \n");
		sb.append("      ,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO 							\n");
		sb.append("      ,case when pf.copa_nvalor5 = 0 									\n"); 
		sb.append("                     then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
		sb.append("                when pf.copa_nvalor5 = 1									\n"); 
		sb.append("                      then c.coce_mt_prima_pura							\n"); 
		sb.append("                else 0 end PRIMA_NETA									\n");
		sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS \n");
		sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  \n");
		sb.append("      ,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA \n");
		sb.append("      ,case when pf.copa_nvalor5 = 0 then \n"); 
		sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) \n"); 
		sb.append("                when pf.copa_nvalor5 = 1 then \n"); 
		sb.append("                     nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) \n"); 
		sb.append("                   + nvl(c.coce_mt_prima_pura,0) \n"); 
		sb.append("                else 0 end PRIMA_TOTAL  									 \n");
		sb.append("      ,round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4) TARIFA \n");
		sb.append("      , round(case when c.coce_carp_cd_ramo in (61,65) then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");    
		sb.append("            (select sum (cob.cocb_ta_riesgo) tasaVida 					\n");
		sb.append("              from colectivos_coberturas cob 							\n");
		sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
		sb.append("               and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo   	\n");
		sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
		if (polizaSeleccionada > 0) {
		  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
		}
		sb.append("               and cob.cocb_cacb_cd_cobertura  <> '003' 					\n");
		sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
		sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
		sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
		sb.append("            end,2) PRIMA_VIDA 											\n");
		sb.append("      , round(case when c.coce_carp_cd_ramo = 61 then (decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * \n");
		sb.append("            (select sum (cob.cocb_ta_riesgo) tasaDes 					\n");
		sb.append("              from colectivos_coberturas cob 							\n");
		sb.append("             where cob.cocb_casu_cd_sucursal =  1 						\n");
		sb.append("               and cob.cocb_carp_cd_ramo     = 61 						\n");
		sb.append("               and cob.cocb_carb_cd_ramo     = 14 						\n");
		if (polizaSeleccionada > 0) {
		  sb.append("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
		}
		sb.append("               and cob.cocb_cacb_cd_cobertura  = '003' 					\n");
		sb.append("               and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto \n");
		sb.append("               and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan 	\n");
		sb.append("               and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) 	\n");
		sb.append("            end,2) PRIMA_DESEMPLEO 										\n");
		sb.append("      ,cl.cocn_cd_estado        CD_ESTADO 								\n");
		sb.append("      ,es.caes_de_estado        ESTADO 									\n");
		sb.append("      ,cl.cocn_delegmunic       MUNICIPIO 								\n");
		sb.append("      ,cl.cocn_cd_postal        CP 										\n");
		sb.append("      ,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 					\n");
		sb.append("      ,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 			    \n");
		sb.append("      ,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION   \n");
		sb.append("      ,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO						\n");
		sb.append(",c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA     \n"); 
		sb.append(",c.coce_capb_cd_plan             PLAN_ASEGURADORA         \n"); 
		sb.append(",(select sum (cob.cocb_ta_riesgo)                         \n"); 
		sb.append("        from colectivos_coberturas cob                    \n"); 
		sb.append("       where cob.cocb_casu_cd_sucursal =  1               \n"); 
		sb.append("         and cob.cocb_carp_cd_ramo     = 61     and cob.cocb_carb_cd_ramo     = 14              \n"); 
		return sb.toString();
	}
	//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	/**
	 * AUTHOR: TOWA (JJFM)
	 * Metodo que regresa informacion de los nuevos campos para unir el reporte tecnico con datos de DxP .
	 * @return strObtenerNuevosCamposDxP.
	 * 
	 */
	private String strObtenerNuevosCamposDxP(long sucursal, short ramo, long poliza, Short pCuota, int pFiltro) {
		
		StringBuilder strQuery = new StringBuilder();
		
		strQuery.append(this.strQueryDeudorFacturadoCobrado(poliza, sucursal, ramo, pCuota, pFiltro));
		strQuery.append("  , dxp_dif   as (  \n ");
		strQuery.append("                SELECT  CODX_CORE_NU_RECIBO, CODX_COCE_NU_CERTIFICADO,  CODX_NU_CUOTA_POLIZA,  \n "); 
		strQuery.append("                        CODX_MT_DIFERENCIA_PNETA DIFERENCIA_PNETA, \n ");  
		strQuery.append("                        CODX_MT_DIFERENCIA_PTOTAL DIFERENCIA_PTOTAL \n ");
		strQuery.append("                FROM COLECTIVOS_DXP dxp \n ");
		if(poliza == 0) {
			strQuery.append("						, certificados coce \n");
		}
		strQuery.append("                   where 1 = 1 \n");
         if(poliza == 0) {
        	 strQuery.append("                          and dxp.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		strQuery.append("                and dxp.CODX_CASU_CD_SUCURSAL = "+sucursal+" \n ");
		strQuery.append("                AND dxp.CODX_CARP_CD_RAMO = "+ramo+" \n ");
		if (pFiltro == 1) {
			strQuery.append("                AND dxp.CODX_NU_CUOTA_POLIZA = "+pCuota+"   \n ");
		}
		if(poliza != 0) {
		strQuery.append("                AND dxp.CODX_CAPO_NU_POLIZA = "+poliza+"   \n ");  
		}
		strQuery.append("                AND dxp.CODX_NU_PK = ( \n ");
		strQuery.append("                                    SELECT max(CODX_NU_PK) FROM COLECTIVOS_DXP  \n ");
		strQuery.append("                                    WHERE CODX_CASU_CD_SUCURSAL = dxp.CODX_CASU_CD_SUCURSAL  \n ");
		strQuery.append("                                    AND CODX_CARP_CD_RAMO = dxp.CODX_CARP_CD_RAMO \n ");
		strQuery.append("                                    AND CODX_CAPO_NU_POLIZA = dxp.CODX_CAPO_NU_POLIZA \n "); 
		strQuery.append("                                    AND CODX_COCE_NU_CERTIFICADO = dxp.CODX_COCE_NU_CERTIFICADO AND CODX_NU_CUOTA_POLIZA = dxp.CODX_NU_CUOTA_POLIZA \n ");
		strQuery.append("                                    AND CODX_NU_TIPO_DXP = 2 \n ");
		strQuery.append("                                    ) order by CODX_COCE_NU_CERTIFICADO \n ");
		strQuery.append("                ) \n ");
		strQuery.append(this.strQueryDeudorExigibleTotal(poliza, sucursal, ramo, pCuota, pFiltro));
		strQuery.append(" , dxp_cx       AS ( \n ");
		strQuery.append("                SELECT  CODX_CORE_NU_RECIBO, CODX_COCE_NU_CERTIFICADO,CODX_NU_CUOTA_POLIZA,CODX_NU_TIPO_DXP,(case when CODX_NU_TIPO_DXP = 1 then 'Emision' \n ");
		strQuery.append("                            when CODX_NU_TIPO_DXP = 2 then 'Stock' \n ");
		strQuery.append("                            when CODX_NU_TIPO_DXP = 3 then 'Cancelacion' \n ");
		strQuery.append("                            when CODX_NU_TIPO_DXP = 4 then 'Rehabilitacion' \n ");
		strQuery.append("                            when CODX_NU_TIPO_DXP = 5 then 'Renovacion' \n ");
		strQuery.append("                            else ' ' end)  STATUS_CRUCE \n ");
		strQuery.append("                FROM COLECTIVOS_DXP dxp \n ");
		if(poliza == 0) {
			strQuery.append("						, certificados coce \n");
		}
		strQuery.append("                   where 1 = 1 \n");
         if(poliza == 0) {
        	 strQuery.append("                          and dxp.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		strQuery.append("                and dxp.CODX_CASU_CD_SUCURSAL = "+sucursal+" \n ");
		strQuery.append("                AND dxp.CODX_CARP_CD_RAMO = "+ramo+" \n ");
		if(poliza != 0) {
		strQuery.append("                AND dxp.CODX_CAPO_NU_POLIZA = "+poliza+"   \n "); 
		}
		if (pFiltro == 1) {
			strQuery.append("                AND dxp.CODX_NU_CUOTA_POLIZA = "+pCuota+"   \n ");
		}
		strQuery.append("                AND dxp.CODX_NU_PK = ( \n ");
		strQuery.append("                                    SELECT max(CODX_NU_PK) FROM COLECTIVOS_DXP  \n ");
		strQuery.append("                                    WHERE CODX_CASU_CD_SUCURSAL = dxp.CODX_CASU_CD_SUCURSAL  \n ");
		strQuery.append("                                    AND CODX_CARP_CD_RAMO = dxp.CODX_CARP_CD_RAMO \n ");
		strQuery.append("                                    AND CODX_CAPO_NU_POLIZA = dxp.CODX_CAPO_NU_POLIZA  \n ");
		strQuery.append("                                    AND CODX_COCE_NU_CERTIFICADO = dxp.CODX_COCE_NU_CERTIFICADO  AND CODX_NU_CUOTA_POLIZA = dxp.CODX_NU_CUOTA_POLIZA\n ");
		strQuery.append("                                    AND CODX_NU_TIPO_DXP <> 6 AND CODX_NU_TIPO_DXP <> 8  \n ");
		strQuery.append("                                    ) order by CODX_COCE_NU_CERTIFICADO) \n ");
		strQuery.append("  , dxp_st_pago AS ( \n ");
		strQuery.append("                SELECT CODX_CORE_NU_RECIBO, CODX_COCE_NU_CERTIFICADO, CODX_NU_CUOTA_POLIZA STATUS_PAGO  ,CODX_NU_CUOTA_POLIZA \n ");
		strQuery.append("                FROM COLECTIVOS_DXP dxp \n ");
		if(poliza == 0) {
			strQuery.append("						, certificados coce \n");
		}
		strQuery.append("                   where 1 = 1 \n");
         if(poliza == 0) {
        	 strQuery.append("                          and dxp.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		strQuery.append("                and dxp.CODX_CASU_CD_SUCURSAL = "+sucursal+" \n ");
		strQuery.append("                AND dxp.CODX_CARP_CD_RAMO = "+ramo+" \n ");
		if(poliza != 0) {
		strQuery.append("                AND dxp.CODX_CAPO_NU_POLIZA = "+poliza+"   \n "); 
		}
		if (pFiltro == 1) {
			strQuery.append("                AND dxp.CODX_NU_CUOTA_POLIZA = "+pCuota+"   \n ");
		}
		strQuery.append("                AND dxp.CODX_NU_PK = ( \n ");
		strQuery.append("                                    SELECT max(CODX_NU_PK) FROM COLECTIVOS_DXP  \n ");
		strQuery.append("                                    WHERE CODX_CASU_CD_SUCURSAL = dxp.CODX_CASU_CD_SUCURSAL  \n ");
		strQuery.append("                                    AND CODX_CARP_CD_RAMO = dxp.CODX_CARP_CD_RAMO \n ");
		strQuery.append("                                    AND CODX_CAPO_NU_POLIZA = dxp.CODX_CAPO_NU_POLIZA  \n ");
		strQuery.append("                                    AND CODX_COCE_NU_CERTIFICADO = dxp.CODX_COCE_NU_CERTIFICADO AND CODX_NU_CUOTA_POLIZA = dxp.CODX_NU_CUOTA_POLIZA \n ");
		strQuery.append("                                    AND CODX_NU_TIPO_DXP = 6 \n ");
		strQuery.append("                                    ) order by CODX_COCE_NU_CERTIFICADO) \n ");
		strQuery.append(this.strNuevosCamposSelect(pCuota, pFiltro));
		return strQuery.toString();
	}
	
	/**
	 * @author JuanFlores
	 * Se obtienen Select de Nuevos campos
	 *
	 * **/
	private String strNuevosCamposSelect(Short pCuota, int pfiltro) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("  SELECT  distinct \n");
		strQuery.append("        r.RAMO || '|' ||  \n");
		strQuery.append("        r.CENTRO_COSTOS || '|' ||  \n");
		strQuery.append("        r.POLIZA || '|' ||  \n");
		strQuery.append("        r.RECIBO || '|' ||  \n");
		strQuery.append("        r.CREDITO || '|' ||  \n");
		strQuery.append("        r.CERTIFICADO || '|' ||  \n");
		strQuery.append("        r.CREDITONUEVO || '|' ||  \n");
		strQuery.append("        r.COCE_NU_CUOTA || '|' ||  \n");
		strQuery.append("        r.IDENTIFICACION_DE_CUOTA || '|' ||  \n");
		strQuery.append("        r.ESTATUS || '|' ||  \n");
		strQuery.append("        r.ESTATUS_MOVIMIENTO || '|' ||  \n");
		strQuery.append("        r.CUENTA || '|' ||  \n");
		strQuery.append("        r.PRODUCTO || '|' ||  \n");
		strQuery.append("        r.SUBPRODUCTO || '|' ||  \n");
		strQuery.append("        r.SUCURSAL || '|' ||  \n");
		strQuery.append("        r.APELLIDO_PATERNO || '|' ||  \n");
		strQuery.append("        r.APELLIDO_MATERNO || '|' ||  \n");
		strQuery.append("        r.NOMBRE || '|' ||  \n");
		strQuery.append("        r.SEXO || '|' ||  \n");
		strQuery.append("        r.FECHA_NACIMIENTO || '|' ||  \n");
		strQuery.append("        r.NUMERO_CLIENTE || '|' ||  \n");
		strQuery.append("        r.RFC || '|' ||  \n");
		strQuery.append("        r.PLAZO || '|' ||  \n");
		strQuery.append("        r.FECHA_INICIO_POLIZA || '|' ||  \n");
		strQuery.append("        r.FECHA_FIN_POLIZA || '|' ||  \n");
		strQuery.append("        r.FECHA_INGRESO || '|' ||  \n");
		strQuery.append("        r.FECHA_DESDE || '|' ||  \n");
		strQuery.append("        r.FECHA_HASTA || '|' ||  \n");
		strQuery.append("        r.FECHA_INICIO_CREDITO || '|' ||  \n");
		strQuery.append("        r.FECHA_FIN_CREDITO || '|' ||  \n");
		strQuery.append("        REGEXP_SUBSTR(r.FECHA_FIN_CREDITO_2,'[^|]+', 1, 2) || '|' ||  \n");
		strQuery.append("        r.FECHA_ANULACION || '|' ||  \n");
		strQuery.append("        r.FECHA_CANCELACION || '|' ||  \n");
		strQuery.append("        r.SUMA_ASEGURADA || '|' ||  \n");
		strQuery.append("        r.MONTO_DEVOLUCION || '|' ||  \n");
		strQuery.append("        r.MONTO_DEVOLUCION_SIS || '|' ||  \n");
		strQuery.append("        r.BASE_CALCULO || '|' ||  \n");
		strQuery.append("        r.PRIMA_NETA || '|' ||  \n");
		strQuery.append("        r.DERECHOS || '|' ||  \n");
		strQuery.append("        r.RECARGOS || '|' ||  \n");
		strQuery.append("        r.IVA  || '|' ||  \n");
		strQuery.append("        r.PRIMA_TOTAL || '|' ||  \n");
		strQuery.append("        r.TARIFA || '|' ||  \n");
		strQuery.append("        r.PRIMA_VIDA || '|' ||  \n");
		strQuery.append("        r.PRIMA_DESEMPLEO || '|' ||  \n");
		strQuery.append("        r.CD_ESTADO || '|' ||  \n");
		strQuery.append("        r.ESTADO || '|' ||  \n");
		strQuery.append("        r.CP || '|' ||  \n");
		strQuery.append("        r.PRODUCTO_ASEGURADORA || '|' ||  \n");
		strQuery.append("        r.PLAN_ASEGURADORA || '|' ||  \n");
		strQuery.append("        r.CUOTA_BASICA || '|' ||  \n");
		strQuery.append("        r.CUOTA_DESEMPLEO || '|' ||  \n");
		
		strQuery.append("        (case when r.DATOS_OBLIGADO_PU is null or r.DATOS_OBLIGADO_PU ='' then '||||' else  r.DATOS_OBLIGADO_PU  end) || '|' ||  \n");
		strQuery.append("        r.FECHAEMISION || '|' ||  \n");
		strQuery.append("        r.FECHAENVIO || '|' ||  \n");
		strQuery.append("        r.FECHACARGA || '|' ||  \n");
		strQuery.append("        r.FECHARECIBIDO || '|' ||  \n");
		strQuery.append("        r.DIFERENCIA_DEVOLUCION || '|' ||  \n");
		strQuery.append("        s.DIFERENCIA_PNETA || '|' || s.DIFERENCIA_PTOTAL || '|' || case when t.CODX_NU_TIPO_DXP=3 then 'Cancelacion' else cx.STATUS_CRUCE end || '|' || r.Prima_total_PNETA || '|' || r.Prima_total_PTOTAL || '|' || \n ");
		strQuery.append("        (SELECT SUM (COBRADO_PNETA) from dxp_fac_cob x WHERE  x.CODX_COCE_NU_CERTIFICADO = d.CODX_COCE_NU_CERTIFICADO  and  x.CODX_NU_CUOTA_POLIZA = d.CODX_NU_CUOTA_POLIZA) || '|' ||       \n ");
		strQuery.append("        (SELECT SUM (COBRADO_TOTAL) from dxp_fac_cob x WHERE  x.CODX_COCE_NU_CERTIFICADO = d.CODX_COCE_NU_CERTIFICADO  and  x.CODX_NU_CUOTA_POLIZA = d.CODX_NU_CUOTA_POLIZA) || '|' ||    \n ");
		strQuery.append("        (SELECT SUM (FACTURA_ACUM_PNETA) from dxp_fac_cob x WHERE  x.CODX_COCE_NU_CERTIFICADO = d.CODX_COCE_NU_CERTIFICADO  and  x.CODX_NU_CUOTA_POLIZA <= d.CODX_NU_CUOTA_POLIZA) || '|' || \n ");
		strQuery.append("        (SELECT SUM (FACTURA_ACUM_PTOTAL) from dxp_fac_cob x WHERE  x.CODX_COCE_NU_CERTIFICADO = d.CODX_COCE_NU_CERTIFICADO  and  x.CODX_NU_CUOTA_POLIZA <= d.CODX_NU_CUOTA_POLIZA) || '|' || \n ");
		strQuery.append("        ex.DXP_Exigible_PNETA || '|' || ex.DXP_Exigible_PTOTAL || '|' ||  \n ");
		strQuery.append("        (t.Deudor_Total_PNETA - ex.DXP_Exigible_PNETA) || '|' ||  \n ");
		strQuery.append("        (t.Deudor_Total_PTOTAL - ex.DXP_Exigible_PTOTAL)  || '|' || \n ");
		strQuery.append("        t.Deudor_Total_PNETA || '|' || t.Deudor_Total_PTOTAL || '|' || \n ");
		strQuery.append("        stp.STATUS_PAGO \n ");
		if (pfiltro == 1) {
			strQuery.append("FROM  REPORTE R \n ");
					   strQuery.append("LEFT OUTER JOIN  DXP_CX CX  ON  R.CERTIFICADO = CX.CODX_COCE_NU_CERTIFICADO \n ");
					   strQuery.append("LEFT OUTER JOIN DXP_FAC_COB D ON R.CERTIFICADO = D.CODX_COCE_NU_CERTIFICADO \n ");
					  strQuery.append("LEFT OUTER JOIN  DXP_DIF S  ON R.CERTIFICADO = S.CODX_COCE_NU_CERTIFICADO \n ");
					  strQuery.append("LEFT OUTER JOIN  DXP_EX EX  ON R.CERTIFICADO = EX.CODX_COCE_NU_CERTIFICADO \n ");
					strQuery.append("LEFT OUTER JOIN  DXP_TOTAL T  ON  R.CERTIFICADO = T.CODX_COCE_NU_CERTIFICADO \n ");
					strQuery.append("LEFT OUTER JOIN  DXP_ST_PAGO STP  ON  R.CERTIFICADO = STP.CODX_COCE_NU_CERTIFICADO \n "); 
					   if (pfiltro == 1) {
					  strQuery.append("where (EX.CODX_NU_CUOTA_POLIZA =  \n");
					  strQuery.append(pCuota);
					   
					   strQuery.append(" OR (r.ESTATUS = 'CANCELADO' and ex.DXP_Exigible_PNETA >0 ) ) ");
					   }
		} else {
		strQuery.append("  from  dxp_fac_cob d , reporte r , dxp_dif s, dxp_ex ex, dxp_total t, dxp_cx cx, dxp_st_pago stp ,colectivos_recibos cr \n ");
		  strQuery.append("where r.recibo=cr.CORE_NU_RECIBO(+)  \n");
		   strQuery.append("and r.CERTIFICADO = d.CODX_COCE_NU_CERTIFICADO (+)   \n");
		   strQuery.append("and cr.CORE_NU_CONSECUTIVO_CUOTA=d.CODX_NU_CUOTA_POLIZA(+)  \n");
		   strQuery.append("and   r.CERTIFICADO = s.CODX_COCE_NU_CERTIFICADO (+)  \n");
		   strQuery.append("and cr.CORE_NU_CONSECUTIVO_CUOTA=s.CODX_NU_CUOTA_POLIZA(+)  \n");
		   strQuery.append("and   r.CERTIFICADO = ex.CODX_COCE_NU_CERTIFICADO (+)  \n");
		   strQuery.append("and cr.CORE_NU_CONSECUTIVO_CUOTA=ex.CODX_NU_CUOTA_POLIZA(+)  \n");
		   strQuery.append("and   r.CERTIFICADO = cx.CODX_COCE_NU_CERTIFICADO (+)  \n");
		   strQuery.append("and cr.CORE_NU_CONSECUTIVO_CUOTA= cx.CODX_NU_CUOTA_POLIZA(+)  \n");
		   strQuery.append("and   r.CERTIFICADO = stp.CODX_COCE_NU_CERTIFICADO (+)  \n");
		   strQuery.append("and cr.CORE_NU_CONSECUTIVO_CUOTA=stp.CODX_NU_CUOTA_POLIZA(+)  \n");
		   strQuery.append("and   r.CERTIFICADO = t.CODX_COCE_NU_CERTIFICADO (+)  \n");
		   strQuery.append("and cr.CORE_NU_CONSECUTIVO_CUOTA=case when t.CODX_NU_TIPO_DXP(+)=3 then t.CODX_NU_CUOTA_POLIZA(+)-1 else t.CODX_NU_CUOTA_POLIZA(+) end     \n");
		 //  if (pfiltro == 1) {
		  // sstrQuerytrQuery.append("and (d.CODX_NU_CUOTA_POLIZA =  \n");
		  // .append(pCuota);
		   
		  // strQuery.append(" OR (r.ESTATUS = 'CANCELADO' and ex.DXP_Exigible_PNETA >0 ) ) ");
		   //}
		}
		   //strQuery.append("order by certificado,cr.CORE_NU_CONSECUTIVO_CUOTA,t.Deudor_Total_PNETA desc,ex.DXP_Exigible_PTOTAL desc  \n");
		return strQuery.toString();
	}
	
	/**
	 * @author JuanFlores
	 * Se obtienen strQueryDeudorFacturadoCobrado
	 *
	 * **/
	private String strQueryDeudorFacturadoCobrado(long poliza, long sucursal, short ramo, Short pCuota, int pFiltro) {
		StringBuilder strQuery = new StringBuilder();
		strQuery.append("  , dxp_fac_cob as ( \n ");
		strQuery.append("                SELECT   CODX_CORE_NU_RECIBO,CODX_COCE_NU_CERTIFICADO, CODX_NU_CUOTA_POLIZA, \n ");
		strQuery.append("                        sum(case when  dxp.CODX_MT_AJUSTE_NETO < 0  then CODX_MT_AJUSTE_NETO end  ) COBRADO_PNETA, \n ");
		strQuery.append("                        sum(case when  dxp.CODX_MT_AJUSTE_TOTAL < 0  then CODX_MT_AJUSTE_TOTAL end  ) COBRADO_TOTAL, \n ");
		strQuery.append("                        sum(case when  dxp.CODX_MT_AJUSTE_NETO > 0  then CODX_MT_AJUSTE_NETO end  ) Factura_acum_PNETA, \n ");
		strQuery.append("                        sum(case when  dxp.CODX_MT_AJUSTE_TOTAL > 0  then CODX_MT_AJUSTE_TOTAL end  ) Factura_acum_PTOTAL \n ");
		strQuery.append("                FROM COLECTIVOS_DXP dxp \n ");
		if(poliza == 0) {
			strQuery.append("						, certificados coce \n");
		}
		strQuery.append("                   where 1 = 1 \n");
         if(poliza == 0) {
        	 strQuery.append("                          and dxp.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		strQuery.append("                and dxp.CODX_CASU_CD_SUCURSAL = " + sucursal +" \n ");
		strQuery.append("                AND dxp.CODX_CARP_CD_RAMO = "+  ramo + "\n ");
		if(poliza != 0) {
		strQuery.append("                AND dxp.CODX_CAPO_NU_POLIZA = "+ poliza +" \n ");
		}
		if (pFiltro == 1) {
			strQuery.append("                AND dxp.CODX_NU_CUOTA_POLIZA = "+pCuota+"   \n ");
		}
		strQuery.append(" ANd dxp.CODX_NU_TIPO_DXP = 8");
		strQuery.append("                group by CODX_COCE_NU_CERTIFICADO  ,CODX_NU_CUOTA_POLIZA, CODX_CORE_NU_RECIBO  \n ");
		strQuery.append("                order by CODX_COCE_NU_CERTIFICADO) \n ");
		return strQuery.toString();
	}
	
	/**
	 * @author JuanFlores
	 * Se obtienen strQueryDeudorExigibleTotal
	 *
	 * **/
	private String strQueryDeudorExigibleTotal(long poliza, long sucursal, short ramo, Short pCuota, int pFiltro) {
		StringBuilder strQuery =  new StringBuilder();
		strQuery.append("  , dxp_ex      as ( \n ");
		strQuery.append("                SELECT  CODX_CORE_NU_RECIBO, CODX_COCE_NU_CERTIFICADO,  CODX_NU_CUOTA_POLIZA ,   \n ");
		strQuery.append("                        CODX_MT_NETO DXP_Exigible_PNETA,  \n ");
		strQuery.append("                        CODX_MT_TOTAL DXP_Exigible_PTOTAL   \n ");  
		strQuery.append("                FROM COLECTIVOS_DXP dxp \n ");
		if(poliza == 0) {
			strQuery.append("						, certificados coce \n");
		}
		strQuery.append("                   where 1 = 1 \n");
         if(poliza == 0) {
        	 strQuery.append("                          and dxp.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		strQuery.append("                and dxp.CODX_CASU_CD_SUCURSAL = "+sucursal+" \n ");
		strQuery.append("                AND dxp.CODX_CARP_CD_RAMO = "+ramo+" \n ");
		if(poliza != 0) {
		strQuery.append("                AND dxp.CODX_CAPO_NU_POLIZA = "+poliza+"   \n "); 
		}
		//if (pFiltro == 1) {
		//	strQuery.append("                AND dxp.CODX_NU_CUOTA_POLIZA = "+pCuota+"   \n ");
		//}
		strQuery.append("                AND dxp.CODX_NU_PK = ( \n ");
		strQuery.append("                                    SELECT max(CODX_NU_PK) FROM COLECTIVOS_DXP  \n ");
		strQuery.append("                                    WHERE CODX_CASU_CD_SUCURSAL = dxp.CODX_CASU_CD_SUCURSAL  \n ");
		strQuery.append("                                    AND CODX_CARP_CD_RAMO = dxp.CODX_CARP_CD_RAMO \n ");
		strQuery.append("                                    AND CODX_CAPO_NU_POLIZA = dxp.CODX_CAPO_NU_POLIZA  \n ");
		strQuery.append("                                    AND CODX_COCE_NU_CERTIFICADO = dxp.CODX_COCE_NU_CERTIFICADO AND CODX_NU_CUOTA_POLIZA = dxp.CODX_NU_CUOTA_POLIZA\n ");
		strQuery.append("                                    AND CODX_NU_TIPO_DXP = 8 \n ");
		strQuery.append("                                    ) order by CODX_COCE_NU_CERTIFICADO) \n ");
		strQuery.append(" , dxp_total    AS ( \n ");
		strQuery.append("                SELECT  CODX_CORE_NU_RECIBO, CODX_COCE_NU_CERTIFICADO,CODX_NU_TIPO_DXP, CODX_NU_CUOTA_POLIZA,  \n ");
	
		strQuery.append("                 CODX_MT_NETO Deudor_Total_PNETA,  CODX_MT_TOTAL Deudor_Total_PTOTAL \n ");
		strQuery.append("                FROM COLECTIVOS_DXP dxp \n ");
		if(poliza == 0) {
			strQuery.append("						, certificados coce \n");
		}
		strQuery.append("                   where 1 = 1 \n");
         if(poliza == 0) {
        	 strQuery.append("                          and dxp.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		strQuery.append("                and dxp.CODX_CASU_CD_SUCURSAL = "+sucursal +" \n ");
		strQuery.append("                AND dxp.CODX_CARP_CD_RAMO = "+ramo+" \n ");
		if(poliza != 0) {
		strQuery.append("                AND dxp.CODX_CAPO_NU_POLIZA = "+poliza+"   \n "); 
		}
		if (pFiltro == 1) {
			strQuery.append("                AND dxp.CODX_NU_CUOTA_POLIZA = "+pCuota+"   \n ");
		}
		strQuery.append("                AND dxp.CODX_NU_PK = (SELECT max(CODX_NU_PK) FROM COLECTIVOS_DXP  \n ");
		strQuery.append("                            WHERE CODX_CASU_CD_SUCURSAL = dxp.CODX_CASU_CD_SUCURSAL  \n ");
		strQuery.append("                            AND CODX_CARP_CD_RAMO = dxp.CODX_CARP_CD_RAMO \n ");
		strQuery.append("                            AND CODX_CAPO_NU_POLIZA = dxp.CODX_CAPO_NU_POLIZA  \n ");
		strQuery.append("                            AND CODX_COCE_NU_CERTIFICADO = dxp.CODX_COCE_NU_CERTIFICADO  AND  CODX_NU_CUOTA_POLIZA=dxp.CODX_NU_CUOTA_POLIZA\n ");
		strQuery.append("                            AND CODX_NU_TIPO_DXP <> 8) order by CODX_COCE_NU_CERTIFICADO) \n ");
		return strQuery.toString();
	}
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * AUTHOR: TOWA (JJFM)
	 * Metodo que regresa informacion del reporte de Recibos.
	 * @return Arreglo con la informacion del reporte de Recibos.
	 * 
	 */
	@Override
	public ArrayList<String[]> ExportarReporteRecibos(Short canal, Short ramo, long poliza1, String fecha1, String fecha2,
			Integer inIdVenta, List<Integer> statusRecibos, String rutaReportes)
			throws Excepciones {
		
		try {
		
		StringBuilder strQuery = new StringBuilder();
		
		strQuery.append("WITH \n");
		strQuery.append("RFI_IVA as ( \n");
		strQuery.append("SELECT  \n");
		strQuery.append("COCM_NU_RECIBO, \n");
		strQuery.append("REGEXP_SUBSTR(min(COCE.COCM_DI_COBRO1), '[^|A-Z]+', 1, 2) RFI,  --RFI  \n");
		strQuery.append("REGEXP_SUBSTR(min(COCE.COCM_DI_COBRO1), '[^|A-Z]+', 1, 3) IVA --IVA  \n");
		strQuery.append("FROM COLECTIVOS_CERTIFICADOS_MOV COCE \n");
		strQuery.append("WHERE COCE.COCM_CASU_CD_SUCURSAL = "+canal+" \n");
		strQuery.append("AND COCE.COCM_CARP_CD_RAMO  = "+ramo+" \n");
		if(poliza1 != 0)
		strQuery.append("and COCE.COCM_CAPO_NU_POLIZA = "+poliza1+" \n");
		strQuery.append("AND NVL(COCE.COCM_SUB_CAMPANA, 0) = " + inIdVenta + "\n");
		strQuery.append("GROUP BY COCM_NU_RECIBO \n");
		strQuery.append("Union ALL \n"); 
		strQuery.append("(SELECT \n"); 
		strQuery.append("COCE_NO_RECIBO, \n"); 
		strQuery.append("REGEXP_SUBSTR(min(COCE.COCE_DI_COBRO1), '[^|A-Z]+', 1, 2) RFI,  --RFI \n");  
		strQuery.append("REGEXP_SUBSTR(min(COCE.COCE_DI_COBRO1), '[^|A-Z]+', 1, 3) IVA --IVA \n"); 
		strQuery.append("FROM COLECTIVOS_CERTIFICADOS COCE  \n");
		strQuery.append("WHERE COCE.COCE_CASU_CD_SUCURSAL = "+canal+" \n"); 
		strQuery.append("AND COCE.COCE_CARP_CD_RAMO  = "+ramo+" \n");
		if(poliza1 != 0)
		strQuery.append("and COCE.COCE_CAPO_NU_POLIZA = "+poliza1+" \n"); 
		strQuery.append("AND NVL(COCE.COCE_SUB_CAMPANA, 0) = "+inIdVenta+ " \n");
		strQuery.append("GROUP BY COCE.COCE_NO_RECIBO) \n");
		strQuery.append(") \n");
		
		strQuery.append("SELECT DISTINCT CORE.CORE_FE_EMISION                          || '|' || --MES_CONTABLE \n");
		strQuery.append("CORE.CORE_CAPO_NU_POLIZA                             || '|' || --NUM_POLIZA \n");
		strQuery.append("CORE.CORE_NU_RECIBO                                  || '|' || --NUM_RECIBO \n");
		strQuery.append("CORE.CORE_MT_PRIMA_PURA                              || '|' || --PRIMA_NETA \n");
		strQuery.append("(SELECT COCE.RFI FROm RFI_IVA WHERE rownum = 1)      || '|' || --RFI \n");
		strQuery.append("(SELECT COCE.IVA FROm RFI_IVA WHERE rownum = 1)	  || '|' || --IVA \n");
		strQuery.append("CORE.CORE_MT_PRIMA                                   || '|' || --PRIMA_TOTAL \n");
		strQuery.append("CORE.CORE_ST_RECIBO                                  || '|' || --ESTATUS_RECIBO \n");
		strQuery.append("COCM.COCM_FE_COBRO                                   || '|' || --FECHA_COBRO \n");
		strQuery.append("COCM.COCM_MT_COB_PRIMA_NETA                          || '|' || --COBRO_PRIMA_NETA \n");
		strQuery.append("COCM.COCM_MT_COB_RFI                                 || '|' || --COBRO_RFI \n");
		strQuery.append("COCM.COCM_MT_COB_IVA                                 || '|' || --COBRO_IVA \n");
		strQuery.append("COCM.COCM_MT_COB_PRIMA_TOTAL                         || '|' || --COBRO_PRIMA_TOTAL \n");
		strQuery.append("COCM.COCM_FE_COBRO                                   || '|' || --NOC_FECHA \n");
		strQuery.append("COCM.COCM_MT_NOC_PRIMA_NETA                          || '|' || --NOC_PRIMA_NETA \n");
		strQuery.append("COCM.COCM_MT_NOC_RFI                                 || '|' || --NOC_RFI \n");
		strQuery.append("COCM.COCM_MT_NOC_IVA                                 || '|' || --NOC_IVA \n");
		strQuery.append("COCM.COCM_MT_NOC_PRIMA_TOTAL                                   --NOC_PRIMA_TOTAL \n");
		strQuery.append("FROM COLECTIVOS_RECIBOS CORE, \n");
		strQuery.append("RFI_IVA COCE, \n");
		strQuery.append("COLECTIVOS_COBRANZA COCM \n");
		strQuery.append("WHERE	1 = 1 \n");
		strQuery.append("AND core.CORE_CASU_CD_SUCURSAL          = 1  \n");
		strQuery.append("AND CORE.CORE_CARP_CD_RAMO          	= "+ramo+"  \n");
		if(poliza1 != 0) {
		strQuery.append("AND CORE.CORE_CAPO_NU_POLIZA        	= "+poliza1+"  \n");
		}
	
		strQuery.append("AND	COCE.COCM_NU_RECIBO = CORE.CORE_NU_RECIBO \n");
		strQuery.append("AND	COCM.COCM_CAPO_NU_POLIZA(+)  =	CORE.CORE_CAPO_NU_POLIZA \n");
		strQuery.append("AND COCM.COCM_CORE_NU_RECIBO(+) = core.CORE_NU_RECIBO \n");
		if (
				!statusRecibos.isEmpty()
		) {
			strQuery.append(
					String.format("AND CORE.CORE_ST_RECIBO IN (%s) \n", this.strParseArrayBy(statusRecibos, ","))
							);
			
		}
		strQuery.append(
				String.format(
						"AND CORE.CORE_FE_EMISION BETWEEN TO_DATE ('%s', 'DD/MM/YYYY') AND TO_DATE ('%s', 'DD/MM/YYYY') \n", fecha1, fecha2)
						);
		
		Query qry = this.getSession().createSQLQuery(strQuery.toString());
		
		List darrobj = qry.list();
		ArrayList<String[]> darrstrResults = new ArrayList<String[]>();
		
		Iterator<Object> it = darrobj.iterator();
		
		while(it.hasNext()) {
			String[] arrDatos = {(String) it.next()};
			darrstrResults.add(arrDatos);
		}
		
		return darrstrResults;
		} catch (Exception ex) {
			throw new Excepciones("Error al consultar base de datos.", ex);
		}
		
	}
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * @author: Towa (JJFM)
	 * Parse un arreglo y regresa un string concatenado por el separador 
	 * @return string con los elemenos concatenados por el separador.
	 */
	private String strParseArrayBy(List<Integer> arrint, String strSeparator) {
		String strOutput = "";
		
		for (int intI = 0 ; intI < arrint.size(); intI = intI + 1) {
			strOutput = strOutput.concat("" + arrint.get(intI));
			
			if (
					intI < arrint.size() - 1
			) {
				strOutput = strOutput.concat(",");
			}
		}
		return strOutput;
	}
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * AUTHOR: TOWA (JJFM)
	 * Metodo que regresa las vigencias disponibles para una poliza
	 * @return Arreglo con las vigencias disponibles de la pliza.
	 */
	@Override
	public ArrayList<String> arrstrObtenerVigencias(Short shortCanal, Short shortRamo, long longPoliza, short shortCert)
			throws Excepciones {
		try {
		
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("WITH vigencias as (SELECT COCM_FE_DESDE FE_DESDE \n");
			strQuery.append("FROM COLECTIVOS_CERTIFICADOS_MOV \n");
			strQuery.append(String.format("WHERE COCM_CASU_CD_SUCURSAL = %d \n", shortCanal));
			strQuery.append(String.format("AND COCM_CARP_CD_RAMO = %d \n", shortRamo));
			strQuery.append(String.format("AND COCM_CAPO_NU_POLIZA = %d \n", longPoliza));
			strQuery.append(String.format("AND COCM_NU_CERTIFICADO = %d \n", shortCert));
			strQuery.append("GROUP BY COCM_FE_DESDE \n");
			strQuery.append("UNION ALL( \n");
			strQuery.append("		SELECT COCE_FE_DESDE   \n");
			strQuery.append("		FROM COLECTIVOS_CERTIFICADOS \n");
			strQuery.append(String.format("		WHERE COCE_CASU_CD_SUCURSAL = %d  \n", shortCanal));
			strQuery.append(String.format("		AND COCE_CARP_CD_RAMO = %d  \n",shortRamo));
			strQuery.append(String.format("		AND COCE_CAPO_NU_POLIZA = %d  \n", longPoliza));
			strQuery.append("		AND COCE_NU_CERTIFICADO = 0 \n"); 
			strQuery.append("		GROUP BY COCE_FE_DESDE  \n");
			strQuery.append("		) \n");
			strQuery.append("		ORDER BY FE_DESDE) \n");
			strQuery.append("SELECT to_char(FE_DESDE, 'dd/mm/yyyy') FE_VIG \n");
			strQuery.append("from vigencias  \n");
			strQuery.append("GROUP BY FE_DESDE \n");
			strQuery.append("ORDER BY FE_VIG \n");

					
			Query qry = this.getSession().createSQLQuery(strQuery.toString());
			
			List darrobj = qry.list();
			ArrayList<String> darrstrResults = new ArrayList<String>();
			
			Iterator<Object> it = darrobj.iterator();
			
			while(it.hasNext()) {
				//									//To easy code
				String arrDatos = (String) it.next();
				darrstrResults.add(arrDatos);
				
			}
			
			return darrstrResults;
		} catch (Exception ex) {
			throw new Excepciones("Error al consultar base de datos.", ex);
		}
	}
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * AUTHOT: Towa (JJFM)
	 * ObtenerFechaEnFormato
	 * @return strObtenerFechaEnFormato.
	 */
	private String strObtenerFechaEnFormato(String strFormat) throws DateParseException {
		
		Calendar now = new GregorianCalendar();
		Date nowDate = now.getTime();
		return (new SimpleDateFormat(strFormat)).format(nowDate);
		
	}
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * AUTHOR: TOWA (JJFM)
	 * Consulta base de datos para obtener la información de reporteDxP
	 * @return Regresa un arreglo del ReporteDxP.
	 */
	@Override
	public ArrayList<ReporteDxP> rdxpObtenerResumenDxP(Short shortCanal, Short shortRamo, long longPoliza,
			Short shortCert, String strFechaVigencia, String strFechaBusqueda, short idVenta) throws Excepciones {
		
		try {
			Query qry = this.getSession().createSQLQuery(this.strObtenerQueryResumenDxP(shortCanal, shortRamo, longPoliza, strFechaVigencia, strFechaBusqueda, idVenta));
			List darrobj = qry.list();
			
			ArrayList<ReporteDxP> result = new ArrayList<ReporteDxP>();
			
			Iterator<Object> it = darrobj.iterator();
			
			while(it.hasNext()) {
				//									//To easy code
				Object[] arrDatos = (Object[]) it.next();
				result.add(new ReporteDxP(arrDatos));
				
			}
			
			 
			return result;
		} catch (NullPointerException ex) {
			this.logger.info("No se encontraron resultados.");
			return null;
		} 
		
		
		
	}
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * AUTHOR: TOWA (JJFM)
	 * ObtenerQueryResumenDxP
	 * @return strObtenerQueryResumenDxP.
	 */
	private String strObtenerQueryResumenDxP(Short shortCanal, Short shortRamo, long longPoliza, 
			String strFechaVigencia, String strFechaBusqueda, short idventa
	) {
		
		StringBuilder sb = new StringBuilder();
		Archivo objArchivo;
		
		try {
			
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("ResumenDXP.tst");
			
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_SUCURSAL_DXP), String.valueOf(shortCanal));
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_RAMO_DXP), String.valueOf(shortRamo));
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_POLIZA_DXP), String.valueOf(longPoliza));
			Utilerias.replaceAll(sb,  Pattern.compile("pjFechaVigencia"), strFechaVigencia);
			Utilerias.replaceAll(sb,  Pattern.compile("pjFechaBusqueda"), strFechaBusqueda);
			
		}  catch (Exception e) {
			e.printStackTrace();
		} 
		
		return sb.toString();
	}
	
	/**
	 * AUTHOR: TOWA (JJFM)
	 * strQueryDeudorTotalExigible
	 * @return strQueryDeudorTotalExigible.
	 */
	private String strQueryDeudorTotalExigible(long longPoliza, short shortCanal, short shortRamo, String strFechaVigencia , String strFechaBusqueda) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("dxp_total\n"); 
		sbQuery.append("        AS (   \n");  
		sbQuery.append("            select CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO,CODX_MT_NETO, CODX_MT_TOTAL \n"); 
		sbQuery.append("            FROM colectivos_dxp c\n"); 
		if(longPoliza == 0) {
			sbQuery.append("						, certificados coce \n");
		}
		sbQuery.append("                   where 1 = 1 \n");
         if(longPoliza == 0) {
        	 sbQuery.append("                          and c.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		sbQuery.append(
		 String.format("            and c.CODX_CASU_CD_SUCURSAL = %d\n", shortCanal)); 
		sbQuery.append(
		 String.format("            AND c.CODX_CARP_CD_RAMO = %d\n", shortRamo)); 
		if(longPoliza != 0) {
		sbQuery.append(
		 String.format("            AND c.CODX_CAPO_NU_POLIZA = %d\n", longPoliza)); 
		sbQuery.append(
				 String.format("            and c.CODX_FE_INICIO_VIGENCIA =  to_date('%s', 'dd/mm/yyyy')\n", strFechaVigencia));
		}
		sbQuery.append("            and c.CODX_NU_TIPO_DXP <>8\n");
		if(longPoliza == 0) {
		sbQuery.append(
		 String.format("            and c.CODX_FE_CONTABLE_REG >=  to_date('%s', 'dd/mm/yyyy')\n", strFechaVigencia));
		}
		sbQuery.append(
		 String.format("            and c.CODX_FE_CONTABLE_REG <=  to_date('%s', 'dd/mm/yyyy')\n", strFechaBusqueda)); 
		sbQuery.append("            and c.CODX_NU_PK = (\n"); 
		sbQuery.append("                                select max (CODX_NU_PK) from colectivos_dxp cd\n");   
		sbQuery.append("                                WHERE cd.CODX_CASU_CD_SUCURSAL = c.CODX_CASU_CD_SUCURSAL\n"); 
		sbQuery.append("                                AND cd.CODX_CARP_CD_RAMO = c.CODX_CARP_CD_RAMO\n"); 
		sbQuery.append("                                AND cd.CODX_CAPO_NU_POLIZA = c.CODX_CAPO_NU_POLIZA\n"); 
		sbQuery.append("                                and cd.CODX_COCE_NU_CERTIFICADO = c.CODX_COCE_NU_CERTIFICADO\n"); 
		sbQuery.append("                                and  cd.CODX_NU_TIPO_DXP <> 8 \n"); 
		sbQuery.append("                                )\n"); 
		sbQuery.append("            ),\n"); 
		sbQuery.append("dxp_exi\n"); 
		sbQuery.append("        AS (\n"); 
		sbQuery.append("            select CODX_CASU_CD_SUCURSAL, CODX_CARP_CD_RAMO, CODX_CAPO_NU_POLIZA, CODX_COCE_NU_CERTIFICADO,CODX_MT_NETO, CODX_MT_TOTAL \n"); 
		sbQuery.append("            FROM colectivos_dxp c\n"); 
		if(longPoliza == 0) {
			sbQuery.append("						, certificados coce \n");
		}
		sbQuery.append("                   where 1 = 1 \n");
         if(longPoliza == 0) {
        	 sbQuery.append("                          and c.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
		sbQuery.append(		 
		 String.format("            and c.CODX_CASU_CD_SUCURSAL = %d\n", shortCanal)); 
		sbQuery.append(
		 String.format("            AND c.CODX_CARP_CD_RAMO = %d\n", shortRamo)); 
		if(longPoliza != 0) {
		sbQuery.append(
		 String.format("            AND c.CODX_CAPO_NU_POLIZA = %d\n", longPoliza));
		sbQuery.append(
		 String.format("            and c.CODX_FE_INICIO_VIGENCIA =  to_date('%s', 'dd/mm/yyyy')\n", strFechaVigencia)); 
		}
		sbQuery.append("            and c.CODX_NU_TIPO_DXP = 8\n"); 		
		if(longPoliza == 0) {
		sbQuery.append(
		 String.format("            and c.CODX_FE_CONTABLE_REG >=  to_date('%s', 'dd/mm/yyyy')\n", strFechaVigencia));
		}
		sbQuery.append(
		 String.format("            and c.CODX_FE_CONTABLE_REG <=  to_date('%s', 'dd/mm/yyyy')\n", strFechaBusqueda)); 
		sbQuery.append("            and c.CODX_NU_PK = (\n"); 
		sbQuery.append("                                select max (CODX_NU_PK) from colectivos_dxp cd\n");   
		sbQuery.append("                                WHERE cd.CODX_CASU_CD_SUCURSAL = c.CODX_CASU_CD_SUCURSAL\n"); 
		sbQuery.append("                                AND cd.CODX_CARP_CD_RAMO = c.CODX_CARP_CD_RAMO\n"); 
		sbQuery.append("                                AND cd.CODX_CAPO_NU_POLIZA = c.CODX_CAPO_NU_POLIZA\n"); 
		sbQuery.append("                                and cd.CODX_COCE_NU_CERTIFICADO = c.CODX_COCE_NU_CERTIFICADO\n"); 
		sbQuery.append("                                and  cd.CODX_NU_TIPO_DXP = 8 \n"); 
		sbQuery.append("                                )\n"); 
		sbQuery.append("            ), \n");
		return sbQuery.toString();
	}
	
	/**
	 * AUTHOR: TOWA (JJFM)
	 * strQuerySelectNuevosCampos
	 * @return strQuerySelectNuevosCampos.
	 */
	private String strQuerySelectNuevosCampos() {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select \n");   
		sbQuery.append("    nvl(d.CODX_CAPO_NU_POLIZA, 0) IdPoliza, \n"); 
		sbQuery.append("    0.0 EPNA, \n"); 
		sbQuery.append("    nvl(f.FAPN, 0) FAPN, \n"); 
		sbQuery.append("    nvl(SUM(e.CODX_MT_NETO), 0) DxPEPN, \n");  
		sbQuery.append("    nvl(SUM((d.CODX_MT_NETO - e.CODX_MT_NETO)), 0) \"DxP NEPN\", \n");  
		sbQuery.append("    nvl(SUM(d.CODX_MT_NETO), 0) DT, \n");  
		sbQuery.append("    0.0 EPTA, \n"); 
		sbQuery.append("    nvl(f.FAPT, 0) FAPT, \n"); 
		sbQuery.append("    nvl(cob.MCPT, 0) MCPT, \n"); 
		sbQuery.append("    nvl(SUM(e.CODX_MT_TOTAL), 0) \"DxP EPT\", \n");  
		sbQuery.append("    nvl(SUM((d.CODX_MT_TOTAL - e.CODX_MT_TOTAL)), 0) \"DxP NEPT\", \n"); 
		sbQuery.append("    nvl(SUM(d.CODX_MT_TOTAL), 0) DT_PT \n"); 
		sbQuery.append("from dxp_total d , dxp_exi e, facturacion f, cobranza cob \n");
		sbQuery.append("where d.CODX_CASU_CD_SUCURSAL = e.CODX_CASU_CD_SUCURSAL \n"); 
		sbQuery.append("AND d.CODX_CARP_CD_RAMO = e.CODX_CARP_CD_RAMO  \n");
		sbQuery.append("AND d.CODX_CAPO_NU_POLIZA = e.CODX_CAPO_NU_POLIZA  \n");
		sbQuery.append("AND d.CODX_COCE_NU_CERTIFICADO = e.CODX_COCE_NU_CERTIFICADO \n"); 
		sbQuery.append(" \n");
		sbQuery.append("and d.CODX_CASU_CD_SUCURSAL = f.CODX_CASU_CD_SUCURSAL (+) \n");
		sbQuery.append("AND d.CODX_CARP_CD_RAMO = f.CODX_CARP_CD_RAMO (+) \n");
		sbQuery.append("AND d.CODX_CAPO_NU_POLIZA = f.CODX_CAPO_NU_POLIZA (+) \n");
		sbQuery.append("AND d.CODX_COCE_NU_CERTIFICADO = f.CODX_COCE_NU_CERTIFICADO (+) \n");
		sbQuery.append(" \n");
		sbQuery.append("and d.CODX_CASU_CD_SUCURSAL = cob.CODX_CASU_CD_SUCURSAL (+) \n");
		sbQuery.append("AND d.CODX_CARP_CD_RAMO = cob.CODX_CARP_CD_RAMO (+) \n");
		sbQuery.append("AND d.CODX_CAPO_NU_POLIZA = cob.CODX_CAPO_NU_POLIZA (+) \n");
		sbQuery.append("AND d.CODX_COCE_NU_CERTIFICADO = cob.CODX_COCE_NU_CERTIFICADO (+) \n");
		sbQuery.append("GROUP BY d.CODX_CAPO_NU_POLIZA, 0.0 ");
		return sbQuery.toString();
	}
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * AUTHOR: TOWA (JJFM)
	 * Consulta base de datos para obtener la información del detalle para reporteDxP.
	 * @return Regresa un arreglo con la informacion del detalle para reporteDxP .
	 */
	@Override
	public ArrayList<DetalleReporteDxP> arrdetdxpObtenerDetalleDxP(Short shortCanal, Short shortRamo, long longPoliza,
			Short shortCert, String strFechaVigencia, String strFechaBusqueda, short idVenta) throws Excepciones {
		
		try {
			Query qry = this.getSession().createSQLQuery(this.strObtenerQueryDetalleDxP(shortCanal, shortRamo, 
					longPoliza, strFechaVigencia, strFechaBusqueda, idVenta));
			List<Object> darrobj = qry.list();

			ArrayList<DetalleReporteDxP> darrstrResults = new ArrayList<DetalleReporteDxP>();
			
			Iterator<Object> it = darrobj.iterator();
			
			while(it.hasNext()) {
				//									//To easy code
				Object[] arrDatos = (Object[]) it.next();
				darrstrResults.add(new DetalleReporteDxP(arrDatos));
				
			}
			
			return darrstrResults;
		} catch (Excepciones ex) {
			 throw new Excepciones(ex.toString());
		}
		
		
		
	}
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * @author: Towa (Juan Jose Flores)
	 * ObtenerQueryDetalleDxP
	 * @return strQueryDetalleDxP
	 * 
	 * */
	private String strObtenerQueryDetalleDxP(Short shortCanal, Short shortRamo, long longPoliza, 
			String strFechaVigencia, String strFechaBusqueda, short idventa
	) {
		StringBuilder sb = new StringBuilder();
		Archivo objArchivo;
		
		try {
			
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("DetalleDXP.tst");
			
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_SUCURSAL_DXP), String.valueOf(shortCanal));
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_RAMO_DXP), String.valueOf(shortRamo));
			Utilerias.replaceAll(sb,  Pattern.compile(Constantes.PARAM_POLIZA_DXP), String.valueOf(longPoliza));
			Utilerias.replaceAll(sb,  Pattern.compile("pjFechaVigencia"), strFechaVigencia);
			Utilerias.replaceAll(sb,  Pattern.compile("pjFechaBusqueda"), strFechaBusqueda);
			
		}  catch (Exception e) {
			e.printStackTrace();
		} 
		
		return sb.toString();
	}
	
	/**
	 * @author: Towa (Juan Jose Flores)
	 * strQueryDeudores
	 * @return strQueryDeudores
	 * 
	 * */
	private String strQueryDeudores(long longPoliza, short shortCanal, short shortRamo, String strFechaVigencia , String strFechaBusqueda) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("     dxp_total           AS ( \n");
		sbQuery.append("                    select CODX_NU_CUOTA_POLIZA, \n"); 
		sbQuery.append("                    CODX_FE_INICIO_VIGENCIA, \n"); 
		sbQuery.append("                    sum(CODX_MT_NETO) CODX_MT_NETO, \n"); 
		sbQuery.append("                    sum(CODX_MT_TOTAL) CODX_MT_TOTAL \n");
		sbQuery.append("                    from colectivos_dxp c \n");
		if(longPoliza == 0) {
		sbQuery.append("						, certificados coce \n");
		}
		sbQuery.append("                   where 1 = 1 \n");
         if(longPoliza == 0) {
        	 sbQuery.append("              and c.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");
         }
			
		sbQuery.append(
		 String.format("                    and c.CODX_CASU_CD_SUCURSAL = %d \n", shortCanal)); 
		sbQuery.append(
		 String.format("                    and  c.CODX_CARP_CD_RAMO = %d \n", shortRamo)); 
		if(longPoliza != 0) {
		sbQuery.append(
		 String.format("                    and  c.CODX_CAPO_NU_POLIZA = %d \n", longPoliza));
		sbQuery.append(
				 String.format("                    AND c.codx_fe_inicio_vigencia = to_date('%s', 'dd/mm/yyyy') \n", strFechaVigencia));
		}
		if(longPoliza == 0) {
		sbQuery.append(
		 String.format("           		 	and c.CODX_FE_CONTABLE_REG >=  to_date('%s', 'dd/mm/yyyy')\n", strFechaVigencia));
		}
		sbQuery.append(
		 String.format("                    AND c.CODX_FE_CONTABLE_REG <= to_date('%s', 'dd/mm/yyyy') \n", strFechaBusqueda));
		sbQuery.append("                    and CODX_NU_TIPO_DXP <> 8 \n");
		sbQuery.append("                    and CODX_NU_PK=( \n"); 
		sbQuery.append("                                    select max(CODX_NU_PK) from colectivos_dxp cd \n");
		sbQuery.append("                                    where c.CODX_CASU_CD_SUCURSAL = cd.CODX_CASU_CD_SUCURSAL \n");
		sbQuery.append("                                    and c.CODX_CARP_CD_RAMO = cd.CODX_CARP_CD_RAMO \n"); 
		sbQuery.append("                                    and c.CODX_CAPO_NU_POLIZA=cd.CODX_CAPO_NU_POLIZA \n"); 
		sbQuery.append("                                    and c.CODX_COCE_NU_CERTIFICADO =cd.CODX_COCE_NU_CERTIFICADO \n");
		sbQuery.append("                                    and c.CODX_NU_CUOTA_POLIZA = cd.CODX_NU_CUOTA_POLIZA \n");
		sbQuery.append("                                    and CODX_NU_TIPO_DXP <> 8 \n");
		sbQuery.append("                                    ) \n");
		sbQuery.append("                    group by CODX_NU_CUOTA_POLIZA, CODX_FE_INICIO_VIGENCIA \n");
		sbQuery.append("                    ), \n");
		sbQuery.append("dxp_exi \n"); 
		sbQuery.append("                AS ( \n");
		sbQuery.append("                    select CODX_NU_CUOTA_POLIZA, \n");
		sbQuery.append("                    CODX_FE_INICIO_VIGENCIA, \n");    
		sbQuery.append("                    sum(CODX_MT_NETO) CODX_MT_NETO, \n"); 
		sbQuery.append("                    sum(CODX_MT_TOTAL) CODX_MT_TOTAL \n");
		sbQuery.append("                    from colectivos_dxp c \n");
		if(longPoliza == 0) {
		sbQuery.append("						, certificados coce \n");
		}
		sbQuery.append("                   where 1 = 1 \n");
         if(longPoliza == 0) {
        	 sbQuery.append("                          and c.CODX_CAPO_NU_POLIZA= coce.COCE_CAPO_NU_POLIZA \n");

         }
		
		sbQuery.append(
		 String.format("                    and c.CODX_CASU_CD_SUCURSAL = %d \n", shortCanal)); 
		sbQuery.append(
		 String.format("                    and  c.CODX_CARP_CD_RAMO = %d \n", shortRamo));
		if(longPoliza != 0) {
		sbQuery.append(
		 String.format("                    and  c.CODX_CAPO_NU_POLIZA = %d \n", longPoliza));
		sbQuery.append(
		 String.format("                    AND c.codx_fe_inicio_vigencia = to_date('%s', 'dd/mm/yyyy') \n", strFechaVigencia));
		}
		if(longPoliza == 0) {
		sbQuery.append(
		 String.format("            		and c.CODX_FE_CONTABLE_REG >=  to_date('%s', 'dd/mm/yyyy')\n", strFechaVigencia));
		}
		sbQuery.append(
		 String.format("                    AND c.CODX_FE_CONTABLE_REG <= to_date('%s', 'dd/mm/yyyy') \n", strFechaBusqueda));
		sbQuery.append("                    and CODX_NU_TIPO_DXP = 8 \n");
		sbQuery.append("                    and CODX_NU_PK=( \n"); 
		sbQuery.append("                                    select max(CODX_NU_PK) \n"); 
		sbQuery.append("                                    from colectivos_dxp cd \n");
		sbQuery.append("                                    where c.CODX_CASU_CD_SUCURSAL = cd.CODX_CASU_CD_SUCURSAL \n");
		sbQuery.append("                                    and  c.CODX_CARP_CD_RAMO = cd.CODX_CARP_CD_RAMO \n"); 
		sbQuery.append("                                    and  c.CODX_CAPO_NU_POLIZA=cd.CODX_CAPO_NU_POLIZA \n"); 
		sbQuery.append("                                    and c.CODX_COCE_NU_CERTIFICADO =cd.CODX_COCE_NU_CERTIFICADO \n");
		sbQuery.append("                                    and c.CODX_NU_CUOTA_POLIZA = cd.CODX_NU_CUOTA_POLIZA \n");
		sbQuery.append("                                    and  CODX_NU_TIPO_DXP = 8 \n");
		sbQuery.append("                                    ) \n");
		sbQuery.append("                    group by CODX_NU_CUOTA_POLIZA, CODX_FE_INICIO_VIGENCIA \n");
		sbQuery.append("                    ), \n");
		return sbQuery.toString();
	}
	// ----------------------------------------------------------------------------------------------------------------

	/**
	 * @return the cuota
	 */
	public Integer getCuota() {
		return cuota;
	}

	/**
	 * @param cuota the cuota to set
	 */
	public void setCuota(Integer cuota) {
		this.cuota = cuota;
	}
	
	
}
//=====================================================================================================================

