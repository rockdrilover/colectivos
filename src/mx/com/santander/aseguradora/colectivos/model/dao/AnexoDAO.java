package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosAnexo;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

public interface AnexoDAO extends CatalogoDao {

	/**
	 * Consulta anexo de anexo
	 * 
	 * @param filtro
	 *            suscripcion a consultar
	 * @return Lista de anexos
	 */
	List<ColectivosAnexo> consultaAnexoSuscripcion(
			ColectivosAnexo filtro) throws Excepciones;
	
	/**
	 * Consecutivo anexo 
	 * @param filtro filtro a buscar
	 * @return consecutivo anexo
	 * @throws Excepciones
	 */
	long consecutivoAnexo(ColectivosAnexo filtro) throws Excepciones;
	
	/**
	 * Obtiene objeto
	 * @param filtro filtro a buscar
	 * @return objeto anexo
	 */
	ColectivosAnexo obtieneObjeto(ColectivosAnexo filtro);
	
	
	/**
	 * Guardar Anexo
	 * @param guardar
	 * @return
	 * @throws Excepciones
	 */
	ColectivosAnexo guardarAnexo(ColectivosAnexo guardar) throws Excepciones;
	
	void actualizarAnexo(ColectivosAnexo guardar) throws Excepciones;
}
