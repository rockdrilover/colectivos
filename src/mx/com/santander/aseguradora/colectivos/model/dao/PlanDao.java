/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface PlanDao extends CatalogoDao{

	/**
	 * 
	 * @param poliza
	 * @return
	 */
	public abstract Plan getPlan(PlanId id);
	/**
	 * 
	 * @return
	 */
	public abstract List<Plan> getPlanes();
	/**
	 * 
	 * @param ramo
	 * @return
	 */
	public abstract List<Plan> getPlanes(final Integer producto);
	
	public abstract List<Integer> getIdPlanes();
	public abstract Integer siguentePlanExtraPma( )throws Excepciones;
	public abstract Integer siguentePlan(final String filtro )throws Excepciones;
	
	
	
}
