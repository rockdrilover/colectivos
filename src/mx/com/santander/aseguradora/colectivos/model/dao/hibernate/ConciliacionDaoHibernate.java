package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.model.dao.ConciliacionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
@Repository
public class ConciliacionDaoHibernate extends HibernateDaoSupport implements ConciliacionDao{

	@Autowired
	public ConciliacionDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Object> consultar(String strQuery) throws Exception {
		ArrayList<Object> arlResutlados;
		Query query;
		
		try {
			arlResutlados = new ArrayList<Object>();
			if(!strQuery.equals("")){
				query = getSession().createSQLQuery(strQuery);
				arlResutlados = (ArrayList<Object>) query.list();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error: ConciliacionDao.consultar:: " + e.getMessage());
		} 
		return arlResutlados;
	}
	
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		
		
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		
		try {
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					StringBuilder consulta = new StringBuilder();
					consulta.append("from VtaPrecarga PC\n");
					consulta.append("where 1 = 1\n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					
					List<T> lista = qry.list();
					
					return lista;
				}
				
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error al cargar vta_precarga.", e);
		}
	}

	

}
