package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.model.dao.HistorialDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

@Repository
public class HistorialDaoHibernet extends HibernateDaoSupport implements HistorialDao {

	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Autowired
	public HistorialDaoHibernet(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}

	@Override
	public <T> List<T> consultaHistorial(short ramo, Long poliza, String certificado,String credito ,String nombre,
		String paterno, String materno) throws Excepciones {
		StringBuilder select = new StringBuilder();
		Session session=null;
		Query qry;
		try {
			select= consultaHistorialDetalle();
			if(ramo > 0){
				select.append("AND C.COCE_CARP_CD_RAMO       =" + ramo+"\n");
			}
			if (poliza != null && poliza != 0 ){
				select.append("AND C.COCE_CAPO_NU_POLIZA     =" + poliza+"\n");
			}
			if(certificado != null && !certificado.trim().equalsIgnoreCase("")){
				select.append("AND C.COCE_NU_CERTIFICADO         =" + certificado.trim().toUpperCase()+"\n");
			}
			if(nombre != null && !nombre.trim().equalsIgnoreCase("")){
				select.append("AND CL.COCN_NOMBRE = '" + nombre.trim().toUpperCase()+"'\n");
			}
			if(paterno != null && !paterno.trim().equalsIgnoreCase("")){
				select.append("AND CL.COCN_APELLIDO_PAT = '" + paterno.trim().toUpperCase()+"'\n");
			}
			if(materno != null && !materno.trim().equalsIgnoreCase("")){
				select.append("AND CL.COCN_APELLIDO_MAT = '" + materno.trim().toUpperCase()+"'\n");
			}
			if(credito != null && !credito.trim().equalsIgnoreCase("")){
				select.append("AND C.COCE_NU_CREDITO       = '" + credito.trim() + "' \n");
			}

			qry = this.getSession().createSQLQuery(select.toString());
			List<T> lista = qry.list();
			
			return lista;
			
			
		}catch(Exception ex){
			throw new Excepciones ("Error en DAO Consulta Historial de Credito.",ex);
		}
		
	}

	@Override
	public <T> List<T> consultaHistorialCobertura(short ramo, Long poliza, String certificado, Integer producto,Integer plan) throws Excepciones{
		StringBuilder select = new StringBuilder();
		Query qry;
		
		try {
			select.append("Select cc.cocb_carb_cd_ramo as Ramo_contable,\n");
			select.append("cc.cocb_cacb_cd_cobertura as Cobertura,\n");
			select.append("cob.cacb_de_cobertura     as Descripcion_cobertura,\n");
			select.append("cc.cocb_ta_riesgo         as Tarifa\n");
			select.append("From colectivos_coberturas cc\n");
			select.append("Inner Join cart_coberturas cob On (cob.cacb_carb_cd_ramo = cc.cocb_carb_cd_ramo And cob.cacb_cd_cobertura = cc.cocb_cacb_cd_cobertura ) \n");
			select.append("Where cc.cocb_casu_cd_sucursal = 1 \n");
			select.append("And cc.cocb_carp_cd_ramo = ").append(ramo).append(" \n");
			select.append("And cc.cocb_capo_nu_poliza = ").append(poliza).append(" \n");
			select.append("And cc.cocb_capu_cd_producto = ").append(producto).append(" \n");
			select.append("And cc.cocb_capb_cd_plan = ").append(plan).append(" \n");		
		
			qry = this.getSession().createSQLQuery(select.toString());
			return  qry.list();
		} catch(Exception ex){
			throw new Excepciones ("Error en DAO Consulta Cobertura.",ex);
		}
	}
	
	@Override
	public List<Object[]> consultaDetalleComponente(short ramo, Long nPoliza, String certificado, Integer producto, Integer plan) throws Excepciones {		
		StringBuilder select = new StringBuilder();
		Query qry;
		
		try {
	
			select.append("Select CC.COCT_CAPP_CD_COMPONENTE, \n");
			select.append("c.capp_de_componente      Desc_componente,\n");
			select.append("0     as Mt_componente, \n");
			select.append("CC.COCT_TA_COMPONENTE  as Tarifa \n");
			select.append("From colectivos_componentes cc \n");
			select.append("Inner Join  cart_componentes c On (c.capp_cd_componente = CC.COCT_CAPP_CD_COMPONENTE ) \n");
			select.append("Where CC.COCT_CASU_CD_SUCURSAL = 1 \n");
			select.append("And CC.COCT_carp_cd_ramo = ").append(ramo).append(" \n");
			select.append("And CC.COCT_capo_nu_poliza = ").append(nPoliza).append(" \n");
			select.append("And CC.COCT_capu_cd_producto = ").append(producto).append(" \n");
			select.append("And CC.COCT_capb_cd_plan = ").append(plan).append(" \n");

			qry = this.getSession().createSQLQuery(select.toString());
			return  qry.list();
		} catch(Exception ex){
			throw new Excepciones ("Error en DAO Consulta Componentes.",ex);
		}
	}
	
	@Override
	public <T> List<T> consultaHistorialRecibos(short ramo, Long poliza,
			String certificado) throws Excepciones{
		StringBuilder select = new StringBuilder();
		Query qry;
		
		try {
			select.append("select cr.core_nu_consecutivo_cuota as Consecutivo,\n");
			select.append("cr.core_nu_recibo as Recibo,\n");
			select.append("cr.core_mt_prima as Monto,\n");
			select.append("cr.core_st_recibo || ' - ' || rv.rv_meaning as Estatus_recibo,\n");
			select.append("cr.core_fe_desde as Fecha_desde,\n");
			select.append("cr.core_fe_hasta as Fecha_hasta\n");
			select.append("from colectivos_certificados ce\n");
			select.append("Inner Join colectivos_recibos cr\n");
			select.append("on cr.core_casu_cd_sucursal = ce.coce_casu_cd_sucursal\n");
			select.append("and cr.core_carp_cd_ramo = ce.coce_carp_cd_ramo\n");
			select.append("and cr.core_capo_nu_poliza = ce.coce_capo_nu_poliza\n");
			select.append("and cr.core_cace_nu_certificado = ce.coce_nu_certificado\n");
			select.append("and cr.core_fe_desde >= ce.coce_fe_desde\n");
			select.append("and cr.core_fe_hasta <= ce.coce_fe_hasta\n");
			select.append("Inner Join cg_ref_codes rv\n");
			select.append("on rv.rv_low_value = cr.core_st_recibo\n");
			select.append("And rv.rv_domain = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO'\n");
			select.append("where ce.coce_casu_cd_sucursal = 1\n");
			select.append("AND ce.coce_carp_cd_ramo = ").append(ramo).append(" \n");
			select.append("And ce.COCE_CAPO_NU_POLIZA = ").append(poliza).append(" \n");
			select.append("AND ce.coce_nu_certificado = ").append(certificado).append(" \n");
					
			qry = this.getSession().createSQLQuery(select.toString());
			return  qry.list();
		} catch(Exception ex){
			throw new Excepciones ("Error en DAO Consulta recibos.",ex);
		}
	}
	
	@Override
	public <T> List<T> consultaHistorialRecibosRamo57_58(short ramo, Long poliza, String certificado) throws Excepciones {
		StringBuilder select = new StringBuilder();
		Query qry;
		
		try {
			select.append("select cr.care_nu_consecutivo_cuota as Consecutivo,\n");
			select.append("cr.care_nu_recibo as Recibo,\n");
			select.append("cr.care_mt_prima as Monto,\n");
			select.append("cr.care_st_recibo || ' - ' || rv.rv_meaning as Estatus_recibo,\n");
			select.append("cr.care_fe_desde as Fecha_desde,\n");
			select.append("cr.care_fe_hasta as Fecha_hasta\n");
			select.append("from colectivos_certificados ce\n");
			select.append("Inner Join cart_recibos cr\n");
			select.append("on cr.care_casu_cd_sucursal = ce.coce_casu_cd_sucursal\n");
			select.append("and cr.care_carp_cd_ramo = ce.coce_carp_cd_ramo\n");
			select.append("and cr.care_capo_nu_poliza = ce.coce_capo_nu_poliza\n");
			select.append("and cr.care_cace_nu_certificado = ce.coce_nu_certificado\n");
			select.append("and cr.care_fe_desde >= ce.coce_fe_desde\n");
			select.append("and cr.care_fe_hasta <= ce.coce_fe_hasta\n");
			select.append("Inner Join cg_ref_codes rv\n");
			select.append("on rv.rv_low_value = cr.care_st_recibo\n");
			select.append("And rv.rv_domain = 'CART_RECIBOS.CARE_ST_RECIBO'\n");
			select.append("where ce.coce_casu_cd_sucursal = 1\n");
			select.append("AND ce.coce_carp_cd_ramo = ").append(ramo).append(" \n");
			select.append("And ce.coce_capo_nu_poliza = ").append(poliza).append(" \n");
			select.append("AND ce.coce_nu_certificado = ").append(certificado).append(" \n");
			
			qry = this.getSession().createSQLQuery(select.toString());
			return  qry.list();
		} catch(Exception ex){
			throw new Excepciones ("Error en DAO Consulta recibo.",ex);
		} 
	}
	
	public StringBuilder consultaHistorialDetalle(){
		StringBuilder selectDetalle = new StringBuilder();
		selectDetalle.append("select 'Datos Generales del Solicitante / contratante' as Datos_Contratante,\n");
		selectDetalle.append("cacn.cacn_nm_apellido_razon   ,\n");
		selectDetalle.append("cacn.cacn_rfc ,\n");
		selectDetalle.append("Decode(cacn.cacn_nu_telefono_cobro,\n");
		selectDetalle.append("null, ' - ' \n");
		selectDetalle.append(" , cacn.cacn_nu_telefono_cobro) ,\n");
		selectDetalle.append("cacn.cacn_di_cobro1 ,\n");
		selectDetalle.append("cacn.cacn_cazp_colonia_cob ,\n");
		selectDetalle.append("cu.caci_de_ciudad ,\n");
		selectDetalle.append("ec.caes_de_estado ea,\n");
		selectDetalle.append("cacn.cacn_zn_postal_cobro ,\n");
		selectDetalle.append("'Datos Generales del Prospecto / Asegurado' as Datos_asegurado,\n");
		selectDetalle.append("cl.cocn_nombre || ' ' || cl.cocn_apellido_pat || ' ' || cl.cocn_apellido_mat ,\n");
		selectDetalle.append("cl.cocn_buc_cliente ,\n");
		selectDetalle.append("cl.cocn_nu_cliente ,\n");
		selectDetalle.append("cl.cocn_cd_sexo ,\n");
		selectDetalle.append("cl.cocn_rfc  ,\n");
		selectDetalle.append("cl.cocn_cd_cobro ,\n");
		selectDetalle.append("cl.cocn_fe_nacimiento ,\n");
		selectDetalle.append("cl.cocn_calle_num ,\n");
		selectDetalle.append("cl.cocn_colonia ,\n");
		selectDetalle.append("cl.cocn_delegmunic ,\n");
		selectDetalle.append("e.caes_de_estado ec,\n");
		selectDetalle.append("cl.cocn_cd_postal ,\n");
		selectDetalle.append("cl.cocn_nu_lada || cl.cocn_nu_telefono ,\n");
		selectDetalle.append("c.coce_st_certificado||' - '|| ae.ales_descripcion ,\n");
		selectDetalle.append("ae.ales_campo2 ||' - '|| ae.ales_campo1 ,\n");
		selectDetalle.append("c.coce_fe_anulacion ,\n");
		selectDetalle.append("c.coce_capu_cd_producto ||'- '|| pr.alpr_de_producto ,\n");
		selectDetalle.append("c.coce_capb_cd_plan  ||'- '|| ap.alpl_de_plan ,\n");
		selectDetalle.append("c.coce_fe_carga ,\n");
		selectDetalle.append("c.coce_fe_suscripcion ,\n");
		selectDetalle.append("c.coce_fe_emision, \n");
		selectDetalle.append("c.coce_mt_suma_asegurada ,\n");
		selectDetalle.append("c.coce_mt_suma_aseg_si ,\n");
		selectDetalle.append("c.coce_mt_prima_subsecuente ,\n");
		selectDetalle.append("c.coce_mt_prima_pura ,\n");
		selectDetalle.append("c1.coce_fe_desde dep,\n");
		selectDetalle.append("c1.coce_fe_hasta hap,\n");
		selectDetalle.append("c.coce_fr_pago ,\n");
		selectDetalle.append("c.coce_fe_desde dece,\n");
		selectDetalle.append("c.coce_fe_hasta hace,\n");
		selectDetalle.append("c.coce_fe_fin_credito ,\n");			
		selectDetalle.append("c.coce_cazb_cd_sucursal ||' - '|| csb.capj_de_sucursal ,\n");
		selectDetalle.append("c.coce_carp_cd_ramo ,\n");		
		selectDetalle.append("c.coce_nu_certificado ,\n");
		selectDetalle.append("c.coce_capo_nu_poliza ,\n");
		selectDetalle.append("DECODE(C.COCE_NU_CREDITO, NULL, C.COCE_NU_CUENTA, C.COCE_NU_CREDITO) ,\n");
		selectDetalle.append("NVL(C.COCE_SUB_CAMPANA,0) ,\n");
		selectDetalle.append("c.coce_casu_cd_sucursal ||' - '|| can.canl_de_canal ,\n");
		selectDetalle.append("c.coce_cd_causa_anulacion ||' - '|| ae.ales_descripcion ,\n");
		selectDetalle.append("c.coce_carp_cd_ramo ||' - '|| rp.carp_de_ramo \n");
		
		selectDetalle.append("from colectivos_certificados c \n");
		selectDetalle.append("Inner Join colectivos_cliente_certif cc on (cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal and cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo and cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza and cc.cocc_nu_certificado = c.coce_nu_certificado) \n");
		selectDetalle.append("Inner Join colectivos_clientes cl on (cl.cocn_nu_cliente = cc.cocc_nu_cliente) \n");
		selectDetalle.append("Inner Join cart_estados e on (e.caes_cd_estado = cl.cocn_cd_estado) \n");
		selectDetalle.append("Inner Join colectivos_certificados c1 on (c1.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and c1.coce_carp_cd_ramo = c.coce_carp_cd_ramo and c1.coce_capo_nu_poliza = c.coce_capo_nu_poliza and c1.coce_nu_certificado = 0) \n");
		selectDetalle.append("Inner Join alterna_estatus ae on (ae.ales_cd_estatus = c.coce_st_certificado and ae.ales_desc_corta = 'CERTIFICADO') \n");
		selectDetalle.append("Inner Join alterna_productos pr on (pr.alpr_cd_ramo = c.coce_carp_cd_ramo and pr.alpr_cd_producto = c.coce_capu_cd_producto) \n");
		selectDetalle.append("Inner Join alterna_planes ap on (ap.alpl_cd_ramo = c.coce_carp_cd_ramo and ap.alpl_cd_producto = c.coce_capu_cd_producto and ap.alpl_cd_plan = c.coce_capb_cd_plan) \n");
		selectDetalle.append("Left Join cart_sucursal_banco csb on (csb.capj_cd_sucursal = c.coce_cazb_cd_sucursal) \n");
		selectDetalle.append("Inner Join cart_certificados cert on (cert.cace_casu_cd_sucursal = c1.coce_casu_cd_sucursal and cert.cace_carp_cd_ramo = c1.coce_carp_cd_ramo and cert.cace_capo_nu_poliza = c1.coce_capo_nu_poliza and cert.cace_nu_certificado = c1.coce_nu_certificado) \n"); 
		selectDetalle.append("Inner Join cart_clientes cacn on (cacn.cacn_cd_nacionalidad = cert.cace_cacn_cd_nacionalidad and cacn.cacn_nu_cedula_rif = cert.cace_cacn_nu_cedula_rif)  \n");
		selectDetalle.append("Inner Join cart_ciudades cu On (cu.caci_caes_cd_estado = cacn.cacn_caes_cd_estado_hab and cu.caci_cd_ciudad = cacn.cacn_caci_cd_ciudad_hab)  \n");
		selectDetalle.append("Inner Join cart_estados ec On (ec.caes_cd_estado = cacn.cacn_caes_cd_estado_hab)  \n");
		selectDetalle.append("Inner Join cart_canales can On (can.canl_cd_canal = c.coce_casu_cd_sucursal)  \n");
		selectDetalle.append("Inner Join cart_ramos_polizas rp On (rp.carp_cd_ramo = c.coce_carp_cd_ramo)  \n");
		selectDetalle.append("Where c.coce_casu_cd_sucursal = 1 \n");
		
		return selectDetalle;
	}
	
}
