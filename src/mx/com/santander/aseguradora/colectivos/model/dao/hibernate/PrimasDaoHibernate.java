package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.model.dao.PrimasDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

@Repository
public class PrimasDaoHibernate<T> extends HibernateDaoSupport implements PrimasDao {
	
	@Autowired
	public PrimasDaoHibernate(SessionFactory sessionFactory) {		
		super.setSessionFactory(sessionFactory);
	}
		
	@Override
	public <T> List<T> cargaIdVenta(final String feinicio, String fefin,String producto,Short ramo) throws Exception {
		List<T> idVenta = null;		
		StringBuilder sb;
		Query query; 
		try{
			
			sb = new StringBuilder()
							
			.append(" select tabla.mes,tabla.prod,sum(tabla.pmdep),sum(tabla.pmrec) \n")
			.append(" from ( \n")
			.append(" select to_char(ct.cocu_fecha_ingreso,'mm') mes, cp.COPA_VVALOR4 prod,sum(ct.COCU_MONTO_PRIMA) pmdep,0 pmrec \n")     
			.append(" from colectivos_cuentas ct,colectivos_parametros cp     \n")
			.append(" where ct.cocu_calificador        not in(2,4) \n")
			.append(" and ct.cocu_fecha_ingreso between to_Date('").append( feinicio).append("', 'dd/MM/yyyy') and to_Date('").append(fefin).append("', 'dd/MM/yyyy')  \n")									
			.append(" and ct.cocu_ramo                  =  ").append(ramo).append(" \n")
			.append(" and ct.COCU_TIPO_MOVIMIENTO       =  'A' \n")
			.append(" and cp.COPA_NVALOR1               =  1 \n")//canal
			.append(" and cp.copa_nvalor2               =  ct.cocu_ramo \n")
			.append(" and cp.COPA_NVALOR4               =  ct.COCU_ID_VENTA \n")
			.append(" and cp.copa_id_parametro          =  'IDVENTA' \n")
			.append(" and cp.COPA_VVALOR4 = decode('").append(producto).append("', 0, cp.COPA_VVALOR4, '").append(producto).append("')  \n")
			.append(" group by cp.COPA_VVALOR4,ct.cocu_fecha_ingreso,ct.COCU_FECHA_CONCILIA,ct.COCU_CALIFICADOR \n")  
			.append(" union all \n")
			.append(" select to_char(ct.cocu_fecha_ingreso,'mm') mes, cp.COPA_VVALOR4 prod,0 pmrec,sum(ct.COCU_MONTO_PRIMA) pmdep \n")     
			.append(" from colectivos_cuentas ct,colectivos_parametros cp \n")    
			.append(" where ct.cocu_calificador         =  4 \n")
			.append(" and ct.cocu_fecha_ingreso between to_Date('").append( feinicio).append("', 'dd/MM/yyyy') and to_Date('").append(fefin).append("', 'dd/MM/yyyy')  \n")
//			.append(" and ct.cocu_fecha_ingreso between to_Date('"+ feinicio +"', 'dd/MM/yyyy') and to_Date('"+fefin+"', 'dd/MM/yyyy')  \n")					
			.append(" and ct.cocu_ramo                  =  ").append(ramo).append(" \n")					
			.append(" and ct.COCU_TIPO_MOVIMIENTO       =  'A' \n")
			.append(" and cp.COPA_NVALOR1               =  1 \n")//canal
			.append(" and cp.copa_nvalor2               =  ct.cocu_ramo \n")
			.append(" and cp.COPA_NVALOR4               =  ct.COCU_ID_VENTA \n")
			.append(" and cp.copa_id_parametro          =  'IDVENTA' \n")
			.append(" and cp.COPA_VVALOR4 = decode('").append(producto).append("', 0, cp.COPA_VVALOR4, '").append(producto).append("')  \n")
//			.append(" and cp.COPA_VVALOR4 = decode('" + producto + "', 0, cp.COPA_VVALOR4, '" + producto + "')  \n")					
			.append(" group by cp.COPA_VVALOR4,ct.cocu_fecha_ingreso,ct.COCU_FECHA_CONCILIA,ct.COCU_CALIFICADOR  ) tabla \n")
			.append(" group by tabla.mes,tabla.prod \n");	
	
			query = this.getSession().createSQLQuery(sb.toString());
			idVenta = query.list();
			
			return idVenta;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	@Override
	public Long maxConsecutivo() throws Exception {	
	
	String res = null;
	Long consecutivo = null;
	StringBuilder sb = null;
	List<Object> lista = null;
	
	try {
		
		sb = new StringBuilder();
		lista = new ArrayList<Object>();
		
		
		sb = new StringBuilder();
		sb.append("select  max(copd_consecutivo) \n");
		sb.append("	from COLECTIVOS_PRIMAS_DEPOSITO \n"); 				
		
		lista = getSession().createSQLQuery(sb.toString()).list();
					
		res = lista.get(0).toString();
		res = lista.get(0).toString();
		consecutivo = Long.parseLong(res);
		
	

		
	} catch (Exception e) {
		// TODO: handle exception
		throw new Excepciones("Error al recuperar siguiente cd colectivos_parametros", e);
	}
	
	return consecutivo;
	}
	
	@Override
	public <T> List<T> generaDetallePrimas(String param, String producto) throws Exception {
		List<T> detalle = null;
		
		try{
			
			StringBuilder sb = new StringBuilder();		
			
			sb.append(" select tabla.fecha,tabla.prod,tabla.cuenta,tabla.credito,sum(tabla.pmdep),sum(tabla.pmrec) ");
			sb.append(" from (    ");
			sb.append(" select ct.cocu_fecha_ingreso fecha, cp.COPA_VVALOR8 prod,sum(ct.COCU_MONTO_PRIMA) pmdep,0 pmrec,ct.cocu_cuenta_pago cuenta,ct.cocu_no_credito credito ");  
			sb.append(" from colectivos_cuentas ct,colectivos_parametros cp   ");
			sb.append(" where ct.cocu_calificador not in(2,4)    ");
			sb.append(" and to_char(ct.cocu_fecha_ingreso,'mm')="+param+"");   
			sb.append(" and to_char(ct.COCU_CUENTA_PAGO)=cp.COPA_VVALOR1    ");
			sb.append(" and cp.copa_id_parametro='CUENTAS'    ");
			sb.append(" and cp.copa_des_parametro='CONCILIACION'  ");  
//			sb.append(" and cp.COPA_VVALOR8 <> 'DES-TDC'    ");
			sb.append(" and ct.cocu_tipo_movimiento = 'A'  ");
			sb.append(" and cp.COPA_VVALOR8 = decode('" + producto + "', 0, cp.COPA_VVALOR8, '" + producto + "')  ");   
			sb.append(" group by cp.COPA_VVALOR8,ct.cocu_fecha_ingreso,ct.COCU_FECHA_CONCILIA,ct.COCU_CALIFICADOR,ct.cocu_cuenta_pago,ct.cocu_no_credito ");  
			sb.append(" union all ");   
			sb.append(" select ct.cocu_fecha_ingreso fecha,cp.COPA_VVALOR8,0 pmdep,sum(ct.COCU_MONTO_PRIMA) pmrec,ct.cocu_cuenta_pago cuenta,ct.cocu_no_credito credito ");   
			sb.append(" from colectivos_cuentas ct,colectivos_parametros cp ");      
			sb.append(" where ct.cocu_calificador = 4    " );
			sb.append(" and to_char(ct.cocu_fecha_ingreso,'mm')="+param+""); 			   
			sb.append("and cp.copa_id_parametro='CUENTAS' ");   
			sb.append(" and cp.copa_des_parametro='CONCILIACION' ");
			sb.append(" and ct.cocu_tipo_movimiento = 'A'  ");
//			sb.append(" and cp.COPA_VVALOR8 <> 'DES-TDC' ");   
			sb.append(" and cp.COPA_VVALOR8 = decode('" + producto + "', 0, cp.COPA_VVALOR8, '" + producto + "')  ");   
			sb.append(" group by cp.COPA_VVALOR8,ct.cocu_fecha_ingreso,ct.COCU_FECHA_CONCILIA,ct.COCU_CALIFICADOR,ct.cocu_cuenta_pago,ct.cocu_no_credito)tabla ");   
			sb.append(" group by tabla.fecha,tabla.prod,tabla.cuenta,tabla.credito ");
			sb.append(" order by tabla.prod ");  
			Query query = this.getSession().createSQLQuery(sb.toString());
			detalle = query.list();
			
			return detalle;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}

	@Override
	public <T> List<T> cargaFotosGuardadas(int canal, Short ramo2, String mesRe,String dia) throws Exception {
		List<T> fotos = null;
		StringBuilder sb;
		Query query; 
		
		try{
			sb = new StringBuilder();
			sb.append("select cpd.copd_idventa idVenta, cpd.copd_fecha_carga \n"); 
			sb.append("	from colectivos_primas_deposito cpd \n"); 
			sb.append("where cpd.copd_canal="+canal+" \n");
			sb.append("and cpd.copd_ramo="+ramo2+" \n");						
			sb.append("	and to_char(cpd.copd_fecha_carga,'mm')= '"+ mesRe + "' \n");
			sb.append("	and to_char(cpd.copd_fecha_carga,'dd')='"+ dia + "' \n");  //31
			
			query = this.getSession().createSQLQuery(sb.toString());
			fotos = query.list();
			
			return fotos;
		} catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	@Override
	public void guardaFoto(int canal, Short inRamo, Integer inIdVenta, Long consecutivo, BigDecimal primDep, BigDecimal primRec, String fechaCarga, String remesa,int indicador) {
		StringBuffer sbquery;                                                     
		                                                 
		Query query;  
		String res;
		try {
		
			sbquery = new StringBuffer();
			sbquery.append("INSERT INTO COLECTIVOS_PRIMAS_DEPOSITO \n");                 
			sbquery.append(" (COPD_CANAL , \n");                 
			sbquery.append("  COPD_RAMO, \n");                 
			sbquery.append("  COPD_IDVENTA , \n");                 
			sbquery.append("  COPD_CONSECUTIVO , \n");                 
			sbquery.append("  COPD_PRIMA_DEP , \n");                 
			sbquery.append("  COPD_PRIMA_REC , \n");                 
			sbquery.append("  COPD_FECHA_CARGA, \n");                 
			sbquery.append("  COPD_REMESA , \n");
			sbquery.append("  COPD_CAMPON1 ) \n");
			sbquery.append("VALUES ( \n");                 
			sbquery.append(canal).append(",");                 
			sbquery.append(inRamo).append(",");                 
			sbquery.append(inIdVenta).append(",");                 
			sbquery.append(consecutivo).append(",");                         
			sbquery.append(primDep).append(",");                         
			sbquery.append(primRec).append(",");                         
			sbquery.append("to_date('"+fechaCarga+"', 'dd/MM/yyyy'),") ;                                               
			sbquery.append(remesa).append(",") ;
			sbquery.append(indicador).append(")");
			
			                                       		
			query = getSession().createSQLQuery(sbquery.toString());                     			                                                                        	                          
							                                                                
			query.executeUpdate();                        
		
			res = "Registros guardados correctamente";
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}


	@Override
	public <T> int validaGuardar(int canal, Short ramo, int id, String desc2, String desc3)
			throws Exception {
		List<T> lista = null;
		StringBuilder sb;
		Query query; 
		int consecutivo = 0;
		try{
			sb = new StringBuilder();
			sb.append("select decode(pd.COPD_CAMPON1,null,2,1,0) \n");
			sb.append(" from colectivos_primas_deposito pd\n");
			sb.append("where pd.copd_canal="+canal+" \n");			
			sb.append(" and pd.copd_ramo="+ramo+"\n");
			sb.append(" and pd.copd_idventa="+id+"\n");
			
//			sb.append(" and pd.COPD_PRIMA_DEP="+desc2+"\n");
//			sb.append(" and pd.COPD_PRIMA_REC="+desc3+"\n");						
			
			query = this.getSession().createSQLQuery(sb.toString());
			lista = query.list();
			if(!lista.isEmpty()) {
				consecutivo = ((BigDecimal) lista.get(0)).intValue();
			} 
			
		} catch (Exception e){
			throw new Exception("Error.", e);
		}return consecutivo;
	}


	public <T> List<T> cargaGuardados(int canal, Short ramo,String fechaCarga) throws Exception {
		List<T> idVenta = null;		
		StringBuilder sb;
		Query query; 
		try{
			
			sb = new StringBuilder();

			sb.append(" select pd.copd_fecha_carga,pd.copd_ramo,pd.copd_idventa\n");			
			sb.append(" from colectivos_primas_deposito pd \n");
			sb.append(" where pd.copd_canal="+canal+" \n");
			sb.append(" and pd.copd_ramo="+ramo+" \n");
			//sb.append(" and pd.copd_idventa="+inIdVenta+" \n");					
			sb.append(" and pd.copd_fecha_carga = to_Date('"+ fechaCarga +"', 'dd/MM/yyyy')\n");
			
			query = this.getSession().createSQLQuery(sb.toString());
			idVenta = query.list();
			
			return idVenta;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> consultaProductosColectivos(String producto) throws Exception {
		List<String> productos = new ArrayList<String>();		
		StringBuilder sb = new StringBuilder()
			.append(" SELECT DISTINCT COPA_VVALOR4 FROM COLECTIVOS_PARAMETROS ")
			.append(" WHERE  COPA_ID_PARAMETRO = 'IDVENTA' ");
			if(producto.equals("0")){
				sb.append(" AND COPA_NVALOR4 IN (5,6,7,8) ");				
			}else{
				sb.append(" AND COPA_NVALOR4 = ").append(producto);
			}
			sb.append(" AND COPA_VVALOR4 <> 'BT' ")
			.append("ORDER BY COPA_VVALOR4");				 		 		
			
		Query query; 
		try{
			query = this.getSession().createSQLQuery(sb.toString());
			productos = (List<String>)query.list();
			
			return productos;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
												
}
