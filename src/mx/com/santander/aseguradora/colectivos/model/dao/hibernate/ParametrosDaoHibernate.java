package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * 
 * @author Sergio Plata
 *
 */
@Repository
public class ParametrosDaoHibernate extends HibernateDaoSupport implements ParametrosDao{

	@Autowired
	public ParametrosDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().update(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
		
			for(T objeto: lista){
				
				this.getHibernateTemplate().save(objeto);
			}
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) {
		// TODO Auto-generated method stub
		return (T) this.getHibernateTemplate().get(objeto, id);
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().loadAll(objeto);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) {
		// TODO Auto-generated method stub
		
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuffer consulta = new StringBuffer(150);
				consulta.append("from Parametros P \n");
				consulta.append("where 1=1 \n");
				consulta.append(filtro);
						
				Query qry = sesion.createQuery(consulta.toString());
				
				
				List<T> lista = qry.list();
				
				return lista;
			}
			
		});
	}
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerCatalogoFechasAjuste(Class<T> objeto) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().loadAll(objeto);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerCatalogoFechasAjuste(final String filtro) {
		// TODO Auto-generated method stub
		
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				
				StringBuffer consulta = new StringBuffer(150);
				consulta.append("select P.COPA_CD_PARAMETRO, P.COPA_ID_PARAMETRO ID, P.COPA_DES_PARAMETRO DES, P.COPA_NVALOR1, P.COPA_NVALOR2, P.COPA_NVALOR3, P.COPA_NVALOR4, \n");
				consulta.append("TRUNC(CR.CORE_FE_DESDE + P.COPA_NVALOR4), NVL(P.COPA_NVALOR6,0) \n");
				consulta.append("from COLECTIVOS_PARAMETROS p \n");
				consulta.append("inner join  colectivos_recibos cr \n");
				consulta.append("ON(CR.CORE_CASU_CD_SUCURSAL = P.COPA_NVALOR1 \n");
				consulta.append("and CR.CORE_CARP_CD_RAMO = P.COPA_NVALOR2    \n");
				consulta.append("and CR.CORE_CAPO_NU_POLIZA = P.COPA_NVALOR3) \n");
				consulta.append("WHERE p.COPA_DES_PARAMETRO='RECIBO' \n");
				consulta.append("and p.COPA_ID_PARAMETRO='AJUSTE' \n");
				consulta.append(filtro);
				consulta.append(" AND CR.CORE_NU_CONSECUTIVO_CUOTA = \n");
				consulta.append("(SELECT min(CR.CORE_NU_CONSECUTIVO_CUOTA) \n");
				consulta.append("FROM colectivos_recibos cr \n");
				consulta.append("where CR.CORE_CASU_CD_SUCURSAL = p.COPA_NVALOR1 \n");
				consulta.append("and CR.CORE_CARP_CD_RAMO = p.COPA_NVALOR2 \n");
				consulta.append("and CR.CORE_CAPO_NU_POLIZA = p.COPA_NVALOR3 \n");
				consulta.append("and CR.CORE_ST_RECIBO = 1) ");			
				consulta.append("ORDER BY P.COPA_NVALOR3 DESC ");

				Query qry = sesion.createSQLQuery(consulta.toString());
				
				List<T> lista = qry.list();
				
				return lista;
			}
			
		});
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerCatalogoEndoso(final String filtro) {
		// TODO Auto-generated method stub
		
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuffer consulta = new StringBuffer(150);
				consulta.append("from CatEndosos P \n");
				consulta.append("where 1=1 \n");
				consulta.append(filtro);
						
				Query qry = sesion.createQuery(consulta.toString());
				
				
				List<T> lista = qry.list();
				
				return lista;
			}
			
		});
	}
	
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Long siguente() throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			StringBuilder consulta = new StringBuilder(100);
			consulta.append("Select max(nvl(P.copaCdParametro,0)) + 1      	\n");
			consulta.append("from Parametros P							    \n");
		
			Query qry = this.getSession().createQuery(consulta.toString());
			
			Long valParamnMax = Long.parseLong(qry.list().get(0).toString());
	
			return valParamnMax;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
}

	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Long siguienteCdParam() throws Excepciones {
		// TODO Auto-generated method stub
		String res = null;
		Long next = null;
    	StringBuilder query = null;
    	List<Object> lista = null;
    	Session session;
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<Object>();
    		session = this.getHibernateTemplate().getSessionFactory().openSession();
    		
    		query.append(" select max(copa_cd_parametro)+1   \n");
    		query.append(" from colectivos_parametros   \n");
    		lista = session.createSQLQuery(query.toString()).list();
    		res = lista.get(0).toString();
    		next = Long.parseLong(res);
    		session.clear();
    		session.close();
   
    		
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error al recuperar siguiente cd colectivos_parametros", e);
		}
    	
    	return next;
	}

	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS) 
	public <T> List<T> obtenerProductos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
    	StringBuilder query = null;
    	List<T> lista = null;
    	Session session;
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<T>();
    		session = this.getHibernateTemplate().getSessionFactory().openSession();
    		
    		//query.append("select a.alpr_cd_producto CD_PRODUCTO,   \n");
    		//query.append("       a.alpr_de_producto DESC_PRODUCTO  \n");
    		query.append("select a.*   \n");
    		query.append("  from alterna_productos a,     \n");
    		query.append("       colectivos_coberturas co \n");
    		query.append(" where a.alpr_cd_ramo = co.cocb_carp_cd_ramo  \n");
    		query.append("   and a.alpr_cd_producto = co.cocb_capu_cd_producto \n");
    		query.append("   and co.cocb_casu_cd_sucursal = 1 \n");
    		query.append(filtro);
    		query.append(" group by a.alpr_cd_ramo,a.alpr_cd_producto, \n");
    		query.append("          a.alpr_de_producto,a.alpr_dato1,   \n");
    		query.append("          a.alpr_dato2,a.alpr_dato3,         \n");
    		query.append("          a.alpr_dato4,a.alpr_dato5,         \n");
    		query.append("          a.alpr_dato6\n");

    		lista = session.createSQLQuery(query.toString()).list();

    		session.clear();
    		session.close();
   
    		
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error al recuperar siguiente cd colectivos_parametros", e);
		}
    	
    	return lista;
    }

	public Integer existe(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		Integer existe = null;
		String a;
    	StringBuilder query = null;
    	List<Object> lista = null;
    	Session session;
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<Object>();
    		session = this.getHibernateTemplate().getSessionFactory().openSession();
    		
    		query.append(" select count(cp.copa_cd_parametro)   \n");
    		query.append(" from colectivos_parametros cp   \n");
    		query.append(filtro);
    		
    		lista = session.createSQLQuery(query.toString()).list();
    		a = lista.get(0).toString();
    		existe = Integer.parseInt(a);   		
    		session.clear();
    		session.close();
   
    		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error al recuperar existe en colectivos_parametros", e);
		}
    	
    	return existe;
	}

	
	public <T> List<T> obtenerCanalVenta(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		StringBuilder consulta = new StringBuilder(500);
		//consulta.append("select distinct(P.copaVvalor1) ,  \n"); 
		//consulta.append("       P.copaNvalor4    \n");
		consulta.append("from Parametros P		 \n");
		consulta.append("where 1=1      		 \n");
		consulta.append(filtro);
		consulta.append(" order by P.copaVvalor1  asc \n");
	
		Query qry = this.getSession().createQuery(consulta.toString());
		
		List<T> canalVenta = qry.list();
		
		return canalVenta;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao#
	 * consultarConfiguracionComisiones(mx.com.santander.aseguradora.colectivos.
	 * model.bussinessObject.Parametros)
	 */
	@Override
	public List<Parametros> consultarConfiguracionComisiones(Parametros filtro) throws Excepciones {
		StringBuilder consulta = new StringBuilder(500);
		// consulta.append("select distinct(P.copaVvalor1) , \n");
		// consulta.append(" P.copaNvalor4 \n");
		consulta.append("from Parametros P		 \n");
		consulta.append("where P.copaDesParametro  like '");
		consulta.append(filtro.getCopaDesParametro());
		consulta.append("%' ");
		consulta.append(" and P.copaIdParametro = '");
		consulta.append(filtro.getCopaIdParametro());
		consulta.append("' and P.copaCdParametro in (select max(Pa.copaCdParametro) from Parametros Pa  ");
		consulta.append("where Pa.copaDesParametro  like '");
		consulta.append(filtro.getCopaDesParametro());
		consulta.append("%' ");
		consulta.append(" and Pa.copaIdParametro = '");
		consulta.append(filtro.getCopaIdParametro());
		consulta.append("' group by Pa.copaDesParametro, Pa.copaDesParametro  )");
		consulta.append(" order by copaDesParametro  \n");
		// consulta.append(" group by copaDesParametro \n");

		Query qry = this.getSession().createQuery(consulta.toString());

		// Criteria criteria = getSession().createCriteria(Parametros.class);
		// criteria.setProjection(Projections.distinct(Projections.property("copaDesParametro")));
		// qry.set

		List<Parametros> res = (List<Parametros>) qry.list();

		return res;
	}

	
	
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao#validaParametrosComision(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros)
	 */
	@Override
	public void validaParametrosComision(Parametros parametros) throws Excepciones {
		 
		StringBuilder consulta = new StringBuilder(500);
		// consulta.append("select distinct(P.copaVvalor1) , \n");
		// consulta.append(" P.copaNvalor4 \n");
		consulta.append("from Parametros P		 \n");
		consulta.append("where P.copaDesParametro  = '");
		consulta.append(parametros.getCopaDesParametro());
		consulta.append("' and copaIdParametro = '");
		consulta.append(parametros.getCopaIdParametro());
		consulta.append("' and copaVvalor1 = '");
		consulta.append(parametros.getCopaVvalor1());
		consulta.append("' and copaVvalor2 = '");
		consulta.append(parametros.getCopaVvalor2());
		consulta.append("' and copaNvalor1 = ");
		consulta.append(parametros.getCopaNvalor1());
		consulta.append(" and copaNvalor2 = ");
		consulta.append(parametros.getCopaNvalor2());
		consulta.append(" and copaNvalor3 = ");
		consulta.append(parametros.getCopaNvalor3());
		
		consulta.append(" and copaNvalor4 = ");
		consulta.append(parametros.getCopaNvalor4());
		
		Query qry = this.getSession().createQuery(consulta.toString());

		List<Parametros> res = (List<Parametros>) qry.list();
		
		if (res.size() > 0) {
			throw new Excepciones("Registro duplicado.");
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao#
	 * consultarConfiguracionComisionesDetalle(mx.com.santander.aseguradora.
	 * colectivos.model.bussinessObject.Parametros)
	 */
	@Override
	public List<Object> consultarConfiguracionComisionesDetalle(Parametros filtro) throws Excepciones {
		StringBuilder consulta = new StringBuilder();

		consulta.append(" select cp.*, 	 \n");
		consulta.append(" crp.CARP_DE_RAMO RAMO  \n");
		consulta.append(" , nvl(cpol.COPA_NVALOR3,0) poliza  \n");
		consulta.append(" , nvl(cventa.COPA_VVALOR1, 'RECURRENTE') canal_venta,  \n");
		consulta.append(" p.ALPR_DE_PRODUCTO producto,  \n");
		consulta.append(" ap.ALPL_DE_PLAN plan  \n");
		consulta.append(" from COLECTIVOS_PARAMETROS cp  \n");
		consulta.append(" join CART_RAMOS_POLIZAS crp on cp.COPA_NVALOR2 = crp.CARP_CD_RAMO  \n");
		consulta.append(
				" left join COLECTIVOS_PARAMETROS cpol on cpol.copa_Nvalor1 = 1 and cpol.copa_Des_Parametro= 'POLIZA' and cpol.copa_id_parametro = 'GEP' and  cpol.copa_Nvalor2 = cp.copa_Nvalor2 and cpol.COPA_NVALOR3 = cp.COPA_NVALOR3  \n");
		consulta.append(
				" left join COLECTIVOS_PARAMETROS cventa on cventa.copa_Id_Parametro = 'IDVENTA' and cventa.copa_Nvalor1 = 1 and cventa.COPA_NVALOR2 = cp.COPA_NVALOR2  and  cventa.COPA_NVALOR4 = cp.COPA_NVALOR4   \n");
		consulta.append(
				" join ALTERNA_PRODUCTOS p on p.ALPR_CD_RAMO = cp.COPA_NVALOR2 and  p.ALPR_DATO3 = cp.COPA_NVALOR4 and p.ALPR_CD_PRODUCTO = cp.COPA_VVALOR1  \n");
		consulta.append(
				" join ALTERNA_PLANES ap on  ap.ALPL_CD_RAMO = cp.COPA_NVALOR2 and ap.ALPL_CD_PRODUCTO = cp.COPA_VVALOR1 and ap.ALPL_CD_PLAN = cp.COPA_VVALOR2  \n");
		consulta.append(" where cp.copa_id_parametro = '");
		consulta.append(filtro.getCopaIdParametro());
		consulta.append("' and cp.COPA_DES_PARAMETRO = '");
		consulta.append(filtro.getCopaDesParametro());
		consulta.append("' ");
		Query qry = this.getSession().createSQLQuery(consulta.toString());
		@SuppressWarnings("unchecked")
		List<Object> lista = (List<Object>) qry.list();
		return lista;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao#eliminaParametrosComision(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros)
	 */
	@Override
	public void eliminaParametrosComision(Parametros parametros) throws Excepciones {
		StringBuilder consulta = new StringBuilder();
		consulta.append(" delete from COLECTIVOS_PARAMETROS cp  \n");
		consulta.append(" where cp.copa_id_parametro = '");
		consulta.append(parametros.getCopaIdParametro());
		consulta.append("' and cp.COPA_DES_PARAMETRO = '");
		consulta.append(parametros.getCopaDesParametro());
		consulta.append("' ");
		
		Query qry = this.getSession().createSQLQuery(consulta.toString());
		try {
			qry.executeUpdate();
		} catch (HibernateException e) {
			throw new Excepciones(e.getMessage());
		}
		
	}
	
	
	
}



