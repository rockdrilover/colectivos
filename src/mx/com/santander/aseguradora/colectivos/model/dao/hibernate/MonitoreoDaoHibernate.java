package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.util.List;

import org.apache.poi.hssf.record.formula.functions.T;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.model.dao.MonitoreoDao;

@Repository
public class MonitoreoDaoHibernate extends HibernateDaoSupport implements MonitoreoDao {
	
	@Autowired
	public MonitoreoDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Override
	public <T> List<T> cargaPolizasEmiCan(final short inRamo,final Integer inPoliza,final Integer inIdVenta,final String fInicio,final String fFin,final int opcion,final int tarea)throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			if(tarea == 1){
				sb.append("select count(*), sum(c.coce_mt_prima_subsecuente) \n");
			} else if(tarea == 2){
				sb.append("select count(*), sum(c.coce_mt_devolucion) \n");
			}
			sb.append("from colectivos_certificados c \n");
			sb.append("where c.coce_casu_cd_sucursal = 1 \n");
			sb.append("and c.coce_carp_cd_ramo = ").append(inRamo).append(" \n");
			
			if(inPoliza == 0){
				sb.append("and c.coce_sub_campana = '").append(inIdVenta).append("' \n");
			} else {
				sb.append("and c.coce_capo_nu_poliza = ").append(inPoliza).append(" \n");
			}
			
			if(tarea == 1){
				sb.append("and c.coce_fe_carga >= to_date('").append(fInicio).append("','dd/MM/yyyy') \n");
				sb.append("and c.coce_fe_carga <= to_date('").append(fFin).append("','dd/MM/yyyy') \n");
				sb.append("and c.coce_st_certificado in (1,2) \n");
			} else if(tarea == 2){
				sb.append("and c.coce_fe_anulacion >= to_date('").append(fInicio).append("','dd/MM/yyyy') \n");
				sb.append("and c.coce_fe_anulacion <= to_date('").append(fFin).append("','dd/MM/yyyy') \n");
				sb.append("and c.coce_st_certificado in (10,11) \n");
			}
			sb.append("and c.coce_nu_certificado > 0 \n");
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	@Override
	public <T> List<T> cargaPolizasConcilia(final short inRamo,final Integer inPoliza,final String fInicio,final String fFin,final int opcion,final int tarea) throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select  'Conciliados' as detalle, count(*) as registros, decode(sum(c.cocu_monto_prima), null, 0, sum(c.cocu_monto_prima)) as primas \n");
			sb.append("from colectivos_cuentas c \n");
			sb.append("where c.cocu_ramo = ").append(inRamo).append(" \n");
			sb.append("and c.cocu_poliza = ").append(inPoliza).append(" \n");
			sb.append("and c.cocu_fecha_ingreso >= to_date('").append(fInicio).append("','dd/MM/yyyy') \n");
			sb.append("and c.cocu_fecha_ingreso <= to_date('").append(fFin).append("','dd/MM/yyyy') \n");
			sb.append("and c.cocu_calificador in (1,2) \n");
			sb.append("union all \n");
			sb.append("      select 'No Conciliados', count(*), decode(sum(cc.cocu_monto_prima), null, 0, sum(cc.cocu_monto_prima)) \n");
			sb.append("      from colectivos_cuentas cc \n");
			sb.append("      where cc.cocu_ramo = ").append(inRamo).append(" \n");
			sb.append("      and cc.cocu_poliza = ").append(inPoliza).append(" \n");
			sb.append("      and cc.cocu_fecha_ingreso >= to_date('").append(fInicio).append("','dd/MM/yyyy') \n");
			sb.append("      and cc.cocu_fecha_ingreso <= to_date('").append(fFin).append("','dd/MM/yyyy') \n");
			sb.append("      and cc.cocu_calificador = 0 \n");
			
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	@Override
	public <T> List<T> cargaPolizasTimbrados(final short inRamo,final Integer inPoliza,final Integer inIdVenta,final String fInicio,final String fFin,final int opcion,final int tarea) throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select 'Recibos Timbrados' as detalle, count(*) as registro, decode(sum(c.cofa_mt_tot_pma), null, 0, sum(c.cofa_mt_tot_pma)) \n");
			sb.append("from cart_emisioncfd_i t,colectivos_facturacion c \n");
			sb.append("where c.cofa_casu_cd_sucursal = 1 \n");
			sb.append("and c.cofa_carp_cd_ramo = ").append(inRamo).append(" \n");
			if (inPoliza == 0){
				sb.append("and c.cofa_capo_nu_poliza between ").append(inIdVenta).append("00000000 and ").append(inIdVenta).append("99999999 \n");
			} else if(opcion == 2){
				sb.append("and c.cofa_capo_nu_poliza = ").append(inPoliza).append(" \n");
			}
			sb.append("and c.cofa_fe_facturacion >= to_date('").append(fInicio).append("','dd/MM/yyyy') \n");
			sb.append("and c.cofa_fe_facturacion <= to_date('").append(fFin).append("','dd/MM/yyyy') \n");
			sb.append("and t.canal =  c.cofa_casu_cd_sucursal \n");
			sb.append("and t.ramo = c.cofa_carp_cd_ramo \n");
			sb.append("and t.poliza = c.cofa_capo_nu_poliza \n");
			sb.append("and t.folio = c.cofa_nu_recibo_fiscal \n");
			sb.append("and t.sellosat is not null \n");
			sb.append("union all \n");
			sb.append("		 select 'Recibos no Timbrados', count(*) as registro, decode(sum(cc.cofa_mt_tot_pma), null, 0, sum(cc.cofa_mt_tot_pma)) \n");
			sb.append("      from cart_emisioncfd_i tt,colectivos_facturacion cc \n");
			sb.append("      where cc.cofa_casu_cd_sucursal = 1 \n");
			sb.append("      and cc.cofa_carp_cd_ramo = ").append(inRamo).append(" \n");
			if (inPoliza == 0){
				sb.append("and cc.cofa_capo_nu_poliza between ").append(inIdVenta).append("00000000 and ").append(inIdVenta).append("99999999 \n");
			} else if(opcion == 2){
				sb.append("and cc.cofa_capo_nu_poliza = ").append(inPoliza).append(" \n");
			}
			sb.append("      and cc.cofa_fe_facturacion >= to_date('").append(fInicio).append("','dd/MM/yyyy') \n");
			sb.append("      and cc.cofa_fe_facturacion <= to_date('").append(fFin).append("','dd/MM/yyyy') \n");
			sb.append("      and tt.canal = cc.cofa_casu_cd_sucursal \n");
			sb.append("      and tt.ramo = cc.cofa_carp_cd_ramo \n");
			sb.append("      and tt.poliza = cc.cofa_capo_nu_poliza \n");
			sb.append("      and tt.folio = cc.cofa_nu_recibo_fiscal \n");
			sb.append("      and tt.sellosat is null \n");
			
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
	
	@Override
	public <T> List<T> cargaPolizasEndosos(final short inRamo,final Integer inPoliza,final Integer inIdVenta,final String fInicio,final String fFin,final int opcion,final int tarea) throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select c.coed_capo_nu_poliza, substr(c.coed_campov1,1,1) tipo, c.coed_campov1, c.coed_fecha_inicio, c.coed_fecha_fin, C.COED_MTO_NVO, \n");
			sb.append("nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'O',1,1) + 1,instr(c.coed_campov4, '|',1,2) -instr(c.coed_campov4, 'O',1,1) -1)),0) DERECHOS, \n");
			sb.append("nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'I',1,1) + 1,instr(c.coed_campov4, '|',1,3) -instr(c.coed_campov4, 'I',1,1) -1)),0) RECARGOS, \n");
			sb.append("nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'V',1,1) + 2,instr(c.coed_campov4, '|',1,4) -instr(c.coed_campov4, 'V',1,1) -2)),0) IVA, \n");
			sb.append("nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'T',1,1) + 2,instr(c.coed_campov4, '|',1,5) -instr(c.coed_campov4, 'T',1,1) -2)),0) TOTAL \n");
			sb.append("from colectivos_endosos c \n");
			sb.append("where c.coed_casu_cd_sucursal = 1 \n");
			sb.append("and c.coed_carp_cd_ramo = ").append(inRamo).append(" \n");
			if (inPoliza == 0){
				sb.append("and c.coed_capo_nu_poliza between ").append(inIdVenta).append("00000000 and ").append(inIdVenta).append("99999999 \n");
			} else if(opcion == 2){
				sb.append("and c.coed_capo_nu_poliza = ").append(inPoliza).append(" \n");
			}
			sb.append("and c.coed_nu_certificado = 0 \n");
			sb.append("and c.coed_fe_recepcion >= to_date('").append(fInicio).append("','dd/MM/yyyy') \n");
			sb.append("and c.coed_fe_recepcion <= to_date('").append(fFin).append("','dd/MM/yyyy') \n");
			
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
}
