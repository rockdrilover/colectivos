/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CargaId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartEmisioncfdI;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Endosos;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.FacturacionId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.database.ProcesosPL;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;
import oracle.jdbc.OracleTypes;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sun.xml.ws.org.objectweb.asm.Type;

/**
 * @author Sergio Plata
 *
 */
@Repository
public class ProcesosDaoHibernate extends HibernateDaoSupport implements ProcesosDao {

	@Autowired
	public ProcesosDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public void emisionMasiva(Short canal, Short ramo, Long poliza, Integer idVenta) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
									
			EmisionMasiva emision = new EmisionMasiva(ds);
			emision.execute(canal, ramo, poliza, idVenta);				
		}
		
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}
			
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class )
	public void cancelacionmasiva(Short canal, Short ramo, Long poliza,
			Integer causa, Colectivos.OPC_CANACEL opc) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			CancelacionMasiva cancelacion = new CancelacionMasiva(ds);
			cancelacion.execute(canal, ramo, poliza, causa, opc.ordinal()+1);
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		

	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class )
	@SuppressWarnings("unchecked")
	public String renovacion(Short canal, Short ramo, Long poliza) throws Excepciones {
		// TODO Auto-generated method stub
		
       try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			Renovacion renovacion = new Renovacion(ds);
			String respuesta = (String) ((Map)renovacion.execute(canal, ramo, poliza, 0)).get("respuesta");
			return respuesta;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	/**
	 * 
	 * @author Fernando Gomez
	 * @throws Excepciones 
	 *
	 */
	@SuppressWarnings("unchecked")
	public String cancelacionCri(Short inCanal, Short inRamo, Long inPoliza,
			Integer inIdVenta, Integer causaAnula) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			CancelacionCRI cancelacionCri = new CancelacionCRI(ds);
			String respuesta = (String) ((Map)cancelacionCri.execute(inCanal, inRamo, inPoliza, inIdVenta, causaAnula)).get("respuesta");
			return respuesta;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	/**
	 * 
	 */
	public Map<String, Object> preFacturacion(Short canal, Short ramo) {
		// TODO Auto-generated method stub
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
		PreFacturacion preFacturacion = new PreFacturacion(ds);
		
		parametros = preFacturacion.execute(canal, ramo);
		
		return parametros;
	}
	
	@SuppressWarnings("unchecked")
	public void cancelaCertif(final CertificadoId  id, final Short cdanula ,final Date fechaanula) {
		//			
		this.getHibernateTemplate().executeFind(new HibernateCallback() {
			public Object doInHibernate(Session sesion)
			throws HibernateException , SQLException {
				Long numovimiento;
			    DataSource ds;	
				//DataSource 
				ds = SessionFactoryUtils.getDataSource(sesion.getSessionFactory());
        		NoMovimiento nomovimiento = new NoMovimiento(ds);		
        		numovimiento =  Long.parseLong(((Map)nomovimiento.execute(id.getCoceCasuCdSucursal(),id.getCoceCarpCdRamo(), id.getCoceCapoNuPoliza(), id.getCoceNuCertificado())).get("returnname").toString());
                // otro metodo		
        		GeneraMovimiento movimiento = new GeneraMovimiento(ds);		
        		movimiento.execute(id.getCoceCasuCdSucursal(),id.getCoceCarpCdRamo(), id.getCoceCapoNuPoliza(), id.getCoceNuCertificado());		
        		//
        		

                
				StringBuffer actSt = new StringBuffer(300);				
				actSt.append("update Certificado certif \n" +
						"set certif.estatus.alesCdEstatus = 11 \n" +
						", certif.coceNuMovimiento  =:numovimiento \n" +
						", certif.coceFeAnulacion   =trunc(sysdate)\n" +
						", certif.coceFeAnulacionCol=:fechaanula\n" +
						", certif.coceFeEstatus     =trunc(sysdate)\n" +
						", certif.coceCdCausaAnulacion = :cdanula\n"+
						"where certif.id            =:id");
				
				Query queryAct = sesion.createQuery(actSt.toString());
				queryAct.setParameter("id", id);
				queryAct.setParameter("numovimiento", numovimiento);
				queryAct.setParameter("cdanula", cdanula);
				queryAct.setParameter("fechaanula", fechaanula);
				queryAct.executeUpdate();
				return null;
			}

		});
		
	}	
	
	/**
	 * @author Omar Cort�s
	 * @throws Excepciones 
	 */
	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public String procesoRenovacionPolizas(Short canal, Short ramo, Long poliza) throws Excepciones{
			String resp="";
		try {
			System.out.println("Entre a ProcesosDaoHibernate.procesoRenovacionPolizas");
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			RenovacionPolizas ejecuta = new RenovacionPolizas(ds);
			resp = (String)((Map)ejecuta.execute(canal, ramo, poliza)).get("respuesta");
			System.out.println("resp " + resp);
			System.out.println("Salgo de ProcesosDaoHibernate.procesoRenovacionPolizas");
		return resp;
		
		}
		catch (DataIntegrityViolationException e) {
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public void ejecutaEndososPU(FacturacionId id) throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			EndososPUnica ejecuta = new EndososPUnica(ds);
			//System.out.println("Executo Proceso");
			ejecuta.execute(id);
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public void ejecutaFacturacion(FacturacionId id) throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			Facturacion ejecuta = new Facturacion(ds);
			ejecuta.execute(id);
			
			/*
			StringBuilder actmontosf = new StringBuilder() ;
			actmontosf.append("update colectivos_facturacion cf \n");
			actmontosf.append("set cf.cofa_nu_recibo_fiscal = 99999.4 \n");
			actmontosf.append("where cf.cofa_casu_cd_sucursal =  " + id.getCofaCasuCdSucursal()  +  " \n");
			actmontosf.append("and   cf.cofa_carp_cd_ramo     =  " + id.getCofaCarpCdRamo()   +  " \n");
			actmontosf.append("and   cf.cofa_capo_nu_poliza   =  " + id.getCofaCapoNuPoliza() +  " \n");
			actmontosf.append("and   cf.cofa_nu_recibo_fiscal =  0 \n");
			
			Query queryAct = getSession().createSQLQuery(actmontosf.toString());
			queryAct.executeUpdate();
			*/
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			System.out.println(e);
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			throw new Excepciones("Error.", e);
		}
		
	}
	
	public CartEmisioncfdI estatusCobradoRecibo(FacturacionId id) throws Excepciones {

		try { 
			StringBuffer consulta = new StringBuffer(100);
			consulta.append("select nvl(max(R.id.careNuRecibo),0)    \n");
			consulta.append("  from Facturacion F        \n");
			consulta.append("      ,Recibo R             \n");
			consulta.append("  where F.id.cofaCasuCdSucursal = :canal                   \n");
			consulta.append("    and F.id.cofaCarpCdRamo     = :ramo                    \n");
		    consulta.append("    and F.id.cofaCapoNuPoliza   = :poliza                  \n");
		    consulta.append("    and F.id.cofaNuCertificado  = 0                        \n");
		    consulta.append("    and F.id.cofaNuReciboCol    = :reciboCol               \n");
		    consulta.append("    and R.id.careCasuCdSucursal = F.id.cofaCasuCdSucursal	\n");
		    consulta.append("    and R.careCarpCdRamo        = F.id.cofaCarpCdRamo		\n");
		    consulta.append("    and R.careCapoNuPoliza      = F.id.cofaCapoNuPoliza	\n");
		    consulta.append("    and R.careCaceNuCertificado = F.id.cofaNuCertificado	\n");
		    consulta.append("    and R.id.careNuRecibo       = F.cofaNuReciboFiscal	    \n");
		    
		    Query queryRecibo = getSession().createQuery(consulta.toString());
		    
		    queryRecibo.setParameter("canal", id.getCofaCasuCdSucursal());
		    queryRecibo.setParameter("ramo", id.getCofaCarpCdRamo());
		    queryRecibo.setParameter("poliza", id.getCofaCapoNuPoliza());
		    queryRecibo.setParameter("reciboCol", id.getCofaNuReciboCol());
		    
		    BigDecimal recibo = (BigDecimal)queryRecibo.list().get(0);
		    CartEmisioncfdI cfdi = null;
		    
		    if (recibo.intValue() > 0){
		    	consulta = null;
		    	consulta = new StringBuffer();
		    	
		    	consulta.append("update Recibo R                         \n");
		    	consulta.append("   set R.careStRecibo = 4                \n");
		    	consulta.append(" where R.id.careCasuCdSucursal = :canal  \n");
		    	consulta.append("   and R.careCarpCdRamo     = :ramo      \n");
		    	consulta.append("   and R.careCapoNuPoliza   = :poliza    \n");
		    	consulta.append("   and R.careCaceNuCertificado = 0       \n");
		    	consulta.append("   and R.id.careNuRecibo       = :recibo \n");
		    	
		    	queryRecibo = getSession().createQuery(consulta.toString());
			    
			    queryRecibo.setParameter("canal", id.getCofaCasuCdSucursal());
			    queryRecibo.setParameter("ramo", id.getCofaCarpCdRamo());
			    queryRecibo.setParameter("poliza", new Long(id.getCofaCapoNuPoliza()).intValue());
			    queryRecibo.setParameter("recibo", recibo);
			    
			    queryRecibo.executeUpdate();
			    
			    
			    consulta = null;
		    	consulta = new StringBuffer();
		    	
		    	consulta.append("update CartReservaDigI RE                  \n");
		    	consulta.append("   set RE.cardStProceso = 'COL'            \n");
		    	consulta.append(" where RE.id.cardCasuCdSucursal = :canal   \n");
		    	consulta.append("   and RE.cardCarpCdRamo        = :ramo    \n");
		    	consulta.append("   and RE.cardCapoNuPoliza      = :poliza  \n");
		    	consulta.append("   and RE.cardCaceNuCertificado = 0        \n");
		    	consulta.append("   and RE.id.cardCareNuRecibo   = :recibo  \n");
		    	
		    	queryRecibo = getSession().createQuery(consulta.toString());
			    
			    queryRecibo.setParameter("canal", id.getCofaCasuCdSucursal());
			    queryRecibo.setParameter("ramo", new Short(id.getCofaCarpCdRamo()));
			    queryRecibo.setParameter("poliza", new Long(id.getCofaCapoNuPoliza()).intValue());
			    queryRecibo.setParameter("recibo", recibo);
			    
			    queryRecibo.executeUpdate();
			    
			    
			    consulta = null;
		    	consulta = new StringBuffer();
		    	
		    	consulta.append("  from CartEmisioncfdI C        \n");
		    	consulta.append(" where C.id.canal     = :canal  \n");
		    	consulta.append("   and C.ramo         = :ramo   \n");
		    	consulta.append("   and C.poliza       = :poliza \n");
		    	consulta.append("   and C.id.folio     = :recibo \n");
		    	
		    	queryRecibo = getSession().createQuery(consulta.toString());
			    
			    queryRecibo.setParameter("canal", id.getCofaCasuCdSucursal());
			    queryRecibo.setParameter("ramo", id.getCofaCarpCdRamo());
			    queryRecibo.setParameter("poliza", new Long(id.getCofaCapoNuPoliza()).intValue());
			    queryRecibo.setParameter("recibo", recibo);
			    
			    cfdi = (CartEmisioncfdI) queryRecibo.list().get(0);
		    	
		    }
		    
			return cfdi;
			
		} catch (Exception e){
			Log.error(e);
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
		
	}


	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public void ejecutaFacturacion5758(Short ramo) throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			Facturacion5758 ejecuta = new Facturacion5758(ds);
			ejecuta.execute(ramo.intValue());
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			System.out.println(e);
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			throw new Excepciones("Error.", e);
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public Map<String, Object> ejecutaPrefactura(CertificadoId id, Integer nNumRecibo) throws Excepciones{
		try {
			/* Instancia de Data Source */
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			/* Llamado ala clase de validacion */		
			PreFacturacion ejecuta = new PreFacturacion(ds);
			/* Ejecucion del metodo de validacion y Regresamos Resuesta */
			return ejecuta.execute(id, nNumRecibo);
		} catch (DataIntegrityViolationException e) {
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
		
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public int facturacion(Short canal, Short ramo){
		// TODO Auto-generated method stub
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		PreFacturacion preFacturacion  = null;
		Facturacion facturacion = null;
		String salida;
		BigDecimal respuesta;
		
		DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
		
		preFacturacion = new PreFacturacion(ds);
		
		parametros = preFacturacion.execute(canal, ramo);
		respuesta = (BigDecimal)parametros.get("P_RESP");
		
		if(respuesta.intValue() == 0){
		
			facturacion = new Facturacion(ds);
			
			salida = (String) ((Map)facturacion.execute(canal, ramo, respuesta.intValue())).get("p_mensaje");
			
			if(salida != null && salida.length() > 0){
				return -1;
			}
			else{
				return 0;
			}
		}
		else
		{
			return -1;
		}
	}

	
	@SuppressWarnings("unchecked")
	public void aplicaEndosoDevolucion(Short inCanal, Short inRamo,
			Integer inIdVenta, PreCarga dato)
			throws Excepciones {
		// TODO Auto-generated method stub
		
		StringBuffer consulta = new StringBuffer();
		List<Object> resultado = new ArrayList<Object>();
		
		try {
			
			consulta.append("       select clc.id.coccCapoNuPoliza      							\n");
			consulta.append("		      ,clc.id.coccNuCertificado     							\n");
			
			if (inIdVenta > 0 && (dato.getCopcPrima() == null || dato.getCopcPrima().length() == 0 || 
					                   Double.parseDouble(dato.getCopcPrima()) == 0))
				consulta.append("		      ,cer.coceMtDevolucion      							\n");
			else
				consulta.append("		      ,cer.coceMtBcoDevolucion      						\n");
			
			consulta.append("		      ,cer.coceMtPrimaSubsecuente   							\n");
			consulta.append("		      ,cer.coceNoRecibo             							\n");
			consulta.append("		  from ClienteCertif clc										\n");
			consulta.append("		      ,Certificado   cer										\n");
			consulta.append("		 where clc.id.coccCasuCdSucursal  = :canal						\n");
			consulta.append("		   and clc.id.coccCarpCdRamo      = :ramo						\n");
			consulta.append("		   and clc.id.coccIdCertificado   = :id							\n");
			consulta.append("		   and cer.id.coceCasuCdSucursal  = clc.id.coccCasuCdSucursal 	\n");
			consulta.append("		   and cer.id.coceCarpCdRamo      = clc.id.coccCarpCdRamo		\n");
			consulta.append("		   and cer.id.coceCapoNuPoliza    = clc.id.coccCapoNuPoliza 	\n");
			consulta.append("		   and cer.id.coceNuCertificado   = clc.id.coccNuCertificado	\n");
			
			if (dato.getId().getCopcSumaAsegurada() != null && dato.getId().getCopcSumaAsegurada().length() > 0 
					&& Double.parseDouble(dato.getId().getCopcSumaAsegurada()) != 0)
				consulta.append("		   and cer.coceMtSumaAsegurada    = :sumaAsegurada			\n");
			
			if (dato.getCopcFeIngreso() != null && dato.getCopcFeIngreso().length() > 0) 
				consulta.append("		   and cer.coceFeSuscripcion      = :fechaIngreso			\n");
			
			consulta.append("		   and cer.coceSubCampana         = :idVenta					\n");
			
			if (inIdVenta > 0 && (dato.getCopcPrima() == null || dato.getCopcPrima().length() == 0 || 
					                   Double.parseDouble(dato.getCopcPrima()) == 0))
				consulta.append("		   and cer.coceMtDevolucion is not null						\n");
			else
				consulta.append("		   and cer.coceMtBcoDevolucion is not null					\n");
			
			
			
			Query q = getSession().createQuery(consulta.toString());
			q.setParameter("canal", inCanal);
			q.setParameter("ramo", inRamo);
			q.setParameter("id", dato.getId().getCopcIdCertificado());
			
			if (dato.getId().getCopcSumaAsegurada() != null && dato.getId().getCopcSumaAsegurada().length() > 0 
					&& Double.parseDouble(dato.getId().getCopcSumaAsegurada()) != 0)
				q.setParameter("sumaAsegurada", new BigDecimal(dato.getId().getCopcSumaAsegurada()));
			
			if (dato.getCopcFeIngreso() != null && dato.getCopcFeIngreso().length() > 0)
				q.setParameter("fechaIngreso", new SimpleDateFormat("dd/MM/yyyy").parse(dato.getCopcFeIngreso()));
			
			q.setParameter("idVenta", inIdVenta.toString());
			
			consulta = null;
			
			resultado = q.list();
			
			if (resultado.size() != 0){
				
				Object[] o = (Object[]) resultado.get(0);
				
				consulta = new StringBuffer();
				
				consulta.append(" select count(*) 	     								\n");
				consulta.append("   from Endosos endoso									\n");
				consulta.append("  where endoso.id.coedCasuCdSucursal   = :canal		\n");
				consulta.append("    and endoso.id.coedCarpCdRamo       = :ramo			\n");
				consulta.append("    and endoso.id.coedCapoNuPoliza     = :poliza		\n");
				consulta.append("    and endoso.id.coedNuCertificado    = :certificado	\n");
				consulta.append("    and endoso.coedIdCertificado       = :id	        \n");
				consulta.append("    and endoso.id.coedCdEndoso         = 200			\n");
				
				q = getSession().createQuery(consulta.toString());
				q.setParameter("canal", inCanal);
				q.setParameter("ramo", inRamo);
				q.setParameter("poliza", (Long)o[0]);
				q.setParameter("certificado", (Long)o[1]);
				q.setParameter("id", (String)dato.getId().getCopcIdCertificado());
				   
				Long existe = (Long) q.list().get(0);
				
				consulta = null;
				
				if (existe == 0){
					
					consulta = new StringBuffer();
					
					/*consulta.append(" select nvl(MAX(endoso.id.coedNuEndosoDetalle),0)+1            	 	        \n");
					consulta.append("       ,to_number(to_char(sysdate,'yyyymm')) 								    \n");
					consulta.append("       ,trunc(sysdate)                       								    \n");
					consulta.append("   from Endosos endoso															\n");
					consulta.append("  where endoso.id.coedNuEndosoGral    = to_number(to_char(sysdate,'yyyymm'))	\n");
					consulta.append("    and endoso.id.coedCdEndoso        = 200									\n");*/
					
					
					consulta.append("  select nvl(MAX(endoso.coed_nu_endoso_detalle),0)+1							\n");
					consulta.append("        ,to_number(to_char(sysdate,'yyyymm'))									\n");
					consulta.append("        ,trunc(sysdate)                       								    \n");
					consulta.append("    from colectivos_endosos endoso												\n");
					consulta.append("   where endoso.coed_nu_endoso_gral    = to_number(to_char(sysdate,'yyyymm'))	\n");
					consulta.append("     and endoso.coed_cd_endoso         = 200									\n");
					
					q = getSession().createSQLQuery(consulta.toString());
					
					Object[] sig = (Object[]) q.list().get(0);
					
					EndososId idE = new EndososId();
					Endosos endo = new Endosos();
					
					idE.setCoedCasuCdSucursal(inCanal);
					idE.setCoedCarpCdRamo(inRamo);
					idE.setCoedCapoNuPoliza((Long)o[0]);
					idE.setCoedNuCertificado((Long)o[1]);
					idE.setCoedNuEndosoGral(((BigDecimal)sig[1]).longValue());
					idE.setCoedNuEndosoDetalle(((BigDecimal)sig[0]).longValue());
					idE.setCoedCdEndoso(200);					
					
					endo.setId(idE);
					
					endo.setCoedIdCertificado(dato.getId().getCopcIdCertificado());
					endo.setCoedCapjCdSucursal(9000);
					endo.setCoedNuRecibo((BigDecimal)o[4]);
					endo.setCoedFeSolicitud((Date)sig[2]);
					endo.setCoedFeRecepcion((Date)sig[2]);
					endo.setCoedFeAplica((Date)sig[2]);
					endo.setCoedCdUsuarioReg("AUTOENDOSO");
					endo.setCoedCdUsuarioAplica("GIOSEG");
					endo.setCoedMtoAnt((BigDecimal)o[3]);
					endo.setCoedMtoNvo((BigDecimal)o[2]);
					endo.setCoedCampov1("V"+inRamo+"-"+sig[1].toString()+sig[0].toString());
					
					this.getHibernateTemplate().save(endo);
					
				}
			}
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}	
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class )
	public void rehabilitacion(Short canal, Short ramo, Long poliza)
			throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			Rehabilitacion rehabilitacion = new Rehabilitacion(ds);
			rehabilitacion.execute(canal, ramo, poliza);
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}
	
	public Double calculaDevolucion(Certificado cer, Double tasaAmortiza,
			Date fechaAnulacion) throws Excepciones {
		// TODO Auto-generated method stub
				
		DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
		Devolucion devo= new Devolucion(ds);
		
		return Double.parseDouble(devo.execute(cer, tasaAmortiza, fechaAnulacion).get("returnname").toString());

	}
	
	
	@SuppressWarnings("unchecked")
	public String endosarPoliza(Short inCanal, Short inRamo, Long inPoliza,
			Integer inIdVenta, BigDecimal recibo, Date feDesde, Date feHasta,
			Integer inOperEndoso) throws Excepciones {
		// TODO Auto-generated method stub

		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			EndosarPoliza endoso = new EndosarPoliza(ds);
			return (String)((Map)(endoso.execute(inCanal, inRamo, inPoliza, inIdVenta, recibo
					             , feDesde, feHasta, inOperEndoso))).get("respuesta");
			
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	public void validaObligados() throws Excepciones {
		// TODO Auto-generated method stub
		StringBuffer valida = new StringBuffer(300);
		
		try {
			valida.append("update colectivos_precarga CP set CP.COPC_REGISTRO = 'El credito rebasa el numero de participantes que intenta emitir en Prima Unica', \n");
			valida.append("                                  CP.COPC_CARGADA = 3  \n");                                                                            
			valida.append(" where CP.COPC_CD_SUCURSAL    = 1                      \n");
			valida.append("   and CP.COPC_CD_RAMO        = 61                     \n");
			valida.append("   and CP.COPC_NUM_POLIZA     = 0                      \n");
			valida.append("   and CP.COPC_TIPO_REGISTRO  = 'VID'                  \n");
			valida.append("   and CP.COPC_CARGADA        = '0'                    \n");
			valida.append("   and CP.COPC_DATO16         = '7'                    \n");
			valida.append("   and ((CP.COPC_DATO7 = '31.54' and nvl(CP.COPC_DATO17,'0') <> '0') or (CP.COPC_DATO7 in ('43.86','43.85') and nvl(CP.COPC_DATO17,'0') <> '1')) \n");
			
			Query queryAct = getSession().createSQLQuery(valida.toString()); 
		
			queryAct.executeUpdate();
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class )
	public void validarMultidisposicion(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta) throws Excepciones {
		try {
			ProcesosPL proceso= new ProcesosPL (getSession().connection());
			proceso.validarMultidisposicion(inCanal, inRamo,inPoliza, inIdVenta); 
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	
	//Mago Manos Magicas 22/05/2015
	public Integer validaFechaNac(ClienteCertif cliente, Long poliza, Integer edadMin, Integer edadMax) throws Excepciones {
		
       try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			System.out.println("DATA SOURCE ----->"+ds);
			Fechas fechas = new Fechas(ds);
			Integer respuesta = (Integer) ((Map)fechas.execute(cliente,poliza,edadMin,edadMax)).get("existe");
			return respuesta;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
		
	}

	public Integer busca_negative(String  wc_paterno_nf, String wc_materno_nf , String  wc_nombre_nf, Date  wc_fecha_nac_cons) throws Excepciones {
	   try {
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			Busca busca = new Busca(ds);
			Integer respuesta = busca.execute(wc_paterno_nf,wc_materno_nf,wc_nombre_nf,wc_fecha_nac_cons);
			return respuesta;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}
	
	public Map<String, Object> buscaCliente(ContratanteDTO contratanteDTO) throws Excepciones {
		Map<String, Object> mResultado = null;
		try { 
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			IniciaAlta alta = new IniciaAlta(ds);
			mResultado = ((Map<String, Object>) alta.execute(contratanteDTO));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mResultado;
	}
	
	public void contaColectivos(short canal, Short ramo, Long poliza, int certif, int ultRecibo, int mtUltRecibo, int operacion, int subOperacion, String fDesde, String fHasta, String fContable, String nombreFac, int estatusRecibo) throws Exception {
		
		try {			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());

			if(ramo == 57 || ramo == 58) {
				Contabilidad1 ejecuta = new Contabilidad1(ds);
				ejecuta.execute(canal,ramo,poliza,certif,ultRecibo,mtUltRecibo,operacion,subOperacion,fDesde,fHasta,fContable,nombreFac,estatusRecibo);
			} else {
				Contabilidad2 ejecuta = new Contabilidad2(ds);
				ejecuta.execute(canal,ramo,poliza,certif,ultRecibo,mtUltRecibo,operacion,subOperacion,fDesde,fHasta,fContable,nombreFac,estatusRecibo);
			}
		} catch (DataIntegrityViolationException e) {
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
		
	}

	@Override
	public void procesoCargaCuenta(Long nRamo, Integer nOperCargo, Double montoAC, Long cuenta, String newFechaFin, String fecha_hoy) throws Excepciones {
		try {									
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			CargaCuentas ejecuta = new CargaCuentas(ds);
			ejecuta.execute(nRamo,nOperCargo,montoAC,cuenta,newFechaFin,fecha_hoy);
		} catch (DataIntegrityViolationException e) {
			throw new ObjetoDuplicado("Error.", e);	
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	@Override
	public Long getNumeroCliente() throws Excepciones {
		try {									
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			GeneraCliente genera = new GeneraCliente(ds);
			return Long.parseLong(genera.execute().get("returnname").toString());
		} catch (DataIntegrityViolationException e) {
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@Override
	public Long getNumeroEndoso(short coedCarpCdRamo, String coedStEndoso) throws Excepciones {
		try {									
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			GeneraNoEndoso genera = new GeneraNoEndoso(ds);			
			return Long.parseLong(genera.execute(coedCarpCdRamo, coedStEndoso).get("returnname").toString());
		} catch (DataIntegrityViolationException e) {
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@Override
	public String migraColectivos(ClienteCertifId id, Short inTipoCliente, Date feOcurrencia, Integer inBandera) throws Excepciones {
		try {
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			MigraColectivos migra = new MigraColectivos(ds);
			return (String)((Map)(migra.execute(id.getCoccCasuCdSucursal(), id.getCoccCarpCdRamo(), id.getCoccCapoNuPoliza(), id.getCoccNuCertificado(), inTipoCliente, id.getCoccNuCliente(), feOcurrencia, inBandera))).get("respuesta");
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	/************************** CLASES PARA PROCESOS DE LA BASE DE DATOS ************************/
	
	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	private class EmisionMasiva extends StoredProcedure{

		public EmisionMasiva(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.EMISION_MASIVA);
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramIdVenta", Types.INTEGER));
			super.compile();
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.StoredProcedure#execute(java.util.Map)
		 */
		public void execute(Short canal, Short ramo, Long poliza, Integer idVenta) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new HashMap<String, Object>();
			
			inParams.put("paramCanal", canal);
			inParams.put("paramRamo", ramo);
			inParams.put("paramPoliza", poliza);
			inParams.put("paramIdVenta", idVenta);
			
			super.execute(inParams);
		}
	}
	
	/**
	 * @author Sergio Plata
	 */
	private class CancelacionMasiva extends StoredProcedure{
		
		public CancelacionMasiva(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.CANCELACION_MASIVA);
			super.declareParameter(new SqlParameter("paramCanal", OracleTypes.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", OracleTypes.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", OracleTypes.INTEGER));
			super.declareParameter(new SqlParameter("paramCausa", OracleTypes.INTEGER));
			super.declareParameter(new SqlParameter("paramOpc",OracleTypes.INTEGER));
			super.compile();
		}
		
		public void execute(Short canal, Short ramo, Long poliza, Integer causa, Integer opc){
			
			Map<String, Object> inParams = new HashMap<String, Object>();
			
			inParams.put("paramCanal", canal);
			inParams.put("paramRamo", ramo);
			inParams.put("paramPoliza", poliza);
			inParams.put("paramCausa", causa);
			inParams.put("paramOpc", opc);
			
			super.execute(inParams);
		}
	}
	
	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	private class Renovacion extends StoredProcedure{
		
		public Renovacion(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.RENOVACION);
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramIdVenta", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", OracleTypes.VARCHAR));
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		public Map execute(Short canal, Short ramo, Long poliza, Integer idVenta) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("paramCanal", canal);
			inParams.put("paramRamo", ramo);
			inParams.put("paramPoliza", poliza);
			inParams.put("paramIdVenta", idVenta);
			return super.execute(inParams);
		}
	}
	
	/**
	 * @author Sergio Plata
	 */
	private class Rehabilitacion extends StoredProcedure{
		
		public Rehabilitacion(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.REHABILITACION);
			super.declareParameter(new SqlParameter("paramCanal", OracleTypes.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", OracleTypes.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", OracleTypes.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", OracleTypes.VARCHAR));
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		public Map execute(Short canal, Short ramo, Long poliza){
			
			Map<String, Object> inParams = new HashMap<String, Object>();
			
			inParams.put("paramCanal", canal);
			inParams.put("paramRamo", ramo);
			inParams.put("paramPoliza", poliza);
			
			return super.execute(inParams);
		}
	}
	
	/**
	 * 
	 * @author Fernando Gomez
	 *
	 */
	private class CancelacionCRI extends StoredProcedure{

		public CancelacionCRI(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.CANCELACION_CRI);
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramIdVenta", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramCausa", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", OracleTypes.VARCHAR));
			super.compile();
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.StoredProcedure#execute(java.util.Map)
		 */
		@SuppressWarnings("unchecked")
		public Map execute(Short canal, Short ramo, Long poliza, Integer idVenta, Integer causaAnula) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new HashMap<String, Object>();
			
			inParams.put("paramCanal", canal);
			inParams.put("paramRamo", ramo);
			inParams.put("paramPoliza", poliza);
			inParams.put("paramIdVenta", idVenta);
			inParams.put("paramCausa", causaAnula);
			
			return super.execute(inParams);
		}
	}	
	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	private class PreFacturacion extends StoredProcedure{
		
		public PreFacturacion(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.PRE_FACTURACION);
			super.declareParameter(new SqlParameter("p_canal", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_ramo", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_poliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_numberAjusRec", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_operacion", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_sub_operacion", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("p_resp", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("p_mensaje", OracleTypes.VARCHAR));
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		public Map execute(CertificadoId id, Integer nNumRecibo) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("p_canal", id.getCoceCasuCdSucursal());
			inParams.put("p_ramo", id.getCoceCarpCdRamo());
			inParams.put("p_poliza", id.getCoceCapoNuPoliza());
			inParams.put("p_numberAjusRec", nNumRecibo);
			inParams.put("p_operacion", 6001);
			inParams.put("p_sub_operacion", 0);
			
			return super.execute(inParams);
		}
	}
	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	private class Facturacion extends StoredProcedure{
		
		public Facturacion(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.FACTURACION);
			super.declareParameter(new SqlParameter("w_cd_sucursal", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_ramo", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_nu_poliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_prefact", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_operacion", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_sub_operacion", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("p_mensaje", OracleTypes.VARCHAR));
			super.compile();
		}
		@SuppressWarnings("unchecked")
		public Map execute(FacturacionId id) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("w_cd_sucursal", id.getCofaCasuCdSucursal());
			inParams.put("w_cd_ramo", id.getCofaCarpCdRamo());
			inParams.put("w_nu_poliza", id.getCofaCapoNuPoliza());
			inParams.put("w_cd_prefact", 0);
			inParams.put("w_operacion", 6001);
			inParams.put("w_sub_operacion", 0);
			
			return super.execute(inParams);
		}
	}
	
    @SuppressWarnings("unused")
	private class Facturacion5758 extends StoredProcedure{
		
		public Facturacion5758(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.FACTURACIONB);
			super.declareParameter(new SqlOutParameter("returnname", Types.FLOAT));
			super.declareParameter(new SqlParameter("pramo", Types.FLOAT));
			super.compile();
		}

		@SuppressWarnings({ "unchecked" })
		public Map execute(Short ramo) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("pramo", ramo);
			
			return super.execute(inParams);
		}
	}
	/**
	 * 
	 */
	public DataSource getDataSource() {
		// TODO Auto-generated method stub
		return SessionFactoryUtils.getDataSource(this.getSessionFactory());
	}
	
	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	public static class GeneraMovimiento extends StoredProcedure{
		
		public GeneraMovimiento(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.GENERA_MOVIMIENTO);
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramCertificado", Types.INTEGER));
			super.compile();
		}
		
		public void execute(Short canal, Short ramo, Long poliza, Long certificado) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new LinkedHashMap<String, Object>();
			
			inParams.put("paramCanal", canal);
			inParams.put("paramRamo", ramo);
			inParams.put("paramPoliza", poliza);
			inParams.put("paramCertificado", certificado);
			
			super.execute(inParams);
		}
	}

	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	public static class NoMovimiento extends StoredProcedure{

		public NoMovimiento(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.NO_MOVIMIENTO);
			super.declareParameter(new SqlOutParameter("returnname", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramCertificado", Types.INTEGER));
			
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		public Map execute(Short canal, Short ramo, Long poliza, Long certificado) throws DataAccessException {
			// TODO Auto-generated method stub
			
			Map<String, Object> inParams = new LinkedHashMap<String, Object>();
			
			inParams.put("paramCanal", canal);
			inParams.put("paramRamo", ramo);
			inParams.put("paramPoliza", poliza);
			inParams.put("paramCertificado", certificado);
			
			
			return super.execute(inParams);
		}
	}
	
	/**
	 * Clase para mandar llamar la funcion colectivos.generaCliente
	 * @author Ing. IBB
	 *
	 */
	public static class GeneraCliente extends StoredProcedure{

		/**
		 * Constructor de clase GeneraCliente
		 * @param ds DataSource para conexion a base de datos
		 */
		public GeneraCliente(DataSource ds) {
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.GENERA_CLIENTE);
			super.declareParameter(new SqlOutParameter("returnname", Types.INTEGER));

			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		/**
		 * Metodo que manda ejecutar la funcion generaCliente
		 * @return
		 * @throws DataAccessException
		 */
		public Map execute() throws DataAccessException {
			Map<String, Object> inParams = new LinkedHashMap<String, Object>();
			return super.execute(inParams);
		}
	}
	
	/**
	 * Clase para mandar llamar la funcion colectivos.noEndosoDetalle
	 * @author Ing. IBB
	 *
	 */
	public static class GeneraNoEndoso extends StoredProcedure{

		/**
		 * Constructor de clase GeneraNoEndoso
		 * @param ds DataSource para conexion a base de datos
		 */
		public GeneraNoEndoso(DataSource ds) {
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.NO_ENDOSO_DETALLE);
			super.declareParameter(new SqlOutParameter("returnname", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramTpEndoso", Types.VARCHAR));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));

			super.compile();
		}
		
		/**
		 * Metodo que manda ejecutar la funcion noEndosoDetalle
		 * @param coedCarpCdRamo ramo 
		 * @param coedStEndoso estatus
		 * @return numero de endoso siguiente
		 * @throws DataAccessException
		 */
		public Map execute(short coedCarpCdRamo, String coedStEndoso) throws DataAccessException {
			Map<String, Object> inParams = new LinkedHashMap<String, Object>();
			
			inParams.put("paramTpEndoso", coedStEndoso);
			inParams.put("paramRamo", coedCarpCdRamo);
			
			return super.execute(inParams);
		}
	}
	
	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	public static class CoberturaCertificado extends StoredProcedure{
		
		public CoberturaCertificado(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.COBERTURA_CERTIFICADO);
			super.declareParameter(new SqlOutParameter("returnname", OracleTypes.STRUCT));
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramProducto", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPlan", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramSumaAsegurada", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramFactor", Types.INTEGER));
			super.compile();
		}
		@SuppressWarnings("unchecked")
		@Override
		public Map execute(Map inParams) throws DataAccessException {
			// TODO Auto-generated method stub
			return super.execute(inParams);
		}
	}
	/**
	 * 
	 * @author Sergio Plata
	 *
	 */
	public static class ComponenteCertificado extends StoredProcedure{

		public ComponenteCertificado(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.COMPONENTE_CERTIFICADO);
			super.declareParameter(new SqlOutParameter("returnname", OracleTypes.ARRAY));
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramProducto", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPlan", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPrimaPura", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramSumaAsegurada", Types.INTEGER));
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public Map execute(Map inParams) throws DataAccessException {
			// TODO Auto-generated method stub
			return super.execute(inParams);
		}
	}

	
	/**
	 * 
	 * @author Omar Cort�s
	 *
	 */
	private class EndososPUnica extends StoredProcedure{
		
		public EndososPUnica(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.ENDOSOS_PUNICA);
			super.declareParameter(new SqlOutParameter("vRetorna", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_canal", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_ramo", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_poliza", Types.INTEGER));
			super.compile();
		}
		@SuppressWarnings("unchecked")
		public Map execute(FacturacionId id) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("p_canal", id.getCofaCasuCdSucursal());
			inParams.put("p_ramo", id.getCofaCarpCdRamo());
			inParams.put("p_poliza", id.getCofaCapoNuPoliza());

			
			return super.execute(inParams);
		}
	}
	
	private class Devolucion extends StoredProcedure{

		public Devolucion(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.DEVOLUCION);
			super.declareParameter(new SqlOutParameter("returnname", Types.DOUBLE));
			super.declareParameter(new SqlParameter("paramPlazo", Types.DOUBLE));
			super.declareParameter(new SqlParameter("paramTasa", Types.DOUBLE));
			super.declareParameter(new SqlParameter("paramFechaInicio", Types.DATE));
			super.declareParameter(new SqlParameter("paramFechaFin", Types.DATE));
			super.declareParameter(new SqlParameter("paramFechaCancelacion", Types.DATE));
			super.declareParameter(new SqlParameter("paramPrimaUnica", Types.DOUBLE));
			
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		public Map execute(Certificado cer, Double tasaAmortiza, Date fechaAnulacion) throws DataAccessException {
			// TODO Auto-generated method stub
			
			Map<String, Object> inParams = new LinkedHashMap<String, Object>();
			
			inParams.put("paramPlazo", cer.getCoceCdPlazo());
			inParams.put("paramTasa", tasaAmortiza);
			inParams.put("paramFechaInicio", cer.getCoceFeIniCredito());
			inParams.put("paramFechaFin", cer.getCoceFeFinCredito());
			inParams.put("paramFechaCancelacion", fechaAnulacion);
			inParams.put("paramPrimaUnica", cer.getCoceMtPrimaSubsecuente());
			
			return super.execute(inParams);
		}
	}
	
	/**
	 * 
	 * @author Omar Cort�s
	 *
	 */
	private static class RenovacionPolizas extends StoredProcedure{
		
		public RenovacionPolizas(DataSource ds) {
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.RENOVACIONA);
			super.declareParameter(new SqlOutParameter("vRetorna", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_canal", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_ramo", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_poliza", Types.INTEGER));
			super.compile();
		}
		@SuppressWarnings("unchecked")
		public Map execute(Short canal, Short ramo, Long poliza) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("p_canal", canal);
			inParams.put("p_ramo", ramo);
			inParams.put("p_poliza", poliza);

			
			return super.execute(inParams);
		}
	}
	
	/**
	 * 
	 * @author Fernando Gomez
	 *
	 */
	private static class EndosarPoliza extends StoredProcedure{
		
		public EndosarPoliza(DataSource ds) {
			
			super.setDataSource(ds);
			super.setSql(Colectivos.ENDOSAR_POLIZA);
			super.declareParameter(new SqlParameter("paramCanal", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza", Types.NUMERIC));
			super.declareParameter(new SqlParameter("paramIdVenta", Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRecibo", Types.NUMERIC));
			super.declareParameter(new SqlParameter("paramFeDesde", Types.DATE));
			super.declareParameter(new SqlParameter("paramFeHasta", Types.DATE));
			super.declareParameter(new SqlParameter("paramOperacion", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", Types.VARCHAR));
			super.compile();
		}
		@SuppressWarnings("unchecked")
		public Map execute(Short inCanal, Short inRamo, Long inPoliza,
				Integer inIdVenta, BigDecimal recibo, Date feDesde, Date feHasta,
				Integer inOperEndoso) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("paramCanal", inCanal);
			inParams.put("paramRamo", inRamo);
			inParams.put("paramPoliza", inPoliza);
			inParams.put("paramIdVenta", inIdVenta);
			inParams.put("paramRecibo", recibo);
			inParams.put("paramFeDesde", feDesde);
			inParams.put("paramFeHasta", feHasta);
			inParams.put("paramOperacion", inOperEndoso);
			
			return super.execute(inParams);
		}
	}
	
	public void validaCumulosBT() throws Excepciones {
		ProcesosPL proceso;
		
		try {
			proceso= new ProcesosPL (getSession().connection());
			
			proceso.validaCumulosBT(); 
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}	
	}
	
	public void actualizaDatosComplementarios(List<Object> lstDatos) throws Excepciones {
		StringBuffer sbquery;
		Query query; 
		Iterator<Object> it;
		String[] arrDatos;
		
		try {
			it = lstDatos.iterator();
			while(it.hasNext()) {
				arrDatos = (String[]) it.next();
				
				sbquery = new StringBuffer();
				sbquery.append("UPDATE COLECTIVOS_COMPLEMENTARIOS CC \n");
				sbquery.append("   SET CC.COCO_HOG_COLONIA    = :segmento, \n");                                                                            
				sbquery.append("       CC.COCO_HOG_DELEGMUNIC = :segto_banco, \n");
				sbquery.append("       CC.COCO_HOG_DS_POBLAC  = :mod_atn, \n");
				sbquery.append("       CC.COCO_HOG_CALLE_NUM  = :ejecutivo, \n"); 								//nm_ejecutivo+ejecutivo
				sbquery.append("       CC.COCO_HOG_VCAMPO2    = :cat_eje, \n");
				sbquery.append("       CC.COCO_HOG_VCAMPO3    = :sucursal, \n");
				sbquery.append("       CC.COCO_VIDT_VCAMPO2   = :suc_virtual, \n");
				sbquery.append("       CC.COCO_VIDT_VCAMPO3   = :region, \n"); 									//cve_rg+region
				sbquery.append("       CC.COCO_VIDT_VCAMPO1   = :zona, \n"); 									//cve_zn+zona
				sbquery.append("       CC.COCO_VIDT_DCAMPO1   = TO_DATE(:fecha, 'DD/MM/YYYY HH24:MI:SS') \n "); //FECHAACTUALIZACION 
				sbquery.append("WHERE CC.COCO_CACE_CASU_CD_SUCURSAL = :canal \n");
				sbquery.append("	AND CC.COCO_CACE_CARP_CD_RAMO = :ramo \n");
				sbquery.append("    AND CC.COCO_CACE_CAPO_NU_POLIZA = :poliza \n");
				sbquery.append("    AND CC.COCO_CACE_NU_CERTIFICADO = :certificado \n");
				
				query = getSession().createSQLQuery(sbquery.toString()); 
				query.setString("segmento", arrDatos[1] == null ? "S/D" : arrDatos[1].toString());
				query.setString("segto_banco", arrDatos[2] == null ? "S/D" : arrDatos[2].toString());
				query.setString("mod_atn", arrDatos[3] == null ? "S/D" : arrDatos[3].toString());
				query.setString("ejecutivo", (arrDatos[4] == null ? "S/D" : arrDatos[4].toString()) + "-" + (arrDatos[5] == null ? "S/D" : arrDatos[5].toString()));
				query.setString("cat_eje", arrDatos[6] == null ? "S/D" : arrDatos[6].toString());
				query.setString("sucursal", arrDatos[7] == null ? "S/D" : arrDatos[7].toString());
				query.setString("suc_virtual", arrDatos[8] == null ? "S/D" : arrDatos[8].toString());
				query.setString("region", (arrDatos[9] == null ? "S/D" : arrDatos[9].toString()) + "-" + (arrDatos[10] == null ? "S/D" : arrDatos[10].toString()));
				query.setString("zona", (arrDatos[11] == null ? "S/D" : arrDatos[11].toString()) + "-" + (arrDatos[12] == null ? "S/D" : arrDatos[12].toString()));
				query.setString("fecha", GestorFechas.formatDate(new Date(), "dd/MM/yyyy HH:mm:ss"));
				query.setInteger("canal", new Integer(arrDatos[13].toString()));
				query.setInteger("ramo", new Integer(arrDatos[14].toString()));
				query.setInteger("poliza", new Integer(arrDatos[15].toString()));
				query.setInteger("certificado", new Integer(arrDatos[16].toString()));
				query.executeUpdate();
			}
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar datos complementarios. ", e);
		} 
	}
	
	
	//dfg
	@SuppressWarnings("unused")
	public String numeradorCliente() throws Excepciones{
    	String reporte = "";
    	StringBuilder query = null;
    	List<Object> lista = null;
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<Object>();
    		
    		query.append(" SELECT MAX(CANC_NU_CLIENTE)+1   \n");
    		query.append(" FROM CART_NUMERADOR_CLIENTES    \n");
    		lista = getSession().createSQLQuery(query.toString()).list();

    		reporte = lista.get(0).toString();
   
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error al recuperar reporte impresion de poliza", e);
		}
    	
    	return reporte;

    }
	
	public void actNumeradorCliente() throws Excepciones {
		StringBuffer act = new StringBuffer(300);
		try {
			act.append("UPDATE CART_NUMERADOR_CLIENTES \n");
			act.append(" SET CANC_NU_CLIENTE = CANC_NU_CLIENTE + 1  \n");                                                                            
			Query queryAct = getSession().createSQLQuery(act.toString()); 
			queryAct.executeUpdate();
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}

	public void insertaContratante(String res, String inRazonSocial, Date inFechaCons,
            String inCalleNum ,String inCp, String inRfc,String inPoblacion,String inColonia) throws Excepciones {
		// TODO Auto-generated method stub
		
		StringBuffer sbquery;
		Query query; 
		try {
				sbquery = new StringBuffer();
				sbquery.append(" INSERT INTO CART_CLIENTES (                \n");
				sbquery.append("  CACN_CD_NACIONALIDAD        ,             \n");
				sbquery.append("  CACN_NU_CEDULA_RIF          ,             \n");
				sbquery.append("  CACN_TP_CLIENTE             ,             \n");
				sbquery.append("  CACN_IN_EMPLEADO            ,             \n");
				sbquery.append("  CACN_IN_TRATAMIENTO_ESPECIAL,             \n");
				sbquery.append("  CACN_NM_APELLIDO_RAZON      ,             \n");
				sbquery.append("  CACN_CD_VIDA                ,             \n");
				sbquery.append("  CACN_IN_USADO_FINANCIAMIENTO,             \n");
				sbquery.append("  CACN_CAAE_CD_ACTIVIDAD      ,             \n");
				sbquery.append("  CACN_CAES_CD_ESTADO_HAB     ,             \n");
				sbquery.append("  CACN_CACI_CD_CIUDAD_HAB     ,             \n");
				sbquery.append("  CACN_CAPA_CD_PAIS           ,             \n");
				sbquery.append("  CACN_CAES_CD_ESTADO_COB     ,             \n");
				sbquery.append("  CACN_CACI_CD_CIUDAD_COB     ,             \n");
				sbquery.append("  CACN_NM_PERSONA_NATURAL     ,             \n");
				sbquery.append("  CACN_FE_NACIMIENTO          ,             \n");
				sbquery.append("  CACN_CD_EDO_CIVIL           ,             \n");
				sbquery.append("  CACN_CD_SEXO                ,             \n");
				sbquery.append("  CACN_DI_COBRO1              ,             \n");
				sbquery.append("  CACN_DI_COBRO2              ,             \n");
				sbquery.append("  CACN_NU_TELEFONO_COBRO      ,             \n");
				sbquery.append("  CACN_NU_FAX_COBRO           ,             \n");
				sbquery.append("  CACN_ZN_POSTAL_COBRO        ,             \n");
				sbquery.append("  CACN_NU_APARTADO_COBRO      ,             \n");
				sbquery.append("  CACN_DI_HABITACION1         ,             \n");
				sbquery.append("  CACN_DI_HABITACION2         ,             \n");
				sbquery.append("  CACN_NU_TELEFONO_HABITACION ,             \n");
				sbquery.append("  CACN_ZN_POSTAL_HABITACION   ,             \n");
				sbquery.append("  CACN_NU_APARTADO_HABITACION ,             \n");
				sbquery.append("  CACN_NU_EMPLEADO_FICHA      ,             \n");
				sbquery.append("  CACN_CD_DEPENDENCIA1        ,             \n");
				sbquery.append("  CACN_CD_DEPENDENCIA2        ,             \n");
				sbquery.append("  CACN_CD_DEPENDENCIA3        ,             \n");
				sbquery.append("  CACN_CD_DEPENDENCIA4        ,             \n");
				sbquery.append("  CACN_CD_NACIONALIDAD_A      ,             \n");
				sbquery.append("  CACN_NU_CEDULA_ANTERIOR     ,             \n");
				sbquery.append("  CACN_DATOS_REG_1            ,             \n");
				sbquery.append("  CACN_DATOS_REG_2            ,             \n");
				sbquery.append("  CACN_REGISTRO_1             ,             \n");
				sbquery.append("  CACN_REGISTRO_2             ,             \n");
				sbquery.append("  CACN_CAZP_COLONIA_COB       ,             \n");
				sbquery.append("  CACN_CAZP_POBLAC_COB        ,             \n");
				sbquery.append("  CACN_RFC                    ,             \n");
				sbquery.append("  CACN_NU_SERVICIOS           ,             \n");
				sbquery.append("  CACN_NU_CONSULTAS           ,             \n");
				sbquery.append("  CACN_NU_LLAMADAS            ,             \n");
				sbquery.append("  CACN_NOMBRE                 ,             \n");
				sbquery.append("  CACN_APELLIDO_PAT           ,             \n");
				sbquery.append("  CACN_APELLIDO_MAT           ,             \n");
				sbquery.append("  CACN_ST_ACTUA_NOM           ,             \n");
				sbquery.append("  CACN_ST_CLIENTE             ,             \n");
				sbquery.append("  CACN_NACIONALIDAD_FATCA     ,             \n");
				sbquery.append("  CACN_PAIS_FATCA             ,             \n");
				sbquery.append("  CACN_CD_TAXID                )            \n");
				sbquery.append("  VALUES(                                   \n");
				sbquery.append("  'J'        ,                              \n");
				sbquery.append("  :cedula    ,                              \n");
				sbquery.append("  'N'        ,                              \n");
				sbquery.append("  'N'        ,                              \n");
				sbquery.append("  'N'        ,                              \n");
				sbquery.append(" :nombre     ,                              \n");
				sbquery.append("  'V'        ,                              \n");
				sbquery.append("  'N'        ,                              \n");
				sbquery.append("  47         ,                              \n");
				sbquery.append("  :estado    ,                              \n");
				sbquery.append("  :ciudad    ,                              \n");
				sbquery.append("  NULL ,                                    \n");
				sbquery.append("  NULL ,                                    \n");
				sbquery.append("  NULL ,                                    \n");
				sbquery.append("  NULL ,                                    \n");
				sbquery.append("  :fecha_cons,                              \n");
				sbquery.append("  'S',                                      \n");
				sbquery.append("  'M',                                      \n");
				sbquery.append("  :direccion,                               \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  :cp,                                      \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  :rfc,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  :nombre,                                  \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL,                                     \n");
				sbquery.append("  NULL)                                     \n");

				query = getSession().createSQLQuery(sbquery.toString()); 
				
				query.setString("cedula", res== null ? "S/D" : res.toString());
				query.setString("nombre", inRazonSocial == null ? "S/D" : inRazonSocial.toString());
				query.setString("fecha_cons", GestorFechas.formatDate(inFechaCons, "dd/MM/yyyy"));
				query.setString("direccion", inCalleNum == null ? "S/D" : inCalleNum.toString());
				query.setString("cp", inCp);		
				query.setString("rfc", inRfc.toString());	
				query.setString("estado", inPoblacion.toString());	
				query.setString("ciudad", inColonia.toString());	
				query.executeUpdate();

		} catch (Exception e) {
		
			throw new Excepciones("Error: Al insertar Contratante ", e);
		}
	}

	/* Magos Magias 22/05/2015 */
	private class Fechas extends StoredProcedure{
		
		public Fechas(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setFunction(true);
			super.setSql(Colectivos.VALIDA_FECHA);
			
			super.declareParameter(new SqlOutParameter("existe"					,Types.INTEGER));
			
			super.declareParameter(new SqlParameter("paramCanal"				,Types.INTEGER));
			super.declareParameter(new SqlParameter("paramRamo"					,Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPoliza"				,Types.INTEGER));
			
			super.declareParameter(new SqlParameter("paramProducto"				,Types.INTEGER));
			super.declareParameter(new SqlParameter("paramPlan"					,Types.INTEGER));
			
			super.declareParameter(new SqlParameter("paramFechaNacimiento"		,Types.DATE));
			super.declareParameter(new SqlParameter("paramFechaInicioCredito"	,Types.DATE));
			super.declareParameter(new SqlParameter("paramFechaFinCredito"		,Types.DATE));
	
			super.declareParameter(new SqlParameter("paramPlazo"				,Types.INTEGER));
			super.declareParameter(new SqlParameter("paramCodEdadMin"			,Types.INTEGER));
			super.declareParameter(new SqlParameter("paramCodEdadMax"			,Types.INTEGER));
			super.declareParameter(new SqlParameter("paramProceso"				,Types.INTEGER));
			
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		public Map execute(ClienteCertif cliente, Long poliza, Integer edadMin, Integer edadMax) throws DataAccessException {
			
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("paramCanal"				, cliente.getId().getCoccCasuCdSucursal()); //canal
			inParams.put("paramRamo"				, cliente.getId().getCoccCarpCdRamo());//ramo
			inParams.put("paramPoliza"				, poliza);
			
			System.out.println("------------------>" + cliente.getCertificado().getPlanes().getId().getAlplCdProducto());
			
			inParams.put("paramProducto"          	, cliente.getCertificado().getPlanes().getId().getAlplCdProducto());
			inParams.put("paramPlan"              	, cliente.getCertificado().getPlanes().getId().getAlplCdPlan());
			inParams.put("paramFechaNacimiento"   	, cliente.getCliente().getCocnFeNacimiento());

			inParams.put("paramFechaInicioCredito"	, cliente.getCertificado().getCoceFeIniCredito());
			inParams.put("paramFechaFinCredito"   	, cliente.getCertificado().getCoceFeFinCredito());
			inParams.put("paramPlazo"             	, cliente.getCertificado().getCoceCdPlazo());

			inParams.put("paramCodEdadMin"        	,edadMin);
			inParams.put("paramCodEdadMax"        	,edadMax);
			inParams.put("paramProceso"           	,1);
			
			return super.execute(inParams);
		}

	}
	
	private class Busca extends StoredProcedure{
		CallableStatement cs;
		Connection conn;
		
		public Busca(DataSource ds) throws SQLException {
			conn = ds.getConnection();
			cs = conn.prepareCall("DECLARE A BOOLEAN; B VARCHAR(2); BEGIN A := " + Colectivos.NEGATIVE_FILE + "(?, ?, ?, ?); IF A THEN B := '1'; ELSE B := '0'; END IF; ? := B; END;");
		}
		
		public Integer execute(String  wc_paterno_nf, String wc_materno_nf , String  wc_nombre_nf, Date  wc_fecha_nac_cons) throws DataAccessException {
			Integer nResultado = 0;
			
			try {
				cs.setString(1, wc_nombre_nf);
				cs.setString(2, wc_materno_nf);
				cs.setString(3, wc_paterno_nf);
				cs.setDate(4, new java.sql.Date(wc_fecha_nac_cons.getTime()));
				cs.registerOutParameter(5, Types.VARCHAR);
				cs.execute();
				nResultado = Integer.valueOf(cs.getString(5));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if(cs != null) {
						cs.close();
					}
					if(conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			return nResultado;
		}
	}
	
	
	
	private class IniciaAlta extends StoredProcedure {
		public IniciaAlta(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.BUSCA_CLIENTE);
			super.declareParameter(new SqlParameter("P_naci_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_cedu_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_nomb_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_cd_vida", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_rfc_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_tipe_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_acti_cte", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_fena_cte", Types.DATE));
			super.declareParameter(new SqlParameter("P_edci_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_sexo_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_call_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_colo_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_pobl_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_ciud_cte", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_edo_cte", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_codp_cte", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_lada_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_telc_cte", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_cd_banco", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_nu_cuenta", Types.VARCHAR));
			super.declareParameter(new SqlParameter("P_tp_cuenta", Types.VARCHAR));
			super.declareParameter(new SqlInOutParameter("W_nac", Types.VARCHAR));
			super.declareParameter(new SqlInOutParameter("W_cedula", Types.VARCHAR));
			super.declareParameter(new SqlInOutParameter("W_nu_dom", Types.INTEGER));
			super.declareParameter(new SqlInOutParameter("W_mensaje", Types.VARCHAR));
			super.compile();
		}
		
		public Map<String, Object> execute(ContratanteDTO contratanteDTO) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<String, Object>();	
			try {
				inParams.put("P_naci_cte",contratanteDTO.getCacnCdNacionalidad()); 
				inParams.put("P_cedu_cte",contratanteDTO.getCacnNuCedulaRif()); 
				inParams.put("P_nomb_cte",contratanteDTO.getCacnApellidoPat()+"/"+contratanteDTO.getCacnApellidoMat()+"/"+contratanteDTO.getCacnNombre()); //NOMBRE
				inParams.put("P_cd_vida","V"); 
				inParams.put("P_rfc_cte",contratanteDTO.getCacnRfc().toString() );  
				inParams.put("P_tipe_cte","N"); //'N'
				inParams.put("P_acti_cte",contratanteDTO.getCacnCaaeCdActividad()); //
				inParams.put("P_fena_cte",contratanteDTO.getCacnFeNacimiento()); 
				inParams.put("P_edci_cte",contratanteDTO.getCacnCdEdoCivil()); 
				inParams.put("P_sexo_cte",contratanteDTO.getCacnCdSexo()); 
				inParams.put("P_call_cte",contratanteDTO.getCacnDiCobro1()); 
				inParams.put("P_colo_cte",contratanteDTO.getCacnCazpColoniaCob());
				inParams.put("P_pobl_cte",contratanteDTO.getCacnCazpPoblacCob());//inPoblacion.toString() );
				inParams.put("P_ciud_cte",0); //inCd ); //number
				inParams.put("P_edo_cte", contratanteDTO.getCacnCaesCdEstadoCob()); //inCdEdo);  //number
				inParams.put("P_codp_cte",contratanteDTO.getCacnZnPostalCobro()); //inCP );   //number
				inParams.put("P_lada_cte",Constantes.DEFAULT_STRING_ID); //inLada); 
				inParams.put("P_telc_cte",contratanteDTO.getCacnNuTelefonoCobro()); //inTel ); 
				inParams.put("P_cd_banco","4"); //4 
				inParams.put("P_nu_cuenta",contratanteDTO.getStrCuenta());
				inParams.put("P_tp_cuenta",contratanteDTO.getStrTpCuenta());
				inParams.put("W_nac","0");//nacionalidad2 );     
				inParams.put("W_cedula","J");//cedula2 );  
				inParams.put("W_nu_dom",1);//,domicilio2 );  
				inParams.put("W_mensaje","M");//mensaje2 );
			} catch (Exception e) {
				e.printStackTrace();
			}

			return super.execute(inParams);
		}
	}
	
	private class Contabilidad1 extends StoredProcedure{
		
		public Contabilidad1(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.CONTCOLECTIVOS1);
			super.declareParameter(new SqlParameter("w_cd_sucursal", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_ramo",Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_poliza",Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_certificado", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_ult_recibo", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_mt_ult_recibo", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_operacion", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_sub_operacion", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_fe_desde", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_fe_hasta", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_fe_contable", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_nombre_facturador", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_estatus_recibo", Types.INTEGER));
			super.compile();
		}

		@SuppressWarnings("unchecked")
		public Map execute(short canal, Short ramo, Long poliza, int certif, int ultRecibo, int mtUltRecibo, int operacion, 
				int subOperacion, String fDesde, String fHasta, String fContable, String nombreFac, int estatusRecibo) throws DataAccessException {
			
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("w_cd_sucursal", canal);
			inParams.put("w_cd_ramo",ramo);
			inParams.put("w_cd_poliza",poliza);
			inParams.put("w_cd_certificado", certif);
			inParams.put("w_ult_recibo", ultRecibo);
			inParams.put("w_mt_ult_recibo", mtUltRecibo);
			inParams.put("w_operacion", operacion);
			inParams.put("w_sub_operacion", subOperacion);
			inParams.put("w_fe_desde", fDesde);
			inParams.put("w_fe_hasta", fHasta);
			inParams.put("w_fe_contable", fContable);
			inParams.put("w_nombre_facturador", nombreFac);
			inParams.put("w_estatus_recibo", estatusRecibo);
			
			return super.execute(inParams);
		}
	}

	
	private class Contabilidad2 extends StoredProcedure{
		
		public Contabilidad2(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.CONTCOLECTIVOS2);
			super.declareParameter(new SqlParameter("w_cd_sucursal", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_ramo",Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_poliza",Types.INTEGER));
			super.declareParameter(new SqlParameter("w_cd_certificado", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_ult_recibo", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_mt_ult_recibo", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_operacion", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_sub_operacion", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_fe_desde", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_fe_hasta", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_fe_contable", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_nombre_facturador", Types.VARCHAR));
			super.declareParameter(new SqlParameter("w_estatus_recibo", Types.INTEGER));
			super.compile();

		}
		
		@SuppressWarnings("unchecked")
		public Map execute(short canal, Short ramo, Long poliza, int certif, int ultRecibo, int mtUltRecibo, int operacion, 
				int subOperacion, String fDesde, String fHasta, String fContable, String nombreFac, int estatusRecibo) throws DataAccessException {
			
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("w_cd_sucursal", canal);
			inParams.put("w_cd_ramo",ramo);
			inParams.put("w_cd_poliza",poliza);
			inParams.put("w_cd_certificado", certif);
			inParams.put("w_ult_recibo", ultRecibo);
			inParams.put("w_mt_ult_recibo", mtUltRecibo);
			inParams.put("w_operacion", operacion);
			inParams.put("w_sub_operacion", subOperacion);
			inParams.put("w_fe_desde", fDesde);
			inParams.put("w_fe_hasta", fHasta);
			inParams.put("w_fe_contable", fContable);
			inParams.put("w_nombre_facturador", nombreFac);
			inParams.put("w_estatus_recibo", estatusRecibo);
			
			return super.execute(inParams);
		}
	}
	
	private class CargaCuentas extends StoredProcedure{
		
		public CargaCuentas(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.CARGACUENTA);
			super.declareParameter(new SqlParameter("P_RAMO", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_OPERACION", Types.INTEGER));
			super.declareParameter(new SqlParameter("P_MONTO", Types.DOUBLE));
			super.declareParameter(new SqlParameter("P_CUENTA", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("P_FE_DEPOSITO", OracleTypes.VARCHAR));
			super.declareParameter(new SqlOutParameter("P_FE_CONTABLE", OracleTypes.VARCHAR));
			super.compile();
			
		}
		
		@SuppressWarnings("unchecked")
		public Map execute(Long nRamo, Integer nOperCargo, Double montoAC, Long cuenta, String newFechaFin, String fecha_hoy) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("P_RAMO", nRamo);
			inParams.put("P_OPERACION", nOperCargo);
			inParams.put("P_MONTO", montoAC);
			inParams.put("P_CUENTA", cuenta);
			inParams.put("P_FE_DEPOSITO", newFechaFin);
			inParams.put("P_FE_CONTABLE", fecha_hoy);
			return super.execute(inParams);
		}
	}

	/**
	 * 
	 * @author Ing. IBB
	 *
	 */
	private static class MigraColectivos extends StoredProcedure{
		
		public MigraColectivos(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.MIGRA_COLECTIVOS);
			super.declareParameter(new SqlParameter("w_canal", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_ramop", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_poliza", Types.NUMERIC));
			super.declareParameter(new SqlParameter("w_certificado", Types.NUMERIC));
			super.declareParameter(new SqlParameter("w_tpclien", Types.INTEGER));
			super.declareParameter(new SqlParameter("w_nuclie", Types.NUMERIC));
			super.declareParameter(new SqlParameter("w_feocurre", Types.DATE));
			super.declareParameter(new SqlParameter("w_bandera", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", Types.VARCHAR));
			super.compile();
		}
		
		@SuppressWarnings("unchecked")
		/**
		 * Metodo que manda a ejecutar el paquete de Migra Colectivo
		 * @param inCanal canal
		 * @param inRamo ramo
		 * @param inPoliza poliza
		 * @param inCertificado certificado
		 * @param inTpCliente tipo cliente
		 * @param inNumCliente numero de cliente
		 * @param feOcurrencia fecha ocurrencia
		 * @param inBandera bandera
		 * @return regresa respuesta del paquete
		 * @throws DataAccessException
		 */
		public Map execute(Short inCanal, Short inRamo, Long inPoliza, Long inCertificado, Short inTpCliente, Long inNumCliente, Date feOcurrencia, Integer inBandera) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<String, Object>();
			inParams.put("w_canal", inCanal);
			inParams.put("w_ramop", inRamo);
			inParams.put("w_poliza", inPoliza);
			inParams.put("w_certificado", inCertificado);
			inParams.put("w_tpclien", inTpCliente);
			inParams.put("w_nuclie", inNumCliente);
			inParams.put("w_feocurre", feOcurrencia);
			inParams.put("w_bandera", inBandera);
			
			return super.execute(inParams);
		}
	}
	
	private class ProcesaReserva extends StoredProcedure{

		public ProcesaReserva(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.PROCESA_RESERVA);
			super.declareParameter(new SqlParameter("vv_anio", Types.INTEGER));
			super.declareParameter(new SqlParameter("vv_mes", Types.INTEGER));
			
			super.compile();
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.StoredProcedure#execute(java.util.Map)
		 */
		public void execute(int anio, int mes) throws DataAccessException {
			// TODO Auto-generated method stub
			Map<String, Object> inParams = new HashMap<String, Object>();
			
			inParams.put("vv_anio", anio);
			inParams.put("vv_mes", mes);
			
			
			super.execute(inParams);
		}
	}
	
	private class ConsultaReserva extends StoredProcedure{

		public ConsultaReserva(DataSource ds) {
			// TODO Auto-generated constructor stub
			
			super.setDataSource(ds);
			super.setSql(Colectivos.CONSULTA_RESERVA);
			super.declareParameter(new SqlParameter("PrRamo", Types.INTEGER));
			super.declareParameter(new SqlParameter("PrMes", Types.INTEGER));
			super.declareParameter(new SqlParameter("PrAnio", Types.INTEGER));
			super.declareParameter(new SqlOutParameter("tablist13", OracleTypes.CURSOR));

			super.compile();
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.StoredProcedure#execute(java.util.Map)
		 */
		@SuppressWarnings("unchecked")
		public List<Object> execute(int ramo, int mes, int anio) throws DataAccessException {
			// TODO Auto-generated method stub
			ResultSet rs = null;
			Map<String, Object> inParams = new HashMap<String, Object>();
			
			inParams.put("PrRamo", ramo);
			inParams.put("PrMes", mes);			
			inParams.put("PrAnio", anio);
			
			Map<String, Object> outParams = super.execute(inParams);
			
			return (List<Object>)outParams.get("tablist13");
		   
		}
	}
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public void procesaReserva(int anio, int mes) throws Excepciones {
		try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
									
			ProcesaReserva emision = new ProcesaReserva(ds);
			emision.execute(anio, mes);				
		}
		
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao#consultaReserva(int, int, int)
	 */
	@Override
	public List<Object> consultaReserva(int ramo, int mes, int anio) throws Excepciones{
try {
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
									
			ConsultaReserva emision = new ConsultaReserva(ds);
			return emision.execute(ramo, mes, anio);				
		}
		
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao#calculaPrimas(mx.com.santander.aseguradora.colectivos.model.bussinessObject.CargaId)
	 */
	@Override
	public void calculaPrimas(CargaId id) throws Excepciones {
		try {
			ProcesosPL proceso = new ProcesosPL(getSession().connection());
			proceso.calculaPrima(id); 
		} catch (Exception e) {
			throw new Excepciones("Error: al calcular Primas", e);
		}
	}

	/**
	 * 
	 * @author Ing. IBB
	 *
	 */
	public static class UpdateCertificado extends StoredProcedure{
		
		/**
		 * Constructor de clase
		 * @param ds datasource
		 */
		public UpdateCertificado(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.UPDATE_CERTIFICADOS);
			super.declareParameter(new SqlParameter(Constantes.PARAM_CANAL, Types.INTEGER));
			super.declareParameter(new SqlParameter(Constantes.PARAM_RAMO, Types.INTEGER));
			super.declareParameter(new SqlParameter(Constantes.PARAM_POLIZA, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_CERTIFICADO, Types.NUMERIC));
			super.declareParameter(new SqlParameter("p_fe_desde", Types.DATE));
			super.declareParameter(new SqlParameter("p_fe_hasta", Types.DATE));
			super.declareParameter(new SqlParameter(Constantes.PARAM_SUMA, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_PRIMA, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_RESPALDO, Types.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", Types.VARCHAR));
			super.compile();
		}
		
		/**
		 * Metodo que manda a ejecutar el StoreProcedure UpdateCertificados
		 * @param id datos de certificado
		 * @param feDesde fecha desde
		 * @param feHasta fecha hasta
		 * @param inSuma suma asegurada
		 * @param inPrima prima
		 * @param inRespaldo respaldo
		 * @return respuesta de sp
		 * @throws DataAccessException error de proceso
		 */
		public Map execute(EndososDatosId id, Date feDesde, Date feHasta, Double inSuma, Double inPrima, Integer inRespaldo) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<>();
			inParams.put(Constantes.PARAM_CANAL, id.getCedaCasuCdSucursal());
			inParams.put(Constantes.PARAM_RAMO, id.getCedaCarpCdRamo());
			inParams.put(Constantes.PARAM_POLIZA, id.getCedaCapoNuPoliza());
			inParams.put(Constantes.PARAM_CERTIFICADO, id.getCedaNuCertificado());
			inParams.put("p_fe_desde", feDesde);
			inParams.put("p_fe_hasta", feHasta);
			inParams.put(Constantes.PARAM_SUMA, inSuma);
			inParams.put(Constantes.PARAM_PRIMA, inPrima);
			inParams.put(Constantes.PARAM_RESPALDO, inRespaldo);
			
			return super.execute(inParams);
		}
	}

	/**
	 * 
	 * @author Ing. IBB
	 *
	 */
	public static class UpdateComponentes extends StoredProcedure{
		
		/**
		 * Constructor de clase
		 * @param ds datasource
		 */
		public UpdateComponentes(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.UPDATE_COMPONENTES);
			super.declareParameter(new SqlParameter(Constantes.PARAM_CANAL, Types.INTEGER));
			super.declareParameter(new SqlParameter(Constantes.PARAM_RAMO, Types.INTEGER));
			super.declareParameter(new SqlParameter(Constantes.PARAM_POLIZA, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_CERTIFICADO, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_SUMA, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_PRIMA, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_RESPALDO, Types.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", Types.VARCHAR));
			super.compile();
		}
		
		/**
		 * Metodo que manda a ejecutar el StoreProcedure UpdateComponentes
		 * @param id datos de certificado
		 * @param inSuma suma asegurada
		 * @param inPrima prima
		 * @param inRespaldo respaldo
		 * @return respuesta de sp
		 * @throws DataAccessException error de proceso
		 */
		public Map execute(EndososDatosId id, Double inSuma, Double inPrima, Integer inRespaldo) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<>();
			inParams.put(Constantes.PARAM_CANAL, id.getCedaCasuCdSucursal());
			inParams.put(Constantes.PARAM_RAMO, id.getCedaCarpCdRamo());
			inParams.put(Constantes.PARAM_POLIZA, id.getCedaCapoNuPoliza());
			inParams.put(Constantes.PARAM_CERTIFICADO, id.getCedaNuCertificado());
			inParams.put(Constantes.PARAM_SUMA, inSuma);
			inParams.put(Constantes.PARAM_PRIMA, inPrima);
			inParams.put(Constantes.PARAM_RESPALDO, inRespaldo);
			
			return super.execute(inParams);
		}
	}
	
	/**
	 * 
	 * @author Ing. IBB
	 *
	 */
	public static class UpdateCoberturas extends StoredProcedure{
		
		/**
		 * Constructor de clase
		 * @param ds datasource
		 */
		public UpdateCoberturas(DataSource ds) {
			super.setDataSource(ds);
			super.setSql(Colectivos.UPDATE_COBERTURAS);
			super.declareParameter(new SqlParameter(Constantes.PARAM_CANAL, Types.INTEGER));
			super.declareParameter(new SqlParameter(Constantes.PARAM_RAMO, Types.INTEGER));
			super.declareParameter(new SqlParameter(Constantes.PARAM_POLIZA, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_CERTIFICADO, Types.NUMERIC));
			super.declareParameter(new SqlParameter(Constantes.PARAM_SUMA, Types.NUMERIC));
			super.declareParameter(new SqlParameter("p_producto", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_plan", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_nu_cober", Types.INTEGER));
			super.declareParameter(new SqlParameter("p_tipol", Types.INTEGER));
			super.declareParameter(new SqlParameter(Constantes.PARAM_RESPALDO, Types.INTEGER));
			super.declareParameter(new SqlOutParameter("respuesta", Types.VARCHAR));
			super.compile();
		}
		
		/**
		 * Metodo que manda a ejecutar el StoreProcedure UpdateCoberturas 
		 * @param id datos de certificado
		 * @param inSuma suma asegurada
		 * @param inProducto producto
		 * @param inPlan plan
		 * @param inNuCober cobertura
		 * @param inTipoPol tipo poliza
		 * @param inRespaldo respaldo
		 * @return respuesta de SP
		 * @throws DataAccessException error de proceso
		 */
		public Map execute(EndososDatosId id, Double inSuma, Integer inProducto, Integer inPlan, Integer inNuCober, Integer inTipoPol, Integer inRespaldo) throws DataAccessException {
			Map<String, Object> inParams = new HashMap<>();
			inParams.put(Constantes.PARAM_CANAL, id.getCedaCasuCdSucursal());
			inParams.put(Constantes.PARAM_RAMO, id.getCedaCarpCdRamo());
			inParams.put(Constantes.PARAM_POLIZA, id.getCedaCapoNuPoliza());
			inParams.put(Constantes.PARAM_CERTIFICADO, id.getCedaNuCertificado());
			inParams.put(Constantes.PARAM_SUMA, inSuma);
			inParams.put("p_producto", inProducto);
			inParams.put("p_plan", inPlan);
			inParams.put("p_nu_cober", inNuCober);
			inParams.put("p_tipol", inTipoPol);
			inParams.put(Constantes.PARAM_RESPALDO, inRespaldo);
			
			return super.execute(inParams);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao#migraBeneficiario(mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId)
	 */
	@Override
	public void migraBeneficiario(EndososDatosId id) throws Excepciones {
		try {
			ProcesosPL proceso = new ProcesosPL(getSession().connection());
			proceso.migraBeneficiarios(id); 
		} catch (Exception e) {
			throw new Excepciones("Error: al migrar beneficiarios", e);
		}
		
	}
	
	
	@Override
		public String genRFCColCerti(String RFC, int numCertificado) throws Excepciones {
			// TODO Auto-generated method stub
			Map<String, Object> parametros = new HashMap<String, Object>();
			StringBuilder param=new StringBuilder();
			try {
				DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
				GeneraRFCCertificados rfcnu = new GeneraRFCCertificados(ds);
				this.logger.info("Valor de numero de certificado ProcesosDaoHibernate----"+numCertificado);
				
				parametros =  rfcnu.execute(RFC ,numCertificado);
				param.append(parametros.get("pValorRfc"));
				param.append("|");
				param.append(parametros.get("pMensaje"));
				this.logger.info("Valor de retorno de ejcuccion de porceso GENERARFC ************"+rfcnu);
				this.logger.info("Valores de Regreso-*/*/*/*/"+param);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Excepciones("Error en la ejecucion del proceso "+Colectivos.GENERA_RFC , e);
			}
			
			return param.toString();
		}
	
	public static class GeneraRFCCertificados extends StoredProcedure{

		
		public GeneraRFCCertificados(DataSource ds) {
			super.setDataSource(ds); 
			super.setFunction(true); 
			super.setSql(Colectivos.GENERA_RFC);
			super.declareParameter(new SqlOutParameter("returnname", Types.INTEGER));
			super.declareParameter(new SqlParameter("pRfc",Types.VARCHAR));
			super.declareParameter(new SqlParameter("pIdCliente", Types.INTEGER));
			super.declareParameter(new SqlParameter("pBand", Types.VARCHAR));
			super.declareParameter(new SqlOutParameter("pValorRfc", Types.VARCHAR));
			super.declareParameter(new SqlOutParameter("pMensaje",Types.VARCHAR));
			
			super.compile();
		}
			public Map<String, Object> execute(String RFC, int idcliente) throws DataAccessException {
			 Map<String, Object> inParams = new HashMap<>();
			 
			 inParams.put("pRfc",RFC);
			 inParams.put("pIdCliente",idcliente);
			 inParams.put("pBand", null);
			 
			 return super.execute(inParams);
			 
			}
			
			public Map<String, Object> executeCamFech(int idcliente,String camfech) throws DataAccessException {
				 Map<String, Object> inParams = new HashMap<>();
				 
				 inParams.put("pRfc",null);
				 inParams.put("pIdCliente",idcliente);
				 inParams.put("pBand", camfech);
				 
				 return super.execute(inParams);
				 
				}
		}
	
	
	
	
}