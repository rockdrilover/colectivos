/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Complementarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;

/**
 * @author Sergio Plata
 *
 */
public interface ClienteCertifDao extends CatalogoDao {

	public abstract  <T> List<T> consultaCredCte(String idCertificado, String nombre, String apellidoPaterno, String apellidoMaterno) throws Exception;
	public abstract  <T> List<T> consultaCredCert(String certs) throws Exception;
	public abstract  <T> List<T> consultaCredCobs(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
	public abstract  <T> List<T> consultaCredAsis(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
	public abstract  <T> List<T> consultaCredAsegurados(String certs, Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
	public abstract  Complementarios consultaCredComplementos(String credito,	Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
    public abstract String impresionCertificado(String rutaTemporal, short canal, short ramo, long poliza, long certificado, Date fecha_impresion ); 	
    public abstract String impresionPoliza(String rutaTemporal, short canal,short ramo, long poliza, Long certificado, Integer producto, Date fecha_emision);
	public abstract String getPlantilla(Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan, Date fechaEmision) throws Exception;
	public abstract ArrayList<Object> getDatosPlantilla(StringBuilder filtro)  throws Exception;
	
	/**
	 * Metodo que consulta las RECAS de un ramo y producto en especifico
	 * @param ramo ramo que tiene el certificado
	 * @param producto producto que tiene el certificado
	 * @return datos de RECAS
	 * @throws Exception
	 */
	Object[] getDatosRecas(Short ramo, String producto) throws Exception;
	
	/**
	 * Metodo que consulta parametrizacion de plantilla de un certificado
	 * @param canal canal que tiene el certificado
	 * @param ramo ramo que tiene el certificado
	 * @param poliza poliza que tiene el certificado
	 * @param certificado numero de certificado
	 * @param producto producto que tiene el certificado
	 * @param plan plan que tiene el certificado
	 * @return lista de parametros 
	 * @throws Exception
	 */
	<T> List<T> consultaDatos(Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan) throws Exception;
	
	/**
	 * Metodo que regresa objeto con datos de plantilla a generar 
	 * @return objeto con datos de plantilla a generar
	 * @throws Exception
	 */
	Parametros getRutaPlantilla() throws Exception;
	
	/**
	 * Metodo que consulta el plazo que tiene un certificado
	 * @param coceCarpCdRamo ramo del certificado
	 * @param alprCdProducto producto del certificado
	 * @param alplCdPlan plan del certificado
	 * @return plazo que tiene el certificado
	 * @throws Exception
	 */
	String getPlazo(Short coceCarpCdRamo, Integer alprCdProducto, Integer alplCdPlan) throws Exception;
	
	/**
	 * Metodo que consulta las coberturas de un certificado
	 * @param canal canal que tiene el certificado
	 * @param ramo ramo que tiene el certificado
	 * @param poliza poliza que tiene el certificado
	 * @param certificado numero de certificado
	 * @param producto producto que tiene el certificado
	 * @param plan plan que tiene el certificado
	 * @return lista con coberturas de un certificado
	 * @throws Exception
	 */
	ArrayList<Object> getCoberturas(Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan) throws Exception;
	
	/**
	 * Metodo que consulta los beneficiarios de un certificado
	 * @param canal canal que tiene el certificado
	 * @param ramo ramo que tiene el certificado
	 * @param poliza poliza que tiene el certificado
	 * @param certificado numero de certificado
	 * @return lista con beneficiarios de un certificado
	 * @throws Exception
	 */
	ArrayList<Object> getBeneficiarios(Short canal, Short ramo, Long poliza, Long certificado) throws Exception;
	
	/**
	 * Metodo que actualiza el titular por obligado
	 * @param id objeto con canal, ramo, poliza, certificado
	 * @throws Exception
	 */
	void actualizaTitular(CertificadoId id) throws Exception;
	
	/**
	 * Metodo que consulta los endodos de datos de un certificado
	 * @param strFiltro filtro de consulta
	 * @return lista con endosos de un certificado
	 * @throws Exception
	 */
	List<Object> getEndosos(String strFiltro) throws Exception;
   
}

