/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ReciboDao extends CatalogoDao,Serializable {
	
	public static enum FORMATO {pdf};

	public void impresionRecibo(String rutaTemporal, String nombre,
			Short careCasuCdSucursal, Short careCarpCdRamo, Integer careCapoNuPoliza,
			BigDecimal careNuRecibo, Short primaUnica);
	
	public void impresionPreRecibo(String rutaTemporal, String nombre,
			short cofaCasuCdSucursal, short cofaCarpCdRamo, long cofaCapoNuPoliza,
			String cofaCampov1);

	public <T> List<T> consulta2 (String strFiltro ) throws Excepciones;
	public List<Object[]> consultaDetalleCobertura(short ramo, double no_recibo, int sucursal) throws Exception;
	public List<Object[]> consultaDetalleComponente(short ramo, double no_recibo, int sucursal) throws Exception;
	
	public abstract <T> List<T> obtenerObjetos1(String filtro) throws Excepciones;

}
