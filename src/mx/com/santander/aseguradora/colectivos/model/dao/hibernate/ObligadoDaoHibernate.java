/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.util.List;



import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClientePK;
import mx.com.santander.aseguradora.colectivos.model.dao.ObligadoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;


/**
 * @author dflores
 *
 */
@Repository
public class ObligadoDaoHibernate extends HibernateDaoSupport implements
		ObligadoDao {

	@Autowired
	public ObligadoDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public void guardaObligado(Long cdParametro, String desParametro,
			String idParametro, Integer inProducto, String inPlan,
			String inPeriodicidad, Short inCanal, Short inRamo,
			Integer inPoliza, Short inNumObligados) throws Excepciones {
		// TODO Auto-generated method stub
		StringBuffer sbquery;                                                     
		Query query;  

	try { 
		sbquery = new StringBuffer();
		sbquery.append("INSERT INTO COLECTIVOS_PARAMETROS \n");       
		sbquery.append(" (COPA_CD_PARAMETRO ,   \n");             
		sbquery.append("  COPA_DES_PARAMETRO,   \n");             
		sbquery.append("  COPA_ID_PARAMETRO ,   \n");             
		sbquery.append("  COPA_VVALOR1      ,   \n");             
		sbquery.append("  COPA_VVALOR2      ,   \n");             
		sbquery.append("  COPA_VVALOR3      ,   \n");             
		sbquery.append("  COPA_NVALOR1      ,   \n");             
		sbquery.append("  COPA_NVALOR2      ,   \n");             
		sbquery.append("  COPA_NVALOR3      ,   \n");             
		sbquery.append("  COPA_NVALOR4      )   \n");             
		sbquery.append("VALUES (                \n");             
		sbquery.append("  :cdParam,             \n");             
		sbquery.append("  :desParam,            \n");             
		sbquery.append("  :idParam ,            \n");             
		sbquery.append("  :producto,    		\n");             
		sbquery.append("  :plan ,      			\n");             
		sbquery.append("  :tipo ,       		\n");             
		sbquery.append("  :canal,         		\n");             
		sbquery.append("  :ramo,          		\n");             
		sbquery.append("  :poliza,          	\n");             
		sbquery.append("  :obligados )   	    \n");             
                       
                                                                        
		query = getSession().createSQLQuery(sbquery.toString());                     
		                                                                        
		query.setString("cdParam", cdParametro.toString());                                    
		query.setString("desParam", desParametro);                                  
		query.setString("idParam", idParametro);                                    
		query.setString("producto",inProducto.toString());                           
		query.setString("plan",inPlan);                                 
		query.setString("tipo",inPeriodicidad);                                  
		query.setString("canal",inCanal.toString());                                   
		query.setString("ramo",inRamo.toString());                                
		query.setString("poliza",inPoliza.toString());                             
		query.setString("obligados",inNumObligados.toString());                             
		query.executeUpdate();                        
	}catch (Exception e){
		throw new Excepciones("Error: Al insertar parametros obligado ", e);   
     }		
  }
}




