/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		27-07-2020
 * Description: Interface que implementa la clase GenericDaoHibernate.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
 @Local
public interface GenericDao extends Serializable {
	
	/**
	 * Metodo que manda ejecutar consulta con metodo SQL
	 * @param sbConsulta consulta a ejecutar
	 * @return lista con resultados
	 * @throws Excepciones com errores en general
	 */
	List<Object> consultaSQL(StringBuilder sbConsulta) throws Excepciones;
	
	/**
	 * Metodo que consulta objetos de base de datos 
	 * @param <T> tipo de objeto a conultar
	 * @param filtro para aplicar en la consulta
	 * @param nTabla para saber de que tabla consultar
	 * @return lista de resultados
	 * @throws Excepciones con error generar
	 */
	<T> List<T> obtenerObjetos(String filtro, Integer nTabla) throws Excepciones;
	
	/**
	 * Metodo para realizar update a la base de datos
	 * @param sbQuery query a ejecutar
	 * @throws Excepciones con errores en general
	 */
	void updateSinParametros(StringBuilder sbQuery) throws Excepciones;
	
}
