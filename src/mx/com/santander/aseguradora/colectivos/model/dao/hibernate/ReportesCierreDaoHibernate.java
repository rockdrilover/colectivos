package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.dao.ReportesCierreDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import oracle.jdbc.OracleTypes;

@Repository
public class ReportesCierreDaoHibernate extends HibernateDaoSupport implements ReportesCierreDao {
	
	@Autowired
	public ReportesCierreDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeComCom(int canal) throws Exception {
		ArrayList<Object> arlRegreso;
		ArrayList<Object> resultado = null;
		String[] arrEncabezados = null;
		
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			arlRegreso = new ArrayList<Object>();
			resultado = new ArrayList<Object>();
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());		
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAECOMCOM + "(?,?)}");
			stmt.setInt(1, canal);
			stmt.registerOutParameter(2, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(2);						
			while(rs.next()) {
				System.out.println(rs.getString("valor"));
				if(rs.getString("dato1").equals("0")){
					arrEncabezados = rs.getString("valor").split("\\,");
				}else{					
					resultado.add(rs.getString("valor").split("\\,"));					
				}
			}
			
			arlRegreso.add(0, arrEncabezados);
			arlRegreso.add(1, resultado);
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return arlRegreso;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraerMontoRfi(String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		Object[] arrDatos = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			resultado = new ArrayList<Object>();
			arrDatos = new Object[9];
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAERFI + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				arrDatos[0] = rs.getInt("ramo");
				arrDatos[1] = rs.getInt("operacion");
				arrDatos[2] = rs.getString("fr_pago");
				arrDatos[3] = rs.getInt("coaseguro");
				arrDatos[4] = rs.getString("codigo_cuenta");
				arrDatos[5] = rs.getInt("debe");
				arrDatos[5] = rs.getInt("haber");
				arrDatos[7] = rs.getInt("debe_coa");
				arrDatos[8] = rs.getInt("haber_coa");
				resultado.add(arrDatos);
				
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraerRecCob(String finicio, String ffin) throws Exception {
		ArrayList<Object> arlRegreso;
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		String[] arrEncabezados = null;
		
		try {
			arlRegreso = new ArrayList<Object>();
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAECOBR + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				System.out.println(rs.getString("valor"));
				if(rs.getString("dato1").equals("0")){
					arrEncabezados = rs.getString("valor").split("\\,");
				}else{					
					resultado.add(rs.getString("valor").split("\\,"));					
				}
			}
			arlRegreso.add(0, arrEncabezados);
			arlRegreso.add(1, resultado);
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeRepEmi() throws Exception {
		ArrayList<Object> arlRegreso;
		ArrayList<Object> resultado = null;
		String[] arrEncabezados = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			arlRegreso = new ArrayList<Object>();
			resultado = new ArrayList<Object>();
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAEREPEMI + "(?)}");
			stmt.registerOutParameter(1, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(1);						
			while(rs.next()) {
				System.out.println(rs.getString("valor"));
				if(rs.getString("dato1").equals("0")){
					arrEncabezados = rs.getString("valor").split("\\,");
				}else{					
					resultado.add(rs.getString("valor").split("\\,"));					
				}
			}
			arlRegreso.add(0, arrEncabezados);
			arlRegreso.add(1, resultado);
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraerRecSnCob(String finicio, String ffin) throws Exception {
		ArrayList<Object> arlRegreso;
		ArrayList<Object> resultado = null;
		String[] arrEncabezados = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			arlRegreso = new ArrayList<Object>();
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAESNCOBR + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);
			while(rs.next()) {
				System.out.println(rs.getString("valor"));
				if(rs.getString("dato1").equals("0")){
					arrEncabezados = rs.getString("valor").split("\\,");
				}else{					
					resultado.add(rs.getString("valor").split("\\,"));					
				}
			}
			arlRegreso.add(0, arrEncabezados);
			arlRegreso.add(1, resultado);
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeRepTotCoa(String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAEREPTOTCOA + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraerCifCom(String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		Object[] arrDatos = null;
		
		try {
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			arrDatos = new Object[9];
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAECIFCOM + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				arrDatos[0] = rs.getInt("canal");
				arrDatos[1] = rs.getInt("ramo");
				arrDatos[2] = rs.getInt("porcentaje");
				arrDatos[3] = rs.getInt("pmaNeta");
				arrDatos[4] = rs.getInt("asistencia");
				arrDatos[5] = rs.getInt("Recargo");
				arrDatos[5] = rs.getInt("Comision");
				arrDatos[7] = rs.getInt("ComisionAsistencia");
				arrDatos[8] = rs.getInt("correcto");
				resultado.add(arrDatos);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraerCifComU(String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		Object[] arrDatos = null;
		
		try {
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			arrDatos = new Object[9];
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAECIFCOMU + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				arrDatos[0] = rs.getInt("canal");
				arrDatos[1] = rs.getInt("ramo");
				arrDatos[2] = rs.getInt("porcentaje");
				arrDatos[3] = rs.getInt("pmaNeta");
				arrDatos[4] = rs.getInt("asistencia");
				arrDatos[5] = rs.getInt("Recargo");
				arrDatos[5] = rs.getInt("Comision");
				arrDatos[7] = rs.getInt("ComisionAsistencia");
				arrDatos[8] = rs.getInt("correcto");
				resultado.add(arrDatos);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeDosOp(String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		Object[] arrDatos = null;
		
		try {
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			arrDatos = new Object[8];
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAEREPDPOD + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				arrDatos[0] = rs.getInt("ramo");
				arrDatos[1] = rs.getInt("operacion");
				arrDatos[2] = rs.getString("periodo");
				arrDatos[3] = rs.getString("cuenta");
				arrDatos[4] = rs.getInt("mt_debe");
				arrDatos[5] = rs.getInt("mt_haber");
				arrDatos[5] = rs.getInt("mt_debe_coa");
				arrDatos[7] = rs.getInt("mt_haber_coa");
				resultado.add(arrDatos);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeCuatroOP(String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		Object[] arrDatos = null;
		
		try {
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			arrDatos = new Object[8];
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAEREPDPOC + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				arrDatos[0] = rs.getInt("ramo");
				arrDatos[1] = rs.getInt("operacion");
				arrDatos[2] = rs.getString("periodo");
				arrDatos[3] = rs.getString("cuenta");
				arrDatos[4] = rs.getInt("mt_debe");
				arrDatos[5] = rs.getInt("mt_haber");
				arrDatos[5] = rs.getInt("mt_debe_coa");
				arrDatos[7] = rs.getInt("mt_haber_coa");
				resultado.add(arrDatos);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeComTec(int canal) throws Exception {
		ArrayList<Object> arlRegreso;
		ArrayList<Object> resultado = null;
		String[] arrEncabezados = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			arlRegreso = new ArrayList<Object>();
			resultado = new ArrayList<Object>();
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAECOMTEC + "(?,?)}");
			stmt.setInt(1, canal);
			stmt.registerOutParameter(2, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(2);						
			while(rs.next()) {
				System.out.println(rs.getString("valor"));
				if(rs.getString("dato1").equals("0")){
					arrEncabezados = rs.getString("valor").split("\\,");
				}else{					
					resultado.add(rs.getString("valor").split("\\,"));					
				}
			}
			
			arlRegreso.add(0, arrEncabezados);
			arlRegreso.add(1, resultado);
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeCifTec(String finicio, String ffin, int operacion) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		Object[] arrDatos = null;
		
		try {
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			arrDatos = new Object[9];
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAECIFTEC + "(?,?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.setInt(3, operacion);
			stmt.registerOutParameter(4, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(4);						
			while(rs.next()) {
				arrDatos[0] = rs.getInt("canal");
				arrDatos[1] = rs.getInt("ramo");
				arrDatos[2] = rs.getInt("porcentaje");
				arrDatos[3] = rs.getInt("pmaNeta");
				arrDatos[4] = rs.getInt("asistencia");
				arrDatos[5] = rs.getInt("Recargo");
				arrDatos[5] = rs.getInt("Comision");
				arrDatos[7] = rs.getInt("ComisionAsistencia");
				arrDatos[8] = rs.getInt("correcto");
				resultado.add(arrDatos);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeDetaRfi(String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		Object[] arrDatos = null;
		
		try {
			resultado = new ArrayList<Object>();
//			java.sql.Date f_inicio = new java.sql.Date(finicio.getTime());
//			java.sql.Date f_fin = new java.sql.Date(ffin.getTime());
			arrDatos = new Object[10];
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			con = ds.getConnection();
			stmt = con.prepareCall("{call " + Colectivos.EXTRAEDETARFI + "(?,?,?)}");
			stmt.setString(1, finicio);
			stmt.setString(2, ffin);
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.execute();
			
			rs = (ResultSet) stmt.getObject(3);						
			while(rs.next()) {
				arrDatos[0] = rs.getInt("ramo");
				arrDatos[1] = rs.getInt("mov");
				arrDatos[2] = rs.getString("cuenta");
				arrDatos[3] = rs.getString("in_cierre");
				arrDatos[4] = rs.getString("mes");
				arrDatos[5] = rs.getInt("nu_regs");
				arrDatos[5] = rs.getInt("mt_debe");
				arrDatos[7] = rs.getInt("mt_haber");
				arrDatos[8] = rs.getInt("mt_debe_coa");
				arrDatos[9] = rs.getInt("mt_haber_coa");
				resultado.add(arrDatos);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		
		return resultado;
	}


	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeReservas(int canal, int ramo, String finicio, String ffin) throws Exception {
		ArrayList<Object> resultado = null;
		try {
			StringBuilder sb = new StringBuilder();
			if(! finicio.equals("") && !ffin.equals("")){
				System.out.println("consulta con fechas");
				sb.append(" Select cc.coce_capo_nu_poliza POLIZA,cc.coce_nu_cuenta NU_CUENTA,cln.cocn_nombre||' '||cln.cocn_apellido_pat||' '||cln.cocn_apellido_mat NOMBRE,"
						+ "cc.coce_fr_pago F_P,0 RP,cc.coce_fe_desde INICIO,cc.coce_fe_hasta FIN,0 T_Por_Dev,"
						+ "0 Su_Ase_basica,0 Su_Ase_BIT,0 PrimaNetaCai,0 PrimaNetaDi,0 PrimasNetasSERF,"
						+ "0 EXTR_PRI,0 Dotal_CP,0 RBASICA,0 BIT,0 ReservasCAI,0 ReservasDI,0 ReservasSERF,"
						+ "0 EXTRAPRI, 0 DotalCP, 0 TOTAL, 0 t1,0 t2,"
						+ "cln.COCN_CD_ESTADO ESTADO,cln.cocn_cd_sexo SEXO ");
				sb.append(" from colectivos_certificados cc,cart_recibos cr,colectivos_cliente_certif clc,colectivos_clientes cln ");   				
				sb.append(" where cc.coce_casu_cd_sucursal >=  ").append(canal);
				sb.append(" and cc.coce_carp_cd_ramo = ").append(ramo);
				sb.append(" and cc.coce_capo_nu_poliza >= 0 ");
				sb.append(" and cc.coce_nu_certificado >= 0 ");
				sb.append(" and cc.coce_st_certificado in (1,2) ");
				sb.append(" and cc.coce_fe_desde between to_date('").append(finicio).append("','dd/MM/yyyy') and to_date('").append(ffin).append("','dd/MM/yyyy')" ); 
//				sb.append(" and cc.coce_fe_desde >= to_date('").append(finicio).append("','dd/MM/yyyy')"); 
//				sb.append(" and cc.coce_fe_hasta <=to_date('").append(ffin).append("','dd/MM/yyyy')");
				sb.append(" and cc.coce_casu_cd_sucursal = cr.care_casu_cd_sucursal ");
				sb.append(" and cc.coce_carp_cd_ramo = cr.care_carp_cd_ramo ");
				sb.append(" and cc.coce_capo_nu_poliza = cr.care_capo_nu_poliza ");
				sb.append(" and cc.coce_nu_certificado = cr.care_cace_nu_certificado ");
				sb.append(" and cc.coce_fe_desde >= cr.care_fe_desde ");
				sb.append(" and cc.coce_fe_hasta <= cr.care_fe_hasta ");							    
				sb.append(" and clc.cocc_casu_cd_sucursal = cc.coce_casu_cd_sucursal ");
				sb.append(" and clc.cocc_carp_cd_ramo = cc.coce_carp_cd_ramo ");
				sb.append(" and clc.cocc_capo_nu_poliza = cc.coce_capo_nu_poliza ");
				sb.append(" and clc.cocc_nu_certificado = cc.coce_nu_certificado ");
				sb.append(" and clc.cocc_nu_cliente = cln.cocn_nu_cliente ");								
									 				         				
				Query query = this.getSession().createSQLQuery(sb.toString());
				resultado = (ArrayList<Object>) query.list();
			}else {
				System.out.println("consulta sin fechas");					
				sb.append(" Select cc.coce_capo_nu_poliza POLIZA,cc.coce_nu_cuenta NU_CUENTA,cln.cocn_nombre||' '||cln.cocn_apellido_pat||' '||cln.cocn_apellido_mat NOMBRE,"
						+ "cc.coce_fr_pago F_P,0 RP,cc.coce_fe_desde INICIO,cc.coce_fe_hasta FIN,0 T_Por_Dev,"
						+ "0 Su_Ase_basica,0 Su_Ase_BIT,0 PrimaNetaCai,0 PrimaNetaDi,0 PrimasNetasSERF,"
						+ "0 EXTR_PRI,0 Dotal_CP,0 RBASICA,0 BIT,0 ReservasCAI,0 ReservasDI,0 ReservasSERF,"
						+ "0 EXTRAPRI, 0 DotalCP, 0 TOTAL, 0 t1,0 t2,"
						+ "cln.COCN_CD_ESTADO ESTADO,cln.cocn_cd_sexo SEXO");
				sb.append(" from colectivos_certificados cc,cart_recibos cr,colectivos_cliente_certif clc,colectivos_clientes cln ");   				
				sb.append(" where cc.coce_casu_cd_sucursal >=  ").append(canal);
				sb.append(" and cc.coce_carp_cd_ramo = ").append(ramo);
				sb.append(" and cc.coce_capo_nu_poliza >= 0 ");
				sb.append(" and cc.coce_nu_certificado >= 0 ");
				sb.append(" and cc.coce_st_certificado in (1,2) ");							
				sb.append(" and cc.coce_casu_cd_sucursal = cr.care_casu_cd_sucursal ");
				sb.append(" and cc.coce_carp_cd_ramo = cr.care_carp_cd_ramo ");
				sb.append(" and cc.coce_capo_nu_poliza = cr.care_capo_nu_poliza ");
				sb.append(" and cc.coce_nu_certificado = cr.care_cace_nu_certificado ");
				sb.append(" and cc.coce_fe_desde >= cr.care_fe_desde ");
				sb.append(" and cc.coce_fe_hasta <= cr.care_fe_hasta ");							    
				sb.append(" and clc.cocc_casu_cd_sucursal = cc.coce_casu_cd_sucursal ");
				sb.append(" and clc.cocc_carp_cd_ramo = cc.coce_carp_cd_ramo ");
				sb.append(" and clc.cocc_capo_nu_poliza = cc.coce_capo_nu_poliza ");
				sb.append(" and clc.cocc_nu_certificado = cc.coce_nu_certificado ");
				sb.append(" and clc.cocc_nu_cliente = cln.cocn_nu_cliente ");
				
				Query query = this.getSession().createSQLQuery(sb.toString());
				resultado = (ArrayList<Object>) query.list();				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return resultado;

	}


	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public ArrayList<Object> extraeSumaAseg(int canal, int ramo, String finicio, String ffin) throws Exception {
		
		ArrayList<Object> resultado = null;
		try {
			StringBuilder sb = new StringBuilder();
			if(! finicio.equals("") && !ffin.equals("")){
				System.out.println("consulta con fechas");
				sb.append(" select cc.coce_capo_nu_poliza POLIZA,cc.coce_nu_cuenta NU_CUENTA,cc.coce_fe_desde INICIO,"
						+ "cc.coce_fe_hasta FECHA_HASTA,cln.cocn_nombre||' '||cln.cocn_apellido_pat||' '||cln.cocn_apellido_mat NOMBRE,"
						+ "cc.coce_mt_suma_asegurada SUMA_ASEG,0 Su_Ase_basica,0 Su_Ase_BIT,"
						+ "0 Su_Ase_invalidez,0 Su_Ase_Accidental,0 Su_Ase_SERF,0 Extra_Prima,0 Sum_Cob_Dotal_CP,"
						+ "0 Sum_Aseg_Tot,0 Num_Aseg_Cob_Bas,0 Num_Aseg_Cob_BIT, cc.coce_fr_pago FR_PAGO,cr.care_nu_recibo RECIBO,"
						+ "0 Num_Aseg_Cob_INV,0 Num_Aseg_Cob_ACC,0 Num_Aseg_Cob_SERF,0 Num_Aseg_Cob_Extr_PRRIM,0 Num_Aseg_Cob_Dotal_CP,"
						+ "cc.coce_mt_prima_pura PRIMA_PURA,cln.cocn_cd_sexo SEXO,cln.COCN_CD_ESTADO ESTADO,"
						+ "max(cc.coce_nu_certificado) ASEGURADOS ");			      				
				sb.append(" from colectivos_certificados cc,cart_recibos cr,colectivos_cliente_certif clc,colectivos_clientes cln ");   				
				sb.append(" where cc.coce_casu_cd_sucursal >=  ").append(canal);
				sb.append(" and cc.coce_carp_cd_ramo = ").append(ramo);
				sb.append(" and cc.coce_capo_nu_poliza >= 0 ");
				sb.append(" and cc.coce_nu_certificado >= 0 ");
				sb.append(" and cc.coce_st_certificado in (1,2) ");			
				sb.append(" and cc.coce_fe_desde >= to_date('").append(finicio).append("','dd/MM/yyyy')"); 
				sb.append(" and cc.coce_fe_hasta <=to_date('").append(ffin).append("','dd/MM/yyyy')");
				sb.append(" and cc.coce_casu_cd_sucursal = cr.care_casu_cd_sucursal ");
				sb.append(" and cc.coce_carp_cd_ramo = cr.care_carp_cd_ramo ");
				sb.append(" and cc.coce_capo_nu_poliza = cr.care_capo_nu_poliza ");
				sb.append(" and cc.coce_nu_certificado = cr.care_cace_nu_certificado ");
				sb.append(" and cc.coce_fe_desde >= cr.care_fe_desde ");
				sb.append(" and cc.coce_fe_hasta <= cr.care_fe_hasta ");							    
				sb.append(" and clc.cocc_casu_cd_sucursal = cc.coce_casu_cd_sucursal ");
				sb.append(" and clc.cocc_carp_cd_ramo = cc.coce_carp_cd_ramo ");
				sb.append(" and clc.cocc_capo_nu_poliza = cc.coce_capo_nu_poliza ");
				sb.append(" and clc.cocc_nu_certificado = cc.coce_nu_certificado ");
				sb.append(" and clc.cocc_nu_cliente = cln.cocn_nu_cliente ");
				sb.append(" group by clc.cocc_nu_cliente,cc.coce_capo_nu_poliza ,cc.coce_nu_cuenta ,cc.coce_fe_desde,");
				sb.append(" cln.cocn_nombre ,cc.coce_mt_suma_asegurada,cc.coce_fr_pago,cr.care_nu_recibo,");
				sb.append(" cc.coce_mt_prima_pura,cc.coce_fe_hasta,cln.cocn_cd_sexo,cln.COCN_CD_ESTADO,");
				sb.append(" cln.cocn_nombre,cln.cocn_apellido_pat,cln.cocn_apellido_mat");
				
				Query query = this.getSession().createSQLQuery(sb.toString());
				resultado = (ArrayList<Object>) query.list();
			}else {
				System.out.println("consulta sin fechas");
				sb.append(" select cc.coce_capo_nu_poliza POLIZA,cc.coce_nu_cuenta NU_CUENTA,cc.coce_fe_desde INICIO,"
						+ "cc.coce_fe_hasta FECHA_HASTA,cln.cocn_nombre||' '||cln.cocn_apellido_pat||' '||cln.cocn_apellido_mat NOMBRE,"
						+ "cc.coce_mt_suma_asegurada SUMA_ASEG,0 Su_Ase_basica,0 Su_Ase_BIT,"
						+ "0 Su_Ase_invalidez,0 Su_Ase_Accidental,0 Su_Ase_SERF,0 Extra_Prima,0 Sum_Cob_Dotal_CP,"
						+ "0 Sum_Aseg_Tot,0 Num_Aseg_Cob_Bas,0 Num_Aseg_Cob_BIT, cc.coce_fr_pago FR_PAGO,cr.care_nu_recibo RECIBO,"
						+ "0 Num_Aseg_Cob_INV,0 Num_Aseg_Cob_ACC,0 Num_Aseg_Cob_SERF,0 Num_Aseg_Cob_Extr_PRRIM,0 Num_Aseg_Cob_Dotal_CP,"
						+ "cc.coce_mt_prima_pura PRIMA_PURA,cln.cocn_cd_sexo SEXO,cln.COCN_CD_ESTADO ESTADO,"
						+ "max(cc.coce_nu_certificado) ASEGURADOS ");			      				
				sb.append(" from colectivos_certificados cc,cart_recibos cr,colectivos_cliente_certif clc,colectivos_clientes cln ");   				
				sb.append(" where cc.coce_casu_cd_sucursal >=  ").append(canal);
				sb.append(" and cc.coce_carp_cd_ramo = ").append(ramo);
				sb.append(" and cc.coce_capo_nu_poliza >= 0 ");
				sb.append(" and cc.coce_nu_certificado >= 0 ");
				sb.append(" and cc.coce_st_certificado in (1,2) ");							
				sb.append(" and cc.coce_casu_cd_sucursal = cr.care_casu_cd_sucursal ");
				sb.append(" and cc.coce_carp_cd_ramo = cr.care_carp_cd_ramo ");
				sb.append(" and cc.coce_capo_nu_poliza = cr.care_capo_nu_poliza ");
				sb.append(" and cc.coce_nu_certificado = cr.care_cace_nu_certificado ");
				sb.append(" and cc.coce_fe_desde >= cr.care_fe_desde ");
				sb.append(" and cc.coce_fe_hasta <= cr.care_fe_hasta ");							    
				sb.append(" and clc.cocc_casu_cd_sucursal = cc.coce_casu_cd_sucursal ");
				sb.append(" and clc.cocc_carp_cd_ramo = cc.coce_carp_cd_ramo ");
				sb.append(" and clc.cocc_capo_nu_poliza = cc.coce_capo_nu_poliza ");
				sb.append(" and clc.cocc_nu_certificado = cc.coce_nu_certificado ");
				sb.append(" and clc.cocc_nu_cliente = cln.cocn_nu_cliente ");								
				sb.append(" group by clc.cocc_nu_cliente,cc.coce_capo_nu_poliza ,cc.coce_nu_cuenta ,cc.coce_fe_desde,");
				sb.append(" cln.cocn_nombre ,cc.coce_mt_suma_asegurada,cc.coce_fr_pago,cr.care_nu_recibo,");
				sb.append(" cc.coce_mt_prima_pura,cc.coce_fe_hasta,cln.cocn_cd_sexo,cln.COCN_CD_ESTADO,");
				sb.append(" cln.cocn_nombre,cln.cocn_apellido_pat,cln.cocn_apellido_mat");
				
				Query query = this.getSession().createSQLQuery(sb.toString());
				resultado = (ArrayList<Object>) query.list();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return resultado;
	}



}
