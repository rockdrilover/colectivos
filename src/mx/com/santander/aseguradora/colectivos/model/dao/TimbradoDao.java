package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-04-2020
 * Description: Interface que implementa la clase TimbradoDaoHibernate.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Local
public interface TimbradoDao extends Serializable {
	
	/**
	 * Metodo que consulta los recibos timbrados
	 * @param filtro where a ejecutar
	 * @param feHasta fecha desde para filtro
	 * @param feDesde fecha hasta para filtro
	 * @param tipoBusqueda tipo de busqueda
	 * @return lista de datos 
	 * @throws Excepciones con error en general
	 */
	List<Object> getRecTimbrados(StringBuilder filtro, String feDesde, String feHasta, Integer tipoBusqueda) throws Excepciones;
	
	/**
	 * Metodo que consulta el detalle de un timbrado
	 * @param recibo numero de recibo
	 * @return lista con el detalle 
	 * @throws Excepciones con error en general
	 */
	List<Object> getDetalleTimbrado(BigDecimal recibo) throws Excepciones;
	
	
	/**
	 * Metodo que consulta las coberturas 
	 * @param recibo recibo para buscar
	 * @return lista con coberturas
	 * @throws Excepciones con error general
	 */
	List<ColectivosCoberturas> getDetalleCober(BigDecimal recibo) throws Excepciones;

	/**
	 * Metodo que consulta los componentes
	 * @param recibo recibo para buscar
	 * @return lista con datos componentes
	 * @throws Excepciones con error general
	 */
	List<ColectivosComponentes> getDetalleComp(BigDecimal recibo) throws Excepciones;

	/**
	 * Metodo que obtiene los datos que se ocuparan para timbrar
	 * @param canal canal del recibo a buscar
	 * @param ramo ramo del recibo a buscar
	 * @param poliza poliza del recibo a buscar
	 * @param certi certi del recibo a buscar
	 * @param recibo recibo a buscar
	 * @return lista con valores de la busqueda
	 * @throws Excepciones con error general
	 */
	Map<String, Object> getDatosTimbrado(int canal, byte ramo, int poliza, int certi, BigDecimal recibo) throws Excepciones;

	/**
	 * Metodo que consulta la descripcion del error
	 * @param cdError codigo de error
	 * @return descripcion del error
	 * @throws Excepciones con error general 
	 */
	String getDescError(String cdError) throws Excepciones;
}
