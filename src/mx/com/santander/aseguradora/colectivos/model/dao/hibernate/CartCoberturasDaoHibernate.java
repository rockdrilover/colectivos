/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.dao.CartCoberturasDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Leobardo Preciado
 *
 */
@Repository
public class CartCoberturasDaoHibernate extends HibernateDaoSupport implements
		CartCoberturasDao {

	@Autowired
	public CartCoberturasDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			T producto = (T) this.getHibernateTemplate().get(objeto, id);
			return producto;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			List<T> lista = this.getHibernateTemplate().loadAll(objeto);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro)throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer sb = new StringBuffer(150);
					sb.append("from CartCoberturas P	\n");
					sb.append("where 1 = 1      \n");
					sb.append(filtro);
					sb.append("order by P.id");
									
					Query query = sesion.createQuery(sb.toString());
					
					List<T> lista = query.list();
					return lista;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}

		
	}

	
	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		
		try {
			this.getHibernateTemplate().merge(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);
			
		}
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			for(T objeto: lista){
				
				this.guardarObjeto(objeto);
			}
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}


	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS) 
	public <T> List<T> obtenerCartCoberturas(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
    	StringBuilder query = null;
    	List<T> lista = null;
    	Session sesion;
    	
    
    	try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer query = new StringBuffer(150);
		    		query.append("		  FROM CartCoberturas ca  																\n");
		    		query.append("       WHERE ( ca.id.cacbCarbCdRamo,															\n");
		    		query.append("               ca.id.cacbCdCobertura)															\n");
		    		query.append("      IN(SELECT DISTINCT cb.id.cocbCarbCdRamo, cb.id.cocbCacbCdCobertura  					\n");
		    		query.append("                                       from  ColectivosCoberturas cb      					\n");
		    		query.append("                                      where  cb.id.cocbCasuCdSucursal = 1      				\n");
		    		query.append("                                      and  cb.id.cocbCarpCdRamo =") .append(filtro);
									
					Query qry = sesion.createQuery(query.toString());
					
					List<T> lista = qry.list();
					return lista;
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}	
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS) 
	public <T> List<T> obtenerCartCoberturasObj(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
    	StringBuilder query = null;
    	List<T> lista = null;
    	Session sesion;
    	
    
    	try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer query = new StringBuffer(150);
		    		query.append("		  FROM CartCoberturas ca  											\n");
		    		query.append("       WHERE ( ca.id.cacbCarbCdRamo,										\n");
		    		query.append("               ca.id.cacbCdCobertura)										\n");
		    		query.append("      IN(SELECT DISTINCT cb.id.cocbCarbCdRamo, cb.id.cocbCacbCdCobertura	\n");
		    		query.append("                                       from  ColectivosCoberturas cb		\n");
		    		query.append("                                      where  1=1 							\n");
		    		query.append("                                      and cb.id.cocbCasuCdSucursal = 1	\n");
		    		query.append(filtro);
		    		query.append(" 		   )	");
		    		
					Query qry = sesion.createQuery(query.toString());
					
					List<T> lista = qry.list();
					return lista;
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}	
	}
	
	
}
