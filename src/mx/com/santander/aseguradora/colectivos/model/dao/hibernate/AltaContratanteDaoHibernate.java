/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.dao.AltaContratanteDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author dflores
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class AltaContratanteDaoHibernate extends HibernateDaoSupport implements AltaContratanteDao {
	
	
	@Autowired
	public AltaContratanteDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}
	
	public <T> T guardarObjeto(T objeto) throws Excepciones {
        return null;
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		try {
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){
				public Object doInHibernate(Session sesion) throws HibernateException, SQLException {
					StringBuilder consulta = new StringBuilder();
					consulta.append("from VtaPrecarga PC\n");
					consulta.append("where 1 = 1\n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					List<T> lista = qry.list();
					
					return lista;
				}
			});
		} catch (Exception e) {
			throw new Excepciones("Error al consultar registros de tabla: .", e);
		}
	}

	@SuppressWarnings("unchecked")
	 public boolean actualizaCliente(ContratanteDTO contratanteDTO, Map<String, Object> resultadoCliente) throws Excepciones {
       boolean proceso=false;
       StringBuffer actualiza = new StringBuffer();
       try {
           actualiza.append("UPDATE CART_CLIENTES CL  \n");
           actualiza.append("    SET CL.CACN_DI_HABITACION1 = '" + contratanteDTO.getCacnDiHabitacion1() + "', \n");
           actualiza.append("        CL.CACN_ZN_POSTAL_HABITACION = '" + contratanteDTO.getCacnZnPostalHabitacion() + "', \n");
           actualiza.append("        CL.CACN_DI_COBRO2 = '" + contratanteDTO.getCacnDiCobro2() + "', \n");
           actualiza.append("        CL.CACN_DATOS_REG_1 = '" + contratanteDTO.getCacnDatosReg1() + "', \n");
           actualiza.append("        CL.CACN_CAES_CD_ESTADO_HAB = " + contratanteDTO.getCacnCaesCdEstadoHab()  + ",\n");
           actualiza.append("        CL.CACN_NM_PERSONA_NATURAL ='" + contratanteDTO.getCacnNmPersonaNatural()  + "', \n");
           actualiza.append("        CL.CACN_NU_TELEFONO_HABITACION = '" + contratanteDTO.getCacnNuTelefonoHabitacion() + "', \n");
           actualiza.append("        CL.CACN_REGISTRO_2  = '" + contratanteDTO.getCacnRegistro2() + "', \n");
           actualiza.append("        CL.CACN_DI_HABITACION2 = '" + contratanteDTO.getCacnDiHabitacion2()  + "'\n");
           actualiza.append("WHERE CL.CACN_CD_NACIONALIDAD = '" + resultadoCliente.get("W_nac").toString()  + "' \n");
           actualiza.append("AND CL.CACN_NU_CEDULA_RIF = '" + resultadoCliente.get("W_cedula").toString()  + "'");
           Query queryAct = getSession().createSQLQuery(actualiza.toString());
           queryAct.executeUpdate();
           proceso=true;
       } catch (Exception e) {
           e.printStackTrace();
          
       }
      
   return proceso;
   }
	
	@SuppressWarnings("unchecked")
	public void actualizaCertificados(Integer producto, Integer plan, String sumaAseg, String Prima, Short sucursal,Short ramo, Long poliza, Integer certificado, String formaPago) throws Excepciones {
		StringBuffer actualiza = new StringBuffer();
		try {
   	   actualiza.append("UPDATE COLECTIVOS_CERTIFICADOS\n");
	       actualiza.append("SET COCE_CAPU_CD_PRODUCTO =" + producto + ",\n"); 
	       actualiza.append("		COCE_CAPB_CD_PLAN =" + plan + ",\n");
	       actualiza.append("		COCE_MT_SUMA_ASEGURADA ='" + sumaAseg + "',\n"); 
	       actualiza.append("		COCE_MT_SUMA_ASEG_SI ='" + sumaAseg + "',\n"); 
	       actualiza.append("		COCE_MT_PRIMA_SUBSECUENTE ='" + Prima + "',\n");
	       actualiza.append("		COCE_FR_PAGO ='"+ formaPago + "',\n");
	       actualiza.append("		COCE_ST_FACTURACION = 1 \n");
	       actualiza.append("WHERE COCE_CASU_CD_SUCURSAL =" + sucursal + "\n"); 
	       actualiza.append("      	AND COCE_CARP_CD_RAMO =" + ramo + "\n"); 
	       actualiza.append("       AND COCE_CAPO_NU_POLIZA ='" + poliza + "'\n");
	       actualiza.append("		AND COCE_NU_CERTIFICADO =" + certificado);
   	          
          Query queryAct = getSession().createSQLQuery(actualiza.toString());
          queryAct.executeUpdate();
      } catch (Exception e) {
          e.printStackTrace();
         
      }
  }

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Integer obtenerFormaPago(Short ramo, Integer producto, Integer plan) throws Excepciones {
		StringBuffer actualiza = new StringBuffer();
		Integer formaPago=null;
		try {
    	   actualiza.append("SELECT  ALPL_DATO4 from ALTERNA_PLANES\n");
    	   actualiza.append("	WHERE ALPL_CD_RAMO=" + ramo +"\n");
    	   actualiza.append("	AND ALPL_CD_PRODUCTO=" + producto +"\n");
    	   actualiza.append("	AND ALPL_CD_PLAN=" + plan);     
           Query queryAct = getSession().createSQLQuery(actualiza.toString());
           queryAct.executeUpdate();
           if (queryAct != null){
        	   formaPago= Integer.parseInt(queryAct.list().get(0).toString());
           }
           
       } catch (Exception e) {
           e.printStackTrace();
          
       }
		return formaPago;
		
	}

	@Override
	public ArrayList<Object> consultar(String strQuery) throws Exception {
		ArrayList<Object> arlResutlados;
		Query query;
		
		try {
			arlResutlados = new ArrayList<Object>();
			if(!strQuery.equals("")){
				query = getSession().createSQLQuery(strQuery);
				arlResutlados = (ArrayList<Object>) query.list();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error: AltaContratanteDao.consultar:: " + e.getMessage());
		}
		return arlResutlados;
	}

	@Override
	public void actualizaCartCertificados(short cdSucursal, Short inRamo, Long numeroPoliza, Map<String, Object> resultadoCliente2) throws Exception {
		StringBuffer actualiza = new StringBuffer();
		try {
			actualiza.append("UPDATE CART_CERTIFICADOS CA  \n");
			actualiza.append("    SET CA.CACE_CACN_CD_NACIONALIDAD = '" + resultadoCliente2.get("W_nac").toString() + "', \n");
			actualiza.append("        CA.CACE_CACN_NU_CEDULA_RIF = '" + resultadoCliente2.get("W_cedula").toString() + "' \n");
			actualiza.append("WHERE CA.CACE_CASU_CD_SUCURSAL = " + cdSucursal  + " \n");
			actualiza.append("AND CA.CACE_CARP_CD_RAMO = " + inRamo + " \n");
			actualiza.append("AND CA.CACE_CAPO_NU_POLIZA = " + numeroPoliza  + " \n");
			actualiza.append("AND CA.CACE_NU_CERTIFICADO = 0");
			Query queryAct = getSession().createSQLQuery(actualiza.toString());
			queryAct.executeUpdate();       
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
	}

}
