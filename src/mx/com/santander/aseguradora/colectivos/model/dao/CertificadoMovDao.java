/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

/**
 * @author z014155
 *
 */
public interface CertificadoMovDao {

	public abstract List<Object> consultaRecibos(short sucursal, byte ramo, long poliza, long certificado);
	
}

