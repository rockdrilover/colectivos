package mx.com.santander.aseguradora.colectivos.model.dao;

import java.math.BigDecimal;
import java.util.Collection;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

/**
 * Interface para Endosos de Datos
 * @author Ing. Issac Bautista
 *
 */
public interface EndososDatosDao {
	
	/**
	 * Metodo que consulta registros de EndososDatos
	 * @param filtro filtro a consultar 
	 * @return lista con Endosos
	 * @throws Excepciones error de consulta
	 */
	Collection<Object> getEndososDatos(final String filtro) throws Excepciones;

	/**
	 * Metodo que actualiza datos en Colectivos_Certificados
	 * @param seleccionado datos de endoso
	 * @throws Excepciones error de proceso
	 */
	void actualizaCertificado(EndosoDatosDTO seleccionado) throws Excepciones;
	
	/**
	 * Metodo que actualiza datos en Colectivos_Clientes
	 * @param seleccionado datos de endoso
	 * @throws Excepciones error de proceso
	 */
	void actualizaCliente(EndosoDatosDTO seleccionado) throws Excepciones;

	/**
	 * Metodo que respalda informacion del certificado
	 * @param coceCasuCdSucursal canal del certificado
	 * @param coceCarpCdRamo ramo del certificado
	 * @param coceCapoNuPoliza poliza del certificado
	 * @param coceNuCertificado numero de certificado
	 * @return numero de movimiento
	 * @throws Excepciones error de proceso
	 */
	Long generaMovimiento(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza, Long coceNuCertificado) throws Excepciones;

	/**
	 * Metodo que actualiza datos en Cart_Clientes
	 * @param seleccionado datos a actualizar
	 * @param arrResultado datos actuales
	 * @throws Excepciones error de proceso
	 */
	void actualizaCliente(EndosoDatosDTO seleccionado, Object[] arrResultado) throws Excepciones;
	
	/**
	 * Metodo que actualiza datos en Cart_Certificados
	 * @param seleccionado datos a actualizar
	 * @param arrResultado datos actuales
	 * @throws Excepciones error de proceso
	 */
	void actualizaCertificado(EndosoDatosDTO seleccionado, Object[] arrResultado) throws Excepciones;
	
	/**
	 * Metodo para actualizar el RFC por un endoso de Fecha de nacimiento
	 * 
	 * **/
      void updateEndosoFecha(BigDecimal idcli) throws Excepciones;


}
