/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;


/**
 * @author Sergio Plata
 *
 */
public interface UsuarioDao extends CatalogoDao{
	
	public abstract List<Object> consultarMapeo(String strQuery) throws Exception;
	
}
