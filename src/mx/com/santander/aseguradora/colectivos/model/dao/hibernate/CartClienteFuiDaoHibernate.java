package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;



import java.io.Serializable;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.CartClienteFuiDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class CartClienteFuiDaoHibernate extends HibernateDaoSupport implements  CartClienteFuiDao {
	
	@Autowired
	public CartClienteFuiDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			System.out.println("Entramos a guardarObjeto del CartClienteFuiDaoHibernate ");
			System.out.println("objeto = " + objeto.toString());
			this.getHibernateTemplate().save(objeto);
			
			return objeto;
		} catch (DataIntegrityViolationException e) {
			System.out.println("Exception 1 CartClienteFuiDaoHibernate");
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			System.out.println("Exception 2 CartClienteFuiDaoHibernate");
			e.printStackTrace();
			throw new Excepciones("Error.", e);
			
		}
		
	}
	
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}
	
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}
	
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}
	
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		return null;
	}
	
	public Integer guardaRepresentante(Short ramo) throws Excepciones {
		return null;
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

}
