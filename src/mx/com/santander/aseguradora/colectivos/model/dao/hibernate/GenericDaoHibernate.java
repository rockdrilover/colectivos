package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import mx.com.santander.aseguradora.colectivos.model.dao.GenericDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		27-07-2020
 * Description: Clase de tipo DAO que tiene acceso a base de datos para 
 * 				el procesos genericos
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Repository
public class GenericDaoHibernate extends HibernateDaoSupport implements GenericDao {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor de clase
	 * @param sessionFactory session factory para conexion a DB
	 */
	@Autowired
	public GenericDaoHibernate(SessionFactory sessionFactory) {
		//Se inicializa session factory
		super.setSessionFactory(sessionFactory);
	}

	/**
	 * Metodo que ejecuta consulta SQL 
	 * @param sbConsulta consulta a ejecutar
	 * @return lista de resultados
	 * @throws Excepciones con error generar
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> consultaSQL(StringBuilder sbConsulta) throws Excepciones {
		try {
			return this.getSession().createSQLQuery(sbConsulta.toString()).list();
		} catch (HibernateException e) {
			throw new Excepciones("Error: GenericDao.consultaSQL()" + e);
		}
	}
	
	/**
	 * Metodo que consulta objetos de base de datos 
	 * @param <T> tipo de objeto a conultar
	 * @param filtro para aplicar en la consulta
	 * @param nTabla para saber de que tabla consultar
	 * @return lista de resultados
	 * @throws Excepciones con error generar
	 */
	@SuppressWarnings("unchecked")
	@Override	
	public <T> List<T> obtenerObjetos(final String filtro, final Integer nTabla) throws Excepciones {
		try {
			//Ocupa Hibernate Template para realizar la buqueda
			return this.getHibernateTemplate().executeFind(new HibernateCallback<Object>() {
				public Object doInHibernate(Session sesion) throws HibernateException, SQLException {
					//Creamos consulta
					StringBuilder consulta = getConsulta(filtro, nTabla);
					
					//Agrega consulta a Session
					Query qry = sesion.createQuery(consulta.toString());
					
					//Ejecuta consulta
					return  qry.list();
				}
				
			});
		} catch (HibernateException e) {
			throw new Excepciones("Error en GenericDao.obtenerObjetos: ", e);
		}
	}

	/**
	 * Metodo que genera el filtro para la bsuqueda de objetos
	 * @param filtro filtro a ejecutar
	 * @param nTabla tabla a la que se desea hacer filtro
	 * @return consulta para ejecutar
	 */
	protected StringBuilder getConsulta(String filtro, Integer nTabla) {
		StringBuilder sbConsulta = new StringBuilder();
		
		switch (nTabla) {
			case Constantes.TABLA_CIFRAS_CONTROL:
				//Consulta para tabla Cifras Control
				sbConsulta.append("from CifrasControl  E    \n");
				break;

			case Constantes.TABLA_BITACORA_ERRORES:
				//Consulta para tabla Bitacora Erres
				sbConsulta.append("from BitacoraErrores  B    \n");
				break;
			
			case Constantes.TABLA_PRECARGA:
				//Consulta para tabla Bitacora Erres
				sbConsulta.append("from PreCarga  P    \n");
				break;
				
			case Constantes.TABLA_CARGA:
				//Consulta para tabla Bitacora Erres
				sbConsulta.append("from Carga  C    \n");
				break;
				
			default:
				//Consulta para tabla Cedula Control
				sbConsulta.append("from ColectivosCedula  CC \n");
				break;
		}
		
		//Se complemente filtro
		sbConsulta.append("where 1=1  \n");
		sbConsulta.append(filtro);
		
		return sbConsulta;
	}
	
	/**
	 * Metodo para realizar update a la base de datos
	 * @param sbQuery query a ejecutar
	 * @throws Excepciones con errores en general
	 */
	@Override
	public void updateSinParametros(StringBuilder sbQuery) throws Excepciones {
		Query query;
		
		try {
			query = getSession().createSQLQuery(sbQuery.toString()); 
			query.executeUpdate();
		} catch (HibernateException e) {
			throw new Excepciones("Error: GenericDao.updateSinParametros()" + e);
		}
	}
}
