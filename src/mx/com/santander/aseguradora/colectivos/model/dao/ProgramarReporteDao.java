package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

public interface ProgramarReporteDao {
	/*public String consultaMensualEmision(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta
            , boolean chk1, boolean chk2, boolean chk3, String rutaReportes, String usuario) throws Excepciones;
	public String consultaMensualEmision5758(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta
            , boolean chk1, boolean chk2, boolean chk3, String rutaReportes, String usuario) throws Excepciones;*/
	public String consultaMensualEmision5758(List<Object> listaReporte) throws Excepciones;

	public String consultaMensualEmision(List<Object> listaReporte) throws Excepciones;
}
  