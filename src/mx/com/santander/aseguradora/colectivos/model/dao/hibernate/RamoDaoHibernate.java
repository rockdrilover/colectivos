/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.RamoDao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * @author Sergio Plata
 *
 */
@Repository
public class RamoDaoHibernate extends HibernateDaoSupport implements RamoDao {

	/**
	 * Anotacion que le indica a spring que hay que inyectarle el sessionFactory
	 */
	@Autowired
	public RamoDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	public <T> void actualizarObjeto(T objeto) {
		// TODO Auto-generated method stub
		
	}
	
	public <T> void borrarObjeto(T objeto) {
		// TODO Auto-generated method stub
		
	}
	
	public <T> T guardarObjeto(T objeto) {
		// TODO Auto-generated method stub
		return null;
	}
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) {
		// TODO Auto-generated method stub
		return (T) this.getHibernateTemplate().load(objeto, id);
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) {
		// TODO Auto-generated method stub
		final List<T> lista = this.getHibernateTemplate().loadAll(objeto);
		return lista;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) {
		// TODO Auto-generated method stub
		
		return getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuffer sb = new StringBuffer(100);
				sb.append("from Ramo R\n");
				sb.append("where 1 = 1\n");
				sb.append(filtro);
				sb.append("order by R.carpCdRamo");
				
				Query query = sesion.createQuery(sb.toString());
				
				List<T> lista = query.list();
												
				return lista;
			}
			
		});
	}
	public <T> void guardarObjetos(List<T> lista) {
		// TODO Auto-generated method stub
		
	}

}
