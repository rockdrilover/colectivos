/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * 
 * @author Ing. Hery Tyler
 *
 */
public interface TarifasDao extends CatalogoDao{

	public List<Object> getConsultaTarifas(Integer canal, Short ramo, Integer producto)throws Excepciones;
	public List<Object> getConsultaCoberturas(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones;
	public List<Object> getConsultaComponentes(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones;
	public List<Object> getConsultaHomologa(Short ramo, Integer producto, Short plan) throws Excepciones;
	public void reporteDetalleTarifas(String rutaTemporal, String rutaReporte,Map<String, Object> inParams)throws Excepciones;
	
}
