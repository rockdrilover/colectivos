package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.hibernate.Query;
import org.hibernate.SessionFactory;


@Repository
public class ProgramaReporteDaoHibernate extends HibernateDaoSupport{
	
	@Autowired
	public ProgramaReporteDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}


	public String RecibirQueryReporteTecnico(String query)throws Excepciones {
		 
	 String nulo = null;
/*	 String query_reemplazado = null;*/
	    	try{
	    		if(query==null||query.isEmpty()|| query.equalsIgnoreCase(nulo) || query.equals(nulo)){	    			 
	    			Log.info("Query no esta inicializado (referencia null) o esta vacio");
	    			query = "Error";
	    		}
	    		else{
                    /*query_reemplazado = query.replace('\'','\"');*/
	    			Log.info("Query recibido correctamente");
	    			/*System.out.println(query_reemplazado);*/
	    		}
	    		return query;
	    	} catch (Exception e) {
	    		Log.info("No se recibio query correctamente",e);
				throw new Excepciones("Error.", e);
			}
	    }
	
	/**
	 * Metodo que inserta peticion para reportes batch
	 * @param reporte query a ejecutar
	 * @param nombre nombre del reporte
	 * @param usuario usuario que da de alta la peticion
	 * @param fechainicio fecha de guardado
	 * @param columnas nombre de columnas
	 * @throws Excepciones con error en general
	 */
	public void insertarQueryReporteTecnico(String reporte, String nombre, String usuario, String fechainicio, String columnas) throws Excepciones {
		StringBuilder sbquery = new StringBuilder();                                                            
		Query query;  
		
		Log.info("Reporte: "+nombre);
		try {	
			
			sbquery.append("INSERT INTO GIOSEG.COLECTIVOS_REP_TECNICOS \n");                 			
			sbquery.append(" (REPORTE , \n");                 
			sbquery.append("  NOMBRE, \n");                 
			sbquery.append("  ESTATUS,  \n");
			sbquery.append("  DESC_ESTATUS,  \n");	
			sbquery.append("  USUARIO,  \n");	
			sbquery.append("  FECHA_INICIO)  \n");
			sbquery.append("VALUES ("); 
			sbquery.append(":lob").append(",");          
			sbquery.append("'" + nombre).append("' ,"); 
			sbquery.append("'EN PROCESO'").append(",");
			sbquery.append("'" + columnas).append("' ,");
			sbquery.append("'" + usuario).append("' ,");
			sbquery.append("TO_DATE('"+fechainicio+"', 'dd/mm/yyyy hh24:mi:ss'))"); 

			                                       		
            query = getSession().createSQLQuery(sbquery.toString());
			query.setParameter("lob", reporte);
			query.executeUpdate();          			          

			Log.info("Sea a programado el reporte con exito");
		} catch (Exception e) {
			Log.info("Error al insertar el nombre del reporte",e);
		}
	}
}
