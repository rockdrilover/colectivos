package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSeguimiento;

import mx.com.santander.aseguradora.colectivos.model.dao.SeguimientoDAO;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class SeguimientoDAOHibernate extends HibernateDaoSupport implements SeguimientoDAO {

	/**
	 * Constructor de sesion hibernate.
	 * 
	 * @param sessionFactory
	 *            Session factory hibernate
	 */
	@Autowired
	public SeguimientoDAOHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {

		return (T) this.getHibernateTemplate().get(ColectivosSeguimiento.class, id);
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.getHibernateTemplate().update(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);

		}

	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> consultaGrupoUsuarios(String usuario) throws Excepciones {
		StringBuilder consulta = new StringBuilder();

		consulta.append("select grupo from CAT_USUARIO_GRUPO where username = '" + usuario + "'");

		Query qrySQL = this.getSession().createSQLQuery(consulta.toString());
		List<Object> lista;
		lista = qrySQL.list();
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ColectivosSeguimiento> consultaSeguimientoSuscripcion(ColectivosSeguimiento filtro) throws Excepciones {

		List<ColectivosSeguimiento> res = null;
		StringBuilder consulta = new StringBuilder();

	

		List<Object> lista;
		lista = consultaGrupoUsuarios(filtro.getCoseArea());
		Iterator<Object> it;
		BigDecimal g;
		it = lista.iterator();
		StringBuilder grupos = new StringBuilder();
		while (it.hasNext()) {
			g = (BigDecimal) it.next();
			grupos.append(g.toString()).append(",");
		}

		grupos.delete(grupos.length() - 1, grupos.length());

		consulta = new StringBuilder();
		consulta.append("from ColectivosSeguimiento sus \n");
		consulta.append("where sus.colectivosSuscripcion.cosuId = :cosuId");
		consulta.append(" and sus.coseGrupo in ( :grupos )");
		consulta.append(" order by sus.coseFechaAsignacion desc");
		
		try {
			Query qry = this.getSession().createQuery(consulta.toString());
			qry.setParameter("cosuId", filtro.getColectivosSuscripcion().getCosuId());
			qry.setParameter("grupos", grupos.toString());
			
			res = (List<ColectivosSeguimiento>) qry.list();

		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error consultar seguimiento por suscripcion", e);
		}

		return res;
	}

	@Override
	public long consecutivoSeguimiento(ColectivosSeguimiento filtro) throws Excepciones {
		StringBuilder sbConsulta = new StringBuilder();
		Query qry;

		sbConsulta.append("SELECT NVL(MAX(COSE_CONSECUTIVO),0) + 1 \n");
		sbConsulta.append("	FROM COLECTIVOS_SEGUIMIENTO SEG \n");
		sbConsulta.append("WHERE COSE_COSU_ID 	= :cosuId \n");
		sbConsulta.append("and COSE_GRUPO = :coseGrupo  \n");

		try {
			qry = this.getSession().createSQLQuery(sbConsulta.toString());
			qry.setParameter("cosuId", filtro.getColectivosSuscripcion().getCosuId());
			qry.setParameter("coseGrupo", filtro.getCoseGrupo());
			BigDecimal res = (BigDecimal) qry.uniqueResult();
			return res.longValue();
		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw new Excepciones("Error consulta consecutivo seguimiento", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.SeguimientoDAO#
	 * guardarSeguimiento(mx.com.santander.aseguradora.colectivos.model.
	 * bussinessObject.ColectivosSeguimiento)
	 */
	@Override
	public ColectivosSeguimiento guardarSeguimiento(ColectivosSeguimiento guardar) throws Excepciones {
		try {
			guardar.setCoseConsecutivo(new BigDecimal(consecutivoSeguimiento(guardar)));
			this.getHibernateTemplate().save(guardar);
			return guardar;
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new ObjetoDuplicado("Error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}

	@Override
	public ColectivosSeguimiento obtieneObjeto(ColectivosSeguimiento filtro) {

		return this.getHibernateTemplate().get(ColectivosSeguimiento.class, filtro.getCoseId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.SeguimientoDAO#
	 * actualizarSeguimiento(mx.com.santander.aseguradora.colectivos.model.
	 * bussinessObject.ColectivosSeguimiento)
	 */
	@Override
	public void actualizarSeguimiento(ColectivosSeguimiento guardar) throws Excepciones {
		try {
			this.getHibernateTemplate().update(guardar);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Excepciones("Error.", e);

		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.SeguimientoDAO#consultaSeguimientoPrevio(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSeguimiento)
	 */
	@Override
	public ColectivosSeguimiento consultaSeguimientoPrevio(ColectivosSeguimiento filtro)  throws Excepciones {
		ColectivosSeguimiento res = null;
		StringBuilder consulta = new StringBuilder();

		consulta.append("from ColectivosSeguimiento  ");
		consulta.append(" where coseGrupo = :pcoseGrupo and coseEstatus = :pcoseEstatus and  colectivosSuscripcion.cosuId = :pcosuId ");
		
		Query qry = this.getSession().createQuery(consulta.toString());
		
		qry.setParameter("pcoseGrupo", filtro.getCoseGrupo());
		qry.setParameter("pcoseEstatus", filtro.getCoseEstatus());
		qry.setParameter("pcosuId", filtro.getColectivosSuscripcion().getCosuId());
		res = (ColectivosSeguimiento) qry.uniqueResult();
		return res;
	}
	
	

}
