package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSuscripcion;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Grupo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author CJPV - vectormx
 * @version 06-09-2017
 */
public interface SuscripcionDAO extends CatalogoDao {
	/**
	 * Obtiene Suscripcion con filtro de folio riesgos
	 * 
	 * @param filtro
	 *            parametros filtro
	 * @return Suscripcion
	 */
	ColectivosSuscripcion obtieneSuscripcionFolioRiesgos(ColectivosSuscripcion filtro)
			throws Excepciones;
	
	/**
	 * Consulta suscripcion detalle.
	 * @param id id a consultar
	 * @return Detalle suscripcion
	 * @throws Excepciones Error
	 */
	ColectivosSuscripcion consultaObjeto(long id) throws Excepciones;
	
	/**
	 * Conulta principal pantalla suscripciones
	 * @param filtro Parametros consulta
	 * @return Lista de suscripciones
	 * @throws Excepciones Excepcion al ejecutar consulta
	 */
	List<ColectivosSuscripcion> consultaSuscripciones(ColectivosSuscripcion filtro)
	throws Excepciones;
	
	/**
	 * Consultar areas a asignar tarea.
	 * @return Lista de grupos posibles a asignar
	 */
	List<Grupo> gruposTareas()throws Excepciones;
	
	/**
	 * Parametros configuracion envio archivos
	 * @return Parametros configuracion
	 * @throws Excepciones
	 */
	Parametros archivoParametros() throws Excepciones;

	
	/**
	 * Tareas atendidas 
	 * @param ColectivosSuscripcion filtro a buscar
	 * @return tareas atendidas
	 * @throws Excepciones
	 */
	long tareaAtendida(ColectivosSuscripcion filtro) throws Excepciones;
}
