/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.FacturacionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanFacturacion;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Z014155
 * Modificacion: Sergio Plata
 *
 */
@Repository
public class FacturacionDaoHibernate extends HibernateDaoSupport implements
		FacturacionDao {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	public FacturacionDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().update(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
		
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS )
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			return this.getHibernateTemplate().get(objeto, id);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			List<T> lista = this.getHibernateTemplate().loadAll(objeto);
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuilder consulta = new StringBuilder(100);
					consulta.append("from Facturacion F        \n");
					consulta.append("where 1 = 1               \n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					
					List<T> lista = qry.list();
					
					return lista;
				}
				
				
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> facturacion(List <BeanFacturacion> lista) throws Excepciones {
		//System.out.println("Entro a facturacion --- DAO");
		List<T> listar=null;
		for(BeanFacturacion item: lista ){
			BeanFacturacion facturas = item;
			//System.out.println("Actualizando montos en colectivos_facturacion");
			if (facturas.getChkUpdate()==true) {
				StringBuilder validamontos = new StringBuilder() ;
				validamontos.append("select cf.cofa_mt_recibo \n");
				validamontos.append("from colectivos_facturacion cf \n");
				validamontos.append("where cf.cofa_casu_cd_sucursal = " + facturas.getId().getCofaCasuCdSucursal() +  "\n");
				validamontos.append("and   cf.cofa_carp_cd_ramo       = " + facturas.getId().getCofaCarpCdRamo() +  "\n");
				validamontos.append("and   cf.cofa_capo_nu_poliza     = " + facturas.getId().getCofaCapoNuPoliza() +  "\n");
				validamontos.append("and   cf.cofa_nu_recibo_fiscal    =0 \n");
				Query queryVal = getSession().createSQLQuery(validamontos.toString());
				listar = queryVal.list();
				BigDecimal obj = (BigDecimal)listar.get(0);
				String dato = obj.toString();
				//System.out.println("Valor --- " + dato + " ****************************************************************");
				double num = facturas.getCofaMtRecibo().doubleValue();
				//System.out.println("Valor --- " + num + " ****************************************************************");
				double num2 = Double.valueOf(dato).doubleValue();
				//System.out.println("Valor --- " + num2 + " ****************************************************************");
				double num3 = num - num2;
				if (num3 < 0)
					num3=num3*-1;
				//System.out.println("Valor --- " + num3 + " ****************************************************************");
				if (num3 <= 1000){
					log.info("Entro a Actualizar los montos en colectivos_facturacion");
					StringBuilder actmontosf = new StringBuilder() ;
					actmontosf.append("update colectivos_facturacion cf \n");
					actmontosf.append("set cf.cofa_mt_recibo           	=" + facturas.getCofaMtRecibo() + "\n");
					actmontosf.append(",cf.cofa_total_certificados  		=" + facturas.getCofaTotalCertificados() + "\n");
					actmontosf.append(",cf.cofa_mt_tot_sua          		=" + facturas.getCofaMtTotSua() + "\n");
					actmontosf.append(",cf.cofa_mt_tot_pma          		=" + facturas.getCofaMtTotPma() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_iva          		=" + facturas.getCofaMtTotIva() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_dpo          		=" + facturas.getCofaMtTotDpo() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_rfi          		=" + facturas.getCofaMtTotRfi() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_com          		=" + facturas.getCofaMtTotCom() + "\n");				
					actmontosf.append("where cf.cofa_casu_cd_sucursal = " + facturas.getId().getCofaCasuCdSucursal() +  "\n");
					actmontosf.append("and   cf.cofa_carp_cd_ramo       = " + facturas.getId().getCofaCarpCdRamo() +  "\n");
					actmontosf.append("and   cf.cofa_capo_nu_poliza     = " + facturas.getId().getCofaCapoNuPoliza() +  "\n");
					actmontosf.append("and   cf.cofa_nu_recibo_fiscal    =0 \n");
					Query queryAct = getSession().createSQLQuery(actmontosf.toString());
					queryAct.executeUpdate();
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> getModificaMontosFac (List <BeanCertificado> lista) throws Excepciones {
		List<T> listaf=null;
		String polizas = "";
		int num=1;
		int suc=0;
		int ramo=0;
		try {
			for(BeanCertificado item: lista ){
				BeanCertificado certificado = item;
				if (certificado.getChkUpdate()==true) {

					StringBuilder actmontos = new StringBuilder() ;
					actmontos.append("update colectivos_facturacion fact \n");
					actmontos.append("set fact.cofa_campov2 = 'PMAT'||fact.cofa_mt_recibo||'|'||'CERTS'||fact.cofa_total_certificados||'|'||'SUM'||fact.cofa_mt_tot_sua||'|'||'PMAP'||fact.cofa_mt_tot_pma||'|'||'IVA'||fact.cofa_mt_tot_iva||'|'||'DPO'||fact.cofa_mt_tot_dpo||'|'||'RFI'||fact.cofa_mt_tot_rfi||'|'||'COM'||fact.cofa_mt_tot_com    \n");
					actmontos.append("where fact.cofa_casu_cd_sucursal = " + certificado.getId().getCoceCasuCdSucursal() +  "\n");
					actmontos.append("and   fact.cofa_carp_cd_ramo       = " + certificado.getId().getCoceCarpCdRamo() +  "\n");
					actmontos.append("and   fact.cofa_capo_nu_poliza     = " + certificado.getId().getCoceCapoNuPoliza() +  "\n");
					actmontos.append("and   fact.cofa_nu_recibo_fiscal   = 0 \n");
					//System.out.println("Actualizando montos");
					Query queryAct = getSession().createSQLQuery(actmontos.toString());
					queryAct.executeUpdate();
					if (num==1)
						polizas=polizas+certificado.getId().getCoceCapoNuPoliza();
					else
						polizas=polizas + "," + certificado.getId().getCoceCapoNuPoliza();
					num=num+1;
					suc=certificado.getId().getCoceCasuCdSucursal();
					ramo=certificado.getId().getCoceCarpCdRamo();
				}
			}
			
			if(Constantes.DEFAULT_STRING.equals(polizas)){
				polizas="0";
			}
			
			StringBuilder consulta = new StringBuilder() ;		
			consulta.append("select b.cofa_casu_cd_sucursal, b.cofa_carp_cd_ramo, b.cofa_capo_nu_poliza,\n");
			consulta.append("b.cofa_nu_certificado, b.cofa_mt_recibo, b.cofa_total_certificados,\n");
			consulta.append("b.cofa_mt_tot_sua, b.cofa_mt_tot_pma, b.cofa_mt_tot_iva, b.cofa_mt_tot_dpo,\n");
			consulta.append("b.cofa_mt_tot_rfi, b.cofa_mt_tot_com, b.cofa_mt_tot_com1, to_char(b.cofa_nu_recibo_col), b.cofa_nu_recibo_fiscal \n");
			consulta.append("from colectivos_facturacion b \n");
			consulta.append("where b.cofa_casu_cd_sucursal = " + suc + " \n");
			consulta.append("and b.cofa_carp_cd_ramo       = " + ramo + " \n");
			consulta.append("and b.cofa_capo_nu_poliza     in (" + polizas + ") \n");
			consulta.append("and b.cofa_nu_recibo_fiscal   >= 0	\n");
			consulta.append("and b.cofa_fe_facturacion    = trunc(sysdate)	\n");
			Query qry = getSession().createSQLQuery(consulta.toString());
	
	    	//System.out.println("Antes de llenar la lista");
			listaf = qry.list();
			//System.out.println("Traigo lista con informacion de los montos de la prefactura");		
				
		}
		catch (Exception e){
			//System.out.println("Error al actualizar montos o al obtener lista de prefacturadas -- FacturacionDAO");
			throw new Excepciones ("Error" ,e);
		}
		return listaf;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> recuperaRecibos (int sucursal, int ramo, String polizas) throws Excepciones {
		List<T> listaR=null;
		try {
			if(polizas==""){
				polizas="0";
			}
			StringBuilder consulta = new StringBuilder() ;		
			consulta.append("select b.cofa_casu_cd_sucursal, b.cofa_carp_cd_ramo, b.cofa_capo_nu_poliza, b.cofa_nu_recibo_fiscal \n");
			consulta.append("from colectivos_facturacion b \n");
			consulta.append("where b.cofa_casu_cd_sucursal = " + sucursal + " \n");
			consulta.append("and b.cofa_carp_cd_ramo       = " + ramo + " \n");
			consulta.append("and b.cofa_capo_nu_poliza     in (" + polizas + ") \n");
			consulta.append("and b.cofa_fe_facturacion    = trunc(sysdate)	\n");
			consulta.append("and b.cofa_nu_recibo_fiscal    <> 0 \n");
			consulta.append("and b.cofa_nu_trasfer IS NULL \n");
			Query qry = getSession().createSQLQuery(consulta.toString());
	
	    	listaR = qry.list();	
	}
	catch (Exception e){
		//System.out.println("Error al actualizar montos o al obtener lista de prefacturadas -- FacturacionDAO");
		throw new Excepciones ("Error" ,e);
		}
		return listaR;
	}

	public void actRecFiscal(Short canal, byte ramo, Long poliza, String reccol, int num) throws Excepciones {
		//System.out.println("Entro a facturacion --- DAO");
		StringBuilder actmontosf = new StringBuilder() ;
		actmontosf.append("update colectivos_facturacion cf \n");
		if(num==1)
			actmontosf.append("set cf.cofa_nu_recibo_fiscal = -1 \n");
		if(num==2)
			actmontosf.append("set cf.cofa_nu_recibo_fiscal = 0 \n");
		actmontosf.append("where cf.cofa_casu_cd_sucursal = " + canal +  "\n");
		actmontosf.append("and   cf.cofa_carp_cd_ramo       = " + ramo +  "\n");
		actmontosf.append("and   cf.cofa_capo_nu_poliza     = " + poliza +  "\n");
		actmontosf.append("and   cf.cofa_nu_recibo_col    <>" + reccol +  "\n");
		if(num==1)
			actmontosf.append("and   cf.cofa_nu_recibo_fiscal  = 0\n");
		if(num==2)
			actmontosf.append("and   cf.cofa_nu_recibo_fiscal  = -1\n");
		Query queryAct = getSession().createSQLQuery(actmontosf.toString());
		queryAct.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> guardaMontosNuevosPrefactura(List <BeanFacturacion> lista) throws Excepciones {
		//System.out.println("Entro a facturacion --- DAO");
		List<T> listar=null;
		for(BeanFacturacion item: lista ){
			BeanFacturacion facturas = item;
			//System.out.println("Actualizando montos en colectivos_facturacion");
			if (facturas.getChkUpdate()==true) {
				StringBuilder validamontos = new StringBuilder() ;
				validamontos.append("select cf.cofa_mt_recibo, cp.copa_vvalor4 \n"); 
				validamontos.append("from colectivos_facturacion cf, \n");
				validamontos.append("	  colectivos_parametros  cp  \n");
				validamontos.append("where cf.cofa_casu_cd_sucursal  = " + facturas.getId().getCofaCasuCdSucursal() +  "\n");
				validamontos.append("  and cf.cofa_carp_cd_ramo      = " + facturas.getId().getCofaCarpCdRamo() +  "\n");
				validamontos.append("  and cf.cofa_capo_nu_poliza    = " + facturas.getId().getCofaCapoNuPoliza() +  "\n");
				validamontos.append("  and cf.cofa_nu_recibo_col	 = " + facturas.getCofaCampov1() + "\n");
				validamontos.append("  and cf.cofa_nu_recibo_fiscal  = 0 \n");
				validamontos.append("  and cp.copa_nvalor1           = cf.cofa_casu_cd_sucursal \n");
				validamontos.append("  and cp.copa_nvalor2           = cf.cofa_carp_cd_ramo \n");
				validamontos.append("  and cp.copa_nvalor3           = decode(cp.copa_nvalor4, 0, cf.cofa_capo_nu_poliza, 1, substr(cf.cofa_capo_nu_poliza,0,3), substr(cf.cofa_capo_nu_poliza,0,2)) \n");
				validamontos.append("  and cp.copa_des_parametro      = 'POLIZA' \n");
				validamontos.append("  and cp.copa_id_parametro       = 'GEPREFAC' \n");			
				Query queryVal = getSession().createSQLQuery(validamontos.toString());
				listar = queryVal.list();
				Object[] x = (Object[]) listar.get(0);
				
				String dato = x[0].toString();
				String dato1 = x[1].toString();
				double num = facturas.getCofaMtRecibo().doubleValue();
				//System.out.println("Valor --- " + num + " ****************************************************************");
				double num2 = Double.valueOf(dato).doubleValue();
				double num3 = Double.valueOf(dato1).doubleValue();
				//System.out.println("Valor --- " + num2 + " ****************************************************************");
				double num4 = num - num2;
				if (num3 < 0)
					num4=num4*-1;
				//System.out.println("Valor --- " + num3 + " ****************************************************************");
				if (num4 <= num3){
					log.info("Entro a Actualizar los montos en colectivos_facturacion");
					StringBuilder actmontosf = new StringBuilder() ;
					actmontosf.append("update colectivos_facturacion cf \n");
					actmontosf.append("set cf.cofa_mt_recibo           	=" + facturas.getCofaMtRecibo() + "\n");
					actmontosf.append(",cf.cofa_total_certificados  		=" + facturas.getCofaTotalCertificados() + "\n");
					actmontosf.append(",cf.cofa_mt_tot_sua          		=" + facturas.getCofaMtTotSua() + "\n");
					actmontosf.append(",cf.cofa_mt_tot_pma          		=" + facturas.getCofaMtTotPma() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_iva          		=" + facturas.getCofaMtTotIva() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_dpo          		=" + facturas.getCofaMtTotDpo() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_rfi          		=" + facturas.getCofaMtTotRfi() + "\n");				
					actmontosf.append(",cf.cofa_mt_tot_com          		=" + facturas.getCofaMtTotCom() + "\n");				
					actmontosf.append("where cf.cofa_casu_cd_sucursal = " + facturas.getId().getCofaCasuCdSucursal() +  "\n");
					actmontosf.append("and   cf.cofa_carp_cd_ramo       = " + facturas.getId().getCofaCarpCdRamo() +  "\n");
					actmontosf.append("and   cf.cofa_capo_nu_poliza     = " + facturas.getId().getCofaCapoNuPoliza() +  "\n");
					actmontosf.append("and   cf.cofa_nu_recibo_col	 	= " + facturas.getCofaCampov1() + "\n");
					actmontosf.append("and   cf.cofa_nu_recibo_fiscal    =0 \n");
					Query queryAct = getSession().createSQLQuery(actmontosf.toString());
					queryAct.executeUpdate();
				}
				else{
					log.info("Monto de Cambio MAYOR al especificado");
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> prefacturas (Short canal, Short ramo, Long poliza, Integer inIdVenta) throws Excepciones {
		List<T> listaf=null;
		Long lPoliza1 = null, lPoliza2 = null;
		
		try {
			StringBuilder consulta = new StringBuilder() ;		
			consulta.append("select b.cofa_casu_cd_sucursal, b.cofa_carp_cd_ramo, b.cofa_capo_nu_poliza,\n");
			consulta.append("b.cofa_nu_certificado, b.cofa_mt_recibo, b.cofa_total_certificados,\n");
			consulta.append("b.cofa_mt_tot_sua, b.cofa_mt_tot_pma, b.cofa_mt_tot_iva, b.cofa_mt_tot_dpo,\n");
			consulta.append("b.cofa_mt_tot_rfi, b.cofa_mt_tot_com, b.cofa_mt_tot_com1, to_char(b.cofa_nu_recibo_col), b.cofa_nu_recibo_fiscal \n");
			consulta.append("from colectivos_facturacion b \n");
			consulta.append("where b.cofa_casu_cd_sucursal 		= " + canal + " \n");
			consulta.append("and b.cofa_carp_cd_ramo       		= " + ramo + " \n");
			if(poliza > Constantes.DEFAULT_LONG) {
				consulta.append("   and b.cofa_Capo_Nu_Poliza  = " + poliza + " \n");
			} else {
				lPoliza1 = Long.valueOf(inIdVenta.toString()) * 100000000;
				lPoliza2 = lPoliza1 + 99999999;
				consulta.append("   and b.cofa_Capo_Nu_Poliza  between " + lPoliza1 + " and " + lPoliza2 + " \n");
			}
			consulta.append("and b.cofa_nu_recibo_fiscal   = 0	\n");
//			consulta.append("and b.cofa_fe_facturacion    = trunc(sysdate)	\n");
			Query qry = getSession().createSQLQuery(consulta.toString());
	
	    	listaf = qry.list();
		}
		catch (Exception e){
			throw new Excepciones ("Error" ,e);
		}
		return listaf;
	}

	@Override
	public void actualizaRecaPenCobro(int sucursal, int ramo, String polizas) throws Excepciones {
		StringBuilder sbUpdate = new StringBuilder();
		Query queryAct; 
		
		try {
			if(polizas==""){
				polizas="0";
			}
			
			sbUpdate.append("UPDATE COLECTIVOS_RECIBOS CR SET CR.CORE_ST_RECIBO = 1 \n");
			sbUpdate.append("WHERE (CR.CORE_CASU_CD_SUCURSAL, CR.CORE_NU_RECIBO) IN \n");  
			sbUpdate.append("   (SELECT CF.COFA_CASU_CD_SUCURSAL, CF.COFA_NU_RECIBO_FISCAL FROM COLECTIVOS_FACTURACION CF \n");
			sbUpdate.append("    WHERE CF.COFA_CASU_CD_SUCURSAL = " + sucursal + " \n");
			sbUpdate.append("        AND CF.COFA_CARP_CD_RAMO = " + ramo + " \n");
			sbUpdate.append("        AND CF.COFA_CAPO_NU_POLIZA IN(" + polizas + ") \n");
			sbUpdate.append("        AND CF.COFA_FE_FACTURACION = TRUNC(SYSDATE) \n");
			sbUpdate.append("		 AND CF.COFA_NU_RECIBO_FISCAL <> 0 \n");
			sbUpdate.append("		 AND CF.COFA_NU_TRASFER IS NULL ) \n");
			sbUpdate.append("	AND CR.CORE_NU_RECIBO <> ( SELECT MIN(CF1.COFA_NU_RECIBO_FISCAL) FROM COLECTIVOS_FACTURACION CF1 \n");
			sbUpdate.append("							 WHERE CF1.COFA_CASU_CD_SUCURSAL = " + sucursal + " \n");
			sbUpdate.append("								AND CF1.COFA_CARP_CD_RAMO = " + ramo + " \n");
			sbUpdate.append("								AND CF1.COFA_CAPO_NU_POLIZA IN(" + polizas + ") \n");
			sbUpdate.append("								AND CF1.COFA_FE_FACTURACION = TRUNC(SYSDATE) \n");
			sbUpdate.append("								AND CF1.COFA_NU_RECIBO_FISCAL <> 0 \n");
			sbUpdate.append("								AND CF1.COFA_NU_TRASFER IS NULL ) \n");
			
			queryAct = getSession().createSQLQuery(sbUpdate.toString());
			queryAct.executeUpdate();
		} catch (Exception e) {
			System.out.println("Error en METODO actualizaRecaPenCobro: ");
			throw new Excepciones ("Error" ,e);
		}
	}
	
    @SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> recuperaRecibos5758 () throws Excepciones {
		List<T> listaR=null;
		try {
			StringBuilder consulta = new StringBuilder() ;		
			consulta.append("select re.care_carp_cd_ramo Ramo" +
							" ,re.care_capo_nu_poliza Poliza" +
							" ,re.care_nu_recibo      Recibo" +
							" ,re.care_nu_consecutivo_cuota Consecutivo" +
							" ,trunc(re.care_fe_emision) FechaEmision" +
							" ,re.care_fe_desde Desde" +
							" ,re.care_fe_hasta Hasta" +
							" ,re.care_mt_prima_pura PrimaPura" +
							" ,re.care_mt_prima      PrimaTotal" +
							"  from cart_recibos re" +
							" where re.care_casu_cd_sucursal= 1" +
							"   and re.care_carp_cd_ramo      in (57,58)" +
							"   and re.care_fe_emision        > sysdate -1" +
							" order by 1,5,2");

			Query qry = getSession().createSQLQuery(consulta.toString());
	
	    	listaR = qry.list();	
	}
	catch (Exception e){
		System.out.println("Error al obtener lista de facturados ramo 57 y 58 --- FacturacionDAO");
		throw new Excepciones ("Error" ,e);
		}
		return listaR;
	}

}
