package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSeguimiento;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

public interface SeguimientoDAO extends CatalogoDao {

	/**
	 * Consulta seguimiento de suscripion
	 * 
	 * @param filtro
	 *            suscripcion a consultar
	 * @return Lista de seguimientos
	 */
	List<ColectivosSeguimiento> consultaSeguimientoSuscripcion(
			ColectivosSeguimiento filtro) throws Excepciones;
	
	/**
	 * Consecutivo seguimiento 
	 * @param filtro filtro a buscar
	 * @return consecutivo seguimiento
	 * @throws Excepciones
	 */
	long consecutivoSeguimiento(ColectivosSeguimiento filtro) throws Excepciones;
	
	/**
	 * Obtiene objeto
	 * @param filtro filtro a buscar
	 * @return objeto seguimiento
	 */
	ColectivosSeguimiento obtieneObjeto(ColectivosSeguimiento filtro);
	
	
	/**
	 * Guardar Seguimiento
	 * @param guardar
	 * @return
	 * @throws Excepciones
	 */
	ColectivosSeguimiento guardarSeguimiento(ColectivosSeguimiento guardar) throws Excepciones;
	
	void actualizarSeguimiento(ColectivosSeguimiento guardar) throws Excepciones;
	/**
	 * Consulta grupo de usuario
	 * @param usuario usuario a consultar
	 * @return Lista de claves de grupo de usuario
	 * @throws Excepciones Excepcion bd
	 */
	 List<Object> consultaGrupoUsuarios(String usuario) throws Excepciones;

	 /**
	  * Valida que no existan tareas pendientes
	  * @param filtro tarea a buscar
	  * @return Seguimiento pendiente si existe
	  */
	 ColectivosSeguimiento consultaSeguimientoPrevio(ColectivosSeguimiento filtro) throws Excepciones;
	
}
