/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;



import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ColectivosCoberturasDao extends CatalogoDao{
	
	/**
	 * Actualiza comision a 0
	 * @param colectivosCoberturas registro modificado
	 * @throws Excepciones Error al registrar 
	 */
	void actualizaProductoPlan(ColectivosCoberturas  colectivosCoberturas) throws Excepciones;
	
	/**
	 * Actualizar configuracion producto plan
	 * @param parametrosConfiguracion Configuracion
	 * @throws Excepciones Excepciones
	 */
	void actualizaConfiguracionProductoPlan(Parametros parametrosConfiguracion)throws Excepciones;
	
	/**
	 * Actualizar configuracion producto plan
	 * @param parametrosConfiguracion Configuracion
	 * @throws Excepciones Excepciones
	 */
	void actualizaConfiguracionCobertura(Parametros parametrosConfiguracion, String filtro)throws Excepciones;
}
