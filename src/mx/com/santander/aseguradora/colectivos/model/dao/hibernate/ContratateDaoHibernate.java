/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClientePK;
import mx.com.santander.aseguradora.colectivos.model.dao.ContratanteDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.OracleCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dflores
 *
 */
@Repository
public class ContratateDaoHibernate extends HibernateDaoSupport implements
		ContratanteDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6293763869890813234L;

	@Autowired
	public ContratateDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	public CartCliente getContratante(CartClientePK id) {
		// TODO Auto-generated method stub
		CartCliente contratante = (CartCliente) this.getHibernateTemplate().load(CartCliente.class, id);
		return contratante;
	}


	public List<CartCliente> getContratante() {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {

		try {
			this.getHibernateTemplate().merge(objeto);
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
		
	}


	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {

		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}


	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return Collections.emptyList();
		
	}


	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consulta2 (String strFiltro ) throws Excepciones {
			try {
				StringBuilder consulta = new StringBuilder() ;
				consulta.append("select c.cacn_cd_nacionalidad   NACIONALIDAD,     \n");
				consulta.append("       c.cacn_nu_cedula_rif     CEDULA,           \n");
				consulta.append("       c.cacn_nm_apellido_razon RAZONSOCIAL,      \n");
				consulta.append("       c.cacn_di_cobro1         DIRECCION,        \n");
				consulta.append("       c.cacn_zn_postal_cobro   CP,               \n");
				consulta.append("       c.cacn_rfc               RFC,              \n");
				consulta.append("       c.cacn_Fe_Nacimiento     FECHA_CONS,       \n");
				consulta.append("       cc.caci_de_ciudad        CIUDAD,           \n");
				consulta.append("       e.caes_de_estado         ESTADO,           \n");
				consulta.append("		c.CACN_CAZP_COLONIA_COB  COLONIA,		   \n");
				consulta.append("		c.CACN_CAES_CD_ESTADO_HAB CDESTADO,	   	   \n");
				consulta.append("		c.CACN_CACI_CD_CIUDAD_HAB CDCIUDAD	   	   \n");
				consulta.append("   from cart_clientes c ,                         \n");
				consulta.append("        cart_ciudades cc,                         \n");
				consulta.append("        cart_estados  e                           \n");
				consulta.append("  where c.cacn_caes_cd_estado_hab = cc.caci_caes_cd_estado   \n");
				consulta.append("    and c.cacn_caci_cd_ciudad_hab = cc.caci_cd_ciudad   \n");
				consulta.append("    and c.cacn_caes_cd_estado_hab = e.caes_cd_estado    \n");
				consulta.append(strFiltro);

				Query qry = this.getSession().createSQLQuery(consulta.toString());

				List<T> lista = qry.list();
				
				return lista;
				}	
				catch (Exception e){
					throw new Excepciones ("Error .",e);
			}
	    }

	public <T> List<T> consulta(String nacionalidad, String cedula,
			String razonSocial,String strFiltro ) throws Excepciones {
		try {
			StringBuilder consulta = new StringBuilder() ;
			consulta.append("select c.cacn_cd_nacionalidad   NACIONALIDAD,     \n");
			consulta.append("       c.cacn_nu_cedula_rif     CEDULA,           \n");
			consulta.append("       c.cacn_nm_apellido_razon RAZONSOCIAL,      \n");
			consulta.append("       c.cacn_di_cobro1         DIRECCION,        \n");
			consulta.append("       c.cacn_zn_postal_cobro   CP,               \n");
			consulta.append("       c.cacn_rfc               RFC,              \n");
			consulta.append("       to_char(c.cacn_Fe_Nacimiento,'dd/MM/yyyy') FECHA_CONS,   \n");
			consulta.append("       cc.caci_de_ciudad        CIUDAD,           \n");
			consulta.append("       e.caes_de_estado         ESTADO            \n");
			consulta.append("  from cart_clientes c ,                         \n");
			consulta.append("       cart_ciudades cc,                         \n");
			consulta.append("       cart_estados  e                           \n");
			consulta.append(" where c.cacn_caes_cd_estado_hab = cc.caci_caes_cd_estado   \n");
			consulta.append("   and c.cacn_caci_cd_ciudad_hab = cc.caci_cd_ciudad   \n");
			consulta.append("   and c.cacn_caes_cd_estado_hab = e.caes_cd_estado    \n");
			consulta.append(strFiltro);

			Query qry = this.getSession().createSQLQuery(consulta.toString());

			List<T> lista = qry.list();
			
			return lista;
		}
		catch (Exception e){
			throw new Excepciones ("Error .",e);
	}
  }

	public String guarda(Long next,String idParam, String desParam, String nacionalidad,
			String cedula, short canal, short ramo, Integer idVenta,
			Integer idProducto) throws Excepciones {
		// TODO Auto-generated method stub
			StringBuffer sbquery;                                                     
			Query query;  
			String res;
		try { 

			sbquery = new StringBuffer();
			sbquery.append("INSERT INTO COLECTIVOS_PARAMETROS \n");                 
			sbquery.append(" (COPA_CD_PARAMETRO ,             \n");                 
			sbquery.append("  COPA_DES_PARAMETRO,             \n");                 
			sbquery.append("  COPA_ID_PARAMETRO ,             \n");                 
			sbquery.append("  COPA_VVALOR1      ,             \n");                 
			sbquery.append("  COPA_VVALOR2      ,             \n");                 
			sbquery.append("  COPA_NVALOR1      ,             \n");                 
			sbquery.append("  COPA_NVALOR2      ,             \n");                 
			sbquery.append("  COPA_NVALOR3      ,             \n");                 
			sbquery.append("  COPA_NVALOR4      )             \n");                 
			sbquery.append("VALUES (                          \n");                 
			sbquery.append("  :cdParam,                       \n");                 
			sbquery.append("  :desParam,                      \n");                 
			sbquery.append("  :idParam ,                      \n");                 
			sbquery.append("  :nacionalidad, 				  \n");                         
			sbquery.append("  :cedula ,      				  \n");                         
			sbquery.append("  :canal ,       				  \n");                         
			sbquery.append("  :ramo,         				  \n");                         
			sbquery.append("  :idVenta,       				  \n");                       
			sbquery.append("  :idProducto )   				  \n");                       
                                                                            
			query = getSession().createSQLQuery(sbquery.toString());                     
			                                                                        
			query.setString("cdParam", next.toString());                                    
			query.setString("desParam", desParam);                                  
			query.setString("idParam", idParam);                                    
			query.setString("nacionalidad",nacionalidad);                           
			query.setString("cedula",cedula);                                 
			query.setLong("canal",canal);                                  
			query.setLong("ramo",ramo);                                   
			query.setLong("idVenta",idVenta);                                
			query.setLong("idProducto",idProducto);                             
							                                                                
			query.executeUpdate();                        
			res = "Parametro Guardado Correctamente";
			return null;    
		}catch (Exception e){
			throw new Excepciones("Error: Al insertar parametros ", e);   
	     }		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Integer nTipoConsulta, CartClientePK cartClientePK) throws Excepciones {
		final Map<String, Object> hmParams;
		final StringBuilder filtro = new StringBuilder();
		
		try {
			hmParams = new HashMap<String, Object>();
			filtro.append(" where P.id.cacnCdNacionalidad = :cdnac \n");
			
			if(nTipoConsulta.equals(Constantes.CLIENTE_CONSULTA_ALL)) {
				filtro.append(" and P.cacnTpCliente = :tpcli \n");
				filtro.append(" and P.cacnInEmpleado = :inemp \n");
				filtro.append(" and P.cacnInTratamientoEspecial = :intra \n");
				filtro.append(" and P.cacnCaaeCdActividad   = :cdact \n");
				
				hmParams.put("tpcli", "J");
				hmParams.put("inemp", "N");
				hmParams.put("intra", "N");
				hmParams.put("cdact", new BigDecimal(47));
			} else {
				filtro.append(" and P.id.cacnNuCedulaRif = :cedula \n");
					
				hmParams.put("cedula", cartClientePK.getCacnNuCedulaRif());
			}
				
			hmParams.put("cdnac", "J");
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion) throws SQLException {
					StringBuilder sb = new StringBuilder(150);
					sb.append("from CartCliente P	\n");
					sb.append(filtro);
					sb.append("order by P.id");
					
					String strSafeQuery = ESAPI.encoder().encodeForSQL(new OracleCodec(), sb.toString()).replace("''", "'");
									
					Query query = sesion.createQuery(strSafeQuery);
					
					for(Map.Entry<String, Object> entry : hmParams.entrySet()) {
						query.setParameter(entry.getKey(), entry.getValue());
					}
					
					List<T> lista = query.list();
					
					return lista;
				}
			});
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
}



