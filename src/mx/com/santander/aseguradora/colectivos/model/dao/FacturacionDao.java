/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanFacturacion;


/**
 * @author Z014155
 * Aqui se declaran los m�todos que se van a implmentar
 */


public interface FacturacionDao extends CatalogoDao{ 
	<T> List<T> getModificaMontosFac(List <BeanCertificado> lista) throws Excepciones;
	abstract <T> List<T> prefacturas (Short canal, Short ramo, Long poliza, Integer inIdVenta) throws Excepciones;
	<T> List<T> recuperaRecibos(int sucursal, int ramo, String polizas) throws Excepciones;
	<T> List<T> facturacion(List <BeanFacturacion> lista) throws Excepciones;	
	<T> List<T> guardaMontosNuevosPrefactura(List <BeanFacturacion> lista) throws Excepciones;
	void actRecFiscal(Short canal, byte ramo, Long poliza, String reccol, int num) throws Excepciones;
	<T> List<T> recuperaRecibos5758() throws Excepciones;
	void actualizaRecaPenCobro(int sucursal, int ramo, String polizas) throws Excepciones;
}
