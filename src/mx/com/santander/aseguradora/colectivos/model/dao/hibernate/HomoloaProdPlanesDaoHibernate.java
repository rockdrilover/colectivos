/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.model.dao.HomologacionProdPlanesDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author Ing. Issac 
 *
 */
@Repository
public class HomoloaProdPlanesDaoHibernate extends HibernateDaoSupport implements HomologacionProdPlanesDao {

	@Autowired
	public HomoloaProdPlanesDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public <T> void actualizarObjeto(T objeto) {
		this.getHibernateTemplate().update(objeto);
	}

	public <T> void borrarObjeto(T objeto) {
		this.getHibernateTemplate().delete(objeto);
	}

	public <T> T guardarObjeto(T objeto) {
		this.getHibernateTemplate().save(objeto);
		return objeto;
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) {
		return (T) this.getHibernateTemplate().get(objeto, id);
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) {
		final List<T> lista = this.getHibernateTemplate().loadAll(objeto);
		return lista;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto, String filtro) {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion) throws HibernateException, SQLException {
				StringBuffer consulta = new StringBuffer(150);
				consulta.append("from HomologaProductosPlanes HP \n");
				consulta.append("where 1=1 \n");
				consulta.append(filtro);
						
				Query qry = sesion.createQuery(consulta.toString());
				
				
				List<T> lista = qry.list();
				
				return lista;
			}
			
		});
	}

}
