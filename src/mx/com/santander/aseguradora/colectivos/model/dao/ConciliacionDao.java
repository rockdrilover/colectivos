package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.ArrayList;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
public interface ConciliacionDao extends CatalogoDao{
	
	public ArrayList<Object> consultar(String strQuery) throws Exception;

}
