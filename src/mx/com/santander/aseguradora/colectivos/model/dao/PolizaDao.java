/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author Sergio Plata
 *
 */
public interface PolizaDao extends CatalogoDao {
	public <T> List<T> consultaPolizasRenovacion() throws Excepciones;
}
