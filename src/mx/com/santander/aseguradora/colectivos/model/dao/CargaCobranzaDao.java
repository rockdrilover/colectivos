package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		03-11-2022
 * Description: Interface que implementa la clase CargaCobranzaDaoHibernate.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:		
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Local
public interface CargaCobranzaDao extends Serializable {
	
	/**
	 * Metodo que ejecuta proceso de cobranza
	 * @param sucursal canal seleccionado
	 * @param ramo ramo seleccionado
	 * @param poliza poliza seleccionado
	 * @param tipoCarga tipo de carga seleccionado
	 * @param archivo nombre archivo cargado
	 * @return numero de errores
	 * @throws Excepciones  con errores en general
	 */
	Integer cargaCobranza(Short sucursal, Short ramo, Integer poliza, Integer tipoCarga, String archivo) throws Excepciones;
	
	/**
	 * Metodo que consulta los errores de la carga
	 * @param canal canal seleccionado
	 * @param ramo ramo seleccionado
	 * @param poliza poliza seleccionado
	 * @param tipoCarga tipo de carga seleccionado
	 * @param nombreArchivo nombre archivo cargado
	 * @return lista de errores
	 * @throws Excepciones  con errores en general
	 */
	List<Object> erroresCargaCobranza(Short canal, Short ramo, Integer poliza, Integer tipoCarga, String nombreArchivo) throws Excepciones;

	/**
	 * Metodo que aplica cobranza al registro seleccionado
	 * @param coceCasuCdSucursal canal del recibo
	 * @param coceCarpCdRamo ramo del recibo
	 * @param coceCapoNuPoliza poliza del recibo
	 * @param coceNuCertificado certificado del recibo
	 * @param coceNuCredito numero de credito
	 * @param coceNoRecibo numero de recibo
	 * @throws Excepciones con error en general
	 */
	void actualizaReciboCobranza(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza, Long coceNuCertificado, String coceNuCredito, BigDecimal coceNoRecibo) throws Excepciones;

}
