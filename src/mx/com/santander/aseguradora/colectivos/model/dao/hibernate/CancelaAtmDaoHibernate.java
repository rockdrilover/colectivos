package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.dao.CancelaAtmDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCancelaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase de tipo DAO que tiene acceso a base de datos para 
 * 				el proceso de Cancelacion Automatica.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Repository
public class CancelaAtmDaoHibernate extends HibernateDaoSupport implements CancelaAtmDao {
	//Implementacion de serializable
	private static final long serialVersionUID = 298075158826438461L;
	
	//Constante para update de tabla
	private static final String UPTABPRE = "UPDATE COLECTIVOS_PRECARGA \n";
	
	/**
	 * Constructor de clase
	 * @param sessionFactory session factory para conexion a DB
	 */
	@Autowired
	public CancelaAtmDaoHibernate(SessionFactory sessionFactory) {
		//Se inicializa session factory
		super.setSessionFactory(sessionFactory);
	}

	/**
	 * Metodo que consulta el resumen de los creditos a cancelar
	 * @param nombreLayOut nombre de archivo
	 * @param feDesde fecha desde
	 * @param feHasta dehca hasta 
	 * @return lista de datos 
	 * @throws Excepciones con error en general
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getResumenCancela(String nombreLayOut, String feDesde, String feHasta) throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT COUNT(*), COPC_CD_SUCURSAL, COPC_CD_RAMO, COPC_NUM_POLIZA, COPC_DATO16 || ' - ' || COPC_DATO13, COPC_DATO25 || ' - ' || COPC_DATO14, COPC_DATO33 || ' - ' || COPC_DATO15, SUM(NVL(COPC_PRIMA, 0)), SUM(TO_NUMBER(NVL(COPC_NUM_CLIENTE, 0))), COPC_DATO11   \n");
				consulta.append("FROM COLECTIVOS_PRECARGA \n");
			consulta.append("WHERE COPC_FE_CARGA BETWEEN TO_DATE(:feDesde, 'dd/MM/yyyy') AND TO_DATE(:feHasta, 'dd/MM/yyyy') \n");
			
			if(!nombreLayOut.equals(Constantes.DEFAULT_STRING)) {
				consulta.append("AND UPPER(COPC_DATO11) like '%" + nombreLayOut.trim().toUpperCase() + "%' \n" );
			}
			
				consulta.append("AND COPC_CARGADA = '7' \n");
				consulta.append("AND COPC_TIPO_REGISTRO = 'CCM' \n");
			consulta.append("GROUP BY COPC_CD_SUCURSAL, COPC_CD_RAMO, COPC_NUM_POLIZA, COPC_DATO16, COPC_DATO13, COPC_DATO25, COPC_DATO14, COPC_DATO33, COPC_DATO15, COPC_DATO11 \n");
			consulta.append("UNION \n");
			consulta.append("SELECT COUNT(*), COPC_CD_SUCURSAL, COPC_CD_RAMO, COPC_NUM_POLIZA, 'ERR', 'ERRORES DE CARGA', '0', 0 ,0, COPC_DATO11  \n");
				consulta.append("FROM COLECTIVOS_PRECARGA \n");
			consulta.append("WHERE COPC_FE_CARGA BETWEEN TO_DATE(:feDesde1, 'dd/MM/yyyy') AND TO_DATE(:feHasta1, 'dd/MM/yyyy') \n");
			
			if(!nombreLayOut.equals(Constantes.DEFAULT_STRING)) {
				consulta.append("and UPPER(COPC_DATO11) like '%" + nombreLayOut.trim().toUpperCase() + "%' \n" );
			}
			
				consulta.append("AND COPC_CARGADA = '77' \n");
				consulta.append("AND COPC_TIPO_REGISTRO = 'CCM' \n");
			consulta.append("GROUP BY COPC_CD_SUCURSAL, COPC_CD_RAMO, COPC_NUM_POLIZA, COPC_DATO11 \n");
			consulta.append("ORDER BY COPC_DATO11, 2, 3, 4, 5 \n");
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se agregan parametros necesarios para realizar consulta
			query.setParameter("feDesde", feDesde);
			query.setParameter("feHasta", feHasta);
			query.setParameter("feDesde1", feDesde);			
			query.setParameter("feHasta1", feHasta);
			
			//se ejecuta la consulta
			return query.list();
		} catch (HibernateException e) {
			throw new Excepciones("Error en CancelaAtmDao.getResumenCancela(): ", e); 
		}
	}

	/**
	 * Metodo que actualiza creditos a cancelar
	 * @param seleccionado datos para hacer update
	 * @param lista creditos a cancelar
	 * @throws Excepciones con error general
	 */
	@Override
	public void cancelaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones {
		try {
			//se cancelan creditos de una lista
			this.cancelaCreditos(getSession().connection(), seleccionado, lista, 1); 
		} catch (Excepciones | SQLException e) {
			throw new Excepciones("Error en CancelaAtmDao.cancelarCreditos(): ", e);
		}
		
	}

	/**
	 * Metodo que elimina creditos por errores o que no se quieren cancelar
	 * @param seleccionado datos para hacer delete
	 * @param lista creditos a eliminar
	 * @throws Excepciones con error general
	 */
	@Override
	public void eliminaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones {
		try {
			//se valida que sean errores
			if("ERR".equals(seleccionado.getDescVenta())) {
				//se eliminan errores de una lista
				this.eliminaErrores(getSession().connection(), seleccionado, lista);
			} else {
				//se cancelan creditos de una lista
				this.cancelaCreditos(getSession().connection(), seleccionado, lista, 2);
			}
		} catch (Excepciones | SQLException e) {
			throw new Excepciones("Error en CancelaAtmDao.eliminaCreditos(): ", e);
		}
		
	}
	
	/**
	 * Metodo que actualiza creditos a cancelar
	 * @param conn conexion a la base de datos 
	 * @param seleccionado datos para hacer update
	 * @param lista creditos a cancelar
	 * @param nAccion accion a aplicar
	 * @throws Excepciones con error general
	 * @throws SQLException 
	 */
	private void cancelaCreditos(Connection conn, ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista, Integer nAccion) throws Excepciones, SQLException {
		HashMap<Integer, Object> hmValues;
		StringBuilder sbCommand;
		Iterator<DetalleCancelaDTO> it;
		DetalleCancelaDTO dto;
		PreparedStatement psCancelaAtm = null;
		
		try {
			sbCommand = new StringBuilder();
			
			if(nAccion == 1) { //Para Actualizar
				sbCommand.append(UPTABPRE);
				sbCommand.append("	SET COPC_CARGADA = '8', \n");
				sbCommand.append("		COPC_SUMA_ASEGURADA = COPC_PRIMA_REAL \n");
			} else { //Para Eliminar
				sbCommand.append(UPTABPRE);
				sbCommand.append("	SET COPC_CARGADA = '78', \n");
				sbCommand.append("		COPC_REGISTRO = 'No cancelado por decision de usuario.' \n");
			}
			
			//se inicializa el comando a ejecutar
			sbCommand.append("WHERE COPC_CD_SUCURSAL = ? \n");
			sbCommand.append(" 	AND COPC_CD_RAMO = ? \n");
			sbCommand.append(" 	AND COPC_NUM_POLIZA = ? \n");
			sbCommand.append(" 	AND COPC_TIPO_REGISTRO = 'CCM' \n");
			sbCommand.append(" 	AND COPC_CARGADA = '7' \n");
			sbCommand.append(" 	AND COPC_DATO11 = ? \n");
			sbCommand.append(" 	AND NVL(COPC_DATO16, 0) = ? \n");
			sbCommand.append(" 	AND COPC_DATO2 = ? \n");
			sbCommand.append(" 	AND COPC_ID_CERTIFICADO = ? \n");
			
			//se prepara sentencia a ejecutar
			psCancelaAtm = conn.prepareStatement(sbCommand.toString());
			
			//se va agregando cada registro al batch
			it = lista.iterator();
			while(it.hasNext()) {
				dto = it.next();
				hmValues = new HashMap<Integer, Object>();
				hmValues.put(1, seleccionado.getCdSucursal());
				hmValues.put(2, seleccionado.getCdRamo());
				hmValues.put(3, seleccionado.getNuPoliza());
				hmValues.put(4, seleccionado.getArchivo());
				hmValues.put(5, seleccionado.getDescVenta().split("\\-")[0].trim());
				hmValues.put(6, dto.getCertificado());
				hmValues.put(7, dto.getCredito());
				
				for(Map.Entry<Integer, Object> entry : hmValues.entrySet()) {
					psCancelaAtm.setObject(entry.getKey(), entry.getValue());
				}
				psCancelaAtm.addBatch();
			}
			
			//ya que se agregaron todos se mandaa ejecutar la sentencia en batch
			psCancelaAtm.executeBatch();
		} catch (SQLException e) {
			throw new Excepciones("Error en CancelaAtmDao.cancelaCreditos(), " + e.getMessage(), e);
		} finally {
			if(psCancelaAtm != null) {
				psCancelaAtm.close();
			}
		}
	}
	
	/**
	 * Metodo que actualiza creditos a cancelar
	 * @param conn conexion a la base de datos 
	 * @param seleccionado datos para hacer update
	 * @param lista creditos a cancelar
	 * @throws Excepciones con error general
	 * @throws SQLException 
	 */
	private void eliminaErrores(Connection conn, ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones, SQLException {
		HashMap<Integer, Object> hmValues;
		StringBuilder sbCommand;
		Iterator<DetalleCancelaDTO> it;
		DetalleCancelaDTO dto;
		PreparedStatement psCancelaAtm = null;
		
		try {
			
			//se inicializa el comando a ejecutar
			sbCommand = new StringBuilder();
			sbCommand.append(UPTABPRE);
			sbCommand.append("	SET COPC_CARGADA = '78' \n");
			sbCommand.append("WHERE COPC_CD_SUCURSAL = ? \n");
			sbCommand.append(" 	AND COPC_CD_RAMO = ? \n");
			sbCommand.append(" 	AND COPC_NUM_POLIZA = ? \n");
			sbCommand.append(" 	AND COPC_TIPO_REGISTRO = 'CCM' \n");
			sbCommand.append(" 	AND COPC_CARGADA = '77' \n");
			sbCommand.append(" 	AND COPC_DATO11 = ? \n");
			sbCommand.append(" 	AND COPC_ID_CERTIFICADO = ? \n");
			
			//se prepara sentencia a ejecutar
			psCancelaAtm = conn.prepareStatement(sbCommand.toString());
			
			//se va agregando cada registro al batch
			it = lista.iterator();
			while(it.hasNext()) {
				dto = it.next();
				hmValues = new HashMap<Integer, Object>();
				hmValues.put(1, seleccionado.getCdSucursal());
				hmValues.put(2, seleccionado.getCdRamo());
				hmValues.put(3, seleccionado.getNuPoliza());
				hmValues.put(4, seleccionado.getArchivo());
				hmValues.put(5, dto.getCredito());
				
				for(Map.Entry<Integer, Object> entry : hmValues.entrySet()) {
					psCancelaAtm.setObject(entry.getKey(), entry.getValue());
				}
				psCancelaAtm.addBatch();
			}
			
			//ya que se agregaron todos se mandaa ejecutar la sentencia en batch
			psCancelaAtm.executeBatch();
		} catch (SQLException e) {
			throw new Excepciones("Error en CancelaAtmDao.eliminaErrores(), " + e.getMessage(), e);
		} finally {
			if(psCancelaAtm != null) {
				psCancelaAtm.close();
			}
		}
		
	}
}
