/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;


/**
 * @author Sergio Plata
 *
 */
public interface CargaDao extends CatalogoDao{

	public abstract void borrarDatosTemporales(Short canal, Short ramo, Long poliza, Integer inIdVenta, String archivo, String origen)throws Excepciones;
	public abstract <T> List<T> estatusEmision(Short canal, Short ramo, Long poliza, Integer idVenta, String archivo)throws Excepciones;
	public abstract <T> List<T> estatusCancelacion(Short canal, Short ramo, Long poliza, String archivo)throws Excepciones;
	public <T> void actualizarObjeto(T objeto) throws Excepciones;
	public <T> void borrarObjeto(T objeto) throws Excepciones;
	public <T> T guardarObjeto(T objeto) throws Excepciones;
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones;
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones;
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones;
	public <T> void guardarObjetos(List<T> lista) throws Excepciones;
	public abstract List<Object> existeCargaConcilia(Short origen) throws Excepciones;
	public abstract Double getCumulo(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta) throws Excepciones;
	public abstract void reporteErrores(String rutaTemporal, String rutaReporte, Map<String, Object> inParams) throws Excepciones;
	public abstract void estatusCancelacion( List<BeanClienteCertif> listaCertificados, List<BeanClienteCertif> listaCertificadosCancelacion)throws Excepciones;
	public abstract Collection<? extends Object> consultarVentasConsumo(long tipoReg, int ramo, String strOrigen, String string) throws Exception;
	public abstract Collection<? extends Object> consultar(String strQuery) throws Exception;
	public abstract List<Object> consultarMapeo(String strQuery) throws Exception;
	public <T> void actualizarObjetos(List<T> lista) throws Excepciones;
	public ArrayList<Object> consultarRamos(int nDatosRamos) throws Exception;
	public abstract void divisionAutoCredito() throws Exception;
	public abstract void actualizarCargasManuales(String idCertificado, String canal, String ramo, String poliza, String sistemaOrigen) throws Exception;
	public void respaldarErrores(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta, String nombreArchivo) throws Exception;
	public abstract void actualizarCuentasLE(String strIdCertificado, String strFechaIngreso) throws Exception;
	public abstract void eliminarDatosReprocesoLE(String tipoError, String strFiltro) throws Exception;
	public abstract void updatePampa() throws Excepciones ;
	public abstract void buscarCuentaPampa(String[] strCuenta) throws Exception;
	public abstract void updatePampaCancelacion() throws Excepciones;
	public String getCodigoEstado(String strCP) throws Exception;
}
