/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ColectivosCoberturasDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;


import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Leobardo Preciado
 *
 */
@Repository
public class ColectivosCoberturasDaoHibernate extends HibernateDaoSupport implements
		ColectivosCoberturasDao {

	@Autowired
	public ColectivosCoberturasDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			T producto = (T) this.getHibernateTemplate().get(objeto, id);
			return producto;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			List<T> lista = this.getHibernateTemplate().loadAll(objeto);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro)throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuffer sb = new StringBuffer(150);
					sb.append("from ColectivosCoberturas P	\n");
					sb.append("where 1 = 1      \n");
					sb.append(filtro);
					sb.append("order by P.id");
									
					Query query = sesion.createQuery(sb.toString());
					
					List<T> lista = query.list();
					return lista;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}

		
	}

	@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().merge(objeto);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.getHibernateTemplate().delete(objeto);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			this.getHibernateTemplate().save(objeto);
			return objeto;
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error.", e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			for(T objeto: lista){
				
				this.guardarObjeto(objeto);
			}
			
		}
		catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Error.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ColectivosCoberturasDao#actualizaProductoPlan(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas)
	 */
	@Override
	public void actualizaProductoPlan(ColectivosCoberturas colectivosCoberturas) throws Excepciones {
		 StringBuffer actualiza = new StringBuffer();
		actualiza.append("UPDATE COLECTIVOS_COBERTURAS SET COCB_MT_FIJO = 0, COCB_PO_COMISION = 0 ");
		actualiza.append("WHERE COCB_CASU_CD_SUCURSAL =  ");
		actualiza.append(colectivosCoberturas.getId().getCocbCasuCdSucursal());
		actualiza.append(" AND COCB_CARP_CD_RAMO =  ");
		actualiza.append(colectivosCoberturas.getId().getCocbCarpCdRamo());
		actualiza.append(" AND COCB_CAPO_NU_POLIZA =  ");
		actualiza.append(colectivosCoberturas.getId().getCocbCapoNuPoliza());
		 try {
			 Query queryAct = getSession().createSQLQuery(actualiza.toString());
						 
			 queryAct.executeUpdate();
			 
		 } catch (Exception e) {
			throw new Excepciones(e.getMessage());
		}
		 
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ColectivosCoberturasDao#actualizaConfiguracionProductoPlan(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros)
	 */
	@Override
	public void actualizaConfiguracionProductoPlan(Parametros parametrosConfiguracion) throws Excepciones {
		 StringBuffer actualiza = new StringBuffer();
			actualiza.append("UPDATE COLECTIVOS_COBERTURAS SET COCB_MT_FIJO = 0, COCB_PO_COMISION = 0 ");
			actualiza.append("WHERE COCB_CASU_CD_SUCURSAL =  ");
			actualiza.append(parametrosConfiguracion.getCopaNvalor1() );
			actualiza.append(" AND COCB_CARP_CD_RAMO =  ");
			actualiza.append(parametrosConfiguracion.getCopaNvalor2());
			actualiza.append(" AND COCB_CAPO_NU_POLIZA   ");
			StringBuilder fpoliza = new StringBuilder(); 
			if(parametrosConfiguracion.getCopaNvalor3() == 0){
				fpoliza.append(" between ").append(parametrosConfiguracion.getCopaNvalor4()).append("0000 and ").append(parametrosConfiguracion.getCopaNvalor4()).append("9999 \n");
					
			 } else {
				 fpoliza.append(" = ").append(parametrosConfiguracion.getCopaNvalor3()).append(" \n");
			 }
			
			actualiza.append(fpoliza.toString());
			actualiza.append(" AND COCB_CAPU_CD_PRODUCTO =  ");
			actualiza.append(parametrosConfiguracion.getCopaVvalor1());
			
			actualiza.append(" AND COCB_CAPB_CD_PLAN =  ");
			actualiza.append(parametrosConfiguracion.getCopaVvalor2());
			
			
		
			 try {
				 Query queryAct = getSession().createSQLQuery(actualiza.toString());
							 
				 queryAct.executeUpdate();
				 
			 } catch (Exception e) {
				throw new Excepciones(e.getMessage());
			}
		
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.ColectivosCoberturasDao#actualizaConfiguracionCobertura(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros, java.lang.String)
	 */
	@Override
	public void actualizaConfiguracionCobertura(Parametros parametrosConfiguracion, String filtro) throws Excepciones {
		 StringBuffer actualiza = new StringBuffer();
			actualiza.append("UPDATE COLECTIVOS_COBERTURAS SET COCB_MT_FIJO = ");
			actualiza.append( parametrosConfiguracion.getCopaNvalor7());
			actualiza.append(" , COCB_PO_COMISION =  ");
			actualiza.append(parametrosConfiguracion.getCopaNvalor8());
			actualiza.append(" WHERE COCB_CASU_CD_SUCURSAL =  ");
			actualiza.append(parametrosConfiguracion.getCopaNvalor1() );
			actualiza.append(" AND COCB_CARP_CD_RAMO =  ");
			actualiza.append(parametrosConfiguracion.getCopaNvalor2());
			actualiza.append(" AND COCB_CAPO_NU_POLIZA   ");
			StringBuilder fpoliza = new StringBuilder(); 
			if(parametrosConfiguracion.getCopaNvalor3() == 0){
				fpoliza.append(" between ").append(parametrosConfiguracion.getCopaNvalor4()).append("0000 and ").append(parametrosConfiguracion.getCopaNvalor4()).append("9999 \n");
					
			 } else {
				 fpoliza.append(" = ").append(parametrosConfiguracion.getCopaNvalor3()).append(" \n");
			 }
			actualiza.append(fpoliza.toString());
			actualiza.append(" AND COCB_CAPU_CD_PRODUCTO =  ");
			actualiza.append(parametrosConfiguracion.getCopaVvalor1());
			
			actualiza.append(" AND COCB_CAPB_CD_PLAN =  ");
			actualiza.append(parametrosConfiguracion.getCopaVvalor2());
			
			actualiza.append(" AND COCB_CAPB_CD_PLAN =  ");
			actualiza.append(parametrosConfiguracion.getCopaVvalor2());
			
			actualiza.append(" AND COCB_CACB_CD_COBERTURA  ");
			actualiza.append(filtro);
			
			 try {
				 Query queryAct = getSession().createSQLQuery(actualiza.toString());
							 
				 queryAct.executeUpdate();
				 
			 } catch (Exception e) {
				throw new Excepciones(e.getMessage());
			}
		
	}
	
	
	

}
