/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author Leobardo Preciado 27/03/2015
 *
 */
public interface CiudadDao extends CatalogoDao{

	public abstract <T> List<T> obtenerObjetos() throws Excepciones;
	public abstract <T> List<T> obtenerObjetos(String filtro) throws Excepciones;
	public abstract List<Object> obtenerCiudad() throws Excepciones;
	
}
