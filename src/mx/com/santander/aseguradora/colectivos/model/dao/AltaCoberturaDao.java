package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;


/**
 * @author dflores
 *
 */

public interface AltaCoberturaDao<AltaCober> {
	
	public abstract AltaCober saveAltaCober(AltaCober altaCober);
	/**
	 * 
	 * @param lista
	 */
	public abstract void saveListaAltaCober(List<Object> lista);
	/**
	 * 
	 * @param id
	 * @return
	 */
	
	public abstract List<AltaCober> getListaAltaCober();
	/**
	 * 
	 * @param ramo
	 * @param poliza
	 * @return
	 */
	public abstract List<AltaCober> getListaAltaCober( Short ramo, String descCober,Integer ramoCont );
	/**
	 * 
	 * @param canal
	 * @param ramo
	 * @param poliza
	 */
	public abstract Integer iniciaAlta(  Short ramo, String descCober, Integer ramoCont );
	

	

}
