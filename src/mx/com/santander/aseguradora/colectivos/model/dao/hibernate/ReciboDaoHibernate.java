/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.dao.ReciboDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

/**
 * @author dflores
 *
 */
@Repository
public class ReciboDaoHibernate extends HibernateDaoSupport implements ReciboDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 298075158826438461L;

	@Autowired
	public ReciboDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){

				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					// TODO Auto-generated method stub
					
					StringBuilder consulta = new StringBuilder();
					consulta.append("from Recibo  R    \n");
					consulta.append("where 1=1  \n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					List<T> lista = qry.list();
					
					return lista;
				}
				
				
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

	
	/*
	 * Consulta para ColectivosRecibos
	 */
	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos1(final String filtro) throws Excepciones {
		try {
			
			return this.getHibernateTemplate().executeFind(new HibernateCallback(){
				public Object doInHibernate(Session sesion)
						throws HibernateException, SQLException {
					
					StringBuilder consulta = new StringBuilder();
					consulta.append("from ColectivosRecibo  R    \n");
					consulta.append("where 1=1  \n");
					consulta.append(filtro);
					
					Query qry = sesion.createQuery(consulta.toString());
					List<T> lista = qry.list();
					
					return lista;
				}
			});
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void impresionRecibo(String rutaTemporal, String nombre,
			Short coreCasuCdSucursal, Short coreCarpCdRamo,
			Integer coreCapoNuPoliza, BigDecimal coreNuRecibo, Short primaUnica) {
		// TODO Auto-generated method stub
		
		try {
			Map parametros = new HashMap();
			
			
			String rutaImages = FacesUtils.getServletRequest().getRealPath("images") + File.separator;		
									
		    parametros.put("CANAL", coreCasuCdSucursal.toString());
		    parametros.put("RAMO", coreCarpCdRamo.toString());
		    parametros.put("POLIZA", coreCapoNuPoliza.toString());
		    parametros.put("RECIBO",coreNuRecibo.toString());
		    parametros.put("PU", primaUnica);
		    parametros.put("RUTAIMG", rutaImages);		
		    
		    StringBuilder consulta = new StringBuilder() ;
		    
		    if(coreCarpCdRamo == 57 || coreCarpCdRamo == 58){
		    	consulta.append("select p.copa_vvalor7														\n");
			    consulta.append("from colectivos_parametros p												\n");
				consulta.append("    ,cart_recibos          r												\n");
				consulta.append("where p.copa_des_parametro = 'RECIBO' 										\n");
				consulta.append(" and p.copa_id_parametro   = 'LEYENDAS2'									\n");
				consulta.append("and r.care_casu_cd_sucursal  = ").append(coreCasuCdSucursal.toString()).append("\n");
				consulta.append("and r.care_carp_cd_ramo      = ").append(coreCarpCdRamo.toString()).append(" \n");
				consulta.append("and r.care_capo_nu_poliza    = ").append(coreCapoNuPoliza.toString()).append(" \n");
				consulta.append("and r.care_nu_recibo         = ").append(coreNuRecibo.toString()).append(" \n");
				consulta.append("and r.care_fe_emision        between p.copa_fvalor1 and p.copa_fvalor2		\n");
		    } else {
		    	consulta.append("select p.copa_vvalor7														\n");
			    consulta.append("from colectivos_parametros p												\n");
				consulta.append("    ,colectivos_recibos          r												\n");
				consulta.append("where p.copa_des_parametro = 'RECIBO' 										\n");
				consulta.append(" and p.copa_id_parametro   = 'LEYENDAS'									\n");
				consulta.append("and r.core_casu_cd_sucursal  = ").append(coreCasuCdSucursal.toString()).append("\n");
				consulta.append("and r.core_carp_cd_ramo      = ").append(coreCarpCdRamo.toString()).append(" \n");
				consulta.append("and r.core_capo_nu_poliza    = ").append(coreCapoNuPoliza.toString()).append(" \n");
				consulta.append("and r.core_nu_recibo         = ").append(coreNuRecibo.toString()).append(" \n");
				consulta.append("and r.core_fe_emision        between p.copa_fvalor1 and p.copa_fvalor2		\n");
		    }

			Query qry = getSession().createSQLQuery(consulta.toString());
			
			String version;
			
			if (qry.list().get(0) != null){
				version = (String) qry.list().get(0);		    
			}else{
				if(coreCarpCdRamo == 57 || coreCarpCdRamo == 58){
					version = "ImpresionReciboCFDI5758.jrxml";
				} else {
					version = "ImpresionReciboCFDI.jrxml";
				}
			}
			
		    JasperReport report = null;
		    report = JasperCompileManager.compileReport(rutaTemporal + version);
		    JasperPrint print = JasperFillManager.fillReport(report, parametros, getSession().connection());
		    
		    JRPdfExporter exporter = new JRPdfExporter ();
		    exporter.setParameter (JRExporterParameter.JASPER_PRINT, print);
		    exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre + ".pdf");
		    exporter.setParameter (JRPdfExporterParameter.METADATA_AUTHOR, "Santander Seguros");
		    exporter.exportReport();
		    //JasperExportManager.exportReportToPdfFile(print,rutaTemporal + nombre + ".pdf");
		}catch (Exception e) {	
		    e.printStackTrace();
		}
		
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public void impresionPreRecibo(String rutaTemporal, String nombre,
			short cofaCasuCdSucursal, short cofaCarpCdRamo, long cofaCapoNuPoliza,
			String cofaCampov1) {
		// TODO Auto-generated method stub
		
		try {
			Map parametros = new HashMap();
			String rutaImages = FacesUtils.getServletRequest().getRealPath("images") + File.separator;		
									
		    parametros.put("CANAL", cofaCasuCdSucursal);
		    parametros.put("RAMO", cofaCarpCdRamo);
		    parametros.put("POLIZA", cofaCapoNuPoliza);
		    parametros.put("RECIBOCOL",cofaCampov1);
		    parametros.put("RUTAIMG", rutaImages);
		  		    
		    JasperReport report = null;
		    report = JasperCompileManager.compileReport(rutaTemporal + "ImpresionPreRecibo.jrxml");
		    JasperPrint print = JasperFillManager.fillReport(report, parametros, getSession().connection());
		    
		    JRPdfExporter exporter = new JRPdfExporter ();
		    exporter.setParameter (JRExporterParameter.JASPER_PRINT, print);
		    exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre + ".pdf");
		    exporter.setParameter (JRPdfExporterParameter.METADATA_AUTHOR, "Santander Seguros");
		    exporter.exportReport();
		    //JasperExportManager.exportReportToPdfFile(print,rutaTemporal + nombre + ".pdf");
		}catch (Exception e) {	
		    e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")		
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> consulta2 (String strFiltro ) throws Excepciones {
		try {
			Query qry = this.getSession().createSQLQuery(strFiltro);
			List<T> lista = qry.list();
			return lista;
		} catch (Exception e){
			throw new Excepciones ("Error .",e);
		}
    }

	@Override
	public List<Object[]> consultaDetalleCobertura(short ramo, double no_recibo, int sucursal) throws Exception {
		StringBuilder selectDetalle = new StringBuilder();
		Query qry;
	
		try {
			if(ramo == 57 || ramo == 58) {
				selectDetalle.append("select mrec.camr_cacb_cd_cobertura	cobertura,\n");
				selectDetalle.append("		cob.cacb_de_cobertura       	des_cobertura,\n");
				selectDetalle.append("		mrec.camr_mt_prima_total    	Monto,\n");
				selectDetalle.append("		mrec.camr_mt_comision       	Tarifa\n");
				selectDetalle.append("	from cart_movimientos_recibos mrec\n");
				selectDetalle.append("	inner Join cart_coberturas cob on (cob.cacb_carb_cd_ramo = mrec.camr_carb_cd_ramo and cob.cacb_cd_cobertura = mrec.camr_cacb_cd_cobertura) \n");
				selectDetalle.append("where mrec.camr_casu_cd_sucursal = " + sucursal +  " \n");
				selectDetalle.append("	and mrec.camr_care_nu_recibo   = " + no_recibo + "\n");
			} else {
				selectDetalle.append("select mrec.comr_cacb_cd_cobertura	cobertura,\n");
				selectDetalle.append("		cob.cacb_de_cobertura       	des_cobertura,\n");
				selectDetalle.append("		mrec.comr_mt_prima_total    	Monto,\n");
				selectDetalle.append("		mrec.comr_mt_comision       	Tarifa\n");
				selectDetalle.append("	from colectivos_movimiento_recibos mrec\n");
				selectDetalle.append("	inner Join cart_coberturas cob on (cob.cacb_carb_cd_ramo = mrec.comr_carb_cd_ramo and cob.cacb_cd_cobertura = mrec.comr_cacb_cd_cobertura) \n");
				selectDetalle.append("where mrec.comr_casu_cd_sucursal = " + sucursal +  " \n");
				selectDetalle.append("	and mrec.comr_care_nu_recibo   = " + no_recibo + "\n");
			}
			
			qry = this.getSession().createSQLQuery(selectDetalle.toString());
			return  qry.list();	
		} catch(Exception ex){
			throw new Excepciones ("Error en DAO Consulta Cobertura.",ex);
		}
	}

	@Override
	public List<Object[]> consultaDetalleComponente(short ramo, double no_recibo, int sucursal) throws Exception {
		StringBuilder selectDetalle = new StringBuilder();
		Query qry;
		
		try {
			if(ramo == 57 || ramo == 58) {
				selectDetalle.append("select rc.carg_capp_cd_componente	componente,\n");
				selectDetalle.append("		cc.capp_de_componente      	Desc_componente,\n");
				selectDetalle.append("		rc.carg_mt_componente      	Mt_componente,\n");
				selectDetalle.append("		rc.carg_ta_componente      	Tarifa_componente \n");
				selectDetalle.append("	from cart_recibos_componentes rc\n");
				selectDetalle.append("	inner Join cart_componentes cc on (cc.capp_cd_componente = rc.carg_capp_cd_componente) \n");
				selectDetalle.append("where rc.carg_casu_cd_sucursal = " + sucursal  + "\n");	
				selectDetalle.append("	and rc.carg_care_nu_recibo   = " + no_recibo + "\n");
			} else {
				selectDetalle.append("select rc.corc_capp_cd_componente	componente,\n");
				selectDetalle.append("		cc.capp_de_componente      	Desc_componente,\n");
				selectDetalle.append("		rc.corc_mt_componente      	Mt_componente,\n");
				selectDetalle.append("		rc.corc_ta_componente      	Tarifa_componente\n");
				selectDetalle.append("	from colectivos_recibos_comp rc\n");
				selectDetalle.append("	inner Join cart_componentes cc on (cc.capp_cd_componente = rc.corc_capp_cd_componente) \n");
				selectDetalle.append("where rc.corc_casu_cd_sucursal = " + sucursal  + " \n");	
				selectDetalle.append("	and rc.corc_care_nu_recibo   = " + no_recibo + " \n");
			}
				
			qry = this.getSession().createSQLQuery(selectDetalle.toString());
			return  qry.list();	
		} catch(Exception ex){
			throw new Excepciones ("Error en DAO Consulta componente.",ex);
		}
	}

}
