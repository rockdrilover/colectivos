package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId;
import mx.com.santander.aseguradora.colectivos.model.dao.EndososDatosDao;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.GeneraMovimiento;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.GeneraRFCCertificados;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.NoMovimiento;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.UpdateCertificado;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.UpdateCoberturas;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.UpdateComponentes;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.ProcesosDaoHibernate.UpdateComponentes;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.database.EndosoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
@Repository
public class EndososDatosDaoHibernate extends HibernateDaoSupport implements EndososDatosDao {
	
	/**
	 * Constructor con SessionFactory
	 * @param sessionFactory session factory para acceso a base de datos
	 */
	@Autowired
	public EndososDatosDaoHibernate(SessionFactory sessionFactory) {		
		super.setSessionFactory(sessionFactory);
	}
	

	/**
	 * Metodo que consulta registros de EndososDatos
	 * @param filtro filtro a consultar 
	 * @return lista con Endosos
	 * @throws Excepciones error de consulta
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Object> getEndososDatos(final String filtro) throws Excepciones {
		List<Object> lstResultado;
		Query query;
		
		try {
			//Crea Query
			query = getSession().createQuery(filtro);
			
			//Ejecuta Query
			lstResultado = query.list();
		} catch (Exception e) {
			throw new Excepciones("Error:: CargaDaoHibernate.getEndososDatos:: " + e.getMessage(), e);
		}
		
		//Regresa Listado
		return lstResultado;
	}

	/**
	 * Metodo que actualiza datos en Colectivos_Certificados
	 * @param seleccionado datos de endoso
	 * @throws Excepciones error de proceso
	 */
	@Override
	public void actualizaCertificado(EndosoDatosDTO seleccionado) throws Excepciones {
		StringBuilder sbQuery;
		String strTipoEndo;
		String[] arrDatos;
		Query query;
		      
		try {
			//Declaracion de variables
			sbQuery = new StringBuilder();
			strTipoEndo = String.valueOf(seleccionado.getCedatpEndoso());
			arrDatos = seleccionado.getCedaCampov5().split("\\-");
			this.logger.info("Datos a autorizar-.-."+strTipoEndo+"::::"+arrDatos);
			
			//Se va creando sentencia Update
			sbQuery.append("UPDATE COLECTIVOS_CERTIFICADOS CC ");
			sbQuery.append("SET CC.COCE_NU_MOVIMIENTO = ").append(seleccionado.getCedaCampon3());
			
			if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_SUMA)) {
				//Se completa sentencia para suma asegurada
				sbQuery.append(", CC.COCE_MT_SUMA_ASEGURADA = ").append(seleccionado.getCedaSumaNvo());
				sbQuery.append(", CC.COCE_MT_PRIMA_ANUAL = ").append(arrDatos[0].trim());
				sbQuery.append(", CC.COCE_MT_PRIMA_REAL = ").append(arrDatos[1].trim());
				sbQuery.append(", CC.COCE_MT_PRIMA_ANUAL_REAL = ").append(arrDatos[2].trim());
				sbQuery.append(", CC.COCE_MT_PRIMA_PURA = ").append(arrDatos[3].trim());
				sbQuery.append(", CC.COCE_MT_PRIMA_PURA_REAL = ").append(arrDatos[4].trim());
				sbQuery.append(", CC.COCE_MT_PRIMA_SUBSECUENTE = ").append(arrDatos[5].trim());
				sbQuery.append(", CC.COCE_MT_PMA_UDIS = ").append(arrDatos[6].trim());
				sbQuery.append(", CC.COCE_DI_COBRO1 = '").append(arrDatos[7].trim());
				sbQuery.append("', CC.COCE_COMISIONES = '").append(arrDatos[8].trim()).append("'");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_VIGENCIAS)) {
				//Se completa sentencia para fechas de vigencia
				sbQuery.append(", CC.COCE_FE_DESDE = TO_DATE('").append(GestorFechas.formatDate(seleccionado.getCedaFeDesNvo(), Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO);
				sbQuery.append("'), CC.COCE_FE_HASTA = TO_DATE('").append(GestorFechas.formatDate(seleccionado.getCedaFeHasNvo(), Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO);
				sbQuery.append("'), CC.COCE_FE_FIN_CREDITO = TO_DATE('").append(arrDatos[10].trim()).append("', '").append(Constantes.FORMATO_FECHA_UNO).append("')");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_FECHA_SUS)) {
				//Se completa sentencia para fecha suscripcion
				sbQuery.append(", CC.COCE_FE_SUSCRIPCION = TO_DATE('").append(GestorFechas.formatDate(seleccionado.getCedaFeSusNvo(), Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO);
				sbQuery.append("'), CC.COCE_FE_DESDE = TO_DATE('").append(GestorFechas.formatDate(seleccionado.getCedaFeDesNvo(), Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO);
				sbQuery.append("'), CC.COCE_FE_HASTA = TO_DATE('").append(GestorFechas.formatDate(seleccionado.getCedaFeHasNvo(), Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO);
				sbQuery.append("'), CC.COCE_FE_INI_CREDITO = TO_DATE('").append(arrDatos[9].trim()).append("', '").append(Constantes.FORMATO_FECHA_UNO).append("')");
			}
				
			//se completa sentencia con WHERE
			sbQuery.append("  WHERE CC.COCE_CASU_CD_SUCURSAL = ").append(seleccionado.getId().getCedaCasuCdSucursal());
			sbQuery.append(" AND CC.COCE_CARP_CD_RAMO = ").append(seleccionado.getId().getCedaCarpCdRamo());
			sbQuery.append(" AND CC.COCE_CAPO_NU_POLIZA = ").append(seleccionado.getId().getCedaCapoNuPoliza());
			sbQuery.append(" AND CC.COCE_NU_CERTIFICADO = ").append(seleccionado.getId().getCedaNuCertificado());
			
			//Se crea Query
			query = getSession().createSQLQuery(sbQuery.toString()); 
			
			//Se ejecuta Query
			query.executeUpdate();
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar certificado colectivo. ", e);
		}
	}

	/**
	 * Metodo que actualiza datos en Colectivos_Clientes
	 * @param seleccionado datos de endoso
	 * @throws Excepciones error de proceso
	 */
	@Override
	public void actualizaCliente(EndosoDatosDTO seleccionado) throws Excepciones {
		StringBuilder sbQuery;
		String strTipoEndo;
		String[] arrDatos;
		Query query;
		      
		try {
			//Declaracion de variables
			sbQuery = new StringBuilder();
			strTipoEndo = String.valueOf(seleccionado.getCedatpEndoso());
			arrDatos = seleccionado.getCedaDireccionNvo().split("\\-");

			//Se va creando sentencia Update
			sbQuery.append("UPDATE COLECTIVOS_CLIENTES CL ");
		
			if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_NOMBRE)) {
				//Se completa sentencia para nombre
				sbQuery.append("SET CL.COCN_NOMBRE = '").append(seleccionado.getCedaNombreNvo());
				sbQuery.append("', CL.COCN_APELLIDO_PAT = '").append(seleccionado.getCedaApNvo());
				sbQuery.append("', CL.COCN_APELLIDO_MAT = '").append(seleccionado.getCedaAmNvo()).append("'");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_DOMICILIO)) {
				//Se completa sentencia para domicilio
				sbQuery.append("SET CL.COCN_CALLE_NUM = '").append(arrDatos[0].trim());
				sbQuery.append("', CL.COCN_COLONIA = '").append(arrDatos[1].trim());
				sbQuery.append("', CL.COCN_CD_POSTAL = '").append(arrDatos[2].trim());
				sbQuery.append("', CL.COCN_DELEGMUNIC = '").append(arrDatos[3].trim());
				sbQuery.append("', CL.COCN_CD_ESTADO = ").append(arrDatos[4]);
				sbQuery.append(", CL.COCN_CD_CIUDAD = ").append(arrDatos[5]);
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_FECHA_NAC)) {
				//Se completa sentencia para fecha nacimiento
				if(seleccionado.getCedaFechaNvo() == null) {
					sbQuery.append("SET CL.COCN_FE_NACIMIENTO = TO_DATE('").append(Constantes.DEFAULT_STRING ).append("', '").append(Constantes.FORMATO_FECHA_UNO).append("')");
				} else {
					sbQuery.append("SET CL.COCN_FE_NACIMIENTO = TO_DATE('").append(GestorFechas.formatDate(seleccionado.getCedaFechaNvo(), Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO).append("')");
				}
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_RFC)) {
				//Se completa sentencia para rfc
				sbQuery.append("SET CL.COCN_RFC = '").append(seleccionado.getCedaRfcNvo()).append("'");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_SEXO)) {
				//Se completa sentencia para sexo
				sbQuery.append("SET CL.COCN_CD_SEXO = '").append(seleccionado.getCedasexoNvo()).append("'");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_CORREO)) {
				//Se completa sentencia para correo
				sbQuery.append("SET CL.COCN_CORREO_ELECT = '").append(seleccionado.getCedaCorreoNvo()).append("'");
			}
			
			//se completa sentencia con WHERE
			sbQuery.append(" WHERE CL.COCN_NU_CLIENTE = ").append(seleccionado.getCedaNuRecibo());
			
			//se crea Query
			query = getSession().createSQLQuery(sbQuery.toString()); 
			
			//se ejecuta Query
			query.executeUpdate();
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar cliente colectivo. ", e);
		}
	}
	
	/**
	 * Metodo que respalda informacion del certificado
	 * @param coceCasuCdSucursal canal del certificado
	 * @param coceCarpCdRamo ramo del certificado
	 * @param coceCapoNuPoliza poliza del certificado
	 * @param coceNuCertificado numero de certificado
	 * @return numero de movimiento
	 * @throws Excepciones error de proceso
	 */
	@Override
	public Long generaMovimiento(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza, Long coceNuCertificado) throws Excepciones {
		Long nMov;
		DataSource ds;
		
		try {
			//Crea Data Source 
			ds = SessionFactoryUtils.getDataSource(this.getSession().getSessionFactory());
			
			//Obtiene siguiente movimiento
			NoMovimiento noMov = new NoMovimiento(ds);
			nMov = Long.parseLong(noMov.execute(coceCasuCdSucursal, coceCarpCdRamo, coceCapoNuPoliza, coceNuCertificado).get("returnname").toString());
			
			//Genera Movimiento
			GeneraMovimiento gm = new GeneraMovimiento(ds);
			gm.execute(coceCasuCdSucursal, coceCarpCdRamo, coceCapoNuPoliza, coceNuCertificado);
			
		} catch (Exception e) {
			throw new Excepciones("Error: Al generar movimiento. ", e);
		}
		
		return nMov;
	}

	/**
	 * Metodo que actualiza datos en Cart_Certificados
	 * @param seleccionado datos a actualizar
	 * @param arrResultado datos actuales
	 * @throws Excepciones error de proceso
	 */
	@Override
	public void actualizaCertificado(EndosoDatosDTO seleccionado, Object[] arrResultado) throws Excepciones {
		DataSource ds;
		String strTipoEndo;
		UpdateCertificado upCert;
		UpdateComponentes upCom;
		UpdateCoberturas upCober;
		
		try {
			//Declaracion de variables
			strTipoEndo = String.valueOf(seleccionado.getCedatpEndoso());
			
			//Se obtiene Data Source
			ds = SessionFactoryUtils.getDataSource(this.getSession().getSessionFactory());
						
			if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_SUMA)) {
				//Actualizacion para endoso de Suma asegurada
				Double dPmaAn = new BigDecimal(seleccionado.getCedaCampov5().split("\\-")[0].trim()).doubleValue();
				Double dPmaReal = new BigDecimal(seleccionado.getCedaCampov5().split("\\ -")[5].trim()).doubleValue();
				
				//Actualiza Certificado
				upCert = new UpdateCertificado(ds);
				upCert.execute(seleccionado.getId(), (Date) arrResultado[5], (Date) arrResultado[6], 
						seleccionado.getCedaSumaNvo().doubleValue(), dPmaReal, Constantes.DEFAULT_INT);
				
				//Actualiza Componentes
				upCom = new UpdateComponentes(ds);
				upCom.execute(seleccionado.getId(), seleccionado.getCedaSumaNvo().doubleValue(), dPmaAn, (int) Constantes.ENDOSO_ESTATUS_APLICADO);
				
				//Actualiza Coberturas
				upCober = new UpdateCoberturas(ds);
				upCober.execute(seleccionado.getId(), seleccionado.getCedaSumaNvo().doubleValue(), ((BigDecimal) arrResultado[7]).intValue(), 
						((BigDecimal) arrResultado[8]).intValue(), ((BigDecimal) arrResultado[9]).intValue(), 
						((BigDecimal) arrResultado[10]).intValue(), (int) Constantes.ENDOSO_ESTATUS_APLICADO);
			} else {
				/*
				 * Actualizacion para fechas de vigencia y fecha suscripcion
				 * Actualiza Certificado
				 */
				upCert = new UpdateCertificado(ds);
				upCert.execute(seleccionado.getId(), seleccionado.getCedaFeDesNvo(), seleccionado.getCedaFeHasNvo(),  
						((BigDecimal) arrResultado[3]).doubleValue(), ((BigDecimal) arrResultado[4]).doubleValue(), 
						(int) Constantes.ENDOSO_ESTATUS_APLICADO);
				
				//Actualiza fecha suscripcion
				actualizaFechaSuscripcion(seleccionado.getId(), seleccionado.getCedaFeSusNvo());
			} 
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualiza Certificado. ", e);
		}
	}
	
	/**
	 * Metodo que actualiza fecha suscripcion en CART_CERTIFICADOS
	 * @param id objeto con el id a modificar
	 * @param cedaFeSusNvo nueva fecha suscripcion
	 * @throws Excepciones error al actualizar
	 */
	private void actualizaFechaSuscripcion(EndososDatosId id, Date cedaFeSusNvo) throws Excepciones {
		StringBuilder sbQuery;
		Query query;
		      
		try {
			//Declaracion de variables
			sbQuery = new StringBuilder();
			
			//se crea sentencia update
			sbQuery.append("UPDATE CART_CERTIFICADOS CC SET ");			
				sbQuery.append(" CC.CACE_FE_SUSCRIPCION = TO_DATE('").append(GestorFechas.formatDate(cedaFeSusNvo, Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO).append("')");				
			sbQuery.append("  WHERE CC.CACE_CASU_CD_SUCURSAL = ").append(id.getCedaCasuCdSucursal());
			sbQuery.append(" AND CC.CACE_CARP_CD_RAMO = ").append(id.getCedaCarpCdRamo());
			sbQuery.append(" AND CC.CACE_CAPO_NU_POLIZA = ").append(id.getCedaCapoNuPoliza());
			sbQuery.append(" AND CC.CACE_NU_CERTIFICADO = ").append(id.getCedaNuCertificado());
			
			//se crea Query
			query = getSession().createSQLQuery(sbQuery.toString()); 
			
			//se ejecuta Query
			query.executeUpdate();
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar fecha suscripcion rector. ", e);
		}
		
	}

	/**
	 * Metodo que actualiza datos en Cart_Clientes
	 * @param seleccionado datos a actualizar
	 * @param arrResultado datos actuales
	 * @throws Excepciones error de proceso
	 */
	@Override
	public void actualizaCliente(EndosoDatosDTO seleccionado, Object[] arrResultado) throws Excepciones {
		StringBuilder sbQuery;
		String strTipoEndo;
		String[] arrDatos;
		Query query;
		      
		try {
			//Declaracion de variables
			sbQuery = new StringBuilder();
			strTipoEndo = String.valueOf(seleccionado.getCedatpEndoso());
			arrDatos = seleccionado.getCedaDireccionNvo().split("\\-");
			
			//Se va creando sentencia update
			sbQuery.append("UPDATE CART_CLIENTES CC ");
		
			if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_NOMBRE)) {
				//Se completa sentencia para nombre
				sbQuery.append("SET CC.CACN_NOMBRE = '").append(seleccionado.getCedaNombreNvo());
				sbQuery.append("', CC.CACN_APELLIDO_PAT = '").append(seleccionado.getCedaApNvo());
				sbQuery.append("', CC.CACN_APELLIDO_MAT = '").append(seleccionado.getCedaAmNvo());
				sbQuery.append("', CC.CACN_NM_APELLIDO_RAZON = '").append(seleccionado.getCedaApNvo()).append("/").append(seleccionado.getCedaAmNvo()).append("/").append(seleccionado.getCedaNombreNvo()).append("'");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_DOMICILIO)) {
				//Se completa sentencia para domicilio
				sbQuery.append("SET CC.CACN_DI_COBRO1 = '").append(arrDatos[0].trim());
				sbQuery.append("', CC.CACN_DI_HABITACION1 = '").append(arrDatos[0].trim());
				sbQuery.append("', CC.CACN_CAZP_COLONIA_COB = '").append(arrDatos[1].trim());
				sbQuery.append("', CC.CACN_ZN_POSTAL_COBRO = '").append(arrDatos[2].trim());
				sbQuery.append("', CC.CACN_ZN_POSTAL_HABITACION = '").append(arrDatos[2].trim());
				sbQuery.append("', CC.CACN_CAZP_POBLAC_COB = '").append(arrDatos[3].trim());
				sbQuery.append("', CC.CACN_CAES_CD_ESTADO_HAB = ").append(arrDatos[4]);
				sbQuery.append(", CC.CACN_CAES_CD_ESTADO_COB = ").append(arrDatos[4]);
				sbQuery.append(", CC.CACN_CACI_CD_CIUDAD_HAB = ").append(arrDatos[5]);
				sbQuery.append(", CC.CACN_CACI_CD_CIUDAD_COB = ").append(arrDatos[5]);
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_FECHA_NAC)) {
				//Se completa sentencia para fecha nacimiento
				if(seleccionado.getCedaFechaNvo() == null) {
					sbQuery.append("SET CC.CACN_FE_NACIMIENTO = TO_DATE('").append(Constantes.DEFAULT_STRING).append("', '").append(Constantes.FORMATO_FECHA_UNO).append("')");
				} else {
					sbQuery.append("SET CC.CACN_FE_NACIMIENTO = TO_DATE('").append(GestorFechas.formatDate(seleccionado.getCedaFechaNvo(), Constantes.FORMATO_FECHA_UNO)).append("', '").append(Constantes.FORMATO_FECHA_UNO).append("')");
				}
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_RFC)) {
				//Se completa sentencia para rfc
				sbQuery.append("SET CC.CACN_RFC = '").append(seleccionado.getCedaRfcNvo()).append("'");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_SEXO)) {
				//Se completa sentencia para sexo
				sbQuery.append("SET CC.CACN_CD_SEXO = '").append(seleccionado.getCedasexoNvo().substring(0, 1)).append("'");
			} else if(strTipoEndo.equals(Constantes.TIPO_ENDOSO_CORREO)) {
				//Se completa sentencia para correo
				sbQuery.append("SET CC.CACN_DI_HABITACION2 = '").append(seleccionado.getCedaCorreoNvo()).append("'");
			}
			
			//se completa sentencia con WHERE
			sbQuery.append(" WHERE CC.CACN_CD_NACIONALIDAD = '").append(arrResultado[0].toString()).append("'");
			sbQuery.append(" AND CC.CACN_NU_CEDULA_RIF = '").append(arrResultado[1].toString()).append("'");
			
			//Se crea Query
			query = getSession().createSQLQuery(sbQuery.toString()); 
			
			//Se ejecuta Query
			query.executeUpdate();
		} catch (Exception e) {
			throw new Excepciones("Error: Al actualizar cliente RECTOR. ", e);
		}
	}
	
	public String endosoFechaRFC(int idcliente,String camfech) throws Excepciones {
		Map<String, Object> nuevoRFC = new HashMap<String, Object>();
		StringBuilder reslt = new StringBuilder();
		try {
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			GeneraRFCCertificados genRfcnu = new GeneraRFCCertificados(ds);
			nuevoRFC=genRfcnu.executeCamFech(idcliente, camfech);
			reslt.append(nuevoRFC.get("pValorRfc"));
			reslt.append("|");
			reslt.append(nuevoRFC.get("pMensaje"));
			this.logger.info("OJECTOS DE REGRESO DE LA EJECUCION DEL NUEVO RFC"+nuevoRFC+"-.-.-..-"+reslt);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Excepciones("Error en la ejecucion del proceso del nuevo RFC con cambio de fecha "+ex);
		}
		
		return reslt.toString();	
	}
	
	public void updateEndosoFecha(BigDecimal idcli) throws Excepciones {
		StringBuilder updaterfc = new StringBuilder();
		Query query;
		String rfcnew,rsl,msj;
		String[] valgene;
		int idclien = idcli.intValue();
		
		
		try {
			rsl=endosoFechaRFC(idclien,"EndosoFecha");
			valgene=rsl.split("\\|");
			rfcnew=valgene[0];
			msj=valgene[1];			
			updaterfc.append("UPDATE COLECTIVOS_CLIENTES SET COCN_RFC ='").append(rfcnew).append("'");
			updaterfc.append(" where COCN_NU_CLIENTE=").append(idcli);
			this.logger.info("Valor RFCNEW::"+rfcnew+" valor de mensaje::"+msj+" valor de idcliente::"+idclien);
			this.logger.info("Actualizacion query::::"+updaterfc);
			query = getSession().createSQLQuery(updaterfc.toString());
			this.logger.info("Script:."+query);
			//Se ejecuta Query
			query.executeUpdate();
			this.logger.info("Ejecucion:."+query);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("Error en la actualizacion de rfc por endoso de fecha "+e);
		}
	}

}
