/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cobertura;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CoberturaId;


/**
 * @author dflores
 *
 */
public interface CoberturaDao {

	/**
	 * 
	 * @param poliza
	 * @return
	 */
	public abstract Cobertura getCobertura(CoberturaId id);
	/**
	 * 
	 * @return
	 */
	public abstract List<Cobertura> getCoberturas();
	/**
	 * 
	 * @param ramo
	 * @return
	 */
	public abstract List<Cobertura> getCoberturas(final Integer ramoContable);
}
