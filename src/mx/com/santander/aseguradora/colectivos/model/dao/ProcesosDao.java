/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CargaId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartEmisioncfdI;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.FacturacionId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;

/**
 * @author Sergio Plata
 *
 */
public interface ProcesosDao {

	/**
	 * 
	 * @param canal
	 * @param ramo
	 * @param poliza
	 * @param idVenta 
	 * @throws Excepciones 
	 */
	public abstract void emisionMasiva(Short canal, Short ramo, Long poliza, Integer idVenta) throws Excepciones;
	/**
	 * 
	 * @param canal
	 * @param ramo
	 * @param poliza
	 * @param causa
	 * @param validaDevo 
	 * @param calculaDevo 
	 * @throws Excepciones 
	 */
	public abstract void cancelacionmasiva(Short canal, Short ramo, Long poliza, Integer causa, Colectivos.OPC_CANACEL opc) throws Excepciones;
	
	/**
	 * 
	 * @param canal
	 * @param ramo
	 * @return
	 */
	public abstract int facturacion(Short canal, Short ramo);
	/**
	 * 
	 * @return
	 */
	public abstract DataSource getDataSource();
	
	public abstract String cancelacionCri(Short inCanal, Short inRamo,
			Long inPoliza, Integer inIdVenta, Integer causaAnula) throws Excepciones;
	
	public abstract void cancelaCertif(CertificadoId  id, Short cdanula , Date fechaanula);
	
	/**
	 * Metodo que ejecuta proceso de PreFctura
	 * @param id datos para facturar
	 * @param nNumRecibo numero de recibo
	 * @return Mapa de respuesta
	 * @throws Excepciones con error en general
	 */
	Map<String, Object> ejecutaPrefactura(CertificadoId id, Integer nNumRecibo) throws Excepciones;
	
	public abstract void ejecutaFacturacion(FacturacionId id) throws Excepciones;
	public abstract void ejecutaEndososPU(FacturacionId id) throws Excepciones;
	public abstract void aplicaEndosoDevolucion(Short inCanal, Short inRamo,
			Integer inIdVenta, PreCarga dato) throws Excepciones;
	public abstract Double calculaDevolucion(Certificado cer, Double tasaAmortiza, Date fechaAnulacion) throws Excepciones;
	public abstract void rehabilitacion(Short canal, Short ramo, Long poliza) throws Excepciones;
	public abstract String procesoRenovacionPolizas(Short canal, Short ramo, Long poliza) throws Excepciones;
	public abstract void ejecutaFacturacion5758(Short ramo) throws Excepciones;
	public abstract String endosarPoliza(Short inCanal, Short inRamo,	Long inPoliza, Integer inIdVenta, BigDecimal recibo, Date feDesde,
			Date feHasta, Integer inOperEndoso) throws Excepciones;
	public abstract CartEmisioncfdI estatusCobradoRecibo(FacturacionId id) throws Excepciones;
	public abstract String renovacion(Short canal, Short ramo, Long poliza) throws Excepciones;
	public abstract void validaObligados() throws Excepciones;
	public void validarMultidisposicion(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta) throws Excepciones;
	public abstract void validaCumulosBT() throws Excepciones;
	public void actualizaDatosComplementarios(List<Object> lstDatos) throws Excepciones;
	
	//dfg
	public abstract String numeradorCliente() throws Excepciones;
	public void  actNumeradorCliente() throws Excepciones;
	public void insertaContratante(String res, String inRazonSocial, Date inFechaCons,
            String inCalleNum ,String inCp, String inRfc, String inPoblacion,String inColonia) throws Excepciones;
			
	public Integer validaFechaNac(ClienteCertif cliente, Long poliza, Integer edadMin, Integer edadMax) throws Excepciones;
		
	public Integer busca_negative(String  wc_paterno_nf, String wc_materno_nf, String  wc_nombre_nf, Date wc_fecha_nac_cons) throws Excepciones;
	public Map<String, Object>  buscaCliente(ContratanteDTO contratanteDTO) throws Excepciones;
	public void contaColectivos(short canal, Short ramo, Long poliza, int certif, int ultRecibo, int mtUltRecibo, int operacion, int subOperacion, String fDesde, String fHasta, String fContable, String nombreFac, int estatusRecibo) throws Exception;
	public abstract void procesoCargaCuenta(Long nRamo, Integer nOperCargo, Double montoAC, Long cuenta, String newFechaFin, String fecha_hoy) throws ObjetoDuplicado, Excepciones;
	
	/**
	 * Reservas
	 * CJPV - VECTOR 28/09/2017
	 */
	public void  procesaReserva(int anio, int mes) throws Excepciones;
	/**
	 * Reservas
	 * CJPV - VECTOR 28/09/2017
	 */
	public List<Object> consultaReserva(int ramo, int mes, int anio) throws Excepciones;

	/**
	 * Metodo que manda llamar la funcion de base de datos (colectivos.generaCliente) 
	 * @return nmunero de cliente a generar
	 * @throws Excepciones
	 */
	public abstract Long getNumeroCliente() throws Excepciones;
	
	/**
	 * Metodo que manda llamar la funcion de base de datos (colectivos.noEndosoDetalle)
	 * @param coedCarpCdRamo ramo del endoso
	 * @param coedStEndoso estatus del endoso
	 * @return numero de endoso a generar
	 * @throws Excepciones
	 */
	public abstract Long getNumeroEndoso(short coedCarpCdRamo, String coedStEndoso) throws Excepciones;
	
	/**
	 * Metodo que manda llamar el procedimiento de base de datos (pack_migracol_sin.p_migracion_colectivos)
	 * @param id objecto con datos de canal, ramo, certificado
	 * @param inTipoCliente tipo de cliente 
	 * @param feOcurrencia fecha ocurrencia 
	 * @param inBandera indicardor de migracion
	 * @return respuesta del paquete
	 * @throws Excepciones
	 */
	public abstract String migraColectivos(ClienteCertifId id, Short inTipoCliente, Date feOcurrencia, Integer inBandera) throws Excepciones;
	
	/**
	 * Metodo que calcula nuevas primas
	 * @param id datos para proceso
	 * @throws Excepciones error de proceso
	 */
	void calculaPrimas(CargaId id) throws Excepciones;
	
	/**
	 * Metodo que migra informacion de beneficiarios
	 * @param id datos a migrar
	 * @throws Excepciones error de proceso
	 */
	void migraBeneficiario(EndososDatosId id) throws Excepciones;
	
	public String genRFCColCerti(String RFC, int numCertificado) throws Excepciones;

}
