/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao;

import java.io.Serializable;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author Sergio Plata
 *
 */
public interface CatalogoDao {
	
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @return
	 * @throws Excepciones
	 */
	public abstract <T> T guardarObjeto(T objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param lista
	 * @throws Excepciones
	 */
	public abstract <T> void guardarObjetos(List<T> lista)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @param id
	 * @return
	 * @throws Excepciones
	 */
	public abstract <T> T obtenerObjeto(Class<T> objeto, Serializable id)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @throws Excepciones
	 */
	public abstract <T> void borrarObjeto(T objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @throws Excepciones
	 */
	public abstract <T> void actualizarObjeto(T objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @return
	 * @throws Excepciones
	 */
	public abstract <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param filtro
	 * @return
	 * @throws Excepciones
	 */
	public abstract <T> List<T> obtenerObjetos(String filtro)throws Excepciones;
}
