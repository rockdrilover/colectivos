/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;





import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cobertura;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CoberturaId;
import mx.com.santander.aseguradora.colectivos.model.dao.CoberturaDao;


import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dflores
 *
 */
public class CoberturaDaoHibernate extends HibernateDaoSupport implements
		CoberturaDao {

	public Cobertura getCobertura(CoberturaId id) {
		// TODO Auto-generated method stub
		Cobertura cobertura = (Cobertura) this.getHibernateTemplate().load(Cobertura.class, id);
		return cobertura;
	}


	public List<Cobertura> getCoberturas() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Cobertura> getCoberturas(final Integer ramoContable) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().executeFind(new HibernateCallback(){

			public Object doInHibernate(Session sesion)
					throws HibernateException, SQLException {
				// TODO Auto-generated method stub
				
				StringBuffer sb = new StringBuffer(150);
				sb.append("from Cobertura B\n" +
						  "where B.id.cacbCarbCdRamo =:ramoContable \n" +
						  "order by B.id");
				
								
				Query query = sesion.createQuery(sb.toString());
				query.setParameter("ramoContable", ramoContable);
				
				List<Object> lista = query.list();
				
				
				
				return lista;
			}
		});
		
	}

	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS) 
	public <T> List<T> obtenerCartCoberturas(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
    	StringBuilder query = null;
    	List<T> lista = null;
    	try {
    		
    		query = new StringBuilder();
    		lista = new ArrayList<T>();
     		
    		query.append(" SELECT distinct (cc.cacb_carp_cd_ramo),            \n"); 	        
    		query.append("	         cc.cacb_cd_cobertura  cve_cob,           \n");	
    		query.append("	         cc.cacb_de_cobertura  desc_cobertura,    \n");	
    		query.append("	         cc.cacb_carb_cd_ramo ramoCont            \n");	
    		query.append("    	FROM COLECTIVOS_COBERTURAS C,  				  \n");	
    		query.append("           cart_coberturas cc						  \n");	
    		query.append("WHERE c.cocb_carp_cd_ramo = ").append(filtro).append(   "\n"); // query.append(filtro);
    		query.append("and c.cocb_carp_cd_ramo = cc.cacb_carp_cd_ramo      \n");	
    		query.append("and c.cocb_cacb_cd_cobertura = cc.cacb_cd_cobertura \n");	
    		query.append("and c.cocb_carb_cd_ramo = cc.cacb_carb_cd_ramo      \n");	
    		query.append("and cc.cacb_in_cobertura_sub = 'C'  \n");
    		
    		lista = getSession().createSQLQuery(query.toString()).list();
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error al recuperar coberturas de cart_coberturas", e);
		}
    	
    	return lista;
    }
	
	



}
