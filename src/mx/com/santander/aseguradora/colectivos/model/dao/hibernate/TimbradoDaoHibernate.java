package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.sql.DataSource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.dao.TimbradoDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-04-2020
 * Description: Clase de tipo DAO que tiene acceso a base de datos para 
 * 				el proceso de Timbrado.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Repository
public class TimbradoDaoHibernate extends HibernateDaoSupport implements TimbradoDao {

	//propuedad serializable
	private static final long serialVersionUID = 298075158826438461L;
	
	/**
	 * Constructor de clase
	 * @param sessionFactory session factory para conexion a DB
	 */
	@Autowired
	public TimbradoDaoHibernate(SessionFactory sessionFactory) {
		//Se inicializa session factory
		super.setSessionFactory(sessionFactory);
	}

	/**
	 * Metodo que consulta los recibos timbrados
	 * @param filtro where a ejecutar
	 * @param feHasta fecha desde para filtro
	 * @param feDesde fecha hasta para filtro
	 * @param tipoBusqueda tipo de busqueda
	 * @return lista de datos 
	 * @throws Excepciones con error en general
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Object> getRecTimbrados(StringBuilder filtro, String feDesde, String feHasta, Integer tipoBusqueda) throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			//se inicializan variables
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT CARTI_CASU_CD_SUCURSAL, CARTI_CARP_CD_RAMO, CARTI_CAPO_NU_POLIZA, CARTI_CACE_NU_CERTIFICADO, CARTI_NU_RECIBO, CARTI_ST_TIMBRADO, CARTI_FE_TIMBRADO_C, CARTI_ST_CANCELADO, CARTI_FE_CANCELADO_C, CARTI_UUID \n");
				consulta.append("FROM CART_RECIBOS_TIMBRADOS \n");
			consulta.append(filtro);
			consulta.append("ORDER BY CARTI_FE_TIMBRADO_T DESC \n");
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se agregan parametros necesarios para realizar consulta
			if(tipoBusqueda == 1) {
				query.setParameter("feDesde", feDesde);
				query.setParameter("feHasta", feHasta);
			}

			//se ejecuta la consulta
			return query.list();
		} catch (Exception e) {
			throw new Excepciones("Error en TimbradoDao.getRecTimbrados(): ", e); 
		}
	}
	
	@Override
	public List<Object> getDetalleTimbrado(BigDecimal recibo) throws Excepciones {
		StringBuilder consulta;
		Query query;
		
		try {
			//se inicializan variables
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT CARTI_CASU_CD_SUCURSAL, CARTI_CARP_CD_RAMO, CARTI_CAPO_NU_POLIZA, CARTI_CACE_NU_CERTIFICADO, CARTI_NU_RECIBO, \n");
					consulta.append("CARTI_ST_TIMBRADO, CARTI_FE_TIMBRADO_C, CARTI_ST_CANCELADO, CARTI_FE_CANCELADO_C, CARTI_UUID, CORE_FE_DESDE, \n");
					consulta.append("CORE_FE_HASTA, CORE_FE_EMISION, COFA_FE_FACTURACION, CARTI_NU_CERTIFICADO, CORE_MT_SUMA_ASEGURADA, CORE_MT_PRIMA, \n");
					consulta.append("CORE_ST_RECIBO||'-'||ESSIS.RV_MEANING, CORE_CACN_CD_NACIONALIDAD ||'-'|| CORE_CACN_NU_CEDULA_RIF||' : '|| CACN_NM_APELLIDO_RAZON, \n");
					consulta.append("NVL(CORE_TP_TRAMITE, 1) ||'-'||ESCOB.RV_MEANING, CORE_NU_CONSECUTIVO_CUOTA, CORE_MT_PRIMA_PURA, \n");
					consulta.append("COFA_NU_RECIBO_COL, COFA_TOTAL_CERTIFICADOS, COFA_MT_TOT_IVA, COFA_MT_TOT_DPO, COFA_MT_TOT_RFI, CACN_RFC \n");
				consulta.append("FROM CART_RECIBOS_TIMBRADOS CRT \n");
				consulta.append("INNER JOIN COLECTIVOS_RECIBOS CR ON(CORE_CASU_CD_SUCURSAL = CARTI_CASU_CD_SUCURSAL AND CORE_NU_RECIBO = CARTI_NU_RECIBO) \n");
				consulta.append("INNER JOIN COLECTIVOS_FACTURACION ON (COFA_CASU_CD_SUCURSAL = CORE_CASU_CD_SUCURSAL AND COFA_NU_RECIBO_FISCAL = CORE_NU_RECIBO) \n");
				consulta.append("INNER JOIN CART_CLIENTES ON(CACN_CD_NACIONALIDAD = CORE_CACN_CD_NACIONALIDAD AND  CACN_NU_CEDULA_RIF = CORE_CACN_NU_CEDULA_RIF) \n");
				consulta.append("LEFT JOIN CG_REF_CODES ESSIS ON(ESSIS.RV_DOMAIN = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO' AND ESSIS.RV_LOW_VALUE = CORE_ST_RECIBO)  \n");
				consulta.append("LEFT JOIN CG_REF_CODES ESCOB ON(ESCOB.RV_DOMAIN = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO' AND ESCOB.RV_LOW_VALUE = NVL(CORE_TP_TRAMITE, 1)) \n");
			consulta.append("WHERE CARTI_CASU_CD_SUCURSAL = 1 \n");
				consulta.append("AND CARTI_NU_RECIBO = ").append(recibo);
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se ejecuta la consulta
			return query.list();
		} catch (Exception e) {
			throw new Excepciones("Error en TimbradoDao.getDetalleTimbrado(): ", e); 
		}
	}

	@Override
	public List<ColectivosCoberturas> getDetalleCober(BigDecimal recibo) throws Excepciones {
		List<ColectivosCoberturas> lstCober;
		ColectivosCoberturas objCober;
		List<Object> lstDatos;
		Object[] arrDatos;
		Iterator<Object> it;
		StringBuilder consulta;
		Query query;
		
		try {
			//se inicializan variables
			lstCober = new ArrayList<ColectivosCoberturas>();
			lstDatos = new ArrayList<Object>();
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT MREC.COMR_CARB_CD_RAMO, MREC.COMR_CACB_CD_COBERTURA, \n");
					consulta.append("COB.CACB_DE_COBERTURA, MREC.COMR_MT_PRIMA_TOTAL, \n");
					consulta.append("MREC.COMR_MT_CAPITAL, MREC.COMR_MT_PRIMA_BRUTA \n");
				consulta.append("FROM COLECTIVOS_MOVIMIENTO_RECIBOS MREC \n");
				consulta.append("INNER JOIN CART_COBERTURAS COB ON (COB.CACB_CARB_CD_RAMO = MREC.COMR_CARB_CD_RAMO AND COB.CACB_CD_COBERTURA = MREC.COMR_CACB_CD_COBERTURA)  \n");
			consulta.append("WHERE MREC.COMR_CASU_CD_SUCURSAL = 1 \n");
				consulta.append("AND MREC.COMR_CARE_NU_RECIBO = ").append(recibo);
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se ejecuta la consulta
			lstDatos.addAll(query.list());
			
			//se recorren resultados para crear lista final
			it = lstDatos.iterator();
			while(it.hasNext()) {
				//se obtiene arreglo de datos
				arrDatos = (Object[]) it.next();
				
				//se inicializa objeto componentes
				objCober = new ColectivosCoberturas();
				objCober.setCocbCampov1(arrDatos[0].toString() + " - " + arrDatos[1].toString());
				objCober.setCocbCampov2(arrDatos[2].toString());
				objCober.setCocbMtFijo((BigDecimal) arrDatos[3]);
				objCober.setCocbCampon2((BigDecimal) arrDatos[4]);
				objCober.setCocbCampon3((BigDecimal) arrDatos[5]);

				//se agrega a lista final
				lstCober.add(objCober);
			}
		} catch (Exception e) {
			throw new Excepciones("Error en TimbradoDao.getDetalleCober(): ", e); 
		}
		   
		return lstCober;
	}

	@Override
	public List<ColectivosComponentes> getDetalleComp(BigDecimal recibo) throws Excepciones {
		List<ColectivosComponentes> lstCompo;
		ColectivosComponentes objCompo;
		List<Object> lstDatos;
		Object[] arrDatos;
		Iterator<Object> it;
		StringBuilder consulta;
		Query query;
		
		try {
			//se inicializan variables
			lstCompo = new ArrayList<ColectivosComponentes>();
			lstDatos = new ArrayList<Object>();
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT CAPP_CD_COMPONENTE, CAPP_DE_COMPONENTE, CORC_TA_COMPONENTE, CORC_MT_COMPONENTE \n");
				consulta.append("FROM COLECTIVOS_RECIBOS_COMP RC \n");
				consulta.append("INNER JOIN CART_COMPONENTES CC ON (CC.CAPP_CD_COMPONENTE = RC.CORC_CAPP_CD_COMPONENTE) \n");
			consulta.append("WHERE RC.CORC_CASU_CD_SUCURSAL = 1 \n");
				consulta.append("AND RC.CORC_CARE_NU_RECIBO = ").append(recibo);
			
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se ejecuta la consulta
			lstDatos.addAll(query.list());
			
			//se recorren resultados para crear lista final
			it = lstDatos.iterator();
			while(it.hasNext()) {
				//se obtiene arreglo de datos
				arrDatos = (Object[]) it.next();
				
				//se inicializa objeto componentes
				objCompo = new ColectivosComponentes();
				objCompo.setCoctCampov1(arrDatos[0].toString() + " - " + arrDatos[1].toString());
				objCompo.setCoctTaComponente((BigDecimal) arrDatos[2]);
				objCompo.setCoctCampon2((BigDecimal) arrDatos[3]);

				//se agrega a lista final
				lstCompo.add(objCompo);
			}
		} catch (Exception e) {
			throw new Excepciones("Error en TimbradoDao.getDetalleComp(): ", e); 
		}
		   
		return lstCompo;
	}

	@Override
	public Map<String, Object> getDatosTimbrado(int canal, byte ramo, int poliza, int certi, BigDecimal recibo) throws Excepciones {
		try {
			/* Instancia de Data Source */
			DataSource ds = SessionFactoryUtils.getDataSource(this.getSessionFactory());
			
			/* Llamado ala clase de validacion */							
			GetDatosTimbrado datos = new GetDatosTimbrado(ds);
			
			/* Ejecucion del metodo de validacion */
			return datos.execute(canal, ramo, poliza, certi, recibo);
			
		}catch (DataIntegrityViolationException e) {
			/* Bateo de excepcion de integridad de datos */
			throw new Excepciones("Error en integridad TimbradoDao.getDatosTimbrado(): ", e);
		}catch (Exception e) {
			/* Bateo de bloque de excepciones especificas */
			throw new Excepciones("Error en TimbradoDao.getDatosTimbrado()", e);
		}
	
	}

	@Override
	public String getDescError(String cdError) throws Excepciones {
		String strResultado = Constantes.DEFAULT_STRING;
		StringBuilder consulta;
		Query query;
		List lstRes;
		
		try {
			//se inicializan variables
			consulta = new StringBuilder();
			
			//se va creando consulta a ejecutar segun los filtros deseados
			consulta.append("SELECT CCEW_DESCRIPCION \n");
				consulta.append("FROM CART_CFDI_ERROR_WS \n");
			consulta.append("WHERE CCEW_TIPO_OPERACION = ").append(Constantes.CFDI_OPERACION_TIMBRAR).append(" \n");
			consulta.append("AND CCEW_CODIGO = 'WS").append(cdError).append("' \n");
            
			//se crea el query en la session
			query = this.getSession().createSQLQuery(consulta.toString());
			
			//se ejecuta la consulta
			lstRes = query.list();
			if(!lstRes.isEmpty()) {
				strResultado = lstRes.get(0).toString();
			}
		} catch (Exception e) {
			throw new Excepciones("Error en TimbradoDao.getDescError(): ", e); 
		}
		
		return strResultado;
	}
}

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-04-2020
 * Description: Clase de tipo StoredProcedure para ejecutar el PS que 
 * conutla datos para timbrado.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	
 * 		Cuando:
 * 		Porque:
 * ------------------------------------------------------------------------
 * 
 */
final class GetDatosTimbrado extends StoredProcedure{

	/**
	 * Constructor de la clase
	 * @param ds DataSource de la clase que la llama
	 */
	public GetDatosTimbrado(DataSource ds) {
		/* Seteo de data source */
		super.setDataSource(ds);
		
		/* Seteo para identificacion de funcion */
		super.setFunction(false);
		
		/* Nombre de la SPn */
		super.setSql(Colectivos.GET_DATOS_TIMBRADO);
		
		/* Parametros de entrada */
		super.declareParameter(new SqlParameter("V_CANAL", Types.INTEGER));
		super.declareParameter(new SqlParameter("V_RAMO", Types.INTEGER));
		super.declareParameter(new SqlParameter("V_POLIZA", Types.INTEGER));
		super.declareParameter(new SqlParameter("V_CERTIFICADO", Types.INTEGER));
		super.declareParameter(new SqlParameter("V_NU_RECIBO", Types.NUMERIC));

		/* Parametros de salida */
		super.declareParameter(new SqlOutParameter("V_FE_COMPROVANTE",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_LUGAR_EXPEDICION",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_TPO_MONEDA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_TIPO_COMPROBANTE",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_REGIMEN",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_RFC_ASEGURADORA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_RFC_CLIENTE",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_NOMBRE",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_USOCFDI",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_CANTIDAD",Types.INTEGER));
		super.declareParameter(new SqlOutParameter("V_CD_PROSERV",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_CLAVE_UNIDAD",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_TASA_CUOTA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_CD_IMPUESTO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_TIPRAMO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_TP_CLIENTE",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_NU_TELEFONO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_CP",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_NU_POLIZA_C",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_DESC_RAMO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_DESC_PRODUCTO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_SUMA_ASEG",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_FR_PAGO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_PAGO",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_PFE_DESDE",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_PFE_HASTA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_TP_CUENTA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_DESC_BANCO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_CUENTA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_DESC_SUCURSAL",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_CD_SUCURSAL",Types.INTEGER));
		super.declareParameter(new SqlOutParameter("V_R_AMPARA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_RFE_DESDE",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_RFE_HASTA",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_REC_CONCEPTO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_MT_PRIMA",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_PRIMAPURA",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_IVA_PRIMA",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_DPO",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_IVA_DPO",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_RFI",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_IVA_RFI",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_RAU",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_MT_IVA",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_SUBTOTAL",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_TOTAL",Types.NUMERIC));
		super.declareParameter(new SqlOutParameter("V_DOMICILIO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_METODO_PAGO",Types.VARCHAR));
		super.declareParameter(new SqlOutParameter("V_CD_ERROR",Types.VARCHAR));
		
		/* Compilacion de las declaraciones */
		super.compile();
	}
	
	/**
	 * Metodo de ejecucion para obtner datos de timbrado
	 * @param canal canal del recibo a buscar
	 * @param ramo ramo del recibo a buscar
	 * @param poliza poliza del recibo a buscar
	 * @param certi certi del recibo a buscar
	 * @param recibo recibo a buscar
	 * @return lista con valores de la busqueda
	 * @throws DataAccessException excepcion especifica de acceso a datos de BD
	 */
	public Map execute(int canal, byte ramo, int poliza, int certi, BigDecimal recibo) throws DataAccessException{
		/* Instancia de mapa de entrada al SP */
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		/* Seteo de valores de entrada al SP */
		inParams.put("V_CANAL", canal);
		inParams.put("V_RAMO", ramo);
		inParams.put("V_POLIZA", poliza);
		inParams.put("V_CERTIFICADO", certi);
		inParams.put("V_NU_RECIBO", recibo);
		
		/* Instancia de ejecucion del SP */
		Map<String, Object> ejecucion;
		
		ejecucion = super.execute(inParams);
		
		/* Retorno de la ejecucion */
		return ejecucion;
	}
}
