package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import mx.com.santander.aseguradora.colectivos.model.dao.FlujoDeEfectivoDao;

@Repository
public class FlujoDeEfectivoDaoHibernate extends HibernateDaoSupport implements FlujoDeEfectivoDao{

	@Autowired
	public FlujoDeEfectivoDaoHibernate(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Override
	public List<Object> consultaCuentas() throws Exception {
		List<Object> arlResultados;
		StringBuffer sbQuery;
	    
		try {
			arlResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			
			sbQuery.append("select distinct(c.copa_vvalor1) ");
			sbQuery.append("from colectivos_parametros c ");
			sbQuery.append("where c.copa_id_parametro = 'CUENTAS' ");
			sbQuery.append("and c.copa_des_parametro = 'CONCILIACION' ");
			sbQuery.append("and c.copa_vvalor3 = 'CARGA_AUT' ");
			
			Query query = this.getSession().createSQLQuery(sbQuery.toString());
			arlResultados = (ArrayList<Object>) query.list();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: FlujoDeEfectivoDaoHibernate.consultaCuentas(): " + e.getMessage()); 
		}
		return arlResultados;
	}

	@Override
	public <T> List<T> cargaReporteCargoAbono(String fInicio, String fFin,String cuenta,Integer operacion) throws Exception {
		List<T> polizas = null;
		
		try{
			StringBuilder sb = new StringBuilder();
	
			sb.append("select tabla.cuenta, sum(tabla.ingresos), sum(tabla.contable), sum(tabla.deposito), sum(tabla.emican) \n");
			sb.append("from ( \n");
				sb.append("select a.cocu_cuenta_pago cuenta,sum(a.cocu_monto_prima) ingresos,0 contable,0 deposito,0 emican \n");
					sb.append("from Colectivos_Cuentas a \n");
				sb.append("where a.cocu_fecha_ingreso between to_date('").append(fInicio).append("', 'dd/MM/yyyy') and to_date('").append(fFin).append("', 'dd/MM/yyyy') \n");
				if(!cuenta.equals("0")){
					sb.append("and a.cocu_cuenta_pago = ").append(cuenta).append(" \n");
				}
				if (operacion == 1){
					sb.append("and a.cocu_tipo_movimiento = 'C' \n");
				} else if(operacion == 2){
					sb.append("and a.cocu_tipo_movimiento = 'A' \n");
				}
				sb.append("group by a.cocu_cuenta_pago \n");
				sb.append("union all \n");
				
				sb.append("select b.cocu_cuenta_pago,0,sum(b.cocu_monto_prima),0,0 \n");
					sb.append("from Colectivos_Cuentas b \n");
				sb.append("where b.cocu_fecha_ingreso between to_date('").append(fInicio).append("', 'dd/MM/yyyy') and to_date('").append(fFin).append("', 'dd/MM/yyyy') \n");
				if(!cuenta.equals("0")){
					sb.append("and b.cocu_cuenta_pago = ").append(cuenta).append(" \n");
				}
				if (operacion == 1){
					sb.append("and b.cocu_tipo_movimiento = 'C' \n");
				} else if(operacion == 2){
					sb.append("and b.cocu_tipo_movimiento = 'A' \n");
				}
					sb.append("and b.cocu_calificador in (2,4) \n");
				sb.append("group by b.cocu_cuenta_pago \n");
				sb.append("union all \n");
				
				sb.append("select d.cocu_cuenta_pago,0,0,sum(d.cocu_monto_prima),0 \n");
					sb.append("from Colectivos_Cuentas d \n");
				sb.append("where d.cocu_fecha_ingreso between to_date('").append(fInicio).append("', 'dd/MM/yyyy') and to_date('").append(fFin).append("', 'dd/MM/yyyy') \n");
				if(!cuenta.equals("0")){
					sb.append("and d.cocu_cuenta_pago = ").append(cuenta).append(" \n");
				}
				if (operacion == 1){
					sb.append("and d.cocu_tipo_movimiento = 'C' \n");
				} else if(operacion == 2){
					sb.append("and d.cocu_tipo_movimiento = 'A' \n");
				}
					sb.append("and d.cocu_calificador NOT IN(2, 4) \n");
				sb.append("group by d.cocu_cuenta_pago \n");
				sb.append("union all \n");
								
				sb.append("select e.cocu_cuenta_pago,0,0,0,sum(e.cocu_monto_prima) \n");
					sb.append("from colectivos_certificados f \n");
					sb.append("INNER JOIN colectivos_cuentas e ON(E.COCU_RAMO = f.coce_carp_cd_ramo AND E.COCU_POLIZA = f.coce_capo_nu_poliza AND E.COCU_CERTIFICADO = F.COCE_NU_CERTIFICADO) \n");
				sb.append("where f.coce_casu_cd_sucursal = 1 \n");
					sb.append("and f.coce_fe_anulacion between to_date('").append(fInicio).append("', 'dd/MM/yyyy') and to_date('").append(fFin).append("', 'dd/MM/yyyy') \n");
					sb.append("and e.cocu_fecha_ingreso between to_date('").append(fInicio).append("', 'dd/MM/yyyy') and to_date('").append(fFin).append("', 'dd/MM/yyyy') \n");
				if(!cuenta.equals("0")){
					sb.append("and e.cocu_cuenta_pago = ").append(cuenta).append(" \n");
				}
				if (operacion == 1){
					sb.append("and e.cocu_tipo_movimiento = 'C' \n");
				} else if(operacion == 2){
					sb.append("and e.cocu_tipo_movimiento = 'A' \n");
				}
					sb.append("and e.cocu_calificador in (2,4) \n");	
				sb.append("group by e.cocu_cuenta_pago) tabla \n");
			sb.append("group by tabla.cuenta \n");
			
			Query query = this.getSession().createSQLQuery(sb.toString());
			polizas = query.list();
			
			return polizas;
		}catch (Exception e){
			throw new Exception("Error.", e);
		}
	}
}
