/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.dao.hibernate;

import java.io.Serializable;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.TraspasoAptcDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author OmarCortes
 *
 */
@Repository
public class TraspasoAptcDaoHibernate extends HibernateDaoSupport implements TraspasoAptcDao {

	@Autowired
	public TraspasoAptcDaoHibernate(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		super.setSessionFactory(sessionFactory);
	}
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao#obtenerObjetos(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(final String filtro) throws Excepciones {
		// TODO Auto-generated method stub
	
		StringBuilder consulta;
		
		try {
		
			
			consulta = new StringBuilder();
			consulta.append("from TlmkTraspasoAptc TA \n");
			consulta.append("where 1 = 1");
			consulta.append(filtro);
			
			Query query = getSession().createQuery(consulta.toString());
			
			
			List<T> lista = query.list();
			
			return lista;
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("Error.", e);
		}
	}

}
