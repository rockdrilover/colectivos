/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.database;

/**
 * @author Sergio plata
 *
 */
public class Colectivos {

	public static final String EMISION_MASIVA     	 	     = "gioseg.colectivos.emisioncolectivos";
	public static final String CANCELACION_MASIVA 	 	     = "gioseg.colectivos.cancelacionMasiva";
	public static final String RENOVACION		  	 	     = "gioseg.colectivos.renovacion";
	public static final String CANCELACION_CRI			     = "gioseg.colectivos.cancelacionCRI";
	public static final String PROCESO_CARGA 	  	 	     = "gioseg.colectivos.preCargacolectivos";
	public static final String PROCESO_ALTA 		 	     = "gioseg.colectivos.P_Guarda_Poliza";
	public static final String PROCESO_ALTAPROD 	 	     = "gioseg.colectivos.P_GUARDA_PRODUC_PLAN";
	public static final String PROCESO_ALTACOBERTURA 	     = "gioseg.colectivos.P_GUARDA_COBERTURA";
	public static final String PROCESO_ALTATARIFACOBERTURA   = "gioseg.colectivos.P_GUARDA_TARIFA_COBERTURA";
	public static final String CALCULO_COMISIONES	 	     = "gioseg.colectivos.calculo";
	public static final String PRE_FACTURACION			     = "gioseg.colectivos_facturador.P_PREFACTURA_130";
	public static final String FACTURACION				     = "gioseg.colectivos_facturador.Facturacion";
	public static final String GENERA_MOVIMIENTO		     = "gioseg.colectivos.generaMovimiento";
	public static final String NO_MOVIMIENTO			     = "gioseg.colectivos.noMovimiento";
	public static final String COBERTURA_CERTIFICADO	     = "gioseg.colectivos.primaPura";
	public static final String COMPONENTE_CERTIFICADO 	     = "gioseg.colectivos.primaNeta";
	public static final String ENDOSOS_PUNICA 			     = "gioseg.colectivos_facturador.f_clasificaDatos";
	public static final String DEVOLUCION				     = "gioseg.colectivos_devoluciones.devoPmaVidConsumo";
	public static final String RENOVACIONA 					 = "gioseg.colectivos.f_renovacionA";
	public static final String REHABILITACION 				 = "gioseg.colectivos.rehabilitacionMasiva";
    public static final String FACTURACIONB				     = "gioseg.colectivos_facturador.facturacionB";
	public static final String ENDOSAR_POLIZA 				 = "gioseg.colectivos.endosoPoliza";
	public static final String ALTA_CLIENTE				     = "gioseg.clien10010.busca_cliente";
	public static final String VALIDA_FECHA				     = "gioseg.colectivos.validaFechaNacimiento";
	public static final String PRE_FACTURA_DEV               = "gioseg.colectivos_facturador.p_prefactura_devolucionInd";//JALM
	public static final String FACTURACION_DEV               = "gioseg.colectivos_facturador.FacturacionDevolucionInd";//JALM
	public static final String VALIDA_POLIZA_EMISION         = "gioseg.colectivos_utilerias.fValidaEmisionColectivos";
	public static final String GENERA_RFC                    = "gioseg.colectivos_utilerias.fValidaRfc";
	
	public static enum OPC_CANACEL{CANCELA,CAN_DEVO,COMP,DEV_COMP,TODAS,DEVO,ERROR};
	
	// ReportesCierre
	public static final String EXTRAECOMCOM					= "colectivos_reportes_cierre.ComComer";
	public static final String EXTRAERFI					= "gioseg.colectivos_reportes_cierre.MonTotRfi";
	public static final String EXTRAECOBR					= "gioseg.colectivos_reportes_cierre.RecCob";
	public static final String EXTRAEREPEMI					= "gioseg.colectivos_reportes_cierre.ExtraeRepEmi";
	public static final String EXTRAESNCOBR					= "gioseg.colectivos_reportes_cierre.RecSnCob";
	public static final String EXTRAEREPTOTCOA				= "gioseg.colectivos_reportes_cierre.RepTotCoa";
	public static final String EXTRAECIFCOM					= "gioseg.colectivos_reportes_cierre.CifCom";
	public static final String EXTRAECIFCOMU				= "gioseg.colectivos_reportes_cierre.CifComU";
	public static final String EXTRAEREPDPOD				= "gioseg.colectivos_reportes_cierre.RepDetDpoDosOp";
	public static final String EXTRAEREPDPOC				= "gioseg.colectivos_reportes_cierre.RepDetDpoCuatroOP";
	public static final String EXTRAECOMTEC					= "gioseg.colectivos_reportes_cierre.ComTecnicas";
	public static final String EXTRAECIFTEC					= "gioseg.colectivos_reportes_cierre.CifrasTecnicas";
	public static final String EXTRAEDETARFI				= "gioseg.colectivos_reportes_cierre.DetaRfi";
	
	//-------- VDA CORTO PLAZO
	public static final String BUSCA_CLIENTE				 = "gioseg.clien10010.busca_cliente";
	public static final String NEGATIVE_FILE				 = "gioseg.busca_negative";
	
	public static final String CONTCOLECTIVOS1				 = "gioseg.CONT20010.contabilidad_colectivos1";
	public static final String CONTCOLECTIVOS2				 = "gioseg.CONT20010.contabilidad_colectivos2";
	public static final String CARGACUENTA				 	 = "gioseg.CONT20010.CONTABILIDAD_PRIMAS_DEPOSITO";
	
	
	public static final String PROCESA_RESERVA     	 	     = "gioseg.colectivos_reportes_cierre.f_calcrsva_pma_recurrente";
	public static final String CONSULTA_RESERVA     	 	 = "gioseg.colectivos_reportes_cierre.RepPmaGan";

	public static final String GENERA_CLIENTE			     = "gioseg.colectivos.generaCliente";
	public static final String NO_ENDOSO_DETALLE			 = "gioseg.colectivos.noEndosoDetalle";
	
	public static final String MIGRA_COLECTIVOS				 = "gioseg.pack_migracol_sin.p_migracion_colectivos";
	public static final String UPDATE_CERTIFICADOS			 = "gioseg.pack_migracol_sin.P_update_certificados";
	public static final String UPDATE_COMPONENTES			 = "gioseg.pack_migracol_sin.P_update_componentes";
	public static final String UPDATE_COBERTURAS			 = "gioseg.pack_migracol_sin.P_update_coberturas";
	
	//Para Timbrado
	public static final String GET_DATOS_TIMBRADO			 = "gioseg.COLECTIVOS_CFDI_TIMBRADO.P_GET_DATOS_RECIBO";
	public static final String PROCESO_CARGA_COBRANZA	     = "gioseg.COLECTIVOS_FACTURADOR.ProcesoCobrazaPoliza";
	
}
