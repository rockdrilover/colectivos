package mx.com.santander.aseguradora.colectivos.model.database;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.dao.CatalogoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

public interface EndosoDatosDao extends CatalogoDao {
	
	/**
	 * Metodo que ejecuta una consulta en especifico
	 * @param strQuery Query a ejecutar
	 * @return Lista con resultados de consulta
	 * @throws Excepciones error de consulta
	 */
	List<Object> consultar(String strQuery) throws Excepciones;

	/**
	 * Metodo que guarda nuevos beneficiarios
	 * @param seleccionado datos a guardar.
	 * @throws Excepciones error de proceso
	 */
	void actualizaBeneficiario(EndosoDatosDTO seleccionado) throws Excepciones;
	
}
