package mx.com.santander.aseguradora.colectivos.model.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CargaId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;

public class ProcesosPL {
	
	private static final Log LOG = LogFactory.getLog(ProcesosPL.class);
	private CallableStatement enable_dbms_output;
	private CallableStatement disable_dbms_output;
	private CallableStatement show__dbms_output;
	private CallableStatement divisionAutoCredito;
	private CallableStatement validaMultidisposicion;
	private CallableStatement validaCumulos;
	private CallableStatement respaldarErrores;
	
	private Connection conn;
	
	public ProcesosPL( Connection conn ) throws SQLException {
		
		this.conn = conn;
		
		enable_dbms_output  = conn.prepareCall ("begin dbms_output.enable(?); end;" );
		disable_dbms_output = conn.prepareCall ("begin dbms_output.disable; end;" );
		  
		show__dbms_output = conn.prepareCall(
			"declare 					\n" +
			"   l_line varchar2(255); 	\n" +
			"   l_done number; 			\n" +
			"   l_buffer long; 			\n" +
			"begin 						\n" +
			"   loop 					\n" +
			"      exit when length(l_buffer)+255 > ? OR l_done = 1; 	\n" +
			"      l_line := ''; 		\n" +
			"      dbms_output.get_line( l_line, l_done ); 				\n" +
			"      l_buffer := l_buffer || l_line || chr(10); 			\n" +
			"   end loop; 				\n" +
			"   ? := l_done; 			\n" +
			"   ? := l_buffer; 			\n" +
			"end; 						\n" 
		);
	}
	
	/**
	 * Metodo que divide los ramos para un credito de auto
	 */
	public void divisionAutoCredito() {
		StringBuilder sb = new StringBuilder();
		Archivo objArchivo;
		
		try {
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("Division de Ramos.tst");
			
			this.divisionAutoCredito = conn.prepareCall(sb.toString());
			this.divisionAutoCredito.execute();
		} catch (Excepciones | SQLException e) {
			LOG.error(e);
		}
		
	}
	
	public void validarMultidisposicion(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta) {
		StringBuilder sb = new StringBuilder();
		Archivo objArchivo;
		
		try {
			
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("Validacion LineaExpress Multi_Rest.tst");
		
			validaMultidisposicion = conn.prepareCall(sb.toString());
			validaMultidisposicion.setShort  (1,inCanal);
			validaMultidisposicion.setShort  (2,inRamo);
			validaMultidisposicion.setLong   (3,inPoliza);
	        
			validaMultidisposicion.execute();
			validaMultidisposicion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void respaldarErrores(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta, String nombreArchivo) {
		StringBuilder sb = new StringBuilder();
		Archivo objArchivo;
		
		try {
			
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("RegistrarErrores.tst");
		
			respaldarErrores = conn.prepareCall(sb.toString());
			respaldarErrores.setShort    (1,inCanal);
			respaldarErrores.setShort    (2,inRamo);
			respaldarErrores.setLong     (3,inPoliza);
			respaldarErrores.setInt      (4,inIdVenta);
			respaldarErrores.setString   (5,nombreArchivo);
			respaldarErrores.execute();
			respaldarErrores.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void showOuput() throws SQLException	{
		
		boolean flag = true;
		
		show__dbms_output.registerOutParameter(2, java.sql.Types.INTEGER );
		show__dbms_output.registerOutParameter(3, java.sql.Types.VARCHAR );
		 
		while(flag){
			show__dbms_output.setInt(1, 32000 );
			show__dbms_output.execute();
			System.out.print("PL/SQL DBMS: " + show__dbms_output.getString(3));
			
			if (show__dbms_output.getInt(2) == 1) flag = false;
		}
	}
	 
	public void close() throws SQLException	{
		enable_dbms_output.close();
		disable_dbms_output.close();
		show__dbms_output.close();
	}

	public void validaCumulosBT() {
		StringBuilder sb = new StringBuilder();
		Archivo objArchivo;
		
		try {
			
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("CumulosBT.tst");
			
			validaCumulos = conn.prepareCall(sb.toString());
			validaCumulos.execute();
			validaCumulos.close();
			
		}  catch (Exception e) {
			LOG.error(e);
		} 
		
	}

	/**
	 * Metodo que ejecuta proceso de Calcula de Primas
	 * @param id datos para procesar 
	 * @throws Excepciones error de proceso
	 */
	public void calculaPrima(CargaId id) throws Excepciones {
		CallableStatement calculaPrimas = null;
		Archivo objArchivo;
		StringBuilder sb = null;
		
		try {
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("Proceso_Calcula_Primas.tst");
			
			calculaPrimas = conn.prepareCall(sb.toString());
			calculaPrimas.setShort(1, id.getCocrCdSucursal());
			calculaPrimas.setShort(2, id.getCocrCdRamo());
			calculaPrimas.setLong(3, id.getCocrNumPoliza());
			calculaPrimas.setString(4, id.getCocrIdCertificado());
			calculaPrimas.setBigDecimal(5, id.getCocrSumaAsegurada());
	        
			calculaPrimas.execute();
		} catch (Excepciones|SQLException e) {
			throw new Excepciones("Error: en el proceso calcular Primas", e);
		} finally {
			if(calculaPrimas != null) {
				try {
					calculaPrimas.close();
				} catch (SQLException e) { 
					LOG.error(e);
				}
			}
		}
		
	}
	
	/**
	 * Metodo que ejecuta proceso de migracion de beneficiarios
	 * @param id datos para migrar
	 * @throws Excepciones error de ejecucion
	 */
	public void migraBeneficiarios(EndososDatosId id) throws Excepciones {
		CallableStatement migraBene = null;
		Archivo objArchivo;
		StringBuilder sb = null;
		
		try {
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("Migra_Beneficiarios.tst");
			
			migraBene = conn.prepareCall(sb.toString());
			migraBene.setShort(1, id.getCedaCasuCdSucursal());
			migraBene.setShort(2, id.getCedaCarpCdRamo());
			migraBene.setLong(3, id.getCedaCapoNuPoliza());
			migraBene.setLong(4, id.getCedaNuCertificado());
	        
			migraBene.execute();
		} catch (Excepciones|SQLException e) {
			throw new Excepciones("Error: en el proceso migracion.", e);
		} finally {
			if(migraBene != null) {
				try {
					migraBene.close();
				} catch (SQLException e) { 
					LOG.error(e);
				}
			}
		}
		
	}
	
	/**
	 * Metodo que manda ejecutar proceso que guarda foto de cedula
	 * @param tipocedula tipo de cedula
	 * @param idVenta id venta del producto
	 * @param coccRemesaCedula remesa
	 * @param tipoCedula2 tipo de cedula 2 para historicos
	 * @throws Excepciones con error en general
	 */
	public void saveFotoCedula(Integer tipocedula, Integer idVenta, String coccRemesaCedula, Integer tipoCedula2) throws Excepciones {
		CallableStatement saveFoto = null;
		StringBuilder sb = null;
		
		try {
			sb = GeneratorQuerys.declareCedula;
			
			saveFoto = conn.prepareCall(sb.toString());
			saveFoto.setString(1, coccRemesaCedula);
			saveFoto.setInt(2, idVenta);
			saveFoto.setInt(3, tipocedula);
			saveFoto.setInt(4, tipoCedula2);
	        
			saveFoto.execute();
		} catch (SQLException e) {
			throw new Excepciones("Error: en el proceso de genera historia cedula.", e);
		} finally {
			if(saveFoto != null) {
				try {
					saveFoto.close();
				} catch (SQLException e) { 
					LOG.error(e);
				}
			}
		}
	}
}
