package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Column;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para mapeo de tabla COLECTIVOS_CEDULA
 * 				Se ocupa en clase ColectivosCedula como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class ColectivosCedula3 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccFeProceso
	@Temporal(TemporalType.DATE)
	@Column(name="COCC_FE_PROCESO")
	private Date coccFeProceso;
	
	//Propiedad para campo coccNuRegistros
	@Column(name="COCC_NU_REGISTROS")
	private BigDecimal coccNuRegistros;
	
	//Propiedad para campo coccNuRegSinpndNook
	@Column(name="COCC_NU_REG_SINPND_NOOK")
	private BigDecimal coccNuRegSinpndNook;

	//Propiedad para campo coccArchivo
	@Column(name="COCC_ARCHIVO")
	private String coccArchivo;

	//Propiedad para campo coccTipoCedula
	@Column(name="COCC_TIPO_CEDULA")
	private BigDecimal coccTipoCedula;

	//Propiedad para campo coccProcesada
	@Column(name="COCC_PROCESADA")
	private BigDecimal coccProcesada;

	/**
	 * Constructor de clase
	 */
	public ColectivosCedula3() {
		// Do nothing because 
	}
	
	/**
	 * @return the coccFeProceso
	 */
	public Date getCoccFeProceso() {
		return new Date( ((Date) Utilerias.objectIsNull(coccFeProceso, Constantes.TIPO_DATO_DATE)).getTime());
	}
	/**
	 * @return the coccNuRegistros
	 */
	public BigDecimal getCoccNuRegistros() {
		return coccNuRegistros;
	}
	/**
	 * @return the coccNuRegSinpndNook
	 */
	public BigDecimal getCoccNuRegSinpndNook() {
		return coccNuRegSinpndNook;
	}
	/**
	 * @return the coccArchivo
	 */
	public String getCoccArchivo() {
		return coccArchivo;
	}
	/**
	 * @return the coccTipoCedula
	 */
	public BigDecimal getCoccTipoCedula() {
		return coccTipoCedula;
	}
	/**
	 * @return the coccProcesada
	 */
	public BigDecimal getCoccProcesada() {
		return coccProcesada;
	}
}