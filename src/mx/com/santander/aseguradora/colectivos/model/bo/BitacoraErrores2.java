package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para mapeo de tabla COLECTIVOS_BITACORA_ERRORES
 * 				Se ocupa en clase BitacoraErrores como @Embedded
 * 				Se tiene que agregegar @Embeddable la anotacion en la clase
 * 				para que las consultas traigan los campos de esta clase 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class BitacoraErrores2 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo coerLinea
	@Column(name="COER_LINEA")
	private String coerLinea;

	//Propiedad para campo coerNombreArchivo
	@Column(name="COER_NOMBRE_ARCHIVO")
	private String coerNombreArchivo;

	//Propiedad para campo coerNuCredito
	@Column(name="COER_NU_CREDITO")
	private String coerNuCredito;
	
	//Propiedad para campo coerFechaCarga
	@Temporal(TemporalType.DATE)
	@Column(name="COER_FECHA_CARGA")
	private Date coerFechaCarga;

	//Propiedad para campo coerFvalor1
	@Temporal(TemporalType.DATE)
	@Column(name="COER_FVALOR1")
	private Date coerFvalor1;

	//Propiedad para campo coerFvalor2
	@Temporal(TemporalType.DATE)
	@Column(name="COER_FVALOR2")
	private Date coerFvalor2;

	//Propiedad para campo coerNvalor1
	@Column(name="COER_NVALOR1")
	private BigDecimal coerNvalor1;

	//Propiedad para campo coerNvalor2
	@Column(name="COER_NVALOR2")
	private BigDecimal coerNvalor2;

	//Propiedad para campo coerNvalor3
	@Column(name="COER_NVALOR3")
	private BigDecimal coerNvalor3;


	/**
	 * Constructor de clase
	 */
	public BitacoraErrores2() {
		coerFechaCarga = new Date();
		coerFvalor1 = new Date();
		coerFvalor2 = new Date();
	}
	/**
	 * @return the coerLinea
	 */
	public String getCoerLinea() {
		return coerLinea;
	}
	/**
	 * @return the coerNombreArchivo
	 */
	public String getCoerNombreArchivo() {
		return coerNombreArchivo;
	}
	/**
	 * @return the coerNuCredito
	 */
	public String getCoerNuCredito() {
		return coerNuCredito;
	}
	/**
	 * @return the coerFechaCarga
	 */
	public Date getCoerFechaCarga() {
		return new Date( ((Date) Utilerias.objectIsNull(coerFechaCarga, Constantes.TIPO_DATO_DATE)).getTime());
	}
	/**
	 * @return the coerFvalor1
	 */
	public Date getCoerFvalor1() {
		return new Date( ((Date) Utilerias.objectIsNull(coerFvalor1, Constantes.TIPO_DATO_DATE)).getTime());
	}
	/**
	 * @return the coerFvalor2
	 */
	public Date getCoerFvalor2() {
		return new Date( ((Date) Utilerias.objectIsNull(coerFvalor2, Constantes.TIPO_DATO_DATE)).getTime());
	}
	/**
	 * @return the coerNvalor1
	 */
	public BigDecimal getCoerNvalor1() {
		return coerNvalor1;
	}
	/**
	 * @return the coerNvalor2
	 */
	public BigDecimal getCoerNvalor2() {
		return coerNvalor2;
	}
	/**
	 * @return the coerNvalor3
	 */
	public BigDecimal getCoerNvalor3() {
		return coerNvalor3;
	}
		
}
	