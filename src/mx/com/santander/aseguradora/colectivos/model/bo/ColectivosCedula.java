package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.Embedded;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

/** =======================================================================
* Autor:		Ing. Issac Bautista
* Fecha:		28-05-2020
* Description: The persistent class for the COLECTIVOS_CEDULA database table.
* 				Se utiliza la clase CifrasControl2 como @Embedded para que 
* 				las consultas realizadas sobre esta tabla contemplen los campos 
* 				de esa clase
* ------------------------------------------------------------------------				
* Modificaciones
* ------------------------------------------------------------------------
* 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
* ------------------------------------------------------------------------
* 
*/
@Entity
@Table(name="COLECTIVOS_CEDULA")
public class ColectivosCedula implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo ID coccCdCedula
	@Id
	@Column(name="COCC_CD_CEDULA")
	private Long coccCdCedula;
	
	//Propiedad para campo coccCasuCdSucursal
	@Column(name="COCC_CASU_CD_SUCURSAL")
	private BigDecimal coccCasuCdSucursal;
	
	//Propiedad para campo coccCarpCdRamo
	@Column(name="COCC_CARP_CD_RAMO")
	private BigDecimal coccCarpCdRamo;
	
	//Propiedad para campo coccRemesaCedula
	@Column(name="COCC_REMESA_CEDULA")
	private String coccRemesaCedula;

	//Propiedad para campo coccUnidadNegocio
	@Column(name="COCC_UNIDAD_NEGOCIO")
	private String coccUnidadNegocio;

	//Propiedad para campo coccFeRegistro
	@Temporal(TemporalType.DATE)
	@Column(name="COCC_FE_REGISTRO")
	private Date coccFeRegistro;

	//Propiedad para campo coccFeEntrega
	@Temporal(TemporalType.DATE)
	@Column(name="COCC_FE_ENTREGA")
	private Date coccFeEntrega;

	//Instancia de clase ColectivosCedula2
	@Embedded
	private ColectivosCedula2 cc2;

	//Instancia de clase ColectivosCedula3
	@Embedded
	private ColectivosCedula3 cc3;

	/**
	 * Constructor de clase
	 */
	public ColectivosCedula() {
		// Do nothing because 
	}

	/**
	 * @return the coccCdCedula
	 */
	public Long getCoccCdCedula() {
		return coccCdCedula;
	}
	/**
	 * @return the coccRemesaCedula
	 */
	public String getCoccRemesaCedula() {
		return coccRemesaCedula;
	}
	/**
	 * @return the coccUnidadNegocio
	 */
	public String getCoccUnidadNegocio() {
		return coccUnidadNegocio;
	}
	/**
	 * @return the coccFeRegistro
	 */
	public Date getCoccFeRegistro() {
		return new Date(coccFeRegistro.getTime());
	}
	/**
	 * @return the coccFeEntrega
	 */
	public Date getCoccFeEntrega() {
		return new Date(coccFeEntrega.getTime());
	}
	/**
	 * @return the coccCasuCdSucursal
	 */
	public BigDecimal getCoccCasuCdSucursal() {
		return coccCasuCdSucursal;
	}
	/**
	 * @return the cocCarpCdRamo
	 */
	public BigDecimal getCoccCarpCdRamo() {
		return coccCarpCdRamo;
	}
	/**
	 * @return the cc2
	 */
	public ColectivosCedula2 getCc2() {
		return cc2;
	}
	/**
	 * @return the cc3
	 */
	public ColectivosCedula3 getCc3() {
		return cc3;
	}
}