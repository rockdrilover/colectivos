package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-12-2022
 * Description: Clase para mapeo de tabla COLECTIVOS_COBRANZA_CARGA
 * 				Se ocupa en clase ColectivosCargaCobranza como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class ColectivosCargaCobranzaId implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo coccFacCanal
	@Column(name = "COCC_FAC_CANAL") 
	private int coccFacCanal;
	
	//Propiedad para campo coccFacRamo
	@Column(name = "COCC_FAC_RAMO")  
	private int coccFacRamo;
	
	//Propiedad para campo coccFacPoliza
	@Column(name = "COCC_FAC_POLIZA")
	private Long coccFacPoliza;
	
	//Propiedad para campo coccNuCredito
	@Column(name = "COCC_NU_CREDITO") 
	private String coccNuCredito;
	
	//Propiedad para campo coccFacRecibo
	@Column(name = "COCC_FAC_RECIBO")
	private BigDecimal coccFacRecibo;

	/**
	 * Constructor de clase
	 */
	public ColectivosCargaCobranzaId() {
		// Do nothing because 
	}
	
	/**
	 * @param coccFacCanal the coccFacCanal to set
	 */
	public void setCoccFacCanal(int coccFacCanal) {
		this.coccFacCanal = coccFacCanal;
	}

	/**
	 * @param coccFacRamo the coccFacRamo to set
	 */
	public void setCoccFacRamo(int coccFacRamo) {
		this.coccFacRamo = coccFacRamo;
	}

	/**
	 * @param coccFacPoliza the coccFacPoliza to set
	 */
	public void setCoccFacPoliza(Long coccFacPoliza) {
		this.coccFacPoliza = coccFacPoliza;
	}

	/**
	 * @param coccNuCredito the coccNuCredito to set
	 */
	public void setCoccNuCredito(String coccNuCredito) {
		this.coccNuCredito = coccNuCredito;
	}

	/**
	 * @param coccFacRecibo the coccFacRecibo to set
	 */
	public void setCoccFacRecibo(BigDecimal coccFacRecibo) {
		this.coccFacRecibo = coccFacRecibo;
	}
	
}
