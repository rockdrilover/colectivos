package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Embedded;
import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: The persistent class for the COLECTIVOS_BITACORA_ERRORES database table.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Entity
@Table(name="COLECTIVOS_BITACORA_ERRORES")
public class BitacoraErrores implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo id coerCdErrorBitacora
	@Id
	@Column(name="COER_CD_ERROR_BITACORA")
	private BigDecimal coerCdErrorBitacora;

	//Propiedad para campo coerCapoNuPoliza
	@Column(name="COER_CAPO_NU_POLIZA")
	private BigDecimal coerCapoNuPoliza;

	//Propiedad para campo coerCapoNuPoliza
	@Column(name="COER_CARP_CD_RAMO")
	private BigDecimal coerCarpCdRamo;
	
	//Propiedad para campo coerCapoNuPoliza
	@Column(name="COER_CASU_CD_SUCURSAL")
	private BigDecimal coerCasuCdSucursal;

	//Propiedad para campo coerCapoNuPoliza
	@Column(name="COER_CD_ERROR")
	private BigDecimal coerCdError;

	//Propiedad para campo coerCapoNuPoliza
	@Column(name="COER_DESC_ERROR")
	private String coerDescError;

	//Propiedad para campo coerCapoNuPoliza
	@Column(name="COER_ID_VENTA")
	private BigDecimal coerIdVenta;

	//Instancia de clase BitacoraErrores2
	@Embedded
	private BitacoraErrores2 be2;
	
	//Instancia de clase BitacoraErrores3
	@Embedded
	private BitacoraErrores3 be3;
	
	/**
	 * Constructor de clase
	 */
	public BitacoraErrores() {
		// Do nothing because 
	}
	
	/**
	 * @return the coerCdErrorBitacora
	 */
	public BigDecimal getCoerCdErrorBitacora() {
		return coerCdErrorBitacora;
	}
	/**
	 * @return the coerCapoNuPoliza
	 */
	public BigDecimal getCoerCapoNuPoliza() {
		return coerCapoNuPoliza;
	}
	/**
	 * @return the coerCarpCdRamo
	 */
	public BigDecimal getCoerCarpCdRamo() {
		return coerCarpCdRamo;
	}
	/**
	 * @return the coerCasuCdSucursal
	 */
	public BigDecimal getCoerCasuCdSucursal() {
		return coerCasuCdSucursal;
	}
	/**
	 * @return the coerCdError
	 */
	public BigDecimal getCoerCdError() {
		return coerCdError;
	}
	/**
	 * @return the coerDescError
	 */
	public String getCoerDescError() {
		return coerDescError;
	}
	/**
	 * @return the coerIdVenta
	 */
	public BigDecimal getCoerIdVenta() {
		return coerIdVenta;
	}
	/**
	 * @return the be2
	 */
	public BitacoraErrores2 getBe2() {
		return be2;
	}
	/**
	 * @return the be3
	 */
	public BitacoraErrores3 getBe3() {
		return be3;
	}
	

}
	