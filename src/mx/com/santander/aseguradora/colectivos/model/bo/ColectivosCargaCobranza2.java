package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-12-2022
 * Description: Clase para mapeo de tabla COLECTIVOS_COBRANZA_CARGA
 * 				Se ocupa en clase ColectivosCargaCobranza como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class ColectivosCargaCobranza2 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo coccFacMesContable
	@Temporal(TemporalType.DATE)
	@Column(name = "COCC_FAC_MES_CONTABLE")  
	private Date coccFacMesContable;
	
	//Propiedad para campo coccFacPrimaNeta
	@Column(name = "COCC_FAC_PRIMA_NETA")
	private BigDecimal coccFacPrimaNeta; 
	
	//Propiedad para campo coccFacRfi
	@Column(name = "COCC_FAC_RFI") 
	private BigDecimal coccFacRfi;
	
	//Propiedad para campo coccFacPrimaTotal
	@Column(name = "COCC_FAC_PRIMA_TOTAL")
	private BigDecimal coccFacPrimaTotal;
	
	//Propiedad para campo coccCobCanal
	@Column(name = "COCC_COB_CANAL")
	private int coccCobCanal;
	
	//Propiedad para campo coccCobRamo
	@Column(name = "COCC_COB_RAMO")
	private int coccCobRamo;
	
	//Propiedad para campo coccCobPoliza
	@Column(name = "COCC_COB_POLIZA")
	private Long coccCobPoliza;
	
	//Propiedad para campo coccCobRecibo
	@Column(name = "COCC_COB_RECIBO")
	private BigDecimal coccCobRecibo;
	
	//Propiedad para campo coccCobFechaCobro
	@Temporal(TemporalType.DATE)
	@Column(name = "COCC_COB_FECHA_COBRO")  
	private Date coccCobFechaCobro;
	
	/**
	 * Constructor de clase
	 */
	public ColectivosCargaCobranza2() {
		// Do nothing because 
	}

	/**
	 * @param coccFacMesContable the coccFacMesContable to set
	 */
	public void setCoccFacMesContable(Date coccFacMesContable) {
		this.coccFacMesContable = new Date(coccFacMesContable.getTime());
	}

	/**
	 * @param coccFacPrimaNeta the coccFacPrimaNeta to set
	 */
	public void setCoccFacPrimaNeta(BigDecimal coccFacPrimaNeta) {
		this.coccFacPrimaNeta = coccFacPrimaNeta;
	}

	/**
	 * @param coccFacRfi the coccFacRfi to set
	 */
	public void setCoccFacRfi(BigDecimal coccFacRfi) {
		this.coccFacRfi = coccFacRfi;
	}

	/**
	 * @param coccFacPrimaTotal the coccFacPrimaTotal to set
	 */
	public void setCoccFacPrimaTotal(BigDecimal coccFacPrimaTotal) {
		this.coccFacPrimaTotal = coccFacPrimaTotal;
	}

	/**
	 * @param coccCobCanal the coccCobCanal to set
	 */
	public void setCoccCobCanal(int coccCobCanal) {
		this.coccCobCanal = coccCobCanal;
	}

	/**
	 * @param coccCobRamo the coccCobRamo to set
	 */
	public void setCoccCobRamo(int coccCobRamo) {
		this.coccCobRamo = coccCobRamo;
	}

	/**
	 * @param coccCobPoliza the coccCobPoliza to set
	 */
	public void setCoccCobPoliza(Long coccCobPoliza) {
		this.coccCobPoliza = coccCobPoliza;
	}

	/**
	 * @param coccCobRecibo the coccCobRecibo to set
	 */
	public void setCoccCobRecibo(BigDecimal coccCobRecibo) {
		this.coccCobRecibo = coccCobRecibo;
	}

	/**
	 * @param coccCobFechaCobro the coccCobFechaCobro to set
	 */
	public void setCoccCobFechaCobro(Date coccCobFechaCobro) {
		this.coccCobFechaCobro = new Date(coccCobFechaCobro.getTime());
	}
	
	
}
