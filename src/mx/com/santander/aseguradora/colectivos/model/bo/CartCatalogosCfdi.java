package mx.com.santander.aseguradora.colectivos.model.bo;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: The persistent class for the CART_CATALOGOS_CFDI database table.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Entity
@Table(name = "CART_CATALOGOS_CFDI")
public class CartCatalogosCfdi implements java.io.Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 4894883100456758389L;
	
	//Instancia de clase CartCatalogosCfdiId
	@EmbeddedId
	private CartCatalogosCfdiId id;
	
	//Propiedad para campo coerTipoError
	@Column(name = "CARTCF_NOM_CATALOGO")
	private String cartcfNomCatalogo;
	
	//Propiedad para campo cartcfValor1
	@Column(name = "CARTCF_VALOR1")
	private String cartcfValor1;
	
	//Propiedad para campo cartcfValor2
	@Column(name = "CARTCF_VALOR2")
	private String cartcfValor2;
	
	//Propiedad para campo cartcfValor3
	@Column(name = "CARTCF_VALOR3")
	private String cartcfValor3;
	
	//Propiedad para campo cartcfValor4
	@Column(name = "CARTCF_VALOR4")
	private String cartcfValor4;
	
	//Propiedad para campo cartcfValor5
	@Column(name = "CARTCF_VALOR5")
	private String cartcfValor5;
	
	//Propiedad para campo cartcfValor6
	@Column(name = "CARTCF_VALOR6")
	private String cartcfValor6;
	
	//Propiedad para campo cartcfValor7
	@Column(name = "CARTCF_VALOR7")
	private String cartcfValor7;
	
	/**
	 * Constructor de clase
	 */
	public CartCatalogosCfdi() {
		// Do nothing because 
	}
	
	/**
	 * @return the id
	 */
	public CartCatalogosCfdiId getId() {
		return id;
	}
	/**
	 * @return the cartcfNomCatalogo
	 */
	public String getCartcfNomCatalogo() {
		return cartcfNomCatalogo;
	}
	/**
	 * @return the cartcfValor1
	 */
	public String getCartcfValor1() {
		return cartcfValor1;
	}
	/**
	 * @return the cartcfValor2
	 */
	public String getCartcfValor2() {
		return cartcfValor2;
	}
	/**
	 * @return the cartcfValor3
	 */
	public String getCartcfValor3() {
		return cartcfValor3;
	}
	/**
	 * @return the cartcfValor4
	 */
	public String getCartcfValor4() {
		return cartcfValor4;
	}
	/**
	 * @return the cartcfValor5
	 */
	public String getCartcfValor5() {
		return cartcfValor5;
	}
	/**
	 * @return the cartcfValor6
	 */
	public String getCartcfValor6() {
		return cartcfValor6;
	}
	/**
	 * @return the cartcfValor7
	 */
	public String getCartcfValor7() {
		return cartcfValor7;
	}

}
