package mx.com.santander.aseguradora.colectivos.model.bo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase para mapeo de tabla CART_CATALOGOS_CFDI
 * 				Se ocupa en clase CartCatalogosCfdi como @EmbeddedId
 * 				Se tiene que agregegar @EmbeddableId la anotacion en la clase
 * 				para que las consultas traigan los campos de esta clase 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class CartCatalogosCfdiId implements java.io.Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 7820789334792077385L;
	
	//Propiedad para campo cartcfCdCfdi
	@Column(name="CARTCF_CD_CFDI")
	private BigDecimal cartcfCdCfdi;
	
	//Propiedad para campo cartcfCdClave
	@Column(name="CARTCF_CD_CLAVE")
	private String cartcfCdClave;

	/**
	 * Constructor de clase
	 */
	public CartCatalogosCfdiId() {
		// Do nothing because 
	}


	/**
	 * @return the cartcfCdCfdi
	 */
	public BigDecimal getCartcfCdCfdi() {
		return cartcfCdCfdi;
	}
	/**
	 * @return the cartcfCdClave
	 */
	public String getCartcfCdClave() {
		return cartcfCdClave;
	}

}
