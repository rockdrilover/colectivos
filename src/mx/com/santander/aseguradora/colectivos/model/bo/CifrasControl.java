package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: The persistent class for the COLECTIVOS_CIFRAS_CONTROL database table.
 * 				Se utiliza la clase CifrasControl2 como @Embedded para que 
 * 				las consultas realizadas sobre esta tabla contemplen los campos 
 * 				de esa clase
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Entity
@Table(name="COLECTIVOS_CIFRAS_CONTROL")
public class CifrasControl implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo id cociCdControl
	@Id
	@Column(name="COCI_CD_CONTROL")
	private BigDecimal cociCdControl;
	
	//Propiedad para campo cociCapoNuPoliza
	@Column(name="COCI_CAPO_NU_POLIZA")
	private BigDecimal cociCapoNuPoliza;

	//Propiedad para campo cociCarpCdRamo
	@Column(name="COCI_CARP_CD_RAMO")
	private BigDecimal cociCarpCdRamo;

	//Propiedad para campo cociCasuCdSucursal
	@Column(name="COCI_CASU_CD_SUCURSAL")
	private BigDecimal cociCasuCdSucursal;

	//Propiedad para campo cociFechaCarga
	@Temporal(TemporalType.DATE)
	@Column(name="COCI_FECHA_CARGA")
	private Date cociFechaCarga;

	//Propiedad para campo cociFvalor1
	@Temporal(TemporalType.DATE)
	@Column(name="COCI_FVALOR1")
	private Date cociFvalor1;

	//Propiedad para campo cociFvalor2
	@Temporal(TemporalType.DATE)
	@Column(name="COCI_FVALOR2")
	private Date cociFvalor2;

	//Propiedad para campo cociIdVenta
	@Column(name="COCI_ID_VENTA")
	private BigDecimal cociIdVenta;

	//Instancia de clase CifrasControl2
	@Embedded
	private CifrasControl2 cc2;
	
	
	/**
	 * Constructor de clase
	 */
	public CifrasControl() {
		// Do nothing because 
	}
	
	/**
	 * @return the cociCdControl
	 */
	public BigDecimal getCociCdControl() {
		return cociCdControl;
	}
	/**
	 * @return the cociCapoNuPoliza
	 */
	public BigDecimal getCociCapoNuPoliza() {
		return cociCapoNuPoliza;
	}
	/**
	 * @return the cociCarpCdRamo
	 */
	public BigDecimal getCociCarpCdRamo() {
		return cociCarpCdRamo;
	}
	/**
	 * @return the cociCasuCdSucursal
	 */
	public BigDecimal getCociCasuCdSucursal() {
		return cociCasuCdSucursal;
	}
	/**
	 * @return the cociFechaCarga
	 */
	public Date getCociFechaCarga() {
		return new Date( ((Date) Utilerias.objectIsNull(cociFechaCarga, Constantes.TIPO_DATO_DATE)).getTime());
	}
	/**
	 * @return the cociFvalor1
	 */
	public Date getCociFvalor1() {
		return new Date( ((Date) Utilerias.objectIsNull(cociFvalor1, Constantes.TIPO_DATO_DATE)).getTime());				
	}
	/**
	 * @return the cociFvalor2
	 */
	public Date getCociFvalor2() {
		return new Date( ((Date) Utilerias.objectIsNull(cociFvalor2, Constantes.TIPO_DATO_DATE)).getTime());
	}
	/**
	 * @return the cociIdVenta
	 */
	public BigDecimal getCociIdVenta() {
		return cociIdVenta;
	}
	/**
	 * @return the cc2
	 */
	public CifrasControl2 getCc2() {
		return cc2;
	}

}