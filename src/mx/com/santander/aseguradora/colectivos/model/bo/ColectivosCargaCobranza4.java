package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-12-2022
 * Description: Clase para mapeo de tabla COLECTIVOS_COBRANZA_CARGA
 * 				Se ocupa en clase ColectivosCargaCobranza como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class ColectivosCargaCobranza4 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccNocRfi
	@Column(name = "COCC_NOC_RFI") 
	private BigDecimal coccNocRfi;
	
	//Propiedad para campo coccNocIva
	@Column(name = "COCC_NOC_IVA")
	private BigDecimal coccNocIva;
	
	//Propiedad para campo coccNocPrimaTotal
	@Column(name = "COCC_NOC_PRIMA_TOTAL") 
	private BigDecimal coccNocPrimaTotal;
	
	//Propiedad para campo coccSaldoDeudor
	@Column(name = "COCC_SALDO_DEUDOR") 
	private BigDecimal coccSaldoDeudor;
	
	//Propiedad para campo coccCargada
	@Column(name = "COCC_CARGADA") 
	private int coccCargada;
	
	//Propiedad para campo coccFechaNc
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "COCC_FECHA_NC") 
	private Date coccFechaNc;
	
	//Propiedad para campo coccArchivo
	@Column(name = "COCC_ARCHIVO") 
	private String coccArchivo;
	
	//Propiedad para campo coccLinea
	@Column(name = "COCC_LINEA")
	private String coccLinea;
	
	//Propiedad para campo coccFacIva
	@Column(name = "COCC_FAC_IVA")
	private BigDecimal coccFacIva;
	
	/**
	 * Constructor de clase
	 */
	public ColectivosCargaCobranza4() {
		// Do nothing because 
	}

	/**
	 * @param coccNocRfi the coccNocRfi to set
	 */
	public void setCoccNocRfi(BigDecimal coccNocRfi) {
		this.coccNocRfi = coccNocRfi;
	}

	/**
	 * @param coccNocIva the coccNocIva to set
	 */
	public void setCoccNocIva(BigDecimal coccNocIva) {
		this.coccNocIva = coccNocIva;
	}

	/**
	 * @param coccNocPrimaTotal the coccNocPrimaTotal to set
	 */
	public void setCoccNocPrimaTotal(BigDecimal coccNocPrimaTotal) {
		this.coccNocPrimaTotal = coccNocPrimaTotal;
	}

	/**
	 * @param coccSaldoDeudor the coccSaldoDeudor to set
	 */
	public void setCoccSaldoDeudor(BigDecimal coccSaldoDeudor) {
		this.coccSaldoDeudor = coccSaldoDeudor;
	}

	/**
	 * @param coccCargada the coccCargada to set
	 */
	public void setCoccCargada(int coccCargada) {
		this.coccCargada = coccCargada;
	}

	/**
	 * @param coccFechaNc the coccFechaNc to set
	 */
	public void setCoccFechaNc(Date coccFechaNc) {
		this.coccFechaNc = new Date(coccFechaNc.getTime());
	}

	/**
	 * @param coccArchivo the coccArchivo to set
	 */
	public void setCoccArchivo(String coccArchivo) {
		this.coccArchivo = coccArchivo;
	}

	/**
	 * @param coccLinea the coccLinea to set
	 */
	public void setCoccLinea(String coccLinea) {
		this.coccLinea = coccLinea;
	}

	/**
	 * @param coccFacIva the coccFacIva to set
	 */
	public void setCoccFacIva(BigDecimal coccFacIva) {
		this.coccFacIva = coccFacIva;
	}
	
	
}
