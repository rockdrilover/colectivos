package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para mapeo de tabla COLECTIVOS_BITACORA_ERRORES
 * 				Se ocupa en clase BitacoraErrores como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class BitacoraErrores3 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo coerTipoError
	@Column(name="COER_TIPO_ERROR")
	private String coerTipoError;

	//Propiedad para campo coerTipoProceso
	@Column(name="COER_TIPO_PROCESO")
	private String coerTipoProceso;

	//Propiedad para campo coerVvalor1
	@Column(name="COER_VVALOR1")
	private String coerVvalor1;

	//Propiedad para campo coerVvalor2
	@Column(name="COER_VVALOR2")
	private String coerVvalor2;

	//Propiedad para campo coerVvalor3
	@Column(name="COER_VVALOR3")
	private String coerVvalor3;
	
	/**
	 * Construtor de clase
	 */
	public BitacoraErrores3() {
		// Do nothing because 
	}

	/**
	 * @return the coerTipoError
	 */
	public String getCoerTipoError() {
		return coerTipoError;
	}
	/**
	 * @return the coerTipoProceso
	 */
	public String getCoerTipoProceso() {
		return coerTipoProceso;
	}
	/**
	 * @return the coerVvalor1
	 */
	public String getCoerVvalor1() {
		return coerVvalor1;
	}
	/**
	 * @return the coerVvalor2
	 */
	public String getCoerVvalor2() {
		return coerVvalor2;
	}
	/**
	 * @return the coerVvalor3
	 */
	public String getCoerVvalor3() {
		return coerVvalor3;
	}

}
	