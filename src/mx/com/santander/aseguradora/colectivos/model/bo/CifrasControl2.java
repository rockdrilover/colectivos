package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para mapeo de tabla COLECTIVOS_CIFRAS_CONTROL
 * 				Se ocupa en clase CifrasControl como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class CifrasControl2 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo cociNombreArchivo
	@Column(name="COCI_NOMBRE_ARCHIVO")
	private String cociNombreArchivo;

	//Propiedad para campo cociNuEmitidos
	@Column(name="COCI_NU_EMITIDOS")
	private BigDecimal cociNuEmitidos;

	//Propiedad para campo cociNuRechazados
	@Column(name="COCI_NU_RECHAZADOS")
	private BigDecimal cociNuRechazados;

	//Propiedad para campo cociNuTotal
	@Column(name="COCI_NU_TOTAL")
	private BigDecimal cociNuTotal;

	//Propiedad para campo cociNvalor1
	@Column(name="COCI_NVALOR1")
	private BigDecimal cociNvalor1;

	//Propiedad para campo cociNvalor2
	@Column(name="COCI_NVALOR2")
	private BigDecimal cociNvalor2;

	//Propiedad para campo cociNvalor3
	@Column(name="COCI_NVALOR3")
	private BigDecimal cociNvalor3;

	//Propiedad para campo cociVvalor1
	@Column(name="COCI_VVALOR1")
	private String cociVvalor1;

	//Propiedad para campo cociVvalor2
	@Column(name="COCI_VVALOR2")
	private String cociVvalor2;

	//Propiedad para campo cociVvalor3
	@Column(name="COCI_VVALOR3")
	private String cociVvalor3;

	/**
	 * Constructo de clase
	 */
	public CifrasControl2() {
		// Do nothing because 
	}

	/**
	 * @return the cociNombreArchivo
	 */
	public String getCociNombreArchivo() {
		return cociNombreArchivo;
	}

	/**
	 * @return the cociNuEmitidos
	 */
	public BigDecimal getCociNuEmitidos() {
		return cociNuEmitidos;
	}

	/**
	 * @return the cociNuRechazados
	 */
	public BigDecimal getCociNuRechazados() {
		return cociNuRechazados;
	}

	/**
	 * @return the cociNuTotal
	 */
	public BigDecimal getCociNuTotal() {
		return cociNuTotal;
	}

	/**
	 * @return the cociNvalor1
	 */
	public BigDecimal getCociNvalor1() {
		return cociNvalor1;
	}

	/**
	 * @return the cociNvalor2
	 */
	public BigDecimal getCociNvalor2() {
		return cociNvalor2;
	}

	/**
	 * @return the cociNvalor3
	 */
	public BigDecimal getCociNvalor3() {
		return cociNvalor3;
	}

	/**
	 * @return the cociVvalor1
	 */
	public String getCociVvalor1() {
		return cociVvalor1;
	}

	/**
	 * @return the cociVvalor2
	 */
	public String getCociVvalor2() {
		return cociVvalor2;
	}

}