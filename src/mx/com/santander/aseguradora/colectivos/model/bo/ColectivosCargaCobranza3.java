package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-12-2022
 * Description: Clase para mapeo de tabla COLECTIVOS_COBRANZA_CARGA
 * 				Se ocupa en clase ColectivosCargaCobranza como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class ColectivosCargaCobranza3 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo coccCobPrimaNeta
	@Column(name = "COCC_COB_PRIMA_NETA")
	private BigDecimal coccCobPrimaNeta;
	
	//Propiedad para campo coccCobRfi
	@Column(name = "COCC_COB_RFI")
	private BigDecimal coccCobRfi;	
	
	//Propiedad para campo coccCobIva
	@Column(name = "COCC_COB_IVA")
	private BigDecimal coccCobIva;
	
	//Propiedad para campo coccCobPrimaTotal
	@Column(name = "COCC_COB_PRIMA_TOTAL")
	private BigDecimal coccCobPrimaTotal;
	
	//Propiedad para campo coccNocCanal
	@Column(name = "COCC_NOC_CANAL") 
	private int coccNocCanal;
	
	//Propiedad para campo coccNocRamo
	@Column(name = "COCC_NOC_RAMO") 
	private int coccNocRamo;
	
	//Propiedad para campo coccNocPoliza
	@Column(name = "COCC_NOC_POLIZA")
	private Long coccNocPoliza;
	
	//Propiedad para campo coccNocRecibo
	@Column(name = "COCC_NOC_RECIBO")
	private BigDecimal coccNocRecibo;
	
	//Propiedad para campo coccNocPrimaNeta
	@Column(name = "COCC_NOC_PRIMA_NETA")
	private BigDecimal coccNocPrimaNeta;
	
	/**
	 * Constructor de clase
	 */
	public ColectivosCargaCobranza3() {
		// Do nothing because 
	}

	/**
	 * @param coccCobPrimaNeta the coccCobPrimaNeta to set
	 */
	public void setCoccCobPrimaNeta(BigDecimal coccCobPrimaNeta) {
		this.coccCobPrimaNeta = coccCobPrimaNeta;
	}

	/**
	 * @param coccCobRfi the coccCobRfi to set
	 */
	public void setCoccCobRfi(BigDecimal coccCobRfi) {
		this.coccCobRfi = coccCobRfi;
	}

	/**
	 * @param coccCobIva the coccCobIva to set
	 */
	public void setCoccCobIva(BigDecimal coccCobIva) {
		this.coccCobIva = coccCobIva;
	}

	/**
	 * @param coccCobPrimaTotal the coccCobPrimaTotal to set
	 */
	public void setCoccCobPrimaTotal(BigDecimal coccCobPrimaTotal) {
		this.coccCobPrimaTotal = coccCobPrimaTotal;
	}

	/**
	 * @param coccNocCanal the coccNocCanal to set
	 */
	public void setCoccNocCanal(int coccNocCanal) {
		this.coccNocCanal = coccNocCanal;
	}

	/**
	 * @param coccNocRamo the coccNocRamo to set
	 */
	public void setCoccNocRamo(int coccNocRamo) {
		this.coccNocRamo = coccNocRamo;
	}

	/**
	 * @param coccNocPoliza the coccNocPoliza to set
	 */
	public void setCoccNocPoliza(Long coccNocPoliza) {
		this.coccNocPoliza = coccNocPoliza;
	}

	/**
	 * @param coccNocRecibo the coccNocRecibo to set
	 */
	public void setCoccNocRecibo(BigDecimal coccNocRecibo) {
		this.coccNocRecibo = coccNocRecibo;
	}

	/**
	 * @param coccNocPrimaNeta the coccNocPrimaNeta to set
	 */
	public void setCoccNocPrimaNeta(BigDecimal coccNocPrimaNeta) {
		this.coccNocPrimaNeta = coccNocPrimaNeta;
	}

	
	
}
