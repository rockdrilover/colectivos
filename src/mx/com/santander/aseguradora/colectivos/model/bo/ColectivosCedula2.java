package mx.com.santander.aseguradora.colectivos.model.bo;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import java.math.BigDecimal;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para mapeo de tabla COLECTIVOS_CEDULA
 * 				Se ocupa en clase ColectivosCedula como @Embedded
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Embeddable
public class ColectivosCedula2 implements Serializable {
	//Implementacion de serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccNuRegPnd
	@Column(name="COCC_NU_REG_PND")
	private BigDecimal coccNuRegPnd;

	//Propiedad para campo coccMtPrima
	@Column(name="COCC_MT_PRIMA")
	private BigDecimal coccMtPrima;

	//Propiedad para campo coccFeComer
	@Temporal(TemporalType.DATE)
	@Column(name="COCC_FE_COMER")
	private Date coccFeComer;

	//Propiedad para campo coccNuRegOk
	@Column(name="COCC_NU_REG_OK")
	private BigDecimal coccNuRegOk;

	//Propiedad para campo coccMtPrimaOk
	@Column(name="COCC_MT_PRIMA_OK")
	private BigDecimal coccMtPrimaOk;

	//Propiedad para campo coccNuRegNook
	@Column(name="COCC_NU_REG_NOOK")
	private BigDecimal coccNuRegNook;

	//Propiedad para campo coccMtPrimaNook
	@Column(name="COCC_MT_PRIMA_NOOK")
	private BigDecimal coccMtPrimaNook;

	//Propiedad para campo coccNuRegSinpnd
	@Column(name="COCC_NU_REG_SINPND")
	private BigDecimal coccNuRegSinpnd;

	//Propiedad para campo coccNuRegSinpndOk
	@Column(name="COCC_NU_REG_SINPND_OK")
	private BigDecimal coccNuRegSinpndOk;

	/**
	 * Constructor de clase
	 */
	public ColectivosCedula2() {
		// Do nothing because 
	}
	
	/**
	 * @return the coccNuRegPnd
	 */
	public BigDecimal getCoccNuRegPnd() {
		return coccNuRegPnd;
	}
	/**
	 * @return the coccMtPrima
	 */
	public BigDecimal getCoccMtPrima() {
		return coccMtPrima;
	}
	/**
	 * @return the coccFeComer
	 */
	public Date getCoccFeComer() {
		return new Date( ((Date) Utilerias.objectIsNull(coccFeComer, Constantes.TIPO_DATO_DATE)).getTime());
	}
	/**
	 * @return the coccNuRegOk
	 */
	public BigDecimal getCoccNuRegOk() {
		return coccNuRegOk;
	}
	/**
	 * @return the coccMtPrimaOk
	 */
	public BigDecimal getCoccMtPrimaOk() {
		return coccMtPrimaOk;
	}
	/**
	 * @return the coccNuRegNook
	 */
	public BigDecimal getCoccNuRegNook() {
		return coccNuRegNook;
	}
	/**
	 * @return the coccMtPrimaNook
	 */
	public BigDecimal getCoccMtPrimaNook() {
		return coccMtPrimaNook;
	}
	/**
	 * @return the coccNuRegSinpnd
	 */
	public BigDecimal getCoccNuRegSinpnd() {
		return coccNuRegSinpnd;
	}
	/**
	 * @return the coccNuRegSinpndOk
	 */
	public BigDecimal getCoccNuRegSinpndOk() {
		return coccNuRegSinpndOk;
	}
}