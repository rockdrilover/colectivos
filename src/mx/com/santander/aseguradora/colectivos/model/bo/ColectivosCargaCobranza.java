package mx.com.santander.aseguradora.colectivos.model.bo;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/** =======================================================================
* Autor:		Ing. Issac Bautista
* Fecha:		10-12-2022
* Description: The persistent class for the COLECTIVOS_COBRANZA_CARGA database table.
* 				Se utiliza las clases ColectivosCargaCobranza2, ColectivosCargaCobranza3,
* 				ColectivosCargaCobranza4 como @Embedded para que  las consultas 
* 				realizadas sobre esta tabla contemplen los campos de esa clase
* ------------------------------------------------------------------------				
* Modificaciones
* ------------------------------------------------------------------------
* 1.-	Por:		
 * 		Cuando: 
 * 		Porque: 
* ------------------------------------------------------------------------
* 
*/
@Entity
@Table(name="COLECTIVOS_COBRANZA_CARGA")
public class ColectivosCargaCobranza implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo ID coccCdCedula
	//Instancia de clase ColectivosCargaCobranzaId
	@EmbeddedId
	private ColectivosCargaCobranzaId id;
	
	//Instancia de clase ColectivosCargaCobranza2
	@Embedded
	private ColectivosCargaCobranza2 ccc2;

	//Instancia de clase ColectivosCargaCobranza3
	@Embedded
	private ColectivosCargaCobranza3 ccc3;

	//Instancia de clase ColectivosCargaCobranza4
	@Embedded
	private ColectivosCargaCobranza4 ccc4;
	
	/**
	 * Constructor de clase
	 */
	public ColectivosCargaCobranza() {
		id = new ColectivosCargaCobranzaId();
		ccc2 = new ColectivosCargaCobranza2();
		ccc3 = new ColectivosCargaCobranza3();
		ccc4 = new ColectivosCargaCobranza4(); 
	}
	
	/**
	 * @return the id
	 */
	public ColectivosCargaCobranzaId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(ColectivosCargaCobranzaId id) {
		this.id = id;
	}

	/**
	 * @return the ccc2
	 */
	public ColectivosCargaCobranza2 getCcc2() {
		return ccc2;
	}

	/**
	 * @param ccc2 the ccc2 to set
	 */
	public void setCcc2(ColectivosCargaCobranza2 ccc2) {
		this.ccc2 = ccc2;
	}

	/**
	 * @return the ccc3
	 */
	public ColectivosCargaCobranza3 getCcc3() {
		return ccc3;
	}

	/**
	 * @param ccc3 the ccc3 to set
	 */
	public void setCcc3(ColectivosCargaCobranza3 ccc3) {
		this.ccc3 = ccc3;
	}

	/**
	 * @return the ccc4
	 */
	public ColectivosCargaCobranza4 getCcc4() {
		return ccc4;
	}

	/**
	 * @param ccc4 the ccc4 to set
	 */
	public void setCcc4(ColectivosCargaCobranza4 ccc4) {
		this.ccc4 = ccc4;
	}
	
}
