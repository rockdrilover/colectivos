/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.ImpresionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioImpresion;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * @author FEBG
 *
 */
@Service
public class ServicioImpresionImpl implements ServicioImpresion {

	@Resource
	private ImpresionDao impresionDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	
	public ImpresionDao getImpresionDao() {
		return impresionDao;
	}
	
	public void setImpresionDao (ImpresionDao impresionDao) {
		this.impresionDao = impresionDao;
	}

	public ServicioImpresionImpl() {
		this.message = new StringBuilder();
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		
		this.message.append("obtenerClientesCertif.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			List<T> lista = this.impresionDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los clientes certififados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	@Override
	public <T> List<T> cargaPolizasImpresion(int ramo, Long poliza) throws Exception {
		try {
			return impresionDao.cargaPolizasImpresion(ramo, poliza);
		} catch (Exception e) {
			this.message.append("Error al cargar polizas para impresion.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	@Override
	public <T> List<T> obtenerRegistros(int ramo, Long poliza) throws Exception {
		try{
			return impresionDao.obtenerRegistros(ramo, poliza);
		} catch (Exception e) {
			this.message.append("Error al obtener las polizas para impresion.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	@Override	
	public String reporteAsegurado(String rutaTemp, short ramo, long poliza){
		return impresionDao.reporteAsegurado(rutaTemp, ramo, poliza);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}
}
