package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ReporteadorDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioReporteador;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Constantes.OPC_FORMATO_REPORTE;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * 
 * @author FEBG
 *
 */
@Service
public class ServicioReporteadorImpl implements ServicioReporteador {

	@Resource
	private CertificadoDao CertificadoDAO;
	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ReporteadorDao reporteadorDao;

	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message = null;
	private boolean flgJoinClientes = false, flgJoinAlternaPlanes = false, flgJoinColectivosCliente = false,
			flgJoinParametros = false, flgJoinHead = false, flgJoinClientes2 = false, flgJoinAlternaPlanes2 = false,
			flgJoinColectivosCliente2 = false, flgJoinParametros2 = false, flgJoinHead2 = false;
	public ServicioReporteadorImpl() {
		this.message = new StringBuilder();
	}

	public List<Object> consultarRegistros(Short ramo, Long poliza, Integer inIdVenta, Integer estatus, Date fecfin,
			Date fecini, String strCampos) throws Excepciones {
		List<Object> lstResultados;
		String strQuery;
		String strQuery2;
		String strQuery3;

		try {
			lstResultados = new ArrayList<Object>();
			flgJoinClientes = false;
			flgJoinAlternaPlanes = false;
			flgJoinColectivosCliente = false;
			flgJoinParametros = false;
			flgJoinHead = false;
			flgJoinClientes2 = false;
			flgJoinAlternaPlanes2 = false;
			flgJoinColectivosCliente2 = false;
			flgJoinParametros2 = false;
			flgJoinHead2 = false;
			strQuery = "SELECT " + strCampos + " FROM";
			strQuery2 = " UNION ALL \n";
			strQuery2 = "SELECT " + strCampos + " FROM";

			switch (estatus) {
			case 1:
				strQuery = crearSelectEmitidas(strCampos);
				strQuery += crearFiltroEmitido(ramo, poliza, inIdVenta, fecini, fecfin, strQuery.contains("GEPREFAC"));
				lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
				break;
			case 2:
				strQuery = crearSelectCancelado(strCampos);
				strQuery += crearFiltroCancelado(ramo, poliza, inIdVenta, fecini, fecfin,
						strQuery.contains("GEPREFAC"));
				lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
				break;
			case 3:
				strQuery = crearSelectFacturado(strCampos);
				strQuery += crearFiltroFacturado(ramo, poliza, inIdVenta, fecini, fecfin,
						strQuery.contains("GEPREFAC"));
				strQuery2 = crearSelectFacturado2(strCampos);
				strQuery2 += crearFiltroFacturado2(ramo, poliza, inIdVenta, fecini, fecfin,
						strQuery2.contains("GEPREFAC"));
				strQuery3 = strQuery + " \n" + " UNION ALL \n" + strQuery2;
				lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery3));
				break;
			}
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.consultarRegistros():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	/**
	 * Convierte archivo de texto a String
	 * @param nombreArchivo Nombre de archivo
	 * @return Cadena
	 * @throws Excepciones Error al abrir archivo
	 */
	private StringBuilder archivoToString(String nombreArchivo) throws Excepciones{
		String line;
  	    StringBuilder sb;
    	BufferedReader br = null;
    	
		try {	    	
	  	    sb = new StringBuilder();
	  	    br = new BufferedReader(new FileReader(FacesUtils.pathPl+File.separator+nombreArchivo));
			
			try {
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
			} finally {
				br.close();
			}
	    	
	    	return sb;
	    } catch (Exception e) {
	    	this.message.append("Error:: ServicioReporteadorImpl.archivoToString():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		} 	    
	}
	
	/**
	 * Reemplaza cadena en stringbuilder
	 * @param sb String builder
	 * @param pattern cadena a buscar
	 * @param replacement reemplazo
	 */
	public static void replaceAll(StringBuilder sb, Pattern pattern, String replacement) {
	    Matcher m = pattern.matcher(sb);
	    int start = 0;
	    while (m.find(start)) {
	        sb.replace(m.start(), m.end(), replacement);
	        start = m.start() + replacement.length();
	    }
	}
	
	@Override
	public String consultarRegistrosComisiones(Integer tipo, Short ramo, Long poliza, Integer inIdVenta, String fecfin, String fecini, String strCampos) throws Excepciones {
		
		StringBuilder strQuery = new StringBuilder();
		String columnas = Constantes.DEFAULT_STRING;
		String nomArchivo = Constantes.DEFAULT_STRING;
		
		switch (tipo) {
			case 7:
				columnas = "BUC,Poliza,Certificado,Porc_Comision,Ramo,DescripcionProducto,Canal,Sucursal,Tipo_Operacion,Prima_Neta,Derecho,Recargo,PrimaNetaAsistencia,IVA,Prima_Total,PrimaNetaCoa,DerechoCoa,RecargoCoa,PrimaNetaCoaAsistencia,IVACoa,PrimaTotalCoa,Monto_Comision,ClaveProducto,Fecha_Registro_Contable,FormaPago,Nombredelejecutivo,ID_Convenios,Descripcion_Convenio_Empresa,CentroCostos,Recibo,Credito,CreditoNuevo,StatusMov,Cuenta,Producto,SubProducto,ApellidoPaterno,ApellidoMaterno,Nombre,Sexo,FechaNacimiento,RFC,Plazo,FechaInicioPoliza,FechaFinPoliza,FechaIngreso,FechaDesde,FechaAnulaci�n,FechaCancelacion,SumaAsegurada,MontoDevolucion,BaseCalculo,Tarifa,CDEstado,Estado,CP,ProductoAseguradora,PlanAseguradora,Cuota,FechaNacimientoOSSexoOSTipodeIntervencionDescripcionParticipacon,FechadeEmision,FechadeCargaVENTA,DiferenciaDevolucion";
				strQuery = archivoToString("comisionTecnicoDetalleBanco.sql");
				nomArchivo = "Comision_Tecnica_Colectivos_Detalle_";
				break;
			case 8:
				columnas = "BUC,Poliza,Certificado,Porc_Comision,Ramo,DescripcionProducto,Canal,Sucursal,Tipo_Operacion,Prima_Neta,Derecho,Recargo,PrimaNetaAsistencia,IVA,Prima_Total,PrimaNetaCoa,DerechoCoa,RecargoCoa,PrimaNetaCoaAsistencia,IVACoa,PrimaTotalCoa,Monto_Comision,ClaveProducto,Fecha_Registro_Contable,FormaPago,Nombredelejecutivo,ID_Convenios,Descripcion_Convenio_Empresa,CentroCostos,Recibo,Credito,CreditoNuevo,StatusMov,Cuenta,Producto,SubProducto,ApellidoPaterno,ApellidoMaterno,Nombre,Sexo,FechaNacimiento,RFC,Plazo,FechaInicioPoliza,FechaFinPoliza,FechaIngreso,FechaDesde,FechaAnulaci�n,FechaCancelacion,SumaAsegurada,MontoDevolucion,BaseCalculo,Tarifa,CDEstado,Estado,CP,ProductoAseguradora,PlanAseguradora,Cuota,FechaNacimientoOSSexoOSTipodeIntervencionDescripcionParticipacon,FechadeEmision,FechadeCargaVENTA,DiferenciaDevolucion";
				strQuery = archivoToString("comisionTecnicoDetalleInterno.sql");
				nomArchivo = "Comision_Recargos_Tecnica_Colectivos_Detalle_";
				break;
			case 9:
				columnas = "BUC,POLIZA,CERTIFICADO,MONTOCOMISION,%COMISION,RAMO,DES_PRODUCTO,CANAL,SUCURSAL,Fecha_Suscripcion";
				strQuery = archivoToString("comisionComercialDetalleBanco.sql");
				nomArchivo = "Comision_Comercial_Colectivos_Detalle_";
				break;
			case 10:
				columnas = "Ramo,Poliza,FormadePago,PrimadeRenovaci�noPrimera�o,CentrodeCosto,Descripci�n,PrimaNetaEmitida,PrimaNetaCancelada,PrimaNetaDevoluciones,PrimaNetaAsistencia,MovimientosAdministrativos,MovimientosAdministrativosMesactual,TOTALREGISTRADO,%,$Com";
				strQuery = archivoToString("resumenComTec.sql");
				nomArchivo = "Comision_Tecnica_Colectivos_Resumen_";
				break;
			case 11:
				columnas = "RAMO,POLIZA,DESCRIPCION,RECIBO,CUOTA,DESDE,HASTA,PRIMANETA,RFI,DPO,IVA,PRIMATOTAL,F_DEPOSITO,CUENTABANCARIA,PrimaNeta+Recargo,%Com,Imp_Com";
				strQuery = archivoToString("resumenComComercial.sql");
				nomArchivo = "Comision_Comercial_Colectivos_Resumen_";
				break;
			default:
				columnas = "RAMO,POLIZA,PRODUCTO,%COMISION,PMANETAXCOBRAR,PROVISIONCOMISION";
				strQuery = archivoToString("calculoTecnicoDesglose.sql");
				nomArchivo = "Reporte_Comsiones_DXP_";
				break;
		}
		
		nomArchivo = nomArchivo + GestorFechas.formatDate(new Date(), "MMyyyy") + ".csv";
		
		//Remplazamos fechas y columnas
		replaceAll(strQuery,  Pattern.compile("---fecha_ini---"), String.valueOf(fecini));
		replaceAll(strQuery,  Pattern.compile("---fecha_fin---"), String.valueOf(fecfin));
		
		return generarArchivosComisiones(strQuery.toString(), "COMISIONES", nomArchivo, columnas, tipo);

	}
	
	/**
	 * Metodo que inserta peticion para genear reportes por batch
	 * @param query consulta a ejecutar
	 * @param mensaje mensaje para correo
	 * @param nomArchivo nombre del archivo 
	 * @param strNomreArchivo nombre archivo de salida
	 * @param tipo tipo reporte
	 * @return mensaje de proceso
	 */
	private String generarArchivosComisiones(String query, String mensaje, String nomArchivo, String strNomreArchivo, Integer tipo) {
		String res = Constantes.DEFAULT_STRING;
		Long next = 0L;
		
		try {	
			this.CertificadoDAO.insertaBanderaBatch(next, query, mensaje, strNomreArchivo, nomArchivo, 2L, Long.valueOf(tipo));

			res = "Reporte: " + nomArchivo + ", se generar� en ruta por proceso batch, se notificara por correo la ejecuci�n del reporte.";
		} catch (Excepciones e) {
			log.error(e.getMessage());
			res = "Error al generar archivo en ruta.";
		} 
		
		return res;
	}
	
	public List<Object> consultarErrores(Short ramo, Long poliza, Integer inIdVenta, Date fecfin, Date fecini,
			Integer inTipoError) throws Excepciones {
		List<Object> lstResultados;
		StringBuffer sbQuery;

		try {
			lstResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append(
					"SELECT TTA.TRSE_TIPO_PRODUCTO,  TTA.TRSE_CUENTA, TTA.TRSE_SUB_CAMPANA, TTA.TRSE_REGISTRO, TTA.TRSE_FECHA_PAGO \n");
			sbQuery.append("FROM TLMK_TRASPASO_APTC TTA \n");
			sbQuery.append("WHERE TTA.TRSE_SUB_CAMPANA = ").append(inIdVenta).append("\n ");
			sbQuery.append(" AND TTA.TRSE_TIPO_REGISTRO = ").append(inTipoError).append("\n ");
			sbQuery.append(" AND TTA.TRSE_FECHA_CARGA BETWEEN TO_DATE('")
					.append(GestorFechas.formatDate(fecini, "dd/MM/yyyy")).append("', 'dd/MM/yyyy') AND TO_DATE('")
					.append(GestorFechas.formatDate(fecfin, "dd/MM/yyyy")).append("', 'dd/MM/yyyy')").append("\n ");

			if (String.valueOf(inTipoError).equals(Constantes.CODIGO_ERROR_EMISION)) {
				sbQuery.append(" AND TTA.TRSE_TIPO_PRODUCTO = ").append(ramo).append("\n ");
				if (inIdVenta != 0) {
					sbQuery.append(" AND TTA.TRSE_NUMERO_SOLICITUD = ").append(poliza).append("\n ");
				}
			}

			lstResultados.addAll(this.CertificadoDAO.consultarRegistros(sbQuery.toString()));
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.consultaErrores():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	public List<Object> consultarRegistrosEspeciales(Integer inIdReporteEspecial, Integer idMes, Integer nFiltroFechas,
			Date fechaIni, Date fechaFin) throws Excepciones {
		List<Object> lstResultados;
		String strQuery;
		String strFechaIni = null, strFechaFin = null;
		try {
			lstResultados = new ArrayList<Object>();
			if (nFiltroFechas == 1) { // Fechas por mes
				strFechaIni = String.format("01/%02d/%d", idMes, Calendar.getInstance().get(Calendar.YEAR));
				Integer nDias = (Integer) GestorFechas
						.getDatosFecha(GestorFechas.generateDate(strFechaIni, "dd/MM/yyyy"), Constantes.DIAS_MES);
				strFechaFin = Utilerias.agregarCaracteres(nDias.toString(), 1, '0', 1) + "/"
						+ Utilerias.agregarCaracteres(idMes.toString(), 1, '0', 1) + "/"
						+ Calendar.getInstance().get(Calendar.YEAR);
			} else if (nFiltroFechas == 2) {// Fechas por dias
				strFechaIni = GestorFechas.formatDate(fechaIni, "dd/MM/yyyy");
				strFechaFin = GestorFechas.formatDate(fechaFin, "dd/MM/yyyy");
			}

			switch (inIdReporteEspecial) {
			case 1:
				strQuery = crearSelectCobranza(inIdReporteEspecial, strFechaIni, strFechaFin);
				lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
				break;
			case 2:
				strQuery = crearSelectEmisionCorteContable(inIdReporteEspecial, strFechaIni, strFechaFin);
				lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
				break;
			case 3:

				break;
			case 5:
				strQuery = crearSelectBaseFinal();
				lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
				break;
			}
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.consultarRegistrosEspeciales():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	public List<Object> consultarRegistrosEspeciales2(Integer inIdReporteEspecial) throws Excepciones {
		List<Object> lstResultados;
		String strQuery = Constantes.DEFAULT_STRING;

		try {
			lstResultados = new ArrayList<Object>();
			switch (inIdReporteEspecial) {
			case 40:
				strQuery = crearSelectReporteEstimacionPR();
				break;
			case 41:
				strQuery = crearSelectReporteEstimacionPU();
				break;
			}
			lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.consultarRegistrosEspeciales2():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	public List<Object> consultaIdVenta() throws Excepciones {
		List<Object> lstResultados;
		String strQuery;

		try {
			lstResultados = new ArrayList<Object>();
			strQuery = crearSelectIdVenta();
			lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.consultaIdVenta():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	public List<Object> consultaFoto(String idVenta) throws Excepciones {
		List<Object> lstResultados;
		String strQuery;

		try {
			lstResultados = new ArrayList<Object>();
			strQuery = crearSelectConsultaFoto();
			lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));

		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.consultaFoto():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> reporteFiscalEmitidos(Short ramo, Integer idVenta, String strFechaIni, String strFechaFin)
			throws Excepciones {
		List<T> lstResultados;
		String strQuery;

		try {
			lstResultados = new ArrayList<T>();
			strQuery = crearSelectReporteFiscalEmitidos(ramo, idVenta, strFechaIni, strFechaFin);
			lstResultados = (List<T>) CertificadoDAO.consultarRegistros(strQuery);
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.reporteFiscal:: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> reporteFiscalBajas(Short ramo, Integer idVenta, String strFechaIni, String strFechaFin)
			throws Excepciones {
		List<T> lstResultados;
		String strQuery;

		try {
			lstResultados = new ArrayList<T>();
			strQuery = crearSelectReporteFiscalBajas(ramo, idVenta, strFechaIni, strFechaFin);
			lstResultados = (List<T>) CertificadoDAO.consultarRegistros(strQuery);
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.reporteFiscal:: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	public List<Object> consultaCuentas() throws Excepciones {
		List<Object> lstResultados;
		String strQuery;

		try {
			lstResultados = new ArrayList<Object>();
			strQuery = crearSelectConsultaCuentas();
			lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.consultaCuentas():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	public List<Object> reporteCuentas(Integer mes, Integer nFiltroFechas, Date fechaIni, Date fechaFin)
			throws Excepciones {
		List<Object> lstResultados;
		String strQuery, strFechaIni = null, strFechaFin = null;

		try {
			lstResultados = new ArrayList<Object>();
			if (nFiltroFechas == 1) { // Fechas por mes
				strFechaIni = String.format("01/%02d/%d", mes, Calendar.getInstance().get(Calendar.YEAR));
				Integer nDias = (Integer) GestorFechas
						.getDatosFecha(GestorFechas.generateDate(strFechaIni, "dd/MM/yyyy"), Constantes.DIAS_MES);
				strFechaFin = Utilerias.agregarCaracteres(nDias.toString(), 1, '0', 1) + "/"
						+ Utilerias.agregarCaracteres(mes.toString(), 1, '0', 1) + "/"
						+ Calendar.getInstance().get(Calendar.YEAR);
			} else if (nFiltroFechas == 2) {// Fechas por dias
				strFechaIni = GestorFechas.formatDate(fechaIni, "dd/MM/yyyy");
				strFechaFin = GestorFechas.formatDate(fechaFin, "dd/MM/yyyy");
			}

			strQuery = crearSelectReporteCuentas();
			strQuery += crearFiltroReporteCuentas(strFechaIni, strFechaFin) + " ORDER BY cuenta, movimento, fecha";
			lstResultados.addAll(this.CertificadoDAO.consultarRegistros(strQuery));
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteadorImpl.reporteCuentas():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return lstResultados;
	}

	public void reporteIntegracion(String rutaTemporal, String rutaReporte, Map<String, Object> inParams,
			OPC_FORMATO_REPORTE opcr) throws Exception {
		this.reporteadorDao.reporteIntegracion(rutaTemporal, rutaReporte, inParams, opcr);
	}

	private String crearSelectEmitidas(String strCampos) throws Exception {
		String strSelect;
		ArrayList<String> arlResultados;
		String[] arrCampos;
		int totalCampos, contCampos = 1;
		String strInner = "";

		try {
			strSelect = "SELECT ";
			arrCampos = strCampos.split(",");
			totalCampos = arrCampos.length;
			for (int i = 0; i < totalCampos; i++) {
				arlResultados = getNombreCampoEmitidas(Integer.parseInt(arrCampos[i].toString().split("\\.")[1]));
				strSelect += arlResultados.get(0);
				if (totalCampos == contCampos) {
					strSelect += "\n";
				} else {
					strSelect += ", \n";
				}
				contCampos++;

				if (!arlResultados.get(1).equals("")) {
					strInner += arlResultados.get(1) + "\n";
				}
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectEmitido:: " + e.getMessage());
		}
		strSelect += "FROM COLECTIVOS_CERTIFICADOS C \n";
		strSelect += strInner;
		return strSelect;
	}

	private String crearSelectCancelado(String strCampos) throws Exception {
		String strSelect;
		ArrayList<String> arlResultados;
		String[] arrCampos;
		int totalCampos, contCampos = 1;
		String strInner = "";

		try {
			strSelect = "SELECT ";
			arrCampos = strCampos.split(",");
			totalCampos = arrCampos.length;
			for (int i = 0; i < totalCampos; i++) {
				arlResultados = getNombreCampoCancelado(Integer.parseInt(arrCampos[i].toString().split("\\.")[1]));
				strSelect += arlResultados.get(0);
				if (totalCampos == contCampos) {
					strSelect += "\n";
				} else {
					strSelect += ", \n";
				}
				contCampos++;

				if (!arlResultados.get(1).equals("")) {
					strInner += arlResultados.get(1) + "\n";
				}
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectCancelado:: " + e.getMessage());
		}
		strSelect += "FROM COLECTIVOS_CERTIFICADOS C \n";
		strSelect += strInner;
		return strSelect;
	}

	private String crearSelectFacturado(String strCampos) throws Exception {
		String strSelect;
		ArrayList<String> arlResultados;
		String[] arrCampos;
		int totalCampos, contCampos = 1;
		String strInner = "";

		try {
			strSelect = "SELECT ";
			arrCampos = strCampos.split(",");
			totalCampos = arrCampos.length;
			for (int i = 0; i < totalCampos; i++) {
				arlResultados = getNombreCampoFacturado(Integer.parseInt(arrCampos[i].toString().split("\\.")[1]));
				strSelect += arlResultados.get(0);
				if (totalCampos == contCampos) {
					strSelect += "\n";
				} else {
					strSelect += ", \n";
				}
				contCampos++;

				if (!arlResultados.get(1).equals("")) {
					strInner += arlResultados.get(1) + "\n";
				}
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectFacturado:: " + e.getMessage());
		}
		strSelect += "FROM COLECTIVOS_CERTIFICADOS C \n";
		strSelect += strInner;
		return strSelect;
	}

	private String crearSelectFacturado2(String strCampos) throws Exception {
		String strSelect;
		ArrayList<String> arlResultados;
		String[] arrCampos;
		int totalCampos, contCampos = 1;
		String strInner = "";

		try {
			strSelect = "SELECT ";
			arrCampos = strCampos.split(",");
			totalCampos = arrCampos.length;
			for (int i = 0; i < totalCampos; i++) {
				arlResultados = getNombreCampoFacturado2(Integer.parseInt(arrCampos[i].toString().split("\\.")[1]));
				strSelect += arlResultados.get(0);
				if (totalCampos == contCampos) {
					strSelect += "\n";
				} else {
					strSelect += ", \n";
				}
				contCampos++;

				if (!arlResultados.get(1).equals("")) {
					strInner += arlResultados.get(1) + "\n";
				}
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectFacturado2:: " + e.getMessage());
		}
		strSelect += "FROM COLECTIVOS_CERTIFICADOS_MOV C \n";
		strSelect += strInner;
		return strSelect;
	}

	private String crearSelectCobranza(Integer inIdReporteEspecial, String strFechaIni, String strFechaFin)
			throws Exception {
		StringBuffer strSelect;
		// En la siguiente sentencia podemos apreciar que los querys se repiten y solo
		// cambie el ramo
		// esto es porque as� se solicit� elaborar
		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"select 'RAMO' ram, 'POLIZA' pol, 'DESCRIPCION' descr, 'RECIBO' rec, 'CUOTA' cuo, 'DESDE' des1, 'HASTA' has1, 'PRIMA NETA' pmaNet1, 'RFI' rfi1, 'DPO' dpo1, 'IVA' iva1, 'PRIMA TOTAL' pmaTot1, 'ENDOSO' endoso, 'DESDE' des2, 'HASTA' has2, 'PRIMA NETA' pmaNet2, 'RFI' rfi2, 'DPO' dpo2, 'IVA' iva2, 'PRIMA TOTAL' pmaTot2 FROM dual \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"SELECT ''||ramo,''||poliza,''||descripcion,''||recibo,''||cuota,''||desde,''||hasta,''||pmaNeta,''||rfi,''||dpo,''||iva,''||pmaTotal,''||endoso,''||desdeE,''||hastaE,''||pmaNetaE,''||rfiE,''||dpoE,''||ivaE,''||pmaTotalE \n");
			strSelect.append("FROM ( ");
			strSelect.append("select \n");
			strSelect.append("b.cofa_carp_cd_ramo ramo, \n");
			strSelect.append("b.cofa_capo_nu_poliza poliza, \n");
			strSelect.append("a.carp_de_ramo descripcion, \n");
			strSelect.append("b.cofa_nu_recibo_fiscal recibo, \n");
			strSelect.append("b.cofa_cuota_recibo  || '/' || e.cafp_nu_pagos_ano cuota, \n");
			strSelect.append("f.core_fe_desde desde, \n");
			strSelect.append("f.core_fe_hasta hasta, \n");
			strSelect.append("b.cofa_mt_tot_pma pmaNeta, \n");
			strSelect.append("b.cofa_mt_tot_rfi rfi, \n");
			strSelect.append("b.cofa_mt_tot_dpo dpo, \n");
			strSelect.append("b.cofa_mt_tot_iva iva, \n");
			strSelect.append(
					"b.cofa_mt_tot_pma + b.cofa_mt_tot_rfi + b.cofa_mt_tot_dpo + b.cofa_mt_tot_iva pmaTotal, \n");
			strSelect.append("c.coed_campov1 endoso, \n");
			strSelect.append("c.coed_fecha_inicio desdeE, \n");
			strSelect.append("c.coed_fecha_fin hastaE, \n");
			strSelect.append("c.coed_mto_nvo pmaNetaE, \n");
			strSelect.append(
					"nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'I',1,1) + 1,instr(c.coed_campov4, '|',1,3) -instr(c.coed_campov4, 'I',1,1) -1)),0) rfiE, \n");
			strSelect.append(
					"nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'O',1,1) + 1,instr(c.coed_campov4, '|',1,2) -instr(c.coed_campov4, 'O',1,1) -1)),0) dpoE, \n");
			strSelect.append(
					"nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'V',1,1) + 2,instr(c.coed_campov4, '|',1,4) -instr(c.coed_campov4, 'V',1,1) -2)),0) ivaE, \n");
			strSelect.append(
					"nvl(to_number(substr(c.coed_campov4,instr(c.coed_campov4, 'T',1,1) + 2,instr(c.coed_campov4, '|',1,5) -instr(c.coed_campov4, 'T',1,1) -2)),0) pmaTotalE \n");
			strSelect.append("from \n");
			strSelect.append("colectivos_facturacion b, \n");
			strSelect.append("colectivos_recibos f, \n");
			strSelect.append("colectivos_endosos c, \n");
			strSelect.append("cart_fr_pagos e, \n");
			strSelect.append("cart_ramos_polizas a \n");
			strSelect.append("where a.carp_cd_ramo = b.cofa_carp_cd_ramo \n");
			strSelect.append("and f.core_casu_cd_sucursal = b.cofa_casu_cd_sucursal \n");
			strSelect.append("and f.core_nu_recibo = b.cofa_nu_recibo_fiscal \n");
			strSelect.append("and c.coed_casu_cd_sucursal (+) = b.cofa_casu_cd_sucursal \n");
			strSelect.append("and c.coed_nu_recibo        (+) = b.cofa_nu_recibo_fiscal \n");
			strSelect.append("and c.coed_nu_certificado   (+) = 0 \n");
			strSelect.append("and e.cafp_cd_fr_pago = f.core_capo_cd_fr_pago \n");
			strSelect.append("and b.cofa_casu_cd_sucursal = 1 \n");
			strSelect.append("and b.cofa_carp_cd_ramo  in (5,9,25,61,65,82) \n");
			strSelect.append("and f.core_st_recibo = 4 \n");
			strSelect.append("and b.cofa_fe_facturacion BETWEEN TO_DATE('" + strFechaIni
					+ "', 'dd/MM/yyyy') AND TO_DATE('" + strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("union all \n");
			strSelect.append("select \n");
			strSelect.append("re.care_carp_cd_ramo ramo, \n");
			strSelect.append("re.care_capo_nu_poliza poliza, \n");
			strSelect.append("a.carp_de_ramo descripcion, \n");
			strSelect.append("re.care_nu_recibo recibo, \n");
			strSelect.append("re.care_nu_consecutivo_cuota || '/' || e.cafp_nu_pagos_ano cuota, \n");
			strSelect.append("re.care_fe_desde desde, \n");
			strSelect.append("re.care_fe_hasta hasta, \n");
			strSelect.append("re.care_mt_prima_pura pmaNeta , \n");
			strSelect.append("nvl((select rc.carg_mt_componente from cart_recibos_componentes rc \n");
			strSelect.append("where rc.carg_casu_cd_sucursal = re.care_casu_cd_sucursal \n");
			strSelect.append("and rc.carg_care_nu_recibo =  re.care_nu_recibo \n");
			strSelect.append("and rc.carg_capp_cd_componente = 'RFI' ), 0) RFI, \n");
			strSelect.append("nvl((select rc.carg_mt_componente from cart_recibos_componentes rc \n");
			strSelect.append("where rc.carg_casu_cd_sucursal = re.care_casu_cd_sucursal \n");
			strSelect.append("and rc.carg_care_nu_recibo =  re.care_nu_recibo \n");
			strSelect.append("and rc.carg_capp_cd_componente = 'DPO' ),0) DPO , \n");
			strSelect.append("nvl((select rc.carg_mt_componente from cart_recibos_componentes rc \n");
			strSelect.append("where rc.carg_casu_cd_sucursal = re.care_casu_cd_sucursal \n");
			strSelect.append("and rc.carg_care_nu_recibo =  re.care_nu_recibo \n");
			strSelect.append("and rc.carg_capp_cd_componente = 'IVA' ),0) IVA, \n");
			strSelect.append("re.care_mt_prima    pmaTotal, \n");
			strSelect.append("'', \n");
			strSelect.append("null, \n");
			strSelect.append("null, \n");
			strSelect.append("0, \n");
			strSelect.append("0, \n");
			strSelect.append("0, \n");
			strSelect.append("0, \n");
			strSelect.append("0 \n");
			strSelect.append("From cart_recibos re, \n");
			strSelect.append("cart_fr_pagos e, \n");
			strSelect.append("cart_ramos_polizas a   \n");
			strSelect.append("where re.care_carp_cd_ramo =  a.carp_cd_ramo \n");
			strSelect.append("and re.care_capo_cd_fr_pago = e.cafp_cd_fr_pago \n");
			strSelect.append("and re.care_casu_cd_sucursal = 1 \n");
			strSelect.append("and re.care_carp_cd_ramo in (57,58) \n");
			strSelect.append("and re.care_st_recibo = 4 \n");
			strSelect.append("and re.care_fe_status BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append(") \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectCobranza:: " + e.getMessage());
		}

		return strSelect.toString();
	}

	private String crearSelectEmisionCorteContable(Integer inIdReporteEspecial, String strFechaIni, String strFechaFin)
			throws Exception {
		StringBuffer strSelect;
		// En la siguiente sentencia podemos apreciar que los querys se repiten y solo
		// cambie el ramo
		// esto es porque as� se solicit� elaborar
		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"SELECT 'BUC' bu,'POLIZA' pol,'CERTIFICADO' cer,'% COMISION' com,'RAMO' ram,'DESCRIPCION PRODUCTO' des,'CANAL' canal,'NUM. CREDITO' cred,'SUCURSAL' suc,'TIPO OPERACION' tipOp,'PRIMA EMITIDA NETA' pmaEmi,'0ERECHOS SOBRE P. EMITIDA' dpos,'RECARGOS SOBRE P. EMITIDA' rfis,'ASISTENCIA SOBRE P. EMITIDA' asis,'IVA SOBRE P. EMITIDA' ivas,'TOTAL P. EMITIDA' pmaTot,'MONTO COMISION' mtoCom,'CLAVE PRODUCTO' prod,'FECHA REGISTRO CONTABLE' fcont,'FORMA DE PAGO' pag,'EJECUTIVO' eje,'ID CONVENIO' conv,'DESCRIP. CONVENIO O EMPRESA' emp FROM dual \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"SELECT ''||buc,''||poliza,''||certificado,''||comision,''||ramo,''||descripcion,''||canal,''||nuCredito,''||sucursal,''||tipoOperacion,''||pmaEmitidaNeta,''||dpo,''||rfi,''||asistencia,''||iva,''||totalPmaEmitida,''||mtoComision,''||clvProducto,''||fRegistroContable,''||frmPago,''||ejecutivo,''||idConvenio,''||movEmpresa from (  \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente  \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 61  \n");
			strSelect.append("and c.coce_capo_nu_poliza = 50  \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 61  \n");
			strSelect.append("and c.coce_capo_nu_poliza = 51  \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 61  \n");
			strSelect.append("and c.coce_capo_nu_poliza = 56  \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 61  \n");
			strSelect.append("and c.coce_capo_nu_poliza = 57  \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 61  \n");
			strSelect.append("and c.coce_capo_nu_poliza = 16102  \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 61  \n");
			strSelect.append("and c.coce_capo_nu_poliza = 16104  \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 9 \n");
			strSelect.append("and c.coce_capo_nu_poliza > 0  \n");
			strSelect.append("and c.coce_sub_campana > 0 \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"select a.cocn_buc_cliente buc, c.coce_capo_nu_poliza poliza, c.coce_nu_certificado certificado, '' comision, c.coce_carp_cd_ramo ramo, d.carp_de_ramo descripcion, c.coce_casu_cd_sucursal canal, decode(c.coce_nu_credito, null, c.coce_nu_cuenta, c.coce_nu_credito) nuCredito, c.coce_cazb_cd_sucursal sucursal, 'ALTA' tipoOperacion, c.coce_mt_prima_pura pmaEmitidaNeta, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'O',1,1)+1,instr(c.coce_di_cobro1,'|',1,1)-instr(c.coce_di_cobro1,'O',1,1)-1)),4),0) dpo, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'I',1,1)+1,instr(c.coce_di_cobro1,'|',1,2)-instr(c.coce_di_cobro1,'I',1,1)-1)),4),0) rfi, '' asistencia, nvl(round(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1,'A',1,1)+1,instr(c.coce_di_cobro1,'|',1,3)-instr(c.coce_di_cobro1,'A',1,1)-1)),4),0) iva, c.coce_mt_prima_subsecuente totalPmaEmitida, '' mtoComision, c.coce_capu_cd_producto clvProducto, '' fRegistroContable, 'PRIMA RECURRENTE' frmPago, '' ejecutivo, '' idConvenio, '' movEmpresa \n");
			strSelect.append(
					"from colectivos_clientes a, colectivos_cliente_certif b, colectivos_certificados c, cart_ramos_polizas d \n");
			strSelect.append("where a.cocn_nu_cliente = b.cocc_nu_cliente \n");
			strSelect.append("and b.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal   \n");
			strSelect.append("and b.cocc_carp_cd_ramo = c.coce_carp_cd_ramo   \n");
			strSelect.append("and b.cocc_capo_nu_poliza = c.coce_capo_nu_poliza   \n");
			strSelect.append("and b.cocc_nu_certificado = c.coce_nu_certificado   \n");
			strSelect.append("and d.carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("and c.coce_casu_cd_sucursal = 1  \n");
			strSelect.append("and c.coce_carp_cd_ramo = 61 \n");
			strSelect.append("and c.coce_capo_nu_poliza > 0  \n");
			strSelect.append("and c.coce_sub_campana > 0 \n");
			strSelect.append("and c.coce_st_certificado in (1,2)   \n");
			strSelect.append("and c.coce_fe_emision BETWEEN TO_DATE('" + strFechaIni + "', 'dd/MM/yyyy') AND TO_DATE('"
					+ strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append(") \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectEmisionCorteContable:: " + e.getMessage());
		}

		return strSelect.toString();
	}

	private String crearSelectReporteEstimacionPR() throws Exception {
		StringBuffer strSelect;
		try {
			strSelect = new StringBuffer();
			strSelect.append("SELECT  \n");
			strSelect.append("    'PRODUCTO' prod,  \n");
			strSelect.append("    '%' porcen,  \n");
			strSelect.append("    'VENTA' vent,  \n");
			strSelect.append("    'RENOVACION' ren,  \n");
			strSelect.append("    'CANCELACION' can,  \n");
			strSelect.append("    'PRIMAS EN DEPOSITO DEPURADAS REALES' pmaDep,  \n");
			strSelect.append("    'PRIMAS EN DEPOSITO DEPURADAS ESTIMADAS' pmaEst,  \n");
			strSelect.append("    'EMISION ESTIMADA' esti,  \n");
			strSelect.append("    'TOTAL PRIMA ESTIMADA' totPma,  \n");
			strSelect.append("    'ASISTENCIA' asis,  \n");
			strSelect.append("    'RECARGOS' rec,  \n");
			strSelect.append("    'PRIMA BASE PARA COMISIONES' pmaComi,  \n");
			strSelect.append("    'COMISION' comi FROM dual  \n");
			strSelect.append("    UNION ALL  \n");
			strSelect.append("SELECT  \n");
			strSelect.append("    prod||' '||pr,  \n");
			strSelect.append("    ''||por,  \n");
			strSelect.append("    ''||ven,  \n");
			strSelect.append("    ''||ren,  \n");
			strSelect.append("    ''||ca,  \n");
			strSelect.append("    ''||dep,  \n");
			strSelect.append("    ''||depEs,  \n");
			strSelect.append("    ''||emMes,  \n");
			strSelect.append("    ''||pma,  \n");
			strSelect.append("    ''||asis,  \n");
			strSelect.append("    ''||rec,  \n");
			strSelect.append("    ''||base,  \n");
			strSelect.append("    ''||comi  \n");
			strSelect.append("    FROM (select  \n");
			strSelect.append("          tabla.prod, tabla.cobe pr, tabla.porcentaje || '%' por,  \n");
			strSelect.append("          sum(tabla.venta) ven,  \n");
			strSelect.append("          sum(tabla.renovacion) ren,'-' || sum(tabla.cancelacion) ca,  \n");
			strSelect.append("          sum(tabla.pmaDepositoReal) dep,  \n");
			strSelect.append("          sum(tabla.pmaDepositoEstimada) depEs,  \n");
			strSelect.append("          sum(tabla.EmisionMes) emMes,  \n");
			strSelect.append(
					"          sum(tabla.venta) + sum(tabla.renovacion) - sum(tabla.cancelacion) + sum(tabla.pmaDepositoReal) + sum(tabla.pmaDepositoEstimada) + sum(tabla.EmisionMes) pma,  \n");
			strSelect.append("          sum(tabla.asistencia) asis,sum(tabla.recargos) rec,  \n");
			strSelect.append(
					"          sum(tabla.venta) + sum(tabla.renovacion) - sum(tabla.cancelacion) + sum(tabla.pmaDepositoReal) + sum(tabla.pmaDepositoEstimada) + sum(tabla.EmisionMes) base,  \n");
			strSelect.append(
					"          (sum(tabla.venta) + sum(tabla.renovacion) - sum(tabla.cancelacion) + sum(tabla.pmaDepositoReal) + sum(tabla.pmaDepositoEstimada) + sum(tabla.EmisionMes)) * sum(tabla.porcentaje) comi  \n");
			strSelect.append("          from (  \n");
			strSelect.append(
					"    SELECT PR.ALPR_DE_PRODUCTO prod,CB.CACB_DE_COBERTURA cobe, NVL(COB.COCB_PO_COMISION,0) porcentaje, \n");
			strSelect.append("            SUM(EN.COED_MTO_NVO) venta, \n");
			strSelect.append("            0 renovacion,  \n");
			strSelect.append("            0 cancelacion,  \n");
			strSelect.append("            0 pmaDepositoReal,  \n");
			strSelect.append("            0 pmaDepositoEstimada,  \n");
			strSelect.append("            0 EmisionMes,  \n");
			strSelect.append("            0 totalPma,  \n");
			strSelect.append("            0 asistencia,  \n");
			strSelect.append("            0 recargos,  \n");
			strSelect.append("            0 pmaBase,  \n");
			strSelect.append("            0 comisi \n");
			strSelect.append("        FROM COLECTIVOS_ENDOSOS EN \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_CERTIFICADOS CE ON(CE.COCE_CASU_CD_SUCURSAL = EN.COED_CASU_CD_SUCURSAL AND CE.COCE_CARP_CD_RAMO = EN.COED_CARP_CD_RAMO AND CE.COCE_CAPO_NU_POLIZA = EN.COED_CAPO_NU_POLIZA AND CE.COCE_NU_CERTIFICADO = EN.COED_NU_CERTIFICADO) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PRODUCTOS PR ON(PR.ALPR_CD_RAMO =  CE.COCE_CARP_CD_RAMO AND PR.ALPR_CD_PRODUCTO =  CE.COCE_CAPU_CD_PRODUCTO) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PLANES AP ON(AP.ALPL_CD_RAMO = PR.ALPR_CD_RAMO AND AP.ALPL_CD_PRODUCTO =  PR.ALPR_CD_PRODUCTO AND AP.ALPL_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_COBERTURAS COB ON(COB.COCB_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL   \n");
			strSelect.append("                                    AND COB.COCB_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPO_NU_POLIZA = DECODE(NVL(CE.COCE_SUB_CAMPANA, 0), 0, CE.COCE_CAPO_NU_POLIZA, SUBSTR(CE.COCE_CAPO_NU_POLIZA, 0, 5))      \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPU_CD_PRODUCTO = CE.COCE_CAPU_CD_PRODUCTO \n");
			strSelect
					.append("                                    AND COB.COCB_CAPB_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN CART_COBERTURAS CB ON(CB.CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA AND CB.CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO) \n");
			strSelect.append("    WHERE EN.COED_CASU_CD_SUCURSAL = 1  \n");
			strSelect.append("        AND EN.COED_CARP_CD_RAMO > 0  \n");
			strSelect.append("        AND EN.COED_CAPO_NU_POLIZA > 0 \n");
			strSelect.append("        AND EN.COED_NU_CERTIFICADO = 0 \n");
			strSelect.append("        AND NVL(CE.COCE_SUB_CAMPANA, 0) = 0  \n");
			strSelect.append("        AND EN.COED_NU_ENDOSO_GRAL = TO_NUMBER(to_char(SYSDATE,'YYYYMM')) \n");
			strSelect.append("    GROUP BY PR.ALPR_DE_PRODUCTO , CB.CACB_DE_COBERTURA,NVL(COB.COCB_PO_COMISION,0) \n");
			strSelect.append("    UNION ALL  \n");
			strSelect.append(
					"    SELECT PR.ALPR_DE_PRODUCTO prod,CB.CACB_DE_COBERTURA cobe, NVL(COB.COCB_PO_COMISION,0) porcentaje, \n");
			strSelect.append("            0, \n");
			strSelect.append(
					"            SUM((COB.COCB_TA_RIESGO*(nvl(COB.COCB_CAMPOV2, CE.COCE_MT_SUMA_ASEGURADA)) * to_number(COB.COCB_CAMPOV1)) / decode(COB.COCB_CAMPON1, null, 1000,COB.COCB_CAMPON1)) renova,  \n");
			strSelect.append("            0 cancelacion,  \n");
			strSelect.append("            0 pmaDepositoReal,  \n");
			strSelect.append("            0 pmaDepositoEstimada,  \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0 totalPma,  \n");
			strSelect.append("            0 asistencia,  \n");
			strSelect.append("            0 recargos,  \n");
			strSelect.append("            0 pmaBase,  \n");
			strSelect.append("            0 comisi  \n");
			strSelect.append("        FROM COLECTIVOS_CERTIFICADOS CE \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PRODUCTOS PR ON(PR.ALPR_CD_RAMO =  CE.COCE_CARP_CD_RAMO AND PR.ALPR_CD_PRODUCTO =  CE.COCE_CAPU_CD_PRODUCTO) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PLANES AP ON(AP.ALPL_CD_RAMO = PR.ALPR_CD_RAMO AND AP.ALPL_CD_PRODUCTO =  PR.ALPR_CD_PRODUCTO AND AP.ALPL_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_COBERTURAS COB ON(COB.COCB_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL   \n");
			strSelect.append("                                    AND COB.COCB_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPO_NU_POLIZA = DECODE(NVL(CE.COCE_SUB_CAMPANA, 0), 0, CE.COCE_CAPO_NU_POLIZA, SUBSTR(CE.COCE_CAPO_NU_POLIZA, 0, 5)) \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPU_CD_PRODUCTO = CE.COCE_CAPU_CD_PRODUCTO \n");
			strSelect
					.append("                                    AND COB.COCB_CAPB_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN CART_COBERTURAS CB ON(CB.CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA AND CB.CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO) \n");
			strSelect.append("    WHERE CE.COCE_CASU_CD_SUCURSAL = 1 \n");
			strSelect.append("        AND CE.COCE_CARP_CD_RAMO > 0 \n");
			strSelect.append("        AND CE.COCE_CAPO_NU_POLIZA > 0 \n");
			strSelect.append("        AND CE.COCE_NU_CERTIFICADO > 0   \n");
			strSelect.append(
					"        AND CE.COCE_FE_RENOVACION BETWEEN TO_DATE('01/'||to_char(SYSDATE,'MM/yyyy'),'dd/mm/yyyy') AND LAST_DAY(SYSDATE)  \n");
			strSelect.append("        AND CE.COCE_ST_CERTIFICADO IN (1,2)  \n");
			strSelect.append("    GROUP BY PR.ALPR_DE_PRODUCTO , CB.CACB_DE_COBERTURA,NVL(COB.COCB_PO_COMISION,0) \n");
			strSelect.append("    UNION ALL  \n");
			strSelect.append(
					"    SELECT PR.ALPR_DE_PRODUCTO prod,CB.CACB_DE_COBERTURA cobe, NVL(COB.COCB_PO_COMISION,0) porcentaje, \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0, \n");
			strSelect.append("            SUM( NVL(CE.COCE_MT_DEVOLUCION, CE.COCE_MT_BCO_DEVOLUCION ) ) cance,  \n");
			strSelect.append("            0 pmaDepositoReal,  \n");
			strSelect.append("            0 pmaDepositoEstimada,  \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0 totalPma,  \n");
			strSelect.append("            0 asistencia,  \n");
			strSelect.append("            0 recargos,  \n");
			strSelect.append("            0 pmaBase,  \n");
			strSelect.append("            0 comisi  \n");
			strSelect.append("        FROM COLECTIVOS_CERTIFICADOS CE \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PRODUCTOS PR ON(PR.ALPR_CD_RAMO =  CE.COCE_CARP_CD_RAMO AND PR.ALPR_CD_PRODUCTO =  CE.COCE_CAPU_CD_PRODUCTO) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PLANES AP ON(AP.ALPL_CD_RAMO = PR.ALPR_CD_RAMO AND AP.ALPL_CD_PRODUCTO =  PR.ALPR_CD_PRODUCTO AND AP.ALPL_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_COBERTURAS COB ON(COB.COCB_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL   \n");
			strSelect.append("                                    AND COB.COCB_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPO_NU_POLIZA = DECODE(NVL(CE.COCE_SUB_CAMPANA, 0), 0, CE.COCE_CAPO_NU_POLIZA, SUBSTR(CE.COCE_CAPO_NU_POLIZA, 0, 5))      \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPU_CD_PRODUCTO = CE.COCE_CAPU_CD_PRODUCTO \n");
			strSelect
					.append("                                    AND COB.COCB_CAPB_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN CART_COBERTURAS CB ON(CB.CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA AND CB.CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO) \n");
			strSelect.append("    WHERE CE.COCE_CASU_CD_SUCURSAL = 1 \n");
			strSelect.append("        AND CE.COCE_CARP_CD_RAMO > 0 \n");
			strSelect.append("        AND CE.COCE_CAPO_NU_POLIZA > 0 \n");
			strSelect.append("        AND CE.COCE_NU_CERTIFICADO > 0   \n");
			strSelect.append(
					"        AND CE.COCE_FE_ANULACION BETWEEN TO_DATE('01/'||to_char(SYSDATE,'MM/yyyy'),'dd/mm/yyyy') AND LAST_DAY(SYSDATE)  \n");
			strSelect.append("        AND CE.COCE_ST_CERTIFICADO IN (4,7,10,11) \n");
			strSelect.append("        AND NVL(CE.COCE_SUB_CAMPANA, 0) = 0  \n");
			strSelect.append(
					"        AND ((CB.CACB_CARB_CD_RAMO || CB.CACB_CD_COBERTURA) = '31003' OR CB.CACB_CD_COBERTURA = '001') \n");
			strSelect.append("    GROUP BY PR.ALPR_DE_PRODUCTO , CB.CACB_DE_COBERTURA,NVL(COB.COCB_PO_COMISION,0) \n");
			strSelect.append("          ) tabla group by tabla.prod, tabla.cobe, tabla.porcentaje order by 1 asc) \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectReporteEstimacion:: " + e.getMessage());
		}
		return strSelect.toString();
	}

	private String crearSelectReporteEstimacionPU() throws Exception {
		StringBuffer strSelect;
		try {
			strSelect = new StringBuffer();
			strSelect.append("SELECT  \n");
			strSelect.append("    'PRODUCTO' prod,  \n");
			strSelect.append("    '%' porcen,  \n");
			strSelect.append("    'VENTA' vent,  \n");
			strSelect.append("    'RENOVACION' ren,  \n");
			strSelect.append("    'CANCELACION' can,  \n");
			strSelect.append("    'PRIMAS EN DEPOSITO DEPURADAS REALES' pmaDep,  \n");
			strSelect.append("    'PRIMAS EN DEPOSITO DEPURADAS ESTIMADAS' pmaEst,  \n");
			strSelect.append("    'EMISION ESTIMADA' esti,  \n");
			strSelect.append("    'TOTAL PRIMA ESTIMADA' totPma,  \n");
			strSelect.append("    'ASISTENCIA' asis,  \n");
			strSelect.append("    'RECARGOS' rec,  \n");
			strSelect.append("    'PRIMA BASE PARA COMISIONES' pmaComi,  \n");
			strSelect.append("    'COMISION' comi FROM dual  \n");
			strSelect.append("    UNION ALL  \n");
			strSelect.append("SELECT  \n");
			strSelect.append("    prod||' '||pr,  \n");
			strSelect.append("    ''||por,  \n");
			strSelect.append("    ''||ven,  \n");
			strSelect.append("    ''||ren,  \n");
			strSelect.append("    ''||ca,  \n");
			strSelect.append("    ''||dep,  \n");
			strSelect.append("    ''||depEs,  \n");
			strSelect.append("    ''||emMes,  \n");
			strSelect.append("    ''||pma,  \n");
			strSelect.append("    ''||asis,  \n");
			strSelect.append("    ''||rec,  \n");
			strSelect.append("    ''||base,  \n");
			strSelect.append("    ''||comi  \n");
			strSelect.append("    FROM (select  \n");
			strSelect.append("          tabla.prod, tabla.cobe pr, tabla.porcentaje || '%' por,  \n");
			strSelect.append("          sum(tabla.venta) ven,  \n");
			strSelect.append("          sum(tabla.renovacion) ren,'-' || sum(tabla.cancelacion) ca,  \n");
			strSelect.append("          sum(tabla.pmaDepositoReal) dep,  \n");
			strSelect.append("          sum(tabla.pmaDepositoEstimada) depEs,  \n");
			strSelect.append("          sum(tabla.EmisionMes) emMes,  \n");
			strSelect.append(
					"          sum(tabla.venta) + sum(tabla.renovacion) - sum(tabla.cancelacion) + sum(tabla.pmaDepositoReal) + sum(tabla.pmaDepositoEstimada) + sum(tabla.EmisionMes) pma,  \n");
			strSelect.append("          sum(tabla.asistencia) asis,sum(tabla.recargos) rec,  \n");
			strSelect.append(
					"          sum(tabla.venta) + sum(tabla.renovacion) - sum(tabla.cancelacion) + sum(tabla.pmaDepositoReal) + sum(tabla.pmaDepositoEstimada) + sum(tabla.EmisionMes) base,  \n");
			strSelect.append(
					"          (sum(tabla.venta) + sum(tabla.renovacion) - sum(tabla.cancelacion) + sum(tabla.pmaDepositoReal) + sum(tabla.pmaDepositoEstimada) + sum(tabla.EmisionMes)) * sum(tabla.porcentaje) comi  \n");
			strSelect.append("          from (  \n");
			strSelect.append(
					"    SELECT PR.ALPR_DE_PRODUCTO prod,CB.CACB_DE_COBERTURA cobe, NVL(COB.COCB_PO_COMISION,0) porcentaje, \n");
			strSelect.append(
					"            SUM((COB.COCB_TA_RIESGO*(nvl(COB.COCB_CAMPOV2, CE.COCE_MT_SUMA_ASEGURADA)) * to_number(COB.COCB_CAMPOV1)) / decode(COB.COCB_CAMPON1, null, 1000,COB.COCB_CAMPON1)) venta, \n");
			strSelect.append("            0 renovacion,  \n");
			strSelect.append("            0 cancelacion,  \n");
			strSelect.append("            0 pmaDepositoReal,  \n");
			strSelect.append("            0 pmaDepositoEstimada,  \n");
			strSelect.append(
					"			  (SUM((COB.COCB_TA_RIESGO*(nvl(COB.COCB_CAMPOV2, CE.COCE_MT_SUMA_ASEGURADA)) * to_number(COB.COCB_CAMPOV1)) / decode(COB.COCB_CAMPON1, null, 1000,COB.COCB_CAMPON1)) /  \n");
			strSelect.append(
					"                        colectivos.p_valida_inhabil(TO_DATE('01/'||to_char(SYSDATE,'MM/yyyy'),'dd/mm/yyyy'), TRUNC(SYSDATE)) ) * colectivos.p_valida_inhabil(TRUNC(SYSDATE), LAST_DAY(SYSDATE)) EmisionMes, \n");
			strSelect.append("            0 totalPma,  \n");
			strSelect.append("            0 asistencia,  \n");
			strSelect.append("            0 recargos,  \n");
			strSelect.append("            0 pmaBase,  \n");
			strSelect.append("            0 comisi  \n");
			strSelect.append("        FROM COLECTIVOS_CERTIFICADOS CE \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PRODUCTOS PR ON(PR.ALPR_CD_RAMO =  CE.COCE_CARP_CD_RAMO AND PR.ALPR_CD_PRODUCTO =  CE.COCE_CAPU_CD_PRODUCTO) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PLANES AP ON(AP.ALPL_CD_RAMO = PR.ALPR_CD_RAMO AND AP.ALPL_CD_PRODUCTO =  PR.ALPR_CD_PRODUCTO AND AP.ALPL_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_COBERTURAS COB ON(COB.COCB_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL   \n");
			strSelect.append("                                    AND COB.COCB_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPO_NU_POLIZA = DECODE(NVL(CE.COCE_SUB_CAMPANA, 0), 0, CE.COCE_CAPO_NU_POLIZA, SUBSTR(CE.COCE_CAPO_NU_POLIZA, 0, 5)) \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPU_CD_PRODUCTO = CE.COCE_CAPU_CD_PRODUCTO \n");
			strSelect
					.append("                                    AND COB.COCB_CAPB_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN CART_COBERTURAS CB ON(CB.CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA AND CB.CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO) \n");
			strSelect.append("    WHERE CE.COCE_CASU_CD_SUCURSAL = 1 \n");
			strSelect.append("        AND CE.COCE_CARP_CD_RAMO > 0 \n");
			strSelect.append("        AND CE.COCE_CAPO_NU_POLIZA BETWEEN 500000000 AND 999999999 \n");
			strSelect.append("        AND CE.COCE_NU_CERTIFICADO > 0   \n");
			strSelect.append(
					"        AND CE.COCE_FE_SUSCRIPCION BETWEEN TO_DATE('01/'||to_char(SYSDATE,'MM/yyyy'),'dd/mm/yyyy') AND LAST_DAY(SYSDATE)  \n");
			strSelect.append("        AND CE.COCE_ST_CERTIFICADO IN (1,2)  \n");
			strSelect.append("    GROUP BY PR.ALPR_DE_PRODUCTO , CB.CACB_DE_COBERTURA,NVL(COB.COCB_PO_COMISION,0) \n");
			strSelect.append("    UNION ALL \n");
			strSelect.append(
					"    SELECT PR.ALPR_DE_PRODUCTO prod,CB.CACB_DE_COBERTURA cobe, NVL(COB.COCB_PO_COMISION,0) porcentaje, \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0, \n");
			strSelect.append("            SUM( NVL(CE.COCE_MT_DEVOLUCION, CE.COCE_MT_BCO_DEVOLUCION ) ) cance,  \n");
			strSelect.append("            0 pmaDepositoReal,  \n");
			strSelect.append("            0 pmaDepositoEstimada,  \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0 totalPma,  \n");
			strSelect.append("            0 asistencia,  \n");
			strSelect.append("            0 recargos,  \n");
			strSelect.append("            0 pmaBase,  \n");
			strSelect.append("            0 comisi  \n");
			strSelect.append("        FROM COLECTIVOS_CERTIFICADOS CE \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PRODUCTOS PR ON(PR.ALPR_CD_RAMO =  CE.COCE_CARP_CD_RAMO AND PR.ALPR_CD_PRODUCTO =  CE.COCE_CAPU_CD_PRODUCTO) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PLANES AP ON(AP.ALPL_CD_RAMO = PR.ALPR_CD_RAMO AND AP.ALPL_CD_PRODUCTO =  PR.ALPR_CD_PRODUCTO AND AP.ALPL_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_COBERTURAS COB ON(COB.COCB_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL   \n");
			strSelect.append("                                    AND COB.COCB_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPO_NU_POLIZA = DECODE(NVL(CE.COCE_SUB_CAMPANA, 0), 0, CE.COCE_CAPO_NU_POLIZA, SUBSTR(CE.COCE_CAPO_NU_POLIZA, 0, 5))      \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPU_CD_PRODUCTO = CE.COCE_CAPU_CD_PRODUCTO \n");
			strSelect
					.append("                                    AND COB.COCB_CAPB_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN CART_COBERTURAS CB ON(CB.CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA AND CB.CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO) \n");
			strSelect.append("    WHERE CE.COCE_CASU_CD_SUCURSAL = 1 \n");
			strSelect.append("        AND CE.COCE_CARP_CD_RAMO > 0 \n");
			strSelect.append("        AND CE.COCE_CAPO_NU_POLIZA BETWEEN 500000000 AND 999999999 \n");
			strSelect.append("        AND CE.COCE_NU_CERTIFICADO > 0   \n");
			strSelect.append(
					"        AND CE.COCE_FE_ANULACION BETWEEN TO_DATE('01/'||to_char(SYSDATE,'MM/yyyy'),'dd/mm/yyyy') AND LAST_DAY(SYSDATE)  \n");
			strSelect.append("        AND CE.COCE_ST_CERTIFICADO IN (4,7,10,11) \n");
			strSelect.append(
					"        AND ((CB.CACB_CARB_CD_RAMO || CB.CACB_CD_COBERTURA) = '31003' OR CB.CACB_CD_COBERTURA = '001') \n");
			strSelect.append("    GROUP BY PR.ALPR_DE_PRODUCTO , CB.CACB_DE_COBERTURA,NVL(COB.COCB_PO_COMISION,0) \n");
			strSelect.append("    UNION ALL  \n");
			strSelect.append(
					"    SELECT PR.ALPR_DE_PRODUCTO prod,CB.CACB_DE_COBERTURA cobe, NVL(COB.COCB_PO_COMISION,0) porcentaje, \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0, \n");
			strSelect.append("            0, \n");
			strSelect.append(
					"            SUM((COB.COCB_TA_RIESGO*(nvl(COB.COCB_CAMPOV2, CE.COCE_MT_SUMA_ASEGURADA)) * to_number(COB.COCB_CAMPOV1)) / decode(COB.COCB_CAMPON1, null, 1000,COB.COCB_CAMPON1))  emiReal, \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0 totalPma,  \n");
			strSelect.append("            0 asistencia,  \n");
			strSelect.append("            0 recargos,  \n");
			strSelect.append("            0 pmaBase,  \n");
			strSelect.append("            0 comisi  \n");
			strSelect.append("        FROM COLECTIVOS_CERTIFICADOS CE \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PRODUCTOS PR ON(PR.ALPR_CD_RAMO =  CE.COCE_CARP_CD_RAMO AND PR.ALPR_CD_PRODUCTO =  CE.COCE_CAPU_CD_PRODUCTO) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PLANES AP ON(AP.ALPL_CD_RAMO = PR.ALPR_CD_RAMO AND AP.ALPL_CD_PRODUCTO =  PR.ALPR_CD_PRODUCTO AND AP.ALPL_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_COBERTURAS COB ON(COB.COCB_CASU_CD_SUCURSAL = CE.COCE_CASU_CD_SUCURSAL   \n");
			strSelect.append("                                    AND COB.COCB_CARP_CD_RAMO = CE.COCE_CARP_CD_RAMO \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPO_NU_POLIZA = DECODE(NVL(CE.COCE_SUB_CAMPANA, 0), 0, CE.COCE_CAPO_NU_POLIZA, SUBSTR(CE.COCE_CAPO_NU_POLIZA, 0, 5))      \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPU_CD_PRODUCTO = CE.COCE_CAPU_CD_PRODUCTO \n");
			strSelect
					.append("                                    AND COB.COCB_CAPB_CD_PLAN = CE.COCE_CAPB_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN CART_COBERTURAS CB ON(CB.CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA AND CB.CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO) \n");
			strSelect.append(
					"        INNER JOIN COLECTIVOS_CUENTAS CU ON(CU.COCU_RAMO = CE.COCE_CARP_CD_RAMO AND CU.COCU_POLIZA = CE.COCE_CAPO_NU_POLIZA AND CU.COCU_CERTIFICADO = CE.COCE_NU_CERTIFICADO)  \n");
			strSelect.append("    WHERE CE.COCE_CASU_CD_SUCURSAL = 1 \n");
			strSelect.append("        AND CE.COCE_CARP_CD_RAMO > 0 \n");
			strSelect.append("        AND CE.COCE_CAPO_NU_POLIZA BETWEEN 500000000 AND 999999999 \n");
			strSelect.append("        AND CE.COCE_NU_CERTIFICADO > 0   \n");
			strSelect.append(
					"        AND CE.COCE_FE_EMISION BETWEEN TO_DATE('01/'||to_char(SYSDATE,'MM/yyyy'),'dd/mm/yyyy') AND LAST_DAY(SYSDATE)  \n");
			strSelect.append("        AND CE.COCE_ST_CERTIFICADO IN (1,2)  \n");
			strSelect.append("        AND CU.COCU_CALIFICADOR = 4    \n");
			strSelect.append("    GROUP BY PR.ALPR_DE_PRODUCTO , CB.CACB_DE_COBERTURA,NVL(COB.COCB_PO_COMISION,0)  \n");
			strSelect.append("    UNION ALL  \n");
			strSelect.append(
					"    SELECT PR.ALPR_DE_PRODUCTO prod,CB.CACB_DE_COBERTURA cobe, NVL(COB.COCB_PO_COMISION,0) porcentaje,  \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0, \n");
			strSelect.append("            0, \n");
			strSelect.append("            0, \n");
			strSelect.append("            SUM(CU.COCU_MONTO_PRIMA) emiReal,  \n");
			strSelect.append("            0,  \n");
			strSelect.append("            0 totalPma,  \n");
			strSelect.append("            0 asistencia,  \n");
			strSelect.append("            0 recargos,  \n");
			strSelect.append("            0 pmaBase,  \n");
			strSelect.append("            0 comisi \n");
			strSelect.append("        FROM COLECTIVOS_CUENTAS CU  \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PRODUCTOS PR ON(PR.ALPR_CD_RAMO =  CU.COCU_RAMO AND PR.ALPR_DATO3 =  CU.COCU_ID_VENTA) \n");
			strSelect.append(
					"        INNER JOIN ALTERNA_PLANES AP ON(AP.ALPL_CD_RAMO = PR.ALPR_CD_RAMO AND AP.ALPL_CD_PRODUCTO =  PR.ALPR_CD_PRODUCTO AND AP.ALPL_CD_PLAN = 1) \n");
			strSelect.append("        INNER JOIN COLECTIVOS_COBERTURAS COB ON(COB.COCB_CASU_CD_SUCURSAL = 1   \n");
			strSelect.append("                                    AND COB.COCB_CARP_CD_RAMO = CU.COCU_RAMO \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPO_NU_POLIZA BETWEEN CU.COCU_ID_VENTA * 10000 AND (CU.COCU_ID_VENTA * 10000)+9999 \n");
			strSelect.append(
					"                                    AND COB.COCB_CAPU_CD_PRODUCTO = PR.ALPR_CD_PRODUCTO \n");
			strSelect.append("                                    AND COB.COCB_CAPB_CD_PLAN = AP.ALPL_CD_PLAN) \n");
			strSelect.append(
					"        INNER JOIN CART_COBERTURAS CB ON(CB.CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA AND CB.CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO)  \n");
			strSelect.append(
					"    WHERE CU.COCU_FECHA_INGRESO BETWEEN TO_DATE('01/'||to_char(SYSDATE,'MM/yyyy'),'dd/mm/yyyy') AND LAST_DAY(SYSDATE) \n");
			strSelect.append("        AND CU.COCU_CALIFICADOR IN(-1, 0)  \n");
			strSelect.append("        AND CB.CACB_CD_COBERTURA in('001', '003')     \n");
			strSelect.append(
					"        AND PR.ALPR_CD_PRODUCTO = (SELECT MIN(P.ALPR_CD_PRODUCTO) FROM ALTERNA_PRODUCTOS P  \n");
			strSelect.append(
					"                                                            WHERE P.ALPR_CD_RAMO = PR.ALPR_CD_RAMO \n");
			strSelect.append(
					"                                                                AND P.ALPR_DATO3 =  CU.COCU_ID_VENTA)    \n");
			strSelect.append("    GROUP BY PR.ALPR_DE_PRODUCTO,CB.CACB_DE_COBERTURA,NVL(COB.COCB_PO_COMISION,0)  \n");
			strSelect.append("          ) tabla group by tabla.prod, tabla.cobe, tabla.porcentaje order by 1 asc) \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectReporteEstimacion:: " + e.getMessage());
		}
		return strSelect.toString();
	}

	private String crearSelectBaseFinal() throws Exception {
		StringBuffer strSelect;
		// En la siguiente sentencia podemos apreciar que los querys se repiten y solo
		// cambie el ramo
		// esto es porque as� se solicit� elaborar
		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"select 'PRODUCTO' pro,'POLIZA' pol,'DESCRIPCION' des,'ASEGURADO' asegl,'FOLIO' fol,'DOC' docu,'FECHA PRODUC' fpro,'INICIO DE VIGENCIA' iniVig,'FIN DE VIGENCIA' fVig,'PRIMA NETA CARGO' pmaCar,'DERECHO CARGO' deCar,'RECARGO CARGO' rec,'SUBTOTAL' sub,'IVA CARGO' ivaCar,'EMITIDO' emi,'ESTADO' est,'FECHA DE PAGO' fpag,'FECHA DE HOY' fhoy,'CUENTA BANCARIA' ctaBan,'CUENTA DEUDOR' ctaDeu FROM dual \n");
			strSelect.append("UNION ALL \n");
			strSelect.append("    SELECT \n");
			strSelect.append(
					"          ''||producto,''||poliza,''||descripcion,''||asegurado,''||folio,''||doc,''||fProduc,''||inicioVigencia,''||finVigencia,''||pmaNetaCargo,''||dpo,''||rfi,''||subtotal,''||iva,''||emtido,''||estado,''||fecPago,''||fecHoy,''||cuentaBancaria,''||cuentaDeudor \n");
			strSelect.append("    FROM ( \n");
			strSelect.append(
					"          select decode(c.coce_capo_nu_poliza,3,'1-01',12590410,'1-01',12590411,'1-01',50,'VC',51,'VC',56,'VC','') producto, \n");
			strSelect.append("                 c.coce_capo_nu_poliza poliza, \n");
			strSelect.append("                 a.carp_de_ramo descripcion, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,'BANCO SANTANDER S.A.',null,'BANCO SANTANDER S.A.','BANCO SANTANDER SERFIN, S.A.') asegurado, \n");
			strSelect.append("                 b.cofa_nu_recibo_fiscal folio, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,b.cofa_cuota_recibo || '/' || d.cafp_nu_pagos_ano,null,b.cofa_cuota_recibo || '/' || d.cafp_nu_pagos_ano,'1/1') doc, \n");
			strSelect.append("                 '' fProduc, \n");
			strSelect.append("                 to_char(c.coce_fe_desde,'dd/MM/yyyy') inicioVigencia, \n");
			strSelect.append("                 to_char(c.coce_fe_hasta,'dd/MM/yyyy') finVigencia, \n");
			strSelect.append("                 b.cofa_mt_tot_pma pmaNetaCargo, \n");
			strSelect.append("                 b.cofa_mt_tot_dpo dpo, \n");
			strSelect.append("                 b.cofa_mt_tot_rfi rfi, \n");
			strSelect.append(
					"                 (b.cofa_mt_tot_pma +  b.cofa_mt_tot_dpo +  b.cofa_mt_tot_rfi) subtotal, \n");
			strSelect.append("                 b.cofa_mt_tot_iva iva, \n");
			strSelect.append("                 b.cofa_mt_recibo emtido, \n");
			strSelect.append("                 'P' estado, \n");
			strSelect.append("                 '' fecPago, \n");
			strSelect.append("                 '' fecHoy, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,'54500000764 (101)',null,'54500000764 (101)','213302111201000') cuentaBancaria, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,'1603-03-99-11-01-000',null,'1603-03-99-11-01-000','') cuentaDeudor \n");
			strSelect.append("          from cart_ramos_polizas a, \n");
			strSelect.append("               colectivos_facturacion b, \n");
			strSelect.append("               colectivos_certificados c, \n");
			strSelect.append("               cart_fr_pagos d \n");
			strSelect.append("          where a.carp_cd_ramo = c.coce_carp_cd_ramo \n");
			strSelect.append("                and b.cofa_casu_cd_sucursal = c.coce_casu_cd_sucursal \n");
			strSelect.append("                and b.cofa_carp_cd_ramo = c.coce_carp_cd_ramo  \n");
			strSelect.append("                and b.cofa_capo_nu_poliza = c.coce_capo_nu_poliza \n");
			strSelect.append("                and b.cofa_nu_certificado = c.coce_nu_certificado \n");
			strSelect.append("                and c.coce_nu_certificado = 0 \n");
			strSelect.append("                and d.cafp_cd_fr_pago = c.coce_fr_pago \n");
			strSelect.append("                and c.coce_sub_campana > 0 \n");
			strSelect.append("          union all \n");
			strSelect.append(
					"          select decode(c.coce_capo_nu_poliza,3,'1-01',12590410,'1-01',12590411,'1-01',50,'VC',51,'VC',56,'VC','') producto, \n");
			strSelect.append("                 c.coce_capo_nu_poliza poliza, \n");
			strSelect.append("                 a.carp_de_ramo descripcion, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,'BANCO SANTANDER S.A.',null,'BANCO SANTANDER S.A.','BANCO SANTANDER SERFIN, S.A.') asegurado, \n");
			strSelect.append("                 b.cofa_nu_recibo_fiscal folio, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,b.cofa_cuota_recibo || '/' || d.cafp_nu_pagos_ano,null,b.cofa_cuota_recibo || '/' || d.cafp_nu_pagos_ano,'1/1') doc, \n");
			strSelect.append("                 '' fProduc, \n");
			strSelect.append("                 to_char(c.coce_fe_desde,'dd/MM/yyyy') inicioVigencia, \n");
			strSelect.append("                 to_char(c.coce_fe_hasta,'dd/MM/yyyy') finVigencia, \n");
			strSelect.append("                 b.cofa_mt_tot_pma pmaNetaCargo , \n");
			strSelect.append("                 b.cofa_mt_tot_dpo dpo, \n");
			strSelect.append("                 b.cofa_mt_tot_rfi rfi, \n");
			strSelect.append(
					"                 (b.cofa_mt_tot_pma +  b.cofa_mt_tot_dpo +  b.cofa_mt_tot_rfi) subtotal, \n");
			strSelect.append("                 b.cofa_mt_tot_iva, \n");
			strSelect.append("                 b.cofa_mt_recibo emtido, \n");
			strSelect.append("                 'P' estado, \n");
			strSelect.append("                 '' fecPago, \n");
			strSelect.append("                 '' fecHoy, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,'54500000764 (101)',null,'54500000764 (101)','213302111201000') cuentaBancaria, \n");
			strSelect.append(
					"                 decode(c.coce_sub_campana,0,'1603-03-99-11-01-000',null,'1603-03-99-11-01-000','') cuentaDeudor \n");
			strSelect.append("          from cart_ramos_polizas a, \n");
			strSelect.append("               colectivos_facturacion b, \n");
			strSelect.append("               colectivos_certificados c, \n");
			strSelect.append("               cart_fr_pagos d \n");
			strSelect.append("          where a.carp_cd_ramo = c.coce_carp_cd_ramo \n");
			strSelect.append("                and b.cofa_casu_cd_sucursal = c.coce_casu_cd_sucursal \n");
			strSelect.append("                and b.cofa_carp_cd_ramo = c.coce_carp_cd_ramo \n");
			strSelect.append("                and b.cofa_capo_nu_poliza = c.coce_capo_nu_poliza \n");
			strSelect.append("                and b.cofa_nu_certificado = c.coce_nu_certificado \n");
			strSelect.append("                and c.coce_nu_certificado = 0 \n");
			strSelect.append("                and d.cafp_cd_fr_pago = c.coce_fr_pago \n");
			strSelect.append(
					"                and c.coce_capo_nu_poliza in (3,10901,12590411,12590410,12513,12501,50,51,56,16102,16104,12590021) \n");
			strSelect.append("          ) \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectBaseFinal:: " + e.getMessage());
		}
		return strSelect.toString();
	}

	private String crearSelectIdVenta() throws Exception {
		StringBuffer strSelect;

		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"select distinct c.copa_nvalor4, c.copa_vvalor1 from colectivos_parametros c where c.copa_id_parametro = 'IDVENTA' \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectIdVenta:: " + e.getMessage());
		}
		return strSelect.toString();
	}

	private String crearSelectConsultaFoto() throws Exception {
		StringBuffer strSelect;
		// En la siguiente sentencia podemos apreciar que los querys se repiten y solo
		// cambie el ramo
		// esto es porque as� se solicit� elaborar
		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"select c.copd_campov1 mesEmision, c.copd_campon3 plazo, c.copd_prima_dep pmaNeta, c.copd_prima_rec bajas, c.copd_campov5 total \n");
			strSelect.append("from colectivos_primas_deposito c \n");
			strSelect.append("where c.copd_canal = 1 \n");
			strSelect.append("and c.copd_ramo = 9 \n");
			strSelect.append("and c.copd_idventa = 6 \n");
			strSelect.append("and c.copd_fecha_carga >= to_date('01/01/2017', 'dd/MM/yyyy') \n");
			strSelect.append("and c.copd_fecha_carga <= to_date('31/05/2017', 'dd/MM/yyyy') \n");
			strSelect.append("and c.copd_campon1 = 2 \n");

		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectIdVenta:: " + e.getMessage());
		}
		return strSelect.toString();
	}

	private String crearSelectReporteFiscalEmitidos(Short ramo, Integer idVenta, String strFechaIni, String strFechaFin)
			throws Exception {
		StringBuffer strSelect;

		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"select COUNT(*), to_char(c.coce_fe_suscripcion, 'YYYY MONTH'), c.coce_cd_plazo plazo, sum(c.coce_mt_prima_subsecuente) pmaNeta \n");
			strSelect.append("from colectivos_certificados c \n");
			strSelect.append("where c.coce_casu_cd_sucursal = 1 \n");
			strSelect.append("and c.coce_carp_cd_ramo = " + ramo + " \n");
			strSelect
					.append("and c.coce_capo_nu_poliza between " + idVenta + "00000000 and " + idVenta + "99999999 \n");
			strSelect.append("and c.coce_nu_certificado > 0 \n");
			strSelect.append("and c.coce_sub_campana = '").append(idVenta).append("' \n");
			strSelect.append("and C.COCE_CAMPON2 IN(6001,2008) \n");
			strSelect.append("and c.coce_fe_suscripcion BETWEEN TO_DATE('" + strFechaIni
					+ "', 'dd/MM/yyyy') AND TO_DATE('" + strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("group by to_char(c.coce_fe_suscripcion, 'YYYY MONTH'), c.coce_cd_plazo  \n");
			strSelect.append("ORDER BY 2,4 \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectReorteFiscalEmitidos:: " + e.getMessage());
		}

		return strSelect.toString();
	}

	// private void insertReporteFiscalEmitidos() throws Exception {
	// StringBuffer strInsert;
	//
	// try {
	// strInsert = new StringBuffer();
	//
	// strInsert.append("INSERT INTO COLECTIVOS_PRIMAS_DEPOSITO( ");
	// strInsert.append("COPD_CANAL, ");
	// strInsert.append("COPD_RAMO, ");
	// strInsert.append("COPD_IDVENTA, ");
	// strInsert.append("COPD_CONSECUTIVO, ");
	// strInsert.append("COPD_PRIMA_DEP, ");
	// strInsert.append("COPD_PRIMA_REC, ");
	// strInsert.append("COPD_FECHA_CARGA, ");
	// strInsert.append("COPD_REMESA ) \n");
	// strInsert.append("VALUES ( ");
	//
	// } catch (Exception e) {
	// throw new Exception("Error::
	// ServicioReporteadorImpl.insertReporteFiscalEmitidos:: " + e.getMessage());
	// }
	// }

	private String crearSelectReporteFiscalBajas(Short ramo, Integer idVenta, String strFechaIni, String strFechaFin)
			throws Exception {
		StringBuffer strSelect;

		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"select COUNT(*), to_char(c.coce_fe_suscripcion, 'YYYY MONTH'), c.coce_cd_plazo plazo, sum(c.coce_mt_devolucion) pma1, sum(c.coce_mt_bco_devolucion) pma2 \n");
			strSelect.append("from colectivos_certificados c \n");
			strSelect.append("where c.coce_casu_cd_sucursal = 1 \n");
			strSelect.append("and c.coce_carp_cd_ramo = " + ramo + " \n");
			strSelect
					.append("and c.coce_capo_nu_poliza between " + idVenta + "00000000 and " + idVenta + "99999999 \n");
			strSelect.append("and c.coce_nu_certificado > 0 \n");
			strSelect.append("and c.coce_sub_campana = '").append(idVenta).append("' \n");
			strSelect.append("and c.coce_st_certificado in (10,11) \n");
			strSelect.append("and c.coce_fe_anulacion BETWEEN TO_DATE('" + strFechaIni
					+ "', 'dd/MM/yyyy') AND TO_DATE('" + strFechaFin + "', 'dd/MM/yyyy') \n");
			strSelect.append("group by to_char(c.coce_fe_suscripcion, 'YYYY MONTH'), c.coce_cd_plazo  \n");
			strSelect.append("ORDER BY 2,4 \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectReorteFiscalBajas:: " + e.getMessage());
		}

		return strSelect.toString();
	}

	private String crearSelectConsultaCuentas() throws Exception {
		StringBuffer strSelect;

		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"select distinct(c.copa_vvalor1) from colectivos_parametros c where c.copa_id_parametro = 'CUENTAS' and c.copa_des_parametro = 'CONCILIACION' \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectConsultaCuentas:: " + e.getMessage());
		}
		return strSelect.toString();
	}

	private String crearSelectReporteCuentas() throws Exception {
		StringBuffer strSelect;

		try {
			strSelect = new StringBuffer();
			strSelect.append(
					"SELECT 'CUENTA' cuenta,'FECHA' fecha,'HORA' hora,'SUCURSAL' sucursal,'MOVIMIENTO' movimento,'+/-' masMenos,'PRIMA (ABS)' pmaAbs,'PRIMA' pma,'SALDO' saldo,'RED' red,'CREDITO' credito FROM dual \n");
			strSelect.append("UNION ALL \n");
			strSelect.append(
					"SELECT ''||cuenta,''||fecha,''||hora,''||sucursal,''||movimiento,''||masmenos,''||primaABS,''||prima,''||saldo,''||red,''||credito from(  \n");
			strSelect.append(
					"select a.cocu_cuenta_pago cuenta, a.cocu_fecha_ingreso fecha, 0 hora, 0 sucursal, a.cocu_tipo_movimiento movimiento, decode(a.cocu_tipo_movimiento,'C','-','+') masmenos, \n");
			strSelect.append(
					"a.cocu_monto_prima primaABS,a.cocu_monto_prima prima,0 saldo,decode(a.cocu_no_credito, null,'','0') red,a.cocu_no_credito credito \n");
			strSelect.append("from colectivos_cuentas a \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearSelectReporteCuentas:: " + e.getMessage());
		}
		return strSelect.toString();
	}

	private ArrayList<String> getNombreCampoEmitidas(int nCampo) {
		ArrayList<String> arlResultados = new ArrayList<String>();
		String strCampo = "";
		String strInner = "";

		switch (nCampo) {
		case 1: // RAMO
			strCampo = "C.COCE_CARP_CD_RAMO";
			break;
		case 2: // CENTRO COSTOS
			strCampo = "PL.ALPL_DATO3";
			break;
		case 3: // POLIZA
			strCampo = "C.COCE_CAPO_NU_POLIZA";
			break;
		case 4: // RECIBO
			strCampo = "C.COCE_NO_RECIBO";
			// strCampo = "NVL(C.COCE_BUC_EMPRESA, 0)";
			break;
		case 5: // CREDITO
			strCampo = "CC.COCC_ID_CERTIFICADO";
			break;
		case 6: // CERTIFICADO
			strCampo = "C.COCE_NU_CERTIFICADO";
			break;
		case 7: // IDENTIFICACION DE CUOTA
			strCampo = "NVL(PL.ALPL_DATO2,PL.ALPL_DE_PLAN)";
			break;
		case 8: // ESTATUS
			strCampo = "EST.ALES_CAMPO1";
			break;
		case 9: // ESTATUS MOVIMIENTO
			strCampo = "'EMITIDO'";
			break;
		case 10: // CUENTA
			strCampo = "C.COCE_NU_CUENTA";
			break;
		case 11: // PRODUCTO
			strCampo = "C.COCE_TP_PRODUCTO_BCO";
			break;
		case 12: // SUBPRODUCTO
			strCampo = "C.COCE_TP_SUBPROD_BCO";
			break;
		case 13: // SUCURSAL
			strCampo = "C.COCE_CAZB_CD_SUCURSAL";
			break;
		case 14: // NOMBRE
			strCampo = "CL.COCN_NOMBRE";
			break;
		case 15: // SEXO
			strCampo = "CL.COCN_CD_SEXO";
			break;
		case 16: // FECHA NACIMIENTO
			strCampo = "TO_CHAR(CL.COCN_FE_NACIMIENTO,'dd/MM/yyyy')";
			break;
		case 17: // NUMERO CLIENTE
			strCampo = "CL.COCN_BUC_CLIENTE";
			break;
		case 18: // RFC
			strCampo = "CL.COCN_RFC";
			break;
		case 19: // PLAZO
			strCampo = "C.COCE_CD_PLAZO";
			break;
		case 20: // FECHA INICIO POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 21: // FECHA FIN POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 22: // FECHA INGRESO
			strCampo = "TO_CHAR(C.COCE_FE_SUSCRIPCION,'dd/MM/yyyy')";
			break;
		case 23: // FECHA DESDE
			strCampo = "TO_CHAR(C.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 24: // FECHA HASTA
			strCampo = "TO_CHAR(C.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 25: // FECHA INICIO CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_INI_CREDITO,'dd/MM/yyyy')";
			break;
		case 26: // FECHA FIN CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_FIN_CREDITO,'dd/MM/yyyy')";
			break;
		case 27: // FECHA FIN CREDITO 2
			strCampo = "C.COCE_CAMPOV6";
			break;
		case 28: // FECHA ANULACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION,'dd/MM/yyyy')";
			break;
		case 29: // FECHA CANCELACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION_COL,'dd/MM/yyyy')";
			break;
		case 30: // SUMA ASEGURADA
			strCampo = "TRIM(TO_CHAR(DECODE(C.COCE_SUB_CAMPANA,'7',TO_NUMBER(C.COCE_MT_SUMA_ASEG_SI),C.COCE_MT_SUMA_ASEGURADA),'999999999.99'))";
			break;
		case 31: // BASE CALCULO
			strCampo = "DECODE(C.COCE_SUB_CAMPANA,'7',C.COCE_MT_SUMA_ASEGURADA,C.COCE_MT_SUMA_ASEG_SI)";
			break;
		case 32: // PRIMA_NETA
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)  ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 ";
			strCampo = strCampo + "then c.coce_mt_prima_pura ";
			strCampo = strCampo + "else 0 end";
			break;
		case 33: // DERECHOS
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1))";
			break;
		case 34: // RECARGOS
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1))";
			break;
		case 35: // IVA
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1))";
			break;
		case 36: // PRIMA TOTAL
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo + "+ nvl(c.coce_mt_prima_pura,0) ";
			strCampo = strCampo + "else 0 end";
			break;
		case 37: // TARIFA
			strCampo = "round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4)";
			break;
		case 38: // PRIMA VIDA
			strCampo = "round(case when c.coce_carp_cd_ramo in (61,65) then ";
			strCampo = strCampo
					+ "(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaVida ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 39: // PRIMA DESEMPLEO
			strCampo = "round(case when c.coce_carp_cd_ramo = 61 then ";
			strCampo = strCampo
					+ "(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaDes ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 40: // ESTADO
			strCampo = "ES.CAES_DE_ESTADO";
			break;
		case 41: // MUNICIPIO
			strCampo = "cl.cocn_delegmunic";
			break;
		case 42: // CODIGO POSTAL
			strCampo = "cl.cocn_cd_postal";
			break;
		case 43: // MONTO DEVOLUCION
			strCampo = "nvl(c.coce_mt_bco_devolucion,0)";
			break;
		case 44: // MONTO DEVOLUCION SIS
			strCampo = "nvl(c.coce_mt_devolucion,0)";
			break;
		case 45: // DIFERENCIA DEVOLUCION
			strCampo = "abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0))";
			break;
		case 46: // CREDITONUEVO
			strCampo = "nvl(c.coce_buc_empresa,0)";
			break;
		case 47: // PRODUCTO ASEGURADORA
			strCampo = "c.coce_capu_cd_producto";
			break;
		case 48: // PLAN ASEGURADORA
			strCampo = "c.coce_capb_cd_plan";
			break;
		case 49: // CUOTA BASICA
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)";
			break;
		case 50: // CUOTA DESEMPLEO
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)";
			break;
		case 51: // DATOS OBLIGADO PU
			strCampo = "case when pf.copa_nvalor4 > 0 then ";
			strCampo = strCampo + "(select cln.cocn_fe_nacimiento   ||'|'|| ";
			strCampo = strCampo + "cln.cocn_cd_sexo         ||'|'|| ";
			strCampo = strCampo + "clc.cocc_tp_cliente      ||'|'|| ";
			strCampo = strCampo + "p.copa_vvalor1           ||'|'|| ";
			strCampo = strCampo + "substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) ";
			strCampo = strCampo + "from colectivos_cliente_certif clc ";
			strCampo = strCampo + ",colectivos_clientes cln ";
			strCampo = strCampo + ",colectivos_parametros p ";
			strCampo = strCampo + "where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal ";
			strCampo = strCampo + "and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo ";
			strCampo = strCampo + "and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza ";
			strCampo = strCampo + "and clc.cocc_nu_certificado   = c.coce_nu_certificado ";
			strCampo = strCampo + "and nvl(clc.cocc_tp_cliente,1)  > 1 ";
			strCampo = strCampo + "and cln.cocn_nu_cliente       = clc.cocc_nu_cliente ";
			strCampo = strCampo + "and p.copa_des_parametro(+)   = 'ASEGURADO' ";
			strCampo = strCampo + "and p.copa_id_parametro(+)    = 'TIPO' ";
			strCampo = strCampo + "and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)";
			strCampo = strCampo + "else '' end";
			break;
		case 52: // FECHA EMISION
			strCampo = "null";
			break;
		case 53: // FECHA ENVIO
			strCampo = "null";
			break;
		case 54: // FECHA CARGA
			strCampo = "null";
			break;
		case 55: // FECHA RECIBIDO
			strCampo = "null";
			break;
		case 56: // APELLIDO PATERNO
			strCampo = "CL.COCN_APELLIDO_PAT";
			break;
		case 57: // APELLIDO MATERNO
			strCampo = "CL.COCN_APELLIDO_MAT";
			break;
		}

		switch (nCampo) {
		case 2: // CENTRO COSTOS
		case 7: // IDENTIFICACION CUOTA
			if (!flgJoinAlternaPlanes) {
				strInner = "INNER JOIN  alterna_planes pl on(pl.alpl_cd_ramo = c.coce_carp_cd_ramo and pl.alpl_cd_producto = c.coce_capu_cd_producto and pl.alpl_cd_plan = c.coce_capb_cd_plan)";
			}
			flgJoinAlternaPlanes = true;
			break;
		case 5: // CREDITO
			if (!flgJoinColectivosCliente) {
				strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza AND cc.cocc_nu_certificado = c.coce_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
			}
			flgJoinColectivosCliente = true;
			break;
		case 8: // Estatus
			strInner = "INNER JOIN  alterna_estatus est on(est.ales_cd_estatus = c.coce_st_certificado)";
			break;
		case 14: // NOMBRE CL
		case 15: // SEXO CL
		case 16: // FECHA NACIMIENTO CL
		case 17: // NUMERO CLIENTE CL
		case 18: // RFC CL
		case 40: // ESTADO CL Y ES
		case 41: // MUNICIPIO CL
		case 42: // CODIGO POSTAL CL
		case 56: // APELLIDO PAT
		case 57: // APELLIDO MAT
			if (!flgJoinClientes) {
				if (!flgJoinColectivosCliente) {
					strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza AND cc.cocc_nu_certificado = c.coce_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
				}
				flgJoinColectivosCliente = true;
				strInner = strInner
						+ "\n INNER JOIN  colectivos_clientes cl on(cl.cocn_nu_cliente = cc.cocc_nu_cliente)";
			}
			if (nCampo == 40) {
				strInner = strInner + "LEFT JOIN   cart_estados es on(es.caes_cd_estado = cl.cocn_cd_estado)";
			}
			flgJoinClientes = true;
			break;
		case 20: // FECHA INICIO POLIZA
		case 21: // FECHA FIN POLIZA
			if (!flgJoinHead) {
				strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and head.coce_carp_cd_ramo = c.coce_carp_cd_ramo and head.coce_capo_nu_poliza = c.coce_capo_nu_poliza and head.coce_nu_certificado = 0)";
			}
			flgJoinHead = true;
			break;
		case 32: // PRIMA NETA PF
		case 36: // PRIMA TOTAL PF
			if (!flgJoinParametros) {
				if (!flgJoinHead) {
					strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and head.coce_carp_cd_ramo = c.coce_carp_cd_ramo and head.coce_capo_nu_poliza = c.coce_capo_nu_poliza and head.coce_nu_certificado = 0)";
				}
				flgJoinHead = true;
				strInner = strInner
						+ "\n INNER JOIN  colectivos_parametros pf on(pf.copa_des_parametro    = 'POLIZA' and pf.copa_id_parametro    = 'GEPREFAC')";
			}
			flgJoinParametros = true;
			break;
		}
		arlResultados.add(0, strCampo);
		arlResultados.add(1, strInner);

		return arlResultados;
	}

	private ArrayList<String> getNombreCampoCancelado(int nCampo) {
		ArrayList<String> arlResultados = new ArrayList<String>();
		String strCampo = "";
		String strInner = "";

		switch (nCampo) {
		case 1: // RAMO
			strCampo = "C.COCE_CARP_CD_RAMO";
			break;
		case 2: // CENTRO COSTOS
			strCampo = "PL.ALPL_DATO3";
			break;
		case 3: // POLIZA
			strCampo = "C.COCE_CAPO_NU_POLIZA";
			break;
		case 4: // RECIBO
			strCampo = "C.COCE_NO_RECIBO";
			// strCampo = "NVL(C.COCE_BUC_EMPRESA, 0)";
			break;
		case 5: // CREDITO
			strCampo = "CC.COCC_ID_CERTIFICADO";
			break;
		case 6: // CERTIFICADO
			strCampo = "C.COCE_NU_CERTIFICADO";
			break;
		case 7: // IDENTIFICACION DE CUOTA
			strCampo = "NVL(PL.ALPL_DATO2,PL.ALPL_DE_PLAN)";
			break;
		case 8: // ESTATUS
			strCampo = "EST.ALES_CAMPO1";
			break;
		case 9: // ESTATUS MOVIMIENTO
			strCampo = "'BAJA'";
			break;
		case 10: // CUENTA
			strCampo = "C.COCE_NU_CUENTA";
			break;
		case 11: // PRODUCTO
			strCampo = "C.COCE_TP_PRODUCTO_BCO";
			break;
		case 12: // SUBPRODUCTO
			strCampo = "C.COCE_TP_SUBPROD_BCO";
			break;
		case 13: // SUCURSAL
			strCampo = "C.COCE_CAZB_CD_SUCURSAL";
			break;
		case 14: // NOMBRE
			strCampo = "CL.COCN_NOMBRE || '/' || CL.COCN_APELLIDO_PAT || '/' || CL.COCN_APELLIDO_MAT";
			break;
		case 15: // SEXO
			strCampo = "CL.COCN_CD_SEXO";
			break;
		case 16: // FECHA NACIMIENTO
			strCampo = "TO_CHAR(CL.COCN_FE_NACIMIENTO,'dd/MM/yyyy')";
			break;
		case 17: // NUMERO CLIENTE
			strCampo = "CL.COCN_BUC_CLIENTE";
			break;
		case 18: // RFC
			strCampo = "CL.COCN_RFC";
			break;
		case 19: // PLAZO
			strCampo = "C.COCE_CD_PLAZO";
			break;
		case 20: // FECHA INICIO POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 21: // FECHA FIN POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 22: // FECHA INGRESO
			strCampo = "TO_CHAR(C.COCE_FE_SUSCRIPCION,'dd/MM/yyyy')";
			break;
		case 23: // FECHA DESDE
			strCampo = "TO_CHAR(C.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 24: // FECHA HASTA
			strCampo = "TO_CHAR(C.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 25: // FECHA INICIO CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_INI_CREDITO,'dd/MM/yyyy')";
			break;
		case 26: // FECHA FIN CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_FIN_CREDITO,'dd/MM/yyyy')";
			break;
		case 27: // FECHA FIN CREDITO 2
			strCampo = "C.COCE_CAMPOV6";
			break;
		case 28: // FECHA ANULACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION,'dd/MM/yyyy')";
			break;
		case 29: // FECHA CANCELACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION_COL,'dd/MM/yyyy')";
			break;
		case 30: // SUMA ASEGURADA
			strCampo = "TRIM(TO_CHAR(DECODE(C.COCE_SUB_CAMPANA,'7',TO_NUMBER(C.COCE_MT_SUMA_ASEG_SI),C.COCE_MT_SUMA_ASEGURADA),'999999999.99'))";
			break;
		case 31: // BASE CALCULO
			strCampo = "DECODE(C.COCE_SUB_CAMPANA,'7',C.COCE_MT_SUMA_ASEGURADA,C.COCE_MT_SUMA_ASEG_SI)";
			break;
		case 32: // PRIMA_NETA
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)  ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 ";
			strCampo = strCampo + "then c.coce_mt_prima_pura ";
			strCampo = strCampo + "else 0 end";
			break;
		case 33: // DERECHOS
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1))";
			break;
		case 34: // RECARGOS
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1))";
			break;
		case 35: // IVA
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1))";
			break;
		case 36: // PRIMA TOTAL
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo + "+ nvl(c.coce_mt_prima_pura,0) ";
			strCampo = strCampo + "else 0 end";
			break;
		case 37: // TARIFA
			strCampo = "round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4)";
			break;
		case 38: // PRIMA VIDA
			strCampo = "round(case when c.coce_carp_cd_ramo in (61,65) then ";
			strCampo = strCampo
					+ "(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaVida ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 39: // PRIMA DESEMPLEO
			strCampo = "round(case when c.coce_carp_cd_ramo = 61 then ";
			strCampo = strCampo
					+ "(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaDes ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 40: // ESTADO
			strCampo = "CL.COCN_CD_ESTADO || ' - ' || ES.CAES_DE_ESTADO";
			break;
		case 41: // MUNICIPIO
			strCampo = "cl.cocn_delegmunic";
			break;
		case 42: // CODIGO POSTAL
			strCampo = "cl.cocn_cd_postal";
			break;
		case 43: // MONTO DEVOLUCION
			strCampo = "nvl(c.coce_mt_bco_devolucion,0)";
			break;
		case 44: // MONTO DEVOLUCION SIS
			strCampo = "nvl(c.coce_mt_devolucion,0)";
			break;
		case 45: // DIFERENCIA DEVOLUCION
			strCampo = "abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0))";
			break;
		case 46: // CREDITONUEVO
			strCampo = "nvl(c.coce_buc_empresa,0)";
			break;
		case 47: // PRODUCTO ASEGURADORA
			strCampo = "c.coce_capu_cd_producto";
			break;
		case 48: // PLAN ASEGURADORA
			strCampo = "c.coce_capb_cd_plan";
			break;
		case 49: // CUOTA BASICA
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)";
			break;
		case 50: // CUOTA DESEMPLEO
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)";
			break;
		case 51: // DATOS OBLIGADO PU
			strCampo = "case when pf.copa_nvalor4 > 0 then ";
			strCampo = strCampo + "(select cln.cocn_fe_nacimiento   ||'|'|| ";
			strCampo = strCampo + "cln.cocn_cd_sexo         ||'|'|| ";
			strCampo = strCampo + "clc.cocc_tp_cliente      ||'|'|| ";
			strCampo = strCampo + "p.copa_vvalor1           ||'|'|| ";
			strCampo = strCampo + "substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) ";
			strCampo = strCampo + "from colectivos_cliente_certif clc ";
			strCampo = strCampo + ",colectivos_clientes cln ";
			strCampo = strCampo + ",colectivos_parametros p ";
			strCampo = strCampo + "where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal ";
			strCampo = strCampo + "and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo ";
			strCampo = strCampo + "and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza ";
			strCampo = strCampo + "and clc.cocc_nu_certificado   = c.coce_nu_certificado ";
			strCampo = strCampo + "and nvl(clc.cocc_tp_cliente,1)  > 1 ";
			strCampo = strCampo + "and cln.cocn_nu_cliente       = clc.cocc_nu_cliente ";
			strCampo = strCampo + "and p.copa_des_parametro(+)   = 'ASEGURADO' ";
			strCampo = strCampo + "and p.copa_id_parametro(+)    = 'TIPO' ";
			strCampo = strCampo + "and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)";
			strCampo = strCampo + "else '' end";
			break;
		case 52: // FECHA EMISION
			strCampo = "null";
			break;
		case 53: // FECHA ENVIO
			strCampo = "null";
			break;
		case 54: // FECHA CARGA
			strCampo = "null";
			break;
		case 55: // FECHA RECIBIDO
			strCampo = "null";
			break;
		case 56: // APELLIDO PATERNO
			strCampo = "CL.COCN_APELLIDO_PAT";
			break;
		case 57: // APELLIDO MATERNO
			strCampo = "CL.COCN_APELLIDO_MAT";
			break;
		}

		switch (nCampo) {
		case 2: // CENTRO COSTOS
		case 7: // IDENTIFICACION CUOTA
			if (!flgJoinAlternaPlanes) {
				strInner = "INNER JOIN  alterna_planes pl on(pl.alpl_cd_ramo = c.coce_carp_cd_ramo and pl.alpl_cd_producto = c.coce_capu_cd_producto and pl.alpl_cd_plan = c.coce_capb_cd_plan)";
			}
			flgJoinAlternaPlanes = true;
			break;
		case 5: // CREDITO
			if (!flgJoinColectivosCliente) {
				strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza AND cc.cocc_nu_certificado = c.coce_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
			}
			flgJoinColectivosCliente = true;
			break;
		case 8: // Estatus
			strInner = "INNER JOIN  alterna_estatus est on(est.ales_cd_estatus = c.coce_st_certificado)";
			break;
		case 14: // NOMBRE CL
		case 15: // SEXO CL
		case 16: // FECHA NACIMIENTO CL
		case 17: // NUMERO CLIENTE CL
		case 18: // RFC CL
		case 40: // ESTADO CL Y ES
		case 41: // MUNICIPIO CL
		case 42: // CODIGO POSTAL CL
			if (!flgJoinClientes) {
				if (!flgJoinColectivosCliente) {
					strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza AND cc.cocc_nu_certificado = c.coce_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
				}
				flgJoinColectivosCliente = true;
				strInner = strInner
						+ "\n INNER JOIN  colectivos_clientes cl on(cl.cocn_nu_cliente = cc.cocc_nu_cliente)";
			}
			if (nCampo == 40) {
				strInner = strInner + "LEFT JOIN   cart_estados es on(es.caes_cd_estado = cl.cocn_cd_estado)";
			}
			flgJoinClientes = true;
			break;
		case 20: // FECHA INICIO POLIZA
		case 21: // FECHA FIN POLIZA
			if (!flgJoinHead) {
				strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and head.coce_carp_cd_ramo = c.coce_carp_cd_ramo and head.coce_capo_nu_poliza = c.coce_capo_nu_poliza and head.coce_nu_certificado = 0)";
			}
			flgJoinHead = true;
			break;
		case 32: // PRIMA NETA PF
		case 36: // PRIMA TOTAL PF
			if (!flgJoinParametros) {
				if (!flgJoinHead) {
					strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and head.coce_carp_cd_ramo = c.coce_carp_cd_ramo and head.coce_capo_nu_poliza = c.coce_capo_nu_poliza and head.coce_nu_certificado = 0)";
				}
				flgJoinHead = true;
				strInner = strInner
						+ "\n INNER JOIN  colectivos_parametros pf on(pf.copa_des_parametro    = 'POLIZA' and pf.copa_id_parametro    = 'GEPREFAC')";
			}
			flgJoinParametros = true;
			break;
		}
		arlResultados.add(0, strCampo);
		arlResultados.add(1, strInner);

		return arlResultados;
	}

	private ArrayList<String> getNombreCampoFacturado(int nCampo) {
		ArrayList<String> arlResultados = new ArrayList<String>();
		String strCampo = "";
		String strInner = "";

		switch (nCampo) {
		case 1: // RAMO
			strCampo = "C.COCE_CARP_CD_RAMO";
			break;
		case 2: // CENTRO COSTOS
			strCampo = "PL.ALPL_DATO3";
			break;
		case 3: // POLIZA
			strCampo = "C.COCE_CAPO_NU_POLIZA";
			break;
		case 4: // RECIBO
			strCampo = "C.COCE_NO_RECIBO";
			// strCampo = "NVL(C.COCE_BUC_EMPRESA, 0)";
			break;
		case 5: // CREDITO
			strCampo = "CC.COCC_ID_CERTIFICADO";
			break;
		case 6: // CERTIFICADO
			strCampo = "C.COCE_NU_CERTIFICADO";
			break;
		case 7: // IDENTIFICACION DE CUOTA
			strCampo = "NVL(PL.ALPL_DATO2,PL.ALPL_DE_PLAN)";
			break;
		case 8: // ESTATUS
			strCampo = "EST.ALES_CAMPO1";
			break;
		case 9: // ESTATUS MOVIMIENTO
			strCampo = "'ALTA'";
			break;
		case 10: // CUENTA
			strCampo = "C.COCE_NU_CUENTA";
			break;
		case 11: // PRODUCTO
			strCampo = "C.COCE_TP_PRODUCTO_BCO";
			break;
		case 12: // SUBPRODUCTO
			strCampo = "C.COCE_TP_SUBPROD_BCO";
			break;
		case 13: // SUCURSAL
			strCampo = "C.COCE_CAZB_CD_SUCURSAL";
			break;
		case 14: // NOMBRE
			strCampo = "CL.COCN_NOMBRE || '/' || CL.COCN_APELLIDO_PAT || '/' || CL.COCN_APELLIDO_MAT";
			break;
		case 15: // SEXO
			strCampo = "CL.COCN_CD_SEXO";
			break;
		case 16: // FECHA NACIMIENTO
			strCampo = "TO_CHAR(CL.COCN_FE_NACIMIENTO,'dd/MM/yyyy')";
			break;
		case 17: // NUMERO CLIENTE
			strCampo = "CL.COCN_BUC_CLIENTE";
			break;
		case 18: // RFC
			strCampo = "CL.COCN_RFC";
			break;
		case 19: // PLAZO
			strCampo = "C.COCE_CD_PLAZO";
			break;
		case 20: // FECHA INICIO POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 21: // FECHA FIN POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 22: // FECHA INGRESO
			strCampo = "TO_CHAR(C.COCE_FE_SUSCRIPCION,'dd/MM/yyyy')";
			break;
		case 23: // FECHA DESDE
			strCampo = "TO_CHAR(C.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 24: // FECHA HASTA
			strCampo = "TO_CHAR(C.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 25: // FECHA INICIO CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_INI_CREDITO,'dd/MM/yyyy')";
			break;
		case 26: // FECHA FIN CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_FIN_CREDITO,'dd/MM/yyyy')";
			break;
		case 27: // FECHA FIN CREDITO 2
			strCampo = "C.COCE_CAMPOV6";
			break;
		case 28: // FECHA ANULACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION,'dd/MM/yyyy')";
			break;
		case 29: // FECHA CANCELACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION_COL,'dd/MM/yyyy')";
			break;
		case 30: // SUMA ASEGURADA
			strCampo = "TRIM(TO_CHAR(DECODE(C.COCE_SUB_CAMPANA,'7',TO_NUMBER(C.COCE_MT_SUMA_ASEG_SI),C.COCE_MT_SUMA_ASEGURADA),'999999999.99'))";
			break;
		case 31: // BASE CALCULO
			strCampo = "DECODE(C.COCE_SUB_CAMPANA,'7',C.COCE_MT_SUMA_ASEGURADA,C.COCE_MT_SUMA_ASEG_SI)";
			break;
		case 32: // PRIMA_NETA
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)  ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 ";
			strCampo = strCampo + "then c.coce_mt_prima_pura ";
			strCampo = strCampo + "else 0 end";
			break;
		case 33: // DERECHOS
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1))";
			break;
		case 34: // RECARGOS
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1))";
			break;
		case 35: // IVA
			strCampo = "to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1))";
			break;
		case 36: // PRIMA TOTAL
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo + "+ nvl(c.coce_mt_prima_pura,0) ";
			strCampo = strCampo + "else 0 end";
			break;
		case 37: // TARIFA
			strCampo = "round(c.coce_mt_prima_pura*1000/decode(c.coce_sub_campana,'7',decode(c.coce_mt_suma_asegurada, 0,1,c.coce_mt_suma_asegurada),decode(c.coce_mt_suma_aseg_si, 0,1,c.coce_mt_suma_aseg_si)),4)";
			break;
		case 38: // PRIMA VIDA
			strCampo = "round(case when c.coce_carp_cd_ramo in (61,65) then ";
			strCampo = strCampo
					+ "(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaVida ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = c.coce_carp_cd_ramo ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 39: // PRIMA DESEMPLEO
			strCampo = "round(case when c.coce_carp_cd_ramo = 61 then ";
			strCampo = strCampo
					+ "(decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaDes ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 40: // ESTADO
			strCampo = "CL.COCN_CD_ESTADO || ' - ' || ES.CAES_DE_ESTADO";
			break;
		case 41: // MUNICIPIO
			strCampo = "cl.cocn_delegmunic";
			break;
		case 42: // CODIGO POSTAL
			strCampo = "cl.cocn_cd_postal";
			break;
		case 43: // MONTO DEVOLUCION
			strCampo = "nvl(c.coce_mt_bco_devolucion,0)";
			break;
		case 44: // MONTO DEVOLUCION SIS
			strCampo = "nvl(c.coce_mt_devolucion,0)";
			break;
		case 45: // DIFERENCIA DEVOLUCION
			strCampo = "abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0))";
			break;
		case 46: // CREDITONUEVO
			strCampo = "nvl(c.coce_buc_empresa,0)";
			break;
		case 47: // PRODUCTO ASEGURADORA
			strCampo = "c.coce_capu_cd_producto";
			break;
		case 48: // PLAN ASEGURADORA
			strCampo = "c.coce_capb_cd_plan";
			break;
		case 49: // CUOTA BASICA
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)";
			break;
		case 50: // CUOTA DESEMPLEO
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.coce_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.coce_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.coce_nu_cobertura)";
			break;
		case 51: // DATOS OBLIGADO PU
			strCampo = "case when pf.copa_nvalor4 > 0 then ";
			strCampo = strCampo + "(select cln.cocn_fe_nacimiento   ||'|'|| ";
			strCampo = strCampo + "cln.cocn_cd_sexo         ||'|'|| ";
			strCampo = strCampo + "clc.cocc_tp_cliente      ||'|'|| ";
			strCampo = strCampo + "p.copa_vvalor1           ||'|'|| ";
			strCampo = strCampo + "substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) ";
			strCampo = strCampo + "from colectivos_cliente_certif clc ";
			strCampo = strCampo + ",colectivos_clientes cln ";
			strCampo = strCampo + ",colectivos_parametros p ";
			strCampo = strCampo + "where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal ";
			strCampo = strCampo + "and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo ";
			strCampo = strCampo + "and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza ";
			strCampo = strCampo + "and clc.cocc_nu_certificado   = c.coce_nu_certificado ";
			strCampo = strCampo + "and nvl(clc.cocc_tp_cliente,1)  > 1 ";
			strCampo = strCampo + "and cln.cocn_nu_cliente       = clc.cocc_nu_cliente ";
			strCampo = strCampo + "and p.copa_des_parametro(+)   = 'ASEGURADO' ";
			strCampo = strCampo + "and p.copa_id_parametro(+)    = 'TIPO' ";
			strCampo = strCampo + "and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)";
			strCampo = strCampo + "else '' end";
			break;
		case 52: // FECHA EMISION
			strCampo = "null";
			break;
		case 53: // FECHA ENVIO
			strCampo = "null";
			break;
		case 54: // FECHA CARGA
			strCampo = "null";
			break;
		case 55: // FECHA RECIBIDO
			strCampo = "null";
			break;
		case 56: // APELLIDO PATERNO
			strCampo = "CL.COCN_APELLIDO_PAT";
			break;
		case 57: // APELLIDO MATERNO
			strCampo = "CL.COCN_APELLIDO_MAT";
			break;
		}

		switch (nCampo) {
		case 2: // CENTRO COSTOS
		case 7: // IDENTIFICACION CUOTA
			if (!flgJoinAlternaPlanes) {
				strInner = "INNER JOIN  alterna_planes pl on(pl.alpl_cd_ramo = c.coce_carp_cd_ramo and pl.alpl_cd_producto = c.coce_capu_cd_producto and pl.alpl_cd_plan = c.coce_capb_cd_plan)";
			}
			flgJoinAlternaPlanes = true;
			break;
		case 5: // CREDITO
			if (!flgJoinColectivosCliente) {
				strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza AND cc.cocc_nu_certificado = c.coce_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
			}
			flgJoinColectivosCliente = true;
			break;
		case 8: // Estatus
			strInner = "INNER JOIN  alterna_estatus est on(est.ales_cd_estatus = c.coce_st_certificado)";
			break;
		case 14: // NOMBRE CL
		case 15: // SEXO CL
		case 16: // FECHA NACIMIENTO CL
		case 17: // NUMERO CLIENTE CL
		case 18: // RFC CL
		case 40: // ESTADO CL Y ES
		case 41: // MUNICIPIO CL
		case 42: // CODIGO POSTAL CL
			if (!flgJoinClientes) {
				if (!flgJoinColectivosCliente) {
					strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza AND cc.cocc_nu_certificado = c.coce_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
				}
				flgJoinColectivosCliente = true;
				strInner = strInner
						+ "\n INNER JOIN  colectivos_clientes cl on(cl.cocn_nu_cliente = cc.cocc_nu_cliente)";
			}
			if (nCampo == 40) {
				strInner = strInner + "LEFT JOIN   cart_estados es on(es.caes_cd_estado = cl.cocn_cd_estado)";
			}
			flgJoinClientes = true;
			break;
		case 20: // FECHA INICIO POLIZA
		case 21: // FECHA FIN POLIZA
			if (!flgJoinHead) {
				strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and head.coce_carp_cd_ramo = c.coce_carp_cd_ramo and head.coce_capo_nu_poliza = c.coce_capo_nu_poliza and head.coce_nu_certificado = 0)";
			}
			flgJoinHead = true;
			break;
		case 32: // PRIMA NETA PF
		case 36: // PRIMA TOTAL PF
			if (!flgJoinParametros) {
				if (!flgJoinHead) {
					strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and head.coce_carp_cd_ramo = c.coce_carp_cd_ramo and head.coce_capo_nu_poliza = c.coce_capo_nu_poliza and head.coce_nu_certificado = 0)";
				}
				flgJoinHead = true;
				strInner = strInner
						+ "\n INNER JOIN  colectivos_parametros pf on(pf.copa_des_parametro    = 'POLIZA' and pf.copa_id_parametro    = 'GEPREFAC')";
			}
			flgJoinParametros = true;
			break;
		}
		arlResultados.add(0, strCampo);
		arlResultados.add(1, strInner);

		return arlResultados;
	}

	private ArrayList<String> getNombreCampoFacturado2(int nCampo) {
		ArrayList<String> arlResultados = new ArrayList<String>();
		String strCampo = "";
		String strInner = "";

		switch (nCampo) {
		case 1: // RAMO
			strCampo = "C.COCM_CARP_CD_RAMO";
			break;
		case 2: // CENTRO COSTOS
			strCampo = "PL.ALPL_DATO3";
			break;
		case 3: // POLIZA
			strCampo = "C.COCM_CAPO_NU_POLIZA";
			break;
		case 4: // RECIBO
			strCampo = "C.COCM_NU_RECIBO";
			// strCampo = "NVL(C.COCE_BUC_EMPRESA, 0)";
			break;
		case 5: // CREDITO
			strCampo = "CC.COCC_ID_CERTIFICADO";
			break;
		case 6: // CERTIFICADO
			strCampo = "C.COCM_NU_CERTIFICADO";
			break;
		case 7: // IDENTIFICACION DE CUOTA
			strCampo = "NVL(PL.ALPL_DATO2,PL.ALPL_DE_PLAN)";
			break;
		case 8: // ESTATUS
			strCampo = "EST.ALES_CAMPO1";
			break;
		case 9: // ESTATUS MOVIMIENTO
			strCampo = "'ALTA'";
			break;
		case 10: // CUENTA
			strCampo = "C.COCM_NU_CUENTA";
			break;
		case 11: // PRODUCTO
			strCampo = "C.COCM_TP_PRODUCTO_BCO";
			break;
		case 12: // SUBPRODUCTO
			strCampo = "C.COCM_TP_SUBPROD_BCO";
			break;
		case 13: // SUCURSAL
			strCampo = "C.COCM_CAZB_CD_SUCURSAL";
			break;
		case 14: // NOMBRE
			strCampo = "CL.COCN_NOMBRE || '/' || CL.COCN_APELLIDO_PAT || '/' || CL.COCN_APELLIDO_MAT";
			break;
		case 15: // SEXO
			strCampo = "CL.COCN_CD_SEXO";
			break;
		case 16: // FECHA NACIMIENTO
			strCampo = "TO_CHAR(CL.COCN_FE_NACIMIENTO,'dd/MM/yyyy')";
			break;
		case 17: // NUMERO CLIENTE
			strCampo = "CL.COCN_BUC_CLIENTE";
			break;
		case 18: // RFC
			strCampo = "CL.COCN_RFC";
			break;
		case 19: // PLAZO
			strCampo = "C.COCM_CD_PLAZO";
			break;
		case 20: // FECHA INICIO POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 21: // FECHA FIN POLIZA
			strCampo = "TO_CHAR(HEAD.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 22: // FECHA INGRESO
			strCampo = "TO_CHAR(C.COCE_FE_SUSCRIPCION,'dd/MM/yyyy')";
			break;
		case 23: // FECHA DESDE
			strCampo = "TO_CHAR(C.COCE_FE_DESDE,'dd/MM/yyyy')";
			break;
		case 24: // FECHA HASTA
			strCampo = "TO_CHAR(C.COCE_FE_HASTA,'dd/MM/yyyy')";
			break;
		case 25: // FECHA INICIO CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_INI_CREDITO,'dd/MM/yyyy')";
			break;
		case 26: // FECHA FIN CREDITO
			strCampo = "TO_CHAR(C.COCE_FE_FIN_CREDITO,'dd/MM/yyyy')";
			break;
		case 27: // FECHA FIN CREDITO 2
			strCampo = "C.COCE_CAMPOV6";
			break;
		case 28: // FECHA ANULACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION,'dd/MM/yyyy')";
			break;
		case 29: // FECHA CANCELACION
			strCampo = "TO_CHAR(C.COCE_FE_ANULACION_COL,'dd/MM/yyyy')";
			break;
		case 30: // SUMA ASEGURADA
			strCampo = "TRIM(TO_CHAR(DECODE(C.COCM_SUB_CAMPANA,'7',TO_NUMBER(C.COCM_MT_SUMA_ASEG_SI),C.COCM_MT_SUMA_ASEGURADA),'999999999.99'))";
			break;
		case 31: // BASE CALCULO
			strCampo = "DECODE(C.COCM_SUB_CAMPANA,'7',C.COCM_MT_SUMA_ASEGURADA,C.COCM_MT_SUMA_ASEG_SI)";
			break;
		case 32: // PRIMA_NETA
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)  ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 ";
			strCampo = strCampo + "then c.cocm_mt_prima_pura ";
			strCampo = strCampo + "else 0 end";
			break;
		case 33: // DERECHOS
			strCampo = "to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1) -instr(c.cocm_di_cobro1, 'O',1,1)-1))";
			break;
		case 34: // RECARGOS
			strCampo = "to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2) -instr(c.cocm_di_cobro1, 'I',1,1)-1))";
			break;
		case 35: // IVA
			strCampo = "to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1))";
			break;
		case 36: // PRIMA TOTAL
			strCampo = "case when pf.copa_nvalor5 = 0 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) ";
			strCampo = strCampo + "when pf.copa_nvalor5 = 1 then ";
			strCampo = strCampo
					+ "nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0) ";
			strCampo = strCampo
					+ "+ nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) ";
			strCampo = strCampo + "+ nvl(c.cocm_mt_prima_pura,0) ";
			strCampo = strCampo + "else 0 end";
			break;
		case 37: // TARIFA
			strCampo = "round(c.cocm_mt_prima_pura*1000/decode(c.cocm_sub_campana,'7',decode(c.cocm_mt_suma_asegurada, 0,1,c.cocm_mt_suma_asegurada),decode(c.cocm_mt_suma_aseg_si, 0,1,c.cocm_mt_suma_aseg_si)),4)";
			break;
		case 38: // PRIMA VIDA
			strCampo = "round(case when c.cocm_carp_cd_ramo in (61,65) then ";
			strCampo = strCampo
					+ "(decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaVida ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = c.cocm_carp_cd_ramo ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 39: // PRIMA DESEMPLEO
			strCampo = "round(case when c.cocm_carp_cd_ramo = 61 then ";
			strCampo = strCampo
					+ "(decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si)/1000) * ";
			strCampo = strCampo + "(select sum (cob.cocb_ta_riesgo) tasaDes ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura) ";
			strCampo = strCampo + "end,2)";
			break;
		case 40: // ESTADO
			strCampo = "CL.COCN_CD_ESTADO || ' - ' || ES.CAES_DE_ESTADO";
			break;
		case 41: // MUNICIPIO
			strCampo = "cl.cocn_delegmunic";
			break;
		case 42: // CODIGO POSTAL
			strCampo = "cl.cocn_cd_postal";
			break;
		case 43: // MONTO DEVOLUCION
			strCampo = "nvl(c.cocm_mt_bco_devolucion,0)";
			break;
		case 44: // MONTO DEVOLUCION SIS
			strCampo = "nvl(c.cocm_mt_devolucion,0)";
			break;
		case 45: // DIFERENCIA DEVOLUCION
			strCampo = "abs( nvl(c.cocm_mt_devolucion,0) - nvl(c.cocm_mt_bco_devolucion,0))";
			break;
		case 46: // CREDITONUEVO
			strCampo = "nvl(c.cocm_buc_empresa,0)";
			break;
		case 47: // PRODUCTO ASEGURADORA
			strCampo = "c.cocm_capu_cd_producto";
			break;
		case 48: // PLAN ASEGURADORA
			strCampo = "c.cocm_capb_cd_plan";
			break;
		case 49: // CUOTA BASICA
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  <> '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura)";
			break;
		case 50: // CUOTA DESEMPLEO
			strCampo = "(select sum (cob.cocb_ta_riesgo) ";
			strCampo = strCampo + "from colectivos_coberturas cob ";
			strCampo = strCampo + "where cob.cocb_casu_cd_sucursal =  1 ";
			strCampo = strCampo + "and cob.cocb_carp_cd_ramo     = 61 ";
			strCampo = strCampo + "and cob.cocb_carb_cd_ramo     = 14 ";
			strCampo = strCampo + "and cob.cocb_cacb_cd_cobertura  = '003' ";
			strCampo = strCampo + "and cob.cocb_capu_cd_producto   = c.cocm_capu_cd_producto ";
			strCampo = strCampo + "and cob.cocb_capb_cd_plan       = c.cocm_capb_cd_plan ";
			strCampo = strCampo + "and cob.cocb_cer_nu_cobertura   = c.cocm_nu_cobertura)";
			break;
		case 51: // DATOS OBLIGADO PU
			strCampo = "case when pf.copa_nvalor4 > 0 then ";
			strCampo = strCampo + "(select cln.cocn_fe_nacimiento   ||'|'|| ";
			strCampo = strCampo + "cln.cocn_cd_sexo         ||'|'|| ";
			strCampo = strCampo + "clc.cocc_tp_cliente      ||'|'|| ";
			strCampo = strCampo + "p.copa_vvalor1           ||'|'|| ";
			strCampo = strCampo + "substr(c.cocm_empresa,instr(c.cocm_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) ";
			strCampo = strCampo + "from colectivos_cliente_certif clc ";
			strCampo = strCampo + ",colectivos_clientes cln ";
			strCampo = strCampo + ",colectivos_parametros p ";
			strCampo = strCampo + "where clc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal ";
			strCampo = strCampo + "and clc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo ";
			strCampo = strCampo + "and clc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza ";
			strCampo = strCampo + "and clc.cocc_nu_certificado   = c.cocm_nu_certificado ";
			strCampo = strCampo + "and nvl(clc.cocc_tp_cliente,1)  > 1 ";
			strCampo = strCampo + "and cln.cocn_nu_cliente       = clc.cocc_nu_cliente ";
			strCampo = strCampo + "and p.copa_des_parametro(+)   = 'ASEGURADO' ";
			strCampo = strCampo + "and p.copa_id_parametro(+)    = 'TIPO' ";
			strCampo = strCampo + "and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)";
			strCampo = strCampo + "else '' end";
			break;
		case 52: // FECHA EMISION
			strCampo = "null";
			break;
		case 53: // FECHA ENVIO
			strCampo = "null";
			break;
		case 54: // FECHA CARGA
			strCampo = "null";
			break;
		case 55: // FECHA RECIBIDO
			strCampo = "null";
			break;
		case 56: // APELLIDO PATERNO
			strCampo = "CL.COCN_APELLIDO_PAT";
			break;
		case 57: // APELLIDO MATERNO
			strCampo = "CL.COCN_APELLIDO_MAT";
			break;
		}

		switch (nCampo) {
		case 2: // CENTRO COSTOS
		case 7: // IDENTIFICACION CUOTA
			if (!flgJoinAlternaPlanes2) {
				strInner = "INNER JOIN  alterna_planes pl on(pl.alpl_cd_ramo = c.cocm_carp_cd_ramo and pl.alpl_cd_producto = c.cocm_capu_cd_producto and pl.alpl_cd_plan = c.cocm_capb_cd_plan)";
			}
			flgJoinAlternaPlanes2 = true;
			break;
		case 5: // CREDITO
			if (!flgJoinColectivosCliente2) {
				strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.cocm_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.cocm_capo_nu_poliza AND cc.cocc_nu_certificado = c.cocm_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
			}
			flgJoinColectivosCliente2 = true;
			break;
		case 8: // Estatus
			strInner = "INNER JOIN  alterna_estatus est on(est.ales_cd_estatus = c.cocm_st_certificado)";
			break;
		case 14: // NOMBRE CL
		case 15: // SEXO CL
		case 16: // FECHA NACIMIENTO CL
		case 17: // NUMERO CLIENTE CL
		case 18: // RFC CL
		case 40: // ESTADO CL Y ES
		case 41: // MUNICIPIO CL
		case 42: // CODIGO POSTAL CL
			if (!flgJoinClientes2) {
				if (flgJoinColectivosCliente2) {
					strInner = "INNER JOIN  colectivos_cliente_certif cc on(cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal AND cc.cocc_carp_cd_ramo = c.coce_carp_cd_ramo AND cc.cocc_capo_nu_poliza = c.coce_capo_nu_poliza AND cc.cocc_nu_certificado = c.coce_nu_certificado AND nvl(cc.cocc_tp_cliente,1) = 1)";
				}
				flgJoinColectivosCliente2 = true;
				strInner = "INNER JOIN  colectivos_clientes cl on(cl.cocn_nu_cliente = cc.cocc_nu_cliente)";
			}
			if (nCampo == 40) {
				strInner = "LEFT JOIN   cart_estados es on(es.caes_cd_estado = cl.cocn_cd_estado)";
			}
			flgJoinClientes2 = true;
			break;
		case 20: // FECHA INICIO POLIZA
		case 21: // FECHA FIN POLIZA
			if (!flgJoinHead2) {
				strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.cocm_casu_cd_sucursal and head.coce_carp_cd_ramo = c.cocm_carp_cd_ramo and head.coce_capo_nu_poliza = c.cocm_capo_nu_poliza and head.coce_nu_certificado = 0)";
			}
			flgJoinHead2 = true;
			break;
		case 32: // PRIMA NETA PF
		case 36: // PRIMA TOTAL PF
			if (!flgJoinParametros2) {
				if (!flgJoinHead2) {
					strInner = "INNER JOIN  colectivos_certificados head on(head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal and head.coce_carp_cd_ramo = c.coce_carp_cd_ramo and head.coce_capo_nu_poliza = c.coce_capo_nu_poliza and head.coce_nu_certificado = 0)";
				}
				flgJoinHead2 = true;
				strInner = "INNER JOIN  colectivos_parametros pf on(pf.copa_des_parametro    = 'POLIZA' and pf.copa_id_parametro    = 'GEPREFAC')";
			}
			flgJoinParametros2 = true;
			break;
		}
		arlResultados.add(0, strCampo);
		arlResultados.add(1, strInner);

		return arlResultados;
	}

	private String crearFiltroEmitido(Short ramo, Long poliza, Integer inIdVenta, Date fecini, Date fecfin,
			boolean filtroParametros) throws Exception {
		StringBuffer sbFiltro, sbFiltro2;
		Integer nCanal = 1;
		String cadenacan = null;
		Integer inicioPoliza = 0;

		try {
			sbFiltro = new StringBuffer();
			sbFiltro2 = new StringBuffer();

			if (poliza == 0) {
				sbFiltro.append(" and P.copaDesParametro = 'POLIZA' \n");
				sbFiltro.append(" and P.copaIdParametro  = 'GEP' \n");
				sbFiltro.append(" and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append(" and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append(" and P.copaNvalor6 = ").append(inIdVenta).append("\n");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0) {
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				} else {
					inicioPoliza = -1;
				}

				cadenacan = " AND C.COCE_CAPO_NU_POLIZA >= " + inicioPoliza + "00000000 AND C.COCE_CAPO_NU_POLIZA <= "
						+ inicioPoliza + "99999999 \n" + " AND C.COCE_SUB_CAMPANA = '" + inIdVenta + "' \n";
			} else {
				sbFiltro.append(" and P.copaDesParametro= 'POLIZA' \n");
				sbFiltro.append("  and P.copaIdParametro = 'GEP'    \n");
				sbFiltro.append("  and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza).append(", 1, substr(")
						.append(poliza).append(",0,3), 2, substr(").append(poliza).append(",0,2))\n");
				sbFiltro.append("order by P.copaNvalor3");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				for (Parametros item : listaPolizas) {
					if (item.getCopaNvalor4().toString().equals("1")) {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA LIKE '" + poliza + "%'";
					} else {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA = " + poliza;
					}
				}
			}

			sbFiltro2.append(" WHERE C.COCE_CASU_CD_SUCURSAL =  " + nCanal + "\n");
			sbFiltro2.append(" AND C.COCE_CARP_CD_RAMO = " + ramo + " \n");
			sbFiltro2.append(cadenacan + "  \n ");
			sbFiltro2.append(" AND C.COCE_NU_CERTIFICADO > 0 \n");

			if (fecini != null) {
				sbFiltro2.append("AND C.COCE_FE_CARGA BETWEEN TO_DATE('")
						.append(GestorFechas.formatDate(fecini, "dd/MM/yyyy")).append("', 'dd/MM/yyyy') AND TO_DATE('")
						.append(GestorFechas.formatDate(fecfin, "dd/MM/yyyy")).append("', 'dd/MM/yyyy')");
			}

			if (filtroParametros) {
				// sbFiltro2.append(" AND PF.COPA_DES_PARAMETRO = 'POLIZA' \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR1 = C.COCE_CASU_CD_SUCURSAL \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR2 = C.COCE_CARP_CD_RAMO \n");
				sbFiltro2.append(
						" AND PF.COPA_NVALOR3 = DECODE(PF.COPA_NVALOR4, 0 ,C.COCE_CAPO_NU_POLIZA, 1, SUBSTR(C.COCE_CAPO_NU_POLIZA, 0, 3), SUBSTR(C.COCE_CAPO_NU_POLIZA, 0, 2)) \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR6 = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA, '0')) \n");
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioRepoteadorImpl.crearFiltroEmitido():: " + e.getMessage());
		}
		return sbFiltro2.toString();
	}

	private String crearFiltroCancelado(Short ramo, Long poliza, Integer inIdVenta, Date fecini, Date fecfin,
			boolean filtroParametros) throws Exception {
		StringBuffer sbFiltro, sbFiltro2;
		Integer nCanal = 1;
		String cadenacan = null;
		Integer inicioPoliza = 0;

		try {
			sbFiltro = new StringBuffer();
			sbFiltro2 = new StringBuffer();

			if (poliza == 0) {
				sbFiltro.append(" and P.copaDesParametro = 'POLIZA' \n");
				sbFiltro.append(" and P.copaIdParametro  = 'GEP' \n");
				sbFiltro.append(" and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append(" and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append(" and P.copaNvalor6 = ").append(inIdVenta).append("\n");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0) {
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				} else {
					inicioPoliza = -1;
				}

				cadenacan = " and c.coce_capo_nu_poliza >= " + inicioPoliza + "00000000 and c.coce_capo_nu_poliza <= "
						+ inicioPoliza + "99999999 \n" + " AND C.COCE_SUB_CAMPANA = '" + inIdVenta + "' \n";
			} else {
				sbFiltro.append(" and P.copaDesParametro= 'POLIZA' \n");
				sbFiltro.append("  and P.copaIdParametro = 'GEP'    \n");
				sbFiltro.append("  and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza).append(", 1, substr(")
						.append(poliza).append(",0,3), 2, substr(").append(poliza).append(",0,2))\n");
				sbFiltro.append("order by P.copaNvalor3");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				for (Parametros item : listaPolizas) {
					if (item.getCopaNvalor4().toString().equals("1")) {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA LIKE '" + poliza + "%'";
					} else {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA = " + poliza;
					}
				}
			}

			sbFiltro2.append(" WHERE C.COCE_CASU_CD_SUCURSAL =  " + nCanal + "\n");
			sbFiltro2.append(" AND C.COCE_CARP_CD_RAMO = " + ramo + " \n");
			sbFiltro2.append(cadenacan + "  \n ");
			sbFiltro2.append(" AND C.COCE_NU_CERTIFICADO > 0 \n");
			sbFiltro2.append(" and c.coce_st_certificado   in (10,11) \n");
			sbFiltro2.append(" and c.coce_cd_causa_anulacion <> 45 \n");
			sbFiltro2.append(" and nvl(cc.cocc_tp_cliente,1) = 1 \n");
			if (!flgJoinHead) {
				sbFiltro2.append(" and head.coce_nu_certificado   = 0 \n");
			}

			if (fecini != null) {
				sbFiltro2.append("AND C.COCE_FE_ANULACION BETWEEN TO_DATE('")
						.append(GestorFechas.formatDate(fecini, "dd/MM/yyyy")).append("', 'dd/MM/yyyy') AND TO_DATE('")
						.append(GestorFechas.formatDate(fecfin, "dd/MM/yyyy")).append("', 'dd/MM/yyyy')");
			}

			if (filtroParametros) {
				// sbFiltro2.append(" AND PF.COPA_DES_PARAMETRO = 'POLIZA' \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR1 = C.COCE_CASU_CD_SUCURSAL \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR2 = C.COCE_CARP_CD_RAMO \n");
				sbFiltro2.append(
						" AND PF.COPA_NVALOR3 = DECODE(PF.COPA_NVALOR4, 0 ,C.COCE_CAPO_NU_POLIZA, 1, SUBSTR(C.COCE_CAPO_NU_POLIZA, 0, 3), SUBSTR(C.COCE_CAPO_NU_POLIZA, 0, 2)) \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR6 = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA, '0')) \n");
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioRepoteadorImpl.crearFiltroCancelado():: " + e.getMessage());
		}
		return sbFiltro2.toString();
	}

	private String crearFiltroFacturado(Short ramo, Long poliza, Integer inIdVenta, Date fecini, Date fecfin,
			boolean filtroParametros) throws Exception {
		StringBuffer sbFiltro, sbFiltro2;
		Integer nCanal = 1;
		String cadenacan = null;
		Integer inicioPoliza = 0;

		try {
			sbFiltro = new StringBuffer();
			sbFiltro2 = new StringBuffer();

			if (poliza == 0) {
				sbFiltro.append(" and P.copaDesParametro = 'POLIZA' \n");
				sbFiltro.append(" and P.copaIdParametro  = 'GEP' \n");
				sbFiltro.append(" and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append(" and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append(" and P.copaNvalor6 = ").append(inIdVenta).append("\n");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0) {
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				} else {
					inicioPoliza = -1;
				}

				cadenacan = " and fac.cofa_capo_nu_poliza >= " + inicioPoliza
						+ "00000000 and fac.cofa_capo_nu_poliza <= " + inicioPoliza + "99999999 \n";
			} else {
				sbFiltro.append(" and P.copaDesParametro= 'POLIZA' \n");
				sbFiltro.append("  and P.copaIdParametro = 'GEP'    \n");
				sbFiltro.append("  and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza).append(", 1, substr(")
						.append(poliza).append(",0,3), 2, substr(").append(poliza).append(",0,2))\n");
				sbFiltro.append("order by P.copaNvalor3");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				for (Parametros item : listaPolizas) {
					if (item.getCopaNvalor4().toString().equals("1")) {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA LIKE '" + poliza + "%'";
					} else {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA = " + poliza;
					}
				}
			}
			sbFiltro2.append(
					"INNER JOIN  colectivos_facturacion fac on(fac.cofa_casu_cd_sucursal = c.coce_casu_cd_sucursal AND fac.cofa_carp_cd_ramo = c.coce_carp_cd_ramo AND fac.cofa_capo_nu_poliza = c.coce_capo_nu_poliza) \n");
			sbFiltro2.append(
					"INNER JOIN  colectivos_recibos r on(r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal and r.core_nu_recibo = fac.cofa_nu_recibo_fiscal) \n");
			sbFiltro2.append(" WHERE fac.cofa_casu_cd_sucursal =  " + nCanal + "\n");
			sbFiltro2.append(" AND fac.cofa_carp_cd_ramo = " + ramo + " \n");
			sbFiltro2.append(cadenacan + "  \n ");
			sbFiltro2.append(" AND C.COCE_NU_CERTIFICADO > 0 \n");
			sbFiltro2.append(" and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal\n");
			sbFiltro2.append(" and c.coce_campon2          in (0,2008)  \n");
			sbFiltro2.append(" and r.core_st_recibo        in (1,4)	 \n");

			if (fecini != null) {
				sbFiltro2.append("and fac.cofa_fe_facturacion BETWEEN TO_DATE('")
						.append(GestorFechas.formatDate(fecini, "dd/MM/yyyy")).append("', 'dd/MM/yyyy') AND TO_DATE('")
						.append(GestorFechas.formatDate(fecfin, "dd/MM/yyyy")).append("', 'dd/MM/yyyy')");
			}

			if (filtroParametros) {
				sbFiltro2.append(" AND PF.COPA_NVALOR1 = fac.cofa_casu_cd_sucursal \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR2 = fac.cofa_carp_cd_ramo \n");
				sbFiltro2.append(
						" AND PF.COPA_NVALOR3 = DECODE(PF.COPA_NVALOR4, 0 ,C.COCE_CAPO_NU_POLIZA, 1, SUBSTR(fac.cofa_capo_nu_poliza,0,3), SUBSTR(fac.cofa_capo_nu_poliza,0,2)) \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR6 = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA, '0')) \n");
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioRepoteadorImpl.crearFiltroFacturado():: " + e.getMessage());
		}
		return sbFiltro2.toString();
	}

	private String crearFiltroFacturado2(Short ramo, Long poliza, Integer inIdVenta, Date fecini, Date fecfin,
			boolean filtroParametros) throws Exception {
		StringBuffer sbFiltro, sbFiltro2;
		Integer nCanal = 1;
		String cadenacan = null;
		Integer inicioPoliza = 0;

		try {
			sbFiltro = new StringBuffer();
			sbFiltro2 = new StringBuffer();

			if (poliza == 0) {
				sbFiltro.append(" and P.copaDesParametro = 'POLIZA' \n");
				sbFiltro.append(" and P.copaIdParametro  = 'GEP' \n");
				sbFiltro.append(" and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append(" and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append(" and P.copaNvalor6 = ").append(inIdVenta).append("\n");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0) {
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				} else {
					inicioPoliza = -1;
				}

				cadenacan = " and fac.cofa_capo_nu_poliza >= " + inicioPoliza
						+ "00000000 and fac.cofa_capo_nu_poliza <= " + inicioPoliza + "99999999 \n";
			} else {
				sbFiltro.append(" and P.copaDesParametro= 'POLIZA' \n");
				sbFiltro.append("  and P.copaIdParametro = 'GEP'    \n");
				sbFiltro.append("  and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza).append(", 1, substr(")
						.append(poliza).append(",0,3), 2, substr(").append(poliza).append(",0,2))\n");
				sbFiltro.append("order by P.copaNvalor3");

				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				for (Parametros item : listaPolizas) {
					if (item.getCopaNvalor4().toString().equals("1")) {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA LIKE '" + poliza + "%'";
					} else {
						cadenacan = " AND C.COCE_CAPO_NU_POLIZA = " + poliza;
					}
				}
			}
			sbFiltro2.append(
					"INNER JOIN  colectivos_facturacion fac on(fac.cofa_casu_cd_sucursal = c.cocm_casu_cd_sucursal AND fac.cofa_carp_cd_ramo = c.cocm_carp_cd_ramo AND fac.cofa_capo_nu_poliza = c.cocm_capo_nu_poliza) \n");
			sbFiltro2.append(
					"INNER JOIN  colectivos_recibos r on(r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal and r.core_nu_recibo = fac.cofa_nu_recibo_fiscal) \n");
			sbFiltro2.append(" WHERE fac.cofa_casu_cd_sucursal =  " + nCanal + "\n");
			sbFiltro2.append(" AND fac.cofa_carp_cd_ramo = " + ramo + " \n");
			sbFiltro2.append(cadenacan + "  \n ");
			sbFiltro2.append(" AND C.COCM_NU_CERTIFICADO > 0 \n");
			sbFiltro2.append(" and c.cocm_nu_recibo        = fac.cofa_nu_recibo_fiscal \n");
			sbFiltro2.append(" and c.cocm_campon2          in (0,2008)  \n");
			sbFiltro2.append(" and r.core_st_recibo        in (1,4)	 \n");
			// sbFiltro2.append(" and nvl(cc.cocc_tp_cliente,1) = 1 \n");

			if (fecini != null) {
				sbFiltro2.append("and fac.cofa_fe_facturacion BETWEEN TO_DATE('")
						.append(GestorFechas.formatDate(fecini, "dd/MM/yyyy")).append("', 'dd/MM/yyyy') AND TO_DATE('")
						.append(GestorFechas.formatDate(fecfin, "dd/MM/yyyy")).append("', 'dd/MM/yyyy')");
			}

			if (filtroParametros) {
				sbFiltro2.append(" AND PF.COPA_NVALOR1 = fac.cofa_casu_cd_sucursal \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR2 = fac.cofa_carp_cd_ramo \n");
				sbFiltro2.append(
						" AND PF.COPA_NVALOR3 = DECODE(PF.COPA_NVALOR4, 0 ,C.COCE_CAPO_NU_POLIZA, 1, SUBSTR(fac.cofa_capo_nu_poliza,0,3), SUBSTR(fac.cofa_capo_nu_poliza,0,2)) \n");
				sbFiltro2.append(" AND PF.COPA_NVALOR6 = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA, '0')) \n");
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioRepoteadorImpl.crearFiltroFacturado()2:: " + e.getMessage());
		}
		return sbFiltro2.toString();
	}

	private String crearFiltroReporteCuentas(String strFechaIni, String strFechaFin) throws Exception {
		StringBuffer sbFiltro;

		try {
			sbFiltro = new StringBuffer();
			sbFiltro.append("where a.cocu_fecha_ingreso BETWEEN TO_DATE('" + strFechaIni
					+ "', 'dd/MM/yyyy') AND TO_DATE('" + strFechaFin + "', 'dd/MM/yyyy')) \n");
		} catch (Exception e) {
			throw new Exception("Error:: ServicioReporteadorImpl.crearFiltroReporteCuentas:: " + e.getMessage());
		}
		return sbFiltro.toString();
	}

	public CertificadoDao getCertificadoDAO() {
		return CertificadoDAO;
	}

	public void setCertificadoDAO(CertificadoDao CertificadoDAO) {
		this.CertificadoDAO = CertificadoDAO;
	}

	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	public ReporteadorDao getReporteadorDao() {
		return reporteadorDao;
	}

	public void setReporteadorDao(ReporteadorDao reporteadorDao) {
		this.reporteadorDao = reporteadorDao;
	}

}
