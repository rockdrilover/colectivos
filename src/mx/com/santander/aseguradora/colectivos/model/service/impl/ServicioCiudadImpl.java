/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.dao.CiudadDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCiudad;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author dflores
 *
 */
@Service
public class ServicioCiudadImpl implements ServicioCiudad {

	@Resource
	private CiudadDao ciudadDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;	
	
	/**
	 * @param estadoDao the estadoDao to set
	 */
	public void setCiudadDao(CiudadDao ciudadDao) {
		this.ciudadDao = ciudadDao;
	}

	/**
	 * @return the estadoDao
	 */
	public CiudadDao getCiudadDao() {
		return ciudadDao;
	}

	public ServicioCiudadImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
	}

	

	public List<Object> obtenerEstados() throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerEstados.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			//List<Object> lista = this.estadoDao.obtenerObjetos();
			List<Object> lista = this.ciudadDao.obtenerCiudad();
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se pueden recuperar los estados");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		this.message.append("obtenerEstados.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			List<T> lista = this.ciudadDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			message.append("No se pueden recuperar las ciudades");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public <T> List<T> obtenerObjetos() throws Excepciones {

		this.message.append("obtenerEstados.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			List<T> lista = this.ciudadDao.obtenerObjetos();
			return lista;
		} catch (Exception e) {
			message.append("No se pueden recuperar los productos");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}



}
