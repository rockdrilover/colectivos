/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author Ing Issac Bautista
 *
 */
public interface ServicioRamo extends ServicioCatalogo {

	public String getDescRamo(Long cdRamo) throws Excepciones;
}
