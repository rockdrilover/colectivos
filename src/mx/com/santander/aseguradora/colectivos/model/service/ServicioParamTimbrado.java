package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.ParamTimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Interface que implementa la clase ServicioParamTimbradoImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Remote
public interface ServicioParamTimbrado {

	/**
	 * Metodo que consulta datos de timbrado
	 * @param canal para realizar consulta
	 * @param ramo ramo para realizar consulta
	 * @param poliza poliza para realizar consulta
	 * @param idVenta id venta para realizar consulta
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	List<ParamTimbradoDTO> consultaDatos(Integer canal, Short ramo, Integer poliza, Integer idVenta) throws Excepciones;

	/**
	 * Metodo que regresa una lista de catalogos de CFDI para combos
	 * @param cfdiMetodosPago tipo de catalogo a regresar
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	List<Object> getComboCatalogosCFDI(int cfdiMetodosPago) throws Excepciones;
	

	/**
	 * Metodo que regresa una lista de catalogos de CFDI para combos
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	Map<String, String> getNombreProducto() throws Excepciones;
	
}
