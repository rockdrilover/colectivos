/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Facturacion;
import mx.com.santander.aseguradora.colectivos.model.dao.FacturacionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioFacturacion;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author Z014155
 * Modificacion: Sergio Plata
 *
 */

@Service
public class ServicioFacturacionImpl implements ServicioFacturacion {

	@Resource
	// declare
	private FacturacionDao facturacionDao;	
	private Log log=LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private List<Object> lista;
	// fin declare
	public ServicioFacturacionImpl() {
		// Constructor
		//this.cacheFacturacion=Collections.synchronizedMap(new LinkedHashMap<Short , Facturacion>());
		this.message= new StringBuilder();
		this.message.append("Inicia Servicio Facturacion");
		this.log.debug(message.toString());
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {		
		// TODO Auto-generated method stub	
		this.message.append("Metodo encargado de actualiza");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		//
		try {
			this.facturacionDao.actualizarObjeto(objeto);						
		} 
		catch (Exception e){
			message.append("Error : Al ejecutar el proceso de actualizacion").append(((Facturacion)objeto).getCofaMtRecibo());
			this.log.error(message.toString(),e);
			throw new Excepciones(message.toString(), e);
		}		
	}
	
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		// segun rosario no se ocupa, porque solo se realzaran actualizaciones y no borrar
		this.message.append("Metodo encargado de borrar");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		//
		try {
			this.facturacionDao.actualizarObjeto(objeto);
			this.lista.remove(((Facturacion)objeto)); 
		} 
		catch (Exception e){
			message.append("Error : Al ejecutar el proceso de borrado").append(((Facturacion)objeto).getCofaMtRecibo());
			this.log.error(message.toString(),e);
			throw new Excepciones(message.toString(), e);
		}		
	}
	
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		// segun rosario no se ocupa, porque solo se realzaran actualizaciones y no guarda reg completos
		this.message.append("Metodo encargado de borrar");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		//
		try {
			this.facturacionDao.guardarObjeto(objeto);
			return(T) objeto;
		} 
		catch (Exception e){
			message.append("Error : Al ejecutar el proceso de borrado").append(((Facturacion)objeto).getCofaMtRecibo());
			this.log.error(message.toString(),e);
			throw new Excepciones(message.toString(), e);
		}		
	

	}
	
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		try {
		return this.facturacionDao.obtenerObjeto(objeto, id);
		}
		catch (Exception e){
			message.append("Error : Al ejecutar consulta (obtenerObjeto)");
			this.log.error(message.toString(),e);
			throw new Excepciones(message.toString(), e);
		}		

	}
	
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		try {
		return this.facturacionDao.obtenerObjetos(objeto);
		}
		catch (Exception e){
			message.append("Error : Al ejecutar consulta (obtenerObjetos)");
			this.log.error(message.toString(),e);
			throw new Excepciones(message.toString(), e);
		}		

	}
	
	public <T> List<T> obtenerObjetos(String filtro)
			throws Excepciones {
		// TODO Auto-generated method stub
		try {
			List<T> lista=this.facturacionDao.obtenerObjetos(filtro);
			return lista;
		}
		catch (Exception e){
			message.append("Error : Al ejecutar consulta (obtenerObjetos x filtro)");
			this.log.error(message.toString(),e);
			throw new Excepciones(message.toString(), e);
		}		
 
	}

	/**
	 * @param facturacionDao the facturacionDao to set
	 */
	public void setFacturacionDao(FacturacionDao facturacionDao) {
		this.facturacionDao = facturacionDao;
	}

	/**
	 * @return the facturacionDao
	 */
	public FacturacionDao getFacturacionDao() {
		return facturacionDao;
	}
	/**
	 * @param lista the lista to set
	 */
	public void setLista(List<Object> lista) {
		this.lista = lista;
	}
	/**
	 * @return the lista
	 */
	public List<Object> getLista() {
		return lista;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}	
}