package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.ConfiguracionImpresionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioConfiguracionImpresion;

@Service
public class ServicioConfiguracionImpresionImpl implements ServicioConfiguracionImpresion{
	
	@Resource
	private ConfiguracionImpresionDao impresionDao;
	
	
	public ConfiguracionImpresionDao getImpresionDao() {
		return impresionDao;
	}


	public void setImpresionDao(ConfiguracionImpresionDao impresionDao) {
		this.impresionDao = impresionDao;
	}


	@Override
	public <T> List<T> getCamposBase() throws Exception {
		try {
			return impresionDao.getCamposBase();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String insertConfigImpresion(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto, Integer nPlan, String formulario, String base) throws Exception {
		
		try {
			return impresionDao.insertConfigImpresion(canal,ramo,poliza,idVenta,nProducto,nPlan,formulario,base);
		} catch (Exception e) {
			e.printStackTrace();
			return "Hubo problema al agregar";
		}
	}
	
	@Override
	public <T> List<T> consultaPlantilla(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto, Integer nPlan) throws Exception {
		try {
			return impresionDao.consultaPlantilla(canal,ramo,poliza,idVenta,nProducto,nPlan);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public String actualizaDatos(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto, Integer nPlan, String formulario, String base, String baseAnte) throws Exception {
		try {
			return impresionDao.actualizaDatos(canal,ramo,poliza,idVenta,nProducto,nPlan,formulario,base,baseAnte);
		}
		catch (Exception e) {
			e.printStackTrace();
			return "Error al modificar";
		}
	}


	@Override
	public String guardaPlantilla(Long next, String idParam, String desParam, int canal, Short ramo, Integer poliza, Integer nProducto, Integer nPlan, String nombreArchivo) throws Exception {
		try {
			return impresionDao.guardaPlantilla(next, idParam, desParam, canal, ramo, poliza, nProducto, nPlan, nombreArchivo);
		} catch (Exception e) {
			e.printStackTrace();
			return "Error al guardar";
		}
	}


	@Override
	public String getPlazo(Short ramo, Integer nProducto, Integer nPlan) throws Exception {
		try {
			return impresionDao.getPlazo(ramo, nProducto, nPlan);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public String existe(String filtro) throws Exception {
		try {
       	 return impresionDao.existe(filtro);
		} catch (Exception e) {
			throw new Excepciones("No se puede ejecutar existePlantilla", e);
		}
	}
	
	@Override
	public String editarNombrePlantilla(String plantilla, Map<String, String > filtro) {
		String respuesta = "0No se pudo llevar acabo la operaci\u00d3n";
		try {
			if (impresionDao.editarNombrePlantilla(plantilla, filtro)) {
				respuesta = "1Se actulizo de forma exitosa el nombre de la plantilla";
			 }
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		return respuesta;
	}
	
	@Override
	public String borrarPlantilla(String plantilla, Map<String, String > filtro) {
		String respuesta = "0No se pudo llevar acabo la operaci\u00d3n";
		try {
			if (impresionDao.borrarPlantilla(plantilla, filtro)) {
				respuesta = "1Se borro de forma exitosa el nombre de la plantilla";
			 }
		} catch (Exception e) {
			respuesta = e.getMessage();
		}
		return respuesta;
	}
}
