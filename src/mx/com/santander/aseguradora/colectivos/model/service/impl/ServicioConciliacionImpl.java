package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.CargaDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ConciliacionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioConciliacion;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
@Service
public class ServicioConciliacionImpl implements ServicioConciliacion {

	@Resource
	private ConciliacionDao conciliacionDao;
	@Resource
	private CargaDao cargaDao;
	private StringBuilder message = null;
	private Log log = LogFactory.getLog(this.getClass());

	public ServicioConciliacionImpl() {
		setMessage(new StringBuilder());
	}
	
	/**
	 * Solo para tipo reportes 3, 8 y 10
	 * Opcion --> 0 - Acumulado
	 * 		 	  1 - Detalle 
	 * 
	 * strDatos --> RAMO|TITULO|NOMBREIMAGEN
	 */
	public ArrayList<Object> consultar(Integer origen, Integer tipoRep, Date fecha, Date fechaFin, Integer opcion, String strDatos) throws Exception {
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery, sbFiltroVtaPre, sbFiltroPre, sbFiltroCar, sbFiltroCer;
		String[] arrColumnas = null;
		String strFechaIni, strFechaFin;
		String[] arrDatos;
		String productos = "";
		String subproductos = "";
		String polizas = "";
		
		try {
			arlResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbFiltroVtaPre = new StringBuffer();
			sbFiltroVtaPre.append("FROM VTA_PRECARGA PRE ");			
			sbFiltroPre = new StringBuffer();
			sbFiltroPre.append("FROM COLECTIVOS_PRECARGA CP ");
			sbFiltroCar = new StringBuffer();
			sbFiltroCar.append("FROM COLECTIVOS_CARGA CC ");
			sbFiltroCer = new StringBuffer();
			sbFiltroCer.append("FROM COLECTIVOS_CERTIFICADOS CC ");
			
			arrDatos = strDatos.split("\\|");
			strFechaIni = GestorFechas.formatDate(fecha, "dd/MM/yyyy");
			strFechaFin = GestorFechas.formatDate(fechaFin, "dd/MM/yyyy");
			if(origen.equals(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR)){
				sbFiltroVtaPre.append("WHERE PRE.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_ALTAMIRA_RAMO).append(" ");
				sbFiltroVtaPre.append("AND PRE.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_ALTAMIRA).append("' ");
				sbFiltroVtaPre.append("AND TO_DATE(PRE.VTAP_FE_INGRESO, 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
				
				sbFiltroPre.append("WHERE CP.COPC_BCO_PRODUCTO = ").append(13);
				sbFiltroCar.append("WHERE CC.COCR_BCO_PRODUCTO = ").append(13);
				sbFiltroCer.append("WHERE CC.COCE_TP_PRODUCTO_BCO = '13' ");
			} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR)){
				sbFiltroVtaPre.append("WHERE PRE.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_ALTAIR_RAMO).append(" ");
				sbFiltroVtaPre.append("AND PRE.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_ALTAIR).append("' ");
				sbFiltroVtaPre.append("AND TO_DATE(PRE.VTAP_FE_ESTATUS, 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
				
				sbFiltroPre.append("WHERE CP.COPC_BCO_PRODUCTO = ").append(90);
				sbFiltroCar.append("WHERE CC.COCR_BCO_PRODUCTO = ").append(90);
				sbFiltroCer.append("WHERE CC.COCE_TP_PRODUCTO_BCO = '90' ");
			} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR)){
				sbFiltroVtaPre.append("WHERE PRE.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_HIPOTECARIO_RAMO).append(" ");
				sbFiltroVtaPre.append("AND PRE.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_HIPOTECARIO).append("' ");
				sbFiltroVtaPre.append("AND TO_DATE(PRE.VTAP_FE_INGRESO , 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
				
				sbFiltroPre.append("WHERE CP.COPC_BCO_PRODUCTO IN ('20', '21') ");
				sbFiltroCar.append("WHERE CC.COCR_BCO_PRODUCTO IN ('20', '21') ");
				sbFiltroCer.append("WHERE CC.COCE_TP_PRODUCTO_BCO IN ('20', '21') ");
			} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_GAP_VALOR)){
				sbFiltroVtaPre.append("WHERE PRE.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_GAP_RAMO).append(" ");
				sbFiltroVtaPre.append("AND PRE.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_GAP).append("' ");
				sbFiltroVtaPre.append("AND TO_DATE(PRE.VTAP_FE_INGRESO , 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
				
				sbFiltroPre.append("WHERE CP.COPC_BCO_PRODUCTO = ").append(12);
				sbFiltroCar.append("WHERE CC.COCR_BCO_PRODUCTO = ").append(12);
				sbFiltroCer.append("WHERE CC.COCE_TP_PRODUCTO_BCO = '12' ");
			} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_LCI_VALOR)) {
				sbFiltroVtaPre.append("WHERE PRE.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_LCI_RAMO).append(" ");
				sbFiltroVtaPre.append("AND PRE.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_LCI).append("' ");
				sbFiltroVtaPre.append("AND TO_DATE(PRE.VTAP_FE_INGRESO , 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
				
				sbFiltroPre.append("WHERE CP.COPC_BCO_PRODUCTO = ").append(13);
				sbFiltroCar.append("WHERE CC.COCR_BCO_PRODUCTO = ").append(13);
				sbFiltroCer.append("WHERE CC.COCE_TP_PRODUCTO_BCO = '13' ");
			} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_LE_VALOR)) {
				sbFiltroVtaPre.append("WHERE PRE.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_LE_RAMO).append(" ");
				sbFiltroVtaPre.append("AND PRE.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_LE).append("' ");
				sbFiltroVtaPre.append("AND TO_DATE(PRE.VTAP_FE_INGRESO , 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
				
				sbFiltroPre.append("WHERE CP.COPC_BCO_PRODUCTO IN ('74', '75') ");
				sbFiltroCar.append("WHERE CC.COCR_BCO_PRODUCTO IN ('74', '75') ");
				sbFiltroCer.append("WHERE CC.COCE_TP_PRODUCTO_BCO IN ('74', '75') ");
			} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR)){
				
				StringBuilder query = new StringBuilder();
				
				query.append("Select CP.copa_vvalor1, CP.copa_vvalor2, CP.copa_nvalor3 \n");
				query.append("from colectivos_parametros CP \n");
				query.append("where CP.copa_des_parametro = 'CONCILIACION' \n");
				query.append("  and CP.copa_id_parametro = 'DES_TDC' \n");
				Collection lista = cargaDao.consultar(query.toString());
				
				for(Object object: lista){
					Object[] datos = (Object[]) object;
					productos = productos + String.format("'%s',", datos[0]);
					subproductos = subproductos + String.format("'%s',", datos[1]);
					polizas = polizas + String.format("%s,", datos[2]);
				}
				productos = productos.substring(0, productos.length() -1);
				subproductos = subproductos.substring(0, subproductos.length() -1);
				polizas = polizas.substring(0, polizas.length() -1);
						
				sbFiltroVtaPre.append("WHERE PRE.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO).append(" ");
				sbFiltroVtaPre.append("  AND PRE.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_DESEMPLEO_TDC).append("' ");
				sbFiltroVtaPre.append("  AND TO_DATE(PRE.VTAP_FE_CARGA , 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
				
				sbFiltroPre.append("WHERE CP.COPC_CD_SUCURSAL = 1 ");
				sbFiltroPre.append("  AND CP.COPC_CD_RAMO = ").append(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO).append(" ");
				sbFiltroPre.append("  AND CP.COPC_NUM_POLIZA in (").append(polizas).append(") ");
				sbFiltroPre.append("  AND CP.COPC_BCO_PRODUCTO in(").append(productos).append(") ");
				sbFiltroPre.append("  AND CP.COPC_SUB_PRODUCTO in(").append(subproductos).append(") ");
				
				sbFiltroCar.append("WHERE CC.COCR_CD_SUCURSAL = 1 ");
				//sbFiltroCar.append("  AND CC.COCR_CD_RAMO = ").append(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO).append(" ");
				sbFiltroCar.append("  AND CC.COCR_NUM_POLIZA in(").append(polizas).append(") ");
				sbFiltroCar.append("  AND CC.COCR_BCO_PRODUCTO in(").append(productos).append(") ");
				sbFiltroCar.append("  AND CC.COCR_SUB_PRODUCTO in(").append(subproductos).append(") ");
				
				//sbFiltroCer.append("WHERE CC.COCE_CASU_CD_SUCURSAL = 1 ");
				//sbFiltroCer.append("WHERE CC.COCE_CARP_CD_RAMO = ").append(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO).append(" ");
				sbFiltroCer.append("WHERE CC.COCE_CAPO_NU_POLIZA in (").append(polizas).append(") ");
				sbFiltroCer.append("  AND CC.COCE_TP_PRODUCTO_BCO in(").append(productos).append(") ");
				sbFiltroCer.append("  AND CC.COCE_TP_SUBPROD_BCO in(").append(subproductos).append(") ");				
			}			
			
			
			
			
			sbFiltroPre.append(" AND TO_DATE(CP.COPC_FE_INGRESO, 'dd/MM/yyyy') BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
			sbFiltroCar.append(" AND CC.COCR_FE_INGRESO BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
			
			sbFiltroCer.append(" AND CC.COCE_FE_SUSCRIPCION BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
			sbFiltroCer.append(" AND CC.COCE_CASU_CD_SUCURSAL = 1 ");
			sbFiltroCer.append(" AND CC.COCE_CAMPOV5 = '").append(Constantes.CONCILIACION_NOMBRE_ARCHIVO).append("' ");
						
			switch (tipoRep) {
				case Constantes.TIPO_REPORTE_VENTAS_NOCONCILIADO_VALOR:
					sbQuery.append("SELECT PRE.VTAP_ID_CERTIFICADO, ROUND(PRE.VTAP_PRIMA, 2) ");						
					sbQuery.append(sbFiltroVtaPre.toString());
						sbQuery.append("AND PRE.VTAP_CARGADA = 0 ");
					
					arrColumnas = new String[2];
					arrColumnas[0] = "NUMEROCREDITO";
					arrColumnas[1] = "PRIMA";
					break;

				case Constantes.TIPO_REPORTE_BAJAS_VALOR:	
					sbQuery.append("SELECT PRE.VTAP_ID_CERTIFICADO, ROUND(PRE.VTAP_PRIMA, 2), ROUND(PRE.VTAP_SUMA_ASEGURADA, 2), PRE.VTAP_DES_ERROR, PRE.VTAP_CD_ERROR ");
					sbQuery.append(sbFiltroVtaPre.toString());
						sbQuery.append("AND PRE.VTAP_CARGADA = 3 ");
						sbQuery.append("AND PRE.VTAP_CD_ERROR = '002'");
						
					arrColumnas = new String[5];					
					arrColumnas[0] = "NUMEROCREDITO";
					arrColumnas[1] = "PRIMA";
					arrColumnas[2] = "SUMAASEGURADA";
					arrColumnas[3] = "CODERROR";
					arrColumnas[4] = "DESCERROR";
					break;
					
//				case Constantes.TIPO_REPORTE_VENTAS_CONCILIADO_ERROR_VALOR:
//					sbQuery.append("SELECT PRE.VTAP_ID_CERTIFICADO, ROUND(PRE.VTAP_PRIMA, 2), PRE.VTAP_CD_ERROR, PRE.VTAP_DES_ERROR, ROUND(PRE.VTAP_NDATO8, 2) ");
//					sbQuery.append(sbFiltroVtaPre.toString());
//						sbQuery.append("AND PRE.VTAP_CARGADA IN (1) ");
//						sbQuery.append("AND (PRE.VTAP_CD_ERROR IS NULL) ");
//						
//					arrColumnas = new String[5];
//					arrColumnas[0] = "NUMEROCREDITO";
//					arrColumnas[1] = "PRIMA";
//					arrColumnas[2] = "CODERROR";
//					arrColumnas[3] = "DESCERROR";
//					arrColumnas[4] = "TARIFA";
//					break;

				case Constantes.TIPO_REPORTE_VENTAS_PREEMISION_VALOR:
					if(opcion.equals(0)){
						sbQuery.append("SELECT DES, SUM(CAN), SUM(PRI), IMG ");
							sbQuery.append("FROM (");
								sbQuery.append("SELECT '' DES, SUM(CAN) CAN, SUM(PRI) PRI, 'total.png' IMG ");
									sbQuery.append("FROM ( ");
										sbQuery.append("SELECT COUNT(*) CAN, 0 PRI ");
										sbQuery.append(sbFiltroPre.toString());
											sbQuery.append("AND CP.COPC_CD_RAMO = 61 ");
											sbQuery.append("AND CP.COPC_CARGADA = 0 ");
										sbQuery.append("UNION ");
										sbQuery.append("SELECT 0 CAN, SUM(ROUND(CP.COPC_PRIMA, 2)) PRI ");
										sbQuery.append(sbFiltroPre.toString());
											sbQuery.append("AND CP.COPC_CARGADA = 0) ");
								sbQuery.append("UNION ");
								sbQuery.append("SELECT '" + arrDatos[1].toString() + "' DES, COUNT(*) CAN, SUM(ROUND(CP.COPC_PRIMA, 2)) PRI, '" + arrDatos[2].toString() + "' IMG ");   
								sbQuery.append(sbFiltroPre.toString());
									sbQuery.append("AND CP.COPC_CD_RAMO = " + arrDatos[0].toString());
									sbQuery.append("AND CP.COPC_CARGADA = 0 ");
								sbQuery.append("UNION "); 
								sbQuery.append("SELECT 'Vida' DES, COUNT(*) CAN, SUM(ROUND(CP.COPC_PRIMA, 2)) PRI, 'vida.png' IMG ");
								sbQuery.append(sbFiltroPre.toString());	
									sbQuery.append("AND CP.COPC_CD_RAMO = 61 ");
									sbQuery.append("AND CP.COPC_CARGADA = 0 ");
								sbQuery.append("UNION ");
								sbQuery.append("SELECT '' DES, SUM(CAN) CAN, SUM(PRI) PRI, 'total.png' IMG ");
									sbQuery.append("FROM ( ");
										sbQuery.append("SELECT COUNT(*) CAN, 0 PRI ");
											sbQuery.append(sbFiltroCar.toString());
												sbQuery.append("AND CC.COCR_CD_RAMO = 61 ");
												sbQuery.append("AND CC.COCR_CARGADA = 1 ");
										sbQuery.append("UNION ");
										sbQuery.append("SELECT 0 CAN, SUM(ROUND(CC.COCR_PRIMA, 2)) PRI ");
										sbQuery.append(sbFiltroCar.toString());
											sbQuery.append("AND CC.COCR_CARGADA = 1) ");
								sbQuery.append("UNION ");
								sbQuery.append("SELECT '" + arrDatos[1].toString() + "' DES, COUNT(*) CAN, SUM(ROUND(CC.COCR_PRIMA, 2)) PRI, '" + arrDatos[2].toString() + "' IMG ");   
								sbQuery.append(sbFiltroCar.toString());	
									sbQuery.append("AND CC.COCR_CD_RAMO = " + arrDatos[0].toString());
									sbQuery.append("AND CC.COCR_CARGADA = 1 ");
								sbQuery.append("UNION "); 
								sbQuery.append("SELECT 'Vida' DES, COUNT(*) CAN, SUM(ROUND(CC.COCR_PRIMA, 2)) PRI, 'vida.png' IMG ");
								sbQuery.append(sbFiltroCar.toString());	
									sbQuery.append("AND CC.COCR_CD_RAMO = 61 ");
									sbQuery.append("AND CC.COCR_CARGADA = 1) ");
						sbQuery.append("GROUP BY DES, IMG ");
						sbQuery.append("ORDER BY 1 DESC ");
					} else {
						sbQuery.append("SELECT NUMCRE, SUM(PRIMA), SUMASEG, SUM(PRIMAVIDA), SUM(PRIMADESEMPLE), 0 ");
							sbQuery.append("FROM ( ");
								sbQuery.append("SELECT CP.COPC_NU_CREDITO NUMCRE, ROUND(CP.COPC_PRIMA, 2) PRIMA, ROUND(CP.COPC_SUMA_ASEGURADA, 2) SUMASEG, 0 PRIMAVIDA, ROUND(CP.COPC_PRIMA, 2) PRIMADESEMPLE ");    
								sbQuery.append(sbFiltroPre.toString());
									sbQuery.append("AND CP.COPC_CD_RAMO = " + arrDatos[0].toString());
									sbQuery.append("AND CP.COPC_CARGADA = 0 ");
								sbQuery.append("UNION ");
								sbQuery.append("SELECT CP.COPC_NU_CREDITO NUMCRE, ROUND(CP.COPC_PRIMA, 2) PRIMA, ROUND(CP.COPC_SUMA_ASEGURADA, 2) SUMASEG, ROUND(CP.COPC_PRIMA, 2) PRIMAVIDA, 0 PRIMADESEMPLE "); 
								sbQuery.append(sbFiltroPre.toString());
									sbQuery.append("AND CP.COPC_CD_RAMO = 61 ");
									sbQuery.append("AND CP.COPC_CARGADA = 0 ");
								sbQuery.append("UNION ");
								sbQuery.append("SELECT CC.COCR_NU_CREDITO NUMCRE, ROUND(CC.COCR_PRIMA, 2) PRIMA, ROUND(CC.COCR_SUMA_ASEGURADA, 2) SUMASEG, 0 PRIMAVIDA, ROUND(CC.COCR_PRIMA, 2) PRIMADESEMPLE ");    
								sbQuery.append(sbFiltroCar.toString());
									sbQuery.append("AND CC.COCR_CD_RAMO = " + arrDatos[0].toString());
									sbQuery.append("AND CC.COCR_CARGADA = 1 ");
								sbQuery.append("UNION ");
								sbQuery.append("SELECT CC.COCR_NU_CREDITO NUMCRE, ROUND(CC.COCR_PRIMA, 2) PRIMA, ROUND(CC.COCR_SUMA_ASEGURADA, 2) SUMASEG, ROUND(CC.COCR_PRIMA, 2) PRIMAVIDA, 0 PRIMADESEMPLE "); 
								sbQuery.append(sbFiltroCar.toString());
									sbQuery.append("AND CC.COCR_CD_RAMO = 61 ");
									sbQuery.append("AND CC.COCR_CARGADA = 1) ");
						sbQuery.append("GROUP BY NUMCRE, SUMASEG ");
						
						arrColumnas = new String[6];					
						arrColumnas[0] = "NUMEROCREDITO";
						arrColumnas[1] = "PRIMA";
						arrColumnas[2] = "SUMAASEGURADA";
						arrColumnas[3] = "PRIMAVIDA";
						arrColumnas[4] =  arrDatos[0].toString().equals("9") ? "PRIMADESEMPLEO" : "PRIMAGAP";
						arrColumnas[5] = "NUMEROPOLIZA";
					}
					break;
				
				case Constantes.TIPO_REPORTE_VENTAS_EMISION_VALOR:
					if(opcion.equals(0)){
						sbQuery.append("SELECT '', SUM(CAN), SUM(PRI), 'total.png' ");
							sbQuery.append("FROM ( ");
								sbQuery.append("SELECT COUNT(*) CAN, 0 PRI ");
								sbQuery.append(sbFiltroCar.toString());
									sbQuery.append("AND CC.COCR_CD_RAMO = 61 ");
									sbQuery.append("AND CC.COCR_CARGADA = 2 ");
								sbQuery.append("UNION ");
								sbQuery.append("SELECT 0 CAN, SUM(ROUND(CC.COCR_PRIMA, 2)) PRI ");
								sbQuery.append(sbFiltroCar.toString());
									sbQuery.append("AND CC.COCR_CARGADA = 2) ");
						sbQuery.append("UNION ");
						sbQuery.append("SELECT '" + arrDatos[1].toString() + "', COUNT(*), SUM(ROUND(CC.COCR_PRIMA, 2)), '" + arrDatos[2].toString() + "' ");   
						sbQuery.append(sbFiltroCar.toString());
							sbQuery.append("AND CC.COCR_CD_RAMO = " + arrDatos[0].toString());
							sbQuery.append("AND CC.COCR_CARGADA = 2 ");
						sbQuery.append("UNION "); 
						sbQuery.append("SELECT 'Vida', COUNT(*), SUM(ROUND(CC.COCR_PRIMA, 2)), 'vida.png' ");
						sbQuery.append(sbFiltroCar.toString());	
							sbQuery.append("AND CC.COCR_CD_RAMO = 61 ");
							sbQuery.append("AND CC.COCR_CARGADA = 2 ");
						sbQuery.append("ORDER BY 1 DESC ");
					} else {
						sbQuery.append("SELECT NUMCRE, SUM(PRIMA), SUMASEG, SUM(PRIMAVIDA), SUM(PRIMADESEMPLE), 0 ");
						sbQuery.append("FROM ( ");
							sbQuery.append("SELECT CC.COCR_NU_CREDITO NUMCRE, ROUND(CC.COCR_PRIMA, 2) PRIMA, ROUND(CC.COCR_SUMA_ASEGURADA, 2) SUMASEG, 0 PRIMAVIDA, ROUND(CC.COCR_PRIMA, 2) PRIMADESEMPLE ");    
							sbQuery.append(sbFiltroCar.toString());
								sbQuery.append("AND CC.COCR_CD_RAMO = " + arrDatos[0].toString());
								sbQuery.append("AND CC.COCR_CARGADA = 2 ");
							sbQuery.append("UNION ");
							sbQuery.append("SELECT CC.COCR_NU_CREDITO NUMCRE, ROUND(CC.COCR_PRIMA, 2) PRIMA, ROUND(CC.COCR_SUMA_ASEGURADA, 2) SUMASEG, ROUND(CC.COCR_PRIMA, 2) PRIMAVIDA, 0 PRIMADESEMPLE "); 
							sbQuery.append(sbFiltroCar.toString());
								sbQuery.append("AND CC.COCR_CD_RAMO = 61 ");
								sbQuery.append("AND CC.COCR_CARGADA = 2) ");
						sbQuery.append("GROUP BY NUMCRE, SUMASEG ");
					
						arrColumnas = new String[6];					
						arrColumnas[0] = "NUMEROCREDITO";
						arrColumnas[1] = "PRIMA";
						arrColumnas[2] = "SUMAASEGURADA";
						arrColumnas[3] = "PRIMAVIDA";
						arrColumnas[4] = arrDatos[0].toString().equals("9") ? "PRIMADESEMPLEO" : "PRIMAGAP";
						arrColumnas[5] = "NUMEROPOLIZA";
					}
					break;
					
				case Constantes.TIPO_REPORTE_EMITIDAS_VALOR:
					sbQuery.delete(0, sbQuery.length());
					if(opcion.equals(0)){
						sbQuery.append("SELECT '', SUM(CAN), SUM(PRI), SUM(PRIPUR), 'total.png' ");
							sbQuery.append("FROM (SELECT COUNT(*) CAN, 0 PRI, 0 PRIPUR ");						
										sbQuery.append(sbFiltroCer.toString());
										sbQuery.append("AND CC.COCE_CARP_CD_RAMO = 61 ");								
									sbQuery.append("UNION ");
									sbQuery.append("SELECT 0 CAN, SUM(ROUND(CC.COCE_MT_PRIMA_REAL, 2)) PRI, SUM(ROUND(CC.COCE_MT_PRIMA_PURA, 2)) PRIPUR ");
										sbQuery.append(sbFiltroCer.toString() + ")");										
						sbQuery.append("UNION ");
						sbQuery.append("SELECT '" + arrDatos[1].toString() + "', COUNT(*), SUM(ROUND(CC.COCE_MT_PRIMA_REAL, 2)), SUM(ROUND(CC.COCE_MT_PRIMA_PURA, 2)), '" + arrDatos[2].toString() + "' ");   
							sbQuery.append(sbFiltroCer.toString());
							sbQuery.append("AND CC.COCE_CARP_CD_RAMO = " + arrDatos[0].toString()).append(" ");							
						sbQuery.append("UNION "); 
						sbQuery.append("SELECT 'Vida', COUNT(*), SUM(ROUND(CC.COCE_MT_PRIMA_REAL, 2)), SUM(ROUND(CC.COCE_MT_PRIMA_PURA, 2)), 'vida.png' ");
							sbQuery.append(sbFiltroCer.toString());	
							sbQuery.append("AND CC.COCE_CARP_CD_RAMO = 61 ");						
						sbQuery.append("ORDER BY 1 DESC ");
					} else {
						sbQuery.append("SELECT NUMCRE, SUM(PRIMA), SUMASEG, SUM(PRIPUR), SUMASEGSI , SUM(PRIMAVIDA), SUM(PRIMADESEMPLE), NUMPOLIZA ");
							sbQuery.append("FROM ( ");
								sbQuery.append("SELECT CC.COCE_NU_CREDITO NUMCRE, ROUND(CC.COCE_MT_PRIMA_REAL, 2) PRIMA, ROUND(CC.COCE_MT_SUMA_ASEGURADA, 2) SUMASEG, ROUND(CC.COCE_MT_PRIMA_PURA, 2) PRIPUR, ROUND(CC.COCE_MT_SUMA_ASEG_SI, 2) SUMASEGSI, 0 PRIMAVIDA, ROUND(CC.COCE_MT_PRIMA_REAL, 2) PRIMADESEMPLE, CC.COCE_CAPO_NU_POLIZA NUMPOLIZA ");    
									sbQuery.append(sbFiltroCer.toString());
									sbQuery.append("AND CC.COCE_CARP_CD_RAMO = " + arrDatos[0].toString());									
								sbQuery.append("UNION ");
								sbQuery.append("SELECT CC.COCE_NU_CREDITO NUMCRE, ROUND(CC.COCE_MT_PRIMA_REAL, 2) PRIMA, ROUND(CC.COCE_MT_SUMA_ASEGURADA, 2) SUMASEG, ROUND(CC.COCE_MT_PRIMA_PURA, 2) PRIPUR, ROUND(CC.COCE_MT_SUMA_ASEG_SI, 2) SUMASEGSI, ROUND(CC.COCE_MT_PRIMA_REAL, 2) PRIMAVIDA, 0 PRIMADESEMPLE, CC.COCE_CAPO_NU_POLIZA NUMPOLIZA "); 
									sbQuery.append(sbFiltroCer.toString());
									sbQuery.append("AND CC.COCE_CARP_CD_RAMO = 61 )");								
						sbQuery.append("GROUP BY NUMCRE, NUMPOLIZA, SUMASEG, SUMASEGSI ");
					
						arrColumnas = new String[8];					
						arrColumnas[0] = "NUMEROCREDITO";
						arrColumnas[1] = "PRIMA";
						arrColumnas[2] = "SUMAASEGURADA";
						arrColumnas[3] = "PRIMAPURA";
						arrColumnas[4] = "SUMAASEGSI";
						arrColumnas[5] = "PRIMAVIDA";
						arrColumnas[6] = arrDatos[0].toString().equals("9") ? "PRIMADESEMPLEO" : "PRIMAGAP";
						arrColumnas[7] = "NUMEROPOLIZA";
					}
					break;
				
				case Constantes.TIPO_REPORTE_DEPOSITOS_NOCONCILIADO_VALOR:
					if(origen.equals(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR) || origen.equals(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR) || 
							origen.equals(Constantes.CONCILIACION_ORIGEN_GAP_VALOR) || origen.equals(Constantes.CONCILIACION_ORIGEN_LCI_VALOR)){
						sbQuery.append("SELECT MP.NU_CUENTA, MP.FOLIO, MP.AGENCIA, DECODE(SUBSTR(MP.VENTA_ORIGEN, 8), '101', 'ALTA' , '102', 'BAJA', '104', 'NO IDENTIFICADO', 'OTROS') ");
							sbQuery.append("FROM TLMK_FOLIOS_MEDIOS_PAGO MP ");
						sbQuery.append("WHERE SUBSTR(MP.VENTA_ORIGEN, 8) <> '103' ");
							sbQuery.append("AND MP.FECHA_APROBACION BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
							sbQuery.append("AND MP.CARGADA = 0 ");
						if(origen.equals(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR) || origen.equals(Constantes.CONCILIACION_ORIGEN_LCI_VALOR)) {
							sbQuery.append("AND LPAD(MP.FOLIO, 2) = '13' ");
						}else {
							sbQuery.append("AND LPAD(MP.FOLIO, 2) IN ('20', '21') ");
						}
						sbQuery.append("ORDER BY 2 ASC");
					} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR)) {
						sbQuery.append("SELECT MP.NU_CUENTA, MP.FOLIO, MP.AGENCIA, DECODE(SUBSTR(MP.VENTA_ORIGEN, 8), '103', 'ALTA', 'OTROS') ");
							sbQuery.append("FROM TLMK_FOLIOS_MEDIOS_PAGO MP ");
						sbQuery.append("WHERE SUBSTR(MP.VENTA_ORIGEN, 8) = '103' ");
							sbQuery.append("AND MP.FECHA_APROBACION BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
							sbQuery.append("AND MP.CARGADA = 0 ");
					} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_LE_VALOR)) {
						sbQuery.append("SELECT MP.NU_CUENTA, MP.NU_DE_CONTRATO, MP.AGENCIA, DECODE(SUBSTR(MP.VENTA_ORIGEN, 8), '101', 'ALTA' , '102', 'BAJA', '104', 'NO IDENTIFICADO', 'OTROS') ");
							sbQuery.append("FROM TLMK_FOLIOS_MEDIOS_PAGO MP ");
						sbQuery.append("WHERE SUBSTR(MP.VENTA_ORIGEN, 8) <> '103' ");
							sbQuery.append("AND MP.FECHA_APROBACION BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
							sbQuery.append("AND MP.CARGADA = 0 ");
							sbQuery.append("AND LENGTH(MP.NU_DE_CONTRATO) > 13 ");
					} else if(origen.equals(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR)) {
						sbQuery.append("SELECT MP.NU_CUENTA, MP.FOLIO, MP.AGENCIA, DECODE(SUBSTR(MP.VENTA_ORIGEN, 8), '105', 'ALTA', 'OTROS') ");
						sbQuery.append("FROM TLMK_FOLIOS_MEDIOS_PAGO MP ");
						sbQuery.append("WHERE SUBSTR(MP.VENTA_ORIGEN, 8) = '105' ");
						sbQuery.append("AND MP.FECHA_APROBACION BETWEEN TO_DATE('").append(strFechaIni).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFechaFin).append("', 'dd/MM/yyyy') ");
						sbQuery.append("AND MP.CARGADA = 0 ");
					}
					
					arrColumnas = new String[4];
					arrColumnas[0] = "CUENTA";
					arrColumnas[1] = "NUMEROCREDITO";
					arrColumnas[2] = "PRIMA";
					arrColumnas[3] = "DESCRIPCION";
					break;
					
				case Constantes.TIPO_REPORTE_ERRORES_CONCI_VALOR:
					sbQuery.append("SELECT PRE.VTAP_ID_CERTIFICADO, ROUND(PRE.VTAP_PRIMA, 2), ROUND(PRE.VTAP_SUMA_ASEGURADA, 2), PRE.VTAP_DES_ERROR, PRE.VTAP_CD_ERROR, ROUND(PRE.VTAP_NDATO8, 2) ");						
					sbQuery.append(sbFiltroVtaPre.toString());
						sbQuery.append("AND PRE.VTAP_CARGADA = 3 ");
						sbQuery.append("AND NVL(PRE.VTAP_CD_ERROR, 0) NOT IN ('002') ");
						
					arrColumnas = new String[6];					
					arrColumnas[0] = "NUMEROCREDITO";
					arrColumnas[1] = "PRIMA";
					arrColumnas[2] = "SUMAASEGURADA";
					arrColumnas[3] = "CODERROR";
					arrColumnas[4] = "DESCERROR";
					arrColumnas[5] = "TARIFA";
					break;
				
				case Constantes.TIPO_REPORTE_ERRORES_EMISI_VALOR:
					sbQuery.append("SELECT CP.COPC_NU_CREDITO, ROUND(CP.COPC_PRIMA, 2), ROUND(CP.COPC_DATO7, 2), CP.COPC_REGISTRO, DECODE(CP.COPC_CD_RAMO, 9, 'DESEMPLEO', 'VIDA'), ROUND((SYSDATE - TO_DATE(CP.COPC_FE_NAC, 'dd/MM/yyyy')) / 365, 2) ");  
					sbQuery.append(sbFiltroPre.toString());	
						sbQuery.append("AND CP.COPC_CARGADA = 3 ");
					sbQuery.append("UNION "); 
					sbQuery.append("SELECT CC.COCR_NU_CREDITO, ROUND(CC.COCR_PRIMA, 2), ROUND(CC.COCR_DATO7, 2), CC.COCR_REGISTRO, DECODE(CC.COCR_CD_RAMO, 9, 'DESEMPLEO', 'VIDA'), ROUND((SYSDATE - TO_DATE(CC.COCR_FE_NAC, 'dd/MM/yyyy')) / 365, 2) ");  
					sbQuery.append(sbFiltroCar.toString());
						sbQuery.append("AND CC.COCR_CARGADA = 3 "); 
						
					arrColumnas = new String[6];					
					arrColumnas[0] = "NUMEROCREDITO";
					arrColumnas[1] = "PRIMA";
					arrColumnas[2] = "SUMAASEGURADA";
					arrColumnas[3] = "DESCERROR";
					arrColumnas[4] = "RAMO";
					arrColumnas[5] = "EDAD";
					break;
					
				default:
					break;
			}
			
			arlResultados.add(conciliacionDao.consultar(sbQuery.toString()));
			arlResultados.add(arrColumnas);
			return arlResultados;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioConciliacion.consultar::: " + e.getMessage());
		}
	}
	
	public Parametros getParemetrosConsulta(Integer nOrigen) throws Exception {
		Parametros objParam = null;
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
		String strOrigen = null;
		
		try {
			arlResultados = new ArrayList<Object>();
			switch (nOrigen) {
				case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR:
					strOrigen = Constantes.CONCILIACION_ALTAMIRA;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR:
					strOrigen = Constantes.CONCILIACION_ALTAIR;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
					strOrigen = Constantes.CONCILIACION_HIPOTECARIO;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
					strOrigen = Constantes.CONCILIACION_LCI;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_LE_VALOR:
					strOrigen = Constantes.CONCILIACION_LE;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:
					strOrigen = Constantes.CONCILIACION_GAP;
					break;
			}
			
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT cp ");
			sbQuery.append("FROM Parametros cp ");
			sbQuery.append("WHERE cp.copaDesParametro = 'CONCILIACION' ");
			sbQuery.append("AND cp.copaIdParametro = '").append(strOrigen).append("'");
			
			arlResultados.addAll(cargaDao.consultarMapeo(sbQuery.toString())); 
			if(!arlResultados.isEmpty()){
				objParam = (Parametros) arlResultados.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioConciliacion.getParemetrosConsulta(): " + e.getMessage());
		}
		
		return objParam;
	}

	@SuppressWarnings("rawtypes")
	public ArrayList<ArrayList> getLayOut(String strSelect, String strCreditos, Integer nOrigen, Integer nTipoVenta) throws Exception {
		ArrayList<ArrayList> arlRegreso;
		ArrayList<Object> arlResultados, arlRegistros, arlRamos = null, arlTarifas;
		Iterator<Object> it;
		StringBuffer sbQuery;
		Object[] arrRegistro;
		Resultado objResultado = null;
		String strProducto, strSubproducto = "", strError, strValoresTmp;
		String[] arrDatos;
		Double nTarifa;
		
		try {
			arlRegreso = new ArrayList<ArrayList>();
			arlResultados = new ArrayList<Object>();
			arlRegistros = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			
			sbQuery.append("SELECT ").append(strSelect);
			sbQuery.append(" FROM VTA_PRECARGA PRE ");
			if(nOrigen == Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR) {
				sbQuery.append("INNER JOIN TLMK_FOLIOS_MEDIOS_PAGO MP ON MP.FOLIO = PRE.VTAP_ID_CERTIFICADO ");	
			}
			sbQuery.append("WHERE PRE.VTAP_ID_CERTIFICADO IN (").append(strCreditos).append(") ");
			sbQuery.append("	AND PRE.VTAP_CARGADA <> ").append(Constantes.CARGADA_CONCILIADO_MANUAL);
			
			if(nOrigen == Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR || nOrigen == Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR){
				arlRamos = cargaDao.consultarRamos(nTipoVenta);
			}
			
			it = conciliacionDao.consultar(sbQuery.toString()).iterator();
			while(it.hasNext()){
				arrRegistro = (Object[])it.next();
				switch (nOrigen) {
					case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR: //VIDA PRIMA TRADICIONAL
						objResultado = new Resultado(arrRegistro[5].toString(), GestorFechas.generateDate(arrRegistro[6].toString(), "dd/MM/yyyy") , arrRegistro[2].toString().trim() + "/" + arrRegistro[3].toString().trim() + "/" + arrRegistro[4].toString().trim());
						
						strProducto = arrRegistro[14].toString();
						nTarifa = new BigDecimal(arrRegistro[12] == null ? "0" : arrRegistro[12].toString()).doubleValue();
						arlTarifas = Utilerias.getTarifas(arlRamos, strProducto, strSubproducto, nTarifa, nTipoVenta);
						
						if( (Boolean) arlTarifas.get(Constantes.TARIFAS_FLAG_ENCONTRO_TARIFA)) {
							arrRegistro[12] = ( (Object[]) arlTarifas.get(Constantes.TARIFAS_ARRAY_VIDA))[2];
							arrRegistro[15] = ( (Object[]) arlTarifas.get(Constantes.TARIFAS_ARRAY_VIDA))[3];
							arrRegistro[13] = (new BigDecimal(arrRegistro[11].toString()).doubleValue() * new BigDecimal(arrRegistro[12].toString()).doubleValue()) / 1000;
						} else {
							strError = "No encontro se encontro tarifa:: " + nTarifa;
							arrRegistro[15] = strError;
							arrRegistro[13] = strError;
							objResultado.setNombreCliente(strError);
						}
						break;
						
					case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR: //DESEMPELO PRIMA TRADICIONAL
						objResultado = new Resultado(arrRegistro[5].toString(), GestorFechas.generateDate(arrRegistro[6].toString(), "dd/MM/yyyy") , arrRegistro[2].toString().trim() + "/" + arrRegistro[3].toString().trim() + "/" + arrRegistro[4].toString().trim());
						
						strProducto = arrRegistro[13].toString();
						nTarifa = new BigDecimal(arrRegistro[21] == null ? "0" : arrRegistro[21].toString()).doubleValue();
						arlTarifas = Utilerias.getTarifas(arlRamos, strProducto, strSubproducto, nTarifa, nTipoVenta);
						
						if( (Boolean) arlTarifas.get(Constantes.TARIFAS_FLAG_ENCONTRO_TARIFA)) {
							arrRegistro[21] = ( (Object[]) arlTarifas.get(Constantes.TARIFAS_ARRAY_DESEMPELO))[2];
							arrRegistro[14] = ( (Object[]) arlTarifas.get(Constantes.TARIFAS_ARRAY_DESEMPELO))[3];
							arrRegistro[12] = (new BigDecimal(arrRegistro[11].toString()).doubleValue() * new BigDecimal(arrRegistro[21].toString()).doubleValue()) / 1000;
							arrRegistro[22] = arrRegistro[10] == null ? "" : GestorFechas.addMesesaFecha(new Date(), new BigDecimal(arrRegistro[10].toString()).intValue());
						} else {
							strError = "No encontro se encontro tarifa:: " + nTarifa;
							arrRegistro[14] = strError;
							arrRegistro[12] = strError;;
							objResultado.setNombreCliente(strError);
						}
						break;
						
					case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
						objResultado = new Resultado(arrRegistro[0].toString(), GestorFechas.generateDate(arrRegistro[18].toString(), "dd/MM/yyyy") , arrRegistro[3].toString().trim() + "/" + arrRegistro[4].toString().trim() + "/" + arrRegistro[5].toString().trim());
						break;
						
					case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
						break;
						
					case Constantes.CONCILIACION_ORIGEN_LE_VALOR:
						objResultado = new Resultado(arrRegistro[0].toString(), GestorFechas.generateDate(arrRegistro[16].toString(), "dd/MM/yyyy") , arrRegistro[4].toString().trim() + "/" + arrRegistro[5].toString().trim() + "/" + arrRegistro[6].toString().trim());
						
						break;
					
					case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:	//LAYOUT GAP COLECTIVO
						objResultado = new Resultado(arrRegistro[0].toString(), GestorFechas.generateDate(arrRegistro[17].toString(), "dd/MM/yyyy") , arrRegistro[4].toString().trim() + "/" + arrRegistro[5].toString().trim() + "/" + arrRegistro[6].toString().trim());
						strValoresTmp = arrRegistro[39] == null ? "" : arrRegistro[39].toString();
						
						if(strValoresTmp == null || strValoresTmp.equals("")){
							arrRegistro[18] = "0";
							arrRegistro[19] = "0";
							arrRegistro[20] = "0";
							arrRegistro[21] = "0";
						} else {
							arrDatos = strValoresTmp.split("\\/")[1].split("\\|");
							arrRegistro[18] = arrDatos[2].toString();
							arrRegistro[19] = arrDatos[1].toString();
							arrRegistro[20] = arrDatos[1].toString();
							arrRegistro[21] = arrDatos[0].toString();
						}
						arrRegistro[39] = "";
						break;
					
					case Constantes.CONCILIACION_ORIGEN_GAP_VALOR_VID:	//LAYOUT VIDA GAP COLECTIVO
						objResultado = new Resultado(arrRegistro[5].toString(), GestorFechas.generateDate(arrRegistro[6].toString(), "dd/MM/yyyy") , arrRegistro[2].toString().trim() + "/" + arrRegistro[3].toString().trim() + "/" + arrRegistro[4].toString().trim());
						strValoresTmp = arrRegistro[40] == null ? "" : arrRegistro[40].toString();
						
						if(strValoresTmp == null || strValoresTmp.equals("")){
							arrRegistro[10] = "0";
							arrRegistro[11] = "0";
							arrRegistro[12] = "0";
							arrRegistro[13] = "0";
						} else {
							arrDatos = strValoresTmp.split("\\/")[0].split("\\|");
							arrRegistro[10] = arrDatos[1].toString();
							arrRegistro[11] = arrDatos[1].toString();
							arrRegistro[12] = arrDatos[0].toString();
							arrRegistro[13] = arrDatos[2].toString();
						}
						arrRegistro[40] = "";
						break;
				}
				
				arlResultados.add(objResultado);
				arlRegistros.add(arrRegistro);
			}
			
			arlRegreso.add(0, arlResultados);
			arlRegreso.add(1, arlRegistros);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioConciliacion.getParametrosConciliacion(): " + e.getMessage());
		}
		
		return arlRegreso;
	}
	
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		
		this.message.append("obtener vtaPrecargas.");
		Utilerias.resetMessage(message);
		
		try {
			List<T> lista = this.conciliacionDao.obtenerObjetos(filtro);
			
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede recuperar la vtaPrecarga.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}


	public ConciliacionDao getConciliacionDao() {
		return conciliacionDao;
	}
	public void setConciliacionDao(ConciliacionDao conciliacionDao) {
		this.conciliacionDao = conciliacionDao;
	}
	public CargaDao getCargaDao() {
		return cargaDao;
	}
	public void setCargaDao(CargaDao cargaDao) {
		this.cargaDao = cargaDao;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public List<Object> consultar(String query) throws Exception {
		// TODO Auto-generated method stub
		
		List<Object> lista = null;
		
		try {
			lista = conciliacionDao.consultar(query);
			
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error al realizar la consulta.", e);
		}
		
		return null;
	}

}
