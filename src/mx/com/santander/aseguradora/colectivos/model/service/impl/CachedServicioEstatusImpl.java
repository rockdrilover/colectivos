/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estatus;
import mx.com.santander.aseguradora.colectivos.model.dao.EstatusDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstatus;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author Sergio Plata
 *
 */
@Service
public class CachedServicioEstatusImpl implements ServicioEstatus {

	@Resource
	private EstatusDao estatusDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private Map<Integer, Estatus> cacheEstatus;
	
	/**
	 * @param estatusDao the estatusDao to set
	 */
	public void setEstatusDao(EstatusDao estatusDao) {
		this.estatusDao = estatusDao;
	}

	/**
	 * @return the estatusDao
	 */
	public EstatusDao getEstatusDao() {
		return estatusDao;
	}

	/**
	 * @param cacheEstatus the cacheEstatus to set
	 */
	public void setCacheEstatus(Map<Integer, Estatus> cacheEstatus) {
		this.cacheEstatus = cacheEstatus;
	}

	/**
	 * @return the cacheEstatus
	 */
	public Map<Integer, Estatus> getCacheEstatus() {
		return cacheEstatus;
	}

	public CachedServicioEstatusImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.cacheEstatus = Collections.synchronizedMap(new LinkedHashMap<Integer, Estatus>());
	}
	
	
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	@SuppressWarnings("unchecked")
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerEstatus");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			Estatus estatus = this.cacheEstatus.get(id);
			return (T) estatus;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al recuperar estatus:").append(id);
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerEstatus.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			List<T> lista = Utilerias.getValueList(this.cacheEstatus);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al recuperar la lista de estatus.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return objeto;
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerEstatus.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			List<T> lista = this.estatusDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los estatus.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
		List<Estatus> lista = this.estatusDao.obtenerObjetos(Estatus.class);
		
		for(Estatus o: lista){
			this.cacheEstatus.put(o.getAlesCdEstatus(), o);
		}
		
	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
