package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.MonitoreoDao;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioMonitoreo;

@Service
public class ServicioMonitoreoImpl implements ServicioMonitoreo {
	
	@Resource
	private MonitoreoDao monitoreoDao;
	
	public MonitoreoDao getMonitoreoDao() {
		return monitoreoDao;
	}

	public void setMonitoreoDao(MonitoreoDao monitoreoDao) {
		this.monitoreoDao = monitoreoDao;
	}
	
	@Override
	public <T> List<T> cargaPolizasEmiCan(short inRamo, Integer inPoliza, Integer inIdVenta, String fInicio, String fFin, int opcion,int tarea) throws Exception {
		try {
			return monitoreoDao.cargaPolizasEmiCan(inRamo,inPoliza,inIdVenta,fInicio,fFin,opcion,tarea);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public <T> List<T> cargaPolizasConcilia(short inRamo, Integer inPoliza, String fInicio,String fFin, int opcion, int tarea) throws Exception {
		try {
			return monitoreoDao.cargaPolizasConcilia(inRamo,inPoliza,fInicio,fFin,opcion,tarea);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public <T> List<T> cargaPolizasTimbrados(short inRamo, Integer inPoliza, Integer inIdVenta, String fInicio, String fFin, int opcion, int tarea) throws Exception {
		try {
			return monitoreoDao.cargaPolizasTimbrados(inRamo,inPoliza,inIdVenta,fInicio,fFin,opcion,tarea);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public <T> List<T> cargaPolizasEndosos(short inRamo, Integer inPoliza, Integer inIdVenta, String fInicio, String fFin, int opcion, int tarea) throws Exception {
		try {
			return monitoreoDao.cargaPolizasEndosos(inRamo,inPoliza,inIdVenta,fInicio,fFin,opcion,tarea);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
