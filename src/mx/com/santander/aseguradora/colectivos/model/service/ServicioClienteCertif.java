/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Complementarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

/**
 * @author Sergio Plata
 *
 */
public interface ServicioClienteCertif extends ServicioCatalogo {
	
	public <T> List<T> consultaCredCte (String idCertificado, String nombre, String apellidoPaterno, String apellidoMaterno) throws Excepciones;
	public <T> List<T> consultaCredCert (String certs) throws Excepciones;
	public <T> List<T> consultaCredCobs (String credito, Short canal, Short ramo, Long poliza, Long nucerd) throws Excepciones;
	public <T> List<T> consultaCredAsis (String credito, Short canal, Short ramo, Long poliza, Long nucerd) throws Excepciones;
	public <T> List<T> consultaCredAsegurados (String credito, Short canal, Short ramo, Long poliza, Long nucerd) throws Excepciones;
	public Complementarios consultaCredComplementos(String credito, Short canal, Short ramo,Long poliza, Long nucerd) throws Excepciones;
    public String impresionCertificado(String rutaTemporal, short canal, short ramo, long poliza, long certificado, Date fecha_emision);
    public String impresionPoliza(String rutaTemporal,short canal, short ramo, long poliza, Long certificado, Integer producto, Date fecha_emision);
	public String generaCertificado(String rutaPlatillas, Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan, Date fechaEmision, String strDestino, String strNomrePlantilla, Long polizaReal) throws Exception;
	
	/**
	 * Metodo que regresa objeto con datos de plantilla a generar 
	 * @return objeto con datos de plantilla a generar
	 * @throws Exception
	 */
	Parametros getRutaPlantilla() throws Exception;
	
	/**
	 * Metodo que consulta el nombre de plantilla a generar
	 * @param coceCasuCdSucursal
	 * @param coceCarpCdRamo
	 * @param coceCapoNuPoliza
	 * @param coceNuCertificado
	 * @param alprCdProducto
	 * @param alplCdPlan
	 * @param fechaEmision
	 * @return
	 * @throws Exception
	 */
	String getPlantilla(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza,Long coceNuCertificado, Integer alprCdProducto, Integer alplCdPlan, Date fechaEmision) throws Exception;
	
	/**
	 * Metodo que consulta el plazo que tiene un certificado
	 * @param coceCarpCdRamo ramo del certificado
	 * @param alprCdProducto producto del certificado
	 * @param alplCdPlan plan del certificado
	 * @return plazo que tiene el certificado
	 * @throws Exception
	 */
	String getPlazo(Short coceCarpCdRamo, Integer alprCdProducto, Integer alplCdPlan) throws Exception;
	
	/**
	 * Metodo que Actualiza el titular a obligado
	 * @param id Objeto con canal, ramo, poliza, certificado
	 * @throws Exception
	 */
	void actualizaTitular(CertificadoId id) throws Exception;
	
	/**
	 * Metodo que consulta los endosos de datos de un certificado
	 * @param canal
	 * @param ramo
	 * @param poliza
	 * @param nucerd
	 * @return lista con los endosos de un certificado
	 * @throws Excepciones
	 */
	List<EndosoDatosDTO> getEndosos(Short canal, Short ramo, Long poliza, Long nucerd) throws Exception;
	
	/**
	 * Metodo que notifica al usuario que hubo un cambio de Obligado a Titular
	 * @param id Objeto con canal, ramo, poliza, certificado
	 * @param tipoCliente tipo cliente
	 * @param tipoCambio cambio se va realizar
	 * @throws Exception
	 */
	void notificarCambio(ClienteCertifId id, Short tipoCliente, Integer tipoCambio) throws Exception;
}
