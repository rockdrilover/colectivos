/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanDetalleComponentes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaCoberturaDetalle;
/**
 * @author dflores
 *
 */
public interface ServicioRecibo extends ServicioCatalogo {

	public void impresionRecibo(String rutaTemporal, String nombre,
			Short careCasuCdSucursal, Short careCarpCdRamo, Integer careCapoNuPoliza,
			BigDecimal careNuRecibo, Short primaUnica);
	
	public void impresionPreRecibo(String rutaTemporal, String nombre,
			short cofaCasuCdSucursal, short cofaCarpCdRamo, long cofaCapoNuPoliza,
			String cofaCampov1);
	
	public List<Object[]> consulta2(short canal, Short ramo, Long poliza, Integer idVenta, Date feDesde, Date feHasta, Double noRecibo) throws Exception;
	public <T> List<T> obtenerObjetos1(String filtro) throws Excepciones;
	public List<BeanDetalleComponentes> consultaDetalleComponentes(short ramo, double no_recibo, int sucursal) throws Exception;
	public List<BeanListaCoberturaDetalle> consultaDetalleCobertura(short ramo, double no_recibo, int sucursal) throws Exception;


}
