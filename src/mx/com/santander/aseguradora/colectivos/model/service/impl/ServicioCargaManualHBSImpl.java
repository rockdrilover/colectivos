package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecargaId;
import mx.com.santander.aseguradora.colectivos.model.dao.CargaDao;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCargaManualHBS;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.RFC;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
@Service
public class ServicioCargaManualHBSImpl implements ServicioCargaManualHBS {

	@Resource
	private CargaDao cargaDao;
	private StringBuilder message = null;
	private Log log = LogFactory.getLog(this.getClass());

	public ServicioCargaManualHBSImpl() {
		setMessage(new StringBuilder());
	}
	
	public ArrayList<Object> getParemetros(String strDesParametros, String strIdParametro) throws Exception {
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
		
		try {
			arlResultados = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT cp ");
			sbQuery.append("FROM Parametros cp ");
			sbQuery.append("WHERE cp.copaDesParametro = '").append(strDesParametros).append("' ");
			sbQuery.append("AND cp.copaIdParametro = '").append(strIdParametro).append("' ");
			sbQuery.append("AND cp.copaNvalor2 = 1");
			
			arlResultados.addAll(cargaDao.consultarMapeo(sbQuery.toString())); 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCargaManualHBSImpl.getParemetros(): " + e.getMessage());
		}
		
		return arlResultados;
	}
	
	public HashMap<String, Object> getRamosPolizas(ArrayList<Object> arlParametros) throws Exception {
		HashMap<String, Object> hmResultado;
		Iterator<Object> it;
		Parametros objParam;
		ArrayList<Parametros> arlParam;
		String strPolSof = Constantes.DEFAULT_STRING, strPolBan = Constantes.DEFAULT_STRING, strRamos = Constantes.DEFAULT_STRING;
		String strKey;
		
		try {
			hmResultado = new HashMap<String, Object>();
			it = arlParametros.iterator();
			while(it.hasNext()) {				
				objParam = (Parametros) it.next();
				
				if(objParam.getCopaVvalor4().equals(Constantes.HBS_BANCO)){
					strPolBan = strPolBan.equals(Constantes.DEFAULT_STRING) ? objParam.getCopaNvalor3().toString() : strPolBan + ", " + objParam.getCopaNvalor3().toString();
				} else if(objParam.getCopaVvalor4().equals(Constantes.HBS_SOFOM)){
					strPolSof = strPolSof.equals(Constantes.DEFAULT_STRING) ? objParam.getCopaNvalor3().toString() : strPolSof + ", " + objParam.getCopaNvalor3().toString();
				}
				
				strKey = Constantes.DES_PARAMETRO_BANCOSOFOM_POLIZAS + objParam.getCopaNvalor1().toString();;
				if(hmResultado.containsKey(strKey)){
					arlParam = (ArrayList<Parametros>) hmResultado.get(strKey);
					arlParam.add(objParam);
					hmResultado.remove(strKey);
					hmResultado.put(strKey, arlParam);
				} else {
					arlParam = new ArrayList<Parametros>();
					arlParam.add(objParam);
					hmResultado.put(strKey, arlParam);
					strRamos = strRamos.equals(Constantes.DEFAULT_STRING) ? objParam.getCopaNvalor1().toString() : strRamos + ", " + objParam.getCopaNvalor1().toString();
				}
			}
			
			hmResultado.put(Constantes.HBS_RAMOS, strRamos);
			hmResultado.put(Constantes.DES_PARAMETRO_BANCOSOFOM_POLIZAS + Constantes.HBS_BANCO, strPolBan);
			hmResultado.put(Constantes.DES_PARAMETRO_BANCOSOFOM_POLIZAS + Constantes.HBS_SOFOM, strPolSof);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.getRamosPolizas():: " + e.getMessage());
		}
		
		return hmResultado;
	}
	
	
	@SuppressWarnings("unchecked")
	public void datosMesPasado(Short inRamo, String strRemesa, String strPolizas) throws Exception {
		StringBuffer sbQuery;
		ArrayList<Object> arlResultados;
		Iterator<Object> itQuery;
		
		try {
			arlResultados = new ArrayList<Object>();
			sbQuery = crearSelect(inRamo, strRemesa, strPolizas);
			arlResultados.addAll(cargaDao.consultar(sbQuery.toString()));
			itQuery = arlResultados.iterator();
			while(itQuery.hasNext()) {
				Object[] arrDatos = (Object[]) itQuery.next();
				if(!arrDatos[7].toString().equals(Constantes.DEFAULT_STRING_ID)) {
					crearObjetoParaEmitir(arrDatos, inRamo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.datosMesPasado::: " + e.getMessage());
		}
		
	}

	public void datosNuevosValidar(Short inRamo, Object arlPolizas, String strPolizas) throws Exception {
		StringBuffer sbQuery;
		ArrayList<Object> arlResultados;
		Iterator<Object> itQuery;
		HashMap<Long, String> hmTarifasxPoliza;
		
		try {
			arlResultados = new ArrayList<Object>();
			hmTarifasxPoliza = getTarifasxPoliza(inRamo, strPolizas);
			sbQuery = crearSelectNuevosValidar(inRamo);
			arlResultados.addAll(cargaDao.consultar(sbQuery.toString()));
			itQuery = arlResultados.iterator();
			while(itQuery.hasNext()) {
				Object[] arrDatos = (Object[]) itQuery.next();
				crearObjetoParaEmitirNuevosValidar(arrDatos, inRamo, arlPolizas, hmTarifasxPoliza);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.datosNuevosValidar::: " + e.getMessage());
		}
		
	}
	
	private StringBuffer crearSelect(Short inRamo, String strRemesa, String strPolizas) throws Exception {
		StringBuffer sbQuery;
		String strFiltroRamo = "";

		try {
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT \n");  
					sbQuery.append("DISTINCT CCE.COCE_CARP_CD_RAMO RAMO, \n"); 
					sbQuery.append("CCE.COCE_CAPO_NU_POLIZA POL, \n");
					sbQuery.append("CCE.COCE_NU_CREDITO IDENTIFICADOR, \n");
					sbQuery.append("NVL(CC.COCN_CAMPOV2, VP.VTAP_CUENTA_ALTERNA) CUENTA, \n"); 
					sbQuery.append("CC.COCN_APELLIDO_PAT APELLIDOPATERNO, \n"); 
					sbQuery.append("CC.COCN_APELLIDO_MAT APELLIDOMATERNO, \n");
					sbQuery.append("CC.COCN_NOMBRE NOMBRE, \n");
					sbQuery.append("CC.COCN_BUC_CLIENTE NUMEROCLIENTE, \n"); 
					sbQuery.append("CC.COCN_RFC RFC, \n");
					sbQuery.append("CC.COCN_FE_NACIMIENTO FECHANACIMIENTO, \n"); 
					sbQuery.append("CC.COCN_CD_SEXO SEXO, \n");
					if(inRamo.toString().equals(Constantes.PRODUCTO_DA�OS)) {
						sbQuery.append("NVL(CCO.COCO_HOG_CALLE_NUM, 'X') CALLE, \n");
						sbQuery.append("NVL(CCO.COCO_HOG_COLONIA, 'X') COLONIA, \n");
						sbQuery.append("NVL(CCO.COCO_HOG_DELEGMUNIC, 'X') DELGMUN, \n");
						sbQuery.append("NVL(CCO.COCO_HOG_CD_ESTADO_REPORTA, '33') ESTADO, \n");
						sbQuery.append("NVL(CCO.COCO_HOG_CD_POSTAL, '50000') CODIGOPOSTAL, \n");
					} else {
						sbQuery.append("NVL(CC.COCN_CALLE_NUM, 'X') CALLE, \n"); 
						sbQuery.append("NVL(CC.COCN_COLONIA, 'X') COLONIA, \n"); 
						sbQuery.append("NVL(CC.COCN_DELEGMUNIC, 'X') DELGMUN, \n"); 
						sbQuery.append("NVL(CC.COCN_CD_ESTADO, '33') ESTADO, \n"); 
						sbQuery.append("NVL(CC.COCN_CD_POSTAL, '50000') CODIGOPOSTAL, \n");
					}
					sbQuery.append("CC.COCN_NU_LADA LADA, \n"); 
					sbQuery.append("CC.COCN_NU_TELEFONO TELEFONO, \n"); 
					sbQuery.append("CCC.COCC_TP_CLIENTE I, \n");   
					sbQuery.append("'01/' ||TO_CHAR(SYSDATE, 'MM/yyyy') FECHAINGRESO, \n"); 
					sbQuery.append("TO_CHAR(SYSDATE, 'YYYYMM') REMESA, \n"); 
					sbQuery.append("TO_CHAR(CCE.COCE_FE_INI_CREDITO, 'dd/MM/yyyy')  FECHAINICIOCREDITO, \n"); 
					sbQuery.append("TO_CHAR(CCE.COCE_FE_FIN_CREDITO, 'dd/MM/yyyy')  FECHAFINCREDITO, \n");
					sbQuery.append("CCE.COCE_TP_PRODUCTO_BCO  PRODUCTO, \n"); 
					sbQuery.append("CCE.COCE_TP_SUBPROD_BCO SUBPRODUCTO, \n"); 
					sbQuery.append("ROUND(MONTHS_BETWEEN(CCE.COCE_FE_FIN_CREDITO, TO_DATE( '01/' ||TO_CHAR(CCE.COCE_FE_INI_CREDITO, 'MM/yyyy') , 'dd/MM/yyyy'))) PLAZO, \n"); 
					sbQuery.append("VP.VTAP_CAZB_CD_SUCURSAL, \n"); 
					sbQuery.append("DECODE(CCO.COCO_HOG_METROS_OLAS, 0, 'N', 1, 'S') METROSDELMAR, \n"); 
					sbQuery.append("CCO.COCO_HOG_ENTREPISOS PISO, \n"); 
					sbQuery.append("CCO.COCO_HOG_NU_PISOS NUMEROPISOS, \n");   
	                 
					sbQuery.append("VPMA.VTAP_NUM_POLIZA, VPMA.VTAP_ESTATUS OPMA, VPMA.VTAP_SUMA_ASEGURADA SUMASEGMA, VPMA.VTAP_PRIMA PMAMA, VPMA.VTAP_VDATO1 TARMA, VPMA.VTAP_FE_NAC FENAMA, VPMA.VTAP_CP, \n"); 
	        
					if(inRamo.toString().equals(Constantes.PRODUCTO_DA�OS)) {
						sbQuery.append("VP.VTAP_VDATO6 PMA, VP.VTAP_SUMA_ASEGURADA CAP, VP.VTAP_VDATO9 TAR \n");
						strFiltroRamo = "AND VP.VTAP_NDATO2 = 4";
					} else if(inRamo.toString().equals(Constantes.PRODUCTO_VIDA)) {
						sbQuery.append("VP.VTAP_VDATO5 PMA, VP.VTAP_PRIMA SUMASEG, VP.VTAP_VDATO8 TAR \n"); 
						strFiltroRamo = "AND VP.VTAP_NDATO1 = 4";
					} else if(inRamo.toString().equals(Constantes.PRODUCTO_DESEMPLEO)) {
						sbQuery.append("VP.VTAP_VDATO7 PMA, VP.VTAP_PRIMA SUMASEG, VP.VTAP_VDATO10 TAR \n");
						strFiltroRamo = "AND VP.VTAP_NDATO3 = 4";
					}
				sbQuery.append("FROM VTA_PRECARGA VPMA \n");
				sbQuery.append("INNER JOIN VTA_PRECARGA VP ON(VP.VTAP_ID_CERTIFICADO = VPMA.VTAP_ID_CERTIFICADO ) \n"); 
				sbQuery.append("INNER JOIN COLECTIVOS_CERTIFICADOS CCE ON(CCE.COCE_NU_CREDITO = VP.VTAP_ID_CERTIFICADO) \n");
				sbQuery.append("INNER JOIN COLECTIVOS_FACTURACION CF ON(CF.COFA_NU_RECIBO_FISCAL = CCE.COCE_NO_RECIBO) \n");
				sbQuery.append("INNER JOIN COLECTIVOS_CLIENTE_CERTIF CCC ON(CCC.COCC_CASU_CD_SUCURSAL = CCE.COCE_CASU_CD_SUCURSAL AND CCC.COCC_CARP_CD_RAMO = CCE.COCE_CARP_CD_RAMO AND CCC.COCC_CAPO_NU_POLIZA = CCE.COCE_CAPO_NU_POLIZA AND CCC.COCC_NU_CERTIFICADO = CCE.COCE_NU_CERTIFICADO) \n"); 
				sbQuery.append("INNER JOIN COLECTIVOS_CLIENTES CC ON(CC.COCN_NU_CLIENTE = CCC.COCC_NU_CLIENTE) \n");
				sbQuery.append("LEFT JOIN COLECTIVOS_COMPLEMENTARIOS CCO ON(CCO.COCO_CACE_CASU_CD_SUCURSAL = CCE.COCE_CASU_CD_SUCURSAL AND CCO.COCO_CACE_CARP_CD_RAMO = CCE.COCE_CARP_CD_RAMO AND CCO.COCO_CACE_CAPO_NU_POLIZA = CCE.COCE_CAPO_NU_POLIZA AND CCO.COCO_CACE_NU_CERTIFICADO = CCE.COCE_NU_CERTIFICADO  AND CCO.COCO_NU_MOVIMIENTO = 0) \n"); 
			sbQuery.append("WHERE VPMA.VTAP_SISTEMA_ORIGEN = '").append(Constantes.HBS_CARGAS_MANUALES).append("' \n");    
				sbQuery.append("AND VPMA.VTAP_CD_RAMO = ").append(inRamo).append(" \n");
				sbQuery.append("AND VPMA.VTAP_CARGADA = 0 \n"); 
	    
				sbQuery.append("AND VP.VTAP_SISTEMA_ORIGEN IN('HBS_SOF', 'HBS_BAN_MX', 'HBS_BAN_UD') \n"); 
				sbQuery.append(strFiltroRamo).append(" \n");
	    
				sbQuery.append("AND CF.COFA_CASU_CD_SUCURSAL = 1 \n"); 
				sbQuery.append("AND CF.COFA_CARP_CD_RAMO = ").append(inRamo).append(" \n");     
				sbQuery.append("AND CF.COFA_CAPO_NU_POLIZA IN(").append(strPolizas).append(") \n");      
				sbQuery.append("AND CF.COFA_NU_CERTIFICADO = 0 \n"); 
				sbQuery.append("AND CF.COFA_NU_RECIBO_COL > 0 \n");
				sbQuery.append("AND TO_CHAR(CF.COFA_FE_FACTURACION, 'YYYYMM') = '").append(strRemesa).append("' \n");  
				sbQuery.append("AND CF.COFA_ST_RECIBO = 100 \n");
				sbQuery.append("AND CCC.COCC_TP_CLIENTE = 1 \n"); 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.crearSelect::: " + e.getMessage());
		}
		
		return sbQuery;
	}
	
	private StringBuffer crearSelectNuevosValidar(Short inRamo) throws Exception {
		StringBuffer sbQuery;
		String strFiltroRamo = "";

		try {
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT \n");  
					sbQuery.append("NVL(WF.VTAP_CD_SUCURSAL, 0), \n");
					sbQuery.append("VP.VTAP_SISTEMA_ORIGEN, \n");
					sbQuery.append("VP.VTAP_ID_CERTIFICADO, \n");         
					sbQuery.append("VP.VTAP_CUENTA CUENTA, \n");          
					sbQuery.append("VP.VTAP_NOMBRE ||'~'|| WF.VTAP_AP_PATERNO_ASEG ||'|'|| WF.VTAP_AP_MATERNO_ASEG ||'|'|| WF.VTAP_NOMBRE_ASEG, \n");     
					sbQuery.append("VP.VTAP_NUM_CLIENTE, \n"); 
					sbQuery.append("VP.VTAP_RFC, \n");        
					sbQuery.append("NVL(WF.VTAP_SEXO, 'MA'), \n");                               
					sbQuery.append("NVL(VP.VTAP_CALLE, 'S/D') ||'~'||  NVL(WF.VTAP_VDATO11, 'S/D'), \n");        
					sbQuery.append("NVL(VP.VTAP_COLONIA, 'S/D') ||'~'|| NVL(WF.VTAP_VDATO10, 'S/D'), \n");        
					sbQuery.append("NVL(VP.VTAP_DELMUNIC, 'S/D') ||'~'|| NVL(WF.VTAP_VDATO8, 'S/D'), \n");
					sbQuery.append("DECODE(VP.VTAP_NOMBRE_RS, 'S/D', '33', VP.VTAP_NOMBRE_RS ) ||'~'||  DECODE(WF.VTAP_CD_ESTADO, '0', '33', '#N/A', '33', NVL(WF.VTAP_CD_ESTADO, '33')), \n");
					sbQuery.append("DECODE(VP.VTAP_CP, '0', '50000', VP.VTAP_CP) ||'~'|| NVL(WF.VTAP_CP, 50000), \n");
					sbQuery.append("'X', \n");
					sbQuery.append("'X', \n");
					sbQuery.append("'1', \n");                                         
					sbQuery.append("'01/' ||TO_CHAR(SYSDATE, 'MM/YYYY'), \n");   
					sbQuery.append("TO_CHAR(SYSDATE, 'YYYYMM'), \n");
					sbQuery.append("VP.VTAP_FE_INGRESO, \n");
					sbQuery.append("VP.VTAP_FE_ESTATUS, \n");
					sbQuery.append("VP.VTAP_CD_RAMO, \n");
					sbQuery.append("VP.VTAP_CD_PRODUCTO, \n");                  
					sbQuery.append("ROUND(MONTHS_BETWEEN(TO_DATE(VP.VTAP_FE_ESTATUS, 'DD/MM/YYYY'), TO_DATE( VP.VTAP_FE_INGRESO , 'DD/MM/YYYY'))), \n");         
					sbQuery.append("VP.VTAP_CAZB_CD_SUCURSAL, \n");
					sbQuery.append("NVL(DECODE(WF.VTAP_CUENTA_ALTERNA, 'NO', 'N', 'SI', 'S'), 'N'), \n"); 
					sbQuery.append("NVL(WF.VTAP_CONDUCTO_ALTERNA, '0'), \n");
					sbQuery.append("NVL(WF.VTAP_BANCO_ALTERNA, '0'), \n");        
				    sbQuery.append("VPMA.VTAP_NUM_POLIZA, VPMA.VTAP_ESTATUS, VPMA.VTAP_SUMA_ASEGURADA, VPMA.VTAP_PRIMA, VPMA.VTAP_VDATO1, VPMA.VTAP_FE_NAC, VPMA.VTAP_CP, \n");
			        
				    if(inRamo.toString().equals(Constantes.PRODUCTO_DA�OS)) {
						sbQuery.append("VP.VTAP_VDATO6 PMA, VP.VTAP_SUMA_ASEGURADA CAP, VP.VTAP_VDATO9 TAR, \n");
						strFiltroRamo = "AND VP.VTAP_NDATO2 IN(1, 2, 5)";
					} else if(inRamo.toString().equals(Constantes.PRODUCTO_VIDA)) {
						sbQuery.append("VP.VTAP_VDATO5 PMA, VP.VTAP_PRIMA SUMASEG, VP.VTAP_VDATO8 TAR, \n"); 
						strFiltroRamo = "AND VP.VTAP_NDATO1 IN(1, 2, 5)";
					} else if(inRamo.toString().equals(Constantes.PRODUCTO_DESEMPLEO)) {
						sbQuery.append("VP.VTAP_VDATO7 PMA, VP.VTAP_PRIMA SUMASEG, VP.VTAP_VDATO10 TAR, \n");
						strFiltroRamo = "AND VP.VTAP_NDATO3 IN(1, 2, 5)";
					}
				    
				    sbQuery.append("NVL(VP.VTAP_FR_PAGO, '0') \n");
			    sbQuery.append("FROM VTA_PRECARGA VPMA \n");
				sbQuery.append("INNER JOIN VTA_PRECARGA VP ON(VP.VTAP_ID_CERTIFICADO = VPMA.VTAP_ID_CERTIFICADO) \n"); 
				sbQuery.append("LEFT JOIN VTA_PRECARGA WF ON(WF.VTAP_ID_CERTIFICADO = VP.VTAP_ID_CERTIFICADO AND WF.VTAP_SISTEMA_ORIGEN = 'HBS_FIRMAS') \n");
			sbQuery.append("WHERE VPMA.VTAP_SISTEMA_ORIGEN = '").append(Constantes.HBS_CARGAS_MANUALES).append("' \n");    
				sbQuery.append("AND VPMA.VTAP_CD_RAMO = ").append(inRamo).append(" \n");
				sbQuery.append("AND VPMA.VTAP_CARGADA = 0 \n");
				
				sbQuery.append("AND VP.VTAP_SISTEMA_ORIGEN IN('HBS_SOF', 'HBS_BAN_MX', 'HBS_BAN_UD') \n");
				sbQuery.append(strFiltroRamo).append(" \n");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.crearSelectNuevosValidar::: " + e.getMessage());
		}

		return sbQuery;
	}
	
	private void crearObjetoParaEmitir(Object[] arrDatos, Short inRamo)  throws Exception {
		PreCarga objPrecarga;
		PreCargaId objPrecargaPK;
		VtaPrecarga objManual;
		VtaPrecargaId objManualId;
		
		try {
			objPrecarga = new PreCarga();
			objPrecargaPK = new PreCargaId();				
			
			objPrecargaPK.setCopcCdSucursal(new Short("1"));
			objPrecargaPK.setCopcCdRamo(((BigDecimal) arrDatos[0]).shortValue());
			objPrecargaPK.setCopcNumPoliza(((BigDecimal) arrDatos[1]).longValue());
			objPrecargaPK.setCopcIdCertificado(arrDatos[2].toString()); 												//identificador		
			objPrecarga.setCopcCuenta(arrDatos[3] == null ? Constantes.DEFAULT_STRING_ID  : arrDatos[3].toString()); 	//cuenta
			objPrecarga.setCopcNuCredito(arrDatos[2].toString()); 														//prestamo
			objPrecarga.setCopcApPaterno(arrDatos[4] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[4].toString()); 	//apellidopaterno
			objPrecarga.setCopcApMaterno(arrDatos[5] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[5].toString()); 	//apellidomaterno 
			objPrecarga.setCopcNombre(arrDatos[6] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[6].toString()); 		//nombre	
			objPrecarga.setCopcNumCliente(arrDatos[7] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[7].toString()); //numerocliente
			objPrecarga.setCopcNumCliente((objPrecarga.getCopcNumCliente().equals(Constantes.DEFAULT_STRING) || objPrecarga.getCopcNumCliente().equals(Constantes.DEFAULT_STRING_ID)) ? "1" : objPrecarga.getCopcNumCliente());
			objPrecarga.setCopcRfc(arrDatos[8] == null ? "XXXX800101" : arrDatos[8].toString());						//rfc
			objPrecarga.setCopcRfc(Utilerias.validaRFC(objPrecarga.getCopcRfc()) == true ? objPrecarga.getCopcRfc() : "XXXX800101"); 
			objPrecarga.setCopcFeNac(arrDatos[9] == null ? "01/01/1980" : GestorFechas.formatDate((Date) arrDatos[9], "dd/MM/yyyy")); //fechanacimiento			
			objPrecarga.setCopcFeNac(GestorFechas.validarFecha(GestorFechas.generarFechaNacimiento(objPrecarga.getCopcRfc())) == true ? GestorFechas.generarFechaNacimiento(objPrecarga.getCopcRfc()) : objPrecarga.getCopcFeNac()); 
			objPrecarga.setCopcCalle(arrDatos[11] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[11].toString());		//calle			
			objPrecarga.setCopcColonia(arrDatos[12] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[12].toString());	//colonia
			objPrecarga.setCopcDelmunic(arrDatos[13] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[13].toString()); 	//delg/mun
			objPrecarga.setCopcCdEstado(arrDatos[14] == null ? "33" : arrDatos[14].toString()); 						//estado
			objPrecarga.setCopcCp(Utilerias.validaCP(arrDatos[15] == null ? Constantes.DEFAULT_STRING : arrDatos[15].toString())); 		//codigopostal
			objPrecarga.setCopcFeIngreso(arrDatos[19].toString()); 														//fechaingreso						
			objPrecarga.setCopcDato33(arrDatos[21].toString()); 														//fechainiciocredito
			objPrecarga.setCopcDato34(arrDatos[22].toString()); 														//fechafincredito
			objPrecarga.setCopcBcoProducto(arrDatos[23].toString()); 													//producto
			objPrecarga.setCopcSubProducto(arrDatos[24].toString()); 													//subproducto					
			objPrecarga.setCopcNuPlazo(((BigDecimal) arrDatos[25]).toString()); 										//plazo
			objPrecarga.setCopcDato11(Constantes.CONCILIACION_NOMBRE_ARCHIVO_MANUAL + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy"));
			objPrecarga.setCopcCargada(Constantes.DEFAULT_STRING_ID);
			objPrecarga.setCopcFeCarga(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
			objPrecarga.setCopcRegistro(Constantes.DEFAULT_STRING);
			
			objPrecarga.setCopcPrima(arrDatos[37].toString()); 															//prima
			objPrecargaPK.setCopcSumaAsegurada(arrDatos[38].toString()); 												//sumaasegurada
			
			//Se crea Dato Temporal de CARGA MANUAL.
			objManual = new VtaPrecarga();
			objManualId = new VtaPrecargaId();
			objManualId.setCdRamo(inRamo.intValue());
			objManualId.setCdSucursal(1);
			objManualId.setIdCertificado(objPrecargaPK.getCopcIdCertificado());
			objManualId.setNumPoliza(((BigDecimal) arrDatos[30]).longValue());
			objManualId.setSistemaOrigen(Constantes.HBS_CARGAS_MANUALES);
			objManual.setId(objManualId);
			objManual.setEstatus(arrDatos[31].toString());
			objManual.setSumaAsegurada(arrDatos[32] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[32].toString());
			objManual.setPrima(arrDatos[33] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[33].toString());
			objManual.setVdato1(arrDatos[34] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[34].toString());
			objManual.setFeNac(arrDatos[35] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[35].toString());
			objManual.setCp(Utilerias.validaCP(arrDatos[36] == null ? Constantes.DEFAULT_STRING : arrDatos[36].toString()));
			objManual.setCargada(Constantes.DEFAULT_STRING_ID);
			
			realizarOperacionManual(objPrecarga, objPrecargaPK, objManual, arrDatos[39].toString());
			switch (inRamo.intValue()) {
				case 61:
					objPrecargaPK.setCopcTipoRegistro("VID");
					objPrecarga.setCopcDato6(objPrecargaPK.getCopcSumaAsegurada()); 									//saldoinsoluto
					objPrecarga.setCopcDato8(objPrecargaPK.getCopcSumaAsegurada()); 									//sumaasegurada
					objPrecarga.setCopcSexo(arrDatos[10] == null ? Constantes.DEFAULT_STRING : arrDatos[10].toString()); 	//sexo
					objPrecarga.setCopcDato27(arrDatos[17] == null ? Constantes.DEFAULT_STRING : arrDatos[17].toString());	//lada
					objPrecarga.setCopcTelefono(arrDatos[16] == null ? Constantes.DEFAULT_STRING : arrDatos[16].toString()); //telefono
					objPrecarga.setCopcTipoCliente(arrDatos[18] == null ? Constantes.DEFAULT_STRING : arrDatos[18].toString()); //tipocliente
					objPrecarga.setCopcTipoCliente((objPrecarga.getCopcTipoCliente().equals(Constantes.DEFAULT_STRING)) ? "1" : objPrecarga.getCopcTipoCliente()); 	
					objPrecarga.setCopcCazbCdSucursal(arrDatos[26].toString()); 										//idproveedor sucursal
					
//					objPrecarga.setCopcDato2(); csv.get(mh.get("nombreasegurados"))
//					objPrecarga.setCopcDato3(); csv.get(mh.get("rfcasegurados"))
//					objPrecarga.setCopcDato4(); csv.get(mh.get("fechanacasegurados"))
//					objPrecarga.setCopcDato5(); csv.get(mh.get("sexoasegurados"))
//					objPrecarga.setCopcDato19(); csv.get(mh.get("tipoasegurados"))
//					objPrecarga.setCopcDato17(); csv.get(mh.get("noasegurados"))
//					objPrecarga.setCopcDato12(); csv.get(mh.get("numerobeneficiarios"))
//					objPrecarga.setCopcDato13(); csv.get(mh.get("nombrebeneficiarios"))
//					objPrecarga.setCopcDato14(); csv.get(mh.get("relacionasegurado"))
//					objPrecarga.setCopcDato15(); csv.get(mh.get("porcentajeparticipacion"))
					break;
					
				case 25:
					objPrecargaPK.setCopcTipoRegistro("DAN");
					objPrecarga.setCopcRemesa(arrDatos[20].toString()); 												//remesa
					objPrecarga.setCopcDato1(arrDatos[27] == null ? "N" : arrDatos[27].toString());	//metrosdelmar 
					objPrecarga.setCopcDato2(arrDatos[28] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[28].toString()); //piso
					objPrecarga.setCopcDato3(arrDatos[29] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[29].toString()); //numeropisos				
					break;
					
				case 9:
					objPrecargaPK.setCopcTipoRegistro("DES");
					objPrecarga.setCopcDato6(objPrecargaPK.getCopcSumaAsegurada()); 									//saldoinsoluto
					objPrecarga.setCopcDato8(objPrecargaPK.getCopcSumaAsegurada()); 									//sumaasegurada
					objPrecarga.setCopcSexo(arrDatos[10] == null ? Constantes.DEFAULT_STRING : arrDatos[10].toString()); //sexo
					objPrecarga.setCopcCazbCdSucursal(arrDatos[26].toString()); 										//sucursal
					if(objPrecarga.getCopcDato6() != null && (objPrecarga.getCopcDato6().equals("") || objPrecarga.getCopcDato6().length() == 0)){
						objPrecarga.setCopcDato6(objPrecarga.getCopcDato8());
					}
					break;
			}
		
			objPrecarga.setId(objPrecargaPK);
			cargaDao.guardarObjeto(objPrecarga);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.crearObjetoParaEmitir():: " + e.getMessage());
		}
	}
	
	private void crearObjetoParaEmitirNuevosValidar(Object[] arrDatos, Short inRamo, Object arlPolizas, HashMap<Long, String> hmTarifasxPoliza)  throws Exception {
		PreCarga objPrecarga;
		PreCargaId objPrecargaPK;
		VtaPrecarga objManual;
		VtaPrecargaId objManualId;
		boolean blnExisteFirmas;
		String strSistemaOrigen, strNombreCompleto, strTarifa;		
		String[] arrCP, arrEstado, arrDel, arrColonia, arrCalle;
		
		try {
			blnExisteFirmas = ((BigDecimal) arrDatos[0]).intValue() == Constantes.DEFAULT_INT ? false : true;
			strSistemaOrigen = arrDatos[1].toString().equals("HBS_SOF") ? Constantes.HBS_SOFOM : Constantes.HBS_BANCO;
			objPrecarga = new PreCarga();
			objPrecargaPK = new PreCargaId();				
			objPrecargaPK.setCopcCdSucursal(new Short("1"));
			objPrecargaPK.setCopcCdRamo(inRamo);																		//ramo
			objPrecargaPK.setCopcIdCertificado(arrDatos[2].toString()); 												//identificador		
			objPrecarga.setCopcCuenta(arrDatos[3] == null ? Constantes.DEFAULT_STRING_ID  : arrDatos[3].toString()); 	//cuenta
			objPrecarga.setCopcNuCredito(arrDatos[2].toString()); 														//prestamo
			arrCalle = arrDatos[8].toString().split("\\~");																//calle
			arrColonia = arrDatos[9].toString().split("\\~");															//colonia
			arrDel = arrDatos[10].toString().split("\\~");																//delg/mun
			arrEstado = arrDatos[11].toString().split("\\~");															//estado
			arrCP = arrDatos[12].toString().split("\\~");																//codigopostal
			
			strNombreCompleto = blnExisteFirmas == true ? arrDatos[4].toString().split("\\~")[1] : Utilerias.separarNombreCompleto(arrDatos[4].toString().split("\\~")[0]);
			objPrecarga.setCopcApPaterno(strNombreCompleto.split("\\|")[0].equals(Constantes.DEFAULT_STRING) ? Constantes.DEFAULT_SIN_DATO : strNombreCompleto.split("\\|")[0]); //apellidopaterno
			objPrecarga.setCopcApMaterno(strNombreCompleto.split("\\|")[1].equals(Constantes.DEFAULT_STRING) ? Constantes.DEFAULT_SIN_DATO : strNombreCompleto.split("\\|")[1]); //apellidomaterno 
			objPrecarga.setCopcNombre(strNombreCompleto.split("\\|")[2].equals(Constantes.DEFAULT_STRING) ? Constantes.DEFAULT_SIN_DATO : strNombreCompleto.split("\\|")[2]); 	 //nombre	
			objPrecarga.setCopcNumCliente(arrDatos[5] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[5].toString()); //numerocliente
			objPrecarga.setCopcNumCliente((objPrecarga.getCopcNumCliente().equals(Constantes.DEFAULT_STRING) || objPrecarga.getCopcNumCliente().equals(Constantes.DEFAULT_STRING_ID)) ? "1" : objPrecarga.getCopcNumCliente());
			objPrecarga.setCopcRfc(arrDatos[6] == null ? "XXXX800101" : arrDatos[6].toString());						//rfc
			objPrecarga.setCopcRfc(Utilerias.validaRFC(objPrecarga.getCopcRfc()) == true ? objPrecarga.getCopcRfc() : "XXXX800101"); 
			objPrecarga.setCopcFeNac(GestorFechas.generarFechaNacimiento(objPrecarga.getCopcRfc())); 					//fechanacimiento 
			objPrecarga.setCopcFeIngreso(arrDatos[16].toString()); 														//fechaingreso
			objPrecarga.setCopcDato33(arrDatos[18].toString()); 														//fechainiciocredito
			objPrecarga.setCopcDato34(arrDatos[19].toString()); 														//fechafincredito
			objPrecarga.setCopcBcoProducto(((BigDecimal) arrDatos[20]).toString());										//producto
			objPrecarga.setCopcSubProducto(arrDatos[21].toString()); 													//subproducto
			objPrecarga.setCopcNuPlazo(((BigDecimal) arrDatos[22]).toString()); 										//plazo
			objPrecarga.setCopcDato11(Constantes.CONCILIACION_NOMBRE_ARCHIVO_MANUAL + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy"));
			objPrecarga.setCopcCargada(Constantes.DEFAULT_STRING_ID);
			objPrecarga.setCopcFeCarga(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
			objPrecarga.setCopcRegistro(Constantes.DEFAULT_STRING);
			objPrecarga.setCopcPrima(arrDatos[34].toString()); 															//prima
			objPrecargaPK.setCopcSumaAsegurada(arrDatos[35].toString()); 												//sumaasegurada
			
			//Se crea Dato Temporal de CARGA MANUAL.
			objManual = new VtaPrecarga();
			objManualId = new VtaPrecargaId();
			objManualId.setCdRamo(inRamo.intValue());
			objManualId.setCdSucursal(1);
			objManualId.setIdCertificado(objPrecargaPK.getCopcIdCertificado());
			objManualId.setNumPoliza(((BigDecimal) arrDatos[27]).longValue());
			objManualId.setSistemaOrigen(Constantes.HBS_CARGAS_MANUALES);
			objManual.setId(objManualId);
			objManual.setEstatus(arrDatos[28].toString());
			objManual.setSumaAsegurada(arrDatos[29] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[29].toString());
			objManual.setPrima(arrDatos[30] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[30].toString());
			objManual.setVdato1(arrDatos[31] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[31].toString());
			objManual.setFeNac(arrDatos[32] == null ? Constantes.DEFAULT_SIN_DATO : arrDatos[32].toString());
			objManual.setCp(Utilerias.validaCP(arrDatos[33] == null ? Constantes.DEFAULT_STRING : arrDatos[33].toString()));
			objManual.setCargada(Constantes.DEFAULT_STRING_ID);
			
			switch (inRamo.intValue()) {
				case 61:
					objPrecargaPK.setCopcTipoRegistro("VID");
					objPrecarga.setCopcDato6(objPrecargaPK.getCopcSumaAsegurada()); 									//saldoinsoluto
					objPrecarga.setCopcDato8(objPrecargaPK.getCopcSumaAsegurada()); 									//sumaasegurada
					objPrecarga.setCopcSexo(arrDatos[7].toString());													//sexo
					objPrecarga.setCopcDato27(arrDatos[13] == null ? Constantes.DEFAULT_STRING : arrDatos[13].toString());	//lada
					objPrecarga.setCopcTelefono(arrDatos[14] == null ? Constantes.DEFAULT_STRING : arrDatos[14].toString()); //telefono
					objPrecarga.setCopcTipoCliente(arrDatos[15] == null ? Constantes.DEFAULT_STRING : arrDatos[15].toString()); //tipocliente
					objPrecarga.setCopcCazbCdSucursal(arrDatos[23].toString()); 										//idproveedor sucursal
					
					if(blnExisteFirmas) {
						if(arrCalle[0].equals(Constantes.DEFAULT_SIN_DATO)) { 
							//SE TOMAN DATOS DEL ARCHIVO DE FIRMAS
							objPrecarga.setCopcCalle(arrCalle[1]);					
							objPrecarga.setCopcColonia(arrColonia[1]);	
							objPrecarga.setCopcDelmunic(arrDel[1]); 	
							objPrecarga.setCopcCdEstado(arrEstado[1]); 						
							objPrecarga.setCopcCp(Utilerias.validaCP(arrCP[1]));
						} else {
							//SE TOMAN DATOS DEL ARCHIVO EXCEL
							objPrecarga.setCopcCalle(arrCalle[0]);					
							objPrecarga.setCopcColonia(arrColonia[0]);	
							objPrecarga.setCopcDelmunic(arrDel[0]); 	
							objPrecarga.setCopcCdEstado(arrEstado[1]); 						
							objPrecarga.setCopcCp(Utilerias.validaCP(arrCP[0]));
						}
					} else {
						objPrecarga.setCopcCalle(arrCalle[0]);			
						objPrecarga.setCopcColonia(arrColonia[0]);
						objPrecarga.setCopcDelmunic(arrDel[0]);
						objPrecarga.setCopcCp(Utilerias.validaCP(arrCP[0]));
						objPrecarga.setCopcCdEstado((arrEstado[0] == null || arrEstado[0].equals(Constantes.DEFAULT_STRING)) ? "33" : arrEstado[0]);
						objPrecarga.setCopcCdEstado(Utilerias.isNumeric(objPrecarga.getCopcCdEstado()) == true ? objPrecarga.getCopcCdEstado() : cargaDao.getCodigoEstado(objPrecarga.getCopcCp()));
					}
					
					
//					objPrecarga.setCopcDato2(); csv.get(mh.get("nombreasegurados"))
//					objPrecarga.setCopcDato3(); csv.get(mh.get("rfcasegurados"))
//					objPrecarga.setCopcDato4(); csv.get(mh.get("fechanacasegurados"))
//					objPrecarga.setCopcDato5(); csv.get(mh.get("sexoasegurados"))
//					objPrecarga.setCopcDato19(); csv.get(mh.get("tipoasegurados"))
//					objPrecarga.setCopcDato17(); csv.get(mh.get("noasegurados"))
//					objPrecarga.setCopcDato12(); csv.get(mh.get("numerobeneficiarios"))
//					objPrecarga.setCopcDato13(); csv.get(mh.get("nombrebeneficiarios"))
//					objPrecarga.setCopcDato14(); csv.get(mh.get("relacionasegurado"))
//					objPrecarga.setCopcDato15(); csv.get(mh.get("porcentajeparticipacion"))
					break;
					
				case 25:
					objPrecargaPK.setCopcTipoRegistro("DAN");
					objPrecarga.setCopcRemesa(arrDatos[17].toString()); 												//remesa
					objPrecarga.setCopcDato1(arrDatos[24] == null ? "N" : arrDatos[24].toString());						//metrosdelmar 
					objPrecarga.setCopcDato2(arrDatos[25] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[25].toString()); //piso
					objPrecarga.setCopcDato3(arrDatos[26] == null ? Constantes.DEFAULT_STRING_ID : arrDatos[26].toString()); //numeropisos
					
					objPrecarga.setCopcCalle(blnExisteFirmas == true ? arrCalle[1] : arrCalle[0]);			
					objPrecarga.setCopcColonia(blnExisteFirmas == true ? arrColonia[1] : arrColonia[0]);
					objPrecarga.setCopcDelmunic(blnExisteFirmas == true ? arrDel[1] : arrDel[0]);
					objPrecarga.setCopcCp(blnExisteFirmas == true ? Utilerias.validaCP(arrCP[1]) : Utilerias.validaCP(arrCP[0]));
					objPrecarga.setCopcCdEstado(blnExisteFirmas == true ? arrEstado[1] : arrEstado[0]);
					objPrecarga.setCopcCdEstado(Utilerias.isNumeric(objPrecarga.getCopcCdEstado()) == true ? objPrecarga.getCopcCdEstado() : cargaDao.getCodigoEstado(objPrecarga.getCopcCp()));
					break;
					
				case 9:
					objPrecargaPK.setCopcTipoRegistro("DES");
					objPrecarga.setCopcDato6(objPrecargaPK.getCopcSumaAsegurada()); 									//saldoinsoluto
					objPrecarga.setCopcDato8(objPrecargaPK.getCopcSumaAsegurada()); 									//sumaasegurada
					objPrecarga.setCopcSexo(arrDatos[7].toString()); 													//sexo
					objPrecarga.setCopcCazbCdSucursal(arrDatos[23].toString()); 										//sucursal
					if(objPrecarga.getCopcDato6() != null && (objPrecarga.getCopcDato6().equals("") || objPrecarga.getCopcDato6().length() == 0)){
						objPrecarga.setCopcDato6(objPrecarga.getCopcDato8());
					}
					
					if(blnExisteFirmas) {
						if(arrCalle[0].equals(Constantes.DEFAULT_SIN_DATO)) { 
							//SE TOMAN DATOS DEL ARCHIVO DE FIRMAS
							objPrecarga.setCopcCalle(arrCalle[1]);					
							objPrecarga.setCopcColonia(arrColonia[1]);	
							objPrecarga.setCopcDelmunic(arrDel[1]); 	
							objPrecarga.setCopcCdEstado(arrEstado[1]); 						
							objPrecarga.setCopcCp(Utilerias.validaCP(arrCP[1]));
						} else {
							//SE TOMAN DATOS DEL ARCHIVO EXCEL
							objPrecarga.setCopcCalle(arrCalle[0]);					
							objPrecarga.setCopcColonia(arrColonia[0]);	
							objPrecarga.setCopcDelmunic(arrDel[0]); 	
							objPrecarga.setCopcCdEstado(arrEstado[1]); 						
							objPrecarga.setCopcCp(Utilerias.validaCP(arrCP[0]));
						}
					} else {
						objPrecarga.setCopcCalle(arrCalle[0]);			
						objPrecarga.setCopcColonia(arrColonia[0]);
						objPrecarga.setCopcDelmunic(arrDel[0]);
						objPrecarga.setCopcCp(Utilerias.validaCP(arrCP[0]));
						objPrecarga.setCopcCdEstado((arrEstado[0] == null || arrEstado[0].equals(Constantes.DEFAULT_STRING)) ? "33" : arrEstado[0]);
						objPrecarga.setCopcCdEstado(Utilerias.isNumeric(objPrecarga.getCopcCdEstado()) == true ? objPrecarga.getCopcCdEstado() : cargaDao.getCodigoEstado(objPrecarga.getCopcCp()));
					}
					break;
			}

			strTarifa = realizarOperacionManual(objPrecarga, objPrecargaPK, objManual, arrDatos[36].toString());
			objPrecargaPK.setCopcNumPoliza(getNumPoliza(strSistemaOrigen, arlPolizas, hmTarifasxPoliza, strTarifa, arrDatos[37].toString()));				//poliza
			
			objPrecarga.setId(objPrecargaPK);
			objPrecarga.setCopcCalle(objPrecarga.getCopcCalle().length() > 150 ? objPrecarga.getCopcCalle().substring(0, 150) : objPrecarga.getCopcCalle());  //PARA RECORTAR LA CALLE A 150Caracteres
			objPrecarga.setCopcColonia(objPrecarga.getCopcColonia().length() > 55 ? objPrecarga.getCopcColonia().substring(0, 55) : objPrecarga.getCopcColonia());  //PARA RECORTAR LA COLONIA A 55Caracteres
			objPrecarga.setCopcDelmunic(objPrecarga.getCopcDelmunic().length() > 50 ? objPrecarga.getCopcDelmunic().substring(0, 50) : objPrecarga.getCopcDelmunic());  //PARA RECORTAR LA DEL/MUN A 50Caracteres
			
			if(objPrecarga.getId().getCopcNumPoliza() == Constantes.DEFAULT_LONG) {
				objPrecarga.setCopcCargada(Constantes.CARGADA_ERROR);
				objPrecarga.setCopcRegistro("Tarifa incorrecta o No Parametrizada.");
			}
			cargaDao.guardarObjeto(objPrecarga);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.crearObjetoParaEmitirNuevosValidar():: " + e.getMessage());
		}
	}

	@SuppressWarnings("rawtypes")
	private HashMap<Long, String> getTarifasxPoliza(Short inRamo, String strPolizas) throws Exception {
		HashMap<Long, String> hmTarifasxPolizas = null;
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
		Iterator it;
		String strTarifas = null;
		Long nPoliza;
		
		try {
			hmTarifasxPolizas = new HashMap<Long, String>();
			arlResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT DISTINCT COCB_CARP_CD_RAMO, COCB_CAPO_NU_POLIZA, HP.HOPP_DATO1 \n");
				sbQuery.append("FROM COLECTIVOS_COBERTURAS CB \n");
				sbQuery.append("INNER JOIN  HOMOLOGA_PRODPLANES HP ON(CB.COCB_CAPU_CD_PRODUCTO = HP.HOPP_PROD_COLECTIVO AND  CB.COCB_CAPB_CD_PLAN = HP.HOPP_PLAN_COLECTIVO) \n");
			sbQuery.append("WHERE CB.COCB_CASU_CD_SUCURSAL = 1 \n");
		    	sbQuery.append("AND CB.COCB_CARP_CD_RAMO = ").append(inRamo).append(" \n");
		    	sbQuery.append("AND CB.COCB_CAPO_NU_POLIZA IN(").append(strPolizas).append(") \n");
		    	sbQuery.append("AND CB.COCB_ESTATUS = 20 \n");     
		    	sbQuery.append("AND HP.HOPP_CD_RAMO = CB.COCB_CARP_CD_RAMO \n");
		    
	    	arlResultados.addAll(cargaDao.consultar(sbQuery.toString()));
			it = arlResultados.iterator();
			while(it.hasNext()) {
				Object[] arrDatos = (Object[]) it.next();
				nPoliza = ((BigDecimal) arrDatos[1]).longValue();
				if(hmTarifasxPolizas.containsKey(nPoliza)) {
					strTarifas = hmTarifasxPolizas.get(nPoliza) + "|" + arrDatos[2].toString();
					hmTarifasxPolizas.put(nPoliza, strTarifas);
				} else {
					hmTarifasxPolizas.put(nPoliza, arrDatos[2].toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.getTarifasxPoliza():: " + e.getMessage());
		}
		
		return hmTarifasxPolizas;
	}
	
	@SuppressWarnings("unchecked")
	private Long getNumPoliza(String strSistemaOrigen, Object arlPolizas, HashMap<Long, String> hmTarifasxPoliza, String strTarifa, String strEsPemex) throws Exception {
		Long nNumPoliza = Constantes.DEFAULT_LONG;
		ArrayList<Parametros> arlParamPolizas;
		Iterator<Parametros> it;
		Parametros objParam;
		String[] arrTarxPol;
		boolean blnRecorreTarifas = false, blnEncontroTar = false;
		
		try {
			arlParamPolizas = (ArrayList<Parametros>) arlPolizas;
			it = arlParamPolizas.iterator();
			while(it.hasNext()) {
				objParam = it.next();
				if(strSistemaOrigen.equals(objParam.getCopaVvalor4()) && !objParam.getCopaVvalor5().equals(Constantes.DEFAULT_STRING_ID)) { //Polizas para creditos nuevos VVALOR5 = 1
					arrTarxPol = hmTarifasxPoliza.get(objParam.getCopaNvalor3()).split("\\|");
					
					if(strEsPemex.equals(Constantes.HBS_ES_PEMEX) && objParam.getCopaVvalor3().equals(Constantes.HBS_ES_PEMEX)) { // ESTO ES PARA QUE NADA MAS TRABAJE CON LAS POLZIAS DE PEMEX 
						blnRecorreTarifas = true;
					} else if(strEsPemex.equals(Constantes.DEFAULT_STRING_ID) && objParam.getCopaVvalor3().equals(Constantes.DEFAULT_STRING_ID)) { // ESTO ES PARA QUE TRABAJE CON LAS DEMAS POLZIAS
						blnRecorreTarifas = true;
					}
					
					if(blnRecorreTarifas) {
						for (int i = 0; i < arrTarxPol.length; i++) {
							if(arrTarxPol[i].equals(strTarifa)) {
								nNumPoliza = objParam.getCopaNvalor3();
								blnEncontroTar = true;
								break;
							}
						}
					}
				}
				
				if(blnEncontroTar){
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.getNumPoliza():: " + e.getMessage());
		}
		return nNumPoliza;
	}
	
	private String realizarOperacionManual(PreCarga objPrecarga, PreCargaId objPrecargaPK, VtaPrecarga objManual, String strTarifa) throws Exception {
		int nOperacion;
		
		try {
			nOperacion = Integer.parseInt(objManual.getEstatus());
			switch (nOperacion) {
				case Constantes.HBS_ACCION_CREDITO:
					break;
				case Constantes.HBS_ACCION_CREDITO_SUMA:
					objPrecargaPK.setCopcSumaAsegurada(objManual.getSumaAsegurada());
					objPrecarga.setCopcPrima(Utilerias.calcularPrima(objPrecargaPK.getCopcSumaAsegurada(), strTarifa, 1000).toString());
					break;
				case Constantes.HBS_ACCION_CREDITO_PRIMA:
					objPrecarga.setCopcPrima(objManual.getPrima());
					break;
				case Constantes.HBS_ACCION_CREDITO_TARIFA:
					strTarifa = objManual.getVdato1().toString();
					objPrecarga.setCopcPrima(Utilerias.calcularPrima(objPrecargaPK.getCopcSumaAsegurada(), strTarifa, 1000).toString());
					break;
				case Constantes.HBS_ACCION_CREDITO_SUMA_TARIFA:
					objPrecargaPK.setCopcSumaAsegurada(objManual.getSumaAsegurada());
					strTarifa = objManual.getVdato1().toString();
					objPrecarga.setCopcPrima(Utilerias.calcularPrima(objPrecargaPK.getCopcSumaAsegurada(), strTarifa, 1000).toString());
					break;
				case Constantes.HBS_ACCION_CREDITO_FECHANAC:
					objPrecarga.setCopcFeNac(objManual.getFeNac());
					objPrecarga.setCopcRfc(RFC.getRFC(objPrecarga.getCopcNombre(), objPrecarga.getCopcApPaterno(), objPrecarga.getCopcApMaterno(), objPrecarga.getCopcFeNac()));
					break;
				case Constantes.HBS_ACCION_CREDITO_CP:
					objPrecarga.setCopcCp(Utilerias.validaCP(objManual.getCp()));
					break;
			}
			
			if(objPrecargaPK.getCopcCdRamo().intValue() == 9) {
				objPrecarga.setCopcDato2(strTarifa);
			} else {
				objPrecarga.setCopcDato7(strTarifa);
			}
			
			//SE ELIMINA DATO TEMPORAL
			cargaDao.borrarObjeto(objManual);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.realizarOperacionManual():: " + e.getMessage());
		}
		
		return strTarifa;
	}

	public Collection<? extends Object> getResultadosCarga(Short inRamo, String strDescArchivo) throws Exception {
		ArrayList<Resultado> arlResultados;
		StringBuffer sbQuery;
		Iterator it;
		Resultado objResultado;
		
		try {
			arlResultados = new ArrayList<Resultado>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT CP.COPC_NUM_POLIZA, COUNT(*), CP.COPC_CARGADA , SUM(CP.COPC_PRIMA) \n");
				sbQuery.append("FROM COLECTIVOS_PRECARGA CP \n");
			sbQuery.append("WHERE CP.COPC_CD_RAMO = ").append(inRamo).append(" \n");
		    	sbQuery.append("AND CP.COPC_DATO11 = '").append(strDescArchivo).append("' \n");
		    sbQuery.append("GROUP BY CP.COPC_NUM_POLIZA, CP.COPC_CARGADA \n");     
		    
	    	it = cargaDao.consultar(sbQuery.toString()).iterator();
			while(it.hasNext()) {
				objResultado = new Resultado((Object[]) it.next());
				arlResultados.add(objResultado);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ServicioCargaManualHBSImpl.getResultadosCarga():: " + e.getMessage());
		}
		
		return arlResultados;
	}
	
	public CargaDao getCargaDao() {
		return cargaDao;
	}
	public void setCargaDao(CargaDao cargaDao) {
		this.cargaDao = cargaDao;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}

}
