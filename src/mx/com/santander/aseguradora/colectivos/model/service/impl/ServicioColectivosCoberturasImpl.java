package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ColectivosCoberturasDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ColectivosComponentesDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosCoberturas;

/*
 * @author: Leobardo Preciado
 */

@Service
public class ServicioColectivosCoberturasImpl implements ServicioColectivosCoberturas {

	@Resource
	private ColectivosCoberturasDao colectivosCoberturasDao;
	
	@Resource
	private ColectivosComponentesDao colectivosComponentesDao;
	
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;

	public <T> T guardarObjeto(T objeto) throws Excepciones {

		try {
			T cartCobertura = this.colectivosCoberturasDao.guardarObjeto(objeto);
			return cartCobertura;
		} catch (ObjetoDuplicado e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Cobertura duplicada.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("No se puede guardar la cobertura.");
		}
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {

		try {

			this.colectivosCoberturasDao.actualizarObjeto(objeto);

		} catch (Exception e) {
			this.message.append("Error al actualizar el plan.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {

		try {
			List<T> lista = this.colectivosCoberturasDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden recuperar los productos");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.santander.aseguradora.colectivos.model.service.
	 * ServicioColectivosCoberturas#actualizaTipoComision(mx.com.santander.
	 * aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas)
	 */
	@Override
	public void actualizaTipoComision(ColectivosCoberturas colectivosCoberturas) throws Excepciones {
		if ("P".equals(colectivosCoberturas.getTipoComision())) {
			colectivosCoberturasDao.actualizaProductoPlan(colectivosCoberturas);
			colectivosComponentesDao.actualizaProductoPlan(colectivosCoberturas);
		} else {
			colectivosComponentesDao.actualizaCobertura(colectivosCoberturas);
		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosCoberturas#actualizaParametrizacionTipoComision(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros)
	 */
	@Override
	public void actualizaParametrizacionTipoComision(Parametros parametrosConfiguracion) throws Excepciones {
		if ("P".equals(parametrosConfiguracion.getCopaVvalor4())) {
			colectivosCoberturasDao.actualizaConfiguracionProductoPlan(parametrosConfiguracion);
			colectivosComponentesDao.actualizaParametrizacionProductoPlan(parametrosConfiguracion);
		} else if ("D".equals(parametrosConfiguracion.getCopaVvalor4())) {
			colectivosCoberturasDao.actualizaConfiguracionCobertura(parametrosConfiguracion, " = '003' ");
			colectivosComponentesDao.actualizaParametrizacionCobertura(parametrosConfiguracion);
		} else if ("C".equals(parametrosConfiguracion.getCopaVvalor4())) {
			colectivosCoberturasDao.actualizaConfiguracionCobertura(parametrosConfiguracion, " <> '003' ");
			colectivosComponentesDao.actualizaParametrizacionCobertura(parametrosConfiguracion);
			
		}
			
		
	}
	
	
	
	
	
}
