/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.dao.ProductoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author dflores
 *
 */
@Service
public class ServicioProductoImpl implements ServicioProducto {

	@Resource
	private ProductoDao productoDao;
	private Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * @param polizaDao the polizaDao to set
	 */
	public void setProductoDao(ProductoDao productoDao) {
		this.productoDao = productoDao;
	}

	/**
	 * @return the polizaDao
	 */
	public ProductoDao getProductoDao() {
		return productoDao;
	}

	public ServicioProductoImpl() {
	}
	
	public <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones{
		try {
			List<T> lista = this.productoDao.obtenerObjetos(objeto);
			return lista;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los productos.", e);
			throw new Excepciones("No se pueden recuperar los productos.", e);
			
		}
	}

	public <T> List<T> obtenerObjetos(String filtro)throws Excepciones {
		try {
			List<T> lista = this.productoDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los productos", e);
			throw new Excepciones("No se pueden recuperar los productos", e);
		}
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		try {
			 T producto = this.productoDao.obtenerObjeto(objeto, id);
			 return producto;
		} catch (Exception e) {
			this.log.error("No se puede obtener el producto" + id.toString(), e);
			throw new Excepciones("No se puede obtener el producto" + id.toString(), e);
		}
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.productoDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			this.log.error("Error al actualizar el producto.", e);
			throw new Excepciones("Error al actualizar el producto.", e);
		}
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		try {
			this.productoDao.borrarObjeto(objeto);
		} catch (Exception e) {
			throw new Excepciones("Error al eliminar el producto.");
		}
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			T producto = this.productoDao.guardarObjeto(objeto);
			return producto;
		} catch (ObjetoDuplicado e) {
			throw new ObjetoDuplicado("Producto duplicado.", e);
		} catch (Exception e) {
			throw new Excepciones("No se puede guardar el producto.");
		}
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		try {
			this.productoDao.guardarObjetos(lista);
		} catch (Exception e) {
			this.log.error("Error al guardar lista de productos.", e);
			throw new Excepciones("Error al guardar lista de productos.", e);
		}
		
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}

	public Integer siguenteProducto(Short ramo) throws Excepciones {
		try {
			return this.productoDao.siguenteProducto(ramo);
		} catch (Exception e) {
			this.log.error("No se puede generar secuencia del producto.", e);
			throw new Excepciones("No se puede generar secuencia del producto.", e);
		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto#productoPlanes(java.lang.String)
	 */
	@Override
	public List<Producto> productoPlanes(String filtro)  throws Excepciones {
		try {
			return productoDao.productoPlanes(filtro);
		} catch (Exception e) {
			this.log.error("No se puede consultar la lista de productos.", e);
			throw new Excepciones("No se puede consultar la lista de productos.", e);
		}
	}
	
	

}
