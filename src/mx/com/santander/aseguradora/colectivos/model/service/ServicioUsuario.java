/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;


/**
 * @author Sergio Plata
 *
 */
public interface ServicioUsuario extends ServicioCatalogo{

	public String validarContraseñaNueva(String passwordnew, String passwordrep) throws Exception;
	
	
}	
	
