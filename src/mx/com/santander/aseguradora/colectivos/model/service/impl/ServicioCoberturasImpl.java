/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cobertura;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CoberturaId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.dao.CoberturaDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCobertura;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author dflores
 *
 */
public class ServicioCoberturasImpl extends HibernateDaoSupport implements ServicioCobertura {

	private Log log = LogFactory.getLog(this.getClass());
	private CoberturaDao coberturaDao;
	private String message;	
	
	/**
	 * @param polizaDao the polizaDao to set
	 */
	public void setCoberturaDao(CoberturaDao coberturaDao) {
		this.coberturaDao = coberturaDao;
	}

	/**
	 * @return the polizaDao
	 */
	public CoberturaDao getCoberturaDao() {
		return coberturaDao;
	}

	public List<Cobertura> getCoberturas()throws Excepciones{
		// TODO Auto-generated method stub
		
		try {
			return this.coberturaDao.getCoberturas();
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se pueden recuperar las coberturas.";
			this.log.error(message, e);
			throw new Excepciones(message, e);
			
		}
	}

	public List<Cobertura> getCoberturas(Integer ramoContable)throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			return this.coberturaDao.getCoberturas(ramoContable);
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se pueden recuperar las coberturas del ramoContable " + ramoContable.toString();
			this.log.error(message, e);
			throw new Excepciones(message, e);
		}
		
	}

	public Cobertura getCobertura(CoberturaId id) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			return this.coberturaDao.getCobertura(id);
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se puede obtener la cobertura con id " + id.toString();
			this.log.error(message, e);
			throw new Excepciones(message, e);
		}
	}
	
	public List<CartCoberturas> RecuperaCartCoberturas(Integer ramo) throws Excepciones  {
		// TODO Auto-generated method stub
		
		StringBuilder consulta = null;
		List<Object> lstResultados;
				
		try {
						
			consulta = new StringBuilder();
    		
    		consulta.append(" SELECT distinct (cc.cacb_carp_cd_ramo),            \n"); 	        
    		consulta.append("	         cc.cacb_cd_cobertura  cve_cob,           \n");	
    		consulta.append("	         cc.cacb_de_cobertura  desc_cobertura,    \n");	
    		consulta.append("	         cc.cacb_carb_cd_ramo ramoCont            \n");	
    		consulta.append("    	FROM COLECTIVOS_COBERTURAS C,  				  \n");	
    		consulta.append("           cart_coberturas cc						  \n");	
    		consulta.append("WHERE c.cocb_carp_cd_ramo = ").append(61).append(   "\n"); 
    		consulta.append("and c.cocb_carp_cd_ramo = cc.cacb_carp_cd_ramo      \n");	
    		consulta.append("and c.cocb_cacb_cd_cobertura = cc.cacb_cd_cobertura \n");	
    		consulta.append("and c.cocb_carb_cd_ramo = cc.cacb_carb_cd_ramo      \n");	
    		consulta.append("and cc.cacb_in_cobertura_sub = 'C'  \n");
    		
    		Query qry = this.getSession().createSQLQuery(consulta.toString());

    		lstResultados = qry.list();
						
			return this.RecuperaCartCoberturas(ramo);
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se pueden recuperar las coberturas del ramo " + ramo.toString();
			this.log.error(message, e);
			throw new Excepciones(message, e);
			
		}
		
	}
	
	

}
