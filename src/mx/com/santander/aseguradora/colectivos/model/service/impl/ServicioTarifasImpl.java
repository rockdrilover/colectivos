/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTarifas;
import mx.com.santander.aseguradora.colectivos.model.dao.TarifasDao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Ing. Hery Tyler
 *
 */
@Service
public class ServicioTarifasImpl implements ServicioTarifas  {

	@Resource
	private TarifasDao TarifasDao;

	private Log log = LogFactory.getLog(this.getClass());
	private String message = null;
	
	public ServicioTarifasImpl() {
		this.message = new String();
	}
	
	public List<Object> getTarifas(Integer canal, Short ramo, Integer producto) throws Excepciones {
		List<Object> lista = null;
		
		try {
			lista = this.TarifasDao.getConsultaTarifas(canal, ramo, producto);
			
			return lista;
		} catch (Exception e) {
			message = "No se puede realizar la consulta tarifas.";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
		
	}

	public List<Object> getCoberturas(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones {
		List<Object> lista = null;
		
		try {
			lista = this.TarifasDao.getConsultaCoberturas(canal, ramo, producto, plan);
			return lista;
		} catch (Exception e) {
			message = "No se puede realizar la consulta de coberturas.";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
	}
	
	public List<Object> getComponentes(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones {
		List<Object> lista = null;
		
		try {
			lista = this.TarifasDao.getConsultaComponentes(canal, ramo, producto, plan);
			return lista;
		} catch (Exception e) {
			message = "No se puede realizar la consulta de componentes.";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
	}

	public List<Object> getProdPlanes(Short ramo, Integer producto, Short plan) throws Excepciones {
		List<Object> lista = null;
		
		try {
			lista = this.TarifasDao.getConsultaHomologa(ramo, producto, plan);
			return lista;
		} catch (Exception e) {
			message = "No se puede realizar la consulta de homologa .";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
	}
	
	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}

	public void reporteDetalleTarifas(String rutaTemporal, String rutaReporte,
			Map<String, Object> inParams) throws Excepciones {
		// TODO Auto-generated method stub
		
		
         this.TarifasDao.reporteDetalleTarifas (rutaTemporal, rutaReporte, inParams);
	}

}
