/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * 
 * @author Ing. Hery Tyler
 *
 */
public interface ServicioTarifas extends InitializingBean, DisposableBean{

	public List<Object> getTarifas(Integer canal, Short ramo, Integer producto) throws Excepciones;

	public List<Object> getCoberturas(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones;
	
	public List<Object> getComponentes(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones;
	
	public List<Object> getProdPlanes(Short ramo, Integer producto, Short plan) throws Excepciones;

	public void reporteDetalleTarifas(String rutaTemporal, String rutaReporte,
			Map<String, Object> inParams)throws Excepciones;
  
}
