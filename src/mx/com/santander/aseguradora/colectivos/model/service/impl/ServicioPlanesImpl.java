/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.dao.PlanDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author dflores
 *
 */

@Service
public class ServicioPlanesImpl implements ServicioPlan {

	@Resource
	private PlanDao planDao;
	private Log log = LogFactory.getLog(this.getClass());
		
	public ServicioPlanesImpl() {
	}
	
	public PlanDao getPlanDao() {
		return planDao;
	}
	public void setPlanDao(PlanDao planDao) {
		this.planDao = planDao;
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.planDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			this.log.error("Error al actualizar el plan.", e);
			throw new Excepciones("Error al actualizar el plan.", e);
		}		
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			T plan = this.planDao.guardarObjeto(objeto);
			return plan;
		} 
		catch (ObjetoDuplicado e) {
			throw new ObjetoDuplicado("Producto duplicado.", e);
		} catch (Exception e) {
			e.printStackTrace();	
			throw new Excepciones("No se puede guardar el producto.");
		}
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro)throws Excepciones {
		try {
			List<T> lista = this.planDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los planes", e);
			throw new Excepciones("No se pueden recuperar los planes", e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}

	public Plan getPlan(PlanId id) throws Excepciones {
		return null;
	}

	public List<Plan> getPlanes()throws Excepciones{
		try {
			return this.planDao.getPlanes();
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los planes.", e);
			throw new Excepciones("No se pueden recuperar los planes.", e);
		}
	}

	public List<Plan> getPlanes(Integer producto)throws Excepciones {
		try {
			return this.planDao.getPlanes(producto);
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los planes del producto" + producto.toString(), e);
			throw new Excepciones("No se pueden recuperar los planes del producto" + producto.toString(), e);
		}
	}

	public List<Integer> getIdPlanes()throws Excepciones{
		try {
			return this.planDao.getIdPlanes();
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los planes.", e);
			throw new Excepciones("No se pueden recuperar los planes.", e);
			
		}
	}
	
	//-----  MGE --//
	public Integer siguentePlanExtraPma() throws Excepciones {
		try {
			return this.planDao.siguentePlanExtraPma();
		} catch (Exception e) {
			this.log.error("No se puede generar el valor de cd plan.", e);
			throw new Excepciones("No se puede generar el valor de cd plan.", e);
		}
	}

	//-----  LPV --//
	public Integer siguentePlan(String filtro) throws Excepciones {
		try {
			return this.planDao.siguentePlan(filtro);
		} catch (Exception e) {
			this.log.error("No se puede generar el valor de cd plan.", e);
			throw new Excepciones("No se puede generar el valor de cd plan.", e);
		}
	}

}
