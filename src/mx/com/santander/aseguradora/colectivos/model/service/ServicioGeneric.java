package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		27-07-2020
 * Description: Interface que implementa la clase ServicioGenericImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 2.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 20-11-2022
 * 		Porque: Consultar fecha de vencimiento del credito
 * ------------------------------------------------------------------------
 * 
 */
@Remote
public interface ServicioGeneric {

	/**
	 * Metodo que verifica si el certificado esta en proceso de Siniestro
	 * @param listaCertificados lista de certificados
	 * @return 0-Notiene Siniestro en proceso
	 * 		difeirente a 0-Si tiene siniestro en proceso
	 * @throws Excepciones con error general
	 */
	String tieneSiniestro(List<BeanClienteCertif> listaCertificados) throws Excepciones;

	/**
	 * Metodo que ejecuta una consulta en base de datos
	 * @param sbConsulta query a ejecutar
	 * @return lsita con resultados de query
	 * @throws Excepciones con error general
	 */
	List<Object> consultaSQL(StringBuilder sbConsulta) throws Excepciones;

	/**
	 * Metodo que migra informacion a base de Individual (Para que la vea Siniestros)
	 * @param objCliCertId datos para migracion
	 * @throws Excepciones con error general
	 */
	void migraInformacion(ClienteCertifId objCliCertId) throws Excepciones;
	
	/**
	 * Metodo que regresa el siguiente numero de endoso
	 * @param ramo numero de ramo
	 * @param stEndoso estatus de endoso
	 * @return numero de endoso
	 * @throws Excepciones con error en general
	 */
	Long getNumeroEndoso(short ramo, String stEndoso) throws Excepciones;
	
	/**
	 * Metodo que ejecuta un update sin parametros
	 * @param sbQuery sentencia a ejecutar
	 * @throws Excepciones con errores generales
	 */
	void updateSinParametros(StringBuilder sbQuery)  throws Excepciones;

	/**
	 * Metodod que obtiene la fecha de vencimiento de un certificado
	 * @param id datos del credito
	 * @return fecha de vencimiento
	 * @throws Excepciones con errores generales
	 */
	Date getFechaVencimiento(CertificadoId id) throws Excepciones;
}
