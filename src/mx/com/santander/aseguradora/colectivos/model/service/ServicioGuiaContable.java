package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;


public interface ServicioGuiaContable{
	
	public List<Object> consultarProductos(int nDatosRamos) throws Exception;

	public List<Object> consultarPlanes(int nDatosProductos, int nDatosRamos) throws Exception;

	public List<Object> obtenerCoberturas(int nDatosRamos, int nDatosProductos,	int nDatosPlanes) throws Exception;

	public List<Object> obtenerGuiasContables(String ramoPoliza, String ramoContable) throws Exception;
	
}
