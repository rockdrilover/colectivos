/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.database.EndosoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndoso;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.utils.Constantes.OPC_FORMATO_REPORTE;

/**
 * @author dflores
 *
 */
@Service
public class ServicioEndosoImpl implements ServicioEndoso {

	@Resource
	private EndosoDao endosoDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	/**
	 * @return the reciboDao
	 */
	public EndosoDao getEndosoDao() {
		return endosoDao;
	}

	/**
	 * @param reciboDao the reciboDao to set
	 */
	public void setEndosoDao(EndosoDao endosoDao) {
		this.endosoDao = endosoDao;
	}

	public ServicioEndosoImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.String)
	 */
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerRecibo.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			List<T> lista = this.endosoDao.obtenerObjetos(filtro);
			//System.out.println("Tama�o lista 1" + lista.size());
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los recibos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.DisposableBean#destroy()
	 */
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}


	public void reporteEndosos(String rutaTemporal, String rutaReporte,
			Map<String, Object> inParams, OPC_FORMATO_REPORTE opcr ) throws Exception {
		// TODO Auto-generated method stub
		this.endosoDao.reporteEndosos(rutaTemporal, rutaReporte, inParams, opcr);
		
	}
}
