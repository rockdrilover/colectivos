package mx.com.santander.aseguradora.colectivos.model.service;

import java.io.File;

import javax.ejb.Local;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		08-09-2020
 * Description: Interface que implementa la clase ServicioParametrosComisionImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 14-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Local
public interface ServicioParametrosComision {
	
	/**
	 * Metodo que procesa, valida y guarda registros de LayOut comisiones
	 * @param archivo lay out con reglas de comision
	 * @return archivo de salida con errores
	 * @throws Excepciones error en general
	 */
	Archivo guardaDatosArchivo(File archivo)  throws Excepciones;

}
