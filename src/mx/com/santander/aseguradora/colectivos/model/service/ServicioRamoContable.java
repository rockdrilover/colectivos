/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.RamoContable;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones; 

/**
 * @author dflores
 *
 */
public interface ServicioRamoContable extends ServicioCatalogo {
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws ExcepcionCatalogo
	 */
	public RamoContable getRamoContable(Integer id)throws Excepciones;

	/**
	 * 
	 * @return
	 * @throws ExcepcionCatalogo
	 */
	public List<Object> getListaRamosContables()throws Excepciones;
	
	
	public <T> List<T> getRamosContables() throws Excepciones;
}
