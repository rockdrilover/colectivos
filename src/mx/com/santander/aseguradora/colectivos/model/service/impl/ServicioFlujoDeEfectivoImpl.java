package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.FlujoDeEfectivoDao;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioFlujoDeEfectivo;

@Service
public class ServicioFlujoDeEfectivoImpl implements ServicioFlujoDeEfectivo{
	
	// Dao
	@Resource
	private FlujoDeEfectivoDao flujoDao;

	// Getters and Setters
	public FlujoDeEfectivoDao getFlujoDao() {
		return flujoDao;
	}

	public void setFlujoDao(FlujoDeEfectivoDao flujoDao) {
		this.flujoDao = flujoDao;
	}
	
	// Metodos
	@Override
	public List<Object> consultaCuentas() throws Exception {
		List<Object> arlDatos = null;
		
		try {
			arlDatos = new ArrayList<Object>();
			arlDatos = flujoDao.consultaCuentas();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arlDatos;
	}

	@Override
	public <T> List<T> cargaReporteCargoAbono(String fInicio, String fFin, String cuenta, Integer operacion) throws Exception {
		try {
			return flujoDao.cargaReporteCargoAbono(fInicio, fFin,cuenta,operacion);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
