package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

public interface ServicioFlujoDeEfectivo {

	public List<Object> consultaCuentas() throws Exception;

	public <T> List<T> cargaReporteCargoAbono(String fInicio, String fFin, String cuenta, Integer operacion) throws Exception;

}
