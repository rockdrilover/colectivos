/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;




import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ServicioPlan extends ServicioCatalogo{

	/**
	 * 
	 * @param poliza
	 * @return
	 */
	public Plan getPlan(PlanId id)throws Excepciones;
	/**
	 * 
	 * @return
	 * @throws ExcepcionCatalogo 
	 */
	public List<Plan> getPlanes() throws Excepciones;
	/**
	 * 
	 * @param ramo
	 * @return
	 */
	public List<Plan> getPlanes(final Integer producto)throws Excepciones;
	
	public List<Integer> getIdPlanes()throws Excepciones;
	public Integer siguentePlanExtraPma()throws Excepciones;
	public Integer siguentePlan(String filtro)throws Excepciones;
	

}
