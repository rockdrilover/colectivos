/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;




import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ServicioAltaTarifaCobertura{



	public Integer iniciaAlta( Integer canal, Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, 
			Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, 
			Float taRiesgo, Integer edadMin, Integer edadMax) throws Excepciones;
  
}
