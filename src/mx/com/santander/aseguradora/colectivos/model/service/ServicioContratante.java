/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClientePK;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author Sergio Plata
 *
 */
public interface ServicioContratante extends ServicioCatalogo {
    //dfg
    public <T> List<T> consulta(String nacionalidad, String cedula, String razonSocial,String strFiltro) throws Excepciones;
    public List<Object[]> consulta2(String strFiltro) throws Exception;
    public String guarda(Long next,String idParam,String desParam,String nacionalidad, String cedula,short canal,short ramo,
					     Integer idVenta, Integer idProducto) throws Exception;
    
    /**
     * Metodo que realiza consulta mediante objetos con lista de parametros
     * @param <T> objeto a regresar en la lista
     * @param nTipoConsulta tipo de consulta a ejecutar
     * @param cartClientePK datos del cliente
     * @return resultados 
     * @throws Excepciones con error en general
     */
	<T> List<T> obtenerObjetos(Integer nTipoConsulta, CartClientePK cartClientePK) throws Excepciones;
}
