package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bo.CartCatalogosCfdi;
import mx.com.santander.aseguradora.colectivos.model.dao.ParamTimbradoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParamTimbrado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.dto.NombreProductoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ParamTimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase de tipo Service que tiene logica para el proceso
 * 				de Parametrizacion timbrado
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioParamTimbradoImpl implements ServicioParamTimbrado {

	//Instancia de DAO
	@Resource
	private ParamTimbradoDao paramTimbradoDao;
	
	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioParamTimbradoImpl.class);
	
	//Variable para mensajes
	private StringBuilder message;

	/**
	 * Cosntructor de Clase
	 */
	public ServicioParamTimbradoImpl() {
		message = new StringBuilder();
	}
	
	@Override
	public List<ParamTimbradoDTO> consultaDatos(Integer canal, Short ramo, Integer poliza, Integer idVenta) throws Excepciones {
		List<ParamTimbradoDTO> lstResultado;
		List<Object> lstDatos;
		Iterator<Object> it;
		ParamTimbradoDTO dto;
		
		try {
			//Se inicializa la lista
			lstResultado = new ArrayList<ParamTimbradoDTO>();
			
			//se valida si la consulta es para PR o PU
			if(poliza == Constantes.DEFAULT_INT) {
				//se consultan datos para PU
				lstDatos = this.paramTimbradoDao.getDatosPU(canal, ramo, idVenta);
			} else {
				//se consultan datos para PR
				lstDatos = this.paramTimbradoDao.getDatosPR(canal, ramo, poliza);
			}
			
			//Se recorren los resultados para crear lista de objetos que se ocupan en la vista
			it = lstDatos.iterator();
			while(it.hasNext()) {
				dto = new ParamTimbradoDTO((Object[])it.next(), idVenta);
				lstResultado.add(dto);
			}

			//Se regresa la lista de resultados
			return lstResultado;
		} catch (Excepciones e) {
			this.message.append("Error al recuperar registros.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	@Override
	public List<Object> getComboCatalogosCFDI(int cfdiMetodosPago) throws Excepciones {
		List<Object> lstCombo = null;
		StringBuilder filtro = new StringBuilder(100);
		List<Object> lstResultados;
		CartCatalogosCfdi catalogo;
		Iterator<Object> it;
		
		try {
			lstCombo = new ArrayList<Object>();
			lstCombo.add(new SelectItem(Constantes.DEFAULT_STRING, "-- Seleccionar --"));
			
			//Se realiza consulta para mostrar el catalogo seleccionado
			filtro.append("and CCC.id.cartcfCdCfdi = 901" );
			lstResultados = this.paramTimbradoDao.obtenerObjetos(filtro.toString());
			
			//se recorren resultados para crear combo que se utiliza en la vista
		    it = lstResultados.iterator();
		    while(it.hasNext()) {
		    	catalogo = (CartCatalogosCfdi) it.next();
		    	lstCombo.add(new SelectItem(catalogo.getId().getCartcfCdClave(), catalogo.getId().getCartcfCdClave() + " - " +catalogo.getCartcfValor1()));
			}
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("No se pueden cargar el combo del catalogo", e);
		}
		
		return lstCombo;
	}
	
	@Override
	public Map<String, String> getNombreProducto() throws Excepciones {
		//Se inicializan variables para proceso
		List<NombreProductoDTO> dtoList = new ArrayList<NombreProductoDTO>();
		List<Object> lstResultados;
		Iterator<Object> it;
		NombreProductoDTO dto;
		Map<String, String> nProducto = null;
		
		try {
			//Se ejecuta consulta
			lstResultados = this.paramTimbradoDao.obtenerNombreProductos();
			
			//Se obtiene iterador para recorrer registros
			it = lstResultados.iterator();
			//Se recorren registros 
			while(it.hasNext()) {
				//Se crea nuevo objeto con datos que regreso consulta
				dto = new NombreProductoDTO((Object[])it.next());
				//Se agrega objeto a lista
				dtoList.add(dto);
			}
			
			//Se crea hashmap para regresar solo descripcion y id
			nProducto = new HashMap<>();
			for(NombreProductoDTO i : dtoList) {
				nProducto.put(i.getDescripcion(), String.valueOf(i.getValue()));
			}
			
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("No se pueden cargar el combo del catalogo", e);
		}
		
		//Se regresan lista de resultados
		return nProducto;
	}
	
	/**
	 * @return the message
	 */
	public StringBuilder getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	/**
	 * @return the paramTimbradoDao
	 */
	public ParamTimbradoDao getParamTimbradoDao() {
		return paramTimbradoDao;
	}
	/**
	 * @param paramTimbradoDao the paramTimbradoDao to set
	 */
	public void setParamTimbradoDao(ParamTimbradoDao paramTimbradoDao) {
		this.paramTimbradoDao = paramTimbradoDao;
	}
}
