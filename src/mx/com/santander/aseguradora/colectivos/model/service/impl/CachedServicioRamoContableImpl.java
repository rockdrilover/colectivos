/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


import mx.com.santander.aseguradora.colectivos.model.bussinessObject.RamoContable;
import mx.com.santander.aseguradora.colectivos.model.dao.RamoContableDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamoContable;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author Dflores
 *
 */
@Service
public class CachedServicioRamoContableImpl implements ServicioRamoContable {

	private Log log = LogFactory.getLog(this.getClass());
	@Resource
	private RamoContableDao ramoContableDao;
	private String message;

	private Map<Integer,Object> ramosContablesCache;
	
	/**
	 * @param ramoDao the ramoDao to set
	 */
	public void setRamoContableDao(RamoContableDao ramoContableDao) {
		this.ramoContableDao = ramoContableDao;
	}

	/**
	 * @return the ramoDao
	 */
	public RamoContableDao getRamoContableDao() {
		return ramoContableDao;
	}



	@SuppressWarnings("unchecked")
	public CachedServicioRamoContableImpl() {
		// TODO Auto-generated constructor stub
		this.ramosContablesCache = Collections.synchronizedMap(new LinkedHashMap());
	}
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioRamo#getListaRamos()
	 */

	public List<Object> getListaRamosContables() throws Excepciones {
		// TODO Auto-generated method stub
		return Utilerias.getValueList(ramosContablesCache);
	}

	public void init() throws Excepciones{
		
		this.log.debug("Inicializando la carga de ramos contables");
		
		
		try {
			this.cargaRamosContables();
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se pueden recuperar los ramos contables.";
			this.log.error(message, e);
			throw new Excepciones(message, e);
		}
	}
	
	/**
	 * 
	 */

	private void cargaRamosContables(){
		
		this.log.debug("Cargando ramos contables...");
		List<RamoContable> ramosContables;//, ramosPolizas;
		
		ramosContables = this.ramoContableDao.getRamosContables();

		//ramosPolizas = this.cargaPolizas(ramos);
		
		for(Object item: ramosContables){
		
			RamoContable ramoContable = (RamoContable) item;
			this.ramosContablesCache.put(new Integer(ramoContable.getCarbCdRamo()+""), ramoContable);
		}
		
	}

	public RamoContable getRamoContable(Integer id) throws Excepciones {
		// TODO Auto-generated method stub
		return (RamoContable) this.ramosContablesCache.get(id);
	}

	//LPV 30/04/2015
	public <T> List<T> getRamosContables() throws Excepciones {
		return Utilerias.getValueList(ramosContablesCache);
	}


	public void afterPropertiesSet() throws Exception {
		cargaRamosContables();
	}

	
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
