package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.ArrayList;
import java.util.Date;


public interface ServicioReportesCierre {
	
	public ArrayList<Object> sacarReportes(int canal, int ramo, String finicio, String ffin, int operacion, int opcion) throws Exception;
}
