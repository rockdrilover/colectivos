/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.dao.TraspasoAptcDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTraspasoAPTC;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author OmarCortes
 *
 */
@Service
public class ServicioTraspasoAPTCImpl implements ServicioTraspasoAPTC {
	
	@Resource
	private TraspasoAptcDao traspasoDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	public TraspasoAptcDao getCargaDao() {
		return traspasoDao;
	}


	public void setCargaDao(TraspasoAptcDao traspasoDao) {
		this.traspasoDao = traspasoDao;
	}


	public ServicioTraspasoAPTCImpl() {
		// TODO Auto-generated constructor stub
		message = new StringBuilder();
	}


	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}


	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		
		this.message.append("Obtener errores traspaso.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			List<T> lista = traspasoDao.obtenerObjetos(filtro);
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede recuperar los errores de trapaso.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}


	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}


	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}


}
