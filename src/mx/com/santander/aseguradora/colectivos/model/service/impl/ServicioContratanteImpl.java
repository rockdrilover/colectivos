/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClientePK;
import mx.com.santander.aseguradora.colectivos.model.dao.ContratanteDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioContratante;

/**
 * @author dflores
 *
 */
@Service
public class ServicioContratanteImpl implements ServicioContratante {

	@Resource
	private ContratanteDao contratanteDao;
	private Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * @return the contratanteDao
	 */
	public ContratanteDao getContratanteDao() {
		return contratanteDao;
	}

	/**
	 * @param contratanteDao the contratanteDao to set
	 */
	public void setContratanteDao(ContratanteDao contratanteDao) {
		this.contratanteDao = contratanteDao;
	}

	public ServicioContratanteImpl() {
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		
		try {
			this.contratanteDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			this.log.error("Error al actualizar el contratante.", e);
			throw new Excepciones("Error al actualizar el contratante.", e);
		}		

	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		
		try {
			T contratante = this.contratanteDao.guardarObjeto(objeto);
			return contratante;
		} catch (ObjetoDuplicado e) {
			throw new ObjetoDuplicado("Contratante duplicado.", e);
		} catch (Exception e) {
			throw new Excepciones("No se puede guardar el contratante.");
		}
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		
		try {
			List<T> lista = this.contratanteDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los contratantes.", e);
			throw new Excepciones("No se pueden recuperar los contratantes.", e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}

	public List<Object[]> consulta2(String strFiltro) throws Exception{
		try {
			return this.contratanteDao.consulta2(strFiltro);		
		}
		catch (Exception e) {
			this.log.error("Error al recuperar los recibos.", e);
			throw new Excepciones ("Error al recuperar los recibos.", e);
		}
	}

	public <T> List<T> consulta(String nacionalidad, String cedula, String razonSocial,String strFiltro) throws Excepciones {
		return this.contratanteDao.consulta(nacionalidad, cedula, razonSocial, strFiltro);
	}
	
	public String guarda(Long next,String idParam,String desParam,String nacionalidad, String cedula,short canal,short ramo,  Integer idVenta, Integer idProducto) throws Excepciones {
		 return this.contratanteDao.guarda(next,idParam, desParam, nacionalidad,  cedula, canal, ramo, idVenta,  idProducto);
	}

	@Override
	public <T> List<T> obtenerObjetos(Integer nTipoConsulta, CartClientePK cartClientePK) throws Excepciones {
		try {
			//Se ejecuta dao y se retorna lista
			return this.contratanteDao.obtenerObjetos(nTipoConsulta, cartClientePK);
		} catch (Excepciones e) {
			//Error al consultar
			throw new Excepciones("Error al recuperar los contratantes.", e);
		}
	}

}
