package mx.com.santander.aseguradora.colectivos.model.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.BitacoraErroresDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Interface que implementa la clase ServicioErrorEmisionImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 14-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Remote
public interface ServicioErrorEmision {

	/**
	 * Metodo que manda consultar los errores
	 * @param nombreLayOut nombre de layout a buscar
	 * @param feDesde fecha desde
	 * @param feHasta fecha hasta
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	List<Object> consulta(String nombreLayOut, Date feDesde, Date feHasta) throws Excepciones;
	
	/**
	 * Metodo que consulta el detalle de errores
	 * @param nombreLayOut nombre del archivo a buscar
	 * @return lista con errores
	 * @throws Excepciones con error general
	 */
	List<Object> getDetalleErrores(String nombreLayOut) throws Excepciones;

	/**
	 * Metodo que emite creditos de Prima Unica
	 * @param nombreArchivo nombre del archivo
	 * @param cdControl id de cifras control
	 * @param lista lista con creditos a emitir
	 * @return mesanje del proceso
	 */
	String emitePU(String nombreArchivo, BigDecimal cdControl, List<BitacoraErroresDTO> lista);

	/**
	 * Metodo que emite creditos de Prima Recurrente
	 * @param nombreArchivo nombre del archivo
	 * @param cdControl id de cifras control
	 * @param lista lista con creditos a emitir
	 * @return mesanje del proceso
	 */
	String emitePR(String nombreArchivo, BigDecimal cdControl, List<BitacoraErroresDTO> lista);

}
