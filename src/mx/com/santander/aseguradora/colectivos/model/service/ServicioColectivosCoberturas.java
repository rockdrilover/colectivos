/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author Leobardo Preciado
 *
 */
public interface ServicioColectivosCoberturas extends ServicioCatalogo{
	
	/**
	 * Actualiza informacion de producto / plan de acuerdo a tipo de comisiones
	 * @param colectivosCoberturas registro modificado
	 */
	void actualizaTipoComision(ColectivosCoberturas  colectivosCoberturas) throws Excepciones;
	/**
	 * Actualiza informacion de producto / plan de acuerdo a tipo de comisiones
	 * @param colectivosCoberturas registro modificado
	 */
	void actualizaParametrizacionTipoComision(Parametros  parametrosConfiguracion) throws Excepciones;

}
