/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;



import mx.com.santander.aseguradora.colectivos.model.dao.AltaProdDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaProducto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author dflores
 *
 */
public class ServicioAltaProdImpl implements ServicioAltaProducto  {

	private Log log = LogFactory.getLog(this.getClass());
	@SuppressWarnings("unchecked")
	private AltaProdDao altaProdDao;
	private String message = null;
	


	@SuppressWarnings("unchecked")
	public void setAltaProdDao(AltaProdDao altaProdDao) { 
		this.altaProdDao = altaProdDao;
	}
	

	@SuppressWarnings("unchecked")
	public AltaProdDao getAltaProdDao() {
		return altaProdDao;
	}

	public Integer iniciaAlta( Short ramo, String descProducto, String descPlan
			) throws Excepciones {
		// TODO Auto-generated method stub
		Integer pro = 0;
		this.log.debug("Iniciando proceso de Alta de producto y plan");
		try {
			
			pro = this.altaProdDao.iniciaAltaProd(ramo, descProducto, descPlan);
			return pro;
			
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
		
	}


 
	

}
