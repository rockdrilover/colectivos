/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author Leobardo Preciado
 *
 */
public interface ServicioColectivosComponentes extends ServicioCatalogo{
	public void respaldaIVA(BigDecimal tarifa) throws Excepciones;
}
