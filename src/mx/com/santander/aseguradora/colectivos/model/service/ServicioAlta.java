/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;




import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ServicioAlta{

	public Long iniciaAlta(int canal, Short ramo,String analista,int sucursal,String domicilio,String cedula_rif,Date fecha_ini) throws Excepciones;
  
}
