
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes.OPC_FORMATO_REPORTE;

/**
 * 
 * @author FEBG
 *
 */
public interface ServicioReporteador {

	public List<Object> consultarRegistros(Short ramo, Long poliza, Integer inIdVenta, Integer estatus, Date fecfin, Date fecini, String strCampos) throws Excepciones;
	public List<Object> consultarRegistrosEspeciales(Integer inIdReporteEspecial, Integer mes, Integer nFiltroFechas, Date fechaIni, Date fechaFin) throws Excepciones;
	public List<Object> consultarRegistrosEspeciales2(Integer inIdReporteEspecial) throws Excepciones;
	public List<Object> consultaIdVenta() throws Excepciones;
	public List<Object> consultaFoto(String idVenta) throws Excepciones;
	public <T> List<T> reporteFiscalEmitidos(Short ramo, Integer idVenta, String strFechaIni, String strFechaFin) throws Excepciones;
	public <T> List<T> reporteFiscalBajas(Short ramo, Integer idVenta, String strFechaIni, String strFechaFin) throws Excepciones;
	public List<Object> consultaCuentas() throws Excepciones;
	public List<Object> reporteCuentas(Integer mes, Integer nFiltroFechas, Date fechaIni, Date fechaFin) throws Excepciones;
	public void reporteIntegracion(String rutaTemporal, String rutaReporte, Map<String, Object> inParams, OPC_FORMATO_REPORTE opcr) throws Exception;
	public List<Object> consultarErrores(Short ramo, Long poliza, Integer inIdVenta, Date fecfin, Date fecini, Integer inTipoError) throws Excepciones;
	
	
	/**
	 * Genera String de consulta de comisiones
	 * @param tipo
	 * @param ramo
	 * @param poliza
	 * @param inIdVenta
	 * @param fecfin
	 * @param fecini
	 * @param strCampos
	 * @return
	 * @throws Excepciones
	 */
	public String consultarRegistrosComisiones(Integer tipo,Short ramo, Long poliza, Integer inIdVenta, String fecfin, String fecini, String strCampos) throws Excepciones;

}
