/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Complementarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ClienteCertifDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Mail;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

/**
 * @author Sergio Plata
 *
 */
@Service
public class ServicioClienteCertifImpl implements ServicioClienteCertif {

	@Resource
	private ProcesosDao procesosDao;
	@Resource
	private ClienteCertifDao clienteCertifDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	/**
	 * @return the clienteCertifDao
	 */
	public ClienteCertifDao getClienteCertifDao() {
		return clienteCertifDao;
	}

	/**
	 * @param clienteCertifDao the clienteCertifDao to set
	 */
	public void setClienteCertifDao(ClienteCertifDao clienteCertifDao) {
		this.clienteCertifDao = clienteCertifDao;
	}

	public ServicioClienteCertifImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		ClienteCertif objObligado;
		try {
			objObligado = (ClienteCertif) ConstruirObjeto.crearBean(ClienteCertif.class, objeto);
			this.clienteCertifDao.actualizarObjeto(objObligado);
		} catch (Exception e) {
			message.append("no se puede actualizar objeto.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		ClienteCertif objObligado;
		try {
			objObligado = (ClienteCertif) ConstruirObjeto.crearBean(ClienteCertif.class, objeto);
			this.clienteCertifDao.borrarObjeto(objObligado);
		} catch (Exception e) {
			message.append("no se puede borrar Objeto.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		ClienteCertif objObligado;
		try {
			objObligado = (ClienteCertif) ConstruirObjeto.crearBean(ClienteCertif.class, objeto);
			this.clienteCertifDao.guardarObjeto(objObligado);
		} catch (Exception e) {
			message.append("no se puede guardar Objeto.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
		return objeto;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.String)
	 */
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerClientesCertif.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			List<T> lista = this.clienteCertifDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los clientes certififados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public <T> List<T> consultaCredCte (String idCertificado, String nombre, String apellidoPaterno, String apellidoMaterno)	throws Excepciones {
		// TODO Auto-generated method stub
		try {
			return this.clienteCertifDao.consultaCredCte(idCertificado, nombre, apellidoPaterno, apellidoMaterno);
		}
		catch (Exception e) {
			this.message.append("Error al recuperar los creditos.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	public <T> List<T> consultaCredCert (String certs)	throws Excepciones {
		// TODO Auto-generated method stub
		try {
			return this.clienteCertifDao.consultaCredCert(certs);
		}
		catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> consultaCredCobs (String credito, Short canal, Short ramo, Long poliza, Long nucerd) throws Excepciones {
		List listaCoberturas;
		List<Object[]> lista2;
		
		try {
			listaCoberturas = new ArrayList<T>();
			lista2 = this.clienteCertifDao.consultaCredCobs(credito, canal, ramo, poliza, nucerd);
			
			for(Object[] cobs: lista2){
				//----------------------------------------------------;
				BeanClienteCertif beanCobs = new BeanClienteCertif();
				beanCobs.setCertificado(new Certificado());
				beanCobs.getCertificado().setId(new CertificadoId());
				beanCobs.setCliente(new Cliente());
				//----------------------------------------------------;
				beanCobs.getCertificado().setCoceNuCredito(cobs[0].toString());
				beanCobs.getCertificado().getId().setCoceCasuCdSucursal(((BigDecimal)cobs[1]).shortValue());
				beanCobs.getCertificado().getId().setCoceCarpCdRamo(((BigDecimal)cobs[2]).shortValue());
				beanCobs.getCertificado().getId().setCoceCapoNuPoliza(((BigDecimal)cobs[3]).longValue());
				beanCobs.getCertificado().getId().setCoceNuCertificado(((BigDecimal)cobs[4]).longValue());
				beanCobs.getCertificado().setCoceCampov1(cobs[5].toString());
				beanCobs.getCertificado().setCoceCampov2(cobs[6].toString());
				beanCobs.getCertificado().setCoceCampon4((BigDecimal)cobs[7]);
				beanCobs.getCertificado().setCoceCampof1((Date) cobs[8]);
				beanCobs.getCertificado().setCoceCampof2((Date) cobs[9]);
				listaCoberturas.add(beanCobs);
			}
		}
		catch (Exception e) {
			this.message.append("Error al recuperar las coberturas.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		
		return listaCoberturas;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> consultaCredAsis (String credito, Short canal, Short ramo, Long poliza, Long nucerd)	throws Excepciones {
		List listaAsistencias;
		List<Object[]> lista3;
		try {
			listaAsistencias = new ArrayList<T>();
			lista3 = this.clienteCertifDao.consultaCredAsis(credito, canal, ramo, poliza, nucerd);
			for(Object[] asis: lista3){
				//----------------------------------------------------;
				BeanClienteCertif beanAsis = new BeanClienteCertif();
				beanAsis.setCertificado(new Certificado());
				beanAsis.getCertificado().setId(new CertificadoId());
				beanAsis.setCliente(new Cliente());
				//----------------------------------------------------;
				beanAsis.getCertificado().setCoceNuCredito(asis[0].toString());
				beanAsis.getCertificado().getId().setCoceCasuCdSucursal(((BigDecimal)asis[1]).shortValue());
				beanAsis.getCertificado().getId().setCoceCarpCdRamo(((BigDecimal)asis[2]).shortValue());
				beanAsis.getCertificado().getId().setCoceCapoNuPoliza(((BigDecimal)asis[3]).longValue());
				beanAsis.getCertificado().getId().setCoceNuCertificado(((BigDecimal)asis[4]).longValue());
				beanAsis.getCertificado().setCoceCampov2(asis[5].toString());
				listaAsistencias.add(beanAsis);
			}
		}
		catch (Exception e) {
			this.message.append("Error al recuperar las asistencias.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		return listaAsistencias;
	}
	
	public Complementarios consultaCredComplementos(String credito, Short canal,
			Short ramo, Long poliza, Long nucerd) throws Excepciones {

		Complementarios compl;
		try {

			compl= this.clienteCertifDao.consultaCredComplementos(credito, canal, ramo, poliza, nucerd);

		}
		catch (Exception e) {
			this.message.append("Error al recuperar las asistencias.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		return compl;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> consultaCredAsegurados (String credito, Short canal, Short ramo, Long poliza, Long nucerd) throws Excepciones {
		List listaAsegurados;
		List<Object[]> lista2;
		
		try {
			listaAsegurados = new ArrayList<T>();
			lista2 = this.clienteCertifDao.consultaCredAsegurados(credito, canal, ramo, poliza, nucerd);
			
			if(lista2.size() > 0){
					for(Object[] aseg: lista2){
						//----------------------------------------------------;
						BeanClienteCertif beanAsegs = new BeanClienteCertif();
						beanAsegs.setCertificado(new Certificado());
						beanAsegs.getCertificado().setId(new CertificadoId());
						beanAsegs.setCliente(new Cliente());
						//----------------------------------------------------;				
						beanAsegs.getCertificado().setCoceNuCredito(aseg[0].toString());
						beanAsegs.getCertificado().getId().setCoceCasuCdSucursal(((BigDecimal)aseg[1]).shortValue());
						beanAsegs.getCertificado().getId().setCoceCarpCdRamo(((BigDecimal)aseg[2]).shortValue());
						beanAsegs.getCertificado().getId().setCoceCapoNuPoliza(((BigDecimal)aseg[3]).longValue());
						beanAsegs.getCertificado().getId().setCoceNuCertificado(((BigDecimal)aseg[4]).longValue());
						beanAsegs.getCliente().setCocnNombre(aseg[5].toString());
						beanAsegs.getCliente().setCocnApellidoPat(aseg[6].toString());
						beanAsegs.getCliente().setCocnApellidoMat(aseg[7].toString());
						beanAsegs.getCliente().setCocnFeNacimiento((Date)aseg[8]);
						beanAsegs.getCliente().setCocnRfc(aseg[9].toString());
						beanAsegs.getCliente().setCocnCdSexo(aseg[10].toString());
						beanAsegs.getCliente().setCocnCampov1(aseg[11].toString());
						
						listaAsegurados.add(beanAsegs);
					}
		   }
		}
		catch (Exception e) {
			this.message.append("Error al recuperar las coberturas.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		
		return listaAsegurados;
	}
	
	public String generaCertificado(String rutaPlatillas, Short canal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan, Date fechaEmision, String strDestino, String strNomrePlantilla, Long polizaReal) throws Exception {
		String strResultado = "1";
		Object[] arrDatosRecas;
		PdfReader pdfReader;
		PdfStamper pdfStamper;
		AcroFields fields;
		HashMap<String, AcroFields> hmFields;
		HashMap<String, String> hmValores;
		Iterator itHash;
		List<Object> datosBase;
		BeanParametros param;
		StringBuilder  filtro;
		ArrayList<Object> arlCoberturas, arlBeneficiarios;
		Integer nBeneficiarios;
		
		try {
			datosBase = new ArrayList<Object>();
			filtro = new StringBuilder();
			arlBeneficiarios = new ArrayList<Object>();
			arlCoberturas = new ArrayList<Object>();
			
			arrDatosRecas = clienteCertifDao.getDatosRecas(ramo, producto.toString());
			if(strNomrePlantilla.equals(Constantes.DEFAULT_STRING)) {
				strResultado = Constantes.DEFAULT_STRING_ID;
			} else {
				nBeneficiarios = Constantes.DEFAULT_INT;
				List<Object[]> campos = clienteCertifDao.consultaDatos(canal, ramo, polizaReal, certificado, producto, plan);
				
				for (Object[] cargar : campos) {
					param = new BeanParametros();
					param.setCopaVvalor1(cargar[0].toString()); //Campo
					param.setAuxDato(cargar[1].toString()); // tablas
					param.setCopaNvalor1(((BigDecimal) cargar[2]).intValue()); //nTabla1
					param.setCopaVvalor2(cargar[3].toString()); // split1
					param.setCopaNvalor2(((BigDecimal) cargar[4]).longValue()); //nTabla2
					param.setCopaVvalor3(cargar[5].toString()); // split2
					param.setCopaNvalor3(((BigDecimal) cargar[6]).longValue()); //nTabla3
					param.setCopaVvalor4(cargar[7].toString()); // split3
					param.setCopaVvalor7(cargar[8].toString()); // nombreCampo
					param.setCopaVvalor6(cargar[9].toString()); // NombreFormulario
					nBeneficiarios = nBeneficiarios + ((BigDecimal) cargar[2]).intValue();
					datosBase.add(param);
				}

				if(ramo.intValue() == 57 || ramo.intValue() == 58) {
					arlCoberturas = clienteCertifDao.getCoberturas(canal, ramo, polizaReal, certificado, producto, plan);
				}

				if(nBeneficiarios > 0) {
					arlBeneficiarios = clienteCertifDao.getBeneficiarios(canal, ramo, poliza, certificado);
				}
				
				if(!datosBase.isEmpty()){
					filtro = generaSelect(datosBase,canal, ramo, poliza, certificado);
					hmValores = crearHashMap(clienteCertifDao.getDatosPlantilla(filtro), datosBase, arrDatosRecas, arlCoberturas, arlBeneficiarios);
					pdfReader = new PdfReader(rutaPlatillas + strNomrePlantilla);
					pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(strDestino));
					
					fields = pdfStamper.getAcroFields(); //Se obtienen los campos del formulario
					hmFields = fields.getFields();
					itHash = hmFields.entrySet().iterator();
					
					while(itHash.hasNext()) {
						Map.Entry<String, AcroFields> e = (Entry<String, AcroFields>) itHash.next();
						System.out.println(e.getKey());
						fields.setField(e.getKey(), hmValores.get(e.getKey()));
					}
					
					pdfStamper.close();
					pdfReader.close();
				} else {
					strResultado = Constantes.DEFAULT_STRING_ID;
				}
			}
		} catch (Exception e) {			
			throw new Exception("ERROR: ServicioClienteCertif.generaCertificado() " + e.getMessage());
		}
		
		return strResultado;
	}

	private HashMap<String, String> crearHashMap(ArrayList<Object> lstDatosPlantilla, List<Object> datosBase, Object[] arrDatosRecas, ArrayList<Object> arlCoberturas, ArrayList<Object> arlBeneficiarios) throws Exception {
		HashMap<String, String> hmResultado;
		Object[] arratos;
		BeanParametros objConf;
		Iterator<Object> it;
		int count = 0;
		
		try {
			hmResultado = new HashMap<String, String>();
			if(!lstDatosPlantilla.isEmpty()) {
				arratos = (Object[]) lstDatosPlantilla.get(0);
				it = datosBase.iterator();
				while (it.hasNext()) {
					objConf = (BeanParametros) it.next();
					hmResultado.put(objConf.getCopaVvalor6(), arratos[count] == null ? Constantes.DEFAULT_STRING : arratos[count].toString());
					count++;
				}
				
				hmResultado.put("lugaryfechadeemision", GestorFechas.formatDate(new Date(), "'Mexico D.F, a' dd 'de' MMMM 'del' yyyy"));
				hmResultado.put("cumplimiento", arrDatosRecas[2].toString());
				hmResultado.put("fechacum", GestorFechas.formatDate((Date)arrDatosRecas[3], "'a partir del dia' dd 'de' MMMM 'de' yyyy,"));
				hmResultado.put("ppizquierdo", arrDatosRecas[1].toString());
				hmResultado.put("ppderecho", arrDatosRecas[0].toString());
				hmResultado.put("moneda", "M.N.");
				
				
				count = 1;
				it = arlCoberturas.iterator();
				while(it.hasNext()){
					arratos = (Object[]) it.next();
					hmResultado.put("cobertura" + count, arratos[0].toString());
					hmResultado.put("regla" + count, Utilerias.customFormat("$###,###.###", new Double(arratos[1] == null ? Constantes.DEFAULT_STRING_ID : arratos[1].toString())));
					count++;
				}
				
				count = 1;
				it = arlBeneficiarios.iterator();
				while(it.hasNext()){
					arratos = (Object[]) it.next();
					hmResultado.put("nombrebenef" + count, arratos[0] == null ? Constantes.DEFAULT_STRING : arratos[0].toString());
					hmResultado.put("fechanacbenef" + count, arratos[1] == null ? Constantes.DEFAULT_STRING : GestorFechas.formatDate((Date) arratos[1], "dd/MM/yyyy"));
					hmResultado.put("parentescobenef" + count, arratos[2] == null ? Constantes.DEFAULT_STRING : arratos[2].toString());
					hmResultado.put("porcentajebenef" + count, arratos[3] == null ? Constantes.DEFAULT_STRING : arratos[3].toString() + "%");
					count++;
				}
				
			}
		} catch (Exception e) {
			throw new Exception("ERROR: ServicioClienteCertif.crearHashMap() " + e.getMessage());
		}
		
		return hmResultado;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.DisposableBean#destroy()
	 */
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}
	
    public String impresionCertificado(String rutaTemporal, short canal, short ramo, long poliza, long certificado, Date fecha_emision){
		// TODO Auto-generated method stub
		return clienteCertifDao.impresionCertificado(rutaTemporal, canal, ramo, poliza, certificado, fecha_emision);
	}
	
    public Parametros getRutaPlantilla() throws Exception{    	
    	return clienteCertifDao.getRutaPlantilla();
    }
    
    public String impresionPoliza(String rutaTemporal, short canal, short ramo, long poliza, Long certificado, Integer producto, Date fecha_emision) {
		// TODO Auto-generated method stub
		return clienteCertifDao.impresionPoliza(rutaTemporal, canal, ramo, poliza, certificado, producto, fecha_emision);
	}
    
    private StringBuilder generaSelect(List<Object> lstConfiguracion, Short canal, Short ramo, Long poliza, Long certificado) throws Exception {
    	StringBuilder  sbCampos, sbInners, sbSelectGeneral = null;
		BeanParametros objConf;
		Iterator<Object> it;
		int nContador = 1;
		String[] numTabla;
		List<Integer> tabla;
		
		try {
			sbCampos = new StringBuilder();
			sbInners = new StringBuilder();
			sbSelectGeneral = new StringBuilder();
			tabla = new ArrayList<Integer>();
			
			tabla.add(0);
			it = lstConfiguracion.iterator();
			while (it.hasNext()) {
				objConf = (BeanParametros) it.next();
				
				if(lstConfiguracion.size() == nContador){
					sbCampos.append(objConf.getCopaVvalor1().trim()).append(" \n");
				} else {
					sbCampos.append(objConf.getCopaVvalor1().trim()).append(", \n");
				}
				
				
				if(!tabla.contains(objConf.getCopaNvalor1())){
					if(objConf.getCopaVvalor2().equals("N")){
						sbInners.append(objConf.getAuxDato()).append(" \n");
					} else {
						numTabla = objConf.getAuxDato().split("\\|");
						sbInners.append(numTabla[Integer.parseInt(objConf.getCopaVvalor2())]).append(" \n");
					}
					tabla.add(objConf.getCopaNvalor1());
				}
				
				if(!tabla.contains(objConf.getCopaNvalor2().intValue())){
					if(objConf.getCopaVvalor3().equals("N")){
						sbInners.append(objConf.getAuxDato()).append(" \n");
					} else {
						numTabla = objConf.getAuxDato().split("\\|");
						sbInners.append(numTabla[Integer.parseInt(objConf.getCopaVvalor3())]).append(" \n");
					}
					tabla.add(objConf.getCopaNvalor2().intValue());
				}
				
				if(!tabla.contains(objConf.getCopaNvalor3().intValue())){
					if(objConf.getCopaVvalor4().equals("N")){
						sbInners.append(objConf.getAuxDato()).append(" \n");
					} else {
						numTabla = objConf.getAuxDato().split("\\|");
						sbInners.append(numTabla[Integer.parseInt(objConf.getCopaVvalor4())]).append(" \n");
					}
					tabla.add(objConf.getCopaNvalor3().intValue());
				}
				
				nContador ++;
			}
			
			sbSelectGeneral.append("SELECT DISTINCT \n");
			sbSelectGeneral.append(sbCampos);
			sbSelectGeneral.append("	FROM  \n");
			sbSelectGeneral.append(sbInners);
			sbSelectGeneral.append("WHERE CC.COCE_CASU_CD_SUCURSAL = ").append(canal).append(" \n");
			sbSelectGeneral.append("	AND CC.COCE_CARP_CD_RAMO   = ").append(ramo).append(" \n");
			sbSelectGeneral.append("	AND CC.COCE_CAPO_NU_POLIZA = ").append(poliza).append(" \n");
			sbSelectGeneral.append("	AND CC.COCE_NU_CERTIFICADO = ").append(certificado).append(" \n");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ERROR: TxProceso.generaSelect(), " + e.getMessage());
		}
		return sbSelectGeneral;
	}

	@Override
	public String getPlantilla(Short sucursal, Short ramo, Long poliza, Long certificado, Integer producto, Integer plan,  Date fechaEmision) throws Exception {
		return clienteCertifDao.getPlantilla(sucursal, ramo, poliza, certificado, producto, plan, fechaEmision);
	}

	@Override
	public String getPlazo(Short coceCarpCdRamo, Integer alprCdProducto, Integer alplCdPlan) {
		try {
			return clienteCertifDao.getPlazo(coceCarpCdRamo, alprCdProducto, alplCdPlan);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void actualizaTitular(CertificadoId id) throws Exception {
		try {
			this.clienteCertifDao.actualizaTitular(id);
		} catch (Exception e) {
			this.log.error("Error al actualizar Titular.", e);
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public List<EndosoDatosDTO> getEndosos(Short canal, Short ramo, Long poliza, Long nucerd) throws Exception {
		ArrayList<EndosoDatosDTO> arlResultado;
		List<Object> lstDatos;
		Iterator<Object> it;
		EndosoDatosDTO objEndoso;
		StringBuilder filtro;
		
		try {
			arlResultado = new ArrayList<EndosoDatosDTO>();
			filtro = new StringBuilder();
			
			filtro.append(" AND CE.id.cedaCasuCdSucursal = ").append(canal);
			filtro.append(" AND CE.id.cedaCarpCdRamo = ").append(ramo);
			filtro.append(" AND CE.id.cedaCapoNuPoliza = ").append(poliza);
			filtro.append(" AND CE.id.cedaNuCertificado = ").append(nucerd);
			filtro.append(" AND CE.id.cedaCdEndoso = ").append(Constantes.ENDOSO_CD_DATOS);
			
			lstDatos = this.clienteCertifDao.getEndosos(filtro.toString());
			it = lstDatos.iterator();
			while (it.hasNext()) {
				objEndoso = (EndosoDatosDTO) ConstruirObjeto.crearObjeto(EndosoDatosDTO.class, it.next());
				arlResultado.add(objEndoso);
			}
			
			return arlResultado;
		} catch (Exception e) {
			this.message.append("Error al obtener endosos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
	}

	@Override
	public void notificarCambio(ClienteCertifId id, Short tipoCliente, Integer tipoCambio) throws Exception {
		StringBuilder sbSelectGeneral = null;
		ArrayList<Object> arlDatos;
		Object[] arrDatos;
		
		try {
			sbSelectGeneral = new StringBuilder();
			arlDatos = new ArrayList<Object>();
			
			sbSelectGeneral.append("SELECT CS.SICE_CACE_CASU_CD_SUCURSAL, CS.SICE_CACE_CARP_CD_RAMO, CS.SICE_CACE_CAPO_NU_POLIZA, CS.SICE_CACE_NU_CERTIFICADO, S.SISI_CAAN_CD_ANALISTA, U.CAUS_NM_USUARIO, U.CAUS_MAIL1, U.CAUS_MAIL2 \n");
			sbSelectGeneral.append("	FROM SINT_CERTIFICADOS_SINIESTROS CS \n");
			sbSelectGeneral.append("	INNER JOIN SINT_SINIESTROS S ON(S.SISI_CASU_CD_SUCURSAL = CS.SICE_SISI_CASU_CD_SUCURSAL AND S.SISI_NU_SINIESTRO = CS.SICE_SISI_NU_SINIESTRO)  \n");
			sbSelectGeneral.append("	INNER JOIN CART_USUARIOS U ON(U.CAUS_CD_USUARIO = S.SISI_CAAN_CD_ANALISTA )");
			sbSelectGeneral.append("WHERE CS.SICE_CACE_CASU_CD_SUCURSAL = ").append(id.getCoccCasuCdSucursal()).append(" \n");
			sbSelectGeneral.append("	AND CS.SICE_CACE_CARP_CD_RAMO   = ").append(id.getCoccCarpCdRamo()).append(" \n");
			sbSelectGeneral.append("	AND CS.SICE_CACE_CAPO_NU_POLIZA = ").append(id.getCoccCapoNuPoliza()).append(" \n");
			sbSelectGeneral.append("	and CS.SICE_CACE_NU_CERTIFICADO = ").append(id.getCoccNuCertificado()).append(" \n");
			
			arlDatos = this.clienteCertifDao.getDatosPlantilla(sbSelectGeneral);
			
			if(!arlDatos.isEmpty()) {
				arrDatos = (Object[]) arlDatos.get(0);
				
				if(arrDatos[6] == null || arrDatos[6].toString().equals(Constantes.DEFAULT_STRING)) {
					this.log.info("Analista no tiene correo registrado.");
				} else {
					enviarCorreoNotificacion(id, tipoCliente, tipoCambio, arrDatos[5].toString(), arrDatos[6].toString());
				}
			}
         
		} catch (Exception e) {			
			this.message.append("Error al enviar correo.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
		
	}
	
	/**
	 * Metodo que envia el correo al analista
	 * @param id
	 * @param tipoCliente
	 * @param tipoCambio 
	 * @param strAnalista 
	 */
	private void enviarCorreoNotificacion(ClienteCertifId id, Short tipoCliente, Integer tipoCambio, String strAnalista, String strCorreo) {
		StringBuilder sbMsgCorreo;
		
		try {
			sbMsgCorreo = new StringBuilder();
			sbMsgCorreo.append("Buen Dia. \n\n\n");
			sbMsgCorreo.append("Estimado, " + strAnalista).append("\n");
			sbMsgCorreo.append("Se le notifica que para el certificado: " + id.getCoccCasuCdSucursal() + "-" + id.getCoccCarpCdRamo() + "-" + id.getCoccCapoNuPoliza() + "-" + id.getCoccNuCertificado() + " \n");
			
			if(tipoCambio == 0) {
				sbMsgCorreo.append("se realizo cambio de Obligado por Titular en el Sistema de Colectivos. \n\n\n");
				this.procesosDao.migraColectivos(id, tipoCliente, new Date(), 2);
			} else {
				sbMsgCorreo.append("se asocio un obligado en el Sistema de Colectivos. \n\n\n");
			}
			
			Mail.enviar2(new ArrayList<String>(), sbMsgCorreo, "Cambios en Obligados y Titular", strCorreo, Constantes.DEFAULT_STRING, Constantes.DEFAULT_STRING);
		} catch (Exception e) {
			this.log.error("Error al migraColectivos.", e);
		}
	}

	public ProcesosDao getProcesosDao() {
		return procesosDao;
	}
	public void setProcesosDao(ProcesosDao procesosDao) {
		this.procesosDao = procesosDao;
	}

}
