/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;


import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author Leobardo Preciado
 * 
 */
public interface ServicioCiudad extends ServicioCatalogo{
	
	public <T> List<T> obtenerObjetos()throws Excepciones;
	public <T> List<T> obtenerObjetos(String filtro)throws Excepciones;
	
}


