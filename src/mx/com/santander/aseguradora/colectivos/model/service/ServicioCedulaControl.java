package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.CedulaControlDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Interface que implementa la clase ServicioCedulaControlImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Remote
public interface ServicioCedulaControl {

	/**
	 * Metodo que manda consultar los errores
	 * @param tipocedula tipo cedula a buscar
	 * @param idVenta ide de venta
	 * @param feDesde fecha desde
	 * @param feHasta fecha hasta
	 * @param remesa remesa para consultar detalle
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	List<CedulaControlDTO> consulta(Integer tipocedula, Integer idVenta, Date feDesde, Date feHasta, String remesa) throws Excepciones;

	/**
	 * Metodo que consulta el detalle de creditos para descargar.
	 * @param tipocedula tipo de cedula (Emision o Cancelacion)
	 * @param idVenta numero de id venta
	 * @param coccRemesaCedula Remesa a consulta
	 * @return lista de datos
	 * @throws Excepciones con error general
	 */
	List<Object> getDetalleCreditos(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones;
	
	
	/**
	 * Metodo que guarda foto de una cedula control
	 * @param tipocedula tipo de cedula (Emision o Cancelacion)
	 * @param idVenta numero de id venta
	 * @param coccRemesaCedula Remesa a consulta
	 * @throws Excepciones con error general
	 */
	void saveFotoCedula(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones;

}
