package mx.com.santander.aseguradora.colectivos.model.service.impl;

import mx.com.santander.aseguradora.colectivos.model.dao.HistorialDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHistorialCredito;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanDetalleComponentes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaCoberturaDetalle;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaCreditoDetalle;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaRecibosHistorico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

@Service
public class ServicioHistorialCreditoImpl  implements ServicioHistorialCredito{
	
	@Resource
	private HistorialDao historialDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message = null;
	private List<BeanListaCreditoDetalle> listaHistorial;
	
	public List<BeanListaCreditoDetalle> getListaHistorial() {
		return listaHistorial;
	}
	public void setListaHistorial(List<BeanListaCreditoDetalle> listaHistorial) {
		this.listaHistorial = listaHistorial;
	}
	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}
		
	public HistorialDao getHistorialClienteDao() {
		return historialDao;
	}

	public void setHistorialClienteDao(HistorialDao historialClienteDao) {
		this.historialDao = historialClienteDao;
	}

	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() throws Exception {
	}

	public ServicioHistorialCreditoImpl(){
		this.listaHistorial = new ArrayList<BeanListaCreditoDetalle>();
	}
	@Override
	public  List<BeanListaCreditoDetalle> consultaHistorial(short ramo, Long poliza, String certificado,String credito ,String nombre, String paterno, String materno) throws Excepciones {
		BeanListaCreditoDetalle beanListaCertificado;
		try {
			this.listaHistorial.clear();
			List<Object[]> listaHistorialCredito = this.historialDao.consultaHistorial(ramo, poliza, certificado, credito,nombre, paterno, materno);
			
			
			for(Object[] historial: listaHistorialCredito){
				beanListaCertificado = new BeanListaCreditoDetalle();
				//CONTRATADO
				if(historial[0] == null )beanListaCertificado.setEncabe("");
				 else  beanListaCertificado.setEncabe(historial[0].toString());
				
				if(historial[1] == null )beanListaCertificado.setNom_cont("");
				 else  beanListaCertificado.setNom_cont(historial[1].toString());
				
				if(historial[2] == null )beanListaCertificado.setRfc_cont("");
				else beanListaCertificado.setRfc_cont(historial[2].toString());
				
				if(historial[3] == null )beanListaCertificado.setTel_cont("");
				else beanListaCertificado.setTel_cont(historial[3].toString());
				
				if(historial[4] == null )beanListaCertificado.setDir_cont("");
				 else  beanListaCertificado.setDir_cont(historial[4].toString());
				
				if(historial[5] == null )beanListaCertificado.setCol_cont("");
				 else  beanListaCertificado.setCol_cont(historial[5].toString());
				
				if(historial[6] == null )beanListaCertificado.setDel_cont("");
				 else  beanListaCertificado.setDel_cont(historial[6].toString());

				if(historial[7] == null )beanListaCertificado.setEdo_cont("");
				 else  beanListaCertificado.setEdo_cont(historial[7].toString());
				
				if(historial[8] == null )beanListaCertificado.setCp_cont("");
				 else  beanListaCertificado.setCp_cont(historial[8].toString());
				//ASEGURADO
				if(historial[9] == null )beanListaCertificado.setEncabe("");
				 else  beanListaCertificado.setEncabe(historial[9].toString());
				
				if(historial[10] == null )beanListaCertificado.setNombre_aseg("");
				 else  beanListaCertificado.setNombre_aseg(historial[10].toString());
				
				if(historial[11] == null )beanListaCertificado.setBuc_cliente("");
				else beanListaCertificado.setBuc_cliente(historial[11].toString());
				
				if(historial[12] == null )beanListaCertificado.setNu_cliente("");
				 else  beanListaCertificado.setNu_cliente(historial[12].toString());
				
				if(historial[13] == null )beanListaCertificado.setCd_sexo("");
				 else   beanListaCertificado.setCd_sexo(historial[13].toString());
				
				if(historial[14] == null )beanListaCertificado.setRfc_aseg("");
				 else   beanListaCertificado.setRfc_aseg(historial[14].toString());
				
				if(historial[15] == null )beanListaCertificado.setCurp_aseg("");
				 else   beanListaCertificado.setCurp_aseg(historial[15].toString());
			 							
				if(historial[16] == null )beanListaCertificado.setFena_aseg("");
				 else  beanListaCertificado.setFena_aseg(GestorFechas.formatDate((Date)historial[16], "dd/MM/yyyy"));
				
				if(historial[17] == null )beanListaCertificado.setDom_aseg("");
				 else   beanListaCertificado.setDom_aseg(historial[17].toString());

				if(historial[18] == null )beanListaCertificado.setCol_aseg("");
				 else  beanListaCertificado.setCol_aseg(historial[18].toString());

				if(historial[19] == null )beanListaCertificado.setDel_aseg("");
				 else  beanListaCertificado.setDel_aseg(historial[19].toString());

				if(historial[20] == null )beanListaCertificado.setEdo_aseg("");
				 else  beanListaCertificado.setEdo_aseg(historial[20].toString());

				if(historial[21] == null )beanListaCertificado.setCp_aseg("");
				 else  beanListaCertificado.setCp_aseg(historial[21].toString());
				
				if(historial[22] == null )beanListaCertificado.setTel_aseg("");
				 else  beanListaCertificado.setTel_aseg(historial[22].toString());				

				if(historial[23] == null )beanListaCertificado.setId_estatus("");
				 else  beanListaCertificado.setId_estatus(historial[23].toString());
				
				if(historial[24] == null )beanListaCertificado.setEstat_tec("");
				 else  beanListaCertificado.setEstat_tec(historial[24].toString());
				
				if(historial[25] == null )beanListaCertificado.setFecha_anu("");
				 else  beanListaCertificado.setFecha_anu(GestorFechas.formatDate((Date)historial[25], "dd/MM/yyyy"));
				
				if(historial[27] == null )beanListaCertificado.setPlan("");
				 else  beanListaCertificado.setPlan(historial[27].toString().substring(0));
				//comprobado
				if(historial[26] == null )beanListaCertificado.setProducto("");
				 else  beanListaCertificado.setProducto(historial[26].toString().substring(0));
								
				if(historial[28] == null )beanListaCertificado.setFec_carga("");
				 else  beanListaCertificado.setFec_carga(GestorFechas.formatDate((Date)historial[28], "dd/MM/yyyy"));
				
				if(historial[29] == null )beanListaCertificado.setFechaingreso(null);				
					else beanListaCertificado.setFechaingreso(GestorFechas.formatDate((Date)historial[29], "dd/MM/yyyy") );
				
				if(historial[30] == null )beanListaCertificado.setFec_emision(null);				
					else beanListaCertificado.setFec_emision((Date)historial[30]);
			
				
				if(historial[31] == null )beanListaCertificado.setSuma_aseg("");
				 else  beanListaCertificado.setSuma_aseg(historial[31].toString());
				
				if(historial[32] == null )beanListaCertificado.setSaldo_base("");
				 else  beanListaCertificado.setSaldo_base(historial[32].toString());
				
				if(historial[33] == null )beanListaCertificado.setPrima_total("");
				 else  beanListaCertificado.setPrima_total(historial[33].toString());
				
				if(historial[34] == null )beanListaCertificado.setPmaneta("");
				 else  beanListaCertificado.setPmaneta(historial[34].toString());
				
				if(historial[35] == null )beanListaCertificado.setFeinivigpol("");
				 else  beanListaCertificado.setFeinivigpol(GestorFechas.formatDate((Date)historial[35], "dd/MM/yyyy"));
				
				if(historial[36] == null )beanListaCertificado.setFefinvigpol("");
				 else  beanListaCertificado.setFefinvigpol( GestorFechas.formatDate((Date)historial[36], "dd/MM/yyyy"));
				
				if(historial[37] == null )beanListaCertificado.setForma_pago("");
				 else  beanListaCertificado.setForma_pago(historial[37].toString());
				
				if(historial[38] == null )beanListaCertificado.setFeinivigcer("");
				 else  beanListaCertificado.setFeinivigcer(GestorFechas.formatDate((Date)historial[38], "dd/MM/yyyy"));
				
				if(historial[39] == null )beanListaCertificado.setFefinvigcer("");
				 else  beanListaCertificado.setFefinvigcer(GestorFechas.formatDate((Date)historial[39], "dd/MM/yyyy"));
				
				if(historial[40] == null )beanListaCertificado.setFefincre("");
				 else  beanListaCertificado.setFefincre(GestorFechas.formatDate((Date)historial[40], "dd/MM/yyyy"));
				
				if(historial[42] == null )beanListaCertificado.setRamo("");
				 else  beanListaCertificado.setRamo(historial[42].toString());
				
				if(historial[43] == null )beanListaCertificado.setCertificado("");
				 else  beanListaCertificado.setCertificado(historial[43].toString());				
						
				if(historial[44] == null )beanListaCertificado.setPoliza("");
				 else  beanListaCertificado.setPoliza(historial[44].toString());
				
				if(historial[45] == null )beanListaCertificado.setCredito("");
				 else  beanListaCertificado.setCredito(historial[45].toString());
				

				
				if(historial[47] == null )beanListaCertificado.setCd_sucursal("");
				 else  beanListaCertificado.setCd_sucursal(historial[47].toString());
				
				if(historial[48] == null )beanListaCertificado.setMotivo_anulacion("");
				 else  beanListaCertificado.setMotivo_anulacion(historial[48].toString());
				
				if(historial[49] == null )beanListaCertificado.setRamo_nom("");
				 else  beanListaCertificado.setRamo_nom(historial[49].toString());
				
				
				//CONSULTA COBERTURAS
				beanListaCertificado.setLstCoberturas(consultaHistorialCobertura(Short.parseShort(beanListaCertificado.getRamo()), Long.parseLong(beanListaCertificado.getPoliza()), beanListaCertificado.getCertificado(), Integer.parseInt(beanListaCertificado.getProducto().split("\\-")[0].trim()), Integer.parseInt(beanListaCertificado.getPlan().split("\\-")[0].trim()), Integer.parseInt(historial[46].toString())));

				//CONSULTA COMPONENTES
				beanListaCertificado.setLstComponentes(consultaDetalleComponentes(Short.parseShort(beanListaCertificado.getRamo()), Long.parseLong(beanListaCertificado.getPoliza()), beanListaCertificado.getCertificado(), Integer.parseInt(beanListaCertificado.getProducto().split("\\-")[0].trim()), Integer.parseInt(beanListaCertificado.getPlan().split("\\-")[0].trim()), Integer.parseInt(historial[46].toString())));
				
				//CONSULTA RECIBOS
				beanListaCertificado.setLstrecibos(consultaHistorialRecibos(Short.parseShort(beanListaCertificado.getRamo()),Long.parseLong(beanListaCertificado.getPoliza()),beanListaCertificado.getCertificado()));
				
				
				this.listaHistorial.add(beanListaCertificado);
			}
			
			return  listaHistorial;
		}
		catch (Exception e) {
			e.printStackTrace();
			this.message.append("Error al recuperar el historial de credito.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	@Override
	public  List<BeanListaCoberturaDetalle> consultaHistorialCobertura(short ramo, Long poliza, String certificado, Integer producto, Integer plan, Integer idVenta) throws Excepciones{
		List<BeanListaCoberturaDetalle> listaHistorialCobertura;
		BeanListaCoberturaDetalle beanListaCoberturas;
		Long nPoliza;
		try {
			listaHistorialCobertura = new ArrayList<BeanListaCoberturaDetalle>();
			if(idVenta > 0) {
				nPoliza = new Long(poliza.toString().substring(0, 5));
			} else {
				if(ramo == 57 || ramo == 58) {
					nPoliza = new Long(poliza.toString().substring(0, 2));
				} else {
					nPoliza = poliza;
				}
			}
			List<Object[]> listaHistorialCober = this.historialDao.consultaHistorialCobertura(ramo, nPoliza, certificado, producto, plan);
			
			for(Object[] historial: listaHistorialCober){
				beanListaCoberturas = new BeanListaCoberturaDetalle();
				
				if(historial[0] == null )beanListaCoberturas.setRamo_cont("");
				 else  beanListaCoberturas.setRamo_cont(historial[0].toString());
				
				if(historial[1] == null )beanListaCoberturas.setCober("");
				 else  beanListaCoberturas.setCober(historial[1].toString());
				
				if(historial[2] == null )beanListaCoberturas.setCober_desc("");
				 else  beanListaCoberturas.setCober_desc(historial[2].toString());
				
				if(historial[3] == null )beanListaCoberturas.setTarif("");
				 else  beanListaCoberturas.setTarif(historial[3].toString());

				listaHistorialCobertura.add(beanListaCoberturas);

			}
			
			return  listaHistorialCobertura;
		} catch (Exception e) {
			this.message.append("Error al recuperar la cobertura.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	@Override
	public  List<BeanListaRecibosHistorico> consultaHistorialRecibos(short ramo, Long poliza, String certificado) throws Excepciones{
		List<BeanListaRecibosHistorico> listaHistorialRec;
		BeanListaRecibosHistorico beanListaRecibos;
		List<Object[]> listaHistorialReci;
		
		try {
			listaHistorialRec = new ArrayList<BeanListaRecibosHistorico>();
			if(ramo==57 || ramo==58) {
				listaHistorialReci = this.historialDao.consultaHistorialRecibosRamo57_58(ramo, poliza, Constantes.DEFAULT_STRING_ID);
			} else {
				listaHistorialReci = this.historialDao.consultaHistorialRecibos(ramo, poliza, Constantes.DEFAULT_STRING_ID);
			}
			
			for(Object[] historial: listaHistorialReci) {
				beanListaRecibos = new BeanListaRecibosHistorico();
				
				if(historial[0] == null )beanListaRecibos.setConsecutivo("");
				 else  beanListaRecibos.setConsecutivo(historial[0].toString());
				
				if(historial[1] == null )beanListaRecibos.setRecibo("");
				 else  beanListaRecibos.setRecibo(historial[1].toString());
				
				if(historial[2] == null )beanListaRecibos.setMonto("");
				 else  beanListaRecibos.setMonto(historial[2].toString());
				
				if(historial[3] == null )beanListaRecibos.setEstatus_rec("");
				 else  beanListaRecibos.setEstatus_rec(historial[3].toString());
				
				if(historial[4] == null )beanListaRecibos.setFech_des("");
				 else  beanListaRecibos.setFech_des(GestorFechas.formatDate((Date)historial[4], "dd/MM/yyyy"));
				
				if(historial[5] == null )beanListaRecibos.setFech_has("");
				 else  beanListaRecibos.setFech_has(GestorFechas.formatDate((Date)historial[5], "dd/MM/yyyy"));

				listaHistorialRec.add(beanListaRecibos);
			}			
			
			return  listaHistorialRec;
		} catch (Exception e) {
			this.message.append("Error al recuperar el recibo.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	@Override
	public List<BeanDetalleComponentes> consultaDetalleComponentes(short ramo,Long poliza, String certificado, Integer producto, Integer plan,Integer idVenta) throws Excepciones {
	
			List<BeanDetalleComponentes> listaHistorialCompo;
			BeanDetalleComponentes beanListaCompo;
			Long nPoliza;
			
			try {
				listaHistorialCompo = new ArrayList<BeanDetalleComponentes>();
				if(idVenta > 0) {
					nPoliza = new Long(poliza.toString().substring(0, 5));
				} else {
					if(ramo == 57 || ramo == 58) {
						nPoliza = new Long(poliza.toString().substring(0, 2));
					} else {
						nPoliza = poliza;
					}
				}
				List<Object[]> listaDetalleCom = this.historialDao.consultaDetalleComponente(ramo, nPoliza, certificado, producto, plan);
				
				
				for(Object[] historial: listaDetalleCom){
					 beanListaCompo = new BeanDetalleComponentes();
					
					if(historial[0] == null )beanListaCompo.setCompo("");
					 else  beanListaCompo.setCompo(historial[0].toString());
					
					if(historial[1] == null )beanListaCompo.setCompo_desc("");
					 else  beanListaCompo.setCompo_desc(beanListaCompo.getCompo());
					
					if(historial[2] == null )beanListaCompo.setTarif("");
					 else  beanListaCompo.setTarif(historial[2].toString());
					
					if(historial[3] == null )beanListaCompo.setMonto_compo("");
					 else  beanListaCompo.setMonto_compo(historial[3].toString());

					listaHistorialCompo.add(beanListaCompo);

				}
				
				return  listaHistorialCompo;
			} catch (Exception e) {
					this.message.append("Error al recuperar componentes.");
					this.log.error(message.toString(), e);
					throw new Excepciones (message.toString(), e);
			}

		}
	}

	

