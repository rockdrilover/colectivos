/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author Sergio Plata
 *
 */
public interface ServicioPoliza extends ServicioCatalogo{
	public <T> List<T> consultaPolizasRenovacion() throws Excepciones;
	public String procesoRenovacionPolizas(Short ramo, long poliza) throws Excepciones, Excepciones;
	public <T> List<T> consultaPolizasRenovadas() throws Excepciones;
	public <T> List<T> detallePolizasRenovadas() throws Excepciones;
}
