package mx.com.santander.aseguradora.colectivos.model.service.impl;



import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.GuiaContableDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGuiaContable;

@Service
public class ServicioGuiaContableImpl implements ServicioGuiaContable{
	
	@Resource
	private GuiaContableDao guiaContable;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;


	public List<Object> consultarProductos(int nDatosRamos)
			throws Exception {
//		message.append("estatusEmision");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			return this.guiaContable.consultarProductos(nDatosRamos);
		} catch (Exception e) {
			message.append("Error al recuperar el estatus de la carga y emisi�n.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	
	public List<Object> consultarPlanes(int nDatosProductos, int nDatosRamos)
			throws Exception {
//		message.append("estatusEmision");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			return this.guiaContable.consultarPlanes(nDatosProductos, nDatosRamos);
		} catch (Exception e) {
			message.append("Error al recuperar el estatus de la carga y emisi�n.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}


	public List<Object> obtenerCoberturas(int nDatosRamos, int nDatosProductos,
			int nDatosPlanes) throws Exception{
		try {
			return this.guiaContable.obtenerCoberturas(nDatosRamos, nDatosProductos, nDatosPlanes);
		} catch (Exception e) {
			message.append("Error al recuperar el estatus de la carga y emisi�n.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}


	public List<Object> obtenerGuiasContables(String ramoPoliza,
			String ramoContable) throws Exception {
		try {
			return this.guiaContable.obtenerGuiasContables(ramoPoliza, ramoContable);
		} catch (Exception e) {
			message.append("Error al recuperar el estatus de la carga y emisi�n.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
}
