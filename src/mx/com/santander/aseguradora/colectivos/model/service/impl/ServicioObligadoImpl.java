/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;


import java.util.Date;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.dao.ObligadoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioObligado;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dflores
 *
 */
@Service
public class ServicioObligadoImpl implements ServicioObligado  {

	private Log log = LogFactory.getLog(this.getClass());
	@SuppressWarnings("unchecked")
	@Resource
	private ObligadoDao obligadoDao;
	private String message = null;
	


	@SuppressWarnings("unchecked")
	public void setObligadoDao(ObligadoDao obligadoDao) { 
		this.obligadoDao = obligadoDao;
	}
	

	@SuppressWarnings("unchecked")
	public ObligadoDao getObligadoDao() {
		return obligadoDao;
	}

	
	public void guardaObligado(Long cdParametro,
			String desParametro, String idParametro, Integer inProducto,
			String inPlan, String inPeriodicidad, Short inCanal, Short inRamo,
			Integer inPoliza, Short inNumObligados)throws Excepciones {
		// TODO Auto-generated method stub
		this.log.debug("Iniciando proceso de Alta de obligado");
		try {
			
			 this.obligadoDao.guardaObligado( cdParametro,
						 desParametro,  idParametro,  inProducto,
						 inPlan,  inPeriodicidad,  inCanal,  inRamo,
						 inPoliza,  inNumObligados);
			
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
		
	}
       
	

}
