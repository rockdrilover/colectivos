/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;



import WSTimbrado.EnviaTimbrado;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanFacturacion;
import mx.com.santander.aseguradora.colectivos.view.dto.ReporteDxP;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqCancelacionCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqDescargaCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResDescargaCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResCancelacionCFDIDTO;

/**
 * @author Z014058
 * Modificacion: Sergio Plata
 *
 */
public interface ServicioCertificado extends ServicioCatalogo {
	
	public List<Object> totalCertificados(Short canal, Short ramo, Long Poliza)throws Excepciones;
	public void cancelaCertificado(Certificado certificado)throws Excepciones;
	public <T> List<T> getPreFacCertificado (Short canal , Short ramo, Long poliza, Integer inIdVenta ) throws Excepciones;
	public void actualizaPreFacPoliza(CertificadoId id) throws Excepciones;
	public String totalCertif(Short canal, Short ramo,
			Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta, String rutaReportes) throws Excepciones;  
	public <T> List<T> seleccionaDatosAutos() throws Excepciones;
	public void actualizaDatosAutos(Short canal, Short ramo, Long poliza, Long nucert, String cred, String nomarch, String cred2) throws Excepciones;
    public <T> List<T> consultaCertificados (Short canal, Short ramo, long Pol, String tiprep, String tippma, String sts, String fechaini, String fechafin) throws Excepciones;

	public <T> List<T> consultaMensualTecnico(Short canal, Short ramo,
			Long poliza1, Long poliza2, String fecha1, String fecha2) throws Excepciones;
	
	

	public <T> List<T> generaReporteAutos() throws Excepciones;
	public <T> List<T> cifrasResumenAutos() throws Excepciones;
	public <T> List<T> generaReporteEmisionAutos() throws Excepciones;
	public void borrarDatosAutosPrecarga()throws Excepciones;
	public <T> List<T> consultaPolizaCancela(Short canal, Short ramo, Long poliza1) throws Excepciones;
	public void cancelaCert(CertificadoId id, String fecha1, Integer causa)throws Excepciones;
	public void conteoCertificados(Certificado certificado)throws Excepciones;
	public void conteoCertificadosMasivo(BeanCertificado certificado)throws Excepciones;
	
	public String consultaMensualEmision(Short canal, Short ramo,
			Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String reporte) throws Excepciones;
	public String reporteFacturados5758() throws Excepciones;
	public String reporteErrores5758() throws Excepciones;
	public void borraErrores5758();
	
	public String generaLayoutRamo5(Short ramo) throws Excepciones;
	public String generaLayoutRamo65(Short ramo) throws Excepciones;
	public String generaLayoutRamo82(Short ramo) throws Excepciones;
	public void borraErrores56582(Short ramo) throws Excepciones;
	public String generaReporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa, String contratoEnlace, String folioEndoso) throws Excepciones;
    public String generaSoporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa, String contratoEnlace, String folioEndoso) throws Excepciones;
	public String consultaEmisionDesempleo(String fecha1, String fecha2)throws Excepciones;
	public String generaObligados(short canal, Short ramo, Long poliza,	Long poliza1, Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String reporte) throws Excepciones;
	public String generaDevoluciones(short canal, Short ramo, Long poliza, Long poliza1, String fecha1, String fecha2, Integer inIdVenta, String reporte) throws Excepciones;
	public EnviaTimbrado sellarRecibos(short canal, byte ramo, Double reciboFiscal) throws Excepciones;
	public void actualizarDatosCFDI(EnviaTimbrado objTimbrado, short canal, byte ramo, Double reciboFiscal) throws Excepciones;
	public String generaDetallePreFactura(int canal, int ramo, int poliza) throws Excepciones;
	public List<Object> resumenPreFactura(int canal, int ramo, Long poliza) throws Excepciones;
	public String consultaReporteCancelacion(String fecha1, String fecha2, String causaAnulacion)throws Excepciones;
	public void actualizarObs(CertificadoId id, String obs)throws Excepciones;
	
	public <T> List<T> obtenerProdPlan(String filtro) throws Excepciones;
	public <T> BigDecimal maxNumeroDeCredito(String filtro) throws Excepciones;
	public abstract <T> List<T> cargaPolizas(int ramo) throws Exception;
	public abstract <T> List<T> cargaPolizasRenovar(int ramo,String rmes2, String anio) throws Exception;
	public  String renovar5758(Short sucursal, Short ramo, Long poliza) throws Excepciones;
	public int getUltimoRecibo(CertificadoId id, BigDecimal prima)  throws Excepciones;
	public abstract ArrayList<Object> reporteVigor(short canal, Short ramo, Long poliza, Integer inIdVenta, Long poliza1) throws Excepciones;
	public  String generaReporte5758(String fecha1, String rmes2) throws Excepciones;
	
	/**
	 * Metodo que manda guardar una peticion de Batch en base de datos
	 * @param next: cd de peticion
	 * @param nombreVigores: nombre archivo peticion
	 * @param rutaParam: ruta donde tomar archivos
	 * @param strMensaje: mensaje que se va presentar en correo
	 * @param strNomArchivo: nombre archivo salida
	 * @param tipoReporte: tipo reporte a generar 
	 * @throws Excepciones: Exception si hay un error
	 */
	void insertaBanderaBatch(Long next, String nombreVigores, String rutaParam, String strMensaje, String strNomArchivo, Long tipoReporte) throws Excepciones;	
	
	/**
	 * Metodo que manda guardar una peticion de Batch en base de datos
	 * @param objPeticion: Objeto con datos de la peticion
	 * @param numRecibo:Numero de recibo de la peticion 
	 * @throws Excepciones: Exception si hay un error
	 */
	void insertaBanderaBatch(Parametros objPeticion, Double numRecibo) throws Excepciones;
	
	/**
	 * Metodo que consulta los parametros para procesos batch
	 * @param strDesc: Descripcion del parametro
	 * @return: regresa objeto con parametros del proceso batch
	 * @throws Excepciones: Exception si hay un error
	 */
	Parametros valoresBatch(String strDesc) throws Excepciones;
	
	/**
	 * Metodo que verifica si hay recibos pendientes de cobro
	 * @param coceCasuCdSucursal: Canal
	 * @param coceCarpCdRamo: Ramo
	 * @param coceCapoNuPoliza: Numero Poliza
	 * @return: verdadero si hay recibos pendientes de cobro, falso si no hay
	 * @throws Exception: Exception si hay un error
	 */
	boolean recibosPendCobro(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza) throws Excepciones;
	
	/**
	 * Llamada a servicio de timbrado cfdi v33 
	 * @param req Informacion de recibo a timbrar
	 * @return Resultado timbrado
	 * @throws Excepciones Manejo de excepciones en el llamado
	 */
	public ResTimbradoCFDIDTO sellarRecibosV33(ReqTimbradoCFDIDTO req) throws Excepciones;
	
	/**
	 * Descarga archivo xml o pdf del CFDI relacionado al RECIBO
	 * @param req Informacion de recibo a descargar
	 * @return Resultado de descarga
	 * @throws Excepciones Excepciones Manejo de excepciones en el llamado
	 */
	public ResDescargaCFDIDTO descargaCFDI(ReqDescargaCFDIDTO req) throws Excepciones;
	
	
	/**
	 * Cancelacion de CFDI
	 * @param req CFDI a cancelar
	 * @return 
	 * @throws Excepciones
	 */
	public ResCancelacionCFDIDTO cancelaCFDI(ReqCancelacionCFDIDTO req) throws Excepciones;
	
	/**
	 * Metodo que actualiza el estatus de un recibo
	 * @param id: Objeto con canal, ramo, poliza.
	 * @throws Exception: Exception si hay un error
	 */
	void actualizaEstatusRecibo(CertificadoId id) throws Excepciones;
	
	/**
	 * Metodo que regresa la fecha de cierre de un periodo contable
	 * @param mes: mes del periodo contable
	 * @param anio: a�o del periodo contable
	 * @return: regresa objeto parametros
	 * @throws Exception: Exception si hay un error
	 */
	Parametros getFechasCierre(Integer mes, Integer anio) throws Excepciones;
	
	/**
	 * Metodo que genera el Query de reporte Endosos
	 * @param canal: Canal para filtrar reporte
	 * @param ramo: Ramo para filtrar reporte
	 * @param poliza: Poliza para filtrar reporte
	 * @param inIdVenta: IdVenta para filtrar reporte	 
	 * @param fechaini: Fecha Inicio para filtrar reporte
	 * @param fechafin: Fecha Fin para filtrar reporte
	 * @return:	String con el query a ejecutar
	 * @throws Exception: Exception si hay un error
	 */
	String queryEndosos(short canal, Short ramo, Long poliza, Integer inIdVenta, String fechaini, String fechafin) throws Excepciones;
	
	/**
	 * Metodo que genera el Query de reporte Endosos Conciliados
	 * @param canal: Canal para filtrar reporte
	 * @param ramo: Ramo para filtrar reporte
	 * @param poliza: Poliza para filtrar reporte
	 * @param inIdVenta: IdVenta para filtrar reporte	 
	 * @param fechaini: Fecha Inicio para filtrar reporte
	 * @param fechafin: Fecha Fin para filtrar reporte
	 * @return:	String con el query a ejecutar
	 * @throws Exception: Exception si hay un error
	 */
	String queryEndososConciliados(short canal, Short ramo, Long poliza, Integer inIdVenta, String fechaini, String fechafin) throws Excepciones;
	
	/**
	 * Metedo que consulta recibos segun filtro seleccionado
	 * @param canal: filtro de canal para consulta 
	 * @param ramo: filtro de ramo para consulta
	 * @param poliza: filtro de poliza para consulta
	 * @param fechaDesde: filtro de fecha desde para consulta 
	 * @param fechaHasta: filtro de fecha hasta para consulta
	 * @return: Lista de recibos encontrados 
	 * @throws Exception: Exception si hay un error
	 */
	List<BeanFacturacion> consultaRecibos(short canal, Short ramo, Long poliza, String fechaDesde, String fechaHasta) throws Excepciones;
	/*public String reporteProgramado(Short canal, Short ramo,
			Long poliza1, Long poliza2, String fecha1, String fecha2, Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String reporte, String usuario) throws Excepciones;*/
	public String reporteProgramado(List<Object> listaReporte) throws Excepciones;
	
	/**
	 * Author: Towa (JJFM)
	 * consultaReporteDxP
	 * @return Array con la informacion para el reporte de Recibos.
	 */
	public ArrayList<ReporteDxP> consultaReporteDxP(Short shortCanal, Short shortRamo, long longPoliza,
			Short shortCert, String strFechaVigencia, String strFechaBusqueda, short idVenta) throws Excepciones;
	
	/**
	 * Author: Towa (JJFM)
	 * exportarReporteDxP
	 * @return File - Archivo de reporte DxP.
	 */
	public File  exportarReporteDxP(Short canal, Short ramo, long poliza1, String fecha1, String fecha2, Integer inIdVenta
			, boolean chk1, boolean chk2, boolean chk3, String rutaReportes,
			Short cuota,
			int filtro
			) throws Excepciones;
	
	/**
	 * Author: Towa (JJFM)
	 * exportarReporteRecibos
	 * @return File - archivo con el Reporte de Recibos. 
	 */
	public File exportarReporteRecibos(Short canal, Short ramo, long poliza1, String fecha1, String fecha2, Integer inIdVenta
			, boolean boolFiltroCobrado, boolean boolFiltroPendientecobro, String rutaReportes) throws Excepciones;
	
	/**
	 * Author: Towa (JJFM)
	 * ObtenerVigencias
	 * @return arrstrObtenerVigencias
	 */
	public ArrayList<String> arrstrObtenerVigencias(Short shortCanal, Short shortRamo, long longPoliza, short shortCert) throws Excepciones;
}

