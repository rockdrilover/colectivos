package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioReporteStock;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
@Service
public class ServicioReporteStockImpl implements ServicioReporteStock {

	@Resource
	private CertificadoDao CertificadoDao;
	@Resource
	private ServicioParametros servicioParametros;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message = null;
	private boolean flgJoinClientes = false, flgJoinAlternaPlanes = false, flgJoinColectivosCliente = false, flgJoinParametros = false, flgJoinHead = false;
	
	public ServicioReporteStockImpl() {
		this.message = new StringBuilder();
		
	}
	
	public List<Object> consultarRegistros(Short ramo, Long poliza, Integer inIdVenta, Date fecfin, Date fecini, String strCampos) throws Excepciones {
		List<Object> lstResultados;
		String strQuery;
		
		try {
			lstResultados = new ArrayList<Object>();
			flgJoinClientes = false;
			flgJoinAlternaPlanes = false;
			flgJoinColectivosCliente = false;
			flgJoinParametros = false;
			flgJoinHead = false;
			strQuery = "SELECT " + strCampos + " FROM ";
			
			strQuery = crearSelectEmitidas(strCampos);
			strQuery += crearFiltro(ramo, poliza, inIdVenta, fecini, fecfin, strQuery.contains("GEPREFAC"));
			lstResultados.addAll(this.CertificadoDao.consultarRegistros(strQuery));
			return lstResultados;
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteStock.consultarRegistros():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	public List<Object> consultarErrores(Short ramo, Long poliza, Integer inIdVenta, Date fecfin, Date fecini, Integer inTipoError) throws Excepciones {
		List<Object> lstResultados;
		StringBuffer sbQuery;
		
		try {
			lstResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT TTA.TRSE_TIPO_PRODUCTO,  TTA.TRSE_CUENTA, TTA.TRSE_SUB_CAMPANA, TTA.TRSE_REGISTRO, TTA.TRSE_FECHA_PAGO \n");
				sbQuery.append("FROM TLMK_TRASPASO_APTC TTA \n");				
			sbQuery.append("WHERE TTA.TRSE_SUB_CAMPANA = ").append(inIdVenta).append("\n ");
				sbQuery.append(" AND TTA.TRSE_TIPO_REGISTRO = ").append(inTipoError).append("\n ");
				sbQuery.append(" AND TTA.TRSE_FECHA_CARGA BETWEEN TO_DATE('").append(GestorFechas.formatDate(fecini, "dd/MM/yyyy")).append("', 'dd/MM/yyyy') AND TO_DATE('").append(GestorFechas.formatDate(fecfin, "dd/MM/yyyy")).append("', 'dd/MM/yyyy')").append("\n ");
			  
			if(String.valueOf(inTipoError).equals(Constantes.CODIGO_ERROR_EMISION)) {
				sbQuery.append(" AND TTA.TRSE_TIPO_PRODUCTO = ").append(ramo).append("\n ");
				if(inIdVenta != 0) {
					sbQuery.append(" AND TTA.TRSE_NUMERO_SOLICITUD = ").append(poliza).append("\n ");	
				}
			}
				 
			lstResultados.addAll(this.CertificadoDao.consultarRegistros(sbQuery.toString()));
			return lstResultados;
		} catch (Exception e) {
			this.message.append("Error:: ServicioReporteStock.consultarErrores():: ");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		
	}
	
	private String crearSelectEmitidas(String strCampos) throws Exception {
		String strSelect;
		ArrayList<String> arlResultados;
		String[] arrCampos;
		int totalCampos, contCampos = 1;
		String strInner = ""; 
		
		try {
			strSelect = "SELECT ";
			arrCampos = strCampos.split(",");
			totalCampos = arrCampos.length;
			for(int i = 0; i < totalCampos; i++){
				arlResultados = getNombreCampo(Integer.parseInt(arrCampos[i].toString().split("\\.")[1])); 
				strSelect += arlResultados.get(0);
				if(totalCampos == contCampos){
					strSelect += "\n";
				} else {
					strSelect += ", \n";
				}
				contCampos++;
				
				if(!arlResultados.get(1).equals("")){
					strInner += arlResultados.get(1) + "\n";
				}
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCertificado.crearSelectEmitidas:: " + e.getMessage());
		}
		
		strSelect += "FROM COLECTIVOS_CERTIFICADOS C \n";
		strSelect += strInner;
		
		return strSelect;
	}
	
	private ArrayList<String> getNombreCampo(int nCampo) {
		ArrayList<String> arlResultados = new ArrayList<String>();
		String strCampo = "";
		String strInner = ""; 
		
		switch (nCampo) {
			case 1:		//RAMO
				strCampo = "C.COCE_CARP_CD_RAMO";
				break;
			case 2:		//NUMERO POLIZA
				strCampo = "C.COCE_CAPO_NU_POLIZA";
				break;
			case 3:		//CREDITO
				strCampo = "CC.COCC_ID_CERTIFICADO";
				break;
			case 4:		//CREDITO NUEVO
				strCampo = "NVL(C.COCE_BUC_EMPRESA, 0)";
				break;
			case 5:		//CERTIFICADO
				strCampo = "C.COCE_NU_CERTIFICADO";
				break;
			case 6:		//RECIBO
				strCampo = "C.COCE_NO_RECIBO";
				break;
			case 7:		//PRODUCTO
				strCampo = "C.COCE_TP_PRODUCTO_BCO";
				break;
			case 8:		//SUBPRODUCTO
				strCampo = "C.COCE_TP_SUBPROD_BCO";
				break;
			case 9:		//ESTATUS
				strCampo = "EST.ALES_CAMPO1";
				break;
			case 10:	//ESTATUS_MOVIMIENTO
				strCampo = "'EMITIDO'";
				break;
			case 11:	//SUCURSAL
				strCampo = "C.COCE_CAZB_CD_SUCURSAL";
				break;
			case 12:	//CENTRO_COSTOS
				strCampo = "PL.ALPL_DATO3";
				break;
			case 13:	//IDENTIFICACION_DE_CUOTA
				strCampo = "NVL(PL.ALPL_DATO2, PL.ALPL_DE_PLAN)";
				break;
			case 14:	//PLAZO
				strCampo = "C.COCE_CD_PLAZO";
				break;
			case 15:	//NUMERO_CLIENTE
				strCampo = "CL.COCN_BUC_CLIENTE";
                break;
			case 16:	//NOMBRE
				strCampo = "CL.COCN_NOMBRE || '/' || CL.COCN_APELLIDO_PAT || '/' || CL.COCN_APELLIDO_MAT";				
				break;
			case 17:	//CUENTA
				strCampo = "C.COCE_NU_CUENTA";
				break;
			case 18:	//FECHA_NACIMIENTO
				strCampo = "CL.COCN_FE_NACIMIENTO";
				break;
			case 19:	//RFC
				strCampo = "CL.COCN_RFC";
				break;
			case 20:	//SEXO
				strCampo = "CL.COCN_CD_SEXO";
				break;
			case 21:	//CP
				strCampo = "CL.COCN_CD_POSTAL";
				break;
			case 22:	//MUNICIPIO
				strCampo = "CL.COCN_DELEGMUNIC";
				break;
			case 23:	//ESTADO
				strCampo = "CL.COCN_CD_ESTADO || ' - ' || ES.CAES_DE_ESTADO";
				break;
			case 24:	//TARIFA
				strCampo = "ROUND(C.COCE_MT_PRIMA_PURA * 1000 / DECODE(C.COCE_SUB_CAMPANA, '7', DECODE(C.COCE_MT_SUMA_ASEGURADA, 0, 1, C.COCE_MT_SUMA_ASEGURADA), DECODE(C.COCE_MT_SUMA_ASEG_SI, 0, 1, C.COCE_MT_SUMA_ASEG_SI)), 2)";
				break;
			case 25:	//PRIMA_NETA
				strCampo = "CASE "; 
				strCampo = strCampo + "WHEN PF.COPA_NVALOR5 = 0 ";                                     
				strCampo = strCampo + "THEN NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'P', 1, 3) + 1, INSTR(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'P', 1, 3) + 1), '|', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "WHEN PF.COPA_NVALOR5 = 1 ";                                    
				strCampo = strCampo + "THEN C.COCE_MT_PRIMA_PURA ";                            
				strCampo = strCampo + "ELSE 0 END";   
				break;
			case 26:	//PRIMA_TOTAL
				strCampo = "CASE WHEN PF.COPA_NVALOR5 = 0 THEN "; 
				strCampo = strCampo + "NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 1) - INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "+ NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 2) - INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "+ NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 3) - INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "+ NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'P', 1, 3) + 1, INSTR(SUBSTR(C.COCE_DI_COBRO1,INSTR(C.COCE_DI_COBRO1, 'P', 1, 3) + 1), '|', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "WHEN PF.COPA_NVALOR5 = 1 THEN "; 
				strCampo = strCampo + "NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 1) - INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "+ NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 2) - INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "+ NVL(TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1, INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) + 1, INSTR(C.COCE_DI_COBRO1, '|', 1, 3) - INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) - 1)), 0) "; 
				strCampo = strCampo + "+ NVL(C.COCE_MT_PRIMA_PURA, 0) "; 
				strCampo = strCampo + "ELSE 0 END";
				break;
			case 27:	//PRIMA_VIDA
				strCampo = "ROUND(CASE WHEN C.COCE_CARP_CD_RAMO = 61 THEN ";
				strCampo = strCampo + "(DECODE(C.COCE_SUB_CAMPANA, '7', C.COCE_MT_SUMA_ASEGURADA, C.COCE_MT_SUMA_ASEG_SI) / 1000) * ";
				strCampo = strCampo + "(SELECT SUM (COB.COCB_TA_RIESGO) TASAVIDA ";
				strCampo = strCampo + "FROM COLECTIVOS_COBERTURAS COB ";                             
				strCampo = strCampo + "WHERE COB.COCB_CASU_CD_SUCURSAL =  1 ";                         
				strCampo = strCampo + "AND COB.COCB_CARP_CD_RAMO = 61 ";                         
				strCampo = strCampo + "AND COB.COCB_CARB_CD_RAMO = 14 ";                         
				strCampo = strCampo + "AND COB.COCB_CACB_CD_COBERTURA <> '003' ";                     
				strCampo = strCampo + "AND COB.COCB_CAPU_CD_PRODUCTO = C.COCE_CAPU_CD_PRODUCTO "; 
				strCampo = strCampo + "AND COB.COCB_CAPB_CD_PLAN = C.COCE_CAPB_CD_PLAN ";     
				strCampo = strCampo + "AND COB.COCB_CER_NU_COBERTURA = C.COCE_NU_COBERTURA) ";     
				strCampo = strCampo + "END, 2)"; 
				break;
			case 28:	//PRIMA_DESEMPLEO
				strCampo = "ROUND(CASE WHEN C.COCE_CARP_CD_RAMO = 61 THEN (DECODE(C.COCE_SUB_CAMPANA, '7', C.COCE_MT_SUMA_ASEGURADA, C.COCE_MT_SUMA_ASEG_SI) / 1000) * "; 
				strCampo = strCampo + "(SELECT SUM (COB.COCB_TA_RIESGO) TASADES ";                     
				strCampo = strCampo + "FROM COLECTIVOS_COBERTURAS COB ";                             
				strCampo = strCampo + "WHERE COB.COCB_CASU_CD_SUCURSAL =  1 ";                         
				strCampo = strCampo + "AND COB.COCB_CARP_CD_RAMO = 61 ";                         
				strCampo = strCampo + "AND COB.COCB_CARB_CD_RAMO = 14 ";                         
				strCampo = strCampo + "AND COB.COCB_CACB_CD_COBERTURA = '003' ";                     
				strCampo = strCampo + "AND COB.COCB_CAPU_CD_PRODUCTO = C.COCE_CAPU_CD_PRODUCTO "; 
				strCampo = strCampo + "AND COB.COCB_CAPB_CD_PLAN     = C.COCE_CAPB_CD_PLAN ";     
				strCampo = strCampo + "AND COB.COCB_CER_NU_COBERTURA = C.COCE_NU_COBERTURA) ";     
				strCampo = strCampo + "END, 2) "; 
				break;
			case 29:	//DERECHOS
				strCampo = "TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1,INSTR(C.COCE_DI_COBRO1, 'O', 1 ,1) + 1,INSTR(C.COCE_DI_COBRO1, '|', 1, 1) - INSTR(C.COCE_DI_COBRO1, 'O', 1, 1) - 1))";
				break;
			case 30:	//RECARGOS
				strCampo = "TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1,INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) + 1,INSTR(C.COCE_DI_COBRO1, '|', 1, 2) - INSTR(C.COCE_DI_COBRO1, 'I', 1, 1) - 1))";
				break;
			case 31:	//IVA
				strCampo = "TO_NUMBER(SUBSTR(C.COCE_DI_COBRO1,INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) + 1,INSTR(C.COCE_DI_COBRO1, '|', 1, 3) - INSTR(C.COCE_DI_COBRO1, 'A', 1, 1) - 1))";
				break;
			case 32:	//SUMA_ASEGURADA
				strCampo = "TRIM(TO_CHAR(DECODE(C.COCE_SUB_CAMPANA, '7', TO_NUMBER(C.COCE_MT_SUMA_ASEG_SI), C.COCE_MT_SUMA_ASEGURADA), '999999999.99'))";
				break;
			case 33:	//BASE_CALCULO
				strCampo = "DECODE(C.COCE_SUB_CAMPANA, '7', C.COCE_MT_SUMA_ASEGURADA, C.COCE_MT_SUMA_ASEG_SI)";
				break;
			case 34:	//MONTO_DEVOLUCION
				strCampo = "NVL(C.COCE_MT_BCO_DEVOLUCION, 0)";
				break;
			case 35:	//MONTO_DEVOLUCION_SIS
				strCampo = "NVL(C.COCE_MT_DEVOLUCION, 0)";
                break;
			case 36:	//FECHA_INGRESO
				strCampo = "C.COCE_FE_SUSCRIPCION";				
				break;
			case 37:	//FECHA_ANULACION
				strCampo = "C.COCE_FE_ANULACION";
				break;
			case 38:	//FECHA_CANCELACION
				strCampo = "C.COCE_FE_ANULACION_COL";
				break;
			case 39:	//FECHA_INICIO_POLIZA
				strCampo = "HEAD.COCE_FE_DESDE";
				break;
			case 40:	//FECHA_FIN_POLIZA
				strCampo = "HEAD.COCE_FE_HASTA";
				break;
			case 41:	//FECHA_DESDE
				strCampo = "C.COCE_FE_DESDE";
				break;			
			case 42:	//FECHA_HASTA
				strCampo = "C.COCE_FE_HASTA";
				break;
			case 43:	//FECHA_INICIO_CREDITO
				strCampo = "C.COCE_FE_INI_CREDITO";
				break;
			case 44:	//FECHA_FIN_CREDITO
				strCampo = "C.COCE_FE_FIN_CREDITO";
				break;
			case 45:	//FECHA_FIN_CREDITO_2
				strCampo = "C.COCE_CAMPOV6";
				break;	
		}
		
		switch (nCampo) {
			case 3:		//CREDITO
				if(!flgJoinColectivosCliente) {
					strInner = "INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC ON(CC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND CC.COCC_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND CC.COCC_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND CC.COCC_NU_CERTIFICADO = C.COCE_NU_CERTIFICADO AND NVL(CC.COCC_TP_CLIENTE, 1) = 1)";
				}
				flgJoinColectivosCliente = true;
				break;
			case 9:		//ESTATUS
				strInner = "INNER JOIN ALTERNA_ESTATUS EST ON EST.ALES_CD_ESTATUS = C.COCE_ST_CERTIFICADO";
				break;
			case 12:	//CENTRO_COSTOS
			case 13:	//IDENTIFICACION_DE_CUOTA
				if(!flgJoinAlternaPlanes){
					strInner = "INNER JOIN ALTERNA_PLANES PL ON(PL.ALPL_CD_RAMO = C.COCE_CARP_CD_RAMO AND PL.ALPL_CD_PRODUCTO = C.COCE_CAPU_CD_PRODUCTO AND PL.ALPL_CD_PLAN = C.COCE_CAPB_CD_PLAN)";	
				}
				flgJoinAlternaPlanes = true;
				break;
				
			case 15:	//NUMERO_CLIENTE
			case 16:	//NOMBRE
			case 18:	//FECHA_NACIMIENTO
			case 19:	//RFC
			case 20:	//SEXO
			case 21:	//CP
			case 22:	//MUNICIPIO
			case 23:	//ESTADO
				if(!flgJoinClientes) {
					if(!flgJoinColectivosCliente) {
						strInner = "INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC ON(CC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND CC.COCC_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND CC.COCC_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND CC.COCC_NU_CERTIFICADO = C.COCE_NU_CERTIFICADO AND NVL(CC.COCC_TP_CLIENTE, 1) = 1) \n";
					}
					flgJoinColectivosCliente = true;
					strInner += "INNER JOIN COLECTIVOS_CLIENTES CL ON CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE \n";
				}
				if(nCampo == 23) {
					strInner += "INNER JOIN CART_ESTADOS ES ON ES.CAES_CD_ESTADO = CL.COCN_CD_ESTADO ";
				}
				flgJoinClientes = true;
				break;

			case 25:	//PRIMA_NETA
			case 26:	//PRIMA_TOTAL
				if(!flgJoinParametros) {
					if(!flgJoinHead) {
						strInner = "INNER JOIN COLECTIVOS_CERTIFICADOS HEAD ON(HEAD.COCE_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND HEAD.COCE_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND HEAD.COCE_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND HEAD.COCE_NU_CERTIFICADO = 0) \n";
					}
					flgJoinHead = true;
					strInner += "INNER JOIN COLECTIVOS_PARAMETROS PF ON PF.COPA_ID_PARAMETRO = 'GEPREFAC'";
				}
				flgJoinParametros = true;
				break;
				
			case 39:	//FECHA_INICIO_POLIZA
			case 40:	//FECHA_FIN_POLIZA
				if(!flgJoinHead) {
					strInner = "INNER JOIN COLECTIVOS_CERTIFICADOS HEAD ON(HEAD.COCE_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND HEAD.COCE_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND HEAD.COCE_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND HEAD.COCE_NU_CERTIFICADO = 0)";
				}
				flgJoinHead = true;
				break;
		}
		
		arlResultados.add(0, strCampo);
		arlResultados.add(1, strInner);
		
		return arlResultados;
	}


	private String crearFiltro(Short ramo, Long poliza, Integer inIdVenta, Date fecini, Date fecfin, boolean filtroParametros) throws Exception {
		StringBuffer sbFiltro, sbFiltro2;
		Integer nCanal = 1;
		String cadenacan = null;
		Integer inicioPoliza = 0;
		
		try {
			sbFiltro = new StringBuffer();
			sbFiltro2 = new StringBuffer();
			
			if(poliza == 0) {
				sbFiltro.append("  and P.copaDesParametro = 'POLIZA' \n");
				sbFiltro.append("  and P.copaIdParametro  = 'GEP' \n");
				sbFiltro.append("  and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append("  and P.copaNvalor6 = ").append(inIdVenta).append("\n");
			    
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				if (listaPolizas != null && listaPolizas.size() > 0) {
					inicioPoliza = Integer.parseInt(listaPolizas.get(0).getCopaNvalor3().toString().substring(0, 1));
				} else  {
					inicioPoliza = -1;
				}
				
				cadenacan = "  AND C.COCE_CAPO_NU_POLIZA >= " + inicioPoliza + "00000000 AND C.COCE_CAPO_NU_POLIZA <= " + inicioPoliza + "99999999 \n"
				          + "  AND C.COCE_SUB_CAMPANA = '" + inIdVenta + "' \n";

			} else {
				sbFiltro.append("  and P.copaDesParametro= 'POLIZA' \n");
				sbFiltro.append("  and P.copaIdParametro = 'GEP'    \n" );
				sbFiltro.append("  and P.copaNvalor1 = ").append(nCanal).append(" \n");
				sbFiltro.append("  and P.copaNvalor2 = ").append(ramo).append("\n");
				sbFiltro.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(poliza).append(", 1, substr(").append(poliza).append(",0,3), 2, substr(").append(poliza).append(",0,2))\n");
				sbFiltro.append("order by P.copaNvalor3");
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(sbFiltro.toString());
				for(Parametros item: listaPolizas){
					if(item.getCopaNvalor4().toString().equals("1")){
						cadenacan="  AND C.COCE_CAPO_NU_POLIZA LIKE '" + poliza + "%'";
					} else {
						cadenacan="  AND C.COCE_CAPO_NU_POLIZA = " + poliza;
					}
				}
			}
			
			sbFiltro2.append(" WHERE C.COCE_CASU_CD_SUCURSAL =  " + nCanal + "\n");
			sbFiltro2.append(" AND C.COCE_CARP_CD_RAMO = " + ramo +" \n");
			sbFiltro2.append(cadenacan + "  \n ");
			sbFiltro2.append(" AND C.COCE_NU_CERTIFICADO > 0 \n");
			
			if(fecini != null){
				sbFiltro2.append("AND C.COCE_FE_CARGA BETWEEN TO_DATE('").append(GestorFechas.formatDate(fecini, "dd/MM/yyyy")).append("', 'dd/MM/yyyy') AND TO_DATE('").append(GestorFechas.formatDate(fecfin, "dd/MM/yyyy")).append("', 'dd/MM/yyyy')");
			}

			if(filtroParametros) {
				sbFiltro2.append(" AND PF.COPA_DES_PARAMETRO = 'POLIZA' \n");                                                            
				sbFiltro2.append(" AND PF.COPA_NVALOR1 = C.COCE_CASU_CD_SUCURSAL \n");                    
				sbFiltro2.append(" AND PF.COPA_NVALOR2 = C.COCE_CARP_CD_RAMO \n");                     
				sbFiltro2.append(" AND PF.COPA_NVALOR3 = DECODE(PF.COPA_NVALOR4, 0 ,C.COCE_CAPO_NU_POLIZA, 1, SUBSTR(C.COCE_CAPO_NU_POLIZA, 0, 3), SUBSTR(C.COCE_CAPO_NU_POLIZA, 0, 2)) \n"); 
				sbFiltro2.append(" AND PF.COPA_NVALOR6 = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA, '0')) \n");
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCertificado.crearFiltro():: " + e.getMessage());
		}
		
		return sbFiltro2.toString();
		
	}
	
	public CertificadoDao getCertificadoDao() {
		return CertificadoDao;
	}
	public void setCertificadoDao(CertificadoDao CertificadoDao) {
		this.CertificadoDao = CertificadoDao;
	}
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	
}
