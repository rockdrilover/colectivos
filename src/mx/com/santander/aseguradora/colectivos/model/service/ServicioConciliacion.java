package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.ArrayList;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
public interface ServicioConciliacion extends ServicioCatalogo{

	public ArrayList<Object> consultar(Integer nOrigen, Integer nTipoRep, Date fecha, Date fechaFin, Integer opcion, String strDatos) throws Exception;
	public Parametros getParemetrosConsulta(Integer nOrigen) throws Exception;
	public ArrayList<ArrayList> getLayOut(String strSelect, String strCreditos, Integer nTipoVenta, Integer nLayOut) throws Exception;
}
