/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.ArrayList;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;

/**
 * @author dflores
 *
 */
public interface ServicioAltaContratante extends ServicioCatalogo{

	public String getAnalista(String strCdAnalista) throws Exception;
	public String getSucursal(Integer getnCdSucursal) throws Exception;
	public Object[] getDatosPoliza(Short inRamo, Integer nProducto, Integer nPlan) throws Exception;
	public ArrayList<Object> getDatosCP(String strCp) throws Exception;
	public boolean actualizaCliente (ContratanteDTO contratanteDTO, Map<String, Object> resultado) throws Excepciones;
	public Integer obtenerFormaPago(Short ramo, Integer producto,  Integer plan) throws Excepciones;
	public boolean actualizaCertificados (Integer producto, Integer plan, String sumaAseg, String Prima, Short sucursal, Short ramo, Long poliza, Integer certificado, String formaPago) throws Excepciones;
	public ArrayList<Object> getDatosParentesco() throws Exception;
	public void actualizaCartCertificados(short cdSucursal, Short inRamo, Long numeroPoliza, Map<String, Object> resultadoCliente2) throws Exception;;

}
