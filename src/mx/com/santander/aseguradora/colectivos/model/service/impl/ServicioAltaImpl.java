/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;


import java.util.Date;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.dao.AltaDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAlta;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dflores
 *
 */
@Service
public class ServicioAltaImpl implements ServicioAlta  {

	private Log log = LogFactory.getLog(this.getClass());
	@Resource
	private AltaDao altaDao;
	private String message = null;
	
	public void setAltaDao(AltaDao altaDao) { 
		this.altaDao = altaDao;
	}
	public AltaDao getAltaDao() {
		return altaDao;
	}

	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public Long iniciaAlta( int canal, Short ramo,String analista,int sucursal,String domicilio,String cedula_rif,Date fecha_ini) throws Excepciones {
		// TODO Auto-generated method stub
		Long pol = 0L;
		this.log.debug("Iniciando proceso de Alta de poliza");
		try {
			
			pol = this.altaDao.iniciaAlta( canal,ramo,analista,sucursal,domicilio,cedula_rif,fecha_ini);
			return pol;
			
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
		
	}
       
	

}
