package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.ColectivosComponentesDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosComponentes;

/*
 * @author: Leobardo Preciado
 */

@Service
public class ServicioColectivosComponentesImpl implements ServicioColectivosComponentes{

	@Resource
	private ColectivosComponentesDao colectivosComponentesDao;
	private Log log = LogFactory.getLog(this.getClass());

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			T cartCobertura = this.colectivosComponentesDao.guardarObjeto(objeto);
			return cartCobertura;
		} catch (ObjetoDuplicado e) {
			throw new ObjetoDuplicado("Cobertura duplicada.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("No se puede guardar la cobertura.");
		}
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		colectivosComponentesDao.borrarObjeto(objeto);
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		try {
			List<T> lista = this.colectivosComponentesDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden recuperar los productos", e);
			throw new Excepciones("No se pueden recuperar los productos", e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}
	
	public void respaldaIVA(BigDecimal tarifa) throws Excepciones {
		try {
			this.colectivosComponentesDao.respaldaIVA(tarifa);
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("Error en el proceso de actualizacion de IVA.", e);
			throw new Excepciones("Error en el proceso de actualizacion de IVA.", e);
		}
	}
	

}
