/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartEmisioncfdI;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.FacturacionId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.dao.FacturacionDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanFacturacion;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Sergio Plata 
 *
 */
@Service
public class ServicioProcesosImpl implements ServicioProcesos {

	@Resource
	private ProcesosDao procesosDao;
	@Resource
	private FacturacionDao facturacionDao;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	/**
	 * @return the facturacionDao
	 */
	public FacturacionDao getFacturacionDao() {
		return facturacionDao;
	}

	/**
	 * @param facturacionDao the facturacionDao to set
	 */
	public void setFacturacionDao(FacturacionDao facturacionDao) {
		this.facturacionDao = facturacionDao;
	}

	
	/**
	 * @param procesosDao the procesosDao to set
	 */
	public void setProcesosDao(ProcesosDao procesosDao) {
		this.procesosDao = procesosDao;
	}

	/**
	 * @return the procesosDao
	 */
	public ProcesosDao getProcesosDao() {
		return procesosDao;
	}

	public ServicioProcesosImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
	}
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos#cancelacionmasiva(java.lang.Short, java.lang.Short, java.lang.Long, java.lang.Integer)
	 */
	public void cancelacionMasiva(Short canal, Short ramo, Long poliza,
			Integer causa, Colectivos.OPC_CANACEL opc) throws Excepciones {
		// TODO Auto-generated method stub
		
		this.log.debug("Iniciando proceso de cancelacion masiva");
		
		try {
			
			this.procesosDao.cancelacionmasiva(canal, ramo, poliza, causa, opc);
			message.append("Procesos de cancelacion masiva finalizado.");
			this.log.debug(message.toString());
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("Hubo un error al ejecutar el proceso de cancelacion masiva.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

	}		

	
	public String cancelacionCri(Short inCanal, Short inRamo, Long inPoliza,
			Integer inIdVenta, Integer causaAnula) throws Excepciones {
		// TODO Auto-generated method stub
		String respuesta = this.procesosDao.cancelacionCri (inCanal, inRamo, inPoliza, inIdVenta, causaAnula);
		
		return respuesta;
		
	}
	
	/**
	 * 
	 */
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public String facturacion(Short canal, Short ramo) throws Excepciones {
		// TODO Auto-generated method stub
		
		message.append("Proceso de facuración.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		int respuesta;
		String salida = null;
		
		
		try {
			
			respuesta = this.procesosDao.facturacion(canal, ramo);
			
			if(respuesta == -1){
			
				message.append("Proceso de facturación finalizó con errores.");
				this.log.error(message.toString());
				salida = message.toString();
				throw new Excepciones(message.toString());
			}
			else
			{
				message.append("Proceso de facturación finalizó correctamente.");
				this.log.debug(message.toString());
				salida = message.toString();
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede realizar el proceso de facturación, debido a errores previos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
		return salida;
	}
	
	public void emisionMasiva(Short canal, Short ramo, Long poliza, Integer idVenta)
			throws Excepciones {
		// TODO Auto-generated method stub
		this.log.debug("Iniciando proceso de emision");
		try {
			
			this.procesosDao.emisionMasiva(canal, ramo, poliza, idVenta);
			
			message.append("Proceso de emision finalizado.");
			this.log.debug(message.toString());
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("Hubo un problema al realizar el proceso de emision.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}
	
	public DataSource getDataSource() throws Excepciones {
		// TODO Auto-generated method stub
		return this.procesosDao.getDataSource();
	}

	public Map<String, Object> prefactura(CertificadoId id, Integer nNumRecibo) throws Excepciones{
		try {
			return this.procesosDao.ejecutaPrefactura(id, nNumRecibo);
		} catch (Exception e) {
			message.append("Error: Al realizar proceso de Prefacturacion");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	//@SuppressWarnings("unchecked")
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public <T> List<T> modificaPrefactura(List <BeanCertificado> lista) throws Excepciones{
		System.out.println("Entre a modificaPrefactura --- ServicioProcesoImpl");
		List <T> listaM = null;
		try {			
			listaM = this.facturacionDao.getModificaMontosFac(lista);
			log.info("Sali del facturacionDao");
		} catch (Exception e) {
			message.append("Error: Al realizar la cancelacion del certificado.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return listaM;
	}

	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public <T> List<T> recuperaRecibos(int Sucursal, int Ramo, String polizas) throws Excepciones{
		System.out.println("Entre a modificaPrefactura --- ServicioProcesoImpl");
		List <T> listaR = null;
		try {			
			
			this.facturacionDao.actualizaRecaPenCobro(Sucursal, Ramo, polizas);
			listaR = this.facturacionDao.recuperaRecibos(Sucursal, Ramo, polizas);
			log.info("Sali del facturacionDao");
		} catch (Exception e) {
			message.append("Error: Al recuperar numero de recibo.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return listaR;
	}

	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public <T> List<T> facturacion(List <BeanFacturacion> lista) throws Excepciones{
		log.info("Entre a facturacion --- ServicioProcesoImpl");
		List <T> listaF = null;
		listaF = this.facturacionDao.facturacion(lista);
		return listaF;
	}
	
	public void factura(FacturacionId id) throws Excepciones{
		CartEmisioncfdI cfdi;
		
		try {
			log.info("Se corre, proceso de endosos.");
			this.procesosDao.ejecutaEndososPU(id);
			log.info("Entro a factura --- ServicioProceso");
			this.procesosDao.ejecutaFacturacion(id);
			log.info("Cambio Recibo a Cobrado");
			cfdi = this.procesosDao.estatusCobradoRecibo(id);
		} catch (Exception e) {
			message.append("Error: Al realizar proceso de Facturacion");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void actRecFiscal(Short canal, byte ramo, Long poliza, String reccol, int num) throws Excepciones{
		log.info("Entre a actualizar colectivos_facturacion para que solo tome un recibo a la hora de facturar --- ServicioProcesoImpl");
		this.facturacionDao.actRecFiscal(canal, ramo, poliza, reccol, num);
	}

	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public <T> List<T> guardaMontosNuevosPrefactura(List <BeanFacturacion> lista) throws Excepciones{
		log.info("Entre a facturacion --- ServicioProcesoImpl");
		List <T> listaF = null;
		listaF = this.facturacionDao.guardaMontosNuevosPrefactura(lista);
		return listaF;	
	}

	public <T> List<T> prefacturas(Short canal, Short ramo, Long poliza, Integer inIdVenta ) throws Excepciones {
		try {						
			return this.facturacionDao.prefacturas(canal, ramo, poliza, inIdVenta);
		}
		catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	public void aplicaEndosoDevolucion(Short inCanal, Short inRamo,
			Integer inIdVenta, List<Object> lista)
			throws Excepciones {
		// TODO Auto-generated method stub
		
		PreCarga dato;
		
		try{
			for(Object pre: lista){
				dato = (PreCarga)pre;
				this.procesosDao.aplicaEndosoDevolucion(inCanal, inRamo, inIdVenta, dato);
			}
			
		}catch (Exception e){
			this.message.append("Error al aplicar endosos de devolución.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	public Double calcularDevolucion(Certificado cer, Double tasaAmortiza,
			Date fechaAnulacion) throws Excepciones {
		// TODO Auto-generated method stub
		return this.procesosDao.calculaDevolucion(cer, tasaAmortiza, fechaAnulacion);
	}
	
	public void rehabilitacion(Short canal, Short ramo, Long poliza) throws Excepciones {
		// TODO Auto-generated method stub
		
		try {
			
			this.procesosDao.rehabilitacion(canal,ramo, poliza);
			message.append("Procesos de rehabilitacion masiva finalizado.");
			this.log.debug(message.toString());
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("Hubo un error al ejecutar el proceso de rehabilitacion masiva.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public <T> List<T> recuperaRecibos5758() throws Excepciones{
		System.out.println("Entre a recuperaRecibos5758() --- ServicioProcesoImpl");
		List <T> listaR = null;
		try {			
			listaR = this.facturacionDao.recuperaRecibos5758();
			log.info("Regreso a recuperaRecibos5758() --- ServicioProcesoImpl");
		} catch (Exception e) {
			message.append("Error: Al recuperar numero de recibo.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return listaR;
	}

	public String endosarPoliza(Short inCanal, Short inRamo, Long inPoliza,
			Integer inIdVenta, BigDecimal recibo, Date feDesde, Date feHasta,
			Integer inOperEndoso) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			log.info("Entro a Endosar --- ServicioProceso");
			return this.procesosDao.endosarPoliza(inCanal, inRamo, inPoliza, inIdVenta, recibo, feDesde, feHasta, inOperEndoso);
		} catch (Exception e) {
			message.append("Error: Al realizar proceso de Endoso");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
			// TODO: handle exception
		}
	}
	
	public void validarObligados() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			
			this.procesosDao.validaObligados();
			
			message.append("Proceso de vida empresa finalizado.");
			this.log.debug(message.toString());
			
		} catch (Exception e) {
			// TODO: handle exception	l
			message.append("Hubo un error al ejecutar el proceso de vida empresa.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void validarMultidisposicion(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta) throws Excepciones {
		try {
			this.procesosDao.validarMultidisposicion(inCanal, inRamo, inPoliza, inIdVenta);
			message.append("Proceso de validacion de LEx finalizado.");
			this.log.debug(message.toString());
		} catch (Exception e) {
			message.append("Hubo un error al ejecutar el proceso de validacion de LEx.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}		
	}

	public void validarCumulosBT() throws Excepciones {
		try {
			this.procesosDao.validaCumulosBT();
			message.append("Proceso de validacion BT finalizado.");
			this.log.debug(message.toString());
		} catch (Exception e) {
			message.append("Hubo un error al ejecutar el proceso de validacion BT.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}	
		
	}
	
	public void actualizarDatosComerciales(List<Object> lstObjetosGuardar) throws Exception {
		try {
			this.procesosDao.actualizaDatosComplementarios(lstObjetosGuardar);
			message.append("Proceso de actualizacion datos comerciales finalizado.");
			this.log.debug(message.toString());
		} catch (Exception e) {
			message.append("Hubo un error al ejecutar el proceso de actualizacion datos comerciales.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}	
		
	}

	public String numeradorCliente() throws Excepciones {
		String next; 
		try {
			
			next = this.procesosDao.numeradorCliente();
			message.append("Proceso genera numero de cliente");
			this.log.debug(message.toString());
		} catch (Exception e) {
			message.append("Hubo un error al ejecutar el proceso de numerador de cliente");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
		return next;	
	}

	public void actNumeradorCliente() throws Excepciones {
      try {
			
			this.procesosDao.actNumeradorCliente();
			message.append("Proceso actualiza numero de cliente");
			this.log.debug(message.toString());
		} catch (Exception e) {
			message.append("Hubo un error al ejecutar el proceso de actualizar numerador de cliente");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

	}

	public void insertaContratante(String res, String inRazonSocial, Date inFechaCons,
            String inCalleNum,String inCp, String inRfc,String inPoblacion,String inColonia) throws Excepciones {
		 try {
				
				this.procesosDao.insertaContratante(res, inRazonSocial, inFechaCons,
			             inCalleNum, inCp,  inRfc, inPoblacion, inColonia);
				message.append("Proceso insertar numero de cliente");
				this.log.debug(message.toString());
			} catch (Exception e) {
				message.append("Hubo un error al ejecutar el proceso de insertar numerador de cliente");
				this.log.error(message.toString(), e);
				throw new Excepciones(message.toString(), e);
			}
	
		
	}

	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public Integer buscaNegative(String cacnApellidoPat, String cacnApellidoMat, String cacnNombre, Date cacnFeNacimiento) throws Excepciones {
		try{
			Integer existe = this.procesosDao.busca_negative(cacnApellidoPat, cacnApellidoMat, cacnNombre, cacnFeNacimiento);
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se puede realizar el proceso de alta.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public Map<String, Object> buscaCliente(ContratanteDTO contratanteDTO) throws Excepciones {
		String result="";
		Map<String, Object> resultadoCliente = null;
		try{
			resultadoCliente= this.procesosDao.buscaCliente(contratanteDTO);
		}catch(Exception e){
			message.append("No se puede realizar el proceso de alta.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return resultadoCliente;
	}
	
	public void factura5758(CertificadoId id) throws Excepciones{
		try {
			log.info("Entro a factura5758 --- ServicioProceso");
			this.procesosDao.ejecutaPrefactura(id, 0);
		} catch (Exception e) {
			message.append("Error: Al realizar proceso de Facturacion");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void contabilidadColectivos(short canal, Short ramo, Long poliza, int certif, int ultRecibo, int mtUltRecibo, int operacion, int subOperacion, String fDesde, String fHasta, String fContable, String nombreFac, int estatusRecibo) throws Exception{
		try {
			log.info("Entro a contabilidadColectivos --- ServicioProceso");
			this.procesosDao.contaColectivos(canal,ramo,poliza,certif,ultRecibo,mtUltRecibo,operacion,subOperacion,fDesde,fHasta, fContable,nombreFac,estatusRecibo);

		} catch (Exception e) {
			message.append("Error: Al realizar proceso de Contabilidad");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

	}
	
	@Override
	public void cargaCuentas(Long nRamo, Integer nOperCargo, Double montoAC, Long cuenta, String newFechaFin,
			String fecha_hoy) throws Excepciones {
		try {
			log.info("servicio --- dao");
			this.procesosDao.procesoCargaCuenta( nRamo, nOperCargo,montoAC,cuenta,  newFechaFin,  fecha_hoy);
			System.out.println("Salgo de ServicioCargaConiImpl.procesoCargaConc");
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		
	}
	
	@Override
	public void procesaReserva(int anio, int mes) throws Excepciones {
		// TODO Auto-generated method stub
		this.log.debug("Iniciando proceso de emision");
		try {
			
			this.procesosDao.procesaReserva(anio, mes);
			
			message.append("Proceso de emision finalizado.");
			this.log.debug(message.toString());
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("Hubo un problema al realizar el proceso de emision.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos#consultaReserva(int, int, int)
	 */
	@Override
	public List<Object> consultaReserva(int ramo, int mes, int anio) throws Excepciones {
		// TODO Auto-generated method stub
		this.log.debug("Iniciando proceso de emision");
		try {
			
			return this.procesosDao.consultaReserva(ramo, mes, anio);
			
						
		} catch (Exception e) {
			// TODO: handle exception
			message.append("Hubo un problema al realizar el proceso de emision.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	
	
	
}