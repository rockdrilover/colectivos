/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.FoliosMediosPago;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.TlmkTraspasoAptc;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.TlmkTraspasoAptcId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecarga;
import mx.com.santander.aseguradora.colectivos.model.dao.CargaDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ConciliacionDao;
import mx.com.santander.aseguradora.colectivos.model.dao.HomologacionProdPlanesDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Mail;
import mx.com.santander.aseguradora.colectivos.utils.RFC;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.utils.Validaciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author Sergio Plata
 *
 */
@Service
public class ServicioCargaImpl implements ServicioCarga {

	@Resource
	private CargaDao cargaDao;
	@Resource 
	private ConciliacionDao conciliacionDao;
	@Resource 
	private HomologacionProdPlanesDao homologaProductosDao;
	@Resource 
	private ProcesosDao procesosDao;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	

	public void setCargaDao(CargaDao cargaDao) {
		this.cargaDao = cargaDao;
	}
	public CargaDao getCargaDao() {
		return cargaDao;
	}
	public ConciliacionDao getConciliacionDao() {
		return conciliacionDao;
	}
	public void setConciliacionDao(ConciliacionDao conciliacionDao) {
		this.conciliacionDao = conciliacionDao;
	}
	public HomologacionProdPlanesDao getHomologaProductosDao() {
		return homologaProductosDao;
	}
	public void setHomologaProductosDao(HomologacionProdPlanesDao homologaProductosDao) {
		this.homologaProductosDao = homologaProductosDao;
	}
	public ProcesosDao getProcesosDao() {
		return procesosDao;
	}
	public void setProcesosDao(ProcesosDao procesosDao) {
		this.procesosDao = procesosDao;
	}
	
	
	public ServicioCargaImpl() {
		this.message = new StringBuilder();
	}

	
	public void borrarDatosTemporales(Short canal, Short ramo, Long poliza, Integer inIdVenta, String archivo, String origen) throws Excepciones {
//		message.append("Borrando datos de la preCarga y carga.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			this.cargaDao.borrarDatosTemporales(canal, ramo, poliza, inIdVenta, archivo, origen);
		} catch (Exception e) {
			this.message.append("No se pueden borrar los datos de la preCarga y carga.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public <T> List<T> estatusEmision(Short canal, Short ramo, Long poliza, Integer idVenta, String archivo) throws Excepciones {
//		message.append("estatusEmision");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			return this.cargaDao.estatusEmision(canal, ramo, poliza, idVenta, archivo);
		} catch (Exception e) {
			message.append("Error al recuperar el estatus de la carga y emisión.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> estatusCancelacion(Short canal, Short ramo, Long poliza, String archivo) throws Excepciones {
//		message.append("estatusCancelacion.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			return this.cargaDao.estatusCancelacion(canal, ramo, poliza, archivo);
		} catch (Exception e) {
			message.append("Error en el proceso de cancelación masiva.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
//		this.message.append("actualizarPreCarga.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			this.cargaDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			this.message.append("Error al actualizar la preCarga.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	public <T> void borrarObjeto(T objeto) throws Excepciones {
//		message.append("borrarObjeto");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			this.cargaDao.borrarObjeto(objeto);
		} catch (Exception e) {
			message.append("no se puede borrar la preCarga.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public <T> T guardarObjeto(T objeto) throws Excepciones {
//		message.append("guardarObjeto.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			this.cargaDao.guardarObjeto(objeto);
			return objeto;
		} catch (ObjetoDuplicado e) {
			message.append("No se puede guardar la precarga, objeto duplicado.");
			this.log.error(message.toString(), e);
			throw new ObjetoDuplicado(message.toString(), e);
		} catch (Exception e) {
			message.append("No se puede guardar la precarga.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
//		this.message.append("obtenerPreCarga.");
//		this.log.debug(message);
//		Utilerias.resetMessage(message);
		
		try {
			T carga = this.cargaDao.obtenerObjeto(objeto, id);
			return carga;
		} catch (Exception e) {
			this.message.append("Error al recuperar carga:").append(id);
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString());
		}
	}
	
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
//		this.message.append("obtenerPrecarga.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			List<T> lista = this.cargaDao.obtenerObjetos(objeto);
			return lista;
		} catch (Exception e) {
			this.message.append("No se puede recuperar la preCarega.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
//		this.message.append("obtenerPrecarga.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			List<T> lista = this.cargaDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.message.append("No se puede recuperar la preCarega.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public boolean existeCargaConcilia(Short origen) throws Excepciones {
//		this.message.append("obtenerPrecarga.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			List<Object> lista = this.cargaDao.existeCargaConcilia(origen);
			if (lista != null && lista.size()> 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			this.message.append("No se puede recuperar la preCarega.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}
	
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
//		this.message.append("guardarObjetos.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			this.cargaDao.guardarObjetos(lista);
		} catch (ObjetoDuplicado e) {
			throw new ObjetoDuplicado("Registro duplicado.", e);
		} catch (Exception e) {
			this.message.append("No se puede guardar la lista de objetos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public Double getCumulo(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta) throws Excepciones{
		return this.cargaDao.getCumulo(inCanal, inRamo, inPoliza, inIdVenta);
	}

	public void reporteErrores(String rutaTemporal, String rutaReporte, Map<String, Object> inParams) throws Excepciones {
		this.cargaDao.reporteErrores(rutaTemporal, rutaReporte, inParams);
	}
	
	public void estatusCancelacion(List<BeanClienteCertif> listaCertificados, List<BeanClienteCertif> listaCertificadosCancelacion) throws Excepciones {
//		message.append("estatusCancelacion.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			this.cargaDao.estatusCancelacion(listaCertificados, listaCertificadosCancelacion);
		} catch (Exception e) {
			message.append("Error en el proceso de cancelación masiva.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public List<Object> estatusRehabilitacion(Short canal, Short ramo, Long poliza, String nombreArchivo) throws Excepciones {
		return null;
	}
	
	public List<Object> getParametrosConciliacion(String tipoParametros) throws Exception {
		StringBuffer sbQuery;
		
		try {
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT cp ");
			sbQuery.append("FROM Parametros cp ");
			sbQuery.append("WHERE cp.copaDesParametro = 'CONCILIACION' ");
			sbQuery.append("AND cp.copaIdParametro = '").append(tipoParametros).append("'");
			
			if(tipoParametros.equals(Constantes.CONCILIACION_LE)) {
				sbQuery.append("AND cp.copaVvalor3 = 'CARGA_AUT'");
			}
			
			return cargaDao.consultarMapeo(sbQuery.toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.getParametrosConciliacion(): " + e.getMessage());
		}
	}
	
	public ArrayList<Object> consultarVentasConsumo(long tipoReg, int ramo, String strOrigen) throws Exception {
		ArrayList<Object> arlResultado;
		StringBuffer sbQuery;
		boolean desempleo = false;
		
		try {
			arlResultado = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			
			switch (ramo) {
				case Constantes.CONCILIACION_ALTAMIRA_RAMO:
				case Constantes.CONCILIACION_HIPOTECARIO_RAMO:
				case Constantes.CONCILIACION_LCI_RAMO:
					sbQuery.append("SELECT pre, mp ");
					sbQuery.append("FROM VtaPrecarga pre, FoliosMediosPago mp ");
					sbQuery.append("WHERE pre.id.idCertificado = mp.id.folio ");
					sbQuery.append("AND TO_NUMBER(SUBSTR(LPAD(pre.id.numPoliza, 10, 0), 10)) = :tiporeg ");
					sbQuery.append("AND pre.id.cdRamo = :ramo ");
					sbQuery.append("AND pre.id.sistemaOrigen = :origen ");
					sbQuery.append("AND pre.cargada = 0 ");
					sbQuery.append("AND mp.cargada = 0 ");
					sbQuery.append("AND SUBSTR(mp.id.ventaOrigen, 8) = '").append(Constantes.CUENTAS_ORIGEN_ALTAMIRA_ALTA).append("'");
					break;
					
	            case Constantes.CONCILIACION_GAP_RAMO:
					sbQuery.append("SELECT pre, mp ");
					sbQuery.append("FROM VtaPrecarga pre, FoliosMediosPago mp ");
					sbQuery.append("WHERE pre.id.idCertificado = mp.id.folio ");
					sbQuery.append("AND TO_NUMBER(SUBSTR(LPAD(pre.id.numPoliza, 10, 0), 10)) = :tiporeg ");
					sbQuery.append("AND pre.id.cdRamo = :ramo ");
					sbQuery.append("AND pre.id.sistemaOrigen = :origen ");
					sbQuery.append("AND pre.cargada = 0 ");
					sbQuery.append("AND mp.cargada = 0 ");
					sbQuery.append("AND SUBSTR(mp.id.ventaOrigen, 8) = '").append(Constantes.CUENTAS_ORIGEN_DESCONOCIDO).append("'");
					break;
					
				case Constantes.CONCILIACION_ALTAIR_RAMO:
					sbQuery.append("SELECT SUM(pre.prima), pre.tipoVenta, pre.feEstatus ");
					sbQuery.append("FROM VtaPrecarga pre ");
					sbQuery.append("WHERE TO_NUMBER(SUBSTR(LPAD(pre.id.numPoliza, 10, 0), 10)) = :tiporeg ");
					sbQuery.append("AND pre.id.cdRamo = :ramo ");
					sbQuery.append("AND pre.id.sistemaOrigen = :origen ");
					sbQuery.append("AND pre.cargada = 0 ");
					sbQuery.append("GROUP BY pre.tipoVenta, pre.feEstatus ");
					break;
					
				case Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO:
					desempleo = true;
					sbQuery.append("Select CP, F \n");
					sbQuery.append("From VtaPrecarga CP, FoliosMediosPago F \n");
					sbQuery.append("Where to_number(substr(CP.vdato3,5)) = to_number(substr(F.id.ventaOrigen,5,2)||lpad(to_char(F.id.folio),13,'0')) \n");
				    sbQuery.append("  and CP.id.sistemaOrigen = '").append(strOrigen).append("'\n");;
				    sbQuery.append("  and CP.id.cdRamo = ").append(ramo).append(" \n");
				    sbQuery.append("  and CP.cargada = ").append(Constantes.CARGADA_PARA_CONCILIAR).append("\n");
				    sbQuery.append("  and F.cargada = ").append(Constantes.CARGADA_PARA_CONCILIAR).append("\n");
				    arlResultado.addAll(cargaDao.consultarMapeo(sbQuery.toString()));
					break;
					
				case Constantes.CONCILIACION_LE_RAMO:
					sbQuery.append("SELECT pre, mp ");
					sbQuery.append("FROM VtaPrecarga pre, FoliosMediosPago mp ");
					sbQuery.append("WHERE pre.id.idCertificado = mp.nuDeContrato ");
					sbQuery.append("AND TO_NUMBER(SUBSTR(LPAD(pre.id.numPoliza, 10, 0), 10)) = :tiporeg ");
					sbQuery.append("AND pre.id.cdRamo = :ramo ");
					sbQuery.append("AND pre.id.sistemaOrigen = :origen ");
					sbQuery.append("AND pre.cargada = 0 ");
					sbQuery.append("AND mp.cargada = 0 ");
					sbQuery.append("AND ABS(mp.agencia - NVL(pre.prima, NVL(pre.prima, 0))) <= 1");    
					sbQuery.append("AND SUBSTR(mp.id.ventaOrigen, 8) = '").append(Constantes.CUENTAS_ORIGEN_ALTAMIRA_ALTA).append("'");
					break;
			}
			
			if(arlResultado != null && arlResultado.size() == 0 && !desempleo){
				arlResultado.addAll(cargaDao.consultarVentasConsumo(tipoReg, ramo, strOrigen, sbQuery.toString()));
			}
			
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCarga.consultarVentas:: " + e.getMessage());
		}
		
		return arlResultado;
	}

	public HashMap<String, String> getEstados() throws Exception {
		HashMap<String, String> hsmResultados = null;
		ArrayList<Object> arlResultado;
		StringBuffer sbQuery;
		Iterator<Object> it;
		Object[] arrEstado;
		
		try {
			hsmResultados = new HashMap<String, String>();
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT caes_cd_estado, caes_de_estado ");
			sbQuery.append("FROM cart_estados ");
			 
			arlResultado.addAll(cargaDao.consultar(sbQuery.toString()));
			it = arlResultado.iterator();
			while(it.hasNext()){
				arrEstado = (Object[]) it.next();
				hsmResultados.put(arrEstado[1].toString().trim(), arrEstado[0].toString().trim());				
			}
			
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCarga.getEstados:: " + e.getMessage());
		}
		
		return hsmResultados;
	}

	public Map<String, String> getPlazos() throws Excepciones {
		Map<String, String> hsmResultados = null;
		String[] arrCdPlazo; 
		String[] arrPlazo;
		String[] arrArchivo;
		Archivo objArchivo = new Archivo();

		try {
			hsmResultados = new HashMap<String, String>();
			StringBuilder sb = objArchivo.archivoToString("plazos.txt");
			
			arrArchivo = sb.toString().split("\\n");
			arrCdPlazo = arrArchivo[0].split(",");
			arrPlazo = arrArchivo[1].split(",");
			
			for(int i = 0; arrCdPlazo.length > i; i++){
				hsmResultados.put(arrCdPlazo[i].trim(), arrPlazo[i].trim());
			}
		} catch (Excepciones e) {
			this.log.error("Error:: ServicioCarga.getPlazos:: ", e);
			throw new Excepciones("Error:: ServicioCarga.getPlazos:: " + e.getMessage());
		}
		
		return hsmResultados;
	}

	public List<Object> getCuentas() throws Exception {
		ArrayList<Object> arlResultado;
		Iterator<Object> it;
		Parametros objParametro;
		
		try {
			arlResultado = new ArrayList<Object>();			
			it = getParametrosConciliacion(Constantes.CONCILIA_PARAMETRO_CUENTAS).iterator();
			while(it.hasNext()){
				objParametro = (Parametros) it.next();
				arlResultado.add(new SelectItem(objParametro.getCopaVvalor1(), objParametro.getCopaVvalor1()));
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCarga.getCuentas:: " + e.getMessage());
		}
		
		return arlResultado;
	}

	public ArrayList<ArrayList<Resultado>> cargadasAnteriormente(Integer inOrigen, int nRamo, String strSistemaOrigen, Date fechaIni, Date fechaFin, String[] arrCuentas) throws Exception{
		ArrayList<ArrayList<Resultado>> arlRegistros = null;
		StringBuffer sbQuery;
		
		try {
			sbQuery = new StringBuffer();
			switch (inOrigen) {
				case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR:
				case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR:
				case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
				case Constantes.CONCILIACION_ORIGEN_BT_VALOR:
				case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
				case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:
					sbQuery.append("SELECT COUNT(pre.VTAP_ID_CERTIFICADO), TO_DATE(pre.VTAP_FE_INGRESO, 'dd/MM/yyyy'), '").append(Constantes.CUENTA_VENTAS + "'");
						sbQuery.append("FROM VTA_PRECARGA pre ");
					sbQuery.append("WHERE TO_DATE(pre.VTAP_FE_INGRESO, 'dd/MM/yyyy') BETWEEN TO_DATE('");
						sbQuery.append(GestorFechas.formatDate(fechaIni, "dd/MM/yyyy"));
						sbQuery.append("' , 'dd/MM/yyyy') AND TO_DATE('");
						sbQuery.append(GestorFechas.formatDate(fechaFin, "dd/MM/yyyy"));
						sbQuery.append("', 'dd/MM/yyyy') AND pre.VTAP_CD_RAMO = ").append(nRamo);
						sbQuery.append(" AND pre.VTAP_SISTEMA_ORIGEN = '").append(strSistemaOrigen).append("' ");
					sbQuery.append("GROUP BY pre.VTAP_FE_INGRESO ");
					sbQuery.append("ORDER BY 2 DESC");
					
					arlRegistros = separaFechasCargadasAnteriormente(cargaDao.consultar(sbQuery.toString()), GestorFechas.obtenerFechas(fechaIni, fechaFin), Constantes.CUENTA_VENTAS);
					break;
					
				case Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR:
					arlRegistros = new ArrayList<ArrayList<Resultado>>();
					
					for(int i = 0; arrCuentas.length > i; i++) {
						sbQuery = new StringBuffer();
						sbQuery.append("SELECT COUNT(CC.COCU_NO_CREDITO), CC.COCU_FECHA_INGRESO, CC.COCU_CUENTA_PAGO  ");
							sbQuery.append("FROM COLECTIVOS_CUENTAS CC ");
						sbQuery.append("WHERE CC.COCU_FECHA_INGRESO BETWEEN TO_DATE('");
							sbQuery.append(GestorFechas.formatDate(fechaIni, "dd/MM/yyyy"));
							sbQuery.append("' , 'dd/MM/yyyy') AND TO_DATE('");
							sbQuery.append(GestorFechas.formatDate(fechaFin, "dd/MM/yyyy"));
							sbQuery.append("', 'dd/MM/yyyy') AND CC.COCU_CUENTA_PAGO = ").append(arrCuentas[i]);
						sbQuery.append("GROUP BY CC.COCU_FECHA_INGRESO, CC.COCU_CUENTA_PAGO ");
						sbQuery.append("ORDER BY 3, 2 DESC");
						
						arlRegistros.addAll(separaFechasCargadasAnteriormente(cargaDao.consultar(sbQuery.toString()), GestorFechas.obtenerFechas(fechaIni, fechaFin), arrCuentas[i]));
					}
					
					break;
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCarga.consultarVentas:: " + e.getMessage());
		}
		
		return arlRegistros;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getPreEmisionDesempleo(Date fechaInicial, Date fechaFinal){
		StringBuffer sbQuery = null;
		List<Object> datos = null;
		List<Object> resultados =  null;
		
		try {
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT count(VP.VTAP_ID_CERTIFICADO), VP.VTAP_FE_CARGA \n");
			sbQuery.append("FROM VTA_PRECARGA VP \n");
			sbQuery.append("WHERE VP.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_DESEMPLEO_TDC).append("' \n");
			sbQuery.append("  AND VP.VTAP_CARGADA = 0 \n");
			sbQuery.append("  AND TO_DATE(VP.VTAP_FE_CARGA, 'dd/MM/yyyy') between TO_DATE('")
				   .append(new SimpleDateFormat("dd/MM/yyyy").format(fechaInicial)).append("','dd/MM/yyyy')");
			sbQuery.append("  AND TO_dATE('").append(new SimpleDateFormat("dd/MM/yyyy").format(fechaFinal)).append("','dd/MM/yyyy')\n");
			sbQuery.append("GROUP BY VP.VTAP_FE_CARGA");
			
			datos =  (List<Object>) cargaDao.consultar(sbQuery.toString());
			if(datos != null && datos.size() > 0){
				resultados = new ArrayList<Object>();
				for(Object obj: datos){
					Object[] dato = (Object[]) obj;
					Resultado resultado = new Resultado();
					resultado.setFecha(new SimpleDateFormat("dd/MM/yyyy").parse(dato[1].toString()));
					resultado.setDescripcion(Constantes.REGISTROS_CARGADOS);
					resultado.setCuenta(dato[0].toString());
					resultados.add(resultado);
				}
			}
		} catch (Exception e) {
			log.error("Error al consultar pre emision de desempleo tdc.", e);
		}
		
		return resultados;
	}
	
	@SuppressWarnings("rawtypes")
	private ArrayList<ArrayList<Resultado>> separaFechasCargadasAnteriormente(Collection<? extends Object> lstRegistrosCargados, ArrayList<Date> arlRangoFechas, String strNumCuenta) throws Exception{
		ArrayList<ArrayList<Resultado>> arlResultados;
		ArrayList<Resultado> arlFechasCargadas;
		ArrayList<Resultado> arlFechasNoCargadas;
		Iterator<Date> it1;
		Iterator it2;
		Date objDate;
		Object[] arrRegistro;
		boolean cargada = false;
		
		try {
			arlResultados = new ArrayList<ArrayList<Resultado>>();
			arlFechasCargadas = new ArrayList<Resultado>();
			arlFechasNoCargadas = new ArrayList<Resultado>();
			
			it1 = arlRangoFechas.iterator();
			while(it1.hasNext()){
				objDate = it1.next();
				it2 = lstRegistrosCargados.iterator();
				while (it2.hasNext()) {
					arrRegistro = (Object[]) it2.next();
					if(((Timestamp) arrRegistro[1]).compareTo(objDate) == 0 && arrRegistro[2].toString().equals(strNumCuenta)){
						arlFechasCargadas.add(new Resultado(objDate, Constantes.REGISTROS_EXISTENTES, arrRegistro[2].toString()));
						cargada = true;
						break;
					} 
				}
				
				if(!cargada){
					arlFechasNoCargadas.add(new Resultado(strNumCuenta, objDate));
				}
				cargada = false;
			}
			
			if(arlFechasNoCargadas.isEmpty()){
				arlFechasNoCargadas.add(new Resultado(strNumCuenta, ""));
			}
			
			arlResultados.add(0, arlFechasNoCargadas);
			arlResultados.add(1, arlFechasCargadas);
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCarga.consultarVentas:: " + e.getMessage());
		}
		
		return arlResultados;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Resultado> buscarRegistrosGuardados(Integer inOrigen, int nRamo, String strSistemaOrigen, ArrayList<Resultado> arlFechas) throws Exception {
		ArrayList<Resultado> arlResultado;
		ArrayList<Object[]> lstRegistrosCargados;
		StringBuffer sbQuery;
		Iterator<Resultado> it;
		Resultado objResultado;
		String tempCuenta = null;
		
		try {
			arlResultado = new ArrayList<Resultado>();
			it = arlFechas.iterator();
			while(it.hasNext()){
				objResultado = it.next();
				sbQuery = new StringBuffer();
				
				if(objResultado.getFecha() != null){
					if(!objResultado.getCuenta().equals(tempCuenta)){
						switch (inOrigen) {
							case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR:
							case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR:
							case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
							case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
							case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:
							case Constantes.CONCILIACION_ORIGEN_BT_VALOR:
								sbQuery.append("SELECT COUNT(pre.VTAP_ID_CERTIFICADO), TO_DATE(pre.VTAP_FE_INGRESO, 'dd/MM/yyyy'), '").append(Constantes.CUENTA_VENTAS + "'");
									sbQuery.append("FROM VTA_PRECARGA pre ");
								sbQuery.append("WHERE pre.VTAP_FE_INGRESO = '");
									sbQuery.append(GestorFechas.formatDate(objResultado.getFecha(), "dd/MM/yyyy"));
									sbQuery.append("' AND pre.VTAP_CD_RAMO = ").append(nRamo);
									sbQuery.append(" AND pre.VTAP_SISTEMA_ORIGEN = '").append(strSistemaOrigen).append("' ");
								sbQuery.append("GROUP BY pre.VTAP_FE_INGRESO ");					
								break;
								
							case Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR:
								sbQuery.append("SELECT COUNT(CC.COCU_NO_CREDITO), CC.COCU_FECHA_INGRESO, CC.COCU_CUENTA_PAGO ");
									sbQuery.append("FROM COLECTIVOS_CUENTAS CC ");
								sbQuery.append("WHERE CC.COCU_FECHA_INGRESO = TO_DATE('");
									sbQuery.append(GestorFechas.formatDate(objResultado.getFecha(), "dd/MM/yyyy")).append("', 'dd/MM/yyyy')");
									sbQuery.append(" AND CC.COCU_CUENTA_PAGO = ").append(objResultado.getCuenta());
								sbQuery.append("GROUP BY CC.COCU_FECHA_INGRESO, CC.COCU_CUENTA_PAGO ");					
								break;
						}
						lstRegistrosCargados = (ArrayList<Object[]>) cargaDao.consultar(sbQuery.toString());
					
						if(lstRegistrosCargados.isEmpty()){					
							objResultado.setDescripcion(Constantes.REGISTROS_NO_CARGADOS);
						} else {
							objResultado.setDescripcion(lstRegistrosCargados.get(0)[0].toString() + Constantes.REGISTROS_CARGADOS);
						}
						arlResultado.add(objResultado);
					}
				} else {
					tempCuenta = objResultado.getCuenta();
				}
			}
		} catch (Exception e) {
			throw new Exception("Error:: ServicioCarga.buscarRegistrosGuardados:: " + e.getMessage());
		}
		
		return arlResultado;
	}
	
	public void conciliarVentas(HashMap<Integer, ArrayList<Object>> hmConciliar) throws Exception {
		ArrayList<Object> arlConciliados;
		ArrayList<Object> listaDesempleo = null;
		Parametros objParam;
		int nDatoRamos = 0;
		
		try {
			arlConciliados = new ArrayList<Object>();
			
			if(hmConciliar.containsKey(Constantes.INDEX_CONCILIA_ALTAMIRA)){
				objParam = (Parametros) getParametrosConciliacion(Constantes.CONCILIACION_ALTAMIRA).get(0);
				arlConciliados.addAll(conciliarAltamira(hmConciliar.get(Constantes.INDEX_CONCILIA_ALTAMIRA), objParam));
				nDatoRamos = objParam.getCopaNvalor4();
				if(!arlConciliados.isEmpty()) {
					separarRamos(arlConciliados, nDatoRamos);
				}
			}
			
			if(hmConciliar.containsKey(Constantes.INDEX_CONCILIA_ALTAIR)){
				objParam = (Parametros)getParametrosConciliacion(Constantes.CONCILIACION_ALTAIR).get(0);
				arlConciliados.addAll(conciliarAltair(hmConciliar.get(Constantes.INDEX_CONCILIA_ALTAIR), objParam));
				nDatoRamos = objParam.getCopaNvalor4();
				if(!arlConciliados.isEmpty()) {
					separarRamos(arlConciliados, nDatoRamos);
				}
			}
			
			if(hmConciliar.containsKey(Constantes.INDEX_CONCILIA_HPU)) {
				objParam = (Parametros)getParametrosConciliacion(Constantes.CONCILIACION_HIPOTECARIO).get(0);
				arlConciliados.addAll(conciliarHipotecario(hmConciliar.get(Constantes.INDEX_CONCILIA_HPU), objParam));
				nDatoRamos = objParam.getCopaNvalor4();
				if(!arlConciliados.isEmpty()) {
					separarRamos(arlConciliados, nDatoRamos);
				}
			}
			
			if(hmConciliar.containsKey(Constantes.INDEX_CONCILIA_LCI)) {
				objParam = (Parametros)getParametrosConciliacion(Constantes.CONCILIACION_LCI).get(0);
				arlConciliados.addAll(conciliarLCI(hmConciliar.get(Constantes.INDEX_CONCILIA_LCI), objParam));
				nDatoRamos = objParam.getCopaNvalor4();
				if(!arlConciliados.isEmpty()) {
					separarRamos(validacionesLCI(arlConciliados, nDatoRamos), nDatoRamos);
				}
			}

			if(hmConciliar.containsKey(Constantes.INDEX_CONCILIA_GAP)) {
				objParam = (Parametros)getParametrosConciliacion(Constantes.CONCILIACION_GAP).get(0);
				arlConciliados.addAll(conciliarGAP(hmConciliar.get(Constantes.INDEX_CONCILIA_GAP), objParam));
				nDatoRamos = objParam.getCopaNvalor4();
				if(!arlConciliados.isEmpty()) {
					separarRamos(arlConciliados, nDatoRamos);
				}
				divisionAutoCredito();
			}
			
			if(hmConciliar.containsKey(Constantes.INDEX_CONCILIA_DESEMPLEO)){
				listaDesempleo = hmConciliar.get(Constantes.INDEX_CONCILIA_DESEMPLEO);
				conciliarDesempleo(listaDesempleo);				
			}
			
			if(hmConciliar.containsKey(Constantes.INDEX_CONCILIA_LE)) {
				StringBuilder sbMsgCorreo = new StringBuilder();
				objParam = (Parametros)getParametrosConciliacion(Constantes.CONCILIACION_LE).get(0);
				arlConciliados.addAll(conciliarLE(hmConciliar.get(Constantes.INDEX_CONCILIA_LE), objParam));
				nDatoRamos = objParam.getCopaNvalor4();
				if(!arlConciliados.isEmpty()) {
					separarRamos(arlConciliados, nDatoRamos);
				}
				sbMsgCorreo.append("Buen Dia. \n");
				sbMsgCorreo.append("Se envia errores de Conciliacion de LE. \n\n\n");
				enviarErroresLE(Constantes.CODIGO_ERROR_CONCILIACION, sbMsgCorreo, "Errores Conciliacion LE");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarVentas:: " + e.getMessage());
		}
	}

	public void conciliarManualmente(ArrayList<Object> arlDatos) throws Exception {
		Iterator<Object> it;
		Object[] arrDatos;
		PreCarga objPrecarga;
		
		try {
			it = arlDatos.iterator();
			while(it.hasNext()) {
				arrDatos = (Object[]) it.next();
				objPrecarga = new PreCarga(arrDatos);
				cargaDao.guardarObjeto(objPrecarga);
				
				cargaDao.actualizarCargasManuales(objPrecarga.getId().getCopcIdCertificado(), arrDatos[33].toString(), arrDatos[34].toString(), arrDatos[35].toString(), arrDatos[36].toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarManualmente:: " + e.getMessage());
		}
	}
	
	
	private Collection<? extends Object> conciliarLE(ArrayList<Object> arlConLE, Parametros objParam) throws Exception {
		ArrayList<Object> arlConciliados;
		Iterator<Object> it;
		Iterator itHash;
		VtaPrecarga objVentas;
		FoliosMediosPago objDepositos;
		Object[] arrObjetos;
		int nConseError = 0;
		HashMap<String, ArrayList<Object>> hmDatosResti = null;
		HashMap<String, ArrayList<Object>> hmDatosMulti = null;
		
		try {
			arlConciliados = new ArrayList<Object>();
			hmDatosResti = new HashMap<String, ArrayList<Object>>();
			hmDatosMulti = new HashMap<String, ArrayList<Object>>();
			
			it = arlConLE.iterator();
			while(it.hasNext()){
				arrObjetos = (Object[]) it.next();
				objVentas = (VtaPrecarga) arrObjetos[0];
				
				if(Integer.valueOf(objVentas.getTipoVenta()) == Constantes.TIPO_VENTA_LEX) { //IGUAL A 8 - MULTIDISPOSICION
					if(hmDatosMulti.containsKey(objVentas.getId().getIdCertificado() + objVentas.getSumaAsegurada())) {
						hmDatosMulti.get(objVentas.getId().getIdCertificado() + objVentas.getSumaAsegurada()).add(arrObjetos);
					} else {
						arlConciliados.add(arrObjetos);
						hmDatosMulti.put(objVentas.getId().getIdCertificado() + objVentas.getSumaAsegurada(), arlConciliados);
					}
				} else { //IGUAL A 7 - RESTITUCION
					if(hmDatosResti.containsKey(objVentas.getId().getIdCertificado())) {
						hmDatosResti.get(objVentas.getId().getIdCertificado()).add(arrObjetos);
					} else {
						arlConciliados.add(arrObjetos);
						hmDatosResti.put(objVentas.getId().getIdCertificado(), arlConciliados);
					}
				}
				arlConciliados = new ArrayList<Object>();
			}
			
			arlConciliados = new ArrayList<Object>();
			//SE HACEN OPERACIONES PARA VENTAS DE RESTITUCION.
			itHash = hmDatosResti.entrySet().iterator();
			while(itHash.hasNext()) {
				Map.Entry<String, ArrayList<Object>> e = (Entry<String, ArrayList<Object>>) itHash.next();
				//SI llegan varios registros con mismo IDCERTIFICADO o CREDITO se manejan como ERRORES DE DUPLICADO.
				if(e.getValue().size() > 1){
					ArrayList<Object> arlRepRest = e.getValue(); 
					Iterator<Object> itRepRest = arlRepRest.iterator();
					while(itRepRest.hasNext()) {
						arrObjetos = (Object[]) itRepRest.next();
						objVentas = (VtaPrecarga) arrObjetos[0];
						objDepositos = (FoliosMediosPago) arrObjetos[1];
						
						objVentas.setCargada(Constantes.CARGADA_ERROR);
						objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
						objVentas.setCdError(Constantes.ERROR_014_ID);
						objVentas.setDesError(Constantes.ERROR_014_DESC);
						cargaDao.actualizarObjeto(objVentas);
							 
						objDepositos.setCargada(new Integer(Constantes.CARGADA_ERROR));
						objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
						cargaDao.actualizarObjeto(objDepositos);
						
						nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, Constantes.ERROR_014_DESC, nConseError);
						nConseError++;
					}
					arlRepRest.clear();
					arlRepRest = null;
				} else {
					arrObjetos = (Object[]) e.getValue().get(0);
					objVentas = (VtaPrecarga) arrObjetos[0];
					objDepositos = (FoliosMediosPago) arrObjetos[1];
					
					if(objParam.getCopaNvalor2().equals(0L)){ //CONCILIACION POR CREDITO
						ArrayList<Object> arlDatos = conciliaxCredito(objVentas, objDepositos, objParam.getCopaNvalor1());
						if(arlDatos.isEmpty()) { //No coincide las primas.
							objVentas.setCargada(Constantes.CARGADA_ERROR);
							objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							objVentas.setCdError(Constantes.ERROR_009_ID);
							objVentas.setDesError(Constantes.ERROR_009_DESC);
							cargaDao.actualizarObjeto(objVentas);
								 
							objDepositos.setCargada(new Integer(Constantes.CARGADA_ERROR_PRIMAS));
							objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							cargaDao.actualizarObjeto(objDepositos);
							
							nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, Constantes.ERROR_CONCILIACION_001, nConseError);
							nConseError++;
						} else { //CONCILIADO
							arlConciliados.addAll(arlDatos);
						}	
						arlDatos.clear();
						arlDatos = null;
					} else { //CONCILIACION POR MONTO
						
					}
				}
			}
			
			//SE HACEN OPERACIONES PARA VENTAS DE MULTIDISPOSICION.
			itHash = hmDatosMulti.entrySet().iterator();
			while(itHash.hasNext()) {
				Map.Entry<String, ArrayList<Object>> e = (Entry<String, ArrayList<Object>>) itHash.next();
				if(e.getValue().size() > 1){
					ArrayList<Object> arlRepMulti = e.getValue();
					Iterator<Object> itRepMulti = arlRepMulti.iterator();
					int nCons = 1;
					while(itRepMulti.hasNext()) {
						arrObjetos = (Object[]) itRepMulti.next();
						objVentas = (VtaPrecarga) arrObjetos[0];
						objDepositos = (FoliosMediosPago) arrObjetos[1];
						objVentas.setPregunta5("D" + nCons);
						arlConciliados.addAll(conciliaxCredito(objVentas, objDepositos, objParam.getCopaNvalor1()));
						nCons++;
					}
					arlRepMulti.clear();
					arlRepMulti = null;
				} else {
					arrObjetos = (Object[]) e.getValue().get(0);
					objVentas = (VtaPrecarga) arrObjetos[0];
					objDepositos = (FoliosMediosPago) arrObjetos[1];
					
					if(objParam.getCopaNvalor2().equals(0L)){ //CONCILIACION POR CREDITO
						ArrayList<Object> arlDatos = conciliaxCredito(objVentas, objDepositos, objParam.getCopaNvalor1());
						if(arlDatos.isEmpty()) { //No coincide las primas.
							objVentas.setCargada(Constantes.CARGADA_ERROR);
							objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							objVentas.setCdError(Constantes.ERROR_009_ID);
							objVentas.setDesError(Constantes.ERROR_009_DESC);
							cargaDao.actualizarObjeto(objVentas);
								 
							objDepositos.setCargada(new Integer(Constantes.CARGADA_ERROR_PRIMAS));
							objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							cargaDao.actualizarObjeto(objDepositos);
							
							nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, Constantes.ERROR_CONCILIACION_001, nConseError);
							nConseError++;
						} else { //CONCILIADO
							arlConciliados.addAll(arlDatos);
						}
						arlDatos.clear();
						arlDatos = null;
					} else { //CONCILIACION POR MONTO
						
					}
				}
			}
			
			//PARA QUITAR OBJETOS DUPLICADOS
			HashSet<Object> hs = new HashSet<Object>();
			hs.addAll(arlConciliados);
			arlConciliados.clear();
			arlConciliados.addAll(hs);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarLE::" + e.getMessage());
		}
		
		return arlConciliados;
	}

	private ArrayList<Object> conciliarAltamira(ArrayList<Object> arlConAltamira, Parametros objParametros) throws Exception{
		ArrayList<Object> arlConciliados;
		Iterator<Object> it;
		VtaPrecarga objVentas;
		FoliosMediosPago objDepositos;
		Object[] arrObjetos;
		
		try {
			arlConciliados = new ArrayList<Object>();
			it = arlConAltamira.iterator();
			while(it.hasNext()){
				arrObjetos = (Object[]) it.next();
				objVentas = (VtaPrecarga) arrObjetos[0];
				objDepositos = (FoliosMediosPago) arrObjetos[1];
				
				if(objParametros.getCopaNvalor2().equals(0L)){
					//CONCILIACION POR CREDITO
					arlConciliados.addAll(conciliaxCredito(objVentas, objDepositos, objParametros.getCopaNvalor1()));
				} else {
					//CONCILIACION POR MONTO
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarAltamira::" + e.getMessage());
		}
		
		return arlConciliados;
	}
	
    private ArrayList<Object> conciliarGAP(ArrayList<Object> arlConGAP, Parametros objParametros) throws Exception{
		ArrayList<Object> arlConciliados;
		Iterator<Object> it;
		VtaPrecarga objVentas;
		FoliosMediosPago objDepositos;
		Object[] arrObjetos;
		
		try {
			arlConciliados = new ArrayList<Object>();
			it = arlConGAP.iterator();
			while(it.hasNext()){
				arrObjetos = (Object[]) it.next();
				objVentas = (VtaPrecarga) arrObjetos[0];
				objDepositos = (FoliosMediosPago) arrObjetos[1];
				
				if(objParametros.getCopaNvalor2().equals(0L)){
					//CONCILIACION POR CREDITO
					arlConciliados.addAll(conciliaxCredito(objVentas, objDepositos, objParametros.getCopaNvalor1()));
				} else {
					//CONCILIACION POR MONTO
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarGAP::" + e.getMessage());
		}
		
		return arlConciliados;
	}
	
	private ArrayList<Object> conciliarAltair(ArrayList<Object> arlConAltamira, Parametros objParametros) throws Exception{
		ArrayList<Object> arlConciliados;
		Iterator<Object> itVentas;
		Object[] arrVentas;
		
		try {
			arlConciliados = new ArrayList<Object>();
			itVentas = arlConAltamira.iterator();
			while(itVentas.hasNext()){
				arrVentas = (Object[]) itVentas.next();
				if(objParametros.getCopaNvalor2().equals(0L)){
					//CONCILIACION POR TIPO POLIZA
				} else {
					//CONCILICACION POR MONTO
					arlConciliados.addAll(conciliaxMonto(null, arrVentas[0].toString(), arrVentas[1].toString(), arrVentas[2].toString(), objParametros.getCopaNvalor1(), Constantes.CUENTAS_ORIGEN_ALTAIR));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarAltair::" + e.getMessage());
		}
		
		return arlConciliados;
	}
	
	private ArrayList<Object>  conciliarHipotecario(ArrayList<Object> arlConHipotecario, Parametros objParametros) throws Exception {
		ArrayList<Object> arlConciliados;
		Iterator<Object> it;
		VtaPrecarga objVentas;
		FoliosMediosPago objDepositos;
		Object[] arrObjetos;
		Double nSumaAseg, nBaseCalculo, nPrima;
		boolean flgConcilado = true;
		int nConseError = 0;
		try {
			arlConciliados = new ArrayList<Object>();
			it = arlConHipotecario.iterator();
			while(it.hasNext()){
				arrObjetos = (Object[]) it.next();
				objVentas = (VtaPrecarga) arrObjetos[0];
				objDepositos = (FoliosMediosPago) arrObjetos[1];
				flgConcilado = true;
				
				if(objParametros.getCopaNvalor2().equals(0L)){
					//CONCILIACION POR CREDITO
					if(new Double(objVentas.getPrima()) >= new Double(objDepositos.getAgencia()) - objParametros.getCopaNvalor1() && new Double(objVentas.getPrima()) <= new Double(objDepositos.getAgencia()) + objParametros.getCopaNvalor1()){
						if(new Double(objVentas.getRelacionBen()) == 0 && !objVentas.getPregunta2().equals("SI")) {	//ADQUISION - REMODELACION
							nSumaAseg = new Double(objVentas.getSexoBen());
							nBaseCalculo = new Double(nSumaAseg - new Double(objVentas.getOcupacionAseg()));
							nPrima = (nBaseCalculo * objVentas.getNdato8()) / 1000;
							if(!(new Double(objVentas.getPrima()) >= nPrima - objParametros.getCopaNvalor1() && new Double(objVentas.getPrima()) <= nPrima + objParametros.getCopaNvalor1())) {
								objVentas.setCargada(Constantes.CARGADA_ERROR);
								objVentas.setCdError(Constantes.ERROR_007_ID);
								objVentas.setDesError(Constantes.ERROR_007_DESC);
								cargaDao.actualizarObjeto(objVentas);
								
								nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objVentas.getDesError(), nConseError);
								nConseError++;
								flgConcilado = false;
							}
						} else {	// SUSTITUCION -  CONVENIO MODIFICATORIO
							nSumaAseg = new Double(objVentas.getSexoBen()) + new Double(objVentas.getOcupacionAseg());
							nBaseCalculo = new Double(nSumaAseg - new Double(objVentas.getOcupacionAseg()));
							nPrima = (nBaseCalculo * objVentas.getNdato8()) / 1000;
							if(!(new Double(objVentas.getPrima()) >= nPrima - objParametros.getCopaNvalor1() && new Double(objVentas.getPrima()) <= nPrima + objParametros.getCopaNvalor1())) {
								nBaseCalculo = new Double(objVentas.getRelacionAseg());
								nPrima = (nBaseCalculo * objVentas.getNdato8()) / 1000;
								if(!(new Double(objVentas.getPrima()) >= nPrima - objParametros.getCopaNvalor1() && new Double(objVentas.getPrima()) <= nPrima + objParametros.getCopaNvalor1())) {
									objVentas.setCargada(Constantes.CARGADA_ERROR);
									objVentas.setCdError(Constantes.ERROR_007_ID);
									objVentas.setDesError(Constantes.ERROR_007_DESC);
									cargaDao.actualizarObjeto(objVentas);
									
									nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objVentas.getDesError(), nConseError);
									nConseError++;
									flgConcilado = false;
								}
							}
						}

						if(flgConcilado) {
							if(objVentas.getCazbCdSucursal().equals(Constantes.CONCILIACION_SUCURSAL_HPU_GE)){
								objVentas.setVdato4(objVentas.getVdato4() + Constantes.CONCILIACION_IDENTIFICADOR_HPU_GE);
							}
							objVentas.setEdoCivilAseg(nPrima.toString());	//PRIMA CALCULADA
							objVentas.setSumaAsegurada(nSumaAseg.toString());
//							objVentas.setVdato6(new Double(nSumaAseg - new Double(objVentas.getOcupacionAseg())).toString());
							objVentas.setVdato6(nBaseCalculo.toString());
							objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							objVentas.setCargada(Constantes.CARGADA_CONCILIADO);
							cargaDao.actualizarObjeto(objVentas);
							 
							objDepositos.setCargada(new Integer(Constantes.CARGADA_CONCILIADO));
							objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							cargaDao.actualizarObjeto(objDepositos);
							arlConciliados.add(objVentas);
						}
					} else {
						objVentas.setCargada(Constantes.CARGADA_ERROR);
						objVentas.setCdError(Constantes.ERROR_009_ID);
						objVentas.setDesError(Constantes.ERROR_009_DESC);
						cargaDao.actualizarObjeto(objVentas);
						
						nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objVentas.getDesError(), nConseError);
						nConseError++;
					}
				} else {
					//CONCILIACION POR MONTO
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarHipotecario::" + e.getMessage());
		}
		
		return arlConciliados;
	}
		
	private ArrayList<Object> conciliarLCI(ArrayList<Object> arlConLCI, Parametros objParametros) throws Exception{
		ArrayList<Object> arlConciliados;
		Iterator<Object> it;
		Iterator itHash;
		VtaPrecarga objVentas;
		FoliosMediosPago objDepositos;
		Object[] arrObjetos;
		int nConseError = 0;
		HashMap<String, ArrayList<Object>> hmDatos = null;
		
		try {			
			hmDatos = new HashMap<String, ArrayList<Object>>();
			arlConciliados = new ArrayList<Object>();
			it = arlConLCI.iterator();
			while(it.hasNext()){
				arrObjetos = (Object[]) it.next();
				objVentas = (VtaPrecarga) arrObjetos[0];
				if(hmDatos.containsKey(objVentas.getId().getIdCertificado())) {
					hmDatos.get(objVentas.getId().getIdCertificado()).add(arrObjetos);
				} else {
					arlConciliados.add(arrObjetos);
					hmDatos.put(objVentas.getId().getIdCertificado(), arlConciliados);
				}
				arlConciliados = new ArrayList<Object>();
			}
			
			
			arlConciliados = new ArrayList<Object>();
			itHash = hmDatos.entrySet().iterator();
			while(itHash.hasNext()) {
				Map.Entry<String, ArrayList<Object>> e = (Entry<String, ArrayList<Object>>) itHash.next();
				//SI llegan varios registros con mismo IDCERTIFICADO o CREDITO se manejan como ERRORES DE DUPLICADO.
				if(e.getValue().size() > 1){
					Iterator<Object> itRep = e.getValue().iterator();
					while(itRep.hasNext()) {
						arrObjetos = (Object[]) itRep.next(); //e.getValue().get(0);
						objVentas = (VtaPrecarga) arrObjetos[0];
						objDepositos = (FoliosMediosPago) arrObjetos[1];
						
						objVentas.setCargada(Constantes.CARGADA_ERROR);
						objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
						objVentas.setCdError(Constantes.ERROR_014_ID);
						objVentas.setDesError(Constantes.ERROR_014_DESC);
						cargaDao.actualizarObjeto(objVentas);
							 
						objDepositos.setCargada(new Integer(Constantes.CARGADA_ERROR));
						objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
						cargaDao.actualizarObjeto(objDepositos);
						
						nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objVentas.getDesError(), nConseError);
						nConseError++;
					}
				} else {
					arrObjetos = (Object[]) e.getValue().get(0);
					objVentas = (VtaPrecarga) arrObjetos[0];
					objDepositos = (FoliosMediosPago) arrObjetos[1];
					
					if(objParametros.getCopaNvalor2().equals(0L)){ //CONCILIACION POR CREDITO
						ArrayList<Object> arlDatos = conciliaxCredito(objVentas, objDepositos, objParametros.getCopaNvalor1());
						if(arlDatos.isEmpty()) { //No coincide las primas.
							objVentas.setCargada(Constantes.CARGADA_ERROR);
							objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							objVentas.setCdError(Constantes.ERROR_009_ID);
							objVentas.setDesError(Constantes.ERROR_009_DESC);
							cargaDao.actualizarObjeto(objVentas);
								 
							objDepositos.setCargada(new Integer(Constantes.CARGADA_ERROR_PRIMAS));
							objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
							cargaDao.actualizarObjeto(objDepositos);
							
							nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, Constantes.ERROR_CONCILIACION_001, nConseError);
							nConseError++;
						} else { //CONCILIADO
							arlConciliados.addAll(arlDatos);
						}					
					} else { //CONCILIACION POR MONTO
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliarLCI::" + e.getMessage());
		}
		
		return arlConciliados;
	}
	
	private ArrayList<Object> validacionesLCI(ArrayList<Object> arlDatos, int nDatoRamos) throws Exception {
		ArrayList<Object> arlConciliados;
		Iterator<Object> it;
		VtaPrecarga objVentas;
		Object[] arrObjetos;
		String[] arrPendientes;
		boolean error = false;
		boolean blnSumaAseg = true;
		boolean blnRfc = true;		
		boolean blnRelacionFechas = true;
		int nConseError = 0;
		String strError = "";
		
		try {
			arlConciliados = new ArrayList<Object>();
			it = arlDatos.iterator();
			while(it.hasNext()){
				objVentas = (VtaPrecarga) it.next();
				
				//Se busca en base de rechazos si el credito tiene disposicion pendiente.
				arrPendientes = buscarDisposicionPendiente(nDatoRamos, objVentas.getId().getIdCertificado());
				if(arrPendientes != null) {
					error = true;
					strError = "Credito con disposicion previa pendiente de emision, FechaIngreso: " + arrPendientes[0] + ", SaldoInsoluto: " + arrPendientes[1];
				} 
				
				if(objVentas.getSumaAsegurada() == null || objVentas.getSumaAsegurada().equals("") || objVentas.getSumaAsegurada().equals("0")) {
					error = true;
					blnSumaAseg = false;
					strError = strError + "|Campo -SumaAsegurada- Vacio.";
				} 
				if(objVentas.getTipoVenta() == null || objVentas.getTipoVenta().equals("") || objVentas.getTipoVenta().equals("0")) {
					error = true;
					blnSumaAseg = false;
					strError = strError + "|Campo -IdVenta- Vacio.";
				} 				
				if(blnSumaAseg) {
					if(Integer.parseInt(objVentas.getTipoVenta()) == Constantes.TIPO_VENTA_LCI_OP1 && Double.parseDouble(objVentas.getSumaAsegurada()) > Constantes.SUMA_ASEG_LCI_OP1) {
						error = true;
						strError = strError + "|Suma Asegurada no valida para esta opcion.";
					} else if(Integer.parseInt(objVentas.getTipoVenta()) == Constantes.TIPO_VENTA_LCI_OP2 && Double.parseDouble(objVentas.getSumaAsegurada()) > Constantes.SUMA_ASEG_LCI_OP2) {
						error = true;
						strError = strError + "|Suma Asegurada no valida para esta opcion.";
					}
				}
				blnSumaAseg = true;
				
				if(objVentas.getApPaterno() == null || objVentas.getApPaterno().equals("")) {
					error = true;
					blnRfc = false;
					strError = strError + "|Campo -ApellidoPaterno- Vacio.";
				} else {
					if(Validaciones.caracteresEspeciales(objVentas.getApPaterno())){
						error = true;
						blnRfc = false;
						strError = strError + "|Ap Paterno contiene Caracteres Especiales: " + objVentas.getApPaterno();
					}
				}
				if(objVentas.getNombre() == null || objVentas.getNombre().equals("")) {
					error = true;
					blnRfc = false;
					strError = strError + "|Campo -Nombre- Vacio.";
				} else {
					if(Validaciones.caracteresEspeciales(objVentas.getNombre())){
						error = true;
						blnRfc = false;
						strError = strError + "|Nombre contiene Caracteres Especiales: " + objVentas.getNombre();
					}
				}
				if(Validaciones.caracteresEspeciales(objVentas.getApMaterno())){
					error = true;
					blnRfc = false;
					strError = strError + "|Ap Materno contiene Caracteres Especiales: " + objVentas.getApMaterno();
				}
				if(objVentas.getFeNac() == null || objVentas.getFeNac().equals("")) {
					error = true;
					blnRfc = false;
					blnRelacionFechas = false;
					strError = strError + "|Campo -FechaNacimiento- Vacio.";
				} else {
					try {
						Date fechaNac = GestorFechas.generateDate2(objVentas.getFeNac(), "dd/MM/yyyy");
						if(GestorFechas.comparaFechas(new Date(), fechaNac) == 1){
							error = true;
							blnRfc = false;
							blnRelacionFechas = false;
							strError = strError + "|Fecha Nacimiento mayor a Fecha Actual: " + objVentas.getFeNac();
						}
					} catch (ParseException e) {
						error = true;
						blnRfc = false;
						blnRelacionFechas = false;
						strError = strError + "|Fecha Nacimiento no Valida: " + objVentas.getFeNac();
					}
				}
				if(blnRfc) {
					if(objVentas.getRfc() == null || objVentas.getRfc().equals("")) {
						objVentas.setRfc(RFC.getRFC(objVentas.getNombre(), objVentas.getApPaterno(), objVentas.getApMaterno(), objVentas.getFeNac()));
					} else {
						if(!(objVentas.getRfc().length() >= 10 || objVentas.getRfc().length() <= 13)){
							error = true;
							strError = strError + "|RFC con longitud no valida: " + objVentas.getRfc();
						}
						if(Validaciones.caracteresEspeciales(objVentas.getRfc())) {
							error = true;
							strError = strError + "|RFC contiene Caracteres Especiales: " + objVentas.getRfc();
						}
					}
				}
				blnRfc = true;
				
				if(objVentas.getSexo() == null || objVentas.getSexo().equals("")) {
					error = true;
					strError = strError + "|Campo -Sexo- Vacio.";
				} else { 
					if(Validaciones.caracteresEspeciales(objVentas.getSexo())) {
						error = true;
						strError = strError + "|Sexo contiene Caracteres Especiales: " + objVentas.getSexo();
					}
					if(!(objVentas.getSexo().equals("MA") || objVentas.getSexo().equals("FE"))) {
						error = true;
						strError = strError + "|Sexo no Valido: " + objVentas.getSexo();
					}
				}
				
				
				if(objVentas.getFeIngreso() == null || objVentas.getFeIngreso().equals("")) {
					error = true;
					blnRelacionFechas = false;
					strError = strError + "|Campo -FechaIngreso- Vacio.";
				} else {
					try {
						Date fechaIngreso = GestorFechas.generateDate2(objVentas.getFeIngreso(), "dd/MM/yyyy");
						if(GestorFechas.comparaFechas(new Date(), fechaIngreso) == 1){
							error = true;
							blnRelacionFechas = false;
							strError = strError + "|Fecha Ingreso mayor a Fecha Actual: " + objVentas.getFeIngreso();
						}
					} catch (ParseException e) {
						error = true;
						blnRelacionFechas = false;
						strError = strError + "|Fecha Ingreso no Valida: " + objVentas.getFeNac();
					}
				}
				if(GestorFechas.comparaFechas(GestorFechas.generateDate("01/01/1900", "dd/MM/yyyy"), objVentas.getDdato1()) == 0){
					error = true;
					blnRelacionFechas = false;
					strError = strError + "|Fecha Inicio de Credito no Valida: ";
				} else {
					if(GestorFechas.comparaFechas(new Date(), objVentas.getDdato1()) == 1){
						error = true;
						blnRelacionFechas = false;
						strError = strError + "|Fecha Inicio de Credito mayor a Fecha Actual: ";
					}
				}
				if(GestorFechas.comparaFechas(GestorFechas.generateDate("01/01/1900", "dd/MM/yyyy"), objVentas.getDdato2()) == 0){
					error = true;
					blnRelacionFechas = false;
					strError = strError + "|Fecha Fin de Credito no Valida: ";
				} 
//				else {
//					if(GestorFechas.comparaFechas(new Date(), objVentas.getDdato2()) == 1){
//						error = true;
//						blnRelacionFechas = false;
//						strError = strError + "|Fecha Fin de Credito mayor a Fecha Actual: ";
//					}
//				}
				if(blnRelacionFechas) {
					if(!(GestorFechas.comparaFechas(GestorFechas.generateDate(objVentas.getFeNac(), "dd/MM/yyyy"), GestorFechas.generateDate(objVentas.getFeIngreso(), "dd/MM/yyyy")) == 1 &&
						(GestorFechas.comparaFechas(GestorFechas.generateDate(objVentas.getFeIngreso(), "dd/MM/yyyy"), objVentas.getDdato1()) == 1 || GestorFechas.comparaFechas(GestorFechas.generateDate(objVentas.getFeIngreso(), "dd/MM/yyyy"), objVentas.getDdato1()) == 0) &&
						GestorFechas.comparaFechas(objVentas.getDdato1(), objVentas.getDdato2()) == 1)) {
						error = true;						
						strError = strError + "|La Relacion de Fechas no es correcta.";
					}
				}
				blnRelacionFechas = true;
				
				if(objVentas.getCp() == null || objVentas.getCp().equals("") || objVentas.getCp().equals("0")) {
					error = true;
					strError = strError + "|Campo -CodigoPostal- Vacio.";
				} else {
					if(objVentas.getCdEstado() == null || objVentas.getCdEstado().equals("") || objVentas.getCdEstado().equals("0")){
						String estado = buscarEstado(objVentas.getCp());
						if(estado == null) {
							error = true;
							strError = strError + "|No se encontro Estado con el CP: " + objVentas.getCp();
						} else {
							objVentas.setCdEstado(estado);
						}
					} 
				}
				
				if(objVentas.getVdato4() == null || objVentas.getVdato4().equals("")){
					error = true;
					strError = strError + "|Campo -Subproducto- Vacio.";
				} else {
					if(objVentas.getVdato4().length() < 4) {
						objVentas.setVdato4(Utilerias.agregarCaracteres(objVentas.getVdato4(), 3, '0', 1));
					}
				}
				
				if(objVentas.getId().getIdCertificado() == null || objVentas.getId().getIdCertificado().equals("") || objVentas.getId().getIdCertificado().equals("0")){
					error = true;
					strError = strError + "|Campo -Identificador o Prestamo- Vacio.";
				} 
				if(objVentas.getCuenta() == null || objVentas.getCuenta().equals("") || objVentas.getCuenta().equals("0")){
					error = true;
					strError = strError + "|Campo -Cuenta- Vacio.";
				} 
				if(objVentas.getNumCliente() == null || objVentas.getNumCliente().equals("") || objVentas.getNumCliente().equals("0")){
					error = true;
					strError = strError + "|Campo -NumeroCliente- Vacio.";
				}
				if(objVentas.getCalle().equals("X")){
					error = true;
					strError = strError + "|Campo -Calle- Vacio.";
				}
				if(objVentas.getColonia().equals("X")){
					error = true;
					strError = strError + "|Campo -Colonia- Vacio.";
				}
				if(objVentas.getDelmunic().equals("X")){
					error = true;
					strError = strError + "|Campo -Delg/Mun- Vacio.";
				}
				if(objVentas.getPrima() == null || objVentas.getPrima().equals("") || objVentas.getPrima().equals("0")){
					error = true;
					strError = strError + "|Campo -Prima- Vacio.";
				}
				if(objVentas.getVdato6() == null || objVentas.getVdato6().equals("") || objVentas.getVdato6().equals("0")){
					error = true;
					strError = strError + "|Campo -SaldoInsoluto- Vacio.";
				}
				if(objVentas.getVdato3() == null || objVentas.getVdato3().equals("")|| objVentas.getVdato3().equals("0")){
					error = true;
					strError = strError + "|Campo -Producto- Vacio.";
				}
				
				if(error) {
					nConseError = guardarErroresTraspaso(objVentas.getTipoVenta(), objVentas.getId().getIdCertificado(), objVentas.getFeIngreso(), objVentas.getSumaAsegurada(), objVentas.getVdato6(), Constantes.CODIGO_ERROR_VALIDACION, strError, nConseError);
					nConseError++;
					
					objVentas.setCargada(Constantes.CARGADA_ERROR);
					objVentas.setCdError(Constantes.ERROR_013_ID);
					objVentas.setDesError(Constantes.ERROR_013_DESC);
					objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
					cargaDao.actualizarObjeto(objVentas);
				} else {
					arlConciliados.add(objVentas);
				}
				
				strError = "";
				error = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.validacionesLCI::" + e.getMessage());
		}
		
		return arlConciliados;
	}
	
	private String buscarEstado(String cp) {
		ArrayList<Object> arlResultado;
		StringBuffer sbQuery;
		String strEstado = null; 
		
		try {
			arlResultado = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT DISTINCT CEI.CAIV_CAES_CD_ESTADO ");
				sbQuery.append("FROM CART_ESTADOS_IVA CEI ");				
			sbQuery.append("WHERE CEI.CAIV_CP_POBLACION = ").append(cp);
			
			arlResultado.addAll(cargaDao.consultar(sbQuery.toString()));
			strEstado = arlResultado.get(0) == null ? null : ((BigDecimal)arlResultado.get(0)).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		  	
		return strEstado;
	}

	private String[] buscarDisposicionPendiente(int tipoVenta, String idCertificado) {
		String[] arrResultado = null;
		ArrayList<Object> arlDatos;
		StringBuffer sbQuery;
		
		try {
			arlDatos = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT TO_CHAR(TTA.TRSE_FECHA_PAGO, 'dd/MM/yyyy'),  TTA.TRSE_REMESA ");
				sbQuery.append("FROM TLMK_TRASPASO_APTC TTA ");				
			sbQuery.append("WHERE TTA.TRSE_SUCURSAL = ").append(Constantes.ESTATUS_ERROR_PROCESO);
				sbQuery.append("AND TTA.TRSE_SUB_CAMPANA = ").append(tipoVenta);
				sbQuery.append("AND TTA.TRSE_CUENTA = '").append(idCertificado).append("' ");

			arlDatos.addAll(cargaDao.consultar(sbQuery.toString()));
			if(!arlDatos.isEmpty()) {
				arrResultado = (String[]) arlDatos.get(0);
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}
		  	
		return arrResultado;
	}
	
	public int guardarErroresTraspaso(String tipoVenta, String idCertificado, String strFechaIngreso, String sumaAseg, String saldoInsoluto, String tipoRegistro, String descError, int nConseError) throws Exception {
		TlmkTraspasoAptc objErrores;
		ArrayList<Object> arlResultado;
		StringBuffer sbQuery;
		String strRemesa; 
		Date fechaIngreso; 
		
		try {
			strRemesa = GestorFechas.generaRemesa(new Date());
			try {
				fechaIngreso = GestorFechas.generateDate2(strFechaIngreso, "dd/MM/yyyy");
			} catch (ParseException e) {
				fechaIngreso = null;
			}
			
			if(nConseError == 0) {				
				arlResultado = new ArrayList<Object>();			
				sbQuery = new StringBuffer();
				sbQuery.append("SELECT NVL(MAX(TTA.TRSE_NUMERO_POLIZA), 0) + 1 ");
					sbQuery.append("FROM TLMK_TRASPASO_APTC TTA ");				
				sbQuery.append("WHERE TTA.TRSE_CLAVE_PRODUCTO = ").append(Integer.parseInt(strRemesa.substring(4, 6)));
					sbQuery.append("AND TTA.TRSE_SUCURSAL = ").append(strRemesa.substring(0, 4));	
					sbQuery.append("AND TTA.TRSE_SUB_CAMPANA = ").append(tipoVenta);
					
				arlResultado.addAll(cargaDao.consultar(sbQuery.toString()));
				nConseError = ((BigDecimal)arlResultado.get(0)).intValue();  			
			}
			
			objErrores = new TlmkTraspasoAptc();
			objErrores.setId(new TlmkTraspasoAptcId(tipoRegistro, Short.parseShort(strRemesa.substring(4, 6)), nConseError, Short.parseShort(strRemesa.substring(0, 4))));
			objErrores.setTrseCampana(Constantes.ESTATUS_ERROR_PROCESO);
			objErrores.setTrseSubCampana(Short.parseShort(tipoVenta));
			objErrores.setTrseFechaCarga(new Date());
			objErrores.setTrseRegistro(descError);
			objErrores.setTrseCuenta(idCertificado);
			objErrores.setTrseFechaPago(fechaIngreso);
			objErrores.setTrseUsuario(Utilerias.redondear(new Double(sumaAseg), "2").toString());
			objErrores.setTrseRemesa(Utilerias.redondear(new Double(saldoInsoluto), "2").toString());
			cargaDao.guardarObjeto(objErrores);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.guardarErroresTraspaso::" + e.getMessage());
		}
		return nConseError;									
	}

	private boolean conciliarDesempleo(ArrayList<Object> listaDesempleo){
		Collection<?> parametros = null;
		Map<String, Object> mapParametros = null;
		StringBuilder query = null;
		VtaPrecarga vtaPrecarga = null;
		FoliosMediosPago folio = null;
		List<Object> listaVtaPrecarga = null;
		PreCarga preCarga = null;
		List<Object> listaDiferenciaPrima = null;
		int diferencia = 0;
		int diasRetraso = 0;
		Double sumaPermitida = 0.0;
		int nConseError = 0;
		short causaCancelacion = 43;
		boolean preCargaValida = true;
		boolean borrarCuenta = false;
		Object[] param = null;
		
		try {
			listaVtaPrecarga = new ArrayList<Object>();
			
//			Utilerias.resetMessage(query);
			query = new StringBuilder();
			query.append("select CP.COPA_VVALOR1, CP.COPA_VVALOR2, CP.COPA_NVALOR3, HP.HOPP_DATO1 \n");
			query.append("from colectivos_parametros CP \n");
			query.append("   inner join homologa_prodplanes HP on \n");
			query.append("        CP.COPA_VVALOR1 = HP.HOPP_PROD_BANCO_RECTOR \n");
			query.append("    and CP.COPA_VVALOR2 = HP.HOPP_PLAN_BANCO_RECTOR \n");
			//query.append("    and CP.COPA_NVALOR4 = HP.HOPP_DATO3 \n");
			query.append("  and HP.HOPP_CD_RAMO = ").append(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO).append("\n");
			query.append("  and CP.COPA_DES_PARAMETRO = 'CONCILIACION' \n");
			query.append("  and CP.COPA_ID_PARAMETRO = '").append(Constantes.CONCILIACION_DESEMPLEO_TDC).append("'\n");
			parametros = cargaDao.consultar(query.toString());
			
			if(parametros != null){
				mapParametros = new HashMap<String, Object>();
				for(Object object: parametros){
					param = (Object[]) object;
					mapParametros.put(param[3].toString(), object);
				}
			}
			
			if(listaDesempleo != null && listaDesempleo.size() > 0) {
				query = new StringBuilder();
				query.append("Select CP \n");
				query.append("from Parametros CP \n");
				query.append("where CP.copaDesParametro = 'CONCILIACION' \n");
				query.append("  and CP.copaIdParametro = 'DES_TDC' \n");
				
				listaDiferenciaPrima = consultarMapeo(query.toString());
				if(listaDiferenciaPrima != null && listaDiferenciaPrima.size() > 0){
					diferencia = ((Parametros)listaDiferenciaPrima.get(0)).getCopaNvalor1();
					diasRetraso = ((Parametros)listaDiferenciaPrima.get(0)).getCopaNvalor5();
					sumaPermitida = ((Parametros)listaDiferenciaPrima.get(0)).getCopaNvalor6();
					causaCancelacion = ((Parametros)listaDiferenciaPrima.get(0)).getCopaNvalor7().shortValue();
					listaDiferenciaPrima.clear();
					listaDiferenciaPrima = null;
				}	
				
				for(Object object: listaDesempleo) {
					Object[] datos = (Object[]) object;
					vtaPrecarga =  (VtaPrecarga) datos[0];
					folio = (FoliosMediosPago) datos[1];
					if(vtaPrecarga.getPrima() != null && Double.parseDouble(vtaPrecarga.getPrima()) > 0) {
						if((Double.parseDouble(vtaPrecarga.getPrima()) - Double.parseDouble(folio.getAgencia()))*-1 > diferencia ||
						   (Double.parseDouble(vtaPrecarga.getPrima()) - Double.parseDouble(folio.getAgencia()))*-1 > diferencia){
							vtaPrecarga.setDesError(Constantes.ERROR_009_DESC);
							vtaPrecarga.setCdError(Constantes.ERROR_009_ID);
							vtaPrecarga.setCargada(Constantes.CARGADA_ERROR);
							preCargaValida = false;
						} else {
							preCargaValida = true;
							borrarCuenta = true;
						}
					}
					
					//Validamos suma asegurada
					if(vtaPrecarga.getSumaAsegurada() != null && Double.parseDouble(vtaPrecarga.getSumaAsegurada())> sumaPermitida){
						vtaPrecarga.setDesError(String.format("%s: %s",Constantes.ERROR_012_DESC, vtaPrecarga.getSumaAsegurada()));
						vtaPrecarga.setCdError(Constantes.ERROR_012_ID);
						vtaPrecarga.setCargada(Constantes.CARGADA_ERROR);
						preCargaValida = false;
					}
					
					if(preCargaValida){
						preCarga = new PreCarga(vtaPrecarga);
						param = mapParametros != null? (Object[]) mapParametros.get(preCarga.getCopcDato2()) : null;
										
						if(param != null){
							preCarga.setCopcBcoProducto(param[0].toString());
							preCarga.setCopcSubProducto(param[1].toString());
							preCarga.getId().setCopcNumPoliza(Long.parseLong(param[2].toString()));
							preCarga.setCopcDato11(Constantes.CONCILIACION_NOMBRE_ARCHIVO);
							
//							Utilerias.resetMessage(query);
							query = new StringBuilder();
							query.append("Select C \n");
							query.append("From Certificado C \n");
							query.append("    ,ClienteCertif CC \n");
							query.append("where CC.id.coccCasuCdSucursal = 1  \n");
							query.append("  and CC.id.coccCarpCdRamo 	 = 9  \n");
							query.append("  and CC.id.coccCapoNuPoliza 	 > 0  \n");
							query.append("  and CC.id.coccNuCertificado  > 0  \n");
							query.append("  and CC.id.coccIdCertificado  = '").append(preCarga.getId().getCopcIdCertificado()).append("' \n");
							query.append("  and C.id.coceCasuCdSucursal  = CC.id.coccCasuCdSucursal \n");
							query.append("  and C.id.coceCarpCdRamo      = CC.id.coccCarpCdRamo     \n");
							query.append("  and C.id.coceCapoNuPoliza    = CC.id.coccCapoNuPoliza   \n");
							query.append("  and C.id.coceNuCertificado   = CC.id.coccNuCertificado  \n");
							query.append("  and C.coceNoRecibo > 0       \n");
							query.append("  and C.estatus.alesCdEstatus in(1,2) \n");
							List<Object> listaCertificados = consultarMapeo(query.toString());
							
							if(listaCertificados != null && listaCertificados.size() > 0){
								Certificado certificado = (Certificado) listaCertificados.get(0);
								if((GestorFechas.restarFechas(certificado.getCoceFeFinAmparo(), new SimpleDateFormat("dd/MM/yyyy").parse(preCarga.getCopcFeCarga()))) > diasRetraso){
									vtaPrecarga.setDesError(Constantes.ERROR_010_DESC);
									vtaPrecarga.setCdError(Constantes.ERROR_010_ID);
									vtaPrecarga.setCargada(Constantes.CARGADA_ERROR);
									preCargaValida = false;
									borrarCuenta = false;
									
									//Cancelar certificado por falta de pago, parametrizar causa anulacion
									procesosDao.cancelaCertif(certificado.getId(), causaCancelacion, certificado.getCoceFeFinAmparo());
									
								}								
								listaCertificados.clear();
								listaCertificados = null;
							} else {
								preCargaValida = true;
							}
						} else {
							vtaPrecarga.setDesError(Constantes.ERROR_011_DESC);
							vtaPrecarga.setCdError(Constantes.ERROR_011_ID);
							vtaPrecarga.setCargada(Constantes.CARGADA_ERROR);
							preCargaValida = false;
							borrarCuenta = false;
						}
					}	
					
					if(preCargaValida){
						borrarObjeto(vtaPrecarga);
						guardarObjeto(preCarga);						
					} else {
						actualizarObjeto(vtaPrecarga);
						nConseError = guardarErroresTraspaso(vtaPrecarga.getTipoVenta(), vtaPrecarga.getId().getIdCertificado(), vtaPrecarga.getFeIngreso(), vtaPrecarga.getSumaAsegurada(), vtaPrecarga.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, Constantes.ERROR_CONCILIACION_001, nConseError);
						nConseError++;
					}	
					
					if(borrarCuenta){
						borrarObjeto(folio);
					}
					
					borrarCuenta = false;
					preCargaValida = true;
				}// for	
			}//end if
			
			//Consultamos ventas con prima y suma asegurada cero para pasarlar a precarga
//			Utilerias.resetMessage(query);
			query = new StringBuilder();
			query.append("Select V \n");
			query.append("from VtaPrecarga V \n");
			query.append("where V.id.cdSucursal = 1 \n");
			query.append("  and V.id.cdRamo = ").append(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO).append("\n");
			query.append("  and V.id.sistemaOrigen = '").append(Constantes.CONCILIACION_DESEMPLEO_TDC).append("'\n");
			query.append("  and V.cargada = '0' \n");
			query.append("  and V.prima = '0' \n");
			query.append("  and V.sumaAsegurada = '0'");
			
			listaVtaPrecarga = consultarMapeo(query.toString());
			if(listaVtaPrecarga != null){
				param = mapParametros != null? (Object[]) mapParametros.get(preCarga.getCopcDato2()) : null;				
				for(Object o: listaVtaPrecarga){
					
					PreCarga carga = new PreCarga((VtaPrecarga)o);
					if(param != null){
						carga.setCopcBcoProducto(param[0].toString());
						carga.setCopcSubProducto(param[1].toString());
						carga.getId().setCopcNumPoliza(Long.parseLong(param[2].toString()));
						carga.setCopcDato11(Constantes.CONCILIACION_NOMBRE_ARCHIVO);
					}
					//guardamos precarga
					cargaDao.guardarObjeto(carga);
					//borramos vtaprecarga.
					cargaDao.borrarObjeto(o);
				}
			}
			return true;			
		} catch (Exception e) {
			log.error("Error al conciliar desempleo TDC.", e);
		} finally{
			if(listaVtaPrecarga != null){
				listaVtaPrecarga.clear();
				listaVtaPrecarga = null;
			}
			
			parametros = null;
			mapParametros = null;
			query = null;
		}
		
		return false;
	}
	
    private ArrayList<Object> conciliaxCredito(VtaPrecarga objVentas, FoliosMediosPago objDepositos, Integer nMasMenos) throws Exception {
		ArrayList<Object> arlConciliados;
		
		try {
			arlConciliados = new ArrayList<Object>();
			if(new Double(objVentas.getPrima()) >= new Double(objDepositos.getAgencia()) - nMasMenos && new Double(objVentas.getPrima()) <= new Double(objDepositos.getAgencia()) + nMasMenos){
				objVentas.setCargada(Constantes.CARGADA_CONCILIADO);
				objVentas.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
				cargaDao.actualizarObjeto(objVentas);
					 
				objDepositos.setCargada(new Integer(Constantes.CARGADA_CONCILIADO));
				objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
				cargaDao.actualizarObjeto(objDepositos);
				
				arlConciliados.add(objVentas);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliaxCredito::" + e.getMessage());
		}
		
		return arlConciliados;
	}
	
	private ArrayList<Object> conciliaxMonto(VtaPrecarga objVentaAltamira, String primaAltair, String tipoPolizaAltair, String fechaIngreso, Integer nRangoDinero, String strVentaOrigen) throws Exception{
		ArrayList<Object> arlConciliados, arlVentasTipoPoliza;
		FoliosMediosPago objDepositos;
		VtaPrecarga objVentaAltair;
		Iterator<Object> itDepositos, itVentas;
		Double nPrima;
		int nConseError = 0;
		
		try {
			arlConciliados = new ArrayList<Object>();
			if(objVentaAltamira != null){
				nPrima = new Double(objVentaAltamira.getPrima());
			} else {
				nPrima = new Double(primaAltair);
			}
			
			itDepositos = consultarCuentas(strVentaOrigen).iterator();
			while(itDepositos.hasNext()){
				objDepositos = (FoliosMediosPago) itDepositos.next();
				if((nPrima >= new Double(objDepositos.getAgencia()) - nRangoDinero && nPrima <= new Double(objDepositos.getAgencia()) + nRangoDinero) 
				    && objDepositos.getFechaAprobacion().getTime() == GestorFechas.generateDate(fechaIngreso, "dd/MM/yyyy").getTime()){
					if(objVentaAltamira != null){
						objVentaAltamira.setCargada(Constantes.CARGADA_CONCILIADO);
						objVentaAltamira.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
						cargaDao.actualizarObjeto(objVentaAltamira);
						arlConciliados.add(objVentaAltamira);
					} else {
						arlVentasTipoPoliza = getVentasAltairxTipoPoliza(tipoPolizaAltair, fechaIngreso);
						itVentas = arlVentasTipoPoliza.iterator();
						while(itVentas.hasNext()){
							objVentaAltair = (VtaPrecarga) itVentas.next();
							if(objVentaAltair.getVdato2().equals("0") || objVentaAltair.getVdato2().equals("1") || objVentaAltair.getVdato2().equals("4")) {								
								objVentaAltair.setCargada(Constantes.CARGADA_CONCILIADO);
								objVentaAltair.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
								arlConciliados.add(objVentaAltair);
							} else {
								objVentaAltair.setCargada(Constantes.CARGADA_ERROR);
								objVentaAltair.setCdError(Constantes.ERROR_005_ID);
								objVentaAltair.setDesError(Constantes.ERROR_005_DESC);
								
								nConseError = guardarErroresTraspaso(objVentaAltair.getTipoVenta(), objVentaAltair.getId().getIdCertificado(), objVentaAltair.getFeIngreso(), objVentaAltair.getSumaAsegurada(), objVentaAltair.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objVentaAltair.getDesError(), nConseError);
								nConseError++;
							}
							cargaDao.actualizarObjeto(objVentaAltair);
						}
					}
					 
					objDepositos.setCargada(new Integer(Constantes.CARGADA_CONCILIADO));
					objDepositos.setSemanaCanal(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
					cargaDao.actualizarObjeto(objDepositos);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.conciliaxMonto::" + e.getMessage());
		}
		
		return arlConciliados;
	}

	private ArrayList<Object> consultarCuentas(String strVentaOrigen) throws Exception{
		ArrayList<Object> arlDepositos;
		StringBuffer sbQuery;
		
		try {
			arlDepositos = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT mp ");
			sbQuery.append("FROM FoliosMediosPago mp ");
			sbQuery.append("WHERE mp.cargada = 0 ");			
			sbQuery.append("AND SUBSTR(mp.id.ventaOrigen, 8) = '").append(strVentaOrigen).append("'");
			arlDepositos.addAll(cargaDao.consultarMapeo(sbQuery.toString())); 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.consultarCuentas::" + e.getMessage());
		}
		
		return arlDepositos;
	}
	
	private ArrayList<Object> getVentasAltairxTipoPoliza(String tipoPolizaAltair, String strFechaIngreso)  throws Exception{
		ArrayList<Object> arlVentas;
		StringBuffer sbQuery;
		
		try {
			arlVentas = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			
			sbQuery.append("SELECT pre ");
			sbQuery.append("FROM VtaPrecarga pre ");
			sbQuery.append("WHERE TO_NUMBER(SUBSTR(LPAD(pre.id.numPoliza, 10, 0), 10)) = 1 ");
			sbQuery.append("AND pre.id.cdRamo = ").append(Constantes.CONCILIACION_ALTAIR_RAMO);
			sbQuery.append("AND pre.id.sistemaOrigen = '").append(Constantes.CONCILIACION_ALTAIR).append("' ");
			sbQuery.append("AND pre.cargada = '").append(Constantes.CARGADA_PARA_CONCILIAR).append("' ");
			sbQuery.append("AND pre.tipoVenta = '").append(tipoPolizaAltair).append("' ");
			sbQuery.append("AND pre.feEstatus = '").append(strFechaIngreso).append("' ");
			arlVentas.addAll(cargaDao.consultarMapeo(sbQuery.toString())); 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.getVentasAltairxTipoPoliza::" + e.getMessage());
		}
		
		return arlVentas;
	}
	
	private void separarRamos(ArrayList<Object> arlConciliados, int nDatoRamos)  throws Exception{
		ArrayList<Object> arlRamos, arlDatosTarifas;
		VtaPrecarga objVenta;
		PreCarga objColectivosPrecarga;
		int nEdad, nValidaEdad, nConseError = 0;
		Iterator<Object> it;
		
		try {
			arlRamos = cargaDao.consultarRamos(nDatoRamos);
			it = arlConciliados.iterator();
			while(it.hasNext()){
				objVenta = (VtaPrecarga) it.next();
				arlDatosTarifas = Utilerias.getTarifas(arlRamos, objVenta.getVdato3(), objVenta.getVdato4(), objVenta.getNdato8(), nDatoRamos);
				
				if(!arlDatosTarifas.isEmpty()) {
					if((Boolean) arlDatosTarifas.get(Constantes.TARIFAS_FLAG_ENCONTRO_TARIFA)){
						if(nDatoRamos != Constantes.TIPO_VENTA_GAP) { //SI ES DIFERENTE A GAP
							nEdad = GestorFechas.obtenerEdad(objVenta.getFeNac());
							objColectivosPrecarga = new PreCarga(objVenta, (Object[]) arlDatosTarifas.get(Constantes.TARIFAS_ARRAY_VIDA), Constantes.PRODUCTO_VIDA);
							nValidaEdad = validaEdad(nEdad, String.valueOf(( (Object[]) arlDatosTarifas.get(Constantes.TARIFAS_ARRAY_VIDA))[8]), String.valueOf(( (Object[]) arlDatosTarifas.get(Constantes.TARIFAS_ARRAY_VIDA))[9]), (Boolean) arlDatosTarifas.get(Constantes.TARIFAS_FLAG_INSERTA_DES)); 
							
							if(nValidaEdad == 1){ 
								objColectivosPrecarga.setCopcCargada(Constantes.CARGADA_ERROR);
								objColectivosPrecarga.setCopcRegistro(Constantes.ERROR_006_DESC_01 + objColectivosPrecarga.getCopcDato7());
								nConseError = guardarErroresTraspaso(objVenta.getTipoVenta(), objVenta.getId().getIdCertificado(), objVenta.getFeIngreso(), objVenta.getSumaAsegurada(), objVenta.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objColectivosPrecarga.getCopcRegistro(), nConseError);
								nConseError++;
								
							} else if(nValidaEdad == 2){
								objColectivosPrecarga.setCopcCargada(Constantes.CARGADA_ERROR);
								objColectivosPrecarga.setCopcRegistro(Constantes.ERROR_006_DESC_02);
								nConseError = guardarErroresTraspaso(objVenta.getTipoVenta(), objVenta.getId().getIdCertificado(), objVenta.getFeIngreso(), objVenta.getSumaAsegurada(), objVenta.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objColectivosPrecarga.getCopcRegistro(), nConseError);
								nConseError++;
							}
							
//							validarPlazo
							cargaDao.guardarObjeto(objColectivosPrecarga);
							objVenta.setVdato8(objColectivosPrecarga.getCopcPrima());

							if((Boolean) arlDatosTarifas.get(Constantes.TARIFAS_FLAG_INSERTA_DES)){
								objColectivosPrecarga = new PreCarga(objVenta, (Object[]) arlDatosTarifas.get(Constantes.TARIFAS_ARRAY_DESEMPELO), Constantes.PRODUCTO_DESEMPLEO);
								nValidaEdad = validaEdad(nEdad, String.valueOf(( (Object[]) arlDatosTarifas.get(Constantes.TARIFAS_ARRAY_DESEMPELO))[8]), String.valueOf(( (Object[])arlDatosTarifas.get(Constantes.TARIFAS_ARRAY_DESEMPELO))[9]), (Boolean) arlDatosTarifas.get(Constantes.TARIFAS_FLAG_INSERTA_DES));
								if(nValidaEdad == 1){
									objColectivosPrecarga.setCopcCargada(Constantes.CARGADA_ERROR);
									objColectivosPrecarga.setCopcRegistro(Constantes.ERROR_006_DESC_01 + objColectivosPrecarga.getCopcDato7());
									nConseError = guardarErroresTraspaso(objVenta.getTipoVenta(), objVenta.getId().getIdCertificado(), objVenta.getFeIngreso(), objVenta.getSumaAsegurada(), objVenta.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objColectivosPrecarga.getCopcRegistro(), nConseError);
									nConseError++;
								} else if(nValidaEdad == 2){
									objColectivosPrecarga.setCopcCargada(Constantes.CARGADA_ERROR);
									objColectivosPrecarga.setCopcRegistro(Constantes.ERROR_006_DESC_02);
									nConseError = guardarErroresTraspaso(objVenta.getTipoVenta(), objVenta.getId().getIdCertificado(), objVenta.getFeIngreso(), objVenta.getSumaAsegurada(), objVenta.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objColectivosPrecarga.getCopcRegistro(), nConseError);
									nConseError++;
								}
								
//								validarPlazo
								cargaDao.guardarObjeto(objColectivosPrecarga);
								objVenta.setVdato9(objColectivosPrecarga.getCopcPrima());
							}
							
							objVenta.setCargada(Constantes.CARGADA_PARA_EMITIR);
						}
						
						objVenta.setFeAprobacion(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
						cargaDao.actualizarObjeto(objVenta);
					} else {
						objVenta.setCargada(Constantes.CARGADA_ERROR);
						objVenta.setCdError(Constantes.ERROR_004_ID);
						objVenta.setDesError(Constantes.ERROR_004_DESC);
						cargaDao.actualizarObjeto(objVenta);
						nConseError = guardarErroresTraspaso(objVenta.getTipoVenta(), objVenta.getId().getIdCertificado(), objVenta.getFeIngreso(), objVenta.getSumaAsegurada(), objVenta.getVdato6(), Constantes.CODIGO_ERROR_CONCILIACION, objVenta.getDesError(), nConseError);
						nConseError++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.separarRamos::" + e.getMessage());
		}
	}

	private int validaEdad(int nEdad, String strRangoEdad, String strRangoEdad2, boolean flgVidaDes) {
		int nRegreso = 0;
		int nEdadMin, nEdadMax;
		String[] arrEdades;
		
		if(strRangoEdad.equals("null") || strRangoEdad.equals("")){
			nRegreso = 1;
		} else {			
			arrEdades = strRangoEdad.split("-");
			nEdadMin = Integer.parseInt(arrEdades[0].toString());
			nEdadMax = Integer.parseInt(arrEdades[1].toString());
			
			if(flgVidaDes == false){
				if(!(strRangoEdad2.equals("null") || strRangoEdad2.equals(""))){
					nEdadMax = Integer.parseInt(strRangoEdad2);
				} 
			}
			
			if(!(nEdad >= nEdadMin && nEdad <= nEdadMax)){
				nRegreso = 2;
			}
		}
		
		return nRegreso;
	}

	public long buscarConsecutivoAltair(String fechaAprobacion) throws Exception {
		long lngValor = 0;
		ArrayList<Object> arlResultado;
		StringBuffer sbQuery;
		
		try {
			arlResultado = new ArrayList<Object>();			
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT COUNT(fmp.FOLIO) + 1 ");
				sbQuery.append("FROM TLMK_FOLIOS_MEDIOS_PAGO fmp ");
			sbQuery.append("WHERE LPAD(fmp.FOLIO, 2) = '").append(fechaAprobacion).append("'");
			arlResultado.addAll(cargaDao.consultar(sbQuery.toString()));
			
			lngValor = ((BigDecimal)arlResultado.get(0)).longValue();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.buscarConsecutivoAltair::" + e.getMessage());
		}
			
		return lngValor;
		
	}
	
	public int getConsecutivo(Integer inOrigen) throws Exception {
		StringBuffer sbQuery;
		ArrayList<Object> arlResultado;
		int nRegistro;
		
		try {
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT TO_NUMBER(MAX(LPAD(LPAD(VP.VTAP_NUM_POLIZA,10, 0), 9))) NREGISTRO ");
			sbQuery.append("FROM VTA_PRECARGA VP ");
			sbQuery.append("WHERE TO_CHAR(TO_DATE(VP.VTAP_FE_INGRESO, 'dd/MM/yyyy'), 'yyyyMM') =  TO_CHAR(SYSDATE, 'yyyyMM') ");
			arlResultado = new ArrayList<Object>();	
			switch (inOrigen) {
				case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR:
				case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR:
					sbQuery.append(" AND VP.VTAP_CD_RAMO IN (").append(Constantes.CONCILIACION_ALTAMIRA_RAMO).append(", ").append(Constantes.CONCILIACION_ALTAIR_RAMO).append(")");
					sbQuery.append(" AND VP.VTAP_SISTEMA_ORIGEN IN ('").append(Constantes.CONCILIACION_ALTAMIRA).append("', '").append(Constantes.CONCILIACION_ALTAIR).append("')");
					break;
					
				case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
					sbQuery.append(" AND VP.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_HIPOTECARIO_RAMO);
					sbQuery.append(" AND VP.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_HIPOTECARIO).append("'");
					break;
					
				case Constantes.CONCILIACION_ORIGEN_BT_VALOR:
					sbQuery.append(" AND VP.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_BT_RAMO);
					sbQuery.append(" AND VP.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_BT).append("'");
					break;
					
				case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
					sbQuery.append(" AND VP.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_LCI_RAMO);
					sbQuery.append(" AND VP.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_LCI).append("'");
					break;
					
				case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:
					sbQuery.append(" AND VP.VTAP_CD_RAMO = ").append(Constantes.CONCILIACION_GAP_RAMO);
					sbQuery.append(" AND VP.VTAP_SISTEMA_ORIGEN = '").append(Constantes.CONCILIACION_GAP).append("'");
					break;
				
				case Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR:
					sbQuery = new StringBuffer();
					sbQuery.append("SELECT MAX(CC.COCU_CONSECUTIVO) NREGISTRO ");
					sbQuery.append("FROM COLECTIVOS_CUENTAS CC ");
					sbQuery.append("WHERE TO_CHAR(CC.COCU_FECHA_INGRESO, 'yyyyMM') = TO_CHAR(SYSDATE, 'yyyyMM') ");
					break;
					
				case Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR:
					sbQuery.append(" AND VP.VTAP_CD_RAMO IN (").append(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO).append(")");
					sbQuery.append(" AND VP.VTAP_SISTEMA_ORIGEN IN ('").append(Constantes.CONCILIACION_DESEMPLEO_TDC).append("')");
					break;
					
			}
			
			arlResultado.addAll(cargaDao.consultar(sbQuery.toString()));
			nRegistro = arlResultado.get(0) == null ? 1 : ((BigDecimal)arlResultado.get(0)).intValue() + 1;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.getConsecutivo::" + e.getMessage());
		}
		
		return nRegistro;
	}
	
	public <T> void actualizarObjetos(List<T> lista) throws Excepciones {
//		this.message.append("actualizando Objetos.");
//		this.log.debug(message.toString());
//		Utilerias.resetMessage(message);
		
		try {
			this.cargaDao.actualizarObjetos(lista);
		} catch (Exception e) {
			this.message.append("No se puede actualizar la lista de objetos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	public VtaPrecarga buscarVentaHPU(String strFolio) throws Exception {
		VtaPrecarga objResultado = null;
		ArrayList<Object> arlVentas;
		StringBuffer sbQuery;
		
		try {
			arlVentas = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			
			sbQuery.append("SELECT pre ");
			sbQuery.append("FROM VtaPrecarga pre ");
			sbQuery.append("WHERE TO_NUMBER(SUBSTR(LPAD(pre.id.numPoliza, 10, 0), 10)) = 1 ");
			sbQuery.append("AND pre.id.cdRamo = ").append(Constantes.CONCILIACION_HIPOTECARIO_RAMO);
			sbQuery.append("AND pre.id.sistemaOrigen = '").append(Constantes.CONCILIACION_HIPOTECARIO).append("' ");
			sbQuery.append("AND pre.cargada = '").append(Constantes.CARGADA_PARA_PRECONCILIAR).append("' ");
			sbQuery.append("AND pre.id.idCertificado = '").append(strFolio).append("' ");
			
			arlVentas.addAll(cargaDao.consultarMapeo(sbQuery.toString())); 
			if(!arlVentas.isEmpty()) {
				objResultado = (VtaPrecarga) arlVentas.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.buscarVentaHPU::" + e.getMessage());
		}
		
		return objResultado;
	}
	
	/**
	 * Metodo que envie errores por correo al uusario
	 * @param codigoErrorConciliacion codigoError
	 * @param sbMsgCorreo Mesanje del correo
	 * @param strTituloMsg Titulo del Correo
	 * @throws Exception
	 */
	public void enviarErroresLE(String tipoError, StringBuilder sbMsgCorreo, String strTituloMsg) throws Exception {
		ArrayList<Object> arlResultado;
		ArrayList<Object> arlRegArchivo;
		List<String> lstArchivos;
		StringBuffer sbQuery;
		Archivo objArchivo;
		String strRuta, strNombreArchivo = null;
		String[] arrColumnas = null;
		Iterator it;
		Object[] arrDatos;
		Parametros objParam;
		Date dUltimaEmision, dFechaEmision = new Date();
		long nDias;
		String strFiltro = "";
		String strFormatoFecha = "ddMMyyyy";
		
		try {
			arlResultado = new ArrayList<Object>();
			arlRegArchivo = new ArrayList<Object>();
			lstArchivos = new ArrayList<String>();
			sbQuery = new StringBuffer();
			objParam = (Parametros) getParametrosConciliacion(Constantes.CONCILIACION_LE).get(0);
			
			strRuta = ((Parametros) getParametrosConciliacion(Constantes.CONCILIA_PARAMETRO_RUTA_REPROCESO).get(0)).getCopaRegistro().split("\\|")[1];
			dUltimaEmision = GestorFechas.generateDate2(GestorFechas.formatDate(objParam.getCopaFvalor3(), "dd/MM/yyyy"), strFormatoFecha);
		
			nDias = GestorFechas.restarFechas(dUltimaEmision, dFechaEmision);			
			if(nDias <= 1) { //Ultima Emision un dia antes o el mismo dia
				strFiltro = "= '" + Constantes.CONCILIACION_NOMBRE_ARCHIVO + "_" + GestorFechas.formatDate(dFechaEmision, strFormatoFecha) + "' \n";
			} else { //Ultima Emision mas de dos dias antes
				strFiltro = "IN( '" + Constantes.CONCILIACION_NOMBRE_ARCHIVO + "_" + GestorFechas.formatDate(dFechaEmision, strFormatoFecha) + "' ";
				for (int i = 1; i < nDias; i++) {
					strFiltro = strFiltro + ", '" + Constantes.CONCILIACION_NOMBRE_ARCHIVO + "_" + GestorFechas.formatDate(GestorFechas.sumarRestasDiasAFecha(dUltimaEmision, i, Constantes.FECHA_SUMA_DIAS), strFormatoFecha) + "' ";
				}
				strFiltro = strFiltro + ") \n";
			}			
			
			if(tipoError.equals(Constantes.CODIGO_ERROR_CONCILIACION)) {
				strNombreArchivo = "ALTAS_LE_ERRORES_CONCILIACION_" + GestorFechas.formatDate(new Date(), strFormatoFecha);
				sbQuery.append("SELECT '0','', 'IDENTIFICADOR,CUENTA,PRESTAMO,APELLIDOPATERNO,APELLIDOMATERNO,NOMBRE,SEXO,FECHANACIMIENTO,NUMEROCLIENTE,RFC,CALLE,COLONIA,DELG/MUN,ESTADO,CODIGOPOSTAL,FECHAINGRESO,PRIMA,BASECALCULO,SUMAASEGURADA,TARIFA,FECHAINICIOCREDITO,FECHAFINCREDITO,PLAZO,PRODUCTO,SUBPRODUCTO,SUCURSAL,IDVENTA,ERROR' \n");
				sbQuery.append("FROM DUAL \n");
				sbQuery.append("UNION ALL \n");
				sbQuery.append("SELECT VP.VTAP_ID_CERTIFICADO, VP.VTAP_FE_INGRESO, VP.VTAP_VDATO13 || '|' || VP.VTAP_DES_ERROR \n");
				sbQuery.append("	FROM VTA_PRECARGA VP \n");
				sbQuery.append("WHERE VP.VTAP_SISTEMA_ORIGEN = 'LE' \n");
				sbQuery.append("	AND VP.VTAP_CARGADA = 3 \n");
			} else if(tipoError.equals(Constantes.CODIGO_ERROR_EMISION)) {
				strNombreArchivo = "ALTAS_LE_ERRORES_EMISION_" + GestorFechas.formatDate(new Date(), strFormatoFecha);
				sbQuery.append("SELECT '0','','IDENTIFICADOR,CUENTA,PRESTAMO,APELLIDOPATERNO,APELLIDOMATERNO,NOMBRE,SEXO,FECHANACIMIENTO,NUMEROCLIENTE,RFC,CALLE,COLONIA,DELG/MUN,ESTADO,CODIGOPOSTAL,FECHAINGRESO,PRIMA,BASECALCULO,SUMAASEGURADA,TARIFA,FECHAINICIOCREDITO,FECHAFINCREDITO,PLAZO,PRODUCTO,SUBPRODUCTO,SUCURSAL,IDVENTA,ERROR' \n");
				sbQuery.append("FROM DUAL \n");
				sbQuery.append("UNION ALL \n");
				sbQuery.append("SELECT CP.COPC_ID_CERTIFICADO, CP.COPC_FE_INGRESO, VP.VTAP_VDATO13 || '|' || NVL(CP.COPC_REGISTRO, 'PC-PRODUCTO,PLAN O TARIFA NO PARAMETRIZADA') \n"); 
				sbQuery.append("	FROM COLECTIVOS_PRECARGA CP \n");
			    sbQuery.append("	INNER JOIN VTA_PRECARGA VP ON VP.VTAP_ID_CERTIFICADO = CP.COPC_ID_CERTIFICADO \n");
				sbQuery.append("WHERE CP.COPC_CD_SUCURSAL = 1 \n");
				sbQuery.append("	AND CP.COPC_CD_RAMO = 9 \n"); 
				sbQuery.append("	AND CP.COPC_DATO11 " + strFiltro);
				sbQuery.append("	AND CP.COPC_DATO16 = 8 \n");    
				sbQuery.append("	AND VP.VTAP_FE_INGRESO = CP.COPC_FE_INGRESO \n");
				sbQuery.append("	AND VP.VTAP_CARGADA = 2 \n");
				sbQuery.append("UNION ALL \n");
				sbQuery.append("SELECT CC.COCR_ID_CERTIFICADO, TO_CHAR(CC.COCR_FE_INGRESO, 'DD/MM/YYYY'), VP.VTAP_VDATO13 || '|' || CC.COCR_REGISTRO \n"); 
				sbQuery.append("	FROM COLECTIVOS_CARGA CC \n");
				sbQuery.append("	INNER JOIN VTA_PRECARGA VP ON VP.VTAP_ID_CERTIFICADO = CC.COCR_ID_CERTIFICADO \n");
				sbQuery.append("WHERE CC.COCR_CD_SUCURSAL = 1 \n");
				sbQuery.append("	AND CC.COCR_CD_RAMO = 9 \n");
				sbQuery.append("	AND CC.COCR_DATO11 " + strFiltro);   
				sbQuery.append("	AND CC.COCR_DATO16 = 8 \n"); 
				sbQuery.append("	AND VP.VTAP_FE_INGRESO = CC.COCR_FE_INGRESO \n");
				sbQuery.append("	AND VP.VTAP_CARGADA = 2 \n");
			}
			arlResultado.addAll(cargaDao.consultar(sbQuery.toString()));
			it = arlResultado.iterator();
			while(it.hasNext()) {
				arrDatos = (Object[]) it.next();
				if(arrDatos[0].equals("0")) {
					arrColumnas = arrDatos[2].toString().split("\\|");
				} else {
//					cargaDao.actualizarCuentasLE(arrDatos[0].toString(), arrDatos[1].toString());
					arlRegArchivo.add(arrDatos[2].toString().split("\\~"));
				}
			}
			objArchivo = new Archivo(strRuta, strNombreArchivo, ".txt");
			objArchivo.copiarArchivo(arrColumnas, arlRegArchivo, "|");
			lstArchivos.add(objArchivo.getArchivo().getPath());
			cargaDao.eliminarDatosReprocesoLE(tipoError, strFiltro); //SE ELIMINAN ERRORES DE EMISION Y SE LIMPIA VTA_PRECARGA
			
			objParam.setCopaVvalor4(GestorFechas.formatDate(dFechaEmision, "ddMMyyyy"));
			cargaDao.actualizarObjeto(objParam);
			
			Mail.enviar2(lstArchivos, sbMsgCorreo, strTituloMsg, objParam.getCopaVvalor5() + objParam.getCopaVvalor7(), Constantes.DEFAULT_STRING, Constantes.DEFAULT_STRING);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.enviarErroresLE::" + e.getMessage());
		}
	}
	
	public void divisionAutoCredito() throws Exception {
		cargaDao.divisionAutoCredito();
	}
	
	public List<Object> consultarMapeo(String strQuery) throws Exception {
		try {
			return cargaDao.consultarMapeo(strQuery);
		} catch (Exception e) {
			log.error("Error al consultar mapeo.", e);
		}
		return null;
	}

	public void respaldarErrores(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta, String nombreArchivo) throws Exception {
		try {
			cargaDao.respaldarErrores(inCanal, inRamo, inPoliza, inIdVenta, nombreArchivo);
		} catch (Exception e) {
			log.error("Error al consultar mapeo.", e);
		}
	}

	public List<Object> consultaDatosPampa() throws Exception {
		ArrayList<Object> arlVentas;
		StringBuffer sbQuery;
		
		try {
			arlVentas = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			
			sbQuery.append("select '0014', vt.cuentaAlterna, '484' || '484' || '9999' || '0' || '1',");
			sbQuery.append("  to_char(sysdate, 'yyyymmdd'), '4602',");
			sbQuery.append(" LPAD(TO_CHAR(TRUNC(vt.prima, 0)) ||");
			sbQuery.append(" SUBSTR(TO_CHAR(vt.prima),");
			sbQuery.append(" INSTR(TO_CHAR(vt.prima), '.') + 1, ");
			sbQuery.append(" 2),");
			sbQuery.append(" 15,");
			sbQuery.append(" '0'),'#bt'");
			sbQuery.append(" from VtaPrecarga vt");
			sbQuery.append(" where vt.id.sistemaOrigen ='BT'");
			sbQuery.append(" and vt.estatus='0'");
			arlVentas.addAll(cargaDao.consultarMapeo(sbQuery.toString())); 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.consultaDatosPampa:: " + e.getMessage());
		}
		
		return arlVentas;
	}
	
	public List<Object> consultaDatosPampaCancelacion() throws Exception {
		ArrayList<Object> arlVentas;
		StringBuffer sbQuery;
		
		try {
			arlVentas = new ArrayList<Object>();
			sbQuery = new StringBuffer();
		
			sbQuery.append("select '0014', C.COCE_NU_CREDITO, '484' || '484' || '9999' || '1' || '1',");
		    sbQuery.append("   to_char(sysdate,'yyyymmdd'), '4602',");
		    sbQuery.append("lpad(to_char( round(C.COCE_MT_DEVOLUCION,2)*100),15,'0'),'#bt'");
		    sbQuery.append(" from COLECTIVOS_CERTIFICADOS C");
		    sbQuery.append(" where C.COCE_CASU_CD_SUCURSAL    = 1");
		    sbQuery.append(" and C.COCE_CARP_CD_RAMO          = 9");
		    sbQuery.append(" and C.COCE_CAPO_NU_POLIZA        > 600000000");
		    sbQuery.append(" and C.COCE_CAPO_NU_POLIZA        < 700000000");
		    sbQuery.append(" and C.COCE_SUB_CAMPANA           = '6'");
		    sbQuery.append(" and C.COCE_FE_ANULACION          > sysdate -1");
		    sbQuery.append(" and nvl(C.COCE_MT_DEVOLUCION,-1) >  0");
		    sbQuery.append(" and C.COCE_CAMPOF3               is null");
	     
			arlVentas.addAll(cargaDao.consultar(sbQuery.toString())); 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioCarga.getVentasAltairxTipoPoliza::" + e.getMessage());
		}
		
		return arlVentas;
	}


	public void updatePampa() throws Excepciones {
		this.cargaDao.updatePampa();
	}

	public void updatePampaCancelacion() throws Excepciones {
		this.cargaDao.updatePampaCancelacion();
	}
	
	public void buscarCuentaPampa(String[] arrDatos) throws Exception {
		this.cargaDao.buscarCuentaPampa(arrDatos);
	}

	public void estatusVta() throws Excepciones {
		List<Object> lstObjetosConEstatusVta = null;
		StringBuffer query;
		
		try {
			query = new StringBuffer(100);
			query.append("  from VtaPrecarga vt ");
			query.append("  WHERE vt.estatus = '2' ");
		    query.append("  and vt.id.sistemaOrigen='BT' ");
			lstObjetosConEstatusVta = (List<Object>) cargaDao.consultarMapeo(query.toString());
			
			for(Object o:lstObjetosConEstatusVta){
				VtaPrecarga vt = (VtaPrecarga) o;
				vt.setDdato1(new Date());//Se setea Fecha de Respuesta o recepcion del Archivo Pampa Ddato1.
				PreCarga p = new PreCarga(vt);
				p.getId().setCopcNumPoliza(0L);
				p.getId().setCopcCdRamo(new Short("9"));
				p.getId().setCopcCdSucursal(new Short("1"));
				p.getId().setCopcIdCertificado(vt.getId().getIdCertificado());
				p.setCopcTipoCliente("1");
				p.setCopcNuCredito(vt.getCuentaAlterna());
				p.setCopcDato6(vt.getVdato6());//Base calculo
				p.setCopcDato2(vt.getNdato8().toString());//tarifa
				p.setCopcBcoProducto(vt.getVdato3());//producto
				p.setCopcSubProducto(vt.getVdato4());//subproducto
				p.setCopcCazbCdSucursal(vt.getNdato9().toString());// sucursal
				p.setCopcDato9(vt.getTipoVenta());
				p.setCopcDato16(vt.getTipoVenta());
				p.setCopcNuPlazo(vt.getVdato5());
				
				p.setCopcDato23("CS:" + vt.getNdato2() + "|Seg:" + vt.getVdato10() + "|SeB:" + vt.getVdato11() + "|MA:" + vt.getVdato12() +	 "|Eje:" + vt.getAnalista()+ "|CE:" + vt.getVdato2() + "|Reg:" + vt.getNombreBen() + "|Zon:" + vt.getRegistro());
				p.setCopcDato3("SV:"+vt.getRfcBen()+"|CR:"+vt.getNdato6()+"|CZ:"+vt.getNdato7()+"|FeEn:"+ GestorFechas.formatDate(vt.getDdato2(), "dd/MM/yyyy")+"|FeRe:"+GestorFechas.formatDate( vt.getDdato1(), "dd/MM/yyyy")+"|FeCar:"+vt.getFeCarga()+"|PAseg:"+vt.getVdato3()+"|SpAseg:"+vt.getVdato3());
				p.setCopcDato1("");
				cargaDao.guardarObjeto(p);
				vt.setEstatus("4");
				cargaDao.actualizarObjeto(vt);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
