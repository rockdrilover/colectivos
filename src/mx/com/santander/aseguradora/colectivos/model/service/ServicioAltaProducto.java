/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;




import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ServicioAltaProducto{



	public Integer iniciaAlta(Short ramo, String descProducto, String descPlan
			) throws Excepciones;
  
}
