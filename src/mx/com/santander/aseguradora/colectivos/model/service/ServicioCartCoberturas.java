/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author Leobardo Preciado
 *
 */
public interface ServicioCartCoberturas extends ServicioCatalogo{
	
	 public <T> List<T> obtenerCartCoberturas(String  filtro)throws Excepciones;
	 public <T> List<T> obtenerCartCoberturasObj(String  filtro)throws Excepciones;
	
}
