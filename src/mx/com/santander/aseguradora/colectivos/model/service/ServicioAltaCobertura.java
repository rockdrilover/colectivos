/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;




import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ServicioAltaCobertura{



	public Integer iniciaAlta(Short ramo, String descCober,Integer ramoCont
			) throws Excepciones;
  
}
