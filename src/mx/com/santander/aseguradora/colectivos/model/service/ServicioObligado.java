/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;


import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ServicioObligado{

	public void guardaObligado(Long cdParametro,String desParametro,String idParametro,Integer inProducto,
       	 					   String inPlan,String inPeriodicidad,Short inCanal,Short inRamo,Integer inPoliza,
       	                       Short inNumObligados)throws Excepciones;
	

}
