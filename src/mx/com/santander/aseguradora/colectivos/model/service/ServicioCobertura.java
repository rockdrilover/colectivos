/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;



import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cobertura;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CoberturaId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author dflores
 *
 */
public interface ServicioCobertura {

	/**
	 * 
	 * @param poliza
	 * @return
	 */
	public Cobertura getCobertura(CoberturaId id)throws Excepciones;
	/**
	 * 
	 * @return
	 * @throws ExcepcionCatalogo 
	 */
	public List<Cobertura> getCoberturas() throws Excepciones;
	/**
	 * 
	 * @param ramo
	 * @return
	 */
	public List<Cobertura> getCoberturas(final Integer ramoContable)throws Excepciones;
}
