package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.CartCoberturasDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCartCoberturas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/*
 * @author: Leobardo Preciado
 */

@Service
public class ServicioCartCoberturasImpl implements ServicioCartCoberturas {

	@Resource
	private CartCoberturasDao cartCoberturasDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message = new StringBuilder();	
	

	public <T> T guardarObjeto(T objeto) throws Excepciones {

		try {
			T cartCobertura = this.cartCoberturasDao.guardarObjeto(objeto);
			return cartCobertura;
		} 
		catch (ObjetoDuplicado e) {
			// TODO: handle exception
			throw new ObjetoDuplicado("Cobertura duplicada.", e);
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new Excepciones("No se puede guardar la cobertura.");
		}
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
				
		
		try {
			this.cartCoberturasDao.actualizarObjeto(objeto);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			this.message.append("Error al actualizar el producto.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {

		try {
			List<T> lista = this.cartCoberturasDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden recuperar lAS COBERTURAS");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	// obtener catalogo de cart coberturas
	
	public  <T> List<T>  obtenerCartCoberturas(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
         try {
			
        	 
        	 List<T>  cartcoberturas = this.cartCoberturasDao.obtenerCartCoberturas(filtro.toString());
			
			return cartcoberturas;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede recuperar el siguiente id de parametros.");
			this.log.error(message.toString(), e);
			e.printStackTrace();
			throw new Excepciones(message.toString(), e);
			
		}
         
	}
	
	public  <T> List<T>  obtenerCartCoberturasObj(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
         try {
        	 
        	 List<T>  cartcoberturas = this.cartCoberturasDao.obtenerCartCoberturasObj(filtro.toString());
			
			return cartcoberturas;
			
		} catch (Exception e) {
			this.message.append("No se puede recuperar el siguiente id de parametros.");
			this.log.error(message.toString(), e);
			e.printStackTrace();
			throw new Excepciones(message.toString(), e);
		}
         
	}
	
	
	public CartCoberturasDao getCartCoberturasDao() {
		return cartCoberturasDao;
	}

	public void setCartCoberturasDao(CartCoberturasDao cartCoberturasDao) {
		this.cartCoberturasDao = cartCoberturasDao;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	
}
