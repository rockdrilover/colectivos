/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.ReciboDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanDetalleComponentes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaCoberturaDetalle;

/**
 * @author dflores
 *
 */
@Service
public class ServicioReciboImpl implements ServicioRecibo {

	@Resource
	private ReciboDao reciboDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	/**
	 * @return the reciboDao
	 */
	public ReciboDao getReciboDao() {
		return reciboDao;
	}

	/**
	 * @param reciboDao the reciboDao to set
	 */
	public void setReciboDao(ReciboDao reciboDao) {
		this.reciboDao = reciboDao;
	}

	public ServicioReciboImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.String)
	 */
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerRecibo.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			List<T> lista = this.reciboDao.obtenerObjetos(filtro);
			//System.out.println("Tama�o lista 1" + lista.size());
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los recibos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/* 
	 * Se consulta la tabla COLECTIVOS_RECIBOS
	 */
	public <T> List<T> obtenerObjetos1(String filtro) throws Excepciones {
		this.message.append("obtenerRecibo.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {			
			List<T> lista = this.reciboDao.obtenerObjetos1(filtro);
			return lista;
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los recibos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.DisposableBean#destroy()
	 */
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	public void impresionRecibo(String rutaTemporal, String nombre,
			Short careCasuCdSucursal, Short careCarpCdRamo,
			Integer careCapoNuPoliza, BigDecimal careNuRecibo,Short primaUnica) {
		// TODO Auto-generated method stub
		
		reciboDao.impresionRecibo(rutaTemporal, nombre, careCasuCdSucursal, careCarpCdRamo, careCapoNuPoliza, careNuRecibo, primaUnica);
	}

	public void impresionPreRecibo(String rutaTemporal, String nombre,
			short cofaCasuCdSucursal, short cofaCarpCdRamo, long cofaCapoNuPoliza,
			String cofaCampov1){
		// TODO Auto-generated method stub
		reciboDao.impresionPreRecibo(rutaTemporal, nombre, cofaCasuCdSucursal, cofaCarpCdRamo, cofaCapoNuPoliza, cofaCampov1);
	}
	
	@Override
	public List<Object[]> consulta2(short canal, Short ramo, Long poliza, Integer idVenta, Date feDesde, Date feHasta, Double noRecibo) throws Exception {
		StringBuilder filtro;
		
		try {
			filtro = new StringBuilder();
			if(ramo == 57 || ramo == 58) {
				filtro = GeneratorQuerys.getConsultaRecibos5758(canal, ramo, poliza, feDesde, feHasta, noRecibo);	
			} else {
				filtro = GeneratorQuerys.getConsultaRecibos(canal, ramo, poliza, idVenta, feDesde, feHasta, noRecibo);
			}

			return this.reciboDao.consulta2(filtro.toString());		
		} catch (Exception e) {
			this.message.append("Error al recuperar los recibos.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	@Override
	public List<BeanListaCoberturaDetalle> consultaDetalleCobertura(short ramo, double no_recibo, int sucursal) throws Exception { 
		List<BeanListaCoberturaDetalle> lstCoberturas;
		BeanListaCoberturaDetalle objBeanCobertura;
		
		try {
			lstCoberturas = new ArrayList<BeanListaCoberturaDetalle>();
			List<Object[]> listaDetalleRec = this.reciboDao.consultaDetalleCobertura(ramo, no_recibo, sucursal);
			
			for(Object[] arrCobertura: listaDetalleRec){
				objBeanCobertura = new BeanListaCoberturaDetalle();
				objBeanCobertura.setCober(arrCobertura[0].toString());
				objBeanCobertura.setCober_desc(arrCobertura[0].toString() + " - " + arrCobertura[1].toString());
				objBeanCobertura.setMonto_cober(arrCobertura[2].toString());
				objBeanCobertura.setTarif(arrCobertura[3].toString());
				lstCoberturas.add(objBeanCobertura);
			}
		} catch (Exception e) {
			this.message.append("Error al recuperar el historial de credito contratante.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}

		return  lstCoberturas;
	}
	
	@Override
	public List<BeanDetalleComponentes> consultaDetalleComponentes(short ramo, double no_recibo, int sucursal) throws Exception {
		List<BeanDetalleComponentes> lstComponentes;
		BeanDetalleComponentes objBeanComponente;
		
		try {
			lstComponentes = new ArrayList<BeanDetalleComponentes>();
			List<Object[]> listaDetalleCom = this.reciboDao.consultaDetalleComponente(ramo, no_recibo, sucursal);
			
			for(Object[] arrComponente: listaDetalleCom){
				objBeanComponente = new BeanDetalleComponentes();
				objBeanComponente.setCompo(arrComponente[0].toString());
				objBeanComponente.setCompo_desc(arrComponente[0].toString() + " - " + arrComponente[1].toString());
				objBeanComponente.setMonto_compo(arrComponente[2].toString());
				objBeanComponente.setTarif(arrComponente[3].toString());
				lstComponentes.add(objBeanComponente);
			}
		} catch (Exception e) {
			this.message.append("Error al recuperar el historial de credito contratante.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		
		return  lstComponentes;
	}
	
}
