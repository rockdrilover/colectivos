package mx.com.santander.aseguradora.colectivos.model.service;
import java.math.BigDecimal;
import java.util.List;

//import javax.sql.DataSource;


import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
public interface ServicioPrimasDeposito {

	public abstract <T> List<T> cargaIdVenta(String feinicio, String fefin,String produto,Short ramo) throws Exception;
	public void guardaFoto(int canal, Short inRamo, Integer inIdVenta, Long consecutivo, BigDecimal primDep, BigDecimal primRec,String fechaCarga, String remesa, int indicador) throws Excepciones;
	public Long maxConsecutivo() throws Excepciones;
	
	public abstract <T> List<T> generaDetallePrimas(String param, String producto) throws Excepciones;
	public abstract <T> List<T> cargaFotosGuardadas(int canal, Short ramo2, String mesRe, String dia) throws Exception;
	public abstract <T> int validaGuardar(int canal, Short ramo, int id,  String desc2, String desc3,String fechaCarga, String remesa) throws Excepciones;
	public abstract <T> List<T> cargaGuardados(int canal, Short ramo,String fechaCarga) throws Exception;
	
	List<String> consultaProductosColectivos(String producto) throws Exception;
}


