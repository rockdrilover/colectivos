/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Ramo;
import mx.com.santander.aseguradora.colectivos.model.dao.RamoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamo;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Sergio Plata
 *
 */
@Service
public class ServicioRamoImpl implements ServicioRamo {

	@Resource
	private RamoDao ramoDao;
	private Log log = LogFactory.getLog(this.getClass());
	private Map<Short, Ramo> ramosCache;
	
	public void setRamoDao(RamoDao ramoDao) {
		this.ramoDao = ramoDao;
	}
	public RamoDao getRamoDao() {
		return ramoDao;
	}
	public ServicioRamoImpl() {
		this.ramosCache = Collections.synchronizedMap(new LinkedHashMap<Short, Ramo>());
	}

	
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		return null;
	}

	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		try {
			List<Ramo> lista = Utilerias.getValueList(ramosCache);
			return (List<T>) lista;
		} catch (Exception e) {
			this.log.error("No se pueden cargar los ramos.", e);
			throw new Excepciones("No se pueden cargar los ramos.", e);
		}
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		return Utilerias.getValueList(this.ramosCache);
	}

	public void afterPropertiesSet() throws Exception {
		try {
			String filtro = "and R.colectivo = 'C'\n";
			List<Ramo> lista = this.ramoDao.obtenerObjetos(filtro);
			for(Ramo ramo: lista){
				this.ramosCache.put(ramo.getCarpCdRamo(), ramo);
			}
		} catch (Exception e) {
			this.log.error("Error al recuperar los ramos.", e);
			throw new Excepciones("Error al recuperar los ramos.", e);
		}
	}

	public void destroy() throws Exception {
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		
	}

	public String getDescRamo(Long cdRamo) throws Excepciones {
		String strDato; 
		try {
			String filtro = "and R.carpCdRamo = " + cdRamo + "\n";
			List<Ramo> lista = this.ramoDao.obtenerObjetos(filtro);
			Ramo ramo = (Ramo)lista.get(0);
			strDato = ramo.getCarpCdRamo() + "-->" + ramo.getCarpDeRamo();
			return strDato;
		} catch (Exception e) {
			this.log.error("Error al recuperar los ramos.", e);
			throw new Excepciones("Error al recuperar los ramos.", e);
		}
	}
	
}
