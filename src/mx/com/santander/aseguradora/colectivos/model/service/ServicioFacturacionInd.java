package mx.com.santander.aseguradora.colectivos.model.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author TONY
 *
 */
public interface ServicioFacturacionInd {

	public List<Object[]> getDatosCertificado() throws Exception;
	
	public String impresionXml2(String rutaTemporal, String nombre, Short canal, Short ramo, Long poliza, BigDecimal careNuRecibo) throws Exception;
	
	public String zipFiles(List<String> archivos, String rutaTemporal) throws IOException;
	
	public String imprimeRecibo2(String rutaTemporal, String rutaReporte, String nombre, Short careCasuCdSucursal, Short careCarpCdRamo,
	          	  Long careCapoNuPoliza, BigDecimal careNuRecibo,Short primaUnica, Long certificado)throws Exception;
	
	public void borrarDatosPrecarga() throws Excepciones;

}