package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.FiltroSuscripcionDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.GrupoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SuscripcionDTO;

/**
 * 
 * @author CJPV - vectormx
 * @version 07-09-2017
 *
 */
public interface ServicioSuscripcion extends ServicioCatalogo {

	/**
	 * Actualiza informacion Suscripcion
	 * 
	 * @param modifica
	 *            objeto a modificar
	 */
	void actualizaSuscripcion(SuscripcionDTO modifica) throws Excepciones;

	/**
	 * Consulta detalle suscripcion.
	 * 
	 * @param id
	 *            Id a consultar
	 * @return Suscripcion
	 */
	SuscripcionDTO consultaObjeto(long id);

	/**
	 * Conulta principal pantalla suscripciones
	 * 
	 * @param filtro
	 *            Parametros consulta
	 * @return Lista de suscripciones
	 * @throws Excepciones
	 *             Excepcion al ejecutar consulta
	 */
	List<SuscripcionDTO> consultaSuscripciones(FiltroSuscripcionDTO filtro) throws Excepciones;

	
	/**
	 * Areas tareas
	 * @return areas tareas.
	 */
	List<GrupoDTO> areasTarea() throws Excepciones;
	
	/**
	 * Parametros configuracion envio archivos
	 * @return Parametros configuracion
	 * @throws Excepciones
	 */
	Parametros archivoParametros() throws Excepciones;
	
	/**
	 * Valida acceso a operaciones de modificacion
	 * @param usuario Usuario logeado
	 * @return Verdadero si puede modificar seguimientos / Falso sin acceso
	 * @throws Excepciones Error al realizar operaciones en BD
	 */
	Boolean validaPermisoModificaciones(String usuario);
	
	/**
	 * Cambia estatus a terminado si no ecnuentra tareas pendientes
	 * @param modifica suscripcion a validar
	 * @throws Excepciones Error al ejecutar operacion
 	 */
	Boolean terminaSuscripcion(SuscripcionDTO modifica)throws Excepciones;
}
