package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.bllcolectivos.json.EstatusValidacion;
import mx.com.santander.aseguradora.bllcolectivos.processor.PrimaRecurrente;
import mx.com.santander.aseguradora.bllcolectivos.processor.PrimaUnica;
import mx.com.santander.aseguradora.colectivos.model.dao.ErrorEmisionDao;
import mx.com.santander.aseguradora.colectivos.model.dao.GenericDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioErrorEmision;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.BitacoraErroresDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase de tipo Service que tiene logica para el proceso
 * 				de Errores Emision.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 14-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioErrorEmisionImpl implements ServicioErrorEmision {

	//Instancias de DAO
	@Resource
	private ErrorEmisionDao errorDao;
	@Resource
	private GenericDao genericDao;
	
	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioErrorEmisionImpl.class);
	
	//Variable para mensajes
	private StringBuilder message;

	/**
	 * Cosntructor de Clase
	 */
	public ServicioErrorEmisionImpl() {
		message = new StringBuilder();
	}

	/**
	 * Metodo que manda consultar los errores
	 * @param nombreLayOut nombre de layout a buscar
	 * @param feDesde fecha desde
	 * @param feHasta fecha hasta
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	@Override
	public List<Object> consulta(String nombreLayOut, Date feDesde, Date feHasta) throws Excepciones {
		StringBuilder filtro;
		
		try {
			filtro = new StringBuilder();
			
			if(!nombreLayOut.equals(Constantes.DEFAULT_STRING)) {
				filtro.append("and UPPER(E.cc2.cociNombreArchivo) like '%" + nombreLayOut.trim().toUpperCase() + "%' \n" );
			}
			filtro.append(" and E.cociFechaCarga >= '").append(GestorFechas.formatDate(feDesde, Constantes.FORMATO_FECHA_UNO)).append("' \n");
			filtro.append(" and E.cociFechaCarga <= '").append(GestorFechas.formatDate(feHasta, Constantes.FORMATO_FECHA_UNO)).append("' \n");
			filtro.append("order by E.cociCdControl" );

			return this.genericDao.obtenerObjetos(filtro.toString(), Constantes.TABLA_CIFRAS_CONTROL);
		} catch (Excepciones e) {
			this.message.append("Error al recuperar archivos.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	/**
	 * Metodo que consulta el detalle de errores
	 * @param nombreLayOut nombre del archivo a buscar
	 * @return lista con errores
	 * @throws Excepciones con error general
	 */
	@Override
	public List<Object> getDetalleErrores(String nombreLayOut) throws Excepciones {
		StringBuilder filtro;
		
		try {
			//Inicializamos variables
			filtro = new StringBuilder();
			nombreLayOut = nombreLayOut.substring(0,nombreLayOut.indexOf('.'));
			
			filtro.append("and UPPER(B.be2.coerNombreArchivo) LIKE '" + nombreLayOut.trim().toUpperCase() + "%' \n" );
			filtro.append("order by B.be2.coerNvalor1 desc, B.coerCdErrorBitacora" );

			return this.genericDao.obtenerObjetos(filtro.toString(), Constantes.TABLA_BITACORA_ERRORES);
		} catch (Excepciones e) {
			this.message.append("Error al recuperar los errores.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	/**
	 * Metodo que emite creditos de Prima Unica
	 * @param nombreArchivo nombre del archivo
	 * @param cdControl id de cifras control
	 * @param lista lista con creditos a emitir
	 * @return mesanje del proceso
	 */
	@Override
	public String emitePU(String nombreArchivo, BigDecimal cdControl, List<BitacoraErroresDTO> lista) {
		PrimaUnica proPU;
		JSONObject objJSON = null;
		EstatusValidacion ev = null;
		Integer nLinea = 1;
		Iterator<BitacoraErroresDTO> it;
		
		try {
			//Inicializamos servicio de PU
			proPU = new PrimaUnica();
			
			//Recorremos lista de creditos a emitir
			it = lista.iterator();
			while(it.hasNext()) {
				//obtenemos objeto JSON
				objJSON = getObjectJson(it.next(), nombreArchivo, nLinea);
				
				//Mandamos a validar la linea
				ev = (EstatusValidacion) proPU.validaGuardaLinea(objJSON).get("validaLinea");
				if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
					//Hubo un error en la validacion del registro y sigue con el siguiente
					LOG.info(ev.getMsgEjecucion());
					nLinea++;
					continue;
				}
				
				//Inicializamos objeto Json para conciliar
				objJSON = new JSONObject();
				objJSON.put(Constantes.PARAM_REGISTRO, ev.getObjIdPU());
				objJSON.put(Constantes.PARAM_TIPOPROCESO, Constantes.PROCESO_MAN);

				//Se manda conciliar registro
				ev = (EstatusValidacion) proPU.conciliaRegistro(objJSON).get("conciliacion");
				if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
					//Hubo un error en la conciliacion
					LOG.info(ev.getMsgEjecucion());
				}
			
				nLinea++;
			}
			
			//Se manda a ejecutar la emision
			return emite(proPU, null, nombreArchivo, cdControl);
		}  catch (JSONException je) {
			this.message.append("Error al Emitir registros de PU.");
			LOG.error(message.toString(), je);
			return this.message.toString();
		}
	}

	/**
	 * Metodo que emite creditos de Prima Unica
	 * @param nombreArchivo nombre del archivo
	 * @param cdControl id de cifras control
	 * @param lista lista con creditos a emitir
	 * @return mesanje del proceso
	 */
	@Override
	public String emitePR(String nombreArchivo, BigDecimal cdControl, List<BitacoraErroresDTO> lista) {
		PrimaRecurrente proPR;
		JSONObject objJSON = null;
		EstatusValidacion ev = null;
		Integer nLinea = 1;
		Iterator<BitacoraErroresDTO> it;
		
		try {
			//Inicializamos servicio de PU
			proPR = new PrimaRecurrente();
			
			//Recorremos lista de creditos a emitir
			it = lista.iterator();
			while(it.hasNext()) {
				//obtenemos objeto JSON
				objJSON = getObjectJson(it.next(), nombreArchivo, nLinea);
				
				//Mandamos a validar la linea
				ev = (EstatusValidacion) proPR.validaGuardaLinea(objJSON).get("validaLinea");
				if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
					//Hubo un error en la validacion del registro y sigue con el siguiente
					LOG.info(ev.getMsgEjecucion());					
				}
			
				nLinea++;
			}
			
			//Se manda a ejecutar la emision
			return emite(null, proPR, nombreArchivo, cdControl);
		} catch (JSONException je) {
			this.message.append("Error al Emitir PR.");
			LOG.error(message.toString(), je);
			return this.message.toString();
		}
	}

	/**
	 * Metodo que genera objeto Json para poder emitir registro
	 * @param dto objeto error
	 * @param nombreArchivo nombre del archivo
	 * @param nLinea linea a procesar
	 * @return objeto Json
	 */
	private JSONObject getObjectJson(BitacoraErroresDTO dto, String nombreArchivo, Integer nLinea) {
		JSONObject objJSON = null;
		try {
			//Eliminamos error
			errorDao.eliminaError(dto);
			
			//Inicializamos objeto Json para validar linea
			objJSON = new JSONObject();
			objJSON.put("linea", dto.getCoerLinea());
			objJSON.put(Constantes.PARAM_ARCHIVO, nombreArchivo);
			objJSON.put(Constantes.PARAM_REGISTRO, nLinea);
			objJSON.put(Constantes.PARAM_TIPOPROCESO, Constantes.PROCESO_MAN);
			objJSON.put("cdErrorReg", dto.getCoerCdError());
		} catch (JSONException je) {
			this.message.append("Error al crear objeto Json.");
			LOG.error(message.toString(), je);
		} catch (Excepciones e) {
			this.message.append("Error al eliminar errores.");
			LOG.error(message.toString(), e);
		}
		
		return objJSON;
	}
	
	/**
	 * 
	 * @param proPU proceso para Prima Unica
	 * @param proPR proceso para Prima Recurrente
 	 * @param nombreArchivo nombre de archivo
	 * @param cdControl id de cifras control
	 * @return mensaje de ejecucion
	 */
	private String emite(PrimaUnica proPU, PrimaRecurrente proPR, String nombreArchivo, BigDecimal cdControl) {
		JSONObject objJSON = null;
		EstatusValidacion ev = null;
		String strRespuesta = Constantes.DEFAULT_STRING;
		
		try {
			//Creamos archivo temporal para consulta de polizas
			File objFilePol =  File.createTempFile("emiConPol", ".tmp");
			
			//Inicializamos objeto Json para consulta de polizas
			objJSON = new JSONObject();
			objJSON.put(Constantes.PARAM_ARCHIVO, nombreArchivo);			
			objJSON.put("file", objFilePol);
			objJSON.put(Constantes.PARAM_TIPOPROCESO, Constantes.PROCESO_MAN);
			objJSON.put("cdCifras", cdControl);
			
			//Se ejecuta proceso de consulta de polizas
			if(proPR == null) {
				ev = (EstatusValidacion) proPU.consultarPolizas(objJSON).get("consultarPolizas");
			} else {
				ev = (EstatusValidacion) proPR.consultarPolizas(objJSON).get("consultarPolizas");
			}
			
			if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
				//si hubo error ya no sigue con el flujo
				return ev.getMsgEjecucion();
			}
			
			//Creamos arreglo de polizas a emitir
			Archivo objArchivo = new Archivo(objFilePol); 
			ArrayList<Object[]> arlPolizas = new ArrayList<Object[]>();
			arlPolizas.addAll(objArchivo.leerArchivo("#", false));
			
			//Eliminamos archivo temporal de polizas
			Files.delete(objFilePol.toPath());
			
			//Creamos archivo temporal para consulta de polizas
			File objFileError =  File.createTempFile("emiErrorEmi", ".tmp");
			
			//Se recorre arreglo de polizas
			Iterator<Object[]> it = arlPolizas.iterator();
			while(it.hasNext()) {
				//Inicializamos objeto Json para emitir
				objJSON = new JSONObject(it.next()[0].toString());
				objJSON.put(Constantes.PARAM_ARCHIVO, nombreArchivo);	
				objJSON.put("file", objFileError);
				objJSON.put(Constantes.PARAM_TIPOPROCESO, Constantes.PROCESO_MAN);
				
				//Se ejecuta proceso de emision
				if(proPR == null) {
					ev = (EstatusValidacion) proPU.emitir(objJSON).get("emitir");
				} else {
					ev = (EstatusValidacion) proPR.emitir(objJSON).get("emitir");
				}
				
				if(ev.getStatusEjecucion()  != Constantes.CODIGO_JSON_OK) {
					LOG.info(ev.getMsgEjecucion());
				}
			}
			
			//Eliminamos archivo temporal de errores
			Files.delete(objFileError.toPath());
			strRespuesta = "El proceso de emision finaliz� correctamente, favor de realizar consulta.";
		} catch (JSONException | Excepciones | IOException e) {
			this.message.append("Error al emitir errores seleccionados.");
			LOG.error(message.toString(), e);
			strRespuesta = this.message.toString();
		}
		
		return strRespuesta;
	}
	
}
