/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;



import mx.com.santander.aseguradora.colectivos.model.dao.AltaCoberturaDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaCobertura;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author dflores
 *
 */
public class ServicioAltaCoberImpl implements ServicioAltaCobertura  {

	private Log log = LogFactory.getLog(this.getClass());
	@SuppressWarnings("unchecked")
	private AltaCoberturaDao altaCoberDao;
	private String message = null;
	


	@SuppressWarnings("unchecked")
	public void setAltaCoberDao(AltaCoberturaDao altaCoberDao) { 
		this.altaCoberDao = altaCoberDao;
	}
	

	@SuppressWarnings("unchecked")
	public AltaCoberturaDao getAltaCoberDao() {
		return altaCoberDao;
	}

	public Integer iniciaAlta( Short ramo, String descCober,Integer ramoCont
			) throws Excepciones {
		// TODO Auto-generated method stub
		Integer pro = 0;
		this.log.debug("Iniciando proceso de Alta de cobertura");
		try {
			
			pro = this.altaCoberDao.iniciaAlta(ramo, descCober, ramoCont);
			return pro;
			
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message, e);
			throw new Excepciones(message, e)  ;
		}
		
	}


 
	

}
