package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanDetalleComponentes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaCoberturaDetalle;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaCreditoDetalle;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaRecibosHistorico;

public interface ServicioHistorialCredito extends ServicioCatalogo {
	
	public List<BeanListaCreditoDetalle> consultaHistorial (short ramo, Long poliza, String certificado,String credito ,String nombre, String paterno, String materno) throws Excepciones;

	public List<BeanListaCoberturaDetalle> consultaHistorialCobertura(short ramo, Long poliza, String certificado, Integer plan, Integer producto,Integer idVenta) throws Excepciones;

	public List<BeanListaRecibosHistorico> consultaHistorialRecibos(short ramo,
			Long poliza, String certificado) throws Excepciones;
	
	public List<BeanDetalleComponentes> consultaDetalleComponentes(short ramo, Long poliza, String certificado, Integer producto, Integer plan, Integer idVenta) throws Excepciones;


	
}
