package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSuscripcion;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Grupo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.SeguimientoDAO;
import mx.com.santander.aseguradora.colectivos.model.dao.SuscripcionDAO;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion;
import mx.com.santander.aseguradora.colectivos.view.dto.FiltroSuscripcionDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.GrupoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SuscripcionDTO;

@Service
public class ServicioSuscripcionImpl implements ServicioSuscripcion {

	@Resource
	private SuscripcionDAO suscripcionDAO;

	@Resource
	private SeguimientoDAO seguimientoDAO;

	/**
	 * Convierte suscripcion dto a suscripcion persistencia
	 * 
	 * @param obj
	 *            DTO
	 * @return suscripcion persistencia
	 */
	private ColectivosSuscripcion suscripcionDTOToSuscripcion(SuscripcionDTO obj) {
		ColectivosSuscripcion res = new ColectivosSuscripcion();

		res.setCosuId(obj.getCosuId());
		res.setCosuCelular(obj.getCosuCelular());
		res.setCosuDictamen(obj.getCosuDictamen());
		res.setCosuEmail(obj.getCosuEmail());
		res.setCosuExamMedico(obj.getCosuExamMedico());
		res.setCosuExtraFirma(obj.getCosuExtraFirma());
		res.setCosuFechaAplExam(obj.getCosuFechaAplExam());
		res.setCosuFechaCarga(obj.getCosuFechaCarga());
		res.setCosuFechaCitExam(obj.getCosuFechaCitExam());
		res.setCosuFechaFirm(obj.getCosuFechaFirm());
		res.setCosuFechaNac(obj.getCosuFechaNac());
		res.setCosuFechaNotDic(obj.getCosuFechaNotDic());
		res.setCosuFechaRecAseg(obj.getCosuFechaRecAseg());
		res.setCosuFechaResDoc(obj.getCosuFechaResDoc());
		res.setCosuFechaRespSuc(obj.getCosuFechaRespSuc());
		res.setCosuFechaResult(obj.getCosuFechaResult());
		res.setCosuFechaSolCred(obj.getCosuFechaSolCred());
		res.setCosuFechaSolExam(obj.getCosuFechaSolExam());
		res.setCosuFolioCh(obj.getCosuFolioCh());
		res.setCosuFolioRiesgos(obj.getCosuFolioRiesgos());
		res.setCosuIdConcatenar(obj.getCosuIdConcatenar());
		res.setCosuLocal(obj.getCosuLocal());
		res.setCosuMonto(obj.getCosuMonto());
		res.setCosuMontoFirmado(obj.getCosuMontoFirmado());
		res.setCosuMotExamMed(obj.getCosuMotExamMed());
		res.setCosuNombre(obj.getCosuNombre());
		res.setCosuNumCredito(obj.getCosuNumCredito());
		res.setCosuNumInter(obj.getCosuNumInter());
		res.setCosuObsWf(obj.getCosuObsWf());
		res.setCosuObservaciones(obj.getCosuObservaciones());
		res.setCosuParticipacion(obj.getCosuParticipacion());
		res.setCosuPlaza(obj.getCosuPlaza());
		res.setCosuSucEjec(obj.getCosuSucEjec());
		res.setCosuTelefono1(obj.getCosuTelefono1());
		res.setCosuTelefono2(obj.getCosuTelefono2());
		res.setCosuUsuarioCrea(obj.getCosuUsuarioCrea());

		return res;
	}

	/**
	 * Coniverte suscripcion hibernate a suscripcion dto
	 * 
	 * @param obj
	 *            Suscripcion persistencia
	 * @return dto suscripcion vista
	 */
	private SuscripcionDTO suscripcionToSuscripcionDTO(ColectivosSuscripcion obj) {
		SuscripcionDTO res = new SuscripcionDTO();

		res.setCosuId(obj.getCosuId());
		res.setCosuCelular(obj.getCosuCelular());
		res.setCosuDictamen(obj.getCosuDictamen());
		res.setCosuEmail(obj.getCosuEmail());
		res.setCosuExamMedico(obj.getCosuExamMedico());
		res.setCosuExtraFirma(obj.getCosuExtraFirma());
		res.setCosuFechaAplExam(obj.getCosuFechaAplExam());
		res.setCosuFechaCarga(obj.getCosuFechaCarga());
		res.setCosuFechaCitExam(obj.getCosuFechaCitExam());
		res.setCosuFechaFirm(obj.getCosuFechaFirm());
		res.setCosuFechaNac(obj.getCosuFechaNac());
		res.setCosuFechaNotDic(obj.getCosuFechaNotDic());
		res.setCosuFechaRecAseg(obj.getCosuFechaRecAseg());
		res.setCosuFechaResDoc(obj.getCosuFechaResDoc());
		res.setCosuFechaRespSuc(obj.getCosuFechaRespSuc());
		res.setCosuFechaResult(obj.getCosuFechaResult());
		res.setCosuFechaSolCred(obj.getCosuFechaSolCred());
		res.setCosuFechaSolExam(obj.getCosuFechaSolExam());
		res.setCosuFolioCh(obj.getCosuFolioCh());
		res.setCosuFolioRiesgos(obj.getCosuFolioRiesgos());
		res.setCosuIdConcatenar(obj.getCosuIdConcatenar());
		res.setCosuLocal(obj.getCosuLocal());
		res.setCosuMonto(obj.getCosuMonto());
		res.setCosuMontoFirmado(obj.getCosuMontoFirmado());
		res.setCosuMotExamMed(obj.getCosuMotExamMed());
		res.setCosuNombre(obj.getCosuNombre());
		res.setCosuNumCredito(obj.getCosuNumCredito());
		res.setCosuNumInter(obj.getCosuNumInter());
		res.setCosuObsWf(obj.getCosuObsWf());
		res.setCosuObservaciones(obj.getCosuObservaciones());
		res.setCosuParticipacion(obj.getCosuParticipacion());
		res.setCosuPlaza(obj.getCosuPlaza());
		res.setCosuSucEjec(obj.getCosuSucEjec());
		res.setCosuTelefono1(obj.getCosuTelefono1());
		res.setCosuTelefono2(obj.getCosuTelefono2());
		res.setCosuUsuarioCrea(obj.getCosuUsuarioCrea());

		return res;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		SuscripcionDTO suscripcionDTO = (SuscripcionDTO) objeto;
		ColectivosSuscripcion suscripcion = suscripcionDTOToSuscripcion(suscripcionDTO);

		ColectivosSuscripcion buscar = new ColectivosSuscripcion();

		buscar = suscripcionDAO.obtieneSuscripcionFolioRiesgos(suscripcion);

		if (buscar == null) {
			suscripcion = suscripcionDAO.guardarObjeto(suscripcion);
		} else {
			suscripcionDAO.actualizarObjeto(suscripcion);
		}

		return (T) suscripcion;
	}

	@Override
	public SuscripcionDTO consultaObjeto(long id) {
		SuscripcionDTO res = new SuscripcionDTO();
		try {
			res = suscripcionToSuscripcionDTO(suscripcionDAO.consultaObjeto(id));
			return res;
		} catch (Exception e) {

			return null;
		}
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> void actualizarObjeto(T objeto) throws Excepciones {

		suscripcionDAO.actualizarObjeto(objeto);

	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<SuscripcionDTO> consultaSuscripciones(FiltroSuscripcionDTO filtro) throws Excepciones {
		List<SuscripcionDTO> res = new ArrayList<SuscripcionDTO>();
		ColectivosSuscripcion colectivosSuscripcion = new ColectivosSuscripcion();

		colectivosSuscripcion.setCosuDictamen(filtro.getDictamen());
		colectivosSuscripcion.setCosuFolioCh(filtro.getFolioCH());
		colectivosSuscripcion.setCosuFechaSolCred(filtro.getFechaSolicitud());
		colectivosSuscripcion.setCosuFechaCarga(filtro.getFechaCarga());

		List<ColectivosSuscripcion> datos = suscripcionDAO.consultaSuscripciones(colectivosSuscripcion);

		for (ColectivosSuscripcion d : datos) {
			res.add(this.suscripcionToSuscripcionDTO(d));
		}

		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion#
	 * actualizaObjeto(mx.com.santander.aseguradora.colectivos.view.dto.
	 * SuscripcionDTO)
	 */
	@Override
	public void actualizaSuscripcion(SuscripcionDTO modifica) throws Excepciones {
		ColectivosSuscripcion guardar = suscripcionDTOToSuscripcion(modifica);
		suscripcionDAO.actualizarObjeto(guardar);
	}
	
	
	

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion#terminaSuscripcion(mx.com.santander.aseguradora.colectivos.view.dto.SuscripcionDTO)
	 */
	@Override
	public Boolean terminaSuscripcion(SuscripcionDTO modifica) throws Excepciones {
		ColectivosSuscripcion guardar = suscripcionDTOToSuscripcion(modifica);

		long tareasTerminadas = suscripcionDAO.tareaAtendida(guardar);
		if (tareasTerminadas == 0) {
			guardar.setCosuDictamen("T");
			suscripcionDAO.actualizarObjeto(guardar);
			return true;
		}
		
		return false;
	}

	/**
	 * @return the suscripcionDAO
	 */
	public SuscripcionDAO getSuscripcionDAO() {
		return suscripcionDAO;
	}

	/**
	 * @param suscripcionDAO
	 *            the suscripcionDAO to set
	 */
	public void setSuscripcionDAO(SuscripcionDAO suscripcionDAO) {
		this.suscripcionDAO = suscripcionDAO;
	}

	private GrupoDTO grupoToGrupoDTO(Grupo grupo) {
		GrupoDTO res = new GrupoDTO();

		res.setId(grupo.getId());
		res.setDescripcion(grupo.getDescripcion());

		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion#
	 * areasTarea()
	 */
	@Override
	public List<GrupoDTO> areasTarea() throws Excepciones {

		List<GrupoDTO> res = new ArrayList<GrupoDTO>();
		try {
			List<Grupo> grupos = suscripcionDAO.gruposTareas();

			for (Grupo g : grupos) {
				res.add(grupoToGrupoDTO(g));
			}

			return res;
		} catch (Exception e) {

			throw new Excepciones(e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion#
	 * archivoParametros()
	 */
	@Override
	public Parametros archivoParametros() throws Excepciones {

		return suscripcionDAO.archivoParametros();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion#
	 * validaPermisoModificaciones(java.lang.String)
	 */
	@Override
	public Boolean validaPermisoModificaciones(String usuario) {
		try {
			Parametros parametros = archivoParametros();
			List<Object> grupos = seguimientoDAO.consultaGrupoUsuarios(usuario);

			Iterator<Object> it;
			BigDecimal g;
			it = grupos.iterator();
			String[] gruposPermisos = parametros.getCopaVvalor2().split(",");

			while (it.hasNext()) {
				g = (BigDecimal) it.next();
				for (int i = 0; i < gruposPermisos.length; i++) {
					if (g.toString().equals(gruposPermisos[0])) {
						return true;
					}
					;
				}

			}

			return false;
		} catch (Excepciones e) {
			return false;
		}

	}

}
