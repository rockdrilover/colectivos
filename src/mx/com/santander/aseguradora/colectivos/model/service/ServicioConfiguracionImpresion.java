package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;
import java.util.Map;

public interface ServicioConfiguracionImpresion {

	public abstract <T> List<T> getCamposBase() throws Exception;

	public abstract String insertConfigImpresion(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto, Integer nPlan, String formulario, String base) throws Exception;

	public abstract <T> List<T> consultaPlantilla(int i, Short ramo, Integer poliza, Integer idVenta, Integer nProducto, Integer nPlan) throws Exception;
	
	public abstract String actualizaDatos(int canal, Short ramo, Integer poliza, Integer idVenta, Integer nProducto,Integer nPlan, String formulario, String base, String baseAnte) throws Exception;

	public abstract String guardaPlantilla(Long next, String idParam, String desParam, int canal, Short ramo, Integer poliza, Integer nProducto, Integer nPlan, String nombreArchivo) throws Exception;

	public abstract String getPlazo(Short ramo, Integer nProducto, Integer nPlan) throws Exception;

	public abstract String existe(String filtro) throws Exception;
	
	String editarNombrePlantilla(String plantilla, Map<String, String > filtro);
	
	String borrarPlantilla(String plantilla, Map<String, String > filtro);

}
