package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Map;

import mx.com.santander.aseguradora.colectivos.utils.Constantes.OPC_FORMATO_REPORTE;

public interface ServicioEndoso extends ServicioCatalogo {

	void reporteEndosos(String rutaTemporal, String rutaReporte, Map<String, Object> inParams, OPC_FORMATO_REPORTE opcr) throws Exception;

}
