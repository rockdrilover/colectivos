package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.bllcolectivos.json.EstatusValidacion;
import mx.com.santander.aseguradora.bllcolectivos.processor.Cancelacion;
import mx.com.santander.aseguradora.colectivos.model.dao.CancelaAtmDao;
import mx.com.santander.aseguradora.colectivos.model.dao.GenericDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCancelaAtm;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCancelaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase de tipo Service que tiene logica para el proceso
 * 				de Cancelacion Automatica.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 14-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioCancelaAtmImpl implements ServicioCancelaAtm {

	//Instancias de DAO
	@Resource
	private CancelaAtmDao cancelaDao;
	@Resource
	private GenericDao genericDao;
	
	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioCancelaAtmImpl.class);
	
	//Variable para mensajes
	private StringBuilder message;

	/**
	 * Cosntructor de Clase
	 */
	public ServicioCancelaAtmImpl() {
		//Se incializa propiedad para mensajes
		message = new StringBuilder();
	}

	/**
	 * Metodo que manda consultar los creditos a cancelar
	 * @param nombreLayOut nombre de layout a buscar
	 * @param feDesde fecha desde
	 * @param feHasta fecha hasta
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	@Override
	public List<Object> consulta(String nombreLayOut, Date feDesde, Date feHasta) throws Excepciones {
		try {
			//Consulta y regresa el resumen de creditos a cancelar
			return this.cancelaDao.getResumenCancela(nombreLayOut, GestorFechas.formatDate(feDesde, Constantes.FORMATO_FECHA_UNO), GestorFechas.formatDate(feHasta, Constantes.FORMATO_FECHA_UNO));
		} catch (Excepciones e) {
			this.message.append("Error al recuperar archivos.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	/**
	 * Metodo que consulta el detalle de creditos a cancelar
	 * @param datosResumen datos para buscar detalle
	 * @return lista con detalle
	 * @throws Excepciones con error general
	 */
	@Override
	public List<Object> getDetalle(ResumenCancelaDTO datosResumen) throws Excepciones {
		StringBuilder filtro;
		
		try {
			filtro = new StringBuilder();
			
			//se armar consulta
			filtro.append("and P.id.copcCdSucursal = " + datosResumen.getCdSucursal() + " \n");
			filtro.append("and P.id.copcCdRamo = " + datosResumen.getCdRamo() + " \n");
			filtro.append("and P.id.copcNumPoliza = " + datosResumen.getNuPoliza() + " \n");
			filtro.append("and P.id.copcTipoRegistro = 'CCM' \n");
			filtro.append("and P.copcDato11 = '" + datosResumen.getArchivo() + "' \n");
			
			
			if("ERR".equals(datosResumen.getDescVenta())) {
				filtro.append("and NVL(P.copcDato16, 0) = 0 \n");
				filtro.append("and P.copcCargada = '77' \n");	
			} else {
				filtro.append("and NVL(P.copcDato16, 0) = " + datosResumen.getDescVenta().split("\\-")[0].trim() + " \n");
				filtro.append("and P.copcCargada = '7' \n");
				filtro.append("and P.copcDato25 = " + datosResumen.getCausaAnulacion().split("\\-")[0].trim() + " \n");
				filtro.append("and P.copcDato33 = " + datosResumen.getDescTipo().split("\\-")[0].trim() + " \n");
			}
			filtro.append("order by TO_NUMBER(NVL(P.copcDato2, 0))" );

			//Ejecuta consulta
			return this.genericDao.obtenerObjetos(filtro.toString(), Constantes.TABLA_PRECARGA);
		} catch (Excepciones e) {
			this.message.append("Error al recuperar detalle.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	/**
	 * Metodo que actualiza creditos a cancelar
	 * @param seleccionado datos para hacer update
	 * @param lista creditos a cancelar
	 * @throws Excepciones con error general
	 */
	@Override
	public void cancelaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones {
		try {
			//Se marcan los creditos seleccionados por usuario para cancelar
			this.cancelaDao.cancelaCreditos(seleccionado, lista);
		} catch (Excepciones e) {
			this.message.append("Error al programar cancelacion.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
		
	}	
	
	/**
	 * Metodo que elimina creditos por errores o que no se quieren cancelar
	 * @param seleccionado datos para hacer delete
	 * @param lista creditos a eliminar
	 * @throws Excepciones con error general
	 */
	@Override
	public void eliminaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones {
		try {
			//Eliminar los creditos que no se quieren cancelar
			this.cancelaDao.eliminaCreditos(seleccionado, lista);
		} catch (Excepciones e) {
			this.message.append("Error al eliminar reregistros.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}	
	
	/**
	 * Metodo que manda guardar los registros del archivo
	 * @param archivo archivo a procesar
	 * @return mensaje de ejecucion
	 * @throws Exception con errores en general
	 */
	@Override
	public String guardaDatosArchivo(Archivo archivo) {
		Cancelacion proCan;
		JSONObject objJSON = null;
		EstatusValidacion ev = null;
		Integer nLinea = 1;
		ArrayList<Object[]> arlLineas;
		Iterator<Object[]> it;
		String strRespuesta = Constantes.DEFAULT_STRING;
				
		try {
			//Inicializamos propiedades
			proCan = new Cancelacion();
			arlLineas = new ArrayList<Object[]>();
			
			//Obtenemos lineas del Archivo
			arlLineas.addAll(archivo.leerArchivo("?", false));
			
			//Validamos estructura de primer linea
			objJSON = new JSONObject();
			objJSON.put("linea", arlLineas.get(0)[0].toString());
			objJSON.put("nombreArchivo", archivo.getArchivo().getName());
			objJSON.put("estructura", Constantes.DEFAULT_STRING);
			ev = (EstatusValidacion) proCan.validaEstructura(objJSON).get("validaEstructura");
			
			//Validamos si hay error de estructura
			if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
				//Si hay error ya no segimos
				return ev.getMsgEjecucion();
			}
			
			//Recorremos lineas del archivo
			it = arlLineas.iterator();
			while(it.hasNext()) {
				Object[] linea = it.next();
				
				//Validamos lineas del archivo
				objJSON = new JSONObject();
				objJSON.put("linea", linea[0].toString());
				objJSON.put("nombreArchivo", archivo.getArchivo().getName());
				objJSON.put("registro", nLinea);
				ev = (EstatusValidacion) proCan.validaGuardaLinea(objJSON).get("validaLinea");
				
				if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
					LOG.info(ev.getMsgEjecucion());
				}
				
				nLinea++;
			}
			
			strRespuesta = "El proceso de carga finaliz� correctamente, favor de realizar consulta.";
		} catch (JSONException je) {
			this.message.append("Error al cargar archivo de cancelacion.");
			LOG.error(message.toString(), je);
			strRespuesta = this.message.toString();
		} catch (Excepciones e) {
			this.message.append("Error al leer LayOut.");
			LOG.error(message.toString(), e);
			strRespuesta = this.message.toString();
		}
		
		return strRespuesta;
	}
	
	/**
	 * @return the message
	 */
	public StringBuilder getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	/**
	 * @return the cancelaDao
	 */
	public CancelaAtmDao getCancelaDao() {
		return cancelaDao;
	}

	/**
	 * @param cancelaDao the cancelaDao to set
	 */
	public void setCancelaDao(CancelaAtmDao cancelaDao) {
		this.cancelaDao = cancelaDao;
	}
	
}
