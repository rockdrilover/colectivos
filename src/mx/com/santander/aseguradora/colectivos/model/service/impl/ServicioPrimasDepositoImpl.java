package mx.com.santander.aseguradora.colectivos.model.service.impl;


import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.dao.PrimasDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPrimasDeposito;


@Service
public class ServicioPrimasDepositoImpl implements ServicioPrimasDeposito{
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	@Resource
	private PrimasDao primasDao;
	
	//set and get
	public PrimasDao getPrimasDao() {
		return primasDao;
	}

	public void setPrimasDao(PrimasDao primasDao) {
		this.primasDao = primasDao;
	}
	public ServicioPrimasDepositoImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();

	}

	@Override
	public void guardaFoto(int canal, Short inRamo, Integer inIdVenta, Long consecutivo, BigDecimal primDep, BigDecimal primRec,String fechaCarga, String remesa,int indicador) throws Excepciones {
		 try {
				
				this.primasDao.guardaFoto(canal, inRamo, inIdVenta,consecutivo,  primDep, primRec, fechaCarga,remesa,indicador);
				message.append("Proceso insertar numero de cliente");
				this.log.debug(message.toString());
			} catch (Exception e) {
				message.append("Hubo un error al ejecutar el proceso de insertar numerador de cliente");
				this.log.error(message.toString(), e);
				throw new Excepciones(message.toString(), e);
			}		
	}
	
	public <T> List<T> cargaIdVenta(String feinicio, String fefin, String producto,Short ramo) throws Exception {
		try {
			return primasDao.cargaIdVenta(feinicio,fefin,producto,ramo);
		}
		catch (Exception e) {
			this.message.append("Error al cargar primas en deposito.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	@Override
	public Long maxConsecutivo() throws Excepciones {
		
		try {
			Long resp =  this.primasDao.maxConsecutivo();
			return resp;
		}
		catch (Exception e) {
			this.message.append("Error al cargar maximo consecutivo.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	

	@Override
	public  <T> List<T> generaDetallePrimas(String param, String producto) throws Excepciones {
	
		try {
			
			return primasDao.generaDetallePrimas(param, producto);
			
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}

	@Override                                
	public <T> List<T> cargaFotosGuardadas(int canal, Short ramo2, String mesRe, String dia) throws Exception {
		try {
			return primasDao.cargaFotosGuardadas(canal,ramo2,mesRe,dia); 
		} catch (Exception e) {
			this.message.append("Error al cargar fotos guardadas.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	@Override
	public <T> int validaGuardar(int canal, Short ramo, int id,  String desc2, String desc3,
			String fechaCarga, String remesa) throws Excepciones {
		try {
			return primasDao.validaGuardar(canal,ramo,id,desc2,desc3); 
		} catch (Exception e) {
			this.message.append("Error al cargar fotos guardadas.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}		
	}

	@Override
	public <T> List<T> cargaGuardados(int canal, Short ramo,String fechaCarga) throws Exception {
		try {
			return primasDao.cargaGuardados(canal,ramo,fechaCarga);
		}
		catch (Exception e) {
			this.message.append("Error al cargar primas en deposito.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}	
	}
	
	@Override
	public List<String> consultaProductosColectivos(String producto) throws Exception {
		try {
			return primasDao.consultaProductosColectivos(producto);
		}
		catch (Exception e) {
			this.message.append("Error al cargar los productos-colectivos.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}	
	}
}
