
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
public interface ServicioReporteStock {

	public List<Object> consultarRegistros(Short ramo, Long poliza, Integer inIdVenta, Date fecfin, Date fecini, String strCampos) throws Excepciones;
	public List<Object> consultarErrores(Short ramo, Long poliza, Integer inIdVenta, Date fecfin, Date fecini, Integer inTipoError) throws Excepciones;
	
}
