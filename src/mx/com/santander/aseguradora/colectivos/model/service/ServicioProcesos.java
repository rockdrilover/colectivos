/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.FacturacionId;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos.OPC_CANACEL;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanFacturacion;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;


/**
 * @author Sergio Plata
 *
 */
public interface ServicioProcesos {
	
	/**
	 * 
	 * @param canal
	 * @param ramo
	 * @param poliza
	 * @param inIdVenta 
	 * @throws Excepciones
	 */
	public void emisionMasiva(Short canal, Short ramo, Long poliza, Integer inIdVenta)throws Excepciones;
			
	public String facturacion(Short canal, Short ramo)throws Excepciones;
	/**
	 * 
	 * @return
	 * @throws Excepciones
	 */
	public DataSource getDataSource()throws Excepciones;
	
	/**
	 * Metodo que manda ejecutar prefactura para una poliza
	 * @param id datos para prefacturar
	 * @param nNumRecibo numero de recibo
	 * @return Map de resultados
	 * @throws Excepciones con error en general
	 */
	Map<String, Object> prefactura(CertificadoId id, Integer nNumRecibo) throws Excepciones;
	
	public void factura(FacturacionId id) throws Excepciones;
	public void actRecFiscal(Short canal, byte ramo, Long poliza, String reccol, int num) throws Excepciones;
	public abstract <T> List<T> modificaPrefactura(List<BeanCertificado> listaCertificados) throws Excepciones;
	public <T> List<T> prefacturas (Short canal , Short ramo, Long poliza, Integer inIdVenta ) throws Excepciones;
	public abstract <T> List<T> recuperaRecibos(int suc, int ramo, String poliza) throws Excepciones;
	public abstract <T> List<T> facturacion(List<BeanFacturacion> listaPrefact) throws Excepciones;
	public void cancelacionMasiva(Short inCanal, Short inRamo, Long inPoliza, Integer inCausa, OPC_CANACEL opc) throws Excepciones;
	public String cancelacionCri(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta, Integer i) throws Excepciones;
	public abstract <T> List<T> guardaMontosNuevosPrefactura(List<BeanFacturacion> listaPrefact) throws Excepciones;
	public void aplicaEndosoDevolucion(Short inCanal, Short inRamo,	Integer inIdVenta, List<Object> lista) throws Excepciones;
	public Double calcularDevolucion(Certificado cer, Double tasaAmortiza, Date fechaAnulacion) throws Excepciones;
	public void rehabilitacion(Short canal, Short ramo, Long poliza)throws Excepciones;
	public abstract <T> List<T> recuperaRecibos5758() throws Excepciones;
	public String endosarPoliza(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta, BigDecimal bigDecimal, Date feDesde, Date feHasta, Integer inOperEndoso) throws Excepciones;
	public void validarObligados() throws Excepciones;
	public void validarMultidisposicion(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta) throws Excepciones;
	public void validarCumulosBT()  throws Excepciones;	
	public void actualizarDatosComerciales(List<Object> lstObjetosGuardar) throws Exception;
	
	//dfg
	public abstract String numeradorCliente() throws Excepciones;
	public void  actNumeradorCliente() throws Excepciones;
	public void insertaContratante(String res, String inRazonSocial, Date inFechaCons,
            String inCalleNum,String inCp, String inRfc,String inPoblacion,String inColonia) throws Excepciones;
	
	public Integer buscaNegative(String cacnApellidoPat, String cacnApellidoMat, String cacnNombre, Date cacnFeNacimiento) throws Excepciones;
	public Map<String, Object> buscaCliente (ContratanteDTO contratanteDTO) throws Excepciones;
	public void factura5758(CertificadoId id) throws Excepciones;

	public void contabilidadColectivos(short canal, Short ramo, Long poliza, int certif, int ultRecibo, int mtUltRecibo, int operacion, int subOperacion, String fDesde, String fHasta, String fContable, String nombreFac, int estatusRecibo) throws Exception;
	public void cargaCuentas(Long nRamo, Integer nOperCargo, Double montoAC, Long cuenta, String newFechaFin, String fecha_hoy) throws Excepciones;

	/**
	 * Reservas
	 * CJPV - VECTOR 28/09/2017
	 */
	void  procesaReserva(int anio, int mes) throws Excepciones;
	
	public List<Object> consultaReserva(int ramo, int mes, int anio) throws Excepciones;
}
