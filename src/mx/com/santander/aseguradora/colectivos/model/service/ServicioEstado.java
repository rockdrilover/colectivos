/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;


import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author dflores
 * Modificacion: Sergio Plata
 */
public interface ServicioEstado extends ServicioCatalogo{
	
	public <T> List<T> obtenerObjetos()throws Excepciones;
	public abstract List<Object> obtenerEstados() throws Excepciones;
	
}


