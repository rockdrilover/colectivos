package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.dao.GenericDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys4;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		27-07-2020
 * Description: Clase de tipo Service que tiene logica para el procesos
 * 				Genericos.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 14-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 2.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 20-11-2022
 * 		Porque: Consultar fecha de vencimiento del credito
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioGenericImpl implements ServicioGeneric {

	//Instancia de DAO
	@Resource
	private GenericDao genericDao;
	@Resource
	private ProcesosDao procesosDao;
	
	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioGenericImpl.class);
	
	//Variable para mensajes
	private StringBuilder message;	

	/**
	 * Constructor de Clase
	 */
	public ServicioGenericImpl() {
		message = new StringBuilder();
	}

	@Override
	public String tieneSiniestro(List<BeanClienteCertif> listaCertificados) throws Excepciones {
		//Declaracion de variables
		String strResultado = Constantes.DEFAULT_STRING_ID;
		StringBuilder sbSelect = null;
		ArrayList<Object> arlDatos;
		Object[] arrDatos;
		
		try {
			//Se inicializan variables
			arlDatos = new ArrayList<Object>();
			sbSelect = new StringBuilder();
			
			//Se recorre lista de certificados
			for(BeanClienteCertif objeto: listaCertificados){
				//Si se selecciono u nregistro
				if (objeto.isCheck()){			
					//Se obtiene la consulta
					sbSelect = GeneratorQuerys.getConsultaSiniestros(objeto.getId().getCoccCasuCdSucursal(), 
																	objeto.getId().getCoccCarpCdRamo(), 
																	objeto.getId().getCoccCapoNuPoliza(),
																	objeto.getId().getCoccNuCertificado(), 
																	objeto.getCertificado().getCoceSubCampana());
					//Se ejecuta consulta
					arlDatos.addAll(this.genericDao.consultaSQL(sbSelect));
					break;
				}
			}
			
			//Si no esta vacio el resultado
			if(!arlDatos.isEmpty()) {
				arrDatos = (Object[]) arlDatos.get(0);
				strResultado = arrDatos[0].toString() + "-" + arrDatos[1].toString() + "-" + arrDatos[2].toString();
			}
		} catch (Excepciones e) {			
			//Se escribe error en variable
			this.message.append("Error al consultar siniestro.");
			//Se manda escribir en log
			LOG.error(message.toString(), e);
			//Se regresa excepcion
			throw new Excepciones(e.getMessage());
		}
		
		//Se regresa resultado
		return strResultado;
		
	}

	@Override
	public List<Object> consultaSQL(StringBuilder sbConsulta) throws Excepciones {
		try {
			//Se ejecuta y regresa consulta
			return this.genericDao.consultaSQL(sbConsulta);
		} catch (Excepciones e) {
			//Se escribe error en variable
			this.message.append("Error al ejeuctar consultar.");
			//Se manda escribir en log
			LOG.error(message.toString(), e);
			//Se regresa excepcion
			throw new Excepciones(e.getMessage());
		}
		
	}
	
	@Override
	public void migraInformacion(ClienteCertifId objCliCertId) throws Excepciones {
		try {
			this.procesosDao.migraColectivos(objCliCertId, Short.valueOf("1"), new Date(), Constantes.DEFAULT_INT);
		} catch (Excepciones e) {
			LOG.error("Error al migraColectivos.", e);
		}
	}
	
	@Override
	public Long getNumeroEndoso(short ramo, String stEndoso) throws Excepciones {
		try {
			return this.procesosDao.getNumeroEndoso(ramo, stEndoso);
		} catch (Excepciones e) {
			LOG.error("Error al getNumeroEndoso.", e);
			return Constantes.DEFAULT_LONG;
		}
	}

	@Override
	public void updateSinParametros(StringBuilder sbQuery) throws Excepciones {
		try {
			this.genericDao.updateSinParametros(sbQuery);
		} catch (Excepciones e) {
			//Se escribe error en variable
			this.message.append("Error al ejeuctar update.");
			//Se manda escribir en log
			LOG.error(message.toString(), e);
			//Se regresa excepcion
			throw new Excepciones(e.getMessage());
		}
		
	}
	
	@Override
	public Date getFechaVencimiento(CertificadoId id) throws Excepciones {
		//Declaracion de variables
		Date dResultado = new Date();
		StringBuilder sbSelect = null;
		ArrayList<Object> arlDatos;
		
		try {
			//Se inicializan variables
			arlDatos = new ArrayList<Object>();
			sbSelect = new StringBuilder();
			
			//Se obtiene la consulta
			sbSelect = new StringBuilder(GeneratorQuerys4.queryFechaVencimiento);
			
			//Se reemplazan valores de filtro
			Utilerias.replaceAll(sbSelect,  Pattern.compile("#canal"), String.valueOf(id.getCoceCasuCdSucursal()));
			Utilerias.replaceAll(sbSelect,  Pattern.compile("#ramo"), String.valueOf(id.getCoceCarpCdRamo()));
			Utilerias.replaceAll(sbSelect,  Pattern.compile("#poliza"), String.valueOf(id.getCoceCapoNuPoliza()));
			Utilerias.replaceAll(sbSelect,  Pattern.compile("#cert"), String.valueOf(id.getCoceNuCertificado()));
			
			//Se ejecuta consulta
			arlDatos.addAll(this.genericDao.consultaSQL(sbSelect));
			
			//Si no esta vacio el resultado
			if(!arlDatos.isEmpty()) {
				dResultado = (Date) arlDatos.get(0);
			}
		} catch (Excepciones e) {			
			//Se escribe error en variable
			this.message.append("Error al consultar siniestro.");
			//Se manda escribir en log
			LOG.error(message.toString(), e);
			//Se regresa excepcion
			throw new Excepciones(e.getMessage());
		}
		
		//Se regresa resultado
		return dResultado;
	}
	
	
	/**
	 * @param genericDao the genericDao to set
	 */
	public void setGenericDao(GenericDao genericDao) {
		this.genericDao = genericDao;
	}
	/**
	 * @param procesosDao the procesosDao to set
	 */
	public void setProcesosDao(ProcesosDao procesosDao) {
		this.procesosDao = procesosDao;
	}

}
