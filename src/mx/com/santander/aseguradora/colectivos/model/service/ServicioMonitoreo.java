package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

public interface ServicioMonitoreo {

	public abstract <T> List<T> cargaPolizasEmiCan(short inRamo, Integer inPoliza, Integer inIdVenta, String fInicio, String fFin, int opcion,int tarea) throws Exception;

	public abstract <T> List<T> cargaPolizasConcilia(short inRamo, Integer inPoliza, String fInicio,String fFin, int opcion, int tarea) throws Exception;
	
	public abstract <T> List<T> cargaPolizasTimbrados(short inRamo, Integer inPoliza, Integer inIdVenta, String fInicio,String fFin, int opcion, int tarea) throws Exception;
	
	public abstract <T> List<T> cargaPolizasEndosos(short inRamo, Integer inPoliza, Integer inIdVenta, String fInicio,String fFin, int opcion, int tarea) throws Exception;
	
}
