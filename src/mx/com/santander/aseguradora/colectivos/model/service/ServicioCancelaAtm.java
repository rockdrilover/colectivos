package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCancelaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Interface que implementa la clase ServicioCancelaAtmImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Remote
public interface ServicioCancelaAtm {

	/**
	 * Metodo que manda consultar los creditos a cancelar
	 * @param nombreLayOut nombre de layout a buscar
	 * @param feDesde fecha desde
	 * @param feHasta fecha hasta
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	List<Object> consulta(String nombreLayOut, Date feDesde, Date feHasta) throws Excepciones;
	
	/**
	 * Metodo que consulta el detalle de creditos a cancelar
	 * @param datosResumen datos para buscar detalle
	 * @return lista con detalle
	 * @throws Excepciones con error general
	 */
	List<Object> getDetalle(ResumenCancelaDTO datosResumen) throws Excepciones;

	/**
	 * Metodo que actualiza creditos a cancelar
	 * @param seleccionado datos para hacer update
	 * @param lista creditos a cancelar
	 * @throws Excepciones con error general
	 */
	void cancelaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones;

	/**
	 *  Metodo que elimina creditos por errores o que no se quieren cancelar
	 * @param seleccionado datos para hacer delete
	 * @param lista creditos a eliminar
	 * @throws Excepciones con error general
	 */
	void eliminaCreditos(ResumenCancelaDTO seleccionado, List<DetalleCancelaDTO> lista) throws Excepciones;


	/**
	 * Metodo que manda guardar los registros del archivo
	 * @param archivo archivo a procesar
	 * @return mensaje de ejecucion
	 */
	String guardaDatosArchivo(Archivo archivo);


}
