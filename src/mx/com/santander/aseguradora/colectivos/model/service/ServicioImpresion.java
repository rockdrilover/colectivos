/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

/**
 * @author FEBG
 *
 */
public interface ServicioImpresion extends ServicioCatalogo {
	
	public abstract <T> List<T> obtenerRegistros(int ramo, Long poliza) throws Exception;
	public abstract <T> List<T> cargaPolizasImpresion(int ramo, Long poliza) throws Exception;
	public String reporteAsegurado(String rutaTemp, short ramo, long poliza);
}
