package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.FiltroSuscripcionDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SeguimientoDTO;

/**
 * 
 * @author CJPV - vectormx
 * @version 07-09-2017
 * 
 */
public interface ServicioSeguimiento extends ServicioCatalogo {
	
	/**
	 * Consulta seguimiento de suscripcion por area
	 * @param filtro Parametros consulta
	 * @return Lista de seguimiento
	 * @throws Excepciones Excepcion al ejecutar consulta
	 */
	List<SeguimientoDTO> consultaSeguimientoSuscripcion(
			FiltroSuscripcionDTO filtro) throws Excepciones;
	
	/**
	 * Consulta detalle seguimiento
	 * @param filtro Parametros consulta
	 * @return Objeto Seguimiento 
	 * @throws Excepciones Excepcion al ejecutar consulta
	 */
	SeguimientoDTO consultaObjeto(SeguimientoDTO filtro) throws Excepciones;
	/**
	 * Actualiza datos seguimiento.
	 * @param filtro objeto a actualizar.
	 * @return Objeto guardadp
	 * @throws Excepciones Error al realizar operaciones en BD
	 */
	SeguimientoDTO actualizarSeguimiento(SeguimientoDTO filtro) throws Excepciones;
	
	
}
