/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;


import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.dao.ClienteDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCliente;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.ClienteDTO;

/**
 * @author IBB
 *
 */
@Service
public class ServicioClienteImpl implements ServicioCliente {

	@Resource
	private ClienteDao clienteDao;
	@Resource
	private ProcesosDao procesosDao;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	
	public ServicioClienteImpl() {
		this.message = new StringBuilder();
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#actualizarObjeto(java.lang.Object)
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		Cliente objCliente;
		
		try {
			objCliente = (Cliente) ConstruirObjeto.crearBean(Cliente.class, objeto);
			this.clienteDao.actualizarObjeto(objCliente);
		} catch (Exception e) {
			message.append("no se puede borrar la preCarga.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#borrarObjeto(java.lang.Object)
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		Cliente objCliente;
		try {
			objCliente = (Cliente) ConstruirObjeto.crearBean(Cliente.class, objeto);
			this.clienteDao.borrarObjeto(objCliente);
		} catch (Exception e) {
			message.append("no se puede borrar la preCarga.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjeto(java.lang.Object)
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		Cliente objObligado;
		Long numeroCliente;
		
		try {
			numeroCliente = procesosDao.getNumeroCliente();
			((ClienteDTO) objeto).setCocnNuCliente(numeroCliente);
			
			objObligado = (Cliente) ConstruirObjeto.crearBean(Cliente.class, objeto);
			this.clienteDao.guardarObjeto(objObligado);
		} catch (Exception e) {
			message.append("no se puede guardar el cliente.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
		return objeto;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#guardarObjetos(java.util.List)
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjeto(java.lang.Class, java.io.Serializable)
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.Class)
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.String)
	 */
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		try {
			List<T> lista = this.clienteDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los clientes certififados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	@Override
	public void afterPropertiesSet() throws Exception {
	}
	@Override
	public void destroy() throws Exception {
	}
	
	public ClienteDao getClienteDao() {
		return clienteDao;
	}
	public void setClienteDao(ClienteDao clienteDao) {
		this.clienteDao = clienteDao;
	}
	public ProcesosDao getProcesosDao() {
		return procesosDao;
	}
	public void setProcesosDao(ProcesosDao procesosDao) {
		this.procesosDao = procesosDao;
	}
}
