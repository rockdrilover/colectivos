/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.ValidaEmisionDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioValidaEmision;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;

/**
 * @author Santander Tecnologia JEH
 *
 */
@Service
public class ServicioValidaEmisionImpl implements ServicioValidaEmision{
	/**
	 * Instancia del DAO de validacion
	 */
	@Resource
	private ValidaEmisionDao validaEmisionDao;
	/**
	 * Instancia del log
	 */
	private static final Log LOG = LogFactory.getLog(ServicioValidaEmision.class);
	
	/**
	 * Metodo de logica de negocio para validacion de p�liza colectiva
	 * @param canal se refiere al canal asociado ala poliza
	 * @param ramo se refiere al ramo de la p�liza a validar
	 * @param poliza poliza a validar
	 * @param idVenta identificador de canal de venta
	 * @return mensaje de validacion
	 * @throws Excepciones clase de excepciones especificas de servicios
	 */
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioValidaEmision#validPolizaEmiMasiva(java.lang.Short, java.lang.Short, java.lang.Long, java.lang.Integer)
	 */
	public Map<String, Object> validPolizaEmiMasiva(Short canal, Short ramo, Long poliza, Integer idVenta)
			throws Excepciones {
		/* Inicializacion de cadena de respuesta */
		Map<String, Object> respuesta= new HashMap<String, Object>();
		LOG.info("Iniciando proceso de validacion de p�liza");
		LOG.info("RAMO:" + ramo);
		LOG.info("poliza:" + poliza);
		LOG.info("ideventa" + idVenta);
		try {
			/* Llamado a DAO de validacion de emision */
			respuesta = this.validaEmisionDao.validaemiMasiva(canal, ramo, poliza, idVenta);
			
			LOG.info("Proceso de validaci�n de p�liza finalizado");
			/* Bloque de excepciones */
		} catch (Exception e) {
			/* Bateo de excepcion con mensaje de error */
			LOG.error("Hubo un problema al realizar el proceso de validaci�n.", e);
			throw new Excepciones("Hubo un problema al realizar el proceso de validaci�n.", e);
		}
		/* Retorno de respuesta */
		return respuesta;
		
	}
}
