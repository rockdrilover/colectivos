/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.io.File;
import java.util.List;

/**
 * @author Sergio Plata
 *
 */
public interface ServicioArchivo {
	
	/**
	 * 
	 * @param file
	 * @param delimitador
	 * @return
	 */
	public List<Object> leerArchivoAccidente(File file, String delimitador);
	/**
	 * 
	 * @param file
	 * @param delimitador
	 * @return
	 */
	public List<Object> leerArchivoVida(File file, String delimitador);
	/**
	 * 
	 * @param file
	 * @param delimitador
	 * @return
	 */
	public List<Object> leerArchivoVidaNomina(File file, String delimitador);
	/**
	 * 
	 * @param file
	 * @param delimitador
	 * @return
	 */
	public List<Object> leerArchivoDesempleo(File file, String delimitador);
	/**
	 * 
	 * @param file
	 * @param delimitador
	 * @return
	 */
	public List<Object> leerArchivoDanos(File file, String delimitador);
	/**
	 * 
	 * @param file
	 * @param delimitador
	 * @return
	 */
	public List<Object> leerArchivoSAFE(File file, String delimitador);
}
