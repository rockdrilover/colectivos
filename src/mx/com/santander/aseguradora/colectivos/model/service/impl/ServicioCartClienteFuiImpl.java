package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.dao.CartClienteFuiDao;
import mx.com.santander.aseguradora.colectivos.model.service.*;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class ServicioCartClienteFuiImpl implements ServicioCartClienteFui {
	
	@Resource
	private CartClienteFuiDao cartClienteFuiDao;
	private Log log = LogFactory.getLog(this.getClass());
		
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			T parametro =  this.cartClienteFuiDao.guardarObjeto(objeto);
			return (T) parametro;
		} catch (DataIntegrityViolationException e) {
			this.log.error("Algun dato en cart_cliente _fui es erroneo.", e);
			e.printStackTrace();
			throw new ObjetoDuplicado("Exception.", e);
		}
		catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se puede crear el registro.", e);
			throw new Excepciones("No se puede crear el registro.", e);
		}
	}
	
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		try {
			for(T objeto: lista){
				this.guardarObjeto(objeto);
			}
		} catch (DataIntegrityViolationException e) {
			this.log.error("Exception.", e);
			e.printStackTrace();
			throw new ObjetoDuplicado("Exception.", e);
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se puede guardar el registro.", e);
		}
	}

	
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}


	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public CartClienteFuiDao getCartClienteFuiDao() {
		return cartClienteFuiDao;
	}

	public void setCartClienteFuiDao(CartClienteFuiDao cartClienteFuiDao) {
		this.cartClienteFuiDao = cartClienteFuiDao;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}
}
