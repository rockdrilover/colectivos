/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;




import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.dao.AltaTarifaCoberturaDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaTarifaCobertura;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author dflores
 *
 */
public class ServicioAltaTarifaCoberImpl implements ServicioAltaTarifaCobertura  {

	private Log log = LogFactory.getLog(this.getClass());
	@SuppressWarnings("unchecked")
	private AltaTarifaCoberturaDao altaTarifaCoberDao;
	private String message = null;
	


	@SuppressWarnings("unchecked")
	public void setAltaTarifaCoberDao(AltaTarifaCoberturaDao altaTarifaCoberDao) { 
		this.altaTarifaCoberDao = altaTarifaCoberDao;
	}
	

	@SuppressWarnings("unchecked")
	public AltaTarifaCoberturaDao getAltaTarifaCoberDao() {
		return altaTarifaCoberDao;
	}

	public Integer iniciaAlta( Integer canal, Short ramo, Integer poliza,Integer ramoCont, String cdCobertura, 
			Integer cdProducto, Integer cdPlan ,Date fechIni, Date fechFin, Float monto, Float comision, 
			Float taRiesgo, Integer edadMin, Integer edadMax) throws Excepciones {
		// TODO Auto-generated method stub
		Integer pro = 0;
		this.log.debug("Iniciando proceso de Alta de tarifa y cobertura para esa poliza");
		try {
			
			pro = this.altaTarifaCoberDao.iniciaAlta( canal, ramo,  poliza, ramoCont,  cdCobertura, 
					 cdProducto,  cdPlan , fechIni,  fechFin,  monto,  comision, 
					 taRiesgo,  edadMin,  edadMax);
			return pro;
			
		} catch (Exception e) {
			// TODO: handle exception
			message = "Cobertura ya existencte para esa P�liza ";
			this.log.error(message, e);
			throw new Excepciones(message, e) ;
		}
		
	}


	

}
