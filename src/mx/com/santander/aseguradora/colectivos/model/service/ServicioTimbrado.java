package mx.com.santander.aseguradora.colectivos.model.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.TimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-0-2020
 * Description: Interface que implementa la clase ServicioTimbradoImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 17-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Remote
public interface ServicioTimbrado {

	/**
	 * Metodo que manda consultar los recibos timbrados
	 * @param filtro where a ejecutar
	 * @param fechaHasta fecha desde para filtro
	 * @param fechaDesde fecha hasta para filtro
	 * @param tipoBusqueda tipo de busqueda
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	List<Object> consulta(StringBuilder filtro, Date fechaDesde, Date fechaHasta, Integer tipoBusqueda) throws Excepciones;
	
	/**
	 * Metodo que consulta el detalle de timbrado
	 * @param recibo recibo para buscar
	 * @return lista con datos de timbrado
	 * @throws Excepciones con error general
	 */
	TimbradoDTO getDetalle(BigDecimal recibo) throws Excepciones;

	/**
	 * Metodo que consulta datos para timbrado
	 * @param ramo ramo del recibo a timbrar
	 * @param poliza poliza del recibo a timbrar
	 * @param certificado cert del recibo a timbrar
	 * @param recibo recibo a timbrar
	 * @return objeto con datos de timbrado
	 * @throws Excepciones con error en general
	 */
	TimbradoDTO getDatosTimbrado(byte ramo, int poliza, int certificado, BigDecimal recibo) throws Excepciones;
	
}
