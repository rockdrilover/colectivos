/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author j.estrella.herrera
 *
 */
public interface ServicioValidaEmision {
	
	/**
	 * Metodo del servicio de validacion de p�lizas
	 * @param canal se refiere al canal asociado ala poliza
	 * @param ramo se refiere al ramo de la p�liza a validar
	 * @param poliza poliza a validar
	 * @param idVenta identificador de canal de venta
	 * @return String con mensaje de validacion
	 * @throws Excepciones  clase de excepciones especificas de servicios
	 */
	Map<String, Object> validPolizaEmiMasiva(Short canal, Short ramo, Long poliza, Integer idVenta)
			throws Excepciones;

}
