/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Usuario;
import mx.com.santander.aseguradora.colectivos.model.dao.UsuarioDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioUsuario;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.utils.Validaciones;

/**
 * @author Sergio Plata
 * Modificado Ing: Issac Bautista
 */
 @Service
public class ServicioUsuarioImpl implements ServicioUsuario {
	 
	@Resource 
	private UsuarioDao usuarioDao;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	public ServicioUsuarioImpl() {
		this.message = new StringBuilder();
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		message.append("borrarUsuario");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			this.usuarioDao.borrarObjeto(objeto);
			message.append("Usuario:").append(objeto.toString());
			this.log.debug(message.toString());
		} catch (Exception e) {
			message.append("No se puede borrar el usuario:").append(objeto.toString());
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	@SuppressWarnings("unchecked")
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		message.append("obtenerUsuario.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			Usuario usuario = (Usuario) this.usuarioDao.obtenerObjeto(objeto, id);
			message.append("Usuario:").append(usuario.toString()).append("  recuperado.");
			this.log.debug(message.toString());
			return (T) usuario;
		} catch (Exception e) {
			message.append("No se puede recuperar el usuario.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		message.append("obtenerUsuarios.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			List<Usuario> lista = this.usuarioDao.obtenerObjetos(Usuario.class);
			message.append("Usuarios recuperados satisfactoriamente.");
			this.log.debug(message.toString());
			return (List<T>) lista;
		} catch (Exception e) {
			message.append("No se puede inicializar la lista de usuarios.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		message.append("guardarUsuario.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			Usuario usuario = (Usuario) this.usuarioDao.guardarObjeto(objeto);
			message.append("Usuario con id:").append(usuario.getUsername()).append(" guardado.");
			this.log.debug(message.toString());
			return (T) usuario;
		} catch (DataIntegrityViolationException e) {
			message.append("No se puede guardar el usuario, usuario duplicado.");
			this.log.error(message.toString(), e);
			throw new ObjetoDuplicado(message.toString(), e);
		} catch (Exception e) {
			message.append("No se puede guardar el usuario");
			this.log.error(message, e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Excepciones.class)
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		message.append("actualizarUsuario.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			this.usuarioDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			message.append("No se puede actualizar el usuario.");
			this.log.error(message.toString(), e);
			
			throw new Excepciones(message.toString(), e);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = {"ObjetoDuplicado", "Excepciones"})
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("guardarUsuarios.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			this.usuarioDao.guardarObjetos(lista);
		} catch (Exception e) {
			this.message.append("No se pudó guardar la lista de usuarios.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		this.message.append("obtenerUsuarios.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			List<T> lista = this.usuarioDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.message.append("No se puede recuperar la lista de usuarios.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}

	public String validarContraseñaNueva(String passwordnew, String passwordrep) throws Exception {
		String strRespuesta = Constantes.DEFAULT_STRING;
		StringBuffer sbQuery;
		ArrayList<Object> arlResultados;
		Parametros objParam = null;
		String strPalabras = Constantes.DEFAULT_STRING;
		boolean bPalabra = false; 
		Iterator it;
		
		try {
			if(passwordnew.equals(passwordrep)) {	
				arlResultados = new ArrayList<Object>();
				sbQuery = new StringBuffer();
				sbQuery.append("SELECT cp ");
				sbQuery.append("FROM Parametros cp ");
				sbQuery.append("WHERE cp.copaDesParametro = 'PALABRASNOVALIDAS' ");
				sbQuery.append("AND cp.copaIdParametro = 'PASSWORD'");
				sbQuery.append("AND cp.copaNvalor1 = 1");
				
				arlResultados.addAll(usuarioDao.consultarMapeo(sbQuery.toString())); 
				if(!arlResultados.isEmpty()){
					it = arlResultados.iterator();
					while(it.hasNext()){
						objParam = (Parametros) it.next();
//						if(passwordnew.contains(objParam.getCopaVvalor1())){
						if(passwordnew.toUpperCase().contains(objParam.getCopaVvalor1())){
							bPalabra = true;
						}
						strPalabras = strPalabras + objParam.getCopaVvalor1() + ", ";
					}
				}
				
				if(passwordnew.length() < 8) {
					strRespuesta = strRespuesta + "<br>- Contraseña debe tener almenos 8 caracteres.";
				}
				if(!Validaciones.caracteresEspeciales(passwordnew)) {
					strRespuesta = strRespuesta + "<br>- Contraseña debe tener almenos un caracter especial.";
				}
				if(!Validaciones.tieneNumeros(passwordnew)) {
					strRespuesta = strRespuesta + "<br>- Contraseña debe tener almenos un numero.";
				}
				if(!Validaciones.letrasMayusculas(passwordnew)) {
					strRespuesta = strRespuesta + "<br>- Contraseña debe tener almenos una letra mayuscula.";
				}
				if(!Validaciones.letrasMinusculas(passwordnew)) {
					strRespuesta = strRespuesta + "<br>- Contraseña debe tener almenos una letra minuscula.";
				}
				
				switch (Validaciones.tieneSecuencias(passwordnew)) {
					case 0:
						break;
					case 1:
						strRespuesta = strRespuesta + "<br>- Contraseña no debe tener el mismo caracter repetido 2 o mas veces.";
						break;
					case 2:
						strRespuesta = strRespuesta + "<br>- Contraseña no debe tener secuencias de caracteres.";						
						break;
					case 3:
						strRespuesta = strRespuesta + "<br>- Contraseña no debe tener el mismo caracter repetido 2 o mas veces.";
						strRespuesta = strRespuesta + "<br>- Contraseña no debe tener secuencias de caracteres.";
						break;
				
				}
				if(bPalabra) {
					strRespuesta = strRespuesta + "<br>- Contraseña no debe tener las siguientes palabras: " + strPalabras;
				}
			} else {
				strRespuesta = "- Campo Contraseña y Campo Repetir Contraseña deben ser iguales.";
			}
		} catch (Exception e) {
			strRespuesta = "Error al Validar Contraseña.";
			throw new Exception("Error al Validar Contraseña.", e);
		}
		
		return strRespuesta;
	}

	public void setUsuarioDao(UsuarioDao usuarioDao) {
		this.usuarioDao = usuarioDao;
	}
	public UsuarioDao getUsuarioDao() {
		return usuarioDao;
	}
}
