package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.AnexoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SeguimientoDTO;

/**
 * 
 * @author CJPV - vectormx
 * @version 07-09-2017
 * 
 */
public interface ServicioAnexo extends ServicioCatalogo {
	
	/**
	 * Consulta seguimiento de suscripcion por area
	 * @param filtro Parametros consulta
	 * @return Lista de seguimiento
	 * @throws Excepciones Excepcion al ejecutar consulta
	 */
	List<AnexoDTO> consultaAnexoSuscripcion(
			SeguimientoDTO filtro) throws Excepciones;
	
	/**
	 * Consulta detalle seguimiento
	 * @param filtro Parametros consulta
	 * @return Objeto Anexo 
	 * @throws Excepciones Excepcion al ejecutar consulta
	 */
	AnexoDTO consultaObjeto(AnexoDTO filtro) throws Excepciones;
	
	AnexoDTO actualizarAnexo(AnexoDTO filtro) throws Excepciones;
}
