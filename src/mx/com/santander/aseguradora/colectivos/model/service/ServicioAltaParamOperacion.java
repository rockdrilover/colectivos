package mx.com.santander.aseguradora.colectivos.model.service;


import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


public interface ServicioAltaParamOperacion {
	
	/* public Integer iniciaAltaOpRehabiltacion (  long copaCdParametro, String copaDesParametro,  String copaIdParametro, 
	  String copaVvalor1, String copaVvalor2, 	 String copaVvalor3,
String copaVvalor4,  String copaVvalor5, 	 String copaVvalor6, 	 String copaVvalor7, 	 String copaVvalor8,
Integer copaNvalor1,  Long copaNvalor2, 	 Long copaNvalor3, 	 Integer copaNvalor4, 	 Integer copaNvalor5,
BigDecimal copaNvalor6, 	 BigDecimal copaNvalor7, 	 BigDecimal copaNvalor8, 	 Date copaFvalor1,
Date copaFvalor2, 	 Date copaFvalor3, 	 Date copaFvalor4, 	 Date copaFvalor5 ) throws Excepciones; */
	
	//Dar de alta parametrizacion de la REHABLTACION 
	public Integer iniciaAltaOpRehabiltacion ( String copaDesParametro,  String copaIdParametro, String copaVvalor1,
											   String copaVvalor3,  String copaVvalor5,	String copaVvalor6						
										) throws Excepciones;
	
	//Dar de alta parametrizacion del ENDOSO 
	public Integer iniciaAltaOpEndoso ( String copaDesParametro,  String copaIdParametro, String copaVvalor3, 
										String copaVvalor4,   String copaVvalor5, Integer copaNvalor1,  
										Long copaNvalor2, 	 Long copaNvalor3,  Integer copaNvalor5,  
										BigDecimal copaNvalor6, 	 BigDecimal copaNvalor7
										) throws Excepciones;
	
	// Dar de alta parametrizacion de la FACTURACION  
	public Integer iniciaAltaFacturacion ( String copaDesParametro,  String copaIdParametro,  String copaVvalor4,  
			                               String copaVvalor5, 	 String copaVvalor6,   String copaVvalor7, 	 
			                               String copaVvalor8,   Integer copaNvalor1,  Long copaNvalor2, 	 
			                               Long copaNvalor3, 	 Integer copaNvalor4, 	 Integer copaNvalor5,
			                               BigDecimal copaNvalor7, 	 BigDecimal copaNvalor8
										) throws Excepciones;
	
	
	// Dar de alta parametrizacion de SALDO INSOLUTO Y SUMA ASEGURADA
	
	public Integer iniciaAltaFacturacion ( String copaDesParametro,  String copaIdParametro,  String copaVvalor1, 
										String copaVvalor2,  Integer copaNvalor1,  Long copaNvalor2, 	 
							            Long copaNvalor3, 	 Integer copaNvalor4, 	 Integer copaNvalor5,
							            BigDecimal copaNvalor6
										) throws Excepciones;
	
	// Dar de alta parametrizacion de RESTITUCION INMEDIATA.
	
	
	
	
	
	
	
	
	
	
	
	
	
	  

}
