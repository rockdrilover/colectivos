package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.ReportesCierreDao;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioReportesCierre;


@Service
public class ServicioReportesCierreImpl implements ServicioReportesCierre {
	
	@Resource
	private ReportesCierreDao reportesDao;

	public ReportesCierreDao getReportesDao() {
		return reportesDao;
	}

	public void setReportesDao(ReportesCierreDao reportesDao) {
		this.reportesDao = reportesDao;
	}

	public ArrayList<Object> sacarReportes(int canal,int ramo, String finicio, String ffin, int operacion, int opcion) throws Exception {
		ArrayList<Object> resultado = null;
		
		try {
			resultado = new ArrayList<Object>();
			
			switch (opcion) {
			case 1:
				resultado = reportesDao.extraeComCom(canal);				
				break;
			case 2:
				resultado = reportesDao.extraerMontoRfi(finicio,ffin);
				break;
			case 3:
				resultado = reportesDao.extraerRecCob(finicio,ffin);
				break;
			case 4:
				resultado = reportesDao.extraerRecSnCob(finicio,ffin);
				break;
			case 5:
				resultado = reportesDao.extraerCifCom(finicio,ffin);
				break;
			case 6:
				resultado = reportesDao.extraerCifComU(finicio,ffin);
				break;
			case 7:
				resultado = reportesDao.extraeCifTec(finicio,ffin,operacion);
				break;
			case 8:
				resultado = reportesDao.extraeDetaRfi(finicio,ffin);
				break;
			case 9:
				resultado = reportesDao.extraeComTec(canal);
				break;
			case 10:
				resultado = reportesDao.extraeDosOp(finicio,ffin);
				break;
			case 11:
				resultado = reportesDao.extraeCuatroOP(finicio,ffin);
				break;
			case 12:
				resultado = reportesDao.extraeRepTotCoa(finicio,ffin);								
				break;
			case 13:
				resultado = reportesDao.extraeRepEmi();								
				break;
			case 14:
				resultado = reportesDao.extraeReservas(canal,ramo,finicio,ffin);								
				break;
			case 15:
				resultado = reportesDao.extraeSumaAseg(canal,ramo,finicio,ffin);								
				break;
			default:
				break;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}

}
