package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
public interface ServicioCargaManualHBS {

	public ArrayList<Object> getParemetros(String strDesParametros, String strIdParametro) throws Exception;
	public HashMap<String, Object> getRamosPolizas(ArrayList<Object> arlParam) throws Exception;
	public void datosMesPasado(Short inRamo, String strRemesa, String strPolizas) throws Exception;	
	public void datosNuevosValidar(Short inRamo, Object arlPolizas, String strPolizas) throws Exception;
	public Collection<? extends Object> getResultadosCarga(Short inRamo, String strDescArchivo) throws Exception;
	
}
