package mx.com.santander.aseguradora.colectivos.model.service.impl;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao;
import mx.com.santander.aseguradora.colectivos.model.dao.FacturacionIndDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ReciboDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioFacturacionInd;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

/**
 * @author TONY
 *
 */
	@Service
	public class ServicioFacturacionIndImpl implements ServicioFacturacionInd {

		@Resource
		private FacturacionIndDao facturacionIndDao;
		
		
		public FacturacionIndDao getFacturacionIndDao() {
			return facturacionIndDao;
		}

		public void setFacturacionIndDao(FacturacionIndDao facturacionIndDao) {
			this.facturacionIndDao = facturacionIndDao;
		}

		private Log log = LogFactory.getLog(this.getClass());
		private StringBuilder message;
		/**
		 * @return the reciboDao
		 */
		

		public ServicioFacturacionIndImpl() {
			// TODO Auto-generated constructor stub
			this.message = new StringBuilder();
		}
			
		public String impresionXml2(String rutaTemporal, String nombre, Short canal, Short ramo, Long poliza, BigDecimal careNuRecibo)throws Exception{
			
			String strReciboFiscal;
			String archivo;
			
			try {
				strReciboFiscal = String.valueOf(careNuRecibo);
				
				archivo = this.facturacionIndDao.getXmlTimbrar(canal, ramo, poliza,  strReciboFiscal, rutaTemporal,nombre);
					    
			}catch(Exception e){
				e.printStackTrace();
				this.message.append("No se pudo generar XML");
				this.log.error(message.toString(), e);
				throw new Excepciones(message.toString(), e);
			}
			return archivo;
		}
		
		public String zipFiles(List<String> archivos, String rutaTemporal) throws IOException {
			
			FileInputStream in = null;
			FileOutputStream out = null;
			byte temp[] = new byte[2048];
			ZipOutputStream zip = null;
			ZipEntry entry = null;
			int len = 0;
			String salida = null;
			
			try {
				if(archivos != null && archivos.size() > 0){
					salida = "recibos_" + GestorFechas.formatDate(new Date(), "ddMMyyyy") + ".zip";
					out = new FileOutputStream(rutaTemporal + salida);
					zip = new ZipOutputStream(out);
					
					for(String archivo: archivos){
						in = new FileInputStream(rutaTemporal + archivo);
						entry = new ZipEntry(archivo);
						zip.putNextEntry(entry);
						len = 0;
						while((len = in.read(temp)) != -1){
							zip.write(temp, 0 , len);
							zip.flush();
						}
					}
					
					zip.closeEntry();
					zip.close();
				} else {
					salida = null;
				}
				
				return salida;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if(in != null){
					in.close();
				}
				if(out != null){
					out.close();
				}
			}
			
			return null;
		}
		
		public String imprimeRecibo2(String rutaTemporal, String rutaReporte, String nombre, Short careCasuCdSucursal, Short careCarpCdRamo,
				              Long careCapoNuPoliza, BigDecimal careNuRecibo,Short primaUnica, Long certificado)throws Exception {
			String archivo;
			try{

			archivo=    this.facturacionIndDao.imprimeRecibo2(rutaTemporal, rutaReporte, nombre, careCasuCdSucursal, careCarpCdRamo, careCapoNuPoliza, careNuRecibo, primaUnica, certificado);
			   
			}catch(Exception e){
				e.printStackTrace();
				this.message.append("No se pudo generar zip");
				this.log.error(message.toString(), e);
				throw new Excepciones(message.toString(), e);
			}
			
		    return archivo;	
		}

		
		public List<Object[]> getDatosCertificado() throws Exception{
			
			try {
				
				return this.facturacionIndDao.getDatosCertificado();			
			}
			catch (Exception e) {
				e.printStackTrace();
				this.message.append("Error al recuperar los recibos.");
				this.log.error(message.toString(), e);
				throw new Excepciones (message.toString(), e);
			}
			
		}
		
		public void borrarDatosPrecarga()  throws Excepciones {
			try {
				
				this.facturacionIndDao.borrarDatosPrecarga();
				
			} catch (Exception e) {
				this.message.append("No se pueden borrar los datos de la preCarga y carga.");
				this.log.error(message.toString(), e);
				throw new Excepciones(message.toString(), e);
			}
		}

		    
	}


	
	

