/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * @author Sergio Plata 
 *
 */
@Service
public class ServicioParametrosImpl implements ServicioParametros {

	@Resource	
	private ParametrosDao parametrosDao;
	private Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * @param parametrosDao the parametrosDao to set
	 */
	public void setParametrosDao(ParametrosDao parametrosDao) {
		this.parametrosDao = parametrosDao;
	}

	/**
	 * @return the parametrosDao
	 */
	public ParametrosDao getParametrosDao() {
		return parametrosDao;
	}

	public ServicioParametrosImpl() {
	}
	
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.parametrosDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			throw new Excepciones("No se puede actualizar el parametro:" + objeto.toString(), e);
		}
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		try {
			this.parametrosDao.borrarObjeto(objeto);
		} catch (Exception e) {
			this.log.error("No se puede borrar el parametro:" + objeto.toString(), e);
			throw new Excepciones("No se puede borrar el parametro:" + objeto.toString(), e);
		}
		
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			T parametro =  this.parametrosDao.guardarObjeto(objeto);
			return (T) parametro;
		} catch (DataIntegrityViolationException e) {
			this.log.error("Parametro con id duplicado.", e);
			throw new ObjetoDuplicado("Parametro con id duplicado.", e);
		}
		catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se puede crear el parametro.", e);
			throw new Excepciones("No se puede crear el parametro.", e);
		}
		
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		try {
			for(T objeto: lista){
				this.guardarObjeto(objeto);
			}
		} catch (DataIntegrityViolationException e) {
			this.log.error("Parametro duplicado.", e);
			throw new ObjetoDuplicado("Parametro duplicado.", e);
		} catch (Exception e) {
			this.log.error("No se puede guardar la lista de parametros.", e);
			throw new Excepciones("No se puede guardar la lista de parametros.", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		try {
			Parametros parametros = (Parametros) this.parametrosDao.obtenerObjeto(objeto, id);
			return (T) parametros;
		} catch (Exception e) {
			this.log.error("No se puede recuperar el parametro:" + id, e);
			throw new Excepciones("No se puede recuperar el parametro:" + id, e);
		}
		
	}
	
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		try {
			List<T> lista = this.parametrosDao.obtenerObjetos(objeto);
			return lista;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los parametros.", e);
			throw new Excepciones("No se pueden recuperar los parametros.", e);
		}
		
	}
	
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		try {
			List<T> lista = this.parametrosDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			this.log.error("No se puede recuperar la lista de parametros.", e);
			throw new Excepciones("No se puede recuperar la lista de parametros.", e);
		}
		
	}
	
	public <T> List<T> obtenerCatalogoFechasAjuste(String filtro) throws Excepciones {
		try {
			List<T> lista = this.parametrosDao.obtenerCatalogoFechasAjuste(filtro);
			return lista;
		} catch (Exception e) {
			this.log.error("No se puede recuperar la lista de parametros.", e);
			throw new Excepciones("No se puede recuperar la lista de parametros.", e);
		}
		
	}
	
	public <T> List<T> obtenerCatalogoEndoso(String filtro) throws Excepciones {
		try {
			List<T> lista = this.parametrosDao.obtenerCatalogoEndoso(filtro);
			return lista;
		} catch (Exception e) {
			this.log.error("No se puede recuperar la lista de parametros.", e);
			throw new Excepciones("No se puede recuperar la lista de parametros.", e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}
	
	
	//-----  MGE --//
	public Long siguente() throws Excepciones {
		try {
			return this.parametrosDao.siguente();
		} catch (Exception e) {
			this.log.error("No se puede generar el valor del parametro.", e);
			throw new Excepciones("No se puede generar el valor del parametro.", e);
		}
	}

	public Long siguienteCdParam() throws Excepciones {
		try {
			Long next = this.parametrosDao.siguienteCdParam();
			return next;
		} catch (Exception e) {
			this.log.error("No se puede recuperar el siguiente id de parametros.", e);
			throw new Excepciones("No se puede recuperar el siguiente id de parametros.", e);
		}
	}

	public  <T> List<T>  obtenerProductos(String filtro) throws Excepciones {
         try {
        	 List<T>  producto = this.parametrosDao.obtenerProductos(filtro);
        	 return producto;
		} catch (Exception e) {
			this.log.error("No se puede recuperar el siguiente id de parametros.", e);
			throw new Excepciones("No se puede recuperar el siguiente id de parametros.", e);
		}
	}

	//dfg
	public Integer existe(String filtro) throws Excepciones {
		try {
        	 Integer existe = this.parametrosDao.existe(filtro);
        	 return existe;
		} catch (Exception e) {
			this.log.error("No se puede ejecutar dao existeParametro", e);
			throw new Excepciones("No se puede ejecutar dao existeParametro", e);
		}
	}

	public <T> List<T> obtenerCanalVenta(String filtro) throws Excepciones {
		try{
			List<T> canalVenta = this.parametrosDao.obtenerCanalVenta(filtro);
			return canalVenta;	
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se puede ejecutar servico Obtener Canal Venta", e);
			throw new Excepciones("No se puede ejecutar servico Obtener Canal Venta", e);
		}	
	}

	@Override
	public String getErrorCatalogo(String errores) throws Exception {
		String strErrores = Constantes.DEFAULT_STRING;
		StringBuilder filtro;
		String[] arrCDError, arrError;
		ArrayList<Object> arlDato;
		
		try {
			arrCDError = errores.split("\\|");
			for(int i = 0; arrCDError.length > i; i++){
				if(arrCDError[i] != null || !arrCDError[i].equals(Constantes.DEFAULT_STRING)) {
					arrError = arrCDError[i].split("\\~");
					arlDato = new ArrayList<Object>();
					filtro = new StringBuilder();
					filtro.append("and P.copaDesParametro = 'CATALOGO'" );
					filtro.append("and P.copaIdParametro  = 'ERRORES'" );
					filtro.append("and P.copaNvalor1 = ").append(arrError[0]);
					arlDato.addAll(obtenerObjetos(filtro.toString()));
					
					if(!arlDato.isEmpty()) {
						strErrores = strErrores + ((Parametros) arlDato.get(0)).getCopaRegistro();
						if(arrError.length > 1) {
							strErrores = strErrores + " " + arrError[1] + ".";
						} else {
							strErrores = strErrores + ".";
						}						
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al consultar errores de Catalogo", e);
		}
		
		return strErrores;
	}
	
}
