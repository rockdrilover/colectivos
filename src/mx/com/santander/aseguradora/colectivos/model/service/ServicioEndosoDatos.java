package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.view.dto.BeneficiariosDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.CargaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ClienteCertifDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.TipoEndosoDTO;

public interface ServicioEndosoDatos extends ServicioCatalogo {
  
	/**
	 * Metodo que manda a validar la fecha de nacimiento del cliente 
	 * @param cliente datos del cliente 
	 * @param isPrimaUnica bandera que nos indica si el certificado es prima unica 
	 * @return 
	 * @throws Excepciones
	 */
	Integer validaFecha(ClienteCertifDTO cliente, String isPrimaUnica) throws Excepciones;

	/**
	 * Metodo que consulta los beneficiarios
	 * @param id Objeto con canal, ramo, poliza y certificado
	 * @return Lista de beneficiarios
	 * @throws Excepciones error de consulta
	 */
	List<BeneficiariosDTO> getBeneficiarios(CertificadoId id) throws Excepciones;

	/**
	 * Metodo para consultar descripcion de parentesco.
	 * @param cobeCdParentAseg codigo del parentesco
	 * @return descripcion de parentesco
	 * @throws Excepciones error de consulta
	 */
	String getDescParentesco(String cobeCdParentAseg) throws Excepciones;
	
	/**
	 * Metodo que manda ejecutar funcion de noEndosoDetalle
	 * @param objBeneficiario Datos del beneficiario
	 * @param objTipoEndoso objeto autorizacion
	 * @return regresa numero de endoso siguiente
	 * @throws Excepciones error de proceso
	 */
	String generaEndoso(BeneficiariosDTO objBeneficiario, TipoEndosoDTO objTipoEndoso) throws Excepciones;

	/**
	 * Metodo que regresa un cliente 
	 * @param cliente cliente a consultar
	 * @return cliente
	 * @throws Excepciones error de consulta
	 */
	ClienteCertif getClienteAnterior(ClienteCertifId cliente) throws Excepciones;
	
	/**
	 * Metodo que manda guardar un Endoso en tabla Coletivos_Endosos
	 * @param objClienteAnt Datos Anteriores
	 * @param objEndoso    Datos Nuevos
	 * @param tipoEndoso   Tipo Endoso a Realizar
	 * @param stEndoso Esatus del endoso
	 * @return regresa respuesta de proceso
	 * @throws Excepciones error de proceso
	 */
	String generaEndoso(ClienteCertif objClienteAnt, ClienteCertifDTO objEndoso, String tipoEndoso, short stEndoso) throws Excepciones;

	/**
	 * Metodo que consulta tipo de endosos
	 * @return lista de tipo endosos
	 * @throws Excepciones error en la consulta
	 */
	Collection<Object> getTipoEndosos() throws Excepciones;

	/**
	 * Metodo que regresa los aurotizadores de un tipo endoso
	 * @param tipoEndoso Tipo de endoso a consultar
	 * @return lista de autotizadores tipo endosos
	 * @throws Excepciones Error de consulta
	 */
	Collection<TipoEndosoDTO> getAutorizaTpEndoso(String tipoEndoso) throws Excepciones;

	/**
	 * Metodo que consulta Endosos Pendientes
	 * @param userName Usuario conectaado
	 * @return lista de endosos pendientes 
	 * @throws Excepciones Error de consulta
	 */
	Collection<EndosoDatosDTO> getEndososPendientes(String userName) throws Excepciones;

	/**
	 * Metodo que autoriza / aplica cambios a un endoso
	 * @param seleccionado datos de endoso
	 * @throws Excepciones Error de proceso
	 */
	void autorizarEndoso(EndosoDatosDTO seleccionado) throws Excepciones;

	/**
	 * Metodo que verifica si suma asegurada esta dentro de rango aceptable
	 * @param objEndoso datos de endosos
	 * @return false si no esta dentro de rango
	 * @throws Excepciones error de validacion
	 */
	boolean validaRangoSumas(Certificado objEndoso) throws Excepciones;

	/**
	 * Metodo que Caulcula nuevas primas por cambio de suma asegurada
	 * @param certificado objeto con datos para hacer calculo
	 * @return calculo de nuesvas primas
	 * @throws Excepciones erro en el calculo
	 */
	CargaDTO calculaNuevaPrima(Certificado certificado) throws Excepciones;
	
	/**
	 * Metodo que consulta las fechas desde y hasta de la poliza
	 * @param id datos a consultar
	 * @return fechas desde y hasta
	 * @throws Excepciones error de consulta
	 */
	Object[] getFechasPoliza(CertificadoId id) throws Excepciones;
	
	/**
	 * Metodo para generar RFC
	 * @param id cliente o numero de certificado
	 * @param RFC
	 * @throws Error en execucion de proceso para la generacion de RFC 
	 * 
	 * */
	 
	public String genRFCCert(String RFC, int numCertificado) throws Excepciones ;
	
}
