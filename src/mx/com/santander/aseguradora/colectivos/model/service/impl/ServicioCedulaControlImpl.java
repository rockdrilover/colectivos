package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCedula;
import mx.com.santander.aseguradora.colectivos.model.dao.CedulaControlDao;
import mx.com.santander.aseguradora.colectivos.model.dao.GenericDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCedulaControl;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.CedulaControlDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase de tipo Service que tiene logica para el proceso
 * 				de Cedula Control.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 14-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioCedulaControlImpl implements ServicioCedulaControl {

	//Instancias de DAO
	@Resource
	private CedulaControlDao cedulaDao;
	@Resource
	private GenericDao genericDao;
	
	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioCedulaControlImpl.class);
	
	//Variable para mensajes
	private StringBuilder message;

	/**
	 * Cosntructor de Clase
	 */
	public ServicioCedulaControlImpl() {
		message = new StringBuilder();
	}

	/**
	 * Metodo que manda consultar los errores
	 * @param tipocedula tipo cedula a buscar
	 * @param idVenta ide de venta
	 * @param feDesde fecha desde
	 * @param feHasta fecha hasta
	 * @param remesa remesa para consultar detalle
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	@Override
	public List<CedulaControlDTO> consulta(Integer tipocedula, Integer idVenta, Date feDesde, Date feHasta, String remesa) throws Excepciones {
		List<CedulaControlDTO> lstCedulas;
		List<Object> lstResultado;
		StringBuilder filtro;
		String[] arrRemesa;
		
		try {
			lstCedulas = new ArrayList<CedulaControlDTO>();
			
			//Se crea filtro para consulta
			filtro = new StringBuilder();
			filtro.append("and CC.cc3.coccTipoCedula = " + tipocedula.toString() + " \n" );
			filtro.append("and CC.coccUnidadNegocio = " + idVenta.toString() + " \n" );
			if(remesa != null) {
				//Obetenemos remesa y version de historico
				arrRemesa = remesa.split("\\_"); 
				
				filtro.append("and CC.coccRemesaCedula = '" + arrRemesa[0] + "' \n" );
				
				if(arrRemesa.length == 2) {
					filtro.append("and CC.cc3.coccProcesada = " + arrRemesa[1] + " \n" );
				}
			} else {
				filtro.append(" and CC.cc2.coccFeComer >= '").append(GestorFechas.formatDate(feDesde, Constantes.FORMATO_FECHA_UNO)).append("' \n");
				filtro.append(" and CC.cc2.coccFeComer <= '").append(GestorFechas.formatDate(feHasta, Constantes.FORMATO_FECHA_UNO)).append("' \n");
			}
			filtro.append("order by CC.cc2.coccFeComer" );

			
			//Se ejecuta consulta
			lstResultado = this.genericDao.obtenerObjetos(filtro.toString(), Constantes.TABLA_CEDULA);
			
			//Si no hay resultados regresa lista vacia
			if(lstResultado.isEmpty()) {
				return lstCedulas;
			}
			
			lstCedulas.addAll(setHashMap(lstResultado, tipocedula, remesa).values());
			return lstCedulas;
		} catch (Excepciones e) {
			this.message.append("Error al recuperar cedulas.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	/**
	 * Metodo que genera hash map de cedulas
	 * @param lstResultado lista de resultados
	 * @param tipocedula tipo cedula
	 * @param remesa remesa a generar
	 * @return hash map con datos.
	 */
	private HashMap<String, CedulaControlDTO> setHashMap(List<Object> lstResultado, Integer tipocedula, String remesa) throws Excepciones {
		Iterator<Object> it;
		HashMap<String, CedulaControlDTO> hmCedulas = new HashMap<String, CedulaControlDTO>();
		ColectivosCedula objCedula;
		CedulaControlDTO dto;
		StringBuilder key;
		
		//Iteramos resultados
		it = lstResultado.iterator();
		while(it.hasNext()) {
			objCedula = (ColectivosCedula) it.next();
	    	dto = new CedulaControlDTO(objCedula);
	    	
	    	//set remesa
	    	key = new StringBuilder();
	    	key.append(dto.getCoccRemesaCedula());
	    	
	    	//Si es cedula historica se modifica remesa
	    	if(tipocedula > 2) {
	    		key.append("_").append(dto.getCoccProcesada().toString());
	    		dto.setCoccRemesaCedula(key.toString());
	    	}
	    	
	    	//Si remesa es null se toma como key la fecha
	    	if(remesa != null) {
	    		key = new StringBuilder();
				key.append(dto.getCoccFeComer().toString());
			}
	    	
	    	//Se pregunta se hashmap tiene la key
			if(hmCedulas.containsKey(key.toString())) {
				hmCedulas.put(key.toString(), sumarValores((CedulaControlDTO) hmCedulas.get(key.toString()), dto));
			} else {
				hmCedulas.put(key.toString(), dto);
			}
		}
		
		
		return hmCedulas;
	}

	/**
	 * Metodo que suma valores de cedulas
	 * @param dtoAnt  cedula acumulada anterior 
	 * @param dto cedula nueva
	 * @return nueva Cedula
	 */
	private CedulaControlDTO sumarValores(CedulaControlDTO dtoAnt, CedulaControlDTO dto) {
		dtoAnt.setCoccNuRegistros(dtoAnt.getCoccNuRegistros().add(dto.getCoccNuRegistros()));
		dtoAnt.setCoccNuRegPnd(dtoAnt.getCoccNuRegPnd().add(dto.getCoccNuRegPnd()));
		dtoAnt.setCoccMtPrima(dtoAnt.getCoccMtPrima().add(dto.getCoccMtPrima()));
		dtoAnt.setCoccNuRegOk(dtoAnt.getCoccNuRegOk().add(dto.getCoccNuRegOk()));
		dtoAnt.setCoccMtPrimaOk(dtoAnt.getCoccMtPrimaOk().add(dto.getCoccMtPrimaOk()));
		dtoAnt.setCoccNuRegNook(dtoAnt.getCoccNuRegNook().add(dto.getCoccNuRegNook()));
		dtoAnt.setCoccMtPrimaNook(dtoAnt.getCoccMtPrimaNook().add(dto.getCoccMtPrimaNook()));
		dtoAnt.setCoccNuRegSinpnd(dtoAnt.getCoccNuRegSinpnd().add(dto.getCoccNuRegSinpnd()));
		dtoAnt.setCoccNuRegSinpndOk(dtoAnt.getCoccNuRegSinpndOk().add(dto.getCoccNuRegSinpndOk()));
		dtoAnt.setCoccNuRegSinpndNook(dtoAnt.getCoccNuRegSinpndNook().add(dto.getCoccNuRegSinpndNook()));
		
		return dtoAnt;
	}

	/**
	 * Metodo que consulta el detalle de creditos para descargar.
	 * @param tipocedula tipo de cedula (Emision o Cancelacion)
	 * @param idVenta numero de id venta
	 * @param coccRemesaCedula Remesa a consulta
	 * @return lista de datos
	 * @throws Excepciones con error general
	 */
	@Override
	public List<Object> getDetalleCreditos(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones {
		try {
			switch (tipocedula) {
				case 1:
				case 2:
					return this.cedulaDao.getDetalleCreditos(tipocedula, idVenta, coccRemesaCedula);

				default:
					return this.cedulaDao.getDetalleCredHist(tipocedula, idVenta, coccRemesaCedula);
			}
			
		} catch (Excepciones e) {
			this.message.append("Error al recuperar detalle de creditos.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	@Override
	public void saveFotoCedula(Integer tipocedula, Integer idVenta, String coccRemesaCedula) throws Excepciones {
		//Inicializamos tipocedula2, 3 es historico emision
		Integer tipoCedula2 = 3;
		
		try {		
			//Si tipocedula = 2, tipocedula2 = 4 es historico cancelacion
			if(tipocedula == 2) {
				tipoCedula2 = 4;	
			}
			this.cedulaDao.saveFotoCedula(tipocedula, idVenta, coccRemesaCedula, tipoCedula2);
		} catch (Excepciones e) {
			this.message.append("Error al guardar foto de cedula.");
			LOG.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
		}
	}
	
	/**
	 * @return the cedulaDao
	 */
	public CedulaControlDao getCedulaDao() {
		return cedulaDao;
	}
	/**
	 * @param cedulaDao the cedulaDao to set
	 */
	public void setCedulaDao(CedulaControlDao cedulaDao) {
		this.cedulaDao = cedulaDao;
	}

	
}
