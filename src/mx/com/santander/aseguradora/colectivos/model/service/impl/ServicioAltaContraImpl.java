/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.dao.AltaContratanteDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaContratante;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dflores
 *
 */
@Service
public class ServicioAltaContraImpl implements ServicioAltaContratante  {

	@Resource
	private AltaContratanteDao altaContratanteDao;
	private String message = null;
	private Log log = LogFactory.getLog(this.getClass());


	public void setAltaContratanteDao(AltaContratanteDao altaContratanteDao) { 
		this.altaContratanteDao = altaContratanteDao;
	}
	public AltaContratanteDao getAltaContratanteDao() {
		return altaContratanteDao;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getAnalista(String strCdAnalista) throws Exception {
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
		String strResultado = null;
		
		try {
			arlResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT A.CAAN_NM_ANALISTA ");
			sbQuery.append("FROM CART_ANALISTAS A ");
			sbQuery.append("WHERE A.CAAN_CD_ANALISTA = '").append(strCdAnalista).append("' ");
			
			arlResultados.addAll(altaContratanteDao.consultar(sbQuery.toString())); 
			if(!arlResultados.isEmpty()){
				strResultado = (String) arlResultados.get(0);
			} else {
				strResultado = "NO EXISTE FUNCIONARIO";
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioAltaContra.getAnalista(): " + e.getMessage()); 
		}
		
		return strResultado;
	}

	@Override
	public String getSucursal(Integer nCdSucursal) throws Exception {
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
		String strResultado = null;
		
		try {
			arlResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT SRS.DSSUCBAN ");
			sbQuery.append("FROM SERFIN_REG_SUC SRS ");
			sbQuery.append("WHERE SRS.CDSUCBAN = ").append(nCdSucursal);
			
			arlResultados.addAll(altaContratanteDao.consultar(sbQuery.toString())); 
			if(!arlResultados.isEmpty()){
				strResultado = (String) arlResultados.get(0);
			} else {
				strResultado = "NO EXISTE SUCURSAL";
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioAltaContra.getSucursal(): " + e.getMessage()); 
		}
		
		return strResultado;
	}	

	@Override
	public Object[] getDatosPoliza(Short inRamo, Integer nProducto, Integer nPlan) throws Exception {
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
		Object[] arrResultado = null;
		
		try {
			arlResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			sbQuery.append("SELECT MAX(TO_NUMBER(NVL(CB.COCB_CAMPOV2, 0))) SUMA, SUM(DECODE(CB.COCB_TA_RIESGO, 0, CB.COCB_MT_FIJO, (CB.COCB_TA_RIESGO * CB.COCB_CAMPOV2 * CB.COCB_CAMPOV1)/CB.COCB_CAMPON1)) PMA ");
			sbQuery.append("FROM COLECTIVOS_COBERTURAS CB ");
			sbQuery.append("WHERE CB.COCB_CASU_CD_SUCURSAL = 1");
			sbQuery.append("AND CB.COCB_CARP_CD_RAMO = ").append(inRamo);
			sbQuery.append("AND CB.COCB_CAPU_CD_PRODUCTO = ").append(nProducto);
			sbQuery.append("AND CB.COCB_CAPB_CD_PLAN = ").append(nPlan);
			
			arlResultados.addAll(altaContratanteDao.consultar(sbQuery.toString())); 
			if(!arlResultados.isEmpty()){
				arrResultado = (Object[]) arlResultados.get(0);
			} 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioAltaContra.getDatosPoliza(): " + e.getMessage()); 
		}
		
		return arrResultado;
	}
	
	
	@Override
	public ArrayList<Object> getDatosCP(String strCp) throws Exception {
		Object[] arrResultado = null;
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
	    List<Object> lstColonias, lstPoblaciones;
	    HashMap<String, String> hmPoblacion;
	    String strEstado = null;
	    Iterator<Object> it;
	    
		try {
			arlResultados = new ArrayList<Object>();
			lstColonias = new ArrayList<Object>();
			lstPoblaciones = new ArrayList<Object>();
			hmPoblacion = new HashMap<String, String>();
			sbQuery = new StringBuffer();
			
			sbQuery.append("SELECT DISTINCT CEI.CAIV_DS_POBLACION COLONIA, APF.AUPF_DS_POBLACION POBLACION, CAIV_CAES_CD_ESTADO || ' - ' || CE.CAES_DE_ESTADO DESCEDO ");
			sbQuery.append("FROM CART_ESTADOS_IVA CEI ");
			sbQuery.append("INNER JOIN AUTT_POBLAC_FRONTERA APF ON(AUPF_CAOR_CD_ORG = CAIV_CAOR_CD_ORG AND AUPF_AUFR_CD_ESTADO = CAIV_CAES_CD_ESTADO AND AUPF_CD_POBLACION = CAIV_CD_POBLACION) ");
			sbQuery.append("INNER JOIN CART_ESTADOS CE ON CE.CAES_CD_ESTADO = CAIV_CAES_CD_ESTADO ");
			sbQuery.append("WHERE CAIV_CP_POBLACION = ").append(strCp);
			arlResultados.addAll(altaContratanteDao.consultar(sbQuery.toString())); 

			if(!arlResultados.isEmpty()){
				it = arlResultados.iterator();
				while(it.hasNext()) {
					arrResultado = (Object[]) it.next();
					
					lstColonias.add(new SelectItem(arrResultado[0].toString().replace(" ", "$"),  arrResultado[0].toString()));
					
					if(!hmPoblacion.containsKey(arrResultado[1].toString())) {
						lstPoblaciones.add(new SelectItem(arrResultado[1].toString(),  arrResultado[1].toString()));
						hmPoblacion.put(arrResultado[1].toString(), arrResultado[1].toString());
					}
					
					strEstado = arrResultado[2].toString();
				}
			} 
			
			arlResultados = new ArrayList<Object>();
			if(lstColonias.isEmpty()) { //No se encontraron datos y se envia una bandera con valor "0"
				arlResultados.add(Constantes.DEFAULT_STRING_ID);
			} else {
				arlResultados.add(0, lstColonias);
				arlResultados.add(1, lstPoblaciones);
				arlResultados.add(2, strEstado);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioAltaContra.getDatosCP(): " + e.getMessage()); 
		}
		
		return arlResultados;
	}


	@Transactional(readOnly=false, propagation=Propagation.REQUIRED, rollbackFor={Excepciones.class})
	public boolean actualizaCliente(ContratanteDTO contratanteDTO, Map<String, Object> resultadoCliente) throws Excepciones {
		boolean proceso=false;
		try{
			proceso= this.altaContratanteDao.actualizaCliente(contratanteDTO,  resultadoCliente);
		}catch(Exception e){
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	return proceso;	
	}


	@Override
	public Integer obtenerFormaPago(Short ramo, Integer producto, Integer plan) throws Excepciones {
		boolean proceso=false;
		Integer formaPago=null;
		try{
			formaPago=this.altaContratanteDao.obtenerFormaPago(ramo,producto,plan);
			proceso=true;
		}catch(Exception e){
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	return formaPago;	
	}	


	public boolean actualizaCertificados (Integer producto, Integer plan, String sumaAseg, String prima, Short sucursal, Short ramo, Long poliza, Integer certificado, String formaPago) throws Excepciones{
		boolean proceso=false;
		try{
			this.altaContratanteDao.actualizaCertificados(producto,plan,sumaAseg,prima,sucursal,ramo,poliza,certificado, formaPago);
			proceso=true;
		}catch(Exception e){
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	return proceso;	
	}


	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}


	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}


	public <T> void actualizarObjeto(T objeto) throws Excepciones {
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}


	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		try {
			List<T> lista = this.altaContratanteDao.obtenerObjetos(filtro);
			
			return lista;
		} catch (Exception e) {
			throw new Excepciones("No se puede recuperar la vtaPrecarga.", e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}


	public void destroy() throws Exception {
	}

	public List<Object> consultar(String query) throws Exception {
		List<Object> lista = null;
		
		try {
			lista = altaContratanteDao.consultar(query);
			
			return lista;
		} catch (Exception e) {
			log.error("Error al realizar la consulta.", e);
		}
		
		return null;
	}


	public ArrayList<Object> getDatosParentesco() throws Exception {
		ArrayList<Object> arlResultados;
		StringBuffer sbQuery;
	    
		try {
			arlResultados = new ArrayList<Object>();
			sbQuery = new StringBuffer();
			
			sbQuery.append(" select CC.RV_LOW_VALUE, CC.RV_MEANING ");
			sbQuery.append(" from cg_ref_codes cc ");
			sbQuery.append(" where cc.rv_domain='PARENTESCO' ");
			
			arlResultados.addAll(altaContratanteDao.consultar(sbQuery.toString()));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: ServicioAltaContra.getDatosParentesco(): " + e.getMessage()); 
		}
		return arlResultados;
	}
	@Override
	public void actualizaCartCertificados(short cdSucursal, Short inRamo, Long numeroPoliza, Map<String, Object> resultadoCliente2) throws Exception {
		try {
			this.altaContratanteDao.actualizaCartCertificados(cdSucursal, inRamo, numeroPoliza, resultadoCliente2);
		} catch(Exception e){
			message = "No se puede realizar el proceso de alta.";
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}
}
