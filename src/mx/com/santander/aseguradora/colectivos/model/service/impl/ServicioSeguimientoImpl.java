package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSeguimiento;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSuscripcion;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.SeguimientoDAO;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioSeguimiento;
import mx.com.santander.aseguradora.colectivos.view.dto.FiltroSuscripcionDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SeguimientoDTO;

@Service
public class ServicioSeguimientoImpl implements ServicioSeguimiento {
	
	@Resource
	private SeguimientoDAO seguimientoDAO;
	
	/**
	 * convierte seguimiento DTO a seguimiento persistencia
	 * @param  obj DTO
	 * @return seguimiento Persistencia
	 */
	private ColectivosSeguimiento seguimientoDTOToSeguimiento(SeguimientoDTO obj ){
		ColectivosSeguimiento res = new ColectivosSeguimiento();
		
		res.setCoseId(obj.getCoseId());
		res.setCoseArea(obj.getCoseArea());
		res.setCoseConsecutivo(obj.getCoseConsecutivo());
		res.setCoseEstatus(obj.getCoseEstatus());
		res.setCoseFechaAsignacion(obj.getCoseFechaAsignacion());
		res.setCoseFechaAtencion(obj.getCoseFechaAtencion());
		res.setCoseObservaciones(obj.getCoseObservaciones());
		res.setCoseRol(obj.getCoseRol());
		res.setCoseUsuarioAsigno(obj.getCoseUsuarioAsigno());
		res.setCoseUsuarioAtendio(obj.getCoseUsuarioAtendio());
		res.setCoseGrupo(new BigDecimal(obj.getCoseGrupo()));
		res.setCoseObsAsig(obj.getCoseObsAsig());
		ColectivosSuscripcion colectivosSuscripcion = new ColectivosSuscripcion();
		colectivosSuscripcion.setCosuId(obj.getCoseCosuId());
		
		res.setColectivosSuscripcion(colectivosSuscripcion);
		return res;
	}
	
	/**
	 * convierte seguimiento Persistencia a seguimiento dto
	 * @param  obj persistencia
	 * @return seguimiento DTO
	 */
	private SeguimientoDTO seguimientoToseguimientoDTO(ColectivosSeguimiento obj ){
		SeguimientoDTO res = new SeguimientoDTO();
		
		res.setCoseId(obj.getCoseId());
		res.setCoseArea(obj.getCoseArea());
		res.setCoseConsecutivo(obj.getCoseConsecutivo());
		res.setCoseEstatus(obj.getCoseEstatus());
		res.setCoseFechaAsignacion(obj.getCoseFechaAsignacion());
		res.setCoseFechaAtencion(obj.getCoseFechaAtencion());
		res.setCoseObservaciones(obj.getCoseObservaciones());
		res.setCoseRol(obj.getCoseRol());
		res.setCoseUsuarioAsigno(obj.getCoseUsuarioAsigno());
		res.setCoseUsuarioAtendio(obj.getCoseUsuarioAtendio());
		res.setCoseCosuId(obj.getColectivosSuscripcion().getCosuId());
		res.setCoseGrupo(obj.getCoseGrupo().longValue());
		res.setCoseObsAsig(obj.getCoseObsAsig());
		return res;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		SeguimientoDTO seguimientoDTO = (SeguimientoDTO) objeto;
		ColectivosSeguimiento seguimiento = seguimientoDTOToSeguimiento(seguimientoDTO);
		ColectivosSeguimiento previo = seguimientoDTOToSeguimiento(seguimientoDTO);
		
		previo.setCoseEstatus("P");
		
		previo = seguimientoDAO.consultaSeguimientoPrevio(previo);
		
		if (previo == null) {
			long folio = seguimientoDAO.consecutivoSeguimiento(seguimiento);
			
			seguimiento.setCoseConsecutivo(new BigDecimal(folio));
			
			seguimiento =  seguimientoDAO.guardarObjeto(seguimiento);
			
			return (T) seguimiento;
		} else {
			return (T) previo;
		}
		
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {

		return null;
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		
		seguimientoDAO.actualizarObjeto( objeto);
	}

	
	
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioSeguimiento#actualizarSeguimiento(mx.com.santander.aseguradora.colectivos.view.dto.SeguimientoDTO)
	 */
	@Override
	public SeguimientoDTO actualizarSeguimiento(SeguimientoDTO filtro) throws Excepciones {
		seguimientoDAO.actualizarSeguimiento(seguimientoDTOToSeguimiento(filtro));
		return filtro;
	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}
	
	

	@Override
	public SeguimientoDTO consultaObjeto(SeguimientoDTO filtro)
			throws Excepciones {
		ColectivosSeguimiento colectivosSeguimiento = seguimientoDTOToSeguimiento(filtro);
				
		return seguimientoToseguimientoDTO(seguimientoDAO.obtieneObjeto(colectivosSeguimiento));
	}

	@Override
	public List<SeguimientoDTO> consultaSeguimientoSuscripcion(
			FiltroSuscripcionDTO filtro) throws Excepciones {
		List<SeguimientoDTO> res = new ArrayList<SeguimientoDTO>();
		ColectivosSeguimiento colectivosSeguimiento = new ColectivosSeguimiento();
		ColectivosSuscripcion colectivosSuscripcion = new ColectivosSuscripcion();
		
		colectivosSuscripcion.setCosuId(filtro.getCosuId());		
		colectivosSeguimiento.setColectivosSuscripcion(colectivosSuscripcion);
		colectivosSeguimiento.setCoseArea(filtro.getArea());
		
		List<ColectivosSeguimiento> seguimientos = seguimientoDAO.consultaSeguimientoSuscripcion(colectivosSeguimiento);
		
		for (ColectivosSeguimiento seguimiento : seguimientos) {
			res.add(seguimientoToseguimientoDTO(seguimiento));
		}
		
		return res;
	}

	
	
}
