package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.TimbradoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTimbrado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.TimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-0-2020
 * Description: Clase de tipo Service que tiene logica para el proceso
 * 				de Timbrado.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 11-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioTimbradoImpl implements ServicioTimbrado {

	//Instancia de DAO
	@Resource
	private TimbradoDao timbradoDao;
	
	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioTimbradoImpl.class);
	
	/**
	 * Cosntructor de Clase
	 */
	public ServicioTimbradoImpl() {
		//Nothing use
	}

	/**
	 * Metodo que manda consultar los recibos timbrados
	 * @param filtro where a ejecutar
	 * @param fechaHasta fecha desde para filtro
	 * @param fechaDesde fecha hasta para filtro
	 * @param tipoBusqueda tipo de busqueda
	 * @return lista con registros encontrados
	 * @throws Excepciones con error general
	 */
	@Override
	public List<Object> consulta(StringBuilder filtro, Date fechaDesde, Date fechaHasta, Integer tipoBusqueda) throws Excepciones {
		try {
			//Se manda llamar metodo de DAO
			return this.timbradoDao.getRecTimbrados(filtro, GestorFechas.formatDate(fechaDesde, Constantes.FORMATO_FECHA_UNO), GestorFechas.formatDate(fechaHasta, Constantes.FORMATO_FECHA_UNO), tipoBusqueda);
		} catch (Excepciones e) {
			//Se escribe error en log
			LOG.error("Error al recuperar timbrados.", e);
			//Se regresa excepcion
			throw new Excepciones ("Error al recuperar timbrados.", e);
		}
	}
	
	/**
	 * Metodo que consulta el detalle de timbrado
	 * @param recibo recibo para buscar
	 * @return lista con datos de timbrado
	 * @throws Excepciones con error general
	 */
	@Override
	public TimbradoDTO getDetalle(BigDecimal recibo) throws Excepciones {
		//Declaracion de variables
		TimbradoDTO objDetalle = null;
		List<Object> lista;
		Object[] datos;
		
		try {
			//se consulta el detalle 
			lista = this.timbradoDao.getDetalleTimbrado(recibo);

			//se obtiene el registro de detalle
			if(!lista.isEmpty()) {
				datos = (Object[]) lista.get(0);
				objDetalle = new TimbradoDTO(datos, 1);

				//se buscan coberturas
				objDetalle.setLstCobe(this.timbradoDao.getDetalleCober(objDetalle.getFactura().getCofaNuReciboFiscal()));
				
				//se buscan componentes
				objDetalle.setLstComp(this.timbradoDao.getDetalleComp(objDetalle.getFactura().getCofaNuReciboFiscal()));
			}
			
		} catch (Excepciones e) {
			//Se escribe error en log
			LOG.error("Error al recuperar detalle.", e);
			//Se regresa excepcion
			throw new Excepciones ("Error al recuperar detalle.", e);
		}
		
		//Se regresa objeto detalle
		return objDetalle;
	}
	
	/**
	 * Metodo que consulta datos para timbrado
	 * @param ramo ramo del recibo a timbrar
	 * @param poliza poliza del recibo a timbrar
	 * @param certificado cert del recibo a timbrar
	 * @param recibo recibo a timbrar
	 * @return objeto con datos de timbrado
	 * @throws Excepciones con error en general
	 */
	@Override
	public TimbradoDTO getDatosTimbrado(byte ramo, int poliza, int certificado, BigDecimal recibo) throws Excepciones {
		//Declaracion de variables
		TimbradoDTO objNuevo = null;
		Map<String, Object> mDatos;
		String cdError = Constantes.DEFAULT_STRING_ID;
		
		try {
			//Iniciamos objeto
			objNuevo = new TimbradoDTO();
			objNuevo.getRecibo().setcoreCarpCdRamo(ramo); 
			objNuevo.getRecibo().setcoreCapoNuPoliza(poliza);
			objNuevo.getRecibo().setcoreCaceNuCertificado(certificado);
			objNuevo.getFactura().setCofaNuReciboFiscal(recibo);

			//se consulta los datos
			mDatos = this.timbradoDao.getDatosTimbrado(1, ramo, poliza, certificado, recibo);
			
			//Se obtiene error
			cdError = mDatos.get("V_CD_ERROR").toString();
			
			//Se valida estatus de error
			if("000".equals(cdError)) {
				setDatos(objNuevo, mDatos);
				objNuevo.getRecibo().setcoreCdErrorCobro(Constantes.DEFAULT_STRING);
			} else {
				objNuevo.getRecibo().setcoreCdErrorCobro(timbradoDao.getDescError(cdError));
			}
		} catch (Excepciones e) {
			//Se escribe error en log
			LOG.error("Error al recuperar datos timbrado.", e);
			//Se regresa excepcion
			throw new Excepciones ("Error al recuperar datos timbrado.", e);
		}
		
		//Se regresa objeto nuevo
		return objNuevo;
	}
	
	/**
	 * Metodo que setea datos a objeto
	 * @param objNuevo objeto donde se setean los datos
	 * @param mDatos lista con valores
	 */
	private void setDatos(TimbradoDTO objNuevo, Map<String, Object> mDatos) {
		objNuevo.getFactura().setCofaCampov1(mDatos.get("V_FE_COMPROVANTE").toString());
		objNuevo.getRecibo().setcoreCaloCdLocalidad(mDatos.get("V_LUGAR_EXPEDICION").toString());
		objNuevo.getRecibo().setcoreCamoCdMoneda(mDatos.get("V_TPO_MONEDA").toString());
		objNuevo.getRecibo().setcoreTpTramite(mDatos.get("V_TIPO_COMPROBANTE").toString());
		objNuevo.getRecibo().setcoreInDevolucion(mDatos.get("V_REGIMEN").toString()); 
		objNuevo.getRecibo().setcoreCacnCdNacionalidad(mDatos.get("V_RFC_ASEGURADORA").toString()); 
		objNuevo.getRecibo().setcoreCacnNuCedulaRif(mDatos.get("V_RFC_CLIENTE").toString()); 
		objNuevo.getRecibo().setcoreInEmision(mDatos.get("V_NOMBRE").toString());
		objNuevo.getRecibo().setcoreInFinanciamiento(mDatos.get("V_USOCFDI").toString()); 
		objNuevo.getRecibo().setcoreCapoCdProvisional((Integer) mDatos.get("V_CANTIDAD"));
		objNuevo.getRecibo().setcoreCjegNuEgreso(Integer.parseInt(mDatos.get("V_CD_PROSERV").toString()));
		objNuevo.getRecibo().setcoreInPagoCoaseguroCedido(mDatos.get("V_CLAVE_UNIDAD").toString());
		objNuevo.getRecibo().setcoreCataTaTasa(new BigDecimal(mDatos.get("V_TASA_CUOTA").toString()));
		objNuevo.getRecibo().setcoreInPagoComision(mDatos.get("V_CD_IMPUESTO").toString()); 
		objNuevo.getRecibo().setcoreInPagoFinanciera(mDatos.get("V_TIPRAMO").toString());
		objNuevo.getRecibo().setcoreInPagoPrestamoAutomati(mDatos.get("V_TP_CLIENTE").toString()); 
		objNuevo.getRecibo().setcoreInPrecob(mDatos.get("V_NU_TELEFONO").toString());
		objNuevo.getRecibo().setcoreNuTramite(mDatos.get("V_CP").toString()); 
		objNuevo.getFactura().setCofaNuTrasfer(mDatos.get("V_NU_POLIZA_C").toString());
		objNuevo.getFactura().setCofaCampov2(mDatos.get("V_DESC_RAMO").toString());
		objNuevo.getFactura().setCofaCampov3(mDatos.get("V_DESC_PRODUCTO").toString());
		objNuevo.getRecibo().setcoreMtSumaAsegurada(((BigDecimal)mDatos.get("V_SUMA_ASEG")).longValue());
		objNuevo.getRecibo().setcoreCapoCdFrPago(mDatos.get("V_FR_PAGO").toString()); 
		objNuevo.getFactura().setCofaCampon5((BigDecimal) mDatos.get("V_PAGO"));
		objNuevo.getRecibo().setcoreFeEmision(GestorFechas.generateDate(mDatos.get("V_PFE_DESDE").toString(), "yyyy-MM-dd"));
		objNuevo.getRecibo().setcoreFePrecob(GestorFechas.generateDate(mDatos.get("V_PFE_HASTA").toString(), "yyyy-MM-dd"));
		objNuevo.getFactura().setCofaNuCuenta(mDatos.get("V_TP_CUENTA").toString());
		objNuevo.getFactura().setCofaNuCuentaDep(mDatos.get("V_CUENTA").toString());
		objNuevo.getFactura().setCofaCdNacionalidad(mDatos.get("V_DESC_BANCO").toString()); 
		objNuevo.getFactura().setCofaCdUsuario(mDatos.get("V_DESC_SUCURSAL").toString());
		objNuevo.getFactura().setCofaCampon1(Long.valueOf(mDatos.get("V_CD_SUCURSAL").toString()));	
		objNuevo.getFactura().setCofaNuCedulaRif(mDatos.get("V_R_AMPARA").toString()); 
		objNuevo.getRecibo().setcoreFeDesde(GestorFechas.generateDate(mDatos.get("V_RFE_DESDE").toString(), Constantes.FORMATO_FECHA_UNO)); 
		objNuevo.getRecibo().setcoreFeHasta(GestorFechas.generateDate(mDatos.get("V_RFE_HASTA").toString(), Constantes.FORMATO_FECHA_UNO));
		objNuevo.getFactura().setCofaStRecibo(mDatos.get("V_REC_CONCEPTO").toString());
		objNuevo.getFactura().setCofaCampov4(mDatos.get("V_DOMICILIO").toString());
		objNuevo.getRecibo().setcoreCdMotivo(mDatos.get("V_METODO_PAGO").toString());
		objNuevo.getRecibo().setcoreMtPrimaPura((BigDecimal) mDatos.get("V_PRIMAPURA"));
		objNuevo.getFactura().setCofaMtTotCom((BigDecimal) mDatos.get("V_IVA_PRIMA"));
		objNuevo.getFactura().setCofaMtTotDpo((BigDecimal) mDatos.get("V_DPO"));
		objNuevo.getFactura().setCofaMtTotCom1((BigDecimal) mDatos.get("V_IVA_DPO"));
		objNuevo.getFactura().setCofaMtTotRfi((BigDecimal) mDatos.get("V_RFI"));
		objNuevo.getFactura().setCofaMtTotSua((BigDecimal) mDatos.get("V_IVA_RFI"));
		objNuevo.getFactura().setCofaMtRecibo((BigDecimal) mDatos.get("V_SUBTOTAL"));
		objNuevo.getFactura().setCofaMtTotIva((BigDecimal) mDatos.get("V_MT_IVA"));
		objNuevo.getRecibo().setcoreMtPrima((BigDecimal) mDatos.get("V_MT_PRIMA"));
		objNuevo.getFactura().setCofaMtTotPma((BigDecimal) mDatos.get("V_TOTAL")); 
	}

	/**
	 * @return the timbradoDao
	 */
	public TimbradoDao getTimbradoDao() {
		return timbradoDao;
	}
	/**
	 * @param timbradoDao the timbradoDao to set
	 */
	public void setTimbradoDao(TimbradoDao timbradoDao) {
		this.timbradoDao = timbradoDao;
	}

	
	
}
