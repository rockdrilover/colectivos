package mx.com.santander.aseguradora.colectivos.model.service;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		05-12-2022
 * Description: Interface que implementa la clase ServicioCobranzaImpl.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:		
 * 		Cuando:
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Remote
public interface ServicioCobranza {
	
	/**
	 * Metodo que genera consulta de consulta de reporte seleccionado para que se genere en proceso batch
	 * @param tipoReporte tipo reporte seleccionado
	 * @param ramo ramo seleccionado
	 * @param poliza poliza seleccionado
	 * @param estatus estatus seleccionado
	 * @param fecfin fecha fin seleccionado
	 * @param fecini fecha inicio seleccionado
	 * @return respuesta de que se mando peticion
	 * @throws Excepciones con error en general
	 */
	String consultarRegistrosCobranza(int tipoReporte, Short ramo, Integer poliza, int estatus , String fecfin, String fecini) throws Excepciones;

	/**
	 * Metodo para generar peticion de detalle endoso
	 * @param coedCasuCdSucursal canal del endoso
	 * @param coedCarpCdRamo ramo del endoso
	 * @param coedCapoNuPoliza poliza del endoso
	 * @param coedNuRecibo recibo del endoso
	 * @return respuesta de generacion
	 * @throws Excepciones con error en general
	 */
	 String generarDetalleEndoso(short coedCasuCdSucursal, short coedCarpCdRamo, long coedCapoNuPoliza, BigDecimal coedNuRecibo) throws Excepciones;
	
	/**
	 * Metodo que consulta datos de timbrado
	 * @param canal para realizar consulta
	 * @param ramo ramo para realizar consulta
	 * @param poliza poliza para realizar consulta
	 * @param archivo nombre del archivo 
	 * @param tipo tipo de reporte
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	List<Object> consultaErroresCobranza(Short canal, Short ramo, Integer poliza, String archivo, Integer tipo) throws Excepciones;

	/**
	 * Metodo que ejecuta paquete  de carga Cobranza
	 * @param canal para realizar consulta
	 * @param ramo ramo para realizar consulta
	 * @param poliza poliza para realizar consulta
	 * @param tipo tipo de reporte
	 * @param archivo nombre del archivo 
	 * @return cantidad de errores
	 * @throws Excepciones con error en general
	 */
	Integer cargaCobranza(Short canal, Short ramo, Integer poliza, Integer tipo, String archivo) throws Excepciones;

	/**
	 * Metodo que aplica cobranza al registro seleccionado
	 * @param coceCasuCdSucursal canal del recibo
	 * @param coceCarpCdRamo ramo del recibo
	 * @param coceCapoNuPoliza poliza del recibo
	 * @param coceNuCertificado certificado del recibo
	 * @param coceNuCredito numero de credito
	 * @param coceNoRecibo numero de recibo
	 * @throws Excepciones con error en general
	 */
	void aplicaCobranza(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza, Long coceNuCertificado, String coceNuCredito, BigDecimal coceNoRecibo) throws Excepciones;
}
