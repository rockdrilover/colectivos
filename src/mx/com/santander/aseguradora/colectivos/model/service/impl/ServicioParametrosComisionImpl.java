package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.bllcolectivos.json.EstatusValidacion;
import mx.com.santander.aseguradora.bllcolectivos.processor.ValEstructura;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametrosComision;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.ComisionesDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		08-09-2020
 * Description: Clase de tipo Service que tiene logica para el proceso
 * 				de Parametrizacion de comisiones.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 14-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioParametrosComisionImpl implements ServicioParametrosComision {
	
	//Instancias de DAO
	@Resource
	private ParametrosDao parametrosDao;

	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioParametrosComisionImpl.class);
	
	//Variable para mensajes
	private StringBuilder message;

	/**
	 * Cosntructor de Clase
	 */
	public ServicioParametrosComisionImpl() {
		//Se incialisa propieda para mensajes
		message = new StringBuilder(); 
	}

	/**
	 * Metodo que procesa, valida y guarda registros de LayOut comisiones
	 * @param archivo lay out con reglas de comision
	 * @return archivo de salida con errores
	 */
	@Override
	public Archivo guardaDatosArchivo(File archivo) {
		ValEstructura proVal;
		JSONObject objJSON = null;
		EstatusValidacion ev = null;
		Archivo objLayOut;
		Archivo objSalida = new Archivo();
		ArrayList<Object[]> arlLineas;
		String strEncabezado;
		
		try {
			//Inicializamos propiedades
			proVal = new ValEstructura();
			arlLineas = new ArrayList<Object[]>();
			
			//Inicizalizamos objeto archivo
			objLayOut = new Archivo(archivo);
			
			//Obtenemos lineas del Archivo
			arlLineas.addAll(objLayOut.leerArchivo("?", true));
			strEncabezado = arlLineas.get(0)[0].toString();
			
			//Validamos estructura de primer linea (encabezado)
			objJSON = new JSONObject();
			objJSON.put("linea", strEncabezado);
			objJSON.put("nombreArchivo", objLayOut.getArchivo().getName());
			objJSON.put("estructura", Constantes.DEFAULT_STRING);
			objJSON.put("layOut", Constantes.ESTRUCTURA_LAYOUT_COMISIONES);
			ev = (EstatusValidacion) proVal.validaEstructura(objJSON).get("validaEstructura");
			
			//Validamos si hay error de estructura
			if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
				//Si hay error ya no segimos
				objSalida.setRuta(ev.getMsgEjecucion());
				
				//Regresamos objeto con mensaje de error
				return objSalida;
			}
			
			//Si no hubo error se guardan las lineas del archivo
			objSalida = guardaLineas(arlLineas, strEncabezado, proVal);
		} catch (JSONException je) {
			this.message.append("Error al cargar archivo de cancelacion.");
			LOG.error(message.toString(), je);
			objSalida.setRuta(this.message.toString());
		} catch (Excepciones e) {
			this.message.append("Error al leer LayOut.");
			LOG.error(message.toString(), e);
			objSalida.setRuta(this.message.toString());
		}

		return objSalida;
	}

	/**
	 * Metdodo que valida y guarda los registros del archivo
	 * @param arlLineas lisa con las lienas del archivo
	 * @param strEncabezado encabezado del archivo de salida
	 * @param proVal proceso de libreria
	 * @return archivo de salida con errores
	 */
	private Archivo guardaLineas(ArrayList<Object[]> arlLineas, String strEncabezado, ValEstructura proVal) {
		ArrayList<Object> arlErrores;
		JSONObject objJSON = null;
		EstatusValidacion ev = null;
		Archivo objSalida = new Archivo();
		Iterator<Object[]> it;
		Parametros objParam;
		
		try {
			//Inicialisa Lista
			arlErrores = new ArrayList<Object>();

			//Se crea archivo de salida
			objSalida = new Archivo(FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator, 
									"ErroresCarga_ " + new SimpleDateFormat("ddMMyyyy").format(new Date()), 
									".csv");
			
			//Recorremos lineas del archivo
			it = arlLineas.iterator();
			
			//Saltamos encabezado
			it.next();
			while(it.hasNext()) {
				Object[] linea = it.next();
				
				//Validamos lineas del archivo
				objJSON = new JSONObject();
				objJSON.put("linea", linea[0].toString());
				objJSON.put("layOut", Constantes.ESTRUCTURA_LAYOUT_COMISIONES);
				ev = (EstatusValidacion) proVal.validaGuardaLinea(objJSON).get("validaLinea");
				
				//Valida si hubo error al procesa linea
				if(ev.getStatusEjecucion() != Constantes.CODIGO_JSON_OK) {
					//se agrega error a lista y seguimos con el siguiente registro
					arlErrores.add(ev.getMsgEjecucion());
					continue;
				}
				
				//Crea objeto parametro para segir con validaciones.
				objParam = creaObjetoComision(linea[0].toString().split("\\,"));
				
				//Se gaurda objeto comision
				if(objParam != null) {
					this.parametrosDao.guardarObjeto(objParam);
				}
			}
			
			//Mandamos errores al arhivo
			objSalida.copiarArchivo(strEncabezado.split("\\,"), arlErrores, ",");
		} catch (JSONException je) {
			this.message.append("Error al cargar archivo de cancelacion.");
			LOG.error(message.toString(), je);
			objSalida.setRuta(this.message.toString());
		} catch (Excepciones | IOException e) {
			this.message.append("Error al leer LayOut.");
			LOG.error(message.toString(), e);
			objSalida.setRuta(this.message.toString());
		} 
		
		return objSalida;
	}

	/**
	 * Metodo que crea el objeto para guardar
	 * @param linea linea con datos
	 * @return objeto parametros
	 */
	private Parametros creaObjetoComision(String[] linea) {
		Parametros parametro = null;
		StringBuilder sbComisiones;
		ComisionesDTO objComision;
		
		try {
			//Se incializa variables
			parametro = new Parametros();
			
			//Se pasan valores necesarios para el guardado
			parametro.setCopaCdParametro(parametrosDao.siguente()); 	 
			parametro.setCopaIdParametro("COMISION");
			parametro.setCopaDesParametro("CATALOGO"); 	
			
			parametro.setCopaVvalor1(linea[6]);
			parametro.setCopaVvalor2(linea[7]);
			parametro.setCopaVvalor8(linea[0]);
			parametro.setCopaNvalor1(1);					
			parametro.setCopaNvalor2(new Long(linea[3]));
			parametro.setCopaNvalor3(new Long(linea[4]));
			parametro.setCopaNvalor4(new Integer(linea[5]));
			parametro.setCopaNvalor5(new Integer(linea[2]));
			parametro.setCopaNvalor7(new Double(linea[1]));
			parametro.setCopaFvalor1(GestorFechas.generateDate(linea[8], Constantes.FORMATO_FECHA_UNO));				
			parametro.setCopaFvalor2(GestorFechas.generateDate(linea[9], Constantes.FORMATO_FECHA_UNO));
			
			sbComisiones = new StringBuilder();
			//Set valores comision tecnica emision
			objComision = new ComisionesDTO(linea[10], linea[11], linea[12], linea[13]);
			sbComisiones.append(objComision.toString()).append("~");
			
			//Set valores comision tecnica renovacion
			objComision = new ComisionesDTO(linea[14], linea[15], linea[16], linea[17]);
			sbComisiones.append(objComision.toString()).append("$");
			
			//Set valores comision comercial emision
			objComision = new ComisionesDTO(linea[18], linea[19], linea[20], linea[21]);
			sbComisiones.append(objComision.toString()).append("~");
			
			//Set valores comision comercial renovacion
			objComision = new ComisionesDTO(linea[22], linea[23], linea[24], linea[25]);
			sbComisiones.append(objComision.toString());
		
			//se agrega cadena de comisiones a propiedad correspondiente
			parametro.setCopaRegistro(sbComisiones.toString());
		} catch (Excepciones e) {
			LOG.error("Error al crear objeto", e);
			parametro = null;
		}
		
		return parametro;
	}
}