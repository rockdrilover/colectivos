/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.HomologacionProdPlanesDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHomologaProdPlanes;

/**
 * @author Sergio Plata
 * Modificado: Ing. Issac Bautista 
 *
 */
@Service
public class ServicioHomologaProdPlanesImpl implements ServicioHomologaProdPlanes {

	@Resource
	private HomologacionProdPlanesDao homologaDao;
	private Log log = LogFactory.getLog(this.getClass());


	public void setHomologaDao(HomologacionProdPlanesDao homologaDao) {
		this.homologaDao = homologaDao;
	}

	public HomologacionProdPlanesDao getHomologaDao() {
		return homologaDao;
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.homologaDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			this.log.error("Error al actualizar el registro de homologa.", e);
			throw new Excepciones("Error al actualizar el registro de homologa.", e);
		}
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		try {
			T homologa = this.homologaDao.guardarObjeto(objeto);
			return homologa;
		} catch (ObjetoDuplicado e) {
			throw new ObjetoDuplicado("Registro duplicada.", e);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Excepciones("No se puede guardar la cobertura.");
		}
	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto, String filtro) throws Excepciones {
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		try {
			for(T objeto: lista){
				this.guardarObjeto(objeto);
			}
		} catch (DataIntegrityViolationException e) {
			this.log.error("Registro duplicado.", e);
			throw new ObjetoDuplicado("Registro duplicado.", e);
		} catch (Exception e) {
			this.log.error("No se puede guardar la lista de registros.", e);
			throw new Excepciones("No se puede guardar la lista de registros.", e);
		}
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		try {
			List<T> lista = this.homologaDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden recuperar los productos", e);
			throw new Excepciones("No se pueden recuperar los productos", e);
		}
	}

	public void afterPropertiesSet() throws Exception {
	}

	public void destroy() throws Exception {
	}

}
