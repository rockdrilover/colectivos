/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author dflores
 * Modificacion: Sergio Plata
 */
public interface ServicioProducto extends ServicioCatalogo{

	public Integer siguenteProducto(Short ramo)throws Excepciones;
	
	/**
	 * Consulta producto - planes 
	 * @param filtro ramo - venta
	 * @return Lista de productos.
	 */
    List<Producto> productoPlanes(String filtro)  throws Excepciones;
}
