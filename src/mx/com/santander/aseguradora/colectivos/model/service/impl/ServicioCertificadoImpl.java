/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCfdiErrorW;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProgramarReporteDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanCertificado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanFacturacion;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaCertificado;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleReporteDxP;
import mx.com.santander.aseguradora.colectivos.view.dto.ReporteDxP;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqCancelacionCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqDescargaCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResDescargaCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResTimbradoCFDIDTO;
import mx.isban.cfdi.ws.cancelado.services.WSCanceladoStub;
import mx.isban.cfdi.ws.cancelado.services.WSCanceladoStub.Cancelado;
import mx.isban.cfdi.ws.cancelado.services.WSCanceladoStub.CanceladoE;
import mx.isban.cfdi.ws.cancelado.services.WSCanceladoStub.CanceladoResponseE;
import mx.isban.cfdi.ws.descarga.services.WSDescargaStub;
import mx.isban.cfdi.ws.descarga.services.WSDescargaStub.Descarga;
import mx.isban.cfdi.ws.descarga.services.WSDescargaStub.DescargaE;
import mx.isban.cfdi.ws.descarga.services.WSDescargaStub.DescargaResponseE;
import mx.isban.cfdi.ws.timbrado.services.WSTimbradoStub;
import mx.isban.cfdi.ws.timbrado.services.WSTimbradoStub.Timbrar;
import mx.isban.cfdi.ws.timbrado.services.WSTimbradoStub.TimbrarE;
import mx.isban.cfdi.ws.timbrado.services.WSTimbradoStub.TimbrarResponseE;

import org.apache.axis2.AxisFault;
import org.apache.commons.httpclient.util.DateParseException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

import mx.com.santander.aseguradora.colectivos.view.dto.ResCancelacionCFDIDTO;

import WSTimbrado.EnviaTimbrado;

/**
 * @author Z014058 Modificacion: Sergio Plata
 *
 */
@Service
public class ServicioCertificadoImpl implements ServicioCertificado {

	@Resource
	private CertificadoDao CertificadoDao;
	
	@Resource
	private ParametrosDao parametrosDao;
	
	@Resource
	private ProgramarReporteDao ProgramarReporteDao;
	
	private final Log log = LogFactory.getLog(this.getClass());
	
	private StringBuilder message = null;

	/**
	 * @return the CertificadoDao
	 */
	public CertificadoDao getCertificadoDao() {
		return CertificadoDao;
	}
 
	/**
	 * @param CertificadoDao
	 *            the CertificadoDao to set
	 */
	public void setCertificadoDao(CertificadoDao CertificadoDao) {
		this.CertificadoDao = CertificadoDao;
	}
	
	/**
	 * @return the parametrosDao
	 */
	public ParametrosDao getParametrosDao() {
		return parametrosDao;
	}

	/**
	 * @param parametrosDao the parametrosDao to set
	 */
	public void setParametrosDao(ParametrosDao parametrosDao) {
		this.parametrosDao = parametrosDao;
	}

	/**
	 * @return the log
	 */
	public Log getLog() {
		return log;
	}
	
	/**
	 * @return the message
	 */
	public StringBuilder getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public ServicioCertificadoImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();

	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		try {
			this.CertificadoDao.actualizarObjeto(objeto);
		} catch (Exception e) {
			this.message.append("Error al actualizar el plan.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerCertificados.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);

		try {

			List<T> lista = this.CertificadoDao.obtenerObjetos(filtro);
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	public List<Object> totalCertificados(Short canal, Short ramo, Long Poliza) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("Consulta certificados.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		try {

			List<Object> lista = this.CertificadoDao.totalCertificados(canal, ramo, Poliza);
			return lista;
		} catch (Excepciones e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void cancelaCertificado(Certificado certificado) throws Excepciones {
		// TODO Auto-generated method stub

		this.message.append("cancelaCertificado.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);

		try {

			this.CertificadoDao.cancelaCertificado(certificado);

		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al cancelar el certificado.");
			this.log.error(message.append("Error interno.").toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> getPreFacCertificado(Short canal, Short ramo, Long poliza, Integer inIdVenta)
			throws Excepciones {

		try {
			return this.CertificadoDao.getPreFacCertificado(canal, ramo, poliza, inIdVenta);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void actualizaPreFacPoliza(CertificadoId id) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			this.CertificadoDao.actualizaPreFacPoliza(id);

		} catch (Exception e) {
			this.message.append("Error al ejecutar Prefactura.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public <T> List<T> consultaCertificados(Short canal, Short ramo, long Pol, String tiprep, String tippma, String sts,
			String fechaini, String fechafin) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			return this.CertificadoDao.consultaCertificados(canal, ramo, Pol, tiprep, tippma, sts, fechaini, fechafin);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public String totalCertif(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1, String fecha2,
			Integer inIdVenta, String rutaReportes) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			log.info("servicio --- dao");
			return this.CertificadoDao.totalCertif(canal, ramo, poliza1, poliza2, fecha1, fecha2, inIdVenta,
					rutaReportes);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> consultaMensualTecnico(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1,
			String fecha2) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			log.info("servicio --- dao");
			return this.CertificadoDao.consultaMensualTecnico(canal, ramo, poliza1, poliza2, fecha1, fecha2);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> seleccionaDatosAutos() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.sleccionaDatosAutos");
			return this.CertificadoDao.seleccionaDatosAutos();

		} catch (Exception e) {
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public void actualizaDatosAutos(Short canal, Short ramo, Long poliza, Long nucert, String cred, String nomarch,
			String cred2) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.actualizaDatosAutos");
			this.CertificadoDao.actualizaDatosAutos(canal, ramo, poliza, nucert, cred, nomarch, cred2);

		} catch (Exception e) {
			this.message.append("Error al actualizar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public <T> List<T> generaReporteAutos() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.generaReporteAutos");
			return this.CertificadoDao.generaReporteAutos();

		} catch (Exception e) {
			this.message.append("Error al seleccionar y actualizar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public <T> List<T> cifrasResumenAutos() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.cifrasResumenAutos");
			return this.CertificadoDao.cifrasResumenAutos();

		} catch (Exception e) {
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public <T> List<T> generaReporteEmisionAutos() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.generaReporteEmisionAutos");
			return this.CertificadoDao.generaReporteEmisionAutos();

		} catch (Exception e) {
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public void borrarDatosAutosPrecarga() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.borrarDatosAutosPrecarga");
			this.CertificadoDao.borrarDatosAutosPrecarga();

		} catch (Exception e) {
			this.message.append("Error al borrar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public String consultaMensualEmision(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1,
			String fecha2, Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String reporte)
			throws Excepciones {
		try {
			log.info("servicio --- dao");
			if (ramo == 57 || ramo == 58) {
				return this.CertificadoDao.consultaMensualEmision5758(canal, ramo, poliza1, poliza2, fecha1, fecha2,
						inIdVenta, chk1, chk2, chk3, reporte);
			} else {
				return this.CertificadoDao.consultaMensualEmision(canal, ramo, poliza1, poliza2, fecha1, fecha2,
						inIdVenta, chk1, chk2, chk3, reporte);
			}
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> consultaPolizaCancela(Short canal, Short ramo, Long poliza1) throws Excepciones {
		// TODO Auto-generated method stub
		try {

			log.info("servicio --- dao");
			return this.CertificadoDao.consultaPolizaCancela(canal, ramo, poliza1);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void cancelaCert(CertificadoId id, String fecha1, Integer causa) throws Excepciones {
		// TODO Auto-generated method stub
		try {

			log.info("servicio --- dao");
			this.CertificadoDao.cancelaCert(id, fecha1, causa);
		} catch (Exception e) {
			this.message.append("Error al cancelar los certificados ServCertIMPL.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void conteoCertificados(Certificado certificado) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.actualizaDatosAutos");
			this.CertificadoDao.conteoCertificados(certificado);

		} catch (Exception e) {
			this.message.append("Error al contar certificados ServicioCertificadoImpl.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public void conteoCertificadosMasivo(BeanCertificado certificado) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.actualizaDatosAutos");
			this.CertificadoDao.conteoCertificadosMasivo(certificado);

		} catch (Exception e) {
			this.message.append("Error al contar certificados ServicioCertificadoImpl.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public String reporteFacturados5758() throws Excepciones {
		try {
			log.info("servicio --- dao");
			return this.CertificadoDao.reporteFacturados5758();
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public String reporteErrores5758() throws Excepciones {
		try {
			log.info("servicio --- dao");
			return this.CertificadoDao.reporteErrores5758();
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void borraErrores5758() {
		this.CertificadoDao.borraErrores5758();
	}

	public String generaLayoutRamo5(Short ramo) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.generaLayoutRamo5");
			return this.CertificadoDao.generaLayoutRamo5(ramo);

		} catch (Exception e) {
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public String generaLayoutRamo65(Short ramo) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.generaLayoutRamo65");
			return this.CertificadoDao.generaLayoutRamo65(ramo);

		} catch (Exception e) {
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public String generaLayoutRamo82(Short ramo) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.generaLayoutRamo82");
			return this.CertificadoDao.generaLayoutRamo82(ramo);

		} catch (Exception e) {
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public void borraErrores56582(Short ramo) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioCertificadoIMPL.borraErrores56582");
			this.CertificadoDao.borraErrores56582(ramo);

		} catch (Exception e) {
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	public String generaReporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa,
			String contratoEnlace, String folioEndoso) throws Excepciones {
		try {
			return CertificadoDao.generaReporteEndosoUpgrade(fechaInicial, fechaFinal, empresa, contratoEnlace,
					folioEndoso);
		} catch (Exception e) {
			log.error("Error al generar el reporte de endosos upgrade", e);
			throw new Excepciones("Error al generar reporte.", e);
		}
	}

	public String generaSoporteEndosoUpgrade(String fechaInicial, String fechaFinal, String empresa,
			String contratoEnlace, String folioEndoso) throws Excepciones {
		try {
			return CertificadoDao.generaSoporteEndosoUpgrade(fechaInicial, fechaFinal, empresa, contratoEnlace,
					folioEndoso);
		} catch (Exception e) {
			log.error("Error al generar el reporte detalle endoso upgrade", e);
			throw new Excepciones("Error al generar reporte.", e);
		}
	}

	public String consultaEmisionDesempleo(String fecha1, String fecha2) throws Excepciones {
		try {
			log.info("servicio --- dao");
			return this.CertificadoDao.consultaEmisionDesempleo(fecha1, fecha2);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public String generaObligados(short canal, Short ramo, Long poliza, Long poliza1, Integer inIdVenta, boolean chk1,
			boolean chk2, boolean chk3, String reporte) throws Excepciones {
		try {
			log.info("servicio --- dao");
			return this.CertificadoDao.generaObligados(canal, ramo, poliza, poliza1, inIdVenta, chk1, chk2, chk3,
					reporte);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public String generaDevoluciones(short canal, Short ramo, Long poliza, Long poliza1, String fecha1, String fecha2,
			Integer inIdVenta, String reporte) throws Excepciones {
		try {
			return this.CertificadoDao.generaDevoluciones(canal, ramo, poliza, poliza1, fecha1, fecha2, inIdVenta,
					reporte);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public EnviaTimbrado sellarRecibos(short canal, byte ramo, Double reciboFiscal) throws Excepciones {
		EnviaTimbrado objResultado;
		String strReciboFiscal;
		byte[] buffer;

		try {
			strReciboFiscal = String.valueOf(reciboFiscal);
			buffer = this.CertificadoDao.getXmlTimbrar2(canal, ramo, strReciboFiscal);
			// objResultado = Utilerias.llamarWSTimbrado(buffer, "0000000001", "pwd",
			// "test.timbrado.com.mx", "180.175.236.36", "8080"); //DESARROLLO
			objResultado = Utilerias.llamarWSTimbrado(buffer, "SSG97_t", "pB*3+6Pyb!8Y-4Lt", "cfdi.timbrado.com.mx",
					Constantes.PROXY, Constantes.PROXY_PUERTO); // PRODUCCION

		} catch (Exception e) {
			this.message.append("Error al recuperar timbrado.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

		return objResultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#
	 * sellarRecibosV33(mx.com.santander.aseguradora.colectivos.view.dto.
	 * ReqTimbradoCFDIDTO)
	 */
	@Override
	public ResTimbradoCFDIDTO sellarRecibosV33(ReqTimbradoCFDIDTO req) throws Excepciones {
		ResTimbradoCFDIDTO res = new ResTimbradoCFDIDTO();

		Timbrar reqTimbrar = new Timbrar();

		reqTimbrar.setUsuario(req.getUsuario());
		reqTimbrar.setAplicativo(req.getAplicativo());
		reqTimbrar.setIpdir(req.getIpdir());
		reqTimbrar.setCanal(req.getCanal());
		reqTimbrar.setRamo(req.getRamo());
		reqTimbrar.setPoliza(req.getPoliza());
		reqTimbrar.setCertificado(req.getCertificado());
		reqTimbrar.setRecibo(req.getRecibo());
		reqTimbrar.setColectivo(req.getColectivo());

		TimbrarE timbrarE = new TimbrarE();
		timbrarE.setTimbrar(reqTimbrar);

		// TODO incluir URL con puertos
		
		List<Parametros> cfdiTimbrarParam = parametrosDao.obtenerObjetos(" and copa_des_parametro = 'CFDI'  and COPA_ID_PARAMETRO = 'DATOS'");

		try {
			String[] arrDirecciones = cfdiTimbrarParam.get(0).getCopaRegistro().split(Pattern.quote("|"));
			WSTimbradoStub wsTimbradoStub = new WSTimbradoStub(arrDirecciones[0]);
			TimbrarResponseE resWS = wsTimbradoStub.timbrar(timbrarE);
				
			res.setMensaje(resWS.getTimbrarResponse().getContenedor().getMensaje());
			
			if (!Constantes.CFDI_TIMBRAR_EXISTOSO.equals(res.getMensaje())) {
				CartCfdiErrorW error = new CartCfdiErrorW();				
				error.setCcewCodigo(res.getMensaje());
				error.setCcewTipoOperacion(new BigDecimal(Constantes.CFDI_OPERACION_TIMBRAR));
				
				error =  CertificadoDao.descripcionErrorMensaje(error);
				
				
				res.setDescripcion(error.getCcewDescripcion());
			}
			
		} catch (AxisFault e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion(Constantes.CFDI_ERROR_DESCRIPCION);
		} catch (RemoteException e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion(Constantes.CFDI_ERROR_DESCRIPCION);
		} catch (Excepciones e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion("Error al consultar descripcion del mensaje WS");
		}
		
		return res;
	}
	
	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#descargaCFDI(mx.com.santander.aseguradora.colectivos.view.dto.ReqDescargaCFDIDTO)
	 */
	@Override
	public ResDescargaCFDIDTO descargaCFDI(ReqDescargaCFDIDTO req) throws Excepciones {
		ResDescargaCFDIDTO res = new ResDescargaCFDIDTO();
		
		Descarga descarga = new Descarga();
		
		descarga.setUsuario(req.getUsuario());
		descarga.setAplicativo(req.getAplicativo());
		descarga.setIpdir(req.getIpdir());
		descarga.setCanal(req.getCanal());
		descarga.setRamo(req.getRamo());
		descarga.setPoliza(req.getPoliza());
		descarga.setCertificado(req.getCertificado());
		descarga.setRecibo(req.getRecibo());
		descarga.setUuid(req.getUUID());
		descarga.setFormato(req.getFormato());
		
		DescargaE descargaE = new DescargaE();
		descargaE.setDescarga(descarga);
		
		List<Parametros> cfdiTimbrarParam = parametrosDao.obtenerObjetos(" and copa_des_parametro = 'CFDI'  and COPA_ID_PARAMETRO = 'DATOS'");
		
		try {
			String[] arrDirecciones = cfdiTimbrarParam.get(0).getCopaRegistro().split(Pattern.quote("|"));

			WSDescargaStub wsDescargaStub = new WSDescargaStub(arrDirecciones[2]);
			DescargaResponseE resWS = wsDescargaStub.descarga(descargaE);
				
			res.setMensaje(resWS.getDescargaResponse().getContenedor().getMensaje());
			
			if (!Constantes.CFDI_DESCARGAR_EXISTOSO.equals(res.getMensaje())) {
				CartCfdiErrorW error = new CartCfdiErrorW();				
				error.setCcewCodigo(res.getMensaje());
				error.setCcewTipoOperacion(new BigDecimal(Constantes.CFDI_OPERACION_DESCARGAR));
				
				error =  CertificadoDao.descripcionErrorMensaje(error);
				
				
				res.setDescripcion(error.getCcewDescripcion());
			} else {
				res.setContent(resWS.getDescargaResponse().getContenedor().getArchivo().getTcontent());
				res.setFnombre(resWS.getDescargaResponse().getContenedor().getArchivo().getFnombre());
				try {
					ByteArrayOutputStream output = new ByteArrayOutputStream();
					resWS.getDescargaResponse().getContenedor().getArchivo().getBytes().writeTo(output);
					res.setBytes(output.toByteArray());
				} catch (IOException e) {
					log.error(e.getMessage());
					res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
					res.setDescripcion(Constantes.CFDI_ERROR_DESCRIPCION);
				}
			}
			
		} catch (AxisFault e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion(Constantes.CFDI_ERROR_DESCRIPCION);
		} catch (RemoteException e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion(Constantes.CFDI_ERROR_DESCRIPCION);
		} catch (Excepciones e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion("Error al consultar descripcion del mensaje WS");
		}
		
		return res;
	}

	/* (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#cancelaCFDI(mx.com.santander.aseguradora.colectivos.view.dto.ReqCancelacionCFDIDTO)
	 */
	@Override
	public ResCancelacionCFDIDTO cancelaCFDI(ReqCancelacionCFDIDTO req) throws Excepciones {
		ResCancelacionCFDIDTO res = new ResCancelacionCFDIDTO();

		Cancelado reqTimbrar = new Cancelado();

		reqTimbrar.setUsuario(req.getUsuario());
		reqTimbrar.setAplicativo(req.getAplicativo());
		reqTimbrar.setIpdir(req.getIpdir());
		reqTimbrar.setCanal(req.getCanal());
		
		reqTimbrar.setRecibo(req.getRecibo());
		

		CanceladoE canceladoE = new CanceladoE();
		canceladoE.setCancelado(reqTimbrar);

		// TODO incluir URL con puertos
		
		List<Parametros> cfdiTimbrarParam = parametrosDao.obtenerObjetos(" and copa_des_parametro = 'CFDI'  and COPA_ID_PARAMETRO = 'DATOS'");

		try {
			String[] arrDirecciones = cfdiTimbrarParam.get(0).getCopaRegistro().split(Pattern.quote("|"));

			WSCanceladoStub wsTimbradoStub = new WSCanceladoStub(arrDirecciones[1]);
			CanceladoResponseE resWS = wsTimbradoStub.cancelado(canceladoE);
				
			res.setMensaje(resWS.getCanceladoResponse().getResultado());
			
			if (!Constantes.CFDI_CANCELAR_EXISTOSO.equals(res.getMensaje())) {
				CartCfdiErrorW error = new CartCfdiErrorW();				
				error.setCcewCodigo(res.getMensaje());
				error.setCcewTipoOperacion(new BigDecimal(Constantes.CFDI_OPERACION_CANCELAR));
				
				error =  CertificadoDao.descripcionErrorMensaje(error);
				
				
				res.setDescripcion(error.getCcewDescripcion());
			}
			
		} catch (AxisFault e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion(Constantes.CFDI_ERROR_DESCRIPCION);
		} catch (RemoteException e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion(Constantes.CFDI_ERROR_DESCRIPCION);
		} catch (Excepciones e) {
			log.error(e.getMessage());
			res.setMensaje(Constantes.CFDI_ERROR_LLAMADO);
			res.setDescripcion("Error al consultar descripcion del mensaje WS");
		}
		
		return res;
	}

	public void actualizarDatosCFDI(EnviaTimbrado objTimbrado, short canal, byte ramo, Double reciboFiscal)
			throws Excepciones {

		try {
			this.CertificadoDao.actualizaDatosDFCI(objTimbrado, canal, ramo, reciboFiscal);
		} catch (Exception e) {

			this.message.append("Error al actualizar timbrado.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);

		}
	}

	// dfg TuCarrera
	public String generaDetallePreFactura(int canal, int ramo, int poliza) throws Excepciones {
		try {
			return this.CertificadoDao.generaDetallePreFactura(canal, ramo, poliza);
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	// dfg TuCarrera
	public List<Object> resumenPreFactura(int canal, int ramo, Long poliza) throws Excepciones {
		try {
			return this.CertificadoDao.resumenPreFactura(canal, ramo, poliza);
		} catch (Exception e) {
			this.message.append("Error al recuperar resumen de prefactura.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public String consultaReporteCancelacion(String fecha1, String fecha2, String causaAnulacion) throws Excepciones {
		try {
			log.info("servicio --- dao CAUSA ANULACION");
			return this.CertificadoDao.consultaReporteCancelacion(fecha1, fecha2, causaAnulacion);
		} catch (Exception e) {
			this.message.append("Error al recuperar los datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

	}

	public void actualizarObs(CertificadoId id, String obs) throws Excepciones {
		try {
			this.CertificadoDao.actualizaObservacionesCan(id, obs);
		} catch (Exception e) {
			this.message.append("Error al actualizar observaciones.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> obtenerProdPlan(String filtro) throws Excepciones {
		try {
			List<T> canalVenta = this.CertificadoDao.obtenerProdPlan(filtro);
			return canalVenta;
		} catch (Exception e) {			
			this.log.error("No se puede ejecutar servico Obtener Producto Plan", e);
			throw new Excepciones("No se puede ejecutar servico Obtener Producto Plan", e);
		}
	}

	public <T> BigDecimal maxNumeroDeCredito(String filtro) throws Excepciones {
		try {
			return this.CertificadoDao.maxNumeroDeCredito(filtro);
		} catch (Exception e) {			
			this.log.error("No se puede ejecutar servico Obtener el maximo numero de credito:", e);
			throw new Excepciones("No se puede ejecutar servico Obtener el maximo numero de credito:", e);
		}
	}

	// CArga Polizas
	public <T> List<T> cargaPolizas(int ramo) throws Exception {
		try {
			return CertificadoDao.cargaPolizas(ramo);
		} catch (Exception e) {
			this.message.append("Error al cargar polizas.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public <T> List<T> cargaPolizasRenovar(int ramo, String rmes2, String anio) throws Exception {
		try {
			return CertificadoDao.cargaPolizasRenovar(ramo, rmes2, anio);
		} catch (Exception e) {
			this.message.append("Error al cargar polizas.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public String renovar5758(Short sucursal, Short ramo, Long poliza) throws Excepciones {
		String resp;

		try {
			log.info("servicio --- dao");
			resp = this.CertificadoDao.procesoRenovacionPolizas(ramo, poliza);
			System.out.println("Salgo de procesoRenovacionPolizas1");
			return resp;
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	@Override
	public int getUltimoRecibo(CertificadoId id, BigDecimal prima) throws Excepciones {
		Integer nResultado = Constantes.DEFAULT_INT;

		try {
			log.info("ServicioPolizaImpl.getUltimoRecibo --- dao");
			nResultado = this.CertificadoDao.consultaNumRecibo(id, prima);
			if (nResultado > 0) {
				nResultado = nResultado + 1;
			}
			System.out.println("Salgo de ServicioPolizaImpl.getUltimoRecibo");
			return nResultado;
		} catch (Exception e) {
			this.message.append("Error al recuperar Ultimo Numero Recibo.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	@Override
	public ArrayList<Object> reporteVigor(short canal, Short ramo, Long poliza, Integer inIdVenta, Long poliza1)
			throws Excepciones {
		ArrayList<Object> resultado = null;
		try {
			resultado = new ArrayList<Object>();
			resultado = CertificadoDao.generaReportevigor(canal, ramo, poliza, inIdVenta, poliza1);
		} catch (Exception e) {
			this.message.append("Error al generar el reporte.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		return resultado;
	}

	public String generaReporte5758(String rmes, String anio) throws Excepciones {
		String resp;

		try {
			log.info("servicio --- dao");
			resp = this.CertificadoDao.getReporteRenovacion5758(rmes, anio);
			System.out.println("Salgo de ServicioPolizaImpl.procesoRenovacionPolizas");
			return resp;
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#insertaBanderaBatch(java.lang.Long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Long)
	 */
	@Override
	public void insertaBanderaBatch(Long next, String nombreVigores, String rutaParam, String strMensaje, String strNomArchivo, Long tipoReporte) throws Excepciones {
		try {
			this.CertificadoDao.insertaBanderaBatch(next, nombreVigores, rutaParam, strMensaje, strNomArchivo, tipoReporte, 0L);
		} catch (Exception e) {
			this.message.append("Error al insertar bandera.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#insertaBanderaBatch(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros, java.lang.Double)
	 */
	@Override
	public void insertaBanderaBatch(Parametros objPeticion, Double numRecibo) throws Excepciones {
		try {
			objPeticion.setCopaVvalor5(numRecibo.toString());
			objPeticion.setCopaNvalor8(Constantes.DEFAULT_DOUBLE);
			this.CertificadoDao.insertaBanderaBatch(objPeticion);
		} catch (Exception e) {
			this.message.append("Error al insertar bandera.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#valoresBatch(java.lang.String)
	 */
	@Override
	public Parametros valoresBatch(String strDesc) throws Excepciones {
		try {
			return this.CertificadoDao.valoresBatch(strDesc);
		} catch (Exception e) {
			this.log.error("No se puede recuperar el siguiente id de parametros.", e);
			throw new Excepciones("No se puede recuperar el siguiente id de parametros.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#recibosPendCobro(java.lang.short, java.lang.Short, java.lang.Long)
	 */
	@Override
	public boolean recibosPendCobro(Short canal, Short ramo, Long poliza)  throws Excepciones {
		try {
			return this.CertificadoDao.recibosPendCobro(canal, ramo, poliza);
		} catch (Exception e) {
			this.log.error("No se puede recuperar el siguiente id de parametros.", e);
			throw new Excepciones("No se puede recuperar el siguiente id de parametros.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#actualizaEstatusRecibo(mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId)
	 */
	@Override
	public void actualizaEstatusRecibo(CertificadoId id) throws Excepciones {
		try {
			this.CertificadoDao.actualizaEstatusRecibo(id);
		} catch (Exception e) {
			this.message.append("Error al actualizaEstatusRecibo.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}	
	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#getFechasCierre(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Parametros getFechasCierre(Integer mes, Integer anio) throws Excepciones {
		List<Parametros> lstParam;
		StringBuilder sbQuery;
		
		try {
			
			sbQuery = new StringBuilder();
			sbQuery.append("AND COPA_ID_PARAMETRO = 'CONTABILIDAD' \n");
			sbQuery.append("AND COPA_DES_PARAMETRO = 'FECHAS_CIERRE_MENSUAL' \n");
			sbQuery.append("AND COPA_NVALOR2 = 2 \n");
			sbQuery.append("AND COPA_NVALOR1 = ").append(anio).append(" \n");
			sbQuery.append("AND COPA_VVALOR2 = '").append(GestorFechas.getNombreMes(mes)).append("' \n");
			
			lstParam = parametrosDao.obtenerObjetos(sbQuery.toString());
			if(!lstParam.isEmpty()) {
				return lstParam.get(0);
			}
		} catch (Exception e) {
			this.log.error("No se puede recuperar las fechas de Cierre.", e);
			throw new Excepciones("No se puede recuperar las fechas de Cierre.", e);
		}
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#queryEndosos(short, java.lang.Short, java.lang.Long, java.lang.Integer, java.lang.String, java.lang.String)
	 */
	@Override
	public String queryEndosos(short canal, Short ramo, Long poliza, Integer inIdVenta, String fechaini, String fechafin) throws Excepciones {
		String strResultado = null;
		StringBuilder sb = null;
		Archivo objArchivo;
		String lPol1;
		String lPol2;
		StringBuilder sbFiltro = new StringBuilder();
		
		try {
			objArchivo = new Archivo();
			sb = objArchivo.archivoToString("queryEndosos.sql");
			if (poliza == 0) {
				lPol1 = String.valueOf(inIdVenta) + "00000000";
				lPol2 = String.valueOf(inIdVenta) + "99999999";
			} else {
				lPol1 = String.valueOf(poliza);
				lPol2 = String.valueOf(poliza);
			}
			
			sbFiltro.append("and ce.coed_fe_aplica between to_date('@fechaini 00:00:00','DD/MM/YYYY HH24:MI:SS') and to_date('@fechafin 23:59:59','DD/MM/YYYY HH24:MI:SS')");
			Utilerias.replaceAll(sbFiltro,  Pattern.compile("@fechaini"), fechaini);
			Utilerias.replaceAll(sbFiltro,  Pattern.compile("@fechafin"), fechafin);
			
			Utilerias.replaceAll(sb,  Pattern.compile("@canal"), String.valueOf(canal));
			Utilerias.replaceAll(sb,  Pattern.compile("@ramo"), String.valueOf(ramo));
			Utilerias.replaceAll(sb,  Pattern.compile("@poliza1"), lPol1);
			Utilerias.replaceAll(sb,  Pattern.compile("@poliza2"), lPol2);
			Utilerias.replaceAll(sb,  Pattern.compile("@filtro"), sbFiltro.toString());
			
			strResultado = sb.toString();		
		} catch (Exception e) {
			strResultado = Constantes.DEFAULT_STRING;
			this.log.error("No se puede generar Query Endosos.", e);
			throw new Excepciones(message.toString(), e);
		}	
		
		return strResultado;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#queryEndososConciliados(short, java.lang.Short, java.lang.Long, java.lang.Integer, java.lang.String, java.lang.String)
	 */
	@Override
	public String queryEndososConciliados(short canal, Short ramo, Long poliza, Integer inIdVenta, String fechaini, String fechafin) throws Excepciones {
		String strResultado = null;
		StringBuilder sb = null;
		
		try {
			sb = new StringBuilder();
			sb.append("SELECT T.RAMO, T.POLIZA, T.CERTIFICADO, T.NUM, MIN(T.LINEA) LINEA \n");
			sb.append("FROM ( \n");
			
				sb.append("SELECT EC.COED_CARP_CD_RAMO	RAMO,  \n");
						sb.append("EC.COED_CAPO_NU_POLIZA POLIZA,  \n");
						sb.append("EC.COED_NU_CERTIFICADO CERTIFICADO, \n");
						sb.append("EC.COED_RECIBO NUM, \n");				
						sb.append("EC.COED_CARP_CD_RAMO || '~' ||  \n");
						sb.append("EC.COED_C_COSTO || '~' ||  \n");
						sb.append("EC.COED_CAPO_NU_POLIZA || '~' ||  \n");
						sb.append("EC.COED_RECIBO || '~' ||  \n");
						sb.append("EC.COED_CREDITO || '~' ||  \n");
						sb.append("EC.COED_NU_CERTIFICADO || '~' ||  \n");
						sb.append("EC.COED_IDENTIFICA || '~' ||  \n");
						sb.append("EC.COED_ESTATUS || '~' ||  \n");
						sb.append("EC.COED_ESTATUS_MOV || '~' ||  \n");
						sb.append("EC.COED_CUENTA || '~' ||  \n");
						sb.append("EC.COED_PRODUCTO || '~' ||  \n");
						sb.append("EC.COED_SUBPRODUCTO || '~' ||  \n");
						sb.append("EC.COED_CASU_CD_SUCURSAL || '~' ||  \n");
						sb.append("EC.COED_APELLIDO_P || '~' ||  \n");
						sb.append("EC.COED_APELLIDO_M || '~' ||  \n");
						sb.append("EC.COED_NOMBRE || '~' ||  \n");
						sb.append("EC.COED_SEXO || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FECHA_NACIM, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("EC.COED_BUC || '~' ||  \n");
						sb.append("EC.COED_RFC || '~' ||  \n");
						sb.append("EC.COED_PLAZO || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FINICIO, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FFIN, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FINGRESO, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FDESDE, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FHASTA, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FINICIO_CRE, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FFIN_CRE, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("EC.COED_FFIN_CRED_DOS || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FANULACION, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FCANCELACION, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("EC.COED_SUMA_ASEG || '~' ||  \n");
						sb.append("EC.COED_DEVOLUCION || '~' ||  \n");
						sb.append("EC.COED_MTODEVSIS || '~' ||  \n");
						sb.append("EC.COED_BASE_CALCU || '~' ||  \n");
						sb.append("EC.COED_PRIMA_NETA || '~' ||  \n");
						sb.append("EC.COED_DERECHOS || '~' ||  \n");
						sb.append("EC.COED_RECARGOS || '~' ||  \n");
						sb.append("EC.COED_IVA || '~' ||  \n");
						sb.append("EC.COED_PRIMA_TOTAL || '~' ||  \n");
						sb.append("EC.COED_TARIFA || '~' ||  \n");
						sb.append("EC.COED_PRIMA_VIDA || '~' ||  \n");
						sb.append("EC.COED_PRIMA_DES || '~' ||  \n");
						sb.append("EC.COED_CD_ESTADO || '~' ||  \n");
						sb.append("EC.COED_ESTADO || '~' ||  \n");
						sb.append("EC.COED_CP || '~' ||  \n");
						sb.append("EC.COED_PRODUCT_AS || '~' ||  \n");
						sb.append("EC.COED_PLAN_AS || '~' ||  \n");
						sb.append("EC.COED_CUOTA_BAS || '~' ||  \n");
						sb.append("EC.COED_CUOTA_DES || '~' ||  \n");
						sb.append("EC.COED_DIF_DEV || '~' ||  \n");
						sb.append("EC.COED_NOMASEGDOS || '~' ||  \n");
						sb.append("EC.COED_RFCASEDOS || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FNACEDOS, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("EC.COED_SESASEDOS || '~' ||  \n");
						sb.append("EC.COED_TIPOASEG || '~' ||  \n");
						sb.append("EC.COED_ENDOSO || '~' ||  \n");
						sb.append("EC.COED_ENDOSOVIDA || '~' ||  \n");
						sb.append("EC.COED_ENDOSO_DES || '~' ||  \n");
						sb.append("EC.COED_NUM_RECIB || '~' ||  \n");
						sb.append("EC.COED_CONSEC || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FEDESDEREC, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FEHASTAREC, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("TO_CHAR(EC.COED_FEFACTURA, 'dd/MM/yyyy') || '~' ||  \n");
						sb.append("EC.CREDITONUEVO || '~' ||  \n");
						sb.append("EC.COED_DESCRIPCION || '~' ||  \n");
						sb.append("EC.COED_PARTICIPACION_CON_INGRESO linea  \n");
					sb.append("FROM COLECTIVOS_ENDO_CONCILIADOS EC  \n");			
				sb.append("WHERE EC.COED_CARP_CD_RAMO = ").append(ramo).append(" \n");			
				if (poliza == 0) {
					sb.append(" AND EC.COED_CAPO_NU_POLIZA BETWEEN ").append(inIdVenta).append("00000000 AND ").append(inIdVenta).append("99999999 \n");
				} else {
					sb.append(" AND EC.COED_CAPO_NU_POLIZA = ").append(poliza).append(" \n");
				}			
					sb.append("AND EC.COED_NU_CERTIFICADO 	> 0  \n");
					
					sb.append("AND EC.COED_FEFACTURA BETWEEN TO_DATE('").append(fechaini).append("' ||' 00:00:00','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('").append(fechafin).append("' ||' 23:59:59','dd/MM/yyyy HH24:MI:SS')  \n");
					
			sb.append(") T  \n");
			sb.append("GROUP BY T.RAMO, T.POLIZA, T.CERTIFICADO, T.NUM  \n");
			sb.append("ORDER BY 1,2,3,4  \n");
			
			strResultado = sb.toString();
		} catch (Exception e) {
			strResultado = Constantes.DEFAULT_STRING;
			this.log.error("No se puede generar Query Endosos Conciliados.", e);
			throw new Excepciones(message.toString(), e);
		}
		
		return strResultado;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado#consultaRecibos(short, java.lang.Short, java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public List<BeanFacturacion> consultaRecibos(short canal, Short ramo, Long poliza, String fechaDesde, String fechaHasta) throws Excepciones {
		List<BeanFacturacion> lstResultados;
		List<Object> lista;
		StringBuilder filtro;
		BeanFacturacion bean;
		Iterator<Object> it;
		Object[] arrDatos;
		
		try {
			lstResultados = new ArrayList();
			lista = new ArrayList();
			filtro = new StringBuilder();

			filtro.append("SELECT CF.COFA_FE_FACTURACION, CF.COFA_CUOTA_RECIBO, CF.COFA_NU_RECIBO_FISCAL, \n");
					filtro.append("CR.CORE_FE_DESDE, CR.CORE_FE_HASTA, CF.COFA_MT_TOT_PMA, CF.COFA_TOTAL_CERTIFICADOS \n");
					filtro.append("FROM COLECTIVOS_FACTURACION CF \n");
					filtro.append("INNER JOIN COLECTIVOS_RECIBOS CR ON(CF.COFA_CASU_CD_SUCURSAL = CR.CORE_CASU_CD_SUCURSAL AND CF.COFA_NU_RECIBO_FISCAL = CR.CORE_NU_RECIBO) \n");
			filtro.append("WHERE CF.COFA_CASU_CD_SUCURSAL = ").append(canal).append(" \n");
				filtro.append("AND CF.COFA_CARP_CD_RAMO = ").append(ramo).append(" \n");
				filtro.append("AND CF.COFA_CAPO_NU_POLIZA = ").append(poliza).append(" \n");
				filtro.append("AND CF.COFA_FE_FACTURACION BETWEEN TO_DATE('").append(fechaDesde).append("', 'dd/MM/yyyy') AND TO_DATE('").append(fechaHasta).append("', 'dd/MM/yyyy') \n");
				filtro.append("AND CR.CORE_ST_RECIBO = 4 \n");
			filtro.append("ORDER BY 1 DESC  \n");

			lista.addAll(this.CertificadoDao.consultarRegistros(filtro.toString()));
			
			if(lista.isEmpty()) {
				lstResultados = null;
			} else {
				it = lista.iterator();
				while(it.hasNext()) {
					arrDatos = (Object[]) it.next();
					bean = new BeanFacturacion();
					bean.setCofaFeFacturacion((Date) arrDatos[0]);
					bean.setCofaCuotaRecibo(((BigDecimal) arrDatos[1]).shortValue());
					bean.setCofaCampon5((BigDecimal) arrDatos[2]);
					bean.setCofaCampof2((Date) arrDatos[3]);
					bean.setCofaCampof3((Date) arrDatos[4]);
					bean.setCofaMtTotPma((BigDecimal) arrDatos[5]);
					bean.setCofaTotalCertificados((BigDecimal) arrDatos[6]);
					
					lstResultados.add(bean);
				}
			}
		} catch (Exception e) {
			message.append("Error al consultar Recibos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
		return lstResultados;
	}
/*	@Override
	public String reporteProgramado(Short canal, Short ramo, Long poliza1, Long poliza2, String fecha1,
			String fecha2, Integer inIdVenta, boolean chk1, boolean chk2, boolean chk3, String reporte, String usuario)
			throws Excepciones {*/
		
	@Override
	public String reporteProgramado(List<Object> listaReporte)
				throws Excepciones {
		
		try {
			log.info("servicio --- dao");
			Short ramo = (Short) listaReporte.get(1);
			
			if (ramo == 57 || ramo == 58) {
				return this.ProgramarReporteDao.consultaMensualEmision5758(listaReporte);
			} else {
				return this.ProgramarReporteDao.consultaMensualEmision(listaReporte);			
			}
		} catch (Exception e) {
			this.message.append("Error en capa de servicio");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * Author: Towa (JJFM)
	 * consultaReporteDxP
	 * @return Array con la informacion para el reporte de Recibos.
	 */
	@Override
	public ArrayList<ReporteDxP> consultaReporteDxP(Short shortCanal, Short shortRamo, long longPoliza,
			Short shortCert, String strFechaVigencia, String strFechaBusqueda, short idVenta) throws Excepciones {
		// TODO Auto-generated method stub
		List<ReporteDxP> lstResultados = new ArrayList<ReporteDxP>();
		try {
			
			//												//Se obtiene el Resumen del reporte de DxP
			ArrayList<ReporteDxP> reportedxp  = this.CertificadoDao.rdxpObtenerResumenDxP(shortCanal, shortRamo, longPoliza, 
					shortCert, strFechaVigencia, strFechaBusqueda, idVenta);
			
			//												//Si el Resumen se obtiene con exito se consulta el 
			//												//		detalle del reporte DxP.
			if (
					reportedxp != null
					&& !reportedxp.isEmpty()
			) {
			
				ArrayList<DetalleReporteDxP> darrdetalleDxP = 
						this.CertificadoDao.arrdetdxpObtenerDetalleDxP(shortCanal, shortRamo, longPoliza, shortCert, 
								strFechaVigencia, strFechaBusqueda, idVenta);
				//ArrayList<DetalleReporteDxP> darrdetalleDxPLast = new ArrayList<DetalleReporteDxP>();
				
				
				//darrdetalleDxPLast.add(darrdetalleDxP.get(darrdetalleDxP.size()-1));
				
				for (ReporteDxP reporte : reportedxp) {
					reporte.setarrDetalleDXP(darrdetalleDxP);
					reporte.subCalculateValues();
				}
			
			
			}
			
			
			return reportedxp;
		
		} catch (Exception e) {
			message.append("Error al consultar Recibos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
		
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * Author: Towa (JJFM)
	 * exportarReporteDxP
	 * @return File - Archivo de reporte DxP.
	 */
	@Override
	public File exportarReporteDxP(
			Short canal, 
			Short ramo, 
			long poliza1, 
			String fecha1, 
			String fecha2, 
			Integer inIdVenta,
			boolean chk1,
			boolean chk2, 
			boolean chk3, 
			String rutaReportes,
			Short cuota,
			int filtro
	) throws Excepciones {
		File fileReport;
		try {
			fileReport = this.CertificadoDao.ExportarReporteDxP(
					canal, ramo, poliza1, fecha1, fecha2, inIdVenta, chk1, chk2, chk3, rutaReportes, cuota, filtro);
		} catch (Excepciones ex) {
			fileReport = null;
		}
		
		return fileReport;
	}
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * Author: Towa (JJFM)
	 * exportarReporteRecibos
	 * @return File - archivo con el Reporte de Recibos. 
	 */
	@Override
	public File exportarReporteRecibos(
			Short canal, 
			Short ramo, 
			long poliza1, 
			String fecha1, 
			String fecha2,
			Integer inIdVenta, 
			boolean boolFiltroCobrado,
			boolean boolFiltroPendientecobro, 
			String rutaReportes
	) throws Excepciones {
		final int intSATUTUS_COBRADO = 4;
		final int intSTATUS_PENDIENTE_COBRO = 1;
		
		try {
			
			//												//Se configura la ruta y nombre del archivo a generar.
			//												//	*La ruta del achivo es recibida como parametro.
			//												//	*El nombre de archivo es preestablecido como:
			//												//			RECIBOS_[YYYYMMDD].csv 
			//												//			donde YYYYMMDD es la fecha en la que se gegnera
			//												//			el reporte.
			
			String strFileName = String.format("RECIBOS_%s.csv", this.strObtenerFechaEnFormato("yyyyMMdd"));
			String strRutaReporte = rutaReportes.concat(strFileName);
			File reporte = new File(strRutaReporte);
			CSVWriter writer = new CSVWriter(new FileWriter(reporte));
			
			//												//Se agrega el encabezado que tendra el archivo separados
			//												//	por pipes(|).
			
			String[] arrstrHeaders = {
									  "Mes_Contable|Poliza|Recibo|Prima_Neta|RFI|IVA|Prima_Total|Estatus|Fecha_Cobro|"
									+ "Cob_Prima_Neta|Cob_RFI|Cob_IVA|Cob_Prima_Total|Noc_Prima_Neta|Noc_RFI|Noc_IVA|"
									+ "Noc_Prima_Total"
									};
			
			writer.writeNext(arrstrHeaders, false);
			
			//												//Se hace consulta a base de datos para obtener los datos
			//												//	y agregarlos en el reporte.
			//												//La consulta ya genera el formato adecuado con los datos
			//												//	separados por pipes(|) en cada fila.
			
			List<Integer> darrStatusRecibos= new ArrayList<>();
			
			if (
					boolFiltroCobrado
			) {
				darrStatusRecibos.add(intSATUTUS_COBRADO);
			}
			
			if (
					boolFiltroPendientecobro
			) {
				darrStatusRecibos.add(intSTATUS_PENDIENTE_COBRO);
			}
			
			ArrayList<String[]> arrstrResults = this.CertificadoDao.ExportarReporteRecibos(
					canal, ramo, poliza1, fecha1, fecha2, inIdVenta, (List<Integer>) darrStatusRecibos, rutaReportes);
			
			writer.writeAll(arrstrResults, false);	
			writer.close();
				
			
			return reporte;
			
		} catch (Excepciones | IOException | DateParseException e) {
			message.append("Error al generar Reporte de Recibos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	
		
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * @author: Towa (JJFM)
	 * @description: Metodo de apoyo para obtener fecha actual en el formato solicitado.
	 * @param: strFormat el formato con el cual se requiere generar la fecha.
	 * @return: String strDate con la fecha actual en el formato solicitado. 
	 */
	private String strObtenerFechaEnFormato(String strFormat) throws DateParseException {
		
		Calendar now = new GregorianCalendar();
		Date nowDate = now.getTime();
		return (new SimpleDateFormat(strFormat)).format(nowDate);
		
	}
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * Author: Towa (JJFM)
	 * ObtenerVigencias
	 * @return arrstrObtenerVigencias
	 */
	@Override
	public ArrayList<String> arrstrObtenerVigencias(Short shortCanal, Short shortRamo, long longPoliza, short shortCert
			) throws Excepciones{
		try {
		ArrayList<String> arrstrResults = 
				this.CertificadoDao.arrstrObtenerVigencias(shortCanal, shortRamo, longPoliza, shortCert);
	
		return arrstrResults;
		} catch (Excepciones ex) {
			throw new Excepciones("Error al obtener las vigencias", ex);
		}
	}
}
//=====================================================================================================================
