package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.Logger;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.dao.CargaCobranzaDao;
import mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCobranza;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		05-12-2022
 * Description: Clase de tipo Service que tiene logica para el proceso
 * 				de Reportes Cobranza.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Stateless
@Service
public class ServicioCobranzaImpl implements ServicioCobranza {
	
	//Instancias de DAO
	@Resource
	private CertificadoDao certificadoDAO;
	
	@Resource
	private CargaCobranzaDao cargaCobranza;
	
	//Variable log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(ServicioCobranzaImpl.class);
	
	/**
	 * Metodo que genera consulta de consulta de reporte seleccionado para que se genere en proceso batch
	 * @param tipoReporte tipo reporte seleccionado
	 * @param ramo ramo seleccionado
	 * @param poliza poliza seleccionado
	 * @param estatus estatus seleccionado
	 * @param fecfin fecha fin seleccionado
	 * @param fecini fecha inicio seleccionado
	 * @return respuesta de que se mando peticion
	 * @throws Excepciones con error en general
	 */
	@Override
	public String consultarRegistrosCobranza(int tipoReporte, Short ramo, Integer poliza, int estatus , String fecfin, String fecini) throws Excepciones {
		
		StringBuilder strQuery = new StringBuilder();
		String columnas = Constantes.DEFAULT_STRING;
		String nomArchivo = Constantes.DEFAULT_STRING;
		
		//Se asignan columnas y nombre de reporte
		switch (tipoReporte) {
			case 1:
				columnas = "RAMO,POLIZA,TIPO DE POLIZA,CREDITO,CERTIFICADO,FECHA_INGRESO,MES_CONTABLE_FACTURADO,VENCIMIENTO_CUOTA,RECIBO_FACTURADO,SALDO_INSOLUTO/SUMA_ASEGURADA,TARIFA,PRIMA_FACTURADA,ESTATUS,FECHA_CANCELACION,FECHA_PAGO,FECHA_APLICACION_NC,MONTO_PAGADO/NOTA_CREDITO,ADEUDO";
				nomArchivo = "Estado_Cuenta";
				strQuery = GeneratorQuerys.repCobEstadoCuenta;
				break;
			case 2:
				columnas = "CANAL,RAMO,POLIZA,CREDITO,PRIMA_NETA,RECARGOS,IVA,PRIMA_TOTAL,ESTATUS,FECHA_COBRO";
				nomArchivo = "Estatus_Cobranza";
				strQuery = GeneratorQuerys.repCobEstatusCobranza;
				break;
			case 3:
				columnas = "POLIZA,CREDITO,RECIBO,ESTATUS";
				nomArchivo = "Estatus_Cobro";
				strQuery = GeneratorQuerys.repCobEstatusCobro;
				break;
			case 4:
				columnas = "RAMO,POLIZA,DESCRIPCION,RECIBO,CUOTA,DESDE,HASTA,PRIMA_NETA,RFI,DPO,IVA,PRIMA_TOTAL,F_DEPOSITO,CUENTA_BANCARIA,CUENTA_CONTABLE ";
				nomArchivo = "Flujo_Efectivo_Mensual";
				strQuery = GeneratorQuerys.repCobFlujoEfectivo;
				break;
			case 5:
				columnas = "RAMO,POLIZA,CREDITO,PRIMA_NETA,RECARGOS,IVA,PRIMA_TOTAL,ESTATUS,FECHA_COBRO/CANCELACION,%COMISION,%COMDESEMPLEO,COMISION_PAGADA_TOTAL,COMISION_VIDA,COMISION_DESEMPLEO";
				nomArchivo = "Pago_Comisiones";
				strQuery = GeneratorQuerys.repCobPagoComisiones;
				break;
			case 6:
				columnas = "BUC,POLIZA,CERTIFICADO,PORC_COMISION,RAMO,DESCRIPCION_PRODUCTO,CANAL,SUCURSAL,TIPO_OPERACION,PRIMA_NETA,DERECHO,RECARGO,PRIMA_NETA_ASISTENCIA,IVA,PRIMA_TOTAL,PRIMA_NETA_COA,DERECHO_COA,RECARGO_COA,PRIMA_NETA_COA_ASISTENCIA,IVA_COA,PRIMA_TOTAL_COA,MONTO_COMISION,CLAVE_PRODUCTO,FECHA_REGISTRO_CONTABLE,FORMA_PAGO,NOMBRE_EJECUTIVO,ID_CONVENIOS,DESCRIPCION_CONVENIO_EMPRESA,CENTROCOSTOS,RECIBO,CREDITO,CREDITONUEVO,STATUSMOV,CUENTA,PRODUCTO,SUBPRODUCTO,APELLIDOPATERNO,APELLIDO MATERNO,NOMBRE,SEXO,FECHANACIMIENTO,RFC,PLAZO,FECHAINICIOPOLIZA,FECHAFINPOLIZA,FECHAINGRESO,FECHADESDE,FECHAANULACIÓN,FECHACANCELACION,SUMAASEGURADA,MONTODEVOLUCION,BASECALCULO,TARIFA,CDESTADO,ESTADO,CP,PRODUCTOASEGURADORA,PLANASEGURADORA,CUOTA,FECHANACIMIENTOOSSEXOOSTIPODEINTERVENCIONDESCRIPCIONPARTICIPACON,FECHADEEMISION,FECHADECARGAVENTA,DIFERENCIADEVOLUCION";
				nomArchivo = "Provision_Emision";
				strQuery = GeneratorQuerys.repCobProvisionEmi;
				break;
			case 7:
				columnas = "RAMO,POLIZA,FORMA_PAGO,CENTRO_COSTO,DESCRIPCION,PRIMA_NETA_EMITIDA,PRIMA_NETA_CANCELADA,PRIMA_NETA_DEVOLUCIONES,PRIMA_NETA_ASISTENCIA,MOVIMIENTOS_ADMINISTRATIVOS,MOVIMIENTOS_ADMINISTRATIVOS_MES_ACTUAL,TOTAL_REGISTRADO,%,$COM";
				nomArchivo = "Provision_Emision_Conta";
				strQuery = GeneratorQuerys.repCobProvisionEmiCon;
				break;
			default:
				columnas = "RAMO,POLIZA,CREDITO,RECIBO,SUB_ESTATUS_RECIBO,ESTATUS_RECIBO,MONTO_PRIMA,FECHA_COBRO";
				nomArchivo = "Rehabilitados";
				strQuery = GeneratorQuerys.repCobRehabilitados;
				break;
		}
		
		//Se reemplazan varieables en query
		StringBuilder sbFechaIni = new StringBuilder();
		StringBuilder sbFechaFin = new StringBuilder();
		
		sbFechaIni.append("TO_DATE('").append(fecini).append("', 'dd/MM/yyyy')");
		sbFechaFin.append("TO_DATE('").append(fecfin).append("', 'dd/MM/yyyy')");
		
		Utilerias.replaceAll(strQuery,  Pattern.compile("#canal"), "1");
		Utilerias.replaceAll(strQuery,  Pattern.compile("#ramo"), String.valueOf(ramo));
		Utilerias.replaceAll(strQuery,  Pattern.compile("#poliza"), String.valueOf(poliza));
		Utilerias.replaceAll(strQuery,  Pattern.compile("#estatus"), String.valueOf(estatus));
		Utilerias.replaceAll(strQuery,  Pattern.compile("#fechaini"), sbFechaIni.toString());
		Utilerias.replaceAll(strQuery,  Pattern.compile("#fechafin"), sbFechaFin.toString());
		
		//Se asigna nombre a archivo
		nomArchivo = nomArchivo + GestorFechas.formatDate(new Date(), "MMyyyy") + ".csv";
		
		//Se inserta peticion
		return generarPeticionReporteConbranza(strQuery.toString(), "COMISIONES", nomArchivo, columnas, tipoReporte);
	}
	
	/**
	 * Metodo que inserta peticion para genear reportes por batch
	 * @param query consulta a ejecutar
	 * @param mensaje mensaje para correo
	 * @param nomArchivo nombre del archivo 
	 * @param strNomreArchivo nombre archivo de salida
	 * @param tipo tipo reporte
	 * @return mensaje de proceso
	 */
	private String generarPeticionReporteConbranza(String query, String mensaje, String nomArchivo, String strNomreArchivo, Integer tipo) {
		String res = Constantes.DEFAULT_STRING;
		Long next = 0L;
		
		try {	
			this.certificadoDAO.insertaBanderaBatch(next, query, mensaje, strNomreArchivo, nomArchivo, 2L, Long.valueOf(tipo));

			res = "Reporte: " + nomArchivo + ", se generará en ruta por proceso batch, se notificara por correo la ejecución del reporte.";
		} catch (Excepciones e) {
			LOG.error(Logger.SECURITY_FAILURE, e);
			res = "Error al generar peticion de reporte.";
		} 
		
		return res;
	}

	/**
	 * Metodo para generar peticion de detalle endoso
	 * @param coedCasuCdSucursal canal del endoso
	 * @param coedCarpCdRamo ramo del endoso
	 * @param coedCapoNuPoliza poliza del endoso
	 * @param coedNuRecibo recibo del endoso
	 * @return respuesta de generacion
	 * @throws Excepciones con error en general
	 */
	@Override
	public String generarDetalleEndoso(short coedCasuCdSucursal, short coedCarpCdRamo, long coedCapoNuPoliza, BigDecimal coedNuRecibo) throws Excepciones {
		StringBuilder strQuery = new StringBuilder();
		
		//Se definen nombre de columnas
		String strColumnas = "Ramo,CentroCostos,Poliza,CuotaPoliza,Recibo,Credito,Certificado,EstatusEndoso,Cuenta,Producto,SubProducto,ApellidoPaterno,ApellidoMaterno,Nombre,Sexo,FechaNacimiento,BUC,RFC,Plazo,FechaInicioPoliza,FechaFinPoliza,FechaIngreso,FechaDesde,FechaHasta,FechaInicioCredito,FechaFinCredito,SumaAsegurada,BaseCalculo,PrimaNetaEndoso,DerechosEndoso,RecargosEndoso,IVAEndoso,PrimaTotalEndoso,Tarifa,PrimaVida,PrimaDesempleo,CDEstado,Estado,CP,ProductoAseguradora,PlanAseguradora,CuotaBasica,CuotaDesempleo";
		//Se define nombre de archivo
		String strNomArchivo = "DetalleEndoso_";
		//Se obtiene query
		strQuery = GeneratorQuerys.repDetalleEndoso;
		
		//Se reemplazan valores
		Utilerias.replaceAll(strQuery,  Pattern.compile("#canal"), String.valueOf(coedCasuCdSucursal));
		Utilerias.replaceAll(strQuery,  Pattern.compile("#ramo"), String.valueOf(coedCarpCdRamo));
		Utilerias.replaceAll(strQuery,  Pattern.compile("#poliza"), String.valueOf(coedCapoNuPoliza));
		Utilerias.replaceAll(strQuery,  Pattern.compile("#recibo"), String.valueOf(coedNuRecibo));
		
		//Se asigna nombre a archivo
		strNomArchivo = strNomArchivo + String.valueOf(coedNuRecibo) + '_' + GestorFechas.formatDate(new Date(), "MMyyyy") + ".csv";
		
		//Se inserta peticion
		return generarPeticionReporteConbranza(strQuery.toString(), "COMISIONES", strNomArchivo, strColumnas, 15);
	}
	
	/**
	 * Metodo que consulta datos de timbrado
	 * @param canal para realizar consulta
	 * @param ramo ramo para realizar consulta
	 * @param poliza poliza para realizar consulta
	 * @param archivo nombre del archivo 
	 * @param tipo tipo de reporte
	 * @return lista de registros
	 * @throws Excepciones con error en general
	 */
	@Override
	public List<Object> consultaErroresCobranza(Short canal, Short ramo, Integer poliza, String archivo, Integer tipo) throws Excepciones {
		List<Object> lstDatos = new ArrayList<Object>();
		List<Object> lstResultado;
		Object[] arrDatos = new Object[1];
		
		try {
			lstResultado = new ArrayList<Object>();
			
			//Se manda ejecutar consulta
			lstDatos = cargaCobranza.erroresCargaCobranza(canal, ramo, poliza, tipo, archivo);
			
			//Regresamos resultados
			for (Iterator<Object> iterator = lstDatos.iterator(); iterator.hasNext();) {
				arrDatos[0] = iterator.next();
				lstResultado.add(arrDatos);
			}
		} catch (Excepciones e) {
			LOG.error(Logger.SECURITY_FAILURE, e);
			throw new Excepciones("Error al ejecutar consulta de errores:: " + e.getMessage()); 
		}
		return lstResultado;
	}
	
	/**
	 * Metodo que ejecuta paquete  de carga Cobranza
	 * @param canal para realizar consulta
	 * @param ramo ramo para realizar consulta
	 * @param poliza poliza para realizar consulta
	 * @param tipo tipo de reporte
	 * @param archivo nombre del archivo 
	 * @return cantidad de errores
	 * @throws Excepciones con error en general
	 */
	@Override
	public Integer cargaCobranza(Short canal, Short ramo, Integer poliza, Integer tipo, String archivo) throws Excepciones {
		Integer errores = 0;
		
		try {
			//ejecuta proceso de cobranza
			errores = cargaCobranza.cargaCobranza(canal, ramo, poliza, tipo, archivo);
		} catch (Excepciones e) {
			LOG.error(Logger.SECURITY_FAILURE, e);
			throw new Excepciones("Error al ejecutar procceso de cobranza:: " + e.getMessage()); 
		}
		
		//Se regresa cantidad de errores
		return errores;
	}

	/**
	 * Metodo que aplica cobranza al registro seleccionado
	 * @param coceCasuCdSucursal canal del recibo
	 * @param coceCarpCdRamo ramo del recibo
	 * @param coceCapoNuPoliza poliza del recibo
	 * @param coceNuCertificado certificado del recibo
	 * @param coceNuCredito numero de credito
	 * @param coceNoRecibo numero de recibo
	 * @throws Excepciones con error en general
	 */
	@Override
	public void aplicaCobranza(Short coceCasuCdSucursal, Short coceCarpCdRamo, Long coceCapoNuPoliza, Long coceNuCertificado, String coceNuCredito, BigDecimal coceNoRecibo) throws Excepciones {
		try {	
			this.cargaCobranza.actualizaReciboCobranza(coceCasuCdSucursal, coceCarpCdRamo, coceCapoNuPoliza, coceNuCertificado, coceNuCredito, coceNoRecibo);
		} catch (Excepciones e) {
			LOG.error(Logger.SECURITY_FAILURE, e);
		} 
	}
}
