/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.dao.CertificadoDao;
import mx.com.santander.aseguradora.colectivos.model.dao.PolizaDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPoliza;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Sergio Plata
 *
 */
@Service
public class ServicioPolizaImpl implements ServicioPoliza {

	@Resource
	private PolizaDao polizaDao;
	@Resource
	private CertificadoDao certificadoDao;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;	
	
	/**
	 * @param polizaDao the polizaDao to set
	 */
	public void setPolizaDao(PolizaDao polizaDao) {
		this.polizaDao = polizaDao;
	}

	/**
	 * @return the polizaDao
	 */
	public PolizaDao getPolizaDao() {
		return polizaDao;
	}

	public ServicioPolizaImpl() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
	}

	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		
	}

	public <T> T guardarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub
		
	}
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)
			throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerPoliza.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			T poliza = this.polizaDao.obtenerObjeto(objeto, id);
			
			return poliza;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede recuperar la poliza:").append(id);
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
	}

	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		this.message.append("obtenerPolizas.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			
			List<T> lista = this.polizaDao.obtenerObjetos(filtro);
			
			return lista;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar las polizas.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public <T> List<T> consultaPolizasRenovacion() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioPolizaImpl.consultaPolizasRenovacion");
			return this.polizaDao.consultaPolizasRenovacion();
			
		}catch (Exception e){
			this.message.append("Error al seleccionar datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
	
		}
	}
	
	public String procesoRenovacionPolizas(Short ramo, long poliza) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			String resp="";
			System.out.println("Entre a ServicioPolizaImpl.procesoRenovacionPolizas");
			System.out.println(ramo);
			System.out.println(poliza);
			resp = this.certificadoDao.procesoRenovacionPolizas(ramo, poliza);
			System.out.println("Salgo de ServicioPolizaImpl.procesoRenovacionPolizas");
			return resp;
		}catch (Exception e){
			this.message.append("Error al seleccionar datos. ServicioPolizaImpl.procesoRenovacionPolizas");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
	
		}
	}
	
	public <T> List<T> consultaPolizasRenovadas() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioPolizaImpl.consultaPolizasRenovacion");
			return this.certificadoDao.consultaPolizasRenovadas();
			
		}catch (Exception e){
			this.message.append("Error al seleccionar datos en ServicioPolizaImpl.consultaPolizasRenovadas");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
	
		}
	}

	public <T> List<T> detallePolizasRenovadas() throws Excepciones {
		// TODO Auto-generated method stub
		try {
			System.out.println("ServicioPolizaImpl.detallePolizasRenovacion");
			return this.certificadoDao.detallePolizasRenovadas();
			
		}catch (Exception e){
			this.message.append("Error al seleccionar datos en ServicioPolizaImpl.detallePolizasRenovadas");
			this.log.error(message.toString(), e);
			throw new Excepciones (message.toString(), e);
	
		}
	}

}
