package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosAnexo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosSeguimiento;

import mx.com.santander.aseguradora.colectivos.model.dao.AnexoDAO;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAnexo;
import mx.com.santander.aseguradora.colectivos.view.dto.AnexoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SeguimientoDTO;

@Service
public class ServicioAnexoImpl implements ServicioAnexo {

	@Resource
	private AnexoDAO anexoDAO;

	/**
	 * convierte seguimiento DTO a seguimiento persistencia
	 * 
	 * @param obj
	 *            DTO
	 * @return seguimiento Persistencia
	 */
	private ColectivosAnexo anexoDTOToAnexo(AnexoDTO obj) {
		ColectivosAnexo res = new ColectivosAnexo();

		res.setCoanId(obj.getCoanId());
		res.setCoseConsecutivo(obj.getCoseConsecutivo());
		res.setCoseNomDocto(obj.getCoseNomDocto());

		ColectivosSeguimiento colectivosSeguimiento = new ColectivosSeguimiento();

		colectivosSeguimiento.setCoseId(obj.getCoanCoseID());

		res.setColectivosSeguimiento(colectivosSeguimiento);

		return res;
	}

	/**
	 * convierte seguimiento Persistencia a seguimiento dto
	 * 
	 * @param obj
	 *            persistencia
	 * @return seguimiento DTO
	 */
	private AnexoDTO anexooToAnexoDTO(ColectivosAnexo obj) {
		AnexoDTO res = new AnexoDTO();

		res.setCoanId(obj.getCoanId());
		res.setCoseConsecutivo(obj.getCoseConsecutivo());
		res.setCoseNomDocto(obj.getCoseNomDocto());
		res.setCoanCoseID(obj.getColectivosSeguimiento().getCoseId());

		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		AnexoDTO anexoDTO = (AnexoDTO) objeto;
		ColectivosAnexo anexo = anexoDTOToAnexo(anexoDTO);

		long folio = anexoDAO.consecutivoAnexo(anexo);

		anexo.setCoseConsecutivo(new BigDecimal(folio));

		anexo = anexoDAO.guardarObjeto(anexo);

		return (T) anexooToAnexoDTO(anexo);
	}

	@Override
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {

		return null;
	}

	@Override
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> void actualizarObjeto(T objeto) throws Excepciones {

		anexoDAO.actualizarObjeto(objeto);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.santander.aseguradora.colectivos.model.service.ServicioSeguimiento#
	 * actualizarSeguimiento(mx.com.santander.aseguradora.colectivos.view.dto.
	 * AnexoDTO)
	 */
	@Override
	public AnexoDTO actualizarAnexo(AnexoDTO filtro) throws Excepciones {
		anexoDAO.actualizarAnexo(anexoDTOToAnexo(filtro));
		return filtro;
	}

	@Override
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public AnexoDTO consultaObjeto(AnexoDTO filtro) throws Excepciones {
		ColectivosAnexo colectivosSeguimiento = anexoDTOToAnexo(filtro);

		return anexooToAnexoDTO(anexoDAO.obtieneObjeto(colectivosSeguimiento));
	}

	@Override
	public List<AnexoDTO> consultaAnexoSuscripcion(SeguimientoDTO filtro) throws Excepciones {
		List<AnexoDTO> res = new ArrayList<AnexoDTO>();
		ColectivosAnexo colectivosAnexo = new ColectivosAnexo();
		ColectivosSeguimiento colectivosSeguimiento = new ColectivosSeguimiento();
		
		colectivosSeguimiento.setCoseId(filtro.getCoseId());
		colectivosAnexo.setColectivosSeguimiento(colectivosSeguimiento);

		List<ColectivosAnexo> seguimientos = anexoDAO.consultaAnexoSuscripcion(colectivosAnexo);

		for (ColectivosAnexo seguimiento : seguimientos) {
			res.add(anexooToAnexoDTO(seguimiento));
		}

		return res;
	}

}
