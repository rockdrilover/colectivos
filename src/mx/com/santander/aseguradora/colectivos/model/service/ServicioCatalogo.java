/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

/**
 * @author Sergio Plata
 *
 */
public interface ServicioCatalogo extends InitializingBean, DisposableBean{

	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @return
	 * @throws Excepciones
	 */
	public <T> T guardarObjeto(T objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param lista
	 * @throws Excepciones
	 */
	public <T> void guardarObjetos(List<T> lista)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @param id
	 * @return
	 * @throws Excepciones
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @throws Excepciones
	 */
	public <T> void borrarObjeto(T objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @throws Excepciones
	 */
	public <T> void actualizarObjeto(T objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @return
	 * @throws Excepciones
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto)throws Excepciones;
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @param filtro
	 * @return
	 * @throws Excepciones
	 */
	public <T> List<T> obtenerObjetos(String filtro)throws Excepciones;
}
