/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Carga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CargaId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosBeneficiarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCertificados;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Endosos;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatos;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.dao.EndososDatosDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ParametrosDao;
import mx.com.santander.aseguradora.colectivos.model.dao.ProcesosDao;
import mx.com.santander.aseguradora.colectivos.model.database.EndosoDatosDao;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.BeneficiariosDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.CargaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ClienteCertifDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.TipoEndosoDTO;

/**
 * Clase de servicio para modulo de Endoso Datos
 * @author Ing. Issac Bautista
 *
 */
@Service
public class ServicioEndosoDatosImpl implements ServicioEndosoDatos {

	/**
	 * Propiedad para acceso a DAO Endoso Datos 
	 */
	@Resource
	private EndosoDatosDao endosoDatosDao;
	/**
	 * Propiedad para acceso a DAO Endoso Datos
	 */
	@Resource
	private EndososDatosDao endososDatosDao;
	/**
	 * Propiedad para acceso a DAO Procesos
	 */
	@Resource
	private ProcesosDao procesosDao;
	/**
	 * Propiedad para acceso a DAO Parametros
	 */
	@Resource
	private ParametrosDao parametrosDao;
	
	/**
	 * Log
	 */
	private Log log = LogFactory.getLog(this.getClass());
	/**
	 * Mensage para regresar resultados
	 */
	private StringBuilder message;
	
	
	/**
	 * @return
	 */
	public EndosoDatosDao getEndosoDatosDao() {
		return endosoDatosDao;
	}
	/**
	 * @param endosoDatosDao
	 */
	public void setEndosoDatosDao(EndosoDatosDao endosoDatosDao) {
		this.endosoDatosDao = endosoDatosDao;
	}
	/**
	 * @return
	 */
	public EndososDatosDao getEndososDatosDao() {
		return endososDatosDao;
	}
	/**
	 * @param endososDatosDao
	 */
	public void setEndososDatosDao(EndososDatosDao endososDatosDao) {
		this.endososDatosDao = endososDatosDao;
	}
	/**
	 * @return
	 */
	public ProcesosDao getProcesosDao() {
		return procesosDao;
	}
	/**
	 * @param procesosDao
	 */
	public void setProcesosDao(ProcesosDao procesosDao) {
		this.procesosDao = procesosDao;
	}
	/**
	 * @return
	 */
	public ParametrosDao getParametrosDao() {
		return parametrosDao;
	}
	/**
	 * @param parametrosDao
	 */
	public void setParametrosDao(ParametrosDao parametrosDao) {
		this.parametrosDao = parametrosDao;
	}
	 
	 /**
	  * Constructor de Clase
	  */
	 public ServicioEndosoDatosImpl() {
		 this.message = new StringBuilder();
	 }

	/**
	 * Metodo que manda actualizar ubn objeto
	 */
	public <T> void actualizarObjeto(T objeto) throws Excepciones {
		ColectivosBeneficiarios objBeneficiario;		
		EndososDatos objEndosoDatos;
		Certificado objCertificado;
		
		try {
			if(objeto instanceof BeneficiariosDTO) {
				objBeneficiario = (ColectivosBeneficiarios) ConstruirObjeto.crearBean(ColectivosBeneficiarios.class, objeto); 
				this.endosoDatosDao.actualizarObjeto(objBeneficiario);
			} else if(objeto instanceof EndosoDatosDTO) {
				objEndosoDatos = (EndososDatos) ConstruirObjeto.crearBean(EndososDatos.class, objeto);			
				this.endosoDatosDao.actualizarObjeto(objEndosoDatos);
			} else if(objeto instanceof Certificado) {
				objCertificado = (Certificado) objeto;
				objCertificado.setCoceNuMovimiento(this.endososDatosDao.generaMovimiento(objCertificado.getId().getCoceCasuCdSucursal(), objCertificado.getId().getCoceCarpCdRamo(),
														objCertificado.getId().getCoceCapoNuPoliza(), objCertificado.getId().getCoceNuCertificado()));
				this.endosoDatosDao.actualizarObjeto(objeto);
			} else {
				this.endosoDatosDao.actualizarObjeto(objeto);
			}
		} catch (Exception e) {
			this.message.append("Error al actualizar el informacion.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}		

	}

	/**
	 * Metodo que manda borrar un objeto
	 */
	public <T> void borrarObjeto(T objeto) throws Excepciones {
		ColectivosBeneficiarios objBeneficiarios;
		try {
			objBeneficiarios = (ColectivosBeneficiarios) ConstruirObjeto.crearBean(ColectivosBeneficiarios.class, objeto);
			this.endosoDatosDao.borrarObjeto(objBeneficiarios);
		} catch (Exception e) {
			message.append("no se puede borrar la informacion.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}

	/**
	 * Metodo que manda a guardar un objeto
	 */
	public <T> T guardarObjeto(T objeto) throws Excepciones {
		ColectivosBeneficiarios objBeneficiario;
		Endosos objEndoso;
		EndososDatos objEndosoDatos;
		
		try {
			if(objeto instanceof BeneficiariosDTO) {
				objBeneficiario = (ColectivosBeneficiarios) ConstruirObjeto.crearBean(ColectivosBeneficiarios.class, objeto);			
				this.endosoDatosDao.guardarObjeto(objBeneficiario);
			} else if(objeto instanceof EndosoDatosDTO) {
				objEndosoDatos = (EndososDatos) ConstruirObjeto.crearBean(EndososDatos.class, objeto);			
				this.endosoDatosDao.guardarObjeto(objEndosoDatos);
			} else {
				objEndoso = (Endosos) ConstruirObjeto.crearBean(Endosos.class, objeto);
				this.endosoDatosDao.guardarObjeto(objEndoso);
			}
		} catch (Exception e) {
			message.append("no se puede guardar el registro.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
		return objeto;
	}

	/**
	 * Metodo que manda guarda una lista de objetos 
	 */
	public <T> void guardarObjetos(List<T> lista) throws Excepciones {
	}

	/**
	 * Metodo que regresa una lista de objetos
	 */
	public <T> T obtenerObjeto(Class<T> objeto, Serializable id) throws Excepciones {
		return null;
	}

	/**
	 * Metodo que regresa una lista de objetos 
	 */
	public <T> List<T> obtenerObjetos(Class<T> objeto) throws Excepciones {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioCatalogo#obtenerObjetos(java.lang.String)
	 */
	public <T> List<T> obtenerObjetos(String filtro) throws Excepciones {
		this.message.append("obtenerClientesCertif.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder consulta = new StringBuilder();
		
		try {
			consulta.append("from ClienteCertif  CC    \n");
			consulta.append("where 1 = 1           \n");
			consulta.append(filtro); 
			return this.endosoDatosDao.obtenerObjetos(consulta.toString());
		} catch (Excepciones e) {
			this.message.append("No se pueden recuperar los clientes certififados.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	/**
	 * Metodo que se ejecuta antes de inicializar propiedasdes
	 */
	public void afterPropertiesSet() {
	}

	/**
	 * Metodo que se ejecuta cuando se destruye la clase
	 */
	public void destroy() {
	}
	
	/**
	 * Metodo que manda a validar la fecha de nacimiento del cliente 
	 * @param cliente datos del cliente 
	 * @param isPrimaUnica bandera que nos indica si el certificado es prima unica 
	 * @return 
	 * @throws Excepciones
	 */
	public Integer validaFecha(ClienteCertifDTO objDTO, String isPrimaUnica) throws Excepciones{
		StringBuilder filtro = new StringBuilder();
		ClienteCertif cliente;
		Long poliza;
		Parametros param;
		Integer existe;
		
		try {
			cliente = (ClienteCertif) ConstruirObjeto.crearObjeto(ClienteCertif.class, objDTO);
			
			poliza = cliente.getId().getCoccCapoNuPoliza();			
			if(!isPrimaUnica.equals(Constantes.DEFAULT_STRING_ID)){
				System.out.println("------> poliza "+ cliente.getId().getCoccCapoNuPoliza()+""+"--" );
				poliza = new Long((poliza + "").substring(0, 5));				
			}
			
			filtro.append(" AND P.copaNvalor1 = ").append(cliente.getId().getCoccCasuCdSucursal());
			filtro.append(" AND P.copaNvalor2 = ").append(cliente.getId().getCoccCarpCdRamo());
			filtro.append(" AND P.copaNvalor3 = ").append(poliza);
			filtro.append(" AND P.copaDesParametro = 'POLIZA' ");
			filtro.append(" AND P.copaIdParametro  = 'CALPMA'");
			
			param = (Parametros) this.parametrosDao.obtenerObjetos(filtro.toString()).get(0);
			existe = getProcesosDao().validaFechaNac(cliente, poliza, new Integer(param.getCopaVvalor4()), new Integer(param.getCopaVvalor5()));
			
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			this.message.append("No se puede validar la Fecha de Nacimiento.");
			throw new Excepciones(e.getMessage());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#getBeneficiarios(mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId)
	 */
	@Override
	public List<BeneficiariosDTO> getBeneficiarios(CertificadoId id) throws Excepciones {
		ArrayList<BeneficiariosDTO> arlResultado;
		List<ColectivosCertificados> lstDatos;
		Iterator<ColectivosCertificados> it;
		BeneficiariosDTO objBeneficiario;
		StringBuilder filtro;
		
		try {
			arlResultado = new ArrayList<BeneficiariosDTO>();
			filtro = new StringBuilder();
			
			filtro.append("from ColectivosBeneficiarios  CB    \n");
			filtro.append("where 1 = 1           \n");
			filtro.append(" AND CB.id.cobeCaceCasuCdSucursal = ").append(id.getCoceCasuCdSucursal());
			filtro.append(" AND CB.id.cobeCaceCarpCdRamo = ").append(id.getCoceCarpCdRamo());
			filtro.append(" AND CB.id.cobeCaceCapoNuPoliza = ").append(id.getCoceCapoNuPoliza());
			filtro.append(" AND CB.id.cobeCaceNuCertificado = ").append(id.getCoceNuCertificado());
			
			lstDatos = this.endosoDatosDao.obtenerObjetos(filtro.toString());
			it = lstDatos.iterator();
			while (it.hasNext()) {
				objBeneficiario = (BeneficiariosDTO) ConstruirObjeto.crearObjeto(BeneficiariosDTO.class, it.next());
				objBeneficiario.setObjEndoso(creaEndoso(objBeneficiario, Constantes.DEFAULT_SIN_DATO));
				objBeneficiario.setCobeVcampo2(getDescParentesco(objBeneficiario.getCobeRelacionBenef()));
				objBeneficiario.setCobeVcampo4(Constantes.DEFAULT_SIN_DATO);
				arlResultado.add(objBeneficiario);
			}
			
			return arlResultado;
		} catch (Exception e) {
			this.message.append("Error al obtener beneficiarios.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#getDescParentesco(java.lang.String)
	 */
	@Override
	public  String getDescParentesco(String cobeCdParentAseg) throws Excepciones {
		String strResultado = null;
		StringBuilder sbQuery;
	    
		try {
			strResultado = Constantes.DEFAULT_STRING;
			sbQuery = new StringBuilder();
			
			sbQuery.append(" select CC.RV_MEANING ");
			sbQuery.append(" from cg_ref_codes cc ");
			sbQuery.append(" where cc.rv_domain = 'PARENTESCO' ");
			sbQuery.append(" and CC.RV_LOW_VALUE = '").append(cobeCdParentAseg).append("' ");
			
			strResultado = endosoDatosDao.consultar(sbQuery.toString()).get(0).toString();
		} catch (Excepciones e) {
			this.message.append("Error al obtener descripcion de parentesco.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
		
		return strResultado;
	}

	private EndosoDatosDTO creaEndoso(BeneficiariosDTO objBeneficiario, String opcion) {		
		EndosoDatosDTO objEndoso = null;
		
		objEndoso = new EndosoDatosDTO();
		objEndoso.getId().setCedaCasuCdSucursal(objBeneficiario.getId().getCobeCaceCasuCdSucursal());
		objEndoso.getId().setCedaCarpCdRamo(Short.valueOf(objBeneficiario.getId().getCobeCaceCarpCdRamo()));
		objEndoso.getId().setCedaCapoNuPoliza(objBeneficiario.getId().getCobeCaceCapoNuPoliza());
		objEndoso.getId().setCedaNuCertificado(objBeneficiario.getId().getCobeCaceNuCertificado());
		objEndoso.getId().setCedaNuEndosoGral(new Long(GestorFechas.formatDate(new Date(), "yyyyMM")));
		objEndoso.getId().setCedaNuEndosoDetalle(0);
		objEndoso.getId().setCedaCdEndoso(Constantes.ENDOSO_CD_DATOS);
		objEndoso.setCedaStEndoso(Constantes.ENDOSO_ESTATUS_APLICADO);
		objEndoso.setCedaNuRecibo(new BigDecimal(Constantes.DEFAULT_INT));
		objEndoso.setCedaFeSolicitud(new Date());
		objEndoso.setCedaFeRecepcion(new Date());			
		objEndoso.setCedaCdUsuarioAplica(FacesUtils.getBeanSesion().getUserName());
		objEndoso.setCedaCdUsuarioReg(FacesUtils.getBeanSesion().getUserName());
		objEndoso.setCedaCorreoAplica(FacesUtils.getBeanSesion().getObjUsuario().getCorreo());
		objEndoso.setCedaCorreoReg(FacesUtils.getBeanSesion().getObjUsuario().getCorreo());
		objEndoso.setCedaCampon1(Constantes.ENDOSO_CD_DATOS);
		objEndoso.setCedaCampov2(Constantes.ENDOSO_ST_DATOS);		
		
		if(!opcion.equals(Constantes.BENEFICIARIO_NUEVO)) {
			objEndoso.setCedaNombreAnt(objBeneficiario.getCobeNombre());
			objEndoso.setCedaApAnt(objBeneficiario.getCobeApellidoPat());
			objEndoso.setCedaAmAnt(objBeneficiario.getCobeApellidoMat());
			objEndoso.setCedaPjParticipaAnt(objBeneficiario.getCobePoParticipacion().shortValue());
			objEndoso.setCedaParentescoAnt(objBeneficiario.getCobeRelacionBenef());
			objEndoso.setCedaFechaAnt(objBeneficiario.getCobeFeDesde());
		}
		
		return objEndoso;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#generaEndoso(mx.com.santander.aseguradora.colectivos.view.dto.BeneficiariosDTO, mx.com.santander.aseguradora.colectivos.view.dto.TipoEndosoDTO)
	 */
	@Override
	public String generaEndoso(BeneficiariosDTO objBeneficiario, TipoEndosoDTO objTipoEndoso) throws Excepciones {
		String strResultado;
		EndosoDatosDTO objEndoso;
		
		try {
			strResultado = Constantes.DEFAULT_STRING;
			objEndoso = objBeneficiario.getObjEndoso();
			if(objBeneficiario.getCobeVcampo4().equals(Constantes.BENEFICIARIO_NUEVO)) {
				objEndoso = creaEndoso(objBeneficiario, objBeneficiario.getCobeVcampo4());
			} 

			objEndoso.getId().setCedaNuEndosoDetalle(getProcesosDao().getNumeroEndoso(objEndoso.getId().getCedaCarpCdRamo(), objEndoso.getCedaCampov2()));			
			objEndoso.setCedaFeAplica(new Date());
			objEndoso.setCedaCampov1(objEndoso.getCedaCampov2() + Utilerias.agregarCaracteres(Short.toString(objEndoso.getId().getCedaCarpCdRamo()), 3, '0', 1) + "-" + objEndoso.getId().getCedaNuEndosoGral() + objEndoso.getId().getCedaNuEndosoDetalle());
			objEndoso.setCedaCampov3(objBeneficiario.getCobeVcampo4());		
			objEndoso.setCedatpEndoso(Short.parseShort(Constantes.TIPO_ENDOSO_BENEFICIARIOS));
			objEndoso.setCedaCampon2(new BigDecimal(objBeneficiario.getId().getCobeNuBeneficiario()));

			if(!objBeneficiario.getCobeVcampo4().equals(Constantes.BENEFICIARIO_ELIMINA)) {
				objEndoso.setCedaNombreNvo(objBeneficiario.getCobeNombre());
				objEndoso.setCedaApNvo(objBeneficiario.getCobeApellidoPat());
				objEndoso.setCedaAmNvo(objBeneficiario.getCobeApellidoMat());
				objEndoso.setCedaPjParticipaNvo(objBeneficiario.getCobePoParticipacion().shortValue());
				objEndoso.setCedaParentescoNvo(objBeneficiario.getCobeRelacionBenef());
			}
			
			if(objTipoEndoso != null) {
				objEndoso.setCedaStEndoso(Constantes.ENDOSO_ESTATUS_PENDIENTE);
			} else {
				actualizaDatosRector(objEndoso);
			}
			
			this.guardarObjeto(objEndoso);
			strResultado = objEndoso.getCedaCampov1();
		} catch (Excepciones e) {			
			this.message.append("Error al generar Endoso.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
		
		return strResultado;
	}
	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#getClienteAnterior(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId)
	 */
	@Override
	public ClienteCertif getClienteAnterior(ClienteCertifId cliente) throws Excepciones {
		try {
			return this.endosoDatosDao.obtenerObjeto(ClienteCertif.class, cliente);
		} catch (Excepciones e) {			
			this.message.append("Error al getClienteAnterior.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
	}

	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#generaEndoso(mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif, mx.com.santander.aseguradora.colectivos.view.dto.ClienteCertifDTO, java.lang.String, short)
	 */
	@Override	
	public String generaEndoso(ClienteCertif objEndosoAnt, ClienteCertifDTO objEndosoNvo, String tipoEndoso, short stEndoso) throws Excepciones {
		EndosoDatosDTO objEndoso = null;
		StringBuilder sbDatos;
		String strLinea;
		
		try {
			objEndoso = new EndosoDatosDTO();
			objEndoso.setCedaCampon1(Constantes.ENDOSO_CD_DATOS);
			objEndoso.setCedaCampov2(Constantes.ENDOSO_ST_DATOS);
			objEndoso.getId().setCedaCasuCdSucursal(objEndosoNvo.getId().getCoccCasuCdSucursal());
			objEndoso.getId().setCedaCarpCdRamo(objEndosoNvo.getId().getCoccCarpCdRamo());
			objEndoso.getId().setCedaCapoNuPoliza(objEndosoNvo.getId().getCoccCapoNuPoliza());
			objEndoso.getId().setCedaNuCertificado(objEndosoNvo.getId().getCoccNuCertificado());
			objEndoso.getId().setCedaNuEndosoGral(new Long(GestorFechas.formatDate(new Date(), "yyyyMM")));
			objEndoso.getId().setCedaNuEndosoDetalle(getProcesosDao().getNumeroEndoso(objEndoso.getId().getCedaCarpCdRamo(), objEndoso.getCedaCampov2()));
			objEndoso.getId().setCedaCdEndoso(Constantes.ENDOSO_CD_DATOS);
			objEndoso.setCedaStEndoso(stEndoso);
			objEndoso.setCedatpEndoso(Short.valueOf(tipoEndoso));
			objEndoso.setCedaNuRecibo(new BigDecimal(objEndosoNvo.getId().getCoccNuCliente()));
			objEndoso.setCedaFeSolicitud(new Date());
			objEndoso.setCedaFeRecepcion(new Date());
			objEndoso.setCedaFeAplica(new Date());
			objEndoso.setCedaCdUsuarioAplica(FacesUtils.getBeanSesion().getUserName());
			objEndoso.setCedaCorreoAplica(FacesUtils.getBeanSesion().getObjUsuario().getCorreo());
			objEndoso.setCedaCdUsuarioReg(FacesUtils.getBeanSesion().getUserName());
			objEndoso.setCedaCorreoReg(FacesUtils.getBeanSesion().getObjUsuario().getCorreo());
			objEndoso.setCedaCampov1(objEndoso.getCedaCampov2() + Utilerias.agregarCaracteres(Short.toString(objEndoso.getId().getCedaCarpCdRamo()), 3, '0', 1) + "-" + objEndoso.getId().getCedaNuEndosoGral() + objEndoso.getId().getCedaNuEndosoDetalle());

			objEndoso.setCedaNombreAnt(objEndosoAnt.getCliente().getCocnNombre());
			objEndoso.setCedaApAnt(objEndosoAnt.getCliente().getCocnApellidoPat());
			objEndoso.setCedaAmAnt(objEndosoAnt.getCliente().getCocnApellidoMat());
			sbDatos = new StringBuilder();
			sbDatos.append(objEndosoAnt.getCliente().getCocnCalleNum()).append(" - ").append(objEndosoAnt.getCliente().getCocnColonia()).append(" - ");
			sbDatos.append(objEndosoAnt.getCliente().getCocnCdPostal()).append(" - ").append(objEndosoAnt.getCliente().getCocnDelegmunic()).append(" - ");
			sbDatos.append(objEndosoAnt.getCliente().getCocnCdEstado()).append(" - ").append(objEndosoAnt.getCliente().getCocnCdCiudad());			
			objEndoso.setCedaDireccionAnt(sbDatos.toString());
			objEndoso.setCedaFechaAnt(objEndosoAnt.getCliente().getCocnFeNacimiento());
			objEndoso.setCedaRfcAnt(objEndosoAnt.getCliente().getCocnRfc());					
			objEndoso.setCedasexoAnt(objEndosoAnt.getCliente().getCocnCdSexo());
			objEndoso.setCedaCorreoAnt(objEndosoAnt.getCliente().getCocnCorreoElect());
			objEndoso.setCedaFeDesAnt(objEndosoAnt.getCertificado().getCoceFeDesde());
			objEndoso.setCedaFeHasAnt(objEndosoAnt.getCertificado().getCoceFeHasta());
			objEndoso.setCedaFeSusAnt(objEndosoAnt.getCertificado().getCoceFeSuscripcion());
			objEndoso.setCedaSumaAnt(objEndosoAnt.getCertificado().getCoceMtSumaAsegurada());
			sbDatos = new StringBuilder();
			sbDatos.append(objEndosoAnt.getCertificado().getCoceMtPrimaAnual()).append(" - ").append(objEndosoAnt.getCertificado().getCoceMtPrimaReal()).append(" - ").append(objEndosoAnt.getCertificado().getCoceMtPrimaAnualReal()).append(" - ");
			sbDatos.append(objEndosoAnt.getCertificado().getCoceMtPrimaPura()).append(" - ").append(objEndosoAnt.getCertificado().getCoceMtPrimaPuraReal()).append(" - ").append(objEndosoAnt.getCertificado().getCoceMtPrimaSubsecuente()).append(" - ");
			sbDatos.append(objEndosoAnt.getCertificado().getCoceMtPmaUdis()).append(" - ").append(objEndosoAnt.getCertificado().getCoceDiCobro1()).append(" - ").append(objEndosoAnt.getCertificado().getCoceComisiones()).append(" - ");
			strLinea = Constantes.DEFAULT_STRING;
			if(objEndosoAnt.getCertificado().getCoceFeIniCredito() != null) {
				strLinea = GestorFechas.formatDate(objEndosoAnt.getCertificado().getCoceFeIniCredito(), Constantes.FORMATO_FECHA_UNO);
			}
			sbDatos.append(strLinea).append(" - ");
			strLinea = Constantes.DEFAULT_STRING;
			if(objEndosoAnt.getCertificado().getCoceFeFinCredito() != null) {
				strLinea = GestorFechas.formatDate(objEndosoAnt.getCertificado().getCoceFeFinCredito(), Constantes.FORMATO_FECHA_UNO);
			}
			sbDatos.append(strLinea);
			objEndoso.setCedaCampov4(sbDatos.toString());
			
			objEndoso.setCedaNombreNvo(objEndosoNvo.getCliente().getCocnNombre());
			objEndoso.setCedaApNvo(objEndosoNvo.getCliente().getCocnApellidoPat());
			objEndoso.setCedaAmNvo(objEndosoNvo.getCliente().getCocnApellidoMat());
			sbDatos = new StringBuilder();
			sbDatos.append(objEndosoNvo.getCliente().getCocnCalleNum()).append(" - ").append(objEndosoNvo.getCliente().getCocnColonia()).append(" - ");			
			sbDatos.append(objEndosoNvo.getCliente().getCocnCdPostal()).append(" - ").append(objEndosoNvo.getCliente().getCocnDelegmunic()).append(" - ");
			sbDatos.append(objEndosoNvo.getCliente().getCocnCdEstado()).append(" - ").append(objEndosoNvo.getCliente().getCocnCdCiudad());			
			objEndoso.setCedaDireccionNvo(sbDatos.toString());
			objEndoso.setCedaFechaNvo(objEndosoNvo.getCliente().getCocnFeNacimiento());
			objEndoso.setCedaRfcNvo(objEndosoNvo.getCliente().getCocnRfc());
			objEndoso.setCedasexoNvo(objEndosoNvo.getCliente().getCocnCdSexo());
			objEndoso.setCedaCorreoNvo(objEndosoNvo.getCliente().getCocnCorreoElect());
			objEndoso.setCedaFeDesNvo(objEndosoNvo.getCertificado().getCoceFeDesde());
			objEndoso.setCedaFeHasNvo(objEndosoNvo.getCertificado().getCoceFeHasta());
			objEndoso.setCedaFeSusNvo(objEndosoNvo.getCertificado().getCoceFeSuscripcion());
			objEndoso.setCedaSumaNvo(objEndosoNvo.getCertificado().getCoceMtSumaAsegurada());
			sbDatos = new StringBuilder();
			sbDatos.append(objEndosoNvo.getCertificado().getCoceMtPrimaAnual()).append(" - ").append(objEndosoNvo.getCertificado().getCoceMtPrimaReal()).append(" - ").append(objEndosoNvo.getCertificado().getCoceMtPrimaAnualReal()).append(" - ");
			sbDatos.append(objEndosoNvo.getCertificado().getCoceMtPrimaPura()).append(" - ").append(objEndosoNvo.getCertificado().getCoceMtPrimaPuraReal()).append(" - ").append(objEndosoNvo.getCertificado().getCoceMtPrimaSubsecuente()).append(" - ");
			sbDatos.append(objEndosoNvo.getCertificado().getCoceMtPmaUdis()).append(" - ").append(objEndosoNvo.getCertificado().getCoceDiCobro1()).append(" - ").append(objEndosoNvo.getCertificado().getCoceComisiones()).append(" - ");
			strLinea = Constantes.DEFAULT_STRING;
			if(objEndosoNvo.getCertificado().getCoceFeIniCredito() != null) {
				strLinea = GestorFechas.formatDate(objEndosoNvo.getCertificado().getCoceFeIniCredito(), Constantes.FORMATO_FECHA_UNO);				
			}
			sbDatos.append(strLinea).append(" - ");
			strLinea = Constantes.DEFAULT_STRING;
			if(objEndosoNvo.getCertificado().getCoceFeFinCredito() != null) {
				strLinea = GestorFechas.formatDate(objEndosoNvo.getCertificado().getCoceFeFinCredito(), Constantes.FORMATO_FECHA_UNO);
			}
			sbDatos.append(strLinea);
			objEndoso.setCedaCampov5(sbDatos.toString());
			
			
			if(objEndoso.getCedaStEndoso() == Constantes.ENDOSO_ESTATUS_APLICADO) {
				actualizaDatosRector(objEndoso);
			}
			this.guardarObjeto(objEndoso);
			return objEndoso.getCedaCampov1();			
		} catch (Excepciones e) {			
			this.message.append("Error al generar Endoso.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#getTipoEndosos()
	 */
	@Override
	public Collection<Object> getTipoEndosos() throws Excepciones {
		ArrayList<Object> arlResultados;
		StringBuilder sbQuery;
	    
		try {
			arlResultados = new ArrayList<>();
			sbQuery = new StringBuilder();
			
			sbQuery.append(" SELECT DISTINCT CATE_CD_TIPO_ENDOSO, CATE_DE_TIPO_ENDOSO ");
				sbQuery.append(" FROM COLECTIVOS_AUT_TP_ENDOSO ");
			sbQuery.append(" WHERE CATE_ST_TIPO_ENDOSO = 1 ");
			sbQuery.append(" ORDER BY CATE_CD_TIPO_ENDOSO ");
			
			arlResultados.addAll(endosoDatosDao.consultar(sbQuery.toString()));
		} catch (Excepciones e) {
			this.message.append("Error al consultar tipo Endosos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage()); 
		}
		
		return arlResultados;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#getAutorizaTpEndoso(java.lang.String)
	 */
	@Override
	public Collection<TipoEndosoDTO> getAutorizaTpEndoso(String tipoEndoso) throws Excepciones {
		ArrayList<Object> arlResultados;
		ArrayList<TipoEndosoDTO> arlAutoriza = null;
		StringBuilder sbQuery;
	    TipoEndosoDTO objTipoEndoso = null;
	    Iterator<Object> it;
	    StringBuilder sbCorreos;
	    
		try {
			arlAutoriza = new ArrayList<>();
			arlResultados = new ArrayList<>();			
			sbQuery = new StringBuilder();
			sbCorreos = new StringBuilder();
			
			sbQuery.append(" SELECT CATE_CD_TIPO_ENDOSO, CATE_DE_TIPO_ENDOSO, CATE_CD_USU_AUT, CATE_MAIL_USU_AUT ");
				sbQuery.append(" FROM COLECTIVOS_AUT_TP_ENDOSO ");
			sbQuery.append(" WHERE CATE_CD_TIPO_ENDOSO = ").append(tipoEndoso);
				sbQuery.append(" AND CATE_ST_TIPO_ENDOSO = 1 ");
				sbQuery.append(" AND CATE_REQ_AUT = 1 ");
			
			arlResultados.addAll(endosoDatosDao.consultar(sbQuery.toString()));
			
			if(!arlResultados.isEmpty()) {
				it = arlResultados.iterator();
				while(it.hasNext()) {
					objTipoEndoso = new TipoEndosoDTO((Object[]) it.next());
					sbCorreos.append(objTipoEndoso.getCorreo()).append(",");
				}
				
				if(objTipoEndoso != null) {
					objTipoEndoso.setCorreo(sbCorreos.toString());
					arlAutoriza.add(objTipoEndoso);
				}
			}
		} catch (Excepciones e) {
			this.message.append("Error al consultar autorizadores de tipo Endoso.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage()); 
		}
		
		return arlAutoriza;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#getEndososPendientes(java.lang.String)
	 */
	@Override
	public Collection<EndosoDatosDTO> getEndososPendientes(String userName) throws Excepciones {
		ArrayList<EndosoDatosDTO> arlResultado;
		List<Object> lstDatos;
		Iterator<Object> it;
		EndosoDatosDTO objEndosoDatos;
		StringBuilder filtro;
		
		try {
			arlResultado = new ArrayList<>();
			lstDatos = new ArrayList<>();
			filtro = new StringBuilder();
			
			filtro.append(" SELECT ed "); 
				filtro.append(" FROM EndososDatos ed, AutorizaTipoEndoso ae ");
			filtro.append(" WHERE ae.id.cateCdTipoEndoso = ed.cedatpEndoso ");
				filtro.append(" AND ed.cedaStEndoso = ").append(Constantes.ENDOSO_ESTATUS_PENDIENTE);
				filtro.append(" AND ae.cateReqAut = 1 ");
				filtro.append(" AND ae.cateStTipoEndoso = 1 ");
				filtro.append(" AND ae.id.cateCdUsuAut = '").append(userName).append("' ");
			filtro.append(" ORDER BY ed.cedaFeAplica ");
					
			lstDatos.addAll(this.endososDatosDao.getEndososDatos(filtro.toString()));
			it = lstDatos.iterator();
			while (it.hasNext()) {
				objEndosoDatos = (EndosoDatosDTO) ConstruirObjeto.crearObjeto(EndosoDatosDTO.class, it.next());				
				arlResultado.add(objEndosoDatos);
			}

			return arlResultado;
		} catch (Excepciones e) {
			this.message.append("Error al obtener endosos datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#autorizarEndoso(mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO)
	 */
	@Override
	public void autorizarEndoso(EndosoDatosDTO seleccionado) throws Excepciones {
		String strTipoEndo;
		
		try {
			strTipoEndo = String.valueOf(seleccionado.getCedatpEndoso());
			switch (strTipoEndo) {
				case Constantes.TIPO_ENDOSO_SUMA:
				case Constantes.TIPO_ENDOSO_VIGENCIAS:
				case Constantes.TIPO_ENDOSO_FECHA_SUS:
					seleccionado.setCedaCampon3(new BigDecimal(this.endososDatosDao.generaMovimiento(seleccionado.getId().getCedaCasuCdSucursal(), seleccionado.getId().getCedaCarpCdRamo(),
							seleccionado.getId().getCedaCapoNuPoliza(), seleccionado.getId().getCedaNuCertificado())));				
					this.endososDatosDao.actualizaCertificado(seleccionado);
					break;

				case Constantes.TIPO_ENDOSO_BENEFICIARIOS:
					this.endosoDatosDao.actualizaBeneficiario(seleccionado);
					break;
				case Constantes.TIPO_ENDOSO_FECHA_NAC:
					this.endososDatosDao.actualizaCliente(seleccionado);
					this.endososDatosDao.updateEndosoFecha(seleccionado.getCedaNuRecibo());
					
				default:
					this.endososDatosDao.actualizaCliente(seleccionado);
					break;
			}
			
			actualizaDatosRector(seleccionado);
			
			seleccionado.setCedaFeAplica(new Date());
			seleccionado.setCedaStEndoso(Constantes.ENDOSO_ESTATUS_APLICADO);
			seleccionado.setCedaCdUsuarioAplica(FacesUtils.getBeanSesion().getUserName());
			seleccionado.setCedaCorreoAplica(FacesUtils.getBeanSesion().getObjUsuario().getCorreo());
			actualizarObjeto(seleccionado);
		} catch (Excepciones e) {
			this.message.append("Error al obtener endosos datos.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#validaRangoSumas(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado)
	 */
	@Override
	public boolean validaRangoSumas(Certificado objEndoso) throws Excepciones {
		ArrayList<Object> arlResultados;
		StringBuilder sbQuery;
	    String isPrimaUnica;
	    Object[] arrRangos;
	    BigDecimal nSumMin;
	    BigDecimal nSumMax;
	    
		try {
			arlResultados = new ArrayList<>();			
			sbQuery = new StringBuilder();
						
			isPrimaUnica = objEndoso.getCoceSubCampana();
			if(objEndoso.getCoceSubCampana() == null) {
				isPrimaUnica = Constantes.DEFAULT_STRING_ID;
			}
			
			sbQuery.append(" SELECT NVL(MIN(TO_NUMBER(CP.COPA_VVALOR1)), 0.0001), NVL(MAX(TO_NUMBER(CP.COPA_VVALOR2)), -1) ");
				sbQuery.append(" FROM COLECTIVOS_PARAMETROS CP ");
			sbQuery.append(" WHERE CP.COPA_DES_PARAMETRO = 'POLIZA' ");
				sbQuery.append(" AND CP.COPA_ID_PARAMETRO  = 'SALDOINSOLUTO' ");
				sbQuery.append(" AND CP.COPA_NVALOR1 = ").append(objEndoso.getId().getCoceCasuCdSucursal());
				sbQuery.append(" AND CP.COPA_NVALOR2 = ").append(objEndoso.getId().getCoceCarpCdRamo());
				sbQuery.append(" AND CP.COPA_NVALOR3 = DECODE(").append(isPrimaUnica).append(", 0, ").append(objEndoso.getId().getCoceCapoNuPoliza()).append(", CP.COPA_NVALOR3)");
				sbQuery.append(" AND CP.COPA_NVALOR6 = DECODE(").append(isPrimaUnica).append(", 0, CP.COPA_NVALOR6, ").append(isPrimaUnica).append(")");
			
			arlResultados.addAll(endosoDatosDao.consultar(sbQuery.toString()));
			arrRangos = (Object[]) arlResultados.get(0);
			nSumMin = (BigDecimal) arrRangos[0];
			nSumMax = (BigDecimal) arrRangos[1];
			
			if( !(objEndoso.getCoceMtSumaAsegurada().compareTo(nSumMin) >= 0 && ( nSumMax.compareTo(new BigDecimal(-1)) == 0 || objEndoso.getCoceMtSumaAsegurada().compareTo(nSumMax) <= 0 )) ) {
				return false;
			}
			
		} catch (Excepciones e) {
			this.message.append("Error al valida rango de Sumas.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage()); 
		}
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#calculaNuevaPrima(mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado)
	 */
	@Override
	public CargaDTO calculaNuevaPrima(Certificado certificado) throws Excepciones {
		CargaDTO objRegreso;		
		Carga objCarga;
		
		try {
			objCarga = new Carga(new CargaId());
			objCarga.getId().setCocrCdSucursal(certificado.getId().getCoceCasuCdSucursal());
			objCarga.getId().setCocrCdRamo(certificado.getId().getCoceCarpCdRamo());
			objCarga.getId().setCocrNumPoliza(certificado.getId().getCoceCapoNuPoliza());
			objCarga.getId().setCocrIdCertificado(certificado.getId().getCoceNuCertificado().toString());
			objCarga.getId().setCocrTipoRegistro("END");
			objCarga.getId().setCocrSumaAsegurada(certificado.getCoceMtSumaAsegurada());
			objCarga.setCocrCdProducto(String.valueOf(certificado.getPlanes().getId().getAlplCdProducto()));
			objCarga.setCocrCdPlan(String.valueOf(certificado.getPlanes().getId().getAlplCdPlan()));
			objCarga.setCocrFeIngreso( certificado.getCoceFeSuscripcion());
			objCarga.setCocrNuPlazo(12);
			
			this.endosoDatosDao.guardarObjeto(objCarga);

			this.procesosDao.calculaPrimas(objCarga.getId());
			
			objCarga = this.endosoDatosDao.obtenerObjeto(Carga.class, objCarga.getId());
			objRegreso = (CargaDTO) ConstruirObjeto.crearBean(CargaDTO.class, objCarga);
			
			this.endosoDatosDao.borrarObjeto(objCarga);
		} catch (Excepciones e) {
			this.message.append("Error al calcular nuevas primas.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
		
		return objRegreso;
	}

	/*
	 * (non-Javadoc)
	 * @see mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos#getFechasPoliza(mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId)
	 */
	@Override
	public Object[] getFechasPoliza(CertificadoId id) throws Excepciones {
		Object[] arrResultado = null;
		StringBuilder sbQuery;
	    
		try {
			sbQuery = new StringBuilder();
			
			sbQuery.append(" SELECT COCE_FE_DESDE, COCE_FE_HASTA, ADD_MONTHS(COCE_FE_DESDE, (-1 * (NVL(COCE_NU_RENOVACION, 0) * 12))) ");
			sbQuery.append(" FROM COLECTIVOS_CERTIFICADOS ");
			sbQuery.append(" WHERE COCE_CASU_CD_SUCURSAL = ").append(id.getCoceCasuCdSucursal());
			sbQuery.append(" AND COCE_CARP_CD_RAMO = ").append(id.getCoceCarpCdRamo());
			sbQuery.append(" AND COCE_CAPO_NU_POLIZA = ").append(id.getCoceCapoNuPoliza());
			sbQuery.append(" AND COCE_NU_CERTIFICADO = ").append(Constantes.DEFAULT_STRING_ID);
			
			arrResultado = (Object[]) endosoDatosDao.consultar(sbQuery.toString()).get(0);
		} catch (Excepciones e) {
			this.message.append("Error al obtener fechas de la poliza.");
			this.log.error(message.toString(), e);
			throw new Excepciones(e.getMessage());
		}
		
		return arrResultado;
	}

	private String actualizaDatosRector(EndosoDatosDTO seleccionado) {		
		List<Object> lstResultados;
		Object[] arrResultado = null;
		StringBuilder sbQuery;
		String strTipoEndo;
		
		try {
			lstResultados = new ArrayList<>();
			sbQuery = new StringBuilder();	
			
			sbQuery.append(" SELECT CACE_CACN_CD_NACIONALIDAD, CACE_CACN_NU_CEDULA_RIF, CACE_FE_SUSCRIPCION, CACE_MT_SUMA_ASEGURADA, CACE_MT_PRIMA_ANUAL, CACE_FE_DESDE, CACE_FE_HASTA, ");
			sbQuery.append(" COCE_CAPU_CD_PRODUCTO, COCE_CAPB_CD_PLAN, COCE_NU_COBERTURA, DECODE(NVL(COCE_SUB_CAMPANA, 0), 0, 0, 1) ");
			sbQuery.append(" FROM CART_CERTIFICADOS ");
			sbQuery.append(" INNER JOIN COLECTIVOS_CERTIFICADOS ON(COCE_CASU_CD_SUCURSAL = CACE_CASU_CD_SUCURSAL AND COCE_CARP_CD_RAMO = CACE_CARP_CD_RAMO AND COCE_CAPO_NU_POLIZA = CACE_CAPO_NU_POLIZA AND COCE_NU_CERTIFICADO = CACE_NU_CERTIFICADO) ");
			sbQuery.append(" WHERE CACE_CASU_CD_SUCURSAL = ").append(seleccionado.getId().getCedaCasuCdSucursal());
			sbQuery.append(" AND CACE_CARP_CD_RAMO = ").append(seleccionado.getId().getCedaCarpCdRamo());
			sbQuery.append(" AND CACE_CAPO_NU_POLIZA = ").append(seleccionado.getId().getCedaCapoNuPoliza());
			sbQuery.append(" AND CACE_NU_CERTIFICADO = ").append(seleccionado.getId().getCedaNuCertificado());
			
			lstResultados.addAll(endosoDatosDao.consultar(sbQuery.toString()));
			
			if(lstResultados.isEmpty()) {
				return Constantes.DEFAULT_STRING;
			}
			
			arrResultado = (Object[]) lstResultados.get(0);
			strTipoEndo = String.valueOf(seleccionado.getCedatpEndoso());
			switch (strTipoEndo) {
				case Constantes.TIPO_ENDOSO_SUMA:
				case Constantes.TIPO_ENDOSO_VIGENCIAS:
				case Constantes.TIPO_ENDOSO_FECHA_SUS:
					this.endososDatosDao.actualizaCertificado(seleccionado, arrResultado);
					break;
	
				case Constantes.TIPO_ENDOSO_BENEFICIARIOS:
					this.procesosDao.migraBeneficiario(seleccionado.getId());
					break;
					
				default:
					this.endososDatosDao.actualizaCliente(seleccionado, arrResultado);
					break;
			}
			
		} catch (Excepciones e) {
			this.message.append("Error al actualizar datos en RECTOR.");
			this.log.error(message.toString(), e);
		}
		
		return Constantes.DEFAULT_STRING_ID;
	}
	
	
	
	
	
	//Metododo para la generacion de RFC
		public String genRFCCert(String RFC, int numCertificado) throws Excepciones {
			// TODO Auto-generated method stub
			return this.procesosDao.genRFCColCerti( RFC,   numCertificado);
		}
}
