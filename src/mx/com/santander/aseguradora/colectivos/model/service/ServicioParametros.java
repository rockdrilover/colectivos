/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;


/**
 * @author Sergio Plata
 *
 */
public interface ServicioParametros extends ServicioCatalogo{
	
	public <T> List<T> obtenerCatalogoEndoso(String filtro)throws Excepciones;
	public Long siguienteCdParam() throws Excepciones;
    public <T> List<T> obtenerProductos(String filtro)throws Excepciones;
    public Long siguente()throws Excepciones;
    public Integer existe(String filtro) throws Excepciones;
    public <T> List<T> obtenerCanalVenta(String filtro) throws Excepciones;
    public <T> List<T> obtenerCatalogoFechasAjuste(String filtro) throws Excepciones;
	public String getErrorCatalogo(String errores) throws Exception;
}
