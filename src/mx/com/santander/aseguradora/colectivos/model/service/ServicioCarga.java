/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecarga;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanClienteCertif;


/**
 * @author Sergio Plata
 *
 */
public interface ServicioCarga extends ServicioCatalogo{
	
	public void borrarDatosTemporales(Short canal, Short ramo, Long poliza, Integer inIdVenta, String archivo, String origen)throws Excepciones;
	public <T> List<T> estatusEmision(Short canal, Short ramo, Long poliza, Integer inIdVenta, String archivo)throws Excepciones;
	public <T> List<T> estatusCancelacion(Short canal, Short ramo, Long inPoliza, String archivo)throws Excepciones;
	public <T> List<T> estatusRehabilitacion(Short canal, Short ramo, Long poliza, String nombreArchivo)throws Excepciones;
	public boolean existeCargaConcilia(Short origen) throws Excepciones;
	public Double getCumulo(Short inCanal, Short inRamo, Long inPoliza,	Integer inIdVenta) throws Excepciones;
	public void reporteErrores(String rutaTemporal, String rutaReporte,	Map<String, Object> inParams) throws Excepciones;
	public void estatusCancelacion(List<BeanClienteCertif> listaCertificados,List<BeanClienteCertif> listaCertificadosCancelacion) throws Excepciones;
	public List<Object> getParametrosConciliacion(String tipoParametros) throws Exception ;
	public ArrayList<Object> consultarVentasConsumo(long tipoReg, int ramo, String strOrigen) throws Exception;
	public HashMap<String, String> getEstados() throws Exception;
	public Map<String, String> getPlazos() throws Exception;
	public List<Object> getCuentas() throws Exception;
	public ArrayList<ArrayList<Resultado>> cargadasAnteriormente(Integer inOrigen, int nRamo, String strSistemaOrigen, Date fechaIni, Date fechaFin, String[] arrCuentas) throws Exception;
	public Collection<? extends Resultado> buscarRegistrosGuardados(Integer inOrigen, int nRamo, String strSistemaOrigen, ArrayList<Resultado> arlFechas) throws Exception;
	public void conciliarVentas(HashMap<Integer, ArrayList<Object>> hmConciliar) throws Exception;
	public long buscarConsecutivoAltair(String fechaAprobacion) throws Exception;
	public int getConsecutivo(Integer inOrigen) throws Exception;
	public <T> void actualizarObjetos(List<T> lista) throws Excepciones;
	public VtaPrecarga buscarVentaHPU(String strFolio) throws Exception;
	public void divisionAutoCredito() throws Exception;
	public List<Object> getPreEmisionDesempleo(Date fechaInicial, Date fechaFinal)throws Excepciones;
	public List<Object> consultarMapeo(String strQuery) throws Exception;
	public void conciliarManualmente(ArrayList<Object> arrayList) throws Exception;
	public int guardarErroresTraspaso(String tipoVenta, String idCertificado, String strFechaIngreso, String sumaAseg, String saldoInsoluto, String tipoRegistro, String descError, int nConseError) throws Exception;
	public void respaldarErrores(Short inCanal, Short inRamo, Long inPoliza, Integer inIdVenta, String nombreArchivo) throws Exception;
	
	/**
	 * Metodo que envie errores por correo al uusario
	 * @param codigoErrorConciliacion codigoError
	 * @param sbMsgCorreo Mesanje del correo
	 * @param strTituloMsg Titulo del Correo
	 * @throws Exception
	 */
	public void enviarErroresLE(String codigoErrorConciliacion, StringBuilder sbMsgCorreo, String strTituloMsg) throws Exception;
	public void buscarCuentaPampa(String[] strCuenta) throws Exception;
	public List<Object> consultaDatosPampa() throws Exception;
	public void updatePampa()throws Excepciones;
	public void updatePampaCancelacion()throws Excepciones;
	public void estatusVta()throws Excepciones;
	public List<Object> consultaDatosPampaCancelacion() throws Exception;

}
