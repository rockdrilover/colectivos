/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.exception;

/**
 * @author Sergio Plata
 *
 */
public class UsuarioDuplicado extends Excepciones {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2401348535854118741L;

	public UsuarioDuplicado(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
