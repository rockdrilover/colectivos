/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.exception;

/**
 * @author Sergio Plata
 *
 */
public class Excepciones extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1479724849944096669L;

	/**
	 * 
	 * @param message
	 */
	public Excepciones(String message) {
		// TODO Auto-generated constructor stub
		super(message);
	}
	
	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public Excepciones(String message, Throwable cause) {
		// TODO Auto-generated constructor stub
		super(message, cause);
	}
}
