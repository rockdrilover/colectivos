/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.exception;

/**
 * @author Sergio Plata
 *
 */
public class PreCargaIdDuplicado extends Excepciones {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8932210822132141475L;

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public PreCargaIdDuplicado(String message, Throwable cause) {
		// TODO Auto-generated constructor stub
		super(message, cause);
	}

}
