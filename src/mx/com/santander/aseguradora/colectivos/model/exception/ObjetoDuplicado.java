/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.exception;

/**
 * @author Sergio Plata
 *
 */
public class ObjetoDuplicado extends Excepciones {

	/**
	 * 
	 */
	private static final long serialVersionUID = -140145113075070445L;

	public ObjetoDuplicado(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
