/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.model.exception;

/**
 * @author Sergio Plata
 *
 */
public class EstatusDuplicado extends Excepciones {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8526350693689978163L;

	public EstatusDuplicado(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
