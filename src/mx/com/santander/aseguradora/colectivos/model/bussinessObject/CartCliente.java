package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CART_CLIENTES database table.
 * 
 */

@Entity
@Table(name="CART_CLIENTES")
public class CartCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CartClientePK id;

	@Column(name="CACN_APELLIDO_MAT")
	private String cacnApellidoMat;

	@Column(name="CACN_APELLIDO_PAT")
	private String cacnApellidoPat;

	@Column(name="CACN_CAAE_CD_ACTIVIDAD")
	private BigDecimal cacnCaaeCdActividad;

	@Column(name="CACN_CACI_CD_CIUDAD_COB")
	private BigDecimal cacnCaciCdCiudadCob;

	@Column(name="CACN_CACI_CD_CIUDAD_HAB")
	private BigDecimal cacnCaciCdCiudadHab;

	@Column(name="CACN_CAES_CD_ESTADO_COB")
	private BigDecimal cacnCaesCdEstadoCob;

	@Column(name="CACN_CAES_CD_ESTADO_HAB")
	private BigDecimal cacnCaesCdEstadoHab;

	@Column(name="CACN_CAPA_CD_PAIS")
	private BigDecimal cacnCapaCdPais;

	@Column(name="CACN_CAZP_COLONIA_COB")
	private String cacnCazpColoniaCob;

	@Column(name="CACN_CAZP_POBLAC_COB")
	private String cacnCazpPoblacCob;

	@Column(name="CACN_CD_DEPENDENCIA1")
	private BigDecimal cacnCdDependencia1;

	@Column(name="CACN_CD_DEPENDENCIA2")
	private BigDecimal cacnCdDependencia2;

	@Column(name="CACN_CD_DEPENDENCIA3")
	private BigDecimal cacnCdDependencia3;

	@Column(name="CACN_CD_DEPENDENCIA4")
	private BigDecimal cacnCdDependencia4;

	@Column(name="CACN_CD_EDO_CIVIL")
	private String cacnCdEdoCivil;

	@Column(name="CACN_CD_NACIONALIDAD_A")
	private String cacnCdNacionalidadA;

	@Column(name="CACN_CD_SEXO")
	private String cacnCdSexo;

	@Column(name="CACN_CD_TAXID")
	private String cacnCdTaxid;

	@Column(name="CACN_CD_VIDA")
	private String cacnCdVida;

	@Column(name="CACN_DATOS_REG_1")
	private String cacnDatosReg1;

	@Column(name="CACN_DATOS_REG_2")
	private String cacnDatosReg2;

	@Column(name="CACN_DI_COBRO1")
	private String cacnDiCobro1;

	@Column(name="CACN_DI_COBRO2")
	private String cacnDiCobro2;

	@Column(name="CACN_DI_HABITACION1")
	private String cacnDiHabitacion1;

	@Column(name="CACN_DI_HABITACION2")
	private String cacnDiHabitacion2;

    @Temporal( TemporalType.DATE)
	@Column(name="CACN_FE_NACIMIENTO")
	private Date cacnFeNacimiento;

	@Column(name="CACN_IN_EMPLEADO")
	private String cacnInEmpleado;

	@Column(name="CACN_IN_TRATAMIENTO_ESPECIAL")
	private String cacnInTratamientoEspecial;

	@Column(name="CACN_IN_USADO_FINANCIAMIENTO")
	private String cacnInUsadoFinanciamiento;

	@Column(name="CACN_NACIONALIDAD_FATCA")
	private BigDecimal cacnNacionalidadFatca;

	@Column(name="CACN_NM_APELLIDO_RAZON")
	private String cacnNmApellidoRazon;

	@Column(name="CACN_NM_PERSONA_NATURAL")
	private String cacnNmPersonaNatural;

	@Column(name="CACN_NOMBRE")
	private String cacnNombre;

	@Column(name="CACN_NU_APARTADO_COBRO")
	private String cacnNuApartadoCobro;

	@Column(name="CACN_NU_APARTADO_HABITACION")
	private String cacnNuApartadoHabitacion;

	@Column(name="CACN_NU_CEDULA_ANTERIOR")
	private String cacnNuCedulaAnterior;

	@Column(name="CACN_NU_CONSULTAS")
	private BigDecimal cacnNuConsultas;

	@Column(name="CACN_NU_EMPLEADO_FICHA")
	private String cacnNuEmpleadoFicha;

	@Column(name="CACN_NU_FAX_COBRO")
	private String cacnNuFaxCobro;

	@Column(name="CACN_NU_LLAMADAS")
	private BigDecimal cacnNuLlamadas;

	@Column(name="CACN_NU_SERVICIOS")
	private BigDecimal cacnNuServicios;

	@Column(name="CACN_NU_TELEFONO_COBRO")
	private String cacnNuTelefonoCobro;

	@Column(name="CACN_NU_TELEFONO_HABITACION")
	private String cacnNuTelefonoHabitacion;

	@Column(name="CACN_PAIS_FATCA")
	private BigDecimal cacnPaisFatca;

	@Column(name="CACN_REGISTRO_1")
	private String cacnRegistro1;

	@Column(name="CACN_REGISTRO_2")
	private String cacnRegistro2;

	@Column(name="CACN_RFC")
	private String cacnRfc;

	@Column(name="CACN_ST_ACTUA_NOM")
	private String cacnStActuaNom;

	@Column(name="CACN_ST_CLIENTE")
	private String cacnStCliente;

	@Column(name="CACN_TP_CLIENTE")
	private String cacnTpCliente;

	@Column(name="CACN_ZN_POSTAL_COBRO")
	private String cacnZnPostalCobro;

	@Column(name="CACN_ZN_POSTAL_HABITACION")
	private String cacnZnPostalHabitacion;

    public CartCliente() {
    }

	public CartClientePK getId() {
		return this.id;
	}

	public void setId(CartClientePK id) {
		this.id = id;
	}
	
	public String getCacnApellidoMat() {
		return this.cacnApellidoMat;
	}

	public void setCacnApellidoMat(String cacnApellidoMat) {
		this.cacnApellidoMat = cacnApellidoMat;
	}

	public String getCacnApellidoPat() {
		return this.cacnApellidoPat;
	}

	public void setCacnApellidoPat(String cacnApellidoPat) {
		this.cacnApellidoPat = cacnApellidoPat;
	}

	public BigDecimal getCacnCaaeCdActividad() {
		return this.cacnCaaeCdActividad;
	}

	public void setCacnCaaeCdActividad(BigDecimal cacnCaaeCdActividad) {
		this.cacnCaaeCdActividad = cacnCaaeCdActividad;
	}

	public BigDecimal getCacnCaciCdCiudadCob() {
		return this.cacnCaciCdCiudadCob;
	}

	public void setCacnCaciCdCiudadCob(BigDecimal cacnCaciCdCiudadCob) {
		this.cacnCaciCdCiudadCob = cacnCaciCdCiudadCob;
	}

	public BigDecimal getCacnCaciCdCiudadHab() {
		return this.cacnCaciCdCiudadHab;
	}

	public void setCacnCaciCdCiudadHab(BigDecimal cacnCaciCdCiudadHab) {
		this.cacnCaciCdCiudadHab = cacnCaciCdCiudadHab;
	}

	public BigDecimal getCacnCaesCdEstadoCob() {
		return this.cacnCaesCdEstadoCob;
	}

	public void setCacnCaesCdEstadoCob(BigDecimal cacnCaesCdEstadoCob) {
		this.cacnCaesCdEstadoCob = cacnCaesCdEstadoCob;
	}

	public BigDecimal getCacnCaesCdEstadoHab() {
		return this.cacnCaesCdEstadoHab;
	}

	public void setCacnCaesCdEstadoHab(BigDecimal cacnCaesCdEstadoHab) {
		this.cacnCaesCdEstadoHab = cacnCaesCdEstadoHab;
	}

	public BigDecimal getCacnCapaCdPais() {
		return this.cacnCapaCdPais;
	}

	public void setCacnCapaCdPais(BigDecimal cacnCapaCdPais) {
		this.cacnCapaCdPais = cacnCapaCdPais;
	}

	public String getCacnCazpColoniaCob() {
		return this.cacnCazpColoniaCob;
	}

	public void setCacnCazpColoniaCob(String cacnCazpColoniaCob) {
		this.cacnCazpColoniaCob = cacnCazpColoniaCob;
	}

	public String getCacnCazpPoblacCob() {
		return this.cacnCazpPoblacCob;
	}

	public void setCacnCazpPoblacCob(String cacnCazpPoblacCob) {
		this.cacnCazpPoblacCob = cacnCazpPoblacCob;
	}

	public BigDecimal getCacnCdDependencia1() {
		return this.cacnCdDependencia1;
	}

	public void setCacnCdDependencia1(BigDecimal cacnCdDependencia1) {
		this.cacnCdDependencia1 = cacnCdDependencia1;
	}

	public BigDecimal getCacnCdDependencia2() {
		return this.cacnCdDependencia2;
	}

	public void setCacnCdDependencia2(BigDecimal cacnCdDependencia2) {
		this.cacnCdDependencia2 = cacnCdDependencia2;
	}

	public BigDecimal getCacnCdDependencia3() {
		return this.cacnCdDependencia3;
	}

	public void setCacnCdDependencia3(BigDecimal cacnCdDependencia3) {
		this.cacnCdDependencia3 = cacnCdDependencia3;
	}

	public BigDecimal getCacnCdDependencia4() {
		return this.cacnCdDependencia4;
	}

	public void setCacnCdDependencia4(BigDecimal cacnCdDependencia4) {
		this.cacnCdDependencia4 = cacnCdDependencia4;
	}

	public String getCacnCdEdoCivil() {
		return this.cacnCdEdoCivil;
	}

	public void setCacnCdEdoCivil(String cacnCdEdoCivil) {
		this.cacnCdEdoCivil = cacnCdEdoCivil;
	}

	public String getCacnCdNacionalidadA() {
		return this.cacnCdNacionalidadA;
	}

	public void setCacnCdNacionalidadA(String cacnCdNacionalidadA) {
		this.cacnCdNacionalidadA = cacnCdNacionalidadA;
	}

	public String getCacnCdSexo() {
		return this.cacnCdSexo;
	}

	public void setCacnCdSexo(String cacnCdSexo) {
		this.cacnCdSexo = cacnCdSexo;
	}

	public String getCacnCdTaxid() {
		return this.cacnCdTaxid;
	}

	public void setCacnCdTaxid(String cacnCdTaxid) {
		this.cacnCdTaxid = cacnCdTaxid;
	}

	public String getCacnCdVida() {
		return this.cacnCdVida;
	}

	public void setCacnCdVida(String cacnCdVida) {
		this.cacnCdVida = cacnCdVida;
	}

	public String getCacnDatosReg1() {
		return this.cacnDatosReg1;
	}

	public void setCacnDatosReg1(String cacnDatosReg1) {
		this.cacnDatosReg1 = cacnDatosReg1;
	}

	public String getCacnDatosReg2() {
		return this.cacnDatosReg2;
	}

	public void setCacnDatosReg2(String cacnDatosReg2) {
		this.cacnDatosReg2 = cacnDatosReg2;
	}

	public String getCacnDiCobro1() {
		return this.cacnDiCobro1;
	}

	public void setCacnDiCobro1(String cacnDiCobro1) {
		this.cacnDiCobro1 = cacnDiCobro1;
	}

	public String getCacnDiCobro2() {
		return this.cacnDiCobro2;
	}

	public void setCacnDiCobro2(String cacnDiCobro2) {
		this.cacnDiCobro2 = cacnDiCobro2;
	}

	public String getCacnDiHabitacion1() {
		return this.cacnDiHabitacion1;
	}

	public void setCacnDiHabitacion1(String cacnDiHabitacion1) {
		this.cacnDiHabitacion1 = cacnDiHabitacion1;
	}

	public String getCacnDiHabitacion2() {
		return this.cacnDiHabitacion2;
	}

	public void setCacnDiHabitacion2(String cacnDiHabitacion2) {
		this.cacnDiHabitacion2 = cacnDiHabitacion2;
	}

	public Date getCacnFeNacimiento() {
		return this.cacnFeNacimiento;
	}

	public void setCacnFeNacimiento(Date cacnFeNacimiento) {
		this.cacnFeNacimiento = cacnFeNacimiento;
	}

	public String getCacnInEmpleado() {
		return this.cacnInEmpleado;
	}

	public void setCacnInEmpleado(String cacnInEmpleado) {
		this.cacnInEmpleado = cacnInEmpleado;
	}

	public String getCacnInTratamientoEspecial() {
		return this.cacnInTratamientoEspecial;
	}

	public void setCacnInTratamientoEspecial(String cacnInTratamientoEspecial) {
		this.cacnInTratamientoEspecial = cacnInTratamientoEspecial;
	}

	public String getCacnInUsadoFinanciamiento() {
		return this.cacnInUsadoFinanciamiento;
	}

	public void setCacnInUsadoFinanciamiento(String cacnInUsadoFinanciamiento) {
		this.cacnInUsadoFinanciamiento = cacnInUsadoFinanciamiento;
	}

	public BigDecimal getCacnNacionalidadFatca() {
		return this.cacnNacionalidadFatca;
	}

	public void setCacnNacionalidadFatca(BigDecimal cacnNacionalidadFatca) {
		this.cacnNacionalidadFatca = cacnNacionalidadFatca;
	}

	public String getCacnNmApellidoRazon() {
		return this.cacnNmApellidoRazon;
	}

	public void setCacnNmApellidoRazon(String cacnNmApellidoRazon) {
		this.cacnNmApellidoRazon = cacnNmApellidoRazon;
	}

	public String getCacnNmPersonaNatural() {
		return this.cacnNmPersonaNatural;
	}

	public void setCacnNmPersonaNatural(String cacnNmPersonaNatural) {
		this.cacnNmPersonaNatural = cacnNmPersonaNatural;
	}

	public String getCacnNombre() {
		return this.cacnNombre;
	}

	public void setCacnNombre(String cacnNombre) {
		this.cacnNombre = cacnNombre;
	}

	public String getCacnNuApartadoCobro() {
		return this.cacnNuApartadoCobro;
	}

	public void setCacnNuApartadoCobro(String cacnNuApartadoCobro) {
		this.cacnNuApartadoCobro = cacnNuApartadoCobro;
	}

	public String getCacnNuApartadoHabitacion() {
		return this.cacnNuApartadoHabitacion;
	}

	public void setCacnNuApartadoHabitacion(String cacnNuApartadoHabitacion) {
		this.cacnNuApartadoHabitacion = cacnNuApartadoHabitacion;
	}

	public String getCacnNuCedulaAnterior() {
		return this.cacnNuCedulaAnterior;
	}

	public void setCacnNuCedulaAnterior(String cacnNuCedulaAnterior) {
		this.cacnNuCedulaAnterior = cacnNuCedulaAnterior;
	}

	public BigDecimal getCacnNuConsultas() {
		return this.cacnNuConsultas;
	}

	public void setCacnNuConsultas(BigDecimal cacnNuConsultas) {
		this.cacnNuConsultas = cacnNuConsultas;
	}

	public String getCacnNuEmpleadoFicha() {
		return this.cacnNuEmpleadoFicha;
	}

	public void setCacnNuEmpleadoFicha(String cacnNuEmpleadoFicha) {
		this.cacnNuEmpleadoFicha = cacnNuEmpleadoFicha;
	}

	public String getCacnNuFaxCobro() {
		return this.cacnNuFaxCobro;
	}

	public void setCacnNuFaxCobro(String cacnNuFaxCobro) {
		this.cacnNuFaxCobro = cacnNuFaxCobro;
	}

	public BigDecimal getCacnNuLlamadas() {
		return this.cacnNuLlamadas;
	}

	public void setCacnNuLlamadas(BigDecimal cacnNuLlamadas) {
		this.cacnNuLlamadas = cacnNuLlamadas;
	}

	public BigDecimal getCacnNuServicios() {
		return this.cacnNuServicios;
	}

	public void setCacnNuServicios(BigDecimal cacnNuServicios) {
		this.cacnNuServicios = cacnNuServicios;
	}

	public String getCacnNuTelefonoCobro() {
		return this.cacnNuTelefonoCobro;
	}

	public void setCacnNuTelefonoCobro(String cacnNuTelefonoCobro) {
		this.cacnNuTelefonoCobro = cacnNuTelefonoCobro;
	}

	public String getCacnNuTelefonoHabitacion() {
		return this.cacnNuTelefonoHabitacion;
	}

	public void setCacnNuTelefonoHabitacion(String cacnNuTelefonoHabitacion) {
		this.cacnNuTelefonoHabitacion = cacnNuTelefonoHabitacion;
	}

	public BigDecimal getCacnPaisFatca() {
		return this.cacnPaisFatca;
	}

	public void setCacnPaisFatca(BigDecimal cacnPaisFatca) {
		this.cacnPaisFatca = cacnPaisFatca;
	}

	public String getCacnRegistro1() {
		return this.cacnRegistro1;
	}

	public void setCacnRegistro1(String cacnRegistro1) {
		this.cacnRegistro1 = cacnRegistro1;
	}

	public String getCacnRegistro2() {
		return this.cacnRegistro2;
	}

	public void setCacnRegistro2(String cacnRegistro2) {
		this.cacnRegistro2 = cacnRegistro2;
	}

	public String getCacnRfc() {
		return this.cacnRfc;
	}

	public void setCacnRfc(String cacnRfc) {
		this.cacnRfc = cacnRfc;
	}

	public String getCacnStActuaNom() {
		return this.cacnStActuaNom;
	}

	public void setCacnStActuaNom(String cacnStActuaNom) {
		this.cacnStActuaNom = cacnStActuaNom;
	}

	public String getCacnStCliente() {
		return this.cacnStCliente;
	}

	public void setCacnStCliente(String cacnStCliente) {
		this.cacnStCliente = cacnStCliente;
	}

	public String getCacnTpCliente() {
		return this.cacnTpCliente;
	}

	public void setCacnTpCliente(String cacnTpCliente) {
		this.cacnTpCliente = cacnTpCliente;
	}

	public String getCacnZnPostalCobro() {
		return this.cacnZnPostalCobro;
	}

	public void setCacnZnPostalCobro(String cacnZnPostalCobro) {
		this.cacnZnPostalCobro = cacnZnPostalCobro;
	}

	public String getCacnZnPostalHabitacion() {
		return this.cacnZnPostalHabitacion;
	}

	public void setCacnZnPostalHabitacion(String cacnZnPostalHabitacion) {
		this.cacnZnPostalHabitacion = cacnZnPostalHabitacion;
	}

}