package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CART_RAMOS_CONTABLES database table.
 * 
 */
@Entity
@Table(name="CART_RAMOS_CONTABLES")
public class RamoContable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CARB_CD_RAMO")
	private long carbCdRamo;

	@Column(name="CARB_CD_BONDAD")
	private String carbCdBondad;

	@Column(name="CARB_DE_RAMO")
	private String carbDeRamo;

	@Column(name="CARB_IN_BORDERAUX")
	private String carbInBorderaux;

	@Column(name="CARB_PO_COMISION")
	private BigDecimal carbPoComision;

    public RamoContable() {
    }

	public long getCarbCdRamo() {
		return this.carbCdRamo;
	}

	public void setCarbCdRamo(long carbCdRamo) {
		this.carbCdRamo = carbCdRamo;
	}

	public String getCarbCdBondad() {
		return this.carbCdBondad;
	}

	public void setCarbCdBondad(String carbCdBondad) {
		this.carbCdBondad = carbCdBondad;
	}

	public String getCarbDeRamo() {
		return this.carbDeRamo;
	}

	public void setCarbDeRamo(String carbDeRamo) {
		this.carbDeRamo = carbDeRamo;
	}

	public String getCarbInBorderaux() {
		return this.carbInBorderaux;
	}

	public void setCarbInBorderaux(String carbInBorderaux) {
		this.carbInBorderaux = carbInBorderaux;
	}

	public BigDecimal getCarbPoComision() {
		return this.carbPoComision;
	}

	public void setCarbPoComision(BigDecimal carbPoComision) {
		this.carbPoComision = carbPoComision;
	}

}