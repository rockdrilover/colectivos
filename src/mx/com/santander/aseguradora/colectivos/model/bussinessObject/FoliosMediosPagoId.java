package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class FoliosMediosPagoId implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long folio;
	private String ventaOrigen;

	public FoliosMediosPagoId(){
	}
	
	public FoliosMediosPagoId(long folio, String ventaOrigen) {
		setFolio(folio);
		setVentaOrigen(ventaOrigen);
	}

	@Column(name = "FOLIO", nullable = false, precision = 13, scale = 0)
	public long getFolio() {
		return this.folio;
	}

	public void setFolio(long folio) {
		this.folio = folio;
	}

	@Column(name = "VENTA_ORIGEN", nullable = false, length = 10)
	public String getVentaOrigen() {
		return this.ventaOrigen;
	}

	public void setVentaOrigen(String ventaOrigen) {
		this.ventaOrigen = ventaOrigen;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof FoliosMediosPagoId))
			return false;
		FoliosMediosPagoId castOther = (FoliosMediosPagoId) other;

		return (this.getFolio() == castOther.getFolio())
				&& ((this.getVentaOrigen() == castOther.getVentaOrigen()) || (this
						.getVentaOrigen() != null
						&& castOther.getVentaOrigen() != null && this
						.getVentaOrigen().equals(castOther.getVentaOrigen())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getFolio();
		result = 37
				* result
				+ (getVentaOrigen() == null ? 0 : this.getVentaOrigen()
						.hashCode());
		return result;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("%s%d", ventaOrigen, folio);
	}
}
