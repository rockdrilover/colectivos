package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * ColectivosClienteCertif generated by hbm2java
 */
@Entity
@Table(name = "COLECTIVOS_CLIENTE_CERTIF", schema = "Z014058")
public class ColectivosClienteCertif implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1801866829780179106L;
	private ColectivosClienteCertifId id;
	private ColectivosClientes colectivosClientes;
	private ColectivosCertificados colectivosCertificados;

	public ColectivosClienteCertif() {
	}

	public ColectivosClienteCertif(ColectivosClienteCertifId id,
			ColectivosClientes colectivosClientes,
			ColectivosCertificados colectivosCertificados) {
		this.id = id;
		this.colectivosClientes = colectivosClientes;
		this.colectivosCertificados = colectivosCertificados;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "coccNuCliente", column = @Column(name = "COCC_NU_CLIENTE", nullable = false, precision = 15, scale = 0)),
			@AttributeOverride(name = "coccCasuCdSucursal", column = @Column(name = "COCC_CASU_CD_SUCURSAL", nullable = false, precision = 3, scale = 0)),
			@AttributeOverride(name = "coccCarpCdRamo", column = @Column(name = "COCC_CARP_CD_RAMO", nullable = false, precision = 2, scale = 0)),
			@AttributeOverride(name = "coccCapoNuPoliza", column = @Column(name = "COCC_CAPO_NU_POLIZA", nullable = false, precision = 12, scale = 0)),
			@AttributeOverride(name = "coccNuCertificado", column = @Column(name = "COCC_NU_CERTIFICADO", nullable = false, precision = 15, scale = 0)),
			@AttributeOverride(name = "coccIdCertificado", column = @Column(name = "COCC_ID_CERTIFICADO", nullable = false, length = 30)) })
	public ColectivosClienteCertifId getId() {
		return this.id;
	}

	public void setId(ColectivosClienteCertifId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COCC_NU_CLIENTE", nullable = false, insertable = false, updatable = false)
	public ColectivosClientes getColectivosClientes() {
		return this.colectivosClientes;
	}

	public void setColectivosClientes(ColectivosClientes colectivosClientes) {
		this.colectivosClientes = colectivosClientes;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "COCC_CASU_CD_SUCURSAL", referencedColumnName = "COCE_CASU_CD_SUCURSAL", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "COCC_CARP_CD_RAMO", referencedColumnName = "COCE_CARP_CD_RAMO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "COCC_CAPO_NU_POLIZA", referencedColumnName = "COCE_CAPO_NU_POLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "COCC_NU_CERTIFICADO", referencedColumnName = "COCE_NU_CERTIFICADO", nullable = false, insertable = false, updatable = false) })
	public ColectivosCertificados getColectivosCertificados() {
		return this.colectivosCertificados;
	}

	public void setColectivosCertificados(
			ColectivosCertificados colectivosCertificados) {
		this.colectivosCertificados = colectivosCertificados;
	}

}
