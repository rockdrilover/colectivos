package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * The primary key class for the COLECTIVOS_CUENTAS database table.
 * 
 */
@Embeddable
public class ColectivosCuentaId implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	
	@Column(name="COCU_CONSECUTIVO")
	private BigDecimal cocuConsecutivo;

	@Column(name="COCU_CUENTA_PAGO")
	private BigDecimal cocuCuentaPago;

	@Column(name="COCU_NO_CREDITO")
	private String cocuNoCredito;

    @Temporal( TemporalType.DATE)
	@Column(name="COCU_FECHA_INGRESO")
	private Date cocuFechaIngreso;

	@Column(name="COCU_MONTO_PRIMA")
	private BigDecimal cocuMontoPrima;

	@Column(name="COCU_CALIFICADOR")
	private String cocuCalificador;

	@Column(name="COCU_TIPO_MOVIMIENTO")
	private String cocuTipoMovimiento;
	

	public ColectivosCuentaId() {
    }

	public ColectivosCuentaId(String strLinea, String strConsecutivo) {
		setCocuConsecutivo(new BigDecimal(strConsecutivo));
		setCocuCuentaPago(new BigDecimal(strLinea.substring(0, 11)));
		setCocuNoCredito(Constantes.DEFAULT_STRING_ID);
		setCocuFechaIngreso(GestorFechas.generateDate(strLinea.substring(16, 24), "ddMMyyyy"));
		setCocuMontoPrima(new BigDecimal(new Double(strLinea.substring(73, 87)) / 100));
		setCocuCalificador(Constantes.CARGADA_PARA_PRECONCILIAR);
		setCocuTipoMovimiento(strLinea.substring(72,73).trim().equals("+") ? "A" : "C");
	}

	public BigDecimal getCocuConsecutivo() {
		return cocuConsecutivo;
	}
	public void setCocuConsecutivo(BigDecimal cocuConsecutivo) {
		this.cocuConsecutivo = cocuConsecutivo;
	}
    public BigDecimal getCocuCuentaPago() {
		return this.cocuCuentaPago;
	}
	public void setCocuCuentaPago(BigDecimal cocuCuentaPago) {
		this.cocuCuentaPago = cocuCuentaPago;
	}
	public String getCocuNoCredito() {
		return this.cocuNoCredito;
	}
	public void setCocuNoCredito(String cocuNoCredito) {
		this.cocuNoCredito = cocuNoCredito;
	}
	public Date getCocuFechaIngreso() {
		return this.cocuFechaIngreso;
	}
	public void setCocuFechaIngreso(Date cocuFechaIngreso) {
		this.cocuFechaIngreso = cocuFechaIngreso;
	}
	public BigDecimal getCocuMontoPrima() {
		return this.cocuMontoPrima;
	}
	public void setCocuMontoPrima(BigDecimal cocuMontoPrima) {
		this.cocuMontoPrima = cocuMontoPrima;
	}
	public String getCocuCalificador() {
		return this.cocuCalificador;
	}
	public void setCocuCalificador(String cocuCalificador) {
		this.cocuCalificador = cocuCalificador;
	}
	public String getCocuTipoMovimiento() {
		return cocuTipoMovimiento;
	}
	public void setCocuTipoMovimiento(String cocuTipoMovimiento) {
		this.cocuTipoMovimiento = cocuTipoMovimiento;
	}
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ColectivosCuentaId)) {
			return false;
		}
		ColectivosCuentaId castOther = (ColectivosCuentaId)other;
		return 
			(this.cocuConsecutivo == castOther.cocuConsecutivo)
			&& (this.cocuCuentaPago == castOther.cocuCuentaPago)
			&& this.cocuNoCredito.equals(castOther.cocuNoCredito)
			&& this.cocuFechaIngreso.equals(castOther.cocuFechaIngreso)
			&& (this.cocuMontoPrima == castOther.cocuMontoPrima)
			&& this.cocuCalificador.equals(castOther.cocuCalificador)
			&& this.cocuTipoMovimiento.equals(castOther.cocuTipoMovimiento);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.cocuConsecutivo.intValue() ^ (this.cocuConsecutivo.intValue() >>> 32)));
		hash = hash * prime + ((int) (this.cocuCuentaPago.intValue() ^ (this.cocuCuentaPago.intValue() >>> 32)));
		hash = hash * prime + this.cocuNoCredito.hashCode();
		hash = hash * prime + this.cocuFechaIngreso.hashCode();
		hash = hash * prime + ((int) (this.cocuMontoPrima.intValue() ^ (this.cocuMontoPrima.intValue() >>> 32)));
		hash = hash * prime + this.cocuCalificador.hashCode();
		hash = hash * prime + this.cocuTipoMovimiento.hashCode();
		
		return hash;
    }
	
	public String toString() {
		return this.getCocuCuentaPago() + "-" + this.getCocuNoCredito() + "-" +  this.getCocuMontoPrima();
	}
}