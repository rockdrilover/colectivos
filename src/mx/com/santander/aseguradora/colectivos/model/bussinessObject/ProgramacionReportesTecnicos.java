package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


/**
 * @author Z042836
 *
 */
@Entity
@Table(name = "COLECTIVOS_REP_TECNICOS")
public class ProgramacionReportesTecnicos{
	
	
	private String reporte;
	private String nombre;
	private String status;
	private String usuario;
	private String fecha_inicio;


	public ProgramacionReportesTecnicos() {
	}
 
	/**
     * @param reporte, Construccion del reporte
     * @param nombre, Nombre del reporte
     * @param status, Estatus
     * 
     */
	public ProgramacionReportesTecnicos(String reporte, String nombre, String status) {
		super();
		this.reporte = reporte;
		this.nombre = nombre;
		this.status = status;
	}
	

	/**
	 * @return
	 */
	@Lob
	@Type(type="text")
	@Column(name = "REPORTE", nullable = false, columnDefinition = "CLOB")
	public String getReporte() {
		return reporte;
	}


	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

	/**
	 * @return
	 */
	@Column(name = "NOMBRE", nullable = false, length = 80)
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return
	 */
	@Column(name = "ESTATUS", nullable = false, length = 20)
	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return
	 */
	@Column(name = "USUARIO", nullable = false, length = 20)
	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	/**
	 * @return
	 */
	@Column(name = "FECHA_INICIO", nullable = false, length = 20)
	public String getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}
	

}
