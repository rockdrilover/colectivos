package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CART_CFDI_ERROR_WS database table.
 * 
 */
@Entity
@Table(name="CART_CFDI_ERROR_WS")
public class CartCfdiErrorW implements Serializable {
	private static final long serialVersionUID = 1L;

	
	  @Id
	  private Integer id;
	 
	@Column(name="CCEW_CODIGO")
	private String ccewCodigo;

	@Column(name="CCEW_DESCRIPCION")
	private String ccewDescripcion;

	@Column(name="CCEW_TIPO_ERROR")
	private String ccewTipoError;

	@Column(name="CCEW_TIPO_OPERACION")
	private BigDecimal ccewTipoOperacion;

    public CartCfdiErrorW() {
    }

	public String getCcewCodigo() {
		return this.ccewCodigo;
	}

	public void setCcewCodigo(String ccewCodigo) {
		this.ccewCodigo = ccewCodigo;
	}

	public String getCcewDescripcion() {
		return this.ccewDescripcion;
	}

	public void setCcewDescripcion(String ccewDescripcion) {
		this.ccewDescripcion = ccewDescripcion;
	}

	public String getCcewTipoError() {
		return this.ccewTipoError;
	}

	public void setCcewTipoError(String ccewTipoError) {
		this.ccewTipoError = ccewTipoError;
	}

	public BigDecimal getCcewTipoOperacion() {
		return this.ccewTipoOperacion;
	}

	public void setCcewTipoOperacion(BigDecimal ccewTipoOperacion) {
		this.ccewTipoOperacion = ccewTipoOperacion;
	}

}