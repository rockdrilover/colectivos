package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

@Entity
@Table(name = "VTA_PRECARGA")
public class VtaPrecarga implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private VtaPrecargaId id;
	private String cargada;
	private String estatus;
	private String feCarga;
	private String feIngreso;
	private String feEstatus;
	private String feAprobacion;
	private String remesa;
	private String cazbCdSucursal;
	private String analista;
	private String prima;
	private String sumaAsegurada;
	private String cdProducto;
	private String cdPlan;
	private String frPago;
	private String tipoVenta;
	private String numCliente;
	private String apPaterno;
	private String apMaterno;
	private String nombre;
	private String rfc;
	private String feNac;
	private String sexo;
	private String cuenta;
	private String conducto;
	private String banco;
	private String cuentaAlterna;
	private String conductoAlterna;
	private String bancoAlterna;
	private String calle;
	private String colonia;
	private String delmunic;
	private String cdEstado;
	private String cp;
	private String lada;
	private String telefono;
	private String nuContrato;
	private String apPaternoAseg;
	private String apMaternoAseg;
	private String nombreAseg;
	private String rfcAseg;
	private String feNacAseg;
	private String sexoAseg;
	private String edoCivilAseg;
	private String ocupacionAseg;
	private String relacionAseg;
	private String apPaternoBen;
	private String apMaternoBen;
	private String nombreBen;
	private String rfcBen;
	private String feNacBen;
	private String sexoBen;
	private String relacionBen;
	private String pregunta1;
	private String pregunta2;
	private String pregunta3;
	private String pregunta4;
	private String pregunta5;
	private String pregunta6;
	private String registro;
	private String cdError;
	private String desError;
	private String archivo;
	private String usuario;
	private Short ndato1;
	private Short ndato2;
	private Short ndato3;
	private Integer ndato4;
	private Integer ndato5;
	private Integer ndato6;
	private Integer ndato7;
	private Double ndato8;
	private BigDecimal ndato9;
	private String vdato1;
	private String vdato2;
	private String vdato3;
	private String vdato4;
	private String vdato5;
	private String vdato6;
	private String vdato7;
	private String vdato8;
	private String vdato9;
	private String vdato10;
	private String vdato11;
	private String vdato12;
	private String vdato13;
	private Date ddato1;
	private Date ddato2;
	private Date ddato3;

	public VtaPrecarga() {
	}

	public VtaPrecarga(VtaPrecargaId id) {
		this.id = id;
	}

	public VtaPrecarga(VtaPrecargaId id, String feCarga, String feIngreso, String feEstatus,
			String feAprobacion, String remesa, String cazbCdSucursal, String analista, String prima,
			String sumaAsegurada, String cdProducto, String cdPlan, String frPago, String tipoVenta, 
			String numCliente, String apPaterno, String apMaterno, String nombre, String rfc, 
			String feNac, String sexo, String cuenta, String conducto, String banco,
			String cuentaAlterna, String conductoAlterna, String bancoAlterna, String calle, String colonia, 
			String delmunic, String cdEstado, String cp, String lada, String telefono, 
			String nuContrato, String apPaternoAseg, String apMaternoAseg, String nombreAseg, String rfcAseg, 
			String feNacAseg, String sexoAseg, String edoCivilAseg, String ocupacionAseg, String relacionAseg,
			String apPaternoBen, String apMaternoBen, String nombreBen, String rfcBen, String feNacBen,
			String sexoBen, String relacionBen, String pregunta1, String pregunta2, String pregunta3, 
			String pregunta4, String pregunta5, String pregunta6, String registro, String cdError, 
			String desError, String archivo, String usuario, Short ndato1, Short ndato2,
			Short ndato3, Integer ndato4, Integer ndato5, Integer ndato6, Integer ndato7, 
			Double ndato8, BigDecimal ndato9, String vdato1, String vdato2, String vdato3, 
			String vdato4, String vdato5, String vdato6, String vdato7, String vdato8,
			String vdato9, String vdato10, String vdato11, String vdato12, String vdato13, 
			Date ddato1, Date ddato2, Date ddato3) {
		this.id = id;
		this.feCarga = feCarga;
		this.feIngreso = feIngreso;
		this.feEstatus = feEstatus;
		this.feAprobacion = feAprobacion;
		this.remesa = remesa;
		this.cazbCdSucursal = cazbCdSucursal;
		this.analista = analista;
		this.prima = prima;
		this.sumaAsegurada = sumaAsegurada;
		this.cdProducto = cdProducto;
		this.cdPlan = cdPlan;
		this.frPago = frPago;
		this.tipoVenta = tipoVenta;
		this.numCliente = numCliente;
		this.apPaterno = apPaterno;
		this.apMaterno = apMaterno;
		this.nombre = nombre;
		this.rfc = rfc;
		this.feNac = feNac;
		this.sexo = sexo;
		this.cuenta = cuenta;
		this.conducto = conducto;
		this.banco = banco;
		this.cuentaAlterna = cuentaAlterna;
		this.conductoAlterna = conductoAlterna;
		this.bancoAlterna = bancoAlterna;
		this.calle = calle;
		this.colonia = colonia;
		this.delmunic = delmunic;
		this.cdEstado = cdEstado;
		this.cp = cp;
		this.lada = lada;
		this.telefono = telefono;
		this.nuContrato = nuContrato;
		this.apPaternoAseg = apPaternoAseg;
		this.apMaternoAseg = apMaternoAseg;
		this.nombreAseg = nombreAseg;
		this.rfcAseg = rfcAseg;
		this.feNacAseg = feNacAseg;
		this.sexoAseg = sexoAseg;
		this.edoCivilAseg = edoCivilAseg;
		this.ocupacionAseg = ocupacionAseg;
		this.relacionAseg = relacionAseg;
		this.apPaternoBen = apPaternoBen;
		this.apMaternoBen = apMaternoBen;
		this.nombreBen = nombreBen;
		this.rfcBen = rfcBen;
		this.feNacBen = feNacBen;
		this.sexoBen = sexoBen;
		this.relacionBen = relacionBen;
		this.pregunta1 = pregunta1;
		this.pregunta2 = pregunta2;
		this.pregunta3 = pregunta3;
		this.pregunta4 = pregunta4;
		this.pregunta5 = pregunta5;
		this.pregunta6 = pregunta6;
		this.registro = registro;
		this.cdError = cdError;
		this.desError = desError;
		this.archivo = archivo;
		this.usuario = usuario;
		this.ndato1 = ndato1;
		this.ndato2 = ndato2;
		this.ndato3 = ndato3;
		this.ndato4 = ndato4;
		this.ndato5 = ndato5;
		this.ndato6 = ndato6;
		this.ndato7 = ndato7;
		this.ndato8 = ndato8;
		this.ndato9 = ndato9;
		this.vdato1 = vdato1;
		this.vdato2 = vdato2;
		this.vdato3 = vdato3;
		this.vdato4 = vdato4;
		this.vdato5 = vdato5;
		this.vdato6 = vdato6;
		this.vdato7 = vdato7;
		this.vdato8 = vdato8;
		this.vdato9 = vdato9;
		this.vdato10 = vdato10;
		this.vdato11 = vdato11;
		this.vdato12 = vdato12;
		this.vdato13 = vdato13;
		this.ddato1 = ddato1;
		this.ddato2 = ddato2;
		this.ddato3 = ddato3;
	}


	public VtaPrecarga(String strLinea, String strOrigen, int ramo, int nRegConsecutivo) throws Exception{
		setId(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + strLinea.substring(127, 128), strLinea.substring(104, 115), strOrigen));
		setNdato1(new Short(strLinea.substring(127, 128)));
		setCargada(Constantes.CARGADA_PARA_CONCILIAR);
		setEstatus("0");
		setCuenta(strLinea.substring(0, 11));
		setNumCliente(strLinea.substring(12, 20));
		setApPaterno(strLinea.substring(21, 51).trim().equals("") ? "X" : strLinea.substring(21, 51).trim());
		setApMaterno(strLinea.substring(52, 72).trim().equals("") ? "X" : strLinea.substring(52, 72).trim());
		setNombre(strLinea.substring(73, 103).trim());
		setFeIngreso(GestorFechas.formatDate(GestorFechas.generateDate(strLinea.substring(116, 126), "dd-MM-yyyy"), "dd/MM/yyyy"));
		setRemesa(strLinea.substring(129, 133));
		setSumaAsegurada(strLinea.substring(134, 150));
		setPrima(strLinea.substring(151, 167));
		setVdato3(strLinea.substring(168, 170));
		setVdato4(strLinea.substring(171, 175));
		setVdato1(strLinea.substring(176, 182));
		setVdato2(strLinea.substring(183, 187));
		setRfc(strLinea.substring(188, 198).trim().equals("") ? "XXXX100101" : strLinea.substring(188, 198));
		setFeNac(GestorFechas.generarFechaNacimiento(getRfc()));
		setCalle(strLinea.substring(199, 259).trim().equals("") ? "X" : strLinea.substring(199, 259).trim());
		setColonia(strLinea.substring(260, 290).trim().equals("") ? "X" : strLinea.substring(260, 290).trim());		
		setVdato7(strLinea.substring(291, 311).trim().equals("") ? Constantes.ESTADO_BD_MEXICO : strLinea.substring(291, 311).trim());
		setDelmunic(getVdato7());	
		setCp(Utilerias.validaCP(strLinea.substring(312, 317).trim()));
		setFeCarga(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
		setFeEstatus(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
		setVdato13(strLinea);
		setArchivo(Constantes.CONCILIACION_NOMBRE_ARCHIVO);
	}

	public VtaPrecarga(String[] arrDatos, String strLinea, String strOrigen, int ramo, int nRegConsecutivo) throws Exception{
		if(strOrigen.equals(Constantes.CONCILIACION_ALTAMIRA)){
			creaObjetoAltamira(arrDatos, ramo, nRegConsecutivo, strOrigen);
		} else if(strOrigen.equals(Constantes.CONCILIACION_ALTAIR)){
			creaObjetoAltair(arrDatos, ramo, nRegConsecutivo, strOrigen);
		} else if(strOrigen.equals(Constantes.CONCILIACION_HIPOTECARIO) || strOrigen.equals(Constantes.CONCILIACION_LCI) || strOrigen.equals(Constantes.CONCILIACION_GAP)){
			creaObjetoOtros(arrDatos, ramo, nRegConsecutivo, strOrigen);
		} else if(strOrigen.equals(Constantes.CONCILIACION_BT)){  /* APV*/
			creaObjetoBT(arrDatos, ramo, nRegConsecutivo, strOrigen);
		} /***APV**/
		
		setVdato13(strLinea);
		setFeCarga(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
		setEstatus("0");
		setArchivo(Constantes.CONCILIACION_NOMBRE_ARCHIVO);
	}

	public VtaPrecarga(VtaPrecargaId id, String strLinea, Date fechaIngreso) {
		setId(id);
		setFeIngreso(GestorFechas.formatDate(fechaIngreso, "dd/MM/yyyy"));
		setFeCarga(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
		setFeEstatus(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
		setVdato13(strLinea);
		setCargada(Constantes.CARGADA_ERROR);
		setEstatus("0");
		setCdError(Constantes.ERROR_001_ID);
		setDesError(Constantes.ERROR_001_DESC);
	}

	private void creaObjetoAltamira(String[] arrDatos, int ramo, int nRegConsecutivo, String strOrigen) {
		setId(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + arrDatos[7].toString(), arrDatos[5], strOrigen));
		setNdato1(new Short(arrDatos[7].toString()));
		setCuenta(arrDatos[0]);
		setNumCliente(arrDatos[1]);
		setApPaterno(arrDatos[2].trim().equals("") ? "X" : arrDatos[2].trim());
		setApMaterno(arrDatos[3].trim().equals("") ? "X" : arrDatos[3].trim());
		setNombre(arrDatos[4].trim());
		setFeIngreso(GestorFechas.formatDate(GestorFechas.generateDate(arrDatos[6], "dd-MM-yyyy"), "dd/MM/yyyy"));
		setRemesa(arrDatos[8]);
		setSumaAsegurada(arrDatos[9]);
		setPrima(arrDatos[10]);
		setVdato3(arrDatos[11]);
		setVdato4(arrDatos[12]);
		setVdato1(arrDatos[13]);
		setVdato2(arrDatos[14]);
		setRfc(arrDatos[15].trim().equals("") ? "XXXX100101" : arrDatos[15].trim());
		setFeNac(GestorFechas.generarFechaNacimiento(getRfc()));
		setCalle(arrDatos[16].trim().equals("") ? "X" : arrDatos[16].trim());
		setColonia(arrDatos[17].trim().equals("") ? "X" : arrDatos[17].trim());			
		setVdato7(arrDatos[18].trim().equals("") ? Constantes.ESTADO_BD_MEXICO : arrDatos[18].trim());
		setDelmunic(getVdato7());	
		setCp(Utilerias.validaCP(arrDatos[19].trim()));
		setFeEstatus(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
		setCargada(Constantes.CARGADA_PARA_CONCILIAR);
	}
	
	private void creaObjetoAltair(String[] arrDatos, int ramo, int nRegConsecutivo, String strOrigen) {
		setCuenta(arrDatos[0]);
		setId(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + "1", arrDatos[12], strOrigen));
		setNumCliente(arrDatos[1]);
		setApPaterno(arrDatos[2].trim().equals("") ? "X" : arrDatos[2].trim());
		setApMaterno(arrDatos[3].trim().equals("") ? "X" : arrDatos[3].trim());
		setNombre(arrDatos[4].trim());
		setRfc(arrDatos[5].trim().equals("") ? "XXXX100101" : arrDatos[5].trim());
		setFeNac(GestorFechas.formatDate(GestorFechas.generateDate(arrDatos[6], "yyyy-MM-dd"), "dd/MM/yyyy"));
		setCalle(arrDatos[7].trim().equals("") ? "X" : arrDatos[7].trim());
		setColonia(arrDatos[8].trim().equals("") ? "X" : arrDatos[8].trim());
		setDelmunic(arrDatos[9].trim());			
		setVdato7(arrDatos[10].trim().equals("") ? Constantes.ESTADO_BD_MEXICO : arrDatos[10].trim());
		setCp(Utilerias.validaCP(arrDatos[11].trim()));
		setFeIngreso(GestorFechas.formatDate(GestorFechas.generateDate(arrDatos[13], "yyyy-MM-dd"), "dd/MM/yyyy"));
		setVdato2(arrDatos[14]);
		setRemesa(arrDatos[15]);
		setSumaAsegurada(String.valueOf(new Double(arrDatos[16]) / 100));
		setPrima(String.valueOf(new Double(arrDatos[17]) / 100));
		setVdato3(arrDatos[18]);
		setVdato1(arrDatos[19]);
		setTipoVenta(arrDatos[20]);
		setDdato1(GestorFechas.generateDate(arrDatos[21], "yyyy-MM-dd"));
		setVdato4(arrDatos[22]);
		setDdato2(GestorFechas.generateDate(arrDatos[23], "yyyy-MM-dd"));
		setddato3(GestorFechas.generateDate(arrDatos[24], "yyyy-MM-dd"));
		setCargada(Constantes.CARGADA_PARA_CONCILIAR);
	}
	
	private void creaObjetoOtros(String[] arrDatos, int ramo, int nRegConsecutivo, String strOrigen) {
		 
		int inc = strOrigen.equals(Constantes.CONCILIACION_GAP)? 1:0;
		
		setId(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + "1", arrDatos[2], strOrigen));
		setCuenta(arrDatos[1]);
		setNumCliente(arrDatos[8]);			
		setApPaterno(arrDatos[3].trim().equals("") ? "X" : arrDatos[3].trim());
		setApMaterno(arrDatos[4].trim().equals("") ? "X" : arrDatos[4].trim());
		setNombre(arrDatos[5].trim());
		setFeNac(arrDatos[6]);
		setCalle(arrDatos[10].trim().equals("") ? "X" : arrDatos[10].trim());
		setColonia(arrDatos[11].trim().equals("") ? "X" : arrDatos[11].trim());
		setDelmunic(arrDatos[12].trim().equals("") ? "X" : arrDatos[12].trim());	
		setCdEstado(arrDatos[13]);
		setVdato7(getCdEstado());
		setFeIngreso(arrDatos[18]);
		setRemesa(arrDatos[25+inc]);
		setVdato5(getRemesa());
		setPrima(arrDatos[19] == null ? "0" : arrDatos[19].trim());
		setVdato3(arrDatos[26+inc].trim());
		setVdato4(arrDatos[27+inc].trim());
		setTipoVenta(arrDatos[28+inc].trim());
		setNdato1(new Short(arrDatos[17].toString()));		
		setLada(arrDatos[15]);
		setTelefono(arrDatos[16]);
		setNdato8(new Double(arrDatos[22+inc].trim()));
		
		if(strOrigen.equals(Constantes.CONCILIACION_HIPOTECARIO)) {
			setAnalista(String.valueOf(Constantes.TIPO_VENTA_HPU));
			setRfc(arrDatos[9].trim().equals("") ? "XXXX100101" : arrDatos[9].trim());
			setCp(Utilerias.validaCP(arrDatos[14].trim()));
			setSexo(arrDatos[7].trim().equals("") ? "MA" : arrDatos[7].trim());
			setCazbCdSucursal(arrDatos.length == 30 ? arrDatos[29].trim() : "0");
			setSumaAsegurada(arrDatos[20].trim());
			setVdato6(arrDatos[21].trim());
			setDdato1(GestorFechas.generateDate(arrDatos[23], "dd/MM/yyyy"));
			setDdato2(GestorFechas.generateDate(arrDatos[24], "dd/MM/yyyy"));
			setPregunta5("V0");
			setCargada(Constantes.CARGADA_PARA_PRECONCILIAR);
		} else if(strOrigen.equals(Constantes.CONCILIACION_LCI)) {
			setAnalista(String.valueOf(Constantes.TIPO_VENTA_BT));
			setRfc(arrDatos[9] == null ? "" : arrDatos[9].trim());
			setCp(Utilerias.validaCP(arrDatos[14]));
			setSexo(arrDatos[7]);
			setSumaAsegurada(arrDatos[21].trim());
			setVdato6(arrDatos[20].trim());
			try {
				setDdato1(GestorFechas.generateDate2(arrDatos[23], "dd/MM/yyyy"));
			} catch (ParseException e) {
				setDdato1(GestorFechas.generateDate("01/01/1900", "dd/MM/yyyy"));
			}
			try {
				setDdato2(GestorFechas.generateDate2(arrDatos[24], "dd/MM/yyyy"));
			} catch (ParseException e) {
				setDdato1(GestorFechas.generateDate("01/01/1900", "dd/MM/yyyy"));
			}
			setPregunta5("V0");
			setCargada(Constantes.CARGADA_PARA_CONCILIAR);
		} else if(strOrigen.equals(Constantes.CONCILIACION_GAP)) {
			setRfc(arrDatos[9].trim().equals("") ? "XXXX100101" : arrDatos[9].trim());
			setCp(Utilerias.validaCP(arrDatos[14].trim()));
			setSexo(arrDatos[7].trim().equals("") ? "MA" : arrDatos[7].trim());
			setNuContrato(arrDatos[0].trim());
			setTipoVenta(String.valueOf(Constantes.TIPO_VENTA_GAP));
			setAnalista(String.valueOf(Constantes.TIPO_VENTA_GAP));
			setSumaAsegurada(arrDatos[20].trim());
			setVdato6(arrDatos[22].trim());
			setApMaternoBen(arrDatos[21].trim());
			setDdato1(GestorFechas.generateDate(arrDatos[24], "dd/MM/yyyy"));
			setDdato2(GestorFechas.generateDate(arrDatos[25], "dd/MM/yyyy"));
			setPregunta5("D0");
			setCargada(Constantes.CARGADA_PARA_CONCILIAR);
		}
	}
	
	/*****APV********/
	private void creaObjetoBT(String[] arrDatos, int ramo, int nRegConsecutivo, String strOrigen) {
		setId(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + "1", arrDatos[0], strOrigen));
		setCuenta(arrDatos[1]);
		setCuentaAlterna(arrDatos [2]);//Plastico
		setApPaterno(arrDatos[3].trim().equals("") ? "X" : arrDatos[3].trim());
		setApMaterno(arrDatos[4].trim().equals("") ? "X" : arrDatos[4].trim());
		setNombre(arrDatos[5].trim());
		setSexo(arrDatos[6].trim().equals("") ? "MA" : arrDatos[6].trim());//preguntar el 
		setFeNac(GestorFechas.formatDate(GestorFechas.generateDate(arrDatos[7],"dd/MM/yyyy" ),"dd/MM/yyyy" ));
		setNumCliente(arrDatos[8]);			
		setRfc(arrDatos[9].trim().equals("") ? "XXXX100101" : arrDatos[9].trim());
		setCalle(arrDatos[10].trim().equals("") ? "X" : arrDatos[10].trim());
		setColonia(arrDatos[11].trim().equals("") ? "X" : arrDatos[11].trim());
		setDelmunic(arrDatos[12].trim().equals("") ? "X" : arrDatos[12].trim());	
		setCdEstado(arrDatos[13]);
		setVdato7(getCdEstado());
		setCp(Utilerias.validaCP(arrDatos[14].trim()));
		setFeIngreso(GestorFechas.formatDate(GestorFechas.generateDate(arrDatos[15],"dd/MM/yyyy" ),"dd/MM/yyyy" ));
		setPrima(String.valueOf(new Double(arrDatos[16])));
		setSumaAsegurada(String.valueOf(new Double(arrDatos[17])));
		setVdato6(arrDatos[18]);//Base calculo prima APV
		setNdato8(new Double(arrDatos[19].trim()));//Tarifa
		setVdato5(arrDatos[20]);//Plazo
		setVdato3(arrDatos[21]);//Producto
		setVdato4(arrDatos[22].trim());//Subproducto
		setTipoVenta(arrDatos[23]);//IdVenta
		setNdato2(Short.valueOf(arrDatos[24].toString())); //Cve_Seg
		setVdato10(arrDatos[25]);//Segmento
		setVdato11(arrDatos[26]);//Segto_Banco
		setVdato12(arrDatos[27]);//Mod_ATN
		setAnalista(arrDatos[28]);//Ejecutivo
		setNombreAseg(arrDatos[29]);//NM_Ejecutivo                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
		setVdato2(arrDatos[30]);//CAT_EJE
		setNdato9(new BigDecimal(arrDatos[31]));//Sucursal
		setRfcBen(arrDatos[32]);//Suc_virtual
		setNdato6(new Integer (arrDatos[33]));//CVE_RG
		setNombreBen(arrDatos[34]);//Region
		setNdato7(new Integer (arrDatos[35]));//Cve_zn
		setRegistro(arrDatos[36]);//Zona
		setCargada(Constantes.CARGADA_PARA_PRECONCILIAR);
		setDdato2(new Date());// Fecha de envio de Archivo PAMPA , solo lo tengo en las vtas, preguntar si a colectivos certificados.
	}
	/*****End APV********/
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "cdSucursal", column = @Column(name = "VTAP_CD_SUCURSAL", nullable = false, precision = 3, scale = 0)),
			@AttributeOverride(name = "cdRamo", column = @Column(name = "VTAP_CD_RAMO", nullable = false, precision = 2, scale = 0)),
			@AttributeOverride(name = "numPoliza", column = @Column(name = "VTAP_NUM_POLIZA", nullable = false, precision = 10, scale = 0)),
			@AttributeOverride(name = "idCertificado", column = @Column(name = "VTAP_ID_CERTIFICADO", nullable = false, length = 50)),
			@AttributeOverride(name = "sistemaOrigen", column = @Column(name = "VTAP_SISTEMA_ORIGEN", nullable = false, length = 10)) })
	public VtaPrecargaId getId() {
		return this.id;
	}
	public void setId(VtaPrecargaId id) {
		this.id = id;
	}

	@Column(name = "VTAP_CARGADA", nullable = false, length = 20)
	public String getCargada() {
		return this.cargada;
	}
	public void setCargada(String cargada) {
		this.cargada = cargada;
	}

	@Column(name = "VTAP_ESTATUS", nullable = false, length = 20)
	public String getEstatus() {
		return this.estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	@Column(name = "VTAP_FE_CARGA", length = 20)
	public String getFeCarga() {
		return this.feCarga;
	}
	public void setFeCarga(String feCarga) {
		this.feCarga = feCarga;
	}

	@Column(name = "VTAP_FE_INGRESO", length = 20)
	public String getFeIngreso() {
		return this.feIngreso;
	}
	public void setFeIngreso(String feIngreso) {
		this.feIngreso = feIngreso;
	}

	@Column(name = "VTAP_FE_ESTATUS", length = 20)
	public String getFeEstatus() {
		return this.feEstatus;
	}
	public void setFeEstatus(String feEstatus) {
		this.feEstatus = feEstatus;
	}

	@Column(name = "VTAP_FE_APROBACION", length = 20)
	public String getFeAprobacion() {
		return this.feAprobacion;
	}
	public void setFeAprobacion(String feAprobacion) {
		this.feAprobacion = feAprobacion;
	}

	@Column(name = "VTAP_REMESA", length = 10)
	public String getRemesa() {
		return this.remesa;
	}
	public void setRemesa(String remesa) {
		this.remesa = remesa;
	}

	@Column(name = "VTAP_CAZB_CD_SUCURSAL", length = 20)
	public String getCazbCdSucursal() {
		return this.cazbCdSucursal;
	}
	public void setCazbCdSucursal(String cazbCdSucursal) {
		this.cazbCdSucursal = cazbCdSucursal;
	}

	@Column(name = "VTAP_ANALISTA", length = 10)
	public String getAnalista() {
		return this.analista;
	}
	public void setAnalista(String analista) {
		this.analista = analista;
	}

	@Column(name = "VTAP_PRIMA", length = 20)
	public String getPrima() {
		return this.prima;
	}
	public void setPrima(String prima) {
		this.prima = prima;
	}

	@Column(name = "VTAP_SUMA_ASEGURADA", length = 20)
	public String getSumaAsegurada() {
		return this.sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	@Column(name = "VTAP_CD_PRODUCTO", length = 6)
	public String getCdProducto() {
		return this.cdProducto;
	}
	public void setCdProducto(String cdProducto) {
		this.cdProducto = cdProducto;
	}

	@Column(name = "VTAP_CD_PLAN", length = 6)
	public String getCdPlan() {
		return this.cdPlan;
	}
	public void setCdPlan(String cdPlan) {
		this.cdPlan = cdPlan;
	}

	@Column(name = "VTAP_FR_PAGO", length = 1)
	public String getFrPago() {
		return this.frPago;
	}
	public void setFrPago(String frPago) {
		this.frPago = frPago;
	}

	@Column(name = "VTAP_TIPO_VENTA", length = 5)
	public String getTipoVenta() {
		return this.tipoVenta;
	}
	public void setTipoVenta(String tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

	@Column(name = "VTAP_NUM_CLIENTE", length = 15)
	public String getNumCliente() {
		return this.numCliente;
	}
	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}

	@Column(name = "VTAP_AP_PATERNO", length = 60)
	public String getApPaterno() {
		return this.apPaterno;
	}
	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	@Column(name = "VTAP_AP_MATERNO", length = 60)
	public String getApMaterno() {
		return this.apMaterno;
	}
	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	@Column(name = "VTAP_NOMBRE", length = 60)
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "VTAP_RFC", length = 20)
	public String getRfc() {
		return this.rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Column(name = "VTAP_FE_NAC", length = 20)
	public String getFeNac() {
		return this.feNac;
	}

	public void setFeNac(String feNac) {
		this.feNac = feNac;
	}

	@Column(name = "VTAP_SEXO", length = 2)
	public String getSexo() {
		return this.sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	@Column(name = "VTAP_CUENTA", length = 20)
	public String getCuenta() {
		return this.cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	@Column(name = "VTAP_CONDUCTO", length = 5)
	public String getConducto() {
		return this.conducto;
	}
	public void setConducto(String conducto) {
		this.conducto = conducto;
	}

	@Column(name = "VTAP_BANCO", length = 5)
	public String getBanco() {
		return this.banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}

	@Column(name = "VTAP_CUENTA_ALTERNA", length = 20)
	public String getCuentaAlterna() {
		return this.cuentaAlterna;
	}
	public void setCuentaAlterna(String cuentaAlterna) {
		this.cuentaAlterna = cuentaAlterna;
	}

	@Column(name = "VTAP_CONDUCTO_ALTERNA", length = 5)
	public String getConductoAlterna() {
		return this.conductoAlterna;
	}
	public void setConductoAlterna(String conductoAlterna) {
		this.conductoAlterna = conductoAlterna;
	}

	@Column(name = "VTAP_BANCO_ALTERNA", length = 5)
	public String getBancoAlterna() {
		return this.bancoAlterna;
	}
	public void setBancoAlterna(String bancoAlterna) {
		this.bancoAlterna = bancoAlterna;
	}

	@Column(name = "VTAP_CALLE", length = 150)
	public String getCalle() {
		return this.calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}

	@Column(name = "VTAP_COLONIA", length = 55)
	public String getColonia() {
		return this.colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	@Column(name = "VTAP_DELMUNIC", length = 50)
	public String getDelmunic() {
		return this.delmunic;
	}
	public void setDelmunic(String delmunic) {
		this.delmunic = delmunic;
	}

	@Column(name = "VTAP_CD_ESTADO", length = 10)
	public String getCdEstado() {
		return this.cdEstado;
	}
	public void setCdEstado(String cdEstado) {
		this.cdEstado = cdEstado;
	}

	@Column(name = "VTAP_CP", length = 5)
	public String getCp() {
		return this.cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}

	@Column(name = "VTAP_LADA", length = 10)
	public String getLada() {
		return this.lada;
	}
	public void setLada(String lada) {
		this.lada = lada;
	}

	@Column(name = "VTAP_TELEFONO", length = 20)
	public String getTelefono() {
		return this.telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name = "VTAP_NU_CONTRATO", length = 20)
	public String getNuContrato() {
		return this.nuContrato;
	}
	public void setNuContrato(String nuContrato) {
		this.nuContrato = nuContrato;
	}

	@Column(name = "VTAP_AP_PATERNO_ASEG", length = 200)
	public String getApPaternoAseg() {
		return this.apPaternoAseg;
	}
	public void setApPaternoAseg(String apPaternoAseg) {
		this.apPaternoAseg = apPaternoAseg;
	}

	@Column(name = "VTAP_AP_MATERNO_ASEG", length = 200)
	public String getApMaternoAseg() {
		return this.apMaternoAseg;
	}
	public void setApMaternoAseg(String apMaternoAseg) {
		this.apMaternoAseg = apMaternoAseg;
	}

	@Column(name = "VTAP_NOMBRE_ASEG", length = 200)
	public String getNombreAseg() {
		return this.nombreAseg;
	}
	public void setNombreAseg(String nombreAseg) {
		this.nombreAseg = nombreAseg;
	}

	@Column(name = "VTAP_RFC_ASEG", length = 200)
	public String getRfcAseg() {
		return this.rfcAseg;
	}
	public void setRfcAseg(String rfcAseg) {
		this.rfcAseg = rfcAseg;
	}

	@Column(name = "VTAP_FE_NAC_ASEG", length = 200)
	public String getFeNacAseg() {
		return this.feNacAseg;
	}
	public void setFeNacAseg(String feNacAseg) {
		this.feNacAseg = feNacAseg;
	}

	@Column(name = "VTAP_SEXO_ASEG", length = 20)
	public String getSexoAseg() {
		return this.sexoAseg;
	}
	public void setSexoAseg(String sexoAseg) {
		this.sexoAseg = sexoAseg;
	}

	@Column(name = "VTAP_EDO_CIVIL_ASEG", length = 20)
	public String getEdoCivilAseg() {
		return this.edoCivilAseg;
	}
	public void setEdoCivilAseg(String edoCivilAseg) {
		this.edoCivilAseg = edoCivilAseg;
	}

	@Column(name = "VTAP_OCUPACION_ASEG", length = 20)
	public String getOcupacionAseg() {
		return this.ocupacionAseg;
	}
	public void setOcupacionAseg(String ocupacionAseg) {
		this.ocupacionAseg = ocupacionAseg;
	}

	@Column(name = "VTAP_RELACION_ASEG", length = 20)
	public String getRelacionAseg() {
		return this.relacionAseg;
	}
	public void setRelacionAseg(String relacionAseg) {
		this.relacionAseg = relacionAseg;
	}

	@Column(name = "VTAP_AP_PATERNO_BEN", length = 200)
	public String getApPaternoBen() {
		return this.apPaternoBen;
	}
	public void setApPaternoBen(String apPaternoBen) {
		this.apPaternoBen = apPaternoBen;
	}

	@Column(name = "VTAP_AP_MATERNO_BEN", length = 200)
	public String getApMaternoBen() {
		return this.apMaternoBen;
	}
	public void setApMaternoBen(String apMaternoBen) {
		this.apMaternoBen = apMaternoBen;
	}

	@Column(name = "VTAP_NOMBRE_BEN", length = 200)
	public String getNombreBen() {
		return this.nombreBen;
	}
	public void setNombreBen(String nombreBen) {
		this.nombreBen = nombreBen;
	}

	@Column(name = "VTAP_RFC_BEN", length = 200)
	public String getRfcBen() {
		return this.rfcBen;
	}
	public void setRfcBen(String rfcBen) {
		this.rfcBen = rfcBen;
	}

	@Column(name = "VTAP_FE_NAC_BEN", length = 200)
	public String getFeNacBen() {
		return this.feNacBen;
	}
	public void setFeNacBen(String feNacBen) {
		this.feNacBen = feNacBen;
	}

	@Column(name = "VTAP_SEXO_BEN", length = 20)
	public String getSexoBen() {
		return this.sexoBen;
	}
	public void setSexoBen(String sexoBen) {
		this.sexoBen = sexoBen;
	}

	@Column(name = "VTAP_RELACION_BEN", length = 20)
	public String getRelacionBen() {
		return this.relacionBen;
	}
	public void setRelacionBen(String relacionBen) {
		this.relacionBen = relacionBen;
	}

	@Column(name = "VTAP_PREGUNTA_1", length = 5)
	public String getPregunta1() {
		return this.pregunta1;
	}
	public void setPregunta1(String pregunta1) {
		this.pregunta1 = pregunta1;
	}

	@Column(name = "VTAP_PREGUNTA_2", length = 5)
	public String getPregunta2() {
		return this.pregunta2;
	}
	public void setPregunta2(String pregunta2) {
		this.pregunta2 = pregunta2;
	}

	@Column(name = "VTAP_PREGUNTA_3", length = 5)
	public String getPregunta3() {
		return this.pregunta3;
	}
	public void setPregunta3(String pregunta3) {
		this.pregunta3 = pregunta3;
	}

	@Column(name = "VTAP_PREGUNTA_4", length = 5)
	public String getPregunta4() {
		return this.pregunta4;
	}
	public void setPregunta4(String pregunta4) {
		this.pregunta4 = pregunta4;
	}

	@Column(name = "VTAP_PREGUNTA_5", length = 5)
	public String getPregunta5() {
		return this.pregunta5;
	}
	public void setPregunta5(String pregunta5) {
		this.pregunta5 = pregunta5;
	}

	@Column(name = "VTAP_PREGUNTA_6", length = 5)
	public String getPregunta6() {
		return this.pregunta6;
	}
	public void setPregunta6(String pregunta6) {
		this.pregunta6 = pregunta6;
	}

	@Column(name = "VTAP_REGISTRO", length = 2000)
	public String getRegistro() {
		return this.registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}

	@Column(name = "VTAP_CD_ERROR", length = 20)
	public String getCdError() {
		return this.cdError;
	}
	public void setCdError(String cdError) {
		this.cdError = cdError;
	}

	@Column(name = "VTAP_DES_ERROR", length = 500)
	public String getDesError() {
		return this.desError;
	}
	public void setDesError(String desError) {
		this.desError = desError;
	}

	@Column(name = "VTAP_ARCHIVO", length = 2000)
	public String getArchivo() {
		return this.archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	@Column(name = "VTAP_USUARIO", length = 20)
	public String getUsuario() {
		return this.usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(name = "VTAP_NDATO1", precision = 3, scale = 0)
	public Short getNdato1() {
		return this.ndato1;
	}
	public void setNdato1(Short ndato1) {
		this.ndato1 = ndato1;
	}

	@Column(name = "VTAP_NDATO2", precision = 3, scale = 0)
	public Short getNdato2() {
		return this.ndato2;
	}
	public void setNdato2(Short ndato2) {
		this.ndato2 = ndato2;
	}

	@Column(name = "VTAP_NDATO3", precision = 3, scale = 0)
	public Short getNdato3() {
		return this.ndato3;
	}
	public void setNdato3(Short ndato3) {
		this.ndato3 = ndato3;
	}

	@Column(name = "VTAP_NDATO4", precision = 5, scale = 0)
	public Integer getNdato4() {
		return this.ndato4;
	}
	public void setNdato4(Integer ndato4) {
		this.ndato4 = ndato4;
	}

	@Column(name = "VTAP_NDATO5", precision = 5, scale = 0)
	public Integer getNdato5() {
		return this.ndato5;
	}
	public void setNdato5(Integer ndato5) {
		this.ndato5 = ndato5;
	}

	@Column(name = "VTAP_NDATO6", precision = 7, scale = 0)
	public Integer getNdato6() {
		return this.ndato6;
	}
	public void setNdato6(Integer ndato6) {
		this.ndato6 = ndato6;
	}

	@Column(name = "VTAP_NDATO7", precision = 7, scale = 0)
	public Integer getNdato7() {
		return this.ndato7;
	}
	public void setNdato7(Integer ndato7) {
		this.ndato7 = ndato7;
	}

	@Column(name = "VTAP_NDATO8", precision = 10, scale = 4)
	public Double getNdato8() {
		return this.ndato8;
	}
	public void setNdato8(Double ndato8) {
		this.ndato8 = ndato8;
	}

	@Column(name = "VTAP_NDATO9", precision = 10, scale = 4)
	public BigDecimal getNdato9() {
		return this.ndato9;
	}
	public void setNdato9(BigDecimal ndato9) {
		this.ndato9 = ndato9;
	}
	
	@Column(name = "VTAP_VDATO1", length = 10)
	public String getVdato1() {
		return this.vdato1;
	}
	public void setVdato1(String vdato1) {
		this.vdato1 = vdato1;
	}

	@Column(name = "VTAP_VDATO2", length = 10)
	public String getVdato2() {
		return this.vdato2;
	}
	public void setVdato2(String vdato2) {
		this.vdato2 = vdato2;
	}

	@Column(name = "VTAP_VDATO3", length = 20)
	public String getVdato3() {
		return this.vdato3;
	}
	public void setVdato3(String vdato3) {
		this.vdato3 = vdato3;
	}

	@Column(name = "VTAP_VDATO4", length = 20)
	public String getVdato4() {
		return this.vdato4;
	}
	public void setVdato4(String vdato4) {
		this.vdato4 = vdato4;
	}

	@Column(name = "VTAP_VDATO5", length = 50)
	public String getVdato5() {
		return this.vdato5;
	}
	public void setVdato5(String vdato5) {
		this.vdato5 = vdato5;
	}

	@Column(name = "VTAP_VDATO6", length = 50)
	public String getVdato6() {
		return this.vdato6;
	}
	public void setVdato6(String vdato6) {
		this.vdato6 = vdato6;
	}

	@Column(name = "VTAP_VDATO7", length = 100)
	public String getVdato7() {
		return this.vdato7;
	}
	public void setVdato7(String vdato7) {
		this.vdato7 = vdato7;
	}

	@Column(name = "VTAP_VDATO8", length = 100)
	public String getVdato8() {
		return this.vdato8;
	}
	public void setVdato8(String vdato8) {
		this.vdato8 = vdato8;
	}

	@Column(name = "VTAP_VDATO9", length = 200)
	public String getVdato9() {
		return this.vdato9;
	}
	public void setVdato9(String vdato9) {
		this.vdato9 = vdato9;
	}

	@Column(name = "VTAP_VDATO10", length = 200)
	public String getVdato10() {
		return this.vdato10;
	}
	public void setVdato10(String vdato10) {
		this.vdato10 = vdato10;
	}

	@Column(name = "VTAP_VDATO11", length = 500)
	public String getVdato11() {
		return this.vdato11;
	}
	public void setVdato11(String vdato11) {
		this.vdato11 = vdato11;
	}

	@Column(name = "VTAP_VDATO12", length = 1000)
	public String getVdato12() {
		return this.vdato12;
	}
	public void setVdato12(String vdato12) {
		this.vdato12 = vdato12;
	}

	@Column(name = "VTAP_VDATO13", length = 2000)
	public String getVdato13() {
		return this.vdato13;
	}
	public void setVdato13(String vdato13) {
		this.vdato13 = vdato13;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "VTAP_DDATO1", length = 7)
	public Date getDdato1() {
		return this.ddato1;
	}
	public void setDdato1(Date ddato1) {
		this.ddato1 = ddato1;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "VTAP_DDATO2", length = 7)
	public Date getDdato2() {
		return this.ddato2;
	}
	public void setDdato2(Date ddato2) {
		this.ddato2 = ddato2;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "VTAP_DDATO3", length = 7)
	public Date getDdato3() {
		return this.ddato3;
	}
	public void setddato3(Date ddato3) {
		this.ddato3 = ddato3;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		
		if(obj == null){
			return false;
		}
		
		VtaPrecarga precarga = (VtaPrecarga) obj;
		if(this.id.equals(precarga.id)){
			return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.getId().getIdCertificado().hashCode();
	}

}
