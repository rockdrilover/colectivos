package mx.com.santander.aseguradora.colectivos.model.bussinessObject;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TraspasoColectivos generated by hbm2java
 */
@Entity
@Table(name = "TRASPASO_COLECTIVOS")
public class TraspasoColectivos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8124462714636776674L;
	private TraspasoColectivosId id;
	private String trseRegistro;
	private Boolean trseCargada;
	private Date trseFechaCarga;
	private String trseNomBase;
	private Short trseTipoProducto;
	private Integer trseSerie;
	private Long trseFolioInteligente;
	private String trseUsuario;
	private String trseRemesa;
	private short trseCampana;
	private String trsePago;
	private Date trseFechaPago;
	private String trseAptc;
	private Integer trseProveedor;
	private Date trseFechaVenta;
	private String trseCuenta;
	private short trseSubCampana;
	private BigDecimal trseNuRecibo;
	private String trseProgramado;
	private String trseNombre;
	private String trseAp;
	private String trseAm;
	private String trseDireccion;
	private String trseColonia;
	private String trseBuc;
	private String trseNuPrestamo;
	private String trseBanderaAlta;
	private Integer trsePlazo;
	private String trseDato36;
	private String trseDato37;
	private String trseDato38;
	private String trseDato39;
	private String trseDato40;
	private String trseDato41;
	private String trseDato42;
	private String trseDato43;
	private String trseDato44;
	private String trseDato45;
	private String trseDato46;
	private String trseDato47;
	private String trseDato48;
	private String trseDato49;
	private String trseDato50;
	private BigDecimal trseDato51;
	private BigDecimal trseDato52;
	private BigDecimal trseDato53;
	private Long trseDato54;
	private Long trseDato55;
	private Integer trseDato56;
	private Date trseDato57;
	private Date trseDato58;
	private Date trseDato59;

	public TraspasoColectivos() {
	}

	public TraspasoColectivos(TraspasoColectivosId id, short trseCampana,
			short trseSubCampana) {
		this.id = id;
		this.trseCampana = trseCampana;
		this.trseSubCampana = trseSubCampana;
	}

	public TraspasoColectivos(TraspasoColectivosId id, String trseRegistro,
			Boolean trseCargada, Date trseFechaCarga, String trseNomBase,
			Short trseTipoProducto, Integer trseSerie,
			Long trseFolioInteligente, String trseUsuario, String trseRemesa,
			short trseCampana, String trsePago, Date trseFechaPago,
			String trseAptc, Integer trseProveedor, Date trseFechaVenta,
			String trseCuenta, short trseSubCampana, BigDecimal trseNuRecibo,
			String trseProgramado, String trseNombre, String trseAp,
			String trseAm, String trseDireccion, String trseColonia,
			String trseBuc, String trseNuPrestamo, String trseBanderaAlta,
			Integer trsePlazo, String trseDato36, String trseDato37,
			String trseDato38, String trseDato39, String trseDato40,
			String trseDato41, String trseDato42, String trseDato43,
			String trseDato44, String trseDato45, String trseDato46,
			String trseDato47, String trseDato48, String trseDato49,
			String trseDato50, BigDecimal trseDato51, BigDecimal trseDato52,
			BigDecimal trseDato53, Long trseDato54, Long trseDato55,
			Integer trseDato56, Date trseDato57, Date trseDato58,
			Date trseDato59) {
		this.id = id;
		this.trseRegistro = trseRegistro;
		this.trseCargada = trseCargada;
		this.trseFechaCarga = trseFechaCarga;
		this.trseNomBase = trseNomBase;
		this.trseTipoProducto = trseTipoProducto;
		this.trseSerie = trseSerie;
		this.trseFolioInteligente = trseFolioInteligente;
		this.trseUsuario = trseUsuario;
		this.trseRemesa = trseRemesa;
		this.trseCampana = trseCampana;
		this.trsePago = trsePago;
		this.trseFechaPago = trseFechaPago;
		this.trseAptc = trseAptc;
		this.trseProveedor = trseProveedor;
		this.trseFechaVenta = trseFechaVenta;
		this.trseCuenta = trseCuenta;
		this.trseSubCampana = trseSubCampana;
		this.trseNuRecibo = trseNuRecibo;
		this.trseProgramado = trseProgramado;
		this.trseNombre = trseNombre;
		this.trseAp = trseAp;
		this.trseAm = trseAm;
		this.trseDireccion = trseDireccion;
		this.trseColonia = trseColonia;
		this.trseBuc = trseBuc;
		this.trseNuPrestamo = trseNuPrestamo;
		this.trseBanderaAlta = trseBanderaAlta;
		this.trsePlazo = trsePlazo;
		this.trseDato36 = trseDato36;
		this.trseDato37 = trseDato37;
		this.trseDato38 = trseDato38;
		this.trseDato39 = trseDato39;
		this.trseDato40 = trseDato40;
		this.trseDato41 = trseDato41;
		this.trseDato42 = trseDato42;
		this.trseDato43 = trseDato43;
		this.trseDato44 = trseDato44;
		this.trseDato45 = trseDato45;
		this.trseDato46 = trseDato46;
		this.trseDato47 = trseDato47;
		this.trseDato48 = trseDato48;
		this.trseDato49 = trseDato49;
		this.trseDato50 = trseDato50;
		this.trseDato51 = trseDato51;
		this.trseDato52 = trseDato52;
		this.trseDato53 = trseDato53;
		this.trseDato54 = trseDato54;
		this.trseDato55 = trseDato55;
		this.trseDato56 = trseDato56;
		this.trseDato57 = trseDato57;
		this.trseDato58 = trseDato58;
		this.trseDato59 = trseDato59;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "trseSucursal", column = @Column(name = "TRSE_SUCURSAL", nullable = false, precision = 4, scale = 0)),
			@AttributeOverride(name = "trseClaveProducto", column = @Column(name = "TRSE_CLAVE_PRODUCTO", nullable = false, precision = 3, scale = 0)),
			@AttributeOverride(name = "trseNumeroPoliza", column = @Column(name = "TRSE_NUMERO_POLIZA", nullable = false, precision = 9, scale = 0)),
			@AttributeOverride(name = "trseTipoRegistro", column = @Column(name = "TRSE_TIPO_REGISTRO", nullable = false, length = 2)),
			@AttributeOverride(name = "trseNumeroPolizaCol", column = @Column(name = "TRSE_NUMERO_POLIZA_COL", nullable = false, precision = 15, scale = 0)),
			@AttributeOverride(name = "trseNumeroSolicitud", column = @Column(name = "TRSE_NUMERO_SOLICITUD", nullable = false, precision = 10, scale = 0)) })
	public TraspasoColectivosId getId() {
		return this.id;
	}

	public void setId(TraspasoColectivosId id) {
		this.id = id;
	}

	@Column(name = "TRSE_REGISTRO", length = 2000)
	public String getTrseRegistro() {
		return this.trseRegistro;
	}

	public void setTrseRegistro(String trseRegistro) {
		this.trseRegistro = trseRegistro;
	}

	@Column(name = "TRSE_CARGADA", precision = 1, scale = 0)
	public Boolean getTrseCargada() {
		return this.trseCargada;
	}

	public void setTrseCargada(Boolean trseCargada) {
		this.trseCargada = trseCargada;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRSE_FECHA_CARGA", length = 7)
	public Date getTrseFechaCarga() {
		return this.trseFechaCarga;
	}

	public void setTrseFechaCarga(Date trseFechaCarga) {
		this.trseFechaCarga = trseFechaCarga;
	}

	@Column(name = "TRSE_NOM_BASE", length = 80)
	public String getTrseNomBase() {
		return this.trseNomBase;
	}

	public void setTrseNomBase(String trseNomBase) {
		this.trseNomBase = trseNomBase;
	}

	@Column(name = "TRSE_TIPO_PRODUCTO", precision = 3, scale = 0)
	public Short getTrseTipoProducto() {
		return this.trseTipoProducto;
	}

	public void setTrseTipoProducto(Short trseTipoProducto) {
		this.trseTipoProducto = trseTipoProducto;
	}

	@Column(name = "TRSE_SERIE", precision = 5, scale = 0)
	public Integer getTrseSerie() {
		return this.trseSerie;
	}

	public void setTrseSerie(Integer trseSerie) {
		this.trseSerie = trseSerie;
	}

	@Column(name = "TRSE_FOLIO_INTELIGENTE", precision = 15, scale = 0)
	public Long getTrseFolioInteligente() {
		return this.trseFolioInteligente;
	}

	public void setTrseFolioInteligente(Long trseFolioInteligente) {
		this.trseFolioInteligente = trseFolioInteligente;
	}

	@Column(name = "TRSE_USUARIO", length = 15)
	public String getTrseUsuario() {
		return this.trseUsuario;
	}

	public void setTrseUsuario(String trseUsuario) {
		this.trseUsuario = trseUsuario;
	}

	@Column(name = "TRSE_REMESA", length = 15)
	public String getTrseRemesa() {
		return this.trseRemesa;
	}

	public void setTrseRemesa(String trseRemesa) {
		this.trseRemesa = trseRemesa;
	}

	@Column(name = "TRSE_CAMPANA", nullable = false, precision = 4, scale = 0)
	public short getTrseCampana() {
		return this.trseCampana;
	}

	public void setTrseCampana(short trseCampana) {
		this.trseCampana = trseCampana;
	}

	@Column(name = "TRSE_PAGO", length = 1)
	public String getTrsePago() {
		return this.trsePago;
	}

	public void setTrsePago(String trsePago) {
		this.trsePago = trsePago;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRSE_FECHA_PAGO", length = 7)
	public Date getTrseFechaPago() {
		return this.trseFechaPago;
	}

	public void setTrseFechaPago(Date trseFechaPago) {
		this.trseFechaPago = trseFechaPago;
	}

	@Column(name = "TRSE_APTC", length = 2)
	public String getTrseAptc() {
		return this.trseAptc;
	}

	public void setTrseAptc(String trseAptc) {
		this.trseAptc = trseAptc;
	}

	@Column(name = "TRSE_PROVEEDOR", precision = 5, scale = 0)
	public Integer getTrseProveedor() {
		return this.trseProveedor;
	}

	public void setTrseProveedor(Integer trseProveedor) {
		this.trseProveedor = trseProveedor;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRSE_FECHA_VENTA", length = 7)
	public Date getTrseFechaVenta() {
		return this.trseFechaVenta;
	}

	public void setTrseFechaVenta(Date trseFechaVenta) {
		this.trseFechaVenta = trseFechaVenta;
	}

	@Column(name = "TRSE_CUENTA", length = 20)
	public String getTrseCuenta() {
		return this.trseCuenta;
	}

	public void setTrseCuenta(String trseCuenta) {
		this.trseCuenta = trseCuenta;
	}

	@Column(name = "TRSE_SUB_CAMPANA", nullable = false, precision = 4, scale = 0)
	public short getTrseSubCampana() {
		return this.trseSubCampana;
	}

	public void setTrseSubCampana(short trseSubCampana) {
		this.trseSubCampana = trseSubCampana;
	}

	@Column(name = "TRSE_NU_RECIBO", precision = 13, scale = 1)
	public BigDecimal getTrseNuRecibo() {
		return this.trseNuRecibo;
	}

	public void setTrseNuRecibo(BigDecimal trseNuRecibo) {
		this.trseNuRecibo = trseNuRecibo;
	}

	@Column(name = "TRSE_PROGRAMADO", length = 1)
	public String getTrseProgramado() {
		return this.trseProgramado;
	}

	public void setTrseProgramado(String trseProgramado) {
		this.trseProgramado = trseProgramado;
	}

	@Column(name = "TRSE_NOMBRE", length = 80)
	public String getTrseNombre() {
		return this.trseNombre;
	}

	public void setTrseNombre(String trseNombre) {
		this.trseNombre = trseNombre;
	}

	@Column(name = "TRSE_AP", length = 80)
	public String getTrseAp() {
		return this.trseAp;
	}

	public void setTrseAp(String trseAp) {
		this.trseAp = trseAp;
	}

	@Column(name = "TRSE_AM", length = 80)
	public String getTrseAm() {
		return this.trseAm;
	}

	public void setTrseAm(String trseAm) {
		this.trseAm = trseAm;
	}

	@Column(name = "TRSE_DIRECCION", length = 80)
	public String getTrseDireccion() {
		return this.trseDireccion;
	}

	public void setTrseDireccion(String trseDireccion) {
		this.trseDireccion = trseDireccion;
	}

	@Column(name = "TRSE_COLONIA", length = 80)
	public String getTrseColonia() {
		return this.trseColonia;
	}

	public void setTrseColonia(String trseColonia) {
		this.trseColonia = trseColonia;
	}

	@Column(name = "TRSE_BUC", length = 10)
	public String getTrseBuc() {
		return this.trseBuc;
	}

	public void setTrseBuc(String trseBuc) {
		this.trseBuc = trseBuc;
	}

	@Column(name = "TRSE_NU_PRESTAMO", length = 25)
	public String getTrseNuPrestamo() {
		return this.trseNuPrestamo;
	}

	public void setTrseNuPrestamo(String trseNuPrestamo) {
		this.trseNuPrestamo = trseNuPrestamo;
	}

	@Column(name = "TRSE_BANDERA_ALTA", length = 5)
	public String getTrseBanderaAlta() {
		return this.trseBanderaAlta;
	}

	public void setTrseBanderaAlta(String trseBanderaAlta) {
		this.trseBanderaAlta = trseBanderaAlta;
	}

	@Column(name = "TRSE_PLAZO", precision = 5, scale = 0)
	public Integer getTrsePlazo() {
		return this.trsePlazo;
	}

	public void setTrsePlazo(Integer trsePlazo) {
		this.trsePlazo = trsePlazo;
	}

	@Column(name = "TRSE_DATO36", length = 50)
	public String getTrseDato36() {
		return this.trseDato36;
	}

	public void setTrseDato36(String trseDato36) {
		this.trseDato36 = trseDato36;
	}

	@Column(name = "TRSE_DATO37", length = 50)
	public String getTrseDato37() {
		return this.trseDato37;
	}

	public void setTrseDato37(String trseDato37) {
		this.trseDato37 = trseDato37;
	}

	@Column(name = "TRSE_DATO38", length = 50)
	public String getTrseDato38() {
		return this.trseDato38;
	}

	public void setTrseDato38(String trseDato38) {
		this.trseDato38 = trseDato38;
	}

	@Column(name = "TRSE_DATO39", length = 50)
	public String getTrseDato39() {
		return this.trseDato39;
	}

	public void setTrseDato39(String trseDato39) {
		this.trseDato39 = trseDato39;
	}

	@Column(name = "TRSE_DATO40", length = 50)
	public String getTrseDato40() {
		return this.trseDato40;
	}

	public void setTrseDato40(String trseDato40) {
		this.trseDato40 = trseDato40;
	}

	@Column(name = "TRSE_DATO41", length = 50)
	public String getTrseDato41() {
		return this.trseDato41;
	}

	public void setTrseDato41(String trseDato41) {
		this.trseDato41 = trseDato41;
	}

	@Column(name = "TRSE_DATO42", length = 50)
	public String getTrseDato42() {
		return this.trseDato42;
	}

	public void setTrseDato42(String trseDato42) {
		this.trseDato42 = trseDato42;
	}

	@Column(name = "TRSE_DATO43", length = 50)
	public String getTrseDato43() {
		return this.trseDato43;
	}

	public void setTrseDato43(String trseDato43) {
		this.trseDato43 = trseDato43;
	}

	@Column(name = "TRSE_DATO44", length = 50)
	public String getTrseDato44() {
		return this.trseDato44;
	}

	public void setTrseDato44(String trseDato44) {
		this.trseDato44 = trseDato44;
	}

	@Column(name = "TRSE_DATO45", length = 50)
	public String getTrseDato45() {
		return this.trseDato45;
	}

	public void setTrseDato45(String trseDato45) {
		this.trseDato45 = trseDato45;
	}

	@Column(name = "TRSE_DATO46", length = 50)
	public String getTrseDato46() {
		return this.trseDato46;
	}

	public void setTrseDato46(String trseDato46) {
		this.trseDato46 = trseDato46;
	}

	@Column(name = "TRSE_DATO47", length = 50)
	public String getTrseDato47() {
		return this.trseDato47;
	}

	public void setTrseDato47(String trseDato47) {
		this.trseDato47 = trseDato47;
	}

	@Column(name = "TRSE_DATO48", length = 100)
	public String getTrseDato48() {
		return this.trseDato48;
	}

	public void setTrseDato48(String trseDato48) {
		this.trseDato48 = trseDato48;
	}

	@Column(name = "TRSE_DATO49", length = 200)
	public String getTrseDato49() {
		return this.trseDato49;
	}

	public void setTrseDato49(String trseDato49) {
		this.trseDato49 = trseDato49;
	}

	@Column(name = "TRSE_DATO50", length = 500)
	public String getTrseDato50() {
		return this.trseDato50;
	}

	public void setTrseDato50(String trseDato50) {
		this.trseDato50 = trseDato50;
	}

	@Column(name = "TRSE_DATO51", precision = 16)
	public BigDecimal getTrseDato51() {
		return this.trseDato51;
	}

	public void setTrseDato51(BigDecimal trseDato51) {
		this.trseDato51 = trseDato51;
	}

	@Column(name = "TRSE_DATO52", precision = 16)
	public BigDecimal getTrseDato52() {
		return this.trseDato52;
	}

	public void setTrseDato52(BigDecimal trseDato52) {
		this.trseDato52 = trseDato52;
	}

	@Column(name = "TRSE_DATO53", precision = 16)
	public BigDecimal getTrseDato53() {
		return this.trseDato53;
	}

	public void setTrseDato53(BigDecimal trseDato53) {
		this.trseDato53 = trseDato53;
	}

	@Column(name = "TRSE_DATO54", precision = 10, scale = 0)
	public Long getTrseDato54() {
		return this.trseDato54;
	}

	public void setTrseDato54(Long trseDato54) {
		this.trseDato54 = trseDato54;
	}

	@Column(name = "TRSE_DATO55", precision = 10, scale = 0)
	public Long getTrseDato55() {
		return this.trseDato55;
	}

	public void setTrseDato55(Long trseDato55) {
		this.trseDato55 = trseDato55;
	}

	@Column(name = "TRSE_DATO56", precision = 5, scale = 0)
	public Integer getTrseDato56() {
		return this.trseDato56;
	}

	public void setTrseDato56(Integer trseDato56) {
		this.trseDato56 = trseDato56;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRSE_DATO57", length = 7)
	public Date getTrseDato57() {
		return this.trseDato57;
	}

	public void setTrseDato57(Date trseDato57) {
		this.trseDato57 = trseDato57;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRSE_DATO58", length = 7)
	public Date getTrseDato58() {
		return this.trseDato58;
	}

	public void setTrseDato58(Date trseDato58) {
		this.trseDato58 = trseDato58;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRSE_DATO59", length = 7)
	public Date getTrseDato59() {
		return this.trseDato59;
	}

	public void setTrseDato59(Date trseDato59) {
		this.trseDato59 = trseDato59;
	}

}
