package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the COLECTIVOS_ANEXOS database table.
 * 
 */
@Entity
@Table(name="COLECTIVOS_ANEXOS")
public class ColectivosAnexo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COAN_ID")
	private long coanId;

	@Column(name="COSE_CONSECUTIVO")
	private BigDecimal coseConsecutivo;

	@Column(name="COSE_NOM_DOCTO")
	private String coseNomDocto;

	//bi-directional many-to-one association to ColectivosSeguimiento
    @ManyToOne
	@JoinColumn(name="COAN_COSE_ID")
	private ColectivosSeguimiento colectivosSeguimiento;

    public ColectivosAnexo() {
    }

	public long getCoanId() {
		return this.coanId;
	}

	public void setCoanId(long coanId) {
		this.coanId = coanId;
	}

	public BigDecimal getCoseConsecutivo() {
		return this.coseConsecutivo;
	}

	public void setCoseConsecutivo(BigDecimal coseConsecutivo) {
		this.coseConsecutivo = coseConsecutivo;
	}

	public String getCoseNomDocto() {
		return this.coseNomDocto;
	}

	public void setCoseNomDocto(String coseNomDocto) {
		this.coseNomDocto = coseNomDocto;
	}

	public ColectivosSeguimiento getColectivosSeguimiento() {
		return this.colectivosSeguimiento;
	}

	public void setColectivosSeguimiento(ColectivosSeguimiento colectivosSeguimiento) {
		this.colectivosSeguimiento = colectivosSeguimiento;
	}
	
}