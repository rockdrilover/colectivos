package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the COLECTIVOS_CUENTAS database table.
 * 
 */
@Entity
@Table(name="COLECTIVOS_CUENTAS")
public class ColectivosCuenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ColectivosCuentaId id;

	@Column(name="COCU_CERTIFICADO")
	private BigDecimal cocuCertificado;

	@Column(name="COCU_COMENTARIOS_BCO")
	private String cocuComentariosBco;

	@Column(name="COCU_ESTATUS_SEGURO")
	private String cocuEstatusSeguro;

    @Temporal( TemporalType.DATE)
	@Column(name="COCU_FECHA_CONCILIA")
	private Date cocuFechaConcilia;

	@Column(name="COCU_MOTIVO_RECHAZO")
	private String cocuMotivoRechazo;

	@Column(name="COCU_POLIZA")
	private BigDecimal cocuPoliza;

	@Column(name="COCU_RAMO")
	private BigDecimal cocuRamo;

	@Column(name="COCU_REGISTRO")
	private String cocuRegistro;

	@Column(name="COCU_ULTIMO_CALIFICADOR")
	private String cocuUltimoCalificador;

	@Column(name="COCU_ID_VENTA")
	private BigDecimal cocuIdVenta;
	
	@Column(name="COCU_FECHA_CARGA")
	private Date cocuFechaCarga;
	

	public ColectivosCuenta() {
    }

	public ColectivosCuenta(String strLinea, String strConsecutivo) {
		setId(new ColectivosCuentaId(strLinea, strConsecutivo));
	}

	public ColectivosCuentaId getId() {
		return this.id;
	}

	public void setId(ColectivosCuentaId id) {
		this.id = id;
	}
	
	public BigDecimal getCocuCertificado() {
		return this.cocuCertificado;
	}

	public void setCocuCertificado(BigDecimal cocuCertificado) {
		this.cocuCertificado = cocuCertificado;
	}

	public String getCocuComentariosBco() {
		return this.cocuComentariosBco;
	}

	public void setCocuComentariosBco(String cocuComentariosBco) {
		this.cocuComentariosBco = cocuComentariosBco;
	}

	public String getCocuEstatusSeguro() {
		return this.cocuEstatusSeguro;
	}

	public void setCocuEstatusSeguro(String cocuEstatusSeguro) {
		this.cocuEstatusSeguro = cocuEstatusSeguro;
	}

	public Date getCocuFechaConcilia() {
		return this.cocuFechaConcilia;
	}

	public void setCocuFechaConcilia(Date cocuFechaConcilia) {
		this.cocuFechaConcilia = cocuFechaConcilia;
	}

	public String getCocuMotivoRechazo() {
		return this.cocuMotivoRechazo;
	}

	public void setCocuMotivoRechazo(String cocuMotivoRechazo) {
		this.cocuMotivoRechazo = cocuMotivoRechazo;
	}

	public BigDecimal getCocuPoliza() {
		return this.cocuPoliza;
	}

	public void setCocuPoliza(BigDecimal cocuPoliza) {
		this.cocuPoliza = cocuPoliza;
	}

	public BigDecimal getCocuRamo() {
		return this.cocuRamo;
	}

	public void setCocuRamo(BigDecimal cocuRamo) {
		this.cocuRamo = cocuRamo;
	}

	public String getCocuRegistro() {
		return this.cocuRegistro;
	}

	public void setCocuRegistro(String cocuRegistro) {
		this.cocuRegistro = cocuRegistro;
	}

	public String getCocuUltimoCalificador() {
		return this.cocuUltimoCalificador;
	}

	public void setCocuUltimoCalificador(String cocuUltimoCalificador) {
		this.cocuUltimoCalificador = cocuUltimoCalificador;
	}
	
	public BigDecimal getCocuIdVenta() {
		return cocuIdVenta;
	}

	public void setCocuIdVenta(BigDecimal cocuIdVenta) {
		this.cocuIdVenta = cocuIdVenta;
	}
	
	public Date getCocuFechaCarga() {
		return cocuFechaCarga;
	}

	public void setCocuFechaCarga(Date cocuFechaCarga) {
		this.cocuFechaCarga = cocuFechaCarga;
	}

}