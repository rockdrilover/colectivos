package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CART_ESTADOS_IVA database table.
 * 
 */
@Embeddable
public class CartEstadosIvaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CAIV_CAOR_CD_ORG")
	private long caivCaorCdOrg;

	@Column(name="CAIV_CAES_CD_ESTADO")
	private long caivCaesCdEstado;

	@Column(name="CAIV_CD_POBLACION")
	private long caivCdPoblacion;

	@Column(name="CAIV_CP_POBLACION")
	private long caivCpPoblacion;

	@Column(name="CAIV_DS_POBLACION")
	private String caivDsPoblacion;

    public CartEstadosIvaPK() {
    }
	public long getCaivCaorCdOrg() {
		return this.caivCaorCdOrg;
	}
	public void setCaivCaorCdOrg(long caivCaorCdOrg) {
		this.caivCaorCdOrg = caivCaorCdOrg;
	}
	public long getCaivCaesCdEstado() {
		return this.caivCaesCdEstado;
	}
	public void setCaivCaesCdEstado(long caivCaesCdEstado) {
		this.caivCaesCdEstado = caivCaesCdEstado;
	}
	public long getCaivCdPoblacion() {
		return this.caivCdPoblacion;
	}
	public void setCaivCdPoblacion(long caivCdPoblacion) {
		this.caivCdPoblacion = caivCdPoblacion;
	}
	public long getCaivCpPoblacion() {
		return this.caivCpPoblacion;
	}
	public void setCaivCpPoblacion(long caivCpPoblacion) {
		this.caivCpPoblacion = caivCpPoblacion;
	}
	public String getCaivDsPoblacion() {
		return this.caivDsPoblacion;
	}
	public void setCaivDsPoblacion(String caivDsPoblacion) {
		this.caivDsPoblacion = caivDsPoblacion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CartEstadosIvaPK)) {
			return false;
		}
		CartEstadosIvaPK castOther = (CartEstadosIvaPK)other;
		return 
			(this.caivCaorCdOrg == castOther.caivCaorCdOrg)
			&& (this.caivCaesCdEstado == castOther.caivCaesCdEstado)
			&& (this.caivCdPoblacion == castOther.caivCdPoblacion)
			&& (this.caivCpPoblacion == castOther.caivCpPoblacion)
			&& this.caivDsPoblacion.equals(castOther.caivDsPoblacion);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.caivCaorCdOrg ^ (this.caivCaorCdOrg >>> 32)));
		hash = hash * prime + ((int) (this.caivCaesCdEstado ^ (this.caivCaesCdEstado >>> 32)));
		hash = hash * prime + ((int) (this.caivCdPoblacion ^ (this.caivCdPoblacion >>> 32)));
		hash = hash * prime + ((int) (this.caivCpPoblacion ^ (this.caivCpPoblacion >>> 32)));
		hash = hash * prime + this.caivDsPoblacion.hashCode();
		
		return hash;
    }
}