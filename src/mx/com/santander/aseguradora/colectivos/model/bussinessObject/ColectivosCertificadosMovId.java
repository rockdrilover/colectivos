package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ColectivosCertificadosMovId generated by hbm2java
 */
@Embeddable
public class ColectivosCertificadosMovId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -113210224195515335L;
	private short cocmCasuCdSucursal;
	private byte cocmCarpCdRamo;
	private long cocmCapoNuPoliza;
	private long cocmNuCertificado;
	private long cocmNuMovimiento;

	public ColectivosCertificadosMovId() {
	}

	public ColectivosCertificadosMovId(short cocmCasuCdSucursal,
			byte cocmCarpCdRamo, long cocmCapoNuPoliza, long cocmNuCertificado,
			long cocmNuMovimiento) {
		this.cocmCasuCdSucursal = cocmCasuCdSucursal;
		this.cocmCarpCdRamo = cocmCarpCdRamo;
		this.cocmCapoNuPoliza = cocmCapoNuPoliza;
		this.cocmNuCertificado = cocmNuCertificado;
		this.cocmNuMovimiento = cocmNuMovimiento;
	}

	@Column(name = "COCM_CASU_CD_SUCURSAL", nullable = false, precision = 3, scale = 0)
	public short getCocmCasuCdSucursal() {
		return this.cocmCasuCdSucursal;
	}

	public void setCocmCasuCdSucursal(short cocmCasuCdSucursal) {
		this.cocmCasuCdSucursal = cocmCasuCdSucursal;
	}

	@Column(name = "COCM_CARP_CD_RAMO", nullable = false, precision = 2, scale = 0)
	public byte getCocmCarpCdRamo() {
		return this.cocmCarpCdRamo;
	}

	public void setCocmCarpCdRamo(byte cocmCarpCdRamo) {
		this.cocmCarpCdRamo = cocmCarpCdRamo;
	}

	@Column(name = "COCM_CAPO_NU_POLIZA", nullable = false, precision = 12, scale = 0)
	public long getCocmCapoNuPoliza() {
		return this.cocmCapoNuPoliza;
	}

	public void setCocmCapoNuPoliza(long cocmCapoNuPoliza) {
		this.cocmCapoNuPoliza = cocmCapoNuPoliza;
	}

	@Column(name = "COCM_NU_CERTIFICADO", nullable = false, precision = 15, scale = 0)
	public long getCocmNuCertificado() {
		return this.cocmNuCertificado;
	}

	public void setCocmNuCertificado(long cocmNuCertificado) {
		this.cocmNuCertificado = cocmNuCertificado;
	}

	@Column(name = "COCM_NU_MOVIMIENTO", nullable = false, precision = 10, scale = 0)
	public long getCocmNuMovimiento() {
		return this.cocmNuMovimiento;
	}

	public void setCocmNuMovimiento(long cocmNuMovimiento) {
		this.cocmNuMovimiento = cocmNuMovimiento;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ColectivosCertificadosMovId))
			return false;
		ColectivosCertificadosMovId castOther = (ColectivosCertificadosMovId) other;

		return (this.getCocmCasuCdSucursal() == castOther
				.getCocmCasuCdSucursal())
				&& (this.getCocmCarpCdRamo() == castOther.getCocmCarpCdRamo())
				&& (this.getCocmCapoNuPoliza() == castOther
						.getCocmCapoNuPoliza())
				&& (this.getCocmNuCertificado() == castOther
						.getCocmNuCertificado())
				&& (this.getCocmNuMovimiento() == castOther
						.getCocmNuMovimiento());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getCocmCasuCdSucursal();
		result = 37 * result + this.getCocmCarpCdRamo();
		result = 37 * result + (int) this.getCocmCapoNuPoliza();
		result = 37 * result + (int) this.getCocmNuCertificado();
		result = 37 * result + (int) this.getCocmNuMovimiento();
		return result;
	}

}
