package mx.com.santander.aseguradora.colectivos.model.bussinessObject;


import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CartReservaDigI generated by hbm2java
 */
@Entity
@Table(name = "CART_RESERVA_DIG_I")
public class CartReservaDigI implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3121698852197255637L;
	private CartReservaDigIId id;
	private Short cardCarpCdRamo;
	private Integer cardCapoNuPoliza;
	private Integer cardCaceNuCertificado;
	private Date cardFeEmision;
	private String cardStReciboDig;
	private String cardStProceso;
	private BigDecimal cardMtRecibo;
	private Date cardFecha;
	private Date cardFeFirma;
	private Date cardFeTimbre;
	private String cardDsObservaciones;
	private String cardTipoCobro;
	private Date cardFeAnulaFolio;
	private String cardTipoOperacion;

	public CartReservaDigI() {
	}

	public CartReservaDigI(CartReservaDigIId id) {
		this.id = id;
	}

	public CartReservaDigI(CartReservaDigIId id, Short cardCarpCdRamo,
			Integer cardCapoNuPoliza, Integer cardCaceNuCertificado,
			Date cardFeEmision, String cardStReciboDig, String cardStProceso,
			BigDecimal cardMtRecibo, Date cardFecha, Date cardFeFirma,
			Date cardFeTimbre, String cardDsObservaciones,
			String cardTipoCobro, Date cardFeAnulaFolio,
			String cardTipoOperacion) {
		this.id = id;
		this.cardCarpCdRamo = cardCarpCdRamo;
		this.cardCapoNuPoliza = cardCapoNuPoliza;
		this.cardCaceNuCertificado = cardCaceNuCertificado;
		this.cardFeEmision = cardFeEmision;
		this.cardStReciboDig = cardStReciboDig;
		this.cardStProceso = cardStProceso;
		this.cardMtRecibo = cardMtRecibo;
		this.cardFecha = cardFecha;
		this.cardFeFirma = cardFeFirma;
		this.cardFeTimbre = cardFeTimbre;
		this.cardDsObservaciones = cardDsObservaciones;
		this.cardTipoCobro = cardTipoCobro;
		this.cardFeAnulaFolio = cardFeAnulaFolio;
		this.cardTipoOperacion = cardTipoOperacion;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "cardCasuCdSucursal", column = @Column(name = "CARD_CASU_CD_SUCURSAL", nullable = false, precision = 3, scale = 0)),
			@AttributeOverride(name = "cardCareNuRecibo", column = @Column(name = "CARD_CARE_NU_RECIBO", nullable = false, precision = 13, scale = 1)) })
	public CartReservaDigIId getId() {
		return this.id;
	}

	public void setId(CartReservaDigIId id) {
		this.id = id;
	}

	@Column(name = "CARD_CARP_CD_RAMO", precision = 2, scale = 0)
	public Short getCardCarpCdRamo() {
		return this.cardCarpCdRamo;
	}

	public void setCardCarpCdRamo(Short cardCarpCdRamo) {
		this.cardCarpCdRamo = cardCarpCdRamo;
	}

	@Column(name = "CARD_CAPO_NU_POLIZA", precision = 9, scale = 0)
	public Integer getCardCapoNuPoliza() {
		return this.cardCapoNuPoliza;
	}

	public void setCardCapoNuPoliza(Integer cardCapoNuPoliza) {
		this.cardCapoNuPoliza = cardCapoNuPoliza;
	}

	@Column(name = "CARD_CACE_NU_CERTIFICADO", precision = 9, scale = 0)
	public Integer getCardCaceNuCertificado() {
		return this.cardCaceNuCertificado;
	}

	public void setCardCaceNuCertificado(Integer cardCaceNuCertificado) {
		this.cardCaceNuCertificado = cardCaceNuCertificado;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CARD_FE_EMISION", length = 7)
	public Date getCardFeEmision() {
		return this.cardFeEmision;
	}

	public void setCardFeEmision(Date cardFeEmision) {
		this.cardFeEmision = cardFeEmision;
	}

	@Column(name = "CARD_ST_RECIBO_DIG", length = 1)
	public String getCardStReciboDig() {
		return this.cardStReciboDig;
	}

	public void setCardStReciboDig(String cardStReciboDig) {
		this.cardStReciboDig = cardStReciboDig;
	}

	@Column(name = "CARD_ST_PROCESO", length = 3)
	public String getCardStProceso() {
		return this.cardStProceso;
	}

	public void setCardStProceso(String cardStProceso) {
		this.cardStProceso = cardStProceso;
	}

	@Column(name = "CARD_MT_RECIBO", precision = 13)
	public BigDecimal getCardMtRecibo() {
		return this.cardMtRecibo;
	}

	public void setCardMtRecibo(BigDecimal cardMtRecibo) {
		this.cardMtRecibo = cardMtRecibo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CARD_FECHA", length = 7)
	public Date getCardFecha() {
		return this.cardFecha;
	}

	public void setCardFecha(Date cardFecha) {
		this.cardFecha = cardFecha;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CARD_FE_FIRMA", length = 7)
	public Date getCardFeFirma() {
		return this.cardFeFirma;
	}

	public void setCardFeFirma(Date cardFeFirma) {
		this.cardFeFirma = cardFeFirma;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CARD_FE_TIMBRE", length = 7)
	public Date getCardFeTimbre() {
		return this.cardFeTimbre;
	}

	public void setCardFeTimbre(Date cardFeTimbre) {
		this.cardFeTimbre = cardFeTimbre;
	}

	@Column(name = "CARD_DS_OBSERVACIONES", length = 500)
	public String getCardDsObservaciones() {
		return this.cardDsObservaciones;
	}

	public void setCardDsObservaciones(String cardDsObservaciones) {
		this.cardDsObservaciones = cardDsObservaciones;
	}

	@Column(name = "CARD_TIPO_COBRO", length = 3)
	public String getCardTipoCobro() {
		return this.cardTipoCobro;
	}

	public void setCardTipoCobro(String cardTipoCobro) {
		this.cardTipoCobro = cardTipoCobro;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CARD_FE_ANULA_FOLIO", length = 7)
	public Date getCardFeAnulaFolio() {
		return this.cardFeAnulaFolio;
	}

	public void setCardFeAnulaFolio(Date cardFeAnulaFolio) {
		this.cardFeAnulaFolio = cardFeAnulaFolio;
	}

	@Column(name = "CARD_TIPO_OPERACION", length = 5)
	public String getCardTipoOperacion() {
		return this.cardTipoOperacion;
	}

	public void setCardTipoOperacion(String cardTipoOperacion) {
		this.cardTipoOperacion = cardTipoOperacion;
	}

}
