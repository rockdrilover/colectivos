package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Clase para mapear y envolver datos de la tabla COLECTIVOS_AUT_TP_ENDOSO
 * @author Ing. Issac Bautista
 *
 */
@Entity
@Table(name = "COLECTIVOS_AUT_TP_ENDOSO")
public class AutorizaTipoEndoso implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Objeto Id
	 */
	@EmbeddedId
	private AutorizaTipoEndosoPK id;

	/**
	 * Campo fecha 1
	 */
	@Temporal(TemporalType.DATE)
	@Column(name="CATE_CAMPOF1")
	private Date cateCampof1;

	/**
	 * Campo fecha 2
	 */
	@Temporal(TemporalType.DATE)
	@Column(name="CATE_CAMPOF2")
	private Date cateCampof2;

	/**
	 * Campo numerico 1
	 */
	@Column(name="CATE_CAMPON1")
	private BigDecimal cateCampon1;

	/**
	 * Campo numerico 2
	 */
	@Column(name="CATE_CAMPON2")
	private BigDecimal cateCampon2;

	/**
	 * Campo numerico 3
	 */
	@Column(name="CATE_CAMPON3")
	private BigDecimal cateCampon3;

	/**
	 * Campo varchar 1
	 */
	@Column(name="CATE_CAMPOV1")
	private String cateCampov1;

	/**
	 * Campo varchar 2
	 */
	@Column(name="CATE_CAMPOV2")
	private String cateCampov2;

	/**
	 * Campo varchar 3
	 */
	@Column(name="CATE_CAMPOV3")
	private String cateCampov3;

	/**
	 * Descripcion tipo endoso
	 */
	@Column(name="CATE_DE_TIPO_ENDOSO")
	private String cateDeTipoEndoso;

	/**
	 * Correo usuario autorizador
	 */
	@Column(name="CATE_MAIL_USU_AUT")
	private String cateMailUsuAut;

	/**
	 * Bandera requiere autorizador
	 */
	@Column(name="CATE_REQ_AUT")
	private BigDecimal cateReqAut;

	/**
	 * Estatus tipo endoso
	 */
	@Column(name="CATE_ST_TIPO_ENDOSO")
	private BigDecimal cateStTipoEndoso;

	/**
	 * Constructor de Clase
	 */
	public AutorizaTipoEndoso() {
		id = new AutorizaTipoEndosoPK();
		cateCampof1 = new Date();
		cateCampof2 = new Date();
	}
	
	/**
	 * @return
	 */
	public AutorizaTipoEndosoPK getId() {
		return this.id;
	}
	/**
	 * @param id
	 */
	public void setId(AutorizaTipoEndosoPK id) {
		this.id = id;
	}
	/**
	 * @return
	 */
	public Date getCateCampof1() {
		return new Date(this.cateCampof1.getTime());
	}
	/**
	 * @param cateCampof1
	 */
	public void setCateCampof1(Date cateCampof1) {
		if(cateCampof1 != null) {
			this.cateCampof1 = new Date(cateCampof1.getTime());
		}
	}
	/**
	 * @return
	 */
	public Date getCateCampof2() {
		return new Date(this.cateCampof2.getTime());
	}
	/**
	 * @param cateCampof2
	 */
	public void setCateCampof2(Date cateCampof2) {
		if(cateCampof2 != null) {
			this.cateCampof2 = new Date(cateCampof2.getTime());
		}
	}
	/**
	 * @return
	 */
	public BigDecimal getCateCampon1() {
		return this.cateCampon1;
	}
	/**
	 * @param cateCampon1
	 */
	public void setCateCampon1(BigDecimal cateCampon1) {
		this.cateCampon1 = cateCampon1;
	}
	/**
	 * @return
	 */
	public BigDecimal getCateCampon2() {
		return this.cateCampon2;
	}
	/**
	 * @param cateCampon2
	 */
	public void setCateCampon2(BigDecimal cateCampon2) {
		this.cateCampon2 = cateCampon2;
	}
	/**
	 * @return
	 */
	public BigDecimal getCateCampon3() {
		return this.cateCampon3;
	}
	/**
	 * @param cateCampon3
	 */
	public void setCateCampon3(BigDecimal cateCampon3) {
		this.cateCampon3 = cateCampon3;
	}
	/**
	 * @return
	 */
	public String getCateCampov1() {
		return this.cateCampov1;
	}
	/**
	 * @param cateCampov1
	 */
	public void setCateCampov1(String cateCampov1) {
		this.cateCampov1 = cateCampov1;
	}
	/**
	 * @return
	 */
	public String getCateCampov2() {
		return this.cateCampov2;
	}
	/**
	 * @param cateCampov2
	 */
	public void setCateCampov2(String cateCampov2) {
		this.cateCampov2 = cateCampov2;
	}
	/**
	 * @return
	 */
	public String getCateCampov3() {
		return this.cateCampov3;
	}
	/**
	 * @param cateCampov3
	 */
	public void setCateCampov3(String cateCampov3) {
		this.cateCampov3 = cateCampov3;
	}
	/**
	 * @return
	 */
	public String getCateDeTipoEndoso() {
		return this.cateDeTipoEndoso;
	}
	/**
	 * @param cateDeTipoEndoso
	 */
	public void setCateDeTipoEndoso(String cateDeTipoEndoso) {
		this.cateDeTipoEndoso = cateDeTipoEndoso;
	}
	/**
	 * @return
	 */
	public String getCateMailUsuAut() {
		return this.cateMailUsuAut;
	}
	/**
	 * @param cateMailUsuAut
	 */
	public void setCateMailUsuAut(String cateMailUsuAut) {
		this.cateMailUsuAut = cateMailUsuAut;
	}
	/**
	 * @return
	 */
	public BigDecimal getCateReqAut() {
		return this.cateReqAut;
	}
	/**
	 * @param cateReqAut
	 */
	public void setCateReqAut(BigDecimal cateReqAut) {
		this.cateReqAut = cateReqAut;
	}
	/**
	 * @return
	 */
	public BigDecimal getCateStTipoEndoso() {
		return this.cateStTipoEndoso;
	}
	/**
	 * @param cateStTipoEndoso
	 */
	public void setCateStTipoEndoso(BigDecimal cateStTipoEndoso) {
		this.cateStTipoEndoso = cateStTipoEndoso;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null) {
			return false;
		}
		
		return (this.getClass() != obj.getClass());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return String.format("%d", this.id.getCateCdTipoEndoso()).hashCode();
	}
}