package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the COLECTIVOS_SUSCRIPCION database table.
 * 
 */
@Entity
@Table(name="COLECTIVOS_SUSCRIPCION")
public class ColectivosSuscripcion implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7346299774898804312L;

	@Id
	@Column(name="COSU_ID" )		
	private long cosuId;

	@Column(name="COSU_CELULAR")
	private String cosuCelular;

	@Column(name="COSU_DICTAMEN")
	private String cosuDictamen;

	@Column(name="COSU_EMAIL")
	private String cosuEmail;

	@Column(name="COSU_EXAM_MEDICO")
	private String cosuExamMedico;

	@Column(name="COSU_EXTRA_FIRMA")
	private BigDecimal cosuExtraFirma;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_APL_EXAM")
	private Date cosuFechaAplExam;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_CARGA")
	private Date cosuFechaCarga;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_CIT_EXAM")
	private Date cosuFechaCitExam;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_FIRM")
	private Date cosuFechaFirm;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_NAC")
	private Date cosuFechaNac;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_NOT_DIC")
	private Date cosuFechaNotDic;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_REC_ASEG")
	private Date cosuFechaRecAseg;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_RES_DOC")
	private Date cosuFechaResDoc;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_RESP_SUC")
	private Date cosuFechaRespSuc;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_RESULT")
	private Date cosuFechaResult;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_SOL_CRED")
	private Date cosuFechaSolCred;

    @Temporal( TemporalType.DATE)
	@Column(name="COSU_FECHA_SOL_EXAM")
	private Date cosuFechaSolExam;

	@Column(name="COSU_FOLIO_CH")
	private BigDecimal cosuFolioCh;

	@Column(name="COSU_FOLIO_RIESGOS")
	private BigDecimal cosuFolioRiesgos;

	@Column(name="COSU_ID_CONCATENAR")
	private String cosuIdConcatenar;

	@Column(name="COSU_LOCAL")
	private String cosuLocal;

	@Column(name="COSU_MONTO")
	private BigDecimal cosuMonto;

	@Column(name="COSU_MONTO_FIRMADO")
	private BigDecimal cosuMontoFirmado;

	@Column(name="COSU_MOT_EXAM_MED")
	private String cosuMotExamMed;

	@Column(name="COSU_NOMBRE")
	private String cosuNombre;

	@Column(name="COSU_NUM_CREDITO")
	private String cosuNumCredito;

	@Column(name="COSU_NUM_INTER")
	private BigDecimal cosuNumInter;

	@Column(name="COSU_OBS_WF")
	private String cosuObsWf;

	@Column(name="COSU_OBSERVACIONES")
	private String cosuObservaciones;

	@Column(name="COSU_PARTICIPACION")
	private String cosuParticipacion;

	@Column(name="COSU_PLAZA")
	private String cosuPlaza;

	@Column(name="COSU_SUC_EJEC")
	private String cosuSucEjec;

	@Column(name="COSU_TELEFONO_1")
	private String cosuTelefono1;

	@Column(name="COSU_TELEFONO_2")
	private String cosuTelefono2;

	@Column(name="COSU_USUARIO_CREA")
	private String cosuUsuarioCrea;

	//bi-directional many-to-one association to ColectivosSeguimiento
	@OneToMany(fetch = FetchType.LAZY, mappedBy="colectivosSuscripcion")
	private Set<ColectivosSeguimiento> colectivosSeguimientos;

    public ColectivosSuscripcion() {
    }

	public long getCosuId() {
		return this.cosuId;
	}

	public void setCosuId(long cosuId) {
		this.cosuId = cosuId;
	}

	public String getCosuCelular() {
		return this.cosuCelular;
	}

	public void setCosuCelular(String cosuCelular) {
		this.cosuCelular = cosuCelular;
	}

	public String getCosuDictamen() {
		return this.cosuDictamen;
	}

	public void setCosuDictamen(String cosuDictamen) {
		this.cosuDictamen = cosuDictamen;
	}

	public String getCosuEmail() {
		return this.cosuEmail;
	}

	public void setCosuEmail(String cosuEmail) {
		this.cosuEmail = cosuEmail;
	}

	public String getCosuExamMedico() {
		return this.cosuExamMedico;
	}

	public void setCosuExamMedico(String cosuExamMedico) {
		this.cosuExamMedico = cosuExamMedico;
	}

	public BigDecimal getCosuExtraFirma() {
		return this.cosuExtraFirma;
	}

	public void setCosuExtraFirma(BigDecimal cosuExtraFirma) {
		this.cosuExtraFirma = cosuExtraFirma;
	}

	public Date getCosuFechaAplExam() {
		return this.cosuFechaAplExam;
	}

	public void setCosuFechaAplExam(Date cosuFechaAplExam) {
		this.cosuFechaAplExam = cosuFechaAplExam;
	}

	public Date getCosuFechaCarga() {
		return this.cosuFechaCarga;
	}

	public void setCosuFechaCarga(Date cosuFechaCarga) {
		this.cosuFechaCarga = cosuFechaCarga;
	}

	public Date getCosuFechaCitExam() {
		return this.cosuFechaCitExam;
	}

	public void setCosuFechaCitExam(Date cosuFechaCitExam) {
		this.cosuFechaCitExam = cosuFechaCitExam;
	}

	public Date getCosuFechaFirm() {
		return this.cosuFechaFirm;
	}

	public void setCosuFechaFirm(Date cosuFechaFirm) {
		this.cosuFechaFirm = cosuFechaFirm;
	}

	public Date getCosuFechaNac() {
		return this.cosuFechaNac;
	}

	public void setCosuFechaNac(Date cosuFechaNac) {
		this.cosuFechaNac = cosuFechaNac;
	}

	public Date getCosuFechaNotDic() {
		return this.cosuFechaNotDic;
	}

	public void setCosuFechaNotDic(Date cosuFechaNotDic) {
		this.cosuFechaNotDic = cosuFechaNotDic;
	}

	public Date getCosuFechaRecAseg() {
		return this.cosuFechaRecAseg;
	}

	public void setCosuFechaRecAseg(Date cosuFechaRecAseg) {
		this.cosuFechaRecAseg = cosuFechaRecAseg;
	}

	public Date getCosuFechaResDoc() {
		return this.cosuFechaResDoc;
	}

	public void setCosuFechaResDoc(Date cosuFechaResDoc) {
		this.cosuFechaResDoc = cosuFechaResDoc;
	}

	public Date getCosuFechaRespSuc() {
		return this.cosuFechaRespSuc;
	}

	public void setCosuFechaRespSuc(Date cosuFechaRespSuc) {
		this.cosuFechaRespSuc = cosuFechaRespSuc;
	}

	public Date getCosuFechaResult() {
		return this.cosuFechaResult;
	}

	public void setCosuFechaResult(Date cosuFechaResult) {
		this.cosuFechaResult = cosuFechaResult;
	}

	public Date getCosuFechaSolCred() {
		return this.cosuFechaSolCred;
	}

	public void setCosuFechaSolCred(Date cosuFechaSolCred) {
		this.cosuFechaSolCred = cosuFechaSolCred;
	}

	public Date getCosuFechaSolExam() {
		return this.cosuFechaSolExam;
	}

	public void setCosuFechaSolExam(Date cosuFechaSolExam) {
		this.cosuFechaSolExam = cosuFechaSolExam;
	}

	public BigDecimal getCosuFolioCh() {
		return this.cosuFolioCh;
	}

	public void setCosuFolioCh(BigDecimal cosuFolioCh) {
		this.cosuFolioCh = cosuFolioCh;
	}

	public BigDecimal getCosuFolioRiesgos() {
		return this.cosuFolioRiesgos;
	}

	public void setCosuFolioRiesgos(BigDecimal cosuFolioRiesgos) {
		this.cosuFolioRiesgos = cosuFolioRiesgos;
	}

	public String getCosuIdConcatenar() {
		return this.cosuIdConcatenar;
	}

	public void setCosuIdConcatenar(String cosuIdConcatenar) {
		this.cosuIdConcatenar = cosuIdConcatenar;
	}

	public String getCosuLocal() {
		return this.cosuLocal;
	}

	public void setCosuLocal(String cosuLocal) {
		this.cosuLocal = cosuLocal;
	}

	public BigDecimal getCosuMonto() {
		return this.cosuMonto;
	}

	public void setCosuMonto(BigDecimal cosuMonto) {
		this.cosuMonto = cosuMonto;
	}

	public BigDecimal getCosuMontoFirmado() {
		return this.cosuMontoFirmado;
	}

	public void setCosuMontoFirmado(BigDecimal cosuMontoFirmado) {
		this.cosuMontoFirmado = cosuMontoFirmado;
	}

	public String getCosuMotExamMed() {
		return this.cosuMotExamMed;
	}

	public void setCosuMotExamMed(String cosuMotExamMed) {
		this.cosuMotExamMed = cosuMotExamMed;
	}

	public String getCosuNombre() {
		return this.cosuNombre;
	}

	public void setCosuNombre(String cosuNombre) {
		this.cosuNombre = cosuNombre;
	}

	public String getCosuNumCredito() {
		return this.cosuNumCredito;
	}

	public void setCosuNumCredito(String cosuNumCredito) {
		this.cosuNumCredito = cosuNumCredito;
	}

	public BigDecimal getCosuNumInter() {
		return this.cosuNumInter;
	}

	public void setCosuNumInter(BigDecimal cosuNumInter) {
		this.cosuNumInter = cosuNumInter;
	}

	public String getCosuObsWf() {
		return this.cosuObsWf;
	}

	public void setCosuObsWf(String cosuObsWf) {
		this.cosuObsWf = cosuObsWf;
	}

	public String getCosuObservaciones() {
		return this.cosuObservaciones;
	}

	public void setCosuObservaciones(String cosuObservaciones) {
		this.cosuObservaciones = cosuObservaciones;
	}

	public String getCosuParticipacion() {
		return this.cosuParticipacion;
	}

	public void setCosuParticipacion(String cosuParticipacion) {
		this.cosuParticipacion = cosuParticipacion;
	}

	public String getCosuPlaza() {
		return this.cosuPlaza;
	}

	public void setCosuPlaza(String cosuPlaza) {
		this.cosuPlaza = cosuPlaza;
	}

	public String getCosuSucEjec() {
		return this.cosuSucEjec;
	}

	public void setCosuSucEjec(String cosuSucEjec) {
		this.cosuSucEjec = cosuSucEjec;
	}

	public String getCosuTelefono1() {
		return this.cosuTelefono1;
	}

	public void setCosuTelefono1(String cosuTelefono1) {
		this.cosuTelefono1 = cosuTelefono1;
	}

	public String getCosuTelefono2() {
		return this.cosuTelefono2;
	}

	public void setCosuTelefono2(String cosuTelefono2) {
		this.cosuTelefono2 = cosuTelefono2;
	}

	public String getCosuUsuarioCrea() {
		return this.cosuUsuarioCrea;
	}

	public void setCosuUsuarioCrea(String cosuUsuarioCrea) {
		this.cosuUsuarioCrea = cosuUsuarioCrea;
	}

	public Set<ColectivosSeguimiento> getColectivosSeguimientos() {
		return this.colectivosSeguimientos;
	}

	public void setColectivosSeguimientos(Set<ColectivosSeguimiento> colectivosSeguimientos) {
		this.colectivosSeguimientos = colectivosSeguimientos;
	}
	
}