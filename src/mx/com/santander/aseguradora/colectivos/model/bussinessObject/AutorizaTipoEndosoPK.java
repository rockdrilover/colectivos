package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Clase para mapear y envolver datos de la Primary Key de tabla COLECTIVOS_AUT_TP_ENDOSO
 * @author Ing. Issac Bautista Bautista
 *
 */
@Embeddable
public class AutorizaTipoEndosoPK implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Codigo tipo endoso 
	 */
	@Column(name="CATE_CD_TIPO_ENDOSO")
	private long cateCdTipoEndoso;

	/**
	 * Codigo de usuatio autorizador 
	 */
	@Column(name="CATE_CD_USU_AUT")
	private String cateCdUsuAut;

	/**
	 * Constructor de clase
	 */
	public AutorizaTipoEndosoPK() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * @return
	 */
	public long getCateCdTipoEndoso() {
		return this.cateCdTipoEndoso;
	}
	/**
	 * @param cateCdTipoEndoso
	 */
	public void setCateCdTipoEndoso(long cateCdTipoEndoso) {
		this.cateCdTipoEndoso = cateCdTipoEndoso;
	}
	/**
	 * @return
	 */
	public String getCateCdUsuAut() {
		return this.cateCdUsuAut;
	}
	/**
	 * @param cateCdUsuAut
	 */
	public void setCateCdUsuAut(String cateCdUsuAut) {
		this.cateCdUsuAut = cateCdUsuAut;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AutorizaTipoEndosoPK)) {
			return false;
		}
		AutorizaTipoEndosoPK castOther = (AutorizaTipoEndosoPK)other;
		return 
			(this.cateCdTipoEndoso == castOther.cateCdTipoEndoso)
			&& this.cateCdUsuAut.equals(castOther.cateCdUsuAut);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.cateCdTipoEndoso ^ (this.cateCdTipoEndoso >>> 32)));
		hash = hash * prime + this.cateCdUsuAut.hashCode();
		
		return hash;
	}
}