package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

// default package
// Generated 20/11/2009 01:20:17 PM by Hibernate Tools 3.2.2.GA

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ColectivosPrecargaId generated by hbm2java
 */
@Embeddable
public class PreCargaId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4236597676813395208L;
	private Short copcCdSucursal;
	private Short copcCdRamo;
	private Long copcNumPoliza;
	private String copcIdCertificado;
	private String copcTipoRegistro;
	private String copcSumaAsegurada;

	public PreCargaId() {
	}

	public PreCargaId(Short copcCdSucursal, Short copcCdRamo,
			Long copcNumPoliza, String copcIdCertificado,
			String copcTipoRegistro, String copcSumaAsegurada) {
		this.copcCdSucursal = copcCdSucursal;
		this.copcCdRamo = copcCdRamo;
		this.copcNumPoliza = copcNumPoliza;
		this.copcIdCertificado = copcIdCertificado;
		this.copcTipoRegistro = copcTipoRegistro;
		this.copcSumaAsegurada = copcSumaAsegurada;
	}

	@Column(name = "COPC_CD_SUCURSAL", nullable = false, precision = 3, scale = 0)
	public Short getCopcCdSucursal() {
		return this.copcCdSucursal;
	}

	public void setCopcCdSucursal(Short copcCdSucursal) {
		this.copcCdSucursal = copcCdSucursal;
	}

	@Column(name = "COPC_CD_RAMO", nullable = false, precision = 2, scale = 0)
	public Short getCopcCdRamo() {
		return this.copcCdRamo;
	}

	public void setCopcCdRamo(Short copcCdRamo) {
		this.copcCdRamo = copcCdRamo;
	}

	@Column(name = "COPC_NUM_POLIZA", nullable = false, precision = 10, scale = 0)
	public Long getCopcNumPoliza() {
		return this.copcNumPoliza;
	}

	public void setCopcNumPoliza(Long copcNumPoliza) {
		this.copcNumPoliza = copcNumPoliza;
	}

	@Column(name = "COPC_ID_CERTIFICADO", nullable = false, length = 30)
	public String getCopcIdCertificado() {
		return this.copcIdCertificado;
	}

	public void setCopcIdCertificado(String copcIdCertificado) {
		this.copcIdCertificado = copcIdCertificado;
	}

	@Column(name = "COPC_TIPO_REGISTRO", nullable = false, length = 3)
	public String getCopcTipoRegistro() {
		return this.copcTipoRegistro;
	}

	public void setCopcTipoRegistro(String copcTipoRegistro) {
		this.copcTipoRegistro = copcTipoRegistro;
	}

	@Column(name = "COPC_SUMA_ASEGURADA", nullable = false, length = 20)
	public String getCopcSumaAsegurada() {
		return this.copcSumaAsegurada;
	}

	public void setCopcSumaAsegurada(String copcSumaAsegurada) {
		this.copcSumaAsegurada = copcSumaAsegurada;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PreCargaId))
			return false;
		PreCargaId castOther = (PreCargaId) other;

		return (this.getCopcCdSucursal() == castOther.getCopcCdSucursal())
				&& (this.getCopcCdRamo() == castOther.getCopcCdRamo())
				&& (this.getCopcNumPoliza() == castOther.getCopcNumPoliza())
				&& ((this.getCopcIdCertificado() == castOther
						.getCopcIdCertificado()) || (this
						.getCopcIdCertificado() != null
						&& castOther.getCopcIdCertificado() != null && this
						.getCopcIdCertificado().equals(
								castOther.getCopcIdCertificado())))
				&& ((this.getCopcTipoRegistro() == castOther
						.getCopcTipoRegistro()) || (this.getCopcTipoRegistro() != null
						&& castOther.getCopcTipoRegistro() != null && this
						.getCopcTipoRegistro().equals(
								castOther.getCopcTipoRegistro())))
				&& ((this.getCopcSumaAsegurada() == castOther
						.getCopcSumaAsegurada()) || (this
						.getCopcSumaAsegurada() != null
						&& castOther.getCopcSumaAsegurada() != null && this
						.getCopcSumaAsegurada().equals(
								castOther.getCopcSumaAsegurada())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getCopcCdSucursal();
		result = 37 * result + this.getCopcCdRamo();
		result = (int) (37 * result + this.getCopcNumPoliza());
		result = 37
				* result
				+ (getCopcIdCertificado() == null ? 0 : this
						.getCopcIdCertificado().hashCode());
		result = 37
				* result
				+ (getCopcTipoRegistro() == null ? 0 : this
						.getCopcTipoRegistro().hashCode());
		result = 37
				* result
				+ (getCopcSumaAsegurada() == null ? 0 : this
						.getCopcSumaAsegurada().hashCode());
		return result;
	}

}
