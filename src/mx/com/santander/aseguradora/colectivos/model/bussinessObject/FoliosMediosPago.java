package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;


@Entity
@Table(name = "TLMK_FOLIOS_MEDIOS_PAGO")
public class FoliosMediosPago implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private FoliosMediosPagoId id;
	private long nuCuenta;
	private String nuDeContrato;
	private String cdCliente;
	private Date fechaAprobacion;
	private String sucPaperless;
	private String nombreCompleto;
	private String rfc;
	private String agencia;
	private String semanaCanal;
	private Integer cargada;
	private Date fechaCarga;
	private Integer ramo;
	private String producto;
	private String plan;
	private String abonocargo;

	public FoliosMediosPago() {
	}

	public FoliosMediosPago(FoliosMediosPagoId id, long nuCuenta) {
		this.id = id;
		this.nuCuenta = nuCuenta;
	}

	public FoliosMediosPago(FoliosMediosPagoId id, long nuCuenta,
			String nuDeContrato, String cdCliente, Date fechaAprobacion,
			String sucPaperless, String nombreCompleto, String rfc,
			String agencia, String semanaCanal, int cargada,
			Date fechaCarga, int ramo, String producto, String plan) {
		this.id = id;
		this.nuCuenta = nuCuenta;
		this.nuDeContrato = nuDeContrato;
		this.cdCliente = cdCliente;
		this.fechaAprobacion = fechaAprobacion;
		this.sucPaperless = sucPaperless;
		this.nombreCompleto = nombreCompleto;
		this.rfc = rfc;
		this.agencia = agencia;
		this.semanaCanal = semanaCanal;
		this.cargada = cargada;
		this.fechaCarga = fechaCarga;
		this.ramo = ramo;
		this.producto = producto;
		this.plan = plan;
	}

	public FoliosMediosPago(String strLinea, String strConsecutivo) {
		setId(new FoliosMediosPagoId(0L, Utilerias.getVentaOrigen(strLinea.substring(109, 149).trim(), strLinea.substring(72, 73).trim(), strConsecutivo)));
		setNuCuenta(new Long(strLinea.substring(0, 11)));
		setFechaAprobacion(GestorFechas.generateDate(strLinea.substring(16, 24), "ddMMyyyy"));
		setProducto(Utilerias.generaFormatoHora(strLinea.substring(24, 28)));
		setSucPaperless(strLinea.substring(28, 32));
		setNombreCompleto(strLinea.substring(32, 72).trim() + " | " + strLinea.substring(109, 149).trim());
		setAgencia(String.valueOf(new Double(strLinea.substring(73, 87)) / 100));
		setRfc(String.valueOf(new Double(strLinea.substring(87, 101)) / 100));
		setFechaCarga(new Date());	
		setCargada(new Integer(Constantes.CARGADA_PARA_CONCILIAR));
		setAbonocargo(strLinea.substring(72,73).trim());
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "folio", column = @Column(name = "FOLIO", nullable = false, precision = 13, scale = 0)),
			@AttributeOverride(name = "ventaOrigen", column = @Column(name = "VENTA_ORIGEN", nullable = false, length = 10)) })
	public FoliosMediosPagoId getId() {
		return this.id;
	}

	public void setId(FoliosMediosPagoId id) {
		this.id = id;
	}

	@Column(name = "NU_CUENTA", nullable = false, precision = 20, scale = 0)
	public Long getNuCuenta() {
		return this.nuCuenta;
	}

	public void setNuCuenta(long nuCuenta) {
		this.nuCuenta = nuCuenta;
	}

	@Column(name = "NU_DE_CONTRATO", length = 24)
	public String getNuDeContrato() {
		return this.nuDeContrato;
	}

	public void setNuDeContrato(String nuDeContrato) {
		this.nuDeContrato = nuDeContrato;
	}

	@Column(name = "CD_CLIENTE", length = 10)
	public String getCdCliente() {
		return this.cdCliente;
	}

	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}

	@Column(name = "FECHA_APROBACION", length = 7)
	public Date getFechaAprobacion() {
		return this.fechaAprobacion;
	}

	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	@Column(name = "SUC_PAPERLESS", length = 10)
	public String getSucPaperless() {
		return this.sucPaperless;
	}

	public void setSucPaperless(String sucPaperless) {
		this.sucPaperless = sucPaperless;
	}

	@Column(name = "NOMBRE_COMPLETO", length = 102)
	public String getNombreCompleto() {
		return this.nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	@Column(name = "RFC", length = 13)
	public String getRfc() {
		return this.rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Column(name = "AGENCIA", length = 12)
	public String getAgencia() {
		return this.agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	@Column(name = "SEMANA_CANAL", length = 10)
	public String getSemanaCanal() {
		return this.semanaCanal;
	}

	public void setSemanaCanal(String semanaCanal) {
		this.semanaCanal = semanaCanal;
	}

	@Column(name = "CARGADA", precision = 1)
	public Integer getCargada() {
		return this.cargada;
	}

	public void setCargada(int cargada) {
		this.cargada = cargada;
	}

	@Column(name = "FECHA_CARGA", length = 7)
	public Date getFechaCarga() {
		return this.fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	@Column(name = "RAMO", precision = 2)
	public Integer getRamo() {
		return ramo;
	}

	public void setRamo(Integer ramo) {
		this.ramo = ramo;
	}

	@Column(name = "PRODUCTO", length = 6)
	public String getProducto() {
		return this.producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	@Column(name = "PLAN", length = 6)
	public String getPlan() {
		return this.plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	@Transient
	public String getAbonocargo() {
		return this.abonocargo;
	}
	public void setAbonocargo(String abonocargo) {
		this.abonocargo = abonocargo;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		
		FoliosMediosPago folio = (FoliosMediosPago) obj;
		if(this.id.equals(folio.id)){
			return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return String.format("%d", this.id.getFolio()).hashCode();
	}
}
