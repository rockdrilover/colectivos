package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CART_CLIENTES database table.
 * 
 */
@Embeddable
public class CartClientePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CACN_CD_NACIONALIDAD")
	private String cacnCdNacionalidad;

	@Column(name="CACN_NU_CEDULA_RIF")
	private String cacnNuCedulaRif;

    public CartClientePK() {
    }
	public String getCacnCdNacionalidad() {
		return this.cacnCdNacionalidad;
	}
	public void setCacnCdNacionalidad(String cacnCdNacionalidad) {
		this.cacnCdNacionalidad = cacnCdNacionalidad;
	}
	public String getCacnNuCedulaRif() {
		return this.cacnNuCedulaRif;
	}
	public void setCacnNuCedulaRif(String cacnNuCedulaRif) {
		this.cacnNuCedulaRif = cacnNuCedulaRif;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CartClientePK)) {
			return false;
		}
		CartClientePK castOther = (CartClientePK)other;
		return 
			this.cacnCdNacionalidad.equals(castOther.cacnCdNacionalidad)
			&& this.cacnNuCedulaRif.equals(castOther.cacnNuCedulaRif);
    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.cacnCdNacionalidad.hashCode();
		hash = hash * prime + this.cacnNuCedulaRif.hashCode();
		
		return hash;
    }
}