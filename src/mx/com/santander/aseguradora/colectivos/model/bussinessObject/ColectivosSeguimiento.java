package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the COLECTIVOS_SEGUIMIENTO database table.
 * 
 */
@Entity
@Table(name="COLECTIVOS_SEGUIMIENTO")
public class ColectivosSeguimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COSE_ID")
	private long coseId;

	@Column(name="COSE_AREA")
	private String coseArea;

	@Column(name="COSE_CONSECUTIVO")
	private BigDecimal coseConsecutivo;

	@Column(name="COSE_ESTATUS")
	private String coseEstatus;

    @Temporal( TemporalType.DATE)
	@Column(name="COSE_FECHA_ASIGNACION")
	private Date coseFechaAsignacion;

    @Temporal( TemporalType.DATE)
	@Column(name="COSE_FECHA_ATENCION")
	private Date coseFechaAtencion;

	@Column(name="COSE_OBSERVACIONES")
	private String coseObservaciones;

	@Column(name="COSE_ROL")
	private String coseRol;

	@Column(name="COSE_USUARIO_ASIGNO")
	private String coseUsuarioAsigno;

	@Column(name="COSE_USUARIO_ATENDIO")
	private String coseUsuarioAtendio;
	
	@Column(name="COSE_GRUPO")
	private BigDecimal coseGrupo;
	
	@Column(name="COSE_OBS_ASIG")
	private String coseObsAsig;
	

	//bi-directional many-to-one association to ColectivosAnexo
	@OneToMany(fetch = FetchType.LAZY, mappedBy="colectivosSeguimiento")
	private Set<ColectivosAnexo> colectivosAnexos;

	//bi-directional many-to-one association to ColectivosSuscripcion
    @ManyToOne
	@JoinColumn(name="COSE_COSU_ID")
	private ColectivosSuscripcion colectivosSuscripcion;

    public ColectivosSeguimiento() {
    }

	public long getCoseId() {
		return this.coseId;
	}

	public void setCoseId(long coseId) {
		this.coseId = coseId;
	}

	public String getCoseArea() {
		return this.coseArea;
	}

	public void setCoseArea(String coseArea) {
		this.coseArea = coseArea;
	}

	public BigDecimal getCoseConsecutivo() {
		return this.coseConsecutivo;
	}

	public void setCoseConsecutivo(BigDecimal coseConsecutivo) {
		this.coseConsecutivo = coseConsecutivo;
	}

	public String getCoseEstatus() {
		return this.coseEstatus;
	}

	public void setCoseEstatus(String coseEstatus) {
		this.coseEstatus = coseEstatus;
	}

	public Date getCoseFechaAsignacion() {
		return this.coseFechaAsignacion;
	}

	public void setCoseFechaAsignacion(Date coseFechaAsignacion) {
		this.coseFechaAsignacion = coseFechaAsignacion;
	}

	public Date getCoseFechaAtencion() {
		return this.coseFechaAtencion;
	}

	public void setCoseFechaAtencion(Date coseFechaAtencion) {
		this.coseFechaAtencion = coseFechaAtencion;
	}

	public String getCoseObservaciones() {
		return this.coseObservaciones;
	}

	public void setCoseObservaciones(String coseObservaciones) {
		this.coseObservaciones = coseObservaciones;
	}

	public String getCoseRol() {
		return this.coseRol;
	}

	public void setCoseRol(String coseRol) {
		this.coseRol = coseRol;
	}

	public String getCoseUsuarioAsigno() {
		return this.coseUsuarioAsigno;
	}

	public void setCoseUsuarioAsigno(String coseUsuarioAsigno) {
		this.coseUsuarioAsigno = coseUsuarioAsigno;
	}

	public String getCoseUsuarioAtendio() {
		return this.coseUsuarioAtendio;
	}

	public void setCoseUsuarioAtendio(String coseUsuarioAtendio) {
		this.coseUsuarioAtendio = coseUsuarioAtendio;
	}

	public Set<ColectivosAnexo> getColectivosAnexos() {
		return this.colectivosAnexos;
	}

	public void setColectivosAnexos(Set<ColectivosAnexo> colectivosAnexos) {
		this.colectivosAnexos = colectivosAnexos;
	}
	
	public ColectivosSuscripcion getColectivosSuscripcion() {
		return this.colectivosSuscripcion;
	}

	public void setColectivosSuscripcion(ColectivosSuscripcion colectivosSuscripcion) {
		this.colectivosSuscripcion = colectivosSuscripcion;
	}

	/**
	 * @return the coseGrupo
	 */
	public BigDecimal getCoseGrupo() {
		return coseGrupo;
	}

	/**
	 * @param coseGrupo the coseGrupo to set
	 */
	public void setCoseGrupo(BigDecimal coseGrupo) {
		this.coseGrupo = coseGrupo;
	}

	public String getCoseObsAsig() {
		return coseObsAsig;
	}

	public void setCoseObsAsig(String coseObsAsig) {
		this.coseObsAsig = coseObsAsig;
	}
	
	
	
}