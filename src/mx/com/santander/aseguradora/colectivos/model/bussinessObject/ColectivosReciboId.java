package mx.com.santander.aseguradora.colectivos.model.bussinessObject;
// default package
// Generated 24/09/2009 10:43:22 AM by Hibernate Tools 3.2.2.GA

import java.math.BigDecimal;

/**
 * CartRecibosId generated by hbm2java
 */
public class ColectivosReciboId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6621199665941208184L;
	private short coreCasuCdSucursal;
	private BigDecimal coreNuRecibo;

	public ColectivosReciboId() {
	}

	public ColectivosReciboId(short coreCasuCdSucursal, BigDecimal coreNuRecibo) {
		this.coreCasuCdSucursal = coreCasuCdSucursal;
		this.coreNuRecibo = coreNuRecibo;
	}

	public short getcoreCasuCdSucursal() {
		return this.coreCasuCdSucursal;
	}

	public void setcoreCasuCdSucursal(short coreCasuCdSucursal) {
		this.coreCasuCdSucursal = coreCasuCdSucursal;
	}

	public BigDecimal getcoreNuRecibo() {
		return this.coreNuRecibo;
	}

	public void setcoreNuRecibo(BigDecimal coreNuRecibo) {
		this.coreNuRecibo = coreNuRecibo;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ColectivosReciboId))
			return false;
		ColectivosReciboId castOther = (ColectivosReciboId) other;

		return (this.getcoreCasuCdSucursal() == castOther
				.getcoreCasuCdSucursal())
				&& ((this.getcoreNuRecibo() == castOther.getcoreNuRecibo()) || (this
						.getcoreNuRecibo() != null
						&& castOther.getcoreNuRecibo() != null && this
						.getcoreNuRecibo().equals(castOther.getcoreNuRecibo())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getcoreCasuCdSucursal();
		result = 37
				* result
				+ (getcoreNuRecibo() == null ? 0 : this.getcoreNuRecibo()
						.hashCode());
		return result;
	}

}
