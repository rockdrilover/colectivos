package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class VtaPrecargaId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private int cdSucursal;
	private int cdRamo;
	private long numPoliza;
	private String idCertificado;
	private String sistemaOrigen;

	public VtaPrecargaId() {
	}
		
	public VtaPrecargaId(int cdRamo, String strConse, String credito, String sistemaOrigen) {
		setCdSucursal(1);
		setCdRamo(cdRamo);
		setNumPoliza(new Long(strConse));
		setIdCertificado(credito);
		setSistemaOrigen(sistemaOrigen);
	}
	
	public VtaPrecargaId(int cdSucursal, int cdRamo, long numPoliza, String idCertificado, String sistemaOrigen) {
		this.cdSucursal = cdSucursal;
		this.cdRamo = cdRamo;
		this.numPoliza = numPoliza;
		this.idCertificado = idCertificado;
		this.sistemaOrigen = sistemaOrigen;
	}


	@Column(name = "VTAP_CD_SUCURSAL", nullable = false, precision = 3, scale = 0)
	public int getCdSucursal() {
		return this.cdSucursal;
	}
	public void setCdSucursal(int cdSucursal) {
		this.cdSucursal = cdSucursal;
	}

	@Column(name = "VTAP_CD_RAMO", nullable = false, precision = 2, scale = 0)
	public int getCdRamo() {
		return this.cdRamo;
	}
	public void setCdRamo(int cdRamo) {
		this.cdRamo = cdRamo;
	}

	@Column(name = "VTAP_NUM_POLIZA", nullable = false, precision = 10, scale = 0)
	public long getNumPoliza() {
		return this.numPoliza;
	}
	public void setNumPoliza(long numPoliza) {
		this.numPoliza = numPoliza;
	}

	@Column(name = "VTAP_ID_CERTIFICADO", nullable = false, length = 50)
	public String getIdCertificado() {
		return this.idCertificado;
	}
	public void setIdCertificado(String idCertificado) {
		this.idCertificado = idCertificado;
	}

	@Column(name = "VTAP_SISTEMA_ORIGEN", nullable = false, length = 10)
	public String getSistemaOrigen() {
		return this.sistemaOrigen;
	}
	public void setSistemaOrigen(String sistemaOrigen) {
		this.sistemaOrigen = sistemaOrigen;
	}

	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof VtaPrecargaId))
			return false;
		VtaPrecargaId castOther = (VtaPrecargaId) other;

		return (this.getCdSucursal() == castOther.getCdSucursal())
				&& (this.getCdRamo() == castOther.getCdRamo())
				&& (this.getNumPoliza() == castOther.getNumPoliza())
				&& ((this.getIdCertificado() == castOther
						.getIdCertificado()) || (this
						.getIdCertificado() != null
						&& castOther.getIdCertificado() != null && this
						.getIdCertificado().equals(
								castOther.getIdCertificado())))
				&& ((this.getSistemaOrigen() == castOther
						.getSistemaOrigen()) || (this
						.getSistemaOrigen() != null
						&& castOther.getSistemaOrigen() != null && this
						.getSistemaOrigen().equals(
								castOther.getSistemaOrigen())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getCdSucursal();
		result = 37 * result + this.getCdRamo();
		result = 37 * result + (int) this.getNumPoliza();
		result = 37
				* result
				+ (getIdCertificado() == null ? 0 : this
						.getIdCertificado().hashCode());
		result = 37
				* result
				+ (getSistemaOrigen() == null ? 0 : this
						.getSistemaOrigen().hashCode());
		return result;
	}

}
