package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * HomologaProdplanes generated by hbm2java
 */
@Entity
@Table(name = "HOMOLOGA_PRODPLANES")
public class HomologaProductosPlanes implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HomologaProductosPlanesId id;
	private String hoppDeProducto;
	private BigDecimal hoppSumaAsegurada;
	private String hoppDato1;
	private String hoppDato2;
	private Integer hoppDato3;
	private Long hoppDato4;
	private Date hoppDato5;
	private Date hoppDato6;
	private String hoppCdComision;
	private BigDecimal hoppPorcComision;
	private Plan plan;
	

	public HomologaProductosPlanes() {
	}

	public HomologaProductosPlanes(HomologaProductosPlanesId id) {
		this.id = id;
	}

	public HomologaProductosPlanes(HomologaProductosPlanesId id, 
			String hoppDeProducto,
			BigDecimal hoppSumaAsegurada, String hoppDato1, String hoppDato2,
			Integer hoppDato3, Long hoppDato4, Date hoppDato5, Date hoppDato6,
			String hoppCdComision, BigDecimal hoppPorcComision) {
		this.id = id;
		this.hoppDeProducto = hoppDeProducto;
		this.hoppSumaAsegurada = hoppSumaAsegurada;
		this.hoppDato1 = hoppDato1;
		this.hoppDato2 = hoppDato2;
		this.hoppDato3 = hoppDato3;
		this.hoppDato4 = hoppDato4;
		this.hoppDato5 = hoppDato5;
		this.hoppDato6 = hoppDato6;
		this.hoppCdComision = hoppCdComision;
		this.hoppPorcComision = hoppPorcComision;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "hoppCdRamo", column = @Column(name = "HOPP_CD_RAMO", nullable = false, precision = 2, scale = 0)),
			@AttributeOverride(name = "hoppProdBancoRector", column = @Column(name = "HOPP_PROD_BANCO_RECTOR", nullable = false, length = 6)),
			@AttributeOverride(name = "hoppPlanBancoRector", column = @Column(name = "HOPP_PLAN_BANCO_RECTOR", nullable = false, length = 6)),
			@AttributeOverride(name = "hoppProdColectivo", column = @Column(name = "HOPP_PROD_COLECTIVO", nullable = false, precision = 6, scale = 0)),
			@AttributeOverride(name = "hoppPlanColectivo", column = @Column(name = "HOPP_PLAN_COLECTIVO", nullable = false, precision = 6, scale = 0)) })
	public HomologaProductosPlanesId getId() {
		return this.id;
	}
	public void setId(HomologaProductosPlanesId id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
			@JoinColumn(name = "HOPP_CD_RAMO", referencedColumnName = "ALPL_CD_RAMO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "HOPP_PROD_COLECTIVO", referencedColumnName = "ALPL_CD_PRODUCTO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "HOPP_PLAN_COLECTIVO", referencedColumnName = "ALPL_CD_PLAN", nullable = false, insertable = false, updatable = false) })
	public Plan getPlan() {
		return this.plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	

	@Column(name = "HOPP_DE_PRODUCTO", length = 100)
	public String getHoppDeProducto() {
		return this.hoppDeProducto;
	}
	public void setHoppDeProducto(String hoppDeProducto) {
		this.hoppDeProducto = hoppDeProducto;
	}

	@Column(name = "HOPP_SUMA_ASEGURADA", precision = 12)
	public BigDecimal getHoppSumaAsegurada() {
		return this.hoppSumaAsegurada;
	}
	public void setHoppSumaAsegurada(BigDecimal hoppSumaAsegurada) {
		this.hoppSumaAsegurada = hoppSumaAsegurada;
	}
	
	@Column(name = "HOPP_DATO1", length = 15)
	public String getHoppDato1() {
		return this.hoppDato1;
	}
	public void setHoppDato1(String hoppDato1) {
		this.hoppDato1 = hoppDato1;
	}

	@Column(name = "HOPP_DATO2", length = 50)
	public String getHoppDato2() {
		return this.hoppDato2;
	}
	public void setHoppDato2(String hoppDato2) {
		this.hoppDato2 = hoppDato2;
	}

	@Column(name = "HOPP_DATO3", precision = 5, scale = 0)
	public Integer getHoppDato3() {
		return this.hoppDato3;
	}
	public void setHoppDato3(Integer hoppDato3) {
		this.hoppDato3 = hoppDato3;
	}

	@Column(name = "HOPP_DATO4", precision = 10, scale = 0)
	public Long getHoppDato4() {
		return this.hoppDato4;
	}
	public void setHoppDato4(Long hoppDato4) {
		this.hoppDato4 = hoppDato4;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "HOPP_DATO5", length = 7)
	public Date getHoppDato5() {
		return this.hoppDato5;
	}
	public void setHoppDato5(Date hoppDato5) {
		this.hoppDato5 = hoppDato5;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "HOPP_DATO6", length = 7)
	public Date getHoppDato6() {
		return this.hoppDato6;
	}
	public void setHoppDato6(Date hoppDato6) {
		this.hoppDato6 = hoppDato6;
	}

	@Column(name = "HOPP_CD_COMISION", length = 6)
	public String getHoppCdComision() {
		return this.hoppCdComision;
	}
	public void setHoppCdComision(String hoppCdComision) {
		this.hoppCdComision = hoppCdComision;
	}

	@Column(name = "HOPP_PORC_COMISION", precision = 6)
	public BigDecimal getHoppPorcComision() {
		return this.hoppPorcComision;
	}
	public void setHoppPorcComision(BigDecimal hoppPorcComision) {
		this.hoppPorcComision = hoppPorcComision;
	}
}
