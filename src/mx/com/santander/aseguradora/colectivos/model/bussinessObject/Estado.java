package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

// default package
// Generated 20/11/2009 01:20:17 PM by Hibernate Tools 3.2.2.GA


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Cart_Estados generated by hbm2java
 */
@Entity
@Table(name = "CART_ESTADOS")
public class Estado  {

	/**
	 * 
	 */
	@Id
	@Column(name = "CAES_CD_ESTADO")
	private Short caesCdEstado;
	@Column(name = "CAES_DE_ESTADO")
	private String caesDeEstado;
	@Column(name = "CAES_DS_CORTA_ESTADO")
	private String caesDsCortaEstado;


	public Estado() {
	}

	public Estado(Short caesCdEstado) {
		this.caesCdEstado = caesCdEstado;
	}


	public Short getCaesCdEstado() {
		return caesCdEstado;
	}

	public void setCaesCdEstado(Short caesCdEstado) {
		this.caesCdEstado = caesCdEstado;
	}

	public String getCaesDeEstado() {
		return this.caesDeEstado;
	}

	public void setCaesDeEstado(String caesDeEstado) {
		this.caesDeEstado = caesDeEstado;
	}

	
	public String getCaesDsCortaEstado() {
		return this.caesDsCortaEstado;
	}

	public void setCaesDsCortaEstado(String caesDsCortaEstado) {
		this.caesDsCortaEstado = caesDsCortaEstado;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.caesCdEstado+ " -> " + this.caesDeEstado;
	}

}
