package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CART_CIUDADES database table.
 * 
 */
@Embeddable
public class CartCiudadesPK implements Serializable {
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name="CACI_CAES_CD_ESTADO")
    private long caciCaesCdEstado;

    @Column(name="CACI_CD_CIUDAD")
    private long caciCdCiudad;

    public CartCiudadesPK() {
    }

    public long getCaciCaesCdEstado() {
            return this.caciCaesCdEstado;
    }
    public void setCaciCaesCdEstado(long caciCaesCdEstado) {
            this.caciCaesCdEstado = caciCaesCdEstado;
    }
    public long getCaciCdCiudad() {
            return this.caciCdCiudad;
    }
    public void setCaciCdCiudad(long caciCdCiudad) {
            this.caciCdCiudad = caciCdCiudad;
    }

    public boolean equals(Object other) {
            if (this == other) {
                    return true;
            }
            if (!(other instanceof CartCiudadesPK)) {
                    return false;
            }
            CartCiudadesPK castOther = (CartCiudadesPK)other;
            return 
                    (this.caciCaesCdEstado == castOther.caciCaesCdEstado)
                    && (this.caciCdCiudad == castOther.caciCdCiudad);

    }

    public int hashCode() {
            final int prime = 31;
            int hash = 17;
            hash = hash * prime + ((int) (this.caciCaesCdEstado ^ (this.caciCaesCdEstado >>> 32)));
            hash = hash * prime + ((int) (this.caciCdCiudad ^ (this.caciCdCiudad >>> 32)));
            
            return hash;
    }
}
