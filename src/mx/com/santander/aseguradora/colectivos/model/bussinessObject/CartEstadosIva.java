package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the CART_ESTADOS_IVA database table.
 * 
 */
@Entity
@Table(name="CART_ESTADOS_IVA")
public class CartEstadosIva implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CartEstadosIvaPK id;

	@Column(name="CAIV_ACTUA_REG")
	private String caivActuaReg;

	@Column(name="CAIV_CD_ASENTA")
	private BigDecimal caivCdAsenta;

	@Column(name="CAIV_CD_CIUDAD")
	private BigDecimal caivCdCiudad;

	@Column(name="CAIV_CD_COLONIA")
	private BigDecimal caivCdColonia;

	@Column(name="CAIV_DESCUENTO")
	private BigDecimal caivDescuento;

    @Temporal( TemporalType.DATE)
	@Column(name="CAIV_FE_ACTUALIZA")
	private Date caivFeActualiza;

    @Temporal( TemporalType.DATE)
	@Column(name="CAIV_FE_DESDE")
	private Date caivFeDesde;

    @Temporal( TemporalType.DATE)
	@Column(name="CAIV_FE_HASTA")
	private Date caivFeHasta;

	@Column(name="CAIV_IVA")
	private BigDecimal caivIva;

	@Column(name="CAIV_MARCA_EMISION")
	private String caivMarcaEmision;

	@Column(name="CAIV_ZONA_A")
	private String caivZonaA;

	@Column(name="CAIV_ZONA_B")
	private String caivZonaB;

	@Column(name="CAIV_ZONA_C")
	private String caivZonaC;

	@Column(name="CAIV_ZONA_D")
	private String caivZonaD;

	@Column(name="CAIV_ZONA_HIDRO")
	private String caivZonaHidro;

	@Column(name="CAIV_ZONA_HURAC")
	private String caivZonaHurac;

	@Column(name="CAIV_ZONA_TERRE_ALF")
	private String caivZonaTerreAlf;

	@Column(name="CAIV_ZONA_TERRE_NUM")
	private String caivZonaTerreNum;

    public CartEstadosIva() {
    }

	public CartEstadosIvaPK getId() {
		return this.id;
	}

	public void setId(CartEstadosIvaPK id) {
		this.id = id;
	}
	
	public String getCaivActuaReg() {
		return this.caivActuaReg;
	}

	public void setCaivActuaReg(String caivActuaReg) {
		this.caivActuaReg = caivActuaReg;
	}

	public BigDecimal getCaivCdAsenta() {
		return this.caivCdAsenta;
	}

	public void setCaivCdAsenta(BigDecimal caivCdAsenta) {
		this.caivCdAsenta = caivCdAsenta;
	}

	public BigDecimal getCaivCdCiudad() {
		return this.caivCdCiudad;
	}

	public void setCaivCdCiudad(BigDecimal caivCdCiudad) {
		this.caivCdCiudad = caivCdCiudad;
	}

	public BigDecimal getCaivCdColonia() {
		return this.caivCdColonia;
	}

	public void setCaivCdColonia(BigDecimal caivCdColonia) {
		this.caivCdColonia = caivCdColonia;
	}

	public BigDecimal getCaivDescuento() {
		return this.caivDescuento;
	}

	public void setCaivDescuento(BigDecimal caivDescuento) {
		this.caivDescuento = caivDescuento;
	}

	public Date getCaivFeActualiza() {
		return this.caivFeActualiza;
	}

	public void setCaivFeActualiza(Date caivFeActualiza) {
		this.caivFeActualiza = caivFeActualiza;
	}

	public Date getCaivFeDesde() {
		return this.caivFeDesde;
	}

	public void setCaivFeDesde(Date caivFeDesde) {
		this.caivFeDesde = caivFeDesde;
	}

	public Date getCaivFeHasta() {
		return this.caivFeHasta;
	}

	public void setCaivFeHasta(Date caivFeHasta) {
		this.caivFeHasta = caivFeHasta;
	}

	public BigDecimal getCaivIva() {
		return this.caivIva;
	}

	public void setCaivIva(BigDecimal caivIva) {
		this.caivIva = caivIva;
	}

	public String getCaivMarcaEmision() {
		return this.caivMarcaEmision;
	}

	public void setCaivMarcaEmision(String caivMarcaEmision) {
		this.caivMarcaEmision = caivMarcaEmision;
	}

	public String getCaivZonaA() {
		return this.caivZonaA;
	}

	public void setCaivZonaA(String caivZonaA) {
		this.caivZonaA = caivZonaA;
	}

	public String getCaivZonaB() {
		return this.caivZonaB;
	}

	public void setCaivZonaB(String caivZonaB) {
		this.caivZonaB = caivZonaB;
	}

	public String getCaivZonaC() {
		return this.caivZonaC;
	}

	public void setCaivZonaC(String caivZonaC) {
		this.caivZonaC = caivZonaC;
	}

	public String getCaivZonaD() {
		return this.caivZonaD;
	}

	public void setCaivZonaD(String caivZonaD) {
		this.caivZonaD = caivZonaD;
	}

	public String getCaivZonaHidro() {
		return this.caivZonaHidro;
	}

	public void setCaivZonaHidro(String caivZonaHidro) {
		this.caivZonaHidro = caivZonaHidro;
	}

	public String getCaivZonaHurac() {
		return this.caivZonaHurac;
	}

	public void setCaivZonaHurac(String caivZonaHurac) {
		this.caivZonaHurac = caivZonaHurac;
	}

	public String getCaivZonaTerreAlf() {
		return this.caivZonaTerreAlf;
	}

	public void setCaivZonaTerreAlf(String caivZonaTerreAlf) {
		this.caivZonaTerreAlf = caivZonaTerreAlf;
	}

	public String getCaivZonaTerreNum() {
		return this.caivZonaTerreNum;
	}

	public void setCaivZonaTerreNum(String caivZonaTerreNum) {
		this.caivZonaTerreNum = caivZonaTerreNum;
	}

}