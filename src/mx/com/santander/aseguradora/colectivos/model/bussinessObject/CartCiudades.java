package mx.com.santander.aseguradora.colectivos.model.bussinessObject;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CART_CIUDADES database table.
 * 
 */
@Entity
@Table(name="CART_CIUDADES")
public class CartCiudades implements Serializable {
        private static final long serialVersionUID = 1L;

        @EmbeddedId
        private CartCiudadesPK id;

        @Column(name="CACI_CD_ZONA_SISMICA")
        private BigDecimal caciCdZonaSismica;

        @Column(name="CACI_DE_CIUDAD")
        private String caciDeCiudad;

        

    public CartCiudades() {
    }

        public CartCiudadesPK getId() {
                return this.id;
        }

        public void setId(CartCiudadesPK id) {
                this.id = id;
        }
        
        public BigDecimal getCaciCdZonaSismica() {
                return this.caciCdZonaSismica;
        }

        public void setCaciCdZonaSismica(BigDecimal caciCdZonaSismica) {
                this.caciCdZonaSismica = caciCdZonaSismica;
        }

        public String getCaciDeCiudad() {
                return this.caciDeCiudad;
        }

        public void setCaciDeCiudad(String caciDeCiudad) {
                this.caciDeCiudad = caciDeCiudad;
        }   
}
