package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;

public class EndosoDatos2DTO extends EndosoDatos3DTO implements java.io.Serializable{

	//Implements Serializable
	private static final long serialVersionUID = 6296120924632342029L;

	//Declara propiedades BigDecimal
		private BigDecimal cedaNuRecibo;
		private BigDecimal cedaSumaNvo;
		private BigDecimal cedaSumaAnt;
		private BigDecimal cedaCampon2;
		private BigDecimal cedaCampon3;
		
		
		/**
		 * @return the cedaNuRecibo
		 */
		public BigDecimal getCedaNuRecibo() {
			return cedaNuRecibo;
		}

		/**
		 * @param cedaNuRecibo the cedaNuRecibo to set
		 */
		public void setCedaNuRecibo(BigDecimal cedaNuRecibo) {
			this.cedaNuRecibo = cedaNuRecibo;
		}
	
		
		/**
		 * @return the cedaSumaNvo
		 */
		public BigDecimal getCedaSumaNvo() {
			return cedaSumaNvo;
		}

		/**
		 * @param cedaSumaNvo the cedaSumaNvo to set
		 */
		public void setCedaSumaNvo(BigDecimal cedaSumaNvo) {
			this.cedaSumaNvo = cedaSumaNvo;
		}
		
		/**
		 * @return the cedaSumaAnt
		 */
		public BigDecimal getCedaSumaAnt() {
			return cedaSumaAnt;
		}

		/**
		 * @param cedaSumaAnt the cedaSumaAnt to set
		 */
		public void setCedaSumaAnt(BigDecimal cedaSumaAnt) {
			this.cedaSumaAnt = cedaSumaAnt;
		}


		/**
		 * @return the cedaCampon2
		 */
		public BigDecimal getCedaCampon2() {
			return cedaCampon2;
		}

		/**
		 * @param cedaCampon2 the cedaCampon2 to set
		 */
		public void setCedaCampon2(BigDecimal cedaCampon2) {
			this.cedaCampon2 = cedaCampon2;
		}

		/**
		 * @return the cedaCampon3
		 */
		public BigDecimal getCedaCampon3() {
			return cedaCampon3;
		}

		/**
		 * @param cedaCampon3 the cedaCampon3 to set
		 */
		public void setCedaCampon3(BigDecimal cedaCampon3) {
			this.cedaCampon3 = cedaCampon3;
		}

}
