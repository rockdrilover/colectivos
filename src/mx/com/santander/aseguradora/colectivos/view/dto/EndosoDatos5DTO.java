package mx.com.santander.aseguradora.colectivos.view.dto;

public class EndosoDatos5DTO implements java.io.Serializable{

	//Implements Serializable
	private static final long serialVersionUID = -6248068994323908746L;

	
	//Declara propiedades String
	private String cedaParentescoNvo;
	private String cedaCorreoNvo;	
	private String cedaNombreAnt;
	private String cedaApAnt;
	private String cedaAmAnt;
	private String cedaRfcAnt;
	private String cedaDireccionAnt;
	private String cedasexoAnt;
	private String cedaParentescoAnt;
	private String cedaCorreoAnt;
	
	/**
	 * @return the cedaParentescoNvo
	 */
	public String getCedaParentescoNvo() {
		return cedaParentescoNvo;
	}

	/**
	 * @param cedaParentescoNvo the cedaParentescoNvo to set
	 */
	public void setCedaParentescoNvo(String cedaParentescoNvo) {
		this.cedaParentescoNvo = cedaParentescoNvo;
	}

	

	/**
	 * @return the cedaCorreoNvo
	 */
	public String getCedaCorreoNvo() {
		return cedaCorreoNvo;
	}

	/**
	 * @param cedaCorreoNvo the cedaCorreoNvo to set
	 */
	public void setCedaCorreoNvo(String cedaCorreoNvo) {
		this.cedaCorreoNvo = cedaCorreoNvo;
	}

	

	/**
	 * @return the cedaNombreAnt
	 */
	public String getCedaNombreAnt() {
		return cedaNombreAnt;
	}

	/**
	 * @param cedaNombreAnt the cedaNombreAnt to set
	 */
	public void setCedaNombreAnt(String cedaNombreAnt) {
		this.cedaNombreAnt = cedaNombreAnt;
	}

	/**
	 * @return the cedaApAnt
	 */
	public String getCedaApAnt() {
		return cedaApAnt;
	}

	/**
	 * @param cedaApAnt the cedaApAnt to set
	 */
	public void setCedaApAnt(String cedaApAnt) {
		this.cedaApAnt = cedaApAnt;
	}

	/**
	 * @return the cedaAmAnt
	 */
	public String getCedaAmAnt() {
		return cedaAmAnt;
	}

	/**
	 * @param cedaAmAnt the cedaAmAnt to set
	 */
	public void setCedaAmAnt(String cedaAmAnt) {
		this.cedaAmAnt = cedaAmAnt;
	}

	/**
	 * @return the cedaRfcAnt
	 */
	public String getCedaRfcAnt() {
		return cedaRfcAnt;
	}

	/**
	 * @param cedaRfcAnt the cedaRfcAnt to set
	 */
	public void setCedaRfcAnt(String cedaRfcAnt) {
		this.cedaRfcAnt = cedaRfcAnt;
	}

	/**
	 * @return the cedaDireccionAnt
	 */
	public String getCedaDireccionAnt() {
		return cedaDireccionAnt;
	}

	/**
	 * @param cedaDireccionAnt the cedaDireccionAnt to set
	 */
	public void setCedaDireccionAnt(String cedaDireccionAnt) {
		this.cedaDireccionAnt = cedaDireccionAnt;
	}

	/**
	 * @return the cedasexoAnt
	 */
	public String getCedasexoAnt() {
		return cedasexoAnt;
	}

	/**
	 * @param cedasexoAnt the cedasexoAnt to set
	 */
	public void setCedasexoAnt(String cedasexoAnt) {
		this.cedasexoAnt = cedasexoAnt;
	}

	/**
	 * @return the cedaParentescoAnt
	 */
	public String getCedaParentescoAnt() {
		return cedaParentescoAnt;
	}

	/**
	 * @param cedaParentescoAnt the cedaParentescoAnt to set
	 */
	public void setCedaParentescoAnt(String cedaParentescoAnt) {
		this.cedaParentescoAnt = cedaParentescoAnt;
	}

	

	/**
	 * @return the cedaCorreoAnt
	 */
	public String getCedaCorreoAnt() {
		return cedaCorreoAnt;
	}

	/**
	 * @param cedaCorreoAnt the cedaCorreoAnt to set
	 */
	public void setCedaCorreoAnt(String cedaCorreoAnt) {
		this.cedaCorreoAnt = cedaCorreoAnt;
	}
}
