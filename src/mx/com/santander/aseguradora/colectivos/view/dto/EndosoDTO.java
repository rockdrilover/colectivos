package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososId;

public class EndosoDTO implements java.io.Serializable {

	private static final long serialVersionUID = 6585309600917444604L;
	private EndososId id;
	private String coedIdCertificado;
	private int coedCapjCdSucursal;
	private Date coedFeSolicitud;
	private Date coedFeRecepcion;
	private Date coedFeAplica;
	private String coedCdUsuarioReg;
	private String coedInformacionAnt;
	private String coedNuFolioCorreo;
	private String coedNuFolioEntrada;
	private String coedCdUsuarioAplica;
	private Long coedNuSolicitud;
	private BigDecimal coedMtoNvo;
	private String coedNombreNvo;
	private String coedApNvo;
	private String coedAmNvo;
	private Short coedPjParticipaNvo;
	private Short coedSecBeneNvo;
	private String coedParentescoNvo;
	private BigDecimal coedMtoAnt;
	private String coedNombreAnt;
	private String coedApAnt;
	private String coedAmAnt;
	private Short coedPjParticipaAnt;
	private Short coedSecBeneAnt;
	private String coedParentescoAnt;
	private String coedCampov1;
	private String coedCampov2;
	private String coedCampov3;
	private String coedCampov4;
	private String coedCampov5;
	private BigDecimal coedNuRecibo;
	private Integer coedCampon1;
	private BigDecimal coedCampon2;
	private BigDecimal coedCampon3;
	private BigDecimal coedCampon4;
	private BigDecimal coedCampon5;
	private Date coedFechaNvo;
	private Date coedFechaAnt;
	private BigDecimal coedNuReciboAnt;
	private Date coedFechaInicio;
	private Date coedFechaFin;

	public EndosoDTO() {
		id = new EndososId();
	}

	public EndosoDTO(EndososId id, String coedIdCertificado,
			int coedCapjCdSucursal, Date coedFeSolicitud, Date coedFeAplica,
			String coedCdUsuarioAplica, BigDecimal coedNuRecibo) {
		this.id = id;
		this.coedIdCertificado = coedIdCertificado;
		this.coedCapjCdSucursal = coedCapjCdSucursal;
		this.coedFeSolicitud = coedFeSolicitud;
		this.coedFeAplica = coedFeAplica;
		this.coedCdUsuarioAplica = coedCdUsuarioAplica;
		this.coedNuRecibo = coedNuRecibo;
	}

	public EndosoDTO(EndososId id, String coedIdCertificado,
			int coedCapjCdSucursal, Date coedFeSolicitud, Date coedFeRecepcion,
			Date coedFeAplica, String coedCdUsuarioReg,
			String coedInformacionAnt, String coedNuFolioCorreo,
			String coedNuFolioEntrada, String coedCdUsuarioAplica,
			Long coedNuSolicitud, BigDecimal coedMtoNvo, String coedNombreNvo,
			String coedApNvo, String coedAmNvo, Short coedPjParticipaNvo,
			Short coedSecBeneNvo, String coedParentescoNvo,
			BigDecimal coedMtoAnt, String coedNombreAnt, String coedApAnt,
			String coedAmAnt, Short coedPjParticipaAnt, Short coedSecBeneAnt,
			String coedParentescoAnt, String coedCampov1, String coedCampov2,
			String coedCampov3, String coedCampov4, String coedCampov5,
			BigDecimal coedNuRecibo, Integer coedCampon1,
			BigDecimal coedCampon2, BigDecimal coedCampon3,
			BigDecimal coedCampon4, BigDecimal coedCampon5, Date coedFechaNvo,
			Date coedFechaAnt, BigDecimal coedNuReciboAnt,
			Date coedFechaInicio, Date coedFechaFin) {
		this.id = id;
		this.coedIdCertificado = coedIdCertificado;
		this.coedCapjCdSucursal = coedCapjCdSucursal;
		this.coedFeSolicitud = coedFeSolicitud;
		this.coedFeRecepcion = coedFeRecepcion;
		this.coedFeAplica = coedFeAplica;
		this.coedCdUsuarioReg = coedCdUsuarioReg;
		this.coedInformacionAnt = coedInformacionAnt;
		this.coedNuFolioCorreo = coedNuFolioCorreo;
		this.coedNuFolioEntrada = coedNuFolioEntrada;
		this.coedCdUsuarioAplica = coedCdUsuarioAplica;
		this.coedNuSolicitud = coedNuSolicitud;
		this.coedMtoNvo = coedMtoNvo;
		this.coedNombreNvo = coedNombreNvo;
		this.coedApNvo = coedApNvo;
		this.coedAmNvo = coedAmNvo;
		this.coedPjParticipaNvo = coedPjParticipaNvo;
		this.coedSecBeneNvo = coedSecBeneNvo;
		this.coedParentescoNvo = coedParentescoNvo;
		this.coedMtoAnt = coedMtoAnt;
		this.coedNombreAnt = coedNombreAnt;
		this.coedApAnt = coedApAnt;
		this.coedAmAnt = coedAmAnt;
		this.coedPjParticipaAnt = coedPjParticipaAnt;
		this.coedSecBeneAnt = coedSecBeneAnt;
		this.coedParentescoAnt = coedParentescoAnt;
		this.coedCampov1 = coedCampov1;
		this.coedCampov2 = coedCampov2;
		this.coedCampov3 = coedCampov3;
		this.coedCampov4 = coedCampov4;
		this.coedCampov5 = coedCampov5;
		this.coedNuRecibo = coedNuRecibo;
		this.coedCampon1 = coedCampon1;
		this.coedCampon2 = coedCampon2;
		this.coedCampon3 = coedCampon3;
		this.coedCampon4 = coedCampon4;
		this.coedCampon5 = coedCampon5;
		this.coedFechaNvo = coedFechaNvo;
		this.coedFechaAnt = coedFechaAnt;
		this.coedNuReciboAnt = coedNuReciboAnt;
		this.coedFechaInicio = coedFechaInicio;
		this.coedFechaFin = coedFechaFin;
	}

	public EndososId getId() {
		return this.id;
	}
	public void setId(EndososId id) {
		this.id = id;
	}
	public String getCoedIdCertificado() {
		return this.coedIdCertificado;
	}
	public void setCoedIdCertificado(String coedIdCertificado) {
		this.coedIdCertificado = coedIdCertificado;
	}
	public int getCoedCapjCdSucursal() {
		return this.coedCapjCdSucursal;
	}
	public void setCoedCapjCdSucursal(int coedCapjCdSucursal) {
		this.coedCapjCdSucursal = coedCapjCdSucursal;
	}
	public Date getCoedFeSolicitud() {
		return this.coedFeSolicitud;
	}
	public void setCoedFeSolicitud(Date coedFeSolicitud) {
		this.coedFeSolicitud = coedFeSolicitud;
	}
	public Date getCoedFeRecepcion() {
		return this.coedFeRecepcion;
	}
	public void setCoedFeRecepcion(Date coedFeRecepcion) {
		this.coedFeRecepcion = coedFeRecepcion;
	}
	public Date getCoedFeAplica() {
		return this.coedFeAplica;
	}
	public void setCoedFeAplica(Date coedFeAplica) {
		this.coedFeAplica = coedFeAplica;
	}
	public String getCoedCdUsuarioReg() {
		return this.coedCdUsuarioReg;
	}
	public void setCoedCdUsuarioReg(String coedCdUsuarioReg) {
		this.coedCdUsuarioReg = coedCdUsuarioReg;
	}
	public String getCoedInformacionAnt() {
		return this.coedInformacionAnt;
	}
	public void setCoedInformacionAnt(String coedInformacionAnt) {
		this.coedInformacionAnt = coedInformacionAnt;
	}
	public String getCoedNuFolioCorreo() {
		return this.coedNuFolioCorreo;
	}
	public void setCoedNuFolioCorreo(String coedNuFolioCorreo) {
		this.coedNuFolioCorreo = coedNuFolioCorreo;
	}
	public String getCoedNuFolioEntrada() {
		return this.coedNuFolioEntrada;
	}
	public void setCoedNuFolioEntrada(String coedNuFolioEntrada) {
		this.coedNuFolioEntrada = coedNuFolioEntrada;
	}
	public String getCoedCdUsuarioAplica() {
		return this.coedCdUsuarioAplica;
	}
	public void setCoedCdUsuarioAplica(String coedCdUsuarioAplica) {
		this.coedCdUsuarioAplica = coedCdUsuarioAplica;
	}
	public Long getCoedNuSolicitud() {
		return this.coedNuSolicitud;
	}
	public void setCoedNuSolicitud(Long coedNuSolicitud) {
		this.coedNuSolicitud = coedNuSolicitud;
	}
	public BigDecimal getCoedMtoNvo() {
		return this.coedMtoNvo;
	}
	public void setCoedMtoNvo(BigDecimal coedMtoNvo) {
		this.coedMtoNvo = coedMtoNvo;
	}
	public String getCoedNombreNvo() {
		return this.coedNombreNvo;
	}
	public void setCoedNombreNvo(String coedNombreNvo) {
		this.coedNombreNvo = coedNombreNvo;
	}
	public String getCoedApNvo() {
		return this.coedApNvo;
	}
	public void setCoedApNvo(String coedApNvo) {
		this.coedApNvo = coedApNvo;
	}
	public String getCoedAmNvo() {
		return this.coedAmNvo;
	}
	public void setCoedAmNvo(String coedAmNvo) {
		this.coedAmNvo = coedAmNvo;
	}
	public Short getCoedPjParticipaNvo() {
		return this.coedPjParticipaNvo;
	}
	public void setCoedPjParticipaNvo(Short coedPjParticipaNvo) {
		this.coedPjParticipaNvo = coedPjParticipaNvo;
	}
	public Short getCoedSecBeneNvo() {
		return this.coedSecBeneNvo;
	}
	public void setCoedSecBeneNvo(Short coedSecBeneNvo) {
		this.coedSecBeneNvo = coedSecBeneNvo;
	}
	public String getCoedParentescoNvo() {
		return this.coedParentescoNvo;
	}
	public void setCoedParentescoNvo(String coedParentescoNvo) {
		this.coedParentescoNvo = coedParentescoNvo;
	}
	public BigDecimal getCoedMtoAnt() {
		return this.coedMtoAnt;
	}
	public void setCoedMtoAnt(BigDecimal coedMtoAnt) {
		this.coedMtoAnt = coedMtoAnt;
	}
	public String getCoedNombreAnt() {
		return this.coedNombreAnt;
	}
	public void setCoedNombreAnt(String coedNombreAnt) {
		this.coedNombreAnt = coedNombreAnt;
	}
	public String getCoedApAnt() {
		return this.coedApAnt;
	}
	public void setCoedApAnt(String coedApAnt) {
		this.coedApAnt = coedApAnt;
	}
	public String getCoedAmAnt() {
		return this.coedAmAnt;
	}
	public void setCoedAmAnt(String coedAmAnt) {
		this.coedAmAnt = coedAmAnt;
	}
	public Short getCoedPjParticipaAnt() {
		return this.coedPjParticipaAnt;
	}
	public void setCoedPjParticipaAnt(Short coedPjParticipaAnt) {
		this.coedPjParticipaAnt = coedPjParticipaAnt;
	}
	public Short getCoedSecBeneAnt() {
		return this.coedSecBeneAnt;
	}
	public void setCoedSecBeneAnt(Short coedSecBeneAnt) {
		this.coedSecBeneAnt = coedSecBeneAnt;
	}
	public String getCoedParentescoAnt() {
		return this.coedParentescoAnt;
	}
	public void setCoedParentescoAnt(String coedParentescoAnt) {
		this.coedParentescoAnt = coedParentescoAnt;
	}
	public String getCoedCampov1() {
		return this.coedCampov1;
	}
	public void setCoedCampov1(String coedCampov1) {
		this.coedCampov1 = coedCampov1;
	}
	public String getCoedCampov2() {
		return this.coedCampov2;
	}
	public void setCoedCampov2(String coedCampov2) {
		this.coedCampov2 = coedCampov2;
	}
	public String getCoedCampov3() {
		return this.coedCampov3;
	}
	public void setCoedCampov3(String coedCampov3) {
		this.coedCampov3 = coedCampov3;
	}
	public String getCoedCampov4() {
		return this.coedCampov4;
	}
	public void setCoedCampov4(String coedCampov4) {
		this.coedCampov4 = coedCampov4;
	}
	public String getCoedCampov5() {
		return this.coedCampov5;
	}
	public void setCoedCampov5(String coedCampov5) {
		this.coedCampov5 = coedCampov5;
	}
	public BigDecimal getCoedNuRecibo() {
		return this.coedNuRecibo;
	}
	public void setCoedNuRecibo(BigDecimal coedNuRecibo) {
		this.coedNuRecibo = coedNuRecibo;
	}
	public Integer getCoedCampon1() {
		return this.coedCampon1;
	}
	public void setCoedCampon1(Integer coedCampon1) {
		this.coedCampon1 = coedCampon1;
	}
	public BigDecimal getCoedCampon2() {
		return this.coedCampon2;
	}
	public void setCoedCampon2(BigDecimal coedCampon2) {
		this.coedCampon2 = coedCampon2;
	}
	public BigDecimal getCoedCampon3() {
		return this.coedCampon3;
	}
	public void setCoedCampon3(BigDecimal coedCampon3) {
		this.coedCampon3 = coedCampon3;
	}
	public BigDecimal getCoedCampon4() {
		return this.coedCampon4;
	}
	public void setCoedCampon4(BigDecimal coedCampon4) {
		this.coedCampon4 = coedCampon4;
	}
	public BigDecimal getCoedCampon5() {
		return this.coedCampon5;
	}
	public void setCoedCampon5(BigDecimal coedCampon5) {
		this.coedCampon5 = coedCampon5;
	}
	public Date getCoedFechaNvo() {
		return this.coedFechaNvo;
	}
	public void setCoedFechaNvo(Date coedFechaNvo) {
		this.coedFechaNvo = coedFechaNvo;
	}
	public Date getCoedFechaAnt() {
		return this.coedFechaAnt;
	}
	public void setCoedFechaAnt(Date coedFechaAnt) {
		this.coedFechaAnt = coedFechaAnt;
	}
	public BigDecimal getCoedNuReciboAnt() {
		return this.coedNuReciboAnt;
	}
	public void setCoedNuReciboAnt(BigDecimal coedNuReciboAnt) {
		this.coedNuReciboAnt = coedNuReciboAnt;
	}
	public Date getCoedFechaInicio() {
		return this.coedFechaInicio;
	}
	public void setCoedFechaInicio(Date coedFechaInicio) {
		this.coedFechaInicio = coedFechaInicio;
	}
	public Date getCoedFechaFin() {
		return this.coedFechaFin;
	}
	public void setCoedFechaFin(Date coedFechaFin) {
		this.coedFechaFin = coedFechaFin;
	}
}
