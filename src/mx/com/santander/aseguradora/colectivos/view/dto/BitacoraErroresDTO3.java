package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bo.BitacoraErrores;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_BITACORA_ERRORES. 
 * 				La ocupa la clase BitacoraErroresDTO2
 * 				Extiende de la clase BitacoraErroresDTO4
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BitacoraErroresDTO3 extends BitacoraErroresDTO4 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coerIdVenta
	private BigDecimal coerIdVenta;
	//Propiedad para campo coerLinea
	private String coerLinea;	
	//Propiedad para campo coerNuCredito
	private String coerNuCredito;
	//Propiedad para campo coerNvalor1
	private BigDecimal coerNvalor1;
	//Propiedad para campo coerFvalor2
	@SuppressWarnings("unused")
	private Date coerFvalor2;
	
	/**
	 * Constructor de clase
	 * @param detalle datos de bitacora errores
	 */
	public BitacoraErroresDTO3(BitacoraErrores detalle) {
		super(detalle);
		this.coerIdVenta = detalle.getCoerIdVenta();
		this.coerLinea = detalle.getBe2().getCoerLinea();
		this.coerNuCredito = detalle.getBe2().getCoerNuCredito();
		this.coerNvalor1 = detalle.getBe2().getCoerNvalor1();
		this.coerFvalor2 = detalle.getBe2().getCoerFvalor2();
		
	}
	/**
	 * Constructor de clase
	 */
	public BitacoraErroresDTO3() {
		super();
		coerFvalor2 = new Date();
	}
	/**
	 * @return the coerIdVenta
	 */
	public BigDecimal getCoerIdVenta() {
		return coerIdVenta;
	}

	/**
	 * @param coerIdVenta the coerIdVenta to set
	 */
	public void setCoerIdVenta(BigDecimal coerIdVenta) {
		this.coerIdVenta = coerIdVenta;
	}

	/**
	 * @return the coerLinea
	 */
	public String getCoerLinea() {
		return coerLinea;
	}

	/**
	 * @param coerLinea the coerLinea to set
	 */
	public void setCoerLinea(String coerLinea) {
		this.coerLinea = coerLinea;
	}

	/**
	 * @return the coerNuCredito
	 */
	public String getCoerNuCredito() {
		return coerNuCredito;
	}

	/**
	 * @param coerNuCredito the coerNuCredito to set
	 */
	public void setCoerNuCredito(String coerNuCredito) {
		this.coerNuCredito = coerNuCredito;
	}

	/**
	 * @return the coerNvalor1
	 */
	public BigDecimal getCoerNvalor1() {
		return coerNvalor1;
	}

	/**
	 * @param coerNvalor1 the coerNvalor1 to set
	 */
	public void setCoerNvalor1(BigDecimal coerNvalor1) {
		this.coerNvalor1 = coerNvalor1;
	}
	
}
	