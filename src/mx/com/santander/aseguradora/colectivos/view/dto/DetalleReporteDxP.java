package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

public class DetalleReporteDxP extends DetalleReporteDxpParams {
	
	//														//Contiene la informaci�n necesaria para generar el
	//														//		detalle del Reporte-Tecnico Deudor por Prima (Dxp).
	
	
	//														//DxP No Exigible del Mes.
	private BigDecimal bdecimalDxPNEM;
	//														//Total DxP del Mes.
	private BigDecimal bdecimalTotalDXP;
	//														//Monto Cobrado Prima Total.
	private BigDecimal bdecimalMCPT;
	//														//Emisi�n de Prima Anualizada.
	private BigDecimal bdecimalEPATOTAL;
	
	//-----------------------------------------------------------------------------------------------------------------
	/*Getters y Setters*/
	//-----------------------------------------------------------------------------------------------------------------
	
	
	public BigDecimal getBdecimalDxPNEM() {
		return bdecimalDxPNEM;
	}
	
	public BigDecimal getBdecimalMCPT() {
		return bdecimalMCPT;
	}

	public BigDecimal getBdecimalTotalDXP() {
		return bdecimalTotalDXP;
	}

	public BigDecimal getBdecimalEPATOTAL() {
		return bdecimalEPATOTAL;
	}

	
	public void setBdecimalEP(BigDecimal bdecimalEP) {
		this.bdecimalEP = bdecimalEP;
	}

	//-----------------------------------------------------------------------------------------------------------------
	/* Constructores */
	//-----------------------------------------------------------------------------------------------------------------
	public DetalleReporteDxP(
			//												//Genera un objeto a partir de la informaci�n de la DB.
			
			//												//Array con los datos necesarios para generar el detalleDxP
			//												//		Su tama�o debe ser 12 elementos.
			Object[] arrobjData
	) throws Excepciones{
		//													//Validamos la cantidad de datos recibidos.
		if (
				arrobjData.length != 13
		) {
			throw new Excepciones("No es posible generar el objeto DetalleReporteDxP", 
					new Exception("La cantidad parametros para generar el objeto debe ser 13, se han rcibido: " + 
							arrobjData.length ));
		}
		
		this.strMes =  (String) arrobjData[0];
		this.shortCuota = ((BigDecimal)arrobjData [1]).shortValue();
		this.bdecimalFMPN = (BigDecimal) arrobjData[2];
		this.bdecimalMCPN = (BigDecimal) arrobjData [3];	
		this.bdecimalEVN = (BigDecimal) arrobjData[4];
		this.bdecimalEDS = (BigDecimal) arrobjData [5];	
		this.bdecimalECPN = (BigDecimal) arrobjData[6];
		this.bdecimalEPA = (BigDecimal) arrobjData [7];	
		this.bdecimalEPATOTAL = (BigDecimal) arrobjData [8];	
		this.bdecimalMCPT = (BigDecimal) arrobjData[9];
		this.bdecimalDxPEM = (BigDecimal) arrobjData [10];	
		this.bdecimalDxPNEM = (BigDecimal) arrobjData[11];
		this.bdecimalTotalDXP = (BigDecimal) arrobjData [12];
	}
	
	// -----------------------------------------------------------------------------------------------------------------

}
