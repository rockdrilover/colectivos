package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCedula;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CEDULA.
 * 				La ocupa la clase CedulaControlDTO4
 * 				Extiende de la clase CedulaControlDTO6
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CedulaControlDTO5 extends CedulaControlDTO6 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccFeProceso
	private Date coccFeProceso;
	//Propiedad para campo coccNuRegistros
	private BigDecimal coccNuRegistros;
	//Propiedad para campo coccNuRegSinpndNook
	private BigDecimal coccNuRegSinpndNook;
	//Propiedad para campo coccArchivo
	private String coccArchivo;

	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cedula objeto con datos
	 */
	public CedulaControlDTO5(ColectivosCedula cedula) {
		super(cedula);
		this.coccFeProceso = cedula.getCc3().getCoccFeProceso();
		this.coccNuRegistros = (BigDecimal) Utilerias.objectIsNull(cedula.getCc3().getCoccNuRegistros(), Constantes.TIPO_DATO_BIGDECIMAL);
		this.coccNuRegSinpndNook = (BigDecimal) Utilerias.objectIsNull(cedula.getCc3().getCoccNuRegSinpndNook(), Constantes.TIPO_DATO_BIGDECIMAL);
		this.coccArchivo = cedula.getCc3().getCoccArchivo();
	}
	/**
	 * Constructor de clase
	 */
	public CedulaControlDTO5() {
		super(); 
	}
	
	/**
	 * @return the coccFeProceso
	 */
	public Date getCoccFeProceso() {
		return new Date(coccFeProceso.getTime());
	}
	/**
	 * @param coccFeProceso the coccFeProceso to set
	 */
	public void setCoccFeProceso(Date coccFeProceso) {
		if(coccFeProceso != null) {
			this.coccFeProceso = new Date(coccFeProceso.getTime());
		}
	}
	/**
	 * @return the coccNuRegistros
	 */
	public BigDecimal getCoccNuRegistros() {
		return coccNuRegistros;
	}
	/**
	 * @param coccNuRegistros the coccNuRegistros to set
	 */
	public void setCoccNuRegistros(BigDecimal coccNuRegistros) {
		this.coccNuRegistros = coccNuRegistros;
	}
	/**
	 * @return the coccNuRegSinpndNook
	 */
	public BigDecimal getCoccNuRegSinpndNook() {
		return coccNuRegSinpndNook;
	}
	/**
	 * @param coccNuRegSinpndNook the coccNuRegSinpndNook to set
	 */
	public void setCoccNuRegSinpndNook(BigDecimal coccNuRegSinpndNook) {
		this.coccNuRegSinpndNook = coccNuRegSinpndNook;
	}
	/**
	 * @return the coccArchivo
	 */
	public String getCoccArchivo() {
		return coccArchivo;
	}
	/**
	 * @param coccArchivo the coccArchivo to set
	 */
	public void setCoccArchivo(String coccArchivo) {
		this.coccArchivo = coccArchivo;
	}
}