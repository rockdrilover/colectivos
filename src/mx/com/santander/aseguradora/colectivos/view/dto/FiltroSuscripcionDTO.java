package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author CJPV - vectormx
 * @version 06-09-2017
 */
public class FiltroSuscripcionDTO {
	private String dictamen;
	private Date fechaCarga;
	private Date fechaSolicitud;
	private BigDecimal folioCH;
	private String area;
	private long cosuId;
	
	/**
	 * Constructor
	 * @param dictamen Estatus suscripcion
	 * @param fechaCarga Fecha Carga suscripcion
	 * @param fechaSolicitud Fecha Solicitud Credito
	 * @param folioCH Folio CH
	 */
	public FiltroSuscripcionDTO(String dictamen, Date fechaCarga,
			Date fechaSolicitud, BigDecimal folioCH) {
		super();
		this.dictamen = dictamen;
		this.fechaCarga = fechaCarga;
		this.fechaSolicitud = fechaSolicitud;
		this.folioCH = folioCH;
	}

	public FiltroSuscripcionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getDictamen() {
		return dictamen;
	}

	public void setDictamen(String dictamen) {
		this.dictamen = dictamen;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public BigDecimal getFolioCH() {
		return folioCH;
	}

	public void setFolioCH(BigDecimal folioCH) {
		this.folioCH = folioCH;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public long getCosuId() {
		return cosuId;
	}

	public void setCosuId(long cosuId) {
		this.cosuId = cosuId;
	}
		
	
}
