package mx.com.santander.aseguradora.colectivos.view.dto;
/**
 * Representa una columna de seleccion en Pantalla
 * @author CJPV - vectormx
 *
 */
public class ColumnaDTO implements Comparable<ColumnaDTO>{
	
	/**
	 * Orden de presentacion
	 */
	private int orden;
	
	/**
	 * Nombre de columna mapeada
	 */
	private String nombreColumna;
	
	/**
	 * Header columna mapeada
	 */
	private String headerColumn;
	
	/**
	 * Columna seleccionada
	 */
	private Boolean selected;

	public ColumnaDTO(int orden, String nombreColumna, String headerColumn, Boolean selected) {
		super();
		this.orden = orden;
		this.nombreColumna = nombreColumna;
		this.headerColumn = headerColumn;
		this.selected = selected;
	}

	public ColumnaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the orden
	 */
	public int getOrden() {
		return orden;
	}

	/**
	 * @param orden the orden to set
	 */
	public void setOrden(int orden) {
		this.orden = orden;
	}

	/**
	 * @return the nombreColumna
	 */
	public String getNombreColumna() {
		return nombreColumna;
	}

	/**
	 * @param nombreColumna the nombreColumna to set
	 */
	public void setNombreColumna(String nombreColumna) {
		this.nombreColumna = nombreColumna;
	}

	/**
	 * @return the headerColumn
	 */
	public String getHeaderColumn() {
		return headerColumn;
	}

	/**
	 * @param headerColumn the headerColumn to set
	 */
	public void setHeaderColumn(String headerColumn) {
		this.headerColumn = headerColumn;
	}

	/**
	 * @return the selected
	 */
	public Boolean getSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ColumnaDTO o) {		
		return orden - o.getOrden();
	}
	
	
	
	
}
