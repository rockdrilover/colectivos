package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;

/**
 * 
 * @author CJPV - vectormx
 * @version 06-09-2017
 */
public class AnexoDTO {

	private long coanId;
	private BigDecimal coseConsecutivo;
	private String coseNomDocto;
	private long coanCoseID;
	/**
	 * 
	 */
	public AnexoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param coanId
	 * @param coseConsecutivo
	 * @param coseNomDocto
	 * @param coanCoseID
	 */
	public AnexoDTO(long coanId, BigDecimal coseConsecutivo,
			String coseNomDocto, long coanCoseID) {
		super();
		this.coanId = coanId;
		this.coseConsecutivo = coseConsecutivo;
		this.coseNomDocto = coseNomDocto;
		this.coanCoseID = coanCoseID;
	}
	public long getCoanId() {
		return coanId;
	}
	public void setCoanId(long coanId) {
		this.coanId = coanId;
	}
	public BigDecimal getCoseConsecutivo() {
		return coseConsecutivo;
	}
	public void setCoseConsecutivo(BigDecimal coseConsecutivo) {
		this.coseConsecutivo = coseConsecutivo;
	}
	public String getCoseNomDocto() {
		return coseNomDocto;
	}
	public void setCoseNomDocto(String coseNomDocto) {
		this.coseNomDocto = coseNomDocto;
	}
	public long getCoanCoseID() {
		return coanCoseID;
	}
	public void setCoanCoseID(long coanCoseID) {
		this.coanCoseID = coanCoseID;
	}
	
	
}
