package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCedula;
import java.math.BigDecimal;

/** =======================================================================
* Autor:		Ing. Issac Bautista
* Fecha:		28-05-2020
* Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CEDULA.
 * 				Extiende de la clase CedulaControlDTO2
* ------------------------------------------------------------------------				
* Modificaciones
* ------------------------------------------------------------------------
* 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
* ------------------------------------------------------------------------
* 
*/
public class CedulaControlDTO extends CedulaControlDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccCdCedula
	private Long coccCdCedula;
	//Propiedad para campo coccCasuCdSucursal
	private BigDecimal coccCasuCdSucursal;
	//Propiedad para campo coccCarpCdRamo
	private BigDecimal coccCarpCdRamo;
	//Propiedad para campo coccRemesaCedula
	private String coccRemesaCedula;

	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cedula objeto con datos
	 */
	public CedulaControlDTO(ColectivosCedula cedula) {
		super(cedula);
		this.coccCdCedula= cedula.getCoccCdCedula();
		this.coccCasuCdSucursal= cedula.getCoccCasuCdSucursal();
		this.coccCarpCdRamo= cedula.getCoccCarpCdRamo();
		this.coccRemesaCedula= cedula.getCoccRemesaCedula();
	}
	
	/**
	 * Constructor de clase
	 */
	public CedulaControlDTO() {
		super();
	}

	/**
	 * @return the coccCdCedula
	 */
	public Long getCoccCdCedula() {
		return coccCdCedula;
	}
	/**
	 * @return the coccRemesaCedula
	 */
	public String getCoccRemesaCedula() {
		return coccRemesaCedula;
	}
	/**
	 * @return the coccCasuCdSucursal
	 */
	public BigDecimal getCoccCasuCdSucursal() {
		return coccCasuCdSucursal;
	}
	/**
	 * @return the cocCarpCdRamo
	 */
	public BigDecimal getCoccCarpCdRamo() {
		return coccCarpCdRamo;
	}
	/**
	 * @param coccCdCedula the coccCdCedula to set
	 */
	public void setCoccCdCedula(Long coccCdCedula) {
		this.coccCdCedula = coccCdCedula;
	}
	/**
	 * @param coccCasuCdSucursal the coccCasuCdSucursal to set
	 */
	public void setCoccCasuCdSucursal(BigDecimal coccCasuCdSucursal) {
		this.coccCasuCdSucursal = coccCasuCdSucursal;
	}
	/**
	 * @param coccCarpCdRamo the coccCarpCdRamo to set
	 */
	public void setCoccCarpCdRamo(BigDecimal coccCarpCdRamo) {
		this.coccCarpCdRamo = coccCarpCdRamo;
	}
	/**
	 * @param coccRemesaCedula the coccRemesaCedula to set
	 */
	public void setCoccRemesaCedula(String coccRemesaCedula) {
		this.coccRemesaCedula = coccRemesaCedula;
	}
}