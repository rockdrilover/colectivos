/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Ing. IBB
 * 
 */
public class ContratanteDTO {

	public ContratanteDTO() {
	}
	
	private String cacnCdNacionalidad;
	private String cacnNuCedulaRif;
	private String cacnTpCliente;
	private String cacnApellidoMat;
	private String cacnApellidoPat;
	private String cacnNombre;
	private Date cacnFeNacimiento;
	private String cacnCdEdoCivil;
	private String cacnCdSexo;
	private String cacnRfc;
	private String cacnNmPersonaNatural; //CURP
	private String cacnDiHabitacion1; //DIRECCION CONTRATANTE
	private String cacnZnPostalHabitacion; //CP CONTRATANTE
	private String cacnDiCobro2; //COLONIA CONTRATANTE
	private String cacnDatosReg1; //POBLACION CONTRATANTE
	private BigDecimal cacnCaesCdEstadoHab; //ESTADO CONTRATANTE
	private String cacnNuTelefonoCobro; //TELEFONO
	private String cacnNuTelefonoHabitacion; //TELEFONO OFICINA
	private String cacnDiHabitacion2; //CORREO
	
	private String cacnDiCobro1; //DIRECCION FISCAL
	private String cacnZnPostalCobro; //CP FISCAL
	private String cacnCazpColoniaCob; //COLONIA FISCAL
	private String cacnCazpPoblacCob; //POBLACION FISCAL
	private BigDecimal cacnCaesCdEstadoCob; //ESTADO FISCAL
	
	//CAMPOS PARA CART_CLIENTES_FUI
	private String strNumeroSerie; //cacn_apellido_pat
	private String strFolioMercantil; //cacn_apellido_mat
	private String strGiroMercantil; //ds_segmento_aseg
	private String strNombreRepLegal; //cacn_nombre
	private String strPuestoRepLegal; //cace_ds_actividad 
	private String strNacionalidad; //ds_segmento
	private String strDireccionEnvio; //CACE_DS_PROFESION 100
	private String strCPEnvio; //CADM_NU_CUENTA1 20
	private String strColoniaEnvio; //CACE_DS_FUENTE_INGRESO 100
	private String strPoblacionEnvio; // CADM_NU_CUENTA2 20
	private Integer nEstadoEnvio; //CADM_NU_DOMICILIO 20
	private String strTpCuenta;
	private String strCuenta;

	    
	    
	
	private Integer cacnCaaeCdActividad;
	private BigDecimal cacnCaciCdCiudadCob;
	private BigDecimal cacnCaciCdCiudadHab;
	private BigDecimal cacnCapaCdPais;
	private BigDecimal cacnCdDependencia1;
	private BigDecimal cacnCdDependencia2;
	private BigDecimal cacnCdDependencia3;
	private BigDecimal cacnCdDependencia4;
	private String cacnCdNacionalidadA;
	private String cacnCdTaxid;
	private String cacnCdVida; 
	private String cacnDatosReg2;
	private String cacnInEmpleado;
	private String cacnInTratamientoEspecial;
	private String cacnInUsadoFinanciamiento;
	private BigDecimal cacnNacionalidadFatca;
	private String cacnNmApellidoRazon;
	private String cacnNuApartadoCobro;
	private String cacnNuApartadoHabitacion;
	private String cacnNuCedulaAnterior;
	private BigDecimal cacnNuConsultas;
	private String cacnNuEmpleadoFicha;
	private String cacnNuFaxCobro;
	private BigDecimal cacnNuLlamadas;
	private BigDecimal cacnNuServicios;
	private BigDecimal cacnPaisFatca;
	private String cacnRegistro1;
	private String cacnRegistro2;
	private String cacnStActuaNom;
	private String cacnStCliente;
	
	private String ciudad;
	private String estado;
	
	
	/* GETTERS Y SETTERS */
	public String getCacnApellidoMat() {
		return cacnApellidoMat;
	}
	public void setCacnApellidoMat(String cacnApellidoMat) {
		this.cacnApellidoMat = cacnApellidoMat;
	}
	public String getCacnApellidoPat() {
		return cacnApellidoPat;
	}
	public void setCacnApellidoPat(String cacnApellidoPat) {
		this.cacnApellidoPat = cacnApellidoPat;
	}
	public Integer getCacnCaaeCdActividad() {
		return cacnCaaeCdActividad;
	}
	public void setCacnCaaeCdActividad(Integer cacnCaaeCdActividad) {
		this.cacnCaaeCdActividad = cacnCaaeCdActividad;
	}
	public BigDecimal getCacnCaciCdCiudadCob() {
		return cacnCaciCdCiudadCob;
	}
	public void setCacnCaciCdCiudadCob(BigDecimal cacnCaciCdCiudadCob) {
		this.cacnCaciCdCiudadCob = cacnCaciCdCiudadCob;
	}
	public BigDecimal getCacnCaciCdCiudadHab() {
		return cacnCaciCdCiudadHab;
	}
	public void setCacnCaciCdCiudadHab(BigDecimal cacnCaciCdCiudadHab) {
		this.cacnCaciCdCiudadHab = cacnCaciCdCiudadHab;
	}
	public BigDecimal getCacnCaesCdEstadoCob() {
		return cacnCaesCdEstadoCob;
	}
	public void setCacnCaesCdEstadoCob(BigDecimal cacnCaesCdEstadoCob) {
		this.cacnCaesCdEstadoCob = cacnCaesCdEstadoCob;
	}
	public BigDecimal getCacnCaesCdEstadoHab() {
		return cacnCaesCdEstadoHab;
	}
	public void setCacnCaesCdEstadoHab(BigDecimal cacnCaesCdEstadoHab) {
		this.cacnCaesCdEstadoHab = cacnCaesCdEstadoHab;
	}
	public BigDecimal getCacnCapaCdPais() {
		return cacnCapaCdPais;
	}
	public void setCacnCapaCdPais(BigDecimal cacnCapaCdPais) {
		this.cacnCapaCdPais = cacnCapaCdPais;
	}
	public String getCacnCazpColoniaCob() {
		return cacnCazpColoniaCob;
	}
	public void setCacnCazpColoniaCob(String cacnCazpColoniaCob) {
		this.cacnCazpColoniaCob = cacnCazpColoniaCob;
	}
	public String getCacnCazpPoblacCob() {
		return cacnCazpPoblacCob;
	}
	public void setCacnCazpPoblacCob(String cacnCazpPoblacCob) {
		this.cacnCazpPoblacCob = cacnCazpPoblacCob;
	}
	public BigDecimal getCacnCdDependencia1() {
		return cacnCdDependencia1;
	}
	public void setCacnCdDependencia1(BigDecimal cacnCdDependencia1) {
		this.cacnCdDependencia1 = cacnCdDependencia1;
	}
	public BigDecimal getCacnCdDependencia2() {
		return cacnCdDependencia2;
	}
	public void setCacnCdDependencia2(BigDecimal cacnCdDependencia2) {
		this.cacnCdDependencia2 = cacnCdDependencia2;
	}
	public BigDecimal getCacnCdDependencia3() {
		return cacnCdDependencia3;
	}
	public void setCacnCdDependencia3(BigDecimal cacnCdDependencia3) {
		this.cacnCdDependencia3 = cacnCdDependencia3;
	}
	public BigDecimal getCacnCdDependencia4() {
		return cacnCdDependencia4;
	}
	public void setCacnCdDependencia4(BigDecimal cacnCdDependencia4) {
		this.cacnCdDependencia4 = cacnCdDependencia4;
	}
	public String getCacnCdEdoCivil() {
		return cacnCdEdoCivil;
	}
	public void setCacnCdEdoCivil(String cacnCdEdoCivil) {
		this.cacnCdEdoCivil = cacnCdEdoCivil;
	}
	public String getCacnCdNacionalidadA() {
		return cacnCdNacionalidadA;
	}
	public void setCacnCdNacionalidadA(String cacnCdNacionalidadA) {
		this.cacnCdNacionalidadA = cacnCdNacionalidadA;
	}
	public String getCacnCdSexo() {
		return cacnCdSexo;
	}
	public void setCacnCdSexo(String cacnCdSexo) {
		this.cacnCdSexo = cacnCdSexo;
	}
	public String getCacnCdTaxid() {
		return cacnCdTaxid;
	}
	public void setCacnCdTaxid(String cacnCdTaxid) {
		this.cacnCdTaxid = cacnCdTaxid;
	}
	public String getCacnCdVida() {
		return cacnCdVida;
	}
	public void setCacnCdVida(String cacnCdVida) {
		this.cacnCdVida = cacnCdVida;
	}
	public String getCacnDatosReg1() {
		return cacnDatosReg1;
	}
	public void setCacnDatosReg1(String cacnDatosReg1) {
		this.cacnDatosReg1 = cacnDatosReg1;
	}
	public String getCacnDatosReg2() {
		return cacnDatosReg2;
	}
	public void setCacnDatosReg2(String cacnDatosReg2) {
		this.cacnDatosReg2 = cacnDatosReg2;
	}
	public String getCacnDiCobro1() {
		return cacnDiCobro1;
	}
	public void setCacnDiCobro1(String cacnDiCobro1) {
		this.cacnDiCobro1 = cacnDiCobro1;
	}
	public String getCacnDiCobro2() {
		return cacnDiCobro2;
	}
	public void setCacnDiCobro2(String cacnDiCobro2) {
		this.cacnDiCobro2 = cacnDiCobro2;
	}
	public String getCacnDiHabitacion1() {
		return cacnDiHabitacion1;
	}
	public void setCacnDiHabitacion1(String cacnDiHabitacion1) {
		this.cacnDiHabitacion1 = cacnDiHabitacion1;
	}
	public String getCacnDiHabitacion2() {
		return cacnDiHabitacion2;
	}
	public void setCacnDiHabitacion2(String cacnDiHabitacion2) {
		this.cacnDiHabitacion2 = cacnDiHabitacion2;
	}
	public Date getCacnFeNacimiento() {
		return cacnFeNacimiento;
	}
	public void setCacnFeNacimiento(Date cacnFeNacimiento) {
		this.cacnFeNacimiento = cacnFeNacimiento;
	}
	public String getCacnInEmpleado() {
		return cacnInEmpleado;
	}
	public void setCacnInEmpleado(String cacnInEmpleado) {
		this.cacnInEmpleado = cacnInEmpleado;
	}
	public String getCacnInTratamientoEspecial() {
		return cacnInTratamientoEspecial;
	}
	public void setCacnInTratamientoEspecial(String cacnInTratamientoEspecial) {
		this.cacnInTratamientoEspecial = cacnInTratamientoEspecial;
	}
	public String getCacnInUsadoFinanciamiento() {
		return cacnInUsadoFinanciamiento;
	}
	public void setCacnInUsadoFinanciamiento(String cacnInUsadoFinanciamiento) {
		this.cacnInUsadoFinanciamiento = cacnInUsadoFinanciamiento;
	}
	public BigDecimal getCacnNacionalidadFatca() {
		return cacnNacionalidadFatca;
	}
	public void setCacnNacionalidadFatca(BigDecimal cacnNacionalidadFatca) {
		this.cacnNacionalidadFatca = cacnNacionalidadFatca;
	}
	public String getCacnNmApellidoRazon() {
		return cacnNmApellidoRazon;
	}
	public void setCacnNmApellidoRazon(String cacnNmApellidoRazon) {
		this.cacnNmApellidoRazon = cacnNmApellidoRazon;
	}
	public String getCacnNmPersonaNatural() {
		return cacnNmPersonaNatural;
	}
	public void setCacnNmPersonaNatural(String cacnNmPersonaNatural) {
		this.cacnNmPersonaNatural = cacnNmPersonaNatural;
	}
	public String getCacnNombre() {
		return cacnNombre;
	}
	public void setCacnNombre(String cacnNombre) {
		this.cacnNombre = cacnNombre;
	}
	public String getCacnNuApartadoCobro() {
		return cacnNuApartadoCobro;
	}
	public void setCacnNuApartadoCobro(String cacnNuApartadoCobro) {
		this.cacnNuApartadoCobro = cacnNuApartadoCobro;
	}
	public String getCacnNuApartadoHabitacion() {
		return cacnNuApartadoHabitacion;
	}
	public void setCacnNuApartadoHabitacion(String cacnNuApartadoHabitacion) {
		this.cacnNuApartadoHabitacion = cacnNuApartadoHabitacion;
	}
	public String getCacnNuCedulaAnterior() {
		return cacnNuCedulaAnterior;
	}
	public void setCacnNuCedulaAnterior(String cacnNuCedulaAnterior) {
		this.cacnNuCedulaAnterior = cacnNuCedulaAnterior;
	}
	public BigDecimal getCacnNuConsultas() {
		return cacnNuConsultas;
	}
	public void setCacnNuConsultas(BigDecimal cacnNuConsultas) {
		this.cacnNuConsultas = cacnNuConsultas;
	}
	public String getCacnNuEmpleadoFicha() {
		return cacnNuEmpleadoFicha;
	}
	public void setCacnNuEmpleadoFicha(String cacnNuEmpleadoFicha) {
		this.cacnNuEmpleadoFicha = cacnNuEmpleadoFicha;
	}
	public String getCacnNuFaxCobro() {
		return cacnNuFaxCobro;
	}
	public void setCacnNuFaxCobro(String cacnNuFaxCobro) {
		this.cacnNuFaxCobro = cacnNuFaxCobro;
	}
	public BigDecimal getCacnNuLlamadas() {
		return cacnNuLlamadas;
	}
	public void setCacnNuLlamadas(BigDecimal cacnNuLlamadas) {
		this.cacnNuLlamadas = cacnNuLlamadas;
	}
	public BigDecimal getCacnNuServicios() {
		return cacnNuServicios;
	}
	public void setCacnNuServicios(BigDecimal cacnNuServicios) {
		this.cacnNuServicios = cacnNuServicios;
	}
	public String getCacnNuTelefonoCobro() {
		return cacnNuTelefonoCobro;
	}
	public void setCacnNuTelefonoCobro(String cacnNuTelefonoCobro) {
		this.cacnNuTelefonoCobro = cacnNuTelefonoCobro;
	}
	public String getCacnNuTelefonoHabitacion() {
		return cacnNuTelefonoHabitacion;
	}
	public void setCacnNuTelefonoHabitacion(String cacnNuTelefonoHabitacion) {
		this.cacnNuTelefonoHabitacion = cacnNuTelefonoHabitacion;
	}
	public BigDecimal getCacnPaisFatca() {
		return cacnPaisFatca;
	}
	public void setCacnPaisFatca(BigDecimal cacnPaisFatca) {
		this.cacnPaisFatca = cacnPaisFatca;
	}
	public String getCacnRegistro1() {
		return cacnRegistro1;
	}
	public void setCacnRegistro1(String cacnRegistro1) {
		this.cacnRegistro1 = cacnRegistro1;
	}
	public String getCacnRegistro2() {
		return cacnRegistro2;
	}
	public void setCacnRegistro2(String cacnRegistro2) {
		this.cacnRegistro2 = cacnRegistro2;
	}
	public String getCacnRfc() {
		return cacnRfc;
	}
	public void setCacnRfc(String cacnRfc) {
		this.cacnRfc = cacnRfc;
	}
	public String getCacnStActuaNom() {
		return cacnStActuaNom;
	}
	public void setCacnStActuaNom(String cacnStActuaNom) {
		this.cacnStActuaNom = cacnStActuaNom;
	}
	public String getCacnStCliente() {
		return cacnStCliente;
	}
	public void setCacnStCliente(String cacnStCliente) {
		this.cacnStCliente = cacnStCliente;
	}
	public String getCacnTpCliente() {
		return cacnTpCliente;
	}
	public void setCacnTpCliente(String cacnTpCliente) {
		this.cacnTpCliente = cacnTpCliente;
	}
	public String getCacnZnPostalCobro() {
		return cacnZnPostalCobro;
	}
	public void setCacnZnPostalCobro(String cacnZnPostalCobro) {
		this.cacnZnPostalCobro = cacnZnPostalCobro;
	}
	public String getCacnZnPostalHabitacion() {
		return cacnZnPostalHabitacion;
	}
	public void setCacnZnPostalHabitacion(String cacnZnPostalHabitacion) {
		this.cacnZnPostalHabitacion = cacnZnPostalHabitacion;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCacnCdNacionalidad() {
		return cacnCdNacionalidad;
	}
	public void setCacnCdNacionalidad(String cacnCdNacionalidad) {
		this.cacnCdNacionalidad = cacnCdNacionalidad;
	}
	public String getCacnNuCedulaRif() {
		return cacnNuCedulaRif;
	}
	public void setCacnNuCedulaRif(String cacnNuCedulaRif) {
		this.cacnNuCedulaRif = cacnNuCedulaRif;
	}
	public String getStrNumeroSerie() {
		return strNumeroSerie;
	}
	public void setStrNumeroSerie(String strNumeroSerie) {
		this.strNumeroSerie = strNumeroSerie;
	}
	public String getStrFolioMercantil() {
		return strFolioMercantil;
	}
	public void setStrFolioMercantil(String strFolioMercantil) {
		this.strFolioMercantil = strFolioMercantil;
	}
	public String getStrGiroMercantil() {
		return strGiroMercantil;
	}
	public void setStrGiroMercantil(String strGiroMercantil) {
		this.strGiroMercantil = strGiroMercantil;
	}
	public String getStrNombreRepLegal() {
		return strNombreRepLegal;
	}
	public void setStrNombreRepLegal(String strNombreRepLegal) {
		this.strNombreRepLegal = strNombreRepLegal;
	}
	public String getStrPuestoRepLegal() {
		return strPuestoRepLegal;
	}
	public void setStrPuestoRepLegal(String strPuestoRepLegal) {
		this.strPuestoRepLegal = strPuestoRepLegal;
	}
	public String getStrNacionalidad() {
		return strNacionalidad;
	}
	public void setStrNacionalidad(String strNacionalidad) {
		this.strNacionalidad = strNacionalidad;
	}
	public String getStrDireccionEnvio() {
		return strDireccionEnvio;
	}
	public void setStrDireccionEnvio(String strDireccionEnvio) {
		this.strDireccionEnvio = strDireccionEnvio;
	}
	public String getStrCPEnvio() {
		return strCPEnvio;
	}
	public void setStrCPEnvio(String strCPEnvio) {
		this.strCPEnvio = strCPEnvio;
	}
	public String getStrColoniaEnvio() {
		return strColoniaEnvio;
	}
	public void setStrColoniaEnvio(String strColoniaEnvio) {
		this.strColoniaEnvio = strColoniaEnvio;
	}
	public String getStrPoblacionEnvio() {
		return strPoblacionEnvio;
	}
	public void setStrPoblacionEnvio(String strPoblacionEnvio) {
		this.strPoblacionEnvio = strPoblacionEnvio;
	}
	public Integer getnEstadoEnvio() {
		return nEstadoEnvio;
	}
	public void setnEstadoEnvio(Integer nEstadoEnvio) {
		this.nEstadoEnvio = nEstadoEnvio;
	}
	public String getStrTpCuenta() {
		return strTpCuenta;
	}
	public void setStrTpCuenta(String strTpCuenta) {
		this.strTpCuenta = strTpCuenta;
	}
	public String getStrCuenta() {
		return strCuenta;
	}
	public void setStrCuenta(String strCuenta) {
		this.strCuenta = strCuenta;
	}

}
