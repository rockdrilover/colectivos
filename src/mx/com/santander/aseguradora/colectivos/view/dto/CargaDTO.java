package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;

import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CargaId;

/**
 * Data Transfer Object (DTO) para tabla de ColectivosCarga
 * Clase para envover datos en la capa de vista para la tabla ColectivosCarga
 * @author Ing. Issac Bautista
 *
 */
public class CargaDTO implements java.io.Serializable {

	//Implementes Serializable
	private static final long serialVersionUID = 1073211340028368185L;
	
	//Declara Propiedad CargaId
	private CargaId id;
	
	//Declara propiedades Integer
	private Integer cocrCazbCdSucursal;
	private Integer cocrCargada;
	private Integer cocrTipoCliente;
	private Integer cocrNuPlazo;
	
	//Declara propiedades String
	private String cocrCdProducto;
	private String cocrCdPlan;
	private String cocrApPaterno;
	private String cocrApMaterno;
	private String cocrNombre;
	private String cocrSexo;
	private String cocrRfc;
	private String cocrCalle;
	private String cocrColonia;
	private String cocrDelmunic;
	private String cocrCp;
	private String cocrTelefono;
	private String cocrBcoProducto;
	private String cocrSubProducto;
	private String cocrFrPago;
	private String cocrPlazoTiempo;
	private String cocrRemesa;
	private String cocrCuenta;
	private String cocrRegistro;
	private String cocrDato1;
	private String cocrDato2;
	private String cocrDato3;
	private String cocrDato4;
	private String cocrDato5;
	private String cocrDato6;
	private String cocrDato7;
	private String cocrDato8;
	private String cocrDato9;
	private String cocrDato10;
	private String cocrDato11;
	private String cocrDato12;
	private String cocrDato13;
	private String cocrDato14;
	private String cocrDato15;
	private String cocrDato16;
	private String cocrDato17;
	private String cocrDato18;
	private String cocrDato19;
	private String cocrDato20;
	private String cocrDato21;
	private String cocrDato22;
	private String cocrDato23;
	private String cocrDato24;
	private String cocrDato25;
	private String cocrDato26;
	private String cocrDato27;
	private String cocrDato28;
	private String cocrDato29;
	private String cocrDato30;
	private String cocrComisiones;
	
	//Declara propiedades Date
	private Date cocrFeCarga;
	private Date cocrFeStatus;
	private Date cocrFeNac;
	private Date cocrFeIngreso;
	private Date cocrDato31;
	private Date cocrDato32;
	private Date cocrDato33;
	private Date cocrDato34;
	private Date cocrDato35;
	
	//Declara propiedades Long
	private Long cocrNumCliente;
	private Long cocrCdCobertura;
	private Long cocrCdComponente;
	private Long cocrNuCredito;
	
	//Declara propiedades BigDecimal
	private BigDecimal cocrPrima;
	private BigDecimal cocrPrimaReal;
	
	//Declara propiedades Byte
	private Byte cocrCdCiudad;
	private Byte cocrCdEstado;
	private Byte cocrAltaBaja;

	/**
	 * Constructor vacio de clase
	 */
	public CargaDTO() {
		//Inicializa fecha carga
		cocrFeCarga = new Date();
		
		//Inicializa fecha ingreso
		cocrFeIngreso = new Date();
		
		//Inicializa fecha nacimiento
		cocrFeNac = new Date();
		
		//Inicializa fecha estatus
		cocrFeStatus = new Date();
		
		//Inicializa fecha campo 31
		cocrDato31  = new Date();

		//Inicializa fecha campo 32
		cocrDato32  = new Date();
		
		//Inicializa fecha campo 33
		cocrDato33  = new Date();
		
		//Inicializa fecha campo 34
		cocrDato34  = new Date();
		
		//Inicializa fecha campo 35
		cocrDato35  = new Date();

	}

	/**
	 * Constructor con id
	 * @param id Id de clase
	 */
	public CargaDTO(CargaId id) {
		//Inicializa Id
		this.id = id;
		
		//Inicializa fecha carga
		cocrFeCarga = new Date();
		
		//Inicializa fecha ingreso
		cocrFeIngreso = new Date();
		
		//Inicializa fecha nacimiento
		cocrFeNac = new Date();
		
		//Inicializa fecha estatus
		cocrFeStatus = new Date();
		
		//Inicializa fecha campo 31
		cocrDato31  = new Date();

		//Inicializa fecha campo 32
		cocrDato32  = new Date();
		
		//Inicializa fecha campo 33
		cocrDato33  = new Date();
		
		//Inicializa fecha campo 34
		cocrDato34  = new Date();
		
		//Inicializa fecha campo 35
		cocrDato35  = new Date();
	}
	
	/**
	 * @return the cocrFeCarga
	 */
	public Date getCocrFeCarga() {
		return new Date(this.cocrFeCarga.getTime());
	}
	/**
	 * @param cocrFeCarga the cocrFeCarga to set
	 */
	public void setCocrFeCarga(Date cocrFeCarga) {
		if(cocrFeCarga != null) {
			//Fecha cocrFeCarga no es null
			this.cocrFeCarga = new Date(cocrFeCarga.getTime());
		}
	}
	/**
	 * @return the cocrFeStatus
	 */
	public Date getCocrFeStatus() {
		return new Date(this.cocrFeStatus.getTime());
	}
	/**
	 * @param cocrFeStatus the cocrFeStatus to set
	 */
	public void setCocrFeStatus(Date cocrFeStatus) {
		if(cocrFeStatus != null) {
			//Fecha cocrFeStatus no es null
			this.cocrFeStatus = new Date(cocrFeStatus.getTime());
		}
	}	
	/**
	 * @return the cocrFeNac
	 */
	public Date getCocrFeNac() {
		//Fecha cocrDato35 no es null
		return new Date(this.cocrFeNac.getTime());
	}
	/**
	 * @param cocrFeNac the cocrFeNac to set
	 */
	public void setCocrFeNac(Date cocrFeNac) {
		if(cocrFeNac != null) {
			//Fecha cocrFeNac no es null
			this.cocrFeNac = new Date(cocrFeNac.getTime());
		}
	}
	/**
	 * @return the cocrFeIngreso
	 */
	public Date getCocrFeIngreso() {
		return new Date(this.cocrFeIngreso.getTime());
	}
	/**
	 * @param cocrFeIngreso the cocrFeIngreso to set
	 */
	public void setCocrFeIngreso(Date cocrFeIngreso) {
		if(cocrFeIngreso != null) {
			//Fecha cocrFeIngreso no es null
			this.cocrFeIngreso = new Date(cocrFeIngreso.getTime());
		}
	}
	/**
	 * @return the cocrDato31
	 */
	public Date getCocrDato31() {
		return new Date(this.cocrDato31.getTime());
	}
	/**
	 * @param cocrDato31 the cocrDato31 to set
	 */
	public void setCocrDato31(Date cocrDato31) {
		if(cocrDato31 != null) {
			//Fecha cocrDato31 no es null
			this.cocrDato31 = new Date(cocrDato31.getTime());
		}
		
	}
	/**
	 * @return the cocrDato32
	 */
	public Date getCocrDato32() {
		return new Date(this.cocrDato32.getTime());
	}
	/**
	 * @param cocrDato32 the cocrDato32 to set
	 */
	public void setCocrDato32(Date cocrDato32) {
		if(cocrDato32 != null) {
			//Fecha cocrDato32 no es null
			this.cocrDato32 = new Date(cocrDato32.getTime());
		}
	}
	/**
	 * @return the cocrDato33
	 */
	public Date getCocrDato33() {
		return new Date(this.cocrDato33.getTime());
	}
	/**
	 * @param cocrDato33 the cocrDato33 to set
	 */
	public void setCocrDato33(Date cocrDato33) {
		if(cocrDato33 != null) {
			//Fecha cocrDato33 no es null
			this.cocrDato33 = new Date(cocrDato33.getTime());
		}
	}
	/**
	 * @return the cocrDato34
	 */
	public Date getCocrDato34() {
		return new Date(this.cocrDato34.getTime());
	}
	/**
	 * @param cocrDato34 the cocrDato34 to set
	 */
	public void setCocrDato34(Date cocrDato34) {
		if(cocrDato34 != null) {
			//Fecha cocrDato34 no es null
			this.cocrDato34 = new Date(cocrDato34.getTime());
		}
	}
	/**
	 * @return the cocrDato35
	 */
	public Date getCocrDato35() {
		return new Date(this.cocrDato35.getTime());
	}
	/**
	 * @param cocrDato35 the cocrDato35 to set
	 */
	public void setCocrDato35(Date cocrDato35) {
		if(cocrDato35 != null) {
			//Fecha cocrDato35 no es null
			this.cocrDato35 = new Date(cocrDato35.getTime());
		}
	}

	/**
	 * @return the cocrDato19
	 */
	public String getCocrDato19() {
		return cocrDato19;
	}

	/**
	 * @param cocrDato19 the cocrDato19 to set
	 */
	public void setCocrDato19(String cocrDato19) {
		this.cocrDato19 = cocrDato19;
	}

	/**
	 * @return the id
	 */
	public CargaId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(CargaId id) {
		this.id = id;
	}

	/**
	 * @return the cocrCazbCdSucursal
	 */
	public Integer getCocrCazbCdSucursal() {
		return cocrCazbCdSucursal;
	}

	/**
	 * @param cocrCazbCdSucursal the cocrCazbCdSucursal to set
	 */
	public void setCocrCazbCdSucursal(Integer cocrCazbCdSucursal) {
		this.cocrCazbCdSucursal = cocrCazbCdSucursal;
	}

	/**
	 * @return the cocrCargada
	 */
	public Integer getCocrCargada() {
		return cocrCargada;
	}

	/**
	 * @param cocrCargada the cocrCargada to set
	 */
	public void setCocrCargada(Integer cocrCargada) {
		this.cocrCargada = cocrCargada;
	}

	/**
	 * @return the cocrCdProducto
	 */
	public String getCocrCdProducto() {
		return cocrCdProducto;
	}

	/**
	 * @param cocrCdProducto the cocrCdProducto to set
	 */
	public void setCocrCdProducto(String cocrCdProducto) {
		this.cocrCdProducto = cocrCdProducto;
	}

	/**
	 * @return the cocrCdPlan
	 */
	public String getCocrCdPlan() {
		return cocrCdPlan;
	}

	/**
	 * @param cocrCdPlan the cocrCdPlan to set
	 */
	public void setCocrCdPlan(String cocrCdPlan) {
		this.cocrCdPlan = cocrCdPlan;
	}

	/**
	 * @return the cocrApPaterno
	 */
	public String getCocrApPaterno() {
		return cocrApPaterno;
	}

	/**
	 * @param cocrApPaterno the cocrApPaterno to set
	 */
	public void setCocrApPaterno(String cocrApPaterno) {
		this.cocrApPaterno = cocrApPaterno;
	}

	/**
	 * @return the cocrApMaterno
	 */
	public String getCocrApMaterno() {
		return cocrApMaterno;
	}

	/**
	 * @param cocrApMaterno the cocrApMaterno to set
	 */
	public void setCocrApMaterno(String cocrApMaterno) {
		this.cocrApMaterno = cocrApMaterno;
	}

	/**
	 * @return the cocrNombre
	 */
	public String getCocrNombre() {
		return cocrNombre;
	}

	/**
	 * @param cocrNombre the cocrNombre to set
	 */
	public void setCocrNombre(String cocrNombre) {
		this.cocrNombre = cocrNombre;
	}

	/**
	 * @return the cocrSexo
	 */
	public String getCocrSexo() {
		return cocrSexo;
	}

	/**
	 * @param cocrSexo the cocrSexo to set
	 */
	public void setCocrSexo(String cocrSexo) {
		this.cocrSexo = cocrSexo;
	}

	/**
	 * @return the cocrNumCliente
	 */
	public Long getCocrNumCliente() {
		return cocrNumCliente;
	}

	/**
	 * @param cocrNumCliente the cocrNumCliente to set
	 */
	public void setCocrNumCliente(Long cocrNumCliente) {
		this.cocrNumCliente = cocrNumCliente;
	}

	/**
	 * @return the cocrRfc
	 */
	public String getCocrRfc() {
		return cocrRfc;
	}

	/**
	 * @param cocrRfc the cocrRfc to set
	 */
	public void setCocrRfc(String cocrRfc) {
		this.cocrRfc = cocrRfc;
	}

	/**
	 * @return the cocrCalle
	 */
	public String getCocrCalle() {
		return cocrCalle;
	}

	/**
	 * @param cocrCalle the cocrCalle to set
	 */
	public void setCocrCalle(String cocrCalle) {
		this.cocrCalle = cocrCalle;
	}

	/**
	 * @return the cocrColonia
	 */
	public String getCocrColonia() {
		return cocrColonia;
	}

	/**
	 * @param cocrColonia the cocrColonia to set
	 */
	public void setCocrColonia(String cocrColonia) {
		this.cocrColonia = cocrColonia;
	}

	/**
	 * @return the cocrDelmunic
	 */
	public String getCocrDelmunic() {
		return cocrDelmunic;
	}

	/**
	 * @param cocrDelmunic the cocrDelmunic to set
	 */
	public void setCocrDelmunic(String cocrDelmunic) {
		this.cocrDelmunic = cocrDelmunic;
	}

	/**
	 * @return the cocrCp
	 */
	public String getCocrCp() {
		return cocrCp;
	}

	/**
	 * @param cocrCp the cocrCp to set
	 */
	public void setCocrCp(String cocrCp) {
		this.cocrCp = cocrCp;
	}

	/**
	 * @return the cocrTelefono
	 */
	public String getCocrTelefono() {
		return cocrTelefono;
	}

	/**
	 * @param cocrTelefono the cocrTelefono to set
	 */
	public void setCocrTelefono(String cocrTelefono) {
		this.cocrTelefono = cocrTelefono;
	}

	/**
	 * @return the cocrTipoCliente
	 */
	public Integer getCocrTipoCliente() {
		return cocrTipoCliente;
	}

	/**
	 * @param cocrTipoCliente the cocrTipoCliente to set
	 */
	public void setCocrTipoCliente(Integer cocrTipoCliente) {
		this.cocrTipoCliente = cocrTipoCliente;
	}

	/**
	 * @return the cocrPrima
	 */
	public BigDecimal getCocrPrima() {
		return cocrPrima;
	}

	/**
	 * @param cocrPrima the cocrPrima to set
	 */
	public void setCocrPrima(BigDecimal cocrPrima) {
		this.cocrPrima = cocrPrima;
	}

	/**
	 * @return the cocrPrimaReal
	 */
	public BigDecimal getCocrPrimaReal() {
		return cocrPrimaReal;
	}

	/**
	 * @param cocrPrimaReal the cocrPrimaReal to set
	 */
	public void setCocrPrimaReal(BigDecimal cocrPrimaReal) {
		this.cocrPrimaReal = cocrPrimaReal;
	}

	/**
	 * @return the cocrBcoProducto
	 */
	public String getCocrBcoProducto() {
		return cocrBcoProducto;
	}

	/**
	 * @param cocrBcoProducto the cocrBcoProducto to set
	 */
	public void setCocrBcoProducto(String cocrBcoProducto) {
		this.cocrBcoProducto = cocrBcoProducto;
	}

	/**
	 * @return the cocrSubProducto
	 */
	public String getCocrSubProducto() {
		return cocrSubProducto;
	}

	/**
	 * @param cocrSubProducto the cocrSubProducto to set
	 */
	public void setCocrSubProducto(String cocrSubProducto) {
		this.cocrSubProducto = cocrSubProducto;
	}

	/**
	 * @return the cocrFrPago
	 */
	public String getCocrFrPago() {
		return cocrFrPago;
	}

	/**
	 * @param cocrFrPago the cocrFrPago to set
	 */
	public void setCocrFrPago(String cocrFrPago) {
		this.cocrFrPago = cocrFrPago;
	}

	/**
	 * @return the cocrNuPlazo
	 */
	public Integer getCocrNuPlazo() {
		return cocrNuPlazo;
	}

	/**
	 * @param cocrNuPlazo the cocrNuPlazo to set
	 */
	public void setCocrNuPlazo(Integer cocrNuPlazo) {
		this.cocrNuPlazo = cocrNuPlazo;
	}

	/**
	 * @return the cocrPlazoTiempo
	 */
	public String getCocrPlazoTiempo() {
		return cocrPlazoTiempo;
	}

	/**
	 * @param cocrPlazoTiempo the cocrPlazoTiempo to set
	 */
	public void setCocrPlazoTiempo(String cocrPlazoTiempo) {
		this.cocrPlazoTiempo = cocrPlazoTiempo;
	}

	/**
	 * @return the cocrRemesa
	 */
	public String getCocrRemesa() {
		return cocrRemesa;
	}

	/**
	 * @param cocrRemesa the cocrRemesa to set
	 */
	public void setCocrRemesa(String cocrRemesa) {
		this.cocrRemesa = cocrRemesa;
	}

	/**
	 * @return the cocrCuenta
	 */
	public String getCocrCuenta() {
		return cocrCuenta;
	}

	/**
	 * @param cocrCuenta the cocrCuenta to set
	 */
	public void setCocrCuenta(String cocrCuenta) {
		this.cocrCuenta = cocrCuenta;
	}

	/**
	 * @return the cocrCdCiudad
	 */
	public Byte getCocrCdCiudad() {
		return cocrCdCiudad;
	}

	/**
	 * @param cocrCdCiudad the cocrCdCiudad to set
	 */
	public void setCocrCdCiudad(Byte cocrCdCiudad) {
		this.cocrCdCiudad = cocrCdCiudad;
	}

	/**
	 * @return the cocrCdEstado
	 */
	public Byte getCocrCdEstado() {
		return cocrCdEstado;
	}

	/**
	 * @param cocrCdEstado the cocrCdEstado to set
	 */
	public void setCocrCdEstado(Byte cocrCdEstado) {
		this.cocrCdEstado = cocrCdEstado;
	}

	/**
	 * @return the cocrCdCobertura
	 */
	public Long getCocrCdCobertura() {
		return cocrCdCobertura;
	}

	/**
	 * @param cocrCdCobertura the cocrCdCobertura to set
	 */
	public void setCocrCdCobertura(Long cocrCdCobertura) {
		this.cocrCdCobertura = cocrCdCobertura;
	}

	/**
	 * @return the cocrCdComponente
	 */
	public Long getCocrCdComponente() {
		return cocrCdComponente;
	}

	/**
	 * @param cocrCdComponente the cocrCdComponente to set
	 */
	public void setCocrCdComponente(Long cocrCdComponente) {
		this.cocrCdComponente = cocrCdComponente;
	}

	/**
	 * @return the cocrNuCredito
	 */
	public Long getCocrNuCredito() {
		return cocrNuCredito;
	}

	/**
	 * @param cocrNuCredito the cocrNuCredito to set
	 */
	public void setCocrNuCredito(Long cocrNuCredito) {
		this.cocrNuCredito = cocrNuCredito;
	}

	/**
	 * @return the cocrAltaBaja
	 */
	public Byte getCocrAltaBaja() {
		return cocrAltaBaja;
	}

	/**
	 * @param cocrAltaBaja the cocrAltaBaja to set
	 */
	public void setCocrAltaBaja(Byte cocrAltaBaja) {
		this.cocrAltaBaja = cocrAltaBaja;
	}

	/**
	 * @return the cocrRegistro
	 */
	public String getCocrRegistro() {
		return cocrRegistro;
	}

	/**
	 * @param cocrRegistro the cocrRegistro to set
	 */
	public void setCocrRegistro(String cocrRegistro) {
		this.cocrRegistro = cocrRegistro;
	}

	/**
	 * @return the cocrDato1
	 */
	public String getCocrDato1() {
		return cocrDato1;
	}

	/**
	 * @param cocrDato1 the cocrDato1 to set
	 */
	public void setCocrDato1(String cocrDato1) {
		this.cocrDato1 = cocrDato1;
	}

	/**
	 * @return the cocrDato2
	 */
	public String getCocrDato2() {
		return cocrDato2;
	}

	/**
	 * @param cocrDato2 the cocrDato2 to set
	 */
	public void setCocrDato2(String cocrDato2) {
		this.cocrDato2 = cocrDato2;
	}

	/**
	 * @return the cocrDato3
	 */
	public String getCocrDato3() {
		return cocrDato3;
	}

	/**
	 * @param cocrDato3 the cocrDato3 to set
	 */
	public void setCocrDato3(String cocrDato3) {
		this.cocrDato3 = cocrDato3;
	}

	/**
	 * @return the cocrDato4
	 */
	public String getCocrDato4() {
		return cocrDato4;
	}

	/**
	 * @param cocrDato4 the cocrDato4 to set
	 */
	public void setCocrDato4(String cocrDato4) {
		this.cocrDato4 = cocrDato4;
	}

	/**
	 * @return the cocrDato5
	 */
	public String getCocrDato5() {
		return cocrDato5;
	}

	/**
	 * @param cocrDato5 the cocrDato5 to set
	 */
	public void setCocrDato5(String cocrDato5) {
		this.cocrDato5 = cocrDato5;
	}

	/**
	 * @return the cocrDato6
	 */
	public String getCocrDato6() {
		return cocrDato6;
	}

	/**
	 * @param cocrDato6 the cocrDato6 to set
	 */
	public void setCocrDato6(String cocrDato6) {
		this.cocrDato6 = cocrDato6;
	}

	/**
	 * @return the cocrDato7
	 */
	public String getCocrDato7() {
		return cocrDato7;
	}

	/**
	 * @param cocrDato7 the cocrDato7 to set
	 */
	public void setCocrDato7(String cocrDato7) {
		this.cocrDato7 = cocrDato7;
	}

	/**
	 * @return the cocrDato8
	 */
	public String getCocrDato8() {
		return cocrDato8;
	}

	/**
	 * @param cocrDato8 the cocrDato8 to set
	 */
	public void setCocrDato8(String cocrDato8) {
		this.cocrDato8 = cocrDato8;
	}

	/**
	 * @return the cocrDato9
	 */
	public String getCocrDato9() {
		return cocrDato9;
	}

	/**
	 * @param cocrDato9 the cocrDato9 to set
	 */
	public void setCocrDato9(String cocrDato9) {
		this.cocrDato9 = cocrDato9;
	}

	/**
	 * @return the cocrDato10
	 */
	public String getCocrDato10() {
		return cocrDato10;
	}

	/**
	 * @param cocrDato10 the cocrDato10 to set
	 */
	public void setCocrDato10(String cocrDato10) {
		this.cocrDato10 = cocrDato10;
	}

	/**
	 * @return the cocrDato11
	 */
	public String getCocrDato11() {
		return cocrDato11;
	}

	/**
	 * @param cocrDato11 the cocrDato11 to set
	 */
	public void setCocrDato11(String cocrDato11) {
		this.cocrDato11 = cocrDato11;
	}

	/**
	 * @return the cocrDato12
	 */
	public String getCocrDato12() {
		return cocrDato12;
	}

	/**
	 * @param cocrDato12 the cocrDato12 to set
	 */
	public void setCocrDato12(String cocrDato12) {
		this.cocrDato12 = cocrDato12;
	}

	/**
	 * @return the cocrDato13
	 */
	public String getCocrDato13() {
		return cocrDato13;
	}

	/**
	 * @param cocrDato13 the cocrDato13 to set
	 */
	public void setCocrDato13(String cocrDato13) {
		this.cocrDato13 = cocrDato13;
	}

	/**
	 * @return the cocrDato14
	 */
	public String getCocrDato14() {
		return cocrDato14;
	}

	/**
	 * @param cocrDato14 the cocrDato14 to set
	 */
	public void setCocrDato14(String cocrDato14) {
		this.cocrDato14 = cocrDato14;
	}

	/**
	 * @return the cocrDato15
	 */
	public String getCocrDato15() {
		return cocrDato15;
	}

	/**
	 * @param cocrDato15 the cocrDato15 to set
	 */
	public void setCocrDato15(String cocrDato15) {
		this.cocrDato15 = cocrDato15;
	}

	/**
	 * @return the cocrDato16
	 */
	public String getCocrDato16() {
		return cocrDato16;
	}

	/**
	 * @param cocrDato16 the cocrDato16 to set
	 */
	public void setCocrDato16(String cocrDato16) {
		this.cocrDato16 = cocrDato16;
	}

	/**
	 * @return the cocrDato17
	 */
	public String getCocrDato17() {
		return cocrDato17;
	}

	/**
	 * @param cocrDato17 the cocrDato17 to set
	 */
	public void setCocrDato17(String cocrDato17) {
		this.cocrDato17 = cocrDato17;
	}

	/**
	 * @return the cocrDato18
	 */
	public String getCocrDato18() {
		return cocrDato18;
	}

	/**
	 * @param cocrDato18 the cocrDato18 to set
	 */
	public void setCocrDato18(String cocrDato18) {
		this.cocrDato18 = cocrDato18;
	}

	/**
	 * @return the cocrDato20
	 */
	public String getCocrDato20() {
		return cocrDato20;
	}

	/**
	 * @param cocrDato20 the cocrDato20 to set
	 */
	public void setCocrDato20(String cocrDato20) {
		this.cocrDato20 = cocrDato20;
	}

	/**
	 * @return the cocrDato21
	 */
	public String getCocrDato21() {
		return cocrDato21;
	}

	/**
	 * @param cocrDato21 the cocrDato21 to set
	 */
	public void setCocrDato21(String cocrDato21) {
		this.cocrDato21 = cocrDato21;
	}

	/**
	 * @return the cocrDato22
	 */
	public String getCocrDato22() {
		return cocrDato22;
	}

	/**
	 * @param cocrDato22 the cocrDato22 to set
	 */
	public void setCocrDato22(String cocrDato22) {
		this.cocrDato22 = cocrDato22;
	}

	/**
	 * @return the cocrDato23
	 */
	public String getCocrDato23() {
		return cocrDato23;
	}

	/**
	 * @param cocrDato23 the cocrDato23 to set
	 */
	public void setCocrDato23(String cocrDato23) {
		this.cocrDato23 = cocrDato23;
	}

	/**
	 * @return the cocrDato24
	 */
	public String getCocrDato24() {
		return cocrDato24;
	}

	/**
	 * @param cocrDato24 the cocrDato24 to set
	 */
	public void setCocrDato24(String cocrDato24) {
		this.cocrDato24 = cocrDato24;
	}

	/**
	 * @return the cocrDato25
	 */
	public String getCocrDato25() {
		return cocrDato25;
	}

	/**
	 * @param cocrDato25 the cocrDato25 to set
	 */
	public void setCocrDato25(String cocrDato25) {
		this.cocrDato25 = cocrDato25;
	}

	/**
	 * @return the cocrDato26
	 */
	public String getCocrDato26() {
		return cocrDato26;
	}

	/**
	 * @param cocrDato26 the cocrDato26 to set
	 */
	public void setCocrDato26(String cocrDato26) {
		this.cocrDato26 = cocrDato26;
	}

	/**
	 * @return the cocrDato27
	 */
	public String getCocrDato27() {
		return cocrDato27;
	}

	/**
	 * @param cocrDato27 the cocrDato27 to set
	 */
	public void setCocrDato27(String cocrDato27) {
		this.cocrDato27 = cocrDato27;
	}

	/**
	 * @return the cocrDato28
	 */
	public String getCocrDato28() {
		return cocrDato28;
	}

	/**
	 * @param cocrDato28 the cocrDato28 to set
	 */
	public void setCocrDato28(String cocrDato28) {
		this.cocrDato28 = cocrDato28;
	}

	/**
	 * @return the cocrDato29
	 */
	public String getCocrDato29() {
		return cocrDato29;
	}

	/**
	 * @param cocrDato29 the cocrDato29 to set
	 */
	public void setCocrDato29(String cocrDato29) {
		this.cocrDato29 = cocrDato29;
	}

	/**
	 * @return the cocrDato30
	 */
	public String getCocrDato30() {
		return cocrDato30;
	}

	/**
	 * @param cocrDato30 the cocrDato30 to set
	 */
	public void setCocrDato30(String cocrDato30) {
		this.cocrDato30 = cocrDato30;
	}

	/**
	 * @return the cocrComisiones
	 */
	public String getCocrComisiones() {
		return cocrComisiones;
	}

	/**
	 * @param cocrComisiones the cocrComisiones to set
	 */
	public void setCocrComisiones(String cocrComisiones) {
		this.cocrComisiones = cocrComisiones;
	}	
}
