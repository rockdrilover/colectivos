package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		09-03-2020
 * Description: Clase para trabajar desde la vista los datos que muestran 
 * 				el resumen de creditos a cancelar
 * 				Extiende de la clase ResumenCancelaDTO2
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class ResumenCancelaDTO extends ResumenCancelaDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo cantidad
	private BigDecimal cantidad;
	//Propiedad para campo cdSucursal
	private BigDecimal cdSucursal;	
	//Propiedad para campo cdRamo
	private BigDecimal cdRamo;
	//Propiedad para campo nuPoliza
	private BigDecimal nuPoliza;

	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param datos objeto con datos
	 */
	public ResumenCancelaDTO(Object[] datos) {
		super(datos);
		this.cantidad = (BigDecimal) datos[0];
		this.cdSucursal = (BigDecimal) datos[1];
		this.cdRamo = (BigDecimal) datos[2];
		this.nuPoliza = (BigDecimal) datos[3];
	}
	
	/**
	 * Constructor de clase
	 */
	public ResumenCancelaDTO() {
		super();
	}
	
	/**
	 * @return the cantidad
	 */
	public BigDecimal getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the cdSucursal
	 */
	public BigDecimal getCdSucursal() {
		return cdSucursal;
	}

	/**
	 * @param cdSucursal the cdSucursal to set
	 */
	public void setCdSucursal(BigDecimal cdSucursal) {
		this.cdSucursal = cdSucursal;
	}

	/**
	 * @return the cdRamo
	 */
	public BigDecimal getCdRamo() {
		return cdRamo;
	}

	/**
	 * @param cdRamo the cdRamo to set
	 */
	public void setCdRamo(BigDecimal cdRamo) {
		this.cdRamo = cdRamo;
	}

	/**
	 * @return the nuPoliza
	 */
	public BigDecimal getNuPoliza() {
		return nuPoliza;
	}

	/**
	 * @param nuPoliza the nuPoliza to set
	 */
	public void setNuPoliza(BigDecimal nuPoliza) {
		this.nuPoliza = nuPoliza;
	}
}