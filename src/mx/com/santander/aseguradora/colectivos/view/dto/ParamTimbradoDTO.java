package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase para trabajar desde la vista los datos para Parametrizacion Timbrado.
 * 				Extiende de la clase ParamTimbradoDTO2
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 2.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 29-12-2022
 * 		Porque: Se extiende de nuevo DTO
 * ------------------------------------------------------------------------
 * 
 */
public class ParamTimbradoDTO extends ParamTimbradoDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo razonSocial
	private String razonSocial;
	//Propiedad para campo metodoPago
	private String metodoPago;
	//Propiedad para campo rfc
	private String rfc;
	//Propiedad para campo objParam
	private transient BeanParametros objParam;


	/**
	 * Constructor de clase
	 */
	public ParamTimbradoDTO() {
		super();
		objParam = new BeanParametros();
		this.rfc = Constantes.DEFAULT_STRING;
		this.razonSocial = Constantes.DEFAULT_STRING;
		this.metodoPago = Constantes.DEFAULT_STRING;
	}
	
	/**
	 * Constructor de clase
	 * @param arrDatos arreglo de datos
	 * @param idVenta id venta del registro
	 */
	public ParamTimbradoDTO(Object[] arrDatos, Integer idVenta) {
		//se setean los valores del arreglo de datos
		this.objParam = new BeanParametros();
		setObjCobranza(new BeanParametros());
		this.objParam.setCopaNvalor1(((BigDecimal) arrDatos[0]).intValue());
		this.objParam.setCopaNvalor2(((BigDecimal) arrDatos[1]).longValue());
		
		//se valida el id de venta
		if(idVenta == Constantes.DEFAULT_INT) {
			this.objParam.setCopaNvalor3(((BigDecimal) arrDatos[2]).longValue());  	//Poliza
			this.objParam.setCopaNvalor4(Constantes.DEFAULT_INT);	//IdVenta
		} else {
			this.objParam.setCopaNvalor3(Constantes.DEFAULT_LONG);  	//Poliza
			this.objParam.setCopaNvalor4(idVenta);	//IdVenta
		}
		
		this.rfc = arrDatos[3].toString();
		this.razonSocial = arrDatos[4].toString() + "-" + arrDatos[5].toString() + " : " + arrDatos[6].toString();
		this.objParam.setAuxDato(this.objParam.getCopaNvalor1().toString() + "-" + 
								this.objParam.getCopaNvalor2().toString() + "-" + 
								this.objParam.getCopaNvalor3().toString() + "-" +
								this.objParam.getCopaNvalor4().toString() + "  " +
								this.rfc);
		
		//si vienen nullos los valores de la parametrizacion de timbrado no se setean los datos
		if(arrDatos[7] != null) {
			llenadoObjetoParametro(arrDatos,objParam,0);
		}
		//si vienen nullos los valores de la parametrizacion de cobranza no se setean los datos
		if(arrDatos[32] != null) {
			llenadoObjetoParametro(arrDatos,getObjCobranza(),1);
			getObjCobranza().setCopaNvalor1(objParam.getCopaNvalor1());
			getObjCobranza().setCopaNvalor2(objParam.getCopaNvalor2());
			getObjCobranza().setCopaNvalor3(objParam.getCopaNvalor3());
		}
				
	}

	/**
	 * @return the razonSocial
	 */
	public String getRazonSocial() {
		return razonSocial;
	}
	/**
	 * @param razonSocial the razonSocial to set
	 */
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	/**
	 * @return the metodoPago
	 */
	public String getMetodoPago() {
		return metodoPago;
	}
	/**
	 * @param metodoPago the metodoPago to set
	 */
	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}
	/**
	 * @return the objParam
	 */
	public BeanParametros getObjParam() {
		return objParam;
	}
	/**
	 * @param objParam the objParam to set
	 */
	public void setObjParam(BeanParametros objParam) {
		this.objParam = objParam;
	}
	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

}