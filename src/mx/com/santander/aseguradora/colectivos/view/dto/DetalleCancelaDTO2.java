package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_PRECARGA.
 * 				La ocupa la clase DetalleCancelaDTO
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class DetalleCancelaDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo pmadev
	private Double pmadev;
	//Propiedad para campo pmacan
	private Double pmacan;
	//Propiedad para campo error
	private String error;
	//Propiedad para campo check
	private boolean check;

	/**
	 * Constructor de clase
	 * @param datos info de bitacora errores
	 */
	public DetalleCancelaDTO2(PreCarga datos) {
		this.pmadev = Double.valueOf((String) Utilerias.objectIsNull(datos.getCopcPrima(), Constantes.TIPO_DATO_STRING_ID));
		this.pmacan = Double.valueOf((String) Utilerias.objectIsNull(datos.getCopcNumCliente(), Constantes.TIPO_DATO_STRING_ID));
		this.error = (String) Utilerias.objectIsNull(datos.getCopcRegistro(), Constantes.TIPO_DATO_STRING);
	}

	/**
	 * Constructor de clase
	 */
	public DetalleCancelaDTO2() {
		super();
	}

	/**
	 * @return the pmadev
	 */
	public Double getPmadev() {
		return pmadev;
	}

	/**
	 * @param pmadev the pmadev to set
	 */
	public void setPmadev(Double pmadev) {
		this.pmadev = pmadev;
	}

	/**
	 * @return the pmacan
	 */
	public Double getPmacan() {
		return pmacan;
	}

	/**
	 * @param pmacan the pmacan to set
	 */
	public void setPmacan(Double pmacan) {
		this.pmacan = pmacan;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	
	/**
	 * @return the check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * @param check the check to set
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
}
	