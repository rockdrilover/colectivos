package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;


public class ClienteCertifDTO implements java.io.Serializable {

	private static final long serialVersionUID = 7436219845823003493L;
	private ClienteCertifId id;
	private Cliente cliente;
	private Certificado certificado;
	private Short coccTpCliente;
    private String coccVvalor1;
    private String coccVvalor2;
    private String coccVvalor3;
    private Integer coccNvalor1;
    private Integer coccNvalor2;
    private BigDecimal coccNvalor3;
    private Date coccFvalor1;
    private Date coccFvalor2;
    private boolean checkE;
    
	public ClienteCertifDTO() {
		id = new ClienteCertifId();
	}

	public ClienteCertifDTO(ClienteCertifId id,
			Cliente colectivosClientes,
			Certificado colectivosCertificados,
			Short coccTpCliente, String coccVvalor1, 
			String coccVvalor2, String coccVvalor3, 
			Integer coccNvalor1, Integer coccNvalor2, 
			BigDecimal coccNvalor3, Date coccFvalor1, Date coccFvalor2){

		this.id = id;
		this.cliente = colectivosClientes;
		this.certificado = colectivosCertificados;
		this.coccTpCliente = coccTpCliente;
		this.coccVvalor1 = coccVvalor1;
		this.coccVvalor2 = coccVvalor2;
		this.coccVvalor3 = coccVvalor3;
		this.coccNvalor1 = coccNvalor1;
		this.coccNvalor2 = coccNvalor2;
		this.coccNvalor3 = coccNvalor3;
		this.coccFvalor1 = coccFvalor1;
		this.coccFvalor2 = coccFvalor2;
	}
	
	public ClienteCertifId getId() {
		return this.id;
	}
	public void setId(ClienteCertifId id) {
		this.id = id;
	}
	public Cliente getCliente() {
		return this.cliente;
	}
	public void setCliente(Cliente colectivosClientes) {
		this.cliente = colectivosClientes;
	}
	public Certificado getCertificado() {
		return this.certificado;
	}
	public void setCertificado(
			Certificado colectivosCertificados) {
		this.certificado = colectivosCertificados;
	}
    public Short getCoccTpCliente() {
        return this.coccTpCliente;
    }
    public void setCoccTpCliente(Short coccTpCliente) {
        this.coccTpCliente = coccTpCliente;
    }
    public String getCoccVvalor1() {
        return this.coccVvalor1;
    }
    public void setCoccVvalor1(String coccVvalor1) {
        this.coccVvalor1 = coccVvalor1;
    }
    public String getCoccVvalor2() {
        return this.coccVvalor2;
    }
    public void setCoccVvalor2(String coccVvalor2) {
        this.coccVvalor2 = coccVvalor2;
    }
    public String getCoccVvalor3() {
        return this.coccVvalor3;
    }
    public void setCoccVvalor3(String coccVvalor3) {
        this.coccVvalor3 = coccVvalor3;
    }
    public Integer getCoccNvalor1() {
        return this.coccNvalor1;
    }
    public void setCoccNvalor1(Integer coccNvalor1) {
        this.coccNvalor1 = coccNvalor1;
    }
    public Integer getCoccNvalor2() {
        return this.coccNvalor2;
    }
    public void setCoccNvalor2(Integer coccNvalor2) {
        this.coccNvalor2 = coccNvalor2;
    }
    public BigDecimal getCoccNvalor3() {
        return this.coccNvalor3;
    }
    public void setCoccNvalor3(BigDecimal coccNvalor3) {
        this.coccNvalor3 = coccNvalor3;
    }
    public Date getCoccFvalor1() {
        return this.coccFvalor1;
    }
    public void setCoccFvalor1(Date coccFvalor1) {
        this.coccFvalor1 = coccFvalor1;
    }
    public Date getCoccFvalor2() {
        return this.coccFvalor2;
    }
    public void setCoccFvalor2(Date coccFvalor2) {
        this.coccFvalor2 = coccFvalor2;
    }
	public void setCheckE(boolean checkE) {
		this.checkE = checkE;
	}
	public boolean isCheckE() {
		return checkE;
	}

}