package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase para trabajar desde la vista los datos para Parametrizacion Timbrado.
 * 				Se agregaron metodos y propiedades para trabajar con 
 * 				datos de cobranza
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class ParamTimbradoDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo objParam cobranza
	private transient BeanParametros objCobranza;


	/**
	 * Constructor de clase
	 */
	public ParamTimbradoDTO2() {
		super();
		objCobranza = new BeanParametros();
	}
	
	/**
	 * Metodo que regresa el objeto parametro con los datos de la base
	 * @param arrDatos datos de la base
	 * @param parametros objeto a llenar
	 * @param opcion opcion a relizar
	 * @return objeto con datos
	 */
	public final BeanParametros llenadoObjetoParametro(Object[] arrDatos, BeanParametros parametros, int opcion) {
		if (opcion == 1) { //Cobranza
			parametros.setCopaCdParametro(((BigDecimal) arrDatos[32]).longValue());
			parametros.setCopaDesParametro(arrDatos[33].toString());
			parametros.setCopaIdParametro(arrDatos[34].toString());
			parametros.setCopaVvalor1(Utilerias.objectIsNull(arrDatos[35], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor2(Utilerias.objectIsNull(arrDatos[36], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor5(Utilerias.objectIsNull(arrDatos[37], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaNvalor5(((BigDecimal) Utilerias.objectIsNull(arrDatos[38], Constantes.TIPO_DATO_BIGDECIMAL)).intValue());
			parametros.setCopaVvalor3(Utilerias.objectIsNull(arrDatos[39], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor4(Utilerias.objectIsNull(arrDatos[40], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor6(Utilerias.objectIsNull(arrDatos[41], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaNvalor4(((BigDecimal) Utilerias.objectIsNull(arrDatos[42], Constantes.TIPO_DATO_BIGDECIMAL)).intValue());
			parametros.setCopaVvalor7(Utilerias.objectIsNull(arrDatos[43], Constantes.TIPO_DATO_STRING).toString());
		}else {//Timbrado
			parametros.setCopaCdParametro(((BigDecimal) arrDatos[7]).longValue());
			parametros.setCopaDesParametro(arrDatos[8].toString());
			parametros.setCopaIdParametro(arrDatos[9].toString());
			parametros.setCopaVvalor1(Utilerias.objectIsNull(arrDatos[10], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor2(Utilerias.objectIsNull(arrDatos[11], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor3(Utilerias.objectIsNull(arrDatos[12], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor4(Utilerias.objectIsNull(arrDatos[13], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor5(Utilerias.objectIsNull(arrDatos[14], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor6(Utilerias.objectIsNull(arrDatos[15], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor7(Utilerias.objectIsNull(arrDatos[16], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaVvalor8(Utilerias.objectIsNull(arrDatos[17], Constantes.TIPO_DATO_STRING).toString());
			parametros.setCopaNvalor5(((BigDecimal) Utilerias.objectIsNull(arrDatos[22], Constantes.TIPO_DATO_BIGDECIMAL)).intValue());
			parametros.setCopaNvalor6(((BigDecimal) Utilerias.objectIsNull(arrDatos[23], Constantes.TIPO_DATO_BIGDECIMAL)).doubleValue());
			parametros.setCopaNvalor7(((BigDecimal) Utilerias.objectIsNull(arrDatos[24], Constantes.TIPO_DATO_BIGDECIMAL)).doubleValue());
			parametros.setCopaNvalor8(((BigDecimal) Utilerias.objectIsNull(arrDatos[25], Constantes.TIPO_DATO_BIGDECIMAL)).doubleValue());
			parametros.setCopaFvalor1((Date) arrDatos[26]);
			parametros.setCopaFvalor2((Date) arrDatos[27]);
			parametros.setCopaFvalor3((Date) arrDatos[28]);
			parametros.setCopaFvalor4((Date) arrDatos[29]);
			parametros.setCopaFvalor5((Date) arrDatos[30]);
			parametros.setCopaRegistro(Utilerias.objectIsNull(arrDatos[31], Constantes.TIPO_DATO_STRING).toString());
		}
		return parametros;
	}
	
	/**
	 * @return the objCobranza
	 */
	public final BeanParametros getObjCobranza() {
		return objCobranza;
	}

	/**
	 * @param objCobranza the objCobranza to set
	 */
	public final void setObjCobranza(BeanParametros objCobranza) {
		this.objCobranza = objCobranza;
	}

}