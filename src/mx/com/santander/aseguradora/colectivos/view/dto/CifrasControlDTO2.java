package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bo.CifrasControl;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CIFRAS_CONTROL.
 * 				La ocupa la clase CifrasControlDTO
 * 				Extiende de la clase CifrasControlDTO3
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CifrasControlDTO2 extends CifrasControlDTO3 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo cociFechaCarga
	private Date cociFechaCarga;
	//Propiedad para campo cociFvalor1
	private Date cociFvalor1;
	//Propiedad para campo cociFvalor2
	@SuppressWarnings("unused")
	private Date cociFvalor2;
	//Propiedad para campo cociNombreArchivo
	private String cociNombreArchivo;
	//Propiedad para campo cociIdVenta
	private BigDecimal cociIdVenta;
	
	
	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cifras objeto con datos
	 */
	public CifrasControlDTO2(CifrasControl cifras) {
		super(cifras);
		this.cociFechaCarga= cifras.getCociFechaCarga();
		this.cociFvalor1= cifras.getCociFvalor1();
		this.cociFvalor2= cifras.getCociFvalor2();
		this.cociIdVenta= cifras.getCociIdVenta();
		this.cociNombreArchivo= cifras.getCc2().getCociNombreArchivo();
		
	}
	
	/**
	 * Constructor de clase
	 */
	public CifrasControlDTO2() {
		super();
		this.cociFechaCarga = new Date();
		this.cociFvalor1 = new Date();
		this.cociFvalor2 = new Date();
	}

	/**
	 * @return the cociFechaCarga
	 */
	public Date getCociFechaCarga() {
		return new Date(cociFechaCarga.getTime());
	}

	/**
	 * @param cociFechaCarga the cociFechaCarga to set
	 */
	public void setCociFechaCarga(Date cociFechaCarga) {
		if(cociFechaCarga != null) {
			this.cociFechaCarga = new Date(cociFechaCarga.getTime());
		}
	}

	/**
	 * @return the cociFvalor1
	 */
	public Date getCociFvalor1() {
		return new Date(cociFvalor1.getTime());
	}

	/**
	 * @param cociFvalor1 the cociFvalor1 to set
	 */
	public void setCociFvalor1(Date cociFvalor1) {
		if(cociFvalor1 != null) {
			this.cociFvalor1 = new Date(cociFvalor1.getTime());
		}
	}

	/**
	 * @return the cociIdVenta
	 */
	public BigDecimal getCociIdVenta() {
		return cociIdVenta;
	}

	/**
	 * @param cociIdVenta the cociIdVenta to set
	 */
	public void setCociIdVenta(BigDecimal cociIdVenta) {
		this.cociIdVenta = cociIdVenta;
	}

	/**
	 * @return the cociNombreArchivo
	 */
	public String getCociNombreArchivo() {
		return cociNombreArchivo;
	}

	/**
	 * @param cociNombreArchivo the cociNombreArchivo to set
	 */
	public void setCociNombreArchivo(String cociNombreArchivo) {
		this.cociNombreArchivo = cociNombreArchivo;
	}

}