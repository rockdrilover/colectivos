package mx.com.santander.aseguradora.colectivos.view.dto;

/**
 * respuesta de timbrado
 * @author CJPV - vectormx
 * @version  1 - 07 -11 -2017
 *
 */
public class ResTimbradoCFDIDTO {
	
	/**
	 * Clave mensaje
	 */
	private String mensaje;
	
	/**
	 * Descripcion mensaje
	 */
	private String descripcion;

	public ResTimbradoCFDIDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
}
