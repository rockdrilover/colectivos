package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.model.bo.BitacoraErrores;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_BITACORA_ERRORES. 
 * 				La ocupa la clase BitacoraErroresDTO4
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BitacoraErroresDTO5 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coerVvalor2
	private String coerVvalor2;
	//Propiedad para campo coerVvalor3
	@SuppressWarnings("unused")
	private String coerVvalor3;
	//Propiedad para campo coerDescError
	private String coerDescError;
	//Propiedad para campo check
	private boolean check;
	//Propiedad para campo checkHabilita
	private boolean checkHabilita;
	
	/**
	 * Constructor de clase
	 * @param detalle datos de bitacora errores
	 */
	public BitacoraErroresDTO5(BitacoraErrores detalle) {
		this.coerDescError = detalle.getCoerDescError();
		this.coerVvalor2 = detalle.getBe3().getCoerVvalor2();
		this.coerVvalor3 = detalle.getBe3().getCoerVvalor3();
	}
	
	/**
	 * Constructor de clase
	 */
	public BitacoraErroresDTO5() {
		// Do nothing because 
	}
	
	/**
	 * @return the check
	 */
	public boolean isCheck() {
		return check;
	}

	/**
	 * @param check the check to set
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}

	/**
	 * @return the coerDescError
	 */
	public String getCoerDescError() {
		return coerDescError;
	}

	/**
	 * @param coerDescError the coerDescError to set
	 */
	public void setCoerDescError(String coerDescError) {
		this.coerDescError = coerDescError;
	}
	
	/**
	 * @return the coerVvalor2
	 */
	public String getCoerVvalor2() {
		return coerVvalor2;
	}

	/**
	 * @param coerVvalor2 the coerVvalor2 to set
	 */
	public void setCoerVvalor2(String coerVvalor2) {
		this.coerVvalor2 = coerVvalor2;
	}

	/**
	 * @return the checkHabilita
	 */
	public boolean isCheckHabilita() {
		return checkHabilita;
	}
	
	/**
	 * @param checkHabilita the checkHabilita to set
	 */
	public void setCheckHabilita(boolean checkHabilita) {
		this.checkHabilita = checkHabilita;
	}
}
	