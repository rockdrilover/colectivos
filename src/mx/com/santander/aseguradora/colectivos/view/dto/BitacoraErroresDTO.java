package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.bo.BitacoraErrores;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_BITACORA_ERRORES.
 * 				Extiende de la clase BitacoraErroresDTO2
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BitacoraErroresDTO extends BitacoraErroresDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coerCapoNuPoliza
	private BigDecimal coerCapoNuPoliza;
	//Propiedad para campo coerCarpCdRamo
	private BigDecimal coerCarpCdRamo;
	//Propiedad para campo coerCasuCdSucursal
	private BigDecimal coerCasuCdSucursal;
	//Propiedad para campo coerCdError
	private BigDecimal coerCdError;

	/**
	 * Constructor de clase
	 * @param detalle datos de bitacora errores
	 */
	public BitacoraErroresDTO(BitacoraErrores detalle) {
		super(detalle);
		this.coerCapoNuPoliza = detalle.getCoerCapoNuPoliza();
		this.coerCarpCdRamo = detalle.getCoerCarpCdRamo();
		this.coerCasuCdSucursal = detalle.getCoerCasuCdSucursal();
		this.coerCdError = detalle.getCoerCdError();
	}

	/**
	 * Constructor de clase
	 */
	public BitacoraErroresDTO() {
		super();
	}

	/**
	 * @return the coerCapoNuPoliza
	 */
	public BigDecimal getCoerCapoNuPoliza() {
		return coerCapoNuPoliza;
	}

	/**
	 * @param coerCapoNuPoliza the coerCapoNuPoliza to set
	 */
	public void setCoerCapoNuPoliza(BigDecimal coerCapoNuPoliza) {
		this.coerCapoNuPoliza = coerCapoNuPoliza;
	}

	/**
	 * @return the coerCarpCdRamo
	 */
	public BigDecimal getCoerCarpCdRamo() {
		return coerCarpCdRamo;
	}

	/**
	 * @param coerCarpCdRamo the coerCarpCdRamo to set
	 */
	public void setCoerCarpCdRamo(BigDecimal coerCarpCdRamo) {
		this.coerCarpCdRamo = coerCarpCdRamo;
	}

	/**
	 * @return the coerCasuCdSucursal
	 */
	public BigDecimal getCoerCasuCdSucursal() {
		return coerCasuCdSucursal;
	}

	/**
	 * @param coerCasuCdSucursal the coerCasuCdSucursal to set
	 */
	public void setCoerCasuCdSucursal(BigDecimal coerCasuCdSucursal) {
		this.coerCasuCdSucursal = coerCasuCdSucursal;
	}

	/**
	 * @return the coerCdError
	 */
	public BigDecimal getCoerCdError() {
		return coerCdError;
	}

	/**
	 * @param coerCdError the coerCdError to set
	 */
	public void setCoerCdError(BigDecimal coerCdError) {
		this.coerCdError = coerCdError;
	}
}
	