package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author CJPV - vectormx
 * @version 06-09-2017
 */
public class SeguimientoDTO {
	private long coseId;
	private String coseArea;
	private BigDecimal coseConsecutivo;
	private String coseEstatus;
	private Date coseFechaAsignacion;
	private Date coseFechaAtencion;
	private String coseObservaciones;
	private String coseRol;
	private String coseUsuarioAsigno;
	private String coseUsuarioAtendio;
	private long coseCosuId;
	private long coseGrupo;
	private String coseObsAsig;
	
	/**
	 * 
	 */
	public SeguimientoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param coseId
	 * @param coseArea
	 * @param coseConsecutivo
	 * @param coseEstatus
	 * @param coseFechaAsignacion
	 * @param coseFechaAtencion
	 * @param coseObservaciones
	 * @param coseRol
	 * @param coseUsuarioAsigno
	 * @param coseUsuarioAtendio
	 * @param coseCosuId
	 */
	public SeguimientoDTO(long coseId, String coseArea,
			BigDecimal coseConsecutivo, String coseEstatus,
			Date coseFechaAsignacion, Date coseFechaAtencion,
			String coseObservaciones, String coseRol, String coseUsuarioAsigno,
			String coseUsuarioAtendio, long coseCosuId) {
		super();
		this.coseId = coseId;
		this.coseArea = coseArea;
		this.coseConsecutivo = coseConsecutivo;
		this.coseEstatus = coseEstatus;
		this.coseFechaAsignacion = coseFechaAsignacion;
		this.coseFechaAtencion = coseFechaAtencion;
		this.coseObservaciones = coseObservaciones;
		this.coseRol = coseRol;
		this.coseUsuarioAsigno = coseUsuarioAsigno;
		this.coseUsuarioAtendio = coseUsuarioAtendio;
		this.coseCosuId = coseCosuId;
	}
	public long getCoseId() {
		return coseId;
	}
	public void setCoseId(long coseId) {
		this.coseId = coseId;
	}
	public String getCoseArea() {
		return coseArea;
	}
	public void setCoseArea(String coseArea) {
		this.coseArea = coseArea;
	}
	public BigDecimal getCoseConsecutivo() {
		return coseConsecutivo;
	}
	public void setCoseConsecutivo(BigDecimal coseConsecutivo) {
		this.coseConsecutivo = coseConsecutivo;
	}
	public String getCoseEstatus() {
		return coseEstatus;
	}
	public void setCoseEstatus(String coseEstatus) {
		this.coseEstatus = coseEstatus;
	}
	public Date getCoseFechaAsignacion() {
		return coseFechaAsignacion;
	}
	public void setCoseFechaAsignacion(Date coseFechaAsignacion) {
		this.coseFechaAsignacion = coseFechaAsignacion;
	}
	public Date getCoseFechaAtencion() {
		return coseFechaAtencion;
	}
	public void setCoseFechaAtencion(Date coseFechaAtencion) {
		this.coseFechaAtencion = coseFechaAtencion;
	}
	public String getCoseObservaciones() {
		return coseObservaciones;
	}
	public void setCoseObservaciones(String coseObservaciones) {
		this.coseObservaciones = coseObservaciones;
	}
	public String getCoseRol() {
		return coseRol;
	}
	public void setCoseRol(String coseRol) {
		this.coseRol = coseRol;
	}
	public String getCoseUsuarioAsigno() {
		return coseUsuarioAsigno;
	}
	public void setCoseUsuarioAsigno(String coseUsuarioAsigno) {
		this.coseUsuarioAsigno = coseUsuarioAsigno;
	}
	public String getCoseUsuarioAtendio() {
		return coseUsuarioAtendio;
	}
	public void setCoseUsuarioAtendio(String coseUsuarioAtendio) {
		this.coseUsuarioAtendio = coseUsuarioAtendio;
	}
	public long getCoseCosuId() {
		return coseCosuId;
	}
	public void setCoseCosuId(long coseCosuId) {
		this.coseCosuId = coseCosuId;
	}
	/**
	 * @return the coseGrupo
	 */
	public long getCoseGrupo() {
		return coseGrupo;
	}
	/**
	 * @param coseGrupo the coseGrupo to set
	 */
	public void setCoseGrupo(long coseGrupo) {
		this.coseGrupo = coseGrupo;
	}
	public String getCoseObsAsig() {
		return coseObsAsig;
	}
	public void setCoseObsAsig(String coseObsAsig) {
		this.coseObsAsig = coseObsAsig;
	}
	
	
	
	
}
