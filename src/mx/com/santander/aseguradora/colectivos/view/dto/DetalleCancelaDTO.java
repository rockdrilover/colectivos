package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_PRECARGA.
 * 				Extiende de la clase DetalleCancelaDTO2
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class DetalleCancelaDTO extends DetalleCancelaDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo certificado
	private String certificado;
	//Propiedad para campo credito
	private String credito;
	//Propiedad para campo fechaBaja
	private Date fechaBaja;
	//Propiedad para campo fechaIngreso
	private Date fechaIngreso;

	/**
	 * Constructor de clase
	 * @param datos info de bitacora errores
	 */
	public DetalleCancelaDTO(PreCarga datos) {
		super(datos);
		this.certificado = (String) Utilerias.objectIsNull(datos.getCopcDato2(), Constantes.TIPO_DATO_STRING_ID);
		this.credito = (String) Utilerias.objectIsNull(datos.getId().getCopcIdCertificado(), Constantes.TIPO_DATO_STRING_ID);
		this.fechaBaja = GestorFechas.generateDate((String) Utilerias.objectIsNull(datos.getCopcFeStatus(), Constantes.TIPO_DATO_DATE_STRING), Constantes.FORMATO_FECHA_UNO);
		this.fechaIngreso = GestorFechas.generateDate((String) Utilerias.objectIsNull(datos.getCopcFeIngreso(), Constantes.TIPO_DATO_DATE_STRING), Constantes.FORMATO_FECHA_UNO);
	}

	/**
	 * Constructor de clase
	 */
	public DetalleCancelaDTO() {
		super();
		this.fechaBaja = new Date();
		this.fechaIngreso = new Date();
	}

	/**
	 * @return the certificado
	 */
	public String getCertificado() {
		return certificado;
	}

	/**
	 * @param certificado the certificado to set
	 */
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	/**
	 * @return the credito
	 */
	public String getCredito() {
		return credito;
	}

	/**
	 * @param credito the credito to set
	 */
	public void setCredito(String credito) {
		this.credito = credito;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return new Date(fechaBaja.getTime());
	}

	/**
	 * @param fechaBaja the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		if(fechaBaja != null) {
			this.fechaBaja = new Date(fechaBaja.getTime());
		}
	}

	/**
	 * @return the fechaIngreso
	 */
	public Date getFechaIngreso() {
		return new Date(fechaIngreso.getTime());
	}

	/**
	 * @param fechaIngreso the fechaIngreso to set
	 */
	public void setFechaIngreso(Date fechaIngreso) {
		if(fechaIngreso != null) {
			this.fechaIngreso = new Date(fechaIngreso.getTime());
		}
	}
	
}
	