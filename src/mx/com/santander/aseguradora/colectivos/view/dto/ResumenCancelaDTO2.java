package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		09-03-2020
 * Description:  Clase para trabajar desde la vista los datos que muestran 
 * 				el resumen de creditos a cancelar
 * 				La ocupa la clase ResumenCancelaDTO
 * 				Extiende de la clase ResumenCancelaDTO3
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class ResumenCancelaDTO2 extends ResumenCancelaDTO3 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo descVenta
	private String descVenta;
	//Propiedad para campo causaAnulacion
	private String causaAnulacion;
	//Propiedad para campo descTipo
	private String descTipo;
	//Propiedad para campo pmaDev
	private Double pmaDev;
	
	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param datos objeto con datos
	 */
	public ResumenCancelaDTO2(Object[] datos) {
		super(datos);
		this.descVenta = datos[4].toString();
		this.causaAnulacion = datos[5].toString();
		this.descTipo = datos[6].toString();
		this.pmaDev = ((BigDecimal) datos[7]).doubleValue();
	}

	/**
	 * Constructor de clase
	 */
	public ResumenCancelaDTO2() {
		super();
	}
	
	/**
	 * @return the descVenta
	 */
	public String getDescVenta() {
		return descVenta;
	}

	/**
	 * @param descVenta the descVenta to set
	 */
	public void setDescVenta(String descVenta) {
		this.descVenta = descVenta;
	}

	/**
	 * @return the causaAnulacion
	 */
	public String getCausaAnulacion() {
		return causaAnulacion;
	}

	/**
	 * @param causaAnulacion the causaAnulacion to set
	 */
	public void setCausaAnulacion(String causaAnulacion) {
		this.causaAnulacion = causaAnulacion;
	}

	/**
	 * @return the descTipo
	 */
	public String getDescTipo() {
		return descTipo;
	}

	/**
	 * @param descTipo the descTipo to set
	 */
	public void setDescTipo(String descTipo) {
		this.descTipo = descTipo;
	}

	/**
	 * @return the pmaDev
	 */
	public Double getPmaDev() {
		return pmaDev;
	}

	/**
	 * @param pmaDev the pmaDev to set
	 */
	public void setPmaDev(Double pmaDev) {
		this.pmaDev = pmaDev;
	}

}