package mx.com.santander.aseguradora.colectivos.view.dto;

import java.util.Date;

public class EndosoDatos3DTO extends EndosoDatos4DTO implements java.io.Serializable{

	//Implements Serializable
	private static final long serialVersionUID = 8122390922741008040L;

	//Declara propiedades Date
		private Date cedaFeSolicitud;
		private Date cedaFeRecepcion;
		private Date cedaFeAplica;
		private Date cedaFechaNvo;
		private Date cedaFeSusNvo;
		private Date cedaFeDesNvo;
		private Date cedaFeHasNvo;
		private Date cedaFechaAnt;
		private Date cedaFeSusAnt;
		private Date cedaFeDesAnt;
		private Date cedaFeHasAnt;
		
		
		/**
		 * Contructor de la clase
		 */
		public EndosoDatos3DTO() {
			//Inicializa fecha aplica
			cedaFeAplica = new Date();

			//Inicializa fecha anterior
			cedaFechaAnt = new Date();
			
			//Inicializa fecha nueva
			cedaFechaNvo = new Date();
			
			//Inicializa fecha desde anterior
			cedaFeDesAnt = new Date();
			
			//Inicializa fecha desde nueva
			cedaFeDesNvo = new Date();
			
			//Inicializa fecha hasta anterior
			cedaFeHasAnt = new Date();
			
			//Inicializa fecha hasta nueva
			cedaFeHasNvo = new Date();
			
			//Inicializa fecha recepcion
			cedaFeRecepcion = new Date();
			
			//Inicializa fecha solicitud
			cedaFeSolicitud = new Date();
			
			//Inicializa fecha suscripcion anterior
			cedaFeSusAnt = new Date();
			
			//Inicializa fecha suscripcion nueva
			cedaFeSusNvo = new Date();
		}
		/**
		 * @return the cedaFeSolicitud
		 */
		public Date getCedaFeSolicitud() {
			return new Date(cedaFeSolicitud.getTime());
		}
		/**
		 * @param cedaFeSolicitud the cedaFeSolicitud to set
		 */
		public void setCedaFeSolicitud(Date cedaFeSolicitud) {
			if(cedaFeSolicitud != null) {
				//fecha cedaFeSolicitud no es nula
				this.cedaFeSolicitud = new Date(cedaFeSolicitud.getTime());
			}
		}
		/**
		 * @return the cedaFeRecepcion
		 */
		public Date getCedaFeRecepcion() {
			return new Date(cedaFeRecepcion.getTime());
		}
		/**
		 * @param cedaFeRecepcion the cedaFeRecepcion to set
		 */
		public void setCedaFeRecepcion(Date cedaFeRecepcion) {
			if(cedaFeRecepcion != null) {
				//fecha cedaFeRecepcion no es nula
				this.cedaFeRecepcion = new Date(cedaFeRecepcion.getTime());
			}
		}
		/**
		 * @return the cedaFeAplica
		 */
		public Date getCedaFeAplica() {
			return new Date(cedaFeAplica.getTime());
		}
		/**
		 * @param cedaFeAplica the cedaFeAplica to set
		 */
		public void setCedaFeAplica(Date cedaFeAplica) {
			if(cedaFeAplica != null) {
				//fecha cedaFeAplica no es nula
				this.cedaFeAplica = new Date(cedaFeAplica.getTime());
			}
		}
		/**
		 * @return the cedaFechaNvo
		 */
		public Date getCedaFechaNvo() {
			return new Date(cedaFechaNvo.getTime());
		}
		/**
		 * @param cedaFechaNvo the cedaFechaNvo to set
		 */
		public void setCedaFechaNvo(Date cedaFechaNvo) {
			if(cedaFechaNvo != null) {
				//fecha cedaFechaNvo no es nula
				this.cedaFechaNvo = new Date(cedaFechaNvo.getTime());
			}
		}
		/**
		 * @return the cedaFeSusNvo
		 */
		public Date getCedaFeSusNvo() {
			return new Date(cedaFeSusNvo.getTime());
		}
		/**
		 * @param cedaFeSusNvo the cedaFeSusNvo to set
		 */
		public void setCedaFeSusNvo(Date cedaFeSusNvo) {
			if(cedaFeSusNvo != null) {
				//fecha cedaFeSusNvo no es nula
				this.cedaFeSusNvo = new Date(cedaFeSusNvo.getTime());
			}
		}
		/**
		 * @return the cedaFeDesNvo
		 */
		public Date getCedaFeDesNvo() {
			return new Date(cedaFeDesNvo.getTime());
		}
		/**
		 * @param cedaFeDesNvo the cedaFeDesNvo to set
		 */
		public void setCedaFeDesNvo(Date cedaFeDesNvo) {
			if(cedaFeDesNvo != null) {
				//fecha cedaFeDesNvo no es nula
				this.cedaFeDesNvo = new Date(cedaFeDesNvo.getTime());
			}
		}
		/**
		 * @return the cedaFeHasNvo
		 */
		public Date getCedaFeHasNvo() {
			return new Date(cedaFeHasNvo.getTime());
		}
		/**
		 * @param cedaFeHasNvo the cedaFeHasNvo to set
		 */
		public void setCedaFeHasNvo(Date cedaFeHasNvo) {
			if(cedaFeHasNvo != null) {
				//fecha cedaFeHasNvo no es nula
				this.cedaFeHasNvo = new Date(cedaFeHasNvo.getTime());
			}
		}
		/**
		 * @return the cedaFechaAnt
		 */
		public Date getCedaFechaAnt() {
			return new Date(cedaFechaAnt.getTime());
		}
		/**
		 * @param cedaFechaAnt the cedaFechaAnt to set
		 */
		public void setCedaFechaAnt(Date cedaFechaAnt) {
			if(cedaFechaAnt != null) {
				//fecha cedaFechaAnt no es nula
				this.cedaFechaAnt = new Date(cedaFechaAnt.getTime());
			}
		}
		/**
		 * @return the cedaFeSusAnt
		 */
		public Date getCedaFeSusAnt() {
			return new Date(cedaFeSusAnt.getTime());
		}
		/**
		 * @param cedaFeSusAnt the cedaFeSusAnt to set
		 */
		public void setCedaFeSusAnt(Date cedaFeSusAnt) {
			if(cedaFeSusAnt != null) {
				//fecha cedaFeSusAnt no es nula
				this.cedaFeSusAnt = new Date(cedaFeSusAnt.getTime());
			}
		}
		/**
		 * @return the cedaFeDesAnt
		 */
		public Date getCedaFeDesAnt() {
			return new Date(cedaFeDesAnt.getTime());
		}
		/**
		 * @param cedaFeDesAnt the cedaFeDesAnt to set
		 */
		public void setCedaFeDesAnt(Date cedaFeDesAnt) {
			if(cedaFeDesAnt != null) {
				//fecha cedaFeDesAnt no es nula
				this.cedaFeDesAnt = new Date(cedaFeDesAnt.getTime());
			}
		}
		/**
		 * @return the cedaFeHasAnt
		 */
		public Date getCedaFeHasAnt() {
			return new Date(cedaFeHasAnt.getTime());
		}
		/**
		 * @param cedaFeHasAnt the cedaFeHasAnt to set
		 */
		public void setCedaFeHasAnt(Date cedaFeHasAnt) {
			if(cedaFeHasAnt != null) {
				//fecha cedaFeHasAnt no es nula
				this.cedaFeHasAnt = new Date(cedaFeHasAnt.getTime());
			}
		}

		
		
}
