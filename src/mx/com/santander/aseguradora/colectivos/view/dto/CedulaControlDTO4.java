package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCedula;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CEDULA.
 * 				La ocupa la clase CedulaControlDTO3
 * 				Extiende de la clase CedulaControlDTO5
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CedulaControlDTO4 extends CedulaControlDTO5 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo coccNuRegNook
	private BigDecimal coccNuRegNook;
	//Propiedad para campo coccMtPrimaNook
	private BigDecimal coccMtPrimaNook;
	//Propiedad para campo coccNuRegSinpnd
	private BigDecimal coccNuRegSinpnd;
	//Propiedad para campo coccNuRegSinpndOk
	private BigDecimal coccNuRegSinpndOk;
	
	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cedula objeto con datos
	 */
	public CedulaControlDTO4(ColectivosCedula cedula) {
		super(cedula);
		this.coccNuRegNook = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccNuRegNook(), Constantes.TIPO_DATO_BIGDECIMAL);
		this.coccMtPrimaNook = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccMtPrimaNook(), Constantes.TIPO_DATO_BIGDECIMAL);
		this.coccNuRegSinpnd = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccNuRegSinpnd(), Constantes.TIPO_DATO_BIGDECIMAL);
		this.coccNuRegSinpndOk = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccNuRegSinpndOk(), Constantes.TIPO_DATO_BIGDECIMAL);
	}
	/**
	 * Constructor de clase
	 */
	public CedulaControlDTO4() {
		super(); 
	}
	
	/**
	 * @return the coccNuRegNook
	 */
	public BigDecimal getCoccNuRegNook() {
		return coccNuRegNook;
	}
	/**
	 * @param coccNuRegNook the coccNuRegNook to set
	 */
	public void setCoccNuRegNook(BigDecimal coccNuRegNook) {
		this.coccNuRegNook = coccNuRegNook;
	}
	/**
	 * @return the coccMtPrimaNook
	 */
	public BigDecimal getCoccMtPrimaNook() {
		return coccMtPrimaNook;
	}
	/**
	 * @param coccMtPrimaNook the coccMtPrimaNook to set
	 */
	public void setCoccMtPrimaNook(BigDecimal coccMtPrimaNook) {
		this.coccMtPrimaNook = coccMtPrimaNook;
	}
	/**
	 * @return the coccNuRegSinpnd
	 */
	public BigDecimal getCoccNuRegSinpnd() {
		return coccNuRegSinpnd;
	}
	/**
	 * @param coccNuRegSinpnd the coccNuRegSinpnd to set
	 */
	public void setCoccNuRegSinpnd(BigDecimal coccNuRegSinpnd) {
		this.coccNuRegSinpnd = coccNuRegSinpnd;
	}
	/**
	 * @return the coccNuRegSinpndOk
	 */
	public BigDecimal getCoccNuRegSinpndOk() {
		return coccNuRegSinpndOk;
	}
	/**
	 * @param coccNuRegSinpndOk the coccNuRegSinpndOk to set
	 */
	public void setCoccNuRegSinpndOk(BigDecimal coccNuRegSinpndOk) {
		this.coccNuRegSinpndOk = coccNuRegSinpndOk;
	}
}