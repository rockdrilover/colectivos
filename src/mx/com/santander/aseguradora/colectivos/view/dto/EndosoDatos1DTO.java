package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;

public class EndosoDatos1DTO extends EndosoDatos2DTO implements Serializable {
	
	
	//Implements Serializable
	private static final long serialVersionUID = 413863490297554030L;
		//Declara propiedades short
		private short cedaStEndoso;
		private short cedatpEndoso;
		private Short cedaPjParticipaNvo;
		private Short cedaPjParticipaAnt;
		
		/**
		 * @return the cedaStEndoso
		 */
		public short getCedaStEndoso() {
			return cedaStEndoso;
		}

		/**
		 * @param cedaStEndoso the cedaStEndoso to set
		 */
		public void setCedaStEndoso(short cedaStEndoso) {
			this.cedaStEndoso = cedaStEndoso;
		}

		/**
		 * @return the cedatpEndoso
		 */
		public short getCedatpEndoso() {
			return cedatpEndoso;
		}

		/**
		 * @param cedatpEndoso the cedatpEndoso to set
		 */
		public void setCedatpEndoso(short cedatpEndoso) {
			this.cedatpEndoso = cedatpEndoso;
		}
		
		/**
		 * @return the cedaPjParticipaNvo
		 */
		public Short getCedaPjParticipaNvo() {
			return cedaPjParticipaNvo;
		}

		/**
		 * @param cedaPjParticipaNvo the cedaPjParticipaNvo to set
		 */
		public void setCedaPjParticipaNvo(Short cedaPjParticipaNvo) {
			this.cedaPjParticipaNvo = cedaPjParticipaNvo;
		}
		
		
		/**
		 * @return the cedaPjParticipaAnt
		 */
		public Short getCedaPjParticipaAnt() {
			return cedaPjParticipaAnt;
		}

		/**
		 * @param cedaPjParticipaAnt the cedaPjParticipaAnt to set
		 */
		public void setCedaPjParticipaAnt(Short cedaPjParticipaAnt) {
			this.cedaPjParticipaAnt = cedaPjParticipaAnt;
		}
}
