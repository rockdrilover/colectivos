package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.bo.CifrasControl;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CIFRAS_CONTROL.
 * 				La ocupa la clase CifrasControlDTO2
 * 				Extiende de la clase CifrasControlDTO4
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CifrasControlDTO3 extends CifrasControlDTO4 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo cociNuEmitidos
	private BigDecimal cociNuEmitidos;
	//Propiedad para campo cociNuRechazados
	private BigDecimal cociNuRechazados;
	//Propiedad para campo cociNuTotal
	private BigDecimal cociNuTotal;
	//Propiedad para campo cociNvalor1
	private BigDecimal cociNvalor1;
	
	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cifras objeto con datos
	 */
	public CifrasControlDTO3(CifrasControl cifras) {
		super(cifras);
		this.cociNuEmitidos= cifras.getCc2().getCociNuEmitidos();
		this.cociNuRechazados= cifras.getCc2().getCociNuRechazados();
		this.cociNuTotal= cifras.getCc2().getCociNuTotal();
		this.cociNvalor1= cifras.getCc2().getCociNvalor1();
	}

	/**
	 * Constructor de clase
	 */
	public CifrasControlDTO3() {
		super();
	}

	/**
	 * @return the cociNuEmitidos
	 */
	public BigDecimal getCociNuEmitidos() {
		return cociNuEmitidos;
	}

	/**
	 * @param cociNuEmitidos the cociNuEmitidos to set
	 */
	public void setCociNuEmitidos(BigDecimal cociNuEmitidos) {
		this.cociNuEmitidos = cociNuEmitidos;
	}

	/**
	 * @return the cociNuRechazados
	 */
	public BigDecimal getCociNuRechazados() {
		return cociNuRechazados;
	}

	/**
	 * @param cociNuRechazados the cociNuRechazados to set
	 */
	public void setCociNuRechazados(BigDecimal cociNuRechazados) {
		this.cociNuRechazados = cociNuRechazados;
	}

	/**
	 * @return the cociNuTotal
	 */
	public BigDecimal getCociNuTotal() {
		return cociNuTotal;
	}

	/**
	 * @param cociNuTotal the cociNuTotal to set
	 */
	public void setCociNuTotal(BigDecimal cociNuTotal) {
		this.cociNuTotal = cociNuTotal;
	}

	/**
	 * @return the cociNvalor1
	 */
	public BigDecimal getCociNvalor1() {
		return cociNvalor1;
	}

	/**
	 * @param cociNvalor1 the cociNvalor1 to set
	 */
	public void setCociNvalor1(BigDecimal cociNvalor1) {
		this.cociNvalor1 = cociNvalor1;
	}

}