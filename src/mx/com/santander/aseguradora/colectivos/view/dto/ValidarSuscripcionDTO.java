package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;

import com.csvreader.CsvReader;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.dto.SuscripcionDTO;

public class ValidarSuscripcionDTO {

	public static SuscripcionDTO isSuscripcionDTO(CsvReader csv) {
		SuscripcionDTO suscripcionDTO = new SuscripcionDTO();
		String tmp;
		try {
			/**
			 * Folio Riesgos
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FOLIO_RIESGO))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FOLIO_RIESGO_VACIO);
				return suscripcionDTO;
			}

			try {
				suscripcionDTO
						.setCosuFolioRiesgos(new BigDecimal(csv.get(Constantes.FOLIO_RIESGO)));
			} catch (NumberFormatException e) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FOLIO_RIESGO_NUMERICO);
				return suscripcionDTO;
			}

			/**
			 * Fin Folio Riesgos
			 */

			/**
			 * Folio CH
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FOLIO_CH))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FOLIO_CH_VACIO);
				return suscripcionDTO;
			}

			try {
				suscripcionDTO.setCosuFolioCh(new BigDecimal(csv.get(Constantes.FOLIO_CH)));
			} catch (NumberFormatException e) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FOLIO_CH_NUMERICO);
				return suscripcionDTO;
			}

			/**
			 * Fin Folio CH
			 */

			/**
			 * ID CONCATENAR
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.ID_CONCATENAR))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.ID_CONCATENAR_VACIO);
				return suscripcionDTO;
			}

			suscripcionDTO.setCosuIdConcatenar(csv.get(Constantes.ID_CONCATENAR));

			/**
			 * Fin ID CONCATENAR
			 */

			/**
			 * NOMBRE
			 */
			if (Constantes.CADENA_VACIA.equals(csv.get(Constantes.NOMBRE))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.NOMBRE_VACIO);
				return suscripcionDTO;
			}

			suscripcionDTO.setCosuNombre(csv.get(Constantes.NOMBRE));

			/**
			 * Fin NOMBRE
			 */

			/**
			 * PARTICIPACION
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.PARTICIPACION))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.PARTICIPACION_VACIO);
				return suscripcionDTO;
			}

			suscripcionDTO.setCosuParticipacion(csv.get(Constantes.PARTICIPACION).toUpperCase());

			/**
			 * Fin PARTICIPACION
			 */

			/**
			 * FECHA_RECIBIDO_ASEGURADORA
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_RECIBIDO_ASEGURADORA))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FECHA_RECIBIDO_ASEGURADORA_VACIO);
				return suscripcionDTO;
			}

			try {
				suscripcionDTO.setCosuFechaRecAseg(
						GestorFechas.generateDate2(csv.get(Constantes.FECHA_RECIBIDO_ASEGURADORA),
								Constantes.FORMATO_FECHA_SUSCRIPCION));

			} catch (ParseException e) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FECHA_RECIBIDO_ASEGURADORA_FORMATO);
				return suscripcionDTO;
			}

			/**
			 * Fin FECHA_RECIBIDO_ASEGURADORA
			 */

			/**
			 * PLAZA
			 */
			if (Constantes.CADENA_VACIA.equals(csv.get(Constantes.PLAZA))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.PLAZA_VACIO);
				return suscripcionDTO;
			}

			suscripcionDTO.setCosuPlaza(csv.get(Constantes.PLAZA));

			/**
			 * PLAZA
			 */

			/**
			 * EJECUTIVO
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.EJECUTIVO))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.EJECUTIVO_VACIO);
				return suscripcionDTO;
			}

			suscripcionDTO.setCosuSucEjec(csv.get(Constantes.EJECUTIVO));

			/**
			 * EJECUTIVO
			 */

			/**
			 * FECHA_NACIMIENTO
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_NACIMIENTO))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FECHA_NACIMIENTO_VACIO);
				return suscripcionDTO;
			}

			try {
				suscripcionDTO.setCosuFechaNac(
						GestorFechas.generateDate2(csv.get(Constantes.FECHA_NACIMIENTO),
								Constantes.FORMATO_FECHA_SUSCRIPCION));

			} catch (ParseException e) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FECHA_NACIMIENTO_FORMATO);
				return suscripcionDTO;
			}

			/**
			 * Fin FECHA_NACIMIENTO
			 */
			
			/**
			 * Mont0
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.MONTO))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.MONTO_VACIO);
				return suscripcionDTO;
			}

			try {
				suscripcionDTO
						.setCosuMonto(new BigDecimal(csv.get(Constantes.MONTO)));
			} catch (NumberFormatException e) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.MONTO_NUMERICO);
				return suscripcionDTO;
			}

			/**
			 * Fin Mont0
			 */

			
			/**
			 * FECHA_SOLICITUD
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_SOLICITUD))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FECHA_SOLICITUD_VACIO);
				return suscripcionDTO;
			}

			try {
				suscripcionDTO.setCosuFechaSolCred(
						GestorFechas.generateDate2(csv.get(Constantes.FECHA_NACIMIENTO),
								Constantes.FORMATO_FECHA_SUSCRIPCION));

			} catch (ParseException e) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.FECHA_SOLICITUD_FORMATO);
				return suscripcionDTO;
			}

			/**
			 * Fin FECHA_SOLICITUD
			 */
			
			
			
			/**
			 * LOCAL
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.LOCAL))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.LOCAL_VACIO);
				return suscripcionDTO;
			}		
			
			tmp = csv.get(Constantes.LOCAL).substring(0, 1);
			
			if ("L".equals(tmp) || "F".equals(tmp)) {
				suscripcionDTO.setCosuLocal(tmp);
			} else {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.LOCAL_VALOR_INVALIDO);
				return suscripcionDTO;
			}
			
			suscripcionDTO.setCosuLocal(tmp);
					
			/**
			 * LOCAL
			 */
			
			/**
			 * EXAMEN_MEDICO
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.EXAMEN_MEDICO))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.EXAMEN_MEDICO_VACIO);
				return suscripcionDTO;
			}

			suscripcionDTO.setCosuExamMedico(csv.get(Constantes.EXAMEN_MEDICO));

			/**
			 * EXAMEN_MEDICO
			 */
			
			/**
			 * FECHA_SOLICITUD
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_SOLICITUD_EXAMEN))) {
				
				try {
					suscripcionDTO.setCosuFechaSolExam(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_SOLICITUD_EXAMEN),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_SOLICITUD_EXAMEN_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_SOLICITUD
			 */
			
			/**
			 * FECHA_CITA_EXAMEN
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_CITA_EXAMEN))) {
				
				try {
					suscripcionDTO.setCosuFechaCitExam(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_CITA_EXAMEN),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_CITA_EXAMEN_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_CITA_EXAMEN
			 */
			
			/**
			 * MOTIVO_EXAMEN_MEDICO
			 */
			suscripcionDTO.setCosuMotExamMed(csv.get(Constantes.MOTIVO_EXAMEN_MEDICO));
			
			/**
			 * MOTIVO_EXAMEN_MEDICO
			 */
			
			/**
			 * FECHA_APLICACION_EXAMEN
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_APLICACION_EXAMEN))) {
				
				try {
					suscripcionDTO.setCosuFechaAplExam(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_APLICACION_EXAMEN),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_APLICACION_EXAMEN_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_APLICACION_EXAMEN
			 */
			
			/**
			 * FECHA_APLICACION_EXAMEN
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_RESULTADOS_EXAMEN))) {
				
				try {
					suscripcionDTO.setCosuFechaResult(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_RESULTADOS_EXAMEN),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_RESULTADOS_EXAMEN_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_APLICACION_EXAMEN
			 */
			
			
			/**
			 * FECHA_RESPUESTA_DOCTOR
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_RESPUESTA_DOCTOR))) {
				
				try {
					suscripcionDTO.setCosuFechaResDoc(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_RESPUESTA_DOCTOR),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_RESPUESTA_DOCTOR_EXAMEN_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_RESPUESTA_DOCTOR
			 */
			/**
			 * DICTAMEN
			 */
			if (csv.get(Constantes.DICTAMEN) != null && !Constantes.CADENA_VACIA.equals(csv.get(Constantes.DICTAMEN))) {
				suscripcionDTO.setCosuDictamen(csv.get(Constantes.DICTAMEN).substring(0, 1));
			} else {
				suscripcionDTO.setCosuDictamen(Constantes.DICTAMEN_PENDIENTE);
			}
			/**
			 * Fin DICTAMEN
			 */
			
			/**
			 * FECHA_RESPUESTA_SUCURSAL
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_RESPUESTA_SUCURSAL))) {
				
				try {
					suscripcionDTO.setCosuFechaRespSuc(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_RESPUESTA_SUCURSAL),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_RESPUESTA_SUCURSAL_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_RESPUESTA_SUCURSAL
			 */
			
			
			/**
			 * FECHA_NOTIFICACION_DICTAMEN
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_NOTIFICACION_DICTAMEN))) {
				
				try {
					suscripcionDTO.setCosuFechaNotDic(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_NOTIFICACION_DICTAMEN),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_NOTIFICACION_DICTAMEN_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_NOTIFICACION_DICTAMEN
			 */
			
			/**
			 * OBSERVACIONES_WF
			 */
			suscripcionDTO.setCosuObsWf(csv.get(Constantes.OBSERVACIONES_WF));
			
			/**
			 * OBSERVACIONES_WF
			 */
			
			
			/**
			 * TELEFONO1
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.TELEFONO1))) {
				
				if (csv.get(Constantes.TELEFONO1).length() > 20) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.TELEFONO1_FORMATO);
					return suscripcionDTO;
				}
				
				suscripcionDTO.setCosuTelefono1(csv.get(Constantes.TELEFONO1));
				
			}

			

			/**
			 * Fin TELEFONO1
			 */
			
			/**
			 * TELEFONO2
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.TELEFONO2))) {
				
				if (csv.get(Constantes.TELEFONO2).length() > 20) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.TELEFONO2_FORMATO);
					return suscripcionDTO;
				}
				suscripcionDTO.setCosuTelefono2(csv.get(Constantes.TELEFONO2));

			}

			

			/**
			 * Fin TELEFONO2
			 */
			
			
			/**
			 * CELULAR
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.CELULAR))) {
				
				if (csv.get(Constantes.CELULAR).length() > 20) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.CELULAR_FORMATO);
					return suscripcionDTO;
				}
				suscripcionDTO.setCosuCelular(csv.get(Constantes.CELULAR));

			}

			

			/**
			 * Fin CELULAR
			 */
			
			/**
			 * EMAIL
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.EMAIL))) {
				
				if (csv.get(Constantes.EMAIL).length() > 50) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.EMAIL_FORMATO);
					return suscripcionDTO;
				}
				suscripcionDTO.setCosuEmail(csv.get(Constantes.EMAIL));

			}

			

			/**
			 * Fin EMAIL
			 */
			
			/**
			 * OBSERVACIONES_WF
			 */
			suscripcionDTO.setCosuObservaciones(csv.get(Constantes.OBSERVACIONES));
			
			/**
			 * OBSERVACIONES_WF
			 */
			
			
			/**
			 * NUM_INTER
			 */
			if (Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.NUM_INTER))) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.NUM_INTER_VACIO);
				return suscripcionDTO;
			}

			try {
				suscripcionDTO.setCosuNumInter(new BigDecimal(csv.get(Constantes.NUM_INTER)));
			} catch (NumberFormatException e) {
				suscripcionDTO.setCorrecto(false);
				suscripcionDTO.setErrorValidacion(Constantes.NUM_INTER_NUMERICO);
				return suscripcionDTO;
			}

			/**
			 * Fin NUM_INTER
			 */
			
			/**
			 * FECHA_FIRMA
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.FECHA_FIRMA))) {
				
				try {
					suscripcionDTO.setCosuFechaFirm(
							GestorFechas.generateDate2(csv.get(Constantes.FECHA_FIRMA),
									Constantes.FORMATO_FECHA_SUSCRIPCION));

				} catch (ParseException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.FECHA_FIRMA_FORMATO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin FECHA_FIRMA
			 */
			
			
			/**
			 * MONTO_FIRMADO
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.MONTO_FIRMADO))) {
				try {
					suscripcionDTO.setCosuMontoFirmado(new BigDecimal(csv.get(Constantes.MONTO_FIRMADO)));
				} catch (NumberFormatException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.MONTO_FIRMADO_NUMERICO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin MONTO_FIRMADO
			 */
			
			
			/**
			 * NUM_CREDITO
			 */
			suscripcionDTO.setCosuNumCredito(csv.get(Constantes.NUM_CREDITO));
			
			/**
			 * NUM_CREDITO
			 */
			
			
			/**
			 * EXTRA_FIRMA
			 */
			if (!Constantes.CADENA_VACIA
					.equals(csv.get(Constantes.EXTRA_FIRMA))) {
				try {
					suscripcionDTO.setCosuExtraFirma(new BigDecimal(csv.get(Constantes.EXTRA_FIRMA)));
				} catch (NumberFormatException e) {
					suscripcionDTO.setCorrecto(false);
					suscripcionDTO.setErrorValidacion(Constantes.EXTRA_FIRMA_NUMERICO);
					return suscripcionDTO;
				}
			}

			

			/**
			 * Fin EXTRA_FIRMA
			 */
			
			suscripcionDTO.setCorrecto(true);
			suscripcionDTO.setErrorValidacion(Constantes.REGISTRO_OK);
			
		} catch (IOException e) {
			suscripcionDTO.setCorrecto(false);
			suscripcionDTO.setErrorValidacion(Constantes.ERORR_LECTURA_SUSCRIPCION);
		}

		
		
		
		return suscripcionDTO;
	}
}
