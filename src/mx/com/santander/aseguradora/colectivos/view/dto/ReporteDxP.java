package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

//======================================================================================================================
public class ReporteDxP extends ReporteDxpParams{
	
	
	private BigDecimal bdecimalDxPNEPT;
	private BigDecimal bdecimalDxPEM;
	
	private ArrayList<DetalleReporteDxP> arrDetalleDXP;
	
	// -----------------------------------------------------------------------------------------------------------------
	/* Getters and Setters */
	// -----------------------------------------------------------------------------------------------------------------
	public BigDecimal getBdecimalDxPNEPT() {
		return bdecimalDxPNEPT;
	}

	public BigDecimal getBdecimalDxPEM() {
		return bdecimalDxPEM;
	}
	
	public ArrayList<DetalleReporteDxP> getarrDetalleDXP() {
		if (
				this.arrDetalleDXP != null
		) {
			return new ArrayList<DetalleReporteDxP>(arrDetalleDXP);
		}
		return null;
	}

	public void setarrDetalleDXP(ArrayList<DetalleReporteDxP> arrDetalleDXP) {
		if (
				arrDetalleDXP != null
		) {
			this.arrDetalleDXP = new ArrayList<DetalleReporteDxP>(arrDetalleDXP);
		} else {
		this.arrDetalleDXP = null;
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	/* Constructores */
	// -----------------------------------------------------------------------------------------------------------------

	
	public ReporteDxP(Object[] arrDatos) throws Excepciones {
		
		if (
				arrDatos.length != 13
		) {
			throw new Excepciones("No es posible generar el ReporteDxP", 
					new Exception("La cantidad parametros para generar el objeto debe ser 12, se han rcibido: " + 
									arrDatos.length ));
		}
	
		this.bdecimalIdPoliza =  (BigDecimal) arrDatos[0];
		this.bdecimalEPNA = (BigDecimal) arrDatos [1];
		this.bdecimalFAPN = (BigDecimal) arrDatos[2];
		this.bdecimalDxPEPN = (BigDecimal) arrDatos [3];	
		this.bdecimalDxPNEPN = (BigDecimal) arrDatos[4];
		this.bdecimalDT = (BigDecimal) arrDatos [5];	
		this.bdecimalEPTA = (BigDecimal) arrDatos[6];
		this.bdecimalFAPT = (BigDecimal) arrDatos [7];	
		this.bdecimalMCPT = (BigDecimal) arrDatos[8];
		this.bdecimalDxPEPT = (BigDecimal) arrDatos [9];	
		this.bdecimalDxPNEPT = (BigDecimal) arrDatos[10];
		this.bdecimalDxPEM = (BigDecimal) arrDatos [11];
		this.arrDetalleDXP =  new ArrayList<>();
		
	}	
	// ----------------------------------------------------------------------------------------------------------------
	/*Metodos*/
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * @author TOWA (JJFM)
	 * para calcular valores que se toman de la lista del detalle DxP.
	 */
	public void subCalculateValues() {
		this.subCalculateEP();
		this.subCalculateEPNA();
		this.subCalculateMCPTTOTAL();
		this.subCalculateEPTA();
	}
	/**
	 * @author TOWA (JJFM)
	 * para calcular el valor de EP.
	 */
	private void subCalculateEP() {
		
		if (this.arrDetalleDXP == null || this.arrDetalleDXP.isEmpty()) {
			return;
		}
		
		this.arrDetalleDXP.get(0).setBdecimalEP(this.arrDetalleDXP.get(0).getBdecimalEPA());
		int intI = 1;
		while (intI < this.arrDetalleDXP.size()) {
			BigDecimal bdecimalEP = 
					this.arrDetalleDXP.get(intI-1).getBdecimalEP().add(this.arrDetalleDXP.get(intI).getBdecimalEPA());
			this.arrDetalleDXP.get(intI).setBdecimalEP(bdecimalEP);
			intI = intI + 1;
		}
		
		
		
	}
	/**
	 * @author TOWA (JJFM)
	 * para calcular el valor de EPNA.
	 */
	private void subCalculateEPNA() {
		
		if (this.arrDetalleDXP == null || this.arrDetalleDXP.isEmpty()) {
			return;
		}
		
		this.bdecimalEPNA = this.arrDetalleDXP.get(this.arrDetalleDXP.size()-1).getBdecimalEP();
		
	}
	/**
	 * @author TOWA (JJFM)
	 * para calcular el valor de MCPT_TOTAL.
	 */
	private void subCalculateMCPTTOTAL() {
		
		if (this.arrDetalleDXP == null || this.arrDetalleDXP.isEmpty()) {
			return;
		}
		
		for (DetalleReporteDxP detalleReporteDxP : arrDetalleDXP) {
			this.bdecimalMCPT = bdecimalMCPT.add(detalleReporteDxP.getBdecimalMCPT());
		}
		
	}
	/**
	 * @author TOWA (JJFM)
	 * para calcular el valor de EPTA.
	 */
	
	private void subCalculateEPTA() {
	
	if (this.arrDetalleDXP == null || this.arrDetalleDXP.isEmpty()) {
		return;
	}
	
	for (DetalleReporteDxP detalleReporteDxP : arrDetalleDXP) {
		this.bdecimalEPTA = bdecimalEPTA.add(detalleReporteDxP.getBdecimalEPATOTAL());
	}
	
	}
}
//======================================================================================================================
