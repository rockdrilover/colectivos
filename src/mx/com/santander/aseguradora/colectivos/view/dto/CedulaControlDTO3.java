package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCedula;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CEDULA.
 * 				La ocupa la clase CedulaControlDTO2
 * 				Extiende de la clase CedulaControlDTO4
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CedulaControlDTO3 extends CedulaControlDTO4 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccMtPrima
	private BigDecimal coccMtPrima;
	//Propiedad para campo coccFeComer
	private Date coccFeComer;
	//Propiedad para campo coccNuRegOk
	private BigDecimal coccNuRegOk;
	//Propiedad para campo coccMtPrimaOk
	private BigDecimal coccMtPrimaOk;
	
	
	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cedula objeto con datos
	 */
	public CedulaControlDTO3(ColectivosCedula cedula) {
		super(cedula);
		this.coccMtPrima = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccMtPrima(), Constantes.TIPO_DATO_BIGDECIMAL);
		this.coccFeComer = cedula.getCc2().getCoccFeComer();
		this.coccNuRegOk = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccNuRegOk(), Constantes.TIPO_DATO_BIGDECIMAL);
		this.coccMtPrimaOk = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccMtPrimaOk(), Constantes.TIPO_DATO_BIGDECIMAL);
	}
	/**
	 * Constructor de clase
	 */
	public CedulaControlDTO3() {
		super(); 
	}
	
	/**
	 * @return the coccMtPrima
	 */
	public BigDecimal getCoccMtPrima() {
		return coccMtPrima;
	}
	/**
	 * @param coccMtPrima the coccMtPrima to set
	 */
	public void setCoccMtPrima(BigDecimal coccMtPrima) {
		this.coccMtPrima = coccMtPrima;
	}
	/**
	 * @return the coccFeComer
	 */
	public Date getCoccFeComer() {
		return new Date(coccFeComer.getTime());
	}
	/**
	 * @param coccFeComer the coccFeComer to set
	 */
	public void setCoccFeComer(Date coccFeComer) {
		if(coccFeComer != null) {
			this.coccFeComer = new Date(coccFeComer.getTime());
		}
	}
	/**
	 * @return the coccNuRegOk
	 */
	public BigDecimal getCoccNuRegOk() {
		return coccNuRegOk;
	}
	/**
	 * @param coccNuRegOk the coccNuRegOk to set
	 */
	public void setCoccNuRegOk(BigDecimal coccNuRegOk) {
		this.coccNuRegOk = coccNuRegOk;
	}
	/**
	 * @return the coccMtPrimaOk
	 */
	public BigDecimal getCoccMtPrimaOk() {
		return coccMtPrimaOk;
	}
	/**
	 * @param coccMtPrimaOk the coccMtPrimaOk to set
	 */
	public void setCoccMtPrimaOk(BigDecimal coccMtPrimaOk) {
		this.coccMtPrimaOk = coccMtPrimaOk;
	}
	
}