package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para trabajar desde la vista los datos de Comisiones.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class ComisionesDTO extends ComisionesDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo comTotal
	private BigDecimal comTotal;
	//Propiedad para campo comTecnica
	private BigDecimal comTecnica;

	/**
	 * Constructor de clase 
	 */
	public ComisionesDTO() {
		super();
		
		this.comTotal = new BigDecimal(0);
		this.comTecnica = new BigDecimal(0);
	}

	/**
	 * Contructor para inicializar comisiones
	 * @param tecnica comision tecnica
	 * @param contingente comision contingente
	 * @param comercial comision comercializacion
	 * @param repartos comision reparto
	 */
	public ComisionesDTO(String tecnica, String contingente, String comercial, String repartos) {
		this.comTecnica = new BigDecimal(tecnica);
		this.comContingente = new BigDecimal(contingente);
		this.comComercial = new BigDecimal(comercial);
		this.comRepartos = new BigDecimal(repartos);
	}

	/**
	 * Metodo to string.
	 * @return cadena de comisiones
	 */
	public String toString() {
		StringBuilder sbComisiones = new StringBuilder();
		
		sbComisiones.append(getComTecnica().toString()).append("|");
		sbComisiones.append(getComContingente().toString()).append("|");
		sbComisiones.append(getComComercial().toString()).append("|");
		sbComisiones.append(getComRepartos().toString()).append("|");
		sbComisiones.append(getComTotal().toString());
		
		return sbComisiones.toString(); 
	}
	
	/**
	 * Metodo que suma las comisiones
	 */
	public void getTotal() {
		comTotal = getComTecnica().add(getComContingente().add(getComComercial().add(getComRepartos()))); 
	}
	
	/**
	 * Metodo que setea comisiones que vienen en una cadena de texto
	 * @param comisiones cadena con comisiones
	 * @param nAcccion accion a extraer
	 */
	public void setComisiones(String comisiones, Integer nAcccion) {
		setComTecnica(new BigDecimal(Utilerias.getPorcentaje(comisiones, nAcccion, Constantes.POR_TECNICA)));
		setComContingente(new BigDecimal(Utilerias.getPorcentaje(comisiones, nAcccion, Constantes.POR_CONTINGENTE)));
		setComComercial(new BigDecimal(Utilerias.getPorcentaje(comisiones, nAcccion, Constantes.POR_COMERCIALIZACION)));
		setComRepartos(new BigDecimal(Utilerias.getPorcentaje(comisiones, nAcccion, Constantes.POR_REPARTOS)));
	}
	
	/**
	 * @return the comTecnica
	 */
	public BigDecimal getComTecnica() {
		return comTecnica;
	}
	/**
	 * @param comTecnica the comTecnica to set
	 */
	public void setComTecnica(BigDecimal comTecnica) {
		this.comTecnica = comTecnica;
	}
	/**
	 * @return the comTotal
	 */
	public BigDecimal getComTotal() {
		getTotal();
		return comTotal;
	}
	/**
	 * @param comTotal the comTotal to set
	 */
	public void setComTotal(BigDecimal comTotal) {
		this.comTotal = comTotal;
	}
}