package mx.com.santander.aseguradora.colectivos.view.dto;

public class EndosoDatos4DTO extends EndosoDatos5DTO implements java.io.Serializable{

	//Implements Serializable
	private static final long serialVersionUID = -6253353726784790677L;

	//Declara propiedades String
		private String cedaCdUsuarioReg;
		private String cedaCorreoReg;
		private String cedaCdUsuarioAplica;
		private String cedaCorreoAplica;
		private String cedaNombreNvo;
		private String cedaApNvo;
		private String cedaAmNvo;
		private String cedaRfcNvo;
		private String cedaDireccionNvo;
		private String cedasexoNvo;
		
		/**
		 * @return the cedaCdUsuarioReg
		 */
		public String getCedaCdUsuarioReg() {
			return cedaCdUsuarioReg;
		}

		/**
		 * @param cedaCdUsuarioReg the cedaCdUsuarioReg to set
		 */
		public void setCedaCdUsuarioReg(String cedaCdUsuarioReg) {
			this.cedaCdUsuarioReg = cedaCdUsuarioReg;
		}

		/**
		 * @return the cedaCorreoReg
		 */
		public String getCedaCorreoReg() {
			return cedaCorreoReg;
		}

		/**
		 * @param cedaCorreoReg the cedaCorreoReg to set
		 */
		public void setCedaCorreoReg(String cedaCorreoReg) {
			this.cedaCorreoReg = cedaCorreoReg;
		}

		/**
		 * @return the cedaCdUsuarioAplica
		 */
		public String getCedaCdUsuarioAplica() {
			return cedaCdUsuarioAplica;
		}

		/**
		 * @param cedaCdUsuarioAplica the cedaCdUsuarioAplica to set
		 */
		public void setCedaCdUsuarioAplica(String cedaCdUsuarioAplica) {
			this.cedaCdUsuarioAplica = cedaCdUsuarioAplica;
		}

		/**
		 * @return the cedaCorreoAplica
		 */
		public String getCedaCorreoAplica() {
			return cedaCorreoAplica;
		}

		/**
		 * @param cedaCorreoAplica the cedaCorreoAplica to set
		 */
		public void setCedaCorreoAplica(String cedaCorreoAplica) {
			this.cedaCorreoAplica = cedaCorreoAplica;
		}

		/**
		 * @return the cedaNombreNvo
		 */
		public String getCedaNombreNvo() {
			return cedaNombreNvo;
		}

		/**
		 * @param cedaNombreNvo the cedaNombreNvo to set
		 */
		public void setCedaNombreNvo(String cedaNombreNvo) {
			this.cedaNombreNvo = cedaNombreNvo;
		}

		/**
		 * @return the cedaApNvo
		 */
		public String getCedaApNvo() {
			return cedaApNvo;
		}

		/**
		 * @param cedaApNvo the cedaApNvo to set
		 */
		public void setCedaApNvo(String cedaApNvo) {
			this.cedaApNvo = cedaApNvo;
		}

		/**
		 * @return the cedaAmNvo
		 */
		public String getCedaAmNvo() {
			return cedaAmNvo;
		}

		/**
		 * @param cedaAmNvo the cedaAmNvo to set
		 */
		public void setCedaAmNvo(String cedaAmNvo) {
			this.cedaAmNvo = cedaAmNvo;
		}

		/**
		 * @return the cedaRfcNvo
		 */
		public String getCedaRfcNvo() {
			return cedaRfcNvo;
		}

		/**
		 * @param cedaRfcNvo the cedaRfcNvo to set
		 */
		public void setCedaRfcNvo(String cedaRfcNvo) {
			this.cedaRfcNvo = cedaRfcNvo;
		}

		/**
		 * @return the cedaDireccionNvo
		 */
		public String getCedaDireccionNvo() {
			return cedaDireccionNvo;
		}

		/**
		 * @param cedaDireccionNvo the cedaDireccionNvo to set
		 */
		public void setCedaDireccionNvo(String cedaDireccionNvo) {
			this.cedaDireccionNvo = cedaDireccionNvo;
		}

		/**
		 * @return the cedasexoNvo
		 */
		public String getCedasexoNvo() {
			return cedasexoNvo;
		}

		/**
		 * @param cedasexoNvo the cedasexoNvo to set
		 */
		public void setCedasexoNvo(String cedasexoNvo) {
			this.cedasexoNvo = cedasexoNvo;
		}
}
