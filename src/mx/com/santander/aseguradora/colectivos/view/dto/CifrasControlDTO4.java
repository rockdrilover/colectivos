package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.bo.CifrasControl;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CIFRAS_CONTROL.
 * 				La ocupa la clase CifrasControlDTO3				
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CifrasControlDTO4 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo cociNvalor2
	private BigDecimal cociNvalor2;
	//Propiedad para campo cociNvalor3
	@SuppressWarnings("unused")
	private BigDecimal cociNvalor3;
	//Propiedad para campo cociVvalor1
	private String cociVvalor1;
	//Propiedad para campo cociVvalor2
	private String cociVvalor2;
	//Propiedad para campo cociVvalor3
	private String cociVvalor3;
	
	
	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cifras objeto con datos
	 */
	public CifrasControlDTO4(CifrasControl cifras) {
		this.cociNvalor2= cifras.getCc2().getCociNvalor2();
		this.cociNvalor3= cifras.getCc2().getCociNvalor3();
		this.cociVvalor1= cifras.getCc2().getCociVvalor1();
		this.cociVvalor2= cifras.getCc2().getCociVvalor2();
	}

	/**
	 * Constructor de clase
	 */
	public CifrasControlDTO4() {
		// Dont Using
	}
	
	/**
	 * @return the cociVvalor1
	 */
	public String getCociVvalor1() {
		return cociVvalor1;
	}

	/**
	 * @param cociVvalor1 the cociVvalor1 to set
	 */
	public void setCociVvalor1(String cociVvalor1) {
		this.cociVvalor1 = cociVvalor1;
	}

	/**
	 * @return the cociVvalor2
	 */
	public String getCociVvalor2() {
		return cociVvalor2;
	}

	/**
	 * @param cociVvalor2 the cociVvalor2 to set
	 */
	public void setCociVvalor2(String cociVvalor2) {
		this.cociVvalor2 = cociVvalor2;
	}

	/**
	 * @return the cociVvalor3
	 */
	public String getCociVvalor3() {
		return cociVvalor3;
	}

	/**
	 * @param cociVvalor3 the cociVvalor3 to set
	 */
	public void setCociVvalor3(String cociVvalor3) {
		this.cociVvalor3 = cociVvalor3;
	}

	/**
	 * @return the cociNvalor2
	 */
	public BigDecimal getCociNvalor2() {
		return cociNvalor2;
	}

	/**
	 * @param cociNvalor2 the cociNvalor2 to set
	 */
	public void setCociNvalor2(BigDecimal cociNvalor2) {
		this.cociNvalor2 = cociNvalor2;
	}
	

}