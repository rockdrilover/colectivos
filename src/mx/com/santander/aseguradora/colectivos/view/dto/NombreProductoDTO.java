package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-12-2022
 * Description: Clase para trabajar desde la vista los datos para 
 * 				nombres de producto en proceso de Comisiones.
 * 				
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class NombreProductoDTO  implements java.io.Serializable{
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//valor para la lista
	private BigDecimal value;
	
	//Descripcion para la lista
	private String descripcion;
	
	/**
	 * Constructor de Clase
	 * @param arrDatos datos para inicializar propiedades
	 */
	public NombreProductoDTO (Object[] arrDatos) {
		value = (BigDecimal) arrDatos[0];
		descripcion = (String) arrDatos[1];
	}
	

	/**
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
