package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		09-03-2020
 * Description: Clase para trabajar desde la vista los datos que muestran 
 * 				el resumen de creditos a cancelar
 * 				La ocupa la clase ResumenCancelaDTO2
 * 				
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class ResumenCancelaDTO3 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo pmaCan
	private Double pmaCan;
	//Propiedad para campo archivo
	private String archivo;

	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param datos objeto con datos
	 */
	public ResumenCancelaDTO3(Object[] datos) {
		this.pmaCan = ((BigDecimal) datos[8]).doubleValue();
		this.archivo = datos[9].toString();
	}

	/**
	 * Constructor de clase
	 */
	public ResumenCancelaDTO3() {
		super();
	}
	
	/**
	 * @return the pmaCan
	 */
	public Double getPmaCan() {
		return pmaCan;
	}
	/**
	 * @param pmaCan the pmaCan to set
	 */
	public void setPmaCan(Double pmaCan) {
		this.pmaCan = pmaCan;
	}
	/**
	 * @return the archivo
	 */
	public String getArchivo() {
		return archivo;
	}
	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	
}