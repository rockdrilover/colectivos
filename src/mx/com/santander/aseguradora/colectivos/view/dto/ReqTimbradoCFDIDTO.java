package mx.com.santander.aseguradora.colectivos.view.dto;

/**
 * Peticion de servicio de timbrado
 * 
 * @author CJPV - vectormx
 * @version 1 - 07 -11 -2017
 *
 */
public class ReqTimbradoCFDIDTO {

	/**
	 * Usuario pistas de auditoria.
	 */
	private String usuario;

	/**
	 * Aplicativo pistas de auditoria.
	 */
	private String aplicativo;

	/**
	 * Ipdir pistas de auditoria.
	 */
	private String ipdir;
	
	/**
	 * Canal pistas de auditoria.
	 */
	private String canal;
	
	/**
	 * Ramo pistas de auditoria.
	 */
	private String ramo;
	
	/**
	 * poliza pistas de auditoria.
	 */
	private String poliza;
	
	/**
	 * Certificado pistas de auditoria.
	 */
	private String certificado;
	
	/**
	 * recibo pistas de auditoria.
	 */
	private String recibo;
	
	
	/**
	 * recibo pistas de colectivo.
	 */
	private boolean colectivo;


	public ReqTimbradoCFDIDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ReqTimbradoCFDIDTO(String usuario, String aplicativo, String ipdir, String canal, String ramo, String poliza,
			String certificado, String recibo, Boolean colectivo) {
		super();
		this.usuario = usuario;
		this.aplicativo = aplicativo;
		this.ipdir = ipdir;
		this.canal = canal;
		this.ramo = ramo;
		this.poliza = poliza;
		this.certificado = certificado;
		this.recibo = recibo;
		this.colectivo = colectivo;
	}


	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}


	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	/**
	 * @return the aplicativo
	 */
	public String getAplicativo() {
		return aplicativo;
	}


	/**
	 * @param aplicativo the aplicativo to set
	 */
	public void setAplicativo(String aplicativo) {
		this.aplicativo = aplicativo;
	}


	/**
	 * @return the ipdir
	 */
	public String getIpdir() {
		return ipdir;
	}


	/**
	 * @param ipdir the ipdir to set
	 */
	public void setIpdir(String ipdir) {
		this.ipdir = ipdir;
	}


	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}


	/**
	 * @param canal the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}


	/**
	 * @return the ramo
	 */
	public String getRamo() {
		return ramo;
	}


	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}


	/**
	 * @return the poliza
	 */
	public String getPoliza() {
		return poliza;
	}


	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}


	/**
	 * @return the certificado
	 */
	public String getCertificado() {
		return certificado;
	}


	/**
	 * @param certificado the certificado to set
	 */
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}


	/**
	 * @return the recibo
	 */
	public String getRecibo() {
		return recibo;
	}


	/**
	 * @param recibo the recibo to set
	 */
	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}


	/**
	 * @return the colectivo
	 */
	public boolean getColectivo() {
		return colectivo;
	}


	/**
	 * @param colectivo the colectivo to set
	 */
	public void setColectivo(boolean colectivo) {
		this.colectivo = colectivo;
	}
	
	
	

}
