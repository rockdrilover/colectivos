package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosBeneficiariosId;

public class BeneficiariosDTO implements java.io.Serializable {

	private static final long serialVersionUID = -5485573778670780497L;
	private ColectivosBeneficiariosId id;
	private EndosoDatosDTO objEndoso;
	private String cobeNombre;
	private String cobeApellidoPat;
	private String cobeApellidoMat;
	private String cobeRelacionBenef;
	private BigDecimal cobePoParticipacion;
	private Date cobeFeNacimiento;
	private String cobeCdSexo;
	private String cobeTpPersonaBenef;
	private String cobeCdParentAseg;
	private Date cobeFeDesde;
	private Date cobeFeHasta;
	private String cobeVcampo1;
	private String cobeVcampo2;
	private String cobeVcampo3;
	private String cobeVcampo4;
	private Long cobeNcampo1;
	private Long cobeNcampo2;
	private Long cobeNcampo3;
	private BigDecimal cobeNcampo4;
	private Date cobeDcampo1;
	private Date cobeDcampo2;
	private Date cobeDcampo3;
	private Date cobeDcampo4;

	public BeneficiariosDTO() {
		id = new ColectivosBeneficiariosId();
		objEndoso = new EndosoDatosDTO();
	}

	public BeneficiariosDTO(ColectivosBeneficiariosId id,
			String cobeNombre, String cobeApellidoPat, String cobeApellidoMat,
			String cobeRelacionBenef, BigDecimal cobePoParticipacion,
			Date cobeFeDesde) {
		this.id = id;
		this.cobeNombre = cobeNombre;
		this.cobeApellidoPat = cobeApellidoPat;
		this.cobeApellidoMat = cobeApellidoMat;
		this.cobeRelacionBenef = cobeRelacionBenef;
		this.cobePoParticipacion = cobePoParticipacion;
		this.cobeFeDesde = cobeFeDesde;
	}

	public BeneficiariosDTO(ColectivosBeneficiariosId id,
			String cobeNombre, String cobeApellidoPat, String cobeApellidoMat,
			String cobeRelacionBenef, BigDecimal cobePoParticipacion,
			Date cobeFeNacimiento, String cobeCdSexo,
			String cobeTpPersonaBenef, String cobeCdParentAseg,
			Date cobeFeDesde, Date cobeFeHasta, String cobeVcampo1,
			String cobeVcampo2, String cobeVcampo3, String cobeVcampo4,
			Long cobeNcampo1, Long cobeNcampo2, Long cobeNcampo3,
			BigDecimal cobeNcampo4, Date cobeDcampo1, Date cobeDcampo2,
			Date cobeDcampo3, Date cobeDcampo4) {
		this.id = id;
		this.cobeNombre = cobeNombre;
		this.cobeApellidoPat = cobeApellidoPat;
		this.cobeApellidoMat = cobeApellidoMat;
		this.cobeRelacionBenef = cobeRelacionBenef;
		this.cobePoParticipacion = cobePoParticipacion;
		this.cobeFeNacimiento = cobeFeNacimiento;
		this.cobeCdSexo = cobeCdSexo;
		this.cobeTpPersonaBenef = cobeTpPersonaBenef;
		this.cobeCdParentAseg = cobeCdParentAseg;
		this.cobeFeDesde = cobeFeDesde;
		this.cobeFeHasta = cobeFeHasta;
		this.cobeVcampo1 = cobeVcampo1;
		this.cobeVcampo2 = cobeVcampo2;
		this.cobeVcampo3 = cobeVcampo3;
		this.cobeVcampo4 = cobeVcampo4;
		this.cobeNcampo1 = cobeNcampo1;
		this.cobeNcampo2 = cobeNcampo2;
		this.cobeNcampo3 = cobeNcampo3;
		this.cobeNcampo4 = cobeNcampo4;
		this.cobeDcampo1 = cobeDcampo1;
		this.cobeDcampo2 = cobeDcampo2;
		this.cobeDcampo3 = cobeDcampo3;
		this.cobeDcampo4 = cobeDcampo4;
	}

	public ColectivosBeneficiariosId getId() {
		return this.id;
	}
	public void setId(ColectivosBeneficiariosId id) {
		this.id = id;
	}
	public String getCobeNombre() {
		return this.cobeNombre;
	}
	public void setCobeNombre(String cobeNombre) {
		this.cobeNombre = cobeNombre;
	}
	public String getCobeApellidoPat() {
		return this.cobeApellidoPat;
	}
	public void setCobeApellidoPat(String cobeApellidoPat) {
		this.cobeApellidoPat = cobeApellidoPat;
	}
	public String getCobeApellidoMat() {
		return this.cobeApellidoMat;
	}
	public void setCobeApellidoMat(String cobeApellidoMat) {
		this.cobeApellidoMat = cobeApellidoMat;
	}
	public String getCobeRelacionBenef() {
		return this.cobeRelacionBenef;
	}
	public void setCobeRelacionBenef(String cobeRelacionBenef) {
		this.cobeRelacionBenef = cobeRelacionBenef;
	}
	public BigDecimal getCobePoParticipacion() {
		return this.cobePoParticipacion;
	}
	public void setCobePoParticipacion(BigDecimal cobePoParticipacion) {
		this.cobePoParticipacion = cobePoParticipacion;
	}
	public Date getCobeFeNacimiento() {
		return this.cobeFeNacimiento;
	}
	public void setCobeFeNacimiento(Date cobeFeNacimiento) {
		this.cobeFeNacimiento = cobeFeNacimiento;
	}
	public String getCobeCdSexo() {
		return this.cobeCdSexo;
	}
	public void setCobeCdSexo(String cobeCdSexo) {
		this.cobeCdSexo = cobeCdSexo;
	}
	public String getCobeTpPersonaBenef() {
		return this.cobeTpPersonaBenef;
	}
	public void setCobeTpPersonaBenef(String cobeTpPersonaBenef) {
		this.cobeTpPersonaBenef = cobeTpPersonaBenef;
	}
	public String getCobeCdParentAseg() {
		return this.cobeCdParentAseg;
	}
	public void setCobeCdParentAseg(String cobeCdParentAseg) {
		this.cobeCdParentAseg = cobeCdParentAseg;
	}
	public Date getCobeFeDesde() {
		return this.cobeFeDesde;
	}
	public void setCobeFeDesde(Date cobeFeDesde) {
		this.cobeFeDesde = cobeFeDesde;
	}
	public Date getCobeFeHasta() {
		return this.cobeFeHasta;
	}
	public void setCobeFeHasta(Date cobeFeHasta) {
		this.cobeFeHasta = cobeFeHasta;
	}
	public String getCobeVcampo1() {
		return this.cobeVcampo1;
	}
	public void setCobeVcampo1(String cobeVcampo1) {
		this.cobeVcampo1 = cobeVcampo1;
	}
	public String getCobeVcampo2() {
		return this.cobeVcampo2;
	}
	public void setCobeVcampo2(String cobeVcampo2) {
		this.cobeVcampo2 = cobeVcampo2;
	}
	public String getCobeVcampo3() {
		return this.cobeVcampo3;
	}
	public void setCobeVcampo3(String cobeVcampo3) {
		this.cobeVcampo3 = cobeVcampo3;
	}
	public String getCobeVcampo4() {
		return this.cobeVcampo4;
	}
	public void setCobeVcampo4(String cobeVcampo4) {
		this.cobeVcampo4 = cobeVcampo4;
	}
	public Long getCobeNcampo1() {
		return this.cobeNcampo1;
	}
	public void setCobeNcampo1(Long cobeNcampo1) {
		this.cobeNcampo1 = cobeNcampo1;
	}
	public Long getCobeNcampo2() {
		return this.cobeNcampo2;
	}
	public void setCobeNcampo2(Long cobeNcampo2) {
		this.cobeNcampo2 = cobeNcampo2;
	}
	public Long getCobeNcampo3() {
		return this.cobeNcampo3;
	}
	public void setCobeNcampo3(Long cobeNcampo3) {
		this.cobeNcampo3 = cobeNcampo3;
	}
	public BigDecimal getCobeNcampo4() {
		return this.cobeNcampo4;
	}
	public void setCobeNcampo4(BigDecimal cobeNcampo4) {
		this.cobeNcampo4 = cobeNcampo4;
	}
	public Date getCobeDcampo1() {
		return this.cobeDcampo1;
	}
	public void setCobeDcampo1(Date cobeDcampo1) {
		this.cobeDcampo1 = cobeDcampo1;
	}
	public Date getCobeDcampo2() {
		return this.cobeDcampo2;
	}
	public void setCobeDcampo2(Date cobeDcampo2) {
		this.cobeDcampo2 = cobeDcampo2;
	}
	public Date getCobeDcampo3() {
		return this.cobeDcampo3;
	}
	public void setCobeDcampo3(Date cobeDcampo3) {
		this.cobeDcampo3 = cobeDcampo3;
	}
	public Date getCobeDcampo4() {
		return this.cobeDcampo4;
	}
	public void setCobeDcampo4(Date cobeDcampo4) {
		this.cobeDcampo4 = cobeDcampo4;
	}
	public EndosoDatosDTO getObjEndoso() {
		return objEndoso;
	}
	public void setObjEndoso(EndosoDatosDTO objEndoso) {
		this.objEndoso = objEndoso;
	}
}
