package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReporteDxpParams implements Serializable  {
	protected BigDecimal bdecimalIdPoliza;
	protected BigDecimal bdecimalEPNA;
	protected BigDecimal bdecimalFAPN;
	protected BigDecimal bdecimalDxPEPN;
	protected BigDecimal bdecimalDxPNEPN;
	protected BigDecimal bdecimalDT;
	protected BigDecimal bdecimalEPTA;
	protected BigDecimal bdecimalFAPT;
	protected BigDecimal bdecimalMCPT;
	protected BigDecimal bdecimalDxPEPT;
	
	// -----------------------------------------------------------------------------------------------------------------
	/* Getters and Setters */
	// -----------------------------------------------------------------------------------------------------------------
	public BigDecimal getBdecimalIdPoliza() {
		return bdecimalIdPoliza;
	}

	public BigDecimal getBdecimalEPNA() {
		return bdecimalEPNA;
	}

	public BigDecimal getBdecimalFAPN() {
		return bdecimalFAPN;
	}

	public BigDecimal getBdecimalDxPEPN() {
		return bdecimalDxPEPN;
	}

	public BigDecimal getBdecimalDxPNEPN() {
		return bdecimalDxPNEPN;
	}

	public BigDecimal getBdecimalDT() {
		return bdecimalDT;
	}

	public BigDecimal getBdecimalEPTA() {
		return bdecimalEPTA;
	}

	public BigDecimal getBdecimalFAPT() {
		return bdecimalFAPT;
	}

	public BigDecimal getBdecimalMCPT() {
		return bdecimalMCPT;
	}

	public BigDecimal getBdecimalDxPEPT() {
		return bdecimalDxPEPT;
	}

}
