package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId;

/**
 * Data Transfer Object (DTO) para tabla de ColectivosEndososDatos
 * Clase para envover datos en la capa de vista para la tabla ColectivosEndososDatos
 * @author Ing. Issac Buatista
 *
 */
public class EndosoDatosDTO extends EndosoDatos1DTO  implements java.io.Serializable {

	//Implements Serializable
	private static final long serialVersionUID = 6585309600917444604L;
	
	//Declara propiedad EndososDatosId
	private EndososDatosId id;	
	
	//Declara propiedades String
	private String cedaCampov1;
	private String cedaCampov2;
	private String cedaCampov3;
	private String cedaCampov4;
	private String cedaCampov5;
	
	//Declara propiedades Integer
	private Integer cedaCampon1;
	
	/**
	 * Constructor de clase
	 */
	public EndosoDatosDTO() {
		//Inicializa id
		id = new EndososDatosId();
	}
		
	
	/**
	 * @return the id
	 */
	public EndososDatosId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(EndososDatosId id) {
		this.id = id;
	}

	/**
	 * @return the cedaCampov1
	 */
	public String getCedaCampov1() {
		return cedaCampov1;
	}

	/**
	 * @param cedaCampov1 the cedaCampov1 to set
	 */
	public void setCedaCampov1(String cedaCampov1) {
		this.cedaCampov1 = cedaCampov1;
	}

	/**
	 * @return the cedaCampov2
	 */
	public String getCedaCampov2() {
		return cedaCampov2;
	}

	/**
	 * @param cedaCampov2 the cedaCampov2 to set
	 */
	public void setCedaCampov2(String cedaCampov2) {
		this.cedaCampov2 = cedaCampov2;
	}

	/**
	 * @return the cedaCampov3
	 */
	public String getCedaCampov3() {
		return cedaCampov3;
	}

	/**
	 * @param cedaCampov3 the cedaCampov3 to set
	 */
	public void setCedaCampov3(String cedaCampov3) {
		this.cedaCampov3 = cedaCampov3;
	}

	/**
	 * @return the cedaCampon1
	 */
	public Integer getCedaCampon1() {
		return cedaCampon1;
	}

	/**
	 * @param cedaCampon1 the cedaCampon1 to set
	 */
	public void setCedaCampon1(Integer cedaCampon1) {
		this.cedaCampon1 = cedaCampon1;
	}

	/**
	 * @return the cedaCampov4
	 */
	public String getCedaCampov4() {
		return cedaCampov4;
	}

	/**
	 * @param cedaCampov4 the cedaCampov4 to set
	 */
	public void setCedaCampov4(String cedaCampov4) {
		this.cedaCampov4 = cedaCampov4;
	}

	/**
	 * @return the cedaCampov5
	 */
	public String getCedaCampov5() {
		return cedaCampov5;
	}

	/**
	 * @param cedaCampov5 the cedaCampov5 to set
	 */
	public void setCedaCampov5(String cedaCampov5) {
		this.cedaCampov5 = cedaCampov5;
	}
	
}
