package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ClienteDTO implements java.io.Serializable {

	private static final long serialVersionUID = -1947589344396908359L;
	private long cocnNuCliente;
	private String cocnNombre;
	private String cocnApellidoPat;
	private String cocnApellidoMat;
	private String cocnCdSexo;
	private Date cocnFeNacimiento;
	private String cocnRfc;
	private String cocnCdEdoCivil;
	private String cocnBucCliente;
	private Short cocnTpCliente;
	private String cocnCdParentesco;
	private String cocnSegmentoCliente;
	private String cocnCdCobro;
	private Short cocnCdActividad;
	private String cocnCalleNum;
	private String cocnColonia;
	private String cocnDelegmunic;
	private Byte cocnCdEstado;
	private Short cocnCdCiudad;
	private String cocnCdPostal;
	private String cocnDsPoblac;
	private String cocnNuLada;
	private String cocnNuTelefono;
	private String cocnCorreoElect;
	private String cocnDiCobro1;
	private Long cocnCampon1;
	private Long cocnCampon2;
	private BigDecimal cocnCampon3;
	private BigDecimal cocnCampon4;
	private String cocnCampov1;
	private String cocnCampov2;
	private String cocnCampov3;
	private String cocnCampov4;
	private Date cocnCampof1;
	private Date cocnCampof2;
	private Date cocnCampof3;
	private Date cocnCampof4;

	public ClienteDTO() {
	}

	public ClienteDTO(long cocnNuCliente, String cocnNombre,
			String cocnApellidoPat, String cocnApellidoMat, String cocnRfc,
			String cocnBucCliente) {
		this.cocnNuCliente = cocnNuCliente;
		this.cocnNombre = cocnNombre;
		this.cocnApellidoPat = cocnApellidoPat;
		this.cocnApellidoMat = cocnApellidoMat;
		this.cocnRfc = cocnRfc;
		this.cocnBucCliente = cocnBucCliente;
	}

	public ClienteDTO(long cocnNuCliente, String cocnNombre,
			String cocnApellidoPat, String cocnApellidoMat, String cocnCdSexo,
			Date cocnFeNacimiento, String cocnRfc, String cocnCdEdoCivil,
			String cocnBucCliente, Short cocnTpCliente,
			String cocnCdParentesco, String cocnSegmentoCliente,
			String cocnCdCobro, Short cocnCdActividad, String cocnCalleNum,
			String cocnColonia, String cocnDelegmunic, Byte cocnCdEstado,
			Short cocnCdCiudad, String cocnCdPostal, String cocnDsPoblac,
			String cocnNuLada, String cocnNuTelefono, String cocnCorreoElect,
			String cocnDiCobro1, Long cocnCampon1, Long cocnCampon2,
			BigDecimal cocnCampon3, BigDecimal cocnCampon4, String cocnCampov1,
			String cocnCampov2, String cocnCampov3, String cocnCampov4,
			Date cocnCampof1, Date cocnCampof2, Date cocnCampof3,
			Date cocnCampof4) {
		this.cocnNuCliente = cocnNuCliente;
		this.cocnNombre = cocnNombre;
		this.cocnApellidoPat = cocnApellidoPat;
		this.cocnApellidoMat = cocnApellidoMat;
		this.cocnCdSexo = cocnCdSexo;
		this.cocnFeNacimiento = cocnFeNacimiento;
		this.cocnRfc = cocnRfc;
		this.cocnCdEdoCivil = cocnCdEdoCivil;
		this.cocnBucCliente = cocnBucCliente;
		this.cocnTpCliente = cocnTpCliente;
		this.cocnCdParentesco = cocnCdParentesco;
		this.cocnSegmentoCliente = cocnSegmentoCliente;
		this.cocnCdCobro = cocnCdCobro;
		this.cocnCdActividad = cocnCdActividad;
		this.cocnCalleNum = cocnCalleNum;
		this.cocnColonia = cocnColonia;
		this.cocnDelegmunic = cocnDelegmunic;
		this.cocnCdEstado = cocnCdEstado;
		this.cocnCdCiudad = cocnCdCiudad;
		this.cocnCdPostal = cocnCdPostal;
		this.cocnDsPoblac = cocnDsPoblac;
		this.cocnNuLada = cocnNuLada;
		this.cocnNuTelefono = cocnNuTelefono;
		this.cocnCorreoElect = cocnCorreoElect;
		this.cocnDiCobro1 = cocnDiCobro1;
		this.cocnCampon1 = cocnCampon1;
		this.cocnCampon2 = cocnCampon2;
		this.cocnCampon3 = cocnCampon3;
		this.cocnCampon4 = cocnCampon4;
		this.cocnCampov1 = cocnCampov1;
		this.cocnCampov2 = cocnCampov2;
		this.cocnCampov3 = cocnCampov3;
		this.cocnCampov4 = cocnCampov4;
		this.cocnCampof1 = cocnCampof1;
		this.cocnCampof2 = cocnCampof2;
		this.cocnCampof3 = cocnCampof3;
		this.cocnCampof4 = cocnCampof4;
	}
	
	public long getCocnNuCliente() {
		return this.cocnNuCliente;
	}
	public void setCocnNuCliente(long cocnNuCliente) {
		this.cocnNuCliente = cocnNuCliente;
	}
	public String getCocnNombre() {
		return this.cocnNombre;
	}
	public void setCocnNombre(String cocnNombre) {
		this.cocnNombre = cocnNombre;
	}
	public String getCocnApellidoPat() {
		return this.cocnApellidoPat;
	}
	public void setCocnApellidoPat(String cocnApellidoPat) {
		this.cocnApellidoPat = cocnApellidoPat;
	}
	public String getCocnApellidoMat() {
		return this.cocnApellidoMat;
	}
	public void setCocnApellidoMat(String cocnApellidoMat) {
		this.cocnApellidoMat = cocnApellidoMat;
	}
	public String getCocnCdSexo() {
		return this.cocnCdSexo;
	}
	public void setCocnCdSexo(String cocnCdSexo) {
		this.cocnCdSexo = cocnCdSexo;
	}
	public Date getCocnFeNacimiento() {
		return this.cocnFeNacimiento;
	}
	public void setCocnFeNacimiento(Date cocnFeNacimiento) {
		this.cocnFeNacimiento = cocnFeNacimiento;
	}
	public String getCocnRfc() {
		return this.cocnRfc;
	}
	public void setCocnRfc(String cocnRfc) {
		this.cocnRfc = cocnRfc;
	}
	public String getCocnCdEdoCivil() {
		return this.cocnCdEdoCivil;
	}
	public void setCocnCdEdoCivil(String cocnCdEdoCivil) {
		this.cocnCdEdoCivil = cocnCdEdoCivil;
	}
	public String getCocnBucCliente() {
		return this.cocnBucCliente;
	}
	public void setCocnBucCliente(String cocnBucCliente) {
		this.cocnBucCliente = cocnBucCliente;
	}
	public Short getCocnTpCliente() {
		return this.cocnTpCliente;
	}
	public void setCocnTpCliente(Short cocnTpCliente) {
		this.cocnTpCliente = cocnTpCliente;
	}
	public String getCocnCdParentesco() {
		return this.cocnCdParentesco;
	}
	public void setCocnCdParentesco(String cocnCdParentesco) {
		this.cocnCdParentesco = cocnCdParentesco;
	}
	public String getCocnSegmentoCliente() {
		return this.cocnSegmentoCliente;
	}
	public void setCocnSegmentoCliente(String cocnSegmentoCliente) {
		this.cocnSegmentoCliente = cocnSegmentoCliente;
	}
	public String getCocnCdCobro() {
		return this.cocnCdCobro;
	}
	public void setCocnCdCobro(String cocnCdCobro) {
		this.cocnCdCobro = cocnCdCobro;
	}
	public Short getCocnCdActividad() {
		return this.cocnCdActividad;
	}
	public void setCocnCdActividad(Short cocnCdActividad) {
		this.cocnCdActividad = cocnCdActividad;
	}
	public String getCocnCalleNum() {
		return this.cocnCalleNum;
	}
	public void setCocnCalleNum(String cocnCalleNum) {
		this.cocnCalleNum = cocnCalleNum;
	}
	public String getCocnColonia() {
		return this.cocnColonia;
	}
	public void setCocnColonia(String cocnColonia) {
		this.cocnColonia = cocnColonia;
	}
	public String getCocnDelegmunic() {
		return this.cocnDelegmunic;
	}
	public void setCocnDelegmunic(String cocnDelegmunic) {
		this.cocnDelegmunic = cocnDelegmunic;
	}
	public Byte getCocnCdEstado() {
		return this.cocnCdEstado;
	}
	public void setCocnCdEstado(Byte cocnCdEstado) {
		this.cocnCdEstado = cocnCdEstado;
	}
	public Short getCocnCdCiudad() {
		return this.cocnCdCiudad;
	}
	public void setCocnCdCiudad(Short cocnCdCiudad) {
		this.cocnCdCiudad = cocnCdCiudad;
	}
	public String getCocnCdPostal() {
		return this.cocnCdPostal;
	}
	public void setCocnCdPostal(String cocnCdPostal) {
		this.cocnCdPostal = cocnCdPostal;
	}
	public String getCocnDsPoblac() {
		return this.cocnDsPoblac;
	}
	public void setCocnDsPoblac(String cocnDsPoblac) {
		this.cocnDsPoblac = cocnDsPoblac;
	}
	public String getCocnNuLada() {
		return this.cocnNuLada;
	}
	public void setCocnNuLada(String cocnNuLada) {
		this.cocnNuLada = cocnNuLada;
	}
	public String getCocnNuTelefono() {
		return this.cocnNuTelefono;
	}
	public void setCocnNuTelefono(String cocnNuTelefono) {
		this.cocnNuTelefono = cocnNuTelefono;
	}
	public String getCocnCorreoElect() {
		return this.cocnCorreoElect;
	}
	public void setCocnCorreoElect(String cocnCorreoElect) {
		this.cocnCorreoElect = cocnCorreoElect;
	}
	public String getCocnDiCobro1() {
		return this.cocnDiCobro1;
	}
	public void setCocnDiCobro1(String cocnDiCobro1) {
		this.cocnDiCobro1 = cocnDiCobro1;
	}
	public Long getCocnCampon1() {
		return this.cocnCampon1;
	}
	public void setCocnCampon1(Long cocnCampon1) {
		this.cocnCampon1 = cocnCampon1;
	}
	public Long getCocnCampon2() {
		return this.cocnCampon2;
	}
	public void setCocnCampon2(Long cocnCampon2) {
		this.cocnCampon2 = cocnCampon2;
	}
	public BigDecimal getCocnCampon3() {
		return this.cocnCampon3;
	}
	public void setCocnCampon3(BigDecimal cocnCampon3) {
		this.cocnCampon3 = cocnCampon3;
	}
	public BigDecimal getCocnCampon4() {
		return this.cocnCampon4;
	}
	public void setCocnCampon4(BigDecimal cocnCampon4) {
		this.cocnCampon4 = cocnCampon4;
	}
	public String getCocnCampov1() {
		return this.cocnCampov1;
	}
	public void setCocnCampov1(String cocnCampov1) {
		this.cocnCampov1 = cocnCampov1;
	}
	public String getCocnCampov2() {
		return this.cocnCampov2;
	}
	public void setCocnCampov2(String cocnCampov2) {
		this.cocnCampov2 = cocnCampov2;
	}
	public String getCocnCampov3() {
		return this.cocnCampov3;
	}
	public void setCocnCampov3(String cocnCampov3) {
		this.cocnCampov3 = cocnCampov3;
	}
	public String getCocnCampov4() {
		return this.cocnCampov4;
	}
	public void setCocnCampov4(String cocnCampov4) {
		this.cocnCampov4 = cocnCampov4;
	}
	public Date getCocnCampof1() {
		return this.cocnCampof1;
	}
	public void setCocnCampof1(Date cocnCampof1) {
		this.cocnCampof1 = cocnCampof1;
	}
	public Date getCocnCampof2() {
		return this.cocnCampof2;
	}
	public void setCocnCampof2(Date cocnCampof2) {
		this.cocnCampof2 = cocnCampof2;
	}
	public Date getCocnCampof3() {
		return this.cocnCampof3;
	}
	public void setCocnCampof3(Date cocnCampof3) {
		this.cocnCampof3 = cocnCampof3;
	}
	public Date getCocnCampof4() {
		return this.cocnCampof4;
	}
	public void setCocnCampof4(Date cocnCampof4) {
		this.cocnCampof4 = cocnCampof4;
	}
}
