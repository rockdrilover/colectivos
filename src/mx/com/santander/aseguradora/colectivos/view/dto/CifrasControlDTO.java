package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.bo.CifrasControl;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CIFRAS_CONTROL.
 * 				Extiende de la clase CifrasControlDTO2
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CifrasControlDTO extends CifrasControlDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo cociCapoNuPoliza
	private BigDecimal cociCapoNuPoliza;
	//Propiedad para campo cociCarpCdRamo
	private BigDecimal cociCarpCdRamo;
	//Propiedad para campo cociCasuCdSucursal
	private BigDecimal cociCasuCdSucursal;
	//Propiedad para campo cociCdControl
	private BigDecimal cociCdControl;

	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cifras objeto con datos
	 */
	public CifrasControlDTO(CifrasControl cifras) {
		super(cifras);
		this.cociCapoNuPoliza= cifras.getCociCapoNuPoliza();
		this.cociCarpCdRamo= cifras.getCociCarpCdRamo();
		this.cociCasuCdSucursal= cifras.getCociCasuCdSucursal();
		this.cociCdControl= cifras.getCociCdControl();
	}

	/**
	 * Constructor de clase
	 */
	public CifrasControlDTO() {
		super();
	}

	/**
	 * @return the cociCapoNuPoliza
	 */
	public BigDecimal getCociCapoNuPoliza() {
		return cociCapoNuPoliza;
	}

	/**
	 * @param cociCapoNuPoliza the cociCapoNuPoliza to set
	 */
	public void setCociCapoNuPoliza(BigDecimal cociCapoNuPoliza) {
		this.cociCapoNuPoliza = cociCapoNuPoliza;
	}

	/**
	 * @return the cociCarpCdRamo
	 */
	public BigDecimal getCociCarpCdRamo() {
		return cociCarpCdRamo;
	}

	/**
	 * @param cociCarpCdRamo the cociCarpCdRamo to set
	 */
	public void setCociCarpCdRamo(BigDecimal cociCarpCdRamo) {
		this.cociCarpCdRamo = cociCarpCdRamo;
	}

	/**
	 * @return the cociCasuCdSucursal
	 */
	public BigDecimal getCociCasuCdSucursal() {
		return cociCasuCdSucursal;
	}

	/**
	 * @param cociCasuCdSucursal the cociCasuCdSucursal to set
	 */
	public void setCociCasuCdSucursal(BigDecimal cociCasuCdSucursal) {
		this.cociCasuCdSucursal = cociCasuCdSucursal;
	}

	/**
	 * @return the cociCdControl
	 */
	public BigDecimal getCociCdControl() {
		return cociCdControl;
	}

	/**
	 * @param cociCdControl the cociCdControl to set
	 */
	public void setCociCdControl(BigDecimal cociCdControl) {
		this.cociCdControl = cociCdControl;
	}

}