package mx.com.santander.aseguradora.colectivos.view.dto;

/**
 * DTO Vista grupo.
 * @author CJPV SEP 2017- vectormx
 *
 */
public class GrupoDTO {
	private int id;
	private String descripcion;
 	private Boolean selected;
 	private String observaciones;
 	
 	
 	
	public GrupoDTO(int id, String descripcion, Boolean selected) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.selected = selected;
	}
	public GrupoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the selected
	 */
	public Boolean getSelected() {
		return selected;
	}
	/**
	 * @param selected the selected to set
	 */
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	
 	
 	
}
