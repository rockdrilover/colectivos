package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.santander.aseguradora.colectivos.model.bo.BitacoraErrores;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_BITACORA_ERRORES. 
 * 				La ocupa la clase BitacoraErroresDTO
 * 				Extiende de la clase BitacoraErroresDTO3
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BitacoraErroresDTO2 extends BitacoraErroresDTO3 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coerCdErrorBitacora
	private BigDecimal coerCdErrorBitacora;
	//Propiedad para campo coerFechaCarga
	private Date coerFechaCarga;
	//Propiedad para campo coerNombreArchivo
	private String coerNombreArchivo;
	//Propiedad para campo coerFvalor1
	private Date coerFvalor1;
	
	/**
	 * Constructor de clase
	 * @param detalle datos de bitacora errores
	 */
	public BitacoraErroresDTO2(BitacoraErrores detalle) {
		super(detalle);
		this.coerCdErrorBitacora = detalle.getCoerCdErrorBitacora();
		this.coerFechaCarga = detalle.getBe2().getCoerFechaCarga();
		this.coerFvalor1 = detalle.getBe2().getCoerFvalor1();
		this.coerNombreArchivo = detalle.getBe2().getCoerNombreArchivo();
		
	}
	
	/**
	 * Constructor de clase
	 */
	public BitacoraErroresDTO2() {
		super();
		coerFechaCarga = new Date();
		coerFvalor1 = new Date();
	}
	
	/**
	 * @return the coerNombreArchivo
	 */
	public String getCoerNombreArchivo() {
		return coerNombreArchivo;
	}
	/**
	 * @param coerNombreArchivo the coerNombreArchivo to set
	 */
	public void setCoerNombreArchivo(String coerNombreArchivo) {
		this.coerNombreArchivo = coerNombreArchivo;
	}
	/**
	 * @return the coerCdErrorBitacora
	 */
	public BigDecimal getCoerCdErrorBitacora() {
		return coerCdErrorBitacora;
	}

	/**
	 * @param coerCdErrorBitacora the coerCdErrorBitacora to set
	 */
	public void setCoerCdErrorBitacora(BigDecimal coerCdErrorBitacora) {
		this.coerCdErrorBitacora = coerCdErrorBitacora;
	}

	/**
	 * @return the coerFechaCarga
	 */
	public Date getCoerFechaCarga() {
		return new Date(coerFechaCarga.getTime());
	}

	/**
	 * @param coerFechaCarga the coerFechaCarga to set
	 */
	public void setCoerFechaCarga(Date coerFechaCarga) {
		if(coerFechaCarga != null) {
			this.coerFechaCarga = new Date(coerFechaCarga.getTime());
		}
	}

	/**
	 * @return the coerFvalor1
	 */
	public Date getCoerFvalor1() {
		return new Date(coerFvalor1.getTime());
	}

	/**
	 * @param coerFvalor1 the coerFvalor1 to set
	 */
	public void setCoerFvalor1(Date coerFvalor1) {
		if(coerFvalor1 != null) {
			this.coerFvalor1 = new Date(coerFvalor1.getTime());
		}
	}
	
}
	