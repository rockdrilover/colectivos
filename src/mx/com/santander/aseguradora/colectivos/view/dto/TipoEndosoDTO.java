package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;

/**
 * Data Transfer Object (DTO) para tabla de Tipo Endoso
 * @author Issac Bautista
 *
 */
public class TipoEndosoDTO {
	
	/**
	 * Tipo endoso
	 */
	private int tipoEndoso;
	/**
	 * Descripcion
	 */
	private String descripcion;
	/**
	 * Usuario
	 */
	private String usuario;
	/**
	 * Correo  
	 */
	private String correo;
	
	/**
	 * Constructor de Clase
	 */
	public TipoEndosoDTO() {
		super();
	}

	/**
	 * Constructor de clase con datos
	 * @param arrDatos arreglo con datos
	 */
	public TipoEndosoDTO(Object[] arrDatos) {
		super();
		//Inicializa Variables
		this.tipoEndoso = ((BigDecimal)arrDatos[0]).intValue();
		this.descripcion = arrDatos[1].toString();
		this.usuario = arrDatos[2].toString();
		this.correo = arrDatos[3].toString();
	}

	/**
	 * @return the tipoEndoso
	 */
	public int getTipoEndoso() {
		return tipoEndoso;
	}

	/**
	 * @param tipoEndoso the tipoEndoso to set
	 */
	public void setTipoEndoso(int tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
}
