package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author CJPV - vectormx
 * @version 07-09-2017
 *
 */
public class SuscripcionDTO {
	
	private long cosuId;
	private String cosuCelular;
	private String cosuDictamen;
	private String cosuEmail;
	private String cosuExamMedico;
	private BigDecimal cosuExtraFirma;
	private Date cosuFechaAplExam;
	private Date cosuFechaCarga;
	private Date cosuFechaCitExam;
	private Date cosuFechaFirm;
	private Date cosuFechaNac;
	private Date cosuFechaNotDic;
	private Date cosuFechaRecAseg;
	private Date cosuFechaResDoc;
	private Date cosuFechaRespSuc;
	private Date cosuFechaResult;
	private Date cosuFechaSolCred;
	private Date cosuFechaSolExam;
	private BigDecimal cosuFolioCh;
	private BigDecimal cosuFolioRiesgos;
	private String cosuIdConcatenar;
	private String cosuLocal;
	private BigDecimal cosuMonto;
	private BigDecimal cosuMontoFirmado;
	private String cosuMotExamMed;
	private String cosuNombre;
	private String cosuNumCredito;
	private BigDecimal cosuNumInter;
	private String cosuObsWf;
	private String cosuObservaciones;
	private String cosuParticipacion;
	private String cosuPlaza;
	private String cosuSucEjec;
	private String cosuTelefono1;
	private String cosuTelefono2;
	private String cosuUsuarioCrea;
	private Boolean selected;
	
	/**
	 * Registro Correcto
	 */
	private Boolean correcto;
	/**
	 * Mensaje de error de validacion
	 */
	private String errorValidacion;
	/**
	 * 
	 */
	public SuscripcionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param cosuId
	 * @param cosuCelular
	 * @param cosuDictamen
	 * @param cosuEmail
	 * @param cosuExamMedico
	 * @param cosuExtraFirma
	 * @param cosuFechaAplExam
	 * @param cosuFechaCarga
	 * @param cosuFechaCitExam
	 * @param cosuFechaFirm
	 * @param cosuFechaNac
	 * @param cosuFechaNotDic
	 * @param cosuFechaRecAseg
	 * @param cosuFechaResDoc
	 * @param cosuFechaRespSuc
	 * @param cosuFechaResult
	 * @param cosuFechaSolCred
	 * @param cosuFechaSolExam
	 * @param cosuFolioCh
	 * @param cosuFolioRiesgos
	 * @param cosuIdConcatenar
	 * @param cosuLocal
	 * @param cosuMonto
	 * @param cosuMontoFirmado
	 * @param cosuMotExamMed
	 * @param cosuNombre
	 * @param cosuNumCredito
	 * @param cosuNumInter
	 * @param cosuObsWf
	 * @param cosuObservaciones
	 * @param cosuParticipacion
	 * @param cosuPlaza
	 * @param cosuSucEjec
	 * @param cosuTelefono1
	 * @param cosuTelefono2
	 * @param cosuUsuarioCrea
	 */
	public SuscripcionDTO(long cosuId, String cosuCelular, String cosuDictamen,
			String cosuEmail, String cosuExamMedico, BigDecimal cosuExtraFirma,
			Date cosuFechaAplExam, Date cosuFechaCarga, Date cosuFechaCitExam,
			Date cosuFechaFirm, Date cosuFechaNac, Date cosuFechaNotDic,
			Date cosuFechaRecAseg, Date cosuFechaResDoc, Date cosuFechaRespSuc,
			Date cosuFechaResult, Date cosuFechaSolCred, Date cosuFechaSolExam,
			BigDecimal cosuFolioCh, BigDecimal cosuFolioRiesgos,
			String cosuIdConcatenar, String cosuLocal, BigDecimal cosuMonto,
			BigDecimal cosuMontoFirmado, String cosuMotExamMed,
			String cosuNombre, String cosuNumCredito, BigDecimal cosuNumInter,
			String cosuObsWf, String cosuObservaciones,
			String cosuParticipacion, String cosuPlaza, String cosuSucEjec,
			String cosuTelefono1, String cosuTelefono2, String cosuUsuarioCrea) {
		super();
		this.cosuId = cosuId;
		this.cosuCelular = cosuCelular;
		this.cosuDictamen = cosuDictamen;
		this.cosuEmail = cosuEmail;
		this.cosuExamMedico = cosuExamMedico;
		this.cosuExtraFirma = cosuExtraFirma;
		this.cosuFechaAplExam = cosuFechaAplExam;
		this.cosuFechaCarga = cosuFechaCarga;
		this.cosuFechaCitExam = cosuFechaCitExam;
		this.cosuFechaFirm = cosuFechaFirm;
		this.cosuFechaNac = cosuFechaNac;
		this.cosuFechaNotDic = cosuFechaNotDic;
		this.cosuFechaRecAseg = cosuFechaRecAseg;
		this.cosuFechaResDoc = cosuFechaResDoc;
		this.cosuFechaRespSuc = cosuFechaRespSuc;
		this.cosuFechaResult = cosuFechaResult;
		this.cosuFechaSolCred = cosuFechaSolCred;
		this.cosuFechaSolExam = cosuFechaSolExam;
		this.cosuFolioCh = cosuFolioCh;
		this.cosuFolioRiesgos = cosuFolioRiesgos;
		this.cosuIdConcatenar = cosuIdConcatenar;
		this.cosuLocal = cosuLocal;
		this.cosuMonto = cosuMonto;
		this.cosuMontoFirmado = cosuMontoFirmado;
		this.cosuMotExamMed = cosuMotExamMed;
		this.cosuNombre = cosuNombre;
		this.cosuNumCredito = cosuNumCredito;
		this.cosuNumInter = cosuNumInter;
		this.cosuObsWf = cosuObsWf;
		this.cosuObservaciones = cosuObservaciones;
		this.cosuParticipacion = cosuParticipacion;
		this.cosuPlaza = cosuPlaza;
		this.cosuSucEjec = cosuSucEjec;
		this.cosuTelefono1 = cosuTelefono1;
		this.cosuTelefono2 = cosuTelefono2;
		this.cosuUsuarioCrea = cosuUsuarioCrea;
	}
	public long getCosuId() {
		return cosuId;
	}
	public void setCosuId(long cosuId) {
		this.cosuId = cosuId;
	}
	public String getCosuCelular() {
		return cosuCelular;
	}
	public void setCosuCelular(String cosuCelular) {
		this.cosuCelular = cosuCelular;
	}
	public String getCosuDictamen() {
		return cosuDictamen;
	}
	public void setCosuDictamen(String cosuDictamen) {
		this.cosuDictamen = cosuDictamen;
	}
	public String getCosuEmail() {
		return cosuEmail;
	}
	public void setCosuEmail(String cosuEmail) {
		this.cosuEmail = cosuEmail;
	}
	public String getCosuExamMedico() {
		return cosuExamMedico;
	}
	public void setCosuExamMedico(String cosuExamMedico) {
		this.cosuExamMedico = cosuExamMedico;
	}
	public BigDecimal getCosuExtraFirma() {
		return cosuExtraFirma;
	}
	public void setCosuExtraFirma(BigDecimal cosuExtraFirma) {
		this.cosuExtraFirma = cosuExtraFirma;
	}
	public Date getCosuFechaAplExam() {
		return cosuFechaAplExam;
	}
	public void setCosuFechaAplExam(Date cosuFechaAplExam) {
		this.cosuFechaAplExam = cosuFechaAplExam;
	}
	public Date getCosuFechaCarga() {
		return cosuFechaCarga;
	}
	public void setCosuFechaCarga(Date cosuFechaCarga) {
		this.cosuFechaCarga = cosuFechaCarga;
	}
	public Date getCosuFechaCitExam() {
		return cosuFechaCitExam;
	}
	public void setCosuFechaCitExam(Date cosuFechaCitExam) {
		this.cosuFechaCitExam = cosuFechaCitExam;
	}
	public Date getCosuFechaFirm() {
		return cosuFechaFirm;
	}
	public void setCosuFechaFirm(Date cosuFechaFirm) {
		this.cosuFechaFirm = cosuFechaFirm;
	}
	public Date getCosuFechaNac() {
		return cosuFechaNac;
	}
	public void setCosuFechaNac(Date cosuFechaNac) {
		this.cosuFechaNac = cosuFechaNac;
	}
	public Date getCosuFechaNotDic() {
		return cosuFechaNotDic;
	}
	public void setCosuFechaNotDic(Date cosuFechaNotDic) {
		this.cosuFechaNotDic = cosuFechaNotDic;
	}
	public Date getCosuFechaRecAseg() {
		return cosuFechaRecAseg;
	}
	public void setCosuFechaRecAseg(Date cosuFechaRecAseg) {
		this.cosuFechaRecAseg = cosuFechaRecAseg;
	}
	public Date getCosuFechaResDoc() {
		return cosuFechaResDoc;
	}
	public void setCosuFechaResDoc(Date cosuFechaResDoc) {
		this.cosuFechaResDoc = cosuFechaResDoc;
	}
	public Date getCosuFechaRespSuc() {
		return cosuFechaRespSuc;
	}
	public void setCosuFechaRespSuc(Date cosuFechaRespSuc) {
		this.cosuFechaRespSuc = cosuFechaRespSuc;
	}
	public Date getCosuFechaResult() {
		return cosuFechaResult;
	}
	public void setCosuFechaResult(Date cosuFechaResult) {
		this.cosuFechaResult = cosuFechaResult;
	}
	public Date getCosuFechaSolCred() {
		return cosuFechaSolCred;
	}
	public void setCosuFechaSolCred(Date cosuFechaSolCred) {
		this.cosuFechaSolCred = cosuFechaSolCred;
	}
	public Date getCosuFechaSolExam() {
		return cosuFechaSolExam;
	}
	public void setCosuFechaSolExam(Date cosuFechaSolExam) {
		this.cosuFechaSolExam = cosuFechaSolExam;
	}
	public BigDecimal getCosuFolioCh() {
		return cosuFolioCh;
	}
	public void setCosuFolioCh(BigDecimal cosuFolioCh) {
		this.cosuFolioCh = cosuFolioCh;
	}
	public BigDecimal getCosuFolioRiesgos() {
		return cosuFolioRiesgos;
	}
	public void setCosuFolioRiesgos(BigDecimal cosuFolioRiesgos) {
		this.cosuFolioRiesgos = cosuFolioRiesgos;
	}
	public String getCosuIdConcatenar() {
		return cosuIdConcatenar;
	}
	public void setCosuIdConcatenar(String cosuIdConcatenar) {
		this.cosuIdConcatenar = cosuIdConcatenar;
	}
	public String getCosuLocal() {
		return cosuLocal;
	}
	public void setCosuLocal(String cosuLocal) {
		this.cosuLocal = cosuLocal;
	}
	public BigDecimal getCosuMonto() {
		return cosuMonto;
	}
	public void setCosuMonto(BigDecimal cosuMonto) {
		this.cosuMonto = cosuMonto;
	}
	public BigDecimal getCosuMontoFirmado() {
		return cosuMontoFirmado;
	}
	public void setCosuMontoFirmado(BigDecimal cosuMontoFirmado) {
		this.cosuMontoFirmado = cosuMontoFirmado;
	}
	public String getCosuMotExamMed() {
		return cosuMotExamMed;
	}
	public void setCosuMotExamMed(String cosuMotExamMed) {
		this.cosuMotExamMed = cosuMotExamMed;
	}
	public String getCosuNombre() {
		return cosuNombre;
	}
	public void setCosuNombre(String cosuNombre) {
		this.cosuNombre = cosuNombre;
	}
	public String getCosuNumCredito() {
		return cosuNumCredito;
	}
	public void setCosuNumCredito(String cosuNumCredito) {
		this.cosuNumCredito = cosuNumCredito;
	}
	public BigDecimal getCosuNumInter() {
		return cosuNumInter;
	}
	public void setCosuNumInter(BigDecimal cosuNumInter) {
		this.cosuNumInter = cosuNumInter;
	}
	public String getCosuObsWf() {
		return cosuObsWf;
	}
	public void setCosuObsWf(String cosuObsWf) {
		this.cosuObsWf = cosuObsWf;
	}
	public String getCosuObservaciones() {
		return cosuObservaciones;
	}
	public void setCosuObservaciones(String cosuObservaciones) {
		this.cosuObservaciones = cosuObservaciones;
	}
	public String getCosuParticipacion() {
		return cosuParticipacion;
	}
	public void setCosuParticipacion(String cosuParticipacion) {
		this.cosuParticipacion = cosuParticipacion;
	}
	public String getCosuPlaza() {
		return cosuPlaza;
	}
	public void setCosuPlaza(String cosuPlaza) {
		this.cosuPlaza = cosuPlaza;
	}
	public String getCosuSucEjec() {
		return cosuSucEjec;
	}
	public void setCosuSucEjec(String cosuSucEjec) {
		this.cosuSucEjec = cosuSucEjec;
	}
	public String getCosuTelefono1() {
		return cosuTelefono1;
	}
	public void setCosuTelefono1(String cosuTelefono1) {
		this.cosuTelefono1 = cosuTelefono1;
	}
	public String getCosuTelefono2() {
		return cosuTelefono2;
	}
	public void setCosuTelefono2(String cosuTelefono2) {
		this.cosuTelefono2 = cosuTelefono2;
	}
	public String getCosuUsuarioCrea() {
		return cosuUsuarioCrea;
	}
	public void setCosuUsuarioCrea(String cosuUsuarioCrea) {
		this.cosuUsuarioCrea = cosuUsuarioCrea;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	/**
	 * @return the correcto
	 */
	public Boolean getCorrecto() {
		return correcto;
	}
	/**
	 * @param correcto the correcto to set
	 */
	public void setCorrecto(Boolean correcto) {
		this.correcto = correcto;
	}
	/**
	 * @return the errorValidacion
	 */
	public String getErrorValidacion() {
		return errorValidacion;
	}
	/**
	 * @param errorValidacion the errorValidacion to set
	 */
	public void setErrorValidacion(String errorValidacion) {
		this.errorValidacion = errorValidacion;
	}
	
	
	
	
	
}
