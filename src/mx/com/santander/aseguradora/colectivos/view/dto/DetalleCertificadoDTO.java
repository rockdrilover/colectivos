package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;

import java.util.ArrayList;
import java.util.List;

/** =======================================================================
* Autor:		Ing. Issac Bautista
* Fecha:		09-03-2021
* Description: Clase para trabajar desde la vista los datos para la pantalla
*				de Consulta detalle certificado siniestros .
* 				Extiende de la clase DetalleCertificadoDTO2
* ------------------------------------------------------------------------				
* Modificaciones
* ------------------------------------------------------------------------
* 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
* ------------------------------------------------------------------------
* 
*/
public class DetalleCertificadoDTO extends DetalleCertificadoDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para datos del cliente
	private Cliente cliente;
	//Propiedad lista para datos de certificados del cliente
	private List<Certificado> lstCertificados;
	//Propiedad lista para coberturas
	private transient List<Object> lstCoberturas;
	//Propiedad lista para asistencias
	private transient List<Object> lstAsistencias;
	
	
	/**
	 * Constructor de clase
	 */
	public DetalleCertificadoDTO() {
		//Llamar constructor de clase padre
		super();
		
		//Inicializamos valores
		this.cliente = new Cliente();
		this.lstCertificados = new ArrayList<Certificado>();
		this.lstCoberturas = new ArrayList<Object>();
		this.lstAsistencias = new ArrayList<Object>();
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}
	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	/**
	 * @return the lstCertificados
	 */
	public List<Certificado> getLstCertificados() {
		return new ArrayList<Certificado>(lstCertificados);
	}
	/**
	 * @param lstCertificados the lstCertificados to set
	 */
	public void setLstCertificados(List<Certificado> lstCertificados) {
		this.lstCertificados = new ArrayList<Certificado>(lstCertificados);
	}
	/**
	 * @return the lstCoberturas
	 */
	public List<Object> getLstCoberturas() {
		return new ArrayList<Object>(lstCoberturas);
	}
	/**
	 * @param lstCoberturas the lstCoberturas to set
	 */
	public void setLstCoberturas(List<Object> lstCoberturas) {
		this.lstCoberturas = new ArrayList<Object>(lstCoberturas);
	}
	/**
	 * @return the lstAsistencias
	 */
	public List<Object> getLstAsistencias() {
		return new ArrayList<Object>(lstAsistencias);
	}
	/**
	 * @param lstAsistencias the lstAsistencias to set
	 */
	public void setLstAsistencias(List<Object> lstAsistencias) {
		this.lstAsistencias = new ArrayList<Object>(lstAsistencias);
	}
	
}