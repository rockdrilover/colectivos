package mx.com.santander.aseguradora.colectivos.view.dto;

/**
 * respuesta de timbrado
 * @author CJPV - vectormx
 * @version  1 - 07 -11 -2017
 *
 */
public class ResDescargaCFDIDTO {
	
	/**
	 * Clave mensaje
	 */
	private String mensaje;
	
	/**
	 * Descripcion mensaje
	 */
	private String descripcion;
	
	private byte[] bytes;
	
	private String fnombre;
	
	private String content;
	
	
	public ResDescargaCFDIDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the bytes
	 */
	public byte[] getBytes() {
		return bytes;
	}

	/**
	 * @param bytes the bytes to set
	 */
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	/**
	 * @return the fnombre
	 */
	public String getFnombre() {
		return fnombre;
	}

	/**
	 * @param fnombre the fnombre to set
	 */
	public void setFnombre(String fnombre) {
		this.fnombre = fnombre;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	
	
}
