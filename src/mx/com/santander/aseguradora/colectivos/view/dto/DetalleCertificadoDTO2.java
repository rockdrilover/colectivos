package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
* Fecha:		09-03-2021
* Description: Clase para trabajar desde la vista los datos para la pantalla
*				de Consulta detalle certificado siniestros .
 * 				La ocupa la clase DetalleCertificadoDTO
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class DetalleCertificadoDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad lista para asegurados
	private transient List<Object> lstAsegurados;
	//Propiedad lista para beneficiarios
	private List<BeneficiariosDTO> lstBeneficiarios;
	//Propiedad lista para endosos
	private List<EndosoDatosDTO> lstEndosos;
	
	/**
	 * Constructor de clase
	 */
	public DetalleCertificadoDTO2() {
		//Inicializamos valores
		this.lstAsegurados = new ArrayList<Object>();
		this.lstBeneficiarios = new ArrayList<BeneficiariosDTO>();
		this.lstEndosos = new ArrayList<EndosoDatosDTO>();
		
	}

	/**
	 * @return the lstAsegurados
	 */
	public List<Object> getLstAsegurados() {
		return new ArrayList<Object>(lstAsegurados);
	}
	/**
	 * @param lstAsegurados the lstAsegurados to set
	 */
	public void setLstAsegurados(List<Object> lstAsegurados) {
		this.lstAsegurados = new ArrayList<Object>(lstAsegurados);
	}
	/**
	 * @return the lstBeneficiarios
	 */
	public List<BeneficiariosDTO> getLstBeneficiarios() {
		return new ArrayList<BeneficiariosDTO>(lstBeneficiarios);
	}
	/**
	 * @param lstBeneficiarios the lstBeneficiarios to set
	 */
	public void setLstBeneficiarios(List<BeneficiariosDTO> lstBeneficiarios) {
		this.lstBeneficiarios = new ArrayList<BeneficiariosDTO>(lstBeneficiarios);
	}
	/**
	 * @return the lstEndosos
	 */
	public List<EndosoDatosDTO> getLstEndosos() {
		return new ArrayList<EndosoDatosDTO>(lstEndosos);
	}
	/**
	 * @param lstEndosos the lstEndosos to set
	 */
	public void setLstEndosos(List<EndosoDatosDTO> lstEndosos) {
		this.lstEndosos = new ArrayList<EndosoDatosDTO>(lstEndosos);
	}
}