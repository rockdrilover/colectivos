package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para trabajar desde la vista los datos de Comisiones.
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class ComisionesDTO2 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para campo comContingente
	protected BigDecimal comContingente;
	//Propiedad para campo comComercial
	protected BigDecimal comComercial;
	//Propiedad para campo comRepartos
	protected BigDecimal comRepartos;
		
	/**
	 * Constructor de clase 
	 */
	public ComisionesDTO2() {
		this.comContingente = new BigDecimal(0);
		this.comComercial = new BigDecimal(0);
		this.comRepartos = new BigDecimal(0);
	}
	
	/**
	 * @return the comRepartos
	 */
	public BigDecimal getComRepartos() {
		return comRepartos;
	}
	/**
	 * @param comRepartos the comRepartos to set
	 */
	public void setComRepartos(BigDecimal comRepartos) {
		this.comRepartos = comRepartos;
	}
	/**
	 * @return the comComercial
	 */
	public BigDecimal getComComercial() {
		return comComercial;
	}
	/**
	 * @param comComercial the comComercial to set
	 */
	public void setComComercial(BigDecimal comComercial) {
		this.comComercial = comComercial;
	}
	/**
	 * @return the comContingente
	 */
	public BigDecimal getComContingente() {
		return comContingente;
	}
	/**
	 * @param comContingente the comContingente to set
	 */
	public void setComContingente(BigDecimal comContingente) {
		this.comContingente = comContingente;
	}
}