package mx.com.santander.aseguradora.colectivos.view.dto;

public class ReqCancelacionCFDIDTO {
	/**
	 * Usuario pistas de auditoria.
	 */
	private String usuario;

	/**
	 * Aplicativo pistas de auditoria.
	 */
	private String aplicativo;

	/**
	 * Ipdir pistas de auditoria.
	 */
	private String ipdir;
	
	/**
	 * Canal pistas de auditoria.
	 */
	private String canal;
	
	/**
	 * recibo pistas de auditoria.
	 */
	private String recibo;

	/**
	 * 
	 */
	public ReqCancelacionCFDIDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param usuario
	 * @param aplicativo
	 * @param ipdir
	 * @param canal
	 * @param recibo
	 */
	public ReqCancelacionCFDIDTO(String usuario, String aplicativo, String ipdir, String canal, String recibo) {
		super();
		this.usuario = usuario;
		this.aplicativo = aplicativo;
		this.ipdir = ipdir;
		this.canal = canal;
		this.recibo = recibo;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the aplicativo
	 */
	public String getAplicativo() {
		return aplicativo;
	}

	/**
	 * @param aplicativo the aplicativo to set
	 */
	public void setAplicativo(String aplicativo) {
		this.aplicativo = aplicativo;
	}

	/**
	 * @return the ipdir
	 */
	public String getIpdir() {
		return ipdir;
	}

	/**
	 * @param ipdir the ipdir to set
	 */
	public void setIpdir(String ipdir) {
		this.ipdir = ipdir;
	}

	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * @param canal the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * @return the recibo
	 */
	public String getRecibo() {
		return recibo;
	}

	/**
	 * @param recibo the recibo to set
	 */
	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}

	
	
}
