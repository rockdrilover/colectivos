package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCedula;

import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CEDULA.
 * 				La ocupa la clase CedulaControlDTO5
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CedulaControlDTO6 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccTipoCedula
	private BigDecimal coccTipoCedula;
	//Propiedad para campo coccProcesada
	private BigDecimal coccProcesada;

	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cedula objeto con datos
	 */
	public CedulaControlDTO6(ColectivosCedula cedula) {
		this.coccTipoCedula = cedula.getCc3().getCoccTipoCedula();
		this.coccProcesada = cedula.getCc3().getCoccProcesada();
	}
	/**
	 * Constructor de clase
	 */
	public CedulaControlDTO6() {
		// Dont Using
	}
	
	/**
	 * @return the coccTipoCedula
	 */
	public BigDecimal getCoccTipoCedula() {
		return coccTipoCedula;
	}
	/**
	 * @param coccTipoCedula the coccTipoCedula to set
	 */
	public void setCoccTipoCedula(BigDecimal coccTipoCedula) {
		this.coccTipoCedula = coccTipoCedula;
	}
	/**
	 * @return the coccProcesada
	 */
	public BigDecimal getCoccProcesada() {
		return coccProcesada;
	}
	/**
	 * @param coccProcesada the coccProcesada to set
	 */
	public void setCoccProcesada(BigDecimal coccProcesada) {
		this.coccProcesada = coccProcesada;
	}
}