package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCedula;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import java.math.BigDecimal;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_CEDULA.
 * 				La ocupa la clase CedulaControlDTO
 * 				Extiende de la clase CedulaControlDTO3
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class CedulaControlDTO2 extends CedulaControlDTO3 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coccUnidadNegocio
	private String coccUnidadNegocio;
	//Propiedad para campo coccFeRegistro
	private Date coccFeRegistro;
	//Propiedad para campo coccFeEntrega
	private Date coccFeEntrega;
	//Propiedad para campo coccNuRegPnd
	private BigDecimal coccNuRegPnd;
	
	/**
	 * Constructor de clase, copia informacion de clase entity 
	 * @param cedula objeto con datos
	 */
	public CedulaControlDTO2(ColectivosCedula cedula) {
		super(cedula);
		this.coccUnidadNegocio = cedula.getCoccUnidadNegocio();
		this.coccFeRegistro = cedula.getCoccFeRegistro();
		this.coccFeEntrega = cedula.getCoccFeEntrega();
		this.coccNuRegPnd = (BigDecimal) Utilerias.objectIsNull(cedula.getCc2().getCoccNuRegPnd(), Constantes.TIPO_DATO_BIGDECIMAL);
	}
	/**
	 * Constructor de clase
	 */
	public CedulaControlDTO2() {
		super();
	}

	/**
	 * @return the coccUnidadNegocio
	 */
	public String getCoccUnidadNegocio() {
		return coccUnidadNegocio;
	}
	/**
	 * @param coccUnidadNegocio the coccUnidadNegocio to set
	 */
	public void setCoccUnidadNegocio(String coccUnidadNegocio) {
		this.coccUnidadNegocio = coccUnidadNegocio;
	}
	/**
	 * @return the coccFeRegistro
	 */
	public Date getCoccFeRegistro() {
		return new Date(coccFeRegistro.getTime());
	}
	/**
	 * @param coccFeRegistro the coccFeRegistro to set
	 */
	public void setCoccFeRegistro(Date coccFeRegistro) {
		if(coccFeRegistro != null) {
			this.coccFeRegistro = new Date(coccFeRegistro.getTime());
		}
	}
	/**
	 * @return the coccFeEntrega
	 */
	public Date getCoccFeEntrega() {
		return new Date(coccFeEntrega.getTime());
	}
	/**
	 * @param coccFeEntrega the coccFeEntrega to set
	 */
	public void setCoccFeEntrega(Date coccFeEntrega) {
		if(coccFeEntrega != null) {
			this.coccFeEntrega = new Date(coccFeEntrega.getTime());
		}
	}
	/**
	 * @return the coccNuRegPnd
	 */
	public BigDecimal getCoccNuRegPnd() {
		return coccNuRegPnd;
	}
	/**
	 * @param coccNuRegPnd the coccNuRegPnd to set
	 */
	public void setCoccNuRegPnd(BigDecimal coccNuRegPnd) {
		this.coccNuRegPnd = coccNuRegPnd;
	}
}