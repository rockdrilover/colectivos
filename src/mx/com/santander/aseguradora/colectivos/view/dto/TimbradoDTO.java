package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosRecibo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Facturacion;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		20-04-2020
 * Description: Clase para trabajar desde la vista los datos para Timbrado.
 * 				
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class TimbradoDTO implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo recibo
	private ColectivosRecibo recibo;
	//Propiedad para campo factura
	private Facturacion factura;
	//Propiedad para campo lstCobe
	private List<ColectivosCoberturas> lstCobe;
	//Propiedad para campo lstComp
	private List<ColectivosComponentes> lstComp;

	/**
	 * Constructor de clase
	 */
	public TimbradoDTO() {
		super();
		recibo = new ColectivosRecibo();
		factura = new Facturacion();
		lstCobe = new ArrayList<>();
		lstComp = new ArrayList<>();
	}
	
	/**
	 * Constructor de Clase
	 * @param datos datos para asignar
	 * @param resulset cursor de donde se tomaran los resultados
	 */
	public TimbradoDTO(Object[] datos, Integer resulset) {
		recibo = new ColectivosRecibo();
		factura = new Facturacion();
		
		if(resulset == 2) {
			factura.setCofaNuReciboFiscal((BigDecimal) datos[0]);
			recibo.setcoreNuConsecutivoCuota(((BigDecimal) datos[1]).shortValue());//consecutivo
			recibo.setcoreFeEmision((Date)datos[2]);//feemision
			recibo.setcoreFeDesde((Date)datos[3]);//fedesde
			recibo.setcoreFeHasta((Date)datos[4]);//fehasta
			factura.setCofaNuCuenta(datos[5].toString()); //Estatus Sistema
			factura.setCofaNuCuentaDep(datos[6].toString()); //Estatus Cobranza
			recibo.setcoreMtPrima((BigDecimal) datos[7]);//prima
		} else {
			this.factura.setCofaCampov4(datos[0].toString() + "-" + datos[1].toString() + "-" + datos[2].toString() + "-" + datos[3].toString());
			this.factura.setCofaNuReciboFiscal((BigDecimal) datos[4]);
			this.factura.setCofaStRecibo(datos[5].toString());		//Estatus Timbrado
			this.factura.setCofaFeFacturacion((Date)datos[6]); 		//Fecha Timbrado
			this.recibo.setcoreStRecibo(Utilerias.objectIsNull(datos[7], Constantes.TIPO_DATO_STRING).toString());  		//Estatus Cancelado
			this.recibo.setcoreFeStatus((Date)datos[8]);			//Fecha Cancelacion
			this.factura.setCofaCampov3(datos[9].toString());
			
			if(resulset != Constantes.DEFAULT_INT) {
				recibo.setcoreFeDesde((Date)datos[10]);
				recibo.setcoreFeHasta((Date)datos[11]);
				recibo.setcoreFeEmision((Date)datos[12]);
				factura.setCofaCampof2((Date)datos[13]);	//Fecha Facturacion
				factura.setCofaCampov1(datos[14].toString()); //Certificado ante el SAT
				factura.setCofaMtTotSua((BigDecimal) datos[15]);
				recibo.setcoreMtPrima((BigDecimal) datos[16]);
				factura.setCofaNuCuenta(datos[17].toString()); //Estatus Sistema
				factura.setCofaCdUsuario(datos[18].toString()); //Cedula, RIF y Razon Social
				factura.setCofaNuCuentaDep(datos[19].toString()); //Estatus Cobranza
				recibo.setcoreNuConsecutivoCuota(((BigDecimal) datos[20]).shortValue());
				recibo.setcoreMtPrimaPura((BigDecimal) datos[21]);
				factura.setCofaTotalCertificados((BigDecimal) datos[23]);
				factura.setCofaMtTotIva((BigDecimal) datos[24]);
				factura.setCofaMtTotDpo((BigDecimal) datos[25]);
				factura.setCofaMtTotRfi((BigDecimal) datos[26]);
				factura.setCofaCampov2(datos[27].toString()); //RFC
			}
		}
	}

	/**
	 * @return the recibo
	 */
	public ColectivosRecibo getRecibo() {
		return recibo;
	}

	/**
	 * @param recibo the recibo to set
	 */
	public void setRecibo(ColectivosRecibo recibo) {
		this.recibo = recibo;
	}

	/**
	 * @return the factura
	 */
	public Facturacion getFactura() {
		return factura;
	}

	/**
	 * @param factura the factura to set
	 */
	public void setFactura(Facturacion factura) {
		this.factura = factura;
	}
	/**
	 * @return the lstCobe
	 */
	public List<ColectivosCoberturas> getLstCobe() {
		return new ArrayList<ColectivosCoberturas>(lstCobe);
	}
	/**
	 * @param lstCobe the lstCobe to set
	 */
	public void setLstCobe(List<ColectivosCoberturas> lstCobe) {
		this.lstCobe = new ArrayList<ColectivosCoberturas>(lstCobe);
	}
	/**
	 * @return the lstComp
	 */
	public List<ColectivosComponentes> getLstComp() {
		return new ArrayList<ColectivosComponentes>(lstComp);
	}
	/**
	 * @param lstComp the lstComp to set
	 */
	public void setLstComp(List<ColectivosComponentes> lstComp) {
		this.lstComp = new ArrayList<ColectivosComponentes>(lstComp);
	}
}
	