package mx.com.santander.aseguradora.colectivos.view.dto;

import java.math.BigDecimal;

public class DetalleReporteDxpParams {
	//-----------------------------------------------------------------------------------------------------------------
		/*Variables de instancia.*/
		//-----------------------------------------------------------------------------------------------------------------

		//														//Mes de la cuota de la poliza.
		protected String strMes;
		//														//Numero de cuota de la poliza.
		protected short shortCuota;
		//														//Factura del Mes Prima Neta.
		protected BigDecimal bdecimalFMPN;
		//														//Monto Cobrado Prima Neta.
		protected BigDecimal bdecimalMCPN;
		//														//Endoso Venta Nueva.
		protected BigDecimal bdecimalEVN;
		//														//Endoso Disminucion de Stock.
		protected BigDecimal bdecimalEDS;
		//														//Endoso Cancelación Prima Neta.
		protected BigDecimal bdecimalECPN;
		//														//Emisión de Prima Anualizada.
		protected BigDecimal bdecimalEPA;
		//														//Emisión de Prima.
		protected BigDecimal bdecimalEP;
		//														//DxP Exigible del Mes
		protected BigDecimal bdecimalDxPEM;
		
		public String getStrMes() {
			return strMes;
		}

		public short getShortCuota() {
			return shortCuota;
		}
		
		public BigDecimal getBdecimalFMPN() {
			return bdecimalFMPN;
		}
		
		public BigDecimal getBdecimalMCPN() {
			return bdecimalMCPN;
		}
		
		public BigDecimal getBdecimalEVN() {
			return bdecimalEVN;
		}
		
		public BigDecimal getBdecimalEDS() {
			return bdecimalEDS;
		}
		public BigDecimal getBdecimalECPN() {
			return bdecimalECPN;
		}

		public BigDecimal getBdecimalEPA() {
			return bdecimalEPA;
		}
		public BigDecimal getBdecimalEP() {
			return bdecimalEP;
		}
		public BigDecimal getBdecimalDxPEM() {
			return bdecimalDxPEM;
		}
}
