package mx.com.santander.aseguradora.colectivos.view.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.bo.BitacoraErrores;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para trabajar desde la vista los datos de la tabla 
 * 				COLECTIVOS_BITACORA_ERRORES. 
 * 				La ocupa la clase BitacoraErroresDTO3
 * 				Extiende de la clase BitacoraErroresDTO5
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 13-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BitacoraErroresDTO4 extends BitacoraErroresDTO5 implements Serializable {
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para campo coerNvalor2
	private BigDecimal coerNvalor2;
	//Propiedad para campo coerNvalor3
	@SuppressWarnings("unused")
	private BigDecimal coerNvalor3;
	//Propiedad para campo coerTipoError
	private String coerTipoError;
	//Propiedad para campo coerTipoProceso
	private String coerTipoProceso;
	//Propiedad para campo coerVvalor1
	private String coerVvalor1;

	/**
	 * Constructor de clase
	 * @param detalle datos de bitacora errores
	 */
	public BitacoraErroresDTO4(BitacoraErrores detalle) {
		super(detalle);
		this.coerNvalor2 = detalle.getBe2().getCoerNvalor2();
		this.coerNvalor3 = detalle.getBe2().getCoerNvalor3();
		this.coerTipoError = detalle.getBe3().getCoerTipoError();
		this.coerTipoProceso = detalle.getBe3().getCoerTipoProceso();
		this.coerVvalor1 = detalle.getBe3().getCoerVvalor1();
	}
	
	/**
	 * Constructor de clase
	 */
	public BitacoraErroresDTO4() {
		super();
	}
	
	/**
	 * @return the coerNvalor2
	 */
	public BigDecimal getCoerNvalor2() {
		return coerNvalor2;
	}

	/**
	 * @param coerNvalor2 the coerNvalor2 to set
	 */
	public void setCoerNvalor2(BigDecimal coerNvalor2) {
		this.coerNvalor2 = coerNvalor2;
	}

	/**
	 * @return the coerTipoError
	 */
	public String getCoerTipoError() {
		return coerTipoError;
	}

	/**
	 * @param coerTipoError the coerTipoError to set
	 */
	public void setCoerTipoError(String coerTipoError) {
		this.coerTipoError = coerTipoError;
	}

	/**
	 * @return the coerTipoProceso
	 */
	public String getCoerTipoProceso() {
		return coerTipoProceso;
	}

	/**
	 * @param coerTipoProceso the coerTipoProceso to set
	 */
	public void setCoerTipoProceso(String coerTipoProceso) {
		this.coerTipoProceso = coerTipoProceso;
	}

	/**
	 * @return the coerVvalor1
	 */
	public String getCoerVvalor1() {
		return coerVvalor1;
	}

	/**
	 * @param coerVvalor1 the coerVvalor1 to set
	 */
	public void setCoerVvalor1(String coerVvalor1) {
		this.coerVvalor1 = coerVvalor1;
	}

}
	