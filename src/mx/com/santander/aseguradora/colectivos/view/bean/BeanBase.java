/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Sergio Plata
 *
 */
public class BeanBase {

	protected final Log log = LogFactory.getLog(this.getClass());
	
	
	public BeanBase() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	protected void init(){
		
	}
	
}
