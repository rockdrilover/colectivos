/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import javax.annotation.Resource;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CgRefCodes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Recibo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ReciboId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqDescargaCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResDescargaCFDIDTO;




/**
 * @author dflores
 * 
 *
 */
@Controller
@Scope("request")
public class BeanRecibo {
	
	@Resource
	private ServicioRecibo servicioRecibo;
	@Resource
	private ServicioCertificado servicioCertificado;
	
	private Log log = LogFactory.getLog(this.getClass());
	@SuppressWarnings("unused")
	private StringBuilder message;
	private ReciboId id;
	private CgRefCodes estatus;
	private Recibo recibo;
	private Integer careCapoNuPoliza;
	private Short careCarpCdRamo;
	private String careStRecibo;
	private Date careFeEmision;
	private Short primaUnica;
	private BigDecimal reciboAnterior;
	private boolean endosar;
	private String  mensajeEndoso;
	private String  nombre = "";
	
	/**
	 * UUID del cfdi relacionado al RECIBO
	 */
	private String UUID;
	
	private String stCancelado;
	
	/**
	 * Para saber el estatus de cobranza
	 */
	private String stCobranza;
	/**
	 * @return the stCobranza
	 */
	public String getStCobranza() {
		return stCobranza;
	}
	/**
	 * @param stCobranza the stCobranza to set
	 */
	public void setStCobranza(String stCobranza) {
		this.stCobranza = stCobranza;
	}
	
	/**
	 * @return the servicioRecibo
	 */
	public ServicioRecibo getServicioRecibo() {
		return servicioRecibo;
	}
	/**
	 * @param servicioRecibo the servicioReciboto set
	 */
	public void setServicioRecibo(ServicioRecibo servicioRecibo) {
		this.servicioRecibo = servicioRecibo;
	}
	/**
	 * @return the id
	 */
	public ReciboId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ReciboId id) {
		this.id = id;
	}
	
	public CgRefCodes getEstatus() {
		return estatus;
	}
	public void setEstatus(CgRefCodes estatus) {
		this.estatus = estatus;
	}
	
	/**
	 * @return the recibo
	 */
	public Recibo getRecibo() {
		return recibo;
	}
	/**
	 * @param recibo the recibo to set
	 */
	public void setRecibo(Recibo recibo) {
		this.recibo = recibo;
	}

	public int getCareCapoNuPoliza() {
		return this.careCapoNuPoliza;
	}

	public void setCareCapoNuPoliza(int careCapoNuPoliza) {
		this.careCapoNuPoliza = careCapoNuPoliza;
	}
	
	public Short getCareCarpCdRamo() {
		return this.careCarpCdRamo;
	}

	public void setCareCarpCdRamo(Short careCarpCdRamo) {
		this.careCarpCdRamo = careCarpCdRamo;
	}
	
	public String getCareStRecibo() {
		return this.careStRecibo;
	}

	public void setCareStRecibo(String careStRecibo) {
		this.careStRecibo = careStRecibo;
	}
	
	public Date getCareFeEmision() {
		return this.careFeEmision;
	}

	public void setCareFeEmision(Date careFeEmision) {
		this.careFeEmision = careFeEmision;
	}
	
	public void setPrimaUnica(Short primaUnica) {
		this.primaUnica = primaUnica;
	}
	public Short getPrimaUnica() {
		return primaUnica;
	}
	
	/**
	 * @return the reciboAnterior
	 */
	public BigDecimal getReciboAnterior() {
		return reciboAnterior;
	}
	/**
	 * @param reciboAnterior the reciboAnterior to set
	 */
	public void setReciboAnterior(BigDecimal reciboAnterior) {
		this.reciboAnterior = reciboAnterior;
	}
		
	/**
	 * @return the endosar
	 */
	public boolean isEndosar() {
		return endosar;
	}
	/**
	 * @param endosar the endosar to set
	 */
	public void setEndosar(boolean endosar) {
		this.endosar = endosar;
	}
	
	/**
	 * @return the mensajeEndoso
	 */
	public String getMensajeEndoso() {
		return mensajeEndoso;
	}
	/**
	 * @param mensajeEndoso the mensajeEndoso to set
	 */
	public void setMensajeEndoso(String mensajeEndoso) {
		this.mensajeEndoso = mensajeEndoso;
	}
	
	public BeanRecibo() {
		// TODO Auto-generated constructor stub
		//this.log.info("Bean recibo creado.");
		this.message = new StringBuilder();
		setId(new ReciboId());
		setEstatus(new CgRefCodes());
		UUID = "";
		
	}

	/**
	 * Descarga PDF CFDI relacionado al recibo
	 */
	public void descargaCFDIPDF() {
	
		
		String ip = "";
		String usuario = "";
		ExternalContext context;
		context = FacesContext.getCurrentInstance().getExternalContext();

		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		HttpSession sesion = request.getSession(true);
		try {
			HttpServletResponse response = FacesUtils.getServletResponse();

		    OutputStream out = response.getOutputStream ();

		    
		ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null || "".equals(ip)) {
       	 ip = request.getRemoteAddr();
        }
        
        if ("0:0:0:0:0:0:0:1".equalsIgnoreCase(ip)) {
       	    InetAddress inetAddress = InetAddress.getLocalHost();
       	    String ipAddress = inetAddress.getHostAddress();
       	    ip = ipAddress;
       	}
		usuario = (String) sesion.getAttribute("username");
		
		
		ReqDescargaCFDIDTO reqDescargaCFDIDTO = new ReqDescargaCFDIDTO(
				usuario, 
				Constantes.CFDI_APLICACION, 
				ip, 
				String.valueOf(id.getCareCasuCdSucursal()), 
				String.valueOf(careCarpCdRamo), 
				String.valueOf(careCapoNuPoliza), 
				String.valueOf(0), 
				String.format("%.1f",id.getCareNuRecibo()), 
				UUID, 
				Constantes.CFDI_PDF);
		
		ResDescargaCFDIDTO res = this.servicioCertificado.descargaCFDI(reqDescargaCFDIDTO);
		if (Constantes.CFDI_DESCARGAR_EXISTOSO.equals(res.getMensaje())) {
			response.setContentType("application/pdf");
			DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
			httpUtil.setHeader(response, Constantes.DOWNLOAD_CONTENT, Constantes.DOWNLOAD_ATTACH + res.getFnombre());
			
			out.write(res.getBytes());
			out.flush();
			out.close();
		} else {
			
			String resMensaje = res.getMensaje() + " - " + res.getDescripcion();
			 response.setContentType("application/txt");
				response.setHeader(Constantes.DOWNLOAD_CONTENT, Constantes.DOWNLOAD_ATTACH + "\"" + "errorDecargaPDF.txt"  + "\""); 
			
			out.write(ESAPI.encoder().encodeForHTML(resMensaje).getBytes(StandardCharsets.UTF_8));
			
			out.flush();
			out.close();
			
			FacesUtils.addInfoMessage(res.getMensaje() + " - " + res.getDescripcion());
		}
		
		
		} catch (Exception e) {
			this.message.append("Error al generar el PDF del CFDI.");
			this.log.error(message.toString(), e);
			FacesUtils.addInfoMessage("Error al generar el PDF del CFDI");
		}
		
		FacesContext.getCurrentInstance().responseComplete();		

	}
	
	
	/**
	 * Descarga PDF CFDI relacionado al recibo
	 */
	public void descargaCFDIXML() {
	
		
		String ip = "";
		String usuario = "";
		ExternalContext context;
		context = FacesContext.getCurrentInstance().getExternalContext();

		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		HttpSession sesion = request.getSession(true);
		try {
			HttpServletResponse response = FacesUtils.getServletResponse();

		    OutputStream out = response.getOutputStream ();

		    
		ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null || "".equals(ip)) {
       	 ip = request.getRemoteAddr();
        }
        
        if ("0:0:0:0:0:0:0:1".equalsIgnoreCase(ip)) {
       	    InetAddress inetAddress = InetAddress.getLocalHost();
       	    String ipAddress = inetAddress.getHostAddress();
       	    ip = ipAddress;
       	}
		usuario = (String) sesion.getAttribute("username");
		
		
		ReqDescargaCFDIDTO reqDescargaCFDIDTO = new ReqDescargaCFDIDTO(
				usuario, 
				Constantes.CFDI_APLICACION, 
				ip, 
				String.valueOf(id.getCareCasuCdSucursal()), 
				String.valueOf(careCarpCdRamo), 
				String.valueOf(careCapoNuPoliza), 
				String.valueOf(0), 
				String.format("%.1f",id.getCareNuRecibo()), 
				UUID, 
				Constantes.CFDI_XML);
		
		ResDescargaCFDIDTO res = this.servicioCertificado.descargaCFDI(reqDescargaCFDIDTO);
		if (Constantes.CFDI_DESCARGAR_EXISTOSO.equals(res.getMensaje())) {
			response.setContentType("application/xml");

			DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
			httpUtil.setHeader(response, Constantes.DOWNLOAD_CONTENT, Constantes.DOWNLOAD_ATTACH + res.getFnombre());

			out.write(res.getBytes());
			out.flush();
			out.close();
		} else {
			
			String resMensaje = res.getMensaje() + " - " + res.getDescripcion();
			 response.setContentType("application/txt");
				response.setHeader(Constantes.DOWNLOAD_CONTENT, Constantes.DOWNLOAD_ATTACH + "\"" + "errorDecargaXML.txt"  + "\""); 
		
			out.write(ESAPI.encoder().encodeForHTML(resMensaje).getBytes(StandardCharsets.UTF_8));
			
			out.flush();
			out.close();
			
			FacesUtils.addInfoMessage(res.getMensaje() + " - " + res.getDescripcion());
		}
		
		
		} catch (Exception e) {
			this.message.append("Error al generar el PDF del CFDI.");
			this.log.error(message.toString(), e);
			FacesUtils.addInfoMessage("Error al generar el PDF del CFDI");
		}
		
		FacesContext.getCurrentInstance().responseComplete();		

	}
	
	
	
   public void imprimeRecibo(){
		
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		nombre = "ReciboPagoPrimas_"+id.getCareNuRecibo().toString().replace('.', '_');
		this.servicioRecibo.impresionRecibo(rutaTemporal, nombre
				, id.getCareCasuCdSucursal(), careCarpCdRamo
				, careCapoNuPoliza, id.getCareNuRecibo(),this.primaUnica);	
		
		int read = 0;
		byte[] bytes = new byte[1024];
	    
		
		try{
			HttpServletResponse response = FacesUtils.getServletResponse();
			File f = new File (rutaTemporal + nombre + ".pdf");
		    InputStream in = new FileInputStream (f);		    
		    OutputStream out = response.getOutputStream ();
		       
		    response.setContentType("application/pdf");
			response.setHeader(Constantes.DOWNLOAD_CONTENT, Constantes.DOWNLOAD_ATTACH + "\"" + nombre + ".pdf" + "\""); 

			while((read = in.read(bytes)) != -1){
				out.write(bytes,0,read);
			}

			in.close();
		
			out.flush();
			out.close();
			
			f.delete();
			
			} catch (Exception e) {
				log.error("Error al enviar reporte de PreFactura-Asistencias",e);
				FacesUtils.addInfoMessage("Error al generar el reporte");
			}
			
			FacesContext.getCurrentInstance().responseComplete();		
	}

   public void imprimeRecibo2(){
		
	   this.id.setCareNuRecibo(this.reciboAnterior);
	   
		this.imprimeRecibo();	
	}
/**
 * @return the uUID
 */
public String getUUID() {
	return UUID;
}
/**
 * @param uUID the uUID to set
 */
public void setUUID(String uUID) {
	UUID = uUID;
}

   
/**
 * Valida si el recibo se encuentra timbrado   
 * @return false UUID en blanco / true UUID relacionado 
 */
public boolean getBlnReciboTimbrado() {
	return  !("".equals(UUID == null ? "" : UUID ));
}


public boolean getBlnReciboCancelado() {
	return  !("".equals(stCancelado == null ? "" : stCancelado ));
}


public String getStyleColumn() {
	return this.getBlnReciboTimbrado() ? "" : "display: none;"; 
}

public String getStyleColumnTimbrar() {
	return !this.getBlnReciboTimbrado() ? "" : "display: none;"; 
}
/**
 * @return the servicioCertificado
 */	
public ServicioCertificado getServicioCertificado() {
	return servicioCertificado;
}
/**
 * @param servicioCertificado the servicioCertificado to set
 */
public void setServicioCertificado(ServicioCertificado servicioCertificado) {
	this.servicioCertificado = servicioCertificado;
}
/**
 * @return the stCancelado
 */
public String getStCancelado() {
	return stCancelado;
}
/**
 * @param stCancelado the stCancelado to set
 */
public void setStCancelado(String stCancelado) {
	this.stCancelado = stCancelado;
}



}
