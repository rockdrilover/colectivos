/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;


/**
 * @author Z014058
 *
 */
public class BeanCertificadosCount {
	private Long cuentaCertif;
	private String descripcion;
	/**
	 * @return the cuentaCertif
	 */
	public Long getCuentaCertif() {
		return cuentaCertif;
	}
	/**
	 * @param cuentaCertif the cuentaCertif to set
	 */
	public void setCuentaCertif(Long cuentaCertif) {
		this.cuentaCertif = cuentaCertif;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @param cuentaCertif
	 * @param descripcion
	 */
	public BeanCertificadosCount(Long cuentaCertif, String descripcion) {
		super();
		this.cuentaCertif = cuentaCertif;
		this.descripcion = descripcion;		
	}

	
}
