package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.SmbUtil;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.ReporteDxP;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResTimbradoCFDIDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.richfaces.component.html.HtmlProgressBar;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import WSTimbrado.EnviaTimbrado;

/**
 * Class: BeanListaCertificado.java
 * Description: 
 * 
 * @author Z014058 Modificacion. Ing. Issac Bautista
 * 
 * Changes:
 * 1.1    21/10/2019 Towa (Juan Jose Flores) - Reacomodo de codigo para mayor entendimiento.
 * 1.1.2  23/10/2019 Towa (Juan Jose Flores) - Se agrega metodo consultaDxP()
 * 1.1.3  28/10/2019 Towa (Juan Jose Flores) - Se agrega metodo exportarReporteDxP()
 * 1.1.4  04/11/2019 Towa (Juan Jose Flores) - Se agrega metodo exportarReporteRecibos()
 * 1.1.5  04/11/2109 Towa (Juan jose Flores) - Se modific metodos validaFecha() y validaObjetoFechas()
 * 													* Se agrega validacion de fechainicial no sea mayor a la final.
 * 
 * 1.1.6  12/08/2020 Ing. Issac Bautista Se Eliminaron acentos que tenia la clase.
 * 1.1.7  21/10/2020 Ing. Issac Bautista Se modifico flujo para reportes programados. 
 */
@Controller
@Scope("session")
// =====================================================================================================================
public class BeanListaCertificado {

	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioRecibo servicioRecibo;
	@Resource
	private ServicioProcesos servicioProceso;
	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioParametros servicioParametros;

	private Log log = LogFactory.getLog(this.getClass());
	private List<BeanCertificado> listaCertificados;
	private List<BeanCertificado> listaCertificadosFec;
	private List<BeanFacturacion> listaPrefact;
	private List<BeanFacturacion> listaRecibos;
	private List<Object> listaTotalCertificados;
	private List<Object> resultado;
	private StringBuilder message;
	private List<Object> resumenPreFact;

	private List<BeanCertificado> listaCerti;
	private List<BeanCertificado> listaCertiRenovar;

	private short canal;
	private Short ramo;
	private Short ramoFac;
	private Long poliza;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String idCertificado;
	private String respuesta;
	private int numPagina;
	private Date fecini;
	private Date fecfin;
	private Date feccan;
	private Date inFechaIni;
	private Date inFechaFin;
	private Date dateFechaReporteDxp;

	private String strError;
	private String empresa;
	private String contratoEnlace;
	private String folioEndoso;
	private String muestraPE;
	private String DisplayPE;

	private Long poliza1;
	private Long poliza2;
	private Integer causa;

	private int rreporte;
	private String rprima;
	private String rstatus;
	private Short rmes;

	private final String rutaReportes;

	private String causaAnulacion;

	private boolean boolFiltrarFacturadoVigente;
	private boolean boolFiltrarCancelado;
	private boolean boolFiltrarEmitidoTodos;

	private Integer inIdVenta;
	
	private ProgressBar pb;
	private ExecutorService pool;

	private Integer inFiltroFechas;
	private Integer mes;
	private List<Object> listaMes;
	private Integer anio;
	private List<Object> listaComboAnios;

	//Bandera que nos indica si se programa reporte
	private boolean blnProgramaRep;
	
	//														//Para generar reporte DxP
	private ArrayList<ReporteDxP> reporteDxP;
	private String strVigencia;

	// -----------------------------------------------------------------------------------------------------------------
	/* GETTERs AND SETTERs */
	// -----------------------------------------------------------------------------------------------------------------
	
	/**
	 * Fecha para la consulta del reporte Dxp
	 */
	public Date getDateFechaReporteDxp() {
		if (
				this.dateFechaReporteDxp != null
		) {
			return (Date) this.dateFechaReporteDxp.clone();
		}
		return null;
	}

	
	public void setDateFechaReporteDxp(Date dateFechaReporteDxp) {
		if (
				dateFechaReporteDxp  != null
		) {
			this.dateFechaReporteDxp = (Date) dateFechaReporteDxp.clone();
		} else {
			this.dateFechaReporteDxp = null;	
		}
	}
	
	// -----------------------------------------------------------------------------------------------------------------
	public List<BeanCertificado> getListaCerti() {
		return listaCerti;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setListaCerti(List<BeanCertificado> listaCerti) {
		this.listaCerti = listaCerti;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public List<BeanCertificado> getListaCertiRenovar() {
		return listaCertiRenovar;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setListaCertiRenovar(List<BeanCertificado> listaCertiRenovar) {
		this.listaCertiRenovar = listaCertiRenovar;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Short getRamoFac() {
		return ramoFac;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setRamoFac(Short selectedRamoFac) {
		this.ramoFac = selectedRamoFac;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Short getRmes() {
		return rmes;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setRmes(Short rmes) {
		this.rmes = rmes;
	}

	// FIN Para pantalla de facturacion/renovacion 5758
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(List<Object> resultado) {
		this.resultado = resultado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the resultado
	 */
	public List<Object> getResultado() {
		return resultado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getStrError() {
		return strError;
	}
	
	/**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.respuesta = mensaje;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return respuesta;
    }

	// -----------------------------------------------------------------------------------------------------------------
	public void setStrError(String strError) {
		this.strError = strError;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public List<Object> getResumenPreFact() {
		return resumenPreFact;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setResumenPreFact(List<Object> resumenPreFact) {
		this.resumenPreFact = resumenPreFact;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Date getFecini() {
		return fecini;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setFecini(Date fecini) {
		this.fecini = fecini;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Date getFecfin() {
		return fecfin;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setFecfin(Date fecfin) {
		this.fecfin = fecfin;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Date getFeccan() {
		return feccan;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setFeccan(Date feccan) {
		this.feccan = feccan;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Date getInFechaIni() {
		return inFechaIni;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setInFechaIni(Date inFechaIni) {
		this.inFechaIni = inFechaIni;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Date getInFechaFin() {
		return inFechaFin;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setInFechaFin(Date inFechaFin) {
		this.inFechaFin = inFechaFin;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the servicioCertificado
	 */
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param servicioCertificado the servicioCertificado to set
	 */
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the servicioProceso
	 */
	public ServicioProcesos getServicioProceso() {
		return servicioProceso;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param servicioProceso the servicioProceso to set
	 */
	public void setServicioProceso(ServicioProcesos servicioProceso) {
		this.servicioProceso = servicioProceso;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public ServicioRecibo getServicioRecibo() {
		return servicioRecibo;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setServicioRecibo(ServicioRecibo servicioRecibo) {
		this.servicioRecibo = servicioRecibo;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the listaCertificados
	 */
	public List<BeanCertificado> getListaCertificados() {
		return listaCertificados;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param listaCertificados the listaCertificados to set
	 */
	public void setListaCertificados(List<BeanCertificado> listaCertificados) {
		this.listaCertificados = listaCertificados;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the listaPrefact
	 */
	public List<BeanFacturacion> getListaPrefact() {
		return listaPrefact;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param listaPrefact the listaPrefact to set
	 */
	public void setListaPrefact(List<BeanFacturacion> listaPrefact) {
		this.listaPrefact = listaPrefact;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public List<BeanFacturacion> getListaRecibos() {
		return listaRecibos;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setListaRecibos(List<BeanFacturacion> listaRecibos) {
		this.listaRecibos = listaRecibos;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public int getListaRecibosSize() {
		return this.listaRecibos.size();
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the canal
	 */
	public short getCanal() {
		return canal;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param canal the canal to set
	 */
	public void setCanal(short canal) {
		this.canal = canal;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the selectedRamo
	 */
	public Short getRamo() {
		return ramo;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param selectedRamo the selectedRamo to set
	 */
	public void setRamo(Short selectedRamo) {
		this.ramo = selectedRamo;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the poliza
	 */
	public Long getPoliza() {
		return poliza;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the nombre
	 */
	// -----------------------------------------------------------------------------------------------------------------
	public Long getPoliza1() {
		return poliza1;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setPoliza1(Long poliza1) {
		this.poliza1 = poliza1;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Long getPoliza2() {
		return poliza2;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setPoliza2(Long poliza2) {
		this.poliza2 = poliza2;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Integer getCausa() {
		return causa;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setCausa(Integer causa) {
		this.causa = causa;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getNombre() {
		return nombre;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the idCertificado
	 */
	public String getIdCertificado() {
		return idCertificado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param idCertificado the idCertificado to set
	 */
	public void setIdCertificado(String idCertificado) {
		this.idCertificado = idCertificado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setRprima(String rprima) {
		this.rprima = rprima;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getRprima() {
		return rprima;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setRreporte(int rreporte) {
		this.rreporte = rreporte;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public int getRreporte() {
		return rreporte;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setRstatus(String rstatus) {
		this.rstatus = rstatus;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getRstatus() {
		return rstatus;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param listaCertificadosFec the listaCertificadosFec to set
	 */
	public List<BeanCertificado> getListaCertificadosFec() {
		return listaCertificadosFec;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setListaCertificadosFec(List<BeanCertificado> listaCertificadosFec) {
		this.listaCertificadosFec = listaCertificadosFec;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param listaTotalCertificados the listaTotalCertificados to set
	 */
	public void setListaTotalCertificados(List<Object> listaTotalCertificados) {
		this.listaTotalCertificados = listaTotalCertificados;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the listaTotalCertificados
	 */
	public List<Object> getListaTotalCertificados() {
		return listaTotalCertificados;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Integer getInIdVenta() {
		return inIdVenta;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getEmpresa() {
		return empresa;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getContratoEnlace() {
		return contratoEnlace;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setContratoEnlace(String contratoEnlace) {
		this.contratoEnlace = contratoEnlace;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getFolioEndoso() {
		return folioEndoso;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setFolioEndoso(String folioEndoso) {
		this.folioEndoso = folioEndoso;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public boolean isBoolFiltrarFacturadoVigente() {
		return boolFiltrarFacturadoVigente;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setBoolFiltrarFacturadoVigente(boolean boolFiltrarFacturadoVigente) {
		this.boolFiltrarFacturadoVigente = boolFiltrarFacturadoVigente;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public boolean isBoolFiltrarCancelado() {
		return boolFiltrarCancelado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setBoolFiltrarCancelado(boolean boolFiltrarCancelado) {
		this.boolFiltrarCancelado = boolFiltrarCancelado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public boolean isBoolFiltrarEmitidoTodos() {
		return boolFiltrarEmitidoTodos;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setBoolFiltrarEmitidoTodos(boolean boolFiltrarEmitidoTodos) {
		this.boolFiltrarEmitidoTodos = boolFiltrarEmitidoTodos;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the intBarra
	 */
	public Integer getProgressValue() {
		return pb.getProgressValue();
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressValue(Integer progressValue) {
		this.pb.setProgressValue(progressValue);
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the intBarra
	 */
	public Boolean getProgressHabilitar() {
		return pb.isProgressHabilitar();
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressHabilitar(Boolean progressHabilitar) {
		this.pb.setProgressHabilitar(progressHabilitar);
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @return the progressBar
	 */
	public HtmlProgressBar getProgressBar() {
		return pb.getProgressBar();
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @param progressBar the progressBar to set
	 */
	public void setProgressBar(HtmlProgressBar progressBar) {
		this.pb.setProgressBar(progressBar);
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setMuestraPE(String muestraPE) {
		this.muestraPE = muestraPE;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getMuestraPE() {
		return muestraPE;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setDisplayPE(String displayPE) {
		DisplayPE = displayPE;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getDisplayPE() {
		return DisplayPE;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setCausaAnulacion(String causaAnulacion) {
		this.causaAnulacion = causaAnulacion;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String getCausaAnulacion() {
		return causaAnulacion;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Integer getInFiltroFechas() {
		return inFiltroFechas;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setInFiltroFechas(Integer inFiltroFechas) {
		this.inFiltroFechas = inFiltroFechas;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setMes(Integer mes) {
		this.mes = mes;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Integer getMes() {
		return mes;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public List<Object> getListaMes() {
		listaMes.clear();
		listaMes.addAll(GestorFechas.getMeses());
		return new ArrayList<Object>(listaMes);
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setListaMes(List<Object> listaMes) {
		this.listaMes = new ArrayList<Object>(listaMes);
	}

	// -----------------------------------------------------------------------------------------------------------------
	public Integer getAnio() {
		return anio;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public List<Object> getListaComboAnios() {
		try {
			listaComboAnios.clear();
			listaComboAnios.addAll(GestorFechas.getAnios(3, 3));
		} catch (Exception e) {
			this.log.error("Error: getListaComboAnios", e);
		}
		return new ArrayList<Object>(listaComboAnios);
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void setListaComboAnios(List<Object> listaComboAnios) {
		this.listaComboAnios = new ArrayList<Object>(listaComboAnios);
	}
	// -----------------------------------------------------------------------------------------------------------------
	public ArrayList<ReporteDxP> getReporteDxP() {
		if (
				this.reporteDxP != null
		) {
			return new ArrayList<>(this.reporteDxP);
		}
		return null;
	}
	// -----------------------------------------------------------------------------------------------------------------
	public void setReporteDxP(ArrayList<ReporteDxP> reporteDxP) {
		if (
				reporteDxP != null
		) {
			this.reporteDxP = new ArrayList<>();
			this.reporteDxP.clear();
			this.reporteDxP.addAll(reporteDxP);
		} else {
			this.reporteDxP = null;
		}
			
	}
	// -----------------------------------------------------------------------------------------------------------------
	public String getStrVigencia() {
		return strVigencia;
	}
	// -----------------------------------------------------------------------------------------------------------------
	public void setStrVigencia(String strVigencia) {
		this.strVigencia = strVigencia;
	}
	// -----------------------------------------------------------------------------------------------------------------
	/* CONSTRUCTOR */
	// -----------------------------------------------------------------------------------------------------------------
	public BeanListaCertificado() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.listaCertificados = new ArrayList<BeanCertificado>();
		this.listaCertificadosFec = new ArrayList<BeanCertificado>();
		this.listaTotalCertificados = new ArrayList<Object>();
		this.listaPrefact = new ArrayList<BeanFacturacion>();
		this.listaRecibos = new ArrayList<BeanFacturacion>();
		this.resumenPreFact = new ArrayList<Object>();
		this.listaCerti = new ArrayList<BeanCertificado>();
		this.listaCertiRenovar = new ArrayList<BeanCertificado>();
		this.canal = 1;
		this.inIdVenta = 0;
		this.rreporte = 1;
		this.rutaReportes = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		this.pool = Executors.newFixedThreadPool(1);
		this.strError = "";
		this.respuesta = Constantes.DEFAULT_STRING;
		this.listaMes = new ArrayList<Object>();
		this.listaComboAnios = new ArrayList<Object>();
		//this.reporteDxP = new ArrayList<>();
		//this.listaRecibos = new ArrayList<>();

		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_CERTIFICADO);

	}

	// -----------------------------------------------------------------------------------------------------------------
	/* METODOS */
	// -----------------------------------------------------------------------------------------------------------------
	public String consulta(){
		
		StringBuilder filtro = new StringBuilder();
		
		try {
			
			filtro.append(" and C.id.coceCasuCdSucursal = ").append(this.canal).append(" \n");
			
			if(this.ramo != null && this.ramo.intValue() > 0){
				filtro.append(" and C.id.coceCarpCdRamo = ").append(this.ramo).append(" \n");
			}
			
			if(this.poliza != null && this.poliza.intValue() > 0){
				filtro.append(" and C.id.coceCapoNuPoliza = ").append(this.poliza).append(" \n");
			}
			
			filtro.append(" and C.id.coceNuCertificado = ").append(1).append(" \n");
			
			if(this.idCertificado != null && this.idCertificado.length() > 0){
				filtro.append(" and C.clienteCertificados.id.coccIdCertificado = '").append(this.idCertificado).append("' \n");;
			}
			
			if(this.nombre != null && this.nombre.length() > 0){
				filtro.append(" and C.clienteCertificados.cliente.cocnNombre like '%").append(this.nombre).append("%' \n");
			}
			
			if(this.apellidoPaterno != null && this.apellidoPaterno.length() >0){
				filtro.append(" and C.clienteCertificados.cliente.cocnApellidoPat like '%").append(this.apellidoPaterno).append("%' \n");
			}
			
			if(this.apellidoMaterno != null && this.apellidoMaterno.length() > 0){
				filtro.append(" and C.clienteCertificados.cliente.cocnApellidoMat like '%").append(this.apellidoMaterno).append("%' \n");
			}
			
			List<Certificado> lista = this.servicioCertificado.obtenerObjetos(filtro.toString());
			
			for(Certificado certificado: lista){
			
				BeanCertificado bean = (BeanCertificado) ConstruirObjeto.crearBean(BeanCertificado.class, certificado);
				bean.setServicioCertificado(this.servicioCertificado);
				
				this.listaCertificados.add(bean);
			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_CERTIFICADO);
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		return apellidoMaterno;
	}

	// -----------------------------------------------------------------------------------------------------------------
public String totalCertificados(){
		
		Integer totalCertificados = 0;
		List<Object> lista = new ArrayList<Object>();
		this.listaTotalCertificados.clear();
		try {
			
			if(this.ramo != null && this.ramo.intValue() > 0 && this.poliza != null && this.poliza.intValue() > 0){
			
				this.respuesta = "";
				lista = this.servicioCertificado.totalCertificados(canal, ramo, poliza);
				
				for(int i = 0; i< lista.size(); i ++){
					
					Object[] obj = (Object[]) lista.get(i);
					totalCertificados = totalCertificados + Integer.parseInt(obj[0].toString());
					
					this.listaTotalCertificados.add(lista.get(i));
				}
				
				Object[]  total = {totalCertificados,"Total"};
				
				this.listaTotalCertificados.add(total);
			}
			else{
				
				this.respuesta = "Necesita seleccionar un ramo y/o una poliza para realizar la consulta.";
			}
			
			
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_CERTIFICADO);
			
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede realizar la consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
public String consultaPreFactura(){
	this.listaCertificados.clear();
	this.listaPrefact.clear();
	this.listaRecibos.clear();
	boolean bandera = true;
	
	try {
		this.message = new StringBuilder();
		respuesta = Constantes.DEFAULT_STRING;
		log.info(canal +  ramo + poliza + inIdVenta);
		
		if(ramo == 0) {
			respuesta = "Favor de seleccionar un ramo.";
			bandera = false;
		} else if(ramo == 57 || ramo == 58) {
			respuesta = "Estos ramos se facturan en la siguiente seccion: PROCESOS - VIDA EMPRESA - FACTURACION Y RENOVACION.";
			bandera = false;
		}
		
		if(bandera) {
			if(poliza == -1) {
				respuesta = "Favor de seleccionar una Poliza.";
				bandera = false;
			} else if(poliza == Constantes.DEFAULT_LONG) {
				if(inIdVenta == Constantes.DEFAULT_INT) {
					respuesta = "Favor de seleccionar un Id Venta.";
					bandera = false;
				}
			}
		}
		
		if(bandera) {
			List<Object[]> lista= this.servicioCertificado.getPreFacCertificado(canal, ramo, poliza, inIdVenta);
			
			for(Object[] certificado: lista){
				BeanCertificado bean = new BeanCertificado();
				bean.getId().setCoceCasuCdSucursal(((BigDecimal)certificado[0]).shortValue());
				bean.getId().setCoceCarpCdRamo(((BigDecimal)certificado[1]).shortValue());
				bean.getId().setCoceCapoNuPoliza(((BigDecimal)certificado[2]).longValue());
				bean.getId().setCoceNuCertificado(0L);
				bean.setCoceMtPrimaSubsecuente((BigDecimal)certificado[3]);
				bean.setCoceCampon3(((BigDecimal)certificado[4]).longValue());
				bean.setServicioCertificado(this.servicioCertificado);

				this.listaCertificados.add(bean);
			}	
			
			respuesta = "Se realizo consulta con exito.";
		}
		
		return null;
	} catch (Exception e) {
		this.message.append("Error al traer montos de facturacion.");
		this.log.error(message.toString(), e);
		throw new FacesException(message.toString(), e);
	}
}
	// -----------------------------------------------------------------------------------------------------------------
	public String consultaPreFacturas(){
		this.listaCertificados.clear();
		this.listaPrefact.clear();
		this.listaRecibos.clear();
		boolean bandera = true;
		
		try {
			this.message = new StringBuilder();
			respuesta = Constantes.DEFAULT_STRING;
			log.info(canal +  ramo + poliza + inIdVenta);
			
			if(ramo == 0) {
				respuesta = "Favor de seleccionar un ramo.";
				bandera = false;
			} else if(ramo == 57 || ramo == 58) {
				respuesta = "Estos ramos se facturan en la siguiente seccion: PROCESOS - VIDA EMPRESA - FACTURACION Y RENOVACION.";
				bandera = false;
			}
			
			if(bandera) {
				if(poliza == -1) {
					respuesta = "Favor de seleccionar una Poliza.";
					bandera = false;
				} else if(poliza == Constantes.DEFAULT_LONG) {
					if(inIdVenta == Constantes.DEFAULT_INT) {
						respuesta = "Favor de seleccionar un Id Venta.";
						bandera = false;
					}
				}
			}
			
			if(bandera) {
				List<Object[]> lista= this.servicioProceso.prefacturas(canal, ramo, poliza, inIdVenta);
				
				for(Object[] factura: lista){
					BeanFacturacion beanf = new BeanFacturacion();
					beanf.getId().setCofaCasuCdSucursal(((BigDecimal)factura[0]).shortValue());
					beanf.getId().setCofaCarpCdRamo(((BigDecimal)factura[1]).byteValue());
					beanf.getId().setCofaCapoNuPoliza(((BigDecimal)factura[2]).longValue());
					beanf.getId().setCofaNuCertificado(((BigDecimal)factura[3]).longValue());
					beanf.setCofaMtRecibo((BigDecimal)factura[4]);
					beanf.setCofaTotalCertificados((BigDecimal)factura[5]);
					beanf.setCofaMtTotSua((BigDecimal)factura[6]);
					beanf.setCofaMtTotPma((BigDecimal)factura[7]);
					beanf.setCofaMtTotIva((BigDecimal)factura[8]);
					beanf.setCofaMtTotDpo((BigDecimal)factura[9]);
					beanf.setCofaMtTotRfi((BigDecimal)factura[10]);
					beanf.setCofaMtTotCom((BigDecimal)factura[11]);
					beanf.setCofaMtTotCom1((BigDecimal)factura[12]);
					beanf.setCofaCampov1(factura[13].toString());
					beanf.setCofaNuReciboFiscal(((BigDecimal)factura[14]).doubleValue());
					beanf.setServicioProceso(this.servicioProceso);
					
					this.listaPrefact.add(beanf);
				}	
				
				respuesta = "Se realizo consulta con exito.";
			}
			
			this.listaCertificados.clear();
			return null;
		} catch (Exception e) {
			this.message.append("Error al traer montos de facturacion.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void prefacturah() {
		
		this.log.debug("Iniciando proceso de prefacturacion (Hilo)");
		
		this.pb.setProgressHabilitar(true);
		this.setProgressValue(100);
		
		Runnable hilo = new HiloProcesos(this,BeanNames.BEAN_PREFACTURA);
		pool.execute(hilo);
	}
	
	public void reportesh() {
		
		this.log.debug("Iniciando proceso de prefacturacion (Hilo)");
		
		this.pb.setProgressHabilitar(true);
		this.setProgressValue(100);
		
		Runnable hilo = new HiloProcesos(this,BeanNames.BEAN_REPORTE_MENSUAL);
		pool.execute(hilo);
	}

	/**
	 * Metodo para prefactirar polizas seleccionadas
	 * @return mensajes para cada poliza procesada
	 * @throws Excepciones con error en general
	 */
	public String upchk () throws Excepciones {
		int nNumRecibo = Constantes.DEFAULT_INT;
		this.listaPrefact.clear();

		try {
			respuesta = Constantes.DEFAULT_STRING;
			for (Object item: listaCertificados){
				BeanCertificado certificado = (BeanCertificado) item;				
				if (certificado.getChkUpdate()==true) {				
					log.info("Actualiza Status Facturacion para Prefactura --- Bean");
					this.servicioCertificado.actualizaPreFacPoliza(certificado.getId());
					if(getPoliza() == Constantes.DEFAULT_LONG) {
						nNumRecibo = Constantes.DEFAULT_INT;
					} else {
						log.info("Busca el ultimo recibo generado --- Bean");
						nNumRecibo = this.servicioCertificado.getUltimoRecibo(certificado.getId(), certificado.getCoceMtPrimaSubsecuente());
					}
					
					certificado.setCoceCampov8(prefactura(certificado.getId(), nNumRecibo));
				}
			}
			
			log.info("Entro a Modifica Prefactura --- Bean");
			List<Object[]> lista1= this.servicioProceso.modificaPrefactura(listaCertificados);
			
			for(Object[] factura: lista1){
				BeanFacturacion beanf = new BeanFacturacion();
				beanf.getId().setCofaCasuCdSucursal(((BigDecimal)factura[0]).shortValue());
				beanf.getId().setCofaCarpCdRamo(((BigDecimal)factura[1]).byteValue());
				beanf.getId().setCofaCapoNuPoliza(((BigDecimal)factura[2]).longValue());
				beanf.getId().setCofaNuCertificado(((BigDecimal)factura[3]).longValue());
				beanf.setCofaMtRecibo((BigDecimal)factura[4]);
				beanf.setCofaTotalCertificados((BigDecimal)factura[5]);
				beanf.setCofaMtTotSua((BigDecimal)factura[6]);
				beanf.setCofaMtTotPma((BigDecimal)factura[7]);
				beanf.setCofaMtTotIva((BigDecimal)factura[8]);
				beanf.setCofaMtTotDpo((BigDecimal)factura[9]);
				beanf.setCofaMtTotRfi((BigDecimal)factura[10]);
				beanf.setCofaMtTotCom((BigDecimal)factura[11]);
				beanf.setCofaMtTotCom1((BigDecimal)factura[12]);
				beanf.setCofaCampov1(factura[13].toString());
				beanf.setCofaNuReciboFiscal(((BigDecimal)factura[14]).doubleValue());
				beanf.setServicioProceso(this.servicioProceso);
				
				this.listaPrefact.add(beanf);
			}				
			
			respuesta = "Se ejecuto el proceso, favor de revisar resultados.";

			return null;
		} catch (Exception e) {
			this.message.append("Error al Traer los certificados.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

	/**
	 * Metodo que manda facturar una poliza
	 * @param id datos de poliza
	 * @param nNumRecibo ultimo recibo
	 * @return mensaje de prefacturacion
	 * @throws Exception con error en general
	 */
	private String prefactura(CertificadoId id, int nNumRecibo) throws Exception{		
		String strResultado = null;
		StringBuilder sbRespuesta = null;
		Map<String, Object> mapResp;
		
		try {
			sbRespuesta = new StringBuilder();
			if(nNumRecibo >= Constantes.DEFAULT_INT) {
				log.info("Actualizar Recibos en 1 a estatus 7 --- Bean");				
				this.servicioCertificado.actualizaEstatusRecibo(id);
				
				log.info("Manda a llamar proceso de Prefactura --- Bean");
				mapResp = this.servicioProceso.prefactura (id, nNumRecibo);
				
				sbRespuesta.append( mapResp.get("p_mensaje").toString());
				
				log.info("Sali del proceso de Prefactura --- Bean");
			} else {
				sbRespuesta.append(id.getCoceCasuCdSucursal());
				sbRespuesta.append("-");
				sbRespuesta.append(id.getCoceCarpCdRamo());
				sbRespuesta.append("-");
				sbRespuesta.append(id.getCoceCapoNuPoliza());
				sbRespuesta.append(": Esperar al proceso de ajuste por que la prima a facturar es igual al del mes pasado. \n");
			}
			
			strResultado = sbRespuesta.toString();
		} catch (Exception e) {			
			log.error("Error: prefactura()");
			strResultado = Constantes.DEFAULT_STRING;
		}
		
		return strResultado;
	}
	
	public String guardamontosnuevosprefactura() throws Excepciones{
		this.listaRecibos.clear();
		log.info("Entro a guardamontosnuevosprefactura --- BeanlistaCertificado");
		this.servicioProceso.guardaMontosNuevosPrefactura(listaPrefact);
		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que genera un prerecibo.
	 * 
	 * @throws Excepciones
	 */
	public void imprime_PreRecibo() throws Excepciones{
		this.listaRecibos.clear();
		try {
			log.info("Entro a Imprime PreRecibo --- BeanListaCertificado");
			for (Object item1: listaPrefact){
				BeanFacturacion facturas = (BeanFacturacion) item1;			
				if (facturas.getChkUpdate()==true) {				
					log.info("Entre al proceso de imprimeRecibo");
					String rutaTemporal = this.rutaReportes;
					nombre = "PreRecibo_"+ facturas.getId().getCofaCapoNuPoliza()+"_"+facturas.getId().getCofaNuReciboCol();//+id.getCareNuRecibo().toString();
					this.servicioRecibo.impresionPreRecibo(rutaTemporal, 
												nombre, 
												facturas.getId().getCofaCasuCdSucursal(), 
												facturas.getId().getCofaCarpCdRamo(), 
												facturas.getId().getCofaCapoNuPoliza(), 
												facturas.getCofaCampov1());	
					nombre += ".pdf";
					rutaTemporal += nombre;
					
					descargaArchivo(nombre, rutaTemporal, "application/pdf", null);
					FacesContext.getCurrentInstance().responseComplete();		
				}
			}
		} catch (Exception e) {
			log.error("Error al enviar reporte de imprime_PreRecibo",e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		} 
	}
	
	public String factura() throws Excepciones{
		this.listaRecibos.clear();
		log.info("Entro a Facturacion --- Bean");
		int num=1;
		int suc=0;
		int ramo=0;
		String polizas="";
		//this.servicioProceso.facturacion(listaPrefact);
		for (BeanFacturacion facturas: listaPrefact){
			//BeanFacturacion facturas = (BeanFacturacion) item1;			
			//System.out.println(facturas.getChkUpdate());	
			if (facturas.getChkUpdate()==true) {				
				int ves=1;
				this.servicioProceso.actRecFiscal(facturas.getId().getCofaCasuCdSucursal(), facturas.getId().getCofaCarpCdRamo(), facturas.getId().getCofaCapoNuPoliza(), facturas.getCofaCampov1(), ves);
				ves++;
				facturas.getId().setCofaNuReciboCol(Long.parseLong(facturas.getCofaCampov1()));
				this.servicioProceso.factura(facturas.getId());
				this.servicioProceso.actRecFiscal(facturas.getId().getCofaCasuCdSucursal(), facturas.getId().getCofaCarpCdRamo(), facturas.getId().getCofaCapoNuPoliza(), facturas.getCofaCampov1(), ves);
				log.info("Sali del proceso de Prefactura --- Bean");
				if (num==1)
					polizas=polizas+facturas.getId().getCofaCapoNuPoliza();
				else
					polizas=polizas + "," + facturas.getId().getCofaCapoNuPoliza();
				num=num+1;
				suc=facturas.getId().getCofaCasuCdSucursal();
				ramo=facturas.getId().getCofaCarpCdRamo();
			}
		}
		
		List<Object[]> lista2= this.servicioProceso.recuperaRecibos(suc, ramo, polizas);
		
		for(Object[] factura: lista2){
			BeanFacturacion beanr = new BeanFacturacion();
			beanr.getId().setCofaCasuCdSucursal(((BigDecimal)factura[0]).shortValue());
			beanr.getId().setCofaCarpCdRamo(((BigDecimal)factura[1]).byteValue());
			beanr.getId().setCofaCapoNuPoliza(((BigDecimal)factura[2]).longValue());
			beanr.setCofaNuReciboFiscal(((BigDecimal)factura[3]).doubleValue());
			beanr.setCofaCampov3("Listo para Sellar");
			beanr.setServicioProceso(this.servicioProceso);
			
			this.listaRecibos.add(beanr);
		}	
		log.info(listaRecibos.toString());
		this.listaCertificados.clear();
		this.listaPrefact.clear();
		return null;
	}

	public String consultaCertificados(){
		this.listaCertificadosFec.clear();
		String fecini=null;
		String fecfin=null;
				
		try {
			Format formatter;
			// The day
			formatter = new SimpleDateFormat("dd");    // 09
			// The month
			formatter = new SimpleDateFormat("MM");    // 01
			// The year
			formatter = new SimpleDateFormat("yyyy");  // 2002
			// Some examples
			formatter = new SimpleDateFormat("dd/MM/yyyy");

			if(inFechaIni!=null){
				fecini = formatter.format(inFechaIni);
			}
			if(inFechaFin!=null){
				fecfin = formatter.format(inFechaFin);
			}
			log.info("Canal " + canal +  " Ramo " + ramo + " Poliza " + poliza + " Fecha Inicio " + fecini + " Fecha Fin " + fecfin + " Tipo Prima " + rprima + " Tipo Reporte " + rreporte + " Status " + rstatus);
			List<Object[]> lista3= this.servicioCertificado.consultaCertificados(canal, ramo, poliza, rreporte + "", rprima, rstatus, fecini, fecfin);
			//System.out.println(lista3);

			for(Object[] certificado: lista3){

				BeanCertificado bean = new BeanCertificado();
				bean.getId().setCoceCapoNuPoliza(((BigDecimal)certificado[1]).longValue());
				bean.getEstatus().setAlesDescripcion(certificado[2].toString());
				bean.setCoceCampov1(certificado[3].toString());
				//bean.setCoceStCertificado(new Short(certificado[2].toString()));
				//System.out.println((BigDecimal)certificado[2]);				
				bean.getId().setCoceNuCertificado(((BigDecimal)certificado[4]).longValue());
				bean.setCoceMtSumaAsegurada((BigDecimal)certificado[5]);
				bean.setCoceMtPrimaAnual((BigDecimal)certificado[6]);
				bean.setCoceMtPrimaReal((BigDecimal)certificado[7]);
				bean.setCoceMtPrimaPura((BigDecimal)certificado[8]);
				bean.setServicioCertificado(this.servicioCertificado);

				this.listaCertificadosFec.add(bean);
			}	
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al recuperar certificados para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que genera reporte Mensual Tecnico
	 * 
	 * @return
	 */
	public String consultaMensualTecnico() {
		this.listaCertificados.clear();
		File file = null;

		String fecha1 = null;
		String fecha2 = null;
		BigDecimal cencos = null;
		String cred = null;
		String cred1 = null;
		String idcuota = null;
		String statcol = null;
		String stat = null;
		String statmov = null;
		String appat = null;
		String apmat = null;
		String nombre = null;
		String sexo = null;
		Date fecnac = null;
		String buc = null;
		String rfc = null;
		Date fecdes = null;
		Date fechas = null;
		String sumaaseg = null;
		BigDecimal derechos = null;
		BigDecimal recargos = null;
		BigDecimal iva = null;
		BigDecimal pmatot = null;
		BigDecimal tarifa = null;
		BigDecimal pmavid = null;
		BigDecimal pmades = null;
		BigDecimal cdedo = null;
		String edo = null;
		BigDecimal cdmun = null;
		String mun = null;
		String codpos = null;
		BigDecimal primaDev = null;
		BigDecimal primaDev2 = null;
		String porComTec = null;
		String porComCont = null;
		String porComCome = null;
		BigDecimal impComTec = null;
		BigDecimal impComCont = null;
		BigDecimal impComCome = null;

		Format formatter;
		BufferedWriter output = null;
		String ruta;

		try {
			formatter = new SimpleDateFormat("dd/MM/yyyy");
			if (fecini != null) {
				fecha1 = formatter.format(fecini);
			}
			if (fecfin != null) {
				fecha2 = formatter.format(fecfin);
			}

			log.info("Consulta Mensual Tecnico");
			List<Object[]> lista = this.servicioCertificado.consultaMensualTecnico(canal, ramo, poliza1, poliza2,
					fecha1, fecha2);
			int a = 0;

			ruta = this.rutaReportes;
			file = new File(ruta + "ReporteMensualTecnico.txt");
			try {
				output = new BufferedWriter(new PrintWriter(file));
				log.info("Se imprimio el archivo " + a);

				output.write(
						"RAMO,CENTRO DE COSTOS,POLIZA,RECIBO,CREDITO,CERTIFICADO,ID DE CUOTA,ESTATUS COLECTIVOS,ESTATUS GENERAL,ESTATUS MOV,CUENTA,PRODUCTO,SUBPRODUCTO,APELLIDO PATERNO,APELLIDO MATERNO,NOMBRE"
								+ "   ,SEXO,FECHA NACIMIENTO,BUC,RFC,PLAZO,FECHA INICIO POLIZA,FECHA FIN POLIZA,FECHA INGRESO,FECHA DESDE,FECHA HASTA,FECHA INICIO CREDITO,FECHA FIN CREDITO,FECHA FIN CREDITO 2,FECHA ANULACION"
								+ "   ,FECHA CANCELACION,SUMA ASEGURADA,BASE CALCULO,PRIMA NETA,DERECHOS,RECARGOS,IVA,PRIMA TOTAL,TARIFA,PRIMA VIDA,PRIMA DESEMPLEO,CD ESTADO,ESTADO,CD MUNICIPIO,MUNICIPIO,CP,MONTO DEVOLUCION CALCULADO,MONTO DEVOLUCION REPORTADO,CREDITO 2"
								+ "   ,% COMISION TECNICA,% COMISION COMERCIAL,% COMISION CONTINGENTE ");
				output.newLine();

				for (Object[] certificado : lista) {
					StringBuffer sb = new StringBuffer();
					BeanCertificado bean = new BeanCertificado();
					bean.getId().setCoceCarpCdRamo(((BigDecimal) certificado[0]).shortValue());
					cencos = (BigDecimal) certificado[1]; // variable local para manejo de variables
					bean.getId().setCoceCapoNuPoliza(((BigDecimal) certificado[2]).longValue());
					bean.setCoceNoRecibo((BigDecimal) certificado[3]);
					cred = (String) certificado[4]; // variable local para manejo de variables
					bean.getId().setCoceNuCertificado(((BigDecimal) certificado[5]).longValue());
					idcuota = (String) certificado[6]; // variable local para manejo de variables
					statcol = (String) certificado[7];
					stat = (String) certificado[8]; // variable local para manejo de variables
					statmov = ((String) certificado[9].toString()); // variable local para manejo de variables
					bean.setCoceNuCuenta((String) certificado[10]);
					bean.setCoceTpProductoBco((String) certificado[11]);
					bean.setCoceTpSubprodBco((String) certificado[12]);
					appat = (String) certificado[13]; // variable local para manejo de variables
					apmat = (String) certificado[14]; // variable local para manejo de variables
					nombre = (String) certificado[15]; // variable local para manejo de variables
					sexo = (String) certificado[16]; // variable local para manejo de variables
					fecnac = (Date) certificado[17]; // variable local para manejo de variables
					buc = (String) certificado[18]; // variable local para manejo de variables
					rfc = (String) certificado[19]; // variable local para manejo de variables
					bean.setCoceCdPlazo(((BigDecimal) certificado[20]).shortValue());
					bean.setCoceFeDesde((Date) certificado[21]);
					bean.setCoceFeHasta((Date) certificado[22]);
					bean.setCoceFeSuscripcion((Date) certificado[23]);
					fecdes = (Date) certificado[24]; // variable local para manejo de variables
					fechas = (Date) certificado[25]; // variable local para manejo de variables
					bean.setCoceFeIniCredito((Date) certificado[26]);
					bean.setCoceFeFinCredito((Date) certificado[27]);
					bean.setCoceCampov6((String) certificado[28]);
					bean.setCoceFeAnulacion((Date) certificado[29]);
					bean.setCoceFeAnulacionCol((Date) certificado[30]);
					sumaaseg = (String) certificado[31];
					bean.setCoceMtSumaAsegSi((BigDecimal) certificado[32]);
					bean.setCoceMtPrimaPura((BigDecimal) certificado[33]);
					derechos = (BigDecimal) certificado[34]; // variable local para manejo de variables
					recargos = (BigDecimal) certificado[35]; // variable local para manejo de variables
					iva = (BigDecimal) certificado[36]; // variable local para manejo de variables
					// bean.setCoceMtPrimaSubsecuente((BigDecimal)certificado[36]);
					pmatot = (BigDecimal) certificado[37]; // variable local para manejo de variables
					tarifa = (BigDecimal) certificado[38]; // variable local para manejo de variables
					pmavid = (BigDecimal) certificado[39]; // variable local para manejo de variables
					pmades = (BigDecimal) certificado[40]; // variable local para manejo de variables
					cdedo = (BigDecimal) certificado[41]; // variable local para manejo de variables
					edo = (String) certificado[42]; // variable local para manejo de variables
					cdmun = (BigDecimal) certificado[43]; // variable local para manejo de variables
					mun = (String) certificado[44]; // variable local para manejo de variables
					codpos = (String) certificado[45]; // variable local para manejo de variables
					primaDev2 = (BigDecimal) certificado[46];
					primaDev = (BigDecimal) certificado[47];
					cred1 = (String) certificado[48];
					//
					porComTec = (String) certificado[49];
					porComCont = (String) certificado[50];
					porComCome = (String) certificado[51];
					impComTec = (BigDecimal) certificado[52];
					impComCont = (BigDecimal) certificado[53];
					impComCome = (BigDecimal) certificado[54];

					bean.setServicioCertificado(this.servicioCertificado);
					this.listaCertificados.add(bean);

					sb.append(this.listaCertificados.get(a).getId().getCoceCarpCdRamo() + ",");
					sb.append(cencos + ",");
					sb.append(this.listaCertificados.get(a).getId().getCoceCapoNuPoliza() + ",");
					sb.append(this.listaCertificados.get(a).getCoceNoRecibo() + ",");
					sb.append(cred + ",");
					sb.append(this.listaCertificados.get(a).getId().getCoceNuCertificado() + ",");
					sb.append(idcuota + ",");
					sb.append(statcol + ",");
					sb.append(stat + ",");
					sb.append(statmov + ",");
					sb.append(this.listaCertificados.get(a).getCoceNuCuenta() + ",");
					sb.append(this.listaCertificados.get(a).getCoceTpProductoBco() + ",");
					sb.append(this.listaCertificados.get(a).getCoceTpSubprodBco() + ",");
					sb.append(appat + ",");
					sb.append(apmat + ",");
					sb.append(nombre + ",");
					sb.append(sexo + ",");
					sb.append(new SimpleDateFormat("dd/MM/yyyy").format(fecnac) + ",");
					sb.append(buc + ",");
					sb.append(rfc + ",");
					sb.append(this.listaCertificados.get(a).getCoceCdPlazo() + ",");
					sb.append(new SimpleDateFormat("dd/MM/yyyy").format(this.listaCertificados.get(a).getCoceFeDesde())
							+ ",");
					sb.append(new SimpleDateFormat("dd/MM/yyyy").format(this.listaCertificados.get(a).getCoceFeHasta())
							+ ",");
					sb.append(validaFecha(this.listaCertificados.get(a).getCoceFeSuscripcion()));
					sb.append(new SimpleDateFormat("dd/MM/yyyy").format(fecdes) + ",");
					sb.append(new SimpleDateFormat("dd/MM/yyyy").format(fechas) + ",");
					sb.append(validaFecha(this.listaCertificados.get(a).getCoceFeIniCredito()));
					sb.append(validaFecha(this.listaCertificados.get(a).getCoceFeFinCredito()));
					sb.append(this.listaCertificados.get(a).getCoceCampov6() + ",");
					sb.append(validaFecha(this.listaCertificados.get(a).getCoceFeAnulacion()));
					sb.append(validaFecha(this.listaCertificados.get(a).getCoceFeAnulacionCol()));
					sb.append(sumaaseg + ",");
					sb.append(this.listaCertificados.get(a).getCoceMtSumaAsegSi() + ",");
					sb.append(this.listaCertificados.get(a).getCoceMtPrimaPura() + ",");
					sb.append(derechos + ",");
					sb.append(recargos + ",");
					sb.append(iva + ",");
					sb.append(pmatot + ",");
					sb.append(tarifa + ",");
					sb.append(pmavid + ",");
					sb.append(pmades + ",");
					sb.append(cdedo + ",");
					sb.append(edo + ",");
					sb.append(cdmun + ",");
					sb.append(mun + ",");
					sb.append(codpos + ",");
					sb.append(primaDev2 + ",");
					sb.append(primaDev + ",");
					sb.append(cred1 + ",");
					sb.append(porComTec + ",");
					sb.append(porComCont + ",");
					sb.append(porComCome + ",");
					sb.append(impComTec + ",");
					sb.append(impComCont + ",");
					sb.append(impComCome);
					output.write(sb.toString());
					output.newLine();
					a = a + 1;
				}
			} finally {
				output.close();
			}

			log.info("El archivo se ha creado correctamente");
			this.message.append("Archivo creado correctamente");

			descargaArchivo("ReporteMensualTecnico.txt", ruta, "application/octet-stream", file);

			this.log.debug(this.message.toString());
			this.respuesta = this.message.toString();
			Utilerias.resetMessage(message);

		} catch (Exception e) {
			this.message.append("Error al recuperar Certificados(ConsultaMensualTecnico).");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}

		return null;

	}

	// -----------------------------------------------------------------------------------------------------------------
	private String validaFecha(Date objFecha) {
		String strResultado = Constantes.DEFAULT_STRING;

		if (objFecha == null) {
			strResultado = ",";
		} else {
			strResultado = GestorFechas.formatDate(objFecha, "dd/MM/yyyy") + ",";
		}

		return strResultado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String cancelaPoliza() {
		String fecha1 = null;
		int operacion = 2002;
		int subOperacion = 0;
		String fContable = "0";
		int certif = 1;
		String nombreFac = "COLECTIVOSWEB.CANCELAPOLIZA";
		Date fDesde = null, fHasta = null;
		int ultRecibo = 0;
		int mtUltRecibo = 0;
		ArrayList<Object> arlCertificados;
		PreCarga objPrecarga;

		try {
			arlCertificados = new ArrayList<Object>();
			fecha1 = GestorFechas.formatDate(new Date(), "dd/MM/yyyy");
			fContable = fecha1;

			List<Object[]> lista = this.servicioCertificado.consultaPolizaCancela(canal, ramo, poliza1);
			for (Object[] certificado : lista) {
				BeanCertificado bc = new BeanCertificado();
				bc.getId().setCoceCasuCdSucursal(((BigDecimal) certificado[0]).shortValue());
				bc.getId().setCoceCarpCdRamo(((BigDecimal) certificado[1]).shortValue());
				bc.getId().setCoceCapoNuPoliza(((BigDecimal) certificado[2]).longValue());
				bc.getId().setCoceNuCertificado(((BigDecimal) certificado[3]).longValue());
				if (((BigDecimal) certificado[3]).longValue() == 0) {
					fDesde = (Date) certificado[4];
					fHasta = (Date) certificado[5];
					this.servicioCertificado.cancelaCert(bc.getId(), fecha1, causa);
				} else {
					objPrecarga = new PreCarga();
					objPrecarga.setId(new PreCargaId(canal, ramo, poliza1, certificado[6].toString(), "CAN", "0"));
					objPrecarga.setCopcFeStatus(fecha1);
					objPrecarga.setCopcFeIngreso(GestorFechas.formatDate((Date) certificado[7], "dd/MM/yyyy"));
					objPrecarga.setCopcDato11("CANCELACION POLIZA");
					objPrecarga.setCopcDato25(causa.toString());
					objPrecarga.setCopcCargada("9");
					arlCertificados.add(objPrecarga);
				}
			}

			this.servicioCarga.guardarObjetos(arlCertificados);
			this.servicioProceso.cancelacionMasiva(canal, ramo, poliza1, causa, Colectivos.OPC_CANACEL.CANCELA);

			this.servicioProceso.contabilidadColectivos(canal, ramo, poliza1, certif, ultRecibo, mtUltRecibo, operacion,
					subOperacion, GestorFechas.formatDate(fDesde, "dd/MM/yyyy"),
					GestorFechas.formatDate(fHasta, "dd/MM/yyyy"), fContable, nombreFac, 3);

			this.message
					.append("El proceso de cancelacion para la poliza " + poliza1 + ", finaliza satisfactoriamente");
			this.log.info(message.toString());
			this.respuesta = this.message.toString();

			return null;

		} catch (Exception e) {
			this.message.append("Error al recuperar Certificados(ConsultaMensualTecnico).");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}

	}
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que genera el reporte de Desempleo
	 * 
	 * @return
	 * @throws Excepciones
	 */
	public String generarReporteDesempleo() throws Excepciones {
		Format formatter;
		String fecha1 = null;
		String fecha2 = null;
		String reporte, archivo;

		try {
			this.listaCertificados.clear();
			formatter = new SimpleDateFormat("dd/MM/yyyy");
			if (fecini != null) {
				fecha1 = formatter.format(fecini);
			}
			if (fecfin != null) {
				fecha2 = formatter.format(fecfin);
			}

			log.info("Consulta Mensual Emision");
			log.info("Generando Reporte");
			log.info("Consulta Mensual Emision Desempleo");

			reporte = FacesUtils.getServletRequest().getRealPath("/WEB-INF/reportes/") + File.separator;
			archivo = this.servicioCertificado.consultaEmisionDesempleo(fecha1, fecha2);
			reporte += archivo;
			log.info("Ruta del reporte... " + reporte);

			descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);

			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		} catch (Exception e) {
			log.error("Error al enviar la ReporteDesempleo", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}

		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que genera el reporte de cancelacion
	 */
	public String generarReporteCancelacion() throws Excepciones {
		Format formatter;
		String fecha1 = null;
		String fecha2 = null;
		String causaAnulacion = null;
		String reporte, archivo;

		try {
			this.listaCertificados.clear();
			formatter = new SimpleDateFormat("dd/MM/yyyy");
			if (fecini != null) {
				fecha1 = formatter.format(fecini);
			}
			if (fecfin != null) {
				fecha2 = formatter.format(fecfin);
			}
			causaAnulacion = this.getCausaAnulacion();

			log.info("Consulta Reporte Cancelacion");
			log.info("Generando Reporte");
			log.info("Consulta Reporte Cancelacion");

			reporte = FacesUtils.getServletRequest().getRealPath("/WEB-INF/reportes/") + File.separator;
			archivo = this.servicioCertificado.consultaReporteCancelacion(fecha1, fecha2, causaAnulacion);
			reporte += archivo;
			log.info("Ruta del reporte... " + reporte);

			descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);

			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		} catch (Exception e) {
			log.error("Error al enviar la ReporteCancelacion", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}

		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public List<Object> resumenPreFactura() throws Excepciones {
		this.canal = 1;
		if (poliza1 == null) {
			poliza1 = 0L;
		}

		if (poliza == 0) {
			poliza1 = 0L;
		}

		resumenPreFact = this.servicioCertificado.resumenPreFactura(canal, ramo, poliza);

		try {

		} catch (Exception e) {
			log.error("Error al enviar la resumen PreFactura", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}

		FacesContext.getCurrentInstance().responseComplete();
		log.info(Constantes.REPORTE_MSG);
		FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		return resumenPreFact;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void reportes() throws Excepciones {

		switch (
				rreporte
		) {
		
		case 1:
			log.info("Consulta Inteligencia Comercial");
			this.ConsultaCertif();
			break;
		case 2:
			log.info("Consulta Mensual Emision");
			this.consultaMensualEmision();
			break;
		case 3:
			log.info("Reporte Obligados");
			this.reporteObligados();
			break;
		case 4:
			log.info("Reporte Devoluciones");
			this.reporteDevoluciones();
			break;
		case 5:
			log.info("Reporte Detalle Prefactura");
			this.reporteDetallePrefactura();
			break;
		case 6:
			log.info("Reporte Vigor");
			this.reporteVigor();
			break;
		case 7:
			log.info("Reporte Endosos");
			this.reporteEndosos(this.verificaRecibos());
			break;
		case 8:
			log.info("Reporte Endosos Conciliados");
			this.reporteEndososConciliados(this.verificaRecibos());
			break;
		case 9:
			log.info("Reporte Recibos");
			this.exportarReporteRecibos();
			break;
		case 10:
			log.info("Reporte Deudor por Prima (DxP)");
			this.exportarReporteDxP(1);
			break;
		case 11:
			log.info("Reporte Deudor por Prima (DxP) carga inicial");
			this.exportarReporteDxP(0);
			break;
		default:
			break;
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 1.- Metodo que genera reporte de Certificados
	 * 
	 * @return void
	 * @throws Excepciones
	 */
	public String ConsultaCertif()  throws Excepciones{
		this.listaCertificados.clear();
		String fecha1 = null;
		String fecha2 = null;
		
		try {
			if(poliza1 == null) {
				poliza1 = 0L;
			}
			if(poliza == 0) {
				poliza1 = 0L;
			} else {
				inIdVenta = 0;
			}
		
			log.info(canal + "----" +  ramo + "-----" + poliza );
         
            if(fecini != null) {
            	fecha1 = GestorFechas.formatDate(fecini, "dd/MM/yyyy");
            }
            if(fecfin != null) {
            	fecha2 = GestorFechas.formatDate(fecfin, "dd/MM/yyyy");
            }

            log.info("Generando Reporte");
            log.info("Consulta Inteligencia Comercial");
            log.info("Consulta Inteligencia Comercial");

            String reporte = this.rutaReportes;
			String archivo= this.servicioCertificado.totalCertif(canal, ramo, poliza, poliza1, fecha1, fecha2, inIdVenta, this.rutaReportes);
			reporte += archivo;

			descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);
			
			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		} catch (Exception e) {
			log.error("Error al enviar la ConsultaCertif",e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		} 
            
		return null;
	}
	
	/**
	 * Metodo que genera reporte de Mensual Emision
	 * @throws Excepciones
	 */
	private void consultaMensualEmision() throws Excepciones {
		//Se valida se selecciono una fecha valida
		if (!this.validarFechas()) {
			return;
		}

		try {
			//Se limpia lista de certificados
			this.listaCertificados.clear();
			
			poliza1 = (Long)Utilerias.objectIsNull(poliza1, Constantes.TIPO_DATO_LONG);

			if (poliza == 0) {
				poliza1 = 0L;
			} else {
				inIdVenta = 0;
			}

			if(boolFiltrarFacturadoVigente == false &&
					boolFiltrarCancelado == false &&
					boolFiltrarEmitidoTodos == false
					) {
				this.respuesta = ("Favor de seleccionar un status.");
				return;
			}
			
			
			log.info("Generando Reporte");

			String reporte = this.rutaReportes;
			if(blnProgramaRep) {
				reporte = Constantes.DEFAULT_STRING;
			}
			
			String archivo = this.servicioCertificado.consultaMensualEmision(canal, 
																	ramo, 
																	poliza, 
																	poliza1, 
																	GestorFechas.formatDate(fecini, Constantes.FORMATO_FECHA_UNO),
																	GestorFechas.formatDate(fecfin, Constantes.FORMATO_FECHA_UNO), 
																	inIdVenta, 
																	boolFiltrarFacturadoVigente, 
																	boolFiltrarCancelado, 
																	boolFiltrarEmitidoTodos, 
																	reporte);
			if(!blnProgramaRep) {
				//Se manda a descargar reporte
				descargaArchivo(archivo, reporte + archivo, Constantes.REPORTES_CONTENT_TYPE, null);
				
				//Se regresa mensaje
				setRespuesta("Se genero reporte con exito: " + archivo);
				
				//Completamos response
				FacesContext.getCurrentInstance().responseComplete();
			} else {
				//Se regresa mensaje
				setRespuesta("Reporte : " + archivo + ", se generara en ruta por proceso batch. Se notificara por correo la generacion del reporte.");
			}
			
			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
			
		} catch (Exception e) {
			log.error("Error al enviar la ConsultaMensualEmision", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}

	}
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 3.- Metodo que genera reporte de Obligados
	 * 
	 * @throws Excepciones
	 */
	private void reporteObligados() throws Excepciones {
		
		try {
			this.listaCertificados.clear();
			this.canal = 1;
			if (poliza1 == null) {
				poliza1 = 0L;
			}

			if (poliza == 0) {
				poliza1 = 0L;
				if (poliza == 0 && inIdVenta == 0)
					this.respuesta = ("Selecciona un canal de venta.");
			} else {
				inIdVenta = 0;
			}

			if ( //
					boolFiltrarFacturadoVigente || 
					boolFiltrarCancelado || 
					boolFiltrarEmitidoTodos 
			) {
				log.info("Generando Reporte");
				
				String reporte = this.rutaReportes;
				String archivo = this.servicioCertificado.generaObligados(canal, ramo, poliza, poliza1, inIdVenta, boolFiltrarFacturadoVigente, boolFiltrarCancelado,
						boolFiltrarEmitidoTodos, reporte);
				reporte += archivo;
				log.info("Ruta del reporte... " + reporte);

				descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);

				log.info(Constantes.REPORTE_MSG);
				FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
			}
		} catch (Exception e) {
			log.error("Error al enviar la ConsultaMensualEmision", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 4.- Metodo que genera reporte Devoluciones
	 * 
	 * @throws Excepciones
	 */
	private void reporteDevoluciones() throws Excepciones {
		
		if (
				!this.validarFechas()
		) {
			return;
		}

		
		try {
			this.listaCertificados.clear();
			this.canal = 1;
			if (poliza1 == null) {
				poliza1 = 0L;
			}

			if (poliza == 0) {
				poliza1 = 0L;
			} else {
				inIdVenta = 0;
			}

			String fecha1 = (new SimpleDateFormat("dd/MM/yyyy")).format(fecini);
			String fecha2 = (new SimpleDateFormat("dd/MM/yyyy")).format(fecfin);
			
			String reporte = this.rutaReportes;
			String archivo = this.servicioCertificado.generaDevoluciones(canal, ramo, poliza, poliza1, fecha1, fecha2,
					inIdVenta, reporte);
			reporte += archivo;
			log.info("Ruta del reporte... " + reporte);

			descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);

			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		} catch (Exception e) {
			log.error("Error al enviar la reporteDevoluciones", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}
	}
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 5.- Metodo que genera el reporte detalle Prefactura
	 * 
	 */
	private void reporteDetallePrefactura() throws Excepciones {
		
		try {
			this.listaCertificados.clear();
			this.canal = 1;

			if (poliza1 == null) {
				poliza1 = 0L;
			}

			if (poliza == 0) {
				poliza1 = 0L;
			}

			String reporte = this.rutaReportes;
			String archivo = this.servicioCertificado.generaDetallePreFactura((int) canal, (int) ramo,
					Integer.valueOf(poliza.toString()));
			reporte += archivo;
			log.info("Ruta del reporte... " + reporte);
			descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);

			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
			resumenPreFact = this.servicioCertificado.resumenPreFactura(canal, ramo, poliza);
		} catch (Exception e) {
			log.error("Error al enviar la ConsultaDetalle", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que manda guardar una peticion de reporte Vigor.
	 */
	private void reporteVigor() {
		String strNomArchivo;
		StringBuilder cadenacan = null;
		StringBuilder sbCamposFechas = null;
		StringBuilder sbQuery = new StringBuilder();
		String strQuery;
		
		try {
			//Inicializamos variables
			cadenacan = new StringBuilder();
			sbCamposFechas = new StringBuilder();
			
			//Obtenemos nombre de reporte
			strNomArchivo = "ReporteVigores_" + ramo + "-" + poliza + "-" + inIdVenta + ".csv";

			//Obtenemos valores segun tipo poliza
			if(poliza == 0) {
				//Es PU
				cadenacan = GeneratorQuerys.getCadenaCan(poliza, inIdVenta, Constantes.DEFAULT_INT);
				
				if(ramo == 9 && inIdVenta == 6) {								
					sbCamposFechas = GeneratorQuerys.camposFehasPU;
				} else {																		
					sbCamposFechas = GeneratorQuerys.camposFehasPR;
				}
			} else {
				//Es PR
				cadenacan = GeneratorQuerys.getCadenaCan(poliza, inIdVenta, 1);
				sbCamposFechas = GeneratorQuerys.camposFehasPR;
			}
			
			//Obtenemos Query
			sbQuery = GeneratorQuerys.repVigor;
			
			//Remplazamos valores
			strQuery = sbQuery.toString();
			strQuery = strQuery.replace("#camposFechas", sbCamposFechas.toString());
			strQuery = strQuery.replace("#canal", "1");
			strQuery = strQuery.replace("#ramo", ramo.toString());
			strQuery = strQuery.replace("#cadenacan", cadenacan.toString());
			
			//Insertamos peticion en Base de datos
			this.servicioCertificado.insertaBanderaBatch(Constantes.DEFAULT_LONG, 
								strQuery, 
								FacesUtils.getBeanSesion().getUserName(), 
								Constantes.DEFAULT_STRING, 
								strNomArchivo, 
								Constantes.DEFAULT_LONG);

			//Se envia respuesta a pantalla
			setRespuesta(" Reporte: " + strNomArchivo + ", se generara en ruta por proceso batch. Se notificara por correo la generacion del reporte.");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 7.- Metodo que manda guardar una peticion de reporte de Endosos Historicos.
	 * 
	 */
	private void reporteEndosos(Double numRecibo) {
		String strNomreArchivo = "ReporteEndosos";
		String rutaApp = Constantes.DEFAULT_STRING;
		Long next = Constantes.DEFAULT_LONG;
		String res = Constantes.DEFAULT_STRING;
				
		if ( //
				!this.validarFechas()
		) {
			return;
		}
		
		if ( //
				GestorFechas.restarFechas(fecini, fecfin) >= 93
		) {
			setRespuesta("Para este tipo de reporte solo se permite generar consultas maximo de 3 meses.");
			return;
		}

		try {
			rutaApp = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_REPORTES_APP) + File.separator;
			String strMensaje = "Reporte Endosos (Ramo-Poliza-IdVenta) : " + ramo + "-" + poliza + "-" + inIdVenta;
			String strNomArchivo = "Reporte Endosos_" + ramo + "-" + poliza + "-" + inIdVenta;

			next = this.servicioParametros.siguienteCdParam();
			strNomreArchivo = strNomreArchivo + "_" + next.toString() + ".txt";

			Parametros valoresBatch = this.servicioCertificado.valoresBatch(Constantes.RUTA_BATCH_ENDOSOS);
			Parametros objPeticion = new Parametros();
			objPeticion.setCopaCdParametro(next);
			objPeticion.setCopaDesParametro("PROCESO");
			objPeticion.setCopaIdParametro("BATCH");
			objPeticion.setCopaVvalor6(strNomArchivo);
			objPeticion.setCopaVvalor7(strNomreArchivo);
			objPeticion.setCopaVvalor8(strMensaje);
			objPeticion.setCopaNvalor7(3D);
			objPeticion.setCopaNvalor1(Constantes.DEFAULT_INT);
			objPeticion.setCopaFvalor2(fecini);
			objPeticion.setCopaFvalor3(fecfin);

			this.servicioCertificado.insertaBanderaBatch(objPeticion, numRecibo);
			res = this.servicioCertificado.queryEndosos(canal, ramo, poliza, inIdVenta,
					GestorFechas.formatDate(fecini, Constantes.FORMATO_FECHA_UNO),
					GestorFechas.formatDate(fecfin, Constantes.FORMATO_FECHA_UNO));
			setRespuesta(generaArchivo(res, rutaApp, strNomreArchivo, strNomArchivo, valoresBatch));
			
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 8.- Metodo que manda guardar una peticion de reporte de Endosos Conciliados.
	 * 
	 */
	private void reporteEndososConciliados(Double numRecibo) {
		String strNomreArchivo = "ReporteEndososConciliados";
		String rutaApp = Constantes.DEFAULT_STRING;
		Long next = Constantes.DEFAULT_LONG;
		String res = Constantes.DEFAULT_STRING;

		if ( //
				!this.validarFechas()
		) {
			return;
		}
		
		if ( //
				GestorFechas.restarFechas(fecini, fecfin) >= 93
		) {
			setRespuesta("Para este tipo de reporte solo se permite generar consultas maximo de 3 meses.");
			return;
		}
		
 		try {
			
			rutaApp = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_REPORTES_APP) + File.separator;
			String strNomArchivo = "Reporte_Endosos_Conciliados_" + ramo + "-" + poliza + "-" + inIdVenta;
			String strMensaje = "Reporte_Endosos_Conciliados (Ramo-Poliza-IdVenta) : " + ramo + "-" + poliza + "-"
					+ inIdVenta;

			next = this.servicioParametros.siguienteCdParam();
			strNomreArchivo = strNomreArchivo + "_" + next.toString() + ".txt";

			Parametros valoresBatch = this.servicioCertificado.valoresBatch(Constantes.RUTA_BATCH_ENDOSOS_CON);
			Parametros objPeticion = new Parametros();
			objPeticion.setCopaCdParametro(next);
			objPeticion.setCopaDesParametro("PROCESO");
			objPeticion.setCopaIdParametro("BATCH");
			objPeticion.setCopaVvalor6(strNomArchivo);
			objPeticion.setCopaVvalor7(strNomreArchivo);
			objPeticion.setCopaVvalor8(strMensaje);
			objPeticion.setCopaNvalor7(4D);
			objPeticion.setCopaNvalor1(Constantes.DEFAULT_INT);
			objPeticion.setCopaFvalor2(fecini);
			objPeticion.setCopaFvalor3(fecfin);

			this.servicioCertificado.insertaBanderaBatch(objPeticion, numRecibo);
			res = this.servicioCertificado.queryEndososConciliados(canal, ramo, poliza, inIdVenta,
					GestorFechas.formatDate(fecini, Constantes.FORMATO_FECHA_UNO),
					GestorFechas.formatDate(fecfin, Constantes.FORMATO_FECHA_UNO));
			setRespuesta(generaArchivo(res, rutaApp, strNomreArchivo, strNomArchivo, valoresBatch));
		
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @author TOWA (JJFM)
	 * 9.- Metodo que Exporta Reporte de Recibos.
	 * 
	 */
	private void exportarReporteRecibos()  {
		
		if ( //
				//
				!this.boolFiltrarFacturadoVigente &&
				!this.boolFiltrarCancelado
		) {
			setRespuesta("Debes seleccionar almenos un Estatus (Cobrado/Pendiente de Cobro)");
			return;
		}
		
		
		if ( //
				!validarFechas()
		) {
			return;
		}
		
		if ( 
				this.ramo == 61 &&
				this.poliza == 0 &&
				(this.inIdVenta == null ||
				this.inIdVenta <= 0)
				) {
			setRespuesta("Favor de seleccionar un Canal de Venta para el reporte");
			return;
		}

		
		try {	
			String fecha1 = (new SimpleDateFormat("dd/MM/yyyy")).format(fecini);
			String fecha2 =  (new SimpleDateFormat("dd/MM/yyyy")).format(fecfin);
			
			if (
					this.poliza1 == null
			) {
				this.poliza1 = 0L;
			}
			
			if (
					this.poliza2 == null
			) {
				this.poliza2 = 0L;
			}
				
				
			File file = this.servicioCertificado.exportarReporteRecibos(this.canal, this.ramo, this.poliza, fecha1,
					fecha2, this.inIdVenta, this.boolFiltrarFacturadoVigente, this.boolFiltrarCancelado, this.rutaReportes);
			
		
			descargaArchivo(file.getName(), file.getPath(), "text/csv", file);
			setRespuesta("Se genero el reporte con exito.");

			
		} catch (Excepciones e) {
			log.error(e.getMessage());
			setRespuesta("Ocurrio un error al generar el reporte, intente mas tarde.");
			
		} catch (Exception e) {
			log.error(e.getMessage());
			setRespuesta("Ocurrio un error al descargar el reporte, intente mas tarde.");
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 10.- Metodo que Exporta Reporte Deudor por prima (DxP).
	 * 
	 * 
	 */
	private void exportarReporteDxP(int filtro) throws Excepciones {
		
		if ( //
				this.poliza == null  
		) {
			setRespuesta("Favor de ingresar una poliza.");
			return;
		}
			
		
		if( 
				poliza == 0
		) {
			
			if(!this.validarFechas()) {
				return;
			}
			
			if ( 
					this.inIdVenta == null ||
					this.inIdVenta <= 0
					) {
				return;
				
			}
			
			String fecha1 = (new SimpleDateFormat("dd/MM/yyyy")).format(fecini);
			String fecha2 = (new SimpleDateFormat("dd/MM/yyyy")).format(fecfin);
			

			if (
					this.poliza1 == null
			) {
				this.poliza1 = 0L;
			}
			
			if (
					this.poliza2 == null
			) {
				this.poliza2 = 0L;
			}
				

			try {	
								
				
				File fileReport = this.servicioCertificado.exportarReporteDxP(this.canal, this.ramo, this.poliza, fecha1,
						fecha2, this.inIdVenta, true, true, 
						this.boolFiltrarEmitidoTodos, this.rutaReportes, this.reporteDxP.get(0).getarrDetalleDXP().get(this.reporteDxP.get(0).getarrDetalleDXP().size() - 1).getShortCuota(),
						filtro
						);
				
				if ( //
						fileReport != null
				) {
					descargaArchivo(fileReport.getName(), fileReport.getPath(), "text/csv", fileReport);
					setRespuesta("Se genero el reporte con exito.");
				} else {
					setRespuesta("No se pudo generar el reporte, intenta de nuevo mas tarde");
				} 
				
			
			} catch (Exception e) {
				log.error(e.getMessage());
				setRespuesta("No se pudo generar el reporte, intenta de nuevo mas tarde");
			}
			
		} else {
		
			if (
					this.dateFechaReporteDxp == null &&
					this.poliza != 0
			) {
				setRespuesta("Favor de seleccionar una fecha para el reporte");
				return;
			}
			
			try {	
				String fecha1 = (new SimpleDateFormat("dd/MM/yyyy")).format(dateFechaReporteDxp);
				
				if (
						this.poliza1 == null
				) {
					this.poliza1 = 0L;
				}
				
				if (
						this.poliza2 == null
				) {
					this.poliza2 = 0L;
				}
					
					
				File fileReport = this.servicioCertificado.exportarReporteDxP(this.canal, this.ramo, this.poliza, strVigencia,
						fecha1, this.inIdVenta, true, true, 
						this.boolFiltrarEmitidoTodos, this.rutaReportes, this.reporteDxP.get(0).getarrDetalleDXP().get(this.reporteDxP.get(0).getarrDetalleDXP().size() - 1).getShortCuota(), filtro);
				
				if ( //
						fileReport != null
				) {
					descargaArchivo(fileReport.getName(), fileReport.getPath(), "text/csv", fileReport);
					setRespuesta("Se genero el reporte con exito.");
				} else {
					setRespuesta("No se pudo generar el reporte, intenta de nuevo mas tarde");
				} 
				
			
			} catch (Exception e) {
				log.error(e.getMessage());
				setRespuesta("No se pudo generar el reporte, intenta de nuevo mas tarde");
			}
		}
	}
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	private boolean validarFechas() {
		
		try {
			//												//Se limpia respuesta para la siguiente validacion.
			setRespuesta(" ");
			
			if ( //
					//										//Se valida que este seleccionada alguna opcion del filtro
					//										//	 de fechas.
					getInFiltroFechas() == null ||
					getInFiltroFechas() == 0
			) {
				setRespuesta("Favor de seleccionar un filtro de fechas.");
				return false;
			} 
			
			if ( //
					//										//Validacion para el filtro fecha seleccionando opcion de
					//										//		<Por periodo contable>	
					getInFiltroFechas() == 1
			) {
				
				return validaObjetoFechas();
			} 
			
			if (
					//										//valildamos este seleccionada una fecha inicial al estar 
					//										//		seleccionada la opcion <Por fechas>
					getInFiltroFechas() == 2 &&
					fecini == null
			) { 
				setRespuesta("Favor de seleccionar fecha inicial.");
				return false;
			}
			
			if (
					//										//valildamos este seleccionada una fecha final al estar 
					//										//		seleccionada la opcion <Por fechas>
					getInFiltroFechas() == 2 &&
					fecfin == null
			) { 
				setRespuesta("Favor de seleccionar fecha final.");
				return false;
			}
			
			if (
					//										//valildamos que la fecha final no sea menor a la inicial 
					//										//		al estar seleccionada la opcion <Por fechas>
					fecfin.before(fecini)
			) { 
				setRespuesta("La fecha inicial debe ser antes de la fecha final");
				return false;
			}
			
			return true;
		} catch (Excepciones e) {
			setRespuesta("Error al validar fechas, intente nuevamente.");
			log.error(getRespuesta(), e);
			FacesUtils.addInfoMessage(getRespuesta());
			return false;
		}
		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	private boolean validaObjetoFechas() throws Excepciones {
		
		if ( //
				getMes() == null || 
				getAnio() == null
		) {
			setRespuesta("Favor de seleccionar mes o a�o de periodo contable.");
			return false;
		}
		
		
		Parametros objFechas = this.servicioCertificado.getFechasCierre(getMes(), getAnio());
		
		if ( //
				objFechas == null
		) {
			setRespuesta("No existen fechas para el periodo contable seleccionado.");
			return false;
		} 
		
		fecini = GestorFechas.generateDate(objFechas.getCopaVvalor3(), Constantes.FORMATO_FECHA_UNO);
		fecfin = GestorFechas.generateDate(objFechas.getCopaVvalor4(), Constantes.FORMATO_FECHA_UNO);
		return true;
		
	}
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que genera reporte de Facturacion 57-58
	 */
	public String reporteFacturados5758() throws Excepciones {
		String reporte, archivo;

		try {
			this.listaCertificados.clear();
			log.info("Reporte Facturacion 57 y 58");
			log.info("Generando Reporte");

			reporte = this.rutaReportes;
			archivo = this.servicioCertificado.reporteFacturados5758();
			reporte += archivo;
			System.out.println("Ruta del reporte... " + reporte);

			descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);

			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		} catch (Exception e) {
			log.error("Error al enviar el reporteFacturados5758", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}

		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que genera reporte de Errores Facturacion 57-58
	 */
	public String reporteErrores5758() throws Excepciones {
		String reporte, archivo;

		try {
			this.listaCertificados.clear();
			log.info("Reporte Errores 57 y 58");
			log.info("Generando Reporte");
			reporte = this.rutaReportes;
			archivo = this.servicioCertificado.reporteErrores5758();
			reporte += archivo;
			System.out.println("Ruta del reporte... " + reporte);

			descargaArchivo(archivo, reporte, Constantes.REPORTES_CONTENT_TYPE, null);

			this.servicioCertificado.borraErrores5758();
			log.info(Constantes.REPORTE_MSG);
			FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		} catch (Exception e) {
			log.error("Error al enviar la el reporteErrores5758", e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		}

		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que generar reporte de Endosos Updrade
	 */
	public void generarReporteEndosoUpgrade() {
		String ruta = "";
		String archivoEndoso = "";
		String archivoEndosoDetalle = "";
		List<String> archivos = null;
		String zip;
		File fileEndoso = null, fileEndosoDetalle = null;

		try {
			if ((fecini != null && fecfin != null)
					&& ((fecfin.getTime() - fecini.getTime()) / (24 * 60 * 60 * 1000)) <= 70) {
				ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
				empresa = empresa.equals("") ? null : empresa;
				contratoEnlace = contratoEnlace.equals("") ? null : contratoEnlace;

				archivoEndoso = servicioCertificado.generaReporteEndosoUpgrade(
						new SimpleDateFormat("dd/MM/yyyy").format(fecini),
						new SimpleDateFormat("dd/MM/yyyy").format(fecfin), empresa, contratoEnlace, folioEndoso);
				archivoEndosoDetalle = servicioCertificado.generaSoporteEndosoUpgrade(
						new SimpleDateFormat("dd/MM/yyyy").format(fecini),
						new SimpleDateFormat("dd/MM/yyyy").format(fecfin), empresa, contratoEnlace, folioEndoso);

				archivos = new ArrayList<String>();
				archivos.add(archivoEndoso);
				archivos.add(archivoEndosoDetalle);

				// Comprimir archivos y mandar al navegador
				zip = Utilerias.comprimirArchivos(archivos);
				fileEndoso = new File(ruta + archivoEndoso);
				fileEndosoDetalle = new File(ruta + archivoEndosoDetalle);
				descargaArchivo(zip, ruta + zip, "application/zip", null);
			} else {
				respuesta = "El periodo de fecha debe ser menor a 2 meses para generar los reportes.";
				FacesUtils.addInfoMessage("El periodo de fecha debe ser menor a 2 meses para generar los reportes.");
			}
		} catch (Exception e) {
			log.error("Error al generar el reporte de endoso upgrade.", e);
			FacesUtils.addInfoMessage("Error al generar el reporte de endoso upgrade.");
		} finally {
			if (fileEndoso != null) {
				deleteFile(fileEndoso);
			}

			if (fileEndosoDetalle != null) {
				deleteFile(fileEndosoDetalle);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
  private void descargaArchivo(String strNombreArchivo, String strRutaReporte, String strContentType, File objArchivo) throws Exception {
    	FacesContext fc = FacesContext.getCurrentInstance();
        
        
    	HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
    	response.reset();
		OutputStream out = null;
		InputStream in = null;
		int read = 0;
		byte[] bytes = new byte[2048];	
		
		try {	
			if(objArchivo == null) {
				objArchivo = new File(strRutaReporte);
			}
			System.out.println(objArchivo.toString());
			
			response = FacesUtils.getServletResponse();
			response.setContentType(strContentType);
			
			DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
	    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + strNombreArchivo);
			
			out = response.getOutputStream();
			
			in = new FileInputStream(objArchivo);
			while((read = in.read(bytes))!= -1){
				out.write(bytes, 0, read);
			}
			
			out.flush();
			out.close();
			deleteFile(objArchivo);

			FacesContext.getCurrentInstance().responseComplete();
		} finally {
			if(in != null) {
				in.close();				
			}
		}	
	}
    

	// -----------------------------------------------------------------------------------------------------------------
	public String sellarRecibos() throws Excepciones {
		Iterator<BeanFacturacion> it;
		BeanFacturacion objBeanFacturacion;
		EnviaTimbrado objTimbrado;
		List<BeanFacturacion> listaRecibosTMP;
		String ip;
		String usuario;

		try {

			ExternalContext context;
			context = FacesContext.getCurrentInstance().getExternalContext();

			HttpServletRequest request = (HttpServletRequest) context.getRequest();
			HttpSession sesion = request.getSession(true);

			it = listaRecibos.iterator();
			ip = request.getHeader("X-FORWARDED-FOR");
			if (ip == null || "".equals(ip)) {
				ip = request.getRemoteAddr();
			}

			if ("0:0:0:0:0:0:0:1".equalsIgnoreCase(ip)) {
				InetAddress inetAddress = InetAddress.getLocalHost();
				String ipAddress = inetAddress.getHostAddress();
				ip = ipAddress;
			}
			usuario = (String) sesion.getAttribute("username");
			while (it.hasNext()) {
				objBeanFacturacion = it.next();

				if (objBeanFacturacion.getChkUpdate()) {
					ReqTimbradoCFDIDTO req = new ReqTimbradoCFDIDTO(usuario, Constantes.CFDI_APLICACION, ip,
							String.valueOf(objBeanFacturacion.getId().getCofaCasuCdSucursal()),
							String.valueOf(objBeanFacturacion.getId().getCofaCarpCdRamo()),
							String.valueOf(objBeanFacturacion.getId().getCofaCapoNuPoliza()),
							String.valueOf(objBeanFacturacion.getId().getCofaNuCertificado()),
							String.format("%.1f", objBeanFacturacion.getCofaNuReciboFiscal()), true);

					ResTimbradoCFDIDTO res = this.servicioCertificado.sellarRecibosV33(req);

					if (Constantes.CFDI_TIMBRAR_EXISTOSO.equals(res.getMensaje())) {
						objBeanFacturacion.setCofaCampov3("Se realizo timbrado con exito");
					} else {
						objBeanFacturacion.setCofaCampov3(res.getMensaje() + " - " + res.getDescripcion());
					}

					/*
					 * objTimbrado =
					 * this.servicioCertificado.sellarRecibos(objBeanFacturacion.getId().
					 * getCofaCasuCdSucursal(), objBeanFacturacion.getId().getCofaCarpCdRamo(),
					 * objBeanFacturacion.getCofaNuReciboFiscal()); if(objTimbrado.getCFDI() !=
					 * null) { this.servicioCertificado.actualizarDatosCFDI(objTimbrado,
					 * objBeanFacturacion.getId().getCofaCasuCdSucursal(),
					 * objBeanFacturacion.getId().getCofaCarpCdRamo(),
					 * objBeanFacturacion.getCofaNuReciboFiscal());
					 * objBeanFacturacion.setCofaCampov3("Se realizo timbrado con exito"); } else {
					 * objBeanFacturacion.setCofaCampov3("Error: " + objTimbrado.getError()); }
					 */

				}
			}
		} catch (Exception e) {
			this.message.append("Error al sellar recibos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}

		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void sellarRecibosPrefactura() {
		Iterator<BeanFacturacion> it;
		BeanFacturacion objBeanFacturacion;
		EnviaTimbrado objTimbrado;
		List<BeanFacturacion> listaRecibosTMP;
		String ip;
		String usuario;

		try {

			ExternalContext context;
			context = FacesContext.getCurrentInstance().getExternalContext();

			HttpServletRequest request = (HttpServletRequest) context.getRequest();
			HttpSession sesion = request.getSession(true);

			it = listaPrefact.iterator();

			ip = request.getHeader("X-FORWARDED-FOR");
			if (ip == null || "".equals(ip)) {
				ip = request.getRemoteAddr();
			}

			if ("0:0:0:0:0:0:0:1".equalsIgnoreCase(ip)) {
				InetAddress inetAddress = InetAddress.getLocalHost();
				String ipAddress = inetAddress.getHostAddress();
				ip = ipAddress;
			}
			// ip = request.getRemoteAddr();
			usuario = (String) sesion.getAttribute("username");
			while (it.hasNext()) {
				objBeanFacturacion = it.next();

				if (objBeanFacturacion.getChkUpdate()) {
					ReqTimbradoCFDIDTO req = new ReqTimbradoCFDIDTO(usuario, Constantes.CFDI_APLICACION, ip,
							String.valueOf(objBeanFacturacion.getId().getCofaCasuCdSucursal()),
							String.valueOf(objBeanFacturacion.getId().getCofaCarpCdRamo()),
							String.valueOf(objBeanFacturacion.getId().getCofaCapoNuPoliza()),
							String.valueOf(objBeanFacturacion.getId().getCofaNuCertificado()),
							String.format("%.1f", objBeanFacturacion.getCofaNuReciboFiscal()), true);

					ResTimbradoCFDIDTO res = this.servicioCertificado.sellarRecibosV33(req);

					if (Constantes.CFDI_TIMBRAR_EXISTOSO.equals(res.getMensaje())) {
						objBeanFacturacion.setResTimbrado("Se realizo timbrado con exito");
					} else {
						objBeanFacturacion.setResTimbrado(res.getMensaje() + " - " + res.getDescripcion());
					}

					/*
					 * objTimbrado =
					 * this.servicioCertificado.sellarRecibos(objBeanFacturacion.getId().
					 * getCofaCasuCdSucursal(), objBeanFacturacion.getId().getCofaCarpCdRamo(),
					 * objBeanFacturacion.getCofaNuReciboFiscal()); if(objTimbrado.getCFDI() !=
					 * null) { this.servicioCertificado.actualizarDatosCFDI(objTimbrado,
					 * objBeanFacturacion.getId().getCofaCasuCdSucursal(),
					 * objBeanFacturacion.getId().getCofaCarpCdRamo(),
					 * objBeanFacturacion.getCofaNuReciboFiscal());
					 * objBeanFacturacion.setCofaCampov3("Se realizo timbrado con exito"); } else {
					 * objBeanFacturacion.setCofaCampov3("Error: " + objTimbrado.getError()); }
					 */

				}
			}
		} catch (Exception e) {
			this.message.append("Error al sellar recibos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
	// CArga Polizas
	public String cargaPolizas() throws Exception {
		BeanCertificado certi;
		this.listaCerti.clear();
		try {
			List<Object[]> polizas = servicioCertificado.cargaPolizas(ramoFac);
			for (Object[] cargar : polizas) {

				certi = new BeanCertificado();

				certi.getId().setCoceCasuCdSucursal(((BigDecimal) cargar[0]).shortValue());
				certi.getId().setCoceCarpCdRamo(((BigDecimal) cargar[1]).shortValue());
				certi.getId().setCoceCapoNuPoliza(((BigDecimal) cargar[2]).longValue());
				certi.setCoceCampon1(((BigDecimal) cargar[3]).longValue());
				certi.setCoceCampon2(((BigDecimal) cargar[4]).longValue());

				certi.setDesc1("Para Facturar");
				listaCerti.add(certi);

			}
		} catch (Exception e) {
			log.error("Error al Consultar las polizas. " + e.getMessage(), e);
			FacesUtils.addInfoMessage("Error al Consultar las polizas del ramo. " + ramo);
		}
		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String factura5758() throws Excepciones {
		BeanCertificado certificado;
		Iterator<BeanCertificado> it;
		log.info("Entro a factura " + ramoFac + " --- Bean");

		try {
			it = listaCerti.iterator();
			while (it.hasNext()) {
				certificado = it.next();
				if (certificado.getChkUpdate()) {
					this.servicioProceso.factura5758(certificado.getId());
					certificado.setDesc1("Poliza facturada con exito ");
				}
			}
		} catch (Exception e) {
			log.error("Error al facturar las polizas seleccionadas.: " + e.getMessage(), e);
			FacesUtils.addInfoMessage("Error al facturar las polizas seleccionadas. " + canal + ramoFac + poliza);
		}
		log.info(listaCerti.toString());
		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public String cargaPolizasRenovar() throws Exception {
		BeanCertificado certi;
		this.listaCertiRenovar.clear();
		String anio;
		List<Object[]> polizas;
		Integer nMesSis;

		try {
			setRespuesta(Constantes.DEFAULT_STRING);
			anio = GestorFechas.formatDate(new Date(), "yyyy");
			nMesSis = Integer.parseInt(GestorFechas.formatDate(new Date(), "MM"));

			if (Integer.parseInt(rmes.toString()) > nMesSis) {
				setRespuesta("No se pueden renovar polizas con mes en Fecha Hasta mayor al mes actual.");
			} else {
				polizas = servicioCertificado.cargaPolizasRenovar(ramo,
						Utilerias.agregarCaracteres(rmes.toString(), 1, '0', 1), anio);
				for (Object[] cargar : polizas) {
					certi = new BeanCertificado();
					certi.getId().setCoceCasuCdSucursal(((BigDecimal) cargar[0]).shortValue());
					certi.getId().setCoceCarpCdRamo(((BigDecimal) cargar[1]).shortValue());
					certi.getId().setCoceCapoNuPoliza(((BigDecimal) cargar[2]).longValue());
					certi.setCoceFeDesde((Date) cargar[3]);
					certi.setCoceFeHasta((Date) cargar[4]);
					certi.setCoceCampov6("Disponible para renovar");
					listaCertiRenovar.add(certi);
				}
				setRespuesta("Consulta realizada con exito: se encontraron " + polizas.size() + " polizas a renovar.");
			}
		} catch (Exception e) {
			log.error("Error al Consultar las polizas.", e);
			FacesUtils.addInfoMessage("Error al Consultar las polizas del ramo. " + ramo);
		}

		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------
	public void renovar5758() throws Excepciones {
		BeanCertificado certificado;
		Iterator<BeanCertificado> it;
		log.info("entro a renovar " + ramo + "---bean");
		String archivo = "";
		Archivo objArchivo;
		String anio;
		boolean bRecibos = false;

		try {
			setRespuesta(Constantes.DEFAULT_STRING);
			anio = GestorFechas.formatDate(new Date(), "yyyy");
			if (rmes != 0) {
				it = listaCertiRenovar.iterator();
				while (it.hasNext()) {
					certificado = it.next();
					if (certificado.getChkUpdate()) {
						bRecibos = this.servicioCertificado.recibosPendCobro(
								certificado.getId().getCoceCasuCdSucursal(), certificado.getId().getCoceCarpCdRamo(),
								certificado.getId().getCoceCapoNuPoliza());
						if (bRecibos) {
							certificado.setCoceCampov6("Poliza tiene recibos pendientes de cobro");
						} else {
							this.servicioCertificado.renovar5758(certificado.getId().getCoceCasuCdSucursal(),
									certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza());
							certificado.setCoceCampov6("Renovada");
						}
					}
				}

				String reporte = this.rutaReportes;
				archivo = this.servicioCertificado
						.generaReporte5758(Utilerias.agregarCaracteres(rmes.toString(), 1, '0', 1), anio);
				reporte += archivo;
				System.out.println("Ruta del reporte... " + reporte);

				objArchivo = new Archivo(this.rutaReportes, archivo, "");
				objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
				objArchivo.eliminarArchivo();

				FacesContext.getCurrentInstance().responseComplete();
			} else {
				setRespuesta("Debes seleccionar un mes para poder realizar la renovacion");
			}
		} catch (Exception e) {
			log.error("error al renovar las polizas seleccionadas.: " + e.getMessage());
			FacesUtils.addInfoMessage("error al renovar las polizas seleccionadas " + canal + ramo + poliza);
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	public void setValor() {
		setInIdVenta(inIdVenta);
	}
	// -----------------------------------------------------------------------------------------------------------------
	private String generaArchivo(String res, String rutaApp, String strNomreArchivo, String strNomArchivo, Parametros valoresBatch) throws Exception {
		String strRespuesta = Constantes.DEFAULT_STRING;
		SmbUtil smbUtil;
		String sambaRuta = Constantes.DEFAULT_STRING;
		BufferedWriter  output = null ;
		
		if (res != null) {		
			File file = new File (rutaApp + strNomreArchivo );
			sambaRuta = valoresBatch.getCopaRegistro();
			output = new BufferedWriter(new PrintWriter(file));  
			
			try {
				output.write(res);					
			} finally {
				output.close(); 						
			}
								
			smbUtil = new SmbUtil(valoresBatch.getCopaVvalor2(), valoresBatch.getCopaVvalor3());
			Archivo ar = new Archivo(file);
			ar.copiarSmbArchivo2(smbUtil.getFile(sambaRuta + strNomreArchivo));
			strRespuesta = "Reporte: " + strNomArchivo + ", se generara en ruta por proceso batch, se notificara por correo la ejecucion del reporte.";
		} else {
			strRespuesta = "Error al generar archivo en ruta.";
		}
		
		return strRespuesta;
	}

	// -----------------------------------------------------------------------------------------------------------------
	private String deleteFile(File objFile) {
		String strResultado = Constantes.DEFAULT_STRING;
		
		if (!objFile.delete()) {
			strResultado = "No se pudo eliminar el archivo";
		}

		return strResultado;
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que consulta recibos segun filtro seleccionado. Solo para reporte de
	 * endosos (conciliados e historico)
	 */
	public void recibos() throws Excepciones {
		
		if ( //
				!validarFechas()
		) {
			return;
		}
		
		if ( //
				GestorFechas.restarFechas(fecini, fecfin) >= 93
		) {
			setListaRecibos(null);
			setRespuesta("Solo se permite generar consulta de recibos maximo de 3 meses.");
		}
		
		try {		
			List<BeanFacturacion> listaRecibos = this.servicioCertificado.consultaRecibos(canal, ramo, poliza,
					GestorFechas.formatDate(fecini, Constantes.FORMATO_FECHA_UNO),
					GestorFechas.formatDate(fecfin, Constantes.FORMATO_FECHA_UNO));
			setListaRecibos(listaRecibos != null ? listaRecibos : new ArrayList<BeanFacturacion>());
			setRespuesta("Se realizo consulta con exito, favor de revisar recibos");
				
			
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new Excepciones(e.getMessage(), e);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que limpia la consulta de recibos. Solo para reporte de endosos
	 * (conciliados e historico)
	 */
	public void limpiaRecibos() throws Excepciones {
		try {
			if (getListaRecibos() != null) {
				getListaRecibos().clear();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new Excepciones(e.getMessage(), e);
		}
	}

	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Metodo que verifica si se selecciono un recibo.
	 */
	private Double verificaRecibos() {
		Double numRecibo = Constantes.DEFAULT_DOUBLE;

		if (getListaRecibos() != null) {
			for (BeanFacturacion objeto : getListaRecibos()) {
				if (objeto.isChk()) {
					numRecibo = objeto.getCofaCampon5().doubleValue();
					fecini = objeto.getCofaFeFacturacion();
					fecfin = objeto.getCofaFeFacturacion();
					break;
				}
			}
		}

		return numRecibo;
	}
	
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * @author: Towa (Juan Jose Flores)
	 * Metodo que valida si el ArregloDXP esta vacio o es nullo (DxP).
	 * @return boolean - true si el reporteDxP es null o vacio. 
	 */
	public boolean getIsArrDxPNullorEmpty() {
		return	this.reporteDxP == null || this.reporteDxP.isEmpty();
	}
	
	// ----------------------------------------------------------------------------------------------------------------
	/**
	 * @author: Towa (Juan Jose Flores)
	 * Metodo que consulta Reporte Deudor por prima (DxP).
	 * 
	 */
	public void consultaDxP() throws Excepciones {
		
		this.subCleanReports();	
		if ( //
				this.poliza == null  
		) {
			setRespuesta("Favor de ingresar una poliza.");
			return;
		}
		
		
		if (
				this.dateFechaReporteDxp == null
				&& this.poliza != 0
		) {
			setRespuesta("Favor de seleccionar una fecha para el reporte");
			return;
		}
		
		if (
				this.poliza == 0
		) {
			
			
			if(!this.validarFechas()) {
				return;
			}
			
			if ( 
					this.ramo == 61 &&
					(this.inIdVenta == null ||
					this.inIdVenta <= 0)
					) {
				setRespuesta("Favor de seleccionar un Canal de Venta para el reporte");
				return;
				
			}
			
			if (
					this.inIdVenta == null
					) {
				this.inIdVenta = 0;
			}
			
			
			String fecha1 = (new SimpleDateFormat("dd/MM/yyyy")).format(fecini);
			String fecha2 = (new SimpleDateFormat("dd/MM/yyyy")).format(fecfin);
		
			try {	
				ArrayList<ReporteDxP> reporteDxP = 
						this.servicioCertificado.consultaReporteDxP(Short.parseShort("1"), this.ramo, this.poliza,
								Short.parseShort("0"),fecha1, fecha2, Short.parseShort(""+inIdVenta) );
					
				this.setReporteDxP(reporteDxP);
				
				if (this.getReporteDxP() != null) {
					
					setRespuesta("Se realizo consulta con exito, favor de revisar resultados");
				} else {
					setRespuesta("No se Encontraron Resultados para consulta seleccionada.");
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				setRespuesta("Ocurrio un error al generar el reporte, intente mas tarde.");
			}
			
		} else {
			
			
			String strFechaBusqueda = (new SimpleDateFormat("dd/MM/yyyy")).format(dateFechaReporteDxp);
			
			try {	
				ArrayList<ReporteDxP> reporteDxP = 
						this.servicioCertificado.consultaReporteDxP(Short.parseShort("1"), this.ramo, this.poliza,
								Short.parseShort("0"), this.strVigencia, strFechaBusqueda, Short.parseShort("0"));
					
				
				
				if (reporteDxP != null) {
					this.setReporteDxP(reporteDxP);
					setRespuesta("Se realizo consulta con exito, favor de revisar resultados");
				} else {
					setRespuesta("No se Encontraron Resultados para consulta seleccionada.");
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				setRespuesta("Ocurrio un error al generar el reporte, intente mas tarde.");
			}
		}
	}
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * @author: Towa (Juan Jose Flores)
	 * Metodo de apoyo para limpiar registros de tablas DxP y Recibos.
	 * 
	 */
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	public void subCleanReports() {
		
		this.reporteDxP = null;
		this.setRespuesta(" ");
	}
	// -----------------------------------------------------------------------------------------------------------------
	
	/**
	 * @return the blnProgramaRep
	 */
	public boolean isBlnProgramaRep() {
		return blnProgramaRep;
	}
	/**
	 * @param blnProgramaRep the blnProgramaRep to set
	 */
	public void setBlnProgramaRep(boolean blnProgramaRep) {
		this.blnProgramaRep = blnProgramaRep;
	}
}
// =====================================================================================================================