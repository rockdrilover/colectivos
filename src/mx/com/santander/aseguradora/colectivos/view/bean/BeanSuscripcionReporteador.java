package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvWriter;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.view.dto.ColumnaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.FiltroSuscripcionDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SuscripcionDTO;

@Controller
@Scope("session")
public class BeanSuscripcionReporteador {
	@Resource
	private ServicioSuscripcion servicioSuscripcion;

	/**
	 * Resultados columnas
	 */
	private List<ColumnaDTO> listaColumnas = null;

	private List<ColumnaDTO> listaColumnasSeleccionadas = null;

	private String[] columnasSeleccionadas = new String[10];
	private String[] columnasSeleccionadasC2 = new String[10];
	private String[] columnasSeleccionadasC3 = new String[11];

	/**
	 * Logger
	 */
	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * Consulta de suscripciones
	 */
	private List<SuscripcionDTO> suscripciones;

	/**
	 * Objeto filtro de suscriiones
	 */
	private FiltroSuscripcionDTO filtroSuscripcionDTO;

	/**
	 * Progress bar
	 */
	private ProgressBar pb;

	/**
	 * Mapeo de campos
	 */
	private List<SelectItem> listaComboEstatus;
	private List<SelectItem> listaComboEstatusC2;
	private List<SelectItem> listaComboEstatusC3;

	private final String SEPARADOR = ",";

	public BeanSuscripcionReporteador() {
		listaColumnas = new ArrayList<ColumnaDTO>();
		suscripciones = new ArrayList<SuscripcionDTO>();
		inicializaPantalla();
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_SUSCRIPCION_REPORTEADOR);
		// columnasSeleccionadas =;
		listaColumnas.add(new ColumnaDTO(1, "cosuFolioRiesgos", "Folio Riesgos", false));
		listaColumnas.add(new ColumnaDTO(2, "cosuFolioCh", "Folio CH", false));
		listaColumnas.add(new ColumnaDTO(3, "cosuNombre", "Nombre", false));
		listaColumnas.add(new ColumnaDTO(4, "cosuParticipacion", "Participacion", false));
		listaColumnas.add(new ColumnaDTO(5, "cosuFechaRecAseg", "Fecha Recibida en Aseguradora", false));
		listaColumnas.add(new ColumnaDTO(6, "cosuPlaza", "Plaza", false));
		listaColumnas.add(new ColumnaDTO(7, "cosuSucEjec", "Suc. / Ejec.", false));
		listaColumnas.add(new ColumnaDTO(8, "cosuFechaNac", "Fecha Nacimiento", false));
		listaColumnas.add(new ColumnaDTO(9, "cosuMonto", "Monto", false));
		listaColumnas.add(new ColumnaDTO(10, "cosuFechaSolCred", "Fecha Sol. Credito", false));
		listaColumnas.add(new ColumnaDTO(11, "cosuLocal", "Local/Foraneo", false));
		listaColumnas.add(new ColumnaDTO(12, "cosuExamMedico", "Examen Medico", false));
		listaColumnas.add(new ColumnaDTO(12, "cosuFechaSolExam", "Fecha Solicitud Examen", false));
		listaColumnas.add(new ColumnaDTO(13, "cosuFechaCitExam", "Fecha Cita Examen", false));
		listaColumnas.add(new ColumnaDTO(14, "cosuMotExamMed", "Motivo Examen Medico", false));
		listaColumnas.add(new ColumnaDTO(15, "cosuFechaAplExam", "Fecha Aplicacion Examen", false));
		listaColumnas.add(new ColumnaDTO(16, "cosuFechaResult", "Fecha Resultados", false));
		listaColumnas.add(new ColumnaDTO(17, "cosuFechaResDoc", "Fecha Respuesta Doctor", false));
		listaColumnas.add(new ColumnaDTO(18, "cosuDictamen", "Dictamen", false));
		listaColumnas.add(new ColumnaDTO(19, "cosuFechaNotDic", "Fecha Notificación de Dictamen", false));
		listaColumnas.add(new ColumnaDTO(20, "cosuObsWf", "Observaciones WF", false));
		listaColumnas.add(new ColumnaDTO(21, "cosuTelefono1", "Telefono 1", false));
		listaColumnas.add(new ColumnaDTO(22, "cosuTelefono2", "Telefono 2", false));
		listaColumnas.add(new ColumnaDTO(23, "cosuCelular", "Celular", false));
		listaColumnas.add(new ColumnaDTO(24, "cosuEmail", "EMAIL", false));
		listaColumnas.add(new ColumnaDTO(25, "cosuObservaciones", "OBSERVACIONES", false));
		listaColumnas.add(new ColumnaDTO(26, "cosuNumInter", "NumInter", false));
		listaColumnas.add(new ColumnaDTO(27, "cosuFechaFirm", "Fecha Firma", false));
		listaColumnas.add(new ColumnaDTO(28, "cosuMontoFirmado", "Monto Firmado", false));
		listaColumnas.add(new ColumnaDTO(29, "cosuNumCredito", "Numero de Credito", false));
		listaColumnas.add(new ColumnaDTO(30, "cosuExtraFirma", "Extraprima", false));

		Collections.sort(listaColumnas);
		listaComboEstatus = new ArrayList<SelectItem>();
		listaComboEstatusC2 = new ArrayList<SelectItem>();

		listaComboEstatusC3 = new ArrayList<SelectItem>();

		int i = 0;
		ColumnaDTO c;
		for (i = 0; i < 10; i++) {
			c = listaColumnas.get(i);
			listaComboEstatus.add(new SelectItem(c.getNombreColumna(), c.getHeaderColumn()));
		}

		for (i = 10; i < 20; i++) {
			c = listaColumnas.get(i);
			listaComboEstatusC2.add(new SelectItem(c.getNombreColumna(), c.getHeaderColumn()));
		}

		for (i = 20; i < 31; i++) {
			c = listaColumnas.get(i);
			listaComboEstatusC3.add(new SelectItem(c.getNombreColumna(), c.getHeaderColumn()));
		}
	}

	public void inicializaPantalla() {
		filtroSuscripcionDTO = new FiltroSuscripcionDTO();

		listaColumnasSeleccionadas = new ArrayList<ColumnaDTO>();
		suscripciones = new ArrayList<SuscripcionDTO>();

		columnasSeleccionadas = new String[10];
		columnasSeleccionadasC2 = new String[10];
		columnasSeleccionadasC3 = new String[11];
	}

	/**
	 * Realiza la consulta principal de suscripciones
	 * 
	 * @throws Exception
	 */
	public void consultaSuscripciones() throws Exception {
		suscripciones = new ArrayList<SuscripcionDTO>();
		if (columnasSeleccionadas.length == 0 && columnasSeleccionadasC2.length == 0
				&& columnasSeleccionadasC3.length == 0) {
			FacesUtils.addInfoMessage("Debe seleccionar al menos una columna para reporte.");
			return;
		}

		try {
			listaColumnasSeleccionadas = new ArrayList<ColumnaDTO>();

			suscripciones = servicioSuscripcion.consultaSuscripciones(filtroSuscripcionDTO);
			if (suscripciones.size() > 0) {
				for (int i = 0; i < columnasSeleccionadas.length; i++) {

					for (ColumnaDTO c : listaColumnas) {
						if (columnasSeleccionadas[i] != null) {
							if (c.getNombreColumna().equals(columnasSeleccionadas[i])) {
								listaColumnasSeleccionadas.add(c);
							}
						}

					}

				}

				for (int i = 0; i < columnasSeleccionadasC2.length; i++) {

					for (ColumnaDTO c : listaColumnas) {
						if (columnasSeleccionadasC2[i] != null) {
							if (c.getNombreColumna().equals(columnasSeleccionadasC2[i])) {
								listaColumnasSeleccionadas.add(c);
							}
						}

					}

				}

				for (int i = 0; i < columnasSeleccionadasC3.length; i++) {

					for (ColumnaDTO c : listaColumnas) {
						if (columnasSeleccionadasC3[i] != null) {
							if (c.getNombreColumna().equals(columnasSeleccionadasC3[i])) {
								listaColumnasSeleccionadas.add(c);
							}
						}

					}

				}

				Collections.sort(listaColumnasSeleccionadas);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al Consultar las polizas.", e);
			FacesUtils.addInfoMessage("Error al Consultar suscripciones.");
		}
	}

	/**
	 * Genera consulta en formato CSV
	 */
	public void generarReporteCSV() {
		try {
			String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/suscripcion/");
			String strNombre = null;
			strNombre = "ReporteSuscripcion-" + new SimpleDateFormat("ddMMyy").format(new Date()) + ".csv";
			CsvWriter csvResultados = null;

			OutputStream outputStream = new FileOutputStream(ruta + File.separator + strNombre);
			Writer outputStreamWriter = new OutputStreamWriter(outputStream);
			csvResultados = new CsvWriter(outputStreamWriter, SEPARADOR.charAt(0));

			StringBuilder registro = new StringBuilder();

			for (ColumnaDTO c : listaColumnasSeleccionadas) {
				registro.append(c.getHeaderColumn());
				registro.append(SEPARADOR);
			}

			registro.delete(registro.length() - SEPARADOR.length(), registro.length());
			csvResultados.writeRecord(registro.toString().split(SEPARADOR));
			Class cls = Class.forName("mx.com.santander.aseguradora.colectivos.view.dto.SuscripcionDTO");
			// Class[] noparams = [];
			// Object obj = c ls.newInstance();
			Method method;

			for (SuscripcionDTO sus : suscripciones) {
				registro = new StringBuilder();
				for (ColumnaDTO c : listaColumnasSeleccionadas) {
					String metodo = "get" + c.getNombreColumna().substring(0, 1).toUpperCase()
							+ c.getNombreColumna().substring(1);
					method = cls.getDeclaredMethod(metodo);
					if (method.invoke(sus, null) == null) {
						registro.append("");
					} else {
						registro.append(method.invoke(sus, null));
					}

					registro.append(SEPARADOR);
				}
				registro.delete(registro.length() - SEPARADOR.length(), registro.length());
				csvResultados.writeRecord(registro.toString().split(SEPARADOR));
			}

			csvResultados.close();

			int longitud;
			byte[] datos;
			FileInputStream fis = null;
			ServletOutputStream sos;
			File descarga = new File(ruta + File.separator + strNombre);
			fis = new FileInputStream(descarga);
			longitud = fis.available();
			datos = new byte[longitud];
			fis.read(datos);
			fis.close();
			HttpServletResponse response = FacesUtils.getServletResponse();

			response.setHeader("Content-Disposition", "attachment;filename=\"" + strNombre + "\"");

			sos = response.getOutputStream();
			sos.write(datos);
			sos.flush();
			sos.close();
			descarga.delete();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
			this.log.error(e.getMessage());
			// FacesUtils.addErrorMessage(message.toString());
			FacesUtils.addInfoMessage("Error al generar reporte de suscripciones.");

		} catch (Exception e) {
			e.printStackTrace();
			FacesUtils.addInfoMessage("Error al rgenerar eporte de suscripciones.");
			// TODO: handle exception
		}

	}

	/**
	 * @return the servicioSuscripcion
	 */
	public ServicioSuscripcion getServicioSuscripcion() {
		return servicioSuscripcion;
	}

	/**
	 * @param servicioSuscripcion
	 *            the servicioSuscripcion to set
	 */
	public void setServicioSuscripcion(ServicioSuscripcion servicioSuscripcion) {
		this.servicioSuscripcion = servicioSuscripcion;
	}

	/**
	 * @return the listaColumnas
	 */
	public List<ColumnaDTO> getListaColumnas() {
		return listaColumnas;
	}

	/**
	 * @param listaColumnas
	 *            the listaColumnas to set
	 */
	public void setListaColumnas(List<ColumnaDTO> listaColumnas) {
		this.listaColumnas = listaColumnas;
	}

	/**
	 * @return the log
	 */
	public Log getLog() {
		return log;
	}

	/**
	 * @param log
	 *            the log to set
	 */
	public void setLog(Log log) {
		this.log = log;
	}

	/**
	 * @return the suscripciones
	 */
	public List<SuscripcionDTO> getSuscripciones() {
		return suscripciones;
	}

	/**
	 * @param suscripciones
	 *            the suscripciones to set
	 */
	public void setSuscripciones(List<SuscripcionDTO> suscripciones) {
		this.suscripciones = suscripciones;
	}

	/**
	 * @return the filtroSuscripcionDTO
	 */
	public FiltroSuscripcionDTO getFiltroSuscripcionDTO() {
		return filtroSuscripcionDTO;
	}

	/**
	 * @param filtroSuscripcionDTO
	 *            the filtroSuscripcionDTO to set
	 */
	public void setFiltroSuscripcionDTO(FiltroSuscripcionDTO filtroSuscripcionDTO) {
		this.filtroSuscripcionDTO = filtroSuscripcionDTO;
	}

	/**
	 * @return the pb
	 */
	public ProgressBar getPb() {
		return pb;
	}

	/**
	 * @param pb
	 *            the pb to set
	 */
	public void setPb(ProgressBar pb) {
		this.pb = pb;
	}

	/**
	 * @return the listaComboEstatus
	 */
	public List<SelectItem> getListaComboEstatus() {
		return listaComboEstatus;
	}

	/**
	 * @param listaComboEstatus
	 *            the listaComboEstatus to set
	 */
	public void setListaComboEstatus(List<SelectItem> listaComboEstatus) {
		this.listaComboEstatus = listaComboEstatus;
	}

	/**
	 * @return the columnasSeleccionadas
	 */
	public String[] getColumnasSeleccionadas() {
		return columnasSeleccionadas;
	}

	/**
	 * @param columnasSeleccionadas
	 *            the columnasSeleccionadas to set
	 */
	public void setColumnasSeleccionadas(String[] columnasSeleccionadas) {
		this.columnasSeleccionadas = columnasSeleccionadas;
	}

	/**
	 * @return the listaColumnasSeleccionadas
	 */
	public List<ColumnaDTO> getListaColumnasSeleccionadas() {
		return listaColumnasSeleccionadas;
	}

	/**
	 * @param listaColumnasSeleccionadas
	 *            the listaColumnasSeleccionadas to set
	 */
	public void setListaColumnasSeleccionadas(List<ColumnaDTO> listaColumnasSeleccionadas) {
		this.listaColumnasSeleccionadas = listaColumnasSeleccionadas;
	}

	/**
	 * @return the columnasSeleccionadasC2
	 */
	public String[] getColumnasSeleccionadasC2() {
		return columnasSeleccionadasC2;
	}

	/**
	 * @param columnasSeleccionadasC2
	 *            the columnasSeleccionadasC2 to set
	 */
	public void setColumnasSeleccionadasC2(String[] columnasSeleccionadasC2) {
		this.columnasSeleccionadasC2 = columnasSeleccionadasC2;
	}

	/**
	 * @return the columnasSeleccionadasC3
	 */
	public String[] getColumnasSeleccionadasC3() {
		return columnasSeleccionadasC3;
	}

	/**
	 * @param columnasSeleccionadasC3
	 *            the columnasSeleccionadasC3 to set
	 */
	public void setColumnasSeleccionadasC3(String[] columnasSeleccionadasC3) {
		this.columnasSeleccionadasC3 = columnasSeleccionadasC3;
	}

	/**
	 * @return the listaComboEstatusC2
	 */
	public List<SelectItem> getListaComboEstatusC2() {
		return listaComboEstatusC2;
	}

	/**
	 * @param listaComboEstatusC2
	 *            the listaComboEstatusC2 to set
	 */
	public void setListaComboEstatusC2(List<SelectItem> listaComboEstatusC2) {
		this.listaComboEstatusC2 = listaComboEstatusC2;
	}

	/**
	 * @return the listaComboEstatusC3
	 */
	public List<SelectItem> getListaComboEstatusC3() {
		return listaComboEstatusC3;
	}

	/**
	 * @param listaComboEstatusC3
	 *            the listaComboEstatusC3 to set
	 */
	public void setListaComboEstatusC3(List<SelectItem> listaComboEstatusC3) {
		this.listaComboEstatusC3 = listaComboEstatusC3;
	}

}
