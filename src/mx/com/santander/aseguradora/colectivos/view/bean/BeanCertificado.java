/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estatus;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Facturacion;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Ramo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanCertificado {

	private ServicioCertificado servicioCertificado;
	private CertificadoId id;
	private Estatus estatus;
	private Plan plan;
	private Ramo ramo;
	private Short coceStCertificado;
	private long coceNuMovimiento;
	private Integer coceCazbCdSucursal;
	private Integer coceSucursalBanco;
	private long coceNuCobertura;
	private long coceNuComponente;
	private String coceRemesaReporta;
	private String coceRemesaReal;
	private Date coceFeCarga;
	private Date coceFeSuscripcion;
	private BigDecimal coceMtSumaAsegurada;
	private BigDecimal coceMtSumaAsegSi;
	private BigDecimal coceMtPrimaAnual;
	private BigDecimal coceMtPrimaReal;
	private BigDecimal coceMtPrimaAnualReal;
	private BigDecimal coceMtPrimaPura;
	private BigDecimal coceMtPrimaSubsecuente;
	private Date coceFeDesde;
	private Date coceFeHasta;
	private Date coceFeEstatus;
	private Date coceFeEmision;
	private Date coceFeRenovacion;
	private Integer coceNuRenovacion;
	private Date coceFeAnulacion;
	private Date coceFeAnulacionCol;
	private Short coceCdCausaAnulacion;
	private Date coceFeRehabilitacion;
	private Short coceCdRehabilitacion;
	private Date coceFeIniCredito;
	private Date coceFeFinCredito;
	private Date coceFeLiqCredito;
	private Date coceFeIniAmparo;
	private Date coceFeFinAmparo;
	private BigDecimal coceMtSaldoInsoluto;
	private BigDecimal coceMtTotalPrestamo;
	private Short coceCdPlazo;
	private String coceFrPago;
	private String coceCampana;
	private String coceSubCampana;
	private String coceTpPma;
	private Byte coceCdConductoCobro;
	private String coceDiCobro1;
	private String coceTpProductoBco;
	private String coceTpSubprodBco;
	private BigDecimal coceMtSumaAsegUdis;
	private BigDecimal coceMtPmaUdis;
	private String coceNuCuenta;
	private String coceNuCredito;
	private Byte coceTpCuenta;
	private Long coceCampon1;
	private Long coceCampon2;
	private Long coceCampon3;
	private Long coceCampon6;
	private Long coceCampon7;
	private Long coceCampon8;
	private Long coceCampon9;
	private Long coceCampon10;	
	private Long coceCampon11;
	private Long coceCampon12;
	private Long coceCampon13;
	private BigDecimal coceCampon4;
	private BigDecimal coceCampon5;
	private String coceCampov1;
	private String coceCampov2;
	private String coceCampov3;
	private String coceCampov4;
	private String coceCampov5;
	private String coceCampov6;
	private String coceCampov7;
	private String coceCampov8;
	private Date coceCampof1;
	private Date coceCampof2;
	private Date coceCampof3;
	private BigDecimal coceMtReaseguro;
	private Date coceFeFacturacion;
	private BigDecimal coceNoRecibo;
	private boolean coceStFacturacion;
	private BigDecimal coceMtPrimaPuraReal;
	private String coceBucEmpresa;
	private BigDecimal coceMtBcoDevolucion;
	private BigDecimal coceMtDevolucion;
	private Set<Facturacion> facturacion;
	private boolean chkUpdate;
	private String desc1;
	private String desc2;
	private String desc3;
	private String desc4;
	private String desc5;
	private BigDecimal plazo;
	private BigDecimal trisk;
	
	private String idCertificado;
	

	private Set<ClienteCertif> clienteCertificados;
	
	private BigDecimal coceCampon7BD;
	private BigDecimal coceCampon8BD;
	

	
	/**
	 * @return the servicioCertificado
	 */
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}
	/**
	 * @param servicioCertificado the servicioCertificado to set
	 */
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
	/**
	 * @return the id
	 */
	public CertificadoId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(CertificadoId id) {
		this.id = id;
	}
	/**
	 * @return the estatus
	 */
	public Estatus getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Estatus estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the plan
	 */
	public Plan getPlan() {
		return plan;
	}
	/**
	 * @param plan the plan to set
	 */
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	/**
	 * @return the ramo
	 */
	public Ramo getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(Ramo ramo) {
		this.ramo = ramo;
	}
	/**
	 * @return the coceNuMovimiento
	 */
	public long getCoceNuMovimiento() {
		return coceNuMovimiento;
	}
	/**
	 * @param coceNuMovimiento the coceNuMovimiento to set
	 */
	public void setCoceNuMovimiento(long coceNuMovimiento) {
		this.coceNuMovimiento = coceNuMovimiento;
	}
	public void setCoceStCertificado(Short coceStCertificado) {
		this.coceStCertificado = coceStCertificado;
	}
	public Short getCoceStCertificado() {
		return coceStCertificado;
	}
	/**
	 * @return the coceCazbCdSucursal
	 */
	public Integer getCoceCazbCdSucursal() {
		return coceCazbCdSucursal;
	}
	/**
	 * @param coceCazbCdSucursal the coceCazbCdSucursal to set
	 */
	public void setCoceCazbCdSucursal(Integer coceCazbCdSucursal) {
		this.coceCazbCdSucursal = coceCazbCdSucursal;
	}
	/**
	 * @return the coceSucursalBanco
	 */
	public Integer getCoceSucursalBanco() {
		return coceSucursalBanco;
	}
	/**
	 * @param coceSucursalBanco the coceSucursalBanco to set
	 */
	public void setCoceSucursalBanco(Integer coceSucursalBanco) {
		this.coceSucursalBanco = coceSucursalBanco;
	}
	/**
	 * @return the coceNuCobertura
	 */
	public long getCoceNuCobertura() {
		return coceNuCobertura;
	}
	/**
	 * @param coceNuCobertura the coceNuCobertura to set
	 */
	public void setCoceNuCobertura(long coceNuCobertura) {
		this.coceNuCobertura = coceNuCobertura;
	}
	/**
	 * @return the coceNuComponente
	 */
	public long getCoceNuComponente() {
		return coceNuComponente;
	}
	/**
	 * @param coceNuComponente the coceNuComponente to set
	 */
	public void setCoceNuComponente(long coceNuComponente) {
		this.coceNuComponente = coceNuComponente;
	}
	/**
	 * @return the coceRemesaReporta
	 */
	public String getCoceRemesaReporta() {
		return coceRemesaReporta;
	}
	/**
	 * @param coceRemesaReporta the coceRemesaReporta to set
	 */
	public void setCoceRemesaReporta(String coceRemesaReporta) {
		this.coceRemesaReporta = coceRemesaReporta;
	}
	/**
	 * @return the coceRemesaReal
	 */
	public String getCoceRemesaReal() {
		return coceRemesaReal;
	}
	/**
	 * @param coceRemesaReal the coceRemesaReal to set
	 */
	public void setCoceRemesaReal(String coceRemesaReal) {
		this.coceRemesaReal = coceRemesaReal;
	}
	/**
	 * @return the coceFeCarga
	 */
	public Date getCoceFeCarga() {
		return coceFeCarga;
	}
	/**
	 * @param coceFeCarga the coceFeCarga to set
	 */
	public void setCoceFeCarga(Date coceFeCarga) {
		this.coceFeCarga = coceFeCarga;
	}
	/**
	 * @return the coceFeSuscripcion
	 */
	public Date getCoceFeSuscripcion() {
		return coceFeSuscripcion;
	}
	/**
	 * @param coceFeSuscripcion the coceFeSuscripcion to set
	 */
	public void setCoceFeSuscripcion(Date coceFeSuscripcion) {
		this.coceFeSuscripcion = coceFeSuscripcion;
	}
	/**
	 * @return the coceMtSumaAsegurada
	 */
	public BigDecimal getCoceMtSumaAsegurada() {
		return coceMtSumaAsegurada;
	}
	/**
	 * @param coceMtSumaAsegurada the coceMtSumaAsegurada to set
	 */
	public void setCoceMtSumaAsegurada(BigDecimal coceMtSumaAsegurada) {
		this.coceMtSumaAsegurada = coceMtSumaAsegurada;
	}
	/**
	 * @return the coceMtSumaAsegSi
	 */
	public BigDecimal getCoceMtSumaAsegSi() {
		return coceMtSumaAsegSi;
	}
	/**
	 * @param coceMtSumaAsegSi the coceMtSumaAsegSi to set
	 */
	public void setCoceMtSumaAsegSi(BigDecimal coceMtSumaAsegSi) {
		this.coceMtSumaAsegSi = coceMtSumaAsegSi;
	}
	/**
	 * @return the coceMtPrimaAnual
	 */
	public BigDecimal getCoceMtPrimaAnual() {
		return coceMtPrimaAnual;
	}
	/**
	 * @param coceMtPrimaAnual the coceMtPrimaAnual to set
	 */
	public void setCoceMtPrimaAnual(BigDecimal coceMtPrimaAnual) {
		this.coceMtPrimaAnual = coceMtPrimaAnual;
	}
	/**
	 * @return the coceMtPrimaReal
	 */
	public BigDecimal getCoceMtPrimaReal() {
		return coceMtPrimaReal;
	}
	/**
	 * @param coceMtPrimaReal the coceMtPrimaReal to set
	 */
	public void setCoceMtPrimaReal(BigDecimal coceMtPrimaReal) {
		this.coceMtPrimaReal = coceMtPrimaReal;
	}
	/**
	 * @return the coceMtPrimaAnualReal
	 */
	public BigDecimal getCoceMtPrimaAnualReal() {
		return coceMtPrimaAnualReal;
	}
	/**
	 * @param coceMtPrimaAnualReal the coceMtPrimaAnualReal to set
	 */
	public void setCoceMtPrimaAnualReal(BigDecimal coceMtPrimaAnualReal) {
		this.coceMtPrimaAnualReal = coceMtPrimaAnualReal;
	}
	/**
	 * @return the coceMtPrimaPura
	 */
	public BigDecimal getCoceMtPrimaPura() {
		return coceMtPrimaPura;
	}
	/**
	 * @param coceMtPrimaPura the coceMtPrimaPura to set
	 */
	public void setCoceMtPrimaPura(BigDecimal coceMtPrimaPura) {
		this.coceMtPrimaPura = coceMtPrimaPura;
	}
	/**
	 * @return the coceMtPrimaSubsecuente
	 */
	public BigDecimal getCoceMtPrimaSubsecuente() {
		return coceMtPrimaSubsecuente;
	}
	/**
	 * @param coceMtPrimaSubsecuente the coceMtPrimaSubsecuente to set
	 */
	public void setCoceMtPrimaSubsecuente(BigDecimal coceMtPrimaSubsecuente) {
		this.coceMtPrimaSubsecuente = coceMtPrimaSubsecuente;
	}
	/**
	 * @return the coceFeDesde
	 */
	public Date getCoceFeDesde() {
		return coceFeDesde;
	}
	/**
	 * @param coceFeDesde the coceFeDesde to set
	 */
	public void setCoceFeDesde(Date coceFeDesde) {
		this.coceFeDesde = coceFeDesde;
	}
	/**
	 * @return the coceFeHasta
	 */
	public Date getCoceFeHasta() {
		return coceFeHasta;
	}
	/**
	 * @param coceFeHasta the coceFeHasta to set
	 */
	public void setCoceFeHasta(Date coceFeHasta) {
		this.coceFeHasta = coceFeHasta;
	}
	/**
	 * @return the coceFeEstatus
	 */
	public Date getCoceFeEstatus() {
		return coceFeEstatus;
	}
	/**
	 * @param coceFeEstatus the coceFeEstatus to set
	 */
	public void setCoceFeEstatus(Date coceFeEstatus) {
		this.coceFeEstatus = coceFeEstatus;
	}
	/**
	 * @return the coceFeEmision
	 */
	public Date getCoceFeEmision() {
		return coceFeEmision;
	}
	/**
	 * @param coceFeEmision the coceFeEmision to set
	 */
	public void setCoceFeEmision(Date coceFeEmision) {
		this.coceFeEmision = coceFeEmision;
	}
	/**
	 * @return the coceFeRenovacion
	 */
	public Date getCoceFeRenovacion() {
		return coceFeRenovacion;
	}
	/**
	 * @param coceFeRenovacion the coceFeRenovacion to set
	 */
	public void setCoceFeRenovacion(Date coceFeRenovacion) {
		this.coceFeRenovacion = coceFeRenovacion;
	}
	/**
	 * @return the coceNuRenovacion
	 */
	public Integer getCoceNuRenovacion() {
		return coceNuRenovacion;
	}
	/**
	 * @param coceNuRenovacion the coceNuRenovacion to set
	 */
	public void setCoceNuRenovacion(Integer coceNuRenovacion) {
		this.coceNuRenovacion = coceNuRenovacion;
	}
	/**
	 * @return the coceFeAnulacion
	 */
	public Date getCoceFeAnulacion() {
		return coceFeAnulacion;
	}
	/**
	 * @param coceFeAnulacion the coceFeAnulacion to set
	 */
	public void setCoceFeAnulacion(Date coceFeAnulacion) {
		this.coceFeAnulacion = coceFeAnulacion;
	}
	/**
	 * @return the coceFeAnulacionCol
	 */
	public Date getCoceFeAnulacionCol() {
		return coceFeAnulacionCol;
	}
	/**
	 * @param coceFeAnulacionCol the coceFeAnulacionCol to set
	 */
	public void setCoceFeAnulacionCol(Date coceFeAnulacionCol) {
		this.coceFeAnulacionCol = coceFeAnulacionCol;
	}
	/**
	 * @return the coceCdCausaAnulacion
	 */
	public Short getCoceCdCausaAnulacion() {
		return coceCdCausaAnulacion;
	}
	/**
	 * @param coceCdCausaAnulacion the coceCdCausaAnulacion to set
	 */
	public void setCoceCdCausaAnulacion(Short coceCdCausaAnulacion) {
		this.coceCdCausaAnulacion = coceCdCausaAnulacion;
	}
	/**
	 * @return the coceFeRehabilitacion
	 */
	public Date getCoceFeRehabilitacion() {
		return coceFeRehabilitacion;
	}
	/**
	 * @param coceFeRehabilitacion the coceFeRehabilitacion to set
	 */
	public void setCoceFeRehabilitacion(Date coceFeRehabilitacion) {
		this.coceFeRehabilitacion = coceFeRehabilitacion;
	}
	/**
	 * @return the coceCdRehabilitacion
	 */
	public Short getCoceCdRehabilitacion() {
		return coceCdRehabilitacion;
	}
	/**
	 * @param coceCdRehabilitacion the coceCdRehabilitacion to set
	 */
	public void setCoceCdRehabilitacion(Short coceCdRehabilitacion) {
		this.coceCdRehabilitacion = coceCdRehabilitacion;
	}
	/**
	 * @return the coceFeIniCredito
	 */
	public Date getCoceFeIniCredito() {
		return coceFeIniCredito;
	}
	/**
	 * @param coceFeIniCredito the coceFeIniCredito to set
	 */
	public void setCoceFeIniCredito(Date coceFeIniCredito) {
		this.coceFeIniCredito = coceFeIniCredito;
	}
	/**
	 * @return the coceFeFinCredito
	 */
	public Date getCoceFeFinCredito() {
		return coceFeFinCredito;
	}
	/**
	 * @param coceFeFinCredito the coceFeFinCredito to set
	 */
	public void setCoceFeFinCredito(Date coceFeFinCredito) {
		this.coceFeFinCredito = coceFeFinCredito;
	}
	/**
	 * @return the coceFeLiqCredito
	 */
	public Date getCoceFeLiqCredito() {
		return coceFeLiqCredito;
	}
	/**
	 * @param coceFeLiqCredito the coceFeLiqCredito to set
	 */
	public void setCoceFeLiqCredito(Date coceFeLiqCredito) {
		this.coceFeLiqCredito = coceFeLiqCredito;
	}
	/**
	 * @return the coceFeIniAmparo
	 */
	public Date getCoceFeIniAmparo() {
		return coceFeIniAmparo;
	}
	/**
	 * @param coceFeIniAmparo the coceFeIniAmparo to set
	 */
	public void setCoceFeIniAmparo(Date coceFeIniAmparo) {
		this.coceFeIniAmparo = coceFeIniAmparo;
	}
	/**
	 * @return the coceFeFinAmparo
	 */
	public Date getCoceFeFinAmparo() {
		return coceFeFinAmparo;
	}
	/**
	 * @param coceFeFinAmparo the coceFeFinAmparo to set
	 */
	public void setCoceFeFinAmparo(Date coceFeFinAmparo) {
		this.coceFeFinAmparo = coceFeFinAmparo;
	}
	/**
	 * @return the coceMtSaldoInsoluto
	 */
	public BigDecimal getCoceMtSaldoInsoluto() {
		return coceMtSaldoInsoluto;
	}
	/**
	 * @param coceMtSaldoInsoluto the coceMtSaldoInsoluto to set
	 */
	public void setCoceMtSaldoInsoluto(BigDecimal coceMtSaldoInsoluto) {
		this.coceMtSaldoInsoluto = coceMtSaldoInsoluto;
	}
	/**
	 * @return the coceMtTotalPrestamo
	 */
	public BigDecimal getCoceMtTotalPrestamo() {
		return coceMtTotalPrestamo;
	}
	/**
	 * @param coceMtTotalPrestamo the coceMtTotalPrestamo to set
	 */
	public void setCoceMtTotalPrestamo(BigDecimal coceMtTotalPrestamo) {
		this.coceMtTotalPrestamo = coceMtTotalPrestamo;
	}
	/**
	 * @return the coceCdPlazo
	 */
	public Short getCoceCdPlazo() {
		return coceCdPlazo;
	}
	/**
	 * @param coceCdPlazo the coceCdPlazo to set
	 */
	public void setCoceCdPlazo(Short coceCdPlazo) {
		this.coceCdPlazo = coceCdPlazo;
	}
	/**
	 * @return the coceFrPago
	 */
	public String getCoceFrPago() {
		return coceFrPago;
	}
	/**
	 * @param coceFrPago the coceFrPago to set
	 */
	public void setCoceFrPago(String coceFrPago) {
		this.coceFrPago = coceFrPago;
	}
	/**
	 * @return the coceCampana
	 */
	public String getCoceCampana() {
		return coceCampana;
	}
	/**
	 * @param coceCampana the coceCampana to set
	 */
	public void setCoceCampana(String coceCampana) {
		this.coceCampana = coceCampana;
	}
	/**
	 * @return the coceSubCampana
	 */
	public String getCoceSubCampana() {
		return coceSubCampana;
	}
	/**
	 * @param coceSubCampana the coceSubCampana to set
	 */
	public void setCoceSubCampana(String coceSubCampana) {
		this.coceSubCampana = coceSubCampana;
	}
	/**
	 * @return the coceTpPma
	 */
	public String getCoceTpPma() {
		return coceTpPma;
	}
	/**
	 * @param coceTpPma the coceTpPma to set
	 */
	public void setCoceTpPma(String coceTpPma) {
		this.coceTpPma = coceTpPma;
	}
	/**
	 * @return the coceCdConductoCobro
	 */
	public Byte getCoceCdConductoCobro() {
		return coceCdConductoCobro;
	}
	/**
	 * @param coceCdConductoCobro the coceCdConductoCobro to set
	 */
	public void setCoceCdConductoCobro(Byte coceCdConductoCobro) {
		this.coceCdConductoCobro = coceCdConductoCobro;
	}
	/**
	 * @return the coceDiCobro1
	 */
	public String getCoceDiCobro1() {
		return coceDiCobro1;
	}
	/**
	 * @param coceDiCobro1 the coceDiCobro1 to set
	 */
	public void setCoceDiCobro1(String coceDiCobro1) {
		this.coceDiCobro1 = coceDiCobro1;
	}
	/**
	 * @return the coceTpProductoBco
	 */
	public String getCoceTpProductoBco() {
		return coceTpProductoBco;
	}
	/**
	 * @param coceTpProductoBco the coceTpProductoBco to set
	 */
	public void setCoceTpProductoBco(String coceTpProductoBco) {
		this.coceTpProductoBco = coceTpProductoBco;
	}
	/**
	 * @return the coceTpSubprodBco
	 */
	public String getCoceTpSubprodBco() {
		return coceTpSubprodBco;
	}
	/**
	 * @param coceTpSubprodBco the coceTpSubprodBco to set
	 */
	public void setCoceTpSubprodBco(String coceTpSubprodBco) {
		this.coceTpSubprodBco = coceTpSubprodBco;
	}
	/**
	 * @return the coceMtSumaAsegUdis
	 */
	public BigDecimal getCoceMtSumaAsegUdis() {
		return coceMtSumaAsegUdis;
	}
	/**
	 * @param coceMtSumaAsegUdis the coceMtSumaAsegUdis to set
	 */
	public void setCoceMtSumaAsegUdis(BigDecimal coceMtSumaAsegUdis) {
		this.coceMtSumaAsegUdis = coceMtSumaAsegUdis;
	}
	/**
	 * @return the coceMtPmaUdis
	 */
	public BigDecimal getCoceMtPmaUdis() {
		return coceMtPmaUdis;
	}
	/**
	 * @param coceMtPmaUdis the coceMtPmaUdis to set
	 */
	public void setCoceMtPmaUdis(BigDecimal coceMtPmaUdis) {
		this.coceMtPmaUdis = coceMtPmaUdis;
	}
	/**
	 * @return the coceNuCuenta
	 */
	public String getCoceNuCuenta() {
		return coceNuCuenta;
	}
	/**
	 * @param coceNuCuenta the coceNuCuenta to set
	 */
	public void setCoceNuCuenta(String coceNuCuenta) {
		this.coceNuCuenta = coceNuCuenta;
	}
	/**
	 * @return the coceNuCredito
	 */
	public String getCoceNuCredito() {
		return coceNuCredito;
	}
	/**
	 * @param coceNuCredito the coceNuCredito to set
	 */
	public void setCoceNuCredito(String coceNuCredito) {
		this.coceNuCredito = coceNuCredito;
	}
	/**
	 * @return the coceTpCuenta
	 */
	public Byte getCoceTpCuenta() {
		return coceTpCuenta;
	}
	/**
	 * @param coceTpCuenta the coceTpCuenta to set
	 */
	public void setCoceTpCuenta(Byte coceTpCuenta) {
		this.coceTpCuenta = coceTpCuenta;
	}
	/**
	 * @return the coceCampon1
	 */
	public Long getCoceCampon1() {
		return coceCampon1;
	}
	/**
	 * @param coceCampon1 the coceCampon1 to set
	 */
	public void setCoceCampon1(Long coceCampon1) {
		this.coceCampon1 = coceCampon1;
	}
	/**
	 * @return the coceCampon2
	 */
	public Long getCoceCampon2() {
		return coceCampon2;
	}
	/**
	 * @param coceCampon2 the coceCampon2 to set
	 */
	public void setCoceCampon2(Long coceCampon2) {
		this.coceCampon2 = coceCampon2;
	}
	/**
	 * @return the coceCampon3
	 */
	public Long getCoceCampon3() {
		return coceCampon3;
	}
	/**
	 * @param coceCampon3 the coceCampon3 to set
	 */
	public void setCoceCampon3(Long coceCampon3) {
		this.coceCampon3 = coceCampon3;
	}
	/**
	 * @return the coceCampon4
	 */
	public BigDecimal getCoceCampon4() {
		return coceCampon4;
	}
	/**
	 * @param coceCampon4 the coceCampon4 to set
	 */
	public void setCoceCampon4(BigDecimal coceCampon4) {
		this.coceCampon4 = coceCampon4;
	}
	/**
	 * @return the coceCampon5
	 */
	public BigDecimal getCoceCampon5() {
		return coceCampon5;
	}
	/**
	 * @param coceCampon5 the coceCampon5 to set
	 */
	public void setCoceCampon5(BigDecimal coceCampon5) {
		this.coceCampon5 = coceCampon5;
	}
	/**
	 * @return the coceCampov1
	 */
	public String getCoceCampov1() {
		return coceCampov1;
	}
	/**
	 * @param coceCampov1 the coceCampov1 to set
	 */
	public void setCoceCampov1(String coceCampov1) {
		this.coceCampov1 = coceCampov1;
	}
	/**
	 * @return the coceCampov2
	 */
	public String getCoceCampov2() {
		return coceCampov2;
	}
	/**
	 * @param coceCampov2 the coceCampov2 to set
	 */
	public void setCoceCampov2(String coceCampov2) {
		this.coceCampov2 = coceCampov2;
	}
	/**
	 * @return the coceCampov3
	 */
	public String getCoceCampov3() {
		return coceCampov3;
	}
	/**
	 * @param coceCampov3 the coceCampov3 to set
	 */
	public void setCoceCampov3(String coceCampov3) {
		this.coceCampov3 = coceCampov3;
	}
	/**
	 * @return the coceCampov4
	 */
	public String getCoceCampov4() {
		return coceCampov4;
	}
	/**
	 * @param coceCampov4 the coceCampov4 to set
	 */
	public void setCoceCampov4(String coceCampov4) {
		this.coceCampov4 = coceCampov4;
	}
	/**
	 * @return the coceCampov5
	 */
	public String getCoceCampov5() {
		return coceCampov5;
	}
	/**
	 * @param coceCampov5 the coceCampov5 to set
	 */
	public void setCoceCampov5(String coceCampov5) {
		this.coceCampov5 = coceCampov5;
	}
	/**
	 * @return the coceCampov6
	 */
	public String getCoceCampov6() {
		return coceCampov6;
	}
	/**
	 * @param coceCampov6 the coceCampov6 to set
	 */
	public void setCoceCampov6(String coceCampov6) {
		this.coceCampov6 = coceCampov6;
	}
	/**
	 * @return the coceCampov7
	 */
	public String getCoceCampov7() {
		return coceCampov7;
	}
	/**
	 * @param coceCampov7 the coceCampov7 to set
	 */
	public void setCoceCampov7(String coceCampov7) {
		this.coceCampov7 = coceCampov7;
	}
	/**
	 * @return the coceCampov8
	 */
	public String getCoceCampov8() {
		return coceCampov8;
	}
	/**
	 * @param coceCampov8 the coceCampov8 to set
	 */
	public void setCoceCampov8(String coceCampov8) {
		this.coceCampov8 = coceCampov8;
	}
	/**
	 * @return the coceCampof1
	 */
	public Date getCoceCampof1() {
		return coceCampof1;
	}
	/**
	 * @param coceCampof1 the coceCampof1 to set
	 */
	public void setCoceCampof1(Date coceCampof1) {
		this.coceCampof1 = coceCampof1;
	}
	/**
	 * @return the coceCampof2
	 */
	public Date getCoceCampof2() {
		return coceCampof2;
	}
	/**
	 * @param coceCampof2 the coceCampof2 to set
	 */
	public void setCoceCampof2(Date coceCampof2) {
		this.coceCampof2 = coceCampof2;
	}
	/**
	 * @return the coceCampof3
	 */
	public Date getCoceCampof3() {
		return coceCampof3;
	}
	/**
	 * @param coceCampof3 the coceCampof3 to set
	 */
	public void setCoceCampof3(Date coceCampof3) {
		this.coceCampof3 = coceCampof3;
	}
	/**
	 * @return the coceMtReaseguro
	 */
	public BigDecimal getCoceMtReaseguro() {
		return coceMtReaseguro;
	}
	/**
	 * @param coceMtReaseguro the coceMtReaseguro to set
	 */
	public void setCoceMtReaseguro(BigDecimal coceMtReaseguro) {
		this.coceMtReaseguro = coceMtReaseguro;
	}
	/**
	 * @return the coceFeFacturacion
	 */
	public Date getCoceFeFacturacion() {
		return coceFeFacturacion;
	}
	/**
	 * @param coceFeFacturacion the coceFeFacturacion to set
	 */
	public void setCoceFeFacturacion(Date coceFeFacturacion) {
		this.coceFeFacturacion = coceFeFacturacion;
	}
	/**
	 * @return the coceNoRecibo
	 */
	public BigDecimal getCoceNoRecibo() {
		return coceNoRecibo;
	}
	/**
	 * @param coceNoRecibo the coceNoRecibo to set
	 */
	public void setCoceNoRecibo(BigDecimal coceNoRecibo) {
		this.coceNoRecibo = coceNoRecibo;
	}
	/**
	 * @return the coceStFacturacion
	 */
	public boolean isCoceStFacturacion() {
		return coceStFacturacion;
	}
	/**
	 * @param coceStFacturacion the coceStFacturacion to set
	 */
	public void setCoceStFacturacion(boolean coceStFacturacion) {
		this.coceStFacturacion = coceStFacturacion;
	}
	/**
	 * @return the coceMtPrimaPuraReal
	 */
	public BigDecimal getCoceMtPrimaPuraReal() {
		return coceMtPrimaPuraReal;
	}
	/**
	 * @param coceMtPrimaPuraReal the coceMtPrimaPuraReal to set
	 */
	public void setCoceMtPrimaPuraReal(BigDecimal coceMtPrimaPuraReal) {
		this.coceMtPrimaPuraReal = coceMtPrimaPuraReal;
	}
	
	/**
	 * @return the coceMtBcoDevolucion
	 */
	public BigDecimal getCoceMtBcoDevolucion() {
		return coceMtBcoDevolucion;
	}
	/**
	 * @param coceMtBcoDevolucion the coceMtBcoDevolucion to set
	 */
	public void setCoceMtBcoDevolucion(BigDecimal coceMtBcoDevolucion) {
		this.coceMtBcoDevolucion = coceMtBcoDevolucion;
	}
	/**
	 * @return the coceMtDevolucion
	 */
	public BigDecimal getCoceMtDevolucion() {
		return coceMtDevolucion;
	}
	/**
	 * @param coceMtDevolucion the coceMtDevolucion to set
	 */
	public void setCoceMtDevolucion(BigDecimal coceMtDevolucion) {
		this.coceMtDevolucion = coceMtDevolucion;
	}
	public String getDesc1() {
		return desc1;
	}

	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}

	public String getDesc2() {
		return desc2;
	}

	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}

	public String getDesc3() {
		return desc3;
	}

	public void setDesc3(String desc3) {
		this.desc3 = desc3;
	}

	public String getDesc4() {
		return desc4;
	}

	public void setDesc4(String desc4) {
		this.desc4 = desc4;
	}

	public String getDesc5() {
		return desc5;
	}

	public void setDesc5(String desc5) {
		this.desc5 = desc5;
	}

	public BigDecimal getTrisk() {
		return trisk;
	}

	public void setTrisk(BigDecimal trisk) {
		this.trisk = trisk;
	}

	public BigDecimal getPlazo() {
		return plazo;
	}

	public void setPlazo(BigDecimal plazo) {
		this.plazo = plazo;
	}
	
	public Long getCoceCampon6() {
		return coceCampon6;
	}
	public void setCoceCampon6(Long coceCampon6) {
		this.coceCampon6 = coceCampon6;
	}
	public Long getCoceCampon7() {
		return coceCampon7;
	}
	public void setCoceCampon7(Long coceCampon7) {
		this.coceCampon7 = coceCampon7;
	}
	public Long getCoceCampon8() {
		return coceCampon8;
	}
	public void setCoceCampon8(Long coceCampon8) {
		this.coceCampon8 = coceCampon8;
	}
	public Long getCoceCampon9() {
		return coceCampon9;
	}
	public void setCoceCampon9(Long coceCampon9) {
		this.coceCampon9 = coceCampon9;
	}
	public Long getCoceCampon10() {
		return coceCampon10;
	}
	public void setCoceCampon10(Long coceCampon10) {
		this.coceCampon10 = coceCampon10;
	}
	public Long getCoceCampon11() {
		return coceCampon11;
	}
	public void setCoceCampon11(Long coceCampon11) {
		this.coceCampon11 = coceCampon11;
	}
	public Long getCoceCampon12() {
		return coceCampon12;
	}
	public void setCoceCampon12(Long coceCampon12) {
		this.coceCampon12 = coceCampon12;
	}
	public Long getCoceCampon13() {
		return coceCampon13;
	}
	public void setCoceCampon13(Long coceCampon13) {
		this.coceCampon13 = coceCampon13;
	}
	public void setIdCertificado(String idCertificado) {
		this.idCertificado = idCertificado;
	}
	public String getIdCertificado() {
		return idCertificado;
	}
	/**
	 * @return the facturacion
	 */
	public Set<Facturacion> getFacturacion() {
		return facturacion;
	}
	/**
	 * @param facturacion the facturacion to set
	 */
	public void setFacturacion(Set<Facturacion> facturacion) {
		this.facturacion = facturacion;
	}
	/**
	 * @return the clienteCertificados
	 */
	public Set<ClienteCertif> getClienteCertificados() {
		return clienteCertificados;
	}
	/**
	 * @param clienteCertificados the clienteCertificados to set
	 */
	public void setClienteCertificados(Set<ClienteCertif> clienteCertificados) {
		this.clienteCertificados = clienteCertificados;
	}

	/**
	 * @param chk se copio de otro bean
	 */
	public boolean getChkUpdate() {
		return chkUpdate;
	}
	/**
	 * @param chk se copio de otro bean
	 */
	public void setChkUpdate(boolean chkUpdate) {
		this.chkUpdate = chkUpdate;
	}
	
	public String getCoceBucEmpresa() {
		return coceBucEmpresa;
	}
	
	public void setCoceBucEmpresa(String coceBucEmpresa) {
		this.coceBucEmpresa = coceBucEmpresa;
	}
	public BeanCertificado() {
		new StringBuilder();
		id= new CertificadoId();
		estatus = new Estatus();
		//this.log.info("BeanCertificado creado.");
	}
	
	public BigDecimal getCoceCampon7BD() {
		return coceCampon7BD;
	}
	public void setCoceCampon7BD(BigDecimal coceCampon7BD) {
		this.coceCampon7BD = coceCampon7BD;
	}
	public BigDecimal getCoceCampon8BD() {
		return coceCampon8BD;
	}
	public void setCoceCampon8BD(BigDecimal coceCampon8BD) {
		this.coceCampon8BD = coceCampon8BD;
	}

}
