package mx.com.santander.aseguradora.colectivos.view.bean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("request")
public class BeanDetalleComponentes {
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private String compo;
	private String compo_desc;
	private String tarif;
	private String monto_compo;

	
	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	public String getCompo() {
		return compo;
	}
	public void setCompo(String compo) {
		this.compo = compo;
	}
	public String getCompo_desc() {
		return compo_desc;
	}
	public void setCompo_desc(String compo_desc) {
		this.compo_desc = compo_desc;
	}
	public String getTarif() {
		return tarif;
	}
	public void setTarif(String tarif) {
		this.tarif = tarif;
	}
	public String getMonto_compo() {
		return monto_compo;
	}
	public void setMonto_compo(String monto_compo) {
		this.monto_compo = monto_compo;
	}
	
}
