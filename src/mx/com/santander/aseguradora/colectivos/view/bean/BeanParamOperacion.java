package mx.com.santander.aseguradora.colectivos.view.bean;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ProductoId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")

public class BeanParamOperacion  { 	
	
	private static final Long Poliza = null;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	
	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ServicioProducto  servicioProducto;
	@Resource
	private ServicioPlan	servicioPlan;
	
	
	// check  auxiliar

	private boolean nRestitucionChk;  
	private boolean nSaldoChk;
	private boolean nDevolucionChk;
	private boolean nFacturacionChk;
	private boolean nRehabilitacionChk ;
	private boolean nEndosoChk;
	
	private HtmlSelectBooleanCheckbox rehabilitacionChk;
	private HtmlSelectBooleanCheckbox endosoChk;
	private HtmlSelectBooleanCheckbox restitucionChk;
	private HtmlSelectBooleanCheckbox saldoChk;
	private HtmlSelectBooleanCheckbox devolucionChk;
	private HtmlSelectBooleanCheckbox facturacionChk;
	
	//Mapeos
		//--   COLECTIVOS  PARAMETROS
	
	private Long    copaCdParametro; // max 
	private String  copaDesParametro; // concepto poliza
	private String  copaIdParametro; // parametro
	private String  copaVvalor1;
	private String  copaVvalor2;
	private String  copaVvalor3;
	private String  copaVvalor4;
	private String  copaVvalor5;
	private String  copaVvalor6;
	private String  copaVvalor7;
	private String  copaVvalor8;
	private Integer copaNvalor1; // canal 
	private Long    copaNvalor2;    // ramo
	private Long    copaNvalor3;    // poliza
	private Integer copaNvalor4;
	private Integer copaNvalor5;
	private Integer copaNvalor6;
	private Double 	copaNvalor7;
	private Double 	copaNvalor8;
	
	private Date copaFvalor1;
	private Date copaFvalor2;
	private Date copaFvalor3;
	private Date copaFvalor4;
	private Date copaFvalor5;

	private List<Object> listaParametros;
	private List<Object> listaParametrosRehabilitacion;
	private List<Object> listaParametrosEmision;
	private List<Object> listaParametrosRestitucion; // restitucion inmediata 
	private List<Object> listaParametrosSaldo; // Saldo insoluto y suma asegurada
	private List<Object> listaParametrosDevolucion; // diferencia en primas emitidas y devoluciones 
	private List<Object> listaParametrosFacturacion;
	private List<Object> listaParametrosObligados; // obligados
	private List<Object> listaParametrosEndoso; // endoso
	
	
	private List<BeanParamOperacion> listaCheck;
	private List<Object> listaComboProducto;
	private List<Object> listaComboPolizas;
	private List<Object> listaComboIdVenta;
	private List<Object> listaComboPlanes;
	private List<Plan> listaPlanes;
	
	private Boolean habilitaComboIdVenta; 
	private Boolean habilitaPoliza;
	private Boolean habilitaComboProducto;
	private String strRespuesta;
	
	private ProductoId id;
	

	private PlanId idPlan;
	
	private Integer valorProducto;
	private Integer valorIdVenta;
	private Integer valorPlan;
	private Integer PolizaUnica;
	
	private HtmlSelectOneMenu inIdVenta;
		
	private HashMap<String, Object> hmResultados;
	private HashMap<String, Object> hmResultadosEndoso;
	private HashMap<String, Object> hmResultadosRestitucion;
	private HashMap<String, Object> hmResultadosSaldo;
	private HashMap<String, Object> hmResultadosDevolucion;
	private HashMap<String, Object> hmResultadosFacturacion;
	private HashMap<String, Object> hmResultadosObligados;
	
	private Parametros 			    objActual = new Parametros();
	private Parametros			    objNuevo = new Parametros();
	private Set<Integer> 			keys = new HashSet<Integer>();
	private int 					filaActual;

	private Parametros				objActualE = new Parametros();
	private Parametros				objNuevoE = new Parametros();
	private Set<Integer> 			keysEndoso = new HashSet<Integer>();
	private int 					filaActualE;

	private Parametros 				objActualResti = new Parametros();
	private Parametros				objNuevoResti = new Parametros();
	private Set<Integer> 			keysResti = new HashSet<Integer>();
	private int 					filaActualResti;

	private Parametros 				objActualSaldo = new Parametros();
	private Parametros				objNuevoSaldo = new Parametros();
	private Set<Integer> 			keysSaldo = new HashSet<Integer>();
	private int 					filaActualSaldo;
	
	private Parametros 				objActualDev = new Parametros();
	private Parametros				objNuevoDev = new Parametros();
	private Set<Integer> 			keysDev = new HashSet<Integer>();
	private int 					filaActualDev;
	
	private Parametros 				objActualFac = new Parametros();
	private Parametros				objNuevoFac = new Parametros();
	private Set<Integer> 			keysFac = new HashSet<Integer>();
	private int 					filaActualFac;
	
	private Parametros 				objActualObligado = new Parametros();
	private Parametros				objNuevoObligado = new Parametros();
	private Set<Integer> 			keysObligado = new HashSet<Integer>();
	private int 					filaActualObligado;
	
	private Integer plazo;		//plazo
	
	//constructor
	public BeanParamOperacion() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PARAMETRIZA_OPERACION);
		
		this.listaCheck 			= new ArrayList<BeanParamOperacion>();
		this.listaComboIdVenta		= new ArrayList<Object>();
		this.listaComboProducto 	= new ArrayList<Object>();
		this.listaComboPolizas	  	= new ArrayList<Object>();
		this.listaComboPlanes	  	= new ArrayList<Object>();
		this.id						= new ProductoId();
		this.idPlan					= new PlanId();
		//this.rehabilitacionChk	= new HtmlSelectBooleanCheckbox();
		this.listaParametros 		= new ArrayList<Object>();
		this.listaParametrosRehabilitacion	= new ArrayList<Object>();
		this.listaParametrosEmision			= new ArrayList<Object>();
		this.listaParametrosRestitucion		= new ArrayList<Object>(); 
		this.listaParametrosSaldo			= new ArrayList<Object>();
		this.listaParametrosDevolucion		= new ArrayList<Object>(); 
		this.listaParametrosFacturacion		= new ArrayList<Object>();
		this.listaParametrosObligados		= new ArrayList<Object>();
		this.listaParametrosEndoso			= new ArrayList<Object>();
		this.message						= new StringBuilder();
		
	}
		
	
	
	// Fltros 
	
	public String cargaPolizas(){

		StringBuilder filtro = new StringBuilder(100);
			
		this.listaComboPolizas.clear();
		
		if( getCopaNvalor2() != null){
			
			try {
		
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro.append("  and P.copaNvalor1 	   = 1 \n");
				filtro.append("  and P.copaNvalor2 = ").append(getCopaNvalor2()).append("\n");
				filtro.append("  and P.copaNvalor7 = 0 \n");
				filtro.append("order by P.copaNvalor3");
				
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				this.listaComboPolizas.add(new SelectItem("0","PRIMA UNICA"));
				
				for(Parametros item: listaPolizas){
					this.listaComboPolizas.add(new SelectItem(item.getCopaNvalor3().toString(), item.getCopaNvalor3().toString()));
				}
				
				return NavigationResults.SUCCESS;
				
				
			} catch (Exception e) {
				e.printStackTrace();
				message.append("No se pueden cargar los parametros.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}
		}
		else{
			
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.RETRY;
		}
		
	}
	public void cargaIdVenta(){
		
		StringBuilder filtro  = new StringBuilder();
		StringBuilder filtro2 = new StringBuilder();
		
		this.listaComboIdVenta.clear();
		
		try {
			
			if(getCopaNvalor3() == 0 || getCopaNvalor3()== -1){
				
				habilitaComboIdVenta = false;
				habilitaPoliza		 = true;
				
				filtro.append("  and P.copaIdParametro = 'IDVENTA'");
				filtro.append("  and P.copaNvalor1 = 1 \n");
				filtro.append("  and P.copaNvalor2 = ").append(this.getCopaNvalor2()).append("\n");
				
				List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for(Parametros param: lista){
					this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
				}
				
				if (lista == null || lista.size() <= 0) habilitaComboIdVenta=true;
				
			}else{
				
				habilitaComboIdVenta = true;
				
				filtro2.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro2.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro2.append("  and P.copaNvalor1 = 1 \n");
				filtro2.append("  and P.copaNvalor2 = ").append(this.getCopaNvalor2()).append("\n");
				filtro2.append("  and P.copaNvalor3 = ").append(this.getCopaNvalor3()).append("\n");
				filtro2.append("order by P.copaNvalor3");
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro2.toString());
				
				for(Parametros item: listaPolizas){					
					if(item.getCopaNvalor4().toString().equals("1")){
						habilitaPoliza=false;
					}else{
						habilitaPoliza=true;
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	public void consultaProducto(){
		
		StringBuilder filtro  = new StringBuilder();
		
		this.listaComboProducto.clear();
		
		try {

			filtro.append(" and P.id.alprCdRamo = ").append(getCopaNvalor2());
			filtro.append(" and P.alprDato3 	= ").append(getValorIdVenta());
			
			List<Producto> lista = this.servicioProducto.obtenerObjetos(filtro.toString());
			
			for (Producto producto : lista) {
				this.listaComboProducto.add(new SelectItem(producto.getId().getAlprCdProducto(),producto.getId().getAlprCdProducto()+ " - "+producto.getAlprDeProducto()));
			}
			
			
		} catch (Exception e) {

			message.append("No se pueden cargar los Productos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	public String consultaPlanes(){
		
		StringBuilder filtro = new StringBuilder(100);
		getListaComboPlanes().clear();
		
		try {
	
			filtro.append(" and P.id.alplCdRamo 	 = ").append(getCopaNvalor2());
			filtro.append(" and P.id.alplCdProducto = ").append(getValorProducto());
			filtro.append(" ");
			
			List<Plan> listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
			
			
			for (Plan plan : listaPlanes) {
				
				getListaComboPlanes().add(new SelectItem(plan.getId().getAlplCdPlan(),plan.getId().getAlplCdPlan()+" - "+plan.getAlplDePlan()));
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	// termna filtros
	
	// fn fltros 
	
	
	
	///------------  REHABILITACION  --------
	
	public String altaParamRehabilicion(){
		
		try {
			
				
			Parametros parametros  = new Parametros();
			
			// asignar valor de radio 
						
			// Asiganamos valores a la parametrizacion 
		
			setCopaDesParametro("POLIZA");
			setCopaIdParametro("REHABILITACION");
			setCopaVvalor1( copaVvalor1 ); // "11"
			setCopaVvalor3( copaVvalor3 ); // "2"
			setCopaVvalor4( copaVvalor4 ); // verificar  "2"
			setCopaVvalor5( copaVvalor5 ); 
			setCopaVvalor5( copaVvalor6 ); 
			setCopaNvalor1( 1 ); // canal 
			setCopaNvalor2(copaNvalor2); // ramo 
			setCopaNvalor3(copaNvalor3); // poliza
		    setCopaNvalor4(copaNvalor4 );	// id venta	
			setCopaNvalor5( copaNvalor5 );// 3 antes
			setCopaNvalor6( copaNvalor6); // -1 antes
						
			// obtiene maximo de colectivos parametros
			
			setCopaCdParametro( this.servicioParametros.siguente());
			
			// no fuerza tener todos los parametros permte tener null			
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			parametros = ( Parametros ) ConstruirObjeto.crearObjeto(Parametros.class, this);	
			
			// verificar los datos 
							
			
			this.servicioParametros.guardarObjeto(parametros);


			setStrRespuesta("Se modifico con EXITO el registro.");
			
			return NavigationResults.SUCCESS;
		}
		
		catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.message.append(" La Parametrizacion  de Rehabilicion  ya existe para esta poliza.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}
		catch (Excepciones e) {

			e.printStackTrace();
			this.message.append("No se puede dar de alta la parametrizacion.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.FAILURE;
			
		}

	}

	
	//--------**************   PARAMETRIIZA RESTITUCION    -----------************
	
	public String altaParamRestitucion(){
		
		try {
			Parametros parametros  = new Parametros();
			
			setCopaDesParametro("POLIZA");
			setCopaIdParametro("RESTITUCION");
			setCopaVvalor7(copaVvalor7); // ?

			setCopaNvalor1(copaNvalor1); // canal 
			setCopaNvalor2(copaNvalor2); // ramo 
			setCopaNvalor3(copaNvalor3); // ? pol
			setCopaNvalor4(copaNvalor4); // id_venta 
			setCopaNvalor5(copaNvalor5 );
			setCopaNvalor6(copaNvalor6 );
			setCopaNvalor7(copaNvalor7 );
			setCopaNvalor8(copaNvalor8 );
			
			
			// setCopaNvalor5( 1 ); antes

			// obtiene maximo de colectivos parametros
			setCopaCdParametro( this.servicioParametros.siguente());

			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			parametros = ( Parametros ) ConstruirObjeto.crearObjeto(Parametros.class, this);	
			
			this.servicioParametros.guardarObjeto(parametros);
			
			//}
			setStrRespuesta("Se modifico con EXITO el registro.");

			return NavigationResults.SUCCESS;
			
		}
		
		catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.message.append(" La Parametrizacion  de RESTITUCION ya existe para esta poliza.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}
		catch (Excepciones e) {

			e.printStackTrace();
			this.message.append("No se puede dar de alta la parametrizacion.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.FAILURE;
			
		}
	
	}
	
		
	//--------------------------------------------------------------------------
	

//--------**************   PARAMETRIIZA SALDO    -----------************

public String altaParamSaldo(){
	
	try {
		
		Parametros parametros  = new Parametros();
		
		
	   // Asiganamos valores a la parametrizacion 
		
		setCopaDesParametro("POLIZA");
		setCopaIdParametro("SALDOINSOLUTO");
		setCopaVvalor1(copaVvalor1 );   // "0.0001"); antes 
		setCopaVvalor2( copaVvalor2 );  // "-1" ); antes  
		setCopaVvalor4( copaVvalor4 );  
		setCopaVvalor6( copaVvalor6 );  
		
		setCopaNvalor1(copaNvalor1); // canal 
		setCopaNvalor2(copaNvalor2); // ramo 
		setCopaNvalor3(copaNvalor3); // ? pol
		setCopaNvalor4( 0 );   //?
		//setCopaNvalor6(copaNvalor6); // id_venta antes  
	
		
		// obtiene maximo de colectivos parametros
		
		setCopaCdParametro( this.servicioParametros.siguente());
					
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
		parametros = ( Parametros ) ConstruirObjeto.crearObjeto(Parametros.class, this);	
		
	
		this.servicioParametros.guardarObjeto(parametros);
		
		setStrRespuesta("Se modifico con EXITO el registro.");

		return NavigationResults.SUCCESS;
	}
	
	catch (ObjetoDuplicado e) {
		e.printStackTrace();
		this.message.append(" La Parametrizacion  de SALDO INSOLUTO ya existe para esta poliza.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.RETRY;
	}
	catch (Excepciones e) {

		e.printStackTrace();
		this.message.append("No se puede dar de alta la parametrizacion.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.FAILURE;
		
	}

}


//--------------------------------------------------------------------------

//--------**************   PARAMETRIIZA DEVOLUCION   -----------************

public String altaParamDevolucion(){
	
	try {
				
		Parametros parametros  = new Parametros();
		
		setCopaDesParametro("DEVOLUCION");
		setCopaIdParametro("MODELO");
		setCopaVvalor1( "*" );
		setCopaVvalor2( "*" ); 

		setCopaNvalor1(copaNvalor1); // canal 
		setCopaNvalor2(copaNvalor2); // ramo 
		setCopaNvalor3(copaNvalor3); // Id venta 
		setCopaNvalor4(copaNvalor4  );		 // param tpo rest  no se que valor toma este campo
		setCopaNvalor5( copaNvalor5 ); //  1 antes 
	
		
		// obtiene maximo de colectivos parametros
		
		setCopaCdParametro( this.servicioParametros.siguente());
		
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
		parametros = ( Parametros ) ConstruirObjeto.crearObjeto(Parametros.class, this);	
		

		this.servicioParametros.guardarObjeto(parametros);
		
		setStrRespuesta("Se modifico con EXITO el registro.");

		return NavigationResults.SUCCESS;
	}
	
	catch (ObjetoDuplicado e) {
		e.printStackTrace();
		this.message.append(" La Parametrizacion  de DEVOLUCION  ya existe para esta poliza.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.RETRY;
	}
	catch (Excepciones e) {

		e.printStackTrace();
		this.message.append("No se puede dar de alta la parametrizacion.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.FAILURE;
		
	}

}

//--------**************   PARAMETRIIZA FACTURACION   -----------************

public String altaParamFacturacion(){
	
	try {
		
		Parametros parametros  = new Parametros();
		
			// Asiganamos valores a la parametrizacion 
	
		setCopaDesParametro("POLIZA");
		setCopaIdParametro("GEPREFAC");
		setCopaVvalor4( copaVvalor4); // antes "10000" 
		setCopaVvalor5( copaVvalor5 ); // antes "1"
		setCopaVvalor6( copaVvalor6 ); // antes  "1" 
		setCopaVvalor7( copaVvalor7 ); // antes  "0"
		setCopaVvalor8( copaVvalor8  ); // antes  "0"

		setCopaNvalor1(1); // canal 
		setCopaNvalor2(copaNvalor2); // ramo 
		setCopaNvalor3(copaNvalor3); // poliza
	    setCopaNvalor4( 0 );	// 0 antes	
		setCopaNvalor5( 0 );  
	//	setCopaNvalor6(copaNvalor6);	// Id venta 
		setCopaNvalor7( 0.00 );	
		setCopaNvalor8( 0.00 );	
		
		// obtiene maximo de colectivos parametros
		
		setCopaCdParametro( this.servicioParametros.siguente());
			
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
		parametros = ( Parametros ) ConstruirObjeto.crearObjeto(Parametros.class, this);	
		

		this.servicioParametros.guardarObjeto(parametros);
		
		setStrRespuesta("Se modifico con EXITO el registro.");

		return NavigationResults.SUCCESS;
	}
	
	catch (ObjetoDuplicado e) {
		e.printStackTrace();
		this.message.append(" La Parametrizacion  de FACTURACION  ya existe para esta poliza.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.RETRY;
	}
	catch (Excepciones e) {

		e.printStackTrace();
		this.message.append("No se puede dar de alta la parametrizacion.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.FAILURE;
		
	}

}

//--------------------------------------------------------------------------

public String altaParamObligado(){
	
	try {
		
		Parametros parametros  = new Parametros();
		
			// Asiganamos valores a la parametrizacion 
	
		setCopaDesParametro("POLIZA");
		setCopaIdParametro("OBLIGADOS");
		setCopaVvalor1( copaVvalor1); // 1,2
		setCopaVvalor2( copaVvalor2); // consecutivo
		setCopaVvalor3( copaVvalor3); //M
		
		setCopaNvalor1(1); // canal 
		setCopaNvalor2(copaNvalor2); // ramo 
		setCopaNvalor3(copaNvalor3); // poliza
	    setCopaNvalor4( copaNvalor4 );	// id venta 
	
		
		// obtiene maximo de colectivos parametros
		
		setCopaCdParametro( this.servicioParametros.siguente());
		
		// no fuerza tener todos los parametros permte tener null			
		BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
		parametros = ( Parametros ) ConstruirObjeto.crearObjeto(Parametros.class, this);	
		

		this.servicioParametros.guardarObjeto(parametros);
		
		setStrRespuesta("Se modifico con EXITO el registro.");

		return NavigationResults.SUCCESS;
	}
	
	catch (ObjetoDuplicado e) {
		e.printStackTrace();
		this.message.append(" La Parametrizacion  de FACTURACION  ya existe para esta poliza.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.RETRY;
	}
	catch (Excepciones e) {

		e.printStackTrace();
		this.message.append("No se puede dar de alta la parametrizacion.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		Utilerias.resetMessage(message);
		
		return NavigationResults.FAILURE;
		
	}

}




	public String disparaParametros(){
		try
		{
			
			  if(isnRehabilitacionChk() == true && !getRehabilitacionChk().isDisabled())
				  {
				  // validar que no exista el registro
				  
				  altaParamRehabilicion();
				  }
			  
			  if(isnEndosoChk() == true && !getEndosoChk().isDisabled()) {
				  altaParamEndoso();
		      }
			  
			  
			  if(isnSaldoChk() == true && !getSaldoChk().isDisabled()) {
				  altaParamSaldo();
			   }
			  

			  if(isnRestitucionChk() == true && !getRestitucionChk().isDisabled()) {
				  altaParamRestitucion();
			  }
			  

			  if(isnDevolucionChk() == true && !getDevolucionChk().isDisabled()) {
				  altaParamDevolucion();
			  }
			  
			  

			  if(isnFacturacionChk() == true && !getFacturacionChk().isDisabled()) {
				  altaParamFacturacion();
			  }
			  
		
			
			return NavigationResults.SUCCESS;
			
		}
			
		catch (Exception e) {
			log.error("Error al enviar generar la parametrizacion",e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
			
			return NavigationResults.FAILURE;
		}
	}


	
	
	public void listaParametrosOperaciones(){
		
		StringBuilder filtro = new StringBuilder();
		
		Map<String, HtmlSelectBooleanCheckbox> propiedades = new HashMap<String, HtmlSelectBooleanCheckbox>();
		
		propiedades.put("REHABILITACION", getRehabilitacionChk());
		propiedades.put("ENDOSO"		, getEndosoChk());
		propiedades.put("RESTITUCION"	, getRestitucionChk());
		propiedades.put("SALDOINSOLUTO"	, getSaldoChk());
		propiedades.put("MODELO"		, getDevolucionChk());
		propiedades.put("GEPREFAC"		, getFacturacionChk());
		
		for (String key: propiedades.keySet()) {
			propiedades.get(key).setSelected(false);
			propiedades.get(key).setDisabled(false);
		}
		
		try {

			filtro.append("  and P.copaDesParametro  in ('POLIZA','DEVOLUCION')");
			filtro.append("  and P.copaNvalor2 = ").append(this.copaNvalor2);
			filtro.append("  and P.copaNvalor3 = ").append(this.copaNvalor3).append("\n");
				
			List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
						
			for(Parametros param: lista){
				
				if(propiedades.containsKey(param.getCopaIdParametro())){
					propiedades.get(param.getCopaIdParametro()).setSelected(true);
					propiedades.get(param.getCopaIdParametro()).setDisabled(true);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}		
		
	}
	
	
	public String cargaProductos(){
		this.message.append("cargando productos.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder filtro = new StringBuilder(100);
		
		this.listaComboProducto.clear();
		
		if(this.copaNvalor2 != null){
			try {
		
				filtro.append(" and P.id.alprCdRamo = ").append(this.copaNvalor2.intValue()).append("\n");
				List<Producto> lista = this.servicioProducto.obtenerObjetos(filtro.toString());
				
				for(Producto producto: lista){
					this.listaComboProducto.add(new SelectItem(producto.getId().getAlprCdProducto(), producto.getId().getAlprCdProducto().toString()+ " - " + producto.getAlprDeProducto()));
				 }
				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				message.append("No se pueden cargar los productos.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				return NavigationResults.FAILURE;
			}
		} else{
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			return NavigationResults.RETRY;
		}
	}
	
	// Consulta los valores de la parametrizacion por concepto 
		
	public String consultarPrarametros(){

		String filtro = "";
		hmResultados = new HashMap<String, Object>();
		Parametros bean ;
		

		this.listaParametros.clear();
		
		try {
			if(getCopaNvalor2() > 0){
				
				filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n";
				filtro = " and P.copaNvalor3 = " + getCopaNvalor3() + "\n";
				
						
				List<Parametros> lstParametros = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lstParametros) {
					
					
					getListaParametros().add(parametros);
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
					
					
					hmResultados.put(bean.getCopaCdParametro().toString(), bean);
				}

			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
	
	
	
	// dipara los conceptos para consultas 
	
	
	
	public String disparaConceptos(){
		
		try
		{
			
			if(getValorIdVenta() != 0){
				calculaPol (getCopaNvalor3() , getValorIdVenta(),  getValorProducto(), getValorPlan());
			}
							
			consultarPrarametroRehabilitacion();
			consultarPrarametroEndoso();
			consultarPrarametroRestitucion();
			consultarPrarametroSaldo();
			consultarPrarametroDevolucion();
			consultarPrarametroFacturacion();
			consultarPrarametroObligados();
			
			//consultarParametrosExtra();
						
			return NavigationResults.SUCCESS;
			
		}
			
		catch (Exception e) {
			log.error("Error al enviar generar la parametrizacion",e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
			
			return NavigationResults.FAILURE;
		}
	}
	
	
	// Alta de  conceptos 
	
		// ENDOSOS
	
	public String altaParamEndoso(){
				
				try {
					
					Parametros parametros  = new Parametros();
					
					
					// Asiganamos valores a la parametrizacion 
					
					// asignar valor de radio 
					
					setCopaVvalor3(copaVvalor3); // endosoEmi
					setCopaVvalor4(copaVvalor4); // endosoRen
					setCopaVvalor5(copaVvalor5); // endosoCanc
					setCopaNvalor1(1); // canal 
					setCopaNvalor2(copaNvalor2); // ramo 
					setCopaNvalor3(copaNvalor3); // pol a 3 digitos
					setCopaNvalor4(copaNvalor4);
					setCopaNvalor5(copaNvalor5); // endosoTC
					setCopaDesParametro("POLIZA");
					setCopaIdParametro("ENDOSO");
					setCopaNvalor6( 1 );
					setCopaNvalor7( 1.00 );
					
					
					// obtiene maximo de colectivos parametros
					
					setCopaCdParametro( this.servicioParametros.siguente());
					
					
					// no fuerza tener todos los parametros permte tener null			
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					parametros = ( Parametros ) ConstruirObjeto.crearObjeto(Parametros.class, this);	
					
								
					this.servicioParametros.guardarObjeto(parametros);
					
					setStrRespuesta("Se modifico con EXITO el registro.");
					return NavigationResults.SUCCESS;
		
					
				}
				catch (ObjetoDuplicado e) {
					e.printStackTrace();
					this.message.append(" La Parametrizacion  de Endoso ya existe para esta poliza.");
					this.log.error(message.toString(), e);
					FacesUtils.addErrorMessage(message.toString());
					Utilerias.resetMessage(message);
					
					return NavigationResults.RETRY;
				}
				catch (Excepciones e) {
		
					e.printStackTrace();
					this.message.append("No se puede dar de alta la parametrizacion.");
					this.log.error(message.toString(), e);
					FacesUtils.addErrorMessage(message.toString());
					Utilerias.resetMessage(message);
					
					return NavigationResults.FAILURE;
					
				}
			
		}
	
	
	
	
	
	
	//------  consulta Conceptos --- //

	// REHABILITACION 
	public String consultarPrarametroRehabilitacion() throws Excepciones{

		String filtro = "";
		hmResultados = new HashMap<String, Object>();
		Parametros bean ;
		
		
		this.listaParametrosRehabilitacion.clear();
		
		try {
			if(getCopaNvalor2() > 0){
				
				filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+
				         " and P.copaNvalor3 = " + getCopaNvalor3() + "\n"+
				         " and P.copaIdParametro = 'REHABILITACION'"+ "\n"+
				         " and P.copaNvalor1 = 1 \n";
				
				List<Parametros> lstRehabilitacion = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lstRehabilitacion) {
					
					//this.listaParametros.add(new SelectItem(producto.getId().getAlprCdProducto().toString(), producto.getAlprDeProducto()));
					getListaParametrosRehabilitacion().add(parametros);
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
					
					
					hmResultados.put(bean.getCopaCdParametro().toString(), bean);
					
					
				}
				
				setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");

			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
	
    // ENDOSO
	public String consultarPrarametroEndoso(){

	String filtro = "";
	hmResultadosEndoso = new HashMap<String, Object>();
	Parametros bean ;
	

	this.listaParametrosEndoso.clear();
	
	
	try {
		if(getCopaNvalor2() > 0){
			
			if (this.getValorIdVenta() != 0){
			
			filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+
			         " and P.copaNvalor3 =  0 " + "\n"+
			         " and P.copaNvalor4 = " + getValorIdVenta() + "\n" +
			         " and P.copaIdParametro = 'ENDOSO'"+ "\n"+
			         " and P.copaNvalor1 = 1 \n";
			
			}
			else {
				filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+
		         " and P.copaNvalor3 = " + getCopaNvalor3()+ "\n"+
		         " and P.copaNvalor4 = " + getValorIdVenta() + "\n" +
		         " and P.copaIdParametro = 'ENDOSO'"+ "\n"+
		         " and P.copaNvalor1 = 1 \n";
		
			}
			
					
			List<Parametros> lstEndosos = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			for (Parametros parametros : lstEndosos) {
				
				
				this.getListaParametrosEndoso().add(parametros);
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
				
				
				hmResultadosEndoso.put(bean.getCopaCdParametro().toString(), bean);
			}
			
			setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");

		} else {
			setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
		}
		
		return NavigationResults.SUCCESS;
		
	} catch (Exception e) {
		this.message.append("No se pueden recuperar los parametros.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		
		return NavigationResults.FAILURE;
	}
}
	public void modificar(){
		try {
		
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActualE) ;
			
				    	
			servicioParametros.actualizarObjeto(parametro);
			
			hmResultadosEndoso.remove(objActualE.getCopaCdParametro().toString());
			hmResultadosEndoso.put(objActualE.getCopaCdParametro().toString(), objActualE);
			
			
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	} 
	
	//RESTITUCION
	// RESTITUCION 
	
	
	public String consultarPrarametroRestitucion(){

	String filtro = "";
	hmResultadosRestitucion = new HashMap<String, Object>();
	Parametros bean ;
	

	this.listaParametrosRestitucion.clear();
	
	try {
		if(getCopaNvalor2() > 0){
			
			
			filtro = " and P.copaNvalor1 = 1 \n "+
					 " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+
					 " and P.copaNvalor3 = 0  \n" + // Poliza en cero
			         " and P.copaNvalor4 = " + getValorIdVenta() + "\n"+
			         " and P.copaIdParametro = 'RESTITUCION'"+ "\n";
			
			System.out.println("filtro"+ filtro );
					
			List<Parametros> lstRestitucion = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			for (Parametros parametros : lstRestitucion) {
				
				
				this.getListaParametrosRestitucion().add(parametros);
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
				
				
				hmResultadosRestitucion.put(bean.getCopaCdParametro().toString(), bean);
				
			}
			
			setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");

		} else {
			setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
		}
		
		return NavigationResults.SUCCESS;
		
	} catch (Exception e) {
		this.message.append("No se pueden recuperar los parametros.");
		this.log.error(message.toString(), e);
		FacesUtils.addErrorMessage(message.toString());
		
		return NavigationResults.FAILURE;
	}
}
	
	// SALDO INSOLUTO
	
	
	//SALDO
	public String consultarPrarametroSaldo(){

		String filtro = "";
		hmResultadosSaldo = new HashMap<String, Object>();
		Parametros bean ;
		

		this.listaParametrosSaldo.clear();
		
		try {
			if(getCopaNvalor2() > 0){
				
				
				if (getValorIdVenta()!= 0 ){
				
				filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+	
				         " and P.copaNvalor3 =  SUBSTR("+ getCopaNvalor3() + ",1,3) \n"+
				         " and P.copaIdParametro = 'SALDOINSOLUTO' \n";
				
				System.out.println("filtro unica saldo "+ filtro );
				}
				else{
					filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+	
			         		 " and P.copaNvalor3 = "+ getCopaNvalor3() +" \n"+
			         		 " and P.copaIdParametro = 'SALDOINSOLUTO' \n";
					System.out.println("filtro recurrente saldo"+ filtro );
				}
						
				List<Parametros> lstSaldo = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lstSaldo) {
					
					
					this.getListaParametrosSaldo().add(parametros);
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
					
					
					hmResultadosSaldo.put(bean.getCopaCdParametro().toString(), bean);
				}
				
				setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");

			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
		
	
	//DEVOLUCION
	// DIFERENCIA EN PRIMAS EMITIDAS
		
	public String consultarPrarametroDevolucion(){

		String filtro = "";
		hmResultadosDevolucion = new HashMap<String, Object>();
		Parametros bean ;
		

		this.listaParametrosDevolucion.clear();
		
		try {
			if(getCopaNvalor2() > 0){
				
				filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+ // ramo 
				         " and P.copaNvalor3 = 0 \n"+ // no trae poliza
				         " and P.copaNvalor4 = " + getValorIdVenta()+ "\n"+ // ramo
				         " and P.copaDesParametro = 'DEVOLUCION'"+ "\n" +
				         " and P.copaIdParametro = 'MODELO'"+ "\n";
							
				
				System.out.println("filtro"+ filtro );
						
				List<Parametros> lstDevolucion  = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lstDevolucion) {
					
					
					this.getListaParametrosDevolucion().add(parametros);
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
					
					
					hmResultadosDevolucion.put(bean.getCopaCdParametro().toString(), bean);
				}
				
				setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");

			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
	
	// FACTURACION
	
	
	//FACTURACION
	public String consultarPrarametroFacturacion(){

		String filtro = "";
		hmResultadosFacturacion = new HashMap<String, Object>();
		Parametros bean ;
		

		this.listaParametrosFacturacion.clear();
		
		try {
			if(getCopaNvalor2() > 0){
				
				 if ( getValorIdVenta()!= 0){
					 
				filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+ // ramo
				         " and P.copaNvalor3 = SUBSTR("+ getCopaNvalor3() +",1,3) \n"+ // POLIZA a 3 digitos 
				         " and P.copaIdParametro = 'GEPREFAC'"+ "\n";  //  concepto
				
				System.out.println("filtro unica fact"+ filtro );
				
				 }
				 else {
					 filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+ // ramo
			                  " and P.copaNvalor3 = " + getCopaNvalor3() + "\n"+ // poliza
			                  " and P.copaIdParametro = 'GEPREFAC'"+ "\n";  //  concepto
					 System.out.println("filtro recurrente fact  "+ filtro );
					 
				 }
				 
						
				List<Parametros> lstFacturacion = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lstFacturacion) {
					
					
					this.getListaParametrosFacturacion().add(parametros);
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
					
					
					hmResultadosFacturacion.put(bean.getCopaCdParametro().toString(), bean);
				}
				
				setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");

			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
	
	
	//OBLIGADOS
	public String consultarPrarametroObligados(){

		String filtro = "";
		hmResultadosObligados = new HashMap<String, Object>();
		Parametros bean ;
		

		this.listaParametrosObligados.clear();
		
		
		try {
			if(getCopaNvalor2() > 0){
				
				filtro = " and P.copaNvalor2 = " + getCopaNvalor2() + "\n"+ // ramo
				         " and P.copaNvalor3 = " + getCopaNvalor3() + "\n"+ // poliza
				         " and P.copaIdParametro = 'OBLIGADOS'"+ "\n";  //  concepto
				
				System.out.println("filtro"+ filtro );
						
				List<Parametros> lstObligados = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lstObligados) {
					
					
					this.getListaParametrosObligados().add(parametros);
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
					
					
					hmResultadosObligados.put(bean.getCopaCdParametro().toString(), bean);
				}
				
				setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");

			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
	
	// MAP VALUES
	//-- otros metodos
	
	
	
	public ArrayList<Object> getMapValuesEndoso(){ // ENDOSO
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(this.getHmResultadosEndoso().values());
        return lstValues;
    }
	
		//  Termina procesos de Endoso 
	
	public void modificarRehabilita(){
		try {
		
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual) ;
				    	
			servicioParametros.actualizarObjeto(parametro);
			
			hmResultados.remove(objActual.getCopaCdParametro().toString());
			hmResultados.put(objActual.getCopaCdParametro().toString(), objActual);
			
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	} 
	
	
	public void modificarRestitucion(){
		try {
		
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActualResti) ;
				    	
			servicioParametros.actualizarObjeto(parametro);
			
			hmResultadosRestitucion.remove(objActualResti.getCopaCdParametro().toString());
			hmResultadosRestitucion.put(objActualResti.getCopaCdParametro().toString(), objActualResti);
			
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	} 
	
	
	public void modificarSaldo(){
		try {
		
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActualSaldo) ;
			
				    	
			servicioParametros.actualizarObjeto(parametro);
			
			hmResultadosSaldo.remove(objActualSaldo.getCopaCdParametro().toString());
			hmResultadosSaldo.put(objActualSaldo.getCopaCdParametro().toString(), objActualSaldo);
			
			
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	} 
	
	
	public void modificarDevolucion(){
		try {
		
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActualDev) ;
				    	
			servicioParametros.actualizarObjeto(parametro);
			
			hmResultadosFacturacion.remove(objActualDev.getCopaCdParametro().toString());
			hmResultadosFacturacion.put(objActualDev.getCopaCdParametro().toString(), objActualDev);
			
			
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	} 
	
	
	public void modificarFacturacion(){
		try {
		
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActualFac) ;
				    	
			servicioParametros.actualizarObjeto(parametro);
			
			hmResultadosDevolucion.remove(objActualFac.getCopaCdParametro().toString());
			hmResultadosDevolucion.put(objActualFac.getCopaCdParametro().toString(), objActualFac);
			
			
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	} 
	
	// Modifica obligados
	
	public void modificarObligado(){
		try {
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActualObligado) ;
				    	
			servicioParametros.actualizarObjeto(parametro);
			
		    hmResultadosObligados.remove(objActualObligado.getCopaCdParametro().toString());
		    hmResultadosObligados.put(objActualObligado.getCopaCdParametro().toString(), objActualObligado);
			
			
			setStrRespuesta("Se modifico con EXITO el registro.");
			
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	} 
	
	//  func
	
	public void calculaPol(  Long CopaNvalor3,  Integer ValorIdVenta , Integer ValorProducto, Integer ValorPlan) throws Excepciones {
		// TODO Auto-generated method stub
		try {
			//private Integer Poliza;
			Integer poliza = null;

			consultaPlazo();
			
			  if(CopaNvalor3 !=null &&  CopaNvalor3 == 0){
				  poliza = Integer.parseInt(ValorIdVenta+""+Utilerias.agregarCaracteres(getPlazo().toString(), 1, '0', 1)+""+  Utilerias.agregarCaracteres(ValorProducto.toString(), 1, '0', 1));
			  }  
			  
			  setCopaNvalor3(new Long(poliza));
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede recuperar la polza.");
			this.log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	
	
	public String consultaPlazo(){
		String strPlazo;
		StringBuilder filtro = new StringBuilder(100);
		
		try {
	
			filtro.append(" and P.id.alplCdRamo 	= ").append(getCopaNvalor2());   //RAMO
			filtro.append(" and P.id.alplCdProducto = ").append(getValorProducto()); //PRODUCTO
			filtro.append(" and P.id.alplCdPlan		= ").append(getValorPlan());	 //PLAN
			filtro.append(" ");
			
			List<Plan> listaPlan = this.servicioPlan.obtenerObjetos(filtro.toString());
			strPlazo = ((Plan)listaPlan.get(0)).getAlplDato1();
			setPlazo(new Integer(strPlazo != null ? strPlazo : "0"));

			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	
	
	
	// terminan los conceptos de parametros
	
	
public ArrayList<Object> getMapValues(){ // REHABiLiTACiON
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados().values());
        return lstValues;
    }
	
	public ArrayList<Object> getMapValuesRestitucion(){ // REstitucion
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(this.getHmResultadosRestitucion().values());
        return lstValues;
    }
	
	public ArrayList<Object> getMapValuesSaldo(){ // Saldo
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(this.getHmResultadosSaldo().values());
        return lstValues;
    }
		
	public ArrayList<Object> getMapValuesDevolucion(){ // Devolucion
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(this.getHmResultadosDevolucion().values());
        return lstValues;
    }
	
	public ArrayList<Object> getMapValuesFactura(){ // Facturacion
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(this.getHmResultadosFacturacion().values());
        return lstValues;
    }
	public ArrayList<Object> getMapValuesObligado(){ // Facturacion
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(this.getHmResultadosObligados().values());
        return lstValues;
    }
	
	
	
	
	public void buscarFilaActual(ActionEvent event) { // rehabilitacion 	  
		
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  getListaParametros().iterator();
        while(it.hasNext()) {
        	Parametros objParametro = (Parametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActual = objParametro;
                break;
            }
        }
    }
	
	
	
	
	public void buscarFilaActualE(ActionEvent event) { // busca fila actual de endoso 
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        
        filaActualE = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  this.getListaParametrosEndoso().iterator();
        while(it.hasNext()) {
        	Parametros objParametro = (Parametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActualE = objParametro;
                break;
            }
        }
    }
	public void buscarFilaActualRestitucion(ActionEvent event) {  // busca fila actual de restitucion
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        
        filaActualResti = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  getListaParametros().iterator();
        while(it.hasNext()) {
        	Parametros objParametro = (Parametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActualResti = objParametro;
                break;
            }
        }
    }
	
	// GETTERS SETTERS 

	
	
	public void buscarFilaActualSaldo(ActionEvent event) {      // busca fila actual de saldo nsoluto
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        
        filaActualSaldo = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  this.getListaParametrosSaldo().iterator();
        while(it.hasNext()) {
        	Parametros objParametro = (Parametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActualSaldo = objParametro;
                break;
            }
        }
    }
	
	public void buscarFilaActualDevolucion(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        
        filaActualDev = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  this.getListaParametrosDevolucion().iterator();
        while(it.hasNext()) {
        	Parametros objParametro = (Parametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActualDev = objParametro;
                break;
            }
        }
    }
	
	public void buscarFilaActualFacturacion(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        
        filaActualFac = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  this.getListaParametrosFacturacion().iterator();
        while(it.hasNext()) {
        	Parametros objParametro = (Parametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActualFac = objParametro;
                break;
            }
        }
    }
	
	public void buscarFilaActualObligado(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        
        filaActualObligado = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  this.getListaParametrosObligados().iterator();
        while(it.hasNext()) {
        	Parametros objParametro = (Parametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActualObligado  = objParametro;
                break;
            }
        }
    }

	public String consultarPrarametrosExtra(){

		StringBuilder filtro = new StringBuilder();
		hmResultados = new HashMap<String, Object>();
		Parametros bean ;
		
		this.listaParametros.clear();

		try {
			if(getCopaNvalor2() > 0){
				
				filtro.append("  and P.copaDesParametro in ('POLIZA','RECIBO') \n");
				filtro.append("  and P.copaNvalor1 	   = 1 \n");
				filtro.append("  and P.copaNvalor2 = ").append(getCopaNvalor2()).append("\n"); //RAMO
				filtro.append("  and P.copaNvalor3 LIKE '").append(getValorIdVenta()).append("%").append(getValorProducto()).append("' \n");
				filtro.append("  and length(P.copaNvalor3) = 5 \n");
				filtro.append("order by P.copaNvalor3");
				
						
				List<Parametros> lstParametros = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lstParametros) {
					
					
					getListaParametros().add(parametros);
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (Parametros) ConstruirObjeto.crearBean(Parametros.class, parametros);
					
					
					hmResultados.put(bean.getCopaCdParametro().toString(), bean);
				}

			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
	
	// GETTERS SETTERS 
		public ServicioProducto getServicioProducto() {
			return servicioProducto;
		}
	
		public void setServicioProducto(ServicioProducto servicioProducto) {
			this.servicioProducto = servicioProducto;
		}
	
		public ServicioParametros getServicioParametros() {
			return servicioParametros;
		}
		
		public void setServicioParametros(ServicioParametros servicioParametros) {
			this.servicioParametros = servicioParametros;
		}
		
		public ServicioPlan getServicioPlan() {
			return servicioPlan;
		}
		
		public void setServicioPlan(ServicioPlan servicioPlan) {
			this.servicioPlan = servicioPlan;
		}
	
		public Boolean getHabilitaComboProducto() {
			return habilitaComboProducto;
		}
		public void setHabilitaComboProducto(Boolean habilitaComboProducto) {
			this.habilitaComboProducto = habilitaComboProducto;
		}
		public Long getCopaCdParametro() {
			return copaCdParametro;
		}
		public void setCopaCdParametro(Long copaCdParametro) {
			this.copaCdParametro = copaCdParametro;
		}
		public String getCopaDesParametro() {
			return copaDesParametro;
		}
		public void setCopaDesParametro(String copaDesParametro) {
			this.copaDesParametro = copaDesParametro;
		}
		public String getCopaIdParametro() {
			return copaIdParametro;
		}
		public void setCopaIdParametro(String copaIdParametro) {
			this.copaIdParametro = copaIdParametro;
		}
		public String getCopaVvalor1() {
			return copaVvalor1;
		}
		public void setCopaVvalor1(String copaVvalor1) {
			this.copaVvalor1 = copaVvalor1;
		}
		public String getCopaVvalor2() {
			return copaVvalor2;
		}
		public void setCopaVvalor2(String copaVvalor2) {
			this.copaVvalor2 = copaVvalor2;
		}
		public String getCopaVvalor3() {
			return copaVvalor3;
		}
		public void setCopaVvalor3(String copaVvalor3) {
			this.copaVvalor3 = copaVvalor3;
		}
		public String getCopaVvalor4() {
			return copaVvalor4;
		}
		public void setCopaVvalor4(String copaVvalor4) {
			this.copaVvalor4 = copaVvalor4;
		}
		public String getCopaVvalor5() {
			return copaVvalor5;
		}
		public void setCopaVvalor5(String copaVvalor5) {
			this.copaVvalor5 = copaVvalor5;
		}
		public String getCopaVvalor6() {
			return copaVvalor6;
		}
		public void setCopaVvalor6(String copaVvalor6) {
			this.copaVvalor6 = copaVvalor6;
		}
		public String getCopaVvalor7() {
			return copaVvalor7;
		}
		public void setCopaVvalor7(String copaVvalor7) {
			this.copaVvalor7 = copaVvalor7;
		}
		public String getCopaVvalor8() {
			return copaVvalor8;
		}


		public void setCopaVvalor8(String copaVvalor8) {
			this.copaVvalor8 = copaVvalor8;
		}


		public Integer getCopaNvalor1() {
			return copaNvalor1;
		}


		public void setCopaNvalor1(Integer copaNvalor1) {
			this.copaNvalor1 = copaNvalor1;
		}


		public Long getCopaNvalor2() {
			return copaNvalor2;
		}


		public void setCopaNvalor2(Long copaNvalor2) {
			this.copaNvalor2 = copaNvalor2;
		}

		public Long getCopaNvalor3() {
			return copaNvalor3;
		}

		public void setCopaNvalor3(Long copaNvalor3) {
			this.copaNvalor3 = copaNvalor3;
		}


		public Integer getCopaNvalor4() {
			return copaNvalor4;
		}


		public void setCopaNvalor4(Integer copaNvalor4) {
			this.copaNvalor4 = copaNvalor4;
		}



		public Integer getCopaNvalor5() {
			return copaNvalor5;
		}



		public void setCopaNvalor5(Integer copaNvalor5) {
			this.copaNvalor5 = copaNvalor5;
		}


		public Integer getCopaNvalor6() {
			return copaNvalor6;
		}


		public void setCopaNvalor6(Integer copaNvalor6) {
			this.copaNvalor6 = copaNvalor6;
		}


		public Double getCopaNvalor7() {
			return copaNvalor7;
		}


		public void setCopaNvalor7(Double copaNvalor7) {
			this.copaNvalor7 = copaNvalor7;
		}


		public Double getCopaNvalor8() {
			return copaNvalor8;
		}

		public void setCopaNvalor8(Double copaNvalor8) {
			this.copaNvalor8 = copaNvalor8;
		}


		public Date getCopaFvalor1() {
			return copaFvalor1;
		}


		public void setCopaFvalor1(Date copaFvalor1) {
			this.copaFvalor1 = copaFvalor1;
		}

		public Date getCopaFvalor2() {
			return copaFvalor2;
		}

		public void setCopaFvalor2(Date copaFvalor2) {
			this.copaFvalor2 = copaFvalor2;
		}

		public Date getCopaFvalor3() {
			return copaFvalor3;
		}

		public void setCopaFvalor3(Date copaFvalor3) {
			this.copaFvalor3 = copaFvalor3;
		}

		public Date getCopaFvalor4() {
			return copaFvalor4;
		}

		public void setCopaFvalor4(Date copaFvalor4) {
			this.copaFvalor4 = copaFvalor4;
		}

		public Date getCopaFvalor5() {
			return copaFvalor5;
		}

		public void setCopaFvalor5(Date copaFvalor5) {
			this.copaFvalor5 = copaFvalor5;
		}
		
		
		public List<Object> getListaParametros() {
			return listaParametros;
		}


		public void setListaParametros(List<Object> listaParametros) {
			this.listaParametros = listaParametros;
		}
		
		
		public List<BeanParamOperacion> getListaCheck() {
			return listaCheck;
		}


		public void setListaCheckn(List<BeanParamOperacion> listaCheck) {
			this.listaCheck = listaCheck;
		}
		
		
		
		public boolean isnRehabilitacionChk() {
			return nRehabilitacionChk;
		}


		public void setnRehabilitacionChk(boolean nRehabilitacionChk) {
			this.nRehabilitacionChk = nRehabilitacionChk;
		}


		public boolean isnRestitucionChk() {
			return nRestitucionChk;
		}


		public void setnRestitucionChk(boolean nRestitucionChk) {
			this.nRestitucionChk = nRestitucionChk;
		}


		public boolean isnSaldoChk() {
			return nSaldoChk;
		}


		public void setnSaldoChk(boolean nSaldoChk) {
			this.nSaldoChk = nSaldoChk;
		}


		public boolean isnDevolucionChk() {
			return nDevolucionChk;
		}


		public void setnDevolucionChk(boolean nDevolucionChk) {
			this.nDevolucionChk = nDevolucionChk;
		}


		public boolean isnFacturacionChk() {
			return nFacturacionChk;
		}


		public void setnFacturacionChk(boolean nFacturacionChk) {
			this.nFacturacionChk = nFacturacionChk;
		}

		
		public boolean isnEndosoChk() {
			return nEndosoChk;
		}


		public void setnEndosoChk(boolean nEndosoChk) {
			this.nEndosoChk = nEndosoChk;
		}

		
		public List<Object> getListaComboIdVenta() {
			return listaComboIdVenta;
		}
		public void setListaComboIdVenta(List<Object> listaComboIdVenta) {
			this.listaComboIdVenta = listaComboIdVenta;
		}
		
		public Boolean getHabilitaComboIdVenta() {
			return habilitaComboIdVenta;
		}
		public void setHabilitaComboIdVenta(Boolean habilitaComboIdVenta) {
			this.habilitaComboIdVenta = habilitaComboIdVenta;
		}
		
		public Boolean getHabilitaPoliza() {
			return habilitaPoliza;
		}
		public void setHabilitaPoliza(Boolean habilitaPoliza) {
			this.habilitaPoliza = habilitaPoliza;
		}
		
		
		public HtmlSelectBooleanCheckbox getRehabilitacionChk() {
			return rehabilitacionChk;
		}
		
		public void setRehabilitacionChk(HtmlSelectBooleanCheckbox rehabilitacionChk) {
			this.rehabilitacionChk = rehabilitacionChk;
		}
		
		public HtmlSelectBooleanCheckbox getEndosoChk() {
			return endosoChk;
		}
		public void setEndosoChk(HtmlSelectBooleanCheckbox endosoChk) {
			this.endosoChk = endosoChk;
		}
		public HtmlSelectBooleanCheckbox getRestitucionChk() {
			return restitucionChk;
		}
		public void setRestitucionChk(HtmlSelectBooleanCheckbox restitucionChk) {
			this.restitucionChk = restitucionChk;
		}
		public HtmlSelectBooleanCheckbox getSaldoChk() {
			return saldoChk;
		}
		public void setSaldoChk(HtmlSelectBooleanCheckbox saldoChk) {
			this.saldoChk = saldoChk;
		}
		public HtmlSelectBooleanCheckbox getDevolucionChk() {
			return devolucionChk;
		}
		
		public void setDevolucionChk(HtmlSelectBooleanCheckbox devolucionChk) {
			this.devolucionChk = devolucionChk;
		}
		public HtmlSelectBooleanCheckbox getFacturacionChk() {
			return facturacionChk;
		}
		public void setFacturacionChk(HtmlSelectBooleanCheckbox facturacionChk) {
			this.facturacionChk = facturacionChk;
		}


		public String getStrRespuesta() {
			return strRespuesta;
		}



		public void setStrRespuesta(String strRespuesta) {
			this.strRespuesta = strRespuesta;
		}

		public Integer getValorPlan() {
			return valorPlan;
		}



		public void setValorPlan(Integer valorPlan) {
			this.valorPlan = valorPlan;
		}


		public Integer getValorProducto() {
			return valorProducto;
		}



		public void setValorProducto(Integer valorProducto) {
			this.valorProducto = valorProducto;
		}

		public Integer getValorIdVenta() {
			return valorIdVenta;
		}



		public void setValorIdVenta(Integer valorIdVenta) {
			this.valorIdVenta = valorIdVenta;
		}
		
		
		public Integer getPolizaUnica() {
			return PolizaUnica;
		}



		public void setPolizaUnica(Integer polizaUnica) {
			PolizaUnica = polizaUnica;
		}


		public List<Object> getListaComboProducto() {
			return listaComboProducto;
		}



		public void setListaComboProducto(List<Object> listaComboProducto) {
			this.listaComboProducto = listaComboProducto;
		}
		
		public HtmlSelectOneMenu getInIdVenta() {
			return inIdVenta;
		}



		public void setInIdVenta(HtmlSelectOneMenu inIdVenta) {
			this.inIdVenta = inIdVenta;
		}

		
		public HashMap<String, Object> getHmResultados() {
			return hmResultados;
		}



		public void setHmResultados(HashMap<String, Object> hmResultados) {
			this.hmResultados = hmResultados;
		}

		
		public int getFilaActual() {
			return filaActual;
		}



		public void setFilaActual(int filaActual) {
			this.filaActual = filaActual;
		}

		public Parametros getObjActual() {
			return objActual;
		}

		public void setObjActual(Parametros objActual) {
			this.objActual = objActual;
		}
		public Parametros getObjNuevo() {
			return objNuevo;
		}
		public void setObjNuevo(Parametros objNuevo) {
			this.objNuevo = objNuevo;
		}
		public Set<Integer> getKeys() {
			return keys;
		}
		public void setKeys(Set<Integer> keys) {
			this.keys = keys;
		}
		public List<Object> getListaParametrosRehabilitacion() {
			return listaParametrosRehabilitacion;
		}
		public void setListaParametrosRehabilitacion(
				List<Object> listaParametrosRehabilitacion) {
			this.listaParametrosRehabilitacion = listaParametrosRehabilitacion;
		}
		public List<Object> getListaParametrosEmision() {
			return listaParametrosEmision;
		}
		public void setListaParametrosEmision(List<Object> listaParametrosEmision) {
			this.listaParametrosEmision = listaParametrosEmision;
		}
		public List<Object> getListaParametrosRestitucion() {
			return listaParametrosRestitucion;
		}
		public void setListaParametrosRestitucion(
				List<Object> listaParametrosRestitucion) {
			this.listaParametrosRestitucion = listaParametrosRestitucion;
		}



		public List<Object> getListaParametrosSaldo() {
			return listaParametrosSaldo;
		}



		public void setListaParametrosSaldo(List<Object> listaParametrosSaldo) {
			this.listaParametrosSaldo = listaParametrosSaldo;
		}



		public List<Object> getListaParametrosDevolucion() {
			return listaParametrosDevolucion;
		}



		public void setListaParametrosDevolucion(List<Object> listaParametrosDevolucion) {
			this.listaParametrosDevolucion = listaParametrosDevolucion;
		}



		public List<Object> getListaParametrosFacturacion() {
			return listaParametrosFacturacion;
		}



		public void setListaParametrosFacturacion(
				List<Object> listaParametrosFacturacion) {
			this.listaParametrosFacturacion = listaParametrosFacturacion;
		}

		public List<Object> getListaParametrosObligados() {
			return listaParametrosObligados;
		}
		public void setListaParametrosObligados(List<Object> listaParametrosObligados) {
			this.listaParametrosObligados = listaParametrosObligados;
		}
			
		public List<Object> getListaParametrosEndoso() {
			return listaParametrosEndoso;
		}

		public void setListaParametrosEndoso(List<Object> listaParametrosEndoso) {
			this.listaParametrosEndoso = listaParametrosEndoso;
		}
		public HashMap<String, Object> getHmResultadosEndoso() {
			return hmResultadosEndoso;
		}

		public void setHmResultadosEndoso(HashMap<String, Object> hmResultadosEndoso) {
			this.hmResultadosEndoso = hmResultadosEndoso;
		}

		public int getFilaActualE() {
			return filaActualE;
		}
		public void setFilaActualE(int filaActualE) {
			this.filaActualE = filaActualE;
		}
		public Parametros getObjActualE() {
			return objActualE;
		}
		public void setObjActualE(Parametros objActualE) {
			this.objActualE = objActualE;
		}
		public Parametros getObjNuevoE() {
			return objNuevoE;
		}
		public void setObjNuevoE(Parametros objNuevoE) {
			this.objNuevoE = objNuevoE;
		}

		public Set<Integer> getKeysEndoso() {
			return keysEndoso;
		}

		public void setKeysEndoso(Set<Integer> keysEndoso) {
			this.keysEndoso = keysEndoso;
		}

		public HashMap<String, Object> getHmResultadosRestitucion() {
			return hmResultadosRestitucion;
		}

		public void setHmResultadosRestitucion(
				HashMap<String, Object> hmResultadosRestitucion) {
			this.hmResultadosRestitucion = hmResultadosRestitucion;
		}

		public HashMap<String, Object> getHmResultadosDevolucion() {
			return hmResultadosDevolucion;
		}

		public void setHmResultadosDevolucion(
				HashMap<String, Object> hmResultadosDevolucion) {
			this.hmResultadosDevolucion = hmResultadosDevolucion;
		}

		public HashMap<String, Object> getHmResultadosSaldo() {
			return hmResultadosSaldo;
		}

		public void setHmResultadosSaldo(HashMap<String, Object> hmResultadosSaldo) {
			this.hmResultadosSaldo = hmResultadosSaldo;
		}

		public HashMap<String, Object> getHmResultadosFacturacion() {
			return hmResultadosFacturacion;
		}

		public void setHmResultadosFacturacion(
				HashMap<String, Object> hmResultadosFacturacion) {
			this.hmResultadosFacturacion = hmResultadosFacturacion;
		}

		public HashMap<String, Object> getHmResultadosObligados() {
			return hmResultadosObligados;
		}

		public void setHmResultadosObligados(
				HashMap<String, Object> hmResultadosObligados) {
			this.hmResultadosObligados = hmResultadosObligados;
		}

		public Parametros getObjActualResti() {
			return objActualResti;
		}

		public void setObjActualResti(Parametros objActualResti) {
			this.objActualResti = objActualResti;
		}

		public Parametros getObjNuevoResti() {
			return objNuevoResti;
		}

		public void setObjNuevoResti(Parametros objNuevoResti) {
			this.objNuevoResti = objNuevoResti;
		}

		public Set<Integer> getKeysResti() {
			return keysResti;
		}

		public void setKeysResti(Set<Integer> keysResti) {
			this.keysResti = keysResti;
		}

		public int getFilaActualResti() {
			return filaActualResti;
		}

		public void setFilaActualResti(int filaActualResti) {
			this.filaActualResti = filaActualResti;
		}
		
		public Parametros getObjActualSaldo() {
			return objActualSaldo;
		}

		public void setObjActualSaldo(Parametros objActualSaldo) {
			this.objActualSaldo = objActualSaldo;
		}

		public Parametros getObjNuevoSaldo() {
			return objNuevoSaldo;
		}

		public void setObjNuevoSaldo(Parametros objNuevoSaldo) {
			this.objNuevoSaldo = objNuevoSaldo;
		}

		public Set<Integer> getKeysSaldo() {
			return keysSaldo;
		}

		public void setKeysSaldo(Set<Integer> keysSaldo) {
			this.keysSaldo = keysSaldo;
		}

		public int getFilaActualSaldo() {
			return filaActualSaldo;
		}

		public void setFilaActualSaldo(int filaActualSaldo) {
			this.filaActualSaldo = filaActualSaldo;
		}

		public Parametros getObjActualDev() {
			return objActualDev;
		}

		public void setObjActualDev(Parametros objActualDev) {
			this.objActualDev = objActualDev;
		}

		public Parametros getObjNuevoDev() {
			return objNuevoDev;
		}

		public void setObjNuevoDev(Parametros objNuevoDev) {
			this.objNuevoDev = objNuevoDev;
		}

		public Set<Integer> getKeysDev() {
			return keysDev;
		}

		public void setKeysDev(Set<Integer> keysDev) {
			this.keysDev = keysDev;
		}

		public int getFilaActualDev() {
			return filaActualDev;
		}

		public void setFilaActualDev(int filaActualDev) {

			this.filaActualDev = filaActualDev;
		}
		
		public Parametros getObjActualFac() {
			return objActualFac;
		}

		public void setObjActualFac(Parametros objActualFac) {
			this.objActualFac = objActualFac;
		}
		
		public Parametros getObjActualObligado() {
			return objActualObligado;
		}

		public void setObjActualObligado(Parametros objActualObligado) {
			this.objActualObligado = objActualObligado;
		}
		public Parametros getObjNuevoObligado() {
			return objNuevoObligado;
		}
		public void setObjNuevoObligado(Parametros objNuevoObligado) {
			this.objNuevoObligado = objNuevoObligado;
		}
		public Set<Integer> getKeysObligado() {
			return keysObligado;
		}
		public void setKeysObligado(Set<Integer> keysObligado) {
			this.keysObligado = keysObligado;
		}
		public int getFilaActualObligado() {
			return filaActualObligado;
		}
		public void setFilaActualObligado(int filaActualObligado) {
			this.filaActualObligado = filaActualObligado;
		}

		public Parametros getObjNuevoFac() {
			return objNuevoFac;
		}
		public List<Object> getListaComboPlanes() {
			return listaComboPlanes;
		}

		public void setListaComboPlanes(List<Object> listaComboPlanes) {
			this.listaComboPlanes = listaComboPlanes;
		}
		public List<Object> getListaComboPolizas() {
			return listaComboPolizas;
		}
		public void setListaComboPolizas(List<Object> listaComboPolizas) {
			this.listaComboPolizas = listaComboPolizas;
		}
		public List<Plan> getListaPlanes() {
			return listaPlanes;
		}
		public void setListaPlanes(List<Plan> listaPlanes) {
			this.listaPlanes = listaPlanes;
		}

		public void setObjNuevoFac(Parametros objNuevoFac) {
			this.objNuevoFac = objNuevoFac;
		}

		public Set<Integer> getKeysFac() {
			return keysFac;
		}

		public void setKeysFac(Set<Integer> keysFac) {
			this.keysFac = keysFac;
		}

		public int getFilaActualFac() {
			return filaActualFac;
		}

		public void setFilaActualFac(int filaActualFac) {
			this.filaActualFac = filaActualFac;
		}


		
		public ProductoId getId() {
			return id;
		}



		public void setId(ProductoId id) {
			this.id = id;
		}



		public PlanId getIdPlan() {
			return idPlan;
		}



		public void setIdPlan(PlanId idPlan) {
			this.idPlan = idPlan;
		}
		
		public Integer getPlazo() {
			return plazo;
		}
		public void setPlazo(Integer plazo) {
			this.plazo = plazo;
		}
}







	