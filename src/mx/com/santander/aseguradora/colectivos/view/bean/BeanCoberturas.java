package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCoberturasId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartRamosPolizas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturasId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estatus;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class BeanCoberturas {

	@Resource
	private ServicioCartCoberturas servicioCartCoberturas;
	@Resource
	private ServicioColectivosCoberturas servicioColectivosCoberturas;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	//BEANS
	private PlanId idPlan;
	
	//-- COBERTURAS
	private ColectivosCoberturasId id;
	private Estatus alternaEstatus;
	private long cocbPolNuCobertura;
	private Date cocbFeDesde;
	private Date cocbFeHasta;
	private BigDecimal cocbMtComision;
	private BigDecimal cocbTaRiesgo;
	
	
	

	private BigDecimal cocbMtFijo;
	private BigDecimal cocbPoComision;
	private String cocbInSumarizacion;
	private String cocbSexo;
	private String cocbTpRiesgo;
	private BigDecimal cocbEdadMinima;
	private BigDecimal cocbEdadMaxima;
	private Date cocbFeBaja;
	private Long cocbCampon1;
	private BigDecimal cocbCampon2;
	private BigDecimal cocbCampon3;
	private BigDecimal cocbCampon4;
	private String cocbCampov1;
	private String cocbCampov2;
	private String cocbCampov3;
	private String cocbCampov4;
	private Date cocbCampof1;
	private Date cocbCampof2;
	private Date cocbCampof3;
	private Date cocbCampof4;
	
	private Integer idVenta;//idVenta
	private	Integer plazo;	//plazo
	private String descPlan;
	
	private List<Object> listaComboCoberturas;
	private List<ColectivosCoberturas> listaCoberturas;
	
	private String valComboCobertura;
	private String poliza;
	
	//--GuiasContables--
	private String numeroPoliza;
	private String ramoPoliza; 
	private String ramoReactor;
	private String ramoContable;
	private String cdCobertura;
	private String cobertura;
	private String centroCostos;
	private Double tarifa;
	private Double totalTarifas;
	
	private ColectivosCoberturas coberturas;
	private ColectivosCoberturas nwCoberturas;
	private List<CartCoberturas> listaCartCoberturas;
	
	private CartCoberturas	cartCoberturas;
	private CartCoberturasId idCartCoberturas;
	private CartRamosPolizas cartRamosPolizas;
	
	private String strRespuesta;
	
	private BeanParametrizacion bean;
	private Boolean habilitaComisiones;

	public BeanCoberturas() {

		this.idPlan 				= new PlanId();
		this.id						= new ColectivosCoberturasId();
		this.alternaEstatus 		= new Estatus();
		this.listaComboCoberturas 	= new ArrayList<Object>();
		this.listaCoberturas		= new ArrayList<ColectivosCoberturas>();
		this.coberturas				= new ColectivosCoberturas();
		this.listaCartCoberturas	= new ArrayList<CartCoberturas>();
		this.nwCoberturas			= new ColectivosCoberturas();
		this.cartCoberturas			= new CartCoberturas();
		this.idCartCoberturas		= new CartCoberturasId();
		this.cartRamosPolizas		= new CartRamosPolizas();
		this.message				= new StringBuilder();
		this.valComboCobertura		= "";
		this.totalTarifas 			= 0.0;
		this.nwCoberturas.setTipoComision("P");
		habilitaComisiones 			= false;

	}
	
	public ServicioCartCoberturas getServicioCartCoberturas() {
		return servicioCartCoberturas;
	}

	public void setServicioCartCoberturas(
			ServicioCartCoberturas servicioCartCoberturas) {
		this.servicioCartCoberturas = servicioCartCoberturas;
	}
	
	public ServicioColectivosCoberturas getServicioColectivosCoberturas() {
		return servicioColectivosCoberturas;
	}
	
	public void setServicioColectivosCoberturas(
			ServicioColectivosCoberturas servicioColectivosCoberturas) {
		this.servicioColectivosCoberturas = servicioColectivosCoberturas;
	}

	public PlanId getidPlan() {
		return idPlan;
	}	

	public void setidPlan(PlanId idPlan) {
		this.idPlan = idPlan;
	}
	
	//Coberturas
	public ColectivosCoberturasId getId() {
		return id;
	}

	public void setId(ColectivosCoberturasId id) {
		this.id = id;
	}

	public Estatus getAlternaEstatus() {
		return alternaEstatus;
	}

	public void setAlternaEstatus(Estatus alternaEstatus) {
		this.alternaEstatus = alternaEstatus;
	}

	public long getCocbPolNuCobertura() {
		return cocbPolNuCobertura;
	}

	public void setCocbPolNuCobertura(long cocbPolNuCobertura) {
		this.cocbPolNuCobertura = cocbPolNuCobertura;
	}

	public Date getCocbFeDesde() {
		return cocbFeDesde;
	}

	public void setCocbFeDesde(Date cocbFeDesde) {
		this.cocbFeDesde = cocbFeDesde;
	}

	public Date getCocbFeHasta() {
		return cocbFeHasta;
	}

	public void setCocbFeHasta(Date cocbFeHasta) {
		this.cocbFeHasta = cocbFeHasta;
	}

	public BigDecimal getCocbMtComision() {
		return cocbMtComision;
	}

	public void setCocbMtComision(BigDecimal cocbMtComision) {
		this.cocbMtComision = cocbMtComision;
	}

	public BigDecimal getCocbTaRiesgo() {
		return cocbTaRiesgo;
	}

	public void setCocbTaRiesgo(BigDecimal cocbTaRiesgo) {
		this.cocbTaRiesgo = cocbTaRiesgo;
	}

	public BigDecimal getCocbMtFijo() {
		return cocbMtFijo;
	}

	public void setCocbMtFijo(BigDecimal cocbMtFijo) {
		this.cocbMtFijo = cocbMtFijo;
	}

	public BigDecimal getCocbPoComision() {
		return cocbPoComision;
	}

	public void setCocbPoComision(BigDecimal cocbPoComision) {
		this.cocbPoComision = cocbPoComision;
	}

	public String getCocbInSumarizacion() {
		return cocbInSumarizacion;
	}

	public void setCocbInSumarizacion(String cocbInSumarizacion) {
		this.cocbInSumarizacion = cocbInSumarizacion;
	}

	public String getCocbSexo() {
		return cocbSexo;
	}

	public void setCocbSexo(String cocbSexo) {
		this.cocbSexo = cocbSexo;
	}

	public String getCocbTpRiesgo() {
		return cocbTpRiesgo;
	}

	public void setCocbTpRiesgo(String cocbTpRiesgo) {
		this.cocbTpRiesgo = cocbTpRiesgo;
	}

	public BigDecimal getCocbEdadMinima() {
		return cocbEdadMinima;
	}

	public void setCocbEdadMinima(BigDecimal cocbEdadMinima) {
		this.cocbEdadMinima = cocbEdadMinima;
	}

	public BigDecimal getCocbEdadMaxima() {
		return cocbEdadMaxima;
	}

	public void setCocbEdadMaxima(BigDecimal cocbEdadMaxima) {
		this.cocbEdadMaxima = cocbEdadMaxima;
	}

	public Date getCocbFeBaja() {
		return cocbFeBaja;
	}

	public void setCocbFeBaja(Date cocbFeBaja) {
		this.cocbFeBaja = cocbFeBaja;
	}

	public Long getCocbCampon1() {
		return cocbCampon1;
	}

	public void setCocbCampon1(Long cocbCampon1) {
		this.cocbCampon1 = cocbCampon1;
	}

	public BigDecimal getCocbCampon2() {
		return cocbCampon2;
	}

	public void setCocbCampon2(BigDecimal cocbCampon2) {
		this.cocbCampon2 = cocbCampon2;
	}

	public BigDecimal getCocbCampon3() {
		return cocbCampon3;
	}

	public void setCocbCampon3(BigDecimal cocbCampon3) {
		this.cocbCampon3 = cocbCampon3;
	}

	public BigDecimal getCocbCampon4() {
		return cocbCampon4;
	}

	public void setCocbCampon4(BigDecimal cocbCampon4) {
		this.cocbCampon4 = cocbCampon4;
	}

	public String getCocbCampov1() {
		return cocbCampov1;
	}

	public void setCocbCampov1(String cocbCampov1) {
		this.cocbCampov1 = cocbCampov1;
	}

	public String getCocbCampov2() {
		return cocbCampov2;
	}

	public void setCocbCampov2(String cocbCampov2) {
		this.cocbCampov2 = cocbCampov2;
	}

	public String getCocbCampov3() {
		return cocbCampov3;
	}

	public void setCocbCampov3(String cocbCampov3) {
		this.cocbCampov3 = cocbCampov3;
	}

	public String getCocbCampov4() {
		return cocbCampov4;
	}

	public void setCocbCampov4(String cocbCampov4) {
		this.cocbCampov4 = cocbCampov4;
	}

	public Date getCocbCampof1() {
		return cocbCampof1;
	}

	public void setCocbCampof1(Date cocbCampof1) {
		this.cocbCampof1 = cocbCampof1;
	}

	public Date getCocbCampof2() {
		return cocbCampof2;
	}

	public void setCocbCampof2(Date cocbCampof2) {
		this.cocbCampof2 = cocbCampof2;
	}

	public Date getCocbCampof3() {
		return cocbCampof3;
	}

	public void setCocbCampof3(Date cocbCampof3) {
		this.cocbCampof3 = cocbCampof3;
	}

	public Date getCocbCampof4() {
		return cocbCampof4;
	}

	public void setCocbCampof4(Date cocbCampof4) {
		this.cocbCampof4 = cocbCampof4;
	}
	
	public List<Object> getListaComboCoberturas() {
		return listaComboCoberturas;
	}
	
	public void setListaComboCoberturas(List<Object> listaComboCoberturas) {
		this.listaComboCoberturas = listaComboCoberturas;
	}
	
	public String getValComboCobertura() {
		return valComboCobertura;
	}
	
	//LPV
	public List<ColectivosCoberturas> getListaCoberturas() {
		return listaCoberturas;
	}
	
	public void setListaCoberturas(List<ColectivosCoberturas> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	
	public void setValComboCobertura(String valComboCobertura) {
		this.valComboCobertura = valComboCobertura;
	}
	
	public Integer getIdVenta() {
		return idVenta;
	}
	
	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}
	
	public Integer getPlazo() {
		return plazo;
	}
	
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	
	public String getDescPlan() {
		return descPlan;
	}
	
	public void setDescPlan(String descPlan) {
		this.descPlan = descPlan;
	}
	
	public String getPoliza() {
		return poliza;
	}
	
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	public ColectivosCoberturas getCoberturas() {
		return coberturas;
	}
	public void setCoberturas(ColectivosCoberturas coberturas) {
		this.coberturas = coberturas;
	}
	
	public List<CartCoberturas> getListaCartCoberturas() {
		return listaCartCoberturas;
	}
	
	public void setListaCartCoberturas(List<CartCoberturas> listaCartCoberturas) {
		this.listaCartCoberturas = listaCartCoberturas;
	}
	
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	
	public BeanParametrizacion getBean() {
		return bean;
	}
	public void setBean(BeanParametrizacion bean) {
		this.bean = bean;
	}
	
	public ColectivosCoberturas getNwCoberturas() {
		return nwCoberturas;
	}
	
	public void setNwCoberturas(ColectivosCoberturas nwCoberturas) {
		this.nwCoberturas = nwCoberturas;
	}
	
	public CartCoberturas getCartCoberturas() {
		return cartCoberturas;
	}
	public void setCartCoberturas(CartCoberturas cartCoberturas) {
		this.cartCoberturas = cartCoberturas;
	}
	
	public CartCoberturasId getIdCartCoberturas() {
		return idCartCoberturas;
	}
	public void setIdCartCoberturas(CartCoberturasId idCartCoberturas) {
		this.idCartCoberturas = idCartCoberturas;
	}
	
	public CartRamosPolizas getCartRamosPolizas() {
		return cartRamosPolizas;
	}
	public void setCartRamosPolizas(CartRamosPolizas cartRamosPolizas) {
		this.cartRamosPolizas = cartRamosPolizas;
	}
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getRamoPoliza() {
		return ramoPoliza;
	}

	public void setRamoPoliza(String ramoPoliza) {
		this.ramoPoliza = ramoPoliza;
	}

	public String getRamoReactor() {
		return ramoReactor;
	}

	public void setRamoReactor(String ramoReactor) {
		this.ramoReactor = ramoReactor;
	}

	public String getRamoContable() {
		return ramoContable;
	}

	public void setRamoContable(String ramoContable) {
		this.ramoContable = ramoContable;
	}

	public String getCdCobertura() {
		return cdCobertura;
	}

	public void setCdCobertura(String cdCobertura) {
		this.cdCobertura = cdCobertura;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public String getCentroCostos() {
		return centroCostos;
	}

	public void setCentroCostos(String centroCostos) {
		this.centroCostos = centroCostos;
	}

	public Double getTarifa() {
		return tarifa;
	}

	public void setTarifa(Double tarifa) {
		this.tarifa = tarifa;
	}

	public Double getTotalTarifas() {
		return totalTarifas;
	}

	public void setTotalTarifas(Double totalTarifas) {
		this.totalTarifas = totalTarifas;
	}
	
	
	//ACCIONES
	public String consultaCoberturas(){
		StringBuilder filtro = new StringBuilder(100);
		Long poliza;
		
		getListaComboCoberturas().clear();
		poliza = (long) 16100928;
		
		
		if(getPoliza()!=null && getPoliza().equals("0")){
			poliza = Long.parseLong(getIdVenta()+""+getPlazo()+""+getId().getCocbCapuCdProducto());
		}else{
			poliza = Long.parseLong(getPoliza());
		}
		
		getId().setCocbCapoNuPoliza(poliza);
		
		try {
	
			filtro.append(" and P.cartRamosPolizas.carpCdRamo = ").append(getId().getCocbCarpCdRamo());
			filtro.append(" ");
			
			List<CartCoberturas> listaCartCoberturas = this.servicioCartCoberturas.obtenerObjetos(filtro.toString());
			
			for (CartCoberturas cartCoberturas : listaCartCoberturas) {
				
				this.listaComboCoberturas.add(new SelectItem( cartCoberturas.getId().getCacbCarbCdRamo()+"-"+
						cartCoberturas.getId().getCacbCdCobertura(), 
						cartCoberturas.getId().getCacbCarbCdRamo()+"-"+cartCoberturas.getId().getCacbCdCobertura()+"-"+
						cartCoberturas.getCacbDeCobertura().toString()));
			}
			
			this.consultaCoberturasPorPlan();
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	
	public String consultaPropiedadesCoberturas(){
		StringBuilder filtro = new StringBuilder(100);
		
		String ramoContable = getValComboCobertura()!=null?getValComboCobertura().substring(0,getValComboCobertura().length()-4):"";
		String cdCobertura  = getValComboCobertura()!=null?getValComboCobertura().substring(getValComboCobertura().length()-3):"";
		
		try {
	
			filtro.append(" and P.cartRamosPolizas.carpCdRamo = ").append(getId().getCocbCarpCdRamo());
			filtro.append(" and P.id.cacbCarbCdRamo  = ").append(ramoContable);
			filtro.append(" and P.id.cacbCdCobertura = ").append(cdCobertura);
			filtro.append(" ");
			
			List<CartCoberturas> listaCartCoberturas = this.servicioCartCoberturas.obtenerObjetos(filtro.toString());
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

	
	public String guardaCoberturas(){
		try {
			
			if(GestorFechas.comparaFechas(getNwCoberturas().getCocbFeDesde(), getNwCoberturas().getCocbFeHasta())==-1){
				FacesUtils.addErrorMessage("La Fecha Hasta no puede ser MENOR a la Fecha Desde");
				return NavigationResults.FAILURE;
			}
			
			ColectivosCoberturasId coberturasId = new ColectivosCoberturasId();
			
			String ramoContable = getValComboCobertura()!=null?getValComboCobertura().substring(0,getValComboCobertura().length()-4):"";
			String cdCobertura  = getValComboCobertura()!=null?getValComboCobertura().substring(getValComboCobertura().length()-3):"";
			
			coberturasId.setCocbCasuCdSucursal	(new Short("1"));									//SUCURSAL
			coberturasId.setCocbCarpCdRamo		(getBean().getRamo().getCarpCdRamo().byteValue()); 	//RAMO
			coberturasId.setCocbCapoNuPoliza	(getBean().getPolizaUnica());						//POLIZA
			coberturasId.setCocbCapuCdProducto	(getBean().getId().getAlprCdProducto());			//PRODUCTO
			coberturasId.setCocbCapbCdPlan		(getBean().getIdPlan().getAlplCdPlan());			//PLAN
			
			coberturasId.setCocbCarbCdRamo(Byte.parseByte(ramoContable));	//Ramo contable
			coberturasId.setCocbCacbCdCobertura(cdCobertura);				//Cobertura
			coberturasId.setCocbCaceNuCertificado(0);						//Certificado
			coberturasId.setCocbEstatus(new Byte("20"));					//Estatus
			coberturasId.setCocbCerNuCobertura(1L);
			
			getNwCoberturas().setCocbCampon1(1000L); //Valor al millar
			getNwCoberturas().setCocbCampov1("1"); //Porcentaje
			getNwCoberturas().setCocbPolNuCobertura(1L);
			
			getNwCoberturas().setId(coberturasId);
	
			this.servicioColectivosCoberturas.guardarObjeto(getNwCoberturas());
			
			/**
			 * Cambio de tipo de comision
			 */
			servicioColectivosCoberturas.actualizaTipoComision(getNwCoberturas());
			 
			setNwCoberturas(new ColectivosCoberturas());
			
			getBean().setStrRespuesta("El registro se agrego con EXITO");
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("Error interno: No se pueden insetar las coberturas.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

	public String consultaCoberturasPorPlan(){
		
		StringBuilder filtro = new StringBuilder(100);
		
		getListaCoberturas().clear();
		
		try {
	
			filtro.append(" and P.id.cocbCarpCdRamo		= ").append(getId().getCocbCarpCdRamo());
			filtro.append(" and P.id.cocbCapoNuPoliza 	= ").append(getId().getCocbCapoNuPoliza());
			filtro.append(" and P.id.cocbCapuCdProducto = ").append(getId().getCocbCapuCdProducto());
			filtro.append(" and P.id.cocbCapbCdPlan 	= ").append(getId().getCocbCapbCdPlan());

			filtro.append(" ");
			
			List<ColectivosCoberturas> listaCoberturas = this.servicioColectivosCoberturas.obtenerObjetos(filtro.toString());
			
			for (ColectivosCoberturas cobertura : listaCoberturas) {
				getListaCoberturas().add(cobertura);
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

	public String consultaCartCoberturas(){
		
		setStrRespuesta("");
		getBean().setStrRespuesta("");
		setNwCoberturas(new ColectivosCoberturas());
		setIdCartCoberturas(new CartCoberturasId());
		setCartCoberturas(new CartCoberturas());
		
		StringBuilder filtro = new StringBuilder(100);
		getListaComboCoberturas().clear();
		getListaCartCoberturas().clear();
		
		try {
	
			filtro.append(" and cb.id.cocbCarpCdRamo   = ").append(getBean().getRamo().getCarpCdRamo());
			
			List<CartCoberturas> listaCartCoberturas = this.servicioCartCoberturas.obtenerCartCoberturasObj(filtro.toString());
			
			for (CartCoberturas cartCoberturas : listaCartCoberturas) {
				getListaComboCoberturas().add(
						new SelectItem(cartCoberturas.getId().getCacbCarbCdRamo()+"-"+cartCoberturas.getId().getCacbCdCobertura(),
									   cartCoberturas.toString()));
				getListaCartCoberturas().add(cartCoberturas);
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

	public String modificar(){
		
		setStrRespuesta("");
		
		try {
			
			if(GestorFechas.comparaFechas(getCoberturas().getCocbFeDesde(), getCoberturas().getCocbFeHasta())==-1){
				FacesUtils.addErrorMessage("La Fecha Hasta no puede ser MENOR a la Fecha Desde");
				return NavigationResults.FAILURE;
			}
			
			this.servicioColectivosCoberturas.actualizarObjeto(getCoberturas());
			/**
			 * Cambio de tipo de comision
			 */
			servicioColectivosCoberturas.actualizaTipoComision(getCoberturas());
			
			setStrRespuesta("Se modifico con EXITO el registro.");
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

	public String guardaNuevaCobertura(){
		try {

			if(GestorFechas.comparaFechas(getNwCoberturas().getCocbFeDesde(), getNwCoberturas().getCocbFeHasta())==-1){
				FacesUtils.addErrorMessage("La Fecha Hasta no puede ser MENOR a la Fecha Desde");
				return NavigationResults.FAILURE;
			}
			
			ColectivosCoberturasId coberturasId = new ColectivosCoberturasId();
			
			
			getCartRamosPolizas().setCarpCdRamo(getBean().getRamo().getCarpCdRamo().byteValue());
			
			getCartCoberturas().setId(getIdCartCoberturas());
			getCartCoberturas().setCartRamosPolizas(getCartRamosPolizas()); //RAMO
			getCartCoberturas().setCacbCaroCdRamo(Byte.parseByte("33")); //Ramo Oficial
			getCartCoberturas().setCacbMtMaximo(new Long("999999999999")); //Monto Maximo
			getCartCoberturas().setCacbInSumarizacion("S");
			getCartCoberturas().setCacbInCoberturaSub("C");
			getCartCoberturas().setCacbInAumentoAutomatico("S");
			getCartCoberturas().setCacbMtMaximoDolares(new Long(1));
			getCartCoberturas().setCacbInValidaMontoSin("S");
			getCartCoberturas().setCacbInSumapri("S");
			
			this.servicioCartCoberturas.guardarObjeto(getCartCoberturas());
			
			Byte ramoContable   = getCartCoberturas().getId().getCacbCarbCdRamo();
			String cdCobertura  = getCartCoberturas().getId().getCacbCdCobertura();
			
			coberturasId.setCocbCasuCdSucursal	(new Short("1"));									//SUCURSAL
			coberturasId.setCocbCarpCdRamo		(getBean().getRamo().getCarpCdRamo().byteValue()); 	//RAMO
			coberturasId.setCocbCapoNuPoliza	(getBean().getPolizaUnica());						//POLIZA
			coberturasId.setCocbCapuCdProducto	(getBean().getId().getAlprCdProducto());			//PRODUCTO
			coberturasId.setCocbCapbCdPlan		(getBean().getIdPlan().getAlplCdPlan());			//PLAN
			
			coberturasId.setCocbCarbCdRamo(ramoContable);		//Ramo contable
			coberturasId.setCocbCacbCdCobertura(cdCobertura);	//Cobertura
			coberturasId.setCocbCaceNuCertificado(0);			//Certificado
			coberturasId.setCocbEstatus(new Byte("20"));		//Estatus
			coberturasId.setCocbCerNuCobertura(1L);
			
			getNwCoberturas().setCocbCampon1(1000L); //Valor al millar
			getNwCoberturas().setCocbCampov1("1"); //Porcentaje
			getNwCoberturas().setCocbPolNuCobertura(1L);
			
			getNwCoberturas().setId(coberturasId);
	
			this.servicioColectivosCoberturas.guardarObjeto(getNwCoberturas());
			
			/**
			 * Cambio de tipo de comision
			 */
			servicioColectivosCoberturas.actualizaTipoComision(getNwCoberturas());
			 
			setCartCoberturas(new CartCoberturas());
			setNwCoberturas(new ColectivosCoberturas());
			
			getBean().setStrRespuesta("El registro se agrego con EXITO");
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	
	public void add(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(getNwCoberturas().getCocbFeDesde());
		cal.add(Calendar.YEAR, 50);
		getNwCoberturas().setCocbFeHasta(cal.getTime());
	}

	public void validaTipoComision() {
		if ("P".equals(nwCoberturas.getTipoComision())) {
			nwCoberturas.setCocbPoComision(new BigDecimal(0));
			nwCoberturas.setCocbMtFijo(new BigDecimal(0));
			habilitaComisiones = false;
		}
		
		habilitaComisiones = true;
	}

	/**
	 * @return the habilitaComisiones
	 */
	public Boolean getHabilitaComisiones() {
		return habilitaComisiones;
	}

	/**
	 * @param habilitaComisiones the habilitaComisiones to set
	 */
	public void setHabilitaComisiones(Boolean habilitaComisiones) {
		this.habilitaComisiones = habilitaComisiones;
	}	

}
