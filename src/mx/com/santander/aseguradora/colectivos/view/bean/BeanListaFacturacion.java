/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Facturacion;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioFacturacion;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Z014155
 * Modificacion: Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanListaFacturacion extends BeanBase {

	@Resource
	private ServicioFacturacion servicioFacturacion;
	private List<Object> listaFacturacion;
	private Short canal;
	private Short ramo;
	private Long numPoliza;
	private Double numRecibo;
	private int	  numPagina;
	private String respuesta;

	/**
	 * @return the servicioFacturacion
	 */
	public ServicioFacturacion getServicioFacturacion() {
		return servicioFacturacion;
	}

	/**
	 * @param servicioFacturacion the servicioFacturacion to set
	 */
	public void setServicioFacturacion(ServicioFacturacion servicioFacturacion) {
		this.servicioFacturacion = servicioFacturacion;
	}

	/**
	 * @return the listaFacturacion
	 */
	public List<Object> getListaFacturacion() {
		return listaFacturacion;
	}

	/**
	 * @param listaFacturacion the listaFacturacion to set
	 */
	public void setListaFacturacion(List<Object> listaFacturacion) {
		this.listaFacturacion = listaFacturacion;
	}

	/**
	 * @return the canal
	 */
	public Short getCanal() {
		return canal;
	}

	/**
	 * @param canal the canal to set
	 */
	public void setCanal(Short canal) {
		this.canal = canal;
	}

	/**
	 * @return the ramo
	 */
	public Short getRamo() {
		return ramo;
	}

	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}

	/**
	 * @return the numPoliza
	 */
	public Long getNumPoliza() {
		return numPoliza;
	}

	/**
	 * @param numPoliza the numPoliza to set
	 */
	public void setNumPoliza(Long numPoliza) {
		this.numPoliza = numPoliza;
	}

	/**
	 * @return the numRecibo
	 */
	public Double getNumRecibo() {
		return numRecibo;
	}

	/**
	 * @param numRecibo the numRecibo to set
	 */
	public void setNumRecibo(Double numRecibo) {
		this.numRecibo = numRecibo;
	}

	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}

	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public BeanListaFacturacion() {
		new StringBuilder();
		this.listaFacturacion = new ArrayList<Object>();
		this.canal = 1;
		this.numPagina = 1;
	}

	
	public String consultarPreFactura(){
		
		StringBuilder filtro = new StringBuilder();
		
		filtro.append(" and F.id.cofaCasuCdSucursal = ").append(this.canal).append("\n");
		
		try {
			
			if(this.ramo != null && this.ramo.intValue() >0){
				filtro.append(" and F.id.cofaCarpCdRamo = ").append(this.ramo).append("\n");
			}
			
			@SuppressWarnings("unused")
			List<Facturacion> lista = this.servicioFacturacion.obtenerObjetos(filtro.toString());
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return respuesta;
	}
}
	
	
