/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.TlmkTraspasoAptc;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTraspasoAPTC;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanListaTraspasoAptc {

	@Resource
	private ServicioTraspasoAPTC servicioTraspasoAptc;
	private List<BeanTraspasoAptc> listaTraspaso;

	private String idCertificado;
	private int numPagina;
	private Date fechaInicial;
	private Date fechaFinal;
	
	public String getIdCertificado() {
		return idCertificado;
	}

	public void setIdCertificado(String idCertificado) {
		this.idCertificado = idCertificado;
	}

	public ServicioTraspasoAPTC getServicioTraspasoAptc() {
		return servicioTraspasoAptc;
	}

	public void setServicioTraspasoAptc(ServicioTraspasoAPTC servicioTraspasoAptc) {
		this.servicioTraspasoAptc = servicioTraspasoAptc;
	}

	public List<BeanTraspasoAptc> getListaTraspaso() {
		return listaTraspaso;
	}

	public void setListaTraspaso(List<BeanTraspasoAptc> listaTraspaso) {
		this.listaTraspaso = listaTraspaso;
	}
	
	public int getNumPagina() {
		return numPagina;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public BeanListaTraspasoAptc() {
		// TODO Auto-generated constructor stub
		listaTraspaso = new ArrayList<BeanTraspasoAptc>();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_TRASPASO_APTC);
	}
	
	public String consultarErrores()
	{
		StringBuilder filtro;
		
		try {
			
			listaTraspaso.clear();
			
			filtro = new StringBuilder();
			
			if(idCertificado != null && idCertificado.length() > 0)
			{
				filtro.append(" and TA.trseCuenta = ").append(idCertificado);
			}
			
			if(fechaInicial != null)
			{
				filtro.append(" and TA.trseFechaCarga >= '").append(new SimpleDateFormat("dd/MM/yyyy").format(fechaInicial)).append("'");
			}
			
			if(fechaFinal != null)
			{
				filtro.append(" and TA.trseFechaCarga <= '").append(new SimpleDateFormat("dd/MM/yyyy").format(fechaFinal)).append("'");
			}
			
			if(filtro != null && filtro.toString().length() > 0)
			{
				listaTraspaso = servicioTraspasoAptc.obtenerObjetos(filtro.toString());
			}
					
		} catch (Exception e) {
			// TODO: handle exception
		}
		finally
		{
			filtro = null;
		}
		
		return null;
	}
	
	public String erroresEmision(){
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		StringBuilder filtro;
		
		
		try {
			
			if(listaTraspaso == null)
			{			
				filtro = new StringBuilder();
				
				if(idCertificado != null && idCertificado.length() > 0)
				{
					filtro.append(" and TA.trseCuenta = ").append(idCertificado);
				}
				
				if(fechaInicial != null)
				{
					filtro.append(" and TA.trseFechaCarga >= '").append(new SimpleDateFormat("dd/MM/yyyy").format(fechaInicial)).append("'");
				}
				
				if(fechaFinal != null)
				{
					filtro.append(" and TA.trseFechaCarga <= '").append(new SimpleDateFormat("dd/MM/yyyy").format(fechaFinal)).append("'");
				}
				
				if(filtro != null && filtro.length() > 0){
					
					listaTraspaso = servicioTraspasoAptc.obtenerObjetos(filtro.toString());
				}
			}
			
			if(listaTraspaso != null){
				
				rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ErroresNomina.csv");
				
				FileWriter fileWrite = null;
				PrintWriter printWriter = null;
				
				//Escribimos en el archivo
				try {
					
					fileWrite = new FileWriter(rutaTemporal);
					printWriter = new PrintWriter(fileWrite);
					
					printWriter.println("Identificador,Error");
					
					for(Object obj: listaTraspaso)
					{	
						TlmkTraspasoAptc bean = (TlmkTraspasoAptc)obj;
						
						printWriter.println(String.format("%s,%s", bean.getTrseCuenta(), bean.getTrseRegistro()));
					}
					
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					
					if(fileWrite != null){
						fileWrite.close();
						fileWrite = null;
					}
				}
				finally{
					if(fileWrite != null){
						fileWrite.close();
						fileWrite = null;
					}
				}
				
				
				File file = new File(rutaTemporal);
				
				FacesContext context = FacesContext.getCurrentInstance();
				
				if(!context.getResponseComplete()){
				
					input = new FileInputStream(file);
					bytes = new byte[1000];
										
					String contentType = "application/vnd.ms-excel";

					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					
					response.setContentType(contentType);
					
					DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
			    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());
			    	
					ServletOutputStream out = response.getOutputStream();
					
					while((read = input.read(bytes))!= -1){
						
						out.write(bytes, 0 , read);
					}
					
					input.close();
					out.flush();
					out.close();
					file.delete();
					
					context.responseComplete();
					
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return null;
	}

}
