package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import jcifs.smb.SmbFile;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHistorialCredito;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.SmbUtil;

@Controller
@Scope("session")
public class BeanHistorialCredito implements InitializingBean {

		@Resource
		private ServicioHistorialCredito servicioHistorialCredito;
		@Resource
		private ServicioClienteCertif servicioCliente;
		
		private List<BeanListaCreditoDetalle> listaHistorial;
		private  List<BeanListaCoberturaDetalle>  listaHistorialDetalleCobertura;
		private List<Object> listaHistorialDetalle;
		private List<BeanListaRecibosHistorico> listaHistorialDetalleRecibos;
		
		private short ramo;
		private String ramoNom;
		private Date fecha_emision;
		private Long certificadoLong;
		private Long poliza;
		private Long polizaEspecifica;
		private Long polizaLong;
	
		private String nombre;
		private String nombre_aseg;
		private String paterno;
		private String materno;
		private String certificado;
		private String credito;
		private String cober;
		private String cober_desc;
		private String tarif;
		private String canal;
		private Integer producto= 0;
		private Integer plan= 0;
		private String respuesta;
		private int numPagina;
		private BeanListaCreditoDetalle objActual = new BeanListaCreditoDetalle();
		private Log log = LogFactory.getLog(this.getClass());
		private StringBuilder message;
		private int filaActual=0;
		private String tipoBusqueda;
		private String display1="none";
		private String display2="none";
		private String display3="none";
		
		
		public String getDisplay1() {
			return display1;
		}

		public Long getPolizaEspecifica() {
			return polizaEspecifica;
		}

		public void setPolizaEspecifica(Long polizaEspecifica) {
			this.polizaEspecifica = polizaEspecifica;
		}

		public void setDisplay1(String display1) {
			this.display1 = display1;
		}

		public String getDisplay2() {
			return display2;
		}

		public void setDisplay2(String display2) {
			this.display2 = display2;
		}

		public String getDisplay3() {
			return display3;
		}

		public void setDisplay3(String display3) {
			this.display3 = display3;
		}
		
		public String getTipoBusqueda() {
			return tipoBusqueda;
		}

		public void setTipoBusqueda(String tipoBusqueda) {
			this.tipoBusqueda = tipoBusqueda;
		}

		public Log getLog() {
			return log;
		}

		public void setLog(Log log) {
			this.log = log;
		}

		public StringBuilder getMessage() {
			return message;
		}

		public void setMessage(StringBuilder message) {
			this.message = message;
		}

		public int getFilaActual() {
			return filaActual;
		}

		public void setFilaActual(int filaActual) {
			this.filaActual = filaActual;
		}
		
		public Long getPolizaLong() {
			return polizaLong;
		}

		public void setPolizaLong(Long polizaLong) {
			this.polizaLong = polizaLong;
		}


		public BeanHistorialCredito() 
		{
			ramo=0;
			poliza=0L;
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_HISTORIAL_CREDITO);
		}

		public ServicioHistorialCredito getServicioHistorialCredito() {
			return servicioHistorialCredito;
		}

		public void setServicioHistorialCredito(ServicioHistorialCredito servicioHistorialCredito) {
			this.servicioHistorialCredito = servicioHistorialCredito;
		}
		
		public ServicioClienteCertif getServicioCliente() {
			return servicioCliente;
		}

		public void setServicioCliente(ServicioClienteCertif servicioCliente) {
			this.servicioCliente = servicioCliente;
		}
		public List<BeanListaCreditoDetalle> getListaHistorial() {
			return listaHistorial;
		}

		public void setListaHistorial(List<BeanListaCreditoDetalle> listaHitorial) {
			this.listaHistorial = listaHitorial;
		}

		public List<Object> getListaHistorialDetalle() {
			return listaHistorialDetalle;
		}

		public void setListaHistorialDetalle(List<Object> listaHistorialDetalle) {
			this.listaHistorialDetalle = listaHistorialDetalle;
		}

		public short getRamo() {
			return ramo;
		}

		public void setRamo(short ramo) {
			this.ramo = ramo;
		}

		public Long getPoliza() {
			return poliza;
		}

		public void setPoliza(Long poliza) {
			this.poliza = poliza;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getPaterno() {
			return paterno;
		}

		public void setPaterno(String paterno) {
			this.paterno = paterno;
		}

		public String getMaterno() {
			return materno;
		}

		public void setMaterno(String materno) {
			this.materno = materno;
		}

		public String getCertificado() {
			return certificado;
		}

		public void setCertificado(String certificado) {
			this.certificado = certificado;
		}

		public String getCredito() {
			return credito;
		}

		public void setCredito(String credito) {
			this.credito = credito;
		}

		public String getCanal() {
			return canal;
		}

		public void setCanal(String canal) {
			this.canal = canal;
		}
		
		public void setRespuesta(String respuesta){
			this.respuesta = respuesta;
		}
		
		public String getRespuesta(){
			return respuesta;
		}
		public int getNumPagina() {
			return numPagina;
		}

		public void setNumPagina(int numPagina) {
			this.numPagina = numPagina;
		}
		
		public BeanListaCreditoDetalle getObjActual() {
			return objActual;
		}

		public void setObjActual(BeanListaCreditoDetalle objActual) {
			this.objActual = objActual;
		}
	
	public void historialCredito() throws Excepciones{
		this.message = new StringBuilder();	
		
		try{
			if(this.getTipoBusqueda().equals(Constantes.DEFAULT_STRING)) {
				this.message.append("Favor de Seleccionar un tipo de busqueda.");
			} else {
				if(validarDatos()) {
					listaHistorial = this.servicioHistorialCredito.consultaHistorial(this.getRamo(), this.getPoliza() == 1 ? this.getPolizaEspecifica() : this.getPoliza(), this.getCertificado(), this.getCredito(), this.getNombre(), this.getPaterno(), this.getMaterno());
					if(listaHistorial.isEmpty()){
						this.message.append("No hay registros encontrados");
					}
				}
			}				
		} catch (Exception e) {
			System.out.println("exception " + e);
			this.message.append("Error al recuperar el historial de cr�dito para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}	
	}
	
	private boolean validarDatos() {
		this.message = new StringBuilder();
		try {
			if(this.getTipoBusqueda().equals("0")) { //Busqueda por Canal-Ramo-Poliza-Certificado
				if(this.getRamo() == Constantes.DEFAULT_INT) {
					this.message.append("Favor de seleccionar el ramo.");
					return false;
				} else if(this.getPoliza() == Constantes.DEFAULT_LONG) {
					this.message.append("Favor de seleccionar una poliza.");
					return false;
				} else if(this.getPoliza() == 1) {
					if(this.getPolizaEspecifica() == null || this.getPolizaEspecifica() == Constantes.DEFAULT_LONG){
						this.message.append("Es prima unica, favor de ingresar poliza especifica.");
						return false;
					} else if(this.getCertificado().equals(Constantes.DEFAULT_STRING)) {
						this.message.append("Favor de ingresar numero de certificado.");
						return false;
					}
				} else if(this.getCertificado().equals(Constantes.DEFAULT_STRING)) {
					this.message.append("Favor de ingresar numero de certificado.");
					return false;
				}
				
				this.setCredito(Constantes.DEFAULT_STRING);
				this.setNombre(Constantes.DEFAULT_STRING);
				this.setPaterno(Constantes.DEFAULT_STRING);
				this.setMaterno(Constantes.DEFAULT_STRING);
			} else if (this.getTipoBusqueda().equalsIgnoreCase("1")){ //Busqueda por credito
				if(this.getCredito().equals(Constantes.DEFAULT_STRING) || this.getCredito().equals(Constantes.DEFAULT_STRING_ID)) {
					this.message.append("Favor de ingresar un numero de credito valido.");
					return false;
				}
				
				this.setRamo(new Short("0"));
				this.setPoliza(Constantes.DEFAULT_LONG);
				this.setPolizaEspecifica(Constantes.DEFAULT_LONG);
				this.setCertificado(Constantes.DEFAULT_STRING);
				this.setNombre(Constantes.DEFAULT_STRING);
				this.setPaterno(Constantes.DEFAULT_STRING);
				this.setMaterno(Constantes.DEFAULT_STRING);
			} else if(this.getTipoBusqueda().equalsIgnoreCase("2")) { //Busqueda por Nombre
				if(this.getNombre() == null || this.getNombre().trim().equals(Constantes.DEFAULT_STRING)) {
					this.message.append("Favor de ingresar Nombre.");
					return false;
				} else if(this.getPaterno() == null || this.getPaterno().trim().equals(Constantes.DEFAULT_STRING)) {
					this.message.append("Favor de ingresar apellido paterno.");
					return false;
				} else if(this.getMaterno() == null || this.getMaterno().trim().equals(Constantes.DEFAULT_STRING)) {
					this.message.append("Favor de ingresar apellido materno.");
					return false;
				}
					
				this.setRamo(new Short("0"));
				this.setPoliza(Constantes.DEFAULT_LONG);
				this.setPolizaEspecifica(Constantes.DEFAULT_LONG);
				this.setCertificado(Constantes.DEFAULT_STRING);
				this.setCredito(Constantes.DEFAULT_STRING);
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.message.append("Ocurrio un error, favor intentar hacer la consulta.");
		}
		
		return true;
	}

	public String muestraCriterio(){
		if(getTipoBusqueda()!=null && getTipoBusqueda().equals("0")) {
			setDisplay1("block");
			setDisplay2("none");
			setDisplay3("none");		
		} else {
			setDisplay1("none");
		}
		
		if(getTipoBusqueda()!=null && getTipoBusqueda().equals("1")){
			setDisplay1("none");
			setDisplay2("block");
			setDisplay3("none");			
		}else{
			setDisplay2("none");
		}
		
		if(getTipoBusqueda()!=null && getTipoBusqueda().equals("2")){
			setDisplay1("none");
			setDisplay2("none");
			setDisplay3("block");		
		}else{
			setDisplay3("none");
		}
	
		this.setCredito(Constantes.DEFAULT_STRING);
		this.setNombre(Constantes.DEFAULT_STRING);
		this.setPaterno(Constantes.DEFAULT_STRING);
		this.setMaterno(Constantes.DEFAULT_STRING);
		this.setRamo(new Short("0"));
		this.setPoliza(Constantes.DEFAULT_LONG);
		this.setPolizaEspecifica(Constantes.DEFAULT_LONG);
		this.setCertificado(Constantes.DEFAULT_STRING);
		this.listaHistorial = new ArrayList<BeanListaCreditoDetalle>();
		return getTipoBusqueda();
	}

	public void impresionPoliza() {	
		String ruta = "";
		String reporte = "";
		this.message = new StringBuilder();	
		Archivo objArchivo;
		try {
			objArchivo = new Archivo();
			if(this.getCertificado() != null){
				ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
				reporte = servicioCliente.impresionPoliza(ruta,Short.valueOf(objActual.getCd_sucursal().split("\\-")[0].trim()),
																		Short.valueOf(objActual.getRamo()),
																		Long.parseLong(objActual.getPoliza()),
																		Long.parseLong(objActual.getCertificado()),
																		Integer.parseInt(objActual.getProducto().split("\\-")[0].trim()),
																		objActual.getFec_emision());
				
				if(reporte != null){
					objArchivo = new Archivo(ruta, FilenameUtils.getBaseName(reporte), "." + FilenameUtils.getExtension(reporte));
					objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 4);
					objArchivo.eliminarArchivo();
					FacesContext.getCurrentInstance().responseComplete();
				}
			} else {
				// reporte = "No existe reporte para este producto"; 	
				reporte = null;
			}
		} catch (Exception e) {
			log.error("Error al generar la impresion de poliza.",e);
			e.printStackTrace();
			FacesUtils.addInfoMessage("Error al generar el reporte");
		} finally {
			if(reporte == null) {
			   reporte = "No existe reporte para este producto";		
			   log.info (reporte); 
		       this.message.append(reporte);
			}else{
				log.info ("Archivo generado correctamente"); 
			    this.message.append("Archivo generado correctamente");	
			}
		}
	}

	public void getReportePlatilla() {	
		String rutaPlatillas = "";
		String strNombre;
		String strRespuesta = "";		 
		Archivo objArchivo;
		boolean blnDescarga = false;
		this.message = new StringBuilder();	
		String rutaApp = "";
		String strNomrePlantilla = "";
		Parametros smbRuta;
		SmbUtil smbUtil;
		SmbFile[] arrFiles = null;
		SmbFile objDir;
		String polPU = "";
		Long polizaReal = 0L;
		
		try {
			polPU = objActual.getPoliza().toString().substring(0,1);
			if(Short.valueOf(objActual.getRamo()) == 57 || Short.valueOf(objActual.getRamo()) == 58){
				polizaReal = Long.parseLong(objActual.getRamo());
			} else {
				if(polPU.equals("5") || polPU.equals("6") || polPU.equals("7") || polPU.equals("8") || polPU.equals("9")){
					polPU = polPU + "" + servicioCliente.getPlazo(Short.valueOf(objActual.getRamo()), Integer.parseInt(objActual.getProducto().split("\\-")[0].trim()), Integer.parseInt(objActual.getPlan().split("\\-")[0].trim())) + "" + Integer.parseInt(objActual.getProducto().split("\\-")[0].trim());
					polizaReal = Long.parseLong(polPU);
				} else {
					polizaReal = Long.parseLong(objActual.getPoliza());
				}
			}
			
			strNombre = canal + "_" + ramo + "_" + polizaLong + "_" + certificadoLong;
			rutaApp = FacesUtils.getServletContext().getRealPath("/WEB-INF/plantillas/") + File.separator;
			smbRuta =  servicioCliente.getRutaPlantilla();
			strNomrePlantilla = servicioCliente.getPlantilla(Short.valueOf(objActual.getCd_sucursal().split("\\-")[0].trim()),
																Short.valueOf(objActual.getRamo()),
																polizaReal, 
																Long.parseLong(objActual.getCertificado()),
																Integer.parseInt(objActual.getProducto().split("\\-")[0].trim()), 
																Integer.parseInt(objActual.getPlan().split("\\-")[0].trim()), 
																objActual.getFec_emision());
			
			smbUtil = new SmbUtil(smbRuta.getCopaVvalor6(), smbRuta.getCopaVvalor7());
			rutaPlatillas = smbRuta.getCopaRegistro();
			objDir = smbUtil.getFile(rutaPlatillas);
			
			arrFiles = objDir.listFiles();
			if(arrFiles != null) {
				for(SmbFile objFile : arrFiles) {
					if(!objFile.isDirectory()) {
						if(objFile.getName().equals(strNomrePlantilla)){
							System.out.println(":::Copiando Archivo a PathApp::: " + new Date() + " ::" + objFile.getName());
							Archivo objArchivoSmb = new Archivo(smbUtil.getFile(rutaPlatillas + objFile.getName()));
							objArchivoSmb.copiarSmbArchivo2(new File(rutaApp + objFile.getName())); //copiar a carpeta de aplicativo
							System.out.println(":::FIN Copiando Archivo a PathApp::: " + new Date() + " ::" + objFile.getName());
							strRespuesta = "Si hay plantilla en ruta";
							break;
						}
					}
				}
			}

			if(!strRespuesta.equals(Constantes.DEFAULT_STRING_ID)) {
				strRespuesta = servicioCliente.generaCertificado(rutaApp, Short.valueOf(objActual.getCd_sucursal().split("\\-")[0].trim()),
																Short.valueOf(objActual.getRamo()),
																Long.parseLong(objActual.getPoliza()), 
																Long.parseLong(objActual.getCertificado()),
																Integer.parseInt(objActual.getProducto().split("\\-")[0].trim()), 
																Integer.parseInt(objActual.getPlan().split("\\-")[0].trim()), 
																objActual.getFec_emision(), rutaApp + strNombre + ".pdf", strNomrePlantilla, polizaReal);
			}
			
			if(strRespuesta.equals(Constantes.DEFAULT_STRING_ID)) {
				rutaApp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
				strNombre = impresionCertificado(rutaApp);
				if(strNombre == null) {
					System.out.println("No existe Plantilla para este Certificado");
					this.message.append("No existe Plantilla para este Certificado");
				} else {
					blnDescarga = true;	
				}
			} else {	
				blnDescarga = true;
			}
			
			if(blnDescarga) {
				objArchivo = new Archivo(rutaApp, strNombre, ".pdf");
				objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 4);
				objArchivo.eliminarArchivo();
				FacesContext.getCurrentInstance().responseComplete();
				System.out.println("Se genero con exito el Certificado");
				this.message.append("Se genero con exito el Certificado");
			}
		} catch (Exception e) {
			System.out.println("Error al generar la impresion de certificados.");
			e.printStackTrace();
		}
	}
	private String impresionCertificado(String ruta) {	
		String reporte = "";
		this.message = new StringBuilder();	
		try {
			if(this.getCertificado() != null){
				reporte = servicioCliente.impresionCertificado(ruta, Short.valueOf(objActual.getCd_sucursal().split("\\-")[0].trim()),
																Short.valueOf(objActual.getRamo()),
																Long.parseLong(objActual.getPoliza()),
																Long.parseLong(objActual.getCertificado()),
																objActual.getFec_emision()	   );
				if(reporte != null) {
					reporte = reporte.replace(".pdf", "");
				}
			} else {
				reporte = null;
			}
		} catch (Exception e) {
			log.error("Error al generar la impresion de certificados.",e);
			e.printStackTrace();
			FacesUtils.addInfoMessage("Error al generar el reporte");
		} finally {
			if(reporte == null) {
		       this.message.append("No existe Plantilla para este Certificado");
			}else{
				log.info ("Archivo generado correctamente"); 
			    this.message.append("Archivo generado correctamente");	
			}
		}
		
		return reporte;
	}

	


@Override
	public void afterPropertiesSet() throws Exception {
	// TODO Auto-generated method stub
	
	}

public Integer getProducto() {
	return producto;
}

public void setProducto(Integer producto) {
	this.producto = producto;
}

public Integer getPlan() {
	return plan;
}

public void setPlan(Integer plan) {
	this.plan = plan;
}

public String getCober() {
	return cober;
}

public void setCober(String cober) {
	this.cober = cober;
}

public String getCober_desc() {
	return cober_desc;
}

public void setCober_desc(String cober_desc) {
	this.cober_desc = cober_desc;
}

public String getTarif() {
	return tarif;
}

public void setTarif(String tarif) {
	this.tarif = tarif;
}

public List<BeanListaCoberturaDetalle> getListaHistorialDetalleCobertura() {
	return listaHistorialDetalleCobertura;
}

public void setListaHistorialDetalleCobertura(
		List<BeanListaCoberturaDetalle> listaHistorialDetalleCobertura) {
	this.listaHistorialDetalleCobertura = listaHistorialDetalleCobertura;
}

public String getNombre_aseg() {
	return nombre_aseg;
}

public void setNombre_aseg(String nombre_aseg) {
	this.nombre_aseg = nombre_aseg;
}

public List<BeanListaRecibosHistorico> getListaHistorialDetalleRecibos() {
	return listaHistorialDetalleRecibos;
}

public void setListaHistorialDetalleRecibos(
		List<BeanListaRecibosHistorico> listaHistorialDetalleRecibos) {
	this.listaHistorialDetalleRecibos = listaHistorialDetalleRecibos;
}

public String getRamoNom() {
	return ramoNom;
}

public void setRamoNom(String ramoNom) {
	this.ramoNom = ramoNom;
}

public Date getFecha_emision() {
	return fecha_emision;
}

public void setFecha_emision(Date fecha_emision) {
	this.fecha_emision = fecha_emision;
}

public Long getCertificadoLong() {
	return certificadoLong;
}

public void setCertificadoLong(Long certificadoLong) {
	this.certificadoLong = certificadoLong;
}
	
}

