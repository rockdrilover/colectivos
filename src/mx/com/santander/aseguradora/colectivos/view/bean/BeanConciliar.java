package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioConciliacion;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 * @Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanConciliar {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioConciliacion servicioConciliacion;
	
	private Integer nTipoConciliacion;
	private Integer nLayOutPmaTrad;
	private Integer nLayOutGap;
	private List<Object> listaComboOrigen;
	private Integer nOrigen;
	private String strCreditos = "";
	private String respuesta;
	private List<Object> lstResultados;
	private List<Object> lstDatosArchivo;
	private String[] arrNomColumnas;
	private String strNombreArchivo;

	public BeanConciliar(){
		setListaComboOrigen(new ArrayList<Object>());
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONCILIAR);
	}
	

	public String conciliar(){
		String strForward = null;
		HashMap<Integer, ArrayList<Object>> hmConciliar;
		
		try {
			if(validarDatos(1)) {
				hmConciliar = new HashMap<Integer, ArrayList<Object>>();
				
				switch (getnOrigen()) {
					case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR:
					case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR:
						hmConciliar.put(Constantes.INDEX_CONCILIA_ALTAMIRA, servicioCarga.consultarVentasConsumo(1, Constantes.CONCILIACION_ALTAMIRA_RAMO, Constantes.CONCILIACION_ALTAMIRA));
						hmConciliar.put(Constantes.INDEX_CONCILIA_ALTAIR, servicioCarga.consultarVentasConsumo(1, Constantes.CONCILIACION_ALTAIR_RAMO, Constantes.CONCILIACION_ALTAIR));
						break;
						
					case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
						hmConciliar.put(Constantes.INDEX_CONCILIA_HPU, servicioCarga.consultarVentasConsumo(1, Constantes.CONCILIACION_HIPOTECARIO_RAMO, Constantes.CONCILIACION_HIPOTECARIO));
						break;
						
					case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
						hmConciliar.put(Constantes.INDEX_CONCILIA_LCI, servicioCarga.consultarVentasConsumo(1, Constantes.CONCILIACION_LCI_RAMO, Constantes.CONCILIACION_LCI));
						break;
						
					case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:
						hmConciliar.put(Constantes.INDEX_CONCILIA_GAP, servicioCarga.consultarVentasConsumo(1, Constantes.CONCILIACION_GAP_RAMO, Constantes.CONCILIACION_GAP));
						break;
						
					case Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR:
						ArrayList<Object> lista = servicioCarga.consultarVentasConsumo(1, Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO, Constantes.CONCILIACION_DESEMPLEO_TDC);
						hmConciliar.put(Constantes.INDEX_CONCILIA_DESEMPLEO, lista);
						break;
					
					case Constantes.CONCILIACION_ORIGEN_LE_VALOR:
						hmConciliar.put(Constantes.INDEX_CONCILIA_LE, servicioCarga.consultarVentasConsumo(1, Constantes.CONCILIACION_LE_RAMO, Constantes.CONCILIACION_LE));
						break;
				}
				
				servicioCarga.conciliarVentas(hmConciliar);
				
				setRespuesta("El proceso de conciliacion finalizó correctamente.");
				log.info(getRespuesta());
				strForward = NavigationResults.SUCCESS;
			} else {
				log.debug(getRespuesta());
				strForward = NavigationResults.FAILURE;	
			}
		} catch (Exception e) {
			setRespuesta("No se puede realizar la conciliacion." );
			log.error(getRespuesta() + e.getLocalizedMessage(), e);			
			strForward = NavigationResults.FAILURE;
		}
		
		return strForward;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String agregarCreditos(){
		String strForward = null;
		ArrayList<ArrayList> arlResultados;
		Parametros objParametros;
		String strColumnas, strSelect;
		String strCreditos;
		
		try {
			if(validarDatos(2)) {
				objParametros = servicioConciliacion.getParemetrosConsulta(getnOrigen() == Constantes.CONCILIACION_ORIGEN_GAP_VALOR_VID ? Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR : getnOrigen());
				strSelect = objParametros.getCopaRegistro().split("\\#")[0];
				strColumnas = objParametros.getCopaRegistro().split("\\#")[1];
				strNombreArchivo = objParametros.getCopaVvalor8();

				if(getnOrigen() == Constantes.CONCILIACION_ORIGEN_GAP_VALOR_VID){
					strSelect = strSelect + ",PRE.VTAP_NU_CONTRATO,PRE.VTAP_PRIMA,PRE.VTAP_REGISTRO,PRE.VTAP_AP_PATERNO_BEN";
					strColumnas = strColumnas + ",PolizaAuto,PrimaAutocredito,ComisiónApertura";
				}
				
				strCreditos = getStrCreditos().trim().endsWith(",") ? getStrCreditos().trim().substring(0, getStrCreditos().trim().length() - 1) : getStrCreditos().trim();
				arlResultados = servicioConciliacion.getLayOut(strSelect, strCreditos, getnOrigen(), objParametros.getCopaNvalor4());
				
				if(getnOrigen() == Constantes.CONCILIACION_ORIGEN_LE_VALOR) {
					servicioCarga.conciliarManualmente(arlResultados.get(1));
					setRespuesta("Se guardaron " + arlResultados.get(1).size() + " creditos correctamente.");
				} else {
					arrNomColumnas = strColumnas.split(",");
					setLstDatosArchivo(arlResultados.get(1));
					setLstResultados(arlResultados.get(0));
					setRespuesta("Se agregaron los creditos correctamente, favor de descargarlos.");
				}
				
				log.info(getRespuesta());
				strForward = NavigationResults.SUCCESS;
			} else {
				log.debug(getRespuesta());
				strForward = NavigationResults.FAILURE;	
			}
		} catch (Exception e) {
			setRespuesta("No se puede agregar los creditos." );
			log.error(getRespuesta() + e.getLocalizedMessage(), e);			
			strForward = NavigationResults.FAILURE;
		}
		
		return strForward;
	}
	
	public String getArchivo(){
		String strForward = null;
		
		try {
			if(validarDatos(3)) {
				if(getnOrigen() == Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR && getnLayOutPmaTrad() == 2) {
					setnOrigen(Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR);
					agregarCreditos();
					setnOrigen(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR);
				} else if(getnOrigen() == Constantes.CONCILIACION_ORIGEN_GAP_VALOR && getnLayOutGap() == 2) {
					setnOrigen(Constantes.CONCILIACION_ORIGEN_GAP_VALOR_VID);
					agregarCreditos();
					setnOrigen(Constantes.CONCILIACION_ORIGEN_GAP_VALOR);
				}
				
				descargar();
				strForward = NavigationResults.SUCCESS;
			} else {
				log.debug(getRespuesta());
				strForward = NavigationResults.FAILURE;	
			}
		} catch (Exception e) {
			setRespuesta("No se puede descargar el archivo." );
			log.error(getRespuesta() + e.getLocalizedMessage(), e);			
			strForward = NavigationResults.FAILURE;
		}
		
		return strForward;
	}
	
	public void descargar(){
		String strNombre = null, strRutaTemp;
		Archivo objArchivo;
	
		try {
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			strNombre = getStrNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()); 
			
			objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
			objArchivo.copiarArchivo(arrNomColumnas, getLstDatosArchivo(), ",");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
			objArchivo.eliminarArchivo();
			
			FacesContext.getCurrentInstance().responseComplete();
			setRespuesta("Archivo descargado satisfactoriamente.");
			log.error(getRespuesta());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @param nTipoValidacion
	 * 		Tipo de validacion a Realziar.
	 * 		1 - Conciliar.
	 * 		2 - Agregar Creditos.
	 * 		3 - Descargar.
	 * @return
	 * @throws Exception
	 */
	private boolean validarDatos(int nTipoValidacion) throws Exception{
		boolean blnResultado = true;
		
		if(getnTipoConciliacion() == null || getnTipoConciliacion() == 0){
			setRespuesta("Favor de seleccionar un Tipo de Conciliacion.");
			blnResultado = false;
		} else if (getnOrigen() == null || getnOrigen() == 0){			
			setRespuesta("Favor de seleccionar un Origen.");
			blnResultado = false;
		} 
		
		if(blnResultado){
			if(nTipoValidacion == 2) {
				if(getStrCreditos().equals("")) {
					setRespuesta("Favor de ingresar numeros de credito.");
					blnResultado = false;
				}
			}
			
			if(nTipoValidacion == 3){
				if(getnOrigen() == Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR && (getnLayOutPmaTrad() == null || getnLayOutPmaTrad() == 0)){
					setRespuesta("Favor de seleccionar Tipo Lay Out.");
					blnResultado = false;
				}
				
				if(getnOrigen() == Constantes.CONCILIACION_ORIGEN_GAP_VALOR && (getnLayOutGap() == null || getnLayOutGap() == 0)){
					setRespuesta("Favor de seleccionar Tipo Lay Out.");
					blnResultado = false;
				}
			}
			 
		}
		
		return blnResultado;
	}
	
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}
	public ServicioConciliacion getServicioConciliacion() {
		return servicioConciliacion;
	}
	public void setServicioConciliacion(ServicioConciliacion servicioConciliacion) {
		this.servicioConciliacion = servicioConciliacion;
	}
	public Integer getnTipoConciliacion() {
		return nTipoConciliacion;
	}
	public void setnTipoConciliacion(Integer nTipoConciliacion) {
		this.nTipoConciliacion = nTipoConciliacion;
	}
	public Integer getnLayOutPmaTrad() {
		return nLayOutPmaTrad;
	}
	public void setnLayOutPmaTrad(Integer nLayOutPmaTrad) {
		this.nLayOutPmaTrad = nLayOutPmaTrad;
	}
	public Integer getnLayOutGap() {
		return nLayOutGap;
	}
	public void setnLayOutGap(Integer nLayOutGap) {
		this.nLayOutGap = nLayOutGap;
	}
	public List<Object> getListaComboOrigen() {
		listaComboOrigen.clear();
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR, "ALTAMIRA - ALTAIR"));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR, Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_DESC));
//		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_LCI_VALOR, Constantes.CONCILIACION_ORIGEN_LCI_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_GAP_VALOR, Constantes.CONCILIACION_ORIGEN_GAP_DESC));
//		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_LE_VALOR, Constantes.CONCILIACION_ORIGEN_LE_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR, Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_DESC));
		return listaComboOrigen;
	}
	public void setListaComboOrigen(List<Object> listaComboOrigen) {
		this.listaComboOrigen = listaComboOrigen;
	}
	public Integer getnOrigen() {
		return nOrigen;
	}
	public void setnOrigen(Integer nOrigen) {
		this.nOrigen = nOrigen;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getStrCreditos() {
		return strCreditos;
	}
	public void setStrCreditos(String strCreditos) {
		this.strCreditos = strCreditos;
	}
	public List<Object> getLstResultados() {
		return lstResultados;
	}
	public void setLstResultados(List<Object> lstResultados) {
		this.lstResultados = lstResultados;
	}
	public List<Object> getLstDatosArchivo() {
		return lstDatosArchivo;
	}
	public void setLstDatosArchivo(List<Object> lstDatosArchivo) {
		this.lstDatosArchivo = lstDatosArchivo;
	}
	public String[] getArrNomColumnas() {
		return arrNomColumnas;
	}
	public void setArrNomColumnas(String[] arrNomColumnas) {
		this.arrNomColumnas = arrNomColumnas;
	}
	public String getStrNombreArchivo() {
		return strNombreArchivo;
	}
	public void setStrNombreArchivo(String strNombreArchivo) {
		this.strNombreArchivo = strNombreArchivo;
	}
}

