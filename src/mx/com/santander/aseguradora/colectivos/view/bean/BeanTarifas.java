/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTarifas;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


/**
 * @author Ing. Hery Tyler
 *
 */
@Controller
@Scope("request")
public class BeanTarifas {

	@Resource
	private ServicioTarifas servicioTarifas;
	
	//Propiedades para lista Tarifas
	private Integer canal;	
	private Short ramo;
	private Integer producto;	
	private Short plan;
	private double tarifa1;
	private double tarifa2;
	private Integer centro;
	private Date fecini;
	private Date fecfin;
	private String descnegocio;
	
	//Propiedades para lista coberturas
	private List<BeanTarifas> lstCoberturas;
	private String cdcobertura; 	// COD_COBERTURA
    private String desccobertura;	// DESC_COBERTURA
    private String status;          // STATUS
    private Integer edadmin;		// EDAD_MIN
    private Integer edadmax;		// EDAD_MAX
    private String porcentaje;		// PORCENTAJE
    private Integer setcoberturas;	// SET_COBERTURAS
    
    //Propiedades para lista componentes
    private List<BeanTarifas> lstComponentes;
    private String cdcomponente;    //COMPONENTE
    private Double tasacomponente;  //TASA
    
    //Propiedades para lista prod/planes
    private List<BeanTarifas> lstProdPlanes;
    private String subproducto;         //SUB_PRODUCTO
	private String tipo;         		//SUB_TIPO
    private String descproducto;     	//DESCRIPCION    
	private String identificador;   	//IDENTIFICADOR
	private String descplan;        	//DESCRIPCION_PLAN

	
	private StringBuilder message;
	private Log log = LogFactory.getLog(this.getClass());

	
	public BeanTarifas() {
		this.message = new StringBuilder();
		
		if(FacesUtils.getBeanSesion().getCurrentBeanEstatus() != null){
			try {
				ConstruirObjeto.poblarBean(this, FacesUtils.getBeanSesion().getCurrentBeanEstatus());
			} catch (Excepciones e) {
				this.message.append("No se puede recuperar el estatus.");
				this.log.error(message.toString(), e);
				throw new FacesException(message.toString(), e);
			}
		}
	}

	public BeanTarifas(Object[] arrObjeto, int tipoConsulta) {
		switch (tipoConsulta) {
			case 1:	// COBERTURAS
				setCdcobertura(arrObjeto[0] == null ? "" : arrObjeto[0].toString());
				setDesccobertura(arrObjeto[1] == null ? "" : arrObjeto[1].toString());
				setTarifa1(arrObjeto[2] == null ? new Double(0) : ((BigDecimal)arrObjeto[2]).doubleValue());
				setStatus(arrObjeto[3] == null ? "" : arrObjeto[3].toString());
				setFecini(arrObjeto[4] == null ? new Date() : (Date) arrObjeto[4]);
				setFecfin(arrObjeto[5] == null ? new Date() :(Date) arrObjeto[5]);
				setEdadmin(arrObjeto[6] == null ? 0 : ((BigDecimal)arrObjeto[6]).intValue());
				setEdadmax(arrObjeto[7] == null ? 0 : ((BigDecimal)arrObjeto[7]).intValue());
				setPorcentaje(arrObjeto[8] == null ? "0" : arrObjeto[8].toString());
				setSetcoberturas(arrObjeto[9] == null ? 0 : ((BigDecimal)arrObjeto[9]).intValue());
				break;
			 
			case 2:	// COMPONENTES
				setCdcomponente(arrObjeto[0]== null ? "" : arrObjeto[0].toString());
				setTasacomponente(arrObjeto[1] == null ? new Double(0) : ((BigDecimal)arrObjeto[1]).doubleValue());
				setStatus(arrObjeto[2] == null ? "" : arrObjeto[2].toString());
				setFecini(arrObjeto[3] == null ? new Date() : (Date) arrObjeto[3]);
				setFecfin(arrObjeto[4] == null ? new Date() :(Date) arrObjeto[4]);
				setSetcoberturas(arrObjeto[5] == null ? 0 : ((BigDecimal)arrObjeto[5]).intValue());
				break;
				
			case 3:	// PRODUCTOS/PLANES
                setRamo(((BigDecimal)arrObjeto[0]).shortValue());
                setSubproducto(arrObjeto[1] == null ? "" : arrObjeto[1].toString());
                setTipo(arrObjeto[2] == null ? "" : arrObjeto[2].toString());
                setDescproducto(arrObjeto[3]== null ? "" : arrObjeto[3].toString());
                setIdentificador(arrObjeto[4] == null ? "N/A" : arrObjeto[4].toString());
				break;
		}
	}

//018007197230  ext 58437

	public ServicioTarifas getServicioTarifas() {
		return servicioTarifas;
	}
	public void setServicioTarifas(ServicioTarifas servicioTarifas) {
		this.servicioTarifas = servicioTarifas;
	}
	public Integer getCanal() {
		return canal;
	}
	public void setCanal(Integer canal) {
		this.canal = canal;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	public Short getPlan() {
		return plan;
	}
	public void setPlan(Short plan) {
		this.plan = plan;
	}
	public Integer getProducto() {
		return producto;
	}
	public void setProducto(Integer producto) {
		this.producto = producto;
	}
	public String getCdcobertura() {
		return cdcobertura;
	}
	public void setCdcobertura(String cdcobertura) {
		this.cdcobertura = cdcobertura;
	}
	public double getTarifa1() {
		return tarifa1;
	}
	public void setTarifa1(double tarifa1) {
		this.tarifa1 = tarifa1;
	}
	public double getTarifa2() {
		return tarifa2;
	}
	public void setTarifa2(double tarifa2) {
		this.tarifa2 = tarifa2;
	}
	public Date getFecini() {
		return fecini;
	}
	public void setFecini(Date fecini) {
		this.fecini = fecini;
	}
	public Date getFecfin() {
		return fecfin;
	}
	public void setFecfin(Date fecfin) {
		this.fecfin = fecfin;
	}
	public Integer getCentro() {
		return centro;
	}
	public void setCentro(Integer centro) {
		this.centro = centro;
	}
	public String getDesccobertura() {
		return desccobertura;
	}
	public void setDesccobertura(String desccobertura) {
		this.desccobertura = desccobertura;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getEdadmin() {
		return edadmin;
	}
	public void setEdadmin(Integer edadmin) {
		this.edadmin = edadmin;
	}
	public Integer getEdadmax() {
		return edadmax;
	}
	public void setEdadmax(Integer edadmax) {
		this.edadmax = edadmax;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	public Integer getSetcoberturas() {
		return setcoberturas;
	}
	public void setSetcoberturas(Integer setcoberturas) {
		this.setcoberturas = setcoberturas;
	}
	public void setLstCoberturas(List<BeanTarifas> lstCoberturas) {
		this.lstCoberturas = lstCoberturas;
	}
	public List<BeanTarifas> getLstCoberturas() {
		return lstCoberturas;
	}
	
	public String getCdcomponente() {
		return cdcomponente;
	}

	public void setCdcomponente(String cdcomponente) {
		this.cdcomponente = cdcomponente;
	}

	public Double getTasacomponente() {
		return tasacomponente;
	}

	public void setTasacomponente(Double tasacomponente) {
		this.tasacomponente = tasacomponente;
	}
	public List<BeanTarifas> getLstProdPlanes() {
		return lstProdPlanes;
	}
	public List<BeanTarifas> getLstComponentes() {
		return lstComponentes;
	}

	public void setLstComponentes(List<BeanTarifas> lstComponentes) {
		this.lstComponentes = lstComponentes;
	}
	public void setLstProdPlanes(List<BeanTarifas> lstProdPlanes) {
		this.lstProdPlanes = lstProdPlanes;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getDescproducto() {
		return descproducto;
	}
	public void setDescproducto(String descproducto) {
		this.descproducto = descproducto;
	}
	public String getSubproducto() {
		return subproducto;
	}
	public void setSubproducto(String subproducto) {
		this.subproducto = subproducto;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescplan() {
		return descplan;
	}
	public void setDescplan(String descplan) {
		this.descplan = descplan;
	}
	public String getDescnegocio() {
		return descnegocio;
	}
	public void setDescnegocio(String descnegocio) {
		this.descnegocio = descnegocio;
	}
}
