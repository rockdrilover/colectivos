package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;


@Controller
@Scope("session")
public class BeanFechaCierre {

	private Log log = LogFactory.getLog(this.getClass());
	private List<Object> listaComboAnios;
	
	@Resource
	private ServicioParametros servicioParametros;
	private Integer anio;
	private Date cierrefin, cierreini;
	private String mes;
	private boolean chkUpdate;
	private List<Object> 			listaFechasCierre;
	private String strRespuesta, status;
	private int 					filaActual;
	private BeanParametros 			objActual = new BeanParametros();
	private BeanParametros 			objNuevo = new BeanParametros();
	private Set<Integer> 			keys = new HashSet<Integer>();
	
	
	public BeanFechaCierre() {
		this.listaComboAnios = new ArrayList<Object>();
		this.listaFechasCierre = new ArrayList<Object>();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_FECHA_CIERRE);
	}
	
	public List<Object> getListaComboAnios() {
		listaComboAnios.clear();
		listaComboAnios.addAll(GestorFechas.getAnios(3, 3));
		return listaComboAnios;
	}

	public void setListaComboAnios(List<Object> listaComboAnios) {
		this.listaComboAnios = listaComboAnios;
	}

	public Date getCierrefin() {
		return cierrefin;
	}

	public void setCierrefin(Date cierrefin) {
		this.cierrefin = cierrefin;
	}

	public Date getCierreini() {
		return cierreini;
	}

	public void setCierreini(Date cierreini) {
		this.cierreini = cierreini;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}
	
	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public boolean isChkUpdate() {
		return chkUpdate;
	}

	public void setChkUpdate(boolean chkUpdate) {
		this.chkUpdate = chkUpdate;
	}
	
	public List<Object> getListaFechasCierre() {
		return listaFechasCierre;
	}
	public void setListaFechasCierre(List<Object> listaFechasCierre) {
		this.listaFechasCierre = listaFechasCierre;
	}
	public int getFilaActual() {
		return filaActual;
	}
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}

	public BeanParametros getObjActual() {
		return objActual;
	}
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	public BeanParametros getObjNuevo() {
		return objNuevo;
	}
	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}
	public Set<Integer> getKeys() {
		return keys;
	}
	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStrRespuesta() {
		return strRespuesta;
	}
	private void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	

	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  getListaFechasCierre().iterator();
        while(it.hasNext()) {
        	BeanParametros objParametro = (BeanParametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActual = objParametro;
                break;
            }
        }
    }
	
	public String consultaFechaCierre(){
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean;
		List<Object> lstResultados = null;
		Parametros parametro;
		Iterator<Object> it;
		
		try {
			getListaFechasCierre().clear();
			if(this.anio != 0) {
				if(!this.mes.equals("0")) {
					filtro.append("and P.copaNvalor1 = " + this.anio + "\n" );
					filtro.append("and P.copaVvalor2 = '" + this.mes + "'\n" );
					filtro.append("and P.copaDesParametro = 'FECHAS_CIERRE_MENSUAL' \n" );
					filtro.append("and P.copaIdParametro  = 'CONTABILIDAD' \n" );
				} else {
					filtro.append("and P.copaNvalor1 = " + this.anio + "\n" );
					filtro.append("and P.copaDesParametro = 'FECHAS_CIERRE_MENSUAL' \n" );
					filtro.append("and P.copaIdParametro  = 'CONTABILIDAD' \n" );
				}
				
				filtro.append("order by to_date(P.copaVvalor3, 'dd/MM/yyyy') asc" );
				lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
				
			    it = lstResultados.iterator();
			    while(it.hasNext()) {
			    	parametro = (Parametros) it.next();
			    	bean = new BeanParametros();
			    	
			    	bean.setCopaCdParametro(parametro.getCopaCdParametro());
			    	bean.setCopaIdParametro(parametro.getCopaIdParametro());
			    	bean.setCopaDesParametro(parametro.getCopaDesParametro());
			    	bean.setCopaNvalor1(parametro.getCopaNvalor1());
			    	bean.setCopaVvalor2(parametro.getCopaVvalor2());
			    	bean.setCopaVvalor3(parametro.getCopaVvalor3());
			    	bean.setCopaVvalor4(parametro.getCopaVvalor4());
			    	bean.setCopaNvalor2(parametro.getCopaNvalor2());
			    	bean.setCopaNvalor3(parametro.getCopaNvalor2());
			    	
			    	if(parametro.getCopaNvalor2() == 0){
			    		bean.setCopaVvalor8("Cancelado");
			    	} else if (parametro.getCopaNvalor2() == 1){
			    		bean.setCopaVvalor8("Por Ejecutar Cierre Mensual");
			    	} else{
			    		bean.setCopaVvalor8("Cerrado - Ejecuto Cierre Mensual");
			    	}
			    	
			    	objActual = bean;
					listaFechasCierre.add(bean);
				}
			    setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");
			} else {
				setStrRespuesta("Selecciona un A�o para poder realizar la consulta.");
			}
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.log.error("La fecha de cierre ya existe no es posble darlo de alta nuevamente.", e);
			FacesUtils.addErrorMessage("La fecha de cierre ya existe no es posble darlo de alta nuevamente.");
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			e.printStackTrace();
			this.log.error("No se puede dar de alta la fecha de cierre.", e);
			FacesUtils.addErrorMessage("No se puede dar de alta la fecha de cierre.");
			return NavigationResults.FAILURE;
		}
	}
	
	public void modificar(){
		try {
			
			objActual.setCopaVvalor3(GestorFechas.formatDate(objNuevo.getCopaFvalor1(), Constantes.FORMATO_FECHA_UNO));
			objActual.setCopaVvalor4(GestorFechas.formatDate(objNuevo.getCopaFvalor2(), Constantes.FORMATO_FECHA_UNO));
			
			if(objActual.getCopaNvalor3() == 2L) {
				setStrRespuesta("No se puede modificar registro seleccionado, por que ya se ejecuto el cierre mensual para este periodo.");
			} else {				
				if (!objActual.getCopaNvalor2().equals("-1")) {
					objActual.setCopaNvalor3(null);
					objActual.setCopaVvalor8("");
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual) ;
					
					servicioParametros.actualizarObjeto(parametro);
					setStrRespuesta("Se modifico con EXITO el registro.");
					
					if(objActual.getCopaNvalor2() == 0){
						objActual.setCopaVvalor8("Cancelado");
			    	} else if (parametro.getCopaNvalor2() == 1){
			    		objActual.setCopaVvalor8("Por Ejecutar Cierre Mensual");
			    	} else{
			    		objActual.setCopaVvalor8("Cerrado - Ejecuto Cierre Mensual");
			    	}
					
					listaFechasCierre.clear();
					listaFechasCierre.add(objActual);
				} else {
					setStrRespuesta("No se puede modificar registro seleccionado, favor de seleccionar un estatus.");
				}
			}			
		} catch (Exception e) {
			setStrRespuesta("Error al MODIFICAR registro.");
			e.printStackTrace();
		}
	} 
	
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(listaFechasCierre);
        return lstValues;
    }
	
	public void guardar() {
		Long secParametro;
		
		try {
			secParametro = servicioParametros.siguente();
			objNuevo.setCopaCdParametro(secParametro);
			objNuevo.setCopaIdParametro("CONTABILIDAD");
			objNuevo.setCopaDesParametro("FECHAS_CIERRE_MENSUAL");
			objNuevo.setCopaNvalor2(1L);
			
			
			objNuevo.setCopaVvalor3(GestorFechas.formatDate(objNuevo.getCopaFvalor1(), Constantes.FORMATO_FECHA_UNO));
			objNuevo.setCopaVvalor4(GestorFechas.formatDate(objNuevo.getCopaFvalor2(), Constantes.FORMATO_FECHA_UNO));
			
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
			this.servicioParametros.guardarObjeto(parametro);
			setStrRespuesta("Se guardo con EXITO el registro.");
			
			objNuevo = new BeanParametros();
		} catch (Exception e) {
			setStrRespuesta("Error al GUARDAR registro.");
			e.printStackTrace();
		}
	}
}
