package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Usuario;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioUsuario;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Mail;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.AuthenticationManager;
import org.springframework.security.BadCredentialsException;
import org.springframework.security.ui.AbstractProcessingFilter;
import org.springframework.stereotype.Component;

/**
 * @author Sergio Plata
 * Modificado: Ing. Issac Bautista
 *
 */
@Component
@Scope("request")
public class BeanLogin {
	
	@Resource
	private ServicioUsuario servicioUsuario;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private Usuario usuario;
	private int nOpcion = 0;  //0 - Muestra Login 1 - Muestra Cambio de Password
	private String username;
	private String password;
	private String passwordrep;
	private String passwordnew;
	private AuthenticationManager authentication;
	private String respuesta;


	public BeanLogin() {
		this.message = new StringBuilder();
		usuario = new Usuario();
	}
	
	
	/**
	 * Metodo Login para ingresar al modulo
	 * @return si login es correcto regresa nulo 
	 * @throws Excepciones errores de ingreso
	 */
	public String login() throws Excepciones {
		this.message.append("Validando usuario.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		ExternalContext context;
		
		try {
			setRespuesta(Constantes.DEFAULT_STRING);
			context = FacesContext.getCurrentInstance().getExternalContext();
			if(getUsername().equals(Constantes.DEFAULT_STRING)) {
				setRespuesta("* Campo Usuario requerido.");
			}
			
			if(getPassword().equals(Constantes.DEFAULT_STRING)) {
				setRespuesta(getRespuesta() + "   " + "* Campo Contraseña requerido.");
			}
			
			if(getRespuesta().equals(Constantes.DEFAULT_STRING)) {
				setUsuario(this.servicioUsuario.obtenerObjeto(Usuario.class, getUsername()));
				FacesUtils.getBeanSesion().setObjUsuario(getUsuario());
				
				if(getUsuario() != null){
					validaCambioPassword(context);
				} else {
					setRespuesta("No se encontro Usuario.");
				}
			}
		} catch (Exception e) {
			setRespuesta("Error al recuperar datos del usurio.");
			this.log.error(getRespuesta(), e);
			FacesUtils.addErrorMessage(getRespuesta());
		}
		
		return null;
	}

	/**
	 * Metodo que valida si password caduco
	 * @param context: contexto de session
	 * @throws IOException: errores de IO  
	 * @throws ServletException: errores de servlet
	 */
	private void validaCambioPassword(ExternalContext context) throws ServletException, IOException {
		RequestDispatcher dispatcher;
	
		if(GestorFechas.comparaFechas(getUsuario().getCampo3(), new Date()) == 1) {
			setRespuesta("Favor de cambiar contraseña, contraseña anterior ha caducado.");
		} else {
			dispatcher = ((ServletRequest)context.getRequest()).getRequestDispatcher("/j_spring_security_check");
			dispatcher.forward((ServletRequest)context.getRequest(), (ServletResponse)context.getResponse());
			FacesContext.getCurrentInstance().responseComplete();
			
			FacesUtils.getBeanSesion().setUserName(getUsuario().getUsername());
			FacesUtils.getBeanSesion().setNombreUsuario(getUsuario().toString());
			
			HttpServletRequest request = (HttpServletRequest) context.getRequest();
			HttpSession sesion = request.getSession(true);
			
			sesion.setAttribute("username", getUsuario().getUsername());
			sesion.setAttribute("nombre", getUsuario().toString());
			sesion.setAttribute("correo", getUsuario().getCorreo());
			sesion.setAttribute("fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		}
	}
	
	/**
	 * 
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * @throws Excepciones 
	 */
	public String verPantalla() throws ServletException, IOException, Excepciones{		
		
		try {
			setRespuesta(Constantes.DEFAULT_STRING);
			if(getUsuario().getUsername() == null){
				if(getUsername().equals(Constantes.DEFAULT_STRING)) {
					setRespuesta("Campo Usuario requerido.");
				}
				
				if(getRespuesta().equals(Constantes.DEFAULT_STRING)) {
					setUsuario(FacesUtils.getBeanSesion().getObjUsuario());
					if(getUsuario() == null) {
						setUsuario(this.servicioUsuario.obtenerObjeto(Usuario.class, getUsername()));
						FacesUtils.getBeanSesion().setObjUsuario(getUsuario());
					}
					setnOpcion(1);
				}
			} else {
				setnOpcion(1);
			}
		} catch (Exception e) {
			setRespuesta("Error al recuperar datos del usurio.");
			this.log.error(getRespuesta(), e);
			FacesUtils.addErrorMessage(getRespuesta());
		}
		
		return null;
	}
	
	/**
	 * 
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * @throws Excepciones 
	 */
	public String resetPass() throws ServletException, IOException, Excepciones{		
		StringBuilder sbMsgCorreo = new StringBuilder();
		String strNewDate;
		
		try {
			setRespuesta(Constantes.DEFAULT_STRING);
			if(getUsuario().getUsername() == null){
				if(getPasswordnew().equals(Constantes.DEFAULT_STRING)) {
					setRespuesta("* Campo Contraseña requerido.");
				}
				
				if(getPasswordrep().equals(Constantes.DEFAULT_STRING)) {
					setRespuesta(getRespuesta() + "    " + "* Campo Repetir Contraseña requerido.");
				}
				
				if(getRespuesta().equals(Constantes.DEFAULT_STRING)) {
					setRespuesta(this.servicioUsuario.validarContraseñaNueva(getPasswordnew().trim(), getPasswordrep().trim()));
					if(getRespuesta().equals(Constantes.DEFAULT_STRING)) {
						setUsuario(FacesUtils.getBeanSesion().getObjUsuario());
						getUsuario().setCampo1(getPasswordnew().trim());
						getUsuario().setPassword(Utilerias.encriptar(getUsuario().getCampo1()));
						strNewDate = GestorFechas.addMesesaFecha(getUsuario().getCampo3(), 1);
						getUsuario().setCampo3(GestorFechas.generateDate(strNewDate, "dd/MM/yyyy"));
						this.servicioUsuario.actualizarObjeto(getUsuario());
						setRespuesta("Se actualizo con exito la contraseña. Se envio un respaldo al siguiente E-mail: " + getUsuario().getCorreo());
						
						sbMsgCorreo.append("Buen Dia. \n\n");
						sbMsgCorreo.append(getUsuario().toString()).append("\n");
						sbMsgCorreo.append("Su contraseña ha sido cambiada satisfactoriamente. \n\n\n");
						sbMsgCorreo.append("Contraseña: ").append(getUsuario().getCampo1()).append("\n");
						sbMsgCorreo.append("Vigencia hasta: ").append(GestorFechas.formatDate(getUsuario().getCampo3(), "EEE, d MMM yyyy")).append("\n\n");
						Mail.enviar2(new ArrayList<String>(), sbMsgCorreo, "Cambio Contraseña", getUsuario().getCorreo(), Constantes.DEFAULT_STRING, Constantes.DEFAULT_STRING);
						setnOpcion(0);
					} else {
						setUsuario(FacesUtils.getBeanSesion().getObjUsuario());
						setnOpcion(1);
					}
				} else {
					setUsuario(FacesUtils.getBeanSesion().getObjUsuario());
					setnOpcion(1);
				}
			} else {
				setnOpcion(1);
			}
		} catch (Exception e) {
			setRespuesta("Error al recuperar datos del usurio.");
			this.log.error(getRespuesta(), e);
			FacesUtils.addErrorMessage(getRespuesta());
		}
		
		return null;
	}
	
	/**
	 * 
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * @throws Excepciones 
	 */
	public String cancelar() throws ServletException, IOException, Excepciones{		
		
		try {
			setnOpcion(0);
			setRespuesta(Constantes.DEFAULT_STRING);
			setUsuario(new Usuario());
			setUsername(Constantes.DEFAULT_STRING);
			setPassword(Constantes.DEFAULT_STRING);
			setPasswordnew(Constantes.DEFAULT_STRING);
			setPasswordrep(Constantes.DEFAULT_STRING);
		} catch (Exception e) {
			setRespuesta("Error al cacelar.");
			this.log.error(getRespuesta(), e);
			FacesUtils.addErrorMessage(getRespuesta());
		}
		
		return null;
	}
	/**
	 * 
	 */
	@PostConstruct
    @SuppressWarnings("unused")
	private void handleErrorMessage() {
		Exception e = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY);
		if(e instanceof BadCredentialsException){
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY, null);
			FacesUtils.addErrorMessage("Nombre de usuario o password incorrecto.");
			setRespuesta("Nombre de usuario o password incorrecto.");
	    }
    }
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public int getnOpcion() {
		return nOpcion;
	}
	public void setnOpcion(int nOpcion) {
		this.nOpcion = nOpcion;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public void setServicioUsuario(ServicioUsuario servicioUsuario) {
		this.servicioUsuario = servicioUsuario;
	}
	public ServicioUsuario getServicioUsuario() {
		return servicioUsuario;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setAuthentication(AuthenticationManager authentication) {
		this.authentication = authentication;
	}
	public AuthenticationManager getAuthentication() {
		return authentication;
	}
	public String getPasswordrep() {
		return passwordrep;
	}
	public void setPasswordrep(String passwordrep) {
		this.passwordrep = passwordrep;
	}
	public String getPasswordnew() {
		return passwordnew;
	}
	public void setPasswordnew(String passwordnew) {
		this.passwordnew = passwordnew;
	}

}
