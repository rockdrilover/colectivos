package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

@Controller
@Scope("session")
public class BeanReserva {

	@Resource
	private ServicioProcesos servicioProcesos;
	private List<Object> listaMes;
	private int mes;

	private int ramoFiltro;
	private int mesFiltro;

	private int tipoReporte;

	public BeanReserva() {
		super();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_RESERVAS);
		listaMes = new ArrayList<Object>();
		mes = 0;
		ramoFiltro = 0;
		mesFiltro = 0;

		tipoReporte = 0;
	}

	public void procesaReservas() {

		try {
			Date fecha = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(fecha);
			int anio = cal.get(Calendar.YEAR);
			servicioProcesos.procesaReserva(anio, mes);
			FacesUtils.addInfoMessage("Exito al generar informacion.");
		} catch (Exception e) {
			FacesUtils.addInfoMessage("Error al generar informacion.");
		}
	}

	public void consultarReservas() {
		try {
			Date fecha = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(fecha);
			int anio = cal.get(Calendar.YEAR);
			List<Object> res = servicioProcesos.consultaReserva(ramoFiltro, mesFiltro, anio);
			FacesUtils.addInfoMessage("Exito al generar informacion.");
		} catch (Exception e) {
			FacesUtils.addInfoMessage("Error al generar informacion.");
		}
	}

	public List<Object> getListaMes() {
		listaMes.clear();
		Date fecha = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		int mes = cal.get(Calendar.MONTH);
		mes = mes + 1;
		if (mes >= 1)
			listaMes.add(new SelectItem(1, "Enero"));
		if (mes >= 2)
			listaMes.add(new SelectItem(2, "Febrero"));
		if (mes >= 3)
			listaMes.add(new SelectItem(3, "Marzo"));
		if (mes >= 4)
			listaMes.add(new SelectItem(4, "Abril"));
		if (mes >= 5)
			listaMes.add(new SelectItem(5, "Mayo"));
		if (mes >= 6)
			listaMes.add(new SelectItem(6, "Junio"));
		if (mes >= 7)
			listaMes.add(new SelectItem(7, "Julio"));
		if (mes >= 8)
			listaMes.add(new SelectItem(8, "Agosto"));
		if (mes >= 9)
			listaMes.add(new SelectItem(9, "Septiembre"));
		if (mes >= 10)
			listaMes.add(new SelectItem(10, "Octubre"));
		if (mes >= 11)
			listaMes.add(new SelectItem(11, "Noviembre"));
		if (mes >= 12)
			listaMes.add(new SelectItem(12, "Diciembre"));
		return listaMes;
	}

	public void setListaMes(List<Object> listaMes) {
		this.listaMes = listaMes;
	}

	/**
	 * @return the mes
	 */
	public int getMes() {
		return mes;
	}

	/**
	 * @param mes
	 *            the mes to set
	 */
	public void setMes(int mes) {
		this.mes = mes;
	}

	/**
	 * @return the ramoFiltro
	 */
	public int getRamoFiltro() {
		return ramoFiltro;
	}

	/**
	 * @param ramoFiltro
	 *            the ramoFiltro to set
	 */
	public void setRamoFiltro(int ramoFiltro) {
		this.ramoFiltro = ramoFiltro;
	}

	/**
	 * @return the mesFiltro
	 */
	public int getMesFiltro() {
		return mesFiltro;
	}

	/**
	 * @param mesFiltro
	 *            the mesFiltro to set
	 */
	public void setMesFiltro(int mesFiltro) {
		this.mesFiltro = mesFiltro;
	}

	/**
	 * @return the tipoReporte
	 */
	public int getTipoReporte() {
		return tipoReporte;
	}

	/**
	 * @param tipoReporte
	 *            the tipoReporte to set
	 */
	public void setTipoReporte(int tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

}
