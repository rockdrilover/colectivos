/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioReporteador;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.richfaces.component.html.HtmlProgressBar;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author FEBG
 * @version mod Ing IBB
 *
 */
@Controller
@Scope("session")
public class BeanReporteador {

	private Log log = LogFactory.getLog(this.getClass());
	@Resource
	private ServicioReporteador servicioReporteador;
	private Integer inOpcionRep;
	private Short inRamo;
	private Long inPoliza;
	private Integer inIdVenta;
	private Integer estatus;
	private Integer tipoarchivo;
	private Integer inReporteEspecial;
	private Integer inFiltroFechas;
	private Integer mes;
	private Boolean habilitaCombo;

	private Date fechainicio;
	private Date fechafin;
	private String separador = ",";
	private HtmlSelectOneMenu hsomEstatus;
	private HtmlSelectOneMenu hsomTipoArchivo;
	private HtmlSelectOneMenu initRamo;

	private List<Object> listaComboEstatus;
	private List<Object> listaComboTipoArchivo;
	private List<Object> listaMes;
	private List<Object> listaComboIdVenta;
	private List<Object> lstColumnas;
	private List<Object> lstResultados;
	private List<Object> lstResultadosReportesEspeciales;
	private String respuesta = "";
	private String campos1[] = new String[10];
	private String campos2[] = new String[10];
	private String campos3[] = new String[10];
	private String campos4[] = new String[10];
	private String campos5[] = new String[10];
	private String campos6[] = new String[10];
	private String campos7[] = new String[10];
	private List<Object> listaCampos1;
	private List<Object> listaCampos2;
	private List<Object> listaCampos3;
	private List<Object> listaCampos4;
	private List<Object> listaCampos5;
	private List<Object> listaCampos6;
	private List<Object> listaCampos7;
	private Map<String, List<BeanMesFiscal>> canceladas;
	private ArrayList<List<BeanMesFiscal>> lstDatos;

	private StringBuilder message;
	private ProgressBar pb;

	private Integer inReporteComisiones;

	private Short inRamoRC;
	private Long inPolizaRC;
	private Integer inIdVentaRC;
	private Integer estatusRC;
	private Integer mesRC;
	private Date fechainicioRC;
	private Date fechafinRC;
	private Integer inFiltroFechasRC;

	public BeanReporteador() {
		setListaComboEstatus(new ArrayList<Object>());
		setListaComboTipoArchivo(new ArrayList<Object>());
		setListaMes(new ArrayList<Object>());
		setListaComboIdVenta(new ArrayList<Object>());
		this.message = new StringBuilder();
		this.inIdVenta = 0;
		this.setHabilitaCombo(true);
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_REPORTEADOR);
		setListaCampos1(new ArrayList<Object>());
		setListaCampos2(new ArrayList<Object>());
		setListaCampos3(new ArrayList<Object>());
		setListaCampos4(new ArrayList<Object>());
		setListaCampos5(new ArrayList<Object>());
		setListaCampos6(new ArrayList<Object>());
		setListaCampos7(new ArrayList<Object>());
		canceladas = new HashMap<String, List<BeanMesFiscal>>();
		lstDatos = new ArrayList<List<BeanMesFiscal>>();
		eliminarListassession();
		inRamo = 0;
		inPoliza = 0L;
		inIdVenta = tipoarchivo = inReporteEspecial = null;
		// lstResultados = new ArrayList<Object>();
	}

	public void setColumnas() {
		try {
			setCampos1(new String[10]);
			setCampos2(new String[10]);
			setCampos3(new String[10]);
			setCampos4(new String[10]);
			setCampos5(new String[10]);
			setCampos6(new String[10]);
			setCampos7(new String[10]);
			if (getEstatus() == 1 || getEstatus() == 2 || getEstatus() == 3) {
				setCamposEmitidas();
			} else {
				setCamposOtros();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setCamposEmitidas() {
		listaCampos1.clear();
		listaCampos1.add(new SelectItem("1-Ramo", "Ramo"));
		listaCampos1.add(new SelectItem("2-Centro Costos", "Centro Costos"));
		listaCampos1.add(new SelectItem("3-Poliza", "Poliza"));
		listaCampos1.add(new SelectItem("4-Recibo", "Recibo"));
		listaCampos1.add(new SelectItem("5-Credito", "Credito"));
		listaCampos1.add(new SelectItem("6-Certificado", "Certificado"));
		listaCampos1.add(new SelectItem("7-Identificacion Cuota", "Identificacion Cuota"));
		listaCampos1.add(new SelectItem("8-Estatus", "Estatus"));
		listaCampos1.add(new SelectItem("9-Estatus Movimiento", "Estatus Movimiento"));

		listaCampos2.clear();
		listaCampos2.add(new SelectItem("10-Cuenta", "Cuenta"));
		listaCampos2.add(new SelectItem("11-Producto", "Producto"));
		listaCampos2.add(new SelectItem("12-Subproducto", "Subproducto"));
		listaCampos2.add(new SelectItem("13-Sucursal", "Sucursal"));
		listaCampos2.add(new SelectItem("56-Apellido Paterno", "Apellido Paterno"));
		listaCampos2.add(new SelectItem("57-Apellido Materno", "Apellido Materno"));
		listaCampos2.add(new SelectItem("14-Nombre", "Nombre"));
		listaCampos2.add(new SelectItem("15-Sexo", "Sexo"));
		listaCampos2.add(new SelectItem("16-Fecha Nacimiento", "Fecha Nacimiento"));

		listaCampos3.clear();
		listaCampos3.add(new SelectItem("17-Numero Cliente", "Numero Cliente"));
		listaCampos3.add(new SelectItem("18-RFC", "RFC"));
		listaCampos3.add(new SelectItem("19-Plazo", "Plazo"));
		listaCampos3.add(new SelectItem("20-Fecha Inicio Poliza", "Fecha Inicio Poliza"));
		listaCampos3.add(new SelectItem("21-Fecha Fin Poliza", "Fecha Fin Poliza"));
		listaCampos3.add(new SelectItem("22-Fecha Ingreso", "Fecha Ingreso"));
		listaCampos3.add(new SelectItem("23-Fecha Desde", "Fecha Desde"));
		listaCampos3.add(new SelectItem("24-Fecha Hasta", "Fecha Hasta"));

		listaCampos4.clear();
		listaCampos4.add(new SelectItem("25-Fecha Inicio Credito", "Fecha Inicio Credito"));
		listaCampos4.add(new SelectItem("26-Fecha Fin Credito", "Fecha Fin Credito"));
		listaCampos4.add(new SelectItem("27-Fecha Fin Credito 2", "Fecha Fin Credito 2"));
		listaCampos4.add(new SelectItem("28-Fecha Anulacion", "Fecha Anulacion"));
		listaCampos4.add(new SelectItem("29-Fecha Cancelacion", "Fecha Cancelacion"));
		listaCampos4.add(new SelectItem("30-Suma Asegurada", "Suma Asegurada"));
		listaCampos4.add(new SelectItem("31-Base Calculo", "Base Calculo"));
		listaCampos4.add(new SelectItem("32-Prima Neta", "Prima Neta"));
		listaCampos4.add(new SelectItem("33-Derechos", "Derechos"));

		listaCampos5.clear();
		listaCampos5.add(new SelectItem("34-Recargos", "Recargos"));
		listaCampos5.add(new SelectItem("35-IVA", "IVA"));
		listaCampos5.add(new SelectItem("36-Prima Total", "Prima Total"));
		listaCampos5.add(new SelectItem("37-Tarifa", "Tarifa"));
		listaCampos5.add(new SelectItem("38-Prima vida", "Prima Vida"));
		listaCampos5.add(new SelectItem("39-Prima Desempleo", "Prima Desempleo"));
		listaCampos5.add(new SelectItem("40-Estado", "Estado"));
		listaCampos5.add(new SelectItem("41-Municipio", "Municipio"));
		listaCampos5.add(new SelectItem("42-CP", "CP"));

		listaCampos6.clear();
		listaCampos6.add(new SelectItem("43-Monto Devolucion", "Monto Devolucion"));
		listaCampos6.add(new SelectItem("44-Monto Devolucion SIS", "Monto Devolucion Sis"));
		listaCampos6.add(new SelectItem("45-Diferencia Devolucion", "Diferencia Devolucion"));
		listaCampos6.add(new SelectItem("46-Credito Nuevo", "Credito Nuevo"));
		listaCampos6.add(new SelectItem("47-Producto Aseguradora", "Producto Aseguradora"));
		listaCampos6.add(new SelectItem("48-Plan Aseguradora", "Plan Aseguradora"));
		listaCampos6.add(new SelectItem("49-Cuota Basica", "Cuota Basica"));
		listaCampos6.add(new SelectItem("50-Cuota Desempleo", "Cuota Desempleo"));
		listaCampos6.add(new SelectItem("51-Datos Obligado PU", "Datos Obligado PU"));

		listaCampos7.clear();
		listaCampos7.add(new SelectItem("52-Fecha Emision", "Fecha Emision"));
		listaCampos7.add(new SelectItem("53-Fecha Envio", "Fecha Envio"));
		listaCampos7.add(new SelectItem("54-Fecha Carga", "Fecha Carga"));
		listaCampos7.add(new SelectItem("55-Fecha Recibido", "Fecha Recibido"));
	}

	private void setCamposOtros() {
		listaCampos1.clear();
		listaCampos2.clear();
		listaCampos3.clear();
		listaCampos4.clear();
		listaCampos5.clear();
		listaCampos6.clear();
		listaCampos7.clear();
	}

	public void limpiaDatos() {
		setInRamo(new Short("0"));
		setInPoliza(-1L);
		setInIdVenta(Constantes.DEFAULT_INT);
		setInFiltroFechas(Constantes.DEFAULT_INT);
		setMes(1);
		setFechainicio(null);
		setFechafin(null);
		setEstatus(Constantes.DEFAULT_INT);
		setTipoarchivo(Constantes.DEFAULT_INT);

		setInRamoRC(new Short("0"));
		setInPolizaRC(-1L);
		setInIdVentaRC(Constantes.DEFAULT_INT);
		setInFiltroFechasRC(Constantes.DEFAULT_INT);
		setMesRC(1);
		setFechainicioRC(null);
		setFechafinRC(null);
		setEstatusRC(Constantes.DEFAULT_INT);

	}

	@SuppressWarnings("unchecked")
	public String consultar() {
		Object arrCampos[];

		try {
			if (validarDatos()) {
				arrCampos = getCamposColumnas();
				lstColumnas = (List<Object>) arrCampos[1];
				lstResultados = servicioReporteador.consultarRegistros(getInRamo(), getInPoliza(), getInIdVenta(),
						getEstatus(), getFechafin(), getFechainicio(), arrCampos[0].toString());
				setRespuesta("Consulta realizada con exito, favor de revisar resultados.");
			}

			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			setRespuesta("Error al consultar informacion.");
			log.error(getRespuesta(), e);
			return NavigationResults.FAILURE;
		}
	}

	private boolean validarDatos() {
		boolean blnResultado = true;

		if (getInRamo().intValue() == Constantes.DEFAULT_INT) {
			setRespuesta("Favor de seleccionar un RAMO.");
			blnResultado = false;
		} else if (getInPoliza().intValue() == -1) {
			setRespuesta("Favor de seleccionar una POLIZA.");
			blnResultado = false;
		} else if (getInPoliza().intValue() == Constantes.DEFAULT_INT
				&& (getInIdVenta() == null || getInIdVenta().intValue() == Constantes.DEFAULT_INT)) {
			setRespuesta("Favor de seleccionar una ID VENTA.");
			blnResultado = false;
		} else if (getEstatus().intValue() == Constantes.DEFAULT_INT) {
			setRespuesta("Favor de seleccionar una ESTATUS.");
			blnResultado = false;
		} else if (getFechainicio() == null) {
			setRespuesta("Favor de ingresar FECHA INICIO");
			blnResultado = false;
		} else if (getFechafin() == null) {
			setRespuesta("Favor de ingresar FECHA FIN");
			blnResultado = false;
		} else if (getFechainicio().after(getFechafin())) {
			setRespuesta("Fecha Inicio no debe ser mayor a Fecha Fin");
			blnResultado = false;
		} else if (getCampos1().length == 0 && getCampos2().length == 0 && getCampos3().length == 0
				&& getCampos4().length == 0 && getCampos5().length == 0 && getCampos6().length == 0
				&& getCampos7().length == 0) {
			setRespuesta("Seleccionar al menos un campo para poder hacer la consulta");
			blnResultado = false;
		}

		return blnResultado;
	}

	private boolean validarDatosRE() {
		boolean blnResultado = true;

		if (getInReporteEspecial() == 3) {
			if (getInRamo().intValue() == Constantes.DEFAULT_INT) {
				setRespuesta("Favor de seleccionar un RAMO.");
				blnResultado = false;
			} else if (getInPoliza().intValue() == -1) {
				setRespuesta("Favor de seleccionar una POLIZA.");
				blnResultado = false;
			} else if (getInPoliza().intValue() == Constantes.DEFAULT_INT
					&& (getInIdVenta() == null || getInIdVenta().intValue() == Constantes.DEFAULT_INT)) {
				setRespuesta("Favor de seleccionar una ID VENTA.");
				blnResultado = false;
			} else if (getInFiltroFechas() == Constantes.DEFAULT_INT) {
				setRespuesta("Favor de seleccionar una FILTRO FECHAS.");
				blnResultado = false;
			} else if (getInFiltroFechas() == 2) {
				if (getFechainicio() == null) {
					setRespuesta("Favor de ingresar FECHA INICIO");
					blnResultado = false;
				} else if (getFechafin() == null) {
					setRespuesta("Favor de ingresar FECHA FIN");
					blnResultado = false;
				} else if (getFechainicio().after(getFechafin())) {
					setRespuesta("Fecha Inicio no debe ser mayor a Fecha Fin");
					blnResultado = false;
				}
			}
		} else if (getInReporteEspecial() == 1 || getInReporteEspecial() == 2 || getInReporteEspecial() == 6) {
			if (getInFiltroFechas() == Constantes.DEFAULT_INT) {
				setRespuesta("Favor de seleccionar una FILTRO FECHAS.");
				blnResultado = false;
			} else if (getInFiltroFechas() == 2) {
				if (getFechainicio() == null) {
					setRespuesta("Favor de ingresar FECHA INICIO");
					blnResultado = false;
				} else if (getFechafin() == null) {
					setRespuesta("Favor de ingresar FECHA FIN");
					blnResultado = false;
				} else if (getFechainicio().after(getFechafin())) {
					setRespuesta("Fecha Inicio no debe ser mayor a Fecha Fin");
					blnResultado = false;
				}
			}
		}

		return blnResultado;
	}

	/**
	 * Valida parametros RC
	 * 
	 * @return parametros validos
	 */
	private boolean validarDatosRC() {
		boolean blnResultado = true;

		
		if (getInFiltroFechasRC() == Constantes.DEFAULT_INT) {
			setRespuesta("Favor de seleccionar una FILTRO FECHAS.");
			blnResultado = false;
		}
		
		if (getInFiltroFechasRC() == 2) {
			if (getFechainicioRC() == null) {
				setRespuesta("Favor de ingresar FECHA INICIO");
				blnResultado = false;
			} else if (getFechafinRC() == null) {
				setRespuesta("Favor de ingresar FECHA FIN");
				blnResultado = false;
			} else if (getFechainicioRC().after(getFechafinRC())) {
				setRespuesta("Fecha Inicio no debe ser mayor a Fecha Fin");
				blnResultado = false;
			}  else if (GestorFechas.restarFechas(getFechainicioRC(), getFechafinRC()) > 31) {
				setRespuesta("Fecha Fin no debe ser mayor a Fecha inicio en 31 dias");
				blnResultado = false;
			}
		}

		return blnResultado;
	}

	/**
	 * Consulta reporte comisiones
	 * 
	 * @return
	 */
	public String consultaReporteComisiones() {
		String strFechaIni = null, strFechaFin = null;
		String query = "Error al generar informacion.";
		try {
			if (validarDatosRC()) {

				if (getInFiltroFechasRC() == 1) { // Fechas por mes
					strFechaIni = String.format("01/%02d/%d", mesRC, Calendar.getInstance().get(Calendar.YEAR));
					Integer nDias = (Integer) GestorFechas
							.getDatosFecha(GestorFechas.generateDate(strFechaIni, "dd/MM/yyyy"), Constantes.DIAS_MES);
					strFechaFin = Utilerias.agregarCaracteres(nDias.toString(), 1, '0', 1) + "/"
							+ Utilerias.agregarCaracteres(mesRC.toString(), 1, '0', 1) + "/"
							+ Calendar.getInstance().get(Calendar.YEAR);
				} else if (getInFiltroFechasRC() == 2) {// Fechas por dias
					strFechaIni = GestorFechas.formatDate(getFechainicioRC(), "dd/MM/yyyy");
					strFechaFin = GestorFechas.formatDate(getFechafinRC(), "dd/MM/yyyy");
				}
				inReporteEspecial = 0;
				query = servicioReporteador.consultarRegistrosComisiones(inReporteComisiones,
						getInRamoRC(), getInPolizaRC(), Integer.valueOf(getInIdVentaRC()), strFechaFin, strFechaIni,
						"");
				

				setRespuesta(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			setRespuesta(query);
			log.error(getRespuesta(), e);
			return NavigationResults.FAILURE;
		}
		return NavigationResults.SUCCESS;
	}

	public String consultaReporteEspecial() {

		try {
			if (validarDatosRE()) {
				switch (inReporteEspecial) {
				case 1:
					lstResultadosReportesEspeciales = servicioReporteador.consultarRegistrosEspeciales(
							getInReporteEspecial(), getMes(), getInFiltroFechas(), getFechainicio(), getFechafin());
					exportaExcel();
					break;
				case 2:
					lstResultadosReportesEspeciales = servicioReporteador.consultarRegistrosEspeciales(
							getInReporteEspecial(), getMes(), getInFiltroFechas(), getFechainicio(), getFechafin());
					exportaExcel();
					break;
				case 3:
					reporteFiscal();
					break;
				case 4:
					reporteEstimacion();
					break;
				case 5:
					lstResultadosReportesEspeciales = servicioReporteador.consultarRegistrosEspeciales(
							getInReporteEspecial(), getMes(), getInFiltroFechas(), getFechainicio(), getFechafin());
					exportaExcel();
					break;
				case 6:
					reporteCuentas();
					reporteCuentaSeguimiento();
					break;
				}
				setRespuesta("Se descargo con exito el reporte.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			setRespuesta("Error al consultar informacion.");
			log.error(getRespuesta(), e);
			return NavigationResults.FAILURE;
		}
		return NavigationResults.SUCCESS;
	}

	private void reporteEstimacion() {
		List<Object> resultado = null;
		String strNombre = null, strRutaTemp;
		Workbook libro;
		FileOutputStream archivo;
		File archivoXLS;
		Sheet hoja;
		Iterator<Object> it;
		Archivo objDescarga = new Archivo();
		Object[] arrDatos = null;
		boolean primeraFila;

		try {
			resultado = new ArrayList<Object>();
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			strNombre = "Reporte Estimacion CR - " + new SimpleDateFormat("MMyyyy").format(new Date());
			archivoXLS = new File(strRutaTemp + strNombre);
			libro = new HSSFWorkbook();
			archivo = new FileOutputStream(archivoXLS);
			hoja = libro.createSheet("Prima_Recurrente");

			resultado = servicioReporteador.consultarRegistrosEspeciales2(getInReporteEspecial() * 10); // Prima
																										// Recurrente
			it = resultado.iterator();
			primeraFila = true;
			for (int f = 0; f < resultado.size(); f++) {
				Row fila = hoja.createRow(f);
				arrDatos = (Object[]) it.next();
				for (int c = 0; c < arrDatos.length; c++) {

					Cell celda = (Cell) fila.createCell(c);
					celda.setCellValue(arrDatos[c] != null ? String.valueOf(arrDatos[c]) : "");

					if (primeraFila) {
						hoja.setColumnWidth(c, 20 * 210);
					}
				}
				if (primeraFila) {
					primeraFila = false;
				}
			}

			hoja = libro.createSheet("Prima_Unica");
			resultado = servicioReporteador.consultarRegistrosEspeciales2((getInReporteEspecial() * 10) + 1); // Prima
																												// Unica
			it = resultado.iterator();
			primeraFila = true;
			for (int f = 0; f < resultado.size(); f++) {
				Row fila = hoja.createRow(f);
				arrDatos = (Object[]) it.next();
				for (int c = 0; c < arrDatos.length; c++) {

					Cell celda = (Cell) fila.createCell(c);
					celda.setCellValue(arrDatos[c] != null ? String.valueOf(arrDatos[c]) : "");

					if (primeraFila) {
						hoja.setColumnWidth(c, 20 * 210);
					}
				}
				if (primeraFila) {
					primeraFila = false;
				}
			}

			libro.write(archivo);
			archivo.close();

			objDescarga.setArchivo(archivoXLS);
			objDescarga.setNombreArchivo(strNombre);
			objDescarga.setTipo(".xls");
			objDescarga.descargarArchivo(FacesUtils.getServletResponse(), 1);

			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Object[] getCamposColumnas() throws Exception {
		Object arrResultado[] = new Object[2];
		String strCampos = "tabla.";
		ArrayList<String> arlNombreCol;
		int totalCampos, contCampos = 0;

		try {
			arlNombreCol = new ArrayList<String>();
			totalCampos = getCampos1().length + getCampos2().length + getCampos3().length + getCampos4().length
					+ getCampos5().length + getCampos6().length + getCampos7().length;

			for (int i = 0; getCampos1().length > i; i++) {
				contCampos++;
				if (totalCampos == contCampos) {
					strCampos = strCampos + Utilerias.separCampos(getCampos1()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos1()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos1()[i].split("-")[1]);
			}

			for (int i = 0; getCampos2().length > i; i++) {
				contCampos++;
				if (totalCampos == contCampos) {
					strCampos = strCampos + Utilerias.separCampos(getCampos2()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos2()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos2()[i].split("-")[1]);
			}

			for (int i = 0; getCampos3().length > i; i++) {
				contCampos++;
				if (totalCampos == contCampos) {
					strCampos = strCampos + Utilerias.separCampos(getCampos3()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos3()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos3()[i].split("-")[1]);
			}

			for (int i = 0; getCampos4().length > i; i++) {
				contCampos++;
				if (totalCampos == contCampos) {
					strCampos = strCampos + Utilerias.separCampos(getCampos4()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos4()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos4()[i].split("-")[1]);
			}

			for (int i = 0; getCampos5().length > i; i++) {
				contCampos++;
				if (totalCampos == contCampos) {
					strCampos = strCampos + Utilerias.separCampos(getCampos5()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos5()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos5()[i].split("-")[1]);
			}

			for (int i = 0; getCampos6().length > i; i++) {
				contCampos++;
				if (totalCampos == contCampos) {
					strCampos = strCampos + Utilerias.separCampos(getCampos6()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos6()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos6()[i].split("-")[1]);
			}

			for (int i = 0; getCampos7().length > i; i++) {
				contCampos++;
				if (totalCampos == contCampos) {
					strCampos = strCampos + Utilerias.separCampos(getCampos7()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos7()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos7()[i].split("-")[1]);
			}

			arrResultado[0] = strCampos;
			arrResultado[1] = arlNombreCol;
		} catch (Exception e) {
			throw new Exception("BeanReporteador.separarCampos::: " + e.getMessage());
		}

		return arrResultado;
	}

	public void descargar() {
		Iterator<Object> it;
		int contador = 0;
		String strNombre = null, strRutaTemp;
		Archivo objArchivo;

		try {
			getMessage().delete(0, getMessage().length());
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			String arrColumnas[] = new String[getLstColumnas().size()];
			it = getLstColumnas().iterator();
			while (it.hasNext()) {
				arrColumnas[contador] = it.next().toString();
				contador++;
			}
			strNombre = "Reporte Colectivo - " + new SimpleDateFormat("ddMMyy").format(new Date());

			switch (getTipoarchivo()) {
			case 1:
				objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
				objArchivo.copiarArchivo(arrColumnas, getLstResultados(), getSeparador());
				objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
				objArchivo.eliminarArchivo();

				getMessage().append("Archivo descargado satisfactoriamente");
				log.error(getMessage().toString());
				FacesContext.getCurrentInstance().responseComplete();
				break;

			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			getMessage().append("Error al descargar la informacion");
			log.error(getMessage().toString(), e);
		}
	}

	public void exportaExcel() {
		List<Object> resultado = null;
		String strNombre = null, strRutaTemp;
		Workbook libro;
		FileOutputStream archivo;
		File archivoXLS;
		Sheet hoja;
		Iterator<Object> it;
		Archivo objDescarga = new Archivo();

		Object[] arrDatos = null;

		try {
			resultado = new ArrayList<Object>();
			resultado = lstResultadosReportesEspeciales;
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			if (inReporteEspecial == 1) {
				strNombre = "Cierre Cobranza - " + new SimpleDateFormat("MMyyyy").format(new Date());
			} else if (inReporteEspecial == 2) {
				strNombre = "Reporte Emisi�n Corte Contable - " + new SimpleDateFormat("MMyyyy").format(new Date());
			} else if (inReporteEspecial == 4) {
				strNombre = "Reporte Estimacion CR - " + new SimpleDateFormat("MMyyyy").format(new Date());
			} else if (inReporteEspecial == 5) {
				strNombre = "Base Final - " + new SimpleDateFormat("MMyyyy").format(new Date());
			}

			if (inReporteEspecial == 0) {
				switch (inReporteComisiones) {
				case 7:
					strNombre = "Comision Tec Detalle Banco - " + new SimpleDateFormat("MMyyyy").format(new Date());
					break;
				case 8:
					strNombre = "Comision Tec Detalle Interno - " + new SimpleDateFormat("MMyyyy").format(new Date());
					break;
				case 9: // Comision comercial detalle banco
					strNombre = "Comision Com. Detalle Banco - " + new SimpleDateFormat("MMyyyy").format(new Date());
					break;
				case 10:
					strNombre = "Comison Com Detalle Interno - " + new SimpleDateFormat("MMyyyy").format(new Date());
					
					break;
				case 11:
					strNombre = "Resumen Com Tec - " + new SimpleDateFormat("MMyyyy").format(new Date());
					break;
				case 12:
					strNombre = "Calculo Tec  desglose - " + new SimpleDateFormat("MMyyyy").format(new Date());
					break;
				}

			}

			archivoXLS = new File(strRutaTemp + strNombre);
			libro = new HSSFWorkbook();
			archivo = new FileOutputStream(archivoXLS);

			hoja = libro.createSheet("Hoja1");

			it = resultado.iterator();
			boolean primeraFila = true;
			for (int f = 0; f < resultado.size(); f++) {
				Row fila = hoja.createRow(f);
				arrDatos = (Object[]) it.next();
				for (int c = 0; c < arrDatos.length; c++) {

					Cell celda = (Cell) fila.createCell(c);
					celda.setCellValue(arrDatos[c] != null ? String.valueOf(arrDatos[c]) : "");

					if (primeraFila) {
						hoja.setColumnWidth(c, 20 * 210);
					}
				}
				if (primeraFila) {
					primeraFila = false;
				}
			}

			libro.write(archivo);
			archivo.close();

			objDescarga.setArchivo(archivoXLS);
			objDescarga.setNombreArchivo(strNombre);
			objDescarga.setTipo(".xls");
			objDescarga.descargarArchivo(FacesUtils.getServletResponse(), 1);

			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reporteFiscal() {
		ArrayList<BeanMesFiscal> lstEmitidas;
		BeanMesFiscal mesFiscal;
		lstDatos.clear();
		canceladas.clear();
		ArrayList<List<BeanMesFiscal>> lstReal;
		String strFechaIni = null, strFechaFin = null;

		try {
			lstEmitidas = new ArrayList<BeanMesFiscal>();
			lstReal = new ArrayList<List<BeanMesFiscal>>();

			if (getInFiltroFechas() == 1) { // Fechas por mes
				strFechaIni = String.format("01/%02d/%d", mes, Calendar.getInstance().get(Calendar.YEAR));
				Integer nDias = (Integer) GestorFechas
						.getDatosFecha(GestorFechas.generateDate(strFechaIni, "dd/MM/yyyy"), Constantes.DIAS_MES);
				strFechaFin = Utilerias.agregarCaracteres(nDias.toString(), 1, '0', 1) + "/"
						+ Utilerias.agregarCaracteres(mes.toString(), 1, '0', 1) + "/"
						+ Calendar.getInstance().get(Calendar.YEAR);
			} else if (getInFiltroFechas() == 2) {// Fechas por dias
				strFechaIni = GestorFechas.formatDate(getFechainicio(), "dd/MM/yyyy");
				strFechaFin = GestorFechas.formatDate(getFechafin(), "dd/MM/yyyy");
			}

			// Datos para Tiempo real
			List<Object[]> realEmitidos = servicioReporteador.reporteFiscalEmitidos(inRamo, inIdVenta, strFechaIni,
					strFechaFin);
			List<Object[]> realBajas = servicioReporteador.reporteFiscalBajas(inRamo, inIdVenta, strFechaIni,
					strFechaFin);

			// List<Object[]> realEmitidos = leerExcel("C:\\Datos Borrar\\Libro1.xls", 4);
			// List<Object[]> realBajas = leerExcel("C:\\Datos Borrar\\Libro2.xls", 5);

			for (Object[] emi : realEmitidos) {
				mesFiscal = new BeanMesFiscal();

				mesFiscal.setMes(emi[1].toString());
				mesFiscal.setPlazo(((BigDecimal) emi[2]).intValue());
				mesFiscal.setPrimaEmitidaNeta(((BigDecimal) emi[3]).doubleValue());

				lstEmitidas.add(mesFiscal);
				llenaCancelacion(realBajas, mesFiscal);
			}

			lstReal.add(lstEmitidas);

			List<BeanMesFiscal> lstCan, lstNewCan;
			boolean encontrado = false;
			Iterator<List<BeanMesFiscal>> itCan = canceladas.values().iterator();
			String mes;
			int nContador = Constantes.DEFAULT_INT;
			while (itCan.hasNext()) {
				mes = canceladas.keySet().toArray()[nContador].toString().trim();
				lstNewCan = new ArrayList<BeanMesFiscal>();
				lstCan = itCan.next();

				if (mes.equals("2000")) {
					lstReal.add(lstCan);
				} else {
					for (BeanMesFiscal emitido : lstEmitidas) {
						encontrado = false;
						for (BeanMesFiscal cancelado : lstCan) {
							if (emitido.getPlazo() == cancelado.getPlazo()) {
								lstNewCan.add(cancelado);
								encontrado = true;
								break;
							}
						}

						if (!encontrado) {
							BeanMesFiscal newPlazo = new BeanMesFiscal();
							newPlazo.setMes(mes);
							newPlazo.setPlazo(emitido.getPlazo());
							newPlazo.setPrimaBajas(Constantes.DEFAULT_DOUBLE);
							newPlazo.setPrimaTotal(emitido.getPrimaEmitidaNeta());
							lstNewCan.add(newPlazo);
						}
					}
					lstReal.add(lstNewCan);
				}
				nContador++;
			}

			lstDatos.add(lstEmitidas);
			lstDatos.addAll(canceladas.values());

			creaExcelReporteFiscal(lstReal, lstEmitidas);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void llenaCancelacion(List<Object[]> realBajas, BeanMesFiscal mesFiscal) {
		BeanMesFiscal mesBajas;
		Date anioActual;
		List<BeanMesFiscal> lstBajas;
		BeanMesFiscal objAnt = null;
		Boolean bandera = false;

		try {
			anioActual = new Date();

			for (Object[] baj : realBajas) {
				objAnt = new BeanMesFiscal();
				mesBajas = new BeanMesFiscal();
				mesBajas.setMes(baj[1].toString());
				mesBajas.setPlazo(((BigDecimal) baj[2]).intValue());
				mesBajas.setPrimaBajas(((BigDecimal) baj[3]).doubleValue());

				if (mesFiscal.getPlazo() == mesBajas.getPlazo()) {
					if (GestorFechas.formatDate(anioActual, "yyyy").equals(mesBajas.getMes().substring(0, 4))) {
						mesBajas.setPrimaTotal(mesFiscal.getPrimaEmitidaNeta() - mesBajas.getPrimaBajas());
						if (canceladas.containsKey(mesBajas.getMes())) {
							canceladas.get(mesBajas.getMes()).add(mesBajas);
						} else {
							lstBajas = new ArrayList<BeanMesFiscal>();
							lstBajas.add(mesBajas);
							canceladas.put(mesBajas.getMes(), lstBajas);
						}
					} else { // a�osAnteriores
						objAnt.setMes("Anteriores");
						objAnt.setPlazo(mesBajas.getPlazo());
						objAnt.setPrimaBajas(objAnt.getPrimaBajas() + mesBajas.getPrimaBajas());
						objAnt.setPrimaTotal(objAnt.getPrimaBajas());
						if (canceladas.containsKey("2000")) {
							canceladas.get("2000").add(objAnt);
						} else {
							lstBajas = new ArrayList<BeanMesFiscal>();
							lstBajas.add(objAnt);
							canceladas.put("2000", lstBajas);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void creaExcelReporteFiscal(ArrayList<List<BeanMesFiscal>> lstReal, ArrayList<BeanMesFiscal> lstEmitidas) {
		HSSFWorkbook libro;
		HSSFCellStyle style;
		FileOutputStream archivo;
		File archivoXLS;
		HSSFSheet hoja;
		HSSFRow fila;
		HSSFCell cell;
		String strNombre = null, strRutaTemp;
		Archivo objDescarga;

		Iterator<List<BeanMesFiscal>> itHorizontal;
		Iterator<BeanMesFiscal> itVertical;

		int nContFila = 1, nContCol = 0;
		int filaInicial = 0, filaFinal = 0;

		try {
			String[] encabezados = { "Mes", "Plazo", "Prima Neta Emitida", "Bajas", "Total" };

			objDescarga = new Archivo();
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			strNombre = "Reporte Emisi�n y Bajas Prima Unica - " + new SimpleDateFormat("MMyyyy").format(new Date());

			archivoXLS = new File(strRutaTemp + strNombre);
			libro = new HSSFWorkbook();
			archivo = new FileOutputStream(archivoXLS);
			hoja = libro.createSheet("Hoja1");

			// Estilo letra
			HSSFFont font = libro.createFont();
			font.setColor(HSSFColor.WHITE.index);
			font.setFontName("Arial");
			font.setFontHeightInPoints((short) 12);
			// Estilo de celda
			style = libro.createCellStyle();
			style.setFont(font);
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			itHorizontal = lstReal.iterator();
			int finalFilaMerge = 0;
			while (itHorizontal.hasNext()) {
				nContFila = 0;
				List<BeanMesFiscal> lstDatosVertical = itHorizontal.next();
				itVertical = lstDatosVertical.iterator();

				BeanMesFiscal objValida = lstDatosVertical.isEmpty() ? new BeanMesFiscal() : lstDatosVertical.get(0);
				fila = hoja.getRow(nContFila);

				if (fila == null) {
					fila = hoja.createRow(nContFila);
				}

				cell = fila.createCell(nContCol);
				cell.setCellValue("Emision");
				cell.setCellStyle(style);
				if (objValida.getPrimaEmitidaNeta() > 0) {
					cell = fila.createCell(nContCol);
					cell.setCellValue("Emision");
					cell.setCellStyle(style);
					cell = fila.createCell(nContCol + 1);
					cell.setCellValue("Emision");
					cell.setCellStyle(style);
					cell = fila.createCell(nContCol + 2);
					cell.setCellValue("Emision");
					cell.setCellStyle(style);
					filaInicial++;
					hoja.addMergedRegion(new Region(nContFila, (short) nContCol, nContFila, (short) (nContCol + 2)));
				} else {
					cell = fila.createCell(nContCol);
					cell.setCellValue(objValida.getMes());
					cell.setCellStyle(style);
					cell = fila.createCell(nContCol + 1);
					cell.setCellValue(objValida.getMes());
					cell.setCellStyle(style);
					hoja.addMergedRegion(new Region(nContFila, (short) nContCol, nContFila, (short) (nContCol + 1)));
				}

				nContFila = 1;
				// --pintar header
				fila = hoja.getRow(nContFila);

				if (fila == null) {
					fila = hoja.createRow(nContFila);
				}

				if (objValida.getPrimaEmitidaNeta() > 0) {
					cell = fila.createCell(nContCol);
					cell.setCellValue(encabezados[0]);
					cell.setCellStyle(style);
					cell = fila.createCell(nContCol + 1);
					cell.setCellValue(encabezados[1]);
					cell.setCellStyle(style);
					cell = fila.createCell(nContCol + 2);
					cell.setCellValue(encabezados[2]);
					cell.setCellStyle(style);
					filaInicial++;
				} else {
					cell = fila.createCell(nContCol);
					cell.setCellValue(encabezados[3]);
					cell.setCellStyle(style);
					cell = fila.createCell(nContCol + 1);
					cell.setCellValue(encabezados[4]);
					cell.setCellStyle(style);
				}

				nContFila = nContFila + 1;
				while (itVertical.hasNext()) {
					BeanMesFiscal objMes = itVertical.next();
					fila = hoja.getRow(nContFila);

					if (fila == null) {
						fila = hoja.createRow(nContFila);
					}

					if (objMes.getPrimaEmitidaNeta() > 0) {
						finalFilaMerge = lstDatosVertical.size();
						fila.createCell(nContCol).setCellValue(objMes.getMes());
						fila.createCell(nContCol + 1).setCellValue(objMes.getPlazo());
						fila.createCell(nContCol + 2).setCellValue(objMes.getPrimaEmitidaNeta());
					} else {
						fila.createCell(nContCol).setCellValue(objMes.getPrimaBajas());
						fila.createCell(nContCol + 1).setCellValue(objMes.getPrimaTotal());
					}

					filaFinal = nContFila;
					nContFila++;
				}

				nContCol = nContCol + 3;
			}

			hoja.addMergedRegion(new Region(filaInicial, (short) 0, finalFilaMerge + 1, (short) 0));

			libro.write(archivo);
			archivo.close();
			objDescarga.setArchivo(archivoXLS);
			objDescarga.setNombreArchivo(strNombre);
			objDescarga.setTipo(".xls");

			objDescarga.descargarArchivo(FacesUtils.getServletResponse(), 1);
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public <T> List<T> leerExcel(String nombre, int tam) throws Exception {
		List<Object[]> datos = null;

		try {
			datos = new ArrayList<Object[]>();
			FileInputStream file = new FileInputStream(new File(nombre));
			// Crear el objeto que tendra el libro de Excel
			HSSFWorkbook workbook = new HSSFWorkbook(file);

			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();

			Row row;

			while (rowIterator.hasNext()) {
				Object[] arr = new Object[tam];
				row = rowIterator.next();
				int count = 0;
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell celda;

				while (cellIterator.hasNext()) {
					celda = cellIterator.next();
					// Dependiendo del formato de la celda el valor se debe mostrar como String,
					// Fecha, boolean, entero...
					switch (celda.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						if (HSSFDateUtil.isCellDateFormatted(celda)) {
							arr[count] = celda.getDateCellValue();
						} else {
							arr[count] = new BigDecimal(celda.getNumericCellValue());
						}
						break;
					case Cell.CELL_TYPE_STRING:
						arr[count] = celda.getStringCellValue();
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						arr[count] = celda.getBooleanCellValue();
						break;
					}
					count++;
				}

				datos.add(arr);
			}

			// cerramos el libro excel
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (List<T>) datos;
	}

	public void reporteCuentas() {
		List<Object> datos;
		String strNombre = null, strRutaTemp;
		HSSFWorkbook libro;
		FileOutputStream archivo;
		File archivoXLS;
		Sheet hoja = null, hoja2 = null;
		Iterator<Object> it;
		Archivo objDescarga;
		final Map<String, Object> datosFormato = new HashMap<String, Object>();
		final List<String> abonosCargos = new ArrayList<String>();
		HSSFDataFormat dataFormat = null;

		try {
			objDescarga = new Archivo();
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			Calendar fechaDelMes = Calendar.getInstance();
			fechaDelMes.set(Calendar.MONTH, getMes() - 1);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM yyyy", new Locale("es"));
			strNombre = "Reporte Integracion de Cuentas - " + simpleDateFormat.format(fechaDelMes.getTime());

			archivoXLS = new File(strRutaTemp + strNombre);
			libro = new HSSFWorkbook();
			dataFormat = libro.createDataFormat();
			archivo = new FileOutputStream(archivoXLS);

			HSSFCellStyle style = libro.createCellStyle();
			HSSFFont font = libro.createFont();
			HSSFCellStyle style2 = libro.createCellStyle();
			HSSFFont font2 = libro.createFont();
			final String abonoDebe = "2133|02|01|22|01|000|400|COB|213302012101000 MXP PRIM EN DEP VIDA GRUPO|%s|";
			final String abonoHaber = "1502|01|99|21|01|102|400|COB|Bancos (%s)||%s";
			final String cargoDebe = "1502|01|99|21|01|102|400|COB|Bancos (%s)|%s|";
			final String cargoHaber = "2133|02|01|22|01|000|400|COB|213302012101000 MXP PRIM EN DEP VIDA GRUPO||%s";
			BigDecimal sumaDebe = BigDecimal.ZERO;
			BigDecimal sumaHaber = BigDecimal.ZERO;

			datos = servicioReporteador.reporteCuentas(getMes(), getInFiltroFechas(), getFechainicio(), getFechafin());
			it = datos.iterator();
			Object[] encabezados = (Object[]) it.next();
			String cuenta = "---";
			String movimiento = "";
			while (it.hasNext()) {
				Object[] fila = (Object[]) it.next();
				if (!cuenta.equals(fila[0])) {
					if (!"---".equals(cuenta)) {
						llenarFila(hoja, new Object[] { "" }, false, font, style, dataFormat);
						if ("A".equals(movimiento)) {
							llenarFila(hoja, new Object[] { "", "", "", "", "", "", sumaDebe.toString() }, false, font,
									style, dataFormat);
							abonosCargos.add(String.format(abonoDebe, sumaDebe.toString()));
							abonosCargos.add(String.format(abonoHaber,
									cuenta.length() > 4 ? cuenta.substring(cuenta.length() - 4) : "0000",
									sumaDebe.toString()));
						} else if ("C".equals(movimiento)) {
							llenarFila(hoja, new Object[] { "", "", "", "", "", "", sumaHaber.toString() }, false, font,
									style, dataFormat);
							abonosCargos.add(String.format(cargoDebe,
									cuenta.length() > 4 ? cuenta.substring(cuenta.length() - 4) : "0000",
									sumaHaber.toString()));
							abonosCargos.add(String.format(cargoHaber, sumaHaber.toString()));
						}
						llenarFila(hoja, new Object[] { "" }, false, font, style, dataFormat);
						datosFormato.put("detalle", abonosCargos);
						datosFormato.put("conceptoGeneral", String.format("REGISTRO DE COBRANZA VIDA COLECTIVO  DE %s",
								simpleDateFormat.format(fechaDelMes.getTime())).toUpperCase());
						datosFormato.put("sumaDebe", sumaDebe.add(sumaHaber).toString());
						datosFormato.put("sumaHaber", sumaDebe.add(sumaHaber).toString());
						try {
							llenarFormatoPRFCON01F02(libro, hoja2, datosFormato);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					movimiento = "";
					sumaDebe = sumaHaber = BigDecimal.ZERO;
					datosFormato.clear();
					abonosCargos.clear();
					cuenta = String.valueOf(fila[0]);
					hoja = libro.createSheet(cuenta.length() > 4 ? cuenta.substring(cuenta.length() - 4) : cuenta);
					String nombreHoja2 = cuenta.length() > 4
							? String.format("PRFCON01-F02 (%s)", cuenta.substring(cuenta.length() - 4))
							: String.format("PRFCON01-F02 (%s)", cuenta);
					hoja2 = libro.createSheet(nombreHoja2);
					llenarFila(hoja, encabezados, true, font, style, dataFormat);
				}
				if (!movimiento.equals(fila[4])) {
					if (!"".equals(movimiento)) {
						llenarFila(hoja, new Object[] { "" }, false, font, style, dataFormat);
						if ("A".equals(movimiento)) {
							llenarFila(hoja, new Object[] { "", "", "", "", "", "", sumaDebe.toString() }, false, font,
									style, dataFormat);
							abonosCargos.add(String.format(abonoDebe, sumaDebe.toString()));
							abonosCargos.add(String.format(abonoHaber,
									cuenta.length() > 4 ? cuenta.substring(cuenta.length() - 4) : "0000",
									sumaDebe.toString()));
						} else if ("C".equals(movimiento)) {
							llenarFila(hoja, new Object[] { "", "", "", "", "", "", sumaHaber.toString() }, false, font,
									style, dataFormat);
							abonosCargos.add(String.format(cargoDebe,
									cuenta.length() > 4 ? cuenta.substring(cuenta.length() - 4) : "0000",
									sumaHaber.toString()));
							abonosCargos.add(String.format(cargoHaber, sumaHaber.toString()));
						}
						llenarFila(hoja, new Object[] { "" }, false, font, style, dataFormat);
					}
					movimiento = String.valueOf(fila[4]).trim();
				}
				try {
					if ("A".equals(movimiento)) {
						sumaDebe = sumaDebe.add(new BigDecimal(String.valueOf(fila[6])));
					} else if ("C".equals(movimiento)) {
						sumaHaber = sumaHaber.add(new BigDecimal(String.valueOf(fila[6])));
					}
				} catch (NumberFormatException ex) {
				}
				llenarFila(hoja, fila, false, font2, style2, dataFormat);
			}

			llenarFila(hoja, new Object[] { "" }, false, font, style, dataFormat);
			if ("A".equals(movimiento)) {
				llenarFila(hoja, new Object[] { "", "", "", "", "", "", sumaDebe.toString() }, false, font, style,
						dataFormat);
				abonosCargos.add(String.format(abonoDebe, sumaDebe.toString()));
				abonosCargos.add(String.format(abonoHaber,
						cuenta.length() > 4 ? cuenta.substring(cuenta.length() - 4) : "0000", sumaDebe.toString()));
			} else if ("C".equals(movimiento)) {
				llenarFila(hoja, new Object[] { "", "", "", "", "", "", sumaHaber.toString() }, false, font, style,
						dataFormat);
				abonosCargos.add(String.format(cargoDebe,
						cuenta.length() > 4 ? cuenta.substring(cuenta.length() - 4) : "0000", sumaHaber.toString()));
				abonosCargos.add(String.format(cargoHaber, sumaHaber.toString()));
			}
			llenarFila(hoja, new Object[] { "" }, false, font, style, dataFormat);
			llenarFila(hoja, new Object[] { "" }, false, font, style, dataFormat);
			datosFormato.put("detalle", abonosCargos);
			datosFormato.put("conceptoGeneral", String.format("REGISTRO DE COBRANZA VIDA COLECTIVO  DE %s",
					simpleDateFormat.format(new Date()).toUpperCase()));
			datosFormato.put("sumaDebe", sumaDebe.add(sumaHaber).toString());
			datosFormato.put("sumaHaber", sumaDebe.add(sumaHaber).toString());
			try {
				llenarFormatoPRFCON01F02(libro, hoja2, datosFormato);
			} catch (Exception e) {
				e.printStackTrace();
			}

			libro.write(archivo);
			archivo.close();
			objDescarga.setArchivo(archivoXLS);
			objDescarga.setNombreArchivo(strNombre);
			objDescarga.setTipo(".xls");

			objDescarga.descargarArchivo(FacesUtils.getServletResponse(), 1);
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void llenarFila(Sheet hoja, Object[] datos, Boolean encabezado, HSSFFont font, HSSFCellStyle style,
			HSSFDataFormat dataFormat) {
		if (hoja == null) {
			return;
		}
		Row fila = hoja.createRow(encabezado ? 0 : hoja.getLastRowNum() + 1);
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 9);
		style.setFont(font);
		if (encabezado) {
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		} else {
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		}
		Integer[] tananios = new Integer[] { 210, 170, 110, 130, 170, 110, 170, 170, 110, 110, 210, 250 };
		for (int index = 0; index < datos.length; index++) {
			if (encabezado) {
				hoja.setColumnWidth(index, tananios[index] * 20);
			}
			Cell celda = null;
			celda = (Cell) fila.createCell(index);

			if (!encabezado && (index == 6 || index == 7)) {
				style.setDataFormat(dataFormat.getFormat("#,##0.00"));
				try {
					celda.setCellValue(Double.valueOf(String.valueOf(datos[index])));
				} catch (NumberFormatException ex) {
				}
			} else {
				celda.setCellValue(String.valueOf(datos[index]));
			}
			celda.setCellStyle(style);

		}
	}

	private void llenarFormatoPRFCON01F02(HSSFWorkbook libro, Sheet hoja, Map<String, Object> datosFormato)
			throws IOException {
		Integer[] tmanioColumnas = new Integer[] { 65, 34, 34, 34, 34, 61, 61, 61, 534, 118, 118 };
		for (int index = 0; index < 11; index++) {
			hoja.setColumnWidth(index, (int) (tmanioColumnas[index] * 36.575));
		}
		ponerEncabezado(libro, hoja);
		ponerLogo(libro, hoja);
		ponerDetalle(libro, hoja, datosFormato);

	}

	private void ponerEncabezado(HSSFWorkbook libro, Sheet hoja) {
		HSSFCellStyle styleFillColorWhite = libro.createCellStyle();
		styleFillColorWhite.setFillForegroundColor(HSSFColor.WHITE.index);
		styleFillColorWhite.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		HSSFCellStyle styleBorderTopColorBlack = libro.createCellStyle();
		styleBorderTopColorBlack.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderTopColorBlack.setTopBorderColor(HSSFColor.BLACK.index);
		styleBorderTopColorBlack.setFillForegroundColor(HSSFColor.WHITE.index);
		styleBorderTopColorBlack.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		HSSFCellStyle styleBorderLeftColorBlack = libro.createCellStyle();
		styleBorderLeftColorBlack.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderLeftColorBlack.setLeftBorderColor(HSSFColor.BLACK.index);

		ponerFondoBlancoFila(hoja.createRow(1), styleBorderTopColorBlack, styleBorderLeftColorBlack);
		for (int i = 2; i < 19; i++) {
			ponerFondoBlancoFila(hoja.createRow(i), styleFillColorWhite, styleBorderLeftColorBlack);
			;
		}

		HSSFCellStyle styleEnca10 = libro.createCellStyle();
		HSSFFont fontEnca10 = libro.createFont();
		fontEnca10.setFontName("Times New Roman");
		fontEnca10.setFontHeightInPoints((short) 10);
		styleEnca10.setFont(fontEnca10);
		fontEnca10.setBoldweight(Font.BOLDWEIGHT_BOLD);

		HSSFCellStyle styleEnca16Center = libro.createCellStyle();
		styleEnca16Center.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFCellStyle styleEnca16 = libro.createCellStyle();
		HSSFFont fontEnca16 = libro.createFont();
		fontEnca16.setFontName("Times New Roman");
		fontEnca16.setFontHeightInPoints((short) 16);
		fontEnca16.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleEnca16.setFont(fontEnca16);
		styleEnca16Center.setFont(fontEnca16);

		styleEnca10.setFillForegroundColor(HSSFColor.WHITE.index);
		styleEnca10.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleEnca16Center.setFillForegroundColor(HSSFColor.WHITE.index);
		styleEnca16Center.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleEnca16.setFillForegroundColor(HSSFColor.WHITE.index);
		styleEnca16.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		hoja.addMergedRegion(new CellRangeAddress(5, 5, 6, 8));
		Row fila = hoja.createRow(5);
		ponerFondoBlancoFila(fila, styleFillColorWhite, styleBorderLeftColorBlack);
		fila.setHeight((short) (27 * 15.19));
		Cell celda = fila.createCell(6);
		celda.setCellStyle(styleEnca16Center);
		celda.setCellValue("P\u00d3LIZA CONTABLE");
		celda = fila.createCell(9);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("P\u00d3LIZA NUMERO:");

		hoja.addMergedRegion(new CellRangeAddress(6, 6, 0, 1));
		fila = hoja.createRow(6);
		ponerFondoBlancoFila(fila, styleFillColorWhite, styleBorderLeftColorBlack);
		celda = fila.createCell(0);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("FECHA:");
		hoja.addMergedRegion(new CellRangeAddress(6, 6, 2, 5));
		celda = fila.createCell(2);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

		hoja.addMergedRegion(new CellRangeAddress(7, 7, 9, 10));
		fila = hoja.createRow(7);
		ponerFondoBlancoFila(fila, styleFillColorWhite, styleBorderLeftColorBlack);
		fila.setHeight((short) (27 * 15.19));
		celda = fila.createCell(9);
		celda.setCellStyle(styleEnca16);
		celda.setCellValue("(  X  ) DIARIO");

		fila = hoja.createRow(8);
		ponerFondoBlancoFila(fila, styleFillColorWhite, styleBorderLeftColorBlack);
		fila.setHeight((short) (27 * 15.19));
		hoja.addMergedRegion(new CellRangeAddress(8, 8, 0, 2));
		celda = fila.createCell(0);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("DEPARTAMENTO:");
		hoja.addMergedRegion(new CellRangeAddress(8, 8, 3, 5));
		celda = fila.createCell(5);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("COBRANZA");
		hoja.addMergedRegion(new CellRangeAddress(8, 8, 9, 10));
		celda = fila.createCell(9);
		celda.setCellStyle(styleEnca16);
		celda.setCellValue("(     ) INGRESOS");

		fila = hoja.createRow(10);
		ponerFondoBlancoFila(fila, styleFillColorWhite, styleBorderLeftColorBlack);
		celda = fila.createCell(0);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("RAMO:");
		hoja.addMergedRegion(new CellRangeAddress(10, 10, 1, 3));

		fila = hoja.createRow(12);
		ponerFondoBlancoFila(fila, styleFillColorWhite, styleBorderLeftColorBlack);
		hoja.addMergedRegion(new CellRangeAddress(12, 12, 0, 2));
		celda = fila.createCell(0);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("MONEDA:");
		HSSFCellStyle styleFillColorBlack = libro.createCellStyle();
		styleFillColorBlack.setFillForegroundColor(HSSFColor.BLACK.index);
		styleFillColorBlack.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		celda = fila.createCell(3);
		celda.setCellStyle(styleFillColorBlack);
		hoja.addMergedRegion(new CellRangeAddress(12, 12, 5, 6));
		celda = fila.createCell(5);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("NACIONAL");

		fila = hoja.createRow(14);
		ponerFondoBlancoFila(fila, styleFillColorWhite, styleBorderLeftColorBlack);
		HSSFCellStyle styleBorderColorBlack = libro.createCellStyle();
		styleBorderColorBlack.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderColorBlack.setBottomBorderColor(HSSFColor.BLACK.index);
		styleBorderColorBlack.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderColorBlack.setLeftBorderColor(HSSFColor.BLACK.index);
		styleBorderColorBlack.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderColorBlack.setRightBorderColor(HSSFColor.BLACK.index);
		styleBorderColorBlack.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderColorBlack.setTopBorderColor(HSSFColor.BLACK.index);
		celda = fila.createCell(3);
		celda.setCellStyle(styleBorderColorBlack);
		hoja.addMergedRegion(new CellRangeAddress(14, 14, 5, 6));
		celda = fila.createCell(5);
		celda.setCellStyle(styleEnca10);
		celda.setCellValue("D\u00d3LARES");

		HSSFFont fontEnca14 = libro.createFont();
		HSSFCellStyle styleEnca14 = libro.createCellStyle();
		fontEnca14.setFontName("Times New Roman");
		fontEnca14.setFontHeightInPoints((short) 14);
		styleEnca14.setFont(fontEnca14);
		fontEnca14.setBoldweight(Font.BOLDWEIGHT_BOLD);
		styleEnca14.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		styleEnca14.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		styleEnca14.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleEnca14.setBottomBorderColor(HSSFColor.BLACK.index);
		hoja.addMergedRegion(new CellRangeAddress(16, 18, 0, 10));
		fila = hoja.createRow(16);
		ponerFondoBlancoFila(fila, styleBorderTopColorBlack, styleBorderLeftColorBlack);
		celda = fila.createCell(0);
		celda.setCellStyle(styleEnca14);
		celda.setCellValue("REGISTRO CONTABLE");
	}

	private void ponerLogo(HSSFWorkbook libro, Sheet hoja) throws IOException {
		final CreationHelper helper = libro.getCreationHelper();
		final Drawing drawing = hoja.createDrawingPatriarch();
		ClientAnchor anchor = helper.createClientAnchor();
		String imagePath = FacesUtils.getServletContext().getRealPath("/images/logoReporte.png");
		final FileInputStream stream = new FileInputStream(imagePath);
		anchor.setAnchorType(ClientAnchor.MOVE_AND_RESIZE);
		final int pictureIndex = libro.addPicture(IOUtils.toByteArray(stream), HSSFWorkbook.PICTURE_TYPE_PNG);
		anchor.setCol1(0);
		anchor.setRow1(1); // same row is okay
		anchor.setRow2(1); // same row is okay
		anchor.setCol2(8);
		final Picture pict = drawing.createPicture(anchor, pictureIndex);
		pict.resize();
	}

	void ponerDetalle(HSSFWorkbook libro, Sheet hoja, Map<String, Object> datosFormato) {
		HSSFDataFormat dataFormat = libro.createDataFormat();
		HSSFPalette palette = libro.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.BLUE_GREY.index, (byte) 0, (byte) 128, (byte) 128);
		palette.setColorAtIndex(HSSFColor.GREY_40_PERCENT.index, (byte) 192, (byte) 192, (byte) 192);
		palette.setColorAtIndex(HSSFColor.RED.index, (byte) 128, (byte) 0, (byte) 0);

		HSSFCellStyle styleEncaDetalle = libro.createCellStyle();
		styleEncaDetalle.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		styleEncaDetalle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleEncaDetalle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleEncaDetalle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleEncaDetalle.setLeftBorderColor(HSSFColor.BLUE_GREY.index);
		styleEncaDetalle.setTopBorderColor(HSSFColor.BLUE_GREY.index);
		styleEncaDetalle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		HSSFCellStyle styleDetalleFila0 = libro.createCellStyle();
		styleDetalleFila0.setBorderTop(HSSFCellStyle.BORDER_THIN);
		styleDetalleFila0.setTopBorderColor(HSSFColor.BLACK.index);
		styleDetalleFila0.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleDetalleFila0.setLeftBorderColor(HSSFColor.BLUE_GREY.index);

		HSSFCellStyle styleDetalleFila = libro.createCellStyle();
		styleDetalleFila.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleDetalleFila.setLeftBorderColor(HSSFColor.BLUE_GREY.index);

		HSSFCellStyle styleDetalleFila8 = libro.createCellStyle();
		styleDetalleFila8.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleDetalleFila8.setLeftBorderColor(HSSFColor.BLUE_GREY.index);
		styleDetalleFila8.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleDetalleFila8.setRightBorderColor(HSSFColor.BLUE_GREY.index);

		HSSFFont fontEnca8 = libro.createFont();
		fontEnca8.setFontName("Times New Roman");
		fontEnca8.setFontHeightInPoints((short) 8);
		fontEnca8.setColor(HSSFColor.RED.index);
		styleEncaDetalle.setFont(fontEnca8);

		HSSFFont fontDetalle10 = libro.createFont();
		fontDetalle10.setFontName("Times New Roman");
		fontDetalle10.setFontHeightInPoints((short) 10);

		HSSFFont fontDetalle9 = libro.createFont();
		fontDetalle9.setFontName("Times New Roman");
		fontDetalle9.setFontHeightInPoints((short) 9);

		HSSFFont fontDetalleNegrita = libro.createFont();
		fontDetalleNegrita.setFontName("Times New Roman");
		fontDetalleNegrita.setFontHeightInPoints((short) 10);
		fontDetalleNegrita.setBoldweight(Font.BOLDWEIGHT_BOLD);

		HSSFFont fontDetalleRojaNeg = libro.createFont();
		fontDetalleRojaNeg.setFontName("Times New Roman");
		fontDetalleRojaNeg.setFontHeightInPoints((short) 10);
		fontDetalleRojaNeg.setBoldweight(Font.BOLDWEIGHT_BOLD);
		fontDetalleRojaNeg.setColor(HSSFColor.RED.index);

		styleDetalleFila0.setFont(fontDetalle10);
		styleDetalleFila.setFont(fontDetalle10);
		styleDetalleFila8.setDataFormat(dataFormat.getFormat("#,##0.00"));
		styleDetalleFila8.setFont(fontDetalleNegrita);

		HSSFCellStyle styleDetalleFilaNum = libro.createCellStyle();
		styleDetalleFilaNum.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleDetalleFilaNum.setLeftBorderColor(HSSFColor.BLUE_GREY.index);
		styleDetalleFilaNum.setDataFormat(dataFormat.getFormat("#,##0.00"));
		styleDetalleFilaNum.setFont(fontDetalleNegrita);

		HSSFCellStyle styleBorderLeftColorBlack = libro.createCellStyle();
		styleBorderLeftColorBlack.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderLeftColorBlack.setLeftBorderColor(HSSFColor.BLACK.index);

		HSSFCellStyle styleSumaIguales = libro.createCellStyle();
		styleSumaIguales.setBorderTop(HSSFCellStyle.BORDER_THIN);
		styleSumaIguales.setTopBorderColor(HSSFColor.BLACK.index);
		styleSumaIguales.setFont(fontDetalleNegrita);
		styleSumaIguales.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleSumaIguales.setBottomBorderColor(HSSFColor.BLUE_GREY.index);

		HSSFCellStyle styleTotales = libro.createCellStyle();
		styleTotales.setBorderTop(HSSFCellStyle.BORDER_THIN);
		styleTotales.setTopBorderColor(HSSFColor.BLACK.index);
		styleTotales.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleTotales.setLeftBorderColor(HSSFColor.BLACK.index);
		styleTotales.setFont(fontDetalleNegrita);
		styleTotales.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleTotales.setBottomBorderColor(HSSFColor.BLUE_GREY.index);
		styleTotales.setDataFormat(dataFormat.getFormat("#,##0.00"));

		HSSFCellStyle styleEncaPie = libro.createCellStyle();
		styleEncaPie.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		styleEncaPie.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleEncaPie.setBorderTop(HSSFCellStyle.BORDER_THIN);
		styleEncaPie.setTopBorderColor(HSSFColor.BLACK.index);
		styleEncaPie.setFont(fontDetalleRojaNeg);

		HSSFCellStyle stylePieFila0 = libro.createCellStyle();
		stylePieFila0.setBorderTop(HSSFCellStyle.BORDER_THIN);
		stylePieFila0.setTopBorderColor(HSSFColor.BLACK.index);
		stylePieFila0.setFont(fontDetalle9);
		stylePieFila0.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		HSSFCellStyle styleBorderTopColorBlack = libro.createCellStyle();
		styleBorderTopColorBlack.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleBorderTopColorBlack.setTopBorderColor(HSSFColor.BLACK.index);

		HSSFCellStyle styleTituloVoBo = libro.createCellStyle();
		styleTituloVoBo.setFont(fontDetalleNegrita);
		styleTituloVoBo.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		HSSFCellStyle styleTextoVoBo = libro.createCellStyle();
		styleTextoVoBo.setFont(fontDetalle10);
		styleTextoVoBo.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		hoja.addMergedRegion(new CellRangeAddress(19, 19, 0, 4));
		Row fila = hoja.createRow(19);
		ponerFondoBlancoFila(fila, styleEncaDetalle, styleBorderLeftColorBlack);
		Cell celda = fila.createCell(0);
		celda.setCellStyle(styleEncaDetalle);
		celda.setCellValue("No. DE CUENTA");
		celda = fila.createCell(5);
		celda.setCellStyle(styleEncaDetalle);
		celda.setCellValue("AUXILIAR");
		celda = fila.createCell(6);
		celda.setCellStyle(styleEncaDetalle);
		celda.setCellValue("C COSTOS");
		celda = fila.createCell(7);
		celda.setCellStyle(styleEncaDetalle);
		celda.setCellValue("CLA CON");
		celda = fila.createCell(8);
		celda.setCellStyle(styleEncaDetalle);
		celda.setCellValue("NOMBRE DE LA CUENTA");
		celda = fila.createCell(9);
		celda.setCellStyle(styleEncaDetalle);
		celda.setCellValue("DEBE");
		celda = fila.createCell(10);
		celda.setCellStyle(styleEncaDetalle);
		celda.setCellValue("HABER");

		fila = hoja.createRow(20);
		ponerFondoBlancoFila(fila, styleDetalleFila0, styleBorderLeftColorBlack);
		for (int index = 21; index < 72; index++) {
			fila = hoja.createRow(index);
			ponerFondoBlancoFila(fila, styleDetalleFila, styleBorderLeftColorBlack);
		}
		@SuppressWarnings("unchecked")
		List<String> datos = (List<String>) datosFormato.get("detalle");
		int filaNum = 22;
		String[] datosStr = null;
		String filaStr = null;
		if (datos != null) {
			for (Object datosFila : datos) {
				if (String.valueOf(datosFila).contains("|")) {
					fila = hoja.createRow(filaNum);
					filaNum += 2;
					filaStr = String.valueOf(datosFila);
					datosStr = filaStr.split("\\|");
					for (int index = 0; index < datosStr.length; index++) {
						celda = fila.createCell(index);
						if (index > 8) {
							if (index == 9) {
								celda.setCellStyle(styleDetalleFila8);
							} else {
								celda.setCellStyle(styleDetalleFilaNum);
							}
							try {
								celda.setCellValue(Double.valueOf(datosStr[index]));
							} catch (NumberFormatException e) {
								celda.setCellValue("");
							}
						} else {
							celda.setCellStyle(styleDetalleFila);
							celda.setCellValue(datosStr[index]);
						}
						celda = fila.createCell(11);
						celda.setCellStyle(styleBorderLeftColorBlack);
					}
				}
			}
		}
		fila = hoja.createRow(72);
		hoja.addMergedRegion(new CellRangeAddress(72, 72, 0, 7));
		fila = hoja.createRow(73);
		hoja.addMergedRegion(new CellRangeAddress(73, 73, 0, 10));
		celda = fila.createCell(11);
		celda.setCellStyle(styleBorderLeftColorBlack);

		fila = hoja.createRow(72);
		ponerFondoBlancoFila(fila, styleSumaIguales, styleBorderLeftColorBlack);
		celda = fila.createCell(8);
		celda.setCellStyle(styleSumaIguales);
		celda.setCellValue("SUMAS IGUALES");
		celda = fila.createCell(9);
		celda.setCellStyle(styleTotales);
		celda.setCellValue(Double.valueOf(String.valueOf(datosFormato.get("sumaDebe"))));
		celda = fila.createCell(10);
		celda.setCellStyle(styleTotales);
		celda.setCellValue(Double.valueOf(String.valueOf(datosFormato.get("sumaHaber"))));

		fila = hoja.createRow(74);
		ponerFondoBlancoFila(fila, styleEncaPie, styleBorderLeftColorBlack);
		celda = fila.createCell(0);
		celda.setCellStyle(styleEncaPie);
		celda.setCellValue("CONCEPTO GENERAL DE LA P\u00d3LIZA:");

		fila = hoja.createRow(75);
		ponerFondoBlancoFila(fila, stylePieFila0, styleBorderLeftColorBlack);
		hoja.addMergedRegion(new CellRangeAddress(75, 75, 0, 10));
		celda = fila.createCell(0);
		celda.setCellStyle(stylePieFila0);
		celda.setCellValue(String.valueOf(datosFormato.get("conceptoGeneral")));

		for (int index = 0; index < 4; index++) {
			fila = hoja.createRow(76 + index);
			hoja.addMergedRegion(new CellRangeAddress(76 + index, 76 + index, 0, 10));
			celda = fila.createCell(11);
			celda.setCellStyle(styleBorderLeftColorBlack);
		}

		fila = hoja.createRow(80);
		hoja.addMergedRegion(new CellRangeAddress(80, 81, 0, 10));
		ponerFondoBlancoFila(fila, stylePieFila0, styleBorderLeftColorBlack);
		fila = hoja.createRow(81);
		celda = fila.createCell(11);
		celda.setCellStyle(styleBorderLeftColorBlack);

		fila = hoja.createRow(82);
		hoja.addMergedRegion(new CellRangeAddress(82, 82, 0, 6));
		celda = fila.createCell(0);
		celda.setCellStyle(styleTituloVoBo);
		celda.setCellValue("ELABOR\u00d3");
		hoja.addMergedRegion(new CellRangeAddress(82, 82, 7, 8));
		celda = fila.createCell(7);
		celda.setCellStyle(styleTituloVoBo);
		celda.setCellValue("AUTORIZ\u00d3");
		hoja.addMergedRegion(new CellRangeAddress(82, 82, 9, 10));
		celda = fila.createCell(9);
		celda.setCellStyle(styleTituloVoBo);
		celda.setCellValue("Vo.Bo. CONTABLE");
		celda = fila.createCell(11);
		celda.setCellStyle(styleBorderLeftColorBlack);

		fila = hoja.createRow(83);
		hoja.addMergedRegion(new CellRangeAddress(83, 87, 0, 6));
		celda = fila.createCell(0);
		celda.setCellStyle(styleTextoVoBo);
		celda.setCellValue("GUSTAVO MARTINEZ MARTINEZ");
		hoja.addMergedRegion(new CellRangeAddress(83, 87, 7, 8));
		celda = fila.createCell(7);
		celda.setCellStyle(styleTextoVoBo);
		celda.setCellValue("DIEGO ALBA FUENTES");
		hoja.addMergedRegion(new CellRangeAddress(83, 87, 9, 10));
		celda = fila.createCell(9);
		celda.setCellStyle(styleTextoVoBo);
		celda.setCellValue("JOEL LUNA");
		celda = fila.createCell(11);
		celda.setCellStyle(styleBorderLeftColorBlack);

		for (int index = 0; index < 4; index++) {
			fila = hoja.createRow(84 + index);
			celda = fila.createCell(11);
			celda.setCellStyle(styleBorderLeftColorBlack);
		}

		fila = hoja.createRow(88);
		hoja.addMergedRegion(new CellRangeAddress(88, 88, 0, 6));
		hoja.addMergedRegion(new CellRangeAddress(88, 88, 7, 8));
		hoja.addMergedRegion(new CellRangeAddress(88, 88, 9, 10));
		celda = fila.createCell(11);
		celda.setCellStyle(styleBorderLeftColorBlack);
		fila = hoja.createRow(89);
		hoja.addMergedRegion(new CellRangeAddress(89, 89, 0, 6));
		hoja.addMergedRegion(new CellRangeAddress(89, 89, 7, 8));
		hoja.addMergedRegion(new CellRangeAddress(89, 89, 9, 10));
		celda = fila.createCell(11);
		celda.setCellStyle(styleBorderLeftColorBlack);

		fila = hoja.createRow(90);
		for (int index = 0; index < 11; index++) {
			celda = fila.createCell(index);
			celda.setCellStyle(styleBorderTopColorBlack);
		}
	}

	private void ponerFondoBlancoFila(Row row, HSSFCellStyle styleFillColorWhite,
			HSSFCellStyle styleBorderRightColorBlack) {
		Cell cell = null;
		int j = 0;
		for (j = 0; j < 11; j++) {
			cell = row.createCell(j);
			cell.setCellStyle(styleFillColorWhite);
		}
		cell = row.createCell(j);
		cell.setCellStyle(styleBorderRightColorBlack);
	}

	public void reporteCuentaSeguimiento() {
		String rutaTemporal, rutaReporte;
		FileInputStream input;

		byte[] bytes;
		Integer read = 0;

		Map<String, Object> inParams = new HashMap<String, Object>();

		try {
			StringBuffer nombreSalida = new StringBuffer();

			inParams.put("idReporte", this.inReporteEspecial);
			inParams.put("idMes", this.mes);

			nombreSalida.append("Prueba impresion" + this.inReporteEspecial + ".xls");

			rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/" + nombreSalida);
			rutaReporte = FacesUtils.getServletContext()
					.getRealPath("/WEB-INF/reportes/ReporteIntegracionCuentas.jrxml");

			this.servicioReporteador.reporteIntegracion(rutaTemporal, rutaReporte, inParams,
					Constantes.OPC_FORMATO_REPORTE.XLS);

			File file = new File(rutaTemporal);

			FacesContext context = FacesContext.getCurrentInstance();

			if (!context.getResponseComplete()) {
				input = new FileInputStream(file);
				bytes = new byte[1000];

				String contentType = "application/vnd.ms-excel";

				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

				response.setContentType(contentType);
				
				DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
		    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());
		    	
				ServletOutputStream out = response.getOutputStream();

				while ((read = input.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}

				input.close();
				out.flush();
				out.close();
				file.delete();

				context.responseComplete();
			}

			Utilerias.resetMessage(message);

		} catch (Exception e) {
			log.error("Error en el reporte de endoso", e);
		}

	}

	private void eliminarListassession() {
		HttpSession session = FacesUtils.getServletRequest().getSession();
		session.removeAttribute("listaError");
		session.removeAttribute("listaActivacion");
		session.removeAttribute("listaEmitir");
		session.removeAttribute("listaPendientes");
		session.removeAttribute("listaPendientesGP");

		setLstResultados(null);
		setLstResultadosCierreCobranza(null);
	}

	public void afterPropertiesSet() throws Exception {
		try {

		} catch (Exception e) {
			this.log.error("No se pueden cargar los productos.", e);
			throw new FacesException("No se pueden cargar los productos.", e);
		}
	}

	/**
	 * @param inRamo
	 *            the inRamo to set
	 */
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}

	/**
	 * @return the inRamo
	 */
	public Short getInRamo() {
		return inRamo;
	}

	/**
	 * @param inPoliza
	 *            the inPoliza to set
	 */
	public void setInPoliza(Long inPoliza) {
		this.inPoliza = inPoliza;
	}

	/**
	 * @return the inPoliza
	 */
	public Long getInPoliza() {
		return inPoliza;
	}

	/**
	 * @param inReporteEspecial
	 *            the inReporteEspecial to set
	 */
	public void setInReporteEspecial(Integer inReporteEspecial) {
		this.inReporteEspecial = inReporteEspecial;
	}

	/**
	 * @return the inReporteEspecial
	 */
	public Integer getInReporteEspecial() {
		return inReporteEspecial;
	}

	public Integer getInFiltroFechas() {
		return inFiltroFechas;
	}

	public void setInFiltroFechas(Integer inFiltroFechas) {
		this.inFiltroFechas = inFiltroFechas;
	}

	public Integer getInOpcionRep() {
		return inOpcionRep;
	}

	public void setInOpcionRep(Integer inOpcionRep) {
		this.inOpcionRep = inOpcionRep;
	}

	/**
	 * @param mes
	 *            the mes to set
	 */
	public void setMes(Integer mes) {
		this.mes = mes;
	}

	/*
	 * @return the mes
	 */
	public Integer getMes() {
		return mes;
	}

	/**
	 * @param inIdVenta
	 */
	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}

	public Integer getInIdVenta() {
		return inIdVenta;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Integer getTipoarchivo() {
		return tipoarchivo;
	}

	public void setTipoarchivo(Integer tipoarchivo) {
		this.tipoarchivo = tipoarchivo;
	}

	public Date getFechainicio() {
		return fechainicio;
	}

	public void setFechainicio(Date fechainicio) {
		this.fechainicio = fechainicio;
	}

	public Date getFechafin() {
		return fechafin;
	}

	public void setFechafin(Date fechafin) {
		this.fechafin = fechafin;
	}

	public HtmlSelectOneMenu getHsomEstatus() {
		return hsomEstatus;
	}

	public void setHsomEstatus(HtmlSelectOneMenu hsomEstatus) {
		this.hsomEstatus = hsomEstatus;
	}

	public HtmlSelectOneMenu getHsomTipoArchivo() {
		return hsomTipoArchivo;
	}

	public void setHsomTipoArchivo(HtmlSelectOneMenu hsomTipoArchivo) {
		this.hsomTipoArchivo = hsomTipoArchivo;
	}

	public List<Object> getListaComboTipoArchivo() {
		listaComboTipoArchivo.clear();
		/* listaComboTipoArchivo.add(new SelectItem(-1, "-- SELECCIONAR --")); */
		listaComboTipoArchivo.add(new SelectItem(1, "Formato TXT/CSV"));
		return listaComboTipoArchivo;
	}

	public void setListaComboTipoArchivo(List<Object> listaComboTipoArchivo) {
		this.listaComboTipoArchivo = listaComboTipoArchivo;
	}

	public List<Object> getLstColumnas() {
		return lstColumnas;
	}

	public void setLstColumnas(List<Object> lstColumnas) {
		this.lstColumnas = lstColumnas;
	}

	public List<Object> getLstResultados() {
		return lstResultados;
	}

	public void setLstResultados(List<Object> lstResultados) {
		this.lstResultados = lstResultados;
	}

	public List<Object> getLstResultadosCierreCobranza() {
		return lstResultadosReportesEspeciales;
	}

	public void setLstResultadosCierreCobranza(List<Object> lstResultadosCierreCobranza) {
		this.lstResultadosReportesEspeciales = lstResultadosCierreCobranza;
	}

	public List<Object> getListaComboEstatus() throws Exception {
		listaComboEstatus.clear();
		/* listaComboEstatus.add(new SelectItem(-1, "-- SELECCIONAR --")); */
		listaComboEstatus.add(new SelectItem(1, "Emitido"));
		listaComboEstatus.add(new SelectItem(2, "Cancelado"));
		listaComboEstatus.add(new SelectItem(3, "Facturado"));
		return listaComboEstatus;
	}

	public List<Object> getListaMes() throws Exception {
		listaMes.clear();
		listaMes.addAll(GestorFechas.getMeses());		
		return listaMes;
	}

	public List<Object> getListaComboIdVenta() throws Exception {
		listaComboIdVenta.clear();
		listaComboIdVenta.add(new SelectItem(9, "TRADICIONAL"));
		listaComboIdVenta.add(new SelectItem(8, "LINEA EXPRESS"));
		listaComboIdVenta.add(new SelectItem(7, "HIPOTECARIO"));
		listaComboIdVenta.add(new SelectItem(5, "VIDA CREDITO PYME"));
		listaComboIdVenta.add(new SelectItem(6, "LINEA DE CREDITO INMEDIATA (LCI)"));
		listaComboIdVenta.add(new SelectItem(10, "AUTOCREDITO"));
		return listaComboIdVenta;
	}

	public void setListaComboEstatus(List<Object> listaComboEstatus) {
		this.listaComboEstatus = listaComboEstatus;
	}

	public void setListaMes(List<Object> listaMes) {
		this.listaMes = listaMes;
	}

	public void setListaComboIdVenta(List<Object> listaComboIdVenta) {
		this.listaComboIdVenta = listaComboIdVenta;
	}

	public String getSeparador() {
		return separador;
	}

	public void setSeparador(String separador) {
		this.separador = separador;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String[] getCampos1() {
		return campos1;
	}

	public void setCampos1(String[] campos1) {
		this.campos1 = campos1;
	}

	public String[] getCampos2() {
		return campos2;
	}

	public void setCampos2(String[] campos2) {
		this.campos2 = campos2;
	}

	public String[] getCampos3() {
		return campos3;
	}

	public void setCampos3(String[] campos3) {
		this.campos3 = campos3;
	}

	public String[] getCampos4() {
		return campos4;
	}

	public void setCampos4(String[] campos4) {
		this.campos4 = campos4;
	}

	public String[] getCampos5() {
		return campos5;
	}

	public void setCampos5(String[] campos5) {
		this.campos5 = campos5;
	}

	public String[] getCampos6() {
		return campos6;
	}

	public void setCampos6(String[] campos6) {
		this.campos6 = campos6;
	}

	public String[] getCampos7() {
		return campos7;
	}

	public void setCampos7(String[] campos7) {
		this.campos7 = campos7;
	}

	public List<Object> getListaCampos1() {
		return listaCampos1;
	}

	public void setListaCampos1(List<Object> listaCampos1) {
		this.listaCampos1 = listaCampos1;
	}

	public List<Object> getListaCampos2() {
		return listaCampos2;
	}

	public void setListaCampos2(List<Object> listaCampos2) {
		this.listaCampos2 = listaCampos2;
	}

	public List<Object> getListaCampos3() {
		return listaCampos3;
	}

	public void setListaCampos3(List<Object> listaCampos3) {
		this.listaCampos3 = listaCampos3;
	}

	public List<Object> getListaCampos4() {
		return listaCampos4;
	}

	public void setListaCampos4(List<Object> listaCampos4) {
		this.listaCampos4 = listaCampos4;
	}

	public List<Object> getListaCampos5() {
		return listaCampos5;
	}

	public void setListaCampos5(List<Object> listaCampos5) {
		this.listaCampos5 = listaCampos5;
	}

	public List<Object> getListaCampos6() {
		return listaCampos6;
	}

	public void setListaCampos6(List<Object> listaCampos6) {
		this.listaCampos6 = listaCampos6;
	}

	public List<Object> getListaCampos7() {
		return listaCampos7;
	}

	public void setListaCampos7(List<Object> listaCampos7) {
		this.listaCampos7 = listaCampos7;
	}

	public StringBuilder getMessage() {
		return message;
	}

	/**
	 * @return the habilitaCombo
	 */
	public Boolean getHabilitaCombo() {
		return habilitaCombo;
	}

	/**
	 * @param habilitaCombo
	 *            the habilitaCombo to set
	 */
	public void setHabilitaCombo(Boolean habilitaCombo) {
		this.habilitaCombo = habilitaCombo;
	}

	public Boolean getProgressHabilitar() {
		return pb.isProgressHabilitar();
	}

	public void setProgressHabilitar(Boolean progressHabilitar) {
		this.pb.setProgressHabilitar(progressHabilitar);
	}

	public HtmlProgressBar getProgressBar() {
		return pb.getProgressBar();
	}

	public void setProgressBar(HtmlProgressBar progressBar) {
		this.pb.setProgressBar(progressBar);
	}

	/**
	 * @return the intBarra
	 */
	public Integer getProgressValue() {
		return pb.getProgressValue();
	}

	/**
	 * @param intBarra
	 *            the intBarra to set
	 */
	public void setProgressValue(Integer progressValue) {
		this.pb.setProgressValue(progressValue);
	}

	public Map<String, List<BeanMesFiscal>> getCanceladas() {
		return canceladas;
	}

	public void setCanceladas(Map<String, List<BeanMesFiscal>> canceladas) {
		this.canceladas = canceladas;
	}

	public ArrayList<List<BeanMesFiscal>> getLstDatos() {
		return lstDatos;
	}

	public void setLstDatos(ArrayList<List<BeanMesFiscal>> lstDatos) {
		this.lstDatos = lstDatos;
	}

	/**
	 * @return the initRamo
	 */
	public HtmlSelectOneMenu getInitRamo() {
		return initRamo;
	}

	/**
	 * @param initRamo
	 *            the initRamo to set
	 */
	public void setInitRamo(HtmlSelectOneMenu initRamo) {
		this.initRamo = initRamo;
	}

	public static void main(String[] args) {
		BeanReporteador obj = new BeanReporteador();
		obj.reporteFiscal();
	}

	public ServicioReporteador getServicioReporteador() {
		return servicioReporteador;
	}

	public void setServicioReporteador(ServicioReporteador servicioReporteador) {
		this.servicioReporteador = servicioReporteador;
	}

	public Integer getInReporteComisiones() {
		return inReporteComisiones;
	}

	public void setInReporteComisiones(Integer inReporteComisiones) {
		this.inReporteComisiones = inReporteComisiones;
	}

	public Short getInRamoRC() {
		return inRamoRC;
	}

	public void setInRamoRC(Short inRamoRC) {
		this.inRamoRC = inRamoRC;
	}

	public Long getInPolizaRC() {
		return inPolizaRC;
	}

	public void setInPolizaRC(Long inPolizaRC) {
		this.inPolizaRC = inPolizaRC;
	}

	public Integer getInIdVentaRC() {
		return inIdVentaRC;
	}

	public void setInIdVentaRC(Integer inIdVentaRC) {
		this.inIdVentaRC = inIdVentaRC;
	}

	public Integer getEstatusRC() {
		return estatusRC;
	}

	public void setEstatusRC(Integer estatusRC) {
		this.estatusRC = estatusRC;
	}

	public Integer getMesRC() {
		return mesRC;
	}

	public void setMesRC(Integer mesRC) {
		this.mesRC = mesRC;
	}

	public Date getFechainicioRC() {
		return fechainicioRC;
	}

	public void setFechainicioRC(Date fechainicioRC) {
		this.fechainicioRC = fechainicioRC;
	}

	public Date getFechafinRC() {
		return fechafinRC;
	}

	public void setFechafinRC(Date fechafinRC) {
		this.fechafinRC = fechafinRC;
	}

	public Integer getInFiltroFechasRC() {
		return inFiltroFechasRC;
	}

	public void setInFiltroFechasRC(Integer inFiltroFechasRC) {
		this.inFiltroFechasRC = inFiltroFechasRC;
	}

}
