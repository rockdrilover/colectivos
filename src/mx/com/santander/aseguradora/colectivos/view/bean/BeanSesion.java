/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import javax.annotation.Resource;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.com.santander.aseguradora.colectivos.controller.BeanCambiaCredito;
import mx.com.santander.aseguradora.colectivos.controller.BeanCancelaAtm;
import mx.com.santander.aseguradora.colectivos.controller.BeanCargaCobranza;
import mx.com.santander.aseguradora.colectivos.controller.BeanCedula;
import mx.com.santander.aseguradora.colectivos.controller.BeanConfProcesos;
import mx.com.santander.aseguradora.colectivos.controller.BeanConsDetalleCreSin;
import mx.com.santander.aseguradora.colectivos.controller.BeanErrorEmision;
import mx.com.santander.aseguradora.colectivos.controller.BeanGrupos;
import mx.com.santander.aseguradora.colectivos.controller.BeanNombreProducto;
import mx.com.santander.aseguradora.colectivos.controller.BeanParamComision;
import mx.com.santander.aseguradora.colectivos.controller.BeanParamTimbrado;
import mx.com.santander.aseguradora.colectivos.controller.BeanReporteCobranza;
import mx.com.santander.aseguradora.colectivos.controller.BeanReportesTuiio;
import mx.com.santander.aseguradora.colectivos.controller.BeanReversoCobranza;
import mx.com.santander.aseguradora.colectivos.controller.BeanTimbrado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Usuario;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCliente;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioFlujoDeEfectivo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHistorialCredito;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioMonitoreo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParamTimbrado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAlta;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCancelaAtm;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCartClienteFui;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCedulaControl;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCiudad;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCobranza;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioConfiguracionImpresion;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndoso;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioErrorEmision;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstatus;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHomologaProdPlanes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioImpresion;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametrosComision;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPrimasDeposito;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioReporteador;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioReportesCierre;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTarifas;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTimbrado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTraspasoAPTC;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioValidaEmision;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanSesion {

	@Resource
	private ServicioClienteCertif clienteCertif;
	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioRecibo servicioRecibo;
	@Resource
	private ServicioProcesos servicioProcesos;
	@Resource
	private ServicioProducto servicioProducto;
	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioEstatus servicioEstatus;
	@Resource
	private ServicioAlta servicioAlta;
	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ServicioImpresion servicioImpresion;
	@Resource
	private ServicioTarifas servicioTarifas;
	@Resource
	private ServicioEndoso servicioEndoso;
	@Resource
	private ServicioTraspasoAPTC servicioTraspaso;
	@Resource
	private ServicioAltaContratante servicioAltaCotratante;
	@Resource
	private ServicioPlan servicioPlan;
	@Resource
	private ServicioCartCoberturas servicioCartCoberturas;
	@Resource
	private ServicioColectivosCoberturas servicioColectivosCoberturas;
	@Resource
	private ServicioColectivosComponentes servicioColectivosComponentes;
	@Resource
	private ServicioHomologaProdPlanes servicioHomologa;
	@Resource
	private ServicioRamo servicioRamo;
	@Resource
	private ServicioContratante servicioContratante;
	@Resource
	private ServicioCiudad servicioCiudad;
	@Resource
	private ServicioEstado servicioEstado;
	@Resource
	private ServicioCartClienteFui servicioCartClienteFui;
	@Resource
	private ServicioReportesCierre servicioReportesCierre;
	@Resource
	private ServicioMonitoreo servicioMonitoreo;
	@Resource
	private ServicioFlujoDeEfectivo servicioFlujo;
	@Resource
	private ServicioEndosoDatos servicioEndosoDatos;	
	@Resource
	private ServicioHistorialCredito servicioHistorialCredito;
	@Resource
	private ServicioConfiguracionImpresion servicioConfiguracionImpresion;
	@Resource
	private ServicioPrimasDeposito servicioPrimasDeposito;
	@Resource
	private ServicioReporteador servicioReporteador;
	@Resource
	private ServicioCliente servicioCliente;
	@Resource
	private ServicioValidaEmision servicioValidaEmi;
	//recurso para acceder al servicio servicioErrorEmision
	@Resource
	private ServicioErrorEmision servicioErrorEmision;
	//recurso para acceder al servicio servicioCancela
	@Resource
	private ServicioCancelaAtm servicioCancela;
	//recurso para acceder al servicio servicioParamTimbrado
	@Resource
	private ServicioParamTimbrado servicioParamTimbrado;
	//recurso para acceder al servicio servicioTimbrado
	@Resource
	private ServicioTimbrado servicioTimbrado;
	//recurso para acceder al servicio servicioGeneric
	@Resource
	private ServicioGeneric servicioGeneric;
	//recurso para acceder al servicio servicioCedula
	@Resource
	private ServicioCedulaControl servicioCedula;
	//recurso para acceder al servicio servicioParamComision
	@Resource
	private ServicioParametrosComision servicioParamComision;
	//recurso para acceder al servicio servicioCobranza
	@Resource
	private ServicioCobranza servicioCobranza;
	
	
	private BeanRamo currentBeanRamo;
	private BeanEstatus currentBeanEstatus;
	private BeanProducto currentBeanProducto;
	private BeanEstado currentBeanEstado;
	private BeanPlanes currentBeanPlan;

	private String bean;
	private String userName;
	private String nombreUsuario;
	private String grupo;
	private Usuario objUsuario;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * @param clienteCertif
	 *            the clienteCertif to set
	 */
	public void setClienteCertif(ServicioClienteCertif clienteCertif) {
		this.clienteCertif = clienteCertif;
	}

	/**
	 * @return the clienteCertif
	 */
	public ServicioClienteCertif getClienteCertif() {
		return clienteCertif;
	}

	/**
	 * @param currentBeanRamo
	 *            the currentBeanRamo to set
	 */
	public void setCurrentBeanRamo(BeanRamo currentBeanRamo) {
		this.currentBeanRamo = currentBeanRamo;
	}

	/**
	 * @return the currentBeanRamo
	 */
	public BeanRamo getCurrentBeanRamo() {
		return currentBeanRamo;
	}

	/**
	 * @param currentBeanEsatdo
	 *            the currentBeanEstado to set
	 */
	public void setCurrentBeanEstado(BeanEstado currentBeanEstado) {
		this.currentBeanEstado = currentBeanEstado;
	}

	public BeanEstado getCurrentBeanEstado() {
		// TODO Auto-generated method stub
		return currentBeanEstado;
	}

	/**
	 * @param currentBeanEstatus
	 *            the currentBeanEstatus to set
	 */
	public void setCurrentBeanEstatus(BeanEstatus currentBeanEstatus) {
		this.currentBeanEstatus = currentBeanEstatus;
	}

	/**
	 * @return the currentBeanEstatus
	 */
	public BeanEstatus getCurrentBeanEstatus() {
		return currentBeanEstatus;
	}

	/**
	 * @param bean
	 *            the bean to set
	 */
	public void setBean(String bean) {
		this.bean = bean;
	}

	/**
	 * @return the bean
	 */
	public String getBean() {
		return bean;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param nombreUsuario
	 *            the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	/**
	 * @param grupo
	 *            the grupo to set
	 */
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	/**
	 * @return the grupo
	 */
	public String getGrupo() {
		return grupo;
	}

	public Usuario getObjUsuario() {
		return objUsuario;
	}

	public void setObjUsuario(Usuario objUsuario) {
		this.objUsuario = objUsuario;
	}

	/**
	 * @param currentBeanProducto
	 *            the currentBeanProducto to set
	 */
	public void setCurrentBeanProducto(BeanProducto currentBeanProducto) {
		this.currentBeanProducto = currentBeanProducto;
	}

	/**
	 * @return the currentBeanProducto
	 */
	public BeanProducto getCurrentBeanProducto() {
		return currentBeanProducto;
	}

	public BeanPlanes getCurrentBeanPlan() {
		return currentBeanPlan;
	}

	public void setCurrentBeanPlan(BeanPlanes currentBeanPlan) {
		this.currentBeanPlan = currentBeanPlan;
	}

	public BeanSesion() {
		this.log.debug("BeanSesion creado");
		FacesUtils.pathPl = FacesUtils.getServletRequest().getSession().getServletContext().getRealPath("/WEB-INF/Pl/");
	}

	public String menu() {
		if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_PRECARGA)) {
			BeanListaPreCarga beanPreCarga = new BeanListaPreCarga();
			beanPreCarga.setServicioCarga(this.servicioCarga);
			beanPreCarga.setServicioProcesos(this.servicioProcesos);
			beanPreCarga.setServicioCertificado(this.servicioCertificado);
			beanPreCarga.setServicioParametros(servicioParametros);
			beanPreCarga.setServicioValidaEmision(servicioValidaEmi);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaPreCarga", beanPreCarga);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_PRODUCTO)) {
			BeanListaProducto bean = new BeanListaProducto();
			bean.setServicioProducto(this.servicioProducto);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaProducto", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_ALTA)) {
			BeanAlta bean = new BeanAlta();
			bean.setServicioAlta(this.servicioAlta);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanAlta", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_ALTA_CONTRATANTE)) {
			BeanAltaContratante bean = new BeanAltaContratante();
			bean.setServicioAltaContratante(this.servicioAltaCotratante);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanAltaContratante", bean);
			((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).setHabilitaComboEstado(null);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_TARIFAS)) {
			BeanListaTarifas bean = new BeanListaTarifas();
			bean.setServicioTarifas(this.servicioTarifas);
			bean.setServicioProducto(this.servicioProducto);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaTarifas", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_ENDOSO)) {
			BeanListaEndoso beanListaEndoso = new BeanListaEndoso();
			beanListaEndoso.setServicioProcesos(this.servicioProcesos);
			beanListaEndoso.setServicioRecibo(this.servicioRecibo);
			beanListaEndoso.setServicioEndoso(this.servicioEndoso);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaEndoso", beanListaEndoso);

			((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).setInOperEndoso(null);
			((BeanListaEndoso) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_ENDOSO)).getListaEndosos().clear();
			((BeanListaEndoso) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_ENDOSO)).getListaRecibos().clear();
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_TRASPASO_APTC)) {
			BeanListaTraspasoAptc bean = new BeanListaTraspasoAptc();
			bean.setServicioTraspasoAptc(servicioTraspaso);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaTraspasoAptc", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_CLIENTE_CERTIF)) {
			BeanListaClienteCertif bean = new BeanListaClienteCertif();
			bean.setServicioCliente(clienteCertif);
			bean.setServicioEndosoDatos(servicioEndosoDatos);
			bean.setServicioGeneric(servicioGeneric);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaClienteCertif", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_PARAMETRIZA_TARIFA)) {
			BeanParametrizacion bean = new BeanParametrizacion();
			bean.setServicioProducto(this.servicioProducto);
			bean.setServicioParametros(this.servicioParametros);
			bean.setServicioPlan(this.servicioPlan);
			bean.setServicioCartCoberturas(this.servicioCartCoberturas);
			bean.setServicioColectivosCoberturas(this.servicioColectivosCoberturas);
			bean.setServicioColectivosComponentes(this.servicioColectivosComponentes);
			bean.setServicioHomologa(this.servicioHomologa);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanParametrizacion", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_PARAMETRIZA_OPERACION)) {
			BeanParamOperacion bean = new BeanParamOperacion();
			bean.setServicioProducto(this.servicioProducto);
			bean.setServicioParametros(this.servicioParametros);
			bean.setServicioPlan(this.servicioPlan);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanParamOperacion", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_CENTRO_COSTO)) {
			BeanCentroCosto bean = new BeanCentroCosto();
			bean.setServicioParametros(this.servicioParametros);
			bean.setServicioRamo(this.servicioRamo);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanCentroCosto", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_COMPONENTES)) {
			BeanComponentes bean = new BeanComponentes();
			bean.setServicioParametros(this.servicioParametros);
			bean.setServicioColectivosComponentes(this.servicioColectivosComponentes);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanComponentes", bean);

			BeanParametrizacion bean2 = new BeanParametrizacion();
			bean2.setServicioProducto(this.servicioProducto);
			bean2.setServicioParametros(this.servicioParametros);
			bean2.setServicioPlan(this.servicioPlan);
			bean2.setServicioCartCoberturas(this.servicioCartCoberturas);
			bean2.setServicioColectivosCoberturas(this.servicioColectivosCoberturas);
			bean2.setServicioColectivosComponentes(this.servicioColectivosComponentes);
			bean2.setServicioHomologa(this.servicioHomologa);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanParametrizacion", bean2);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_SEGMENTOS)) {
			BeanSegmentos bean = new BeanSegmentos();
			bean.setServicioParametros(this.servicioParametros);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanSegmentos", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_CONTRATANTE)) {
			BeanContratante bean = new BeanContratante();
			bean.setServicioContratante(this.servicioContratante);
			bean.setServicioCiudad(this.servicioCiudad);
			bean.setServicioEstado(this.servicioEstado);
			bean.setServicioProcesos(this.servicioProcesos);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanContratante", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_HOMOLOGACION)) {
			BeanHomologacion bean = new BeanHomologacion();
			bean.setServicioHomologa(this.servicioHomologa);
			bean.setServicioProducto(this.servicioProducto);
			bean.setServicioPlan(this.servicioPlan);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanHomologacion", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_PLAN)) {
			BeanPlan bean = new BeanPlan();
			bean.setServicioParametros(this.servicioParametros);
			bean.setServicioPlan(this.servicioPlan);
			bean.setServicioProducto(this.servicioProducto);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanPlan", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_COMPONENTES)) {
			BeanListaComponentes bean = new BeanListaComponentes();
			bean.setServicioParametros(this.servicioParametros);
			bean.setServicioColectivosComponentes(this.servicioColectivosComponentes);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaComponentes", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_COBERTURAS)) {
			BeanCatalogoCoberturas bean = new BeanCatalogoCoberturas();
			bean.setServicioCartCoberturas(servicioCartCoberturas);
			bean.setServicioColectivosCoberturas(servicioColectivosCoberturas);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanCatalogoCoberturas", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_ALTA_CONTRATANTE_R57)) {
			BeanAltaContratanteR57 bean = new BeanAltaContratanteR57();
			bean.setServicioAlta(servicioAlta);
			bean.setServicioAltaContratante(servicioAltaCotratante);
			bean.setServicioCartClienteFui(servicioCartClienteFui);
			bean.setServicioCertificado(servicioCertificado);
			bean.setServicioCiudad(servicioCiudad);
			bean.setServicioClienteCertif(clienteCertif);
			bean.setServicioColectivosCoberturas(servicioColectivosCoberturas);
			bean.setServicioEstado(servicioEstado);
			bean.setServicioParametros(servicioParametros);
			bean.setServicioPlan(servicioPlan);
			bean.setServicioProcesos(servicioProcesos);
			bean.setServicioProducto(servicioProducto);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanAltaContratanteR57", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_ASEGURADO)) {
			BeanAsegurado bean = new BeanAsegurado();
			bean.setServicioCertificado(servicioCertificado);
			bean.setServicioCiudad(servicioCiudad);
			bean.setServicioParametro(servicioParametros);
			bean.setServicioProceso(servicioProcesos);
			bean.setServicioCarga(servicioCarga);
			bean.setServicioAltaContratante(servicioAltaCotratante);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanAsegurado", bean);		
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_CERTIFICADO)) {
			BeanListaCertificado bean = new BeanListaCertificado();
			bean.setServicioCertificado(this.servicioCertificado);
			bean.setServicioProceso(this.servicioProcesos);
			bean.setServicioRecibo(this.servicioRecibo);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaCertificado", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CANCELACION_INDIVIDUAL)) {
			BeanCancelacionIndividual bean = new BeanCancelacionIndividual();
			bean.setServicioCertificado(this.servicioCertificado);
			bean.setServicioClienteCertif(this.clienteCertif);
			bean.setServicioEstatus(this.servicioEstatus);
			bean.setServicioProceso(this.servicioProcesos);
			bean.setServicioParametro(this.servicioParametros);
			bean.setServicioCarga(this.servicioCarga);
			bean.setServicioGeneric(this.servicioGeneric);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanCancelacionIndividual", bean);			
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CANCELACION_MASIVA)) {
			BeanCancelacionMasiva bean = new BeanCancelacionMasiva();
			bean.setServicioCarga(this.servicioCarga);
			bean.setServicioProcesos(this.servicioProcesos);
			bean.setServicioParametros(this.servicioParametros);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanCancelacionMasiva", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_LISTA_RECIBO)) {
			BeanListaRecibo bean = new BeanListaRecibo();
			bean.setServicioRecibo(servicioRecibo);
			bean.setServicioCertificado(servicioCertificado);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanListaRecibo", bean);		
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_REHABILITACION)) {
			BeanRehabilitacion bean = new BeanRehabilitacion();
			bean.setServicioProcesos(servicioProcesos);
			bean.setServicioCarga(servicioCarga);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanRehabilitacion", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_REPORTES_CIERRE)) {
			BeanReportesCierre bean = new BeanReportesCierre();
			bean.setReportes(servicioReportesCierre);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanReportesCierre", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_FECHA_CIERRE)) {
			BeanFechaCierre bean = new BeanFechaCierre();
			bean.setServicioParametros(servicioParametros);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanFechaCierre", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_FECHAS_AJUSTE)) {
			BeanFechasAjuste bean = new BeanFechasAjuste();
			bean.setServicioParametros(servicioParametros);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanFechasAjuste", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_IMPRESION)) {
			BeanImpresion bean = new BeanImpresion();
			bean.setServicioImpresion(this.servicioImpresion);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanImpresion", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_MONITOREO)) {
			BeanMonitoreo bean = new BeanMonitoreo();
			bean.setServicioMonitoreo(servicioMonitoreo);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanMonitoreo", bean);			
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_FLUJODEEFECTIVO)) {
			BeanFlujoDeEfectivo bean = new BeanFlujoDeEfectivo();
			bean.setServicioFlujo(servicioFlujo);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanFlujoDeEfectivo", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_ENDOSO_DATOS)) {
			BeanEndosoDatos bean = new BeanEndosoDatos();
			bean.setServicioCiudad(servicioCiudad);
			bean.setServicioEndosoDatos(servicioEndosoDatos);
			bean.setServicioAltaContratante(servicioAltaCotratante);
			bean.setServicioParametros(servicioParametros);
			 
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanEndosoDatos", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_HISTORIAL_CREDITO)) {
			BeanHistorialCredito bean = new BeanHistorialCredito();
			bean.setServicioHistorialCredito(servicioHistorialCredito);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanHistorialCredito", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CONFIGURACION_IMPRESION)) {
			BeanConfiguracionImpresion bean = new BeanConfiguracionImpresion();
			bean.setServicioImpresion(servicioConfiguracionImpresion);
			bean.setServicioPlan(servicioPlan);
			bean.setServicioProducto(servicioProducto);
			bean.setServicioParametros(servicioParametros);
			 
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanConfiguracionImpresion", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_PRIMAS_EN_DEPOSITO)) {
			BeanPrimasEnDeposito bean = new BeanPrimasEnDeposito();
			bean.setServicioPrimasDeposito(servicioPrimasDeposito);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanPrimasEnDeposito", bean);					
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_REPORTEADOR)) {
			BeanReporteador bean = new BeanReporteador();
			bean.setServicioReporteador(servicioReporteador);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanReporteador", bean);
		} else if (bean != null && bean.equalsIgnoreCase(BeanNames.BEAN_CATALOGO_ERRORES)) {
			BeanCatalogoErrores bean = new BeanCatalogoErrores();
			bean.setServicioParametros(servicioParametros);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanCatalogoErrores", bean);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CONFIGURA_PARAMETROS)) {
			BeanConfiguraParametros beanConfigura = new BeanConfiguraParametros();
			beanConfigura.setServicioParametros(servicioParametros);
			beanConfigura.setServicioEstatus(servicioEstatus);
			beanConfigura.setServicioProducto(servicioProducto);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanConfiguraParametros", beanConfigura);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_OBLIGADO)) {
			BeanObligado beanObligado = new BeanObligado();
			beanObligado.setServiceCliente(servicioCliente);
			beanObligado.setServicioCertificado(servicioCertificado);
			beanObligado.setServicioCliente(clienteCertif);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanObligado", beanObligado);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_EMISION_MASIVA)) {
			BeanEmisionMasiva beanValidaEmi = new BeanEmisionMasiva();
			beanValidaEmi.setServicioValidaEmi(this.servicioValidaEmi);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("beanEmisionMasiva", beanValidaEmi);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_ENDOSOS_PENDIENTES)) {
			BeanEndososPendientes beanEndoPen = new BeanEndososPendientes();
			beanEndoPen.setServicioEndosoDatos(servicioEndosoDatos);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_ENDOSOS_PENDIENTES, beanEndoPen);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CONF_PROCESOS)) {
			BeanConfProcesos beanConfProcesos = new BeanConfProcesos();
			beanConfProcesos.setServicioParametros(servicioParametros);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_CONF_PROCESOS, beanConfProcesos);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_GRUPOS)) {
			BeanGrupos beanGrupos = new BeanGrupos();
			beanGrupos.setServicioParametros(servicioParametros);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_GRUPOS, beanGrupos);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_ERROR_EMISION)) {
			BeanErrorEmision beanError = new BeanErrorEmision();
			beanError.setServicioError(servicioErrorEmision);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_ERROR_EMISION, beanError);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CANCELA_ATM)) {
			BeanCancelaAtm beanCancela = new BeanCancelaAtm();
			beanCancela.setServicioCancela(servicioCancela);

			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_CANCELA_ATM, beanCancela);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_PARAM_TIMBRADO)) {
			BeanParamTimbrado beanParamTimbrado = new BeanParamTimbrado();
			beanParamTimbrado.setServicioParametros(servicioParametros);
			beanParamTimbrado.setServicioParamTimbrado(servicioParamTimbrado);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_PARAM_TIMBRADO, beanParamTimbrado);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_TIMBRADO)) {
			BeanTimbrado beanTimbrado = new BeanTimbrado();
			beanTimbrado.setServicioTimbrado(servicioTimbrado);
			beanTimbrado.setServicioCertificado(servicioCertificado);
			beanTimbrado.setServicioParametros(servicioParametros);
			beanTimbrado.setServicioGeneric(servicioGeneric);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_TIMBRADO, beanTimbrado);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CEDULA)) {
			BeanCedula beanCedula = new BeanCedula();
			beanCedula.setServicioCedula(servicioCedula);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_CEDULA, beanCedula);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_PARAM_COMISION)) {
			BeanParamComision beanParamComision = new BeanParamComision();
			beanParamComision.setServicioParametros(servicioParametros);
			beanParamComision.setServicioGeneric(this.servicioGeneric);
			beanParamComision.setServicioParamComision(servicioParamComision);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_PARAM_COMISION, beanParamComision);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_REPORTES_TUIIO)) {
			BeanReportesTuiio beanReportesTuiio = new BeanReportesTuiio();
			beanReportesTuiio.setServicioGeneric(this.servicioGeneric);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_REPORTES_TUIIO, beanReportesTuiio);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CAMBIA_CREDITO)) {
			BeanCambiaCredito beanCambia = new BeanCambiaCredito();
			beanCambia.setServicioGeneric(this.servicioGeneric);
			beanCambia.setServicioEndosoDatos(servicioEndosoDatos);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_CAMBIA_CREDITO, beanCambia);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CONS_DETALLE_CRE_SIN)) {
			BeanConsDetalleCreSin beanConsulta = new BeanConsDetalleCreSin();
			beanConsulta.setServicioGeneric(this.servicioGeneric);
			beanConsulta.setServicioCliente(clienteCertif);
			beanConsulta.setServicioEndosoDatos(servicioEndosoDatos);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_CONS_DETALLE_CRE_SIN, beanConsulta);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_COBRANZA_REPORTES)) {
			BeanReporteCobranza beanRepCobranza = new BeanReporteCobranza();
			beanRepCobranza.setServicioCobranza(this.servicioCobranza);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_COBRANZA_REPORTES, beanRepCobranza);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_REVERSO_COBRANZA)) {
			BeanReversoCobranza beanRevCobranza = new BeanReversoCobranza();
			beanRevCobranza.setServicioCobranza(this.servicioCobranza);
			beanRevCobranza.setServicioGeneric(this.servicioGeneric);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_REVERSO_COBRANZA, beanRevCobranza);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_PRODUCTO_COMISION)) {
			BeanNombreProducto beanNombreProducto = new BeanNombreProducto();
			beanNombreProducto.setServicioParametros(servicioParametros);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_PRODUCTO_COMISION, beanNombreProducto);
		} else if (this.bean != null && this.bean.equalsIgnoreCase(BeanNames.BEAN_CARGA_COBRANZA)) {
			BeanCargaCobranza beanCargaCobranza = new BeanCargaCobranza();
			beanCargaCobranza.setServicioCarga(servicioCarga);
			beanCargaCobranza.setServicioCobranza(servicioCobranza);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(BeanNames.BEAN_CARGA_COBRANZA, beanCargaCobranza);
		}

		((BeanListaRamo) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_RAMO)).setInRamo(null);
		((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).setInRamo(null);
		((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).setInPoliza(null);
		((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).getListaComboPolizas().clear();
		((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).getListaComboIdVenta().clear();
		((BeanListaRamo) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_RAMO)).getListaComboPolizas().clear();
		
		return NavigationResults.HOME;
	}

	public String limpia() {

		if (this.bean != null && this.bean.equalsIgnoreCase("BeanAltaTarifaCobertura")) {
			FacesUtils.resetManagedBean("beanAltaTarifaCobertura");
			FacesUtils.resetManagedBean("beanListaRamo");
			FacesUtils.resetManagedBean("beanListaRamoContable");
			FacesUtils.resetManagedBean("beanAlta");
			FacesUtils.resetManagedBean("beanListaCertificado");
			FacesUtils.resetManagedBean("beanListaTarifas");
			FacesUtils.resetManagedBean("beanListaEndoso");
			FacesUtils.resetManagedBean("beanAltaContratante");
			FacesUtils.resetManagedBean("beanListaContratante");
			FacesUtils.resetManagedBean("beanListaEstado");
			FacesUtils.resetManagedBean("beanMonitoreo");
		} 
		
		menu();
		
		return NavigationResults.SUCCESS;
	}

	public String salir() {

		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		HttpSession sesion = request.getSession();

		if (sesion.isNew()) {

			return NavigationResults.SALIR;
		} else {
			sesion.invalidate();
			return NavigationResults.SALIR;
		}
	}

}
