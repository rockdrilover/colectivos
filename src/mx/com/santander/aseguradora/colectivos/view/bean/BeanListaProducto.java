/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ProductoId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author dflores
 * Modificacion: Sergio Plata
 * Modificacion: Ing. Issac Bautista
 * Modificacion: Leobardo Preciado 23/03/2015
 */
@Controller
@Scope("session")
public class BeanListaProducto implements InitializingBean{

	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioProducto servicioProducto;
	private int numPagina;

	private Short inputRamo;
	private List<Object> listaProductos;
	private String strRespuesta;
	private HashMap<String, Object> hmResultados;
	private BeanProducto objActual = new BeanProducto();
	private BeanProducto objNuevo = new BeanProducto();
	private int filaActual;
	private Set<Integer> keys = new HashSet<Integer>();
	
	//lpv
	private Integer poliza;
	
	public BeanListaProducto() {
		this.log.debug("BeanListaProductos creado");
		this.numPagina = 1;
	}

	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it = getListaProductos().iterator();
        while(it.hasNext()) {
        	BeanProducto objProducto = (BeanProducto) it.next();
        	if (objProducto.getAlprCdRamo().toString().equals(clave)){
                objActual = objProducto;
                break;
            }
        }
    }
	
	public String consultarProductos(){
		String filtro = "";
		List<Object> lstResultados;
		Producto producto;
		BeanProducto bean;
		
		try {
			if(this.inputRamo != null && this.inputRamo.intValue() > 0){
				hmResultados = new HashMap<String, Object>();
				listaProductos = new ArrayList<Object>();
				
				filtro = " and P.id.alprCdRamo = " + this.inputRamo.toString() + "\n";
				lstResultados = this.servicioProducto.obtenerObjetos(filtro);
				
				Iterator<Object> it = lstResultados.iterator();
				
				while(it.hasNext()) {
					producto = (Producto) it.next();
					bean = (BeanProducto) ConstruirObjeto.crearBean(BeanProducto.class, producto);
					bean.setServicioProducto(this.servicioProducto);
					
					listaProductos.add(bean);
					hmResultados.put(bean.getId().getAlprCdProducto().toString(), bean);
									
				}
				setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
				setHmResultados(null);
			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_PRODUCTO);
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los productos.", e);
			FacesUtils.addErrorMessage("No se pueden recuperar los productos.");
			
			return NavigationResults.FAILURE;
		}
	}
	
	public void modificar() {
		try {
			Producto producto = (Producto) ConstruirObjeto.crearObjeto(Producto.class, objActual);
			servicioProducto.actualizarObjeto(producto);
			hmResultados.remove(objActual.getId().getAlprCdProducto().toString());
			hmResultados.put(objActual.getId().getAlprCdProducto().toString(), objActual);
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al MODIFICAR registro.");
			e.printStackTrace();
		}
		
	}
	
	public void guardar() {
		Integer secProducto;
		try {
			secProducto = servicioProducto.siguenteProducto(this.inputRamo);
			objNuevo.setAlprCdRamo(this.inputRamo);
			objNuevo.setAlprCdProducto(secProducto);
			objNuevo.setId(new ProductoId(this.inputRamo, secProducto));
			
			Producto producto = (Producto) ConstruirObjeto.crearObjeto(Producto.class, objNuevo);
			this.servicioProducto.guardarObjeto(producto);
				
			setStrRespuesta("Se guardo con EXITO el registro.");
			
			consultarProductos();
			objNuevo = new BeanProducto();
		} catch (Exception e) {
			setStrRespuesta("Error al GUARDAR registro.");
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados().values());
        return lstValues;
    }
	
	public void afterPropertiesSet() throws Exception {
		try {
			
		} catch (Exception e) {
			this.log.error("No se pueden cargar los productos.", e);
			throw new FacesException("No se pueden cargar los productos.", e);
		}
	}

	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}
	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}
	public Short getInputRamo() {
		return inputRamo;
	}
	public void setInputRamo(Short inputRamo) {
		this.inputRamo = inputRamo;
	}
	public List<Object> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<Object> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	public int getNumPagina() {
		return numPagina;
	}
	public HashMap<String, Object> getHmResultados() {
		return hmResultados;
	}
	public void setHmResultados(HashMap<String, Object> hmResultados) {
		this.hmResultados = hmResultados;
	}
	public BeanProducto getObjActual() {
		return objActual;
	}
	public void setObjActual(BeanProducto objActual) {
		this.objActual = objActual;
	}
	public BeanProducto getObjNuevo() {
		return objNuevo;
	}
	public void setObjNuevo(BeanProducto objNuevo) {
		this.objNuevo = objNuevo;
	}
	public int getFilaActual() {
		return filaActual;
	}
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}
	public Set<Integer> getKeys() {
		return keys;
	}
	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public void setPoliza(Integer poliza) {
		this.poliza = poliza;
	}
	public Integer getPoliza() {
		return poliza;
	}
}
