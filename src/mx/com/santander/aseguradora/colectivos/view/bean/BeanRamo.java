/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanRamo {
	
	@Resource
	private ServicioRamo servicioRamo;
	
	private Short carpCdRamo;
	private String carpDeRamo;
	private String colectivo;
	private StringBuilder message;
	
	private Log log = LogFactory.getLog(this.getClass());

	

	/**
	 * @return the servicioRamo
	 */
	public ServicioRamo getServicioRamo() {
		return servicioRamo;
	}



	/**
	 * @param servicioRamo the servicioRamo to set
	 */
	public void setServicioRamo(ServicioRamo servicioRamo) {
		this.servicioRamo = servicioRamo;
	}



	/**
	 * @return the carpCdRamo
	 */
	public Short getCarpCdRamo() {
		return carpCdRamo;
	}



	/**
	 * @param carpCdRamo the carpCdRamo to set
	 */
	public void setCarpCdRamo(Short carpCdRamo) {
		this.carpCdRamo = carpCdRamo;
	}



	/**
	 * @return the carpDeRamo
	 */
	public String getCarpDeRamo() {
		return carpDeRamo;
	}



	/**
	 * @param carpDeRamo the carpDeRamo to set
	 */
	public void setCarpDeRamo(String carpDeRamo) {
		this.carpDeRamo = carpDeRamo;
	}



	/**
	 * @return the colectivo
	 */
	public String getColectivo() {
		return colectivo;
	}



	/**
	 * @param colectivo the colectivo to set
	 */
	public void setColectivo(String colectivo) {
		this.colectivo = colectivo;
	}
	/**
	 * 
	 * @throws Excepciones
	 */
	public BeanRamo(){
		// TODO Auto-generated constructor stub
		this.log.debug("BeanRamo creado");
		this.message = new StringBuilder(100);
		
		if(FacesUtils.getBeanSesion().getCurrentBeanRamo() != null){
			
			try {
				
				ConstruirObjeto.poblarBean(this, FacesUtils.getBeanSesion().getCurrentBeanRamo());
				
			} catch (Exception e) {
				// TODO: handle exception
				message.append("No se puede recuperar el ramo con id:").append(FacesUtils.getBeanSesion().getCurrentBeanRamo().getCarpCdRamo());
				this.log.error(message.toString(), e);
				//throw new Excepciones(message.toString(), e);
			}
		}
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		try {
		return this.carpCdRamo + "--" + this.carpDeRamo.toUpperCase();
		}catch (Exception e) {
			return "Error :"+e;
		}
	}
}
