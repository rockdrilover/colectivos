/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstado;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanEstado {
	
	@Resource
	private ServicioEstado servicioEstado;
	
	private Short caesCdEstado;
	private String caesDeEstado;
	private StringBuilder message;
	
	private Log log = LogFactory.getLog(this.getClass());


	
	public ServicioEstado getServicioEstado() {
		return servicioEstado;
	}

	public void setServicioEstado(ServicioEstado servicioEstado) {
		this.servicioEstado = servicioEstado;
	}

	public Short getCaesCdEstado() {
		return caesCdEstado;
	}

	public void setCaesCdEstado(Short caesCdEstado) {
		this.caesCdEstado = caesCdEstado;
	}

	public String getCaesDeEstado() {
		return caesDeEstado;
	}

	public void setCaesDeEstado(String caesDeEstado) {
		this.caesDeEstado = caesDeEstado;
	}

	/**
	 * 
	 * @throws Excepciones
	 */
	public BeanEstado(){
		// TODO Auto-generated constructor stub
		this.log.debug("BeanEstado creado");
		this.message = new StringBuilder(100);
		
		if(FacesUtils.getBeanSesion().getCurrentBeanEstado() != null){
			
			try {
				
				ConstruirObjeto.poblarBean(this, FacesUtils.getBeanSesion().getCurrentBeanEstado());
				
			} catch (Exception e) {
				// TODO: handle exception
				message.append("No se puede recuperar el estado con id:").append(FacesUtils.getBeanSesion().getCurrentBeanEstado().getCaesCdEstado());
				this.log.error(message.toString(), e);
				//throw new Excepciones(message.toString(), e);
			}
		}
	}
	
	@Override
	public String toString() {
		try {
			return this.caesCdEstado+ "--" + this.caesDeEstado.toUpperCase();
		} catch (Exception e) {
			return "Error "+e;
		}
		
	}
}
