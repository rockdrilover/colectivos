package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

/**
 * @author Tyler
 *
 */
@Controller
@Scope("session")	
public class BeanMarcacion {

	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioCarga servicioCarga;
	
	private Log log = LogFactory.getLog(this.getClass());
	private Short canal;
	private Short ramo;
	private Long  poliza;
	private String status;
	
	private List<String> opciones;
	
	private File archivo;
	private String nombreArchivo;
	private StringBuilder message;
	private String respuesta;
	private boolean marcacion;
	
	private Object listaEstatusMarcacion;
	
	public BeanMarcacion() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.setListaEstatusMarcacion(new ArrayList<Object>());
		this.opciones = new ArrayList<String>();
		this.opciones.clear();
		this.opciones.add("1");
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_MARCACION);
	}

	/**
	 * 
	 */
	public String marcacion(){
		List<Object> lista = null;
		
		this.message.append("Iniciando proceso de calificación");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			this.canal = 1;
			if(this.ramo    != null  && this.canal   != null  && this.poliza  != null  && this.archivo != null) {
				lista = this.leerArchivoMarcacion(this.archivo, ",");
				this.servicioCarga.guardarObjetos(lista);

				message.append("El proceso de carga se realizo correctamente.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				message.append("Comenzando proceso de calificación").append(this.ramo.toString()).append(" poliza:").append(this.poliza.toString()).append(".");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				this.reporteMarcacion(this.status);
				
				this.nombreArchivo = this.archivo.getName().substring(this.archivo.getName().lastIndexOf("\\") + 1);
				lista.clear();
				
				message.append("Proceso de calificación finalizó, descargue el archivo de errores para revisar las inconsistencias.\n");				
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				FacesUtils.addInfoMessage(this.message.toString());
				Utilerias.resetMessage(message);
				
				FacesContext.getCurrentInstance().responseComplete();
				
				return NavigationResults.SUCCESS;
			}
			
		} catch (Exception e) {
			message.append("El proceso de calificación falló.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		
		return respuesta;
	}

	/**
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void listener(UploadEvent event) throws Exception{
		
		UploadItem item = event.getUploadItem();
		File archivoTemporal;
		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		String nombreArchivo = null;
		String nombreRuta;
		
		if(item.isTempFile()){
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			//ystem.out.println("Uno");
		}
		else{
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			
			/**
			 * Guardar el archivo en  carpeta del proyecto
			 */
			nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
			
			archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
			FileUtil.write(archivoTemporal, item.getData());
			
			this.archivo = archivoTemporal;
		}
		
	    this.message.append("Archivo cargado correctamente, puede continuar con el proceso de calificación");
		this.log.debug(this.message.toString());
		this.respuesta = this.message.toString();
		Utilerias.resetMessage(message);
		
	}
	
	/**
	 * 
	 * @param status2 
	 * @return
	 */
	public String reporteMarcacion(String status2){	
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		this.canal = 1;	
		
		if (this.archivo != null) this.nombreArchivo = this.archivo.getName(); else this.nombreArchivo = "*";	
		
		try {
			inParams.put("CANAL",  this.canal.intValue());
			inParams.put("RAMO",   this.ramo.intValue());
			inParams.put("POLIZA", this.poliza.intValue());
			inParams.put("STATUS", status2);
			inParams.put("ARCHIVO",this.nombreArchivo);
			
			
			rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ReporteMarcacion.csv");
			rutaReporte	= FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ReporteMarcacion.jrxml");
			

			this.servicioCarga.reporteErrores(rutaTemporal, rutaReporte, inParams);
						
			File file = new File(rutaTemporal);
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			if(!context.getResponseComplete()){
			
				input = new FileInputStream(file);
				bytes = new byte[1000];
				
				String contentType = "application/octet-stream";

				HttpServletResponse response = FacesUtils.getServletResponse();
				
				response.setContentType(contentType);

				DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
		    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());

		    	ServletOutputStream out = response.getOutputStream();
				
				while((read = input.read(bytes))!= -1){
					
					out.write(bytes, 0 , read);
				}
				
				input.close();
				out.flush();
				out.close();
				file.delete();

			}
			
			// Borramos los datos de la precarga y carga.
			this.servicioCarga.borrarDatosTemporales(this.canal, this.ramo, this.poliza, 0, this.nombreArchivo,"CAL"); 
			
			this.message.append("Archivo descargado.");
			this.log.debug(message.toString());
			FacesUtils.addInfoMessage(message.toString());
			Utilerias.resetMessage(message);

			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al generar el archivo de calificación.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}			
	}
	
	
	
	/**
	 * @param event
	 * @throws Exception
	 */
	public List<Object> leerArchivoMarcacion(File file, String delimitador) {
		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCarga preCarga;
		PreCargaId id;
		List<Object> lista = null;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);
		String[] headers = null;
		Map<String, String> mh;
		int nContador = 1;
		
		try {
			this.message.append("Abriendo archivo de carga");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));

			if(csv.readHeaders()) {
				headers = csv.getHeaders();
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
				bean = new BeanPreCarga();			
				this.message.append("Leyendo datos pre-carga calificación.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
							
				nContador = 0;
				
				while(csv.readRecord()) {	
					if (!csv.get(0).trim().equals("")){
						//System.out.println("Registro: " + nContador);
						id = new PreCargaId(this.canal, this.ramo, this.poliza , csv.get(mh.get("identificador")), "CAL", "16");
						bean.setCopcCargada("16");  //para diferenciar de la emision.
						bean.setId(id);
						bean.setCopcDato11(nombreArchivo);
							
						preCarga = (PreCarga) ConstruirObjeto.crearBean(PreCarga.class, bean);
						lista.add(preCarga);
					}
					nContador++ ;
					
					if (nContador == 30000){
						this.servicioCarga.guardarObjetos(lista);
						lista.clear();
						nContador = 0;
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
			} else {
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
		} catch (IOException e) {
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			this.message.append("No se puede cargar la informacion del archivo.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		
		return lista;
	}

	
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}

	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}

	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}

	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	public Short getCanal() {
		return canal;
	}

	public void setCanal(Short canal) {
		this.canal = canal;
	}

	public Short getRamo() {
		return ramo;
	}

	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}

	public Long getPoliza() {
		return poliza;
	}

	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<String> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<String> opciones) {
		this.opciones = opciones;
	}
	
	public File getArchivo() {
		return archivo;
	}

	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	
	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public void setMarcacion(boolean marcacion) {
		this.marcacion = marcacion;
	}

	public boolean isMarcacion() {
		return marcacion;
	}

	public void setListaEstatusMarcacion(Object listaEstatusMarcacion) {
		this.listaEstatusMarcacion = listaEstatusMarcacion;
	}

	public Object getListaEstatusMarcacion() {
		return listaEstatusMarcacion;
	}

}
