/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanPreCarga {

	@Resource
	private ServicioCarga servicioCarga;
	private Log log  = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private PreCargaId id;
	private String copcCazbCdSucursal;
	private String copcCargada;
	private String copcCdProducto;
	private String copcCdPlan;
	private String copcFeCarga;
	private String copcFeStatus;
	private String copcApPaterno;
	private String copcApMaterno;
	private String copcNombre;
	private String copcFeNac;
	private String copcSexo;
	private String copcNumCliente;
	private String copcRfc;
	private String copcCalle;
	private String copcColonia;
	private String copcDelmunic;
	private String copcCp;
	private String copcTelefono;
	private String copcTipoCliente;
	private String copcFeIngreso;
	private String copcPrima;
	private String copcPrimaReal;
	private String copcBcoProducto;
	private String copcSubProducto;
	private String copcFrPago;
	private String copcNuPlazo;
	private String copcPlazoTiempo;
	private String copcRemesa;
	private String copcCuenta;
	private String copcCdCiudad;
	private String copcCdEstado;
	private String copcCdCobertura;
	private String copcCdComponente;
	private String copcNuCredito;
	private String copcAltaBaja;
	private String copcRegistro;
	private String copcDato1;
	private String copcDato2;
	private String copcDato3;
	private String copcDato4;
	private String copcDato5;
	private String copcDato6;
	private String copcDato7;
	private String copcDato8;
	private String copcDato9;
	private String copcDato10;
	private String copcDato11;
	private String copcDato12;
	private String copcDato13;
	private String copcDato14;
	private String copcDato15;
	private String copcDato16;
	private String copcDato17;
	private String copcDato18;
	private String copcDato19;
	private String copcDato20;
	private String copcDato21;
	private String copcDato22;
	private String copcDato23;
	private String copcDato24;
	private String copcDato25;
	private String copcDato26;
	private String copcDato27;
	private String copcDato28;
	private String copcDato29;
	private String copcDato30;
	private String copcDato31;
	private String copcDato32;
	private String copcDato33;
	private String copcDato34;
	private String copcDato35;
	private String copcDato36;
	private String copcDato37;
	private String copcDato38;
	private String copcDato39;
	private String copcDato40;
	private String copcDato41;
	private String copcDato42;
	private String copcDato43;
	private String copcDato44;
	private String copcDato45;
	private String copcDato46;
	private String copcDato47;
	private String copcDato48;
	private String copcDato49;
	private String copcDato50;

	/**
	 * @return the id
	 */
	public PreCargaId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(PreCargaId id) {
		this.id = id;
	}
	/**
	 * @return the copcCazbCdSucursal
	 */
	public String getCopcCazbCdSucursal() {
		return copcCazbCdSucursal;
	}

	/**
	 * @param copcCazbCdSucursal the copcCazbCdSucursal to set
	 */
	public void setCopcCazbCdSucursal(String copcCazbCdSucursal) {
		this.copcCazbCdSucursal = copcCazbCdSucursal;
	}

	/**
	 * @return the copcCargada
	 */
	public String getCopcCargada() {
		return copcCargada;
	}

	/**
	 * @param copcCargada the copcCargada to set
	 */
	public void setCopcCargada(String copcCargada) {
		this.copcCargada = copcCargada;
	}

	/**
	 * @return the copcCdProducto
	 */
	public String getCopcCdProducto() {
		return copcCdProducto;
	}

	/**
	 * @param copcCdProducto the copcCdProducto to set
	 */
	public void setCopcCdProducto(String copcCdProducto) {
		this.copcCdProducto = copcCdProducto;
	}

	/**
	 * @return the copcCdPlan
	 */
	public String getCopcCdPlan() {
		return copcCdPlan;
	}

	/**
	 * @param copcCdPlan the copcCdPlan to set
	 */
	public void setCopcCdPlan(String copcCdPlan) {
		this.copcCdPlan = copcCdPlan;
	}

	/**
	 * @return the copcFeCarga
	 */
	public String getCopcFeCarga() {
		return copcFeCarga;
	}

	/**
	 * @param copcFeCarga the copcFeCarga to set
	 */
	public void setCopcFeCarga(String copcFeCarga) {
		this.copcFeCarga = copcFeCarga;
	}

	/**
	 * @return the copcFeStatus
	 */
	public String getCopcFeStatus() {
		return copcFeStatus;
	}

	/**
	 * @param copcFeStatus the copcFeStatus to set
	 */
	public void setCopcFeStatus(String copcFeStatus) {
		this.copcFeStatus = copcFeStatus;
	}

	/**
	 * @return the copcApPaterno
	 */
	public String getCopcApPaterno() {
		return copcApPaterno;
	}

	/**
	 * @param copcApPaterno the copcApPaterno to set
	 */
	public void setCopcApPaterno(String copcApPaterno) {
		this.copcApPaterno = copcApPaterno;
	}

	/**
	 * @return the copcApMaterno
	 */
	public String getCopcApMaterno() {
		return copcApMaterno;
	}

	/**
	 * @param copcApMaterno the copcApMaterno to set
	 */
	public void setCopcApMaterno(String copcApMaterno) {
		this.copcApMaterno = copcApMaterno;
	}

	/**
	 * @return the copcNombre
	 */
	public String getCopcNombre() {
		return copcNombre;
	}

	/**
	 * @param copcNombre the copcNombre to set
	 */
	public void setCopcNombre(String copcNombre) {
		this.copcNombre = copcNombre;
	}

	/**
	 * @return the copcFeNac
	 */
	public String getCopcFeNac() {
		return copcFeNac;
	}

	/**
	 * @param copcFeNac the copcFeNac to set
	 */
	public void setCopcFeNac(String copcFeNac) {
		this.copcFeNac = copcFeNac;
	}

	/**
	 * @return the copcSexo
	 */
	public String getCopcSexo() {
		return copcSexo;
	}

	/**
	 * @param copcSexo the copcSexo to set
	 */
	public void setCopcSexo(String copcSexo) {
		this.copcSexo = copcSexo;
	}

	/**
	 * @return the copcNumCliente
	 */
	public String getCopcNumCliente() {
		return copcNumCliente;
	}

	/**
	 * @param copcNumCliente the copcNumCliente to set
	 */
	public void setCopcNumCliente(String copcNumCliente) {
		this.copcNumCliente = copcNumCliente;
	}

	/**
	 * @return the copcRfc
	 */
	public String getCopcRfc() {
		return copcRfc;
	}

	/**
	 * @param copcRfc the copcRfc to set
	 */
	public void setCopcRfc(String copcRfc) {
		this.copcRfc = copcRfc;
	}

	/**
	 * @return the copcCalle
	 */
	public String getCopcCalle() {
		return copcCalle;
	}

	/**
	 * @param copcCalle the copcCalle to set
	 */
	public void setCopcCalle(String copcCalle) {
		this.copcCalle = copcCalle;
	}

	/**
	 * @return the copcColonia
	 */
	public String getCopcColonia() {
		return copcColonia;
	}

	/**
	 * @param copcColonia the copcColonia to set
	 */
	public void setCopcColonia(String copcColonia) {
		this.copcColonia = copcColonia;
	}

	/**
	 * @return the copcDelmunic
	 */
	public String getCopcDelmunic() {
		return copcDelmunic;
	}

	/**
	 * @param copcDelmunic the copcDelmunic to set
	 */
	public void setCopcDelmunic(String copcDelmunic) {
		this.copcDelmunic = copcDelmunic;
	}

	/**
	 * @return the copcCp
	 */
	public String getCopcCp() {
		return copcCp;
	}

	/**
	 * @param copcCp the copcCp to set
	 */
	public void setCopcCp(String copcCp) {
		this.copcCp = copcCp;
	}

	/**
	 * @return the copcTelefono
	 */
	public String getCopcTelefono() {
		return copcTelefono;
	}

	/**
	 * @param copcTelefono the copcTelefono to set
	 */
	public void setCopcTelefono(String copcTelefono) {
		this.copcTelefono = copcTelefono;
	}

	/**
	 * @return the copcTipoCliente
	 */
	public String getCopcTipoCliente() {
		return copcTipoCliente;
	}

	/**
	 * @param copcTipoCliente the copcTipoCliente to set
	 */
	public void setCopcTipoCliente(String copcTipoCliente) {
		this.copcTipoCliente = copcTipoCliente;
	}

	/**
	 * @return the copcFeIngreso
	 */
	public String getCopcFeIngreso() {
		return copcFeIngreso;
	}

	/**
	 * @param copcFeIngreso the copcFeIngreso to set
	 */
	public void setCopcFeIngreso(String copcFeIngreso) {
		this.copcFeIngreso = copcFeIngreso;
	}

	/**
	 * @return the copcPrima
	 */
	public String getCopcPrima() {
		return copcPrima;
	}

	/**
	 * @param copcPrima the copcPrima to set
	 */
	public void setCopcPrima(String copcPrima) {
		this.copcPrima = copcPrima;
	}

	/**
	 * @return the copcPrimaReal
	 */
	public String getCopcPrimaReal() {
		return copcPrimaReal;
	}

	/**
	 * @param copcPrimaReal the copcPrimaReal to set
	 */
	public void setCopcPrimaReal(String copcPrimaReal) {
		this.copcPrimaReal = copcPrimaReal;
	}

	/**
	 * @return the copcBcoProducto
	 */
	public String getCopcBcoProducto() {
		return copcBcoProducto;
	}

	/**
	 * @param copcBcoProducto the copcBcoProducto to set
	 */
	public void setCopcBcoProducto(String copcBcoProducto) {
		this.copcBcoProducto = copcBcoProducto;
	}

	/**
	 * @return the copcSubProducto
	 */
	public String getCopcSubProducto() {
		return copcSubProducto;
	}

	/**
	 * @param copcSubProducto the copcSubProducto to set
	 */
	public void setCopcSubProducto(String copcSubProducto) {
		this.copcSubProducto = copcSubProducto;
	}

	/**
	 * @return the copcFrPago
	 */
	public String getCopcFrPago() {
		return copcFrPago;
	}

	/**
	 * @param copcFrPago the copcFrPago to set
	 */
	public void setCopcFrPago(String copcFrPago) {
		this.copcFrPago = copcFrPago;
	}

	/**
	 * @return the copcNuPlazo
	 */
	public String getCopcNuPlazo() {
		return copcNuPlazo;
	}

	/**
	 * @param copcNuPlazo the copcNuPlazo to set
	 */
	public void setCopcNuPlazo(String copcNuPlazo) {
		this.copcNuPlazo = copcNuPlazo;
	}

	/**
	 * @return the copcPlazoTiempo
	 */
	public String getCopcPlazoTiempo() {
		return copcPlazoTiempo;
	}

	/**
	 * @param copcPlazoTiempo the copcPlazoTiempo to set
	 */
	public void setCopcPlazoTiempo(String copcPlazoTiempo) {
		this.copcPlazoTiempo = copcPlazoTiempo;
	}

	/**
	 * @return the copcRemesa
	 */
	public String getCopcRemesa() {
		return copcRemesa;
	}

	/**
	 * @param copcRemesa the copcRemesa to set
	 */
	public void setCopcRemesa(String copcRemesa) {
		this.copcRemesa = copcRemesa;
	}

	/**
	 * @return the copcCuenta
	 */
	public String getCopcCuenta() {
		return copcCuenta;
	}

	/**
	 * @param copcCuenta the copcCuenta to set
	 */
	public void setCopcCuenta(String copcCuenta) {
		this.copcCuenta = copcCuenta;
	}

	/**
	 * @return the copcCdCiudad
	 */
	public String getCopcCdCiudad() {
		return copcCdCiudad;
	}

	/**
	 * @param copcCdCiudad the copcCdCiudad to set
	 */
	public void setCopcCdCiudad(String copcCdCiudad) {
		this.copcCdCiudad = copcCdCiudad;
	}

	/**
	 * @return the copcCdEstado
	 */
	public String getCopcCdEstado() {
		return copcCdEstado;
	}

	/**
	 * @param copcCdEstado the copcCdEstado to set
	 */
	public void setCopcCdEstado(String copcCdEstado) {
		this.copcCdEstado = copcCdEstado;
	}

	/**
	 * @return the copcCdCobertura
	 */
	public String getCopcCdCobertura() {
		return copcCdCobertura;
	}

	/**
	 * @param copcCdCobertura the copcCdCobertura to set
	 */
	public void setCopcCdCobertura(String copcCdCobertura) {
		this.copcCdCobertura = copcCdCobertura;
	}

	/**
	 * @return the copcCdComponente
	 */
	public String getCopcCdComponente() {
		return copcCdComponente;
	}

	/**
	 * @param copcCdComponente the copcCdComponente to set
	 */
	public void setCopcCdComponente(String copcCdComponente) {
		this.copcCdComponente = copcCdComponente;
	}

	/**
	 * @return the copcNuCredito
	 */
	public String getCopcNuCredito() {
		return copcNuCredito;
	}

	/**
	 * @param copcNuCredito the copcNuCredito to set
	 */
	public void setCopcNuCredito(String copcNuCredito) {
		this.copcNuCredito = copcNuCredito;
	}

	/**
	 * @return the copcAltaBaja
	 */
	public String getCopcAltaBaja() {
		return copcAltaBaja;
	}

	/**
	 * @param copcAltaBaja the copcAltaBaja to set
	 */
	public void setCopcAltaBaja(String copcAltaBaja) {
		this.copcAltaBaja = copcAltaBaja;
	}

	/**
	 * @return the copcRegistro
	 */
	public String getCopcRegistro() {
		return copcRegistro;
	}

	/**
	 * @param copcRegistro the copcRegistro to set
	 */
	public void setCopcRegistro(String copcRegistro) {
		this.copcRegistro = copcRegistro;
	}

	/**
	 * @return the copcDato1
	 */
	public String getCopcDato1() {
		return copcDato1;
	}

	/**
	 * @param copcDato1 the copcDato1 to set
	 */
	public void setCopcDato1(String copcDato1) {
		this.copcDato1 = copcDato1;
	}

	/**
	 * @return the copcDato2
	 */
	public String getCopcDato2() {
		return copcDato2;
	}

	/**
	 * @param copcDato2 the copcDato2 to set
	 */
	public void setCopcDato2(String copcDato2) {
		this.copcDato2 = copcDato2;
	}

	/**
	 * @return the copcDato3
	 */
	public String getCopcDato3() {
		return copcDato3;
	}

	/**
	 * @param copcDato3 the copcDato3 to set
	 */
	public void setCopcDato3(String copcDato3) {
		this.copcDato3 = copcDato3;
	}

	/**
	 * @return the copcDato4
	 */
	public String getCopcDato4() {
		return copcDato4;
	}

	/**
	 * @param copcDato4 the copcDato4 to set
	 */
	public void setCopcDato4(String copcDato4) {
		this.copcDato4 = copcDato4;
	}

	/**
	 * @return the copcDato5
	 */
	public String getCopcDato5() {
		return copcDato5;
	}

	/**
	 * @param copcDato5 the copcDato5 to set
	 */
	public void setCopcDato5(String copcDato5) {
		this.copcDato5 = copcDato5;
	}

	/**
	 * @return the copcDato6
	 */
	public String getCopcDato6() {
		return copcDato6;
	}

	/**
	 * @param copcDato6 the copcDato6 to set
	 */
	public void setCopcDato6(String copcDato6) {
		this.copcDato6 = copcDato6;
	}

	/**
	 * @return the copcDato7
	 */
	public String getCopcDato7() {
		return copcDato7;
	}

	/**
	 * @param copcDato7 the copcDato7 to set
	 */
	public void setCopcDato7(String copcDato7) {
		this.copcDato7 = copcDato7;
	}

	/**
	 * @return the copcDato8
	 */
	public String getCopcDato8() {
		return copcDato8;
	}

	/**
	 * @param copcDato8 the copcDato8 to set
	 */
	public void setCopcDato8(String copcDato8) {
		this.copcDato8 = copcDato8;
	}

	/**
	 * @return the copcDato9
	 */
	public String getCopcDato9() {
		return copcDato9;
	}

	/**
	 * @param copcDato9 the copcDato9 to set
	 */
	public void setCopcDato9(String copcDato9) {
		this.copcDato9 = copcDato9;
	}

	/**
	 * @return the copcDato10
	 */
	public String getCopcDato10() {
		return copcDato10;
	}

	/**
	 * @param copcDato10 the copcDato10 to set
	 */
	public void setCopcDato10(String copcDato10) {
		this.copcDato10 = copcDato10;
	}

	/**
	 * @return the copcDato11
	 */
	public String getCopcDato11() {
		return copcDato11;
	}

	/**
	 * @param copcDato11 the copcDato11 to set
	 */
	public void setCopcDato11(String copcDato11) {
		this.copcDato11 = copcDato11;
	}

	/**
	 * @return the copcDato12
	 */
	public String getCopcDato12() {
		return copcDato12;
	}

	/**
	 * @param copcDato12 the copcDato12 to set
	 */
	public void setCopcDato12(String copcDato12) {
		this.copcDato12 = copcDato12;
	}

	/**
	 * @return the copcDato13
	 */
	public String getCopcDato13() {
		return copcDato13;
	}

	/**
	 * @param copcDato13 the copcDato13 to set
	 */
	public void setCopcDato13(String copcDato13) {
		this.copcDato13 = copcDato13;
	}

	/**
	 * @return the copcDato14
	 */
	public String getCopcDato14() {
		return copcDato14;
	}

	/**
	 * @param copcDato14 the copcDato14 to set
	 */
	public void setCopcDato14(String copcDato14) {
		this.copcDato14 = copcDato14;
	}

	/**
	 * @return the copcDato15
	 */
	public String getCopcDato15() {
		return copcDato15;
	}

	/**
	 * @param copcDato15 the copcDato15 to set
	 */
	public void setCopcDato15(String copcDato15) {
		this.copcDato15 = copcDato15;
	}

	/**
	 * @return the copcDato16
	 */
	public String getCopcDato16() {
		return copcDato16;
	}

	/**
	 * @param copcDato16 the copcDato16 to set
	 */
	public void setCopcDato16(String copcDato16) {
		this.copcDato16 = copcDato16;
	}

	/**
	 * @return the copcDato17
	 */
	public String getCopcDato17() {
		return copcDato17;
	}

	/**
	 * @param copcDato17 the copcDato17 to set
	 */
	public void setCopcDato17(String copcDato17) {
		this.copcDato17 = copcDato17;
	}

	/**
	 * @return the copcDato18
	 */
	public String getCopcDato18() {
		return copcDato18;
	}

	/**
	 * @param copcDato18 the copcDato18 to set
	 */
	public void setCopcDato18(String copcDato18) {
		this.copcDato18 = copcDato18;
	}

	/**
	 * @return the copcDato19
	 */
	public String getCopcDato19() {
		return copcDato19;
	}

	/**
	 * @param copcDato19 the copcDato19 to set
	 */
	public void setCopcDato19(String copcDato19) {
		this.copcDato19 = copcDato19;
	}

	/**
	 * @return the copcDato20
	 */
	public String getCopcDato20() {
		return copcDato20;
	}

	/**
	 * @param copcDato20 the copcDato20 to set
	 */
	public void setCopcDato20(String copcDato20) {
		this.copcDato20 = copcDato20;
	}

	/**
	 * @return the copcDato21
	 */
	public String getCopcDato21() {
		return copcDato21;
	}

	/**
	 * @param copcDato21 the copcDato21 to set
	 */
	public void setCopcDato21(String copcDato21) {
		this.copcDato21 = copcDato21;
	}

	/**
	 * @return the copcDato22
	 */
	public String getCopcDato22() {
		return copcDato22;
	}

	/**
	 * @param copcDato22 the copcDato22 to set
	 */
	public void setCopcDato22(String copcDato22) {
		this.copcDato22 = copcDato22;
	}

	/**
	 * @return the copcDato23
	 */
	public String getCopcDato23() {
		return copcDato23;
	}

	/**
	 * @param copcDato23 the copcDato23 to set
	 */
	public void setCopcDato23(String copcDato23) {
		this.copcDato23 = copcDato23;
	}

	/**
	 * @return the copcDato24
	 */
	public String getCopcDato24() {
		return copcDato24;
	}

	/**
	 * @param copcDato24 the copcDato24 to set
	 */
	public void setCopcDato24(String copcDato24) {
		this.copcDato24 = copcDato24;
	}

	/**
	 * @return the copcDato25
	 */
	public String getCopcDato25() {
		return copcDato25;
	}

	/**
	 * @param copcDato25 the copcDato25 to set
	 */
	public void setCopcDato25(String copcDato25) {
		this.copcDato25 = copcDato25;
	}

	/**
	 * @return the copcDato26
	 */
	public String getCopcDato26() {
		return copcDato26;
	}

	/**
	 * @param copcDato26 the copcDato26 to set
	 */
	public void setCopcDato26(String copcDato26) {
		this.copcDato26 = copcDato26;
	}

	/**
	 * @return the copcDato27
	 */
	public String getCopcDato27() {
		return copcDato27;
	}

	/**
	 * @param copcDato27 the copcDato27 to set
	 */
	public void setCopcDato27(String copcDato27) {
		this.copcDato27 = copcDato27;
	}

	/**
	 * @return the copcDato28
	 */
	public String getCopcDato28() {
		return copcDato28;
	}

	/**
	 * @param copcDato28 the copcDato28 to set
	 */
	public void setCopcDato28(String copcDato28) {
		this.copcDato28 = copcDato28;
	}

	/**
	 * @return the copcDato29
	 */
	public String getCopcDato29() {
		return copcDato29;
	}

	/**
	 * @param copcDato29 the copcDato29 to set
	 */
	public void setCopcDato29(String copcDato29) {
		this.copcDato29 = copcDato29;
	}

	/**
	 * @return the copcDato30
	 */
	public String getCopcDato30() {
		return copcDato30;
	}

	/**
	 * @param copcDato30 the copcDato30 to set
	 */
	public void setCopcDato30(String copcDato30) {
		this.copcDato30 = copcDato30;
	}

	/**
	 * @return the copcDato31
	 */
	public String getCopcDato31() {
		return copcDato31;
	}

	/**
	 * @param copcDato31 the copcDato31 to set
	 */
	public void setCopcDato31(String copcDato31) {
		this.copcDato31 = copcDato31;
	}

	/**
	 * @return the copcDato32
	 */
	public String getCopcDato32() {
		return copcDato32;
	}

	/**
	 * @param copcDato32 the copcDato32 to set
	 */
	public void setCopcDato32(String copcDato32) {
		this.copcDato32 = copcDato32;
	}

	/**
	 * @return the copcDato33
	 */
	public String getCopcDato33() {
		return copcDato33;
	}

	/**
	 * @param copcDato33 the copcDato33 to set
	 */
	public void setCopcDato33(String copcDato33) {
		this.copcDato33 = copcDato33;
	}

	/**
	 * @return the copcDato34
	 */
	public String getCopcDato34() {
		return copcDato34;
	}

	/**
	 * @param copcDato34 the copcDato34 to set
	 */
	public void setCopcDato34(String copcDato34) {
		this.copcDato34 = copcDato34;
	}

	/**
	 * @return the copcDato35
	 */
	public String getCopcDato35() {
		return copcDato35;
	}

	/**
	 * @param edad the copcDato35 to set
	 */
	public void setCopcDato35(String edad) {
		this.copcDato35 = edad;
	}
	
	/**
	 * @return the copcDato36
	 */
	public String getCopcDato36() {
		return copcDato36;
	}

	/**
	 * @param cancer the copcDato36 to set
	 */
	public void setCopcDato36(String cancer) {
		this.copcDato36 = cancer;
	}

	/**
	 * @return the copcDato37
	 */
	public String getCopcDato37() {
		return copcDato37;
	}

	/**
	 * @param diabetes the copcDato37 to set
	 */
	public void setCopcDato37(String diabetes) {
		this.copcDato37 = diabetes;
	}

	/**
	 * @return the copcDato38
	 */
	public String getCopcDato38() {
		return copcDato38;
	}

	/**
	 * @param cardio the copcDato38 to set
	 */
	public void setCopcDato38(String cardio) {
		this.copcDato38 = cardio;
	}

	/**
	 * @return the copcDato39
	 */
	public String getCopcDato39() {
		return copcDato39;
	}

	/**
	 * @param vih the copcDato39 to set
	 */
	public void setCopcDato39(String vih) {
		this.copcDato39 = vih;
	}

	/**
	 * @return the copcDato40
	 */
	public String getCopcDato40() {
		return copcDato40;
	}

	/**
	 * @param otros the copcDato40 to set
	 */
	public void setCopcDato40(String otros) {
		this.copcDato40 = otros;
	}

	/**
	 * @return the copcDato41
	 */
	public String getCopcDato41() {
		return copcDato41;
	}

	/**
	 * @param copcDato41 the copcDato41 to set
	 */
	public void setCopcDato41(String copcDato41) {
		this.copcDato41 = copcDato41;
	}
	
	/**
	 * @return the copcDato35
	 */
	public String getCopcDato49() {
		return copcDato49;
	}

	/**
	 * @param copcDato35 the copcDato35 to set
	 */
	public void setCopcDato49(String copcDato49) {
		this.copcDato49 = copcDato49;
	}
		
	/**
	 * @return the copcDato42
	 */
	public String getCopcDato42() {
		return copcDato42;
	}

	/**
	 * @param copcDato42 the copcDato42 to set
	 */
	public void setCopcDato42(String copcDato42) {
		this.copcDato42 = copcDato42;
	}

	/**
	 * @return the copcDato43
	 */
	public String getCopcDato43() {
		return copcDato43;
	}

	/**
	 * @param copcDato43 the copcDato43 to set
	 */
	public void setCopcDato43(String copcDato43) {
		this.copcDato43 = copcDato43;
	}

	/**
	 * @return the copcDato44
	 */
	public String getCopcDato44() {
		return copcDato44;
	}

	/**
	 * @param copcDato44 the copcDato44 to set
	 */
	public void setCopcDato44(String copcDato44) {
		this.copcDato44 = copcDato44;
	}

	/**
	 * @return the copcDato45
	 */
	public String getCopcDato45() {
		return copcDato45;
	}

	/**
	 * @param copcDato45 the copcDato45 to set
	 */
	public void setCopcDato45(String copcDato45) {
		this.copcDato45 = copcDato45;
	}

	/**
	 * @return the copcDato46
	 */
	public String getCopcDato46() {
		return copcDato46;
	}

	/**
	 * @param copcDato46 the copcDato46 to set
	 */
	public void setCopcDato46(String copcDato46) {
		this.copcDato46 = copcDato46;
	}

	/**
	 * @return the copcDato47
	 */
	public String getCopcDato47() {
		return copcDato47;
	}

	/**
	 * @param copcDato47 the copcDato47 to set
	 */
	public void setCopcDato47(String copcDato47) {
		this.copcDato47 = copcDato47;
	}

	/**
	 * @return the copcDato48
	 */
	public String getCopcDato48() {
		return copcDato48;
	}

	/**
	 * @param copcDato48 the copcDato48 to set
	 */
	public void setCopcDato48(String copcDato48) {
		this.copcDato48 = copcDato48;
	}
	
	/**
	 * @return the copcDato50
	 */
	public String getCopcDato50() {
		return copcDato50;
	}

	/**
	 * @param copcDato50 the copcDato50 to set
	 */
	public void setCopcDato50(String copcDato50) {
		this.copcDato50 = copcDato50;
	}

	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}
	
	public BeanPreCarga() {
		// TODO Auto-generated constructor stub
		
		this.log.debug("Inicializando beanPreCarga.");
		this.message = new StringBuilder();
		this.copcCargada = "0";
		this.copcFeCarga = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		
		
		try {
			
			if(this.id != null && this.id.getCopcNumPoliza() != null && this.id.getCopcNumPoliza().intValue() > 0){
				
				PreCarga preCarga = this.servicioCarga.obtenerObjeto(PreCarga.class, this.id);
				ConstruirObjeto.poblarBean(this, preCarga);
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede recuperar la PreCarga con id:").append(this.id);
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
	}

}
