/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstatus;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanEstatus {

	@Resource
	private ServicioEstatus servicioEstatus;
	
	private Integer alesCdEstatus;
	private String alesDescripcion;
	private String alesDescCorta;
	
	private StringBuilder message;
	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * @param servicioEstatus the servicioEstatus to set
	 */
	public void setServicioEstatus(ServicioEstatus servicioEstatus) {
		this.servicioEstatus = servicioEstatus;
	}
	/**
	 * @return the servicioEstatus
	 */
	public ServicioEstatus getServicioEstatus() {
		return servicioEstatus;
	}
	/**
	 * @return the alesCdEstatus
	 */
	public Integer getAlesCdEstatus() {
		return alesCdEstatus;
	}
	/**
	 * @return the alesDescripcion
	 */
	public String getAlesDescripcion() {
		return alesDescripcion;
	}
	/**
	 * @return the alesDescCorta
	 */
	public String getAlesDescCorta() {
		return alesDescCorta;
	}
	/**
	 * @param alesCdEstatus the alesCdEstatus to set
	 */
	public void setAlesCdEstatus(Integer alesCdEstatus) {
		this.alesCdEstatus = alesCdEstatus;
	}
	/**
	 * @param alesDescripcion the alesDescripcion to set
	 */
	public void setAlesDescripcion(String alesDescripcion) {
		this.alesDescripcion = alesDescripcion;
	}
	/**
	 * @param alesDescCorta the alesDescCorta to set
	 */
	public void setAlesDescCorta(String alesDescCorta) {
		this.alesDescCorta = alesDescCorta;
	}
	
	public BeanEstatus() {
		// TODO Auto-generated constructor stub
		
		this.message = new StringBuilder();
		
		if(FacesUtils.getBeanSesion().getCurrentBeanEstatus() != null){
			
			try {
				ConstruirObjeto.poblarBean(this, FacesUtils.getBeanSesion().getCurrentBeanEstatus());
			} catch (Excepciones e) {
				// TODO Auto-generated catch block
				this.message.append("No se puede recuperar el estatus.");
				this.log.error(message.toString(), e);
				throw new FacesException(message.toString(), e);
			}
		}
		
	}
}
