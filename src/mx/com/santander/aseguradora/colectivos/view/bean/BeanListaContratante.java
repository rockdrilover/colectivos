/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCliente;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author dflores
 *
 */
@Controller
@Scope("session")
public class BeanListaContratante{

	@Resource
	private ServicioContratante servicioContratante;
	@Resource
	private ServicioParametros servicioParametros;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private List<Object> listaContratantes;
	private short canal;
	private int certif;
	private short ramo;
	private String nacionalidad;
	private String cedula;
	private String razonSocial;
	private String direccion;
	private String cp;
	private String rfc;
	private String fecha_cons;
	private String ciudad;
	private String estado;
	private Integer idVenta;
	private Integer idProducto;
	private String tpPoliza ;
	private String respuesta;
	private int numPagina;
	private boolean chkUpdate;
	
	
	public BeanListaContratante() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.canal = 1;
		this.certif = 0;
		this.nacionalidad = "J";
		this.listaContratantes = new ArrayList<Object>();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_CONTRATANTE);
		
	}
	
	/**
	 * @return the servicioContratante
	 */
	public ServicioContratante getServicioContratante() {
		return servicioContratante;
	}
	/**
	 * @param servicioContratante the servicioContratanteto set
	 */
	public void setServicioContratante(ServicioContratante servicioContratante) {
		this.servicioContratante = servicioContratante;
	}
	/**
	 * @return the canal
	 */
	public short getCanal() {
		return canal;
	}
	/**
	 * @param canal the canal to set
	 */
	public void setCanal(short canal) {
		this.canal = canal;
	}
	
	/**
	 * @return the certif
	 */
	public int getCertif() {
		return certif;
	}
	/**
	 * @param certif the certif to set
	 */
	public void setCertif(int certif) {
		this.certif = certif;
	}
	
	
	/**
	 * @return the ramo
	 */
	public short getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(short ramo) {
		this.ramo = ramo;
	}

	public List<Object> getListaContratantes() {
		return listaContratantes;
	}
	public void setListaContratantes(List<Object> listaContratantes) {
		this.listaContratantes = listaContratantes;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getFecha_cons() {
		return fecha_cons;
	}
	public void setFecha_cons(String fecha_cons) {
		this.fecha_cons = fecha_cons;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Integer getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}
    	
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public String getTpPoliza() {
		return tpPoliza;
	}
	public void setTpPoliza(String tpPoliza) {
		this.tpPoliza = tpPoliza;
	}
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}
	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}
	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	
	public boolean getChkUpdate() {
		return chkUpdate;
	}
	public void setChkUpdate(boolean chkUpdate) {
		this.chkUpdate = chkUpdate;
	}
	

	
 public void consulta(){
		
		StringBuilder filtro = new StringBuilder();
		this.listaContratantes.clear();
		
		try {
			
			filtro.append(" and c.cacn_Nu_Cedula_Rif = '").append(this.cedula).append("' \n");
			if(this.nacionalidad.length() > 0 ){
				filtro.append(" and c.cacn_Cd_Nacionalidad = '").append(this.nacionalidad).append("' \n");
			}
			if( this.razonSocial.length() > 0 ){
				filtro.append(" and c.cacn_nm_apellido_razon = '").append(this.razonSocial).append("' \n");
			}
            if(this.nacionalidad !=null || this.razonSocial !=null ){
					List<Object[]> lista = this.servicioContratante.consulta(this.nacionalidad, this.cedula, this.razonSocial,filtro.toString());
					for(Object[] con: lista){
						this.setRazonSocial(con[2].toString());	
						this.setDireccion(con[3].toString());
						this.setCp(Utilerias.validaCP(con[4].toString()));
						this.setRfc(con[5].toString());
						this.setFecha_cons(con[6].toString());
						this.setCiudad(con[7].toString());
						this.setEstado(con[8].toString());
						this.setCedula(con[1].toString());	
						this.setNacionalidad(con[0].toString());
						this.getChkUpdate();
						this.listaContratantes.add(con);
					}	
            }
		} catch (Exception e) {
			e.printStackTrace();
			this.message.append("Error al recuperar los Contratantes");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		//return noRecibo.toString();
	}
	
	public String consulta2(){
		
		StringBuilder filtro = new StringBuilder();
		this.listaContratantes.clear();
		
		try {
					
			if(this.nacionalidad != null ){
				filtro.append(" and C.id.cacnCdNacionalidad = ").append(this.nacionalidad).append(" \n");
				filtro.append(" and C.id.cacnNuCedulaRif = ").append(this.cedula).append(" \n");
			}

			List<Object[]> lista= this.servicioContratante.consulta2(filtro.toString());
				
			for(Object[] contratante: lista){
				BeanContratante bean = new BeanContratante();
				/*bean.getId().setCareCasuCdSucursal(((BigDecimal)certificado[0]).shortValue());
				bean.setCareCarpCdRamo(((BigDecimal)certificado[1]).shortValue());
				bean.setCareCapoNuPoliza(((BigDecimal)certificado[2]).intValue());
				bean.getId().setCareNuRecibo((BigDecimal)certificado[3]);
				bean.setCareStRecibo(certificado[4].toString());
				bean.getEstatus().getId().setRvMeaning(certificado[5].toString());
				bean.setCareFeEmision((Date) certificado[6]);
				bean.setReciboAnterior((BigDecimal)certificado[7]);
				bean.setServicioContratante(this.servicioContratante);*/
				
				this.listaContratantes.add(bean);
			}
								
			return null;
		} catch (Exception e) {
			this.message.append("Error al recuperar certificados para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	// guarda la parametrizacion contratante - ramo para prima unica
	 public String guardar(){
			
			StringBuilder filtro = new StringBuilder();
			String idParam  = "CLIENTE";	
			String desParam = "CLIENTE GENERICO";
			this.idProducto = 0;
			Long next;
			Integer existe = 0;
			try {
				
				filtro.append("  where cp.copa_des_parametro = '").append(desParam).append("' \n");
				filtro.append(" and cp.copa_id_parametro = '").append(idParam).append("' \n");
				filtro.append(" and cp.copa_vvalor1 = 'J' ").append(" \n");
				filtro.append(" and cp.copa_vvalor2 = '").append(this.cedula).append("' \n");
				filtro.append(" and cp.copa_nvalor1 = 1").append(" \n");
				filtro.append(" and cp.copa_nvalor2 = ").append(this.ramo).append(" \n");
				filtro.append(" and cp.copa_nvalor3 = ").append(this.idVenta).append(" \n");
				existe = this.servicioParametros.existe(filtro.toString());

				if(existe == 0){
				
	            if(this.nacionalidad !=null || this.razonSocial !=null){
	            	
	            	next = this.servicioParametros.siguienteCdParam();		
	            	
	            	for(Object c: listaContratantes){
	            		
	            		if (chkUpdate== true && next!= 0 ){
						String res = this.servicioContratante.guarda(next,
								                                     idParam,
								                                     desParam,
								                                     this.nacionalidad,	
								                                     this.cedula,
								                                     this.canal,
								                                     this.ramo,
								                                     this.idVenta,
								                                     this.idProducto);

	            		}
					}
	            	respuesta = "Parametro guardado correctamente";
	            }
	            respuesta = "Paramerización ya existente";   
			 }
			} catch (Exception e) {
				// TODO: handle exception
				this.message.append("Error al guardar contratante - ramo");
				this.log.error(message.toString(), e);
				throw new FacesException(message.toString(), e);
			}
			return respuesta;
		}

}
