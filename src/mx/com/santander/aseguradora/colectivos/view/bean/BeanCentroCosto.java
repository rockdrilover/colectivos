package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class BeanCentroCosto {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ServicioRamo servicioRamo;
		
	private int 	numPagina;
	private Long  	copaCdParametro;
	private String   copaDesParametro;  // CATALOGO
	private String   copaIdParametro;	// CENTROCOSTOS
	private String   copaVvalor1;    	// PLAN ASOCADO
	private Integer  copaNvalor1; 		// canal 
	private Long     copaNvalor2;    	// ramo
	private Integer  copaNvalor5;  		// num centro costos
	private Integer  copaNvalor6;  		// plan asoc
	private Date     copaFvalor1;   	// ini_vig Date
	private Date     copaFvalor2;		// fin_vig   Date
	private HashMap<String, Object> hmResultados;
	private List<Object> 			listaCentroCosto;
	private List<Plan> 				listaPlanes;
	private int 					filaActual;
	private BeanParametros 			objActual = new BeanParametros();
	private BeanParametros			objNuevo = new BeanParametros();
	private String 					strRespuesta;
	private Set<Integer> 			keys = new HashSet<Integer>();
	
	public BeanCentroCosto() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CENTRO_COSTO);
		
		this.listaCentroCosto 	=  new ArrayList<Object>();
		this.log.debug("BeanListaCentroCosto creado");
		this.numPagina = 1;
		
	}
		
	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  getListaCentroCosto().iterator();
        while(it.hasNext()) {
        	BeanParametros objParametro = (BeanParametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActual = objParametro;
                break;
            }
        }
    }
	
	public String consultaCentroCostos(){
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean;
		List<Object> lstResultados;
		Parametros parametro;
		Iterator<Object> it;
		
		try {
			getListaCentroCosto().clear();
			if(this.copaNvalor2 != null && this.copaNvalor2.intValue() > 0) {
				hmResultados = new HashMap<String, Object>();
				filtro.append("and P.copaNvalor2 = " + this.copaNvalor2 + "\n" );
				filtro.append("and P.copaDesParametro = 'CATALOGO'" );
				filtro.append("and P.copaIdParametro  = 'CENTROCOSTOS'" );
				filtro.append("order by P.copaNvalor5" );
				lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
				
			    it = lstResultados.iterator();
			    while(it.hasNext()) {
			    	parametro = (Parametros) it.next();
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
					bean.setAuxDato(servicioRamo.getDescRamo(bean.getCopaNvalor2()));
					bean.setServicioParametros(this.servicioParametros);
					
					listaCentroCosto.add(bean);
					hmResultados.put(bean.getCopaCdParametro().toString(), bean);
				}
			    
			    setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");
			} else {
				setStrRespuesta("Selecciona un RAMO para poder realizar la consulta.");
				setHmResultados(null);
			}
		
			
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.log.error("El centro de costos ya existe no es posble darlo de alta nuevamente.", e);
			FacesUtils.addErrorMessage("El centro de costos ya existe no es posble darlo de alta nuevamente.");
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			e.printStackTrace();
			this.log.error("No se puede dar de alta el centro de costos.", e);
			FacesUtils.addErrorMessage("No se puede dar de alta el centro de costos.");
			return NavigationResults.FAILURE;
		}
	}
	
	public void modificar(){
		try {
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual) ;
			servicioParametros.actualizarObjeto(parametro);
			hmResultados.remove(objActual.getCopaCdParametro().toString());
			hmResultados.put(objActual.getCopaCdParametro().toString(), objActual);
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al MODIFICAR registro.");
			e.printStackTrace();
		}
	} 
	
	public void guardar() {
		Long secParametro;
		
		try {
			secParametro = servicioParametros.siguente();
			objNuevo.setCopaCdParametro(secParametro); 	// SIGUIENTE NUMERO 
			objNuevo.setCopaIdParametro("CENTROCOSTOS");// CENTROCOSTOS
			objNuevo.setCopaDesParametro("CATALOGO"); 	// CATALOGO
			objNuevo.setCopaNvalor1(1);					// CANAL 1-DEFAULT

			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
			this.servicioParametros.guardarObjeto(parametro);
			setStrRespuesta("Se guardo con EXITO el registro.");
			
			this.copaNvalor2 = objNuevo.getCopaNvalor2();
			objNuevo = new BeanParametros();
			consultaCentroCostos();
		} catch (Exception e) {
			setStrRespuesta("Error al GUARDAR registro.");
			e.printStackTrace();
		}
	}
		
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados().values());
        return lstValues;
    }

	public void afterPropertiesSet() throws Exception {
		try {
			
		} catch (Exception e) {
			this.log.error("No se pueden cargar los centros de costos.", e);
			throw new FacesException("No se pueden cargar los centros de costos.", e);
		}
	}
				
	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	public Long getCopaCdParametro() {
		return copaCdParametro;
	}
	public void setCopaCdParametro(Long copaCdParametro) {
		this.copaCdParametro = copaCdParametro;
	}
	public String getCopaDesParametro() {
		return copaDesParametro;
	}
	public void setCopaDesParametro(String copaDesParametro) {
		this.copaDesParametro = copaDesParametro;
	}
	public String getCopaIdParametro() {
		return copaIdParametro;
	}
	public void setCopaIdParametro(String copaIdParametro) {
		this.copaIdParametro = copaIdParametro;
	}
	public String getCopaVvalor1() {
		return copaVvalor1;
	}
	public void setCopaVvalor1(String copaVvalor1) {
		this.copaVvalor1 = copaVvalor1;
	}
	public Integer getCopaNvalor1() {
		return copaNvalor1;
	}
	public void setCopaNvalor1(Integer copaNvalor1) {
		this.copaNvalor1 = copaNvalor1;
	}
	public Long getCopaNvalor2() {
		return copaNvalor2;
	}
	public void setCopaNvalor2(Long copaNvalor2) {
		this.copaNvalor2 = copaNvalor2;
	}
	public Integer getCopaNvalor5() {
		return copaNvalor5;
	}
	public void setCopaNvalor5(Integer copaNvalor5) {
		this.copaNvalor5 = copaNvalor5;
	}
	public Integer getCopaNvalor6() {
		return copaNvalor6;
	}
	public void setCopaNvalor6(Integer copaNvalor6) {
		this.copaNvalor6 = copaNvalor6;
	}
	public Date getCopaFvalor1() {
		return copaFvalor1;
	}
	public void setCopaFvalor1(Date copaFvalor1) {
		this.copaFvalor1 = copaFvalor1;
	}
	public Date getCopaFvalor2() {
		return copaFvalor2;
	}
	public void setCopaFvalor2(Date copaFvalor2) {
		this.copaFvalor2 = copaFvalor2;
	}
	public List<Object> getListaCentroCosto() {
		return listaCentroCosto;
	}
	public void setListaCentroCosto(List<Object> listaCentroCosto) {
		this.listaCentroCosto = listaCentroCosto;
	}
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public HashMap<String, Object> getHmResultados() {
		return hmResultados;
	}
	public void setHmResultados(HashMap<String, Object> hmResultados) {
		this.hmResultados = hmResultados;
	}
	public Set<Integer> getKeys() {
		return keys;
	}
	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}
	public BeanParametros getObjActual() {
		return objActual;
	}
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	public BeanParametros getObjNuevo() {
		return objNuevo;
	}
	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}
	public List<Plan> getListaPlanes() {
		return listaPlanes;
	}
	public void setListaPlanes(List<Plan> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}
	public int getFilaActual() {
		return filaActual;
	}
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}
	public ServicioRamo getServicioRamo() {
		return servicioRamo;
	}
	public void setServicioRamo(ServicioRamo servicioRamo) {
		this.servicioRamo = servicioRamo;
	}
}