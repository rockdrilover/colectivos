/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanExtraPrima {

	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioProducto servicioProducto;
	
	private Log log = LogFactory.getLog(this.getClass());
	private Short canal;
	private Short ramo;
	private Long  poliza;
	private String idCertificado;
	private String rfc;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombre;
	private String suma;
	private String porcentaje;
	private String observaciones;
	private String causa;
	private Integer valorProducto;
	
	private List<String> opciones;
	
	//Causas
	private String edad;
	private String cancer;
	private String diabetes;
	private String cardio;
	private String vih;
	private String otros;

	private List<Object> listaProductos;

	private boolean extraprimas;
	private String respuesta;
	private StringBuilder message;

	public BeanExtraPrima() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.opciones = new ArrayList<String>();
		this.opciones.clear();
		this.opciones.add("1");
		this.listaProductos = new ArrayList<Object>();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_EXTRA_PRIMA);
	}
	
	
	/**
	 * 
	 */
	public String extraprimas() {
		
		
		BeanPreCarga bean;
		PreCarga preCarga;
		PreCargaId id;
		
		try 
		{
			
			this.message.append("Inserta datos en Pre-Carga");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message); 
				
			    bean = new BeanPreCarga();			
				
				this.message.append("Leyendo datos pre-carga Extra Prima.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
									
				this.canal = 99;

				//this.servicioCarga.guardarObjetos(lista);
				
				id = new PreCargaId(this.canal, this.ramo, 0L, this.idCertificado, "EXP", this.suma);
				
							bean.setId(id);
							bean.setCopcApMaterno(this.apellidoMaterno);
							bean.setCopcApPaterno(this.apellidoPaterno);
							bean.setCopcNombre(this.nombre);
							bean.setCopcRfc(this.rfc);
							bean.setCopcDato29(this.porcentaje);
							bean.setCopcDato11(this.observaciones);
							
							//CAUSAS
							bean.setCopcDato35(this.edad.equals("true") ? "Edad Avanzada" : "");
							bean.setCopcDato36(this.cancer.equals("true") ? "Cancer" : "");
							bean.setCopcDato37(this.diabetes.equals("true") ? "Diabetes" : "");
							bean.setCopcDato38(this.cardio.equals("true") ? "Cardiacas" : "");
							bean.setCopcDato39(this.vih.equals("true") ? "Sida" : "");
							bean.setCopcDato40(this.otros.equals("true") ? "Otros" : "");
							bean.setCopcDato12(this.causa);

							
							bean.setCopcCargada("20");  //para diferenciar de la emision.
						
							preCarga = (PreCarga) ConstruirObjeto.crearBean(PreCarga.class, bean);
							
							servicioCarga.guardarObjeto(preCarga);
		}	
		     
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede cargar la informacion de Extra Prima.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		return respuesta;
	}


	public String cargaProductos(){
		this.message.append("cargando productos.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder filtro = new StringBuilder(100);
		
		this.listaProductos.clear();
		if(this.ramo != null){
			try {
		
				filtro.append(" and P.id.alprCdRamo = ").append(this.ramo.intValue()).append("\n");
				List<Producto> lista = this.servicioProducto.obtenerObjetos(filtro.toString());
				
				for(Producto producto: lista){
					this.listaProductos.add(new SelectItem(producto.getId().getAlprCdProducto(), producto.getId().getAlprCdProducto().toString()+ " - " + producto.getAlprDeProducto()));
				 }
				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				message.append("No se pueden cargar los productos.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				return NavigationResults.FAILURE;
			}
		} else{
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			return NavigationResults.RETRY;
		}
	}
	
	
	public Short getCanal() {
		return canal;
	}

	public void setCanal(Short canal) {
		this.canal = canal;
	}

	public Short getRamo() {
		return ramo;
	}

	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}

	public Long getPoliza() {
		return poliza;
	}

 	public void setPoliza(Long poliza) {
		this.poliza = poliza;
		if (this.poliza == 0){
			setExtraprimas(true);
		}else{
			setExtraprimas(false);
			opciones.clear();
			opciones.add("1");
		}
	}

	public String getIdCertificado() {
		return idCertificado;
	}

	public void setIdCertificado(String idCertificado) {
		this.idCertificado = idCertificado;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc.toUpperCase();
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno.toUpperCase();
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno.toUpperCase();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre.toUpperCase();
	}

	public String getSuma() {
		return suma;
	}

	public void setSuma(String suma) {
		this.suma = suma;
	}

	public String getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}

	public List<String> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<String> opciones) {
		this.opciones = opciones;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	public String getCausa() {
		return causa;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getCancer() {
		return cancer;
	}

	public void setCancer(String cancer) {
		this.cancer = cancer;
	}

	public String getDiabetes() {
		return diabetes;
	}

	public void setDiabetes(String diabetes) {
		this.diabetes = diabetes;
	}

	public String getCardio() {
		return cardio;
	}

	public void setCardio(String cardio) {
		this.cardio = cardio;
	}

	public String getVih() {
		return vih;
	}

	public void setVih(String vih) {
		this.vih = vih;
	}

	public String getOtros() {
		return otros;
	}

	public void setOtros(String otros) {
		this.otros = otros;
	}


	public void setExtraprimas(boolean extraprimas) {
		this.extraprimas = extraprimas;
	}


	public boolean isExtraprimas() {
		return extraprimas;
	}

	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}

	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}


	public void setValorProducto(Integer valorProducto) {
		this.valorProducto = valorProducto;
	}


	public Integer getValorProducto() {
		return valorProducto;
	}


	public void setListaProductos(List<Object> listaProductos) {
		this.listaProductos = listaProductos;
	}


	public List<Object> getListaProductos() {
		return listaProductos;
	}

	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}
	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}


	
}

