package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAnexo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioSeguimiento;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioSuscripcion;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.SmbUtil;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.dto.AnexoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.FiltroSuscripcionDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.GrupoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SeguimientoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.SuscripcionDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ValidarSuscripcionDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

@Controller
@Scope("session")
public class BeanSuscripcion {
	@Resource
	private ServicioSuscripcion servicioSuscripcion;

	@Resource
	private ServicioSeguimiento servicioSeguimiento;

	@Resource
	private ServicioAnexo servicioAnexo;

	/**
	 * Logger
	 */
	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * Mensaje de error
	 */
	private StringBuilder message;
	private String respuesta;

	private String FORMATO_FECHA_SUSCRIPCION = "dd/MM/yyyy";

	/**
	 * Consulta de suscripciones
	 */
	private List<SuscripcionDTO> suscripciones;

	/**
	 * Objeto filtro de suscriiones
	 */
	private FiltroSuscripcionDTO filtroSuscripcionDTO;

	private File archivo;

	private String nArchivo;

	private ProgressBar pb;

	private SuscripcionDTO seleccionado;

	private SuscripcionDTO modifica;

	private String strRespuesta;

	private String strRespuestaError;

	private long selectID;

	private List<GrupoDTO> grupos;

	private List<SeguimientoDTO> seguimientos;

	private List<AnexoDTO> archivosSeguimiento;

	private Boolean checking;

	private SeguimientoDTO seguimientoActual;
	private int numPagina;

	private AnexoDTO archivoActual;

	private Boolean permiso;

	/**
	 * Deshabilita campos si no hay examen medico
	 */
	private Boolean bExamMedico = false;

	/**
	 * Constructor del bean en sesion
	 */
	public BeanSuscripcion() {
		this.message = new StringBuilder();
		suscripciones = new ArrayList<SuscripcionDTO>();
		filtroSuscripcionDTO = new FiltroSuscripcionDTO();
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_SUSCRIPCION);
		seleccionado = new SuscripcionDTO();
		modifica = new SuscripcionDTO();
		strRespuesta = "";
		grupos = new ArrayList<GrupoDTO>();
		seguimientos = new ArrayList<SeguimientoDTO>();
		checking = false;
		seguimientoActual = new SeguimientoDTO();
		this.numPagina = 1;
		archivosSeguimiento = new ArrayList<AnexoDTO>();
		nArchivo = "";
		permiso = false;
		bExamMedico = false;

	}

	/**
	 * Inicializa pagina en la carga
	 */
	public void inicializaCarga() {
		this.message = new StringBuilder();
		suscripciones = new ArrayList<SuscripcionDTO>();
		filtroSuscripcionDTO = new FiltroSuscripcionDTO();
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		seleccionado = new SuscripcionDTO();
		modifica = new SuscripcionDTO();
		strRespuesta = "";
		grupos = new ArrayList<GrupoDTO>();
		seguimientos = new ArrayList<SeguimientoDTO>();
		checking = false;
		seguimientoActual = new SeguimientoDTO();
		this.numPagina = 1;
		archivosSeguimiento = new ArrayList<AnexoDTO>();
		nArchivo = "";
		permiso = false;
		bExamMedico = false;
	}

	/**
	 * Habilita elementos si hay examen medico
	 */
	public void habilitaExamenMedico() {

		if ("N".equals(modifica.getCosuExamMedico())) {
			bExamMedico = true;
		} else {
			bExamMedico = false;

		}

	}

	/**
	 * Valida permisos de edicion
	 */
	public void validaPermisoEdicion() {
		/**
		 * Valida si pertenece al grupo de operaciones
		 * 
		 */
		try {
			ExternalContext context;
			context = FacesContext.getCurrentInstance().getExternalContext();

			HttpServletRequest request = (HttpServletRequest) context.getRequest();

			HttpSession sesion = request.getSession(true);
			permiso = servicioSuscripcion.validaPermisoModificaciones((String) sesion.getAttribute("username"));
		} catch (Exception e) {
			log.error("Error al Consultar permisos.", e);
			permiso = false;
		}

		/**
		 * Fin validacion
		 */
	}

	/**
	 * Realiza la consulta principal de suscripciones
	 * 
	 * @throws Exception
	 */
	public void consultaSuscripciones() throws Exception {
		suscripciones = new ArrayList<SuscripcionDTO>();
		strRespuesta = "";
		seguimientos = new ArrayList<SeguimientoDTO>();
		try {
			suscripciones = servicioSuscripcion.consultaSuscripciones(filtroSuscripcionDTO);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al Consultar las polizas.", e);
			FacesUtils.addInfoMessage("Error al Consultar suscripciones");
		}
	}

	public void consultaSeguimientoAreas() throws Exception {
		strRespuesta = "";
		ExternalContext context;
		try {
			seguimientos = new ArrayList<SeguimientoDTO>();
			for (SuscripcionDTO sus : suscripciones) {
				if (sus.getCosuId() != selectID) {
					sus.setSelected(false);
				}
			}
			if (!checking) {
				seleccionado = new SuscripcionDTO();
				seleccionado.setCosuId(selectID);
				seleccionado = servicioSuscripcion.consultaObjeto(selectID);
				modifica = seleccionado;
				FiltroSuscripcionDTO filtroSuscripcionDTO = new FiltroSuscripcionDTO();
				filtroSuscripcionDTO.setCosuId(selectID);

				context = FacesContext.getCurrentInstance().getExternalContext();

				HttpServletRequest request = (HttpServletRequest) context.getRequest();

				HttpSession sesion = request.getSession(true);

				filtroSuscripcionDTO.setArea((String) sesion.getAttribute("username"));
				// TODO Falta filtro grupo
				seguimientos = servicioSeguimiento.consultaSeguimientoSuscripcion(filtroSuscripcionDTO);

				// numPagina = seguimientos.size() / 10;
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al Consultar las polizas.", e);
			FacesUtils.addInfoMessage("Error al Consultar seguimiento suscripcion");
		}
	}

	/**
	 * Guarda el seguimiento de tarea
	 */
	public void atenderTarea() {
		strRespuesta = "";
		strRespuestaError = "";
		try {
			ExternalContext context;
			context = FacesContext.getCurrentInstance().getExternalContext();

			HttpServletRequest request = (HttpServletRequest) context.getRequest();
			HttpSession sesion = request.getSession(true);

			if ("A".equals(seguimientoActual.getCoseEstatus())) {

				FacesUtils.addErrorMessage("Tarea atendida no es posible modificarla.");

			} else {

				seguimientoActual.setCoseFechaAtencion(new Date());
				seguimientoActual.setCoseUsuarioAtendio((String) sesion.getAttribute("username"));
				seguimientoActual.setCoseEstatus("A");
				servicioSeguimiento.actualizarSeguimiento(seguimientoActual);
				Boolean res = servicioSuscripcion.terminaSuscripcion(modifica);

				if (res) {
					consultaSuscripciones();

					for (SuscripcionDTO sus : suscripciones) {
						if (sus.getCosuId() == selectID) {
							sus.setSelected(true);
						}
					}

					consultaSeguimientoAreas();
				}

				FacesUtils.addInfoMessage("Exito al guardar seguimiento");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al guardar seguimiento.", e);
			strRespuestaError = "Error al guardar seguimiento";
			FacesUtils.addInfoMessage("Error al guardar seguimiento");
		}
	}

	/**
	 * Consulta areas a asignar
	 * 
	 * @throws Exception
	 */
	public void consultaAreaAsignar() throws Exception {
		strRespuesta = "";
		try {
			modifica = seleccionado;
			grupos = servicioSuscripcion.areasTarea();
			validaPermisoEdicion();

			if (!permiso) {
				FacesUtils.addInfoMessage("Operacion no permitida.");
			}
		} catch (Exception e) {
			FacesUtils.addInfoMessage("Error al actualizar areas");
		}

	}

	/**
	 * Inicializa edicion de registro
	 */
	public void seleccionaModificar() {
		strRespuesta = "";
		modifica = seleccionado;
		validaPermisoEdicion();
		habilitaExamenMedico();
		if (!permiso) {
			FacesUtils.addInfoMessage("Operacion no permitida.");
		}
	}

	public String actualizaEstatusDictamen() {

		SuscripcionDTO seleccionadoDB = new SuscripcionDTO();
		seleccionadoDB.setCosuId(selectID);
		seleccionadoDB = servicioSuscripcion.consultaObjeto(selectID);

		if ("T".equals(seleccionadoDB.getCosuDictamen())) {

			return actualizaDictamen();
		}
		FacesUtils.addInfoMessage("La suscripcion no se encuentra en estatus terminado favor de completar tareas.");
		return NavigationResults.FAILURE;
	}

	public String suspenderSuscripcion() {
		validaPermisoEdicion();
		try {
			if (permiso) {
				SuscripcionDTO seleccionadoDB = new SuscripcionDTO();
				seleccionadoDB.setCosuId(selectID);
				seleccionadoDB = servicioSuscripcion.consultaObjeto(selectID);
				if ("P".equals(seleccionadoDB.getCosuDictamen()) || "E".equals(seleccionadoDB.getCosuDictamen())) {
					seleccionadoDB.setCosuDictamen("S");
					servicioSuscripcion.actualizaSuscripcion(seleccionadoDB);
					FacesUtils.addInfoMessage("Exito al actualizar suscripcion.");
					consultaSuscripciones();

					return NavigationResults.SUCCESS;
				} else if ("S".equals(seleccionadoDB.getCosuDictamen())) {
					seleccionadoDB.setCosuDictamen("P");
					servicioSuscripcion.actualizaSuscripcion(seleccionadoDB);
					consultaSuscripciones();
					FacesUtils.addInfoMessage("Exito al actualizar suscripcion.");
					return NavigationResults.SUCCESS;
				}
			}
		} catch (Exception e) {
			setStrRespuesta("Error al actualizar Dictamen.");
			e.printStackTrace();
			log.error("Error al actualizar Dictamen.", e);
			FacesUtils.addInfoMessage("Error al actualizar suscripcion.");
			return NavigationResults.FAILURE;
		}
		FacesUtils.addInfoMessage("Operacion no permitida.");
		return NavigationResults.FAILURE;
	}

	public String actualizaDictamen() {
		strRespuesta = "";
		try {

			servicioSuscripcion.actualizaSuscripcion(modifica);
			FacesUtils.addInfoMessage("Exito al actualizar suscripcion.");

			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			setStrRespuesta("Error al actualizar Dictamen.");
			e.printStackTrace();
			log.error("Error al actualizar Dictamen.", e);
			FacesUtils.addInfoMessage("Error al actualizar suscripcion.");
			return NavigationResults.FAILURE;
		}
	}

	/**
	 * Carga archivo en disco
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void listener(UploadEvent event) throws Exception {
		strRespuesta = "";
		UploadItem item = event.getUploadItem();
		File archivoTemporal;
		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		String nombreArchivo = null;
		String nombreRuta;

		Utilerias.resetMessage(message);

		if (item.isTempFile()) {

			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			nArchivo = item.getFileName();
			file.setTamano(item.getData().length);

			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();

		} else {

			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			nArchivo = item.getFileName().substring(item.getFileName().lastIndexOf("\\") + 1);
			file.setTamano(item.getData().length);

			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			/**
			 * Guardar el archivo en carpeta del proyecto
			 */
			nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);

			archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
			FileUtil.write(archivoTemporal, item.getData());

			this.archivo = archivoTemporal;
		}

		this.message.append("Archivo cargado correctamente.");
		this.log.debug(this.message.toString());
		this.respuesta = this.message.toString();
		Utilerias.resetMessage(message);
	}

	/**
	 * Carga Archivo Suscripciones
	 * 
	 * @return Pagina de error
	 * @throws Excepciones
	 *             Excepcion al insertar datos
	 */
	public String cargaArchivoSuscripcion() throws Excepciones {
		strRespuesta = "";
		this.log.debug("Iniciando proceso de carga Suscripciones");

		String strForward = null;
		log.debug("Inicio carga ");

		validaPermisoEdicion();
		if (!permiso) {
			FacesUtils.addInfoMessage("Operacion no permitida.");
			return "";
		}

		if (this.archivo != null) {
			stringToSuscripcionDTO(archivo, ",");
			/*
			 * for (SuscripcionDTO detalle : lista) { try {
			 * detalle.setCosuUsuarioCrea((String) sesion.getAttribute("username"));
			 * detalle.setCosuFechaCarga(new Date());
			 * servicioSuscripcion.guardarObjeto(detalle); strForward =
			 * NavigationResults.SUCCESS; } catch (Exception e) { e.printStackTrace();
			 * strForward = NavigationResults.FAILURE; } }
			 */
		}
		log.debug("Fin carga ");
		return strForward;
	}

	/**
	 * Guarda archivo seguimiento en ruta samba
	 */
	public void cargaArchivoSeguimiento() {
		SmbUtil smbUtil;

		try {
			AnexoDTO colectivosAnexo = new AnexoDTO();
			colectivosAnexo.setCoseNomDocto(nArchivo);
			colectivosAnexo.setCoanCoseID(seguimientoActual.getCoseId());

			colectivosAnexo = servicioAnexo.guardarObjeto(colectivosAnexo);

			Parametros parametros = servicioSuscripcion.archivoParametros();
			String extension = "";
			int i = nArchivo.lastIndexOf('.');
			if (i > 0) {
				extension = nArchivo.substring(i + 1);
			}
			smbUtil = new SmbUtil(parametros.getCopaVvalor6(), parametros.getCopaVvalor7());

			archivosSeguimiento = servicioAnexo.consultaAnexoSuscripcion(seguimientoActual);
			String sambaRuta = parametros.getCopaRegistro();

			Archivo ar = new Archivo(archivo);

			ar.copiarSmbArchivo2(smbUtil.getFile(sambaRuta + String.valueOf(colectivosAnexo.getCoanCoseID())
					+ String.valueOf(colectivosAnexo.getCoseConsecutivo()) + "." + extension));

			FacesUtils.addInfoMessage("Exito al cargar archivo seguimiento");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al cargar archivo seguimiento.", e);
			FacesUtils.addInfoMessage("Error al cargar archivo seguimiento");
		}
	}

	/**
	 * Descarga archivos suscripcion en ZIP
	 */
	public void getFileZip() {
		SmbUtil smbUtil;
		FileOutputStream fos = null;
		ZipOutputStream zipos = null;
		ZipEntry zipEntry = null;
		FileInputStream fis = null;
		int BUFFER_SIZE = 1024;
		byte[] buffer = new byte[BUFFER_SIZE];
		ServletOutputStream sos;
		int longitud;
		byte[] datos;
		try {
			Parametros parametros = servicioSuscripcion.archivoParametros();
			String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
			smbUtil = new SmbUtil(parametros.getCopaVvalor6(), parametros.getCopaVvalor7());
			String sambaRuta = parametros.getCopaRegistro();
			String extension = "";
			fos = new FileOutputStream(ruta + File.separator + seguimientoActual.getCoseId() + ".zip");
			zipos = new ZipOutputStream(fos);

			for (AnexoDTO anexo : archivosSeguimiento) {
				int i = anexo.getCoseNomDocto().lastIndexOf('.');
				if (i > 0) {
					extension = anexo.getCoseNomDocto().substring(i + 1);
				}

				Archivo objArchivoSmb = new Archivo(
						smbUtil.getFile(sambaRuta + String.valueOf(archivoActual.getCoanCoseID())
								+ String.valueOf(archivoActual.getCoseConsecutivo()) + "." + extension));
				objArchivoSmb.copiarSmbArchivo2(new File(ruta + File.separator + anexo.getCoseNomDocto()));
				// File descarga = new File(ruta + File.separator + anexo.getCoseNomDocto());
				zipEntry = new ZipEntry(anexo.getCoseNomDocto());
				fis = new FileInputStream(ruta + File.separator + anexo.getCoseNomDocto());
				zipos.putNextEntry(zipEntry);
				int len = 0;
				while ((len = fis.read(buffer, 0, BUFFER_SIZE)) != -1) {
					zipos.write(buffer, 0, len);
				}
				zipos.flush();
			}

			zipos.close();
			fos.close();

			File descarga = new File(ruta + File.separator + seguimientoActual.getCoseId() + ".zip");
			fis = new FileInputStream(descarga);
			longitud = fis.available();
			datos = new byte[longitud];
			fis.read(datos);
			fis.close();
			HttpServletResponse response = FacesUtils.getServletResponse();
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition",
					"attachment;filename=\"" + seguimientoActual.getCoseId() + ".zip" + "\"");

			sos = response.getOutputStream();
			sos.write(datos);
			sos.flush();
			sos.close();
			descarga.delete();
			FacesContext.getCurrentInstance().responseComplete();

			for (AnexoDTO anexo : archivosSeguimiento) {
				int i = anexo.getCoseNomDocto().lastIndexOf('.');
				if (i > 0) {
					extension = anexo.getCoseNomDocto().substring(i + 1);
				}
				File elimina = new File(ruta + File.separator + anexo.getCoseNomDocto());
				elimina.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al descargar archivo seguimiento.", e);
			FacesUtils.addInfoMessage("Error al descargar archivo seguimiento");
		}
	}

	/**
	 * Obtiene archivo suscripcion
	 */
	public void getFileSuscripcion() {
		SmbUtil smbUtil;
		InputStream fis;
		ServletOutputStream sos;
		int longitud;
		byte[] datos;
		try {
			Parametros parametros = servicioSuscripcion.archivoParametros();
			String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
			smbUtil = new SmbUtil(parametros.getCopaVvalor6(), parametros.getCopaVvalor7());
			String sambaRuta = parametros.getCopaRegistro();
			String extension = "";
			int i = archivoActual.getCoseNomDocto().lastIndexOf('.');
			if (i > 0) {
				extension = archivoActual.getCoseNomDocto().substring(i + 1);
			}

			Archivo objArchivoSmb = new Archivo(
					smbUtil.getFile(sambaRuta + String.valueOf(archivoActual.getCoanCoseID())
							+ String.valueOf(archivoActual.getCoseConsecutivo()) + "." + extension));

			objArchivoSmb.copiarSmbArchivo2(new File(ruta + File.separator + archivoActual.getCoseNomDocto()));

			File descarga = new File(ruta + File.separator + archivoActual.getCoseNomDocto());
			fis = new FileInputStream(descarga);
			longitud = fis.available();
			datos = new byte[longitud];
			fis.read(datos);
			fis.close();
			HttpServletResponse response = FacesUtils.getServletResponse();
			response.setHeader("Content-Disposition",
					"attachment;filename=\"" + archivoActual.getCoseNomDocto() + "\"");

			sos = response.getOutputStream();
			sos.write(datos);
			sos.flush();
			sos.close();
			descarga.delete();
			FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al descargar archivo seguimiento.", e);
			FacesUtils.addInfoMessage("Error al descargar archivo seguimiento");
		}
	}

	/**
	 * Almacena informacion en base de datos
	 * 
	 * @param file
	 *            archivo suscripcion
	 * @param delimitador
	 *            delimitar de CSV
	 * @return Lsita de susucripciones
	 */
	private List<SuscripcionDTO> stringToSuscripcionDTO(File file, String delimitador) {
		strRespuesta = "";
		CsvReader csv = null;
		CsvWriter csvResultados = null;
		FileReader fr;
		List<SuscripcionDTO> res = new ArrayList<SuscripcionDTO>();
		Map<String, String> mh;
		String[] headers = null;
		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		StringBuilder resultado;
		String resultadoRenglon = "";
		try {
			this.message.append("Abriendo archivo de suscripcion");
			this.log.debug(message.toString());

			Utilerias.resetMessage(message);
			OutputStream outputStream = new FileOutputStream(ruta + File.separator + "Resultados" + nArchivo);
			Writer outputStreamWriter = new OutputStreamWriter(outputStream);

			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			csvResultados = new CsvWriter(outputStreamWriter, delimitador.charAt(0));

			if (csv.readHeaders()) {
				headers = csv.getHeaders();

				mh = new HashMap<String, String>();

				for (String h : headers) {
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}

				resultado = new StringBuilder(join(headers, ","));
				resultado.append(",Resultado Operacion");

				csvResultados.writeRecord(resultado.toString().split(","));

				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				ExternalContext context;
				context = FacesContext.getCurrentInstance().getExternalContext();

				HttpServletRequest request = (HttpServletRequest) context.getRequest();
				HttpSession sesion = request.getSession(true);
				while (csv.readRecord()) {
					SuscripcionDTO suscripcionDTO = new SuscripcionDTO();
					ValidarSuscripcionDTO.isSuscripcionDTO(csv);
					
					try {
						suscripcionDTO.setCosuUsuarioCrea((String) sesion.getAttribute("username"));
						suscripcionDTO.setCosuFechaCarga(new Date());
						if (suscripcionDTO.getCorrecto()) {
							servicioSuscripcion.guardarObjeto(suscripcionDTO);
							resultadoRenglon = "OK\n";
						} else {
							resultadoRenglon = suscripcionDTO.getErrorValidacion() + "\n";
						}
					} catch (Exception e) {
						e.printStackTrace();
						log.error(e.getMessage());
						this.log.error(message.toString(), e);
						resultadoRenglon = "Error al agregar registro.\n";
						// FacesUtils.addErrorMessage(message.toString());
					}
					resultado = new StringBuilder(csv.getRawRecord());
					resultado.append(",");
					resultado.append(resultadoRenglon);
					// csvResultados.write(arg0, arg1);
					csvResultados.writeRecord(resultado.toString().split(","));
					// res.add(suscripcionDTO);
				}

				csv.close();
				csvResultados.close();
				fr.close();
				file.delete();
				FacesUtils.addInfoMessage("Exito al procesar archivo de suscripciones.");

			} else {
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
				FacesUtils.addInfoMessage(
						"Error al procesar archivo de suscripciones. \\n El archivo CSV no tiene encabezados");

			}
		} catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			FacesUtils.addInfoMessage("Error al procesar archivo de suscripciones.");

		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pud� cargar la informacion.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			FacesUtils.addInfoMessage("Error al procesar archivo de suscripciones.");

		}
		return res;

	}

	public void descargarArchivoResultados() {
		int longitud;
		byte[] datos;
		FileInputStream fis = null;
		ServletOutputStream sos;
		validaPermisoEdicion();
		if (!permiso) {
			FacesUtils.addInfoMessage("Operacion no permitida.");
			return;
		}

		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		try {
			File descarga = new File(ruta + File.separator + "Resultados" + nArchivo);
			fis = new FileInputStream(descarga);
			longitud = fis.available();
			datos = new byte[longitud];
			fis.read(datos);
			fis.close();
			HttpServletResponse response = FacesUtils.getServletResponse();

			response.setHeader("Content-Disposition", "attachment;filename=\"" + "Resultados" + nArchivo + "\"");

			sos = response.getOutputStream();
			sos.write(datos);
			sos.flush();
			sos.close();
			descarga.delete();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			this.log.error(message.toString(), e);

		}

	}

	public void consultarArchivos() {
		strRespuesta = "";

		archivosSeguimiento = new ArrayList<AnexoDTO>();

		try {
			archivosSeguimiento = servicioAnexo.consultaAnexoSuscripcion(seguimientoActual);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error al Consultar  Archivos seguimiento", e);
			FacesUtils.addInfoMessage("Error al Consultar  Archivos seguimiento");
		}

	}

	/**
	 * Asigna las tareas al grupo
	 */
	public String enviaTareas() {

		String strForward = NavigationResults.SUCCESS;
		ExternalContext context;
		context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		HttpSession sesion = request.getSession(true);
		strRespuesta = "";
		if ("E".equals(modifica.getCosuDictamen()) || "P".equals(modifica.getCosuDictamen())
				|| "T".equals(modifica.getCosuDictamen())) {
			for (GrupoDTO grupo : grupos) {
				try {

					if (grupo.getSelected()) {
						SeguimientoDTO seg = new SeguimientoDTO();
						seg.setCoseEstatus("P");
						seg.setCoseFechaAsignacion(new Date());
						seg.setCoseUsuarioAsigno((String) sesion.getAttribute("username"));
						seg.setCoseGrupo(grupo.getId());
						seg.setCoseArea(grupo.getDescripcion());
						seg.setCoseCosuId(modifica.getCosuId());
						seg.setCoseObsAsig(grupo.getObservaciones());
						servicioSeguimiento.guardarObjeto(seg);
						modifica.setCosuDictamen("E");
						servicioSuscripcion.actualizaSuscripcion(modifica);

						consultaSuscripciones();

						for (SuscripcionDTO sus : suscripciones) {
							if (sus.getCosuId() == selectID) {
								sus.setSelected(true);
							}
						}
					}

					strForward = NavigationResults.SUCCESS;

				} catch (Exception e) {
					FacesUtils.addInfoMessage("Error al enviar  tarea");

					e.printStackTrace();
					strForward = NavigationResults.FAILURE;
				}
			}

			try {
				FiltroSuscripcionDTO filtroSuscripcionDTO = new FiltroSuscripcionDTO();
				filtroSuscripcionDTO.setCosuId(selectID);

				context = FacesContext.getCurrentInstance().getExternalContext();

				filtroSuscripcionDTO.setArea((String) sesion.getAttribute("username"));
				// TODO Falta filtro grupo
				seguimientos = servicioSeguimiento.consultaSeguimientoSuscripcion(filtroSuscripcionDTO);
				FacesUtils.addInfoMessage("Exito al enviar  tarea");
			} catch (Exception e) {
				FacesUtils.addInfoMessage("Error al consultar tareas");

				e.printStackTrace();
				strForward = NavigationResults.FAILURE;
			}

		} else {
			FacesUtils.addInfoMessage("La suscripcion no se encuentra en estatus correcto para asignar tareas");
		}

		return strForward;
	}

	/**
	 * Convierte array[] String a cadena
	 * 
	 * @param array
	 *            arreglo a convertir
	 * @param cement
	 *            separador
	 * @return String concatenado
	 */
	private <T> String join(T[] array, String cement) {
		StringBuilder builder = new StringBuilder();

		if (array == null || array.length == 0) {
			return null;
		}

		for (T t : array) {
			builder.append(t).append(cement);
		}

		builder.delete(builder.length() - cement.length(), builder.length());

		return builder.toString();
	}

	public ServicioSuscripcion getServicioSuscripcion() {
		return servicioSuscripcion;
	}

	public void setServicioSuscripcion(ServicioSuscripcion servicioSuscripcion) {
		this.servicioSuscripcion = servicioSuscripcion;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public List<SuscripcionDTO> getSuscripciones() {
		return suscripciones;
	}

	public void setSuscripciones(List<SuscripcionDTO> suscripciones) {
		this.suscripciones = suscripciones;
	}

	public FiltroSuscripcionDTO getFiltroSuscripcionDTO() {
		return filtroSuscripcionDTO;
	}

	public void setFiltroSuscripcionDTO(FiltroSuscripcionDTO filtroSuscripcionDTO) {
		this.filtroSuscripcionDTO = filtroSuscripcionDTO;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public File getArchivo() {
		return archivo;
	}

	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}

	public ProgressBar getPb() {
		return pb;
	}

	public void setPb(ProgressBar pb) {
		this.pb = pb;
	}

	public long getSelectID() {
		return selectID;
	}

	public void setSelectID(long selectID) {
		this.selectID = selectID;
	}

	public SuscripcionDTO getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(SuscripcionDTO seleccionado) {
		this.seleccionado = seleccionado;
	}

	public SuscripcionDTO getModifica() {
		return modifica;
	}

	public void setModifica(SuscripcionDTO modifica) {
		this.modifica = modifica;
	}

	public String getStrRespuesta() {
		return strRespuesta;
	}

	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}

	/**
	 * @return the grupos
	 */
	public List<GrupoDTO> getGrupos() {
		return grupos;
	}

	/**
	 * @param grupos
	 *            the grupos to set
	 */
	public void setGrupos(List<GrupoDTO> grupos) {
		this.grupos = grupos;
	}

	/**
	 * @return the seguimientos
	 */
	public List<SeguimientoDTO> getSeguimientos() {
		return seguimientos;
	}

	/**
	 * @param seguimientos
	 *            the seguimientos to set
	 */
	public void setSeguimientos(List<SeguimientoDTO> seguimientos) {
		this.seguimientos = seguimientos;
	}

	/**
	 * @return the checking
	 */
	public Boolean getChecking() {
		return checking;
	}

	/**
	 * @param checking
	 *            the checking to set
	 */
	public void setChecking(Boolean checking) {
		this.checking = checking;
	}

	/**
	 * @return the seguimientoActual
	 */
	public SeguimientoDTO getSeguimientoActual() {
		return seguimientoActual;
	}

	/**
	 * @param seguimientoActual
	 *            the seguimientoActual to set
	 */
	public void setSeguimientoActual(SeguimientoDTO seguimientoActual) {
		this.seguimientoActual = seguimientoActual;
	}

	/**
	 * @return the strRespuestaError
	 */
	public String getStrRespuestaError() {
		return strRespuestaError;
	}

	/**
	 * @param strRespuestaError
	 *            the strRespuestaError to set
	 */
	public void setStrRespuestaError(String strRespuestaError) {
		this.strRespuestaError = strRespuestaError;
	}

	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}

	/**
	 * @param numPagina
	 *            the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	/**
	 * @return the archivosSeguimiento
	 */
	public List<AnexoDTO> getArchivosSeguimiento() {
		return archivosSeguimiento;
	}

	/**
	 * @param archivosSeguimiento
	 *            the archivosSeguimiento to set
	 */
	public void setArchivosSeguimiento(List<AnexoDTO> archivosSeguimiento) {
		this.archivosSeguimiento = archivosSeguimiento;
	}

	/**
	 * @return the archivoActual
	 */
	public AnexoDTO getArchivoActual() {
		return archivoActual;
	}

	/**
	 * @param archivoActual
	 *            the archivoActual to set
	 */
	public void setArchivoActual(AnexoDTO archivoActual) {
		this.archivoActual = archivoActual;
	}

	/**
	 * @return the permiso
	 */
	public Boolean getPermiso() {
		return permiso;
	}

	/**
	 * @param permiso
	 *            the permiso to set
	 */
	public void setPermiso(Boolean permiso) {
		this.permiso = permiso;
	}

	/**
	 * @return the bExamMedico
	 */
	public Boolean getbExamMedico() {
		return bExamMedico;
	}

	/**
	 * @param bExamMedico
	 *            the bExamMedico to set
	 */
	public void setbExamMedico(Boolean bExamMedico) {
		this.bExamMedico = bExamMedico;
	}

}
