package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioMonitoreo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;

@Controller
@Scope("session")
public class BeanMonitoreo {
	
	//Servicios
	@Resource
	private ServicioMonitoreo servicioMonitoreo;

	private Log log = LogFactory.getLog(this.getClass());
	
	private List<BeanCertificado> listaCerti;
	private List<Object> listaComboTareas;
	
	//Variables
	private int opcion;
	private String strRespuesta, strTarea;
	private Date fInicio;
	private Date fFin;
	private short inRamo; 
	private Integer inPoliza, inIdVenta;
	private int tarea;
	
	

	// getters and setters
	public ServicioMonitoreo getServicioMonitoreo() {
		return servicioMonitoreo;
	}

	public void setServicioMonitoreo(ServicioMonitoreo servicioMonitoreo) {
		this.servicioMonitoreo = servicioMonitoreo;
	}
	
	public List<BeanCertificado> getListaCerti() {
		return listaCerti;
	}
	public void setListaCerti(List<BeanCertificado> listaCerti) {
		this.listaCerti = listaCerti;
	}
	
	public List<Object> getListaComboTareas() {
		return listaComboTareas;
	}
	
	public void setListaComboTareas(List<Object> listaComboTareas) {
		this.listaComboTareas = listaComboTareas;
	}
	
	public int getOpcion() {
		return opcion;
	}

	public void setOpcion(int opcion) {
		this.opcion = opcion;
	}
	
	public String getStrRespuesta() {
		return strRespuesta;
	}

	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	
	public String getStrTarea() {
		return strTarea;
	}

	public void setStrTarea(String strTarea) {
		this.strTarea = strTarea;
	}

	public Date getfInicio() {
		return fInicio;
	}

	public void setfInicio(Date fInicio) {
		this.fInicio = fInicio;
	}

	public Date getfFin() {
		return fFin;
	}

	public void setfFin(Date fFin) {
		this.fFin = fFin;
	}

	public Short getInRamo() {
		return inRamo;
	}

	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}

	public Integer getInPoliza() {
		return inPoliza;
	}

	public void setInPoliza(Integer inPoliza) {
		this.inPoliza = inPoliza;
	}

	public int getTarea() {
		return tarea;
	}

	public void setTarea(int tarea) {
		this.tarea = tarea;
	}

	public Integer getInIdVenta() {
		return inIdVenta;
	}

	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}

	// m�todos
	public BeanMonitoreo(){
		this.listaCerti = new ArrayList<BeanCertificado>();
		this.listaComboTareas = new ArrayList<Object>();
		strRespuesta = "";
		strTarea = "Resultados";
		opcion = 0;
		inIdVenta = 0;
		tarea = 0;
		inRamo = 0;
		fInicio = null;
		fFin = null;
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_MONITOREO);
	}
	
	public void llenaTarea(){
		try {
			this.listaComboTareas = new ArrayList<Object>();
			tarea = 0;
			inRamo = 0;
			if(opcion == 1 || opcion == 2){
				this.listaComboTareas.add(new SelectItem(1,"Emisi�n"));
				this.listaComboTareas.add(new SelectItem(2,"Cancelaci�n"));
				if(opcion == 2){
					this.listaComboTareas.add(new SelectItem(3,"Conciliaci�n"));
				}
				this.listaComboTareas.add(new SelectItem(4,"Recibos timbrados"));					
				this.listaComboTareas.add(new SelectItem(5,"Endosos por p�liza"));
			} else if(opcion == 3){
				this.listaComboTareas.add(new SelectItem(7,"C�dula de emisi�n"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sacarReportes() {
		BeanCertificado certi;
		this.listaCerti.clear();
		String valida;
		String fechaIni, fechaFin;
		
		try {
			strRespuesta = "";
			strTarea = "";
			valida = validaDatos();
			
			if (valida.equals("0")){
				fechaIni = GestorFechas.formatDate(fInicio, "dd/MM/yyyy");
				fechaFin = GestorFechas.formatDate(fFin, "dd/MM/yyyy");
				if(tarea == 1){
					List<Object[]> polizas = servicioMonitoreo.cargaPolizasEmiCan(inRamo,inPoliza,inIdVenta,fechaIni,fechaFin,opcion,tarea);
					for (Object[] cargar : polizas) {
						certi = new BeanCertificado();
						
						certi.setDesc1("Emitidos");
						certi.setCoceNuMovimiento(((BigDecimal) cargar[0]).longValue());
						certi.setCoceMtPrimaReal(((BigDecimal) cargar[1]));

						listaCerti.add(certi);
					}
					strTarea = "Emisi�n";
				} else if (tarea == 2){
					List<Object[]> polizas = servicioMonitoreo.cargaPolizasEmiCan(inRamo,inPoliza,inIdVenta,fechaIni,fechaFin,opcion,tarea);
					for (Object[] cargar : polizas) {
						certi = new BeanCertificado();
						
						certi.setDesc1("Cancelados");
						certi.setCoceNuMovimiento(((BigDecimal) cargar[0]).longValue());
						certi.setCoceMtPrimaReal(((BigDecimal) cargar[1]));

						listaCerti.add(certi);
					}
					strTarea = "Cancelaci�n";
				} else if (tarea == 3){
					List<Object[]> polizas = servicioMonitoreo.cargaPolizasConcilia(inRamo,inPoliza,fechaIni,fechaFin,opcion,tarea);
					for (Object[] cargar : polizas) {
						certi = new BeanCertificado();
						
						certi.setDesc1((cargar[0]).toString());
						certi.setCoceNuMovimiento(((BigDecimal) cargar[1]).longValue());
						certi.setCoceMtPrimaReal(((BigDecimal) cargar[2]));

						listaCerti.add(certi);
					}
					strTarea = "Conciliaci�n";
				} else if (tarea == 4){
					List<Object[]> polizas = servicioMonitoreo.cargaPolizasTimbrados(inRamo,inPoliza,inIdVenta,fechaIni,fechaFin,opcion,tarea);
					for (Object[] cargar : polizas) {
						certi = new BeanCertificado();
						
						certi.setDesc1((cargar[0]).toString());
						certi.setCoceNuMovimiento(((BigDecimal) cargar[1]).longValue());
						certi.setCoceMtPrimaReal(((BigDecimal) cargar[2]));

						listaCerti.add(certi);
					}
					strTarea = "Recibos Timbrados";
				} else if (tarea == 5){
					List<Object[]> polizas = servicioMonitoreo.cargaPolizasEndosos(inRamo,inPoliza,inIdVenta,fechaIni,fechaFin,opcion,tarea);
					for (Object[] cargar : polizas) {
						certi = new BeanCertificado();
						
						certi.setCoceCampon5(((BigDecimal) cargar[0]));				//Poliza
						certi.setCoceCampov1((cargar[1]).toString()); 	//TipoEndoso
						certi.setCoceCampov2((cargar[2]).toString()); 	//NoEndoso
						if((cargar[3]) == null){
							certi.setCoceCampov3("");
						} else {
							certi.setCoceCampov3(GestorFechas.formatDate((Date) (cargar[3]),"dd/MM/yyyy")); 	//Desde
						}
						if ((cargar[4]) == null) { 
							certi.setCoceCampov4("");
						} else {
							certi.setCoceCampov4(GestorFechas.formatDate((Date) (cargar[4]),"dd/MM/yyyy")); 	//Hasta
						}
						certi.setCoceMtPrimaPura(((BigDecimal) cargar[5])); 		//PrimaNeta
						certi.setCoceCampov5((cargar[6]).toString()); //Derecho
						certi.setCoceCampov6((cargar[7]).toString());	//Recargo
						certi.setCoceCampov7((cargar[8]).toString());	//Impuesto
						certi.setCoceCampov8((cargar[9]).toString());				// Total

						listaCerti.add(certi);
					}
					strTarea = "Endosos por p�liza";
				}  else if (tarea == 7){
					strTarea = "Cedula de Emisi�n";
				}
			} else {
				strRespuesta = valida;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getInit() {
		opcion = 0;
		strRespuesta = strTarea = null;
		fInicio = null;
		fFin = null;
		inRamo = 0; 
		inPoliza = inIdVenta = null;
		tarea = 0;
		return "succes";
	}
	
	private String validaDatos() {
		try {
			if(opcion == 0){ return "Favor de seleccioar un proceso";}
			if(tarea == 0){ return "Favor de seleccionar una tarea";} 
			if(inRamo == 0){ return "Favor de seleccionar un ramo";}
			if(inPoliza == null){ return "Favor de seleccionar una poliza";}
			if(fInicio == null || fFin == null){ return "Debe seleccionar un rango de fechas";}
			if(GestorFechas.comparaFechas(fInicio, fFin) == -1){return "La fecha inicio debe ser menor o igual a la fecha fin";}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Constantes.DEFAULT_STRING_ID;
	}
}
