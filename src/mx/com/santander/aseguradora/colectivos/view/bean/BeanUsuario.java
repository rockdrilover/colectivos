/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Date;
import java.util.Set;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Grupo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Rol;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanUsuario extends BeanBase {

	private String username;
	private String password;
	private boolean enabled;
	private Short sucursal;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String correo;
	private Date fechaRegistro;
	private String campo1;
	private Long campo2;
	private Date campo3;
	private Boolean modulo;
	private Set<Rol> roles;
	private Set<Grupo> grupos;
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}
	/**
	 * @return the sucursal
	 */
	public Short getSucursal() {
		return sucursal;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	/**
	 * @return the campo1
	 */
	public String getCampo1() {
		return campo1;
	}
	/**
	 * @return the campo2
	 */
	public Long getCampo2() {
		return campo2;
	}
	/**
	 * @return the campo3
	 */
	public Date getCampo3() {
		return campo3;
	}
	/**
	 * @return the modulo
	 */
	public Boolean getModulo() {
		return modulo;
	}
	/**
	 * @return the roles
	 */
	public Set<Rol> getRoles() {
		return roles;
	}
	/**
	 * @return the grupos
	 */
	public Set<Grupo> getGrupos() {
		return grupos;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(Short sucursal) {
		this.sucursal = sucursal;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * @param fechaRegistro the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	/**
	 * @param campo1 the campo1 to set
	 */
	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}
	/**
	 * @param campo2 the campo2 to set
	 */
	public void setCampo2(Long campo2) {
		this.campo2 = campo2;
	}
	/**
	 * @param campo3 the campo3 to set
	 */
	public void setCampo3(Date campo3) {
		this.campo3 = campo3;
	}
	/**
	 * @param modulo the modulo to set
	 */
	public void setModulo(Boolean modulo) {
		this.modulo = modulo;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Set<Rol> roles) {
		this.roles = roles;
	}
	/**
	 * @param grupos the grupos to set
	 */
	public void setGrupos(Set<Grupo> grupos) {
		this.grupos = grupos;
	}
}
