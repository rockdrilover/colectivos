package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioHistorialCredito;


@Controller
@Scope("request")
public class BeanListaCreditoDetalle {
	
	@Resource
	private ServicioHistorialCredito servicioHistorialCredito;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private String poliza;
	private String certificado;
	private String credito;
	private String feinivigpol;
	private String fefinvigpol;
	private String feinivigcer;
	private String fefinvigcer;
	private String fechaingreso;
	private String nom_cont;
	private String curp_cont;
	private String tel_cont;
	private String rfc_cont;
	private String dir_cont;
	private String col_cont;
	private String del_cont;
	private String edo_cont;
	private String cp_cont;
	private String nombre_aseg;
	private String curp_aseg;
	private String tel_aseg;
	private String rfc_aseg;
	private String fena_aseg;
	private String dom_aseg;
	private String col_aseg;
	private String del_aseg;
	private String pob_aseg;
	private String edo_aseg;
	private String mun_aseg;
	private String cp_aseg;
	private String suma_aseg;
	private String cobro1;
	private String encabe;
	private String pmaneta;
	private String prima_total;
	private String saldo_base;
	private String forma_pago;
	
	private String dpo;
	private String rfi;
	private String iva;
	private String pma;
	private String pmato;
	private String dom_biaseg;
	private String col_biaseg;
	private String del_biaseg;
	private String pob_biaseg;
	private String edo_biaseg;
	private String mun_biaseg;
	private String cp_biaseg;
	private String cd_sucursal;
	private List<Object> listaHistorial;
	private List<Object> listaHistorialDetalle;
	private String ramo;
	private String nu_cliente;
	private String buc_cliente;
	private String cd_sexo;
	private Date fec_emision;
	private String id_estatus;
	private String fec_carga;
	///////datos credito
	private String estat_tec;
	private String fecha_anu;
	private String producto;
	private String fefincre;
	private String plan;
	private String cober;
	private String cober_desc;
	private String tarif;
	private String motivo_anulacion;
	private String ramo_nom;
	
	
	private List<BeanListaCoberturaDetalle> lstCoberturas;
	private List<BeanDetalleComponentes> lstComponentes;
	private List<BeanListaRecibosHistorico> lstrecibos;
	
	
	public List<BeanListaCoberturaDetalle> getLstCoberturas() {
		return lstCoberturas;
	}
	public void setLstCoberturas(List<BeanListaCoberturaDetalle> lstCoberturas) {
		this.lstCoberturas = lstCoberturas;
	}
	public List<BeanDetalleComponentes> getLstComponentes() {
		return lstComponentes;
	}
	public void setLstComponentes(List<BeanDetalleComponentes> lstComponentes) {
		this.lstComponentes = lstComponentes;
	}
	public List<BeanListaRecibosHistorico> getLstrecibos() {
		return lstrecibos;
	}
	public void setLstrecibos(List<BeanListaRecibosHistorico> lstrecibos) {
		this.lstrecibos = lstrecibos;
	}
	public String getEstat_tec() {
		return estat_tec;
	}
	public String getCober() {
		return cober;
	}
	public void setCober(String cober) {
		this.cober = cober;
	}
	public String getCober_desc() {
		return cober_desc;
	}
	public void setCober_desc(String cober_desc) {
		this.cober_desc = cober_desc;
	}
	public String getTarif() {
		return tarif;
	}
	public void setTarif(String tarif) {
		this.tarif = tarif;
	}
	public void setEstat_tec(String estat_tec) {
		this.estat_tec = estat_tec;
	}
	public String getFecha_anu() {
		return fecha_anu;
	}
	public void setFecha_anu(String fecha_anu) {
		this.fecha_anu = fecha_anu;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getFefincre() {
		return fefincre;
	}
	public void setFefincre(String fefincre) {
		this.fefincre = fefincre;
	}
	public String getId_estatus() {
		return id_estatus;
	}
	public void setId_estatus(String id_estatus) {
		this.id_estatus = id_estatus;
	}
	public Date getFec_emision() {
		return fec_emision;
	}
	public void setFec_emision(Date fec_emision) {
		this.fec_emision = fec_emision;
	}
	public String getFec_carga() {
		return fec_carga;
	}
	public void setFec_carga(String fec_carga) {
		this.fec_carga = fec_carga;
	}
	public String getMotivo_anulacion() {
		return motivo_anulacion;
	}
	public void setMotivo_anulacion(String motivo_anulacion) {
		this.motivo_anulacion = motivo_anulacion;
	}
	public String getRamo_nom() {
		return ramo_nom;
	}
	public void setRamo_nom(String ramo_nom) {
		this.ramo_nom = ramo_nom;
	}
	public String getCd_sucursal() {
		return cd_sucursal;
	}
	public void setCd_sucursal(String cd_sucursal) {
		this.cd_sucursal = cd_sucursal;
	}
	public String getNu_cliente() {
		return nu_cliente;
	}
	public void setNu_cliente(String nu_cliente) {
		this.nu_cliente = nu_cliente;
	}
	public String getBuc_cliente() {
		return buc_cliente;
	}
	public void setBuc_cliente(String buc_cliente) {
		this.buc_cliente = buc_cliente;
	}
	public String getCd_sexo() {
		return cd_sexo;
	}
	public void setCd_sexo(String cd_sexo) {
		this.cd_sexo = cd_sexo;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public String getCredito() {
		return credito;
	}
	public void setCredito(String credito) {
		this.credito = credito;
	}
	public String getFeinivigpol() {
		return feinivigpol;
	}
	public void setFeinivigpol(String feinivigpol) {
		this.feinivigpol = feinivigpol;
	}
	public String getFefinvigpol() {
		return fefinvigpol;
	}
	public void setFefinvigpol(String fefinvigpol) {
		this.fefinvigpol = fefinvigpol;
	}
	public String getFeinivigcer() {
		return feinivigcer;
	}
	public void setFeinivigcer(String feinivigcer) {
		this.feinivigcer = feinivigcer;
	}
	public String getFefinvigcer() {
		return fefinvigcer;
	}
	public void setFefinvigcer(String fefinvigcer) {
		this.fefinvigcer = fefinvigcer;
	}
	public String getFechaingreso() {
		return fechaingreso;
	}
	public void setFechaingreso(String fechaingreso) {
		this.fechaingreso = fechaingreso;
	}
	public String getNom_cont() {
		return nom_cont;
	}
	public void setNom_cont(String nom_cont) {
		this.nom_cont = nom_cont;
	}
	public String getCurp_cont() {
		return curp_cont;
	}
	public void setCurp_cont(String curp_cont) {
		this.curp_cont = curp_cont;
	}
	public String getTel_cont() {
		return tel_cont;
	}
	public void setTel_cont(String tel_cont) {
		this.tel_cont = tel_cont;
	}
	public String getRfc_cont() {
		return rfc_cont;
	}
	public void setRfc_cont(String rfc_cont) {
		this.rfc_cont = rfc_cont;
	}
	public String getDir_cont() {
		return dir_cont;
	}
	public void setDir_cont(String dir_cont) {
		this.dir_cont = dir_cont;
	}
	public String getCol_cont() {
		return col_cont;
	}
	public void setCol_cont(String col_cont) {
		this.col_cont = col_cont;
	}
	public String getDel_cont() {
		return del_cont;
	}
	public void setDel_cont(String del_cont) {
		this.del_cont = del_cont;
	}
	public String getEdo_cont() {
		return edo_cont;
	}
	public void setEdo_cont(String edo_cont) {
		this.edo_cont = edo_cont;
	}
	public String getCp_cont() {
		return cp_cont;
	}
	public void setCp_cont(String cp_cont) {
		this.cp_cont = cp_cont;
	}
	public String getNombre_aseg() {
		return nombre_aseg;
	}
	public void setNombre_aseg(String nombre_aseg) {
		this.nombre_aseg = nombre_aseg;
	}
	public String getCurp_aseg() {
		return curp_aseg;
	}
	public void setCurp_aseg(String curp_aseg) {
		this.curp_aseg = curp_aseg;
	}
	public String getTel_aseg() {
		return tel_aseg;
	}
	public void setTel_aseg(String tel_aseg) {
		this.tel_aseg = tel_aseg;
	}
	public String getRfc_aseg() {
		return rfc_aseg;
	}
	public void setRfc_aseg(String rfc_aseg) {
		this.rfc_aseg = rfc_aseg;
	}
	public String getFena_aseg() {
		return fena_aseg;
	}
	public void setFena_aseg(String fena_aseg) {
		this.fena_aseg = fena_aseg;
	}
	public String getDom_aseg() {
		return dom_aseg;
	}
	public void setDom_aseg(String dom_aseg) {
		this.dom_aseg = dom_aseg;
	}
	public String getCol_aseg() {
		return col_aseg;
	}
	public void setCol_aseg(String col_aseg) {
		this.col_aseg = col_aseg;
	}
	public String getDel_aseg() {
		return del_aseg;
	}
	public void setDel_aseg(String del_aseg) {
		this.del_aseg = del_aseg;
	}
	public String getPob_aseg() {
		return pob_aseg;
	}
	public void setPob_aseg(String pob_aseg) {
		this.pob_aseg = pob_aseg;
	}
	public String getEdo_aseg() {
		return edo_aseg;
	}
	public void setEdo_aseg(String edo_aseg) {
		this.edo_aseg = edo_aseg;
	}
	public String getMun_aseg() {
		return mun_aseg;
	}
	public void setMun_aseg(String mun_aseg) {
		this.mun_aseg = mun_aseg;
	}
	public String getCp_aseg() {
		return cp_aseg;
	}
	public void setCp_aseg(String cp_aseg) {
		this.cp_aseg = cp_aseg;
	}
	public String getCobro1() {
		return cobro1;
	}
	public void setCobro1(String cobro1) {
		this.cobro1 = cobro1;
	}
	public String getPmaneta() {
		return pmaneta;
	}
	public void setPmaneta(String pmaneta) {
		this.pmaneta = pmaneta;
	}
	public String getDpo() {
		return dpo;
	}
	public void setDpo(String dpo) {
		this.dpo = dpo;
	}
	public String getRfi() {
		return rfi;
	}
	public void setRfi(String rfi) {
		this.rfi = rfi;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public String getPma() {
		return pma;
	}
	public void setPma(String pma) {
		this.pma = pma;
	}
	public String getPmato() {
		return pmato;
	}
	public void setPmato(String pmato) {
		this.pmato = pmato;
	}
	public String getDom_biaseg() {
		return dom_biaseg;
	}
	public void setDom_biaseg(String dom_biaseg) {
		this.dom_biaseg = dom_biaseg;
	}
	public String getCol_biaseg() {
		return col_biaseg;
	}
	public void setCol_biaseg(String col_biaseg) {
		this.col_biaseg = col_biaseg;
	}
	public String getDel_biaseg() {
		return del_biaseg;
	}
	public void setDel_biaseg(String del_biaseg) {
		this.del_biaseg = del_biaseg;
	}
	public String getPob_biaseg() {
		return pob_biaseg;
	}
	public void setPob_biaseg(String pob_biaseg) {
		this.pob_biaseg = pob_biaseg;
	}
	public String getEdo_biaseg() {
		return edo_biaseg;
	}
	public void setEdo_biaseg(String edo_biaseg) {
		this.edo_biaseg = edo_biaseg;
	}
	public String getMun_biaseg() {
		return mun_biaseg;
	}
	public void setMun_biaseg(String mun_biaseg) {
		this.mun_biaseg = mun_biaseg;
	}
	public String getCp_biaseg() {
		return cp_biaseg;
	}
	public void setCp_biaseg(String cp_biaseg) {
		this.cp_biaseg = cp_biaseg;
	}
	
	public String getSuma_aseg() {
		return suma_aseg;
	}
	public void setSuma_aseg(String suma_aseg) {
		this.suma_aseg = suma_aseg;
	}
	public String getPrima_total() {
		return prima_total;
	}
	public void setPrima_total(String prima_total) {
		this.prima_total = prima_total;
	}
	public String getSaldo_base() {
		return saldo_base;
	}
	public void setSaldo_base(String saldo_base) {
		this.saldo_base = saldo_base;
	}
	public String getForma_pago() {
		return forma_pago;
	}
	public void setForma_pago(String forma_pago) {
		this.forma_pago = forma_pago;
	}
	public ServicioHistorialCredito getServicioHistorialCredito() {
		return servicioHistorialCredito;
	}
	public void setServicioHistorialCredito(ServicioHistorialCredito servicioHistorialCredito) {
		this.servicioHistorialCredito = servicioHistorialCredito;
	}
	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	public List<Object> getListaHistorial() {
		return listaHistorial;
	}
	public void setListaHistorial(List<Object> listaHistorial) {
		this.listaHistorial = listaHistorial;
	}
	public List<Object> getListaHistorialDetalle() {
		return listaHistorialDetalle;
	}
	public void setListaHistorialDetalle(List<Object> listaHistorialDetalle) {
		this.listaHistorialDetalle = listaHistorialDetalle;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getEncabe() {
		return encabe;
	}
	public void setEncabe(String encabe) {
		this.encabe = encabe;
	}
	
}
