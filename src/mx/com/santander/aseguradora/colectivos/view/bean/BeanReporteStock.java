package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioReporteStock;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.component.html.HtmlProgressBar;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanReporteStock {

	@Resource
	private ServicioReporteStock servicioReporteStock;
	private Log log = LogFactory.getLog(this.getClass());
	private String mensaje;
	private Short ramo;
	private Long poliza;
	private Integer inIdVenta;
	private Date fecini;
	private Date fecfin;
	private String campos1[] = new String[10];
	private String campos2[] = new String[10];
	private String campos3[] = new String[10];
	private String campos4[] = new String[10];
	private String campos5[] = new String[10];
	private List<Object> listaCampos1;
	private List<Object> listaCampos2;
	private List<Object> listaCampos3;
	private List<Object> listaCampos4;
	private List<Object> listaCampos5;
	private List<Object> lstColumnas;
	private List<Object> lstResultados;
	private ProgressBar pb;
	private ExecutorService pool;
	
	public BeanReporteStock() {
		setListaCampos1(new ArrayList<Object>());
		setListaCampos2(new ArrayList<Object>());
		setListaCampos3(new ArrayList<Object>());
		setListaCampos4(new ArrayList<Object>());
		setListaCampos5(new ArrayList<Object>());
		this.inIdVenta = 0;
//		this.rutaReportes = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		this.pool = Executors.newFixedThreadPool(1);
		setCampos();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_REPORTE_STOCK);
	}
	
	
	
	private void setCampos() {
		listaCampos1.clear();
		listaCampos1.add(new SelectItem("1-RAMO", "Ramo"));
		listaCampos1.add(new SelectItem("2-POLIZA", "Poliza"));
		listaCampos1.add(new SelectItem("3-CREDITO", "Credito"));
		listaCampos1.add(new SelectItem("4-CREDITONUEVO", "Credito Nuevo"));
		listaCampos1.add(new SelectItem("5-CERTIFICADO", "Certificado"));
		listaCampos1.add(new SelectItem("6-RECIBO", "Recibo"));
		listaCampos1.add(new SelectItem("7-PRODUCTO", "Producto"));				
		listaCampos1.add(new SelectItem("8-SUBPRODUCTO", "Subproducto"));
		listaCampos1.add(new SelectItem("9-ESTATUS", "Estatus"));
		listaCampos1.add(new SelectItem("10-ESTATUS_MOVIMIENTO", "Estatus Movimiento"));
		
		listaCampos2.clear();
		listaCampos2.add(new SelectItem("11-SUCURSAL", "Sucursal"));
		listaCampos2.add(new SelectItem("12-CENTRO_COSTOS", "Centro Costos"));
		listaCampos2.add(new SelectItem("13-ID_CUOTA", "Identificacion de Cuota"));
		listaCampos2.add(new SelectItem("14-PLAZO", "Plazo"));
		listaCampos2.add(new SelectItem("15-NUMERO_CLIENTE", "Numero Cliente"));
		listaCampos2.add(new SelectItem("16-NOMBRE", "Nombre"));
		listaCampos2.add(new SelectItem("17-CUENTA", "Cuenta"));
		listaCampos2.add(new SelectItem("18-FECHA_NACIMIENTO", "Fecha Nacimiento"));
		listaCampos2.add(new SelectItem("19-RFC", "RFC"));
		
		
		listaCampos3.clear();
		listaCampos3.add(new SelectItem("20-SEXO", "Sexo"));
		listaCampos3.add(new SelectItem("21-CP", "Codigo Postal"));
		listaCampos3.add(new SelectItem("22-MUNICIPIO", "Municipio"));
		listaCampos3.add(new SelectItem("23-ESTADO", "Estado"));
		listaCampos3.add(new SelectItem("24-TARIFA", "Tarifa"));
		listaCampos3.add(new SelectItem("25-PRIMA_NETA", "Prima Neta"));
		listaCampos3.add(new SelectItem("26-PRIMA_TOTAL", "Prima Total"));
		listaCampos3.add(new SelectItem("27-PRIMA_VIDA", "Prima Vida"));
		listaCampos3.add(new SelectItem("28-PRIMA_DESEMPLEO", "Prima Desempleo"));
		
		listaCampos4.clear();	
		listaCampos4.add(new SelectItem("29-DERECHOS", "Derechos"));
		listaCampos4.add(new SelectItem("30-RECARGOS", "Recargos"));
		listaCampos4.add(new SelectItem("31-IVA", "IVA"));
		listaCampos4.add(new SelectItem("32-SUMA_ASEGURADA", "Suma Asegurada"));
		listaCampos4.add(new SelectItem("33-BASE_CALCULO", "Base Calculo"));
		listaCampos4.add(new SelectItem("34-MONTO_DEVOLUCION", "Monto Devolucion"));
		listaCampos4.add(new SelectItem("35-MONTO_DEVOLUCION_SIS", "Monto Devolucion Sistema"));
		listaCampos4.add(new SelectItem("36-FECHA_INGRESO", "Fecha Ingreso"));
		listaCampos4.add(new SelectItem("37-FECHA_ANULACION", "Fecha Anulacion"));
		
		listaCampos5.clear();
		listaCampos5.add(new SelectItem("38-FECHA_CANCELACION", "Fecha Cancelacion"));
		listaCampos5.add(new SelectItem("39-FECHA_INICIO_POLIZA", "Fecha Inicio Poliza"));
		listaCampos5.add(new SelectItem("40-FECHA_FIN_POLIZA", "Fecha Fin Poliza"));
		listaCampos5.add(new SelectItem("41-FECHA_DESDE", "Fecha Desde"));
		listaCampos5.add(new SelectItem("42-FECHA_HASTA", "Fecha Hasta"));
		listaCampos5.add(new SelectItem("43-FECHA_INICIO_CREDITO", "Fecha Inicio Credito"));
		listaCampos5.add(new SelectItem("44-FECHA_FIN_CREDITO", "Fecha Fin Credito"));
		listaCampos5.add(new SelectItem("45-FECHA_FIN_CREDITO_2", "Fecha Fin Credito 2"));
		
	}
	
	public String limpiar(){
		try {
			new BeanReporteStock();
			setRamo(new Short("-1"));
			setPoliza(-1L);
			setInIdVenta(-1);
			setCampos1(new String[10]);
			setCampos2(new String[10]);
			setCampos3(new String[10]);
			setFecini(null);
			setFecfin(null);
			setListaCampos1(new ArrayList<Object>());
			setListaCampos2(new ArrayList<Object>());
			setListaCampos3(new ArrayList<Object>());
			return null;	
		} catch (Exception e) {
			e.printStackTrace();			
			return null;
		}
		
	}
	
	public String generaReporte(){
		Object arrCampos[];
		
		try {
			if(validarDatos()){				
				arrCampos = getCamposColumnas();
				lstColumnas = (List<Object>) arrCampos[1];
				lstResultados = servicioReporteStock.consultarRegistros(getRamo(), getPoliza(), getInIdVenta(), getFecfin(), getFecini(), arrCampos[0].toString());
				descargar();
				setMensaje("Reporte se Genero Satisfactoriamente.");
			} 
			
			return null;	
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje("Error al consultar informacion.");
			log.error(getMensaje(), e);
			return null;
		}
		
	}
	
	public void descargar(){
		String strNombre = null, strRutaTemp;
		Iterator<Object> it;
		int contador = 0;
		Archivo objArchivo;
		
		try {
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes") + File.separator;
			String arrColumnas[] = new String[getLstColumnas().size()];
			it = getLstColumnas().iterator();
			while(it.hasNext()){
				arrColumnas[contador] = it.next().toString();
				contador++;
			}
			strNombre = "Reporte Stock - " + GestorFechas.formatDate(new Date(), "ddMMyyyy"); 
			objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
			objArchivo.copiarArchivo(arrColumnas, getLstResultados(), "|");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
			objArchivo.eliminarArchivo();
			
			
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje("Error al descargar informacion.");
			log.error(getMensaje(), e);
		}
	}
	
	private boolean validarDatos() {
		boolean blnResultado = true;
		
		if (getRamo() == -1 && getPoliza() == -1 && getInIdVenta() == -1 && getFecini() == null && getFecfin() == null){
			setMensaje("Favor de ingresar un filtro de busqueda");
			blnResultado = false;
		} 
		
		if(getFecini() != null  && getFecfin() == null){
			setMensaje("Favor de ingresar fecha fin");
			blnResultado = false;
		} 
		
		if(getFecini() == null && getFecfin() != null){
			setMensaje("Favor de ingresar fecha inicio");
			blnResultado = false;
		}
		
		if(getFecini() != null && getFecfin() != null) {
			if(getFecini().after(getFecfin())){
				setMensaje("Fecha Inicio no debe ser mayor a Fecha Fin");
				blnResultado = false;
			}
		}
		
		if(getCampos1().length == 0 && getCampos2().length == 0 && getCampos3().length == 0){
			setMensaje("Seleccionar al menos un campo para poder hacer la consulta");
			blnResultado = false;
		}
		
		return blnResultado;
	}
	
	private Object[] getCamposColumnas() throws Exception{
		Object arrResultado[] = new Object[2];
		String strCampos = "tabla.";
		ArrayList<String> arlNombreCol;
		int totalCampos, contCampos = 0;
		
		try {
			arlNombreCol = new ArrayList<String>();
			totalCampos = getCampos1().length + getCampos2().length + getCampos3().length + getCampos4().length + getCampos5().length;
			
			for(int i = 0; getCampos1().length > i; i++){
				contCampos++;
				if(totalCampos == contCampos){
					strCampos = strCampos + Utilerias.separCampos(getCampos1()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos1()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos1()[i].split("-")[1]);
			}
			
			for(int i = 0; getCampos2().length > i; i++){
				contCampos++;
				if(totalCampos == contCampos){
					strCampos = strCampos + Utilerias.separCampos(getCampos2()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos2()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos2()[i].split("-")[1]);
			}
			
			for(int i = 0; getCampos3().length > i; i++){
				contCampos++;
				if(totalCampos == contCampos){
					strCampos = strCampos + Utilerias.separCampos(getCampos3()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos3()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos3()[i].split("-")[1]);
			}
			
			for(int i = 0; getCampos4().length > i; i++){
				contCampos++;
				if(totalCampos == contCampos){
					strCampos = strCampos + Utilerias.separCampos(getCampos4()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos4()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos4()[i].split("-")[1]);
			}
			
			for(int i = 0; getCampos5().length > i; i++){
				contCampos++;
				if(totalCampos == contCampos){
					strCampos = strCampos + Utilerias.separCampos(getCampos5()[i].split("-")[0]);
				} else {
					strCampos = strCampos + Utilerias.separCampos(getCampos5()[i].split("-")[0]) + ", tabla.";
				}
				arlNombreCol.add(getCampos5()[i].split("-")[1]);
			}
			
			
			arrResultado[0] = strCampos;
			arrResultado[1] = arlNombreCol;
		} catch (Exception e) {
			throw new Exception("BeanReporteStock.separarCampos::: " + e.getMessage());
		}
		
		return arrResultado;
	}
	
	public ServicioReporteStock getServicioReporteStock() {
		return servicioReporteStock;
	}
	public void setServicioReporteStock(ServicioReporteStock servicioReporteStock) {
		this.servicioReporteStock = servicioReporteStock;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getMensaje() {
		return mensaje;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	public Long getPoliza() {
		return poliza;
	}
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	public Integer getInIdVenta() {
		return inIdVenta;
	}
	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}
	public Date getFecini() {
		return fecini;
	}
	public void setFecini(Date fecini) {
		this.fecini = fecini;
	}
	public Date getFecfin() {
		return fecfin;
	}
	public void setFecfin(Date fecfin) {
		this.fecfin = fecfin;
	}
	public String[] getCampos1() {
		return campos1;
	}
	public void setCampos1(String[] campos1) {
		this.campos1 = campos1;
	}
	public String[] getCampos2() {
		return campos2;
	}
	public void setCampos2(String[] campos2) {
		this.campos2 = campos2;
	}
	public String[] getCampos3() {
		return campos3;
	}
	public void setCampos3(String[] campos3) {
		this.campos3 = campos3;
	}
	public String[] getCampos4() {
		return campos4;
	}
	public void setCampos4(String[] campos4) {
		this.campos4 = campos4;
	}
	public String[] getCampos5() {
		return campos5;
	}
	public void setCampos5(String[] campos5) {
		this.campos5 = campos5;
	}
	public List<Object> getListaCampos1() {
		return listaCampos1;
	}
	public void setListaCampos1(List<Object> listaCampos1) {
		this.listaCampos1 = listaCampos1;
	}
	public List<Object> getListaCampos2() {
		return listaCampos2;
	}
	public void setListaCampos2(List<Object> listaCampos2) {
		this.listaCampos2 = listaCampos2;
	}
	public List<Object> getListaCampos3() {
		return listaCampos3;
	}
	public void setListaCampos3(List<Object> listaCampos3) {
		this.listaCampos3 = listaCampos3;
	}
	public List<Object> getListaCampos4() {
		return listaCampos4;
	}
	public void setListaCampos4(List<Object> listaCampos4) {
		this.listaCampos4 = listaCampos4;
	}
	public List<Object> getListaCampos5() {
		return listaCampos5;
	}
	public void setListaCampos5(List<Object> listaCampos5) {
		this.listaCampos5 = listaCampos5;
	}
	public List<Object> getLstColumnas() {
		return lstColumnas;
	}
	public void setLstColumnas(List<Object> lstColumnas) {
		this.lstColumnas = lstColumnas;
	}
	public List<Object> getLstResultados() {
		return lstResultados;
	}
	public void setLstResultados(List<Object> lstResultados) {
		this.lstResultados = lstResultados;
	}
	public Integer getProgressValue() {
		return pb.getProgressValue();
	}
	public void setProgressValue(Integer progressValue) {
		this.pb.setProgressValue(progressValue);
	}
	public Boolean getProgressHabilitar() {
		return pb.isProgressHabilitar();
	}
	public void setProgressHabilitar(Boolean progressHabilitar) {
		this.pb.setProgressHabilitar(progressHabilitar);
	}
	public HtmlProgressBar getProgressBar() {
		return pb.getProgressBar();
	}
	public void setProgressBar(HtmlProgressBar progressBar) {
		this.pb.setProgressBar(progressBar);
	}
	
}
