package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Tyler
 *
 */
@Controller
@Scope("session")	
public class BeanChimuelos {

	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioCarga servicioCarga;
	private Log log = LogFactory.getLog(this.getClass());
	private Short canal;
	private Short ramo;
	private Long  poliza;
	
	private List<String> opciones;
	
	private StringBuilder message;
	private String respuesta;
	private boolean chimuelos;
	
	
	public BeanChimuelos() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.opciones = new ArrayList<String>();
		this.opciones.clear();
		this.opciones.add("1");
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CHIMUELOS);
	}
	
	/**
	 * @param 
	 * @return
	 */
	public String reporteChimuelos(){	
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		this.canal  = 1;	
		
		if(this.canal != null && this.ramo != null && this.poliza != null){
			
		}
		try {
			inParams.put("CANAL",  this.canal.intValue());
			inParams.put("RAMO",   this.ramo.intValue());
			inParams.put("POLIZA", this.poliza.intValue());
			
			rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ReporteChimuelos.csv");
			if(this.poliza == 57 || this.poliza == 58){
				rutaReporte	= FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ReporteChimuelos5758.jrxml");
			} else{
				rutaReporte	= FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ReporteChimuelos.jrxml");
			}

			this.servicioCarga.reporteErrores(rutaTemporal, rutaReporte, inParams);
						
			File file = new File(rutaTemporal);
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			if(!context.getResponseComplete()){
			
				input = new FileInputStream(file);
				bytes = new byte[1000];
				
				String contentType = "application/vnd.ms-excel";

				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				
				response.setContentType(contentType);
				
				DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
		    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());
				
				ServletOutputStream out = response.getOutputStream();
				
				while((read = input.read(bytes))!= -1){
					
					out.write(bytes, 0 , read);
				}
				
				input.close();
				out.flush();
				out.close();
				//file.delete();

				context.responseComplete();
			}

		this.message.append("Archivo descargado.");
		this.log.debug(message.toString());
		FacesUtils.addInfoMessage(message.toString());
		Utilerias.resetMessage(message);

		
		return NavigationResults.SUCCESS;
		
	} catch (Exception e) {
		// TODO: handle exception
		this.message.append("Error al generar el archivo.");
		this.log.debug(message.toString());
		FacesUtils.addErrorMessage(message.toString());
		
		return NavigationResults.FAILURE;
	}			
}

	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}

	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}

	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}

	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}
	
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public Short getRamo() {
		return ramo;
	}

	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}

	public Long getPoliza() {
		return poliza;
	}

	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}

	public void setChimuelos(boolean chimuelos) {
		this.chimuelos = chimuelos;
	}

	public boolean isChimuelos() {
		return chimuelos;
	}
	
}
