/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Date;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CgRefCodes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosRecibo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosReciboId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;




/**
 * @author IBB
 * 
 *
 */
@Controller
@Scope("request")
public class BeanReciboCol {
	
	@Resource
	private ServicioRecibo servicioRecibo;
	private Log log = LogFactory.getLog(this.getClass());
	@SuppressWarnings("unused")
	private StringBuilder message;
	private ColectivosReciboId id;
	private CgRefCodes estatus;
	private ColectivosRecibo recibo;
	private Integer coreCapoNuPoliza;
	private Short coreCarpCdRamo;
	private String coreStRecibo;
	private Date coreFeEmision;
	private Short primaUnica;
	private BigDecimal reciboAnterior;
	private boolean endosar;
	private String  mensajeEndoso;
	private String  nombre = "";
	
	
	public ServicioRecibo getServicioRecibo() {
		return servicioRecibo;
	}
	public void setServicioRecibo(ServicioRecibo servicioRecibo) {
		this.servicioRecibo = servicioRecibo;
	}
	public ColectivosReciboId getId() {
		return id;
	}
	public void setId(ColectivosReciboId id) {
		this.id = id;
	}
	
	public CgRefCodes getEstatus() {
		return estatus;
	}
	public void setEstatus(CgRefCodes estatus) {
		this.estatus = estatus;
	}
	
	public ColectivosRecibo getRecibo() {
		return recibo;
	}
	public void setRecibo(ColectivosRecibo recibo) {
		this.recibo = recibo;
	}

	public int getCoreCapoNuPoliza() {
		return this.coreCapoNuPoliza;
	}

	public void setCoreCapoNuPoliza(int coreCapoNuPoliza) {
		this.coreCapoNuPoliza = coreCapoNuPoliza;
	}
	
	public Short getCoreCarpCdRamo() {
		return this.coreCarpCdRamo;
	}

	public void setCoreCarpCdRamo(Short coreCarpCdRamo) {
		this.coreCarpCdRamo = coreCarpCdRamo;
	}
	
	public String getCoreStRecibo() {
		return this.coreStRecibo;
	}

	public void setCoreStRecibo(String coreStRecibo) {
		this.coreStRecibo = coreStRecibo;
	}
	
	public Date getCoreFeEmision() {
		return this.coreFeEmision;
	}

	public void setCoreFeEmision(Date coreFeEmision) {
		this.coreFeEmision = coreFeEmision;
	}
	
	public void setPrimaUnica(Short primaUnica) {
		this.primaUnica = primaUnica;
	}
	public Short getPrimaUnica() {
		return primaUnica;
	}
	
	public BigDecimal getReciboAnterior() {
		return reciboAnterior;
	}
	public void setReciboAnterior(BigDecimal reciboAnterior) {
		this.reciboAnterior = reciboAnterior;
	}
		
	public boolean isEndosar() {
		return endosar;
	}
	public void setEndosar(boolean endosar) {
		this.endosar = endosar;
	}
	
	public String getMensajeEndoso() {
		return mensajeEndoso;
	}
	public void setMensajeEndoso(String mensajeEndoso) {
		this.mensajeEndoso = mensajeEndoso;
	}
	
	public BeanReciboCol() {
		this.message = new StringBuilder();
		setId(new ColectivosReciboId());
		setEstatus(new CgRefCodes());
	}

   public void imprimeRecibo(){
		
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		nombre = "ReciboPagoPrimas_"+id.getcoreNuRecibo().toString().replace('.', '_');
		this.servicioRecibo.impresionRecibo(rutaTemporal, nombre
				, id.getcoreCasuCdSucursal(), coreCarpCdRamo
				, coreCapoNuPoliza, id.getcoreNuRecibo(),this.primaUnica);	
		
		int read = 0;
		byte[] bytes = new byte[1024];
	    
		
		try{
			HttpServletResponse response = FacesUtils.getServletResponse();
			File f = new File (rutaTemporal + nombre + ".pdf");
		    InputStream in = new FileInputStream (f);		    
		    OutputStream out = response.getOutputStream ();
		       
		    response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + nombre + ".pdf" + "\""); 

			while((read = in.read(bytes)) != -1){
				out.write(bytes,0,read);
			}

			in.close();
		
			out.flush();
			out.close();
			
			f.delete();
			
			} catch (Exception e) {
				log.error("Error al enviar reporte de PreFactura-Asistencias",e);
				FacesUtils.addInfoMessage("Error al generar el reporte");
			}
			
			FacesContext.getCurrentInstance().responseComplete();		
	}

   public void imprimeRecibo2(){
		
	   this.id.setcoreNuRecibo(this.reciboAnterior);
	   
		this.imprimeRecibo();	
	}
	
}
