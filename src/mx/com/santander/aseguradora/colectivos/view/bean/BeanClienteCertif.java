/**
 * Bean Controlador de detalle de certificados
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import jcifs.smb.SmbFile;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Complementarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.SmbUtil;
import mx.com.santander.aseguradora.colectivos.view.dto.BeneficiariosDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;


/**
 * @author z014058
 * Modificacion : Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanClienteCertif {
	
	@Resource
	private ServicioClienteCertif servicioCliente;
	private StringBuilder message;
	
	private Log log = LogFactory.getLog(this.getClass());
	
	private ClienteCertifId id;
	private Cliente cliente;
	private Certificado certificado;
	private Complementarios complemento; 
	
	private ArrayList<Object> lstCoberturas;
	private ArrayList<Object> lstAsistencias;
	private ArrayList<Object> lstAsegurados;
	private ArrayList<BeneficiariosDTO> lstBeneficiarios;
	private ArrayList<EndosoDatosDTO> lstEndosos;
	
	private String observaciones2="";
	/**
	 * Metodo constructor de la clase
	 */
	public BeanClienteCertif() {
		this.message = new StringBuilder();
		setLstAsistencias(new ArrayList<Object>());
		setLstCoberturas(new ArrayList<Object>());
		setLstAsegurados(new ArrayList<Object>());
		setComplemento(new Complementarios());
		setLstBeneficiarios(new ArrayList<BeneficiariosDTO>());
		setLstEndosos(new ArrayList<EndosoDatosDTO>());
	}
	
	public ArrayList<Object> getLstCoberturas() {
		return lstCoberturas;
	}
	public void setLstCoberturas(ArrayList<Object> lstCoberturas) {
		this.lstCoberturas = lstCoberturas;
	}
	public ArrayList<Object> getLstAsistencias() {
		return lstAsistencias;
	}
	public void setLstAsistencias(ArrayList<Object> lstAsistencias) {
		this.lstAsistencias = lstAsistencias;
	}
	public ArrayList<Object> getLstAsegurados() {
		return lstAsegurados;
	}
	public void setLstAsegurados(ArrayList<Object> lstAsegurados) {
		this.lstAsegurados = lstAsegurados;
	}

	private boolean check;
	private boolean checkE;
	private Integer radioE;
	
	/**
	 * @return the servicioCliente
	 */
	public ServicioClienteCertif getServicioCliente() {
		return servicioCliente;
	}
	/**
	 * @param servicioCliente the servicioCliente to set
	 */
	public void setServicioCliente(ServicioClienteCertif servicioCliente) {
		this.servicioCliente = servicioCliente;
	}

	/**
	 * @return the id
	 */
	public ClienteCertifId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ClienteCertifId id) {
		this.id = id;
	}
	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}
	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	/**
	 * @return the certificado
	 */
	public Certificado getCertificado() {
		return certificado;
	}
	/**
	 * @param certificado the certificado to set
	 */
	 
	public void setCertificado(Certificado certificado) {
		this.certificado = certificado;
	}
		
	/**
	 * @return the complemento
	 */
	public Complementarios getComplemento() {
		return complemento;
	}
	/**
	 * @param complemento the complemento to set
	 */
	public void setComplemento(Complementarios complemento) {
		this.complemento = complemento;
	}
	/**
	 * @return the check
	 */
	public boolean isCheck() {
		return check;
	}
	
	/**
	 * @param check the check to set
	 */
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	public Integer getNumAseguradosAd (){
		if (this.lstAsegurados != null) return this.lstAsegurados.size();
		else return 0;
	}
	
	public String getObservaciones2() {
		return observaciones2;
	}
	public void setObservaciones2(String observaciones2) {
		this.observaciones2 = observaciones2;
	}
	
	// modifico dfg 
	@Override
	public String toString() {
		try{
			return  String.format("%s", this.getCertificado().getId().toString());
		}catch (Exception e){
			e.printStackTrace();
			return "";
		}
	}
	
	// agregado dfg , impresion poliza
	public void impresionPoliza() {	
		String ruta = "";
		String reporte = "";
		Archivo objArchivo;
		
		try {
			objArchivo = new Archivo();
			if(this.getCertificado().getId() != null){
				ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator; //GetRutaPlantilla
				reporte = servicioCliente.impresionPoliza(ruta, this.getCertificado().getId().getCoceCasuCdSucursal()
																   , this.getCertificado().getId().getCoceCarpCdRamo()
																   , this.getCertificado().getId().getCoceCapoNuPoliza()
																   , this.getCertificado().getId().getCoceNuCertificado()
																   , this.getCertificado().getPlanes().getProducto().getId().getAlprCdProducto()
																   , this.getCertificado().getCoceFeEmision());
				if(reporte != null ){
					objArchivo = new Archivo(ruta, FilenameUtils.getBaseName(reporte), "." + FilenameUtils.getExtension(reporte));
					objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 4);
					objArchivo.eliminarArchivo();
					FacesContext.getCurrentInstance().responseComplete();
				}
			} else {
				// reporte = "No existe reporte para este producto"; 	
				reporte = null;
			}
		} catch (Exception e) {
			log.error("Error al generar la impresion de poliza.",e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
		} finally {
			if(reporte == null) {
			   reporte = "No existe reporte para este producto";		
			   log.info (reporte); 
		       this.message.append(reporte);
			}else{
				log.info ("Archivo generado correctamente"); 
			    this.message.append("Archivo generado correctamente");	
			}
		}
	}
	/**
	 * Metodo de obtencion de reporte de certificado
	 */
	public void getReportePlatilla() {	
		String rutaPlatillas = "";
		String strNombre;
		String strRespuesta = "";
		String rutaApp = "";
		String strNomrePlantilla = "";
		Archivo objArchivo;
		Object objBean;
		boolean blnDescarga = false;
		Parametros smbRuta;
		SmbUtil smbUtil;
		SmbFile[] arrFiles = null;
		SmbFile objDir;
		String polPU = "";
		Long polizaReal = 0L;
		
		try {
			polPU = this.getCertificado().getCoceSubCampana();
			if(this.getCertificado().getId().getCoceCarpCdRamo() == 57 || this.getCertificado().getId().getCoceCarpCdRamo() == 58){
				polizaReal = Long.parseLong(this.getCertificado().getId().getCoceCarpCdRamo().toString());
			} else {
				if(polPU.equals("5") || polPU.equals("6") || polPU.equals("7") || polPU.equals("8") || polPU.equals("9")){
					polPU = polPU 
							+ "" + servicioCliente.getPlazo(this.getCertificado().getId().getCoceCarpCdRamo(), this.getCertificado().getPlanes().getProducto().getId().getAlprCdProducto(), this.getCertificado().getPlanes().getId().getAlplCdPlan()) 
							+ "" + this.getCertificado().getPlanes().getProducto().getId().getAlprCdProducto();
					polizaReal = Long.parseLong(polPU);
				} else {
					polizaReal = this.getCertificado().getId().getCoceCapoNuPoliza();
				}
			}
			
			strNombre = this.getCertificado().getId().getCoceCasuCdSucursal().toString() + "_" + this.getCertificado().getId().getCoceCarpCdRamo().toString() + "_" + this.getCertificado().getId().getCoceCapoNuPoliza().toString() + "_" + this.getCertificado().getId().getCoceNuCertificado().toString();
			rutaApp = FacesUtils.getServletContext().getRealPath("/WEB-INF/plantillas/") + File.separator;
			smbRuta =  servicioCliente.getRutaPlantilla();
			strNomrePlantilla = servicioCliente.getPlantilla(this.getCertificado().getId().getCoceCasuCdSucursal()
														   , this.getCertificado().getId().getCoceCarpCdRamo()
														   , polizaReal
														   , this.getCertificado().getId().getCoceNuCertificado()
														   , this.getCertificado().getPlanes().getProducto().getId().getAlprCdProducto()
														   , this.getCertificado().getPlanes().getId().getAlplCdPlan()
														   , this.getCertificado().getCoceFeEmision());
			
			smbUtil = new SmbUtil(smbRuta.getCopaVvalor6(), smbRuta.getCopaVvalor7());
			rutaPlatillas = smbRuta.getCopaRegistro();
			objDir = smbUtil.getFile(rutaPlatillas);
			
			arrFiles = objDir.listFiles();
			if(arrFiles != null) {
				for(SmbFile objFile : arrFiles) {
					if(!objFile.isDirectory()) {
						if(objFile.getName().equals(strNomrePlantilla)){
							log.debug(":::Copiando Archivo a PathApp::: " + new Date() + " ::" + objFile.getName());
							Archivo objArchivoSmb = new Archivo(smbUtil.getFile(rutaPlatillas + objFile.getName()));
							objArchivoSmb.copiarSmbArchivo2(new File(rutaApp + objFile.getName())); //copiar a carpeta de aplicativo
							log.debug(":::FIN Copiando Archivo a PathApp::: " + new Date() + " ::" + objFile.getName());
							strRespuesta = "Si hay plantilla en ruta";
							break;
						}
					}
				}
			}
			
			if(!strRespuesta.equals(Constantes.DEFAULT_STRING_ID)) {
				strRespuesta = servicioCliente.generaCertificado(rutaApp, this.getCertificado().getId().getCoceCasuCdSucursal(), this.getCertificado().getId().getCoceCarpCdRamo(),
														this.getCertificado().getId().getCoceCapoNuPoliza(), this.getCertificado().getId().getCoceNuCertificado(),
														this.getCertificado().getPlanes().getProducto().getId().getAlprCdProducto(), 
														this.getCertificado().getPlanes().getId().getAlplCdPlan() , this.getCertificado().getCoceFeEmision(), rutaApp + strNombre + ".pdf", 
														strNomrePlantilla, polizaReal);
			}				
			
			objBean = FacesUtils.getManagedBean("beanListaClienteCertif");
			if(strRespuesta.equals(Constantes.DEFAULT_STRING_ID)) {
				rutaApp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
				strNombre = impresionCertificado(rutaApp);
				if(strNombre == null) {
					log.debug("No existe Plantilla para este Certificado");
					((BeanListaClienteCertif) objBean).setRespuesta("No existe Plantilla para este Certificado");
				} else {
					blnDescarga = true;	
				}
			} else {	
				blnDescarga = true;
			}
			
			if(blnDescarga) {
				objArchivo = new Archivo(rutaApp, strNombre, ".pdf");
				objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 4);
				objArchivo.eliminarArchivo();
				FacesContext.getCurrentInstance().responseComplete();
				log.debug("Se genero con exito el Certificado");
				((BeanListaClienteCertif) objBean).setRespuesta("Se genero con exito el Certificado");
			}
		} catch (Exception e) {
			log.debug("Error al generar la impresion de certificados.");
			e.printStackTrace();
		}
	}
	/**
	 * Metodo de impresion de certificado
	 * @param ruta donde se deposita el certificado
	 * @return String nombre del reporte
	 */
	private String impresionCertificado(String ruta) {	
		String reporte = "";
		
		try {
			if(this.getCertificado().getId() != null){
				reporte = servicioCliente.impresionCertificado(ruta, this.getCertificado().getId().getCoceCasuCdSucursal()
																   , this.getCertificado().getId().getCoceCarpCdRamo()
																   , this.getCertificado().getId().getCoceCapoNuPoliza()
																   , this.getCertificado().getId().getCoceNuCertificado()
																   , this.getCertificado().getCoceFeEmision());
				if(reporte != null) {
					reporte = reporte.replace(".pdf", "");
				}
			} else {
				reporte = null;
			}
		} catch (Exception e) {
			log.error("Error al generar la impresion de certificados.",e);
			e.printStackTrace();
			FacesUtils.addInfoMessage("Error al generar el reporte");
		} finally {
			if(reporte == null) {
		       this.message.append(reporte);
			}else{
				log.info ("Archivo generado correctamente"); 
			    this.message.append("Archivo generado correctamente");	
			}
		}
		
		return reporte;
	}
	
	
	/**
	 * @param checkE the checkE to set
	 */
	public void setCheckE(boolean checkE) {
		this.checkE = checkE;
	}

	/**
	 * @return the checkE
	 */
	public boolean isCheckE() {
		return checkE;
	}

	public void setRadioE(Integer radioE) {
		this.radioE = radioE;
	}

	public Integer getRadioE() {
		return radioE;
	}
	public List<BeneficiariosDTO> getLstBeneficiarios() {
		return new ArrayList<BeneficiariosDTO>(lstBeneficiarios) ;
	}
	public void setLstBeneficiarios(List<BeneficiariosDTO> lstBeneficiarios) {
		this.lstBeneficiarios = new ArrayList<BeneficiariosDTO>(lstBeneficiarios);
	}
	public List<EndosoDatosDTO> getLstEndosos() {
		return new ArrayList<EndosoDatosDTO>(lstEndosos);
	}
	public void setLstEndosos(List<EndosoDatosDTO> lstEndosos) {
		this.lstEndosos = new ArrayList<EndosoDatosDTO>(lstEndosos);
	}
	
}
