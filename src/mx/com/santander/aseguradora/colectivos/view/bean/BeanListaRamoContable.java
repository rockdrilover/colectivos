/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.RamoContable;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamoContable;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Leobardo Preciado
 *
 */
@Controller
@Scope("session")
public class BeanListaRamoContable implements InitializingBean {

	@Resource
	private ServicioRamoContable servicioRamoContable;
	
	private List<Object> listaRamoContable;
	private List<Object> comboRamoContable;
	
	public BeanListaRamoContable() {
		this.comboRamoContable = new ArrayList<Object>();
		this.listaRamoContable = new ArrayList<Object>();
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {

		try{
			List<RamoContable> listaRamoContable = servicioRamoContable.getRamosContables();
			
			for (RamoContable ramoContable : listaRamoContable) {
				getComboRamoContable().add(new SelectItem(ramoContable.getCarbCdRamo()+"", ramoContable.getCarbCdRamo()+"-"+ramoContable.getCarbDeRamo()));
				getListaRamoContable().add(ramoContable);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new FacesException("No se puede inicializar la lista de ramos contables.", e);
		}

	}


	public ServicioRamoContable getServicioRamoContable() {
		return servicioRamoContable;
	}

	public void setServicioRamoContable(ServicioRamoContable servicioRamoContable) {
		this.servicioRamoContable = servicioRamoContable;
	}

	public List<Object> getListaRamoContable() {
		return listaRamoContable;
	}

	public void setListaRamoContable(List<Object> listaRamoContable) {
		this.listaRamoContable = listaRamoContable;
	}

	public List<Object> getComboRamoContable() {
		return comboRamoContable;
	}

	public void setComboRamoContable(List<Object> comboRamoContable) {
		this.comboRamoContable = comboRamoContable;
	}

	
}
