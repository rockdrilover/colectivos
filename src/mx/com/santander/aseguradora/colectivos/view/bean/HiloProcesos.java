package mx.com.santander.aseguradora.colectivos.view.bean;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

public class HiloProcesos implements Runnable {

	private Object bean;
	private String tipo;

	public HiloProcesos(Object bean, String beanListaPrecarga) {
		this.bean = bean;
		this.tipo = beanListaPrecarga;
	}

	private void emisionMasiva (){
		((BeanListaPreCarga)bean).emitir();
		((BeanListaPreCarga)bean).setProgressValue(101);
	}
	
	private void cancelacionMasiva (){
		((BeanCancelacionMasiva)bean).cancelacionMasiva();
		((BeanCancelacionMasiva)bean).setProgressValue(101);
	}
	
	private void renovacion (){
		try {
			((BeanConsultaRenovacion)bean).upchk();
		} catch (Excepciones e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((BeanConsultaRenovacion)bean).setProgressValue(101);
	}
	
	private void prefactura (){
		try {
			((BeanListaCertificado)bean).upchk();
		} catch (Excepciones e) {
			e.printStackTrace();
		}
		((BeanListaCertificado)bean).setProgressValue(101);
	}
	
	private void reporteMensual (){
		try {
			((BeanListaCertificado)bean).reportes();
		} catch (Excepciones e) {
			e.printStackTrace();
		}
		((BeanListaCertificado)bean).setProgressValue(101);
	}
	
	public void run() {
		// TODO Auto-generated method stub
		
		if (tipo.equals(BeanNames.BEAN_LISTA_PRECARGA))
			emisionMasiva ();
		if (tipo.equals(BeanNames.BEAN_PREFACTURA))
			prefactura ();
		if (tipo.equals(BeanNames.BEAN_REPORTE_MENSUAL))
			reporteMensual ();
		if (tipo.equals(BeanNames.BEAN_CANCELACION_MASIVA))
			cancelacionMasiva ();
		if (tipo.equals(BeanNames.BEAN_CONSULTA_RENOVACION))
			renovacion ();
	}	
}
