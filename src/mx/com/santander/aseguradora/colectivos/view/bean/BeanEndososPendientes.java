/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Mail;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanEndososPendientes {

	@Resource
	private ServicioEndosoDatos servicioEndosoDatos;

	private static final Log LOG = LogFactory.getLog(BeanEndososPendientes.class);
	private ArrayList<EndosoDatosDTO> lstEndosos;
	private String respuesta;
	private int numPagina;
    private EndosoDatosDTO seleccionado;
    private int nEjecuta;

    /**
     * Constructor de clase
     */
	public BeanEndososPendientes() {
		//Inicializa propiedades
		this.lstEndosos = new ArrayList<>();
		respuesta = Constantes.DEFAULT_STRING;
		nEjecuta = Constantes.DEFAULT_INT;
		
		//Agrega Bean a sesion
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ENDOSOS_PENDIENTES);
	}
    
	/**
	 * @return the servicioEndosoDatos
	 */
	public ServicioEndosoDatos getServicioEndosoDatos() {
		return servicioEndosoDatos;
	}
	/**
	 * @param servicioEndosoDatos the servicioEndosoDatos to set
	 */
	public void setServicioEndosoDatos(ServicioEndosoDatos servicioEndosoDatos) {
		this.servicioEndosoDatos = servicioEndosoDatos;
	}
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}
	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}
	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	/**
	 * @return the seleccionado
	 */
	public EndosoDatosDTO getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(EndosoDatosDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the lstEndosos
	 */
	public List<EndosoDatosDTO> getLstEndosos() {
		if(nEjecuta == Constantes.DEFAULT_INT) {
			//Ejecuta igual 1
			//Consulta endosos pendientes
			consultaEndososPendientes();
		}
		return new ArrayList<>(lstEndosos);
	}
	/**
	 * @param lstEndosos the lstEndosos to set
	 */
	public void setLstEndosos(List<EndosoDatosDTO> lstEndosos) {
		this.lstEndosos = new ArrayList<>(lstEndosos);
	}
	/**
	 * Set de nEjecuta
	 * @return valor de propeidad
	 */
	public int getnEjecuta() {
		return nEjecuta;
	}
	/**
	 * Get de nEjecuta
	 * @param nEjecuta valor para setear a propiedad
	 */
	public void setnEjecuta(int nEjecuta) {
		this.nEjecuta = nEjecuta;
	}    
	
	/**
	 * Metodo que consulta los endosos pendientes por usuario
	 */
	private void consultaEndososPendientes() {
		try{
			//Limpia lista de endosos pendientes
			lstEndosos = new ArrayList<>();
			
			//Llama servicio para consultar endosos pendientes
			lstEndosos.addAll(servicioEndosoDatos.getEndososPendientes(FacesUtils.getBeanSesion().getUserName()));
		} catch (Excepciones e) {		
			LOG.error("No se pueden cargar endosos pendientes", e);
		}
		
	}
	
	/**
	 * Metodo que autoriza un endoso
	 */
	public void autorizar() {
		try {
			//Autoriza endoso seleccionado
			this.servicioEndosoDatos.autorizarEndoso(seleccionado);
			this.LOG.info(seleccionado);
			
			//Envia correo
			enviaCorreo("fue autorizado. \n\n\n");
			
			//Genera Respuesta
			setRespuesta("Se autorizo con EXITO el endoso.");
		} catch (Excepciones e) {
			setRespuesta("Error al AUTORIZAR registro.");
			LOG.error(getRespuesta(), e);
			throw new FacesException(getRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que autoriza un endoso
	 */
	public void rechazar() {
		try {
			//Setea valores
			seleccionado.setCedaFeAplica(new Date());
			seleccionado.setCedaStEndoso(Constantes.ENDOSO_ESTATUS_RECHAZADO);
			
			//Rechaza endoso seleccionado
			this.servicioEndosoDatos.actualizarObjeto(seleccionado);
			
			//Envia Correo
			enviaCorreo("fue reachazado. \n\n\n");
			
			//Genera Respuesta
			setRespuesta("Se rechazo con EXITO el endoso.");
			
		} catch (Excepciones e) {
			setRespuesta("Error al RECHAZAR registro.");
			LOG.error(getRespuesta(), e);
			throw new FacesException(getRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que envia correo de notificacion
	 * @param strTitulo titulo del correo
	 */
	private void enviaCorreo(String strTitulo) {
		//Declara Variable
		StringBuilder sbMsgCorreo = new StringBuilder();	
		
		//Crea mensaje para el correo
		sbMsgCorreo.append("Buen Dia. \n\n");
		sbMsgCorreo.append(seleccionado.getCedaCdUsuarioReg()).append("\n");
		sbMsgCorreo.append("El endoso con Folio: ").append(seleccionado.getCedaCampov1()).append(strTitulo);
		
		//Envia correo
		Mail.enviar2(new ArrayList<String>(), sbMsgCorreo, "Respuesta Autorizacion Endoso - Folio: " + seleccionado.getCedaCampov1(), seleccionado.getCedaCorreoReg(), Constantes.DEFAULT_STRING, Constantes.DEFAULT_STRING);
		
		//Consulta endosos pendientes
		consultaEndososPendientes();
		
		//Ejecuta en 1
		setnEjecuta(1);
	}
}
