package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCliente;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.ClienteCertifDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ClienteDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author IBB
 *
 */
@Controller
@Scope("session")
public class BeanObligado {

	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioClienteCertif servicioCliente;
	@Resource
	private ServicioCliente serviceCliente;
	
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private List<Object> listaTitular;
	private List<Object> listaObligados;
	
	private short canal;
	private Short ramo;
	private Long poliza;
	private Long polizaEspecifica;
	private String certificado;
	private String respuesta;
	private int numPagina;
	private Long polizaConsulta;
	private String numeroCredito;
	
	
	/*
	 * Elemento seleccionado lista
	 */
	private ClienteCertifDTO seleccionado;

	/*
	 * Elemento seleccionado consulta Clientes
	 */
	private ClienteDTO clienteSel;
	
	private ClienteDTO newCliente;
	

	/*
	 * Elementos para Popup Nuevo
	 */
	private String tipoBusqueda;
	private String datoBusqueda;
	private String datoBusquedaRFC;
	private String datoBusquedaClie;
	private String nombre;
	private String paterno;
	private String materno;
	private String panelBuc = "none";
	private String panelRfc = "none";
	private String panelClie = "none";
	private String panelNombre = "none";
	private String panelNuevo = "block";
	private List<Object> listaClientes;
	private Boolean consulta;
	private String showBoton;
	
	public BeanObligado() {		
		this.message = new StringBuilder();
		this.canal = 1;
		this.ramo = Constantes.DEFAULT_INT;
		this.poliza = Constantes.DEFAULT_LONG;
		this.polizaEspecifica = Constantes.DEFAULT_LONG;
		this.certificado = Constantes.DEFAULT_STRING_ID;
		this.listaTitular = new ArrayList<Object>();
		this.listaObligados = new ArrayList<Object>();
		this.listaClientes = new ArrayList<Object>();
		this.consulta = false;
		this.newCliente = new ClienteDTO(); 
		this.showBoton = Constantes.DEFAULT_STRING_ID;
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_OBLIGADO);
	}

	/**
	 * Metodo que manda Consultar los datos ingresados en pantalla
	 * @return lista con resultados encontrados
	 */
	public String consultar(){
		List<ClienteCertif> lista;
		StringBuilder filtro;
		
		try {
			this.showBoton = Constantes.DEFAULT_STRING_ID;
			if(validaDatos()) {
				filtro = new StringBuilder();
				listaTitular = new ArrayList<Object>();
				listaObligados = new ArrayList<Object>();
				
				filtro.append(" and CC.id.coccCasuCdSucursal = ").append(this.getCanal()).append(" \n");
				filtro.append(" and CC.id.coccCarpCdRamo = ").append(this.getRamo()).append(" \n");
				filtro.append(" and CC.id.coccCapoNuPoliza = ").append(this.getPolizaConsulta()).append(" \n");
				filtro.append(" and CC.id.coccNuCertificado = ").append(this.getCertificado()).append(" \n");
				lista = this.servicioCliente.obtenerObjetos(filtro.toString());
				
				for(ClienteCertif clienteCertifi : lista){
					ClienteCertifDTO dto = (ClienteCertifDTO) ConstruirObjeto.crearBean(ClienteCertifDTO.class, clienteCertifi);
					setListas(dto);
				}
				
				if(listaObligados.isEmpty()) {
					listaObligados = new ArrayList<Object>();
					setShowBoton(Constantes.CARGADA_CONCILIADO);
				}
				
				setConsulta(false);
				setRespuesta("Se realizo consulta con exito, favor de revisar resultados");
			} else {
				listaTitular = new ArrayList<Object>();
				listaObligados = new ArrayList<Object>();
			}
			return null;
		} catch (Exception e) {
			this.message.append("Error al recuperar certificados para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

	private void setListas(ClienteCertifDTO dto) {
		if(dto.getCoccTpCliente() == null) {
			dto.setCoccTpCliente(Short.valueOf("1"));
		} else {
			dto.setCoccTpCliente(dto.getCoccTpCliente());
		}
		
		
		if(dto.getCoccTpCliente() == 1) {
			setNumeroCredito(dto.getId().getCoccIdCertificado());
			listaTitular.add(dto);
		} else {
			listaObligados.add(dto);
		}
		
	}

	private boolean validaDatos() {
		boolean bRegreso = true;
		
		setPolizaConsulta(getPoliza());
		if(this.getRamo().equals(Constantes.DEFAULT_INT)) {				
			setRespuesta("Favor de seleccionar el ramo.");
			bRegreso =  false;
		} else if(this.getPolizaConsulta().equals(Constantes.DEFAULT_LONG)) {
			setRespuesta("Favor de seleccionar una poliza.");
			bRegreso =  false;
		} else if(this.getPolizaConsulta() == 1) {
			if(this.getPolizaEspecifica() == null || this.getPolizaEspecifica().equals(Constantes.DEFAULT_LONG)) {
				setRespuesta("Es prima unica, favor de ingresar poliza especifica.");
				bRegreso =  false;
			} else if(this.getCertificado().equals(Constantes.DEFAULT_STRING)) {
				setRespuesta("Favor de ingresar numero de certificado.");
				bRegreso =  false;
			}
			if(bRegreso) {
				setPolizaConsulta(this.getPolizaEspecifica());
			}
		} else if(this.getCertificado().equals(Constantes.DEFAULT_STRING)) {
			setRespuesta("Favor de ingresar numero de certificado.");
			bRegreso =  false;
		}
		
		return bRegreso;
	}
		
	/**
	 * Metodo que manda quitar un obligado del certificado
	 * @throws Excepciones
	 */
	public void quitarObligado() throws Excepciones {
    	try {
    		this.servicioCliente.borrarObjeto(seleccionado);
    		consultar();
		} catch (Exception e) {
			this.message.append("Error al quitar obligado.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	/**
	 * Metodo que manda cambiar el titular por obligado
	 * @throws Excepciones
	 */
	public void cambiarObligado() throws Excepciones {
    	try {    		
    		this.servicioCliente.actualizaTitular(seleccionado.getCertificado().getId());
    		
    		seleccionado.setCoccTpCliente(new Short("1"));
    		this.servicioCliente.actualizarObjeto(seleccionado);
   			this.servicioCliente.notificarCambio(seleccionado.getId(), seleccionado.getCoccTpCliente(), 0);
   		
    		consultar();
		} catch (Exception e) {
			this.message.append("Error al cambiar obligado.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

	/**
	 * Metodo que muestra el en pantalla las opciones de buqsqueda
	 * @return
	 */
	public String muestraCriterio(){
		if(getTipoBusqueda() != null && "0".equals(getTipoBusqueda())) {
			setPanelBuc("block");
			setPanelRfc("none");
			setPanelClie("none");
			setPanelNombre("none");
		} else if(getTipoBusqueda() != null && "1".equals(getTipoBusqueda())){
			setPanelBuc("none");
			setPanelRfc("block");
			setPanelClie("none");
			setPanelNombre("none");			
		} else if(getTipoBusqueda() != null && "2".equals(getTipoBusqueda())){
			setPanelBuc("none");
			setPanelRfc("none");
			setPanelClie("block");
			setPanelNombre("none");	
		} else if(getTipoBusqueda() != null && "3".equals(getTipoBusqueda())){
			setPanelBuc("none");
			setPanelRfc("none");
			setPanelClie("none");
			setPanelNombre("block");
		}
		
		this.setDatoBusqueda(Constantes.DEFAULT_STRING);
		this.setNombre(Constantes.DEFAULT_STRING);
		this.setPaterno(Constantes.DEFAULT_STRING);
		this.setMaterno(Constantes.DEFAULT_STRING);
		setConsulta(false);
		
		setPanelNuevo("none");
		return getTipoBusqueda();
	}
	
	/**
	 * Metodo que manda buscar los clientes segun filtro de busqueda
	 * @return
	 */
	public String buscarClientes(){
		List<Cliente> lista;
		StringBuilder filtro;
		
		try {
			if(validaDatosBusqueda()) {
				filtro = new StringBuilder();
				listaClientes = new ArrayList<Object>();
				
				if(getTipoBusqueda() != null && "0".equals(getTipoBusqueda())) { //BUC
					filtro.append(" and CC.cocnBucCliente = '").append(getDatoBusqueda()).append("' \n");
				} else if(getTipoBusqueda() != null && "1".equals(getTipoBusqueda())){  //RFC
					filtro.append(" and CC.cocnRfc = '").append(getDatoBusquedaRFC().toUpperCase()).append("' \n");
				} else if(getTipoBusqueda() != null && "2".equals(getTipoBusqueda())){  //Cliente
					filtro.append(" and CC.cocnNuCliente = ").append(getDatoBusquedaClie()).append(" \n");	
				} else if(getTipoBusqueda() != null && "3".equals(getTipoBusqueda())){ //Nombre
					filtro.append(" and CC.cocnNombre = '").append(getNombre().toUpperCase()).append("' \n");
					filtro.append(" and CC.cocnApellidoPat = '").append(getPaterno().toUpperCase()).append("' \n");
					filtro.append(" and CC.cocnApellidoMat = '").append(getMaterno().toUpperCase()).append("' \n");
				}
				
				
				lista = this.serviceCliente.obtenerObjetos(filtro.toString());
				
				for(Cliente cliente : lista){
					ClienteDTO dto = (ClienteDTO) ConstruirObjeto.crearBean(ClienteDTO.class, cliente);
					listaClientes.add(dto);
				}
				
				setPanelNuevo("block");
				setConsulta(true);
				setRespuesta("Se realizo consulta con exito, favor de revisar resultados");
				return null;
			} else {
				listaClientes = null;
				return NavigationResults.FAILURE;
			}
		} catch (Exception e) {
			this.message.append("Error al recuperar clientes para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	private boolean validaDatosBusqueda() {
		boolean bRegreso = true;
				
		if(getTipoBusqueda() != null && "0".equals(getTipoBusqueda())){ //BUC
			if(getDatoBusqueda().equals(Constantes.DEFAULT_STRING)) {
				FacesUtils.addErrorMessage("Favor de ingresar un BUC para buscar.");
				bRegreso =  false;
			}
		} else if(getTipoBusqueda() != null && "1".equals(getTipoBusqueda())){ //RFC
			if(getDatoBusquedaRFC().equals(Constantes.DEFAULT_STRING)) {
				FacesUtils.addErrorMessage("Favor de ingresar un RFC para buscar.");
				bRegreso =  false;
			}
		} else if(getTipoBusqueda() != null && "2".equals(getTipoBusqueda())){ //Numero Cliente
			if(getDatoBusquedaClie().equals(Constantes.DEFAULT_STRING)) {
				FacesUtils.addErrorMessage("Favor de ingresar un Numero Cliente para buscar.");
				bRegreso =  false;
			}
		} else if(getTipoBusqueda() != null && "3".equals(getTipoBusqueda())){ //Nombre
			if(getNombre().equals(Constantes.DEFAULT_STRING)) {
				FacesUtils.addErrorMessage("Favor de ingresar nombre.");
				bRegreso =  false;
			} else if(getPaterno().equals(Constantes.DEFAULT_STRING)) {
				FacesUtils.addErrorMessage("Favor de ingresar apellido paterno.");
				bRegreso =  false;
			} else if(getMaterno().equals(Constantes.DEFAULT_STRING)) {
				FacesUtils.addErrorMessage("Favor de ingresar apellido materno.");
				bRegreso =  false;
			}
		} 
		
		return bRegreso;
	}

	/**
	 * Metodo que manda asociar un cliente al certificado
	 * @throws Excepciones
	 */
	public void asociarCliente() throws Excepciones {
		ClienteCertifDTO objCliente;
		
    	try {    	
    		if(esTitular(clienteSel.getCocnNuCliente())) {
    			setRespuesta("No se puede asociar el obligado, por que la misma persona esta como Titular.");
    		} else {
    			objCliente = new ClienteCertifDTO();
        		objCliente.getId().setCoccCasuCdSucursal(this.getCanal());
        		objCliente.getId().setCoccCarpCdRamo(this.getRamo());
        		objCliente.getId().setCoccCapoNuPoliza(this.getPolizaConsulta());
        		objCliente.getId().setCoccNuCertificado(new Long(this.getCertificado()));
        		objCliente.getId().setCoccIdCertificado(getNumeroCredito());
        		objCliente.getId().setCoccNuCliente(clienteSel.getCocnNuCliente());
        		objCliente.setCoccTpCliente(new Short("2"));
        		
        		this.servicioCliente.guardarObjeto(objCliente);
       			this.servicioCliente.notificarCambio(objCliente.getId(), objCliente.getCoccTpCliente(), 1);
        		consultar();
    		}
    		
		} catch (Exception e) {
			this.message.append("Error al asociar obligado.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	private boolean esTitular(long nuClienteAsociar) {		
		long nuClienteTitular;
		
		if(!listaTitular.isEmpty()) {
			nuClienteTitular = ((ClienteCertifDTO)listaTitular.get(0)).getId().getCoccNuCliente();
			if(nuClienteTitular == nuClienteAsociar) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Metodo que manda guardar los datos del cliente capturado en pantalla
	 * @throws Excepciones
	 */
	public void guardarCliente() throws Excepciones {
		ClienteCertifDTO objCliente;
		ClienteDTO objGuardado;
		
    	try {
    		if(validaDatosCliente()) {
    			//Guardamos Cliente
        		objGuardado = serviceCliente.guardarObjeto(newCliente);
        		
        		//Guardamos relacion del cliente con el certificado.
        		objCliente = new ClienteCertifDTO();
        		objCliente.getId().setCoccCasuCdSucursal(this.getCanal());
        		objCliente.getId().setCoccCarpCdRamo(this.getRamo());
        		objCliente.getId().setCoccCapoNuPoliza(this.getPolizaConsulta());
        		objCliente.getId().setCoccNuCertificado(new Long(this.getCertificado()));
        		objCliente.getId().setCoccIdCertificado(getNumeroCredito());
        		objCliente.getId().setCoccNuCliente(objGuardado.getCocnNuCliente());
        		
        		if(getNewCliente().getCocnTpCliente() == 1) {
        			servicioCliente.actualizaTitular(new CertificadoId(this.getCanal(), this.getRamo(), this.getPolizaConsulta(), new Long(this.getCertificado())));
        			objCliente.setCoccTpCliente(getNewCliente().getCocnTpCliente());
        		} else {
        			objCliente.setCoccTpCliente(new Short("2"));
        		}
        		
        		this.servicioCliente.guardarObjeto(objCliente);
       			this.servicioCliente.notificarCambio(objCliente.getId(), objCliente.getCoccTpCliente(), 1);
        		        		
        		setPanelNuevo("none");
        		setPanelBuc("block");
    			setPanelRfc("none");
    			setPanelClie("none");
    			setPanelNombre("none");
    			this.setDatoBusqueda(Constantes.DEFAULT_STRING);
    			this.setNombre(Constantes.DEFAULT_STRING);
    			this.setPaterno(Constantes.DEFAULT_STRING);
    			this.setMaterno(Constantes.DEFAULT_STRING);
    			setConsulta(false);
        		consultar();
    		}
		} catch (Exception e) {
			this.message.append("Error al guardar obligado.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	private boolean validaDatosCliente() {
		boolean bRegreso = true;
		
		if(getNewCliente().getCocnBucCliente().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("Favor de ingresar un BUC.");
			bRegreso =  false;
		} else if(getNewCliente().getCocnNombre().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("Favor de ingresar Nombre.");
			bRegreso =  false;
		} else if(getNewCliente().getCocnApellidoPat().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("Favor de ingresar Apellido Paterno.");
			bRegreso =  false;
		} else if(getNewCliente().getCocnApellidoMat().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("Favor de ingresar Apellido Materno.");
			bRegreso =  false;
		} else if(getNewCliente().getCocnRfc().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("Favor de ingresar un RFC.");
			bRegreso =  false;
		} else if(getNewCliente().getCocnCdSexo().equals(Constantes.DEFAULT_STRING_ID)) {
			FacesUtils.addErrorMessage("Favor de seleccionar Sexo del cliente.");
			bRegreso =  false;
		} else if(getNewCliente().getCocnCdEdoCivil().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("Favor de ingresar un Estado Civil del cliente.");
			bRegreso =  false;
		} 
		
		return bRegreso;

	}
	
	public ClienteCertifDTO getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(ClienteCertifDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
	public short getCanal() {
		return canal;
	}
	public void setCanal(short canal) {
		this.canal = canal;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	public Long getPoliza() {
		return poliza;
	}
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public int getNumPagina() {
		return numPagina;
	}
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	public List<Object> getListaTitular() {
		return new ArrayList<Object>(listaTitular);
	}
	public void setListaTitular(List<Object> listaTitular) {
		this.listaTitular = new ArrayList<Object>(listaTitular);
	}
	public List<Object> getListaObligados() {
		return new ArrayList<Object>(listaObligados);
	}
	public void setListaObligados(List<Object> listaObligados) {
		this.listaObligados = new ArrayList<Object>(listaObligados);
	}
	public Long getPolizaEspecifica() {
		return polizaEspecifica;
	}
	public void setPolizaEspecifica(Long polizaEspecifica) {
		this.polizaEspecifica = polizaEspecifica;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public ServicioClienteCertif getServicioCliente() {
		return servicioCliente;
	}
	public void setServicioCliente(ServicioClienteCertif servicioCliente) {
		this.servicioCliente = servicioCliente;
	}
	public Long getPolizaConsulta() {
		return polizaConsulta;
	}
	public void setPolizaConsulta(Long polizaConsulta) {
		this.polizaConsulta = polizaConsulta;
	}
	public String getTipoBusqueda() {
		return tipoBusqueda;
	}
	public void setTipoBusqueda(String tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}
	public String getDatoBusqueda() {
		return datoBusqueda;
	}
	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}
	public String getNombre() {
		return nombre;
	}
	public String getDatoBusquedaRFC() {
		return datoBusquedaRFC;
	}
	public void setDatoBusquedaRFC(String datoBusquedaRFC) {
		this.datoBusquedaRFC = datoBusquedaRFC;
	}
	public String getDatoBusquedaClie() {
		return datoBusquedaClie;
	}
	public void setDatoBusquedaClie(String datoBusquedaClie) {
		this.datoBusquedaClie = datoBusquedaClie;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getPanelBuc() {
		return panelBuc;
	}
	public void setPanelBuc(String panelBuc) {
		this.panelBuc = panelBuc;
	}
	public String getPanelRfc() {
		return panelRfc;
	}
	public void setPanelRfc(String panelRfc) {
		this.panelRfc = panelRfc;
	}
	public String getPanelClie() {
		return panelClie;
	}
	public void setPanelClie(String panelClie) {
		this.panelClie = panelClie;
	}
	public String getPanelNombre() {
		return panelNombre;
	}
	public void setPanelNombre(String panelNombre) {
		this.panelNombre = panelNombre;
	}
	public String getPanelNuevo() {
		return panelNuevo;
	}
	public void setPanelNuevo(String panelNuevo) {
		this.panelNuevo = panelNuevo;
	}
	public List<Object> getListaClientes() {
		return new ArrayList<Object>(listaClientes);
	}
	public void setListaClientes(List<Object> listaClientes) {
		this.listaClientes = new ArrayList<Object>(listaClientes);
	}
	public Boolean getConsulta() {
		return consulta;
	}
	public void setConsulta(Boolean consulta) {
		this.consulta = consulta;
	}
	public ServicioCliente getServiceCliente() {
		return serviceCliente;
	}
	public void setServiceCliente(ServicioCliente serviceCliente) {
		this.serviceCliente = serviceCliente;
	}
	public ClienteDTO getClienteSel() {
		return clienteSel;
	}
	public void setClienteSel(ClienteDTO clienteSel) {
		this.clienteSel = clienteSel;
	}
	public String getNumeroCredito() {
		return numeroCredito;
	}
	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}
	public ClienteDTO getNewCliente() {
		return newCliente;
	}
	public void setNewCliente(ClienteDTO newCliente) {
		this.newCliente = newCliente;
	}
	/**
	 * Metodo que regresa valor de showBoton
	 * @return
	 */
	public String getShowBoton() {
		return showBoton;
	}
	/**
	 * Metodo que da valor de showBoton
	 * @return
	 */
	public void setShowBoton(String showBoton) {
		this.showBoton = showBoton;
	}

}
