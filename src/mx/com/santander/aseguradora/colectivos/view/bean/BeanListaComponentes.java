/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Leobardo Preciado Vidals
 *
 */
@Controller
@Scope("session")
public class BeanListaComponentes extends BeanBase implements InitializingBean{

	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ServicioColectivosComponentes servicioColectivosComponentes;
	
	
	private Log log = LogFactory.getLog(this.getClass());
	private String strRespuesta;

	private List<Parametros> listaComponentes;
	private Parametros newComponente;
	private Parametros editComponente;
	
	public BeanListaComponentes() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_COMPONENTES);
		
		this.listaComponentes = new ArrayList<Parametros>();
		this.log.debug("BeanListaParametros creado");
		this.newComponente  = new Parametros();
		this.editComponente = new Parametros();
	}
	
	
	/*
	 * INICIO DE METODOS
	 * */
	public String consultar(){
		StringBuilder filtro = new StringBuilder();
		getListaComponentes().clear();
		
		try {
			filtro.append("  and P.copaDesParametro = 'CATALOGO'");
			filtro.append("  and P.copaIdParametro  = 'CATCOMPONENTES'");
			List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
			setListaComponentes(lista);
			setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			return NavigationResults.SUCCESS;
		} catch(Exception e){
			e.printStackTrace();
			this.log.error("No se pueden recuperar los parametros.", e);
			return NavigationResults.FAILURE;
		}		
	}
	
	//LPV
	public void modificar() {
		try {
			servicioParametros.actualizarObjeto(getEditComponente());
			if(getEditComponente().getCopaVvalor2().equalsIgnoreCase("IVA")){
				servicioColectivosComponentes.respaldaIVA(new BigDecimal(getEditComponente().getCopaVvalor6()));
			}
			consultar();
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
	}

	//LPV
	public String alta(){
		try {
			
			String comboCompo = getNewComponente().getCopaVvalor2();
			getNewComponente().setCopaVvalor2(comboCompo.substring(comboCompo.indexOf("|")+1));
			getNewComponente().setCopaNvalor2( Long.parseLong(comboCompo.substring(0,comboCompo.indexOf("|"))) );
			
			getNewComponente().setCopaCdParametro(new Long(this.servicioParametros.siguienteCdParam()));
			getNewComponente().setCopaDesParametro("CATALOGO");
			getNewComponente().setCopaIdParametro("CATCOMPONENTES");
			
			this.servicioParametros.guardarObjeto(getNewComponente());
			consultar();
			setStrRespuesta("Se guardo con EXITO el registro.");
			setNewComponente(new Parametros());
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			setStrRespuesta("Error Componente duplicado.");
			e.printStackTrace();
			this.log.error("Componente duplicado.", e);
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			setStrRespuesta("No se puede dar de alta el Componenten.");
			e.printStackTrace();
			this.log.error("No se puede dar de alta el Componenten.", e);
			return NavigationResults.FAILURE;
		}
	}
	
	
	
	public void afterPropertiesSet() throws Exception {
	}

	/**
	 * @return the servicioParametros
	 */
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public String getStrRespuesta() {
		return strRespuesta;
	}

	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}

	public List<Parametros> getListaComponentes() {
		return listaComponentes;
	}
	
	public void setListaComponentes(List<Parametros> listaComponentes) {
		this.listaComponentes = listaComponentes;
	}
	
	public Parametros getNewComponente() {
		return newComponente;
	}
	
	public void setNewComponente(Parametros newComponente) {
		this.newComponente = newComponente;
	}

	public Parametros getEditComponente() {
		return editComponente;
	}
	public void setEditComponente(Parametros editComponente) {
		this.editComponente = editComponente;
	}
	
	public ServicioColectivosComponentes getServicioColectivosComponentes() {
		return servicioColectivosComponentes;
	}
	
	public void setServicioColectivosComponentes(
			ServicioColectivosComponentes servicioColectivosComponentes) {
		this.servicioColectivosComponentes = servicioColectivosComponentes;
	}
}
