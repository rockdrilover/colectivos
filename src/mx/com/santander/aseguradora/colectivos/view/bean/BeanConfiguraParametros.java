package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estatus;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstatus;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;


/**
 * @author Ing. IBB
 */
@Controller
@Scope("session")
public class BeanConfiguraParametros {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ServicioEstatus servicioEstatus;
	@Resource
	private ServicioProducto servicioProducto;
	
	private List<Object> listaDatos;
	private List<Object> listaComboCausas;
	private List<Object> listaComboProductos;
	private String strRespuesta, status;
	private int filaActual;
	private BeanParametros objActual = new BeanParametros();
	private BeanParametros objNuevo = new BeanParametros();
	private Set<Integer> keys = new HashSet<Integer>();
	
	private int tipoCatalogo;

	public BeanConfiguraParametros(){
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONFIGURA_PARAMETROS);
		this.listaDatos = new ArrayList<Object>();
		this.listaComboCausas = new ArrayList<Object>();
		this.listaComboProductos = new ArrayList<Object>();
		strRespuesta = "";
		objActual = new BeanParametros();
		objNuevo = new BeanParametros();
	}
	
	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  getListaDatos().iterator();
        while(it.hasNext()) {
        	BeanParametros objParametro = (BeanParametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActual = objParametro;
                break;
            }
        }
    }
	
	public String consultar(){
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean;
		List<Object> lstResultados = null;
		Parametros parametro;
		Iterator<Object> it;
		
		try {
			getListaDatos().clear();

			if(getTipoCatalogo() == Constantes.CATALOGO_CAUSAS_ANULACION) {
				filtro.append("and P.copaDesParametro = 'DEVOLUCION'" );
				filtro.append("and P.copaIdParametro  = 'CAUSAS'" );
			} else if(getTipoCatalogo() == Constantes.CATALOGO_PRODUCTOS_BENEFICIARIOS) {
				filtro.append("and P.copaDesParametro = 'BENEFICIARIO'" );
				filtro.append("and P.copaIdParametro  = 'ENDOSOS'" );
			}
					
			lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
				
		    it = lstResultados.iterator();
		    while(it.hasNext()) {
		    	parametro = (Parametros) it.next();
		    	
		    	bean = (BeanParametros) ConstruirObjeto.crearObjeto(BeanParametros.class, parametro);
		    	bean.setCopaVvalor2(bean.getCopaVvalor3().equals("1") ? "ACTIVADA" : "DESACTIVADA");

		    	objActual = bean;
				listaDatos.add(bean);
			}
		    setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");
		 
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.log.error(" no es posble consultar la lista de causas.", e);
			FacesUtils.addErrorMessage("no es posble consultar la lista de causas.");
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			e.printStackTrace();
			this.log.error("no es posble consultar la lista de causas.", e);
			FacesUtils.addErrorMessage("no es posble consultar la lista de causas.");
			return NavigationResults.FAILURE;
		}
	}
	
	public void guardar() {
		Long secParametro;
		
		try {
			if(validarDatosNuevo()) {
				if(!existeRegistro()) {
					secParametro = servicioParametros.siguente();
					objNuevo.setCopaCdParametro(secParametro);
					
					if(getTipoCatalogo() == Constantes.CATALOGO_CAUSAS_ANULACION ){
						objNuevo.setCopaIdParametro("CAUSAS");
						objNuevo.setCopaDesParametro("DEVOLUCION");
						objNuevo.setCopaVvalor1(getDescripcionCausa(objNuevo.getCopaNvalor1()));
					} else if(getTipoCatalogo() == Constantes.CATALOGO_PRODUCTOS_BENEFICIARIOS ){
						objNuevo.setCopaIdParametro("ENDOSOS");
						objNuevo.setCopaDesParametro("BENEFICIARIO");
						objNuevo.setCopaVvalor1(getDescProducto());
					}
					
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
					this.servicioParametros.guardarObjeto(parametro);
					setStrRespuesta("Se guardo con EXITO el registro.");
					
					objNuevo = new BeanParametros();
				}
			}
						
		} catch (Exception e) {
			setStrRespuesta("Error al GUARDAR registro.");
			e.printStackTrace();
		}
	}

	private boolean validarDatosNuevo() {
		boolean bRegreso = true;
		
		try {
			if(getTipoCatalogo() == Constantes.CATALOGO_CAUSAS_ANULACION) {
				if(objNuevo.getCopaNvalor1().intValue() == Constantes.DEFAULT_INT) {
					setStrRespuesta("Favor de seleccionar una causa anulacion.");
					bRegreso = false;
				} else if(objNuevo.getCopaVvalor3().equals("0")) {
					setStrRespuesta("Favor de seleccionar un estatus.");
					bRegreso = false;
				}
			} else if(getTipoCatalogo() == Constantes.CATALOGO_PRODUCTOS_BENEFICIARIOS) {
				if(objNuevo.getCopaNvalor1().intValue() == Constantes.DEFAULT_INT) {
					setStrRespuesta("Favor de seleccionar un ramo.");
					bRegreso = false;
				} else if(objNuevo.getCopaNvalor2().intValue() == Constantes.DEFAULT_INT) {
					setStrRespuesta("Favor de seleccionar un producto.");
					bRegreso = false;
				} else if(objNuevo.getCopaVvalor3().equals("0")) {
					setStrRespuesta("Favor de seleccionar un estatus.");
					bRegreso = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			bRegreso = false;
		}
			
		return bRegreso;
	}

	public void modificar(){
		try {
			if (!objActual.getCopaVvalor3().equals("0")){				
				objActual.setCopaVvalor2(objActual.getCopaVvalor3().equals("1") ? "ACTIVADA" : "DESACTIVADA");
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual) ;				
				servicioParametros.actualizarObjeto(parametro);
				setStrRespuesta("Se modifico con EXITO el registro.");								
			}else{
				setStrRespuesta("No se modifico, favor de seleccionar un estatus.");
			}
			listaDatos.clear();
			listaDatos.add(objActual);
		} catch (Exception e) {
			setStrRespuesta("Error al MODIFICAR registro.");
			e.printStackTrace();
		}
	} 
	
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(listaDatos);
        return lstValues;
    }	

	private void getCausasAnulacion() throws Exception {
		List<Estatus> lstEstatus;
		BeanEstatus beanEstatus;
		
		try {
			listaComboCausas = new ArrayList<Object>();
			lstEstatus = this.servicioEstatus.obtenerObjetos(Estatus.class);
			for(Estatus objeto : lstEstatus){
				beanEstatus = (BeanEstatus) ConstruirObjeto.crearBean(BeanEstatus.class, objeto);
				beanEstatus.setServicioEstatus(this.servicioEstatus);
				
				if(beanEstatus.getAlesDescCorta().equalsIgnoreCase("CAUSAANULACION")){
					listaComboCausas.add(new SelectItem(beanEstatus.getAlesCdEstatus().toString(), beanEstatus.getAlesDescripcion()));
				}
			}
			
		} catch (Exception e) {
			throw new FacesException("Error al cargar Causas Anulacion", e);
		}
	}
	
	private String getDescripcionCausa(Integer cdCausa) throws Exception {
		String strDescCausa = Constantes.DEFAULT_STRING;
		List<Estatus> lstEstatus;
		BeanEstatus beanEstatus;
		
		try {
			lstEstatus = this.servicioEstatus.obtenerObjetos(Estatus.class);
			for(Estatus objeto : lstEstatus){
				beanEstatus = (BeanEstatus) ConstruirObjeto.crearBean(BeanEstatus.class, objeto);
				
				if(beanEstatus.getAlesDescCorta().equalsIgnoreCase("CAUSAANULACION")){
					if(beanEstatus.getAlesCdEstatus().equals(cdCausa)) {
						strDescCausa =  beanEstatus.getAlesDescripcion().toUpperCase().trim();
						break;
					}
				}
			}
		} catch (Exception e) {
			this.log.error("No se pueden recuperar descripcion.", e);
			e.printStackTrace();
		}

		return strDescCausa;
	}

	private boolean existeRegistro() {
		Boolean blnResultado = false; 
		StringBuilder filtro = new StringBuilder(100);
		List<Object> lstResultados = null;
		String strMensage = Constantes.DEFAULT_STRING;
		
		try {
			if(getTipoCatalogo() == Constantes.CATALOGO_CAUSAS_ANULACION) {
				filtro.append("and P.copaDesParametro = 'DEVOLUCION'");
				filtro.append("and P.copaIdParametro  = 'CAUSAS'");
				filtro.append("and P.copaNvalor1      = ").append(objNuevo.getCopaNvalor1());
				strMensage = "Esta causa anulacion " + objNuevo.getCopaNvalor1().toString() + ", ya existe en el catalogo.";
			} else if(getTipoCatalogo() == Constantes.CATALOGO_PRODUCTOS_BENEFICIARIOS) {
				filtro.append("and P.copaDesParametro = 'BENEFICIARIO'");
				filtro.append("and P.copaIdParametro  = 'ENDOSOS'");
				filtro.append("and P.copaNvalor1      = ").append(objNuevo.getCopaNvalor1());
				filtro.append("and P.copaNvalor2      = ").append(objNuevo.getCopaNvalor2());
				strMensage = "Esta producto  " + objNuevo.getCopaNvalor1().toString() + " - " + objNuevo.getCopaNvalor2().toString() + ", ya existe en el catalogo.";
			}
			
			lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
			if(!lstResultados.isEmpty()) {
				blnResultado = true; 
				setStrRespuesta(strMensage);
			}
		} catch (Exception e) {
			throw new FacesException("Error al buscar registro.", e);
		}
		
		return blnResultado;
	}
	
	public String consultarProductos(){
		String filtro = "";
		getListaComboProductos().clear();
		
		try {
			if(this.objNuevo.getCopaNvalor1() != null && this.objNuevo.getCopaNvalor1() > 0) {
				filtro = " and P.id.alprCdRamo = " + this.objNuevo.getCopaNvalor1() + "\n";
				List<Producto> lstProducto = this.servicioProducto.obtenerObjetos(filtro);
				
				for (Producto producto : lstProducto) {
					this.listaComboProductos.add(new SelectItem(producto.getId().getAlprCdProducto().toString(), producto.toString()));
				}
			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los productos.", e);
			return NavigationResults.FAILURE;
		}
	}
	
	public String getDescProducto(){
		String strDesc = Constantes.DEFAULT_STRING;
		String filtro = "";
		
		try {
			filtro = " and P.id.alprCdRamo = " + this.objNuevo.getCopaNvalor1() + "\n";
			filtro = filtro + " and P.id.alprCdProducto = " + this.objNuevo.getCopaNvalor2() + "\n";
			List<Producto> lstProducto = this.servicioProducto.obtenerObjetos(filtro);
			
			if(!lstProducto.isEmpty()) {
				strDesc = lstProducto.get(0).getAlprDeProducto();
				if(strDesc.length() > 80) {
					strDesc = strDesc.substring(0, 80);
				}
			}
		} catch (Exception e) {
			this.log.error("No se pueden recuperar descripcion.", e);
			e.printStackTrace();
		}
		
		return strDesc;
	}
	
	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	public ServicioEstatus getServicioEstatus() {
		return servicioEstatus;
	}
	public void setServicioEstatus(ServicioEstatus servicioEstatus) {
		this.servicioEstatus = servicioEstatus;
	}
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	public int getFilaActual() {
		return filaActual;
	}
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}
	public BeanParametros getObjActual() {
		return objActual;
	}
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	public Set<Integer> getKeys() {
		return keys;
	}
	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}
	public List<Object> getListaDatos() {
		return listaDatos;
	}
	public void setListaDatos(List<Object> listaDatos) {
		this.listaDatos = listaDatos;
	}
	public List<Object> getListaComboCausas() throws Exception {
		getCausasAnulacion();
		return listaComboCausas;
	}
	public void setListaComboCausas(List<Object> listaComboCausas) {
		this.listaComboCausas = listaComboCausas;
	}
	public BeanParametros getObjNuevo() {
		return objNuevo;
	}
	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}
	public int getTipoCatalogo() {
		return tipoCatalogo;
	}
	public void setTipoCatalogo(int tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}
	public List<Object> getListaComboProductos() {
		return listaComboProductos;
	}
	public void setListaComboProductos(List<Object> listaComboProductos) {
		this.listaComboProductos = listaComboProductos;
	}
	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}
	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}
}
