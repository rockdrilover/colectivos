/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Date;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAlta;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

/**
 * @author dflores
 *
 */
@Controller
@Scope("session")
public class BeanAlta {

	@Resource
	private ServicioAlta servicioAlta;
	@Resource
	private ServicioAltaContratante servicioAltaContratante;
	@Resource
	private ServicioProcesos servicioProcesos;
	@Resource
	private ServicioParametros servicioParametros;
	
	private Log log = LogFactory.getLog(this.getClass());
	
	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanAlta.class);
	
	private Integer inCanal; 
	private Short   inRamo;
	private Integer inSucursal;
	private String  inAnalista;
	private Date 	inFechaIni;
	private Long	poliza;
	private String  message;
	
	/*******************************/
	private String inNacionalidad;       
	private	String cedulaRif;            
	private	String inRazonSocial;        
	private	String inCdVida; //"V"		   
	private	String inRfc;	               
	private	String inTpCiente;	//"N" 		
	private	long inActividad;    	       
	private	Date inFechaCons;		         
	private	String inEdoCivil;						
	private	String inSexo;								
	private	String inCalleNum; 						
	private	String inColonia;							
	private	String inPoblacion;		
	private	long   inCd;  //0            
	private	long   inCdEdo; 		         
	private	String  inCp;     							
	private	String inLada; //"55"         
	private	String inTel;  //"0"          
	private	long inCdBanco;  //4         
	private	String nuCuenta;             
	private	String tpCuenta;             
	private	String nacionalidad2;        
	private	String cedula2;              
	private	long domicilio2;             
	private	String mensaje2;              

	private String res;
	
	/******************************/
	
	public BeanAlta() {
		// TODO Auto-generated constructor stub
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ALTA);
		this.inAnalista = "377237";
		this.poliza	= 0L;
		
	/************************/
		this.inActividad = 47;
		this.inNacionalidad = "J";
		this.inEdoCivil = "S";
		this.inSexo ="M";
        this.nuCuenta = "56555806964";
        this.tpCuenta = "C";
        this.inCdVida = "V";
        this.inTpCiente = "N";
        this.inCdBanco = 4;
        this.inLada = "55";
        this.inTel = "0";
        //this.inPoblacion = "9";
        /************************/
		
	}

	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}

	public Long getPoliza() {
		return poliza;
	}

	public void setInFechaIni(Date inFechaIni) {
		this.inFechaIni = inFechaIni;
	}

	public Date getInFechaIni() {
		return inFechaIni;
	}

	/**
	 * @param inCanal the inCanal to set
	 */
	public void setInCanal(Integer inCanal) {
		this.inCanal = inCanal;
	}

	/**
	 * @return the inCanal
	 */
	public Integer getInCanal() {
		return inCanal;
	}

	public void setServicioAlta(ServicioAlta servicioAlta) {
		this.servicioAlta = servicioAlta;
	}


	/************************************/
	public void setServicioAltaContratante(ServicioAltaContratante servicioAltaContratante) {
		this.servicioAltaContratante = servicioAltaContratante;
	}


	
	public ServicioProcesos getServicioProcesos() {
		return servicioProcesos;
	}
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}	
	
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	/***********************************/
     
	
	
	public Integer getInSucursal() {
		return inSucursal;
	}

	public void setInSucursal(Integer inSucursal) {
		this.inSucursal = inSucursal;
	}

	public String getInAnalista() {
		return inAnalista;
	}

	public void setInAnalista(String inAnalista) {
		this.inAnalista = inAnalista;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param inRamo the inRamo to set
	 */
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}

	/**
	 * @return the inRamo
	 */
	public Short getInRamo() {
		return inRamo;
	}
	

	public String iniciaAlta(){			
		this.log.debug("Iniciando proceso de alta poliza");
		//System.out.println("Iniciando proceso de alta");
				
		poliza = 0L;
		Integer      canal = 1;
		
		if(this.inRamo     != null &&
		   this.inAnalista != null &&
		   this.inSucursal != null &&
		   this.inFechaIni != null)
		{
		
			try {
				     this.inCanal = 1;			
					 poliza =  this.servicioAlta.iniciaAlta( 
									canal,
									this.inRamo ,												
									this.inAnalista,
									this.inSucursal,
									"J",
									"2",
									this.inFechaIni
									);
					
					
					//Se agrega esta linea para insertar en COLECTIVOS_PARAMETROS 
					Long siguiente = this.servicioParametros.siguente(); 
					this.servicioParametros.guardarObjeto(this.crearObjeto(siguiente, this.inRamo.longValue(), poliza)); 
					 
					message = "La Poliza n�mero " + poliza + " se dio de alta satisfactoriamente.";
					LOG.info(Logger.SECURITY_SUCCESS, message);
					FacesUtils.addInfoMessage(message);
					return NavigationResults.SUCCESS;
					
				}
				catch (DataAccessException e) {
					// TODO: handle exception
					message = "Hubo un error al al dar de alta la poliza." + e.getLocalizedMessage();
					LOG.error(Logger.SECURITY_FAILURE, message, e);
					FacesUtils.addErrorMessage(message);
					
					return NavigationResults.FAILURE;
				}
				catch (Exception e) {
					// TODO: handle exception
					message = "No se pudo realizar el alta de la poliza. Algun dato es incorrecto.";					
					LOG.error(Logger.SECURITY_FAILURE, message, e);
					FacesUtils.addErrorMessage(message);
					
					return NavigationResults.FAILURE;
				}
			
		}
		else
		{
			message = "El ramo o sucursal no son corectos";
			LOG.info(Logger.SECURITY_FAILURE, message);
			FacesUtils.addInfoMessage(message);
			
			return NavigationResults.FAILURE;
			
		} 
		
	}
	
	/********************************************/
	
	public String getInNacionalidad() {
		return inNacionalidad;
	}

	public void setInNacionalidad(String inNacionalidad) {
		this.inNacionalidad = inNacionalidad;
	}

	public String getCedulaRif() {
		return cedulaRif;
	}

	public void setCedulaRif(String cedulaRif) {
		this.cedulaRif = cedulaRif;
	}

	public String getInRazonSocial() {
		return inRazonSocial;
	}

	public void setInRazonSocial(String inRazonSocial) {
		this.inRazonSocial = inRazonSocial;
	}

	public String getInCdVida() {
		return inCdVida;
	}

	public void setInCdVida(String inCdVida) {
		this.inCdVida = inCdVida;
	}

	public String getInRfc() {
		return inRfc;
	}

	public void setInRfc(String inRfc) {
		this.inRfc = inRfc;
	}

	public String getInTpCiente() {
		return inTpCiente;
	}

	public void setInTpCiente(String inTpCiente) {
		this.inTpCiente = inTpCiente;
	}

	public long getInActividad() {
		return inActividad;
	}

	public void setInActividad(long inActividad) {
		this.inActividad = inActividad;
	}

	public Date getInFechaCons() {
		return inFechaCons;
	}

	public void setInFechaCons(Date inFechaCons) {
		this.inFechaCons = inFechaCons;
	}

	public String getInEdoCivil() {
		return inEdoCivil;
	}

	public void setInEdoCivil(String inEdoCivil) {
		this.inEdoCivil = inEdoCivil;
	}

	public String getInSexo() {
		return inSexo;
	}

	public void setInSexo(String inSexo) {
		this.inSexo = inSexo;
	}

	public String getInCalleNum() {
		return inCalleNum;
	}

	public void setInCalleNum(String inCalleNum) {
		this.inCalleNum = inCalleNum;
	}

	public String getInColonia() {
		return inColonia;
	}

	public void setInColonia(String inColonia) {
		this.inColonia = inColonia;
	}

	public String getInPoblacion() {
		return inPoblacion;
	}

	public void setInPoblacion(String inPoblacion) {
		this.inPoblacion = inPoblacion;
	}

	public long getInCdEdo() {
		return inCdEdo;
	}

	public void setInCdEdo(long inCdEdo) {
		this.inCdEdo = inCdEdo;
	}

	

	public String getInCp() {
		return inCp;
	}

	public void setInCp(String inCp) {
		this.inCp = inCp;
	}

	public long getInCdBanco() {
		return inCdBanco;
	}

	public void setInCdBanco(long inCdBanco) {
		this.inCdBanco = inCdBanco;
	}

	public String getNuCuenta() {
		return nuCuenta;
	}

	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}

	public String getTpCuenta() {
		return tpCuenta;
	}

	public void setTpCuenta(String tpCuenta) {
		this.tpCuenta = tpCuenta;
	}

	public String getNacionalidad2() {
		return nacionalidad2;
	}

	public void setNacionalidad2(String nacionalidad2) {
		this.nacionalidad2 = nacionalidad2;
	}

	public String getCedula2() {
		return cedula2;
	}

	public void setCedula2(String cedula2) {
		this.cedula2 = cedula2;
	}

	public long getDomicilio2() {
		return domicilio2;
	}

	public void setDomicilio2(long domicilio2) {
		this.domicilio2 = domicilio2;
	}

	public String getMensaje2() {
		return mensaje2;
	}

	public void setMensaje2(String mensaje2) {
		this.mensaje2 = mensaje2;
	}
	

	public long getInCd() {
		return inCd;
	}

	public void setInCd(long inCd) {
		this.inCd = inCd;
	}

	public String getInLada() {
		return inLada;
	}

	public void setInLada(String inLada) {
		this.inLada = inLada;
	}

	public String getInTel() {
		return inTel;
	}

	public void setInTel(String inTel) {
		this.inTel = inTel;
	}

	public String iniciaAltaContratante(){
		
		this.log.debug("Iniciando proceso de alta contratante");
		if(this.inRazonSocial!= null)
		{
			try {
				
				  res = this.servicioProcesos.numeradorCliente();
				  
				  if (res != null){
					  this.inColonia = "1"; 
					  this.servicioProcesos.insertaContratante(res,
							                                   this.inRazonSocial,
							                                   this.inFechaCons,
							                                   this.inCalleNum,
							                                   this.inCp,
							                                   this.inRfc,
							                                   this.inPoblacion,
							                                   this.inColonia);
					  this.setCedulaRif(res);
					  
					  this.servicioProcesos.actNumeradorCliente();
				  }
				
				//System.out.println("servicioAltaContratante" + this.servicioAltaContratante); 
				/*res =  this.servicioAltaContratante.iniciaAltaContratante(this.inNacionalidad, 
																		  "1895226", //this.cedulaRif,  descomentar
																		  this.inRazonSocial,     
																		  this.inCdVida, 
																		  this.inRfc, 
																		  this.inTpCiente, 
																		  this.inActividad,    	  
																		  this.inFechaCons, 
																		  this.inEdoCivil, 
																		  this.inSexo, 
																		  this.inCalleNum, 				
																		  this.inColonia, 
																		  this.inPoblacion, 
																		  this.inCd, 
																		  this.inCdEdo, 
																		  this.inCP,     					
																		  this.inLada, 
																		  this.inTel, 
																		  this.inCdBanco, 
																		  this.nuCuenta, 
																		  this.tpCuenta,          
																		  this.nacionalidad2, 
																		  this.cedula2, 
																		  this.domicilio2, 
																		  this.mensaje2);
		       */																  
					message = "Contratante con Nacionalidad J y cedula " + cedulaRif + " se dio de alta satisfactoriamente.";
					LOG.info(Logger.SECURITY_SUCCESS, message);
					FacesUtils.addInfoMessage(message);
					return NavigationResults.SUCCESS;
					
				}
				catch (DataAccessException e) {
					// TODO: handle exception
					message = "Hubo un error al al dar de alta contratante." + e.getLocalizedMessage();
					LOG.error(Logger.SECURITY_FAILURE, message, e);
					FacesUtils.addErrorMessage(message);
					
					return NavigationResults.FAILURE;
				}
				catch (Exception e) { 
					e.printStackTrace();
					// TODO: handle exception
					message = "No se pudo realizar el alta de contratante. Algun dato es incorrecto.";					
					LOG.error(Logger.SECURITY_FAILURE, message, e);
					FacesUtils.addErrorMessage(message);
					
					return NavigationResults.FAILURE;
				}
			
		}
		else
		{
			message = "El ramo o sucursal no son corectos";
			LOG.info(Logger.SECURITY_FAILURE, message);
			FacesUtils.addInfoMessage(message);
			
			return NavigationResults.FAILURE;
			
		} 
	}
	
	public Parametros crearObjeto(Long siguiente, Long ramo, Long poliza){
		Parametros params = new Parametros();
		
		params.setCopaCdParametro(siguiente);
		params.setCopaDesParametro("POLIZA");
		params.setCopaIdParametro("GEP");
		params.setCopaVvalor1("0");
		params.setCopaVvalor2("0");
		params.setCopaVvalor3("M");
		params.setCopaVvalor4("1");
		params.setCopaNvalor1(1);
		params.setCopaNvalor2(ramo);
		params.setCopaNvalor3(poliza);
		params.setCopaNvalor4(0);
		params.setCopaNvalor5(12);
		params.setCopaNvalor6(0.0);//ID VENTA
		params.setCopaNvalor7(0.0);//PRIMA RECURRENTE
		
		return params;
	}
}
