package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioValidaEmision;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

/**
 * 
 * @author Santander Tecnologia JEH
 *
 */
@Controller
@Scope("session")
public class BeanEmisionMasiva {

	@Resource
	private ServicioValidaEmision servicioValidaEmi;
	
	/**
	 * Generacion de log
	 */
	private static final Log LOG = LogFactory.getLog(BeanEmisionMasiva.class);
	
	/**
	 * Cadena donde se almacenara el mensaje
	 */
	private StringBuilder message;
	/**
	 * Cadena donde se almacenara la respuesta
	 */
	private String respuesta;
	/**
	 * Cadena donde se almacena el codigo de respuesta
	 */
	private Integer codigoResp;
	
	
	/**
	 * Cadena donde se almacena el codigo de respuesta de ejecucion de validacion
	 */
	private Integer respValidacion;
	
	/**
	 * Constructor de la clase
	 */
	public BeanEmisionMasiva() {
		
		/**
		 * Instancia de sesion
		 */
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_EMISION_MASIVA);
	}
	/**
	 * @param msjVal the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.respuesta = mensaje;
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return respuesta;
	}
	/**
	 * @return the codigoResp
	 */
	public Integer getCodigoResp() {
		return codigoResp;
	}

	/**
	 * @param codigoResp the codigoResp to set
	 */
	public void setCodigoResp(Integer codigoResp) {
		this.codigoResp = codigoResp;
	}

	/**
	 * @return the servicioValidaEmi
	 */
	public ServicioValidaEmision getServicioValidaEmi() {
		return servicioValidaEmi;
	}

	
	
	/**
	 * @return the respValidacion
	 */
	public Integer getRespValidacion() {
		return respValidacion;
	}
	/**
	 * @param respValidacion the respValidacion to set
	 */
	public void setRespValidacion(Integer respValidacion) {
		this.respValidacion = respValidacion;
	}
	/**
	 * @param servicioValidaEmi the servicioValidaEmi to set
	 */
	public void setServicioValidaEmi(ServicioValidaEmision servicioValidaEmi) {
		this.servicioValidaEmi = servicioValidaEmi;
	}
	/**
	 * Metodo que realiza el proceso de validaci�n
	 * @param ramo se refiere al ramo de la p�liza a validar
	 * @param poliza poliza a validar
	 * @param idVenta identificador de canal de venta
	 * @return terminado con exitoso o fallido
	 */
	public String validar(Short ramo,Long poliza,Integer idVenta){
		/* Inicializacion de terminado */
		String terminado=null;
		
		Map<String, Object> respuesta;
		
		
		/* Instancia de mensaje */
		message = new StringBuilder();
		/* Seteo de canal */
		Short canal=1;
		try {
			/* Llamado al servicio de validacion de p�lizas */
			respuesta = servicioValidaEmi.validPolizaEmiMasiva(canal, ramo, poliza, idVenta);
			String[] cadenaMensaje =this.message.toString().split("\\|");
			/* Seteo del valor de respuesta */
			//if (respuesta)
			
			
			if (respuesta.containsKey("pMensaje")) {
				this.respuesta = (String) respuesta.get("pMensaje");
			}
			
			this.codigoResp = -1;
			if (respuesta.containsKey("pValor")) {
				this.codigoResp= (Integer) respuesta.get("pValor");
			}
			
			this.respValidacion = -1;
			
			if (respuesta.containsKey("returnname")) {
				this.respValidacion= (Integer) respuesta.get("returnname");
			}
			
			/* Seteo de terminado exitoso */
			terminado =  NavigationResults.SUCCESS;
			/* Bloque de excepciones */
		} catch (Excepciones e) {
			LOG.error("Excepcion en la clase BeanEmisionMasiva");
			LOG.error(e);
			this.respuesta = "Hubo un error al validar la Emision";
			this.codigoResp= 9;
			/* Seteo de navegacion fallida */
			terminado =  NavigationResults.SUCCESS;
		} catch (NumberFormatException e) {
			LOG.error("Excepcion en la clase BeanEmisionMasiva");
			LOG.error(e);
			this.respuesta = "Error al obtener informacion de la poliza, Valide su estatus";
			this.codigoResp= 9;
			/* Seteo de navegacion fallida */
			terminado =  NavigationResults.SUCCESS;
		}
		/* Retorno de la cadena de terminado */
		return terminado;
	}
}
