package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;

public class BeanRespuesta {
	
	private BigDecimal tipo;
	private String descripcion;
	private String poliza;
	private BigDecimal subtotal;
	
	public BigDecimal getTipo() {
		return tipo;
	}
	public void setTipo(BigDecimal tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	
	
}
