package mx.com.santander.aseguradora.colectivos.view.bean;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Ramo;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.model.service.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")

public class BeanExtraPrimaConfig implements  InitializingBean {
	
	// recursos	
	private StringBuilder message;
	private Log log = LogFactory.getLog(this.getClass());
	@Resource
	private  ServicioPlan servicioPlan;
	@Resource
	private ServicioProducto servicioProducto;
	@Resource
	private ServicioCartCoberturas servicioCartCoberturas;

	
	// mapeo  plan 
		private PlanId id; //ramo- cd producto - plan 
		//private PlanId alplCdProducto; // cd producto
		private String alplDePlan; 	//descPlan
		private Integer alplDato3;	//centro de costos
		
		private List<Plan> listaPlanes;
	    private List<Object> listaComboCoberturas;
	    private Ramo ramo;

	// construye
		
	

		public BeanExtraPrimaConfig() {
			
			this.id	= new PlanId();
			// this.ramo 	= new Ramo();
			// inicializamos lista
			this.listaPlanes = new ArrayList<Plan>();
			this.listaComboCoberturas = new ArrayList<Object>();
			
		}
		
		
		//Setters y Getters  
	
		public PlanId getId() {
			return id;
		}

		public void setId(PlanId id) {
			this.id = id;
		}

		/*public PlanId getAlplCdProducto() {
			return alplCdProducto;
		}

		public void setAlplCdProducto(PlanId alplCdProducto) {
			this.alplCdProducto = alplCdProducto;
		}*/

		public String getAlplDePlan() {
			return alplDePlan;
		}

		public void setAlplDePlan(String alplDePlan) {
			this.alplDePlan = alplDePlan;
		}

		
		public Integer getAlplDato3() {
			return alplDato3;
		}

		public void setAlplDato3(Integer alplDato3) {
			this.alplDato3 = alplDato3;
		}

			
		public List<Plan> getListaPlanes() {
			return listaPlanes;
		}

		public void setListaPlanes(List<Plan> listaPlanes) {
			this.listaPlanes = listaPlanes;
		}
	
		public List<Object> getListaComboCoberturas() {
				return listaComboCoberturas;
			}


		public void setListaComboCoberturas(List<Object> listaComboCoberturas) {
				this.listaComboCoberturas = listaComboCoberturas;
			}
		
		public Ramo getRamo() {
			return ramo;
		}


		public void setRamo(Ramo ramo) {
			this.ramo = ramo;
		}

		
	// termina getters y setters

		public String altaPlan(){
			
		
			try {
				
				
				// obtenemos el plan max para  la extra Prima
				
			
				System.out.println("*** ------  Entro a alta plan ---- ****"); 
				
				// obtiiene maximo 
				id.setAlplCdPlan( this.servicioPlan.siguentePlanExtraPma());
				System.out.println("CD Plan 2 despues  :" + id.getAlplCdPlan());
				
				Plan plan = ( Plan ) ConstruirObjeto.crearObjeto(Plan.class, this);	
				this.servicioPlan.guardarObjeto(plan);
				
				
				
				this.consultaPlanes();
				
			
				
				
				return NavigationResults.SUCCESS;

		
			}
			catch (ObjetoDuplicado e) {
				// TODO: handle exception
				this.message.append("Plan  duplicado.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				Utilerias.resetMessage(message);
				
				return NavigationResults.RETRY;
			}
			catch (Excepciones e) {

				e.printStackTrace();
				this.message.append("No se puede dar de alta el plan extra prima.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				Utilerias.resetMessage(message);
				
				return NavigationResults.FAILURE;
				
			}
		
		}
		
		
         // Cargar informacion desde  que se lanza la pantalla requiere de implementar  --> implements  InitializingBean
		public void afterPropertiesSet() throws Exception {
				
				StringBuilder filtro = new StringBuilder(100);
				
				getListaPlanes().clear();
				
				System.out.println(" ---***** Entra a trae DATOS  DE PLANES ****---");
				
				try {
			
					filtro.append(" and P.id.alplCdRamo =  61 ");
					filtro.append(" and P.alplDePlan = 'CASOS ESPECIAL 51' ");
					filtro.append(" and P.id.alplCdPlan = 67 ");
					
					List<Plan> listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
					
					for (Plan plan : listaPlanes) {
						
						// esta forma convierte el objeto de hibernate a bean para poder verlo en la vista
						getId().setAlplCdRamo(plan.getId().getAlplCdRamo());
						getId().setAlplCdProducto(plan.getId().getAlplCdProducto());
						setAlplDePlan(plan.getAlplDePlan());  
						setAlplDato3(plan.getAlplDato3());
						getId().setAlplCdPlan( this.servicioPlan.siguentePlanExtraPma());

						System.out.println(plan.getId().getAlplCdRamo());
						System.out.println(plan.getId().getAlplCdProducto());
						System.out.println(plan.getAlplDePlan());
						System.out.println(plan.getAlplDato3());
						System.out.println(plan.getId().getAlplCdPlan());
						// max de cd plan 
						
				}
					
					System.out.println("----*****  Termino TRAE DATOS  DE PLANES ****---");
					
					//LPV Reseteamos el Ramo para forzar al usuario a seleccionar uno
					getId().setAlplCdRamo(Byte.parseByte("0"));
					
				} catch (Exception e) {
					e.printStackTrace();
					message.append("No se pueden cargar los parametros.");
					this.log.error(message.toString(), e);
					FacesUtils.addErrorMessage(message.toString());
					
				}
			}

	public String consultaPlanes(){
			
			StringBuilder filtro = new StringBuilder(200);
			
			getListaPlanes().clear();
			
			try {
				System.out.println("ramo: " + getId().getAlplCdRamo());
				System.out.println("plan: " + getId().getAlplCdPlan());
				filtro.append(" and P.id.alplCdRamo 	 = ").append(getId().getAlplCdRamo());
				//filtro.append(" and P.id.alplCdPlan  = ").append(getId().getAlplCdPlan());
				filtro.append(" and P.id.alplCdProducto  = ").append(getId().getAlplCdProducto());
				filtro.append(" and P.alplDePlan  = '").append(getAlplDePlan() + "'");
				filtro.append(" ");
				
				List<Plan> listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
				
				for (Plan plan : listaPlanes) {
					getListaPlanes().add(plan);
				}
				
				System.out.println("-------------  extra plan ------------");
				System.out.println("lsta:" + listaPlanes );
				
				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				e.printStackTrace();
				message.append("No se pueden cargar los parametros.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}
			
		}
		
}





		
	


