package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.*;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.*;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/*
 * Autor  : MGE
 * 
 */

@Controller
@Scope("session")
public class BeanSegmentos {
	
	@Resource
	private ServicioParametros servicioParametros;
	private Log log = LogFactory.getLog(this.getClass());
	private String strRespuesta;
	// Mapeo de parameteos 

	private Long copaCdParametro;	 	// MAX 
	private String copaDesParametro; 	// CATALOGO
	private String copaIdParametro;  	// SEGMENTO 
	private String copaVvalor1;		 	// DESCRiBE SEGMENTO
	private Date copaFvalor1;  			//  iNi ViGENCiA DE SEGMENTO
	private Date copaFvalor2;  			//  FiN ViGENCiA DE SEGMENTO  
	private Date copaFvalor3;  //
	
	private List<Object>     listaSegmento;
	private List<Parametros> listaParametros;

	private HashMap<String, Object> hmResultados;
	private int filaActual;
	private BeanParametros objActual = new BeanParametros();
	private BeanParametros objNuevo = new BeanParametros();
	private Set<Integer> keys = new HashSet<Integer>();
	private int 	numPagina;

	public BeanSegmentos (){
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_SEGMENTOS);
		this.listaSegmento	=  new ArrayList<Object>();
		this.log.debug("BeanListaSegmento creado");
		this.numPagina = 1;
		
	}
	
	//METODOS
	
	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it = getListaSegmento().iterator();
        while(it.hasNext()) {
        	BeanParametros objParametro = (BeanParametros) it.next();
        	if (objParametro.getCopaCdParametro().toString().equals(clave)){
                objActual = objParametro;
                break;
            }
        }
    }
	
	public String consultarSegmentos(){
		String filtro = "";
		List<Object> lstResultados;
		Parametros parametro;
		BeanParametros bean;

		try {
			hmResultados = new HashMap<String, Object>();
			filtro = " and P.copaDesParametro = 'CATALOGO' \n" + " and P.copaIdParametro = 'SEGMENTO' \n";
			lstResultados = this.servicioParametros.obtenerObjetos(filtro);
			listaSegmento = new ArrayList<Object>();
			Iterator<Object> it = lstResultados.iterator();
			while(it.hasNext()) {
				parametro = (Parametros) it.next();
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
				bean.setServicioParametros(this.servicioParametros);
				
				listaSegmento.add(bean);
				hmResultados.put(bean.getCopaCdParametro().toString(), bean);
			}

			setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los segmentos.", e);
			e.printStackTrace();
			return NavigationResults.FAILURE;
		}
	}
	
	public void modificar() {
		try {
			Parametros parametros = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual);
			servicioParametros.actualizarObjeto(parametros);
			hmResultados.remove(objActual.getCopaCdParametro().toString());
			hmResultados.put(objActual.getCopaCdParametro().toString(), objActual);
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
	}
	
	public String altaSegmento(){
		Long secParametro;
		try {
			secParametro = servicioParametros.siguente(); 
			objNuevo.setCopaCdParametro(secParametro); 	// SIGUIENTE NUMERO 
			objNuevo.setCopaIdParametro("SEGMENTO");	// SEGMENTO
			objNuevo.setCopaDesParametro("CATALOGO"); 	// CATALOGO
			objNuevo.setCopaNvalor1(1);					// CANAL 1-DEFAULT
				
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
			this.servicioParametros.guardarObjeto(parametro);
			setStrRespuesta("Se guardo con EXITO el registro.");
			
			objNuevo = new BeanParametros();
			consultarSegmentos();
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			setStrRespuesta("Error Registro duplicado.");
			e.printStackTrace();
			this.log.error("Registro duplicado.", e);
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			setStrRespuesta("No se puede dar de alta el segmento.");
			e.printStackTrace();
			this.log.error("No se puede dar de alta el segmento.", e);
			return NavigationResults.FAILURE;				
		}
	}
		
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados().values());
        return lstValues;
    }
				
	public void afterPropertiesSet() throws Exception {
		try {
		} catch (Exception e) {
			throw new FacesException("No se pueden cargar los segmentos.", e);
		}
	}	
		
	// GETTERS Y SETTERS
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public String getStrRespuesta() {
		return strRespuesta;
	}

	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}

	public Long getCopaCdParametro() {
		return copaCdParametro;
	}

	public void setCopaCdParametro(Long copaCdParametro) {
		this.copaCdParametro = copaCdParametro;
	}

	public String getCopaDesParametro() {
		return copaDesParametro;
	}

	public void setCopaDesParametro(String copaDesParametro) {
		this.copaDesParametro = copaDesParametro;
	}

	public String getCopaIdParametro() {
		return copaIdParametro;
	}

	public void setCopaIdParametro(String copaIdParametro) {
		this.copaIdParametro = copaIdParametro;
	}

	public String getCopaVvalor1() {
		return copaVvalor1;
	}

	public void setCopaVvalor1(String copaVvalor1) {
		this.copaVvalor1 = copaVvalor1;
	}

	public Date getCopaFvalor1() {
		return copaFvalor1;
	}

	public void setCopaFvalor1(Date copaFvalor1) {
		this.copaFvalor1 = copaFvalor1;
	}

	public Date getCopaFvalor2() {
		return copaFvalor2;
	}

	public void setCopaFvalor2(Date copaFvalor2) {
		this.copaFvalor2 = copaFvalor2;
	}

	public Date getCopaFvalor3() {
		return copaFvalor3;
	}

	public void setCopaFvalor3(Date copaFvalor3) {
		this.copaFvalor3 = copaFvalor3;
	}
	
	
	public List<Parametros> getListaParametros() {
		return listaParametros;
	}

	public void setListaParametros(List<Parametros> listaParametros) {
		this.listaParametros = listaParametros;
	}
	
	
	public BeanParametros getObjActual() {
		return objActual;
	}

	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}

	public BeanParametros getObjNuevo() {
		return objNuevo;
	}

	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}
	public int getNumPagina() {
		return numPagina;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	
	public HashMap<String, Object> getHmResultados() {
		return hmResultados;
	}

	public void setHmResultados(HashMap<String, Object> hmResultados) {
		this.hmResultados = hmResultados;
	}
	
	public Set<Integer> getKeys() {
		return keys;
	}

	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}

	
	public List<Object> getListaSegmento() {
		return listaSegmento;
	}

	public void setListaSegmento(List<Object> listaSegmento) {
		this.listaSegmento = listaSegmento;
	}
	
	public int getFilaActual() {
		return filaActual;
	}

	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}

	
}
