package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;


@Controller
@Scope("session")
public class BeanFechasAjuste {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioParametros servicioParametros;
	
	@Resource
	private ServicioCertificado servicioCertificado;
	
	private StringBuilder message;
	private Short 					inRamo;
	private Short					newRamo;	
	private Long 					inPoliza;
	private Long 					newPoliza;
	private Date 					cierrefin, cierreini;
	private boolean 				chkUpdate;
	private boolean 				disable;
	private List<Object> 			listaCentroCosto;
	private String 					strRespuesta, status;
	private int 					filaActual;
	private Integer 				inCanal;
	private BeanParametros 			objActual = new BeanParametros();
	private BeanParametros 			objNuevo = new BeanParametros();
	private Set<Integer> 			keys = new HashSet<Integer>();
	private List<Object> 			listaComboPolizas;
	private HtmlSelectOneMenu 		enRamo;
	
	
	public BeanFechasAjuste() {
		this.message = new StringBuilder();
		this.listaCentroCosto = new ArrayList<Object>();
		this.listaComboPolizas = new ArrayList<Object>();
		this.disable = true;
		this.inRamo = null;
		this.inCanal = 1;
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_FECHAS_AJUSTE);
	}
	
	public HtmlSelectOneMenu getEnRamo() {
		return enRamo;
	}

	public void setEnRamo(HtmlSelectOneMenu enRamo) {
		this.enRamo = enRamo;
	}
	
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}

	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
	
	public List<Object> getListaComboPolizas() {
		return listaComboPolizas;
	}

	public void setListaComboPolizas(List<Object> listaComboPolizas) {
		this.listaComboPolizas = listaComboPolizas;
	}
	
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}

	public Short getInRamo() {
		return inRamo;
	}
	
	public void setNewRamo(Short newRamo) {
		this.newRamo = newRamo;
	}

	public Short getNewRamo() {
		return newRamo;
	}
	
	public void setInPoliza(Long inPoliza) {
		this.inPoliza = inPoliza;
	}

	public Long getInPoliza() {
		return inPoliza;
	}
	
	public void setNewPoliza(Long newPoliza) {
		this.newPoliza = newPoliza;
	}

	public Long getNewPoliza() {
		return newPoliza;
	}

	public Date getCierrefin() {
		return cierrefin;
	}

	public void setCierrefin(Date cierrefin) {
		this.cierrefin = cierrefin;
	}

	public Date getCierreini() {
		return cierreini;
	}

	public void setCierreini(Date cierreini) {
		this.cierreini = cierreini;
	}

	public boolean isChkUpdate() {
		return chkUpdate;
	}

	public void setChkUpdate(boolean chkUpdate) {
		this.chkUpdate = chkUpdate;
	}
	
	public List<Object> getListaCentroCosto() {
		return listaCentroCosto;
	}
	public void setListaCentroCosto(List<Object> listaCentroCosto) {
		this.listaCentroCosto = listaCentroCosto;
	}
	public int getFilaActual() {
		return filaActual;
	}
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}

	public BeanParametros getObjActual() {
		return objActual;
	}
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	public BeanParametros getObjNuevo() {
		return objNuevo;
	}
	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}
	public Set<Integer> getKeys() {
		return keys;
	}
	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStrRespuesta() {
		return strRespuesta;
	}
	private void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	public boolean getDisable() {
		return disable;
	}
	
	public void setDisable(boolean disable) {
		this.disable = disable;
	}
	
public String cargaPolizas(){
		
		this.message.append("cargando polizas.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder filtro = new StringBuilder(100);
		Short oRamo;
		this.listaComboPolizas.clear();
		
			try {
				
				if(this.newRamo != null) {
					oRamo = this.newRamo;
				} else {				
					oRamo = (Short) this.enRamo.getValue();
				}
				
				if(oRamo == 57 || oRamo == 58) {
					filtro.append("  and C.id.coceCasuCdSucursal 	=").append(this.inCanal).append(" \n");
					filtro.append("  and C.id.coceCarpCdRamo 		= ").append(oRamo).append("\n");
					filtro.append("  and C.id.coceCapoNuPoliza 		> 0 \n");
					filtro.append("  and C.id.coceNuCertificado 	= 0 \n");
					filtro.append("  and C.estatus.alesCdEstatus    = 1 \n");
					
					List<Certificado> listaPolizas = this.servicioCertificado.obtenerObjetos(filtro.toString());
					
					for(Certificado item: listaPolizas) {
						this.listaComboPolizas.add(new SelectItem(item.getId().getCoceCapoNuPoliza().toString(), item.getId().getCoceCapoNuPoliza().toString()));
					}
				} else {
					filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
					filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
					filtro.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
					filtro.append("  and P.copaNvalor2 = ").append(oRamo).append("\n");
					filtro.append("  and P.copaNvalor7 = 0 \n");
					filtro.append("order by P.copaNvalor3");
					
					List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
					
					for(Parametros item: listaPolizas) {
						this.listaComboPolizas.add(new SelectItem(item.getCopaNvalor3().toString(), item.getCopaNvalor3().toString()));
					}
				}
				
				return NavigationResults.SUCCESS;
			} catch (Exception e) {
				message.append("No se pueden cargar los parametros.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}
	}
	
	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  getListaCentroCosto().iterator();
        while(it.hasNext()) {
        	BeanParametros objParametro = (BeanParametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActual = objParametro;
                break;
            }
        }
    }
	
	public String consultaFechasAjuste(){
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean;
		List<Object> lstResultados = null;
		Iterator<Object> it;
		Object[] arrDatos;
		
		try {
			getListaCentroCosto().clear();
			if(this.inRamo != 0) {
				if(this.inPoliza != 0) {
					filtro.append(" and p.COPA_NVALOR2 = " + this.inRamo + "\n" );
					if(this.inPoliza == -1) {
						filtro.append("and p.COPA_NVALOR3 > 0 \n" );	
					} else {
						filtro.append("and p.COPA_NVALOR3 = " + this.inPoliza + "\n" );
					}
					filtro.append("and CR.CORE_ST_RECIBO = 1" );

					lstResultados = this.servicioParametros.obtenerCatalogoFechasAjuste(filtro.toString());
				}
				if(lstResultados.isEmpty()){
					setStrRespuesta("NO existen registros para la consulta.");
				} else {
				    it = lstResultados.iterator();
				    while(it.hasNext()) {
				    	arrDatos = (Object[]) it.next();
				    	bean = new BeanParametros();
				    	
				    	bean.setCopaCdParametro(Long.parseLong(arrDatos[0].toString()));
				    	bean.setCopaIdParametro("AJUSTE");
				    	bean.setCopaDesParametro("RECIBO");
				    	bean.setCopaNvalor1(Integer.parseInt(arrDatos[3].toString()));
				    	bean.setCopaNvalor2(Long.parseLong(arrDatos[4].toString()));
				    	bean.setCopaNvalor3(Long.parseLong(arrDatos[5].toString()));
				    	bean.setCopaNvalor4(Integer.parseInt(arrDatos[6].toString()));
				    	bean.setCopaFvalor1((Date) arrDatos[7]);
				    	bean.setCopaNvalor6(Double.parseDouble(arrDatos[8].toString()));
				    	
				    	objActual = bean;
						listaCentroCosto.add(bean);
					}
				    setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");
				}
			} else {
				setStrRespuesta("Selecciona un RAMO para poder realizar la consulta.");
			}
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.log.error("La fecha de cierre ya existe no es posble darlo de alta nuevamente.", e);
			FacesUtils.addErrorMessage("La fecha de cierre ya existe no es posble darlo de alta nuevamente.");
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			e.printStackTrace();
			this.log.error("No se puede dar de alta la fecha de cierre.", e);
			FacesUtils.addErrorMessage("No se puede dar de alta la fecha de cierre.");
			return NavigationResults.FAILURE;
		}
	}
	
	public void modificar(){
		try {
			if(!objActual.getCopaNvalor4().equals(null) && !objActual.getCopaNvalor6().equals(null)){
				objActual.setCopaNvalor4(Integer.parseInt(objActual.getCopaNvalor4().toString()));
				objActual.setCopaNvalor6(Double.parseDouble(objActual.getCopaNvalor6().toString()));
	
				objActual.setCopaVvalor8("");
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual) ;
					
				servicioParametros.actualizarObjeto(parametro);
				setStrRespuesta("Se modifico con EXITO el registro.");
				
				listaCentroCosto.clear();
				listaCentroCosto.add(objActual);
			} else {
				setStrRespuesta("Ingresar datos validos para Dias Gracia y Diferencia Prima.");
			}
		} catch (Exception e) {
			setStrRespuesta("Error al MODIFICAR registro.");
			e.printStackTrace();
		}
	} 
	
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(listaCentroCosto);
        return lstValues;
    }
	public void guardar() {
		Long secParametro;
		
		try {
			secParametro = servicioParametros.siguente();
			objNuevo.setCopaCdParametro(secParametro);
			objNuevo.setCopaIdParametro("AJUSTE");
			objNuevo.setCopaDesParametro("RECIBO");
			objNuevo.setCopaNvalor1(1);
			objNuevo.setCopaNvalor2(objActual.getCopaNvalor2());
			objNuevo.setCopaNvalor3(objActual.getCopaNvalor3());
			objNuevo.setCopaNvalor4(objNuevo.getCopaNvalor4());
			objNuevo.setCopaNvalor6(objNuevo.getCopaNvalor6());

			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
			this.servicioParametros.guardarObjeto(parametro);
			setStrRespuesta("Se guardo con EXITO el registro.");
			
			objNuevo = new BeanParametros();
			//consultaCentroCostos();
		} catch (Exception e) {
			setStrRespuesta("Error al GUARDAR registro.");
			e.printStackTrace();
		}
	}
}
