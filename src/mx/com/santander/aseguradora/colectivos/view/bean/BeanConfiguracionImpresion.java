package mx.com.santander.aseguradora.colectivos.view.bean;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioConfiguracionImpresion;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

@Controller
@Scope("session")
public class BeanConfiguracionImpresion {
	
	// Servicios, listas, Objetos
	@Resource
	private ServicioConfiguracionImpresion servicioImpresion;
	@Resource
	private ServicioProducto servicioProducto;
	@Resource
	private ServicioPlan servicioPlan;
	@Resource
	private ServicioParametros servicioParametros;
	
	private List<Object> listaProductos;
	private List<Object> listaComboPlanes;
	private List<Object> listaCampos;
	private List<Object> listaNuevos;
	private List<Object> listaConsulta;
	
	private BeanParametros 	objActual;
	
	private File archivo;
	
	// Variables
	private Short ramo;
	private Integer poliza;
	private Integer idVenta;
	private Integer nProducto;
	private Integer nPlan;
	private String strRespuesta;
	private String campoFormulario;
	private String campoBase;
	private int filaActual;
	private String nombreArchivo;
	private Integer muestra;
	private Integer polizaReal;
	private int numPagina;
	private String display1 = "none";
	private String display2 = "none";
	private Boolean plantillaExistente = false;
	private String msjPlantilla = "";
	private String nuevoNombreArchivo;
	

	// Getters and Setters
	public ServicioConfiguracionImpresion getServicioImpresion() {
		return servicioImpresion;
	}

	public void setServicioImpresion(ServicioConfiguracionImpresion servicioImpresion) {
		this.servicioImpresion = servicioImpresion;
	}

	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}

	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}
	
	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}

	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}

	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	public List<Object> getListaProductos() {
		return listaProductos;
	}
	
	public void setListaProductos(List<Object> listaProductos) {
		this.listaProductos = listaProductos;
	}
	
	public List<Object> getListaComboPlanes() {
		return listaComboPlanes;
	}
	
	public void setListaComboPlanes(List<Object> listaComboPlanes) {
		this.listaComboPlanes = listaComboPlanes;
	}

	public List<Object> getListaCampos() {
		return listaCampos;
	}

	public void setListaCampos(List<Object> listaCampos) {
		this.listaCampos = listaCampos;
	}

	public List<Object> getListaNuevos() {
		return listaNuevos;
	}

	public void setListaNuevos(List<Object> listaNuevos) {
		this.listaNuevos = listaNuevos;
	}
	
	public List<Object> getListaConsulta() {
		return listaConsulta;
	}

	public void setListaConsulta(List<Object> listaConsulta) {
		this.listaConsulta = listaConsulta;
	}

	public BeanParametros getObjActual() {
		return objActual;
	}
	
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	
	public File getArchivo() {
		return archivo;
	}
	
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}

	public Short getRamo() {
		cargaProductos();
		return ramo;
	}

	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	
	public Integer getPoliza() {
		return poliza;
	}

	public void setPoliza(Integer poliza) {
		this.poliza = poliza;
	}

	public Integer getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}

	public Integer getnProducto() {
		return nProducto;
	}

	public void setnProducto(Integer nProducto) {
		this.nProducto = nProducto;
	}

	public Integer getnPlan() {
		return nPlan;
	}

	public void setnPlan(Integer nPlan) {
		this.nPlan = nPlan;
	}

	public String getStrRespuesta() {
		return strRespuesta;
	}

	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}

	public String getCampoFormulario() {
		return campoFormulario;
	}

	public void setCampoFormulario(String campoFormulario) {
		this.campoFormulario = campoFormulario;
	}

	public String getCampoBase() {
		return campoBase;
	}

	public void setCampoBase(String campoBase) {
		this.campoBase = campoBase;
	}
	
	public int getFilaActual() {
		return filaActual;
	}
	
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}

	
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public Integer getMuestra() {
		return muestra;
	}

	public void setMuestra(Integer muestra) {
		this.muestra = muestra;
	}

	public Integer getPolizaReal() {
		return polizaReal;
	}

	public void setPolizaReal(Integer polizaReal) {
		this.polizaReal = polizaReal;
	}
	
	public int getNumPagina() {
		return numPagina;
	}
	
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	
	public void setDisplay1(String display1) {
		this.display1 = display1;
	}
	
	public String getDisplay1() {
		return display1;
	}
	
	public void setDisplay2(String display2) {
		this.display2 = display2;
	}
	
	public String getDisplay2() {
		return display2;
	}

	// Metodos
	public BeanConfiguracionImpresion(){
		this.listaProductos	= new ArrayList<Object>();
		this.listaComboPlanes = new ArrayList<Object>();
		this.listaCampos = new ArrayList<Object>();
		this.listaNuevos = new ArrayList<Object>();
		this.listaConsulta = new ArrayList<Object>();
		this.objActual = new BeanParametros();
		ramo = 0;
		poliza = -1;
		idVenta = 0;
		nProducto = 0;
		nPlan = 0;
		strRespuesta = "";
		campoBase = "";
		campoFormulario = "";
		nombreArchivo = "";
		muestra = 0;
		this.numPagina = 1;
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONFIGURACION_IMPRESION);
	}
	
	public String cargaProductos() {
		StringBuilder filtro;
		List<Producto> lista;
		 
		try {
			strRespuesta = "";
			listaProductos.clear();
			nProducto = 0;
			nPlan = 0;
			filtro = new StringBuilder(100);
			filtro.append(" and P.id.alprCdRamo = ").append(this.ramo).append("\n");
			lista = this.servicioProducto.obtenerObjetos(filtro.toString());
			
			for(Producto producto: lista){
				listaProductos.add(new SelectItem(producto.getId().getAlprCdProducto().toString(), producto.getId().getAlprCdProducto().toString() + " - " + producto.getAlprDeProducto()));
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			strRespuesta ="Error al buscar productos";
			return NavigationResults.FAILURE;
		}
	}
	
	public String consultaPlanes() {
		StringBuilder filtro;
		List<Plan> listaPlanes;
		
		try {
			strRespuesta = "";
			listaComboPlanes.clear();
			nPlan = 0;
			filtro = new StringBuilder();
			filtro.append(" and P.id.alplCdRamo = " + ramo + "\n");
			filtro.append(" and P.id.alplCdProducto = " + nProducto + "\n");
			listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
			
			for (Plan plan : listaPlanes) {
				listaComboPlanes.add(new SelectItem(plan.getId().getAlplCdPlan().toString(), plan.getId().getAlplCdPlan().toString() + " - " + plan.getAlplDePlan()));
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			strRespuesta = "Error al buscar planes";
			return NavigationResults.FAILURE;
		}
	}
	
	public void limpiaRespuestas(){
		strRespuesta = "";
	}
	
	public void getCamposBase(){
		BeanParametros param;
		this.listaCampos.clear();
		this.listaNuevos.clear();
		String valida;
		String idParam  = "PLANTILLAS";	
		String desParam = "CERTIFICADO";
		StringBuilder filtro = new StringBuilder();
		
		try {
			strRespuesta = "";
			nombreArchivo = "";
			msjPlantilla = "";
			valida = validaDatos();
			if (valida.equals("0")){
				List<Object[]> campos = servicioImpresion.getCamposBase();
				for (Object[] cargar : campos) {
					param = new BeanParametros();
					
					param.setCopaIdParametro(cargar[2].toString());
					param.setCopaVvalor7(cargar[9].toString());
					if(cargar[14] == null) {
						param.setCopaNvalor4(Constantes.DEFAULT_INT);
					} else {
						param.setCopaNvalor4(Integer.parseInt(cargar[14].toString()));
					}
	
					this.listaCampos.add(new SelectItem(param.getCopaIdParametro().toString() + "-" + param.getCopaVvalor7().toString() + "-" + param.getCopaNvalor4().toString(), param.getCopaVvalor7().toString()));
				}
				
				filtro.append("  where cp.copa_des_parametro = '").append(desParam).append("' \n");
				filtro.append(" and cp.copa_id_parametro = '").append(idParam).append("' \n");
				filtro.append("  AND CP.COPA_NVALOR1 = 1 \n");                    
				filtro.append("  AND CP.COPA_NVALOR2 = ").append(ramo).append(" \n");
				filtro.append("  AND CP.COPA_NVALOR3 = ").append(polizaReal).append(" \n");      
				filtro.append("  AND CP.COPA_VVALOR1 = '").append(nProducto.toString()).append("' \n");        
				filtro.append("  AND CP.COPA_VVALOR2 = '").append(nPlan.toString()).append("' \n");
		
				nombreArchivo = servicioImpresion.existe(filtro.toString());
		
				if(nombreArchivo.equals("0")){
					setNombreArchivo("");
					setDisplay2("block");
					setDisplay1("none");
					plantillaExistente = false;
				} else {
					plantillaExistente = true;
					setDisplay1("block");
					setDisplay2("none");
				}
			} else {
				strRespuesta = valida;
			}
		} catch (Exception e) {
			e.printStackTrace();
			strRespuesta = "Error intente mas tarde";
		}
	}
	
	private String validaDatos() {
		String polizaPU;
		
		try {
			muestra = 0;
			if(ramo == 0){ return "Favor de seleccionar un ramo";}
			if(ramo == 57 || ramo == 58){
				polizaReal = ramo.intValue();
			} else {
				if(poliza == -1){ return "Favor de seleccionar una poliza";}
				polizaReal = poliza;
			}
			if(poliza == 0 && idVenta == 0){return "Debe seleccionar un canal de venta";}
			if(nProducto == 0){ return "Debe seleccionar un producto";}
			if(nPlan == 0){return "Debe seleccionar un plan";}
			if(poliza == 0){
				polizaPU = idVenta + "" + servicioImpresion.getPlazo(ramo, nProducto, nPlan) + "" + nProducto;
				polizaReal = Integer.parseInt(polizaPU);
			}
			muestra = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Constantes.DEFAULT_STRING_ID;
	}
	
	public void agregaTabla(){
		BeanParametros param;
		String carga;
		String[] datos;
		String valida;
		
		
		try {
			msjPlantilla = "";
			valida = validaDatos();
			if(valida.equals("0")){
				if(!campoFormulario.equals("") && !campoBase.equals("0")){
					datos = campoBase.trim().split("-");
					carga = servicioImpresion.insertConfigImpresion(1,ramo,polizaReal,idVenta,nProducto,nPlan,campoFormulario,datos[0]);
					
					if(carga.equals("EXITO")){
						param = new BeanParametros();
						
						param.setCopaVvalor6(campoFormulario);
						param.setCopaIdParametro(datos[0]);
						param.setCopaVvalor7(datos[1]);
						param.setCopaVvalor1(campoBase);
						param.setCopaVvalor2(campoFormulario);
			
						listaNuevos.add(param);
						setCampoFormulario("");
						campoBase = "0";
					} else {
						msjPlantilla = "Fallo al cargar, el campo ya existe";
					}
				} else {
					msjPlantilla = "Debe Ingresar los datos del formulario y de la base";
				}
			} else {
				msjPlantilla = "Para agregar los campos a la tabla, " + valida;
			}
		} catch (Exception e) {
			msjPlantilla = "Error al cargar, el campo ya existe";
			e.printStackTrace();
		}
	}
	
	public void consultaPlantilla(){
		BeanParametros param;
		this.listaConsulta.clear();
		this.listaCampos.clear();
		String valida;
		
		try {
			strRespuesta = "";
			valida = validaDatos();
			if (valida.equals("0")){
				List<Object[]> campos = servicioImpresion.consultaPlantilla(1,ramo,polizaReal,idVenta,nProducto,nPlan);
				for (Object[] cargar : campos) {
					param = new BeanParametros();
					
					param.setCopaDesParametro("Prueba");
					param.setCopaVvalor6(cargar[0].toString());
					param.setCopaVvalor7(cargar[1].toString());
					param.setCopaIdParametro(cargar[2].toString());
					param.setCopaVvalor1(cargar[2].toString() + "-" + cargar[1].toString());
					param.setCopaVvalor2(cargar[0].toString());

					listaConsulta.add(param);
				}
				List<Object[]> combo = servicioImpresion.getCamposBase();
				for (Object[] cargar : combo) {
					param = new BeanParametros();
					
					param.setCopaIdParametro(cargar[2].toString());
					param.setCopaVvalor7(cargar[9].toString());
					if(cargar[14] == null) {
						param.setCopaNvalor4(Constantes.DEFAULT_INT);
					} else {
						param.setCopaNvalor4(Integer.parseInt(cargar[14].toString()));
					}
					
					this.listaCampos.add(new SelectItem(param.getCopaIdParametro().toString() + "-" + param.getCopaVvalor7().toString() + "-" + param.getCopaNvalor4().toString(), param.getCopaVvalor7().toString()));
				}
			} else {
				strRespuesta = valida;
			}
		} catch (Exception e) {
			e.printStackTrace();
			strRespuesta = "Error intente mas tarde";
		}
	}
	
	public void buscarFilaActual(ActionEvent event) {
	  String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
	  filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
	  Iterator<Object> it =  getListaConsulta().iterator();
	  while(it.hasNext()) {
	  	BeanParametros objParametro = (BeanParametros) it.next();
	  	if (objParametro.getCopaVvalor1().toString().equals(clave)){
	          objActual = objParametro;
	          break;
	      }
	  }
	}
	
	public void modificar(){
		String[] datos;
		
		try {
			strRespuesta = "";
			if(!objActual.getCopaVvalor1().equals("0") && !objActual.getCopaVvalor2().equals("")){
				datos = objActual.getCopaVvalor1().trim().split("-");
				strRespuesta = servicioImpresion.actualizaDatos(1,ramo,polizaReal,idVenta,nProducto,nPlan,objActual.getCopaVvalor2(),datos[0],objActual.getCopaIdParametro());
				consultaPlantilla();
			} else {
				strRespuesta = "No se modifico debe Ingresar todos los datos.";
			}
		} catch (Exception e) {
			strRespuesta = "Error al modificar";
			e.printStackTrace();
		}
	}
	
	public String guardarPlantilla(){
		Long next;
		String respuesta = "";
		String valida;
		String idParam  = "PLANTILLAS";	
		String desParam = "CERTIFICADO";

		try {			
			msjPlantilla = "";
			valida = validaDatos();
			
			if (valida.equals("0")){
				next = this.servicioParametros.siguienteCdParam();
				if (next!= 0 ){
					if(!(nombreArchivo.equals("") || (nombreArchivo.length() > 4 && !nombreArchivo.trim().endsWith(".pdf")))){
						this.servicioImpresion.guardaPlantilla(next, idParam, desParam, 1, this.ramo, this.polizaReal, this.nProducto, this.nPlan, nombreArchivo);
						msjPlantilla = "La plantilla se guardo con exito";
						setDisplay1("block");
						setDisplay2("none");
						plantillaExistente = true;
					} else {
						msjPlantilla = "El nombre de la plantilla debe contener al menos un caracter y debe tener la extension '.pdf'";
					}
				}
			} else {
				msjPlantilla = valida;
			}
		} catch (Exception e) {
			msjPlantilla ="Error al guardar";
			e.printStackTrace();
		}
		return respuesta;
	}
	
	
	public void editarNombrePlantilla() {
		final String idParam  = "PLANTILLAS";	
		final String desParam = "CERTIFICADO";
		Map<String, String> filtro = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
		{
			put("COPA_DES_PARAMETRO", desParam);
			put("COPA_ID_PARAMETRO", idParam);
			put("COPA_NVALOR1", "1");
			put("COPA_NVALOR2", String.valueOf(ramo));
			put("COPA_NVALOR3", String.valueOf(polizaReal));
			put("COPA_VVALOR1", String.valueOf(nProducto));
			put("COPA_VVALOR2", String.valueOf(nPlan));
		}};
		
		if(nuevoNombreArchivo.equals("") || (nuevoNombreArchivo.length() > 4 && !nuevoNombreArchivo.trim().endsWith(".pdf"))){
			msjPlantilla = "El nombre de la plantilla debe contener al menos un caracter y debe tener la extension '.pdf'";
			return;
		}
		
		msjPlantilla = servicioImpresion.editarNombrePlantilla(nuevoNombreArchivo, filtro);

		if("0".equals(msjPlantilla.substring(0, 1))){
			setDisplay2("block");
			setDisplay1("none");
		} else {
			nombreArchivo = nuevoNombreArchivo;
			setDisplay1("block");
			setDisplay2("none");
		}
		msjPlantilla = msjPlantilla.substring(1);
	}
	
	public void borrarPlantilla() {
		final String idParam  = "PLANTILLAS";	
		final String desParam = "CERTIFICADO";
		Map<String, String> filtro = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
		{
			put("COPA_DES_PARAMETRO", desParam);
			put("COPA_ID_PARAMETRO", idParam);
			put("COPA_NVALOR1", "1");
			put("COPA_NVALOR2", String.valueOf(ramo));
			put("COPA_NVALOR3", String.valueOf(polizaReal));
			put("COPA_VVALOR1", String.valueOf(nProducto));
			put("COPA_VVALOR2", String.valueOf(nPlan));
		}};
		
		msjPlantilla = servicioImpresion.borrarPlantilla(nombreArchivo, filtro);

		if("0".equals(msjPlantilla.substring(0, 1))){
			setDisplay1("block");
			setDisplay2("none");
			setPlantillaExistente(true);
		} else {
			nombreArchivo = "";
			setDisplay2("block");
			setDisplay1("none");
			setPlantillaExistente(false);
			
		}
		msjPlantilla = msjPlantilla.substring(1);
	}
	

	public Boolean getPlantillaExistente() {
		return plantillaExistente;
	}

	public void setPlantillaExistente(Boolean plantillaExistente) {
		this.plantillaExistente = plantillaExistente;
	}

	public String getMsjPlantilla() {
		return msjPlantilla;
	}

	public void setMsjPlantilla(String msjPlantilla) {
		this.msjPlantilla = msjPlantilla;
	}

	public String getNuevoNombreArchivo() {
		return nuevoNombreArchivo;
	}

	public void setNuevoNombreArchivo(String nuevoNombreArchivo) {
		this.nuevoNombreArchivo = nuevoNombreArchivo;
	}
}
