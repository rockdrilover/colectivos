/**
 * Clase BeanListaClienteCertif utilizada como controller de detalle de certificados
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanListaClienteCertif extends BeanListaClienteCertif2 {

	@Resource
	private ServicioClienteCertif servicioCliente;
	@Resource
	private ServicioEndosoDatos servicioEndosoDatos;

	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private List<Object> listaClientesCertificados;
	private List<BeanClienteCertif> listaCreditos;
	private List<BeanClienteCertif> listaCertificados;
	private List<BeanClienteCertif> listaCoberturas;
	private List<BeanClienteCertif> listaAsistencias;
	
	private short canal;
	private Short ramo;
	private Long poliza;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String idCertificado;
	private String credito2;
	private String respuesta;
	private int numPagina;
	private String p1;
	private String p2;
	private String p3;
    private String id;
	
    private EndosoDatosDTO seleccionado;
    
	/**
	 * @return the servicioCliente
	 */
	public ServicioClienteCertif getServicioCliente() {
		return servicioCliente;
	}
	/**
	 * @param servicioCliente the servicioCliente to set
	 */
	public void setServicioCliente(ServicioClienteCertif servicioCliente) {
		this.servicioCliente = servicioCliente;
	}
	/**
	 * @return the canal
	 */
	public short getCanal() {
		return canal;
	}
	/**
	 * @param canal the canal to set
	 */
	public void setCanal(short canal) {
		this.canal = canal;
	}
	/**
	 * @return the ramo
	 */
	public Short getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	/**
	 * @return the poliza
	 */
	public Long getPoliza() {
		return poliza;
	}
	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * @return the idCertificado
	 */
	public String getIdCertificado() {
		return idCertificado;
	}
	/**
	 * @param idCertificado the idCertificado to set
	 */
	public void setIdCertificado(String idCertificado) {
		this.idCertificado = idCertificado;
	}
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}
	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}
	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	
	public String getP1() {
		return p1;
	}
	public void setP1(String p1) {
		this.p1 = p1;
	}
	public String getP2() {
		return p2;
	}
	public void setP2(String p2) {
		this.p2 = p2;
	}
	public String getP3() {
		return p3;
	}
	public void setP3(String p3) {
		this.p3 = p3;
	}
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public List<BeanClienteCertif> getListaCreditos() {
		return listaCreditos;
	}
	
	/**
	 * @param listaClientesCertificados the listaClientesCertificados to set
	 */
	public void setListaClientesCertificados(
			List<Object> listaClientesCertificados) {
		this.listaClientesCertificados = listaClientesCertificados;
	}
	/**
	 * @return the listaClientesCertificados
	 */
	public List<Object> getListaClientesCertificados() {
		return listaClientesCertificados;
	}
	
	public List<BeanClienteCertif> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setListaCoberturas(List<BeanClienteCertif> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public List<BeanClienteCertif> getListaCertificados() {
		return listaCertificados;
	}
	public void setListaCertificados(List<BeanClienteCertif> listaCertificados) {
		this.listaCertificados = listaCertificados;
	}
	public List<BeanClienteCertif> getListaAsistencias() {
		return listaAsistencias;
	}
	public void setListaAsistencias(List<BeanClienteCertif> listaAsistencias) {
		this.listaAsistencias = listaAsistencias;
	}
	
	public String getCredito2() {
		return credito2;
	}
	
	public void setCredito2(String credito2) {
		this.credito2 = credito2;
	}
	/**
	 * Metodo constructor de la clase BeanListaClienteCertif
	 */
	public BeanListaClienteCertif() {
		this.message = new StringBuilder();
		this.canal = 1;
		this.listaClientesCertificados = new ArrayList<Object>();
		this.listaCreditos = new ArrayList<BeanClienteCertif>();
		this.listaCertificados = new ArrayList<BeanClienteCertif>();
		this.listaCoberturas = new ArrayList<BeanClienteCertif>();
		this.listaAsistencias = new ArrayList<BeanClienteCertif>();
		
		setIdCertificado("");
		setNombre("");
		setApellidoPaterno("");
		setApellidoMaterno("");
		setRespuesta("");

		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_CLIENTE_CERTIF);
		
	}
	/**
	 * Metodo de armado de filtros de consulta de certificados
	 * @return String que funcionara como un filtro en un query
	 */
	public String consulta(){
		
		StringBuilder filtro = new StringBuilder();
		this.listaClientesCertificados.clear();
		
		try {
			
			filtro.append(" and CC.id.coccCasuCdSucursal = ").append(this.canal).append(" \n");
			
			if(this.ramo != null && this.ramo.intValue() > 0){
				filtro.append(" and CC.id.coccCarpCdRamo = ").append(this.ramo).append(" \n");
			}
			
			if(this.poliza != null && this.poliza.intValue() > 0){
				filtro.append(" and CC.id.coccCapoNuPoliza = ").append(this.poliza).append(" \n");
			}
			
			if(this.idCertificado != null && this.idCertificado.length() > 0){
				filtro.append(" and (CC.id.coccIdCertificado = '").append(this.idCertificado).append("'");
				filtro.append(" or CC.certificado.coceBucEmpresa = '").append(this.idCertificado).append("'");
				filtro.append(" )");
			}
			
			//if(this.idCertificado != null && this.idCertificado.length() > 0){
			//	filtro.append(" and CC.id.coccIdCertificado = ").append(this.idCertificado);
			//}
			
			if(this.nombre != null && this.nombre.length() > 0){
				filtro.append(" and CC.cliente.cocnNombre = '").append(this.nombre.toUpperCase()).append("' \n");
			}
			
			if(this.apellidoPaterno != null && this.apellidoPaterno.length() >0){
				filtro.append(" and CC.cliente.cocnApellidoPat = '").append(this.apellidoPaterno.toUpperCase()).append("' \n");
			}
			
			if(this.apellidoMaterno != null && this.apellidoMaterno.length() > 0){
				filtro.append(" and CC.cliente.cocnApellidoMat = '").append(this.apellidoMaterno.toUpperCase()).append("' \n");
			}
			
//			filtro.append(" and nvl(CC.coccTpCliente,1) = 1 \n");
			
			List<ClienteCertif> lista = this.servicioCliente.obtenerObjetos(filtro.toString());
			
			for(ClienteCertif clienteCertifi: lista){
			
				BeanClienteCertif bean = (BeanClienteCertif) ConstruirObjeto.crearBean(BeanClienteCertif.class, clienteCertifi);
				bean.setServicioCliente(this.servicioCliente);
				
				this.listaClientesCertificados.add(bean);
			}
			
		} catch (Exception e) {
			this.message.append("Error al recuperar los certificados.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		return apellidoMaterno;
	}
	/**
	 * Metodo de llamado a servicios de credito
	 * @return String que se�ala que termino el proceso
	 */
	public String consultaCred(){
		this.listaCreditos.clear();
		this.listaCertificados.clear();
		this.listaCoberturas.clear();
		this.listaAsistencias.clear();
		
		Double tarifaTotal;
		
		String certs = "";
		int num = 0;
		setRespuesta("");
		try {
			if (idCertificado.equals("") && nombre.equals("") && apellidoPaterno.equals("") && apellidoMaterno.equals("")){
				this.message.append("Favor de ingresar datos e intentar nuevamente");
	            this.log.debug(this.message.toString());
	            this.respuesta = this.message.toString();
	            Utilerias.resetMessage(message);
			}
			else{
				List<Object[]> lista= this.servicioCliente.consultaCredCte(idCertificado, nombre, apellidoPaterno, apellidoMaterno);

				for(Object[] credito: lista){
					BeanClienteCertif beanCte = new BeanClienteCertif();
					beanCte.setCertificado(new Certificado());
					beanCte.setCliente(new Cliente());
					
					beanCte.getCertificado().setCoceNuCredito(credito[0].toString());
					beanCte.getCertificado().setCoceBucEmpresa(credito[1].toString());
					beanCte.getCliente().setCocnNombre(credito[2].toString());
					beanCte.getCliente().setCocnApellidoPat(credito[3].toString());
					beanCte.getCliente().setCocnApellidoMat(credito[4].toString());
					beanCte.getCliente().setCocnCdSexo(credito[5].toString());
					beanCte.getCliente().setCocnFeNacimiento((Date) credito[6]);
					beanCte.getCliente().setCocnRfc(credito[7].toString());
					beanCte.getCliente().setCocnCalleNum(credito[8].toString());
					beanCte.getCliente().setCocnColonia(credito[9].toString());
					beanCte.getCliente().setCocnDelegmunic(credito[10].toString());
					beanCte.getCliente().setCocnCampov1(credito[11].toString());
					beanCte.getCliente().setCocnCdPostal(credito[12].toString());
					beanCte.getCliente().setCocnNuLada(credito[13].toString());
					beanCte.getCliente().setCocnNuTelefono(credito[14].toString());
					beanCte.getCliente().setCocnBucCliente(credito[15].toString());
					beanCte.getCliente().setCocnNuCliente(Long.valueOf(credito[16].toString()));
					beanCte.getCertificado().setCoceNuCuenta(credito[17].toString());
					if (num==0)
						certs="'" + credito[0].toString() + "'";
					else
						certs=certs + "," + "'" + credito[0].toString() + "'";
					num++;
					beanCte.setServicioCliente(this.servicioCliente);
					this.listaCreditos.add(beanCte);
				}
				
				List<Object[]> lista1= this.servicioCliente.consultaCredCert(certs);
				for(Object[] certificado: lista1){
					tarifaTotal = 0d;
					
					BeanClienteCertif beanCert = new BeanClienteCertif();
					beanCert.setCertificado(new Certificado());
					beanCert.getCertificado().setId(new CertificadoId());
					
					beanCert.setCliente(new Cliente());
					beanCert.setId(new ClienteCertifId( ((BigDecimal)certificado[29]).longValue(),
														((BigDecimal)certificado[1]).shortValue(),
														((BigDecimal)certificado[2]).byteValue(),
														((BigDecimal)certificado[3]).longValue(),
														((BigDecimal)certificado[4]).longValue(),
														Constantes.DEFAULT_STRING
														));
					
					beanCert.getCertificado().setCoceNuCredito(certificado[0].toString());
					beanCert.getCertificado().getId().setCoceCasuCdSucursal(((BigDecimal)certificado[1]).shortValue());
					beanCert.getCertificado().getId().setCoceCarpCdRamo(((BigDecimal)certificado[2]).shortValue());
					beanCert.getCertificado().getId().setCoceCapoNuPoliza(((BigDecimal)certificado[3]).longValue());
					beanCert.getCertificado().getId().setCoceNuCertificado(((BigDecimal)certificado[4]).longValue());
					beanCert.getCertificado().setCoceCampon1(Long.valueOf(certificado[5].toString()));
					beanCert.getCertificado().setCoceCampov1(certificado[6].toString());
					beanCert.getCertificado().setCoceCampov2(certificado[7].toString());
					beanCert.getCertificado().setCoceCdCausaAnulacion(((BigDecimal)certificado[8]).shortValue());
					beanCert.getCertificado().setCoceCampov3(certificado[9].toString());
					beanCert.getCertificado().setCoceFeAnulacionCol((Date) certificado[10]);
					beanCert.getCertificado().setCoceTpProductoBco(certificado[11].toString());
					beanCert.getCertificado().getPlanes().getProducto().getId().setAlprCdProducto(((BigDecimal)certificado[11]).intValue());
					beanCert.getCertificado().setCoceTpSubprodBco(certificado[12].toString());
					beanCert.getCertificado().getPlanes().getId().setAlplCdPlan(((BigDecimal)certificado[12]).intValue());
					beanCert.getCertificado().setCoceFeCarga((Date) certificado[13]);
					beanCert.getCertificado().setCoceFeSuscripcion((Date) certificado[14]);
					beanCert.getCertificado().setCoceMtSumaAsegurada(new BigDecimal(certificado[15].toString()));
					beanCert.getCertificado().setCoceMtSumaAsegSi((BigDecimal)certificado[16]);
					beanCert.getCertificado().setCoceMtPrimaSubsecuente((BigDecimal)certificado[17]);
					beanCert.getCertificado().setCoceMtPrimaPura((BigDecimal)certificado[18]);
					beanCert.getCertificado().setCoceFeDesde((Date) certificado[19]);
					beanCert.getCertificado().setCoceFeHasta((Date) certificado[20]);
					beanCert.getCertificado().setCoceFeIniCredito((Date) certificado[21]);
					beanCert.getCertificado().setCoceFeFinCredito((Date) certificado[22]);
					beanCert.getCertificado().setCoceNoRecibo((BigDecimal)certificado[23]);
					beanCert.getCertificado().setCoceCampov4(certificado[24].toString());
					beanCert.getCertificado().setCoceCazbCdSucursal(certificado[25] == null? null:((BigDecimal)certificado[25]).intValue());
					beanCert.getCertificado().setCoceFeEmision((Date) certificado[26]);
					beanCert.getCertificado().setCoceEmpresa(certificado[27] == null? null: certificado[27].toString());
					beanCert.getCertificado().setCoceSubCampana((certificado[28] == null ? Constantes.DEFAULT_STRING_ID : certificado[28].toString()));
					
					beanCert.setServicioCliente(this.servicioCliente);
					beanCert.getLstAsistencias().addAll(this.servicioCliente.consultaCredAsis(beanCert.getCertificado().getCoceNuCredito(),
							  beanCert.getCertificado().getId().getCoceCasuCdSucursal(),
							  beanCert.getCertificado().getId().getCoceCarpCdRamo(),
							  beanCert.getCertificado().getId().getCoceCapoNuPoliza(),
							  beanCert.getCertificado().getId().getCoceNuCertificado()));
					beanCert.getLstCoberturas().addAll(this.servicioCliente.consultaCredCobs(beanCert.getCertificado().getCoceNuCredito(),
																							 beanCert.getCertificado().getId().getCoceCasuCdSucursal(),
																							 beanCert.getCertificado().getId().getCoceCarpCdRamo(),
																							 beanCert.getCertificado().getId().getCoceCapoNuPoliza(),
																							 beanCert.getCertificado().getId().getCoceNuCertificado()));
					
					
					for (int j=0; j<beanCert.getLstCoberturas().size(); j++){
						tarifaTotal += ((BeanClienteCertif)beanCert.getLstCoberturas().get(j)).getCertificado().getCoceCampon4().doubleValue();
					}
					
					beanCert.getCertificado().setCoceTpPma(tarifaTotal.toString());
					
					beanCert.setComplemento(this.servicioCliente.consultaCredComplementos(beanCert.getCertificado().getCoceNuCredito(),
							  beanCert.getCertificado().getId().getCoceCasuCdSucursal(),
							  beanCert.getCertificado().getId().getCoceCarpCdRamo(),
							  beanCert.getCertificado().getId().getCoceCapoNuPoliza(),
							  beanCert.getCertificado().getId().getCoceNuCertificado()));
					
					beanCert.getLstAsegurados().addAll(this.servicioCliente.consultaCredAsegurados(beanCert.getCertificado().getCoceNuCredito(),
							  beanCert.getCertificado().getId().getCoceCasuCdSucursal(),
							  beanCert.getCertificado().getId().getCoceCarpCdRamo(),
							  beanCert.getCertificado().getId().getCoceCapoNuPoliza(),
							  beanCert.getCertificado().getId().getCoceNuCertificado()));
					
					beanCert.getLstBeneficiarios().addAll(this.servicioEndosoDatos.getBeneficiarios(beanCert.getCertificado().getId()));
					
					beanCert.setLstEndosos(this.servicioCliente.getEndosos(
								beanCert.getCertificado().getId().getCoceCasuCdSucursal(),
								beanCert.getCertificado().getId().getCoceCarpCdRamo(),
								beanCert.getCertificado().getId().getCoceCapoNuPoliza(),
								beanCert.getCertificado().getId().getCoceNuCertificado()));
					
					this.listaCertificados.add(beanCert);

				}
			}
			setIdCertificado("");
			setNombre("");
			setApellidoPaterno("");
			setApellidoMaterno("");
			return null;
		} catch (Exception e) {
			this.message.append("Error al recuperar certificados para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	/**
	 * Metodo get del servicio de Endoso Datos
	 * @return el objeto servicioEndosoDatos
	 */
	public ServicioEndosoDatos getServicioEndosoDatos() {
		return servicioEndosoDatos;
	}
	/**
	 * Metodo set del servicio Endoso Datos
	 * @param servicioEndosoDatos
	 */
	public void setServicioEndosoDatos(ServicioEndosoDatos servicioEndosoDatos) {
		this.servicioEndosoDatos = servicioEndosoDatos;
	}
	/**
	 * Metodo get de seleccion de endoso en front
	 * @return DTO seleccionado
	 */
	public EndosoDatosDTO getSeleccionado() {
		return seleccionado;
	}
	/**
	 * Metodo de seteo de seleccino de endoso en front
	 * @param DTO seleccionado
	 */
	public void setSeleccionado(EndosoDatosDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
}
