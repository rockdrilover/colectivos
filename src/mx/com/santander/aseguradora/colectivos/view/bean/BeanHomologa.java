package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.HomologaProductosPlanes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.HomologaProductosPlanesId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHomologaProdPlanes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("request")
public class BeanHomologa {

	@Resource
	private ServicioHomologaProdPlanes servicioHomologa;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	//-- HOMOLOGA
	private HomologaProductosPlanesId id;
	private Plan plan;
	private String hoppDeProducto;
	private BigDecimal hoppSumaAsegurada;
	private String hoppDato1;
	private String hoppDato2;
	private Integer hoppDato3;
	private Long hoppDato4;
	private Date hoppDato5;
	private Date hoppDato6;
	private String hoppCdComision;
	private BigDecimal hoppPorcComision;
	
	private List<Object> listaComboComponentes;
	private List<HomologaProductosPlanes> listaHomologa;
	
	private Integer idVenta;//idVenta
	private	Integer plazo;	//plazo
	private String descProducto;
	private String descPlan;

	private String valComboCobertura;
	
	public BeanHomologa() {
		
		this.id						= new HomologaProductosPlanesId();
		this.listaComboComponentes 	= new ArrayList<Object>();
		this.listaHomologa 			= new ArrayList<HomologaProductosPlanes>();
		
	}
	
	public BeanHomologa(String[] arrDatos) {
		this.id = new HomologaProductosPlanesId();
		this.id.setHoppCdRamo(Byte.parseByte(arrDatos[0]));
		this.id.setHoppProdBancoRector(arrDatos[1]);
		this.id.setHoppPlanBancoRector(arrDatos[2]);
		this.id.setHoppProdColectivo(Integer.parseInt(arrDatos[4]));
		this.id.setHoppPlanColectivo(Integer.parseInt(arrDatos[5]));
		this.hoppDeProducto = arrDatos[3];
		this.hoppDato1 = arrDatos[6];
		this.hoppDato3 = Integer.parseInt(arrDatos[7]);
		this.hoppDato4 = Long.parseLong(arrDatos[8]);
		
	}

	public ServicioHomologaProdPlanes getServicioHomologa() {
		return servicioHomologa;
	}
	public void setServicioHomologa(ServicioHomologaProdPlanes servicioHomologa) {
		this.servicioHomologa = servicioHomologa;
	}
	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	public HomologaProductosPlanesId getId() {
		return id;
	}
	public void setId(HomologaProductosPlanesId id) {
		this.id = id;
	}
	public Plan getPlan() {
		return plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	public String getHoppDeProducto() {
		return hoppDeProducto;
	}
	public void setHoppDeProducto(String hoppDeProducto) {
		this.hoppDeProducto = hoppDeProducto;
	}
	public BigDecimal getHoppSumaAsegurada() {
		return hoppSumaAsegurada;
	}
	public void setHoppSumaAsegurada(BigDecimal hoppSumaAsegurada) {
		this.hoppSumaAsegurada = hoppSumaAsegurada;
	}
	public String getHoppDato1() {
		return hoppDato1;
	}
	public void setHoppDato1(String hoppDato1) {
		this.hoppDato1 = hoppDato1;
	}
	public String getHoppDato2() {
		return hoppDato2;
	}
	public void setHoppDato2(String hoppDato2) {
		this.hoppDato2 = hoppDato2;
	}
	public Integer getHoppDato3() {
		return hoppDato3;
	}
	public void setHoppDato3(Integer hoppDato3) {
		this.hoppDato3 = hoppDato3;
	}
	public Long getHoppDato4() {
		return hoppDato4;
	}
	public void setHoppDato4(Long hoppDato4) {
		this.hoppDato4 = hoppDato4;
	}
	public Date getHoppDato5() {
		return hoppDato5;
	}
	public void setHoppDato5(Date hoppDato5) {
		this.hoppDato5 = hoppDato5;
	}
	public Date getHoppDato6() {
		return hoppDato6;
	}
	public void setHoppDato6(Date hoppDato6) {
		this.hoppDato6 = hoppDato6;
	}
	public String getHoppCdComision() {
		return hoppCdComision;
	}
	public void setHoppCdComision(String hoppCdComision) {
		this.hoppCdComision = hoppCdComision;
	}
	public BigDecimal getHoppPorcComision() {
		return hoppPorcComision;
	}
	public void setHoppPorcComision(BigDecimal hoppPorcComision) {
		this.hoppPorcComision = hoppPorcComision;
	}
	public List<Object> getListaComboComponentes() {
		return listaComboComponentes;
	}
	public void setListaComboComponentes(List<Object> listaComboComponentes) {
		this.listaComboComponentes = listaComboComponentes;
	}
	public List<HomologaProductosPlanes> getListaHomologa() {
		return listaHomologa;
	}
	public void setListaHomologa(List<HomologaProductosPlanes> listaHomologa) {
		this.listaHomologa = listaHomologa;
	}
	public Integer getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public String getDescProducto() {
		return descProducto;
	}
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	public String getDescPlan() {
		return descPlan;
	}
	public void setDescPlan(String descPlan) {
		this.descPlan = descPlan;
	}
	public String getValComboCobertura() {
		return valComboCobertura;
	}



	//ACCIONES
	public String consultaHomologa(){

		StringBuilder filtro = new StringBuilder(100);
		
		try {
	
			filtro.append(" and HP.id.hoppCdRamo 		= ").append(getId().getHoppCdRamo());
			filtro.append("	and HP.id.hoppProdColectivo 	= ").append(getId().getHoppProdColectivo());
			filtro.append("	and HP.id.hoppPlanColectivo 	= ").append(getId().getHoppPlanColectivo());
			filtro.append("	and HP.hoppDato3 			= ").append(getHoppDato3());
			filtro.append(" ");
			
			List<HomologaProductosPlanes> listaHomologa = this.servicioHomologa.obtenerObjetos(filtro.toString());
			
			setListaHomologa(listaHomologa);
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	
	public String guardaHomologa(){
		
		try {
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0); 
			HomologaProductosPlanes homologa =(HomologaProductosPlanes)ConstruirObjeto.crearObjeto(HomologaProductosPlanes.class, this);
			
			this.servicioHomologa.guardarObjeto(homologa);
			this.consultaHomologa();

			getId().setHoppProdBancoRector(null);
			getId().setHoppPlanBancoRector(null);
			setHoppDato1(null);
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

}
