package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioConciliacion;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanConsultaConciliacion {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioConciliacion servicioConciliacion;
	
	private Integer nOrigen;
	private List<Object> lstComboOrigen; 
	private Integer nTipoRep;
	private List<Object> lstComboTipoRep; 
	private Date fecha;
	private Date fechaFin;
	private List<Object> lstResultados;
	private List<Object> lstEmitir;
	private List<Object> lstDatosArchivo;
	private StringBuilder message;
	private String[] arrNomColumnas;

	public BeanConsultaConciliacion() {
		this.message = new StringBuilder();
		setLstComboOrigen(new ArrayList<Object>());
		setLstComboTipoRep(new ArrayList<Object>());
		setLstResultados(new ArrayList<Object>());
		setLstEmitir(new ArrayList<Object>());
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONSULTA_CONCILIACION);
		
	}
	
	@SuppressWarnings("unchecked")
	public String consulta(){
		ArrayList<Object> arlResultados;
		Iterator<Object> it;
		Resultado objResultado;
		String strDatos;
		
		try {
			getMessage().delete(0, getMessage().length());
			setLstResultados(new ArrayList<Object>());
			setLstEmitir(new ArrayList<Object>());
			
			if(getnOrigen().equals(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR) || getnOrigen().equals(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR) || 
					getnOrigen().equals(Constantes.CONCILIACION_ORIGEN_LE_VALOR) || getnOrigen().equals(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR)){
				strDatos = "9|Desempleo|desempleo.png";
			} else if(getnOrigen().equals(Constantes.CONCILIACION_ORIGEN_GAP_VALOR)) {
				strDatos = "89|GAP Colectivo|gap.png";
			} else {
				strDatos = "0|a|vida.png";
			}
				
			arlResultados = getServicioConciliacion().consultar(getnOrigen(), getnTipoRep(), getFecha(), getFechaFin(), 0, strDatos);
			arrNomColumnas = (String[]) arlResultados.get(1);
			lstDatosArchivo = (ArrayList<Object>) arlResultados.get(0);
			
			it = lstDatosArchivo.iterator();
			while(it.hasNext()){
				objResultado = new Resultado((Object[])it.next(), getnTipoRep(), getnOrigen());
				lstResultados.add(objResultado);
			}

			if(getnTipoRep().equals(Constantes.TIPO_REPORTE_VENTAS_PREEMISION_VALOR) || getnTipoRep().equals(Constantes.TIPO_REPORTE_VENTAS_EMISION_VALOR) || getnTipoRep().equals(Constantes.TIPO_REPORTE_EMITIDAS_VALOR)){
				arlResultados = getServicioConciliacion().consultar(getnOrigen(), getnTipoRep(), getFecha(), getFechaFin(), 1, strDatos);
				arrNomColumnas = (String[]) arlResultados.get(1);
				lstDatosArchivo = (ArrayList<Object>) arlResultados.get(0);
				
				it = lstDatosArchivo.iterator();
				while(it.hasNext()){
					objResultado = new Resultado((Object[])it.next(), Constantes.TIPO_REPORTE_VENTAS_LISTOEMITIR_DETALLE, getnOrigen());
					lstEmitir.add(objResultado);
				}
			}
			
			getMessage().append("Se ejecuto con exito la consulta.");
			log.info(getMessage().toString());
		} catch (Exception e) {
			getMessage().append("Error al recuperar los resultados.");
			log.error(getMessage().toString(), e);
			return  NavigationResults.FAILURE;
		}
		
		return  NavigationResults.SUCCESS;
	}
	
	public void descargar(){
		String strNombre = null, strRutaTemp;
		Archivo objArchivo;
		
		try {
			getMessage().delete(0, getMessage().length());
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			strNombre = "Detalle Resultados - " + new SimpleDateFormat("ddMMyyyy").format(new Date()); 
			
			objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
			objArchivo.copiarArchivo(arrNomColumnas, getLstDatosArchivo(), "|");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
			objArchivo.eliminarArchivo();
			
			getMessage().append("Archivo descargado satisfactoriamente.");
			log.error(getMessage().toString());
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
			getMessage().append("Error al descargar informacion.");
			log.error(getMessage().toString(), e);
		}
	}
	
	public Integer getnOrigen() {
		return nOrigen;
	}
	public void setnOrigen(Integer nOrigen) {
		this.nOrigen = nOrigen;
	}
	
	public List<Object> getLstComboOrigen() {
		lstComboOrigen.clear();
		lstComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR, Constantes.CONCILIACION_ORIGEN_ALTAMIRA_DESC));
		lstComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR, Constantes.CONCILIACION_ORIGEN_ALTAIR_DESC));
		lstComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR, Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_DESC));
		lstComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_LCI_VALOR, Constantes.CONCILIACION_ORIGEN_LCI_DESC));
		lstComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_GAP_VALOR, Constantes.CONCILIACION_ORIGEN_GAP_DESC));
		lstComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_LE_VALOR, Constantes.CONCILIACION_ORIGEN_LE_DESC));
		lstComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR, Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_DESC));
		
		return lstComboOrigen;
	}
	
	public void setLstComboOrigen(List<Object> lstComboOrigen) {
		this.lstComboOrigen = lstComboOrigen;
	}
	public Integer getnTipoRep() {
		return nTipoRep;
	}
	public void setnTipoRep(Integer nTipoRep) {
		this.nTipoRep = nTipoRep;
	}
	
	public List<Object> getLstComboTipoRep() {
		lstComboTipoRep.clear();
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_DEPOSITOS_NOCONCILIADO_VALOR, Constantes.TIPO_REPORTE_DEPOSITOS_NOCONCILIADO_DESC));
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_VENTAS_NOCONCILIADO_VALOR, Constantes.TIPO_REPORTE_VENTAS_NOCONCILIADO_DESC));
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_ERRORES_CONCI_VALOR, Constantes.TIPO_REPORTE_ERRORES_CONCI_DESC));
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_BAJAS_VALOR, Constantes.TIPO_REPORTE_BAJAS_DESC));
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_VENTAS_PREEMISION_VALOR, Constantes.TIPO_REPORTE_VENTAS_PREEMISION_DESC));
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_VENTAS_EMISION_VALOR, Constantes.TIPO_REPORTE_VENTAS_EMISION_DESC));
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_EMITIDAS_VALOR, Constantes.TIPO_REPORTE_EMITIDAS_DESC));
		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_ERRORES_EMISI_VALOR, Constantes.TIPO_REPORTE_ERRORES_EMISI_DESC));
		
//		lstComboTipoRep.add(new SelectItem(Constantes.TIPO_REPORTE_VENTAS_CONCILIADO_ERROR_VALOR, Constantes.TIPO_REPORTE_VENTAS_CONCILIADO_ERROR_DESC));
		return lstComboTipoRep;
	}
	
	public void setLstComboTipoRep(List<Object> lstComboTipoRep) {
		this.lstComboTipoRep = lstComboTipoRep;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<Object> getLstResultados() {
		return lstResultados;
	}
	public void setLstResultados(List<Object> lstResultados) {
		this.lstResultados = lstResultados;
	}
	public ServicioConciliacion getServicioConciliacion() {
		return servicioConciliacion;
	}
	public void setServicioConciliacion(ServicioConciliacion servicioConciliacion) {
		this.servicioConciliacion = servicioConciliacion;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	public List<Object> getLstEmitir() {
		return lstEmitir;
	}
	public void setLstEmitir(List<Object> lstEmitir) {
		this.lstEmitir = lstEmitir;
	}
	public String[] getArrNomColumnas() {
		return arrNomColumnas;
	}
	public void setArrNomColumnas(String[] arrNomColumnas) {
		this.arrNomColumnas = arrNomColumnas;
	}
	public List<Object> getLstDatosArchivo() {
		return lstDatosArchivo;
	}
	public void setLstDatosArchivo(List<Object> lstDatosArchivo) {
		this.lstDatosArchivo = lstDatosArchivo;
	}
}
