package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/*
 * Creado por : Leobardo Preciado
 * 
 */
 
@Controller
@Scope("session")
public class BeanPlan {

	@Resource
	private ServicioPlan servicioPlan;
	@Resource
	private ServicioProducto servicioProducto;
	@Resource
	private ServicioParametros servicioParametros;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private String strRespuesta;
	
	private PlanId id;
	private String alplDePlan;
	private Integer alplDato3;
	private Integer plazo;
	
	private List<Plan> listaPlanes;
	private List<Object> listaComboProductos;
	
	private Plan plan 	 = new Plan();
	private Plan newPlan = new Plan(new PlanId(), new Producto(), "");
	
	//lpv
	private List<Object> listaComboCentroCostos;
	private List<Object> listaComboSegmento;


	public BeanPlan() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PLAN);
		
		this.message 	 		 = new StringBuilder();
		this.id			 		 = new PlanId();
		this.listaPlanes 		 = new ArrayList<Plan>();
		this.listaComboProductos = new ArrayList<Object>();

		//lpv
		this.listaComboCentroCostos = new ArrayList<Object>();
		this.listaComboSegmento		= new ArrayList<Object>();
		
	}
	
	public String getAlplDePlan() {
		return alplDePlan;
	}
	public void setAlplDePlan(String alplDePlan) {
		this.alplDePlan = alplDePlan;
	}
	public Integer getAlplDato3() {
		return alplDato3;
	}
	public void setAlplDato3(Integer alplDato3) {
		this.alplDato3 = alplDato3;
	}
	public Integer getPlazo() {
		return plazo;
	}
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}
	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}
	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}
	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}
	public PlanId getId() {
		return id;
	}
	public void setId(PlanId id) {
		this.id = id;
	}
	public List<Plan> getListaPlanes() {
		return listaPlanes;
	}
	public void setListaPlanes(List<Plan> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public List<Object> getListaComboProductos() {
		return listaComboProductos;
	}
	public void setListaComboProductos(List<Object> listaComboProductos) {
		this.listaComboProductos = listaComboProductos;
	}
	public Plan getPlan() {
		return plan;
	}
	public void setPlan(Plan plan) {
		this.plan = plan;
	}
	public Plan getNewPlan() {
		return newPlan;
	}
	public void setNewPlan(Plan newPlan) {
		this.newPlan = newPlan;
	}
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	//lpv
	public List<Object> getListaComboCentroCostos() {
		return listaComboCentroCostos;
	}
	public void setListaComboCentroCostos(List<Object> listaComboCentroCostos) {
		this.listaComboCentroCostos = listaComboCentroCostos;
	}
	public List<Object> getListaComboSegmento() {
		return listaComboSegmento;
	}
	public void setListaComboSegmento(List<Object> listaComboSegmento) {
		this.listaComboSegmento = listaComboSegmento;
	}
	
	//METODOS
	public String consultarProductos(){
		String filtro = "";
		this.listaComboProductos.clear();
		
		try {
			if(getId().getAlplCdRamo() > 0){
				filtro = " and P.id.alprCdRamo = " + getId().getAlplCdRamo() + "\n";
				List<Producto> lstProducto = this.servicioProducto.obtenerObjetos(filtro);
				
				for (Producto producto : lstProducto) {
					//Lpv se utilizan los resultados para generar un combo con los productos
					this.listaComboProductos.add(new SelectItem(producto.getId().getAlprCdProducto().toString(), producto.toString()));
				}
			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			this.message.append("No se pueden recuperar los productos.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}

	//LPV
	public String consultaPlanes(){
		StringBuilder filtro = new StringBuilder(100);
		getListaPlanes().clear();
		
		try {
			if(getId().getAlplCdProducto() > 0) {
				filtro.append(" and P.id.alplCdRamo 	 = ").append(getId().getAlplCdRamo());
				filtro.append(" and P.id.alplCdProducto = ").append(getId().getAlplCdProducto());
				filtro.append(" ");
				List<Plan> listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
				for (Plan plan : listaPlanes) {
					getListaPlanes().add(plan);
				}

				setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			} else {
				setStrRespuesta("Favor de seleccionar un PRODUCTO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
	}
	
	//LPV
	public void modificar() {
		try {
			servicioPlan.actualizarObjeto(getPlan());
			consultaPlanes();
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
	}

	//LPV
	public String altaPlan(){
		try {
			StringBuilder filtro = new StringBuilder();
			
			filtro.append(" and P.id.alplCdRamo 	 = ").append(getId().getAlplCdRamo()); 		//Ramo
			filtro.append(" and P.id.alplCdProducto  = ").append(getNewPlan().getId().getAlplCdProducto());	//Producto
			Integer cdPlan = this.servicioPlan.siguentePlan(filtro.toString());
			
			getNewPlan().getId().setAlplCdRamo(getId().getAlplCdRamo());
			getNewPlan().getId().setAlplCdPlan(cdPlan);
			this.servicioPlan.guardarObjeto(getNewPlan());
			
			setStrRespuesta("Se guardo con EXITO el registro.");
			clearBean();
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			setStrRespuesta("Error Plan duplicado.");
			e.printStackTrace();
			this.message.append("Plan duplicado.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			setStrRespuesta("No se puede dar de alta el plan.");
			e.printStackTrace();
			this.message.append("No se puede dar de alta el producto.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.FAILURE;
			
		}
	}
	
	private void clearBean(){
		getId().setAlplCdRamo(new Byte("0"));
		setNewPlan(new Plan(new PlanId(), new Producto(), ""));
	}
	
	public void cargaCentroDeCostos(){

		StringBuilder filtro = new StringBuilder();

		getListaComboCentroCostos().clear();
		
		try {
						
				filtro.append("  and P.copaDesParametro = 'CATALOGO'");
				filtro.append("  and P.copaIdParametro  = 'CENTROCOSTOS'");
				filtro.append("  and P.copaNvalor2 = ").append(getId().getAlplCdRamo()).append("\n");
				
				List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lista) {
					getListaComboCentroCostos().add(new SelectItem(parametros.getCopaNvalor5(), parametros.getCopaNvalor5()+"-"+parametros.getCopaVvalor1()));
				}
				
		
		}catch(Exception e){

			e.printStackTrace();
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
		}
	}
	
	public void cargaParametros(){
		consultarProductos();
		cargaCentroDeCostos();
	}
	
}
