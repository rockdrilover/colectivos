package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturasId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.HomologaProductosPlanes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Poliza;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ProductoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Ramo;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCartCoberturas;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHomologaProdPlanes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class BeanParametrizacion {

	//Recursos
	@Resource
	private ServicioProducto servicioProducto;
	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ServicioPlan	servicioPlan;
	@Resource
	private ServicioCartCoberturas servicioCartCoberturas;
	@Resource
	private ServicioColectivosCoberturas servicioColectivosCoberturas;
	@Resource
	private ServicioColectivosComponentes servicioColectivosComponentes;
	@Resource
	private ServicioHomologaProdPlanes servicioHomologa;
	
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;

	//Mapeos
	//-- POLIZA
	private Poliza poliza;
	
	//-- PRODUCTO
	private ProductoId id;
	private Ramo ramo;

	private String alprDeProducto;	//descProducto
	private Integer alprDato3; 		//idVenta
	private Integer cdProducto;		//cdProducto
	
	//-- PLAN
	private PlanId idPlan;
	private String alplDePlan; 	//descPlan
	private String alplDato2;	//dato 2
	private Integer alplDato3;	//centro de costos
	private Integer plazo;		//plazo
	
	private List<Plan> listaPlanes;
	private List<Object> listaComboCoberturas;
	private List<Object> listaComboPolizas;
	private List<Object> listaComboIdVenta;
	private List<Object> listaComboProducto;
	private List<Object> listaComboPlanes;
	private List<CartCoberturas> listaCoberturas;
	private List<ColectivosCoberturas> listaColCoberturas;
	private List<ColectivosComponentes> listaColComponentes;
	
	private Boolean habilitaComboIdVenta = true; 
	private Boolean habilitaPoliza		 = true;
	private String strRespuesta;

	private Long polizaUnica;
	
	private Boolean renderConfig = true;
	private Boolean renderClon	 = false;
	private Integer radio		 = new Integer(1);
	
	private List<SelectItem> listaComboPlanesPickList;
	private List<SelectItem> listaComboPlanesSeleccionados;
	
	private List<Plan> listaProductosPlan;
	//constructor
	public BeanParametrizacion() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PARAMETRIZA_TARIFA);
		
		this.message				= new StringBuilder();
		this.ramo 					= new Ramo();
		this.id						= new ProductoId();
		this.idPlan					= new PlanId();
		this.poliza					= new Poliza();
		this.listaPlanes 			= new ArrayList<Plan>();
		this.listaComboCoberturas 	= new ArrayList<Object>();
		this.listaComboPolizas	  	= new ArrayList<Object>();
		this.listaComboIdVenta	  	= new ArrayList<Object>();
		this.listaComboProducto	  	= new ArrayList<Object>();
		this.listaComboPlanes	  	= new ArrayList<Object>();
		this.listaCoberturas		= new ArrayList<CartCoberturas>();
		this.listaColCoberturas		= new ArrayList<ColectivosCoberturas>();
		this.listaColComponentes	= new ArrayList<ColectivosComponentes>();
		this.listaComboPlanesSeleccionados = new ArrayList<SelectItem>();
		this.listaComboPlanesPickList  = new ArrayList<SelectItem>();
		listaProductosPlan =  new ArrayList<Plan>();
	}
	
	public void inicializaConsulta() {
		this.listaPlanes 			= new ArrayList<Plan>();
		this.listaComboCoberturas 	= new ArrayList<Object>();
		this.listaComboPolizas	  	= new ArrayList<Object>();
		this.listaComboIdVenta	  	= new ArrayList<Object>();
		this.listaComboProducto	  	= new ArrayList<Object>();
		this.listaComboPlanes	  	= new ArrayList<Object>();
		this.listaComboPlanesSeleccionados = new ArrayList<SelectItem>();
		this.listaComboPlanesPickList  = new ArrayList<SelectItem>();
		listaProductosPlan =  new ArrayList<Plan>();
	}
	
	
	//Acciones
	
	public String cargaPolizas(){
		
		this.message.append("cargando polizas.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder filtro = new StringBuilder(100);
			
		this.listaComboPolizas.clear();
		this.listaComboIdVenta.clear();
		this.listaComboProducto.clear();
		this.listaComboPlanes.clear();
		this.listaComboPlanesPickList.clear();
		if(getRamo().getCarpCdRamo() != null){
			
			try {
		
				filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro.append("  and P.copaNvalor1 	   = 1 \n");
				filtro.append("  and P.copaNvalor2 = ").append(getRamo().getCarpCdRamo()).append("\n");
				filtro.append("  and P.copaNvalor7 = 0 \n");
				filtro.append("order by P.copaNvalor3");
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				this.listaComboPolizas.add(new SelectItem("0","PRIMA UNICA"));
				
				for(Parametros item: listaPolizas){
					this.listaComboPolizas.add(new SelectItem(item.getCopaNvalor3().toString(), item.getCopaNvalor3().toString()));
				}
				
				return NavigationResults.SUCCESS;
				
				
			} catch (Exception e) {
				e.printStackTrace();
				message.append("No se pueden cargar los parametros.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}
		}
		else{
			
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.RETRY;
		}
		
	}
	
	/*
	 * 
	 */
	public void cargaIdVenta(){
		
		StringBuilder filtro  = new StringBuilder();
		StringBuilder filtro2 = new StringBuilder();
		
		this.listaComboIdVenta.clear();
		this.listaComboProducto.clear();
		this.listaComboPlanes.clear();
		try {

			if(getPoliza().getPoliza() == 0 || getPoliza().getPoliza() == -1){
				
				habilitaComboIdVenta = false;
				habilitaPoliza		 = true;
				
				filtro.append("  and P.copaIdParametro = 'IDVENTA'");
				filtro.append("  and P.copaNvalor1 = 1 \n");
				filtro.append("  and P.copaNvalor2 = ").append(getRamo().getCarpCdRamo()).append("\n");
				
				List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for(Parametros param: lista){
					this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
				}
				
				if (lista == null || lista.size() <= 0) habilitaComboIdVenta=true;
				
			}else{
				habilitaComboIdVenta = false;
				habilitaPoliza		 = true;

				this.listaComboIdVenta.add(new SelectItem("0","RECURRENTE"));
				
				/*habilitaComboIdVenta = true;
				
				filtro2.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro2.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro2.append("  and P.copaNvalor1 = 1 \n");
				filtro2.append("  and P.copaNvalor2 = ").append(getRamo().getCarpCdRamo()).append("\n");
				filtro2.append("  and P.copaNvalor3 = ").append(getPoliza().getPoliza()).append("\n");
				filtro2.append("order by P.copaNvalor3");
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro2.toString());
				
				for(Parametros item: listaPolizas){					
					if(item.getCopaNvalor4().toString().equals("1")){
						habilitaPoliza=false;
					}else{
						habilitaPoliza=true;
					}
				}*/
			}
			
			
		} catch (Exception e) {

			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	

	/**
	 * Consulta informacion de productos plan
	 */
	public void consultaProductoPlan() {
		StringBuilder filtro  = new StringBuilder();
		listaProductosPlan.clear();
		try {

			filtro.append(" and P.id.alplCdRamo = ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.producto.alprDato3 	= ").append(getAlprDato3());
			
			
			listaProductosPlan = this.servicioPlan.obtenerObjetos(filtro.toString());

			
			
			
		} catch (Exception e) {

			message.append("No se pueden cargar los Productos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
	}
	
	/*
	 * 
	 */
	public void consultaProducto(){
		
		StringBuilder filtro  = new StringBuilder();
		
		this.listaComboProducto.clear();
		this.listaComboPlanes.clear();
		this.listaComboPlanesPickList.clear();
		try {

			filtro.append(" and P.id.alprCdRamo = ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.alprDato3 	= ").append(getAlprDato3());
			
			List<Producto> lista = this.servicioProducto.obtenerObjetos(filtro.toString());
			
			for (Producto producto : lista) {
				this.listaComboProducto.add(new SelectItem(producto.getId().getAlprCdProducto(),producto.getId().getAlprCdProducto()+ " - "+producto.getAlprDeProducto()));
			}
			
			
		} catch (Exception e) {

			message.append("No se pueden cargar los Productos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
		
	public String altaProducto(){
		
		try {
			setCdProducto( this.servicioProducto.siguenteProducto(getRamo().getCarpCdRamo()) );
			
			setId( new ProductoId(getRamo().getCarpCdRamo(), getCdProducto()) );
						
			Producto producto = (Producto) ConstruirObjeto.crearObjeto(Producto.class, this);
			
			this.servicioProducto.guardarObjeto(producto);
			
			return NavigationResults.SUCCESS;
			
		}
		catch (ObjetoDuplicado e) {

			this.message.append("Producto duplicado.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}
		catch (Excepciones e) {
			e.printStackTrace();
			this.message.append("No se puede dar de alta el producto.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.FAILURE;
			
		}
	}
	
	/*
	 * 
	 */
	public String altaPlan(){
		
		try {
			
			StringBuilder filtro  = new StringBuilder();
			StringBuilder filtro2 = new StringBuilder();
			
			filtro.append(" and P.copaDesParametro = 'PRIMAUNICA'");
			filtro.append(" and P.copaNvalor1 = ").append("1"); 					 //canal
			filtro.append(" and P.copaNvalor2 = ").append(getRamo().getCarpCdRamo());//Ramo
			filtro.append(" and P.copaNvalor4 = ").append(getAlprDato3()); 			 //idVenta
			
			
			List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			for (Parametros parametros : listaPolizas) {
				setAlplDato2(parametros.getCopaVvalor1());
			}
			
			filtro2.append(" and P.id.alplCdRamo 	 = ").append(getRamo().getCarpCdRamo());
			filtro2.append(" and P.id.alplCdProducto = ").append(getId().getAlprCdProducto());
			filtro2.append(" and substr(P.id.alplCdPlan,length(P.id.alplCdPlan)-1) = ").append(getIdPlan().getAlplCdPlan()).append(" ");

			List<Plan> listaPlanes = this.servicioPlan.obtenerObjetos(filtro2.toString());
			
			setPlazo(getIdPlan().getAlplCdPlan());
			//Validamos si existe el plazo, Si existe se agrega un prefijo consecutivo, si no existe, se deja el actual.
			getIdPlan().setAlplCdPlan(findNewCdPlan(listaPlanes, getIdPlan().getAlplCdPlan()));

			byte ramo  = getRamo().getCarpCdRamo().byteValue();
			int cdProd = getId().getAlprCdProducto().intValue();
			int cdPlan = getIdPlan().getAlplCdPlan();
			
			Plan plan  = new Plan();
			
			plan.setId(new PlanId(ramo, cdProd, cdPlan));
			plan.setAlplDePlan(getAlplDePlan());
			plan.setAlplDato2(getAlplDato2());
			plan.setAlplDato3(getAlplDato3());
			
			this.servicioPlan.guardarObjeto(plan);
			this.consultaPlanes();
			
			return NavigationResults.SUCCESS;

			
		}
		catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.message.append("Producto duplicado.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}
		catch (Excepciones e) {

			e.printStackTrace();
			this.message.append("No se puede dar de alta el producto.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.FAILURE;
			
		}
	}
	
	
	public String consultaPlanes(){
	
		StringBuilder filtro = new StringBuilder(100);
		
		getListaComboPlanes().clear();
		getListaPlanes().clear();
		getListaComboPlanesPickList().clear();
		
		try {
	
			filtro.append(" and P.id.alplCdRamo 	 = ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.id.alplCdProducto = ").append(getId().getAlprCdProducto());
			filtro.append(" ");
			
			List<Plan> listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
			
			for (Plan plan : listaPlanes) {
				getListaPlanes().add(plan);
				getListaComboPlanes().add(new SelectItem(plan.getId().getAlplCdPlan(),plan.getId().getAlplCdPlan()+" - "+plan.getAlplDePlan()));
				getListaComboPlanesPickList().add(new SelectItem(plan.getId().getAlplCdPlan(),plan.getId().getAlplCdPlan()+" - "+plan.getAlplDePlan()));
			}
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	
	//LPV
	private int findNewCdPlan(List<Plan> listaPlanes, int plazo){

		Plan objPlan 		= new Plan();
		String tempPlan 	= "";
		Integer consecutivo = plazo;

		//Este fragmento de codigo obtiene el plazo del plan mas grande de la consulta almacenada en listaPlanes
		//El resultado se usa para validar si existe mas de un plazo, en caso de existir el mismo plazo se le va agregando
		//un prefijo, en este caso es un consecutivo ej. Si el plazo 36 ya existe, se agregara un 1 para formar un 136,
		//si ya tiene el 36 y el 136, el siguiente seria el 236 y asi sucesivamente.
		if(listaPlanes.size()>0){
			
			objPlan = Collections.max(listaPlanes, new Comparator<Plan>() {
				public int compare(Plan arg0, Plan arg1) {
					return arg0.getId().getAlplCdPlan()-arg1.getId().getAlplCdPlan();
				};
			});
			
			tempPlan = Utilerias.agregarCaracteres(objPlan.getId().getAlplCdPlan()+"", 2, '0', 1);
			
			consecutivo = (Integer.parseInt(tempPlan.substring(0,1)))+1;
			
			consecutivo = Integer.parseInt(consecutivo+tempPlan.substring(1));
		}
		
		return consecutivo;
	}
	
	
	
	public String consultaPlazo(){
		String strPlazo;
		StringBuilder filtro = new StringBuilder(100);
		
		try {
	
			filtro.append(" and P.id.alplCdRamo 	= ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.id.alplCdProducto = ").append(getId().getAlprCdProducto());
			filtro.append(" and P.id.alplCdPlan		= ").append(getIdPlan().getAlplCdPlan());
			filtro.append(" ");
			
			List<Plan> listaPlan = this.servicioPlan.obtenerObjetos(filtro.toString());
			strPlazo = ((Plan)listaPlan.get(0)).getAlplDato1();
			setPlazo(new Integer(strPlazo != null ? strPlazo : "0"));
			

			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	
	public String consultaColectivosCoberturas(){
		
		StringBuilder filtro = new StringBuilder(100);
		Integer poliza;
		getListaColCoberturas().clear();
		setStrRespuesta("");
		
		consultaPlazo();
		
		if(getPoliza().getPoliza()!=null && getPoliza().getPoliza() == 0){
			poliza = Integer.parseInt(getAlprDato3().toString() +""+Utilerias.agregarCaracteres(getPlazo().toString(), 1, '0', 1)+""+Utilerias.agregarCaracteres(getId().getAlprCdProducto().toString(), 1, '0', 1));
		}else{
			poliza = getPoliza().getPoliza();
			if(getRamo().getCarpCdRamo() == 57 || getRamo().getCarpCdRamo() == 58) {
				poliza = Integer.valueOf(getRamo().getCarpCdRamo().toString());
			}
		}
		
		setPolizaUnica(new Long(poliza));
		
		try {
	
			filtro.append(" and P.id.cocbCarpCdRamo		= ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.id.cocbCapoNuPoliza 	= ").append(poliza);
			filtro.append(" and P.id.cocbCapuCdProducto = ").append(getId().getAlprCdProducto());
			filtro.append(" and P.id.cocbCapbCdPlan 	= ").append(getIdPlan().getAlplCdPlan());

			filtro.append(" ");
			
			List<ColectivosCoberturas> listaCoberturas = this.servicioColectivosCoberturas.obtenerObjetos(filtro.toString());
			
			for (ColectivosCoberturas cobertura : listaCoberturas) {
				getListaColCoberturas().add(cobertura);
			}
			
			setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

	/**
	 * Valida la configuracion de parametrizacion de tipo comision
	 * 
	 * @return P - Producto Plan / C - Cobertura
	 */
	public void actualizaTipoComision() throws Excepciones {

		StringBuilder filtro = new StringBuilder(100);
		Integer poliza;
		getListaColCoberturas().clear();
		setStrRespuesta("");

		consultaPlazo();

		if (getPoliza().getPoliza() != null && getPoliza().getPoliza() == 0) {
			poliza = Integer.parseInt(
					getAlprDato3().toString() + "" + Utilerias.agregarCaracteres(getPlazo().toString(), 1, '0', 1) + ""
							+ Utilerias.agregarCaracteres(getId().getAlprCdProducto().toString(), 1, '0', 1));
		} else {
			poliza = getPoliza().getPoliza();
			if (getRamo().getCarpCdRamo() == 57 || getRamo().getCarpCdRamo() == 58) {
				poliza = Integer.valueOf(getRamo().getCarpCdRamo().toString());
			}
		}

		setPolizaUnica(new Long(poliza));

		ColectivosCoberturas res = new ColectivosCoberturas();
		ColectivosCoberturasId coberturasId = new ColectivosCoberturasId();

		coberturasId.setCocbCasuCdSucursal(new Short("1")); // SUCURSAL
		coberturasId.setCocbCarpCdRamo(getRamo().getCarpCdRamo().byteValue()); // RAMO
		coberturasId.setCocbCapoNuPoliza(getPolizaUnica()); // POLIZA

		res.setId(coberturasId);

		res.setTipoComision("P");

		try {
			filtro.append(" and P.id.cocbCasuCdSucursal = 1");
			filtro.append(" and P.id.cocbCarpCdRamo		= ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.id.cocbCapoNuPoliza 	= ").append(poliza);

			filtro.append(" ");

			List<ColectivosCoberturas> listaCoberturas = this.servicioColectivosCoberturas
					.obtenerObjetos(filtro.toString());

			if (listaCoberturas.size() > 0) {
				res = listaCoberturas.get(0);

				if (res.getCocbPoComision().compareTo(new BigDecimal(0)) == 0 && res.getCocbMtFijo().compareTo(new BigDecimal(0)) == 0) {
					res.setTipoComision("P");
				} else {
					res.setTipoComision("C");
				}
		
			}
			servicioColectivosCoberturas.actualizaTipoComision(res);

		} catch (Exception e) {
			e.printStackTrace();

			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			throw new Excepciones("No se actualizar el tipo de comision.");
		}

	}
	
	public String consultaColectivosComponentes(){
		
		StringBuilder filtro = new StringBuilder(100);
		Integer poliza;
		
		getListaColComponentes().clear();
		
		setStrRespuesta("");
		
		consultaPlazo();
		
		if(getPoliza().getPoliza()!=null && getPoliza().getPoliza() == 0){
			poliza =  Integer.parseInt(getAlprDato3().toString() +""+Utilerias.agregarCaracteres(getPlazo().toString(), 1, '0', 1)+""+Utilerias.agregarCaracteres(getId().getAlprCdProducto().toString(), 1, '0', 1));
		}else{
			poliza = getPoliza().getPoliza();
			if(getRamo().getCarpCdRamo() == 57 || getRamo().getCarpCdRamo() == 58) {
				poliza = Integer.valueOf(getRamo().getCarpCdRamo().toString());
			}
		}
		
		setPolizaUnica(new Long(poliza));
		
		try {
	
			filtro.append(" and P.id.coctCasuCdSucursal = 1");
			filtro.append("	and P.id.coctCarpCdRamo 	= ").append(getRamo().getCarpCdRamo());
			filtro.append("	and P.id.coctCapoNuPoliza 	= ").append(poliza);
			filtro.append("	and P.id.coctCapuCdProducto = ").append(getId().getAlprCdProducto());
			filtro.append("	and P.id.coctCapbCdPlan 	= ").append(getIdPlan().getAlplCdPlan());
			filtro.append("	and P.id.coctEstatus 		= 20 ");
			filtro.append(" ");
			
			List<ColectivosComponentes> listaColectivosComponentes = this.servicioColectivosComponentes.obtenerObjetos(filtro.toString());
		
			setListaColComponentes(listaColectivosComponentes);
			
			setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}
	
	public void consulta(){
		consultaColectivosCoberturas();
		consultaColectivosComponentes();
	}
	
	public void show(){
		
		switch (getRadio()) {
		case 1:
			setRenderConfig(true);
			setRenderClon(false);
			break;

		case 2:
			setRenderConfig(false);
			setRenderClon(true);
			break;

		default:
			break;
		}

	}

	
	public String clonar(){
		StringBuilder filtro = new StringBuilder();
		
		try {
			
			if(getAlprDato3()==0) return NavigationResults.SUCCESS; //NO PERMITE CLONAR RECURRENTE
			
			//Iniciamos con la clonacion de Producto
			filtro.append(" and P.id.alprCdRamo     = ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.alprDato3 	    = ").append(getAlprDato3()); //ID DE VENTA
			filtro.append(" and P.id.alprCdProducto = ").append(getId().getAlprCdProducto());
			
			List<Producto> lista = this.servicioProducto.obtenerObjetos(filtro.toString());
			
			setCdProducto( this.servicioProducto.siguenteProducto(getRamo().getCarpCdRamo()) );
			
			for(int i=0; i<lista.size();i++){
				lista.get(i).getId().setAlprCdProducto(getCdProducto());
				lista.get(i).setAlprDeProducto( "CLON: "+lista.get(i).getAlprDeProducto());
				this.servicioProducto.guardarObjeto((Producto)lista.get(i));
			}
			
			//Iniciamos con la clonacion de planes
			consultaPlanes();
			
			for(int i=0;i<getListaPlanes().size();i++){
				
				getListaPlanes().get(i).getId().setAlplCdProducto(getCdProducto());
				getListaPlanes().get(i).setAlplDePlan("CLON:"+getListaPlanes().get(i).getAlplDePlan());
				
				this.servicioPlan.guardarObjeto((Plan)getListaPlanes().get(i));
			}
			//Iniciamos con la clonacion de COBERTURAS
			filtro = new StringBuilder();
			filtro.append(" and P.id.cocbCarpCdRamo		= ").append(getRamo().getCarpCdRamo());
			filtro.append(" and P.id.cocbCapuCdProducto = ").append(getId().getAlprCdProducto());
			filtro.append(" ");
			
			String poliza;
			
			List<ColectivosCoberturas> listaCoberturas = this.servicioColectivosCoberturas.obtenerObjetos(filtro.toString());
			
			for(int i=0;i<listaCoberturas.size();i++){
				
				poliza = listaCoberturas.get(i).getId().getCocbCapoNuPoliza()+"";
				
				listaCoberturas.get(i).getId().setCocbCapoNuPoliza( Long.parseLong(poliza.substring(0, poliza.length()-2)+getCdProducto()) );
				listaCoberturas.get(i).getId().setCocbCapuCdProducto(getCdProducto());
				
				this.servicioColectivosCoberturas.guardarObjeto((ColectivosCoberturas)listaCoberturas.get(i));
			}
			
			//Iniciamos con la clonacion de COMPONENTES
			filtro = new StringBuilder();
			filtro.append(" and P.id.coctCasuCdSucursal = 1");
			filtro.append("	and P.id.coctCarpCdRamo 	= ").append(getRamo().getCarpCdRamo());
			filtro.append("	and P.id.coctCapuCdProducto = ").append(getId().getAlprCdProducto());
			filtro.append("	and P.id.coctEstatus 		= 20 ");
			filtro.append(" ");
			
			List<ColectivosComponentes> listaColectivosComponentes = this.servicioColectivosComponentes.obtenerObjetos(filtro.toString());
			
			for(int i=0;i<listaColectivosComponentes.size();i++){
				
				poliza = listaColectivosComponentes.get(i).getId().getCoctCapoNuPoliza()+"";
				
				listaColectivosComponentes.get(i).getId().setCoctCapoNuPoliza( Long.parseLong(poliza.substring(0, poliza.length()-2)+getCdProducto()) );
				listaColectivosComponentes.get(i).getId().setCoctCapuCdProducto(getCdProducto());
				
				this.servicioColectivosComponentes.guardarObjeto((ColectivosComponentes) listaColectivosComponentes.get(i));
			}
			
			//Iniciamos con la clonacion de HOMOLOGACION
			filtro = new StringBuilder();
			filtro.append(" and HP.id.hoppCdRamo 		= ").append(getRamo().getCarpCdRamo());
			filtro.append("	and HP.id.hoppProdColectivo = ").append(getId().getAlprCdProducto());
			filtro.append(" ");
			
			List<HomologaProductosPlanes> listaHomologa = this.servicioHomologa.obtenerObjetos(filtro.toString());

			for(int i=0;i<listaHomologa.size();i++){
				
				listaHomologa.get(i).getId().setHoppProdColectivo(getCdProducto());
				listaHomologa.get(i).setHoppDeProducto("CLON:"+listaHomologa.get(i).getHoppDeProducto());
				
				this.servicioHomologa.guardarObjeto((HomologaProductosPlanes)listaHomologa.get(i));
			}
			
			//Iniciamos con la clonacion de PARAMETROS
			filtro	= new StringBuilder();
			filtro.append("  and P.copaDesParametro in ('POLIZA','RECIBO') \n");
			filtro.append("  and P.copaNvalor1 	   = 1 \n");
			filtro.append("  and P.copaNvalor2 = ").append(getRamo().getCarpCdRamo()).append("\n");
			filtro.append("  and P.copaNvalor3 LIKE '").append(getAlprDato3()).append("%").append(getId().getAlprCdProducto()).append("' \n");
			filtro.append("  and length(P.copaNvalor3) = 5 \n");
			filtro.append("order by P.copaNvalor3");
			
			List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			for (Parametros parametros : listaPolizas) {
				
				parametros.setCopaCdParametro(new Long(this.servicioParametros.siguienteCdParam()));
				poliza = parametros.getCopaNvalor3()+"";
				parametros.setCopaNvalor3(Long.parseLong(poliza.substring(0, poliza.length()-2)+getCdProducto()));
				
				this.servicioParametros.guardarObjeto(parametros);
			}
			
			setStrRespuesta("SE CLONO CON EXITO EL NUEVO PRODUCTO CON ID: "+getCdProducto());
			
			return NavigationResults.SUCCESS;
			
		}
		catch (ObjetoDuplicado e) {

			this.message.append("Producto duplicado.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}
		catch (Excepciones e) {
			e.printStackTrace();
			this.message.append("No se puede dar de alta el producto.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.FAILURE;
			
		}
	}
	
	//--------------------------------------------------
	//Setters y Getters
	public Poliza getPoliza() {
		return poliza;
	}
	public void setPoliza(Poliza poliza) {
		this.poliza = poliza;
	}
	
	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}

	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}

	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	
	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}
	
	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}
	
	public ServicioCartCoberturas getServicioCartCoberturas() {
		return servicioCartCoberturas;
	}
	
	public void setServicioCartCoberturas(
			ServicioCartCoberturas servicioCartCoberturas) {
		this.servicioCartCoberturas = servicioCartCoberturas;
	}
	
	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public ProductoId getId() {
		return id;
	}

	public void setId(ProductoId id) {
		this.id = id;
	}

	public Ramo getRamo() {
		return ramo;
	}

	public void setRamo(Ramo ramo) {
		this.ramo = ramo;
	}

	public String getAlprDeProducto() {
		return alprDeProducto;
	}

	public void setAlprDeProducto(String alprDeProducto) {
		this.alprDeProducto = alprDeProducto;
	}

	public Integer getAlprDato3() {
		return alprDato3;
	}

	public void setAlprDato3(Integer alprDato3) {
		this.alprDato3 = alprDato3;
	}

	public Integer getCdProducto() {
		return cdProducto;
	}

	public void setCdProducto(Integer cdProducto) {
		this.cdProducto = cdProducto;
	}

	public PlanId getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(PlanId idPlan) {
		this.idPlan = idPlan;
	}

	public String getAlplDePlan() {
		return alplDePlan;
	}

	public void setAlplDePlan(String alplDePlan) {
		this.alplDePlan = alplDePlan;
	}

	public Integer getPlazo() {
		return plazo;
	}
	
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	
	public String getAlplDato2() {
		return alplDato2;
	}

	public void setAlplDato2(String alplDato2) {
		this.alplDato2 = alplDato2;
	}

	public Integer getAlplDato3() {
		return alplDato3;
	}

	public void setAlplDato3(Integer alplDato3) {
		this.alplDato3 = alplDato3;
	}

	public List<Plan> getListaPlanes() {
		return listaPlanes;
	}

	public void setListaPlanes(List<Plan> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}

	public List<Object> getListaComboCoberturas() {
		return listaComboCoberturas;
	}
	
	public void setListaComboCoberturas(List<Object> listaComboCoberturas) {
		this.listaComboCoberturas = listaComboCoberturas;
	}

	public List<Object> getListaComboPolizas() {
		return listaComboPolizas;
	}
	public void setListaComboPolizas(List<Object> listaComboPolizas) {
		this.listaComboPolizas = listaComboPolizas;
	}
	
	public List<Object> getListaComboIdVenta() {
		return listaComboIdVenta;
	}
	public void setListaComboIdVenta(List<Object> listaComboIdVenta) {
		this.listaComboIdVenta = listaComboIdVenta;
	}

	public Boolean getHabilitaComboIdVenta() {
		return habilitaComboIdVenta;
	}
	public void setHabilitaPoliza(Boolean habilitaPoliza) {
		this.habilitaPoliza = habilitaPoliza;
	}
	public Boolean getHabilitaPoliza() {
		return habilitaPoliza;
	}
	public void setHabilitaComboIdVenta(Boolean habilitaComboIdVenta) {
		this.habilitaComboIdVenta = habilitaComboIdVenta;
	}
	
	public List<Object> getListaComboProducto() {
		return listaComboProducto;
	}
	public void setListaComboProducto(List<Object> listaComboProducto) {
		this.listaComboProducto = listaComboProducto;
	}
	
	public List<Object> getListaComboPlanes() {
		return listaComboPlanes;
	}
	public void setListaComboPlanes(List<Object> listaComboPlanes) {
		this.listaComboPlanes = listaComboPlanes;
	}
	public void setListaCoberturas(List<CartCoberturas> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public List<CartCoberturas> getListaCoberturas() {
		return listaCoberturas;
	}
	
	public ServicioColectivosCoberturas getServicioColectivosCoberturas() {
		return servicioColectivosCoberturas;
	}
	public void setServicioColectivosCoberturas(
			ServicioColectivosCoberturas servicioColectivosCoberturas) {
		this.servicioColectivosCoberturas = servicioColectivosCoberturas;
	}
	
	public List<ColectivosCoberturas> getListaColCoberturas() {
		return listaColCoberturas;
	}
	public void setListaColCoberturas(
			List<ColectivosCoberturas> listaColCoberturas) {
		this.listaColCoberturas = listaColCoberturas;
	}
	
	public String getStrRespuesta() {
		return strRespuesta;
	}
	
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	
	public Long getPolizaUnica() {
		return polizaUnica;
	}
	public void setPolizaUnica(Long polizaUnica) {
		this.polizaUnica = polizaUnica;
	}
	
	public void setListaColComponentes(
			List<ColectivosComponentes> listaColComponentes) {
		this.listaColComponentes = listaColComponentes;
	}
	
	public List<ColectivosComponentes> getListaColComponentes() {
		return listaColComponentes;
	}
	
	public void setServicioColectivosComponentes(
			ServicioColectivosComponentes servicioColectivosComponentes) {
		this.servicioColectivosComponentes = servicioColectivosComponentes;
	}
	
	public ServicioColectivosComponentes getServicioColectivosComponentes() {
		return servicioColectivosComponentes;
	}
	public void setRenderClon(Boolean renderClon) {
		this.renderClon = renderClon;
	}
	public Boolean getRenderClon() {
		return renderClon;
	}
	public void setRenderConfig(Boolean renderConfig) {
		this.renderConfig = renderConfig;
	}
	public Boolean getRenderConfig() {
		return renderConfig;
	}
	public Integer getRadio() {
		return radio;
	}
	public void setRadio(Integer radio) {
		this.radio = radio;
	}
	
	public ServicioHomologaProdPlanes getServicioHomologa() {
		return servicioHomologa;
	}
	public void setServicioHomologa(ServicioHomologaProdPlanes servicioHomologa) {
		this.servicioHomologa = servicioHomologa;
	}

	/**
	 * @return the listaComboPlanesSeleccionados
	 */
	public List<SelectItem> getListaComboPlanesSeleccionados() {
		return listaComboPlanesSeleccionados;
	}

	/**
	 * @param listaComboPlanesSeleccionados the listaComboPlanesSeleccionados to set
	 */
	public void setListaComboPlanesSeleccionados(List<SelectItem> listaComboPlanesSeleccionados) {
		this.listaComboPlanesSeleccionados = listaComboPlanesSeleccionados;
	}

	/**
	 * @return the listaComboPlanesPickList
	 */
	public List<SelectItem> getListaComboPlanesPickList() {
		return listaComboPlanesPickList;
	}

	/**
	 * @param listaComboPlanesPickList the listaComboPlanesPickList to set
	 */
	public void setListaComboPlanesPickList(List<SelectItem> listaComboPlanesPickList) {
		this.listaComboPlanesPickList = listaComboPlanesPickList;
	}

	/**
	 * @return the listaProductosPlan
	 */
	public List<Plan> getListaProductosPlan() {
		return listaProductosPlan;
	}

	/**
	 * @param listaProductosPlan the listaProductosPlan to set
	 */
	public void setListaProductosPlan(List<Plan> listaProductosPlan) {
		this.listaProductosPlan = listaProductosPlan;
	}
	
	
	
	
}
