package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioHistorialCredito;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class BeanListaRecibosHistorico {
	
	@Resource
	private ServicioHistorialCredito servicioHistorialCredito;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private String consecutivo;
	private String recibo;
	private String monto;
	private String estatus_rec;
	private String fech_des;
	private String fech_has;
	private List<BeanDetalleComponentes> consultaDetalleComponentes;
	public ServicioHistorialCredito getServicioHistorialCredito() {
		return servicioHistorialCredito;
	}
	public void setServicioHistorialCredito(
			ServicioHistorialCredito servicioHistorialCredito) {
		this.servicioHistorialCredito = servicioHistorialCredito;
	}
	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	public String getConsecutivo() {
		return consecutivo;
	}
	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}
	public String getRecibo() {
		return recibo;
	}
	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getFech_des() {
		return fech_des;
	}
	public void setFech_des(String fech_des) {
		this.fech_des = fech_des;
	}
	public String getFech_has() {
		return fech_has;
	}
	public void setFech_has(String fech_has) {
		this.fech_has = fech_has;
	}
	public String getEstatus_rec() {
		return estatus_rec;
	}
	public void setEstatus_rec(String estatus_rec) {
		this.estatus_rec = estatus_rec;
	}
	public List<BeanDetalleComponentes> getConsultaDetalleComponentes() {
		return consultaDetalleComponentes;
	}
	public void setConsultaDetalleComponentes(
			List<BeanDetalleComponentes> consultaDetalleComponentes) {
		this.consultaDetalleComponentes = consultaDetalleComponentes;
	}

}
