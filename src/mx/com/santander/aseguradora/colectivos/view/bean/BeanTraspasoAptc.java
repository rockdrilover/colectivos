/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.TlmkTraspasoAptcId;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanTraspasoAptc {

	private TlmkTraspasoAptcId id;
	private String trseRegistro;
	private Boolean trseCargada;
	private Date trseFechaCarga;
	private Long trseNumeroSolicitud;
	private String trseNomBase;
	private Short trseTipoProducto;
	private Integer trseSerie;
	private Long trseFolioInteligente;
	private String trseUsuario;
	private String trseRemesa;
	private short trseCampana;
	private String trsePago;
	private Date trseFechaPago;
	private String trseAptc;
	private Integer trseProveedor;
	private Date trseFechaVenta;
	private String trseCuenta;
	private short trseSubCampana;
	private BigDecimal trseNuRecibo;
	private String trseProgramado;
	
	public TlmkTraspasoAptcId getId() {
		return id;
	}
	public void setId(TlmkTraspasoAptcId id) {
		this.id = id;
	}
	public String getTrseRegistro() {
		return trseRegistro;
	}
	public void setTrseRegistro(String trseRegistro) {
		this.trseRegistro = trseRegistro;
	}
	public Boolean getTrseCargada() {
		return trseCargada;
	}
	public void setTrseCargada(Boolean trseCargada) {
		this.trseCargada = trseCargada;
	}
	public Date getTrseFechaCarga() {
		return trseFechaCarga;
	}
	public void setTrseFechaCarga(Date trseFechaCarga) {
		this.trseFechaCarga = trseFechaCarga;
	}
	public Long getTrseNumeroSolicitud() {
		return trseNumeroSolicitud;
	}
	public void setTrseNumeroSolicitud(Long trseNumeroSolicitud) {
		this.trseNumeroSolicitud = trseNumeroSolicitud;
	}
	public String getTrseNomBase() {
		return trseNomBase;
	}
	public void setTrseNomBase(String trseNomBase) {
		this.trseNomBase = trseNomBase;
	}
	public Short getTrseTipoProducto() {
		return trseTipoProducto;
	}
	public void setTrseTipoProducto(Short trseTipoProducto) {
		this.trseTipoProducto = trseTipoProducto;
	}
	public Integer getTrseSerie() {
		return trseSerie;
	}
	public void setTrseSerie(Integer trseSerie) {
		this.trseSerie = trseSerie;
	}
	public Long getTrseFolioInteligente() {
		return trseFolioInteligente;
	}
	public void setTrseFolioInteligente(Long trseFolioInteligente) {
		this.trseFolioInteligente = trseFolioInteligente;
	}
	public String getTrseUsuario() {
		return trseUsuario;
	}
	public void setTrseUsuario(String trseUsuario) {
		this.trseUsuario = trseUsuario;
	}
	public String getTrseRemesa() {
		return trseRemesa;
	}
	public void setTrseRemesa(String trseRemesa) {
		this.trseRemesa = trseRemesa;
	}
	public short getTrseCampana() {
		return trseCampana;
	}
	public void setTrseCampana(short trseCampana) {
		this.trseCampana = trseCampana;
	}
	public String getTrsePago() {
		return trsePago;
	}
	public void setTrsePago(String trsePago) {
		this.trsePago = trsePago;
	}
	public Date getTrseFechaPago() {
		return trseFechaPago;
	}
	public void setTrseFechaPago(Date trseFechaPago) {
		this.trseFechaPago = trseFechaPago;
	}
	public String getTrseAptc() {
		return trseAptc;
	}
	public void setTrseAptc(String trseAptc) {
		this.trseAptc = trseAptc;
	}
	public Integer getTrseProveedor() {
		return trseProveedor;
	}
	public void setTrseProveedor(Integer trseProveedor) {
		this.trseProveedor = trseProveedor;
	}
	public Date getTrseFechaVenta() {
		return trseFechaVenta;
	}
	public void setTrseFechaVenta(Date trseFechaVenta) {
		this.trseFechaVenta = trseFechaVenta;
	}
	public String getTrseCuenta() {
		return trseCuenta;
	}
	public void setTrseCuenta(String trseCuenta) {
		this.trseCuenta = trseCuenta;
	}
	public short getTrseSubCampana() {
		return trseSubCampana;
	}
	public void setTrseSubCampana(short trseSubCampana) {
		this.trseSubCampana = trseSubCampana;
	}
	public BigDecimal getTrseNuRecibo() {
		return trseNuRecibo;
	}
	public void setTrseNuRecibo(BigDecimal trseNuRecibo) {
		this.trseNuRecibo = trseNuRecibo;
	}
	public String getTrseProgramado() {
		return trseProgramado;
	}
	public void setTrseProgramado(String trseProgramado) {
		this.trseProgramado = trseProgramado;
	}
	
}
