package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioReportesCierre;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

@Controller
@Scope("session")
public class BeanReportesCierre {

	// variables
	@Resource
	private ServicioReportesCierre reportes;

	private int opcion;
	private int inRamo;
	private int inRamo2;
	private Date f_inicio1;
	private Date f_fin1;
	private Date f_inicio2;
	private Date f_fin2;
	private Date f_inicio3;
	private Date f_fin3;
	private Date f_inicio4;
	private Date f_fin4;
	private Date f_inicio5;
	private Date f_fin5;
	private Date f_inicio6;
	private Date f_fin6;
	private Date f_inicio7;
	private Date f_fin7;
	private Date f_inicio8;
	private Date f_fin8;
	private Date f_inicio9;
	private Date f_fin9;
	private Date f_inicio10;
	private Date f_fin10;
	private Date f_inicio14;
	private Date f_fin14;
	private Date f_inicio15;
	private Date f_fin15;
	private Date infinicio, inffin;
	private int operacion;
	private int canal = 1;
	
	
	public Date getF_inicio14() {
		return f_inicio14;
	}

	public void setF_inicio14(Date f_inicio14) {
		this.f_inicio14 = f_inicio14;
	}

	public Date getF_fin14() {
		return f_fin14;
	}

	public void setF_fin14(Date f_fin14) {
		this.f_fin14 = f_fin14;
	}

	public Date getF_inicio15() {
		return f_inicio15;
	}

	public void setF_inicio15(Date f_inicio15) {
		this.f_inicio15 = f_inicio15;
	}

	public Date getF_fin15() {
		return f_fin15;
	}

	public void setF_fin15(Date f_fin15) {
		this.f_fin15 = f_fin15;
	}

	public int getCanal() {
		return canal;
	}

	public void setCanal(int canal) {
		this.canal = canal;
	}

	// getters and setters
	public int getOpcion() {
		return opcion;
	}

	public void setOpcion(int opcion) {
		this.opcion = opcion;
	}
	
	public Date getF_inicio1() {
		return f_inicio1;
	}

	public void setF_inicio1(Date f_inicio1) {
		this.f_inicio1 = f_inicio1;
	}

	public Date getF_fin1() {
		return f_fin1;
	}

	public void setF_fin1(Date f_fin1) {
		this.f_fin1 = f_fin1;
	}

	public Date getF_inicio2() {
		return f_inicio2;
	}

	public void setF_inicio2(Date f_inicio2) {
		this.f_inicio2 = f_inicio2;
	}

	public Date getF_fin2() {
		return f_fin2;
	}

	public void setF_fin2(Date f_fin2) {
		this.f_fin2 = f_fin2;
	}

	public Date getF_inicio3() {
		return f_inicio3;
	}

	public void setF_inicio3(Date f_inicio3) {
		this.f_inicio3 = f_inicio3;
	}

	public Date getF_fin3() {
		return f_fin3;
	}

	public void setF_fin3(Date f_fin3) {
		this.f_fin3 = f_fin3;
	}

	public Date getF_inicio4() {
		return f_inicio4;
	}

	public void setF_inicio4(Date f_inicio4) {
		this.f_inicio4 = f_inicio4;
	}

	public Date getF_fin4() {
		return f_fin4;
	}

	public void setF_fin4(Date f_fin4) {
		this.f_fin4 = f_fin4;
	}

	public Date getF_inicio5() {
		return f_inicio5;
	}

	public void setF_inicio5(Date f_inicio5) {
		this.f_inicio5 = f_inicio5;
	}

	public Date getF_fin5() {
		return f_fin5;
	}

	public void setF_fin5(Date f_fin5) {
		this.f_fin5 = f_fin5;
	}

	public Date getF_inicio6() {
		return f_inicio6;
	}

	public void setF_inicio6(Date f_inicio6) {
		this.f_inicio6 = f_inicio6;
	}

	public Date getF_fin6() {
		return f_fin6;
	}

	public void setF_fin6(Date f_fin6) {
		this.f_fin6 = f_fin6;
	}

	public Date getF_inicio7() {
		return f_inicio7;
	}

	public void setF_inicio7(Date f_inicio7) {
		this.f_inicio7 = f_inicio7;
	}

	public Date getF_fin7() {
		return f_fin7;
	}

	public void setF_fin7(Date f_fin7) {
		this.f_fin7 = f_fin7;
	}

	public Date getF_inicio8() {
		return f_inicio8;
	}

	public void setF_inicio8(Date f_inicio8) {
		this.f_inicio8 = f_inicio8;
	}

	public Date getF_fin8() {
		return f_fin8;
	}

	public void setF_fin8(Date f_fin8) {
		this.f_fin8 = f_fin8;
	}

	public Date getF_inicio9() {
		return f_inicio9;
	}

	public void setF_inicio9(Date f_inicio9) {
		this.f_inicio9 = f_inicio9;
	}

	public Date getF_fin9() {
		return f_fin9;
	}

	public void setF_fin9(Date f_fin9) {
		this.f_fin9 = f_fin9;
	}

	public Date getF_inicio10() {
		return f_inicio10;
	}

	public void setF_inicio10(Date f_inicio10) {
		this.f_inicio10 = f_inicio10;
	}

	public Date getF_fin10() {
		return f_fin10;
	}

	public void setF_fin10(Date f_fin10) {
		this.f_fin10 = f_fin10;
	}
	
	public Date getInfinicio() {
		return infinicio;
	}

	public void setInfinicio(Date infinicio) {
		this.infinicio = infinicio;
	}

	public Date getInffin() {
		return inffin;
	}

	public void setInffin(Date inffin) {
		this.inffin = inffin;
	}

	public int getOperacion() {
		return operacion;
	}

	public void setOperacion(int operacion) {
		this.operacion = operacion;
	}
	
	public ServicioReportesCierre getReportes() {
		return reportes;
	}

	public void setReportes(ServicioReportesCierre reportes) {
		this.reportes = reportes;
	}

	public int getInRamo() {
		return inRamo;
	}

	public void setInRamo(int inRamo) {
		this.inRamo = inRamo;
	}

	public int getInRamo2() {
		return inRamo2;
	}

	public void setInRamo2(int inRamo2) {
		this.inRamo2 = inRamo2;
	}

	// m�todos
	public BeanReportesCierre() {
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_REPORTES_CIERRE);
	}

	@SuppressWarnings("unchecked")
	public void sacarReportes() {
		List<Object> resultado = null;
		String strNombre = null, strRutaTemp;
		Archivo objArchivo;
		String[] arrNomColumnas;
		String fechaInicio, fechaFin;
		int ramo=0;
		System.out.println("el ramo es " + inRamo + "fecha inicio " + f_inicio14 + f_fin14);
		
		try {
			sacaFechas();
			
			if(opcion == 14){
				if (inRamo != 0){
					ramo = inRamo;
				}	
			}else if(opcion == 15){
				if (inRamo2 != 0){
					ramo = inRamo2;					
				}
			}
				
			DateFormat inputFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
			DateFormat dateFormat  = new SimpleDateFormat("dd/MM/yyyy");
			
			if(infinicio == null) {
				fechaInicio = Constantes.DEFAULT_STRING;
			} else {
				fechaInicio = dateFormat.format(inputFormat.parse(infinicio.toString()));
			}
				
			if(inffin == null) {
				fechaFin = Constantes.DEFAULT_STRING;
			} else {
				fechaFin = dateFormat.format(inputFormat.parse(inffin.toString()));
			}
			
			resultado = this.reportes.sacarReportes(canal, ramo, fechaInicio, fechaFin, operacion, opcion);
			
			if (opcion != 2 && opcion != 5 && opcion != 6 && opcion != 7 && opcion != 8 && opcion != 10 && opcion != 11){
				if(resultado.size() == 2) {
					arrNomColumnas = (String[]) resultado.get(0);
					resultado = (List<Object>) resultado.get(1);
				}else{
					arrNomColumnas = this.encabezados();
				}
			} else {
				arrNomColumnas = this.encabezados();
			}
			
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			strNombre = "Detalle Resultados - " + new SimpleDateFormat("ddMMyyyy").format(new Date());
			
			objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
			objArchivo.copiarArchivo(arrNomColumnas, resultado, "|");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
			objArchivo.eliminarArchivo();
			
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sacaFechas(){
		switch (opcion) {
		case 2:
			infinicio = f_inicio1;
			inffin = f_fin1;
			break;
		case 3:
			infinicio = f_inicio2;
			inffin = f_fin2;
			break;
		case 4:
			infinicio = f_inicio3;
			inffin = f_fin3;
			break;
		case 5:
			infinicio = f_inicio4;
			inffin = f_fin4;
			break;
		case 6:
			infinicio = f_inicio5;
			inffin = f_fin5;
			break;
		case 7:
			infinicio = f_inicio6;
			inffin = f_fin6;
			break;
		case 8:
			infinicio = f_inicio7;
			inffin = f_fin7;
			break;
		case 10:
			infinicio = f_inicio8;
			inffin = f_fin8;
			break;
		case 11:
			infinicio = f_inicio9;
			inffin = f_fin9;
			break;
		case 12:
			infinicio = f_inicio10;
			inffin = f_fin10;
			break;
		case 14:
			infinicio = f_inicio14;
			inffin = f_fin14;
			break;
		case 15:
			infinicio = f_inicio15;
			inffin = f_fin15;
			break;
		}
	}
	
	public String[] encabezados(){
		String[] arrNomColumnas;
		String columnas;
		
		switch (opcion) {
		case 1:
			columnas = "REGION,DESC REGION,ZONA,DESC ZONA,NU SUCURSAL,DESC SUCURSAL,CANAL,RAMO,POLIZA,CAMPANIA,CTA,SUC BAN,A�O,CUOTA,"
					+ "REMESA,RECIBO,PAGO,NOMBRE,F DESDE,F HASTA,F ASIENTO,MOV,DESC MOV,% Coaseguro,PURA,DPO,RFI,RFJ,RAU,PORCEN,IVA,"
					+ "PRIM,BUC,COASEG,PMA_21,PMA_35,PMA_GRAL,PMA_21_C,PMA_35_C,PMA_GRAL_C,IDENTIFICADOR NEGOCIO,% CESION COLABORACION,"
					+ "IDENTIFICADOR CESION COLAB,% COLABORACION TECNICA,% COMISION BANCO,IMPORTE COMISION BANCO,IMPORTE COMISION ZURICH,"
					+ "MSI,Cve Producto,Descripcion Producto,Prima Pura Sin Asistencia,Base Comsision,% Comision,Importe Comision,"
					+ "Segemento,N Ejecutivo,Nombre Ejecutivo,PRIMA TOTAL 100%,COMPLEMENTO PRIMA 95%,COASEGURADOR,TIPO CONTRATO,"
					+ "% COMISION,CENTRO DE COSTOS,REMESA COBRO,AFILIACION,PRIMA NETA 100%,DERECHO 100%,RECARGOS 100%,COMISION REGARGOS,"
					+ "F COBRO";
			arrNomColumnas = columnas.split(",");
			break;
		case 2:
			columnas = "ramo,operacion,forma pago,coaseguro,codigo cuenta,debe,haber,debe_coa,haber_coa";
			arrNomColumnas = columnas.split(",");
			break;
		case 3:
			columnas = "COMPANY_CODE,LEDGER,OPERATION_DATE,ACCOUNTABLE_DATE,ACCOUNT2,COST_CENTER_PRODUCT,CURRENCYCODE,DEBITORCREDIT,"
					+ "AMOUNT,CONCEPT_KEY,COMMENTS";
			arrNomColumnas = columnas.split(",");
			break;
		case 4:
			columnas = "COMPANY_CODE,LEDGER,OPERATION_DATE,ACCOUNTABLE_DATE,ACCOUNT2,COST_CENTER_PRODUCT,CURRENCYCODE,DEBITORCREDIT,"
					+ "AMOUNT,CONCEPT_KEY,COMMENTS";
			arrNomColumnas = columnas.split(",");
			break;
		case 5:
			columnas = "canal,ramo,dia_porcentaje,primaNeta,asistencia,recargo,comision,ComisionAsistencia,correcto";
			arrNomColumnas = columnas.split(",");
			break;
		case 6:
			columnas = "canal,ramo,dia_porcentaje,primaNeta,asistencia,recargo,comision,ComisionAsistencia,correcto";
			arrNomColumnas = columnas.split(",");
			break;
		case 7:
			columnas = "canal,ramo,dia_porcentaje,primaNeta,asistencia,recargo,comision,ComisionAsistencia,correcto";
			arrNomColumnas = columnas.split(",");
			break;
		case 8:
			columnas = "ramo,codigo_operacion,cuenta,in_cierre,mes,no_registros,mt_debe,mt_haber,mt_debe_coa,mt_haber_coa,porcentaje";
			arrNomColumnas = columnas.split(",");
			break;
		case 9:
			columnas = "Canal,Ramo,Poliza,Anio,Forma Pago,Tipo Movimiento,Origen Emision,Cuota,Numero Recibo,Mov. Contable,"
					+ "F. Contabilizacion,F. Suscripcion,F. Desde Poliza,F. Hasta Poliza,F. Desde Recibo,F. Hasta Recibo,Prima_Neta,"
					+ "Prima Neta Asistencia,Derecho,Recargo,Subtotal,IVA,Prima_Total,Prima Neta Coa,Prima Neta Coa Asistencia,"
					+ "Derecho Coa,Recargo Coa,Subtotal Coa,IVA Coa,Prima Total Coa,Suc. Cobro,% Coa,BUC,MSI,Cve. Producto,"
					+ "Descripcion Producto,Prima Pura Sin Asistencia,Base Comision,% Comision,Importe Comision,Cd Region,Region,"
					+ "Cd Zona,Zona,Segmento,Numero de ejecutivo,Nombre del ejecutivo,Expediente del Ejecutivo,F.Cancelacion,"
					+ "Causa Cancelaci�n,Conducto Cobro";
			arrNomColumnas = columnas.split(",");
			break;
		case 10:
			columnas = "ramo,operacion,periodo,cuenta,mt_debe,mt_haber,mt_debe_coa,mt_haber_coa";
			arrNomColumnas = columnas.split(",");
			break;
		case 11:
			columnas = "ramo,operacion,periodo,cuenta,mt_debe,mt_haber,mt_debe_coa,mt_haber_coa";
			arrNomColumnas = columnas.split(",");
			break;
		case 12:
			columnas = "";
			arrNomColumnas = columnas.split(",");
			break;
		case 13:
			columnas = "Canal,Ramo,Poliza,Anio,Forma Pago,Tipo Movimiento,Origen Emision,Cuota,Numero Recibo,Mov. Contable,"
					+ "F.Contabilizacion,F. Suscripcion,F. Desde Poliza,F. Hasta Poliza,F. Desde Recibo,F. Hasta Recibo,Prima_Neta,"
					+ "Prima Neta Asistencia,Derecho,Recargo,Subtotal,IVA,Prima_Total,Prima Neta Coa,Prima Neta Coa Asistencia,"
					+ "Derecho Coa,Recargo Coa,Subtotal Coa,IVA Coa,Prima Total Coa,Suc. Cobro,% Coa,BUC,MSI,Cve. Producto,"
					+ "Descripcion Producto,Cd Regi�n, Regi�n,Cd Zona,Zona,N�mero de ejecutivo,Nombre del ejecutivo";
			arrNomColumnas = columnas.split(",");
			break;
		//RESERVAS
		case 14:
			columnas = "POLIZA,NU_CUENTA,NOMBRE,"
					+ "F_P, RP,INICIO,FIN,T_Por_Dev,"
					+ "Su_Ase_basica,Su_Ase_BIT,PrimaNetaCai,PrimaNetaDi,PrimasNetasSERF,"
					+ "EXTR_PRI,Dotal_CP,RBASICA,BIT,ReservasCAI,ReservasDI,ReservasSERF,"
					+ "EXTRAPRI,DotalCP,TOTAL,t1,t2,"
					+ "ESTADO,SEXO ";
			arrNomColumnas = columnas.split(",");
			break;
		//SUMA ASEGURADS
		case 15:
			columnas = " POLIZA,NU_CUENTA,INICIO,"
					+ "NOMBRE,SUMA_ASEG,Su_Ase_basica,Su_Ase_BIT,"
					+ "Su_Ase_invalidez,Su_Ase_Accidental,Su_Ase_SERF,Extra_Prima,Sum_Cob_Dotal_CP,"
					+ "Sum_Aseg_Tot,Num_Aseg_Cob_Bas,Num_Aseg_Cob_BIT,FR_PAGO,RECIBO,"
					+ "Num_Aseg_Cob_INV,Num_Aseg_Cob_ACC,Num_Aseg_Cob_SERF,Num_Aseg_Cob_Extr_PRRIM,Num_Aseg_Cob_Dotal_CP,"
					+ "PRIMA_PURA,FECHA_HASTA,SEXO,ESTADO,"
					+ "ASEGURADOS ";				
			arrNomColumnas = columnas.split(",");
			break;
		default:
			columnas = "";
			arrNomColumnas = columnas.split(",");
			break;
		}
		
		return arrNomColumnas;
	}
}
