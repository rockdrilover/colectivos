/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

/**
 * @author Hery 
 *
 */
@Controller
@Scope("session")	
public class BeanRehabilitacion {

	@Resource
	private ServicioProcesos servicioProcesos;
	@Resource
	private ServicioCarga servicioCarga;
	
	private Log log = LogFactory.getLog(this.getClass());
	private Short canal;
	private Short ramo;
	private Long  poliza;
	
	
	private List<String> opciones;
	
	private File archivo;
	private String nombreArchivo;
	private StringBuilder message;
	private String respuesta;
	private boolean rehabilitar;
	
	private List<Object> listaEstatusRehabilitacion;
		
	public BeanRehabilitacion() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.setlistaEstatusRehabilitacion(new ArrayList<Object>());
		this.opciones = new ArrayList<String>();
		this.opciones.clear();
		this.opciones.add("1");
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_REHABILITACION);
	}
	
	
	/**
	 * 
	 */
	public String rehabilitar(){
		
		List<Object> lista;
		StringBuffer codOpcion;
		
		this.message.append("Iniciando proceso de rehabilitación");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		try {
			codOpcion = new StringBuffer("");
			
			this.canal = 1;
			
			for (String x:opciones)codOpcion.append(x); 
								
			if(this.ramo    != null  &&
			   this.canal   != null  &&
			   this.poliza  != null  &&
			   this.archivo != null) {
			   			
				// Leemos archivo para rehabilitación 
				lista = this.leerArchivoRehabilitacion(this.archivo, ",");
				
				// Guardamos cancelaciones en tabla temporal
				message.append("Iniciando proceso de carga....");

				Utilerias.resetMessage(message);
				
				this.servicioCarga.guardarObjetos(lista);
				
				message.append("El procedo de carga se realizo correctamente.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				message.append("Comenzando proceso de rehabilitación ramo:").append(this.ramo.toString()).append(" poliza:").append(this.poliza.toString()).append(".");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
							
				this.servicioProcesos.rehabilitacion(this.canal, this.ramo, this.poliza);
				
				this.nombreArchivo = this.archivo.getName().substring(this.archivo.getName().lastIndexOf("\\") + 1);
				
				
				// Estatus rehabilitación				
				this.listaEstatusRehabilitacion = this.servicioCarga.estatusRehabilitacion(this.canal, this.ramo, this.poliza, nombreArchivo);

				lista.clear();
				
				message.append("Proceso de rehabilitación finalizó, descargue el archivo de errores para revisar las inconsistencias.\n");				
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				FacesUtils.addInfoMessage(this.message.toString());
				Utilerias.resetMessage(message);
				
				return NavigationResults.SUCCESS;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("El proceso de rehabilitación falló.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		return respuesta;
	}
	
	/**
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void listener(UploadEvent event) throws Exception{
		
		UploadItem item = event.getUploadItem();
		File archivoTemporal;
		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		String nombreArchivo = null;
		String nombreRuta;
		
		if(item.isTempFile()){
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			//ystem.out.println("Uno");
		}
		else{
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			
			/**
			 * Guardar el archivo en  carpeta del proyecto
			 */
			nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
			
			archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
			FileUtil.write(archivoTemporal, item.getData());
			
			this.archivo = archivoTemporal;
		}
		
	    this.message.append("Archivo cargado correctamente, puede continuar con el proceso de rehabilitación.");
		this.log.debug(this.message.toString());
		this.respuesta = this.message.toString();
		Utilerias.resetMessage(message);
		
	}
	
	/**
	 * 
	 * @return
	 */
public String erroresRehabilitacion(){	
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		this.canal = 1;
		
		if (this.archivo != null) this.nombreArchivo = this.archivo.getName(); else this.nombreArchivo = "*";	
		
		if(this.canal != null && this.ramo != null && this.poliza != null && this.nombreArchivo != null){
		
			try {
				inParams.put("CANAL",  this.canal.intValue());
				inParams.put("RAMO",   this.ramo.intValue());
				inParams.put("POLIZA", this.poliza.intValue());
				inParams.put("ARCHIVO",this.nombreArchivo);
				
				
				rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/Rehabilitacion.csv");
				rutaReporte	= FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/Rehabilitacion.jrxml");
				
				this.servicioCarga.reporteErrores(rutaTemporal, rutaReporte, inParams);
							
				File file = new File(rutaTemporal);
				
				FacesContext context = FacesContext.getCurrentInstance();
				
				if(!context.getResponseComplete()){
				
					input = new FileInputStream(file);
					bytes = new byte[1000];
					
					
					String contentType = "application/vnd.ms-excel";

					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					
					response.setContentType(contentType);
					
					DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
			    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());
			    	
					ServletOutputStream out = response.getOutputStream();
					
					while((read = input.read(bytes))!= -1){
						
						out.write(bytes, 0 , read);
					}
					
					input.close();
					out.flush();
					out.close();
					file.delete();

					context.responseComplete();

				}
				
				// Borramos los datos de la precarga y carga.
				this.servicioCarga.borrarDatosTemporales(this.canal, this.ramo, this.poliza, 0, this.nombreArchivo,"REH"); 
				
				this.message.append("Archivo de errores descargado.");
				this.log.debug(message.toString());
				FacesUtils.addInfoMessage(message.toString());
				Utilerias.resetMessage(message);

				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				// TODO: handle exception
				this.message.append("Error al generar el archivo de errores.");
				this.log.debug(message.toString());
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}			

		}
		else
		{
			this.message.append("El archivo sólo se puede descargar hasta que se haya realizado el proceso.");
			this.log.debug(message.toString());
			FacesUtils.addInfoMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}	
	}
	

	public List<Object> leerArchivoRehabilitacion(File file, String delimitador) {
		// TODO Auto-generated method stub

		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCarga preCarga;
		PreCargaId id;
		List<Object> lista = null;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);
		
		boolean hayCifrasContro = false;
		
		String[] headers = null;
		Map<String, String> mh;
						
		try 
		{
			this.message.append("Abriendo archivo de carga");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));

			if(csv.readHeaders())
			{
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
				bean = new BeanPreCarga();			
				
				this.message.append("Leyendo datos pre-carga rehabilitación.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
							
								
				while(csv.readRecord())
				{	
					if (csv.get(0).equals("*")) hayCifrasContro = true;
					
					if (!hayCifrasContro){
						if (!csv.get(0).trim().equals("")){

							id = new PreCargaId(this.canal, this.ramo, this.poliza, csv.get(mh.get("identificador")), "REH", csv.get(mh.get("base_calculo")));
							if (csv.get(mh.get("base_calculo")) == null || csv.get(mh.get("base_calculo")).length() <= 0 ) id.setCopcSumaAsegurada("0");

							bean.setCopcCargada("14");  //para diferenciar de la emision.

							bean.setId(id);

							bean.setCopcPrima(csv.get(mh.get("prima")));
							bean.setCopcDato9(csv.get(mh.get("buc_empresa")));
							bean.setCopcBcoProducto(csv.get(mh.get("producto_bco")));
							bean.setCopcSubProducto(csv.get(mh.get("plan_bco")));
							bean.setCopcCdProducto(csv.get(mh.get("producto")));
							bean.setCopcCdPlan(csv.get(mh.get("plan")));
							bean.setCopcDato2(csv.get(mh.get("tarifa")));
							bean.setCopcDato29(csv.get(mh.get("suma_aseg")));
							bean.setCopcFeIngreso(csv.get(mh.get("fec_ingreso")));
																												
							System.out.println(mh.get("disposicion"));
							bean.setCopcDato11(nombreArchivo);
							
							preCarga = (PreCarga) ConstruirObjeto.crearBean(PreCarga.class, bean);
							
							lista.add(preCarga);
						}
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede cargar la informacion del archivo.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		return lista;
	}

	/**
	 * @return the servicioProcesos
	 */
	public ServicioProcesos getServicioProcesos() {
		return servicioProcesos;
	}

	/**
	 * @param servicioProcesos the servicioProcesos to set
	 */
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}

	/**
	 * @return the servicioCarga
	 */
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}

	/**
	 * @param servicioCarga the servicioCarga to set
	 */
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	/**
	 * @return the Canal
	 */
	public Short getCanal() {
		return canal;
	}

	/**
	 * @param Canal the Canal to set
	 */
	public void setCanal(Short canal) {
		this.canal = canal;
	}

	/**
	 * @return the Ramo
	 */
	public Short getRamo() {
		return ramo;
	}

	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}

	/**
	 * @return the Poliza
	 */
	public Long getPoliza() {
		return poliza;
	}

	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
		if (this.poliza == 0){
			rehabilitar = true;
		}else{
			rehabilitar = false;
			opciones.clear();
			opciones.add("1");
		}
	}

	/**
	 * @return the archivo
	 */
	public File getArchivo() {
		return archivo;
	}

	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return the habilitar
	 */
	public Boolean getRehabilitar() {
		return !this.rehabilitar;
	}
	
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	/**
	 * @param listaEstatusRehabilitacion the listaEstatusRehabilitacion to set
	 */
	public void setlistaEstatusRehabilitacion(List<Object> listaEstatusRehabilitacion) {
		this.listaEstatusRehabilitacion = listaEstatusRehabilitacion;
	}

	/**
	 * @return the listaEstatusRehabilitacion
	 */
	public List<Object> getListaEstatusCancelacion() {
		return listaEstatusRehabilitacion;
	}
}
