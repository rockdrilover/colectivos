/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estatus;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstatus;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanListaEstatus implements InitializingBean{

	@Resource
	private ServicioEstatus servicioEstatus;
	private Log log = LogFactory.getLog(this.getClass());
	
	private List<Object> listaBeansEstatus;
	private List<Object> listaComboEstatus;
	private List<Object> listaEstatusCancelacion;
	
	private int numPagina;
	private StringBuilder message;
	
	
	/**
	 * @return the listaBeansEstatus
	 */
	public List<Object> getListaBeansEstatus() {
		return listaBeansEstatus;
	}


	/**
	 * @return the listaComboEstatus
	 */
	public List<Object> getListaComboEstatus() {
		return listaComboEstatus;
	}


	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}


	/**
	 * @param listaBeansEstatus the listaBeansEstatus to set
	 */
	public void setListaBeansEstatus(List<Object> listaBeansEstatus) {
		this.listaBeansEstatus = listaBeansEstatus;
	}


	/**
	 * @param listaComboEstatus the listaComboEstatus to set
	 */
	public void setListaComboEstatus(List<Object> listaComboEstatus) {
		this.listaComboEstatus = listaComboEstatus;
	}


	/**
	 * @param listaEstatusCancelacion the listaEstatusCancelacion to set
	 */
	public void setListaEstatusCancelacion(List<Object> listaEstatusCancelacion) {
		this.listaEstatusCancelacion = listaEstatusCancelacion;
	}


	/**
	 * @return the listaEstatusCancelacion
	 */
	public List<Object> getListaEstatusCancelacion() {
		return listaEstatusCancelacion;
	}


	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}


	/**
	 * @param servicioEstatus the servicioEstatus to set
	 */
	public void setServicioEstatus(ServicioEstatus servicioEstatus) {
		this.servicioEstatus = servicioEstatus;
	}


	/**
	 * @return the servicioEstatus
	 */
	public ServicioEstatus getServicioEstatus() {
		return servicioEstatus;
	}


	public BeanListaEstatus() {
		// TODO Auto-generated constructor stub
		this.listaBeansEstatus = new ArrayList<Object>();
		this.listaComboEstatus = new ArrayList<Object>();
		this.listaEstatusCancelacion = new ArrayList<Object>();
		this.numPagina = 1;
		this.message = new StringBuilder();
		
		
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
		try {
			
			List<Estatus> lista = this.servicioEstatus.obtenerObjetos(Estatus.class);
			
			for(Estatus objeto: lista){
				
				BeanEstatus beanEstatus = (BeanEstatus) ConstruirObjeto.crearBean(BeanEstatus.class, objeto);
				beanEstatus.setServicioEstatus(this.servicioEstatus);
				
				this.listaBeansEstatus.add(beanEstatus);
				this.listaComboEstatus.add(new SelectItem(beanEstatus.getAlesCdEstatus().toString(), beanEstatus.getAlesDescripcion()));
				
				// Lista de estatus para las cancelaciones de las polizas
				if(beanEstatus.getAlesDescCorta().equalsIgnoreCase("CAUSAANULACION")){
					
					listaEstatusCancelacion.add(new SelectItem(beanEstatus.getAlesCdEstatus().toString(), beanEstatus.getAlesDescripcion()));
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede inicializar la lista de estatus.");
			this.log.debug(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
	}

}
