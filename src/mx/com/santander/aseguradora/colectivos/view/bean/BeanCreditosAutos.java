package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

//import mx.com.santander.aseguradora.colectivos.view.util.FacesUtils;
@Controller
@Scope("session")
public class BeanCreditosAutos{

	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioCertificado servicioCertificado;
	private Log log = LogFactory.getLog(this.getClass());
	private List<BeanCertificado> listaCertificados;
	private List<BeanCertificado> listaCertificados1;
	private StringBuilder message;
	private String respuesta;
	private List<Object> resultado;
//	private List<String> cifrasControl;
	private Short inOrigen;
	private File archivo;
	private String nombreArchivo;
	private int numPagina;


	/**
	 * @param servicioCarga the servicioCarga to set
	 */
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	/**
	 * @return the servicioCarga
	 */
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}
	
	/**
	 * @return the inOrigen
	 */
	public Short getInOrigen() {
		return inOrigen;
	}

	/**
	 * @param inOrigen the inOrigen to set
	 */
	public void setInOrigen(Short inOrigen) {
		this.inOrigen = inOrigen;
	}

	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}

	/**
	 * @return the archivo
	 */
	public File getArchivo() {
		return archivo;
	}
	
	
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public List<BeanCertificado> getListaCertificados() {
		return listaCertificados;
	}

	public void setListaCertificados(List<BeanCertificado> listaCertificados) {
		this.listaCertificados = listaCertificados;
	}
	public List<BeanCertificado> getListaCertificados1() {
		return listaCertificados1;
	}

	public void setListaCertificados1(List<BeanCertificado> listaCertificados1) {
		this.listaCertificados1 = listaCertificados1;
	}

	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.respuesta = mensaje;
	}
	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return respuesta;
	}
	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(List<Object> resultado) {
		this.resultado = resultado;
	}
	/**
	 * @return the resultado
	 */
	public List<Object> getResultado() {
		return resultado;
	}
	
	public int getNumPagina() {
		return numPagina;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public BeanCreditosAutos() {
		
		this.message = new StringBuilder();
		this.resultado = new ArrayList<Object>();
		this.listaCertificados = new ArrayList<BeanCertificado>();
		this.listaCertificados1 = new ArrayList<BeanCertificado>();
//		this.cifrasControl = new ArrayList<String>();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CREDITOSAUTOS);
	}
	public String cargar(){
		System.out.println("Iniciando proceso de carga");
		this.listaCertificados.clear();
		List<Object> lista = null;
		List<Object[]> lista1 = null;
		List<Object[]> lista2 = null;
		System.out.println("Entre en cargar....(beanCreditosAutos.cargar)");
		if(this.archivo != null)
		{
			try {
					lista = this.leerArchivoAutos(this.archivo, ",");
										
					message.append("Iniciando proceso de carga....");
					this.log.info(message.toString());
					
					//Ejecuta proceso para llenar la table de COLECTIVOS_PRECARGA
					this.servicioCarga.guardarObjetos(lista);
					
					//Hace la actualizacion de COLECTIVOS_CERTIFICADOS
					lista1= this.servicioCertificado.seleccionaDatosAutos();
					for(Object[] certificado: lista1){

						this.actualizaDatosAutos(((BigDecimal)certificado[0]).shortValue(), 
												 ((BigDecimal)certificado[1]).shortValue(), 
												 ((BigDecimal)certificado[2]).longValue(), 
												 ((BigDecimal)certificado[3]).longValue(), 
												 certificado[4].toString(),
												 certificado[5].toString(),
												 certificado[6].toString());
					}

					//Limpiamos lista para utilizacion
					lista.clear();
					lista = this.servicioCertificado.generaReporteAutos();
					lista2= this.servicioCertificado.cifrasResumenAutos();
					for(Object[] certificado: lista2){

						BeanCertificado bean = new BeanCertificado();
						bean.setCoceCampov5(certificado[0].toString());
						bean.setCoceCampov1(certificado[1].toString());
						bean.setServicioCertificado(this.servicioCertificado);

						this.listaCertificados.add(bean);
					}
					this.resultado.clear();
					this.message.append("El proceso de carga finaliz�");
					this.log.info(message.toString());
					this.respuesta = this.message.toString();
					
					//lista.clear();
					//this.archivo.delete();
					
					return null;
					
					//return NavigationResults.SUCCESS;
				}
				catch (DataAccessException e) {
					this.message.append("Hubo un error al guardar los datos.");
					this.log.error(message.toString(), e);
					this.respuesta = this.message.toString();
					// Borramos archivo temporal
					this.archivo.delete();
					return NavigationResults.FAILURE;
				}
				catch (Exception e) {
					this.message.append("No se puede realizar el proceso de carga.").append(e.getLocalizedMessage());
					this.respuesta = this.message.toString();
					this.log.error(message.toString(), e);
					Utilerias.resetMessage(message);
					// Borramos archivo temporal
					this.archivo.delete();
					
					return NavigationResults.FAILURE;
				}
		}
		else
		{
			this.message.append("El archivo a cargar no es correcto, favor de verificarlo.");
			this.log.debug(message.toString());
			this.respuesta = this.message.toString();
			Utilerias.resetMessage(message);
			return NavigationResults.FAILURE;
			
		} // end if archivo not null
	}
	public List<Object> leerArchivoAutos(File file, String delimitador) {
		// TODO Auto-generated method stub
		System.out.println("Entre en cargar....(beanCreditosAutos.leerArchivoAutos)");
		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCargaId id;
		PreCarga preCarga = null;
		List<Object> lista = null;
		
		String[] headers = null;
		Map<String, String> mh;
		
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

						
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			
			if(csv.readHeaders())
			{
				
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				bean = new BeanPreCarga();
				
				
				lista = new ArrayList<Object>();	
				this.message.append("Validando los tipos de datos del archivo");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				while(csv.readRecord())
				{
					
					if (!csv.get(0).trim().equals("")){
						id = new PreCargaId(new Short("666"), new Short("666"), new Long("666"), csv.get(mh.get("cred_anterior")), "AUT", csv.get(mh.get("imp_conc")));
						
						bean.setId(id);
						
						bean.setCopcCuenta(csv.get(mh.get("cred_nuevo")));
						bean.setCopcNumCliente(csv.get(mh.get("num_cte")));
						bean.setCopcDato11(nombreArchivo);
					
						preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
						
						lista.add(preCarga);
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
				
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pud� cargar la informacion.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		
		return lista;
		
	}
	public void listener(UploadEvent event) throws Exception{
		System.out.println("Entre en cargar....(beanCreditosAutos.listener)");
		UploadItem item = event.getUploadItem();
		File archivoTemporal;
		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		String nombreArchivo = null;
		String nombreRuta;
		
		if(item.isTempFile()){
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			
		}
		else{
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			
			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			/**
			 * Guardar el archivo en  carpeta del proyecto
			 */
			nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
			
			archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
			FileUtil.write(archivoTemporal, item.getData());
			
			
			
			this.archivo = archivoTemporal;
		}
		
		this.message.append("Archivo cargado al servidor correctamente");
		this.log.debug(this.message.toString());
		this.respuesta = this.message.toString();
		Utilerias.resetMessage(message);
	}
	public void actualizaDatosAutos(Short suc, Short ramo, Long poliza, Long nucertif, String credito, String nomarch, String credito2)throws Excepciones{
		try {
			System.out.println("actualizarDatosAutos");
			this.servicioCertificado.actualizaDatosAutos(suc, ramo, poliza, nucertif, credito, nomarch, credito2);
		}catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
	}
	public String generaReporteEmisionAutos(){
		
		this.listaCertificados1.clear();
		
		
		try{
			String cred	  		= null;
			String buccte  		= null;
			String observ		= null;
			String archcarga	= null;
 		      			             
            log.info("Reporte de emision autos");
            
            List<Object[]> lista= this.servicioCertificado.generaReporteEmisionAutos();
			BufferedWriter  output = null ;
			int a=0;
			 
			String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			File file = new File ( ruta + "ReporteEmisionAutos.txt" ); 
			output = new BufferedWriter ( new FileWriter ( file ));  
			log.info("Se imprimio el archivo " + a);
			output.write("Credito,BUC,Observaciones,ArchivoCarga");
			output.newLine();

			for(Object[] certificado: lista){

				StringBuffer sb = new StringBuffer ();
				cred = certificado[0].toString();
				buccte = certificado[1].toString();
				observ = certificado[2].toString();
				archcarga = certificado[3].toString();
				
				sb.append(cred + ",");
				sb.append(buccte + ",");
				sb.append(observ + ",");
				sb.append(archcarga);
				
				output.write(sb.toString());
				output.newLine();
				a=a+1;
			}

			output.close (); 
		    log.info ( "El archivo se ha creado correctamente") ; 
		    this.message.append("Archivo creado correctamente");
		    
		    HttpServletResponse response = FacesUtils.getServletResponse();
		    InputStream in = new FileInputStream (file);		    
		    OutputStream out = response.getOutputStream ();
		       
		    response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename=\"ReporteEmisionAutos.txt" + "\"");
			

			int read = 0;
			byte[] bytes = new byte[2048];
			
			while((read = in.read(bytes)) != -1){
				out.write(bytes,0,read);
			}	        

			in.close();
		
			out.flush();
			out.close();
			
			file.delete();
			
			
			this.log.debug(this.message.toString());
	        this.respuesta = this.message.toString();
	        Utilerias.resetMessage(message);
			
	        this.servicioCertificado.borrarDatosAutosPrecarga();
	        
			FacesContext.getCurrentInstance().responseComplete();
	        return null;

		}
		catch (Exception e){
			this.message.append("Error al recuperar datos de reporte emision.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);			
		}
	}
}
