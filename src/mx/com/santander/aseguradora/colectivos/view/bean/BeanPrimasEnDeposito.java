package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioPrimasDeposito;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class BeanPrimasEnDeposito {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioPrimasDeposito servicioPrimasDeposito;
	
	
	private String display1 = "none";
	private String display2 = "none";
	private String display3 = "none";
	private String display4 = "none";
	private String display5 = "none";
	private String display6 = "none";
	private String display7 = "none";
	private String display8 = "none";
	private String display9 = "none";
	private String display10 = "none";
	private String display11 = "none";
	private String display12 = "none";
	private String display13 = "none";
	private String display14 = "none";
	private String display15 = "none";
	private Short mes;
	private Short mes2;
	private Short ramo;
	private Short ramo2;
	private Integer inIdVenta;
	private Integer inIdVenta2; 
	private String opcion;
	private int numPagina;
	private Long poliza;
	private Long poliza2;
	private String producto;
	private String respuesta;
	private BigDecimal sumaDepPyme;
	private BigDecimal sumaRecPyme;
	private BigDecimal sumaDepHpu;
	private BigDecimal sumaRecHpu;
	private BigDecimal sumaDepLci;
	private BigDecimal sumaRecLci;
	private BigDecimal sumaDepLe;
	private BigDecimal sumaRecLe;
	private BigDecimal sumaTotalDep;
	private BigDecimal sumaTotalRec;
	private int bandera2;
	private BigDecimal sumaMesDep;
	private BigDecimal sumaMesRec;
	private String rutaReportes;
	private String param;
	private String fechaIni="";
	private String fechaFin="";
	private String anioActual = GestorFechas.formatDate(new Date(),"yyyy");
	
	
	private List<String> productos = new ArrayList<String>();

	private List<BeanCertificado> listaTraeFotos;
	private List<BeanCertificado> listaPrimasEne;
	private List<BeanCertificado> listaPrimasFeb;
	private List<BeanCertificado> listaPrimasMar;
	private List<BeanCertificado> listaPrimasAbr;
	private List<BeanCertificado> listaPrimasMay;
	private List<BeanCertificado> listaPrimasJun;
	private List<BeanCertificado> listaPrimasJul;
	private List<BeanCertificado> listaPrimasAgo;
	private List<BeanCertificado> listaPrimasSep;
	private List<BeanCertificado> listaPrimasOct;
	private List<BeanCertificado> listaPrimasNov;
	private List<BeanCertificado> listaPrimasDic;
	private List<BeanCertificado> listaGlobal;
	private List<BeanCertificado> listaGuardaFotos;
	
	public BeanPrimasEnDeposito(){		
		
		this.mes=0;
		this.mes2=0;
		this.inIdVenta = -1;
		this.opcion="0";
		this.numPagina = 1;		
		this.producto="";	
		poliza=-1L;
		
		sumaDepPyme = sumaDepHpu = sumaDepLci = sumaDepLe = sumaRecHpu =
		sumaRecLci = sumaRecLe = sumaRecPyme = sumaMesDep = sumaMesRec =
		sumaTotalDep = sumaTotalRec= BigDecimal.ZERO;
		this.bandera2=0;
		this.param="";
		
		this.rutaReportes = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		this.listaPrimasEne = new ArrayList<BeanCertificado>();
		this.listaPrimasFeb = new ArrayList<BeanCertificado>();
		this.listaPrimasMar = new ArrayList<BeanCertificado>();
		this.listaPrimasAbr = new ArrayList<BeanCertificado>();
		this.listaPrimasMay = new ArrayList<BeanCertificado>();
		this.listaPrimasJun = new ArrayList<BeanCertificado>();
		this.listaPrimasJul = new ArrayList<BeanCertificado>();
		this.listaPrimasAgo = new ArrayList<BeanCertificado>();
		this.listaPrimasSep = new ArrayList<BeanCertificado>();
		this.listaPrimasOct = new ArrayList<BeanCertificado>();
		this.listaPrimasNov = new ArrayList<BeanCertificado>();
		this.listaPrimasDic = new ArrayList<BeanCertificado>();
		this.listaGlobal = new ArrayList<BeanCertificado>();
		this.listaTraeFotos = new ArrayList<BeanCertificado>();
		this.listaGuardaFotos = new ArrayList<BeanCertificado>();
	
		setRespuesta(Constantes.DEFAULT_STRING);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PRIMAS_EN_DEPOSITO);
	}
	public Short getMes() {
		return mes;
	}
	public void setMes(Short mes) {
		this.mes = mes;
	}
	public String getDisplay1() {
		return display1;
	}
	public void setDisplay1(String display1) {
		this.display1 = display1;
	}
	public String getDisplay2() {
		return display2;
	}
	public void setDisplay2(String display2) {
		this.display2 = display2;
	}
	public String getDisplay3() {
		return display3;
	}
	public void setDisplay3(String display3) {
		this.display3 = display3;
	}
	public String getDisplay4() {
		return display4;
	}
	public void setDisplay4(String display4) {
		this.display4 = display4;
	}
	public String getDisplay5() {
		return display5;
	}
	public void setDisplay5(String display5) {
		this.display5 = display5;
	}
	public String getDisplay6() {
		return display6;
	}
	public void setDisplay6(String display6) {
		this.display6 = display6;
	}
	public String getDisplay7() {
		return display7;
	}
	public void setDisplay7(String display7) {
		this.display7 = display7;
	}
	public String getDisplay8() {
		return display8;
	}
	public void setDisplay8(String display8) {
		this.display8 = display8;
	}
	public String getDisplay9() {
		return display9;
	}
	public void setDisplay9(String display9) {
		this.display9 = display9;
	}
	public String getDisplay10() {
		return display10;
	}
	public void setDisplay10(String display10) {
		this.display10 = display10;
	}
	public String getDisplay11() {
		return display11;
	}
	public void setDisplay11(String display11) {
		this.display11 = display11;
	}
	public String getDisplay12() {
		return display12;
	}
	public void setDisplay12(String display12) {
		this.display12 = display12;
	}
	
	public String getDisplay13() {
		return display13;
	}
	public void setDisplay13(String display13) {
		this.display13 = display13;
	}
	public String getDisplay14() {
		return display14;
	}
	public String getDisplay15() {
		return display15;
	}
	public void setDisplay15(String display15) {
		this.display15 = display15;
	}
	public List<BeanCertificado> getListaGuardaFotos() {
		return listaGuardaFotos;
	}
	public void setListaGuardaFotos(List<BeanCertificado> listaGuardaFotos) {
		this.listaGuardaFotos = listaGuardaFotos;
	}
	public List<BeanCertificado> getListaTraeFotos() {
		return listaTraeFotos;
	}
	public void setListaTraeFotos(List<BeanCertificado> listaTraeFotos) {
		this.listaTraeFotos = listaTraeFotos;
	}
	public Short getRamo2() {
		return ramo2;
	}
	public void setRamo2(Short ramo2) {
		this.ramo2 = ramo2;
	}
	public Long getPoliza2() {
		return poliza2;
	}
	public void setPoliza2(Long poliza2) {
		this.poliza2 = poliza2;
	}
	public Long getPoliza() {
		return poliza;
	}
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}

	public ServicioPrimasDeposito getServicioPrimasDeposito() {
		return servicioPrimasDeposito;
	}
	public void setServicioPrimasDeposito(ServicioPrimasDeposito servicioPrimasDeposito) {
		this.servicioPrimasDeposito = servicioPrimasDeposito;
	}
	public BigDecimal getSumaTotalDep() {
		return sumaTotalDep;
	}
	public void setSumaTotalDep(BigDecimal sumaTotalDep) {
		this.sumaTotalDep = sumaTotalDep;
	}
	public BigDecimal getSumaTotalRec() {
		return sumaTotalRec;
	}
	public void setSumaTotalRec(BigDecimal sumaTotalRec) {
		this.sumaTotalRec = sumaTotalRec;
	}
	public BigDecimal getSumaDepPyme() {
		return sumaDepPyme;
	}
	public void setSumaDepPyme(BigDecimal sumaDepPyme) {
		this.sumaDepPyme = sumaDepPyme;
	}
	public BigDecimal getSumaRecPyme() {
		return sumaRecPyme;
	}
	public void setSumaRecPyme(BigDecimal sumaRecPyme) {
		this.sumaRecPyme = sumaRecPyme;
	}
	public BigDecimal getSumaDepHpu() {
		return sumaDepHpu;
	}
	public void setSumaDepHpu(BigDecimal sumaDepHpu) {
		this.sumaDepHpu = sumaDepHpu;
	}
	public BigDecimal getSumaRecHpu() {
		return sumaRecHpu;
	}
	public void setSumaRecHpu(BigDecimal sumaRecHpu) {
		this.sumaRecHpu = sumaRecHpu;
	}
	public BigDecimal getSumaDepLci() {
		return sumaDepLci;
	}
	public void setSumaDepLci(BigDecimal sumaDepLci) {
		this.sumaDepLci = sumaDepLci;
	}
	public BigDecimal getSumaRecLci() {
		return sumaRecLci;
	}
	public void setSumaRecLci(BigDecimal sumaRecLci) {
		this.sumaRecLci = sumaRecLci;
	}
	public BigDecimal getSumaDepLe() {
		return sumaDepLe;
	}
	public void setSumaDepLe(BigDecimal sumaDepLe) {
		this.sumaDepLe = sumaDepLe;
	}
	public BigDecimal getSumaRecLe() {
		return sumaRecLe;
	}
	public void setSumaRecLe(BigDecimal sumaRecLe) {
		this.sumaRecLe = sumaRecLe;
	}
	public List<BeanCertificado> getListaGlobal() {
		return listaGlobal;
	}
	public void setListaGlobal(List<BeanCertificado> listaGlobal) {
		this.listaGlobal = listaGlobal;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public List<BeanCertificado> getListaPrimasEne() {
		return listaPrimasEne;
	}
	public void setListaPrimasEne(List<BeanCertificado> listaPrimasEne) {
		this.listaPrimasEne = listaPrimasEne;
	}
	public List<BeanCertificado> getListaPrimasFeb() {
		return listaPrimasFeb;
	}
	public void setListaPrimasFeb(List<BeanCertificado> listaPrimasFeb) {
		this.listaPrimasFeb = listaPrimasFeb;
	}
	public List<BeanCertificado> getListaPrimasMar() {
		return listaPrimasMar;
	}
	public void setListaPrimasMar(List<BeanCertificado> listaPrimasMar) {
		this.listaPrimasMar = listaPrimasMar;
	}
	public List<BeanCertificado> getListaPrimasAbr() {
		return listaPrimasAbr;
	}
	public void setListaPrimasAbr(List<BeanCertificado> listaPrimasAbr) {
		this.listaPrimasAbr = listaPrimasAbr;
	}
	public List<BeanCertificado> getListaPrimasMay() {
		return listaPrimasMay;
	}
	public void setListaPrimasMay(List<BeanCertificado> listaPrimasMay) {
		this.listaPrimasMay = listaPrimasMay;
	}
	public List<BeanCertificado> getListaPrimasJun() {
		return listaPrimasJun;
	}
	public void setListaPrimasJun(List<BeanCertificado> listaPrimasJun) {
		this.listaPrimasJun = listaPrimasJun;
	}
	public List<BeanCertificado> getListaPrimasJul() {
		return listaPrimasJul;
	}
	public void setListaPrimasJul(List<BeanCertificado> listaPrimasJul) {
		this.listaPrimasJul = listaPrimasJul;
	}
	public List<BeanCertificado> getListaPrimasAgo() {
		return listaPrimasAgo;
	}
	public void setListaPrimasAgo(List<BeanCertificado> listaPrimasAgo) {
		this.listaPrimasAgo = listaPrimasAgo;
	}
	public List<BeanCertificado> getListaPrimasSep() {
		return listaPrimasSep;
	}
	public void setListaPrimasSep(List<BeanCertificado> listaPrimasSep) {
		this.listaPrimasSep = listaPrimasSep;
	}
	public List<BeanCertificado> getListaPrimasOct() {
		return listaPrimasOct;
	}
	public void setListaPrimasOct(List<BeanCertificado> listaPrimasOct) {
		this.listaPrimasOct = listaPrimasOct;
	}
	public List<BeanCertificado> getListaPrimasNov() {
		return listaPrimasNov;
	}
	public void setListaPrimasNov(List<BeanCertificado> listaPrimasNov) {
		this.listaPrimasNov = listaPrimasNov;
	}
	public List<BeanCertificado> getListaPrimasDic() {
		return listaPrimasDic;
	}
	public void setListaPrimasDic(List<BeanCertificado> listaPrimasDic) {
		this.listaPrimasDic = listaPrimasDic;
	}
	public void setDisplay14(String display14) {
		this.display14 = display14;
	}
	public int getInIdVenta2() {
		return inIdVenta2;
	}
	public void setInIdVenta2(int inIdVenta2) {
		this.inIdVenta2 = inIdVenta2;
	}
	public void setInIdVenta(int inIdVenta) {
		this.inIdVenta = inIdVenta;
	}
	public Integer getInIdVenta() {
		return inIdVenta;
	}
	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	
	public Short getMes2() {
		return mes2;
	}
	public int getNumPagina() {
		return numPagina;
	}
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	public void setMes2(Short mes2) {
		this.mes2 = mes2;
	}

	public String muestraMesPrima(){	
		sumaDepHpu = sumaRecHpu = sumaDepLci = sumaRecLci = sumaDepLe =
		sumaRecLe = sumaDepPyme = sumaRecPyme = BigDecimal.ZERO;		
		this.setOpcion("1");
		this.listaGlobal.clear();
		this.listaGuardaFotos.clear();
		
		try {
			muestraIdVenta();
			if(producto.equals("0")){
				producto=producto;
			}else{
				producto=inIdVenta.toString();
			}
			productos = servicioPrimasDeposito.consultaProductosColectivos(producto);
			BeanCertificado cer = null;
			muestraIdVenta();
			for (String prd : productos) {				
				cer = new BeanCertificado();
				cer.setDesc1(prd);
				cer.setCoceCampon7BD(BigDecimal.ZERO);
				cer.setCoceCampon8BD(BigDecimal.ZERO);
				listaGlobal.add(cer);				
			}
		} catch (Exception e){
			e.printStackTrace();
			FacesUtils.addInfoMessage("Error al Consultar el catalogo de productos "  );
		}

		if(mes != 0 ){	
			setRespuesta(Constantes.DEFAULT_STRING);
//			muestraIdVenta();
			if(!producto.equals("")){
				display1 = display2 = display3 = display4 = display5 = display6 = display7 =
				display8 = display9 = display10 = display11 = display12 = display15 = "none";
				for (int i = 1; i<= mes; i++) {
				switch (i) {
						case 1: 
							display15 = display1 = "block"; 
							muestraProductoPorMes(Mes.ENERO, listaPrimasEne); 
							break;
						case 2: 
							display2 = "block"; 
							muestraProductoPorMes(Mes.FEBRERO, listaPrimasFeb); 
							break;
						case 3: 
							display3 = "block"; 
							muestraProductoPorMes(Mes.MARZO, listaPrimasMar); 
							break;
						case 4: 
							display4 = "block"; 
							muestraProductoPorMes(Mes.ABRIL, listaPrimasAbr); 
							break;
						case 5: 
							display5 = "block"; 
							muestraProductoPorMes(Mes.MAYO, listaPrimasMay); 
							break;
						case 6: 
							display6 = "block"; 
							muestraProductoPorMes(Mes.JUNIO, listaPrimasJun); 
							break;
						case 7: 
							display7 = "block"; 
							muestraProductoPorMes(Mes.JULIO, listaPrimasJul); 
							break;
						case 8: 
							display8 = "block"; 
							muestraProductoPorMes(Mes.AGOSTO, listaPrimasAgo);
							break;
						case 9: 
							display9 = "block"; 
							muestraProductoPorMes(Mes.SEPTIEMBRE, listaPrimasSep);
							break;
						case 10: 
							display10 = "block"; 
							muestraProductoPorMes(Mes.OCTUBRE, listaPrimasOct);
							break;
						case 11: 
							display11 = "block"; 
							muestraProductoPorMes(Mes.NOVIEMBRE, listaPrimasNov);
							break;
						case 12: 
							display12 = "block"; 
							muestraProductoPorMes(Mes.DICIEMBRE, listaPrimasDic);
							break;
					}
				}
			}else{
				setRespuesta("Debes seleccionar un mes para poder realizar la consulta");
			}
		} else{
			bandera2=0; 
		}
		return NavigationResults.SUCCESS;
	}
	
	public String muestraIdVenta(){
		
		if(inIdVenta != -1){				
			if(inIdVenta == 5){
				producto="PYME";
			}else if(inIdVenta == 6){						
				producto="LCI";
			}else if(inIdVenta == 7){
				producto="HPU";
			}else if(inIdVenta == 8 ){
				producto="LE";				
			}else if(inIdVenta == 0 ){
				producto ="0";
			}
		}else{
			setRespuesta("Debes seleccionar un Producto v�lido");
		}
		return producto;
	}

	public void muestraProductoPorMes(Mes mes, List<BeanCertificado> detalle){
		BeanCertificado primas;				
		List<Object[]> idVentaMes;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, mes.mesNum());
		String fefin = String.format("%s/%s/%s", calendar.getActualMaximum(Calendar.DAY_OF_MONTH), mes.mesStr(), anioActual); 
		String feinicio = String.format("01/%s/%s", mes.mesStr(), anioActual);
		detalle.clear();
		sumaMesDep = sumaMesRec = BigDecimal.ZERO;
		try{				
			idVentaMes = servicioPrimasDeposito.cargaIdVenta(feinicio,fefin,producto, ramo);	
			if(!idVentaMes.isEmpty()){
				idVentaMes = completaDetalleProducto(idVentaMes);
				for (Object[] cargar : idVentaMes) {					
					primas = new BeanCertificado();																						
					primas.setDesc1(cargar[1].toString());
					primas.setDesc2(cargar[2].toString());
					primas.setDesc3(cargar[3].toString());
					primas.setCoceCampon7BD(new BigDecimal(cargar[2].toString()));
					primas.setCoceCampon8BD(new BigDecimal(cargar[3].toString()));
					detalle.add(primas);
					llenaSumas(cargar);	
					listaGuardaFotos.add(primas);
					for (BeanCertificado cer : listaGlobal) {
						if (cer.getDesc1().equals(primas.getDesc1())) {
							cer.setCoceCampon7BD(cer.getCoceCampon7BD().add(new BigDecimal(cargar[2].toString())));
							cer.setCoceCampon8BD(cer.getCoceCampon8BD().add(new BigDecimal(cargar[3].toString())));
							break;
						}
					}					
				}
			}else{
				idVentaMes = completaDetalleProductoVacios(mes.mesStr());
				for (Object[] cargar : idVentaMes) {
					primas = new BeanCertificado();	
					primas.setDesc1(cargar[1].toString());												
					primas.setDesc2(cargar[2].toString());
					primas.setDesc3(cargar[3].toString());	
					primas.setCoceCampon7BD(new BigDecimal(cargar[2].toString()));
					primas.setCoceCampon8BD(new BigDecimal(cargar[3].toString()));
					detalle.add(primas);
					llenaSumas(cargar);	
					}
				}
			primas = new BeanCertificado();								
			primas.setDesc1("TOTAL");
			primas.setDesc2(sumaMesDep.toString());
			primas.setDesc3(sumaMesRec.toString());
			primas.setCoceCampon7BD(sumaMesDep);
			primas.setCoceCampon8BD(sumaMesRec);
			detalle.add(primas);						
		} catch (Exception e){
			e.printStackTrace();
			FacesUtils.addInfoMessage("Error al Consultar producto "  );
		}				
	}
	
	public void guardaFoto(){		
		Long consecutivo;
		int canal=1;
		Iterator<BeanCertificado> it;
		BeanCertificado certificado;
		String anioRe,mesRe,fechaCarga,remesa;
		anioRe = GestorFechas.formatDate(new Date(),"yyyy");
		mesRe = GestorFechas.formatDate(new Date(),"MM");
		fechaCarga = GestorFechas.formatDate(new Date(),"dd/MM/yyyy");		
		remesa = anioRe + mesRe;
		int contador=0,indicador=1,cont=0;		
		Format formatter;
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        List<Object[]> validaGuardar;
		boolean bGuardar = true;
		fechaIni="01/01/"+anioActual;
        try {			
			setRespuesta(Constantes.DEFAULT_STRING);
			if(mes != 0 && !"-1".equals(producto)){	
				fechaFin=sacaFechas(mes);
				validaGuardar= servicioPrimasDeposito.cargaGuardados(canal,ramo,fechaCarga);
				if(!validaGuardar.isEmpty()){									
					for (Object[] valida: validaGuardar){
						if(valida[0] != null){
							valida[0] = formatter.format(valida[0]);
							if(valida[0].equals(fechaCarga) && Integer.parseInt(valida[1].toString())==ramo && Integer.parseInt(valida[2].toString())== inIdVenta){
								cont++;
								log.info("no se puede cargar porque ya existe registro");
								bGuardar = false;
							}
						}	

					}
				} else{
					setRespuesta("Lista vacia");
				}
				
				if(bGuardar) {
					consecutivo = this.servicioPrimasDeposito.maxConsecutivo();					
					it = listaGlobal.iterator();
					while (it.hasNext()) {
						certificado = it.next();						
						consecutivo++; 
						sacaIdProd(certificado.getDesc1());
						this.servicioPrimasDeposito.guardaFoto(canal,ramo,inIdVenta,consecutivo,certificado.getCoceCampon7BD(),certificado.getCoceCampon8BD(),fechaCarga,remesa,indicador);
						contador++;							
					}							
				}
				setRespuesta("Registros guardados: "+contador);
				
			} else {
				setRespuesta(" mes y producto no valido");
			}	
		} catch (DataAccessException e) {		
			setRespuesta ( "Hubo un error al guardar Foto." );
			e.printStackTrace();									
		} catch (Exception e) { 
			e.printStackTrace();
			setRespuesta ( "Hubo un error al guardar Foto." );							
		}
	}
	
	
	private void sacaIdProd(String desc1) {
		if(desc1.equals("HPU")){
			inIdVenta = 7;
		}else if(desc1.equals("PYME")){
			inIdVenta = 5;
		}else if(desc1.equals("LCI")){
			inIdVenta = 6;
		}else if(desc1.equals("LE")){
			inIdVenta = 8;
		}					
	}
private String sacaFechas(Short mes) {
		if(mes==2){			
			fechaFin="28/02/"+anioActual;				
		}else if(mes==4|mes==6|mes==9|mes==11){
			fechaFin="30/0"+mes+"/"+anioActual;
		}else if(mes==1|mes==3|mes==5|mes==7|mes==8|mes==10|mes==12){ 
			fechaFin="31/0"+mes+"/"+anioActual;
		}
		return fechaFin;
	}

	public void llenaSumas( Object[] cargar){		
			sumaDepHpu = sumaDepHpu.add(new BigDecimal(cargar[2].toString()));
			sumaRecHpu = sumaRecHpu.add(new BigDecimal(cargar[3].toString()));
			sumaMesDep = sumaMesDep.add(new BigDecimal(cargar[2].toString()));
			sumaMesRec = sumaMesRec.add(new BigDecimal(cargar[3].toString()));
	}

	
	public void detallePrimas(String param){
		
		List<Object> resultado = null;
		String strNombre = null, strRutaTemp;
		Archivo objArchivo;
		String[] arrNomColumnas = null;
		String columnas;

		try {
			resultado=this.servicioPrimasDeposito.generaDetallePrimas(param,producto);
			columnas = "FechaCarga,Producto,NumCuenta,NumCredito,PDep,PRec";
			arrNomColumnas = columnas.split(",");
			
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			strNombre = "Detalle Resultados - " + new SimpleDateFormat("ddMMyyyy").format(new Date());
			
			objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
			objArchivo.copiarArchivo(arrNomColumnas, resultado, "|");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
			objArchivo.eliminarArchivo();
			
			FacesContext.getCurrentInstance().responseComplete();
		
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	public int sacaProd(){		
		int tamano=0;
		if(inIdVenta == 0){
			tamano = 4;						
		}else{
			tamano = 1;
		}
		return tamano;
	}
	public void regresaMesEne(){
		param="01";
		detallePrimas(param);		
	}
	public void regresaMesFeb(){
		param="02";
		detallePrimas(param);	
	}
	public void regresaMesMar(){
		param="03";
		detallePrimas(param);	
	}
	public void regresaMesAbr(){
		param="04";
		detallePrimas(param);	
	}
	public void regresaMesMay(){
		param="05";
		detallePrimas(param);	
	}
	public void regresaMesJun(){
		param="06";
		detallePrimas(param);	
	}
	public void regresaMesJul(){
		param="07";
		detallePrimas(param);	
	}
	public void regresaMesAgo(){
		param="08";
		detallePrimas(param);	
	}
	public void regresaMesSep(){
		param="09";
		detallePrimas(param);	
	}
	public void regresaMesOct(){
		param="10";
		detallePrimas(param);	
	}
	public void regresaMesNov(){
		param="11";
		detallePrimas(param);	
	}
	public void regresaMesDic(){
		param="12";
		detallePrimas(param);	
	}

	public void cargaFechas(){
		BeanCertificado foto;		
		this.listaTraeFotos.clear();
//		String anio;
		List<Object[]> fotos;
		int canal=1;
		Format formatter;
		String mesRe="",dia=""; 
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        mesRe = GestorFechas.formatDate(new Date(),"MM");
        dia = GestorFechas.formatDate(new Date(),"dd");
                		
		try {
			setRespuesta(Constantes.DEFAULT_STRING);
//			anio = GestorFechas.formatDate(new Date(),"yyyy");
			fotos = servicioPrimasDeposito.cargaFotosGuardadas(canal,ramo,mesRe,dia);
			for (Object[] cargar : fotos) {
				foto = new BeanCertificado();
				//cargar[0]=muestraIdVenta();
				cargar[1] = formatter.format(cargar[1]);
				foto.setDesc1(cargar[0].toString());				
				foto.setDesc2(cargar[1].toString());				
				listaTraeFotos.add(foto);
			}
			setRespuesta("Consulta realizada con exito: se encontraron " + fotos.size() + " registros.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String sacaProd0(int contador){
		String prod="";
		if(producto.equals("0") && contador==0){
			prod="HPU";
		}else if(producto.equals("0") && contador==1){
			prod="LCI";
		}else if(producto.equals("0") && contador==2){
			prod="LE";
		}else if(producto.equals("0") && contador==3){
			prod="PYME";
		}
		return prod;
	}
	public int sacaProdId(int contador){
		int id=0;
		if(producto.equals("0") && contador==0){
			id=7;
		}else if(producto.equals("0") && contador==1){
			id=6;
		}else if(producto.equals("0") && contador==2){
			id=8;
		}else if(producto.equals("0") && contador==3){
			id=5;
		}
		return id;					
	}
	public void limpia(){
		setRespuesta(Constantes.DEFAULT_STRING);
	}
	
	private List<Object[]> completaDetalleProducto(List<Object[]> ventasProd) {
		List<Object[]> prods = new ArrayList<Object[]>();
		Object[] venta = null;
		String mes = ventasProd != null ? String.valueOf(ventasProd.get(0)[0]) : "00";
		for (String product : productos) {
			venta = null;
			for(Object[] vp : ventasProd) {
				if (product.equals(vp[1])) {
					venta = vp;
					break;
				}
			}
			if (venta != null){
				prods.add(venta);
			} else {
				prods.add(new String[]{ mes, product , "0", "0"});
			}
		}
		return prods;
	}
	
	private List<Object[]> completaDetalleProductoVacios(String mes) {
		List<Object[]> prods = new ArrayList<Object[]>();
		for (String product : productos) {
				prods.add(new String[]{ mes, product , "0", "0"});
		}
		return prods;
	}
	
	private enum Mes {
		ENERO(0), FEBRERO(1), MARZO(2), ABRIL(3), MAYO(4), JUNIO(5), 
		JULIO(6), AGOSTO(7), SEPTIEMBRE(8), OCTUBRE(9), NOVIEMBRE(10), DICIEMBRE(11);
		
		private int mes;
		Mes(int mes) {
			this.mes = mes;
		}
		
		int mesNum() {
			return mes;
		}
		String mesStr() {
			return String.format("%02d", (mes + 1));
		}
	}
	
}
