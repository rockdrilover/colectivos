/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

/**
 * @author Sergio Plata
 *
 */
public class BeanNames {

	
	public static final String BEAN_APLLICACION = "beanAplicacion";
	public static final String BEAN_SESION = "beanSesion";
	public static final String BEAN_SERVICE_LOCATOR = "beanServiceLocator";
	public static final String BEAN_PRECARGA = "beanPreCarga";
	public static final String BEAN_LISTA_PRECARGA = "beanListaPreCarga";
	public static final String BEAN_CONSULTA_RENOVACION = "beanConsultaRenovacion";
	public static final String BEAN_LISTA_FACTURACION = "beanListaFacturacion";
	public static final String BEAN_LISTA_RAMO	= "beanListaRamo";
	public static final String BEAN_ESTATUS_LISTA = "beanEstatusLista";
	public static final String BEAN_CANCELACION_MASIVA = "beanCancelacionMasiva";
	public static final String BEAN_CANCELACION_INDIVIDUAL = "beanCancelacionIndividual";
	public static final String BEAN_ENDOSO_DATOS = "beanEndosoDatos";//mago manos magicas
	public static final String BEAN_LISTA_CLIENTE_CERTIF = "beanListaClienteCertif";
	public static final String BEAN_PRODUCTO = "beanProducto";
	public static final String BEAN_LISTA_PRODUCTO = "beanListaProducto";
	public static final String BEAN_PLAN = "beanPlan";
	public static final String BEAN_LISTA_CERTIFICADO = "beanListaCertificado";
	public static final String BEAN_IMPRESION = "beanImpresion";
	public static final String BEAN_ALTA = "beanAlta";
	public static final String BEAN_LISTA_RECIBO = "beanListaRecibo";
	public static final String BEAN_LISTA_PARAMETROS = "beanListaParametros";
	public static final String BEAN_PARAMETROS = "beanParametros";
	
	public static final String BEAN_LISTA_CARGA_CONCILIA = "beanListaCargaConcilia";
	public static final String BEAN_CREDITOSAUTOS = "beanCreditosAutos";
	public static final String BEAN_LISTA_TARIFAS = "beanListaTarifas";
	public static final String BEAN_CONSULTA_CONCILIACION = "beanConsultaConciliacion";
	public static final String BEAN_CONCILIAR = "beanConciliar";
	public static final String BEAN_REHABILITACION = "beanRehabilitacion";
	public static final String BEAN_ENDOSO = "beanEndoso";
	public static final String BEAN_LISTA_ENDOSO = "beanListaEndoso";
	public static final String BEAN_MARCACION = "beanMarcacion";
	public static final String BEAN_CHIMUELOS = "beanChimuelos";
	public static final String BEAN_EXTRA_PRIMA = "beanExtraPrima";
	public static final String BEAN_PREFACTURA = "beanPrefactura";
	public static final String BEAN_REPORTE_MENSUAL = "beanReporteMensual";
	public static final String BEAN_LISTA_TRASPASO_APTC = "beanListaTraspasoAptc";
	public static final String BEAN_REPORTE_STOCK = "beanReporteStock";
	public static final String BEAN_REPORTE_ERRORES = "beanReporteErrores";
	public static final String BEAN_COMPLEMENTARIOS = "beanComplementarios";
	public static final String BEAN_CARGA_MANUAL_HBS = "beanCargaManualHBS";
	public static final String BEAN_GUIAS_CONTABLES = "beanGuiasContables";
	public static final String BEAN_ALTA_CONTRATANTE = "beanAltaContratante";
	public static final String BEAN_LISTA_CONTRATANTE = "beanListaContratante";
	public static final String BEAN_LISTA_ESTADO = "beanListaEstado";
	public static final String BEAN_OBLIGADO = "beanObligado";
	public static final String BEAN_COLECTIVOS_COBERTURAS= "beanColectivosCoberturas";
	public static final String BEAN_CART_COBERTURAS= "beanCartCoberturas";
	public static final String BEAN_CATALOGO_COBERTURAS= "beanCatalogoCoberturas";
	public static final String BEAN_HOMOLOGACION = "beanHomologacion";
	
	public static final String BEAN_PARAMETRIZA_TARIFA 	  = "beanParametrizacion";
	public static final String BEAN_PARAMETRIZA_OPERACION = "beanParamOperacion";
	public static final String BEAN_CENTRO_COSTO = "beanCentroCosto";
	public static final String BEAN_SEGMENTOS = "beanSegmentos";
	public static final String BEAN_CONTRATANTE = "beanContratante";
	public static final String BEAN_COMPONENTES = "beanListaComponentes";
	public static final String BEAN_COBERTURAS = "beanCatalogoCoberturas";
	public static final String BEAN_CONSULTA_FACTURACION_I = "beanConsultaFacturacionI"; //jalm
	public static final String BEAN_REPORTES_CIERRE = "beanReportesCierre";
	public static final String BEAN_ALTA_CONTRATANTE_R57 = "beanAltaContratanteR57";
	public static final String BEAN_LISTA_TIMBRADOS = "beanListaTimbrados";
	public static final String BEAN_ASEGURADO = "beanAsegurado";
	public static final String BEAN_FECHA_CIERRE = "beanFechaCierre";
	public static final String BEAN_FECHAS_AJUSTE = "beanFechasAjuste";
	public static final String BEAN_MONITOREO = "beanMonitoreo";
	public static final String BEAN_FLUJODEEFECTIVO = "beanFlujoDeEfectivo";
	public static final String BEAN_HISTORIAL_CREDITO="beanHistorialCredito";
	public static final String BEAN_CONFIGURACION_IMPRESION = "beanConfiguracionImpresion";
	public static final String BEAN_REPORTEADOR="beanReporteador";
	public static final String BEAN_PRIMAS_EN_DEPOSITO = "beanPrimasEnDeposito";
	public static final String BEAN_CTA_CONCILIACION = "beanCtaConciliacion";
	public static final String BEAN_CONCILIACION = "beanConciliacion";
	public static final String BEAN_CATALOGO_ERRORES = "beanCatalogoErrores";
	public static final String BEAN_CONFIGURA_PARAMETROS = "beanConfiguraParametros";
	
	
	// Suscripcion
	
	public static final String BEAN_SUSCRIPCION = "beanSuscripcion";
	public static final String BEAN_SUSCRIPCION_REPORTEADOR = "beanSuscripcionReporteador";
	
	public static final String BEAN_RESERVAS = "beanReserva";
	public static final String BEAN_PARAMETRIZACION_COMISION = "beanParametrizacionComision";
	
	public static final String BEAN_COMISION_TECNICA = "beanComisionTecnica";
	
	public static final String BEAN_ENDOSOS_PENDIENTES = "beanEndososPendientes";
	//Emision Masiva
	public static final String BEAN_EMISION_MASIVA = "beanEmisionMasiva";

	public static final String BEAN_CONF_PROCESOS = "beanConfProcesos";
	public static final String BEAN_GRUPOS = "beanGrupos";
	public static final String BEAN_ERROR_EMISION = "beanErrorEmision";
	public static final String BEAN_CANCELA_ATM = "beanCancelaAtm";
	public static final String BEAN_PARAM_TIMBRADO = "beanParamTimbrado";
	public static final String BEAN_TIMBRADO = "beanTimbrado";
	public static final String BEAN_CEDULA = "beanCedula";
	public static final String BEAN_PARAM_COMISION = "beanParamComision";
	public static final String BEAN_REPORTES_TUIIO = "beanReportesTuiio";
	public static final String BEAN_CAMBIA_CREDITO = "beanCambiaCredito";
	
	public static final String BEAN_CONS_DETALLE_CRE_SIN = "beanConsDetalleCreSin";
	
	//Para nuevos cambios de Proyecto Cobranza
	public static final String BEAN_CARGA_COBRANZA = "beanCargaCobranza";
	public static final String BEAN_COBRANZA_REPORTES = "beanReporteCobranza";
	public static final String BEAN_PRODUCTO_COMISION = "beanNombreProducto";
	public static final String BEAN_REVERSO_COBRANZA = "beanReversoCobranza";
	
}
