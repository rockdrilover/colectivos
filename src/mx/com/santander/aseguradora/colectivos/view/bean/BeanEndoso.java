/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Endosos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndoso;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Constantes.OPC_FORMATO_REPORTE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


/**
 * @author Fernando Gomez
 *
 */
@Controller
@Scope("request")
public class BeanEndoso {
	
	Endosos endoso;
	Integer idVenta;
	
	private ServicioEndoso servicioEndoso;
	
	private Log log = LogFactory.getLog(this.getClass());
	
	public BeanEndoso() {
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("deprecation")
	public void imprimeEndoso(){
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;

		byte[] bytes;
		int read = 0;
		
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		try{
			
			StringBuffer nombreSalida = new StringBuffer();			
			
			String rutaImages = FacesUtils.getServletRequest().getRealPath("images") + File.separator;
			
			inParams.put("canal", this.endoso.getId().getCoedCasuCdSucursal());
			inParams.put("ramo",this.endoso.getId().getCoedCarpCdRamo() ) ;
			inParams.put("poliza", this.endoso.getId().getCoedCapoNuPoliza());
			inParams.put("idVenta", this.idVenta);
			inParams.put("endosoGral", this.endoso.getId().getCoedNuEndosoGral());
			inParams.put("endosoDetalle", this.endoso.getId().getCoedNuEndosoDetalle());
			inParams.put("operacion", this.endoso.getId().getCoedCdEndoso());
			inParams.put("rutaimg", rutaImages);
			
			
			nombreSalida.append("Endoso " + this.endoso.getId().getCoedCarpCdRamo()+ "_" + this.endoso.getId().getCoedCapoNuPoliza()
					  + "_" + this.endoso.getCoedCampov1() +".pdf");
			
			rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/" + nombreSalida);
			rutaReporte = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/documentoEndoso.jrxml");
			
			this.servicioEndoso.reporteEndosos(rutaTemporal, rutaReporte, inParams, OPC_FORMATO_REPORTE.PDF);
			
			File file = new File(rutaTemporal);
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			if(!context.getResponseComplete()){
			
				input = new FileInputStream(file);
				bytes = new byte[1000];
				
				
				String contentType = "application/pdf";

				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				
				response.setContentType(contentType);
				
				DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
		    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());

				ServletOutputStream out = response.getOutputStream();
				
				while((read = input.read(bytes))!= -1){
					
					out.write(bytes, 0 , read);
				}
				
				input.close();
				out.flush();
				out.close();
				file.delete();
				
				context.responseComplete();
				
			}
			
			
		}catch(Exception e){
			log.error("Error en el reporte de endoso",e);
		}
	}

	/**
	 * @return the endoso
	 */
	public Endosos getEndoso() {
		return endoso;
	}

	/**
	 * @param endoso the endoso to set
	 */
	public void setEndoso(Endosos endoso) {
		this.endoso = endoso;
	}

	/**
	 * @return the servicioEndoso
	 */
	public ServicioEndoso getServicioEndoso() {
		return servicioEndoso;
	}

	/**
	 * @param servicioEndoso the servicioEndoso to set
	 */
	public void setServicioEndoso(ServicioEndoso servicioEndoso) {
		this.servicioEndoso = servicioEndoso;
	}

	/**
	 * @return the idVenta
	 */
	public Integer getIdVenta() {
		return idVenta;
	}

	/**
	 * @param idVenta the idVenta to set
	 */
	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}
}
