/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioArchivo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioValidaEmision;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.Filtros;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.richfaces.component.html.HtmlProgressBar;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanListaPreCarga implements ServicioArchivo{
	
	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioProcesos servicioProcesos;
	@Resource
	private ServicioParametros servicioParametros;
	
	/**
	 * Servicio para validar emision
	 */
	@Resource
	private ServicioValidaEmision servicioValidaEmision;

	
	private Log log = LogFactory.getLog(this.getClass());
	private Short inCanal;
	private Short inRamo;
	private Long inPoliza;
	private Integer inIdVenta;
	private File archivo;
	private String nombreArchivo;
	
	private StringBuilder message;
	private String respuesta;
	private List<Object> resultado;
	private Long totalCarga;
	private List<Certificado> ListaprodPlan;
	
	private List<String> cifrasControl;
	
	private ProgressBar pb;
	private ExecutorService pool;
	


	/**
	 * Cadena donde se almacena el codigo de respuesta
	 */
	private Integer codigoResp;
	
	
	/**
	 * Cadena donde se almacena el codigo de respuesta de ejecucion de validacion
	 */
	private Integer respValidacion;
	
	
	
	/**
	 * @param servicioCarga the servicioCarga to set
	 */
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	/**
	 * @return the servicioCarga
	 */
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}

	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}

	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	
	public List<Certificado> getListaprodPlan() {
		return ListaprodPlan;
	}

	public void setListaprodPlan(List<Certificado> listaprodPlan) {
		ListaprodPlan = listaprodPlan;
	}
	
	/**
	 * @param inCanal the inCanal to set
	 */
	public void setInCanal(Short inCanal) {
		this.inCanal = inCanal;
	}

	/**
	 * @return the inCanal
	 */
	public Short getInCanal() {
		return inCanal;
	}

	/**
	 * @param inRamo the inRamo to set
	 */
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}

	/**
	 * @return the inRamo
	 */
	public Short getInRamo() {
		return inRamo;
	}

	/**
	 * @param inPoliza the inPoliza to set
	 */
	public void setInPoliza(Long inPoliza) {
		this.inPoliza = inPoliza;
	}

	/**
	 * @return the inPoliza
	 */
	public Long getInPoliza() {
		return inPoliza;
	}

	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}

	public Integer getInIdVenta() {
		return inIdVenta;
	}

	public Long getTotalCarga() {
		return totalCarga;
	}

	public void setCifrasControl(List<String> cifrasControl) {
		this.cifrasControl = cifrasControl;
	}

	public List<String> getCifrasControl() {
		return cifrasControl;
	}

	public void setTotalCarga(Long totalCarga) {
		this.totalCarga = totalCarga;
	}
	
	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}

	/**
	 * @return the archivo
	 */
	public File getArchivo() {
		return archivo;
	}
	
	
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @param servicioProcesos the servicioProcesos to set
	 */
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}

	/**
	 * @return the servicioProcesos
	 */
	public ServicioProcesos getServicioProcesos() {
		return servicioProcesos;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.respuesta = mensaje;
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return respuesta;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(List<Object> resultado) {
		this.resultado = resultado;
	}

	/**
	 * @return the resultado
	 */
	public List<Object> getResultado() {
		return resultado;
	}

	/**
	 * @return the intBarra
	 */
	public Integer getProgressValue() {
		return pb.getProgressValue();
	}

	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressValue(Integer progressValue) {
		this.pb.setProgressValue(progressValue);
	}
	
	/**
	 * @return the intBarra
	 */
	public Boolean getProgressHabilitar() {
		return pb.isProgressHabilitar();
	}

	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressHabilitar(Boolean progressHabilitar) {
		this.pb.setProgressHabilitar(progressHabilitar);
	}
	
	/**
	 * @return the progressBar
	 */
	public HtmlProgressBar getProgressBar() {
		return pb.getProgressBar();
	}

	/**
	 * @param progressBar the progressBar to set
	 */
	public void setProgressBar(HtmlProgressBar progressBar) {
		this.pb.setProgressBar(progressBar);
	}

	
	/**
	 * @return the codigoResp
	 */
	public Integer getCodigoResp() {
		return codigoResp;
	}

	/**
	 * @param codigoResp the codigoResp to set
	 */
	public void setCodigoResp(Integer codigoResp) {
		this.codigoResp = codigoResp;
	}

	/**
	 * @return the respValidacion
	 */
	public Integer getRespValidacion() {
		return respValidacion;
	}

	/**
	 * @param respValidacion the respValidacion to set
	 */
	public void setRespValidacion(Integer respValidacion) {
		this.respValidacion = respValidacion;
	}

	
	
	

	/**
	 * @return the servicioValidaEmision
	 */
	public ServicioValidaEmision getServicioValidaEmision() {
		return servicioValidaEmision;
	}

	/**
	 * @param servicioValidaEmision the servicioValidaEmision to set
	 */
	public void setServicioValidaEmision(ServicioValidaEmision servicioValidaEmision) {
		this.servicioValidaEmision = servicioValidaEmision;
	}

	public BeanListaPreCarga() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.resultado = new ArrayList<Object>();
		this.cifrasControl = new ArrayList<String>();
		this.ListaprodPlan = new  ArrayList<Certificado>();
		this.inIdVenta = 0;
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		this.pool = Executors.newFixedThreadPool(1);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_PRECARGA);
	}
	
	public void emitirh() {
		
		this.log.debug("Iniciando proceso de carga (Hilo)");
		
		this.pb.setProgressHabilitar(true);
		this.setProgressValue(100);
		
		Runnable hilo = new HiloProcesos(this,BeanNames.BEAN_LISTA_PRECARGA);
		pool.execute(hilo);
	}

	public String  emitir(){
		this.log.debug("Iniciando proceso de carga");
		List<Object> lista = new ArrayList<Object>();
		boolean flgGuardaDatosArchivo = false;
		boolean flgEmitir = false;
		String strForward = null;
		if (this.archivo == null){
			this.message.append("Debe seleccionar un archivo a cargar.");
			this.log.debug(message.toString());
			this.respuesta = this.message.toString();
			Utilerias.resetMessage(message);
			strForward =  NavigationResults.FAILURE;
			return strForward;
		}
		
		
		if(this.inRamo != null && this.inPoliza != null && ((this.archivo == null && this.inPoliza == -1) || (this.archivo != null && this.inPoliza != -1))|| (archivo == null && inPoliza > 0))	{
			try {
				this.inCanal = 1;
				this.totalCarga = 0L;
				if (inIdVenta == null) inIdVenta = 0;
					
				try {
					switch(this.inRamo){
						case 5: // ACCIDENTES
							lista = this.leerArchivoAccidente(this.archivo, ",");
							flgGuardaDatosArchivo = true;
							break;
							
						case 9: //DESEMPLEO
							if(this.inPoliza == -1 && (this.inIdVenta == 9 || this.inIdVenta == 8 || this.inIdVenta == 6)){	
								this.inPoliza = 0L;
								this.nombreArchivo = Constantes.CONCILIACION_NOMBRE_ARCHIVO + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
								flgEmitir = true;
							} else if(archivo == null && inPoliza > 0){
								//consultar validar poliza
								StringBuilder query = new StringBuilder();
								query.append(" and P.copaDesParametro = 'POLIZA' \n");
								query.append(" and P.copaIdParametro = 'GEP' \n");
								query.append(" and P.copaNvalor1 = 1 \n");
								query.append(" and P.copaNvalor2 = ").append(inRamo).append("\n");
								query.append(" and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(this.inPoliza).append(", 1, substr(").append(this.inPoliza).append(",0,3), 2,substr(").append(this.inPoliza).append(",0,2)\n");
									
								if(servicioParametros.obtenerObjetos(query.toString()) != null){
									flgEmitir = true;
									this.nombreArchivo = Constantes.CONCILIACION_NOMBRE_ARCHIVO_MANUAL + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
								}
							} else if(this.archivo != null) {
								lista = this.leerArchivoDesempleo(this.archivo, ",");
								flgGuardaDatosArchivo = true;
							}
							break;
							
						case 25://DANOS
							if(archivo == null && inPoliza > 0){
								flgEmitir = true;
								this.nombreArchivo = Constantes.CONCILIACION_NOMBRE_ARCHIVO_MANUAL + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
							} else if(this.archivo != null) {
								lista = this.leerArchivoDanos(this.archivo, ",");
								flgGuardaDatosArchivo = true;
							}
							break;
							
						case 57:// VIDA PYME  MGE
							if(this.archivo != null  &&  inPoliza > 0) {
								lista = this.leerArchivoVidaPyme(this.archivo, ",");
								flgGuardaDatosArchivo = true;
							}
							break;
							
						case 58:// VIDA PYME ramo 58 MGE
							if(this.archivo != null) {
								lista = this.leerArchivoVidaPyme(this.archivo, ",");
								flgGuardaDatosArchivo = true;
							}
							break;
						
						case 61:// VIDA HIPOTECARIO
							if(this.inPoliza == -1 && (this.inIdVenta == 9 || this.inIdVenta == 6 || this.inIdVenta == 8 || this.inIdVenta == 7 || this.inIdVenta == 10)) {
								this.inPoliza = 0L;
								this.nombreArchivo = Constantes.CONCILIACION_NOMBRE_ARCHIVO;
								flgEmitir = true;
							} else if(archivo == null && inPoliza > 0){
								flgEmitir = true;
								this.nombreArchivo = Constantes.CONCILIACION_NOMBRE_ARCHIVO_MANUAL + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");								
							} else if(this.archivo != null) {
								lista = this.leerArchivoVida(this.archivo, ",");
								flgGuardaDatosArchivo = true;
							}
							break;
						
						case 65:// VIDA NOMINA
							lista = this.leerArchivoVidaNomina(this.archivo, ",");
							flgGuardaDatosArchivo = true;
							break;
						
						case 82:// SAFE
							lista = this.leerArchivoSAFE(this.archivo, ",");
							flgGuardaDatosArchivo = true;
							break;
							
						case 89:// GAP
							if(this.inPoliza == -1 && (this.inIdVenta == 10)) {
								this.inPoliza = 0L;
								this.nombreArchivo = Constantes.CONCILIACION_NOMBRE_ARCHIVO;
								flgEmitir = true;
							} else if(this.archivo != null) {
								lista = this.leerArchivoGAP(this.archivo, ",");
								flgGuardaDatosArchivo = true;
							}
							break;
					}
						
					if(flgGuardaDatosArchivo) {
						message.append("Iniciando proceso de carga....");
						this.log.debug(message.toString());
						Utilerias.resetMessage(message);
						
						//this.servicioCarga.guardarObjetos(lista);
						
						this.nombreArchivo = this.archivo.getName().substring(this.archivo.getName().lastIndexOf("\\") + 1);
						this.message.append("Comenzando la emisi�n del ramo:").append(this.inRamo.toString()).append(" poliza:" ).append(this.inPoliza.toString()).append(".");
						this.log.debug(message.toString());
						Utilerias.resetMessage(message);
						flgEmitir = true;
					} else if(flgEmitir == false) {
						this.message.append("El archivo a cargar no es correcto.");
						this.log.debug(message.toString());
						this.respuesta = this.message.toString();
						Utilerias.resetMessage(message);
						strForward =  NavigationResults.FAILURE;
					}
				} catch (DataAccessException e) {
					this.message.append("Error al leer los datos del archivo.");
					this.log.error(message.toString(), e);
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
					// Borramos archivo temporal
					this.archivo.delete();
					flgEmitir = false;
					strForward =  NavigationResults.FAILURE;
				} catch (Exception e){
					this.message.append("El proceso de carga no concluy� correctamete. Revise el archivo");					
					this.log.error(message.toString() + " Error interno.", e);
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
					// Borramos archivo temporal
					this.archivo.delete();
					
					flgEmitir = false;
					strForward =  NavigationResults.FAILURE;
				}
					
				if(flgEmitir){
					if (this.inIdVenta == 7 ) this.servicioProcesos.validarObligados();
					if (this.inIdVenta == 8 && (this.inRamo == 9 || this.inRamo == 61)) {
						this.servicioProcesos.validarMultidisposicion(this.inCanal, this.inRamo, this.inPoliza, this.inIdVenta);
					}
					this.servicioProcesos.emisionMasiva(this.inCanal, this.inRamo, this.inPoliza, this.inIdVenta);
											
					this.resultado.clear();
					this.resultado = this.servicioCarga.estatusEmision(this.inCanal, this.inRamo, this.inPoliza,this.inIdVenta, this.nombreArchivo);
					
					for (int i=0; i<this.resultado.size(); i++)
						this.totalCarga = this.totalCarga + ((BigDecimal) ((Object[]) this.resultado.get(i))[3]).longValue();
					
					this.servicioCarga.respaldarErrores(this.inCanal, this.inRamo, this.inPoliza, this.inIdVenta, this.nombreArchivo);
				
					if (this.inIdVenta == 8 ) { //SI ES EMISION DE LE
						StringBuilder sbMsgCorreo = new StringBuilder();
						sbMsgCorreo.append("Buen Dia. \n");
						sbMsgCorreo.append("Se envia errores de Emision de LE. \n\n\n");
						this.servicioCarga.enviarErroresLE(Constantes.CODIGO_ERROR_EMISION, sbMsgCorreo, "Errores Emision LE");
					}
					                           
					
					this.message.append("El proceso de emisi�n finaliz�, descargue el archivo de errores para verificar las inconsistencias.");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
					
					lista.clear();
					strForward = NavigationResults.SUCCESS;
				}
			} catch (Exception e) {
					this.message.append("El proceso de emisi�n no concluy� correctamete.");					
					this.log.error(message.toString() + " Error interno.", e);
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
					// Borramos archivo temporal
					this.archivo.delete();
					
					strForward =  NavigationResults.FAILURE;
			}
		} else {
			this.message.append("El ramo, poliza y/o  el el archivo a cargar no son correctos.");
			this.log.debug(message.toString());
			this.respuesta = this.message.toString();
			Utilerias.resetMessage(message);
			strForward =  NavigationResults.FAILURE;
		} // end if ramo, poliza, archivo not null

		return strForward;
	}
	
	public String cargar(){
		
		this.log.debug("Iniciando proceso de carga");
		List<Object> lista = null;
		
		if(this.inRamo != null &&
		   this.inPoliza != null &&
		   this.archivo != null)
		{
		
			try {
				
					this.inCanal = 1;
					
					switch(this.inRamo){
					
						case 5: // ACCIDENTES
							lista = this.leerArchivoAccidente(this.archivo, ",");
						break;
						case 9: //DESEMPLEO
							lista = this.leerArchivoDesempleo(this.archivo, ",");
						break;
						case 25://DANOS
							lista = this.leerArchivoDanos(this.archivo, ",");
						break;
						case 61:// VIDA HIPOTECARIO
							lista = this.leerArchivoVida(this.archivo, ",");
						break;
						case 65:// VIDA NOMINA
							lista = this.leerArchivoVidaNomina(this.archivo, ",");
						break;
						case 82:// SAFE
							lista = this.leerArchivoSAFE(this.archivo, ",");
						break;
						case 89:// SAFE
							lista = this.leerArchivoGAP(this.archivo, ",");
						break;
					}
					
					message.append("Iniciando proceso de carga....");
					this.log.debug(message.toString());
					
					this.servicioCarga.guardarObjetos(lista);
					
					this.nombreArchivo = this.archivo.getName().substring(this.archivo.getName().lastIndexOf("\\") + 1);
					
					this.resultado.clear();
					
					this.resultado = this.servicioCarga.estatusEmision(this.inCanal, this.inRamo, this.inPoliza, this.inIdVenta, this.nombreArchivo);
					
					this.message.append("El proceso de carga finaliz�, informe a AO para continuar con el proceso de emisi�n.");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
					
					lista.clear();
					
					return NavigationResults.SUCCESS;
					
				}
				catch (DataAccessException e) {
					// TODO: handle exception
					this.message.append("Hubo un error al guardar le precarga de datos.");
					this.log.error(message.toString(), e);
					this.respuesta = this.message.toString();
					// Borramos archivo temporal
					this.archivo.delete();
					return NavigationResults.FAILURE;
				}
				catch (Exception e) {
					// TODO: handle exception
					this.message.append("No se puede realizar el proceso de carga.").append(e.getLocalizedMessage());
					this.respuesta = this.message.toString();
					this.log.error(message.toString(), e);
					Utilerias.resetMessage(message);
					// Borramos archivo temporal
					this.archivo.delete();
					
					return NavigationResults.FAILURE;
				}
			
		}
		else
		{
			this.message.append("El ramo, poliza y/o  el el archivo a cargar no son correctos.");
			this.log.debug(message.toString());
			this.respuesta = this.message.toString();
			Utilerias.resetMessage(message);
			return NavigationResults.FAILURE;
			
		} // end if ramo, poliza, archivo not null
		
		
	}

	public void listener(UploadEvent event) throws Exception{
		
		UploadItem item = event.getUploadItem();
		File archivoTemporal;
		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		String nombreArchivo = null;
		String nombreRuta;
		
		
		
		if(item.isTempFile()){
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			
		}
		else{
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			
			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			/**
			 * Guardar el archivo en  carpeta del proyecto
			 */
			nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
			
			archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
			FileUtil.write(archivoTemporal, item.getData());
			
			
			
			this.archivo = archivoTemporal;
		}
		
		this.message.append("Archivo cargado correctamente, puede continuar con el proceso de emisi�n.");
		this.log.debug(this.message.toString());
		this.respuesta = this.message.toString();
		Utilerias.resetMessage(message);
	}

	public List<Object> leerArchivoAccidente(File file, String delimitador) {
		// TODO Auto-generated method stub
		
		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCargaId id;
		PreCarga preCarga = null;
		List<Object> lista = null;
		
		String[] headers = null;
		Map<String, String> mh;
		
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

						
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			
			if(csv.readHeaders())
			{
				
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				bean = new BeanPreCarga();
				
				
				lista = new ArrayList<Object>();	
				this.message.append("Validando los tipos de datos del archivo");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				while(csv.readRecord())
				{
					
					if (!csv.get(0).trim().equals("")){
						id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "ACC", csv.get(mh.get("sumaasegurada")));
						
						bean.setId(id);
						
						bean.setCopcCuenta(csv.get(mh.get("cuenta")));
						bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")));
						bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")));
						bean.setCopcNombre(csv.get(mh.get("nombre")));
						bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")));
						bean.setCopcSexo(csv.get(mh.get("sexo")));
						bean.setCopcNumCliente(csv.get(mh.get("numerocliente")));
						bean.setCopcRfc(csv.get(mh.get("rfc")));
						bean.setCopcCalle(csv.get(mh.get("calle")));
						bean.setCopcColonia(csv.get(mh.get("colonia")).length() > 55 ? csv.get(mh.get("colonia")).substring(0, 54): csv.get(mh.get("colonia")));
						bean.setCopcDelmunic(csv.get(mh.get("delg/mun")).length() > 50 ? csv.get(mh.get("delg/mun")).substring(0, 49): csv.get(mh.get("delg/mun")));
						bean.setCopcCdEstado(csv.get(mh.get("estado")));
						bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
						bean.setCopcDato27(csv.get(mh.get("lada")));
						bean.setCopcTelefono(csv.get(mh.get("telefono")));
						bean.setCopcTipoCliente(csv.get(mh.get("tipocliente")));
						bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
						bean.setCopcPrima(csv.get(mh.get("prima")));
						
						bean.setCopcBcoProducto(csv.get(mh.get("producto")));
						bean.setCopcSubProducto(csv.get(mh.get("subproducto")));
						
						bean.setCopcDato7(csv.get(mh.get("tarifa")));
						bean.setCopcDato1(csv.get(mh.get("numerobeneficiarios")));
						bean.setCopcDato2(csv.get(mh.get("nombrebeneficiarios")));
						bean.setCopcDato3(csv.get(mh.get("relacionasegurado")));
						bean.setCopcDato4(csv.get(mh.get("porcentajeparticipacion")));
						
						bean.setCopcDato22(csv.get(mh.get("contratoenlace")));
						bean.setCopcDato25(csv.get(mh.get("bucempresa")));
						bean.setCopcDato23(csv.get(mh.get("nombreempresa")));
						
						bean.setCopcDato16(csv.get(mh.get("tiposeguro")));
						
						
						bean.setCopcDato11(nombreArchivo);
					
						preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
						
						//lista.add(preCarga);
						this.servicioCarga.guardarObjeto(preCarga);
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
				
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pud� cargar la informacion.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		
		return lista;
		
	}

	public List<Object> leerArchivoDanos(File file, String delimitador) {
		// TODO Auto-generated method stub
		CsvReader csv = null;
		FileReader fr;
		PreCarga preCarga;
		BeanPreCarga bean;
		PreCargaId id;
		List<Object> lista = null;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

		
						
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			String[] headers = null;
			Map<String, String> mh;
			
			if(csv.readHeaders())
			{
				
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
				bean = new BeanPreCarga();			
				
				this.message.append("Leyendo datos  pre carga DANOS.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				while(csv.readRecord())
				{
					if (!csv.get(0).trim().equals("")){
						id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "DAN", csv.get(mh.get("sumaasegurada")));
						
						bean.setId(id);
						
						bean.setCopcCuenta(csv.get(mh.get("cuenta")));
						bean.setCopcNuCredito(csv.get(mh.get("prestamo")));
						bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")));
						bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")));
						bean.setCopcNombre(csv.get(mh.get("nombre")));
						bean.setCopcNumCliente(csv.get(mh.get("numerocliente")));
						bean.setCopcRfc(csv.get(mh.get("rfc")));
						bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")));
						bean.setCopcCalle(csv.get(mh.get("calle")).length() > 150 ? csv.get(mh.get("calle")).substring(0, 149) : csv.get(mh.get("calle")));
						bean.setCopcColonia(csv.get(mh.get("colonia")).length() > 55 ? csv.get(mh.get("colonia")).substring(0, 54) : csv.get(mh.get("colonia")));
						bean.setCopcDelmunic(csv.get(mh.get("delg/mun")).length() > 50 ? csv.get(mh.get("delg/mun")).substring(0, 49) : csv.get(mh.get("delg/mun")));
						bean.setCopcCdEstado(csv.get(mh.get("estado")));
						bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
						bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
						bean.setCopcPrima(csv.get(mh.get("prima")));
						bean.setCopcBcoProducto(csv.get(mh.get("producto")));
						bean.setCopcSubProducto(csv.get(mh.get("subproducto")));
						bean.setCopcRemesa(csv.get(mh.get("remesa")));
						bean.setCopcDato33(csv.get(mh.get("fechainiciocredito")));
						bean.setCopcDato34(csv.get(mh.get("fechafincredito")));
						bean.setCopcNuPlazo(csv.get(mh.get("plazo")));
						bean.setCopcDato1(csv.get(mh.get("metrosdelmar")));
						bean.setCopcDato2(csv.get(mh.get("piso")));
						bean.setCopcDato3(csv.get(mh.get("numeropisos")));
						bean.setCopcDato7(csv.get(mh.get("tarifa")));
						
						bean.setCopcDato11(nombreArchivo);
						
						
						preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
						
						this.servicioCarga.guardarObjeto(preCarga);
						//lista.add(preCarga);
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
				
				
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
				FacesUtils.addInfoMessage(message.toString());
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pudo cargar la informacion del archivo.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		
		return lista;
		
	}

	public List<Object> leerArchivoDesempleo(File file, String delimitador) {
		
		// TODO Auto-generated method stub
		CsvReader csv = null;
		FileReader fr;
		PreCarga preCarga;
		PreCargaId id;
		BeanPreCarga bean;
		List<Object> lista = null;
		boolean hayCifrasContro = false;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

		String[] headers = null;
		Map<String, String> mh;
		
		try {
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			
			if(csv.readHeaders()) {
				
			    lista = new ArrayList<Object>();
				bean = new BeanPreCarga();	
				
				this.message.append("Validando los tipos de datos del archivo");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				cifrasControl.clear();
				
				while(csv.readRecord()) {
					if (csv.get(0).equals("*")) hayCifrasContro = true;

					if (!hayCifrasContro){
						if (!csv.get(0).trim().equals("")){
							id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "DES", csv.get(mh.get("sumaasegurada")));
									
							bean.setId(id);					
							
							bean.setCopcCuenta(csv.get(mh.get("cuenta")));
							bean.setCopcNuCredito(csv.get(mh.get("prestamo")));
							
							if (this.inIdVenta == Constantes.TIPO_VENTA_BT) bean.setCopcNuCredito(csv.get(mh.get("plastico")));
							
							bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")));
							bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")));
							bean.setCopcNombre(csv.get(mh.get("nombre")));
							bean.setCopcSexo(csv.get(mh.get("sexo")));
							bean.setCopcNumCliente(csv.get(mh.get("numerocliente")));
							bean.setCopcRfc(csv.get(mh.get("rfc")));
							bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")));
							bean.setCopcCalle(csv.get(mh.get("calle")).length() > 150 ? csv.get(mh.get("calle")).substring(0, 150) : csv.get(mh.get("calle")));
							bean.setCopcColonia(csv.get(mh.get("colonia")).length() > 55 ? csv.get(mh.get("colonia")).substring(0, 55) : csv.get(mh.get("colonia"))); 
							bean.setCopcDelmunic(csv.get(mh.get("delg/mun")).length() > 50 ? csv.get(mh.get("delg/mun")).substring(0, 50) : csv.get(mh.get("delg/mun")));
							bean.setCopcCdEstado(csv.get(mh.get("estado")));
							bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
							bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
							bean.setCopcDato6(csv.get(mh.get("saldoinsoluto")));
							bean.setCopcDato8(csv.get(mh.get("sumaasegurada")));
							if(bean.getCopcDato6() != null && (bean.getCopcDato6().equals("") || bean.getCopcDato6().length() == 0)){
								bean.setCopcDato6(bean.getCopcDato8());
							}
							
							if (this.inIdVenta == Constantes.TIPO_VENTA_BT || this.inIdVenta == Constantes.TIPO_VENTA_LEX)
								bean.setCopcDato6(csv.get(mh.get("basecalculo")));
							
							bean.setCopcPrima(csv.get(mh.get("prima")));
							bean.setCopcDato2(this.inPoliza.intValue() == 2? csv.get(mh.get("plazo")): csv.get(mh.get("tarifa"))); // tomar plazo como identificador para el caso de la poliza 2 de nomina
							
							bean.setCopcDato33(csv.get(mh.get("fechainiciocredito")));
							bean.setCopcDato34(csv.get(mh.get("fechafincredito")));
							bean.setCopcNuPlazo(csv.get(mh.get("plazo")));
							bean.setCopcBcoProducto(csv.get(mh.get("producto")));
							bean.setCopcSubProducto(csv.get(mh.get("subproducto")));
							bean.setCopcCazbCdSucursal(csv.get(mh.get("sucursal")));
							
							bean.setCopcDato4(this.inPoliza.intValue() == 2? csv.get(mh.get("periodo"))+","+csv.get(mh.get("tarifa")): csv.get(mh.get("periodo"))); // concatenamos la cuota
							
							if (this.inIdVenta != null && this.inIdVenta > 0) {
								bean.setCopcDato9(csv.get(mh.get("idventa")));
								bean.setCopcDato16(this.inIdVenta.toString());
							}
							
							//Mapeo para TDC Desempleo
							bean.setCopcDato27(csv.get(mh.get("lada")));
							bean.setCopcTelefono(csv.get(mh.get("telefono")));
							bean.setCopcTipoCliente(csv.get(mh.get("tipocliente")));
							bean.setCopcDato40(csv.get(mh.get("remesa")));
							bean.setCopcDato2(csv.get(mh.get("tarifa")));
							bean.setCopcDato45(csv.get(mh.get("correoelectronico")));
							bean.setCopcDato43(csv.get(mh.get("canalventa")));
							bean.setCopcDato44(csv.get(mh.get("cuentacheques")));
							bean.setCopcDato21(csv.get(mh.get("curp")));
							bean.setCopcDato49( (csv.get(mh.get("ejecutivo")))+"-"+(csv.get(mh.get("cod_puesto"))) );
							
							//bean.setCopcDato1(csv.get(mh.get("indicador")));
							
							bean.setCopcDato11(nombreArchivo);
							
							
							preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
													
							try {
								this.servicioCarga.guardarObjeto(preCarga);
							} catch(Exception e) {
								this.log.info("Duplicado " + e.getMessage());
							}
						}
					} else {
						if (!csv.get(0).equals("*"))
							cifrasControl.add(csv.get(0)+": "+Double.parseDouble(csv.get(1)));
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
			} else {
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
			
		} catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error interno.");
			this.log.error(message.toString(), e);
		}
		
		return lista;
	}
	
	public List<Object> leerArchivoSAFE(File file, String delimitador) {
		// TODO Auto-generated method stub

		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCarga preCarga;
		PreCargaId id;
		List<Object> lista = null;
		boolean hayCifrasContro = false;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

		String[] headers = null;
		Map<String, String> mh;
						
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			if(csv.readHeaders())
			{
				
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
							
				this.message.append("Validando los tipos de datos del archivo");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				cifrasControl.clear();
				
				while(csv.readRecord())
				{
					if (csv.get(0).equals("*")) hayCifrasContro = true;

					if (!hayCifrasContro){
						
					
						if (!csv.get(0).trim().equals("")){					
							bean = new BeanPreCarga();
							id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "SAF", "0");
							
							bean.setId(id);		
							
							if (bean.getId().getCopcIdCertificado().trim().length() == 0) 
								bean.getId().setCopcIdCertificado(csv.get(mh.get("cuenta")));
							
							bean.setCopcCuenta(csv.get(mh.get("cuenta")));
							
							bean.getId().setCopcIdCertificado(new Long(bean.getId().getCopcIdCertificado()).toString());
							
							bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")));
							bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")));
							bean.setCopcNombre(csv.get(mh.get("nombre")));
							bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")));
							bean.setCopcTipoCliente(csv.get(mh.get("tipocliente")));
							if (bean.getCopcTipoCliente() == null || bean.getCopcTipoCliente().isEmpty())
								bean.setCopcTipoCliente("1");
							bean.setCopcSexo(csv.get(mh.get("sexo")));
							//bean.setCopcNumCliente(csv.get(mh.get("numerocliente")));
							bean.setCopcNumCliente(csv.get(mh.get("numerocliente")).trim().length() == 0 ? csv.get(mh.get("numercliente")).trim() : csv.get(mh.get("numerocliente")).trim());
							bean.setCopcRfc(csv.get(mh.get("rfc")));
							bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")));
							bean.setCopcCalle(csv.get(mh.get("calle")));
							bean.setCopcColonia(csv.get(mh.get("colonia")));
							//bean.setCopcDelmunic(csv.get(mh.get("delg/mun")));
							bean.setCopcDelmunic(csv.get(mh.get("delg/mun")).trim().length() == 0 ? csv.get(mh.get("del/mun")).trim() : csv.get(mh.get("delg/mun")).trim());
							bean.setCopcCdEstado(csv.get(mh.get("estado")));
							bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
							bean.setCopcTelefono(csv.get(mh.get("telefono")));
							bean.setCopcCazbCdSucursal(csv.get(mh.get("sucursalbanco")));
							bean.setCopcAltaBaja(csv.get(mh.get("tipooperacion")));
							bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
							bean.setCopcBcoProducto(csv.get(mh.get("producto")));
							bean.setCopcSubProducto(csv.get(mh.get("subproducto")));
							bean.setCopcDato23(csv.get(mh.get("nombreempresa")));
							bean.setCopcDato25(csv.get(mh.get("bucempresa")));
							bean.setCopcDato22(csv.get(mh.get("contratoenlace")));
							bean.setCopcDato16(csv.get(mh.get("tiposeguro")));
							bean.setCopcDato11(nombreArchivo);
							
							preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
							
							//lista.add(preCarga);
							this.servicioCarga.guardarObjeto(preCarga);
						}
						
					}else{
						if (!csv.get(0).equals("*"))
							cifrasControl.add(csv.get(0)+": "+Double.parseDouble(csv.get(1)));
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
				
				
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
				
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede cargar la informacion del archivo.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		
		return lista;
		
	}

	public List<Object> leerArchivoVida(File file, String delimitador) {
		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCargaId id;
		PreCarga preCarga = null;
		List<Object> lista = null;	
		boolean hayCifrasContro = false;
		String[] headers = null;
		Map<String, String> mh;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);
						
		try {
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			if(csv.readHeaders()) {
				headers = csv.getHeaders();
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
				bean = new BeanPreCarga();
				
				
				log.debug("Leyendo datos  pre carga VIDA.");
				cifrasControl.clear();
				while(csv.readRecord()) {
					if (csv.get(0).equals("*")) hayCifrasContro = true;
					if (!hayCifrasContro) {
						if (!csv.get(0).trim().equals("")) {
							bean.setCopcCargada("0");
							bean.setCopcRegistro("");
							
							id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "VID", csv.get(mh.get("sumaasegurada")).length() > 0 ? csv.get(mh.get("sumaasegurada")) : csv.get(mh.get("saldoinsoluto")));
							bean.setId(id);
													
							bean.setCopcCuenta(csv.get(mh.get("cuenta")).trim());
							bean.setCopcNuCredito(csv.get(mh.get("prestamo")).trim());
							bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")).trim());
							bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")).trim().length() == 0 ? "." : csv.get(mh.get("apellidomaterno")).trim());
							bean.setCopcNombre(csv.get(mh.get("nombre")).trim());
							bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")).trim());
							bean.setCopcSexo(csv.get(mh.get("sexo")).trim());
							bean.setCopcNumCliente(csv.get(mh.get("numerocliente")).trim());
							bean.setCopcRfc(csv.get(mh.get("rfc")).trim());
							bean.setCopcCalle(csv.get(mh.get("calle")).length() > 150 ? csv.get(mh.get("calle")).substring(0, 149) : csv.get(mh.get("calle")).trim());
							bean.setCopcColonia(csv.get(mh.get("colonia")).length() > 55? csv.get(mh.get("colonia")).substring(0, 44) : csv.get(mh.get("colonia")).trim());
							bean.setCopcDelmunic(csv.get(mh.get("delg/mun")).length() > 50 ? csv.get(mh.get("delg/mun")).substring(0, 49) : csv.get(mh.get("delg/mun")).trim());
							bean.setCopcCdEstado(csv.get(mh.get("estado")));
							bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
							bean.setCopcDato27(csv.get(mh.get("lada")));
							bean.setCopcTelefono(csv.get(mh.get("telefono")));
							bean.setCopcTipoCliente(csv.get(mh.get("tipocliente")));
							bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
							bean.setCopcPrima(csv.get(mh.get("prima")));
							bean.setCopcDato6(csv.get(mh.get("saldoinsoluto")));
							bean.setCopcDato8(csv.get(mh.get("sumaasegurada"))); // respaldo de la suma asegurada.	
							bean.setCopcDato7(csv.get(mh.get("tarifa")).trim());							
							bean.setCopcBcoProducto(csv.get(mh.get("producto")).trim());
							bean.setCopcSubProducto(csv.get(mh.get("subproducto")).trim());
							bean.setCopcDato33(csv.get(mh.get("fechainiciocredito")));							
							bean.setCopcNuPlazo(csv.get(mh.get("plazo")));
							
							bean.setCopcDato2(csv.get(mh.get("nombreasegurados")));
							bean.setCopcDato3(csv.get(mh.get("rfcasegurados")));
							bean.setCopcDato4(csv.get(mh.get("fechanacasegurados")));
							bean.setCopcDato5(csv.get(mh.get("sexoasegurados")));
							bean.setCopcDato19(csv.get(mh.get("tipoasegurados")));
							bean.setCopcDato17(csv.get(mh.get("noasegurados")));
							bean.setCopcDato12(csv.get(mh.get("numerobeneficiarios")));
							bean.setCopcDato13(csv.get(mh.get("nombrebeneficiarios")));
							bean.setCopcDato14(csv.get(mh.get("relacionasegurado")));
							bean.setCopcDato15(csv.get(mh.get("porcentajeparticipacion")));
							bean.setCopcCazbCdSucursal(csv.get(mh.get("idproveedor")));
							
							if (bean.getCopcCazbCdSucursal() == null || bean.getCopcCazbCdSucursal().length() == 0) bean.setCopcCazbCdSucursal(csv.get(mh.get("sucursal")));
							
							if (this.inIdVenta != null && this.inIdVenta > 0) {
								bean.setCopcDato9(csv.get(mh.get("idventa")));
								bean.setCopcDato16(this.inIdVenta.toString());
							}
							
							bean.setCopcDato11(nombreArchivo);
							
							// Para Prima �nica Hipotecario;
							if (inIdVenta == 7 && inPoliza == 0) {
								bean.setCopcDato6(csv.get(mh.get("saldoinsoluto/sumaasegurada"))); 
								bean.setCopcDato8(csv.get(mh.get("basecalculoprima"))); 
								bean.getId().setCopcSumaAsegurada(csv.get(mh.get("basecalculoprima"))); 
								bean.setCopcDato22(csv.get(mh.get("folioch"))); 
								bean.setCopcDato49("OCUPACION: " + csv.get(mh.get("ocupacion")).toUpperCase() 
	                                     + "|OBLIGADO CON INGRESOS: " + csv.get(mh.get("participaciondeingresos")).toUpperCase());
								if (bean.getCopcCazbCdSucursal().equalsIgnoreCase("8871")) 
									bean.setCopcSubProducto(bean.getCopcSubProducto().concat("_GE"));
							}
							
							//AUTOCREDITO
							if (inIdVenta == 10 && inPoliza == 0){
								bean.setCopcDato49("PRIMA AUTOCREDITO BCO: "+csv.get(mh.get("primaautocredito"))+"|"+
										           "COMISION x APERTURA: "+csv.get(mh.get("comisi�napertura")));
							}							
							
							// Vida Pyme
							if (inIdVenta == 5 && inPoliza == 0) {
								bean.setCopcDato22(csv.get(mh.get("atril"))); 						
								bean.setCopcDato23(csv.get(mh.get("nombreempresa")));//jalm
								bean.setCopcDato25(csv.get(mh.get("numeroclienteempresa")));//jalm
								bean.setCopcDato20(csv.get(mh.get("calleempresa")));//jalm 
								bean.setCopcDato36(csv.get(mh.get("numeroexteriorempresa")));//jalm 
								bean.setCopcDato37(csv.get(mh.get("numerointeriorempresa"))); //jalm
								bean.setCopcDato39(csv.get(mh.get("coloniaempresa"))); //jalm
								bean.setCopcDato38(csv.get(mh.get("delg/munempresa"))); //jalm
								bean.setCopcDato40(csv.get(mh.get("codigopostalempresa"))); //jalm
								bean.setCopcDato41(csv.get(mh.get("ciudadempresa"))); //jalm
								bean.setCopcDato42(csv.get(mh.get("estadoempresa"))); //jalm
								bean.setCopcDato43(csv.get(mh.get("rfcempresa"))); //jalm
								bean.setCopcDato44(csv.get(mh.get("correocliente"))); //jalm
								bean.setCopcDato45(csv.get(mh.get("conductocobro"))); //jalm
								bean.setCopcDato46(csv.get(mh.get("cuentacargo"))); //jalm
								bean.setCopcDato47(csv.get(mh.get("sucursalsantander"))); //jalm
								bean.setCopcDato48(csv.get(mh.get("correoejecutivo"))); //jalm
								bean.setCopcDato31(csv.get(mh.get("fechapago")));//jalm
								bean.setCopcDato1(csv.get(mh.get("giroempresa"))); //jalm								
 							}
							
							//LINEA EXPRESS
							if (inIdVenta == 8 && inPoliza == 0){
								bean.setCopcTipoCliente("1");
								bean.setCopcDato6(csv.get(mh.get("basecalculo")));
//								bean.setCopcDato8(csv.get(mh.get("basecalculoprima")));
//								bean.getId().setCopcSumaAsegurada(csv.get(mh.get("basecalculoprima")));
							}
							
							if (inPoliza == 0 &&(inIdVenta == 7 || inIdVenta == 6 || inIdVenta == 5 || inIdVenta == 9 || inIdVenta == 8)) {
								bean.setCopcDato18(csv.get(mh.get("fechafincredito")));
							} else {
								bean.setCopcDato34(csv.get(mh.get("fechafincredito")));
							}
						
							preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
							this.servicioCarga.guardarObjeto(preCarga);
						}
					} else {
						if (!csv.get(0).equals("*"))
							cifrasControl.add(csv.get(0)+": "+Double.parseDouble(csv.get(1)));
					}
				}
				
				csv.close();
				fr.close();
				file.delete();
			} else {
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
		} catch (IOException e) {
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		} catch (Exception e) {
			this.message.append("No se pud� cargar la informaci�n.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		
		return lista;
	}

	public List<Object> leerArchivoVidaNomina(File file, String delimitador) {
		// TODO Auto-generated method stub
		
		CsvReader csv = null;
		FileReader fr;
		PreCarga preCarga;
		BeanPreCarga bean;
		PreCargaId id;
		List<Object> lista = null;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

		String[] headers = null;
		Map<String, String> mh;
						
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			
			if(csv.readHeaders())
			{
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
				
				bean = new BeanPreCarga();
				
				this.message.append("Leyendo datos  pre carga VIDA.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				
				while(csv.readRecord())
				{
					if (!csv.get(0).trim().equals("")){
					
						id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "VID", csv.get(mh.get("sumaasegurada")));
						bean.setId(id);
											
						bean.setCopcCuenta(csv.get(mh.get("cuenta")));
						bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")));
						bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")));
						bean.setCopcNombre(csv.get(mh.get("nombre")));
						bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")));
						bean.setCopcSexo(csv.get(mh.get("sexo")));
						bean.setCopcNumCliente(csv.get(mh.get("numerocliente")));
						
						// tu carrera 
						bean.setCopcRfc(csv.get(mh.get("rfc"))+  csv.get(mh.get("homoclave")));
						bean.setCopcDato31(csv.get(mh.get("fechainiciovigencia")));
						bean.setCopcDato32(csv.get(mh.get("fechafinvigencia")));
						bean.setCopcDato36(csv.get(mh.get("curp")));
						bean.setCopcDato37(csv.get(mh.get("estadocivil")));
						bean.setCopcDato38(csv.get(mh.get("nacionalidad")));
						bean.setCopcDato39(csv.get(mh.get("telefonoParticular")));
						bean.setCopcDato40(csv.get(mh.get("telefonoficina")));
						bean.setCopcDato41(csv.get(mh.get("plazoseguro")));
						
						bean.setCopcCalle(csv.get(mh.get("calle")));
						bean.setCopcColonia(csv.get(mh.get("colonia")));
						 
						if ( mh.get("delg/mun") != null ) {
							bean.setCopcDelmunic(csv.get(mh.get("delg/mun")));
						} else 
						{
							bean.setCopcDelmunic(csv.get(mh.get("delegacion/municipio")));
						}
						bean.setCopcCdEstado(csv.get(mh.get("estado")));
						bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
						bean.setCopcDato27(csv.get(mh.get("lada")));
						bean.setCopcTelefono(csv.get(mh.get("telefono")));
						bean.setCopcTipoCliente(csv.get(mh.get("tipocliente")));
						bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
						bean.setCopcPrima(csv.get(mh.get("prima")));
						
						bean.setCopcBcoProducto(csv.get(mh.get("producto")));
						bean.setCopcSubProducto(csv.get(mh.get("subproducto")));
						bean.setCopcNuCredito(csv.get(mh.get("prestamo")));
						
						bean.setCopcDato7(csv.get(mh.get("tarifa")));
						
						bean.setCopcDato22(csv.get(mh.get("contratoenlace")));
						if(mh.containsKey("bucempresa")){ 
							bean.setCopcDato25(csv.get(mh.get("bucempresa")));
						} else if(mh.containsKey("fideicomiso")) {
							bean.setCopcDato25(csv.get(mh.get("fideicomiso")));
						}
						bean.setCopcDato23(csv.get(mh.get("nombreempresa")));
						
						bean.setCopcDato16(csv.get(mh.get("tiposeguro")));
						
						
						//datos comerciales 
						bean.setCopcDato46(csv.get(mh.get("segmento")));
						bean.setCopcDato41(csv.get(mh.get("segto_banco")));
						bean.setCopcDato42(csv.get(mh.get("mod_atn")));
						bean.setCopcDato49(csv.get(mh.get("ejecutivo"))+"-"+csv.get(mh.get("nm_ejecutivo")));								            
						bean.setCopcDato44(csv.get(mh.get("cat_eje")));
						bean.setCopcDato45(csv.get(mh.get("sucursal")));
						bean.setCopcDato43(csv.get(mh.get("suc_virtual")));
						bean.setCopcDato47(
								             csv.get(mh.get("cve_rg"))+"-"+
								             csv.get(mh.get("region"))             
								           );
						bean.setCopcDato48(
								            csv.get(mh.get("cve_zn"))+"-"+
								            csv.get(mh.get("zona"))
								           );

						//
						
						bean.setCopcDato11(nombreArchivo);
					
						preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
						
						//lista.add(preCarga);
						this.servicioCarga.guardarObjeto(preCarga);
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
				
				
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al carga la informacion del archivo.");
			this.log.error(message.toString(), e);
		}
		
		return lista;
	}
	
	public List<Object> leerArchivoGAP(File file, String delimitador) {
		// TODO Auto-generated method stub
		
		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCargaId id;
		PreCarga preCarga = null;
		List<Object> lista = null;	
		boolean hayCifrasContro = false;
				
		String[] headers = null;
		Map<String, String> mh;
				
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

						
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			if(csv.readHeaders())
			{
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
				bean = new BeanPreCarga();
				
				
				log.debug("Leyendo datos  pre carga GAP.");
								
				cifrasControl.clear();
								
				while(csv.readRecord())
				{
									
					if (csv.get(0).equals("*")) hayCifrasContro = true;
					
					if (!hayCifrasContro){
						if (!csv.get(0).trim().equals("")){
							
							bean.setCopcCargada("0");
							bean.setCopcRegistro("");
							
							id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "GAP", csv.get(mh.get("sumaasegurada")).length() > 0 ? csv.get(mh.get("sumaasegurada")) : csv.get(mh.get("saldoinsoluto")));
							
							bean.setId(id);
													
							bean.setCopcCuenta(csv.get(mh.get("cuenta")).trim());
							bean.setCopcNuCredito(csv.get(mh.get("prestamo")).trim());
							bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")).trim());
							bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")).trim().length() == 0 ? "." : csv.get(mh.get("apellidomaterno")).trim());
							bean.setCopcNombre(csv.get(mh.get("nombre")).trim());
							bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")).trim());
							bean.setCopcSexo(csv.get(mh.get("sexo")).trim());
							bean.setCopcNumCliente(csv.get(mh.get("numerocliente")).trim());
							bean.setCopcRfc(csv.get(mh.get("rfc")).trim());
							bean.setCopcCalle(csv.get(mh.get("calle")).length() > 150 ? csv.get(mh.get("calle")).substring(0, 149) : csv.get(mh.get("calle")).trim());
							bean.setCopcColonia(csv.get(mh.get("colonia")).length() > 55? csv.get(mh.get("colonia")).substring(0, 44) : csv.get(mh.get("colonia")).trim());
							bean.setCopcDelmunic(csv.get(mh.get("delg/mun")).length() > 50 ? csv.get(mh.get("delg/mun")).substring(0, 49) : csv.get(mh.get("delg/mun")).trim());
							bean.setCopcCdEstado(csv.get(mh.get("estado")));
							bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
							bean.setCopcDato27(csv.get(mh.get("lada")));
							bean.setCopcTelefono(csv.get(mh.get("telefono")));
							bean.setCopcTipoCliente(csv.get(mh.get("tipocliente")));
							bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
							bean.setCopcPrima(csv.get(mh.get("prima")));
							bean.setCopcDato6(csv.get(mh.get("basecalculoprima")));
							bean.setCopcDato8(csv.get(mh.get("sumaasegurada"))); // respaldo de la suma asegurada.	
							bean.setCopcDato7(csv.get(mh.get("tarifa")).trim());							
							bean.setCopcBcoProducto(csv.get(mh.get("producto")).trim());
							bean.setCopcSubProducto(csv.get(mh.get("subproducto")).trim());
							bean.setCopcDato33(csv.get(mh.get("fechainiciocredito")));	
							bean.setCopcDato34(csv.get(mh.get("fechafincredito")));
							bean.setCopcNuPlazo(csv.get(mh.get("plazo")));
							bean.setCopcDato2(csv.get(mh.get("tipo_veh")));
							bean.setCopcDato3(csv.get(mh.get("descripcionaut")));
							bean.setCopcDato4(csv.get(mh.get("modelo")));
							bean.setCopcDato5(csv.get(mh.get("numserie")));
							bean.setCopcDato19(csv.get(mh.get("numplaca")));
							bean.setCopcDato17(csv.get(mh.get("marca")));
							bean.setCopcDato20(csv.get(mh.get("tipocobertura")));
							bean.setCopcDato13(csv.get(mh.get("ramo")));
							bean.setCopcDato14(csv.get(mh.get("primaauto")));
							bean.setCopcDato15(csv.get(mh.get("nummotor")));
							
							if (this.inIdVenta != null && this.inIdVenta > 0) {
								bean.setCopcDato9(csv.get(mh.get("idventa")));
								bean.setCopcDato16(this.inIdVenta.toString());
							}
							
							//AUTOCREDITO
							if (inIdVenta == 10 && inPoliza == 0){
								bean.setCopcDato49("PRIMA AUTOCREDITO BCO: "+csv.get(mh.get("primaautocredito"))+"|"+
										           "COMISION x APERTURA: "+csv.get(mh.get("comisi�napertura")));
							}
							
							bean.setCopcDato22(csv.get(mh.get("polizaauto"))); 
							bean.setCopcDato11(nombreArchivo);
																				
							preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
						
							//lista.add(preCarga);
							this.servicioCarga.guardarObjeto(preCarga);
						}
					}else{
						if (!csv.get(0).equals("*"))
							cifrasControl.add(csv.get(0)+": "+Double.parseDouble(csv.get(1)));
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
				
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pud� cargar la informaci�n.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
		}
		
		return lista;
	}

	public String erroresEmision(){	
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		File objFile;
		Map<String, Object> inParams = new HashMap<String, Object>();
		StringBuffer nombreSalida, nombreNewSalida;
		FacesContext context;
		HttpServletResponse response;
		ServletOutputStream out;
		
		if(this.inRamo != null && this.inPoliza != null){			
			try {
				nombreSalida = new StringBuffer();
				nombreNewSalida = new StringBuffer();
				if (this.nombreArchivo == null || this.nombreArchivo.isEmpty())	
					this.nombreArchivo = "*";
				
				if (this.inCanal == null) 
					this.inCanal = 1;
				
				inParams.put("canal", this.inCanal);
				inParams.put("ramo", this.inRamo);
				inParams.put("poliza", this.inPoliza);
				inParams.put("idVenta", this.inIdVenta);
				inParams.put("archivo", this.nombreArchivo);
				nombreSalida.append("ErroresCarga_" + this.inCanal + "_" + this.inRamo + "_");
				nombreNewSalida.append("Errores_" + this.inCanal + "_" + this.inRamo + "_");
				
				if (this.inPoliza == 0) {
					nombreSalida.append("_" + inIdVenta + "_PU.csv");
					nombreNewSalida.append("_" + inIdVenta + "_PU");
				} else {
					nombreSalida.append(this.inPoliza + ".csv");
					nombreNewSalida.append(this.inPoliza);
				}
				
				rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/" + nombreSalida);
				rutaReporte = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ErroresCarga.jrxml");
				
				this.servicioCarga.reporteErrores(rutaTemporal, rutaReporte, inParams);
				objFile = crearArchivoErrorCatalogo(new File(rutaTemporal), nombreNewSalida.toString());
				
				if(objFile != null) {
					context = FacesContext.getCurrentInstance();
					if(!context.getResponseComplete()){
						input = new FileInputStream(objFile);
						bytes = new byte[1000];
						String contentType = "application/vnd.ms-excel";
						response = (HttpServletResponse) context.getExternalContext().getResponse();
						response.setContentType(contentType);
						
						DefaultHTTPUtilities httpUtils = new DefaultHTTPUtilities();
						httpUtils.setHeader(response, "Content-Disposition", "attachment;filename=" + objFile.getName());
						
						String bytesStr = ESAPI.encoder().encodeForBase64(bytes, false);
						byte[] newBytes = ESAPI.encoder().decodeFromBase64(bytesStr);
						
						out = response.getOutputStream();
						
						while((read = input.read(newBytes))!= -1){
							out.write(newBytes, 0 , read);
						}
						
						input.close();
						out.flush();
						out.close();
						objFile.delete();
						
						context.responseComplete();
					}
					
					// Borramos los datos de la precarga y carga.
					this.servicioCarga.borrarDatosTemporales(this.inCanal, this.inRamo, this.inPoliza, this.inIdVenta, this.nombreArchivo,"*");
					Utilerias.resetMessage(message);
					this.message.append("Archivo de errores descargado.");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
				} else {
					this.respuesta = "Ocurrio un error al descargar archivo de errores, intentelo nuevamente.";
				}

				return NavigationResults.SUCCESS;
			} catch (Exception e) {
				this.message.append("Error al generar el archivo de errores.");
				this.log.error(message.toString(), e);
				this.respuesta = this.message.toString();
				return NavigationResults.FAILURE;
			}			
		} else {
			this.message.append("El archivo de errores s�lo se puede descargar hasta que se haya realizado el proceso de emisi�n.");
			this.log.debug(message.toString());
			this.respuesta = this.message.toString();
			return NavigationResults.RETRY;
		}	
	}

	private File crearArchivoErrorCatalogo(File file, String nombreNewSalida) {
		CsvReader csv = null;
		FileReader fr;		
		List<Object> lista = null;
		String[] arrDatos, arrColumnas;
		Integer i = 0;
		String[] headers = null;
		Map<String, String> mh;
		Archivo objArchivo = null;
		
		try {
			arrColumnas = new String[8];
			fr = new FileReader(file);
			csv = new CsvReader(fr, ",".charAt(0));
			if(csv.readHeaders()) {
				headers = csv.getHeaders();
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
					if(!h.equals("Tabla")) {
						arrColumnas[i] = h;
						i++;
					}					
				}
				
				lista = new ArrayList<Object>();
				while(csv.readRecord()) {
					arrDatos = new String[8];
					arrDatos[0] = csv.get(mh.get("canal"));
					arrDatos[1] = csv.get(mh.get("ramo"));
					arrDatos[2] = csv.get(mh.get("poliza"));
					arrDatos[3] = csv.get(mh.get("identificador"));
					arrDatos[4] = csv.get(mh.get("producto"));
					arrDatos[5] = csv.get(mh.get("subproducto"));
					arrDatos[6] = csv.get(mh.get("archivocarga"));
					try {
						arrDatos[7] = csv.get(mh.get("tabla")) + servicioParametros.getErrorCatalogo(csv.get(mh.get("estatus")));
					} catch (Exception e) {
						arrDatos[7] = csv.get(mh.get("tabla"));
					}
					
					lista.add(arrDatos);
				}
				
				csv.close();
				fr.close();
				file.delete();
				
				objArchivo = new Archivo(FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator, nombreNewSalida, ".csv");
				objArchivo.copiarArchivo(arrColumnas, lista, ",");
			}
		} catch (IOException e) {
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		} catch (Exception e) {
			this.message.append("No se puede cargar la informacion del archivo.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}

		return objArchivo.getArchivo();
	}

	public void setValor(){
		this.inIdVenta = this.inIdVenta.intValue();  
	}
	
    public String cargaArchivoEnlace() throws Excepciones{
		
		this.log.debug("Iniciando proceso de carga ENLACE");
		List<Object> lista = new ArrayList<Object>();
		
		boolean flgGuardaDatosArchivo = false;
		boolean flgEmitir = false;
		String strForward = null;
		System.out.println("00");
		if(this.inRamo != null && this.archivo != null)	{
		
			try {
				
					this.inCanal = 1;
					this.totalCarga = 0L;
					System.out.println("0");
					try{
						System.out.println("1");
						lista = this.leerArchivoEnlace(this.archivo, ";");
						System.out.println("2");
						flgGuardaDatosArchivo = true;
						System.out.println("3");
						if(flgGuardaDatosArchivo){
						
							message.append("Iniciando proceso de carga....");
							this.log.debug(message.toString());
							Utilerias.resetMessage(message);
							
							this.servicioCarga.guardarObjetos(lista);
							
							this.nombreArchivo = this.archivo.getName().substring(this.archivo.getName().lastIndexOf("\\") + 1);
												
							this.message.append("Comenzando la emisi�n del ramo:").append(this.inRamo.toString()).append(" poliza:" ).append(this.inPoliza.toString()).append(".");
							this.log.debug(message.toString());
							Utilerias.resetMessage(message);
							flgEmitir = true;
							
						} else if(flgEmitir == false){
							this.message.append("El archivo a cargar no es correcto.");
							this.log.debug(message.toString());
							this.respuesta = this.message.toString();
							Utilerias.resetMessage(message);
							strForward =  NavigationResults.FAILURE;
						}
						
						this.generaLayoutRamo56582(inRamo);
						this.servicioCertificado.borraErrores56582(inRamo);
						
					}catch (DataAccessException e) {
							this.message.append("Error al leer los datos del archivo.");
							this.log.error(message.toString(), e);
							this.respuesta = this.message.toString();
							Utilerias.resetMessage(message);
							// Borramos archivo temporal
							this.archivo.delete();
							flgEmitir = false;
							strForward =  NavigationResults.FAILURE;
					}catch (Exception e){
						this.message.append("El proceso de carga no concluy� correctamete. Revise el archivo");					
						this.log.error(message.toString() + " Error interno.", e);
						this.respuesta = this.message.toString();
						Utilerias.resetMessage(message);
						// Borramos archivo temporal
						this.archivo.delete();
						
						flgEmitir = false;
						strForward =  NavigationResults.FAILURE;
					}

			}catch (Exception e) {
					this.message.append("El proceso de emisi�n no concluy� correctamete.");					
					this.log.error(message.toString() + " Error interno.", e);
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
					// Borramos archivo temporal
					this.archivo.delete();
					
					strForward =  NavigationResults.FAILURE;
			}
			
		} else {
			
			this.message.append("El ramo, poliza y/o  el el archivo a cargar no son correctos.");
			this.log.debug(message.toString());
			this.respuesta = this.message.toString();
			Utilerias.resetMessage(message);
			
			strForward =  NavigationResults.FAILURE;
			
		} // end if ramo, poliza, archivo not null
		return strForward;
		
	}
	
	public List<Object> leerArchivoEnlace(File file, String delimitador) {
		
		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCargaId id;
		PreCarga preCarga = null;
		List<Object> lista = null;
		
		String[] headers = null;
		Map<String, String> mh;
		inCanal = 1;
		inPoliza = 999L;
		int iden=0;
		int suma=15000;
		int ts = 1;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			if(csv.readHeaders())
			{
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				bean = new BeanPreCarga();
				
				lista = new ArrayList<Object>();	
				this.message.append("Validando los tipos de datos del archivo");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				iden = 1;
				int tc = 1;
				while(csv.readRecord())
				{
					if (!csv.get(0).trim().equals("")){
						id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, String.valueOf(iden), "ENL", String.valueOf(suma));

						bean.setId(id);
						bean.setCopcCuenta(csv.get(mh.get("0cuenta_abon")));
						bean.setCopcApPaterno(csv.get(mh.get("apell_paterno")));
						bean.setCopcApMaterno(csv.get(mh.get("apell_materno")));
						bean.setCopcNombre(csv.get(mh.get("nombre")));
						bean.setCopcSexo(csv.get(9));
						bean.setCopcNumCliente(csv.get(29));
						bean.setCopcRfc(csv.get(mh.get("reg_fed_caus")));
						bean.setCopcCalle(csv.get(mh.get("calle_numero")));
						bean.setCopcColonia(csv.get(mh.get("colonia")).length() > 55 ? csv.get(mh.get("colonia")).substring(0, 54): csv.get(mh.get("colonia")));
						bean.setCopcDelmunic(csv.get(mh.get("deleg_municipio")).length() > 50 ? csv.get(mh.get("deleg_municipio")).substring(0, 49): csv.get(mh.get("deleg_municipio")));
						bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codi7"))));
						bean.setCopcTelefono(csv.get(22));
						bean.setCopcDato16(String.valueOf(ts));
						bean.setCopcDato22(csv.get(mh.get("num_cuenta2")));
						bean.setCopcDato25(csv.get(mh.get("num_per")));
						bean.setCopcDato33(csv.get(28));
						bean.setCopcTipoCliente(String.valueOf(tc));
						bean.setCopcDato11(nombreArchivo);
					
						preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
						lista.add(preCarga);
						iden++;
					}
				}
				
				csv.close();
				fr.close();
				file.delete();
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
			
		} 
		catch (IOException e) {
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			this.message.append("No se pud� cargar la informacion.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}
		
		return lista;
		
	}

	@SuppressWarnings("deprecation")
	public String generaLayoutRamo56582(Short inRamo) throws Excepciones, IOException{
		
		String archivo=null;
		
		System.out.println("Entre a generaLayoutRamo56582 --- BeanListaPreCarga");
	    //------------------------------------------------------------------
	    log.info("Reporte LayOut's");
	    log.info("Generando Reporte");
	    //------------------------------------------------------------------
	    
	    if(inRamo==5){
	    	archivo = this.servicioCertificado.generaLayoutRamo5(inRamo);
	    }
	    else if (inRamo==65){
		    	archivo= this.servicioCertificado.generaLayoutRamo65(inRamo);
		    }
		    else if (inRamo==82){
			    	archivo= this.servicioCertificado.generaLayoutRamo82(inRamo);
			    }
	    
	    String reporte = FacesUtils.getServletRequest().getRealPath("/WEB-INF/reportes/") + File.separator;
        System.out.println("Ruta del reporte... " + reporte);
        
        HttpServletResponse response = FacesUtils.getServletResponse();
		int read = 0;
		byte[] bytes = new byte[1024];
	    
	    reporte += archivo;
	    response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=\"" + archivo + " \"");
		
		File file = new File(reporte);
		
		FileInputStream in;
		try {
			System.out.println(file.toString());
			in = new FileInputStream(file);
		
			OutputStream out = response.getOutputStream();

			while((read = in.read(bytes)) != -1){
				out.write(bytes,0,read);
			}

			in.close();
	
			out.flush();
			out.close();
		
		} catch (Exception e) {
			log.error("Error al enviar la el reporteErrores5758",e);
			FacesUtils.addInfoMessage("Error al generar el reporte");
			//FacesUtilidad.addInfoMessage("Error al generar el reporte");
		}
		
		FacesContext.getCurrentInstance().responseComplete();
		
        log.info("Reporte Generado con �xito");
		FacesUtils.getServletRequest().getSession().setMaxInactiveInterval(1800);
		
		return NavigationResults.SUCCESS;

	}
	
	public List<Object> leerArchivoVidaPyme(File file, String delimitador) {
		// TODO Auto-generated method stub
		int i=1;
		int valResult;
		int valSuma;
		int valPrima;
		int fechaVigencia;
		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCargaId id;
		PreCarga preCarga = null;
		List<Object> lista = null;	
		boolean hayCifrasContro = false;

		Filtros filtros		   	 = new Filtros();
		BigDecimal idCertificado = null;
		
		String[] headers = null;
		Map<String, String> mh;
				
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);

		System.out.println("Entro a leerArchivoVidaPyme");				
		try 
		{
			this.message.append("Abriendo archivo de carga masiva ramo 57 o 58");
			System.out.println("Abriendo archivo de carga masiva ramo 57 o 58  ");		
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));
			
			if(csv.readHeaders()) {
				headers = csv.getHeaders();
				mh = new HashMap<String, String>();
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
					
				lista = new ArrayList<Object>();
				bean = new BeanPreCarga();
				
				
				log.debug("Leyendo datos pre carga VIDA PYME.");
				System.out.println("Leyendo datos  pre carga VIDA PYME  ");		
				cifrasControl.clear();
							
				idCertificado = this.servicioCertificado.maxNumeroDeCredito(filtros.maxNumeroDeCredito(this.inRamo, this.inPoliza));
				while(csv.readRecord()) {
					if (csv.get(0).equals("*")) hayCifrasContro = true;
					if (!hayCifrasContro){
						if (!csv.get(0).trim().equals("")){
							idCertificado = new BigDecimal(filtros.nextCertificado(idCertificado,this.inPoliza));
							bean.setCopcCargada("0");
							bean.setCopcRegistro("");
							id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, idCertificado.toString() , "PYM", csv.get(mh.get("sumaasegurada")).length() > 0 ? csv.get(mh.get("sumaasegurada")) : csv.get(mh.get("saldoinsoluto")));
							
							// obtenemos producto plan
						StringBuilder filtro = new StringBuilder(100);
						    Integer cdProd;
							Integer cdPlan;
							String  saseg;
							
							filtro.append("  and C.id.coceCasuCdSucursal= 1 \n");
							filtro.append("  and C.id.coceCarpCdRamo = ").append(inRamo).append("\n");
							filtro.append("  and C.id.coceCapoNuPoliza = ").append(inPoliza).append("\n");
						    filtro.append("  and C.id.coceNuCertificado = 0 \n" );
						    
						    bean.setCopcCargada("0");
							bean.setCopcRegistro("");
							bean.setCopcDato16("0"); 		//Dato16 = ID_VENTA
							bean.setCopcTipoCliente("1"); 	//TIPO CLIENTE
							
							
							
							System.out.println("--------- TESTING PASSING OBJECTS -------- ");
							System.out.println(bean.getCopcCargada());
							System.out.println(bean.getCopcApPaterno());
					

						    
						    List<Certificado> ListaprodPlan = this.servicioCertificado.obtenerProdPlan(filtro.toString());
						    
						    for (Certificado certificado : ListaprodPlan) {
								
								this.ListaprodPlan.add(certificado);
								
								cdPlan = certificado.getPlanes().getId().getAlplCdPlan();
								cdProd = certificado.getPlanes().getId().getAlplCdProducto();
								saseg = certificado.getCoceMtSumaAsegurada().toString();
								bean.setCopcPrima(certificado.getCoceMtPrimaSubsecuente().toPlainString());
								
								valPrima 	= 	certificado.getCoceMtPrimaSubsecuente().intValue();
								valSuma 	=  	certificado.getCoceMtSumaAsegurada().intValue();
								valResult 	=	(valPrima*1000)/valSuma;
								////////////////////////////////////////////////
								bean.setCopcDato7(""+valResult);
								bean.setCopcFrPago(certificado.getCoceFrPago());
								bean.setCopcCazbCdSucursal(certificado.getCoceCazbCdSucursal().toString());
								System.out.println(" saseg " + saseg);
								System.out.println(" cdPlan " + cdPlan);
								System.out.println(" cdProd " + cdProd);
								
								bean.setCopcCdPlan(cdPlan.toString());
								bean.setCopcCdProducto(cdProd.toString());
							
								id.setCopcSumaAsegurada(saseg);

								//LPV dato 8 y dato 6 26/01/2016
								bean.setCopcDato6(saseg);
								bean.setCopcDato8(saseg);

							}
						    
						    
						    bean.setId(id);
							bean.setCopcNuCredito(id.getCopcIdCertificado()); //Se utilza el mismo valor que IdCertificado
							bean.setCopcCuenta(id.getCopcIdCertificado());    //Se utilza el mismo valor que IdCertificado
							
							bean.setId(id);
							bean.setCopcApPaterno(csv.get(mh.get("apellidopaterno")).trim());
							bean.setCopcApMaterno(csv.get(mh.get("apellidomaterno")).trim().length() == 0 ? "." : csv.get(mh.get("apellidomaterno")).trim());
							bean.setCopcNombre(csv.get(mh.get("nombre")).trim());
							bean.setCopcFeNac(csv.get(mh.get("fechanacimiento")).trim());
							bean.setCopcSexo(csv.get(mh.get("sexo")).trim());
							bean.setCopcNumCliente(csv.get(mh.get("numerocliente")).trim());
							bean.setCopcRfc(csv.get(mh.get("rfc")).trim());
							bean.setCopcDato20(csv.get(mh.get("curp")).trim());			// curp  ver en que campo MGE
							//bean.setCopcNuCredito(csv.get(mh.get("estadocivil")).trim());		//  Estado Civil  MGE
							
							bean.setCopcDato40(csv.get(mh.get("ocupacion")).trim());			// ocupacion      MGE
							bean.setCopcCalle(csv.get(mh.get("calle")).length() > 150 ? csv.get(mh.get("calle")).substring(0, 149) : csv.get(mh.get("calle")).trim());
							bean.setCopcColonia(csv.get(mh.get("colonia")).length() > 55? csv.get(mh.get("colonia")).substring(0, 44) : csv.get(mh.get("colonia")).trim());
							bean.setCopcDelmunic(csv.get(mh.get("delg/mun")).length() > 50 ? csv.get(mh.get("delg/mun")).substring(0, 49) : csv.get(mh.get("delg/mun")).trim());
							bean.setCopcCdEstado(csv.get(mh.get("estado")));
							
							bean.setCopcCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
							bean.setCopcDato27(csv.get(mh.get("lada")));
							bean.setCopcTelefono(csv.get(mh.get("telefono")));
							bean.setCopcDato21(csv.get(mh.get("correoelectronico")));				   //Correo Electronico MGE
							bean.setCopcTipoCliente(csv.get(mh.get("tipocliente")));
							bean.setCopcDato16(csv.get(mh.get("idventa")));   			//id venta  	0 		Alfanumerico 2  	 MGE
							//-----
							bean.setCopcDato12(csv.get(mh.get("numerobeneficiarios"))); 		//NumeroBeneficiarios 	Numerc 2			 MGE
							bean.setCopcDato13(csv.get(mh.get("nombrebeneficiarios")));  		//NombreBeneficiarios	Alfanumerico 300	 MGE	
							bean.setCopcDato14(csv.get(mh.get("relacionasegurado")));  			// RelacionAsegurado 	Alfanumerico 10		 MGE
							bean.setCopcDato15(csv.get(mh.get("porcentajeparticipacion")));  	// PorcentajeParticipacion Alfanumerico 30	 MGE
							bean.setCopcDato25(csv.get(mh.get("fechanacimientobeneficiario"))); //Fecha de Nacimiento Beneficiario  Alfanumerico  50 	 MGE
							//bean.setCopcFeIngreso(csv.get(mh.get("feinclusionbeneficiario")));  	//Fe. Inclusi�n Beneficiario		Alfanumerico  50 	 MGE
							bean.setCopcDato49(csv.get(mh.get("callebeneficiario")));  			//Calle Beneficiario				Alfanumerico  300	 MGE
							bean.setCopcDato50(csv.get(mh.get("coloniabeneficiario")));  		//Colonia Beneficiario				Alfanumerico  300	 MGE
							bean.setCopcDato2(csv.get(mh.get("delg/munbeneficiario")));  		//Delg/Mun Beneficiario				Alfanumerico  300	 MGE
							bean.setCopcDato1(csv.get(mh.get("estadobeneficiario")));  			//Estado Beneficiario				Alfanumerico  100	 MGE
							bean.setCopcDato3(csv.get(mh.get("codigopostalbeneficiario")));  	//CodigoPostal Beneficiario			Alfanumerico  100	 MGE
							bean.setCopcPlazoTiempo(csv.get(mh.get("pregunta1")));  		//Pregunta1							Alfanumerico  2		 MGE
							
							
							bean.setCopcDato4(csv.get(mh.get("pregunta3")));  			//Pregunta3							Alfanumerico  2		 MGE
							bean.setCopcDato41(csv.get(mh.get("pregunta4")));  				//Pregunta4							Alfanumerico  2		 MGE
							bean.setCopcDato5(csv.get(mh.get("pregunta5")));  		//Pregunta5							Alfanumerico  2		 MGE
							bean.setCopcDato11(nombreArchivo);
							bean.setCopcDato5((csv.get(mh.get("pregunta2"))));
							bean.setCopcNuPlazo("12");  								// plazo es para todos el mismo 12 
							bean.setCopcDato10("1"); 									 //tipo de poliza 1 poliza no retroactiva 
							
							
							
							DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
						    Date date = new Date();
						    						    						    
						    bean.setCopcRemesa(GestorFechas.formatDate((date), "yyyyMM"));  				//Pregunta2							Alfanumerico  2		 MGE
						    						    
						    bean.setCopcDato34(GestorFechas.addMesesaFecha(date, 12));
						    
							bean.setCopcDato33( (dateFormat.format(date)).toString() ); // sysdate 
							int day = date.getDay();
							int month = date.getMonth();							
																	
							bean.setCopcFeIngreso((dateFormat.format(date)).toString());
							
														
							// impreaion de campos 
							
							System.out.println("Antes de entrar  a insertar pre carga--> ");
							
							// Validamos respuestas de preguntas   				
							String texto =  bean.getCopcPlazoTiempo().toString()+'|'+bean.getCopcRemesa().toString()+'|'+
									        bean.getCopcDato4().toString().toString() +'|'+
									        bean.getCopcDato41().toString()+'|'+ bean.getCopcDato5().toString();
							
							String palabraBuscada = "SI";
						
							if ( ValidaRespuesta(texto , palabraBuscada) <= 0 )
							{
								
								preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
								lista.add(preCarga);
								
								this.servicioCarga.guardarObjeto(preCarga);
								idCertificado = idCertificado.add(new BigDecimal("1"));
							}
							else {
								this.message.append("El regstro no puede ser cargado validar preguntas");
							}
					
						}		
							
							
					}else{
						if (!csv.get(0).equals("*"))
							cifrasControl.add(csv.get(0)+": "+Double.parseDouble(csv.get(1)));
					}
				//i++;	
					
				}
				
				csv.close();
				fr.close();
				file.delete();
				
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo ");
			this.log.error(message.toString(), e);
			e.printStackTrace();
			FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pud� cargar la informaci�n.");
			this.log.error(message.toString(), e);
			e.printStackTrace();
			FacesUtils.addErrorMessage(message.toString());
			
		}
		System.out.println("Regresara la lista ");
		return lista;
	}
	
	public static int ValidaRespuesta(String texto, String palabraBuscada)
    {

	   
	   String palabraPuntoArray[] = texto.split("\\|");
	   int cont=0;
	   boolean resp ;
	   
	   for(String palabra : palabraPuntoArray){
   
		   resp = palabra.equalsIgnoreCase(palabraBuscada);
		   
		   if(resp == true)
			   {
				   cont = cont+1 ;
			   }
	   }
	   System.out.println( "Cont:" + cont );
	 return  cont; 
	   
    }
	
	
	/**
	 * Metodo que realiza el proceso de validaci�n
	 * @param ramo se refiere al ramo de la p�liza a validar
	 * @param poliza poliza a validar
	 * @param idVenta identificador de canal de venta
	 * @return terminado con exitoso o fallido
	 */
	public String preEmision(){
		/* Inicializacion de terminado */
		String terminado=null;
		
		Map<String, Object> respuesta;
		
		log.info("Inicia validacion BeanEmisionMasiva");
		/* Instancia de mensaje */
		this.message = new StringBuilder();
		/* Seteo de canal */
		Short canal=1;
		
		if (this.inRamo == null) {
			this.respuesta = "Debe seleccionar un ramo";
			this.codigoResp = -1;
			return  NavigationResults.SUCCESS;
		}
		
		if (this.inPoliza == null) {
			this.respuesta = "Debe seleccionar una poliza";
			this.codigoResp = -1;
			return  NavigationResults.SUCCESS;
		}
		
		if (this.archivo == null){
			
			this.respuesta = "Debe seleccionar un archivo a cargar.";
			this.codigoResp = -1;
			return  NavigationResults.SUCCESS;
						
		}
		
		
		
		try {
			/* Llamado al servicio de validacion de p�lizas */
			respuesta = this.servicioValidaEmision.validPolizaEmiMasiva(canal, this.inRamo, this.inPoliza, 0);

						
			if (respuesta.containsKey("pMensaje")) {
				this.respuesta = (String) respuesta.get("pMensaje");
			}
			
			this.codigoResp = -1;
			if (respuesta.containsKey("pValor")) {
				this.codigoResp= (Integer) respuesta.get("pValor");
			}
			
			this.respValidacion = -1;
			
			if (respuesta.containsKey("returnname")) {
				this.respValidacion= (Integer) respuesta.get("returnname");
			}
			
			/* Seteo de terminado exitoso */
			terminado =  NavigationResults.SUCCESS;
			/* Bloque de excepciones */
		} catch (Excepciones e) {
			log.error("Excepcion en la clase BeanEmisionMasiva");
			log.error(e);
			this.respuesta = "Hubo un error al validar la Emision";
			this.codigoResp= 9;
			/* Seteo de navegacion fallida */
			terminado =  NavigationResults.SUCCESS;
		} catch (NumberFormatException e) {
			log.error("Excepcion en la clase BeanEmisionMasiva");
			log.error(e);
			this.respuesta = "Error al obtener informacion de la poliza, Valide su estatus";
			this.codigoResp= 9;
			/* Seteo de navegacion fallida */
			terminado =  NavigationResults.SUCCESS;
		}
		/* Retorno de la cadena de terminado */
		return terminado;
	}
}
