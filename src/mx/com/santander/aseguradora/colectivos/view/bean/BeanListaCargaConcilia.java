package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.controller.BeanListaCargaConcilia2;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecargaId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

/**
 * @author Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanListaCargaConcilia extends BeanListaCargaConcilia2 implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
		
	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioParametros servicioParametros;
	
	private Integer inOrigen;
	private File archivo;
	private String nombreArchivo;
	private String respuesta;
	private List<Object> listaComboOrigen; 
	private Date fechaIni;
	private Date fechaFin;
	private Map<String, String> hsmEstados, hsmPlazos;
	private List<Object> lstResultados;
	private List<Object> listaCuentas;
	private String cuentas[];
	private Integer nTotalArchivos;
	private ArrayList<Archivo> lstArchivos = new ArrayList<Archivo>();
	private Integer nTipoArchivo = 1;
	private Integer nTipoArchivoHPU;
	private Integer nTipoArchivoBT = 1;
	private Integer nResultado;
	
	public BeanListaCargaConcilia() {
		setListaComboOrigen(new ArrayList<Object>());
		setListaCuentas(new ArrayList<Object>());
		setLstArchivos(new ArrayList<Archivo>());
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_CARGA_CONCILIA);
	}

		
	public void listener(UploadEvent event) throws Exception {
		UploadItem item;
		File archivoTemporal;
		String ruta;
		String nombreArchivo = null;
		String nombreRuta;
		Archivo file;
		
		try {
			item = event.getUploadItem();
			ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS);
			file = new Archivo();
			
			if(item.getData() != null){
				file.setData(item.getData());
				file.setNombreArchivo(item.getFileName());
				file.setTamano(item.getData().length);
				nombreRuta = file.getNombreArchivo() + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
				nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
				archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
				FileUtil.write(archivoTemporal, item.getData());
				setArchivo(archivoTemporal);
				setRespuesta("Archivo cargado al servidor correctamente");
				log.debug(getRespuesta());
			} 
		} catch (Exception e) {
			setRespuesta("Error al cargar Archivo al servidor.");
			log.debug(getRespuesta());
		}
	}
	
	public void listenerCuentas(UploadEvent event) throws Exception {
		UploadItem item;
		File archivoTemporal;
		String ruta;
		String nombreArchivo = null;
		String nombreRuta;
		Archivo file;
		
		try {
			if(inOrigen == Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR) {
				item = event.getUploadItem();
				ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS);
				file = new Archivo();
				
				if(!item.isTempFile()){
					file.setData(item.getData());
					file.setNombreArchivo(item.getFileName());
					file.setTamano(item.getData().length);
					nombreRuta = file.getNombreArchivo() + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
					nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
					archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
					FileUtil.write(archivoTemporal, item.getData());
					file.setArchivo(archivoTemporal);
					file.setNombreArchivo(nombreArchivo);
					getLstArchivos().add(file);
					setnTotalArchivos(getnTotalArchivos() - 1);
				}
				
				setRespuesta("Archivo cargado al servidor correctamente");
				log.debug(getRespuesta());
			}				
		} catch (Exception e) {
			setRespuesta("Error al cargar Archivo al servidor.");
			log.debug(getRespuesta());
		}
	}
	
	public String cargar() {
		String strForward = null;
		int nRegConsecutivo = 0;
		
		try {
			if(validarDatos()) {
				switch (getInOrigen()) {
					case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR:
					case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR:
					case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
					case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:
						nRegConsecutivo = servicioCarga.getConsecutivo(getInOrigen());
						strForward = cargarVentas(nRegConsecutivo);
						break;
						
					case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
						if(getnTipoArchivoHPU() == 1) {
							nRegConsecutivo = servicioCarga.getConsecutivo(getInOrigen());
							strForward = cargarVentas(nRegConsecutivo);
						} else {
							strForward = actualizarDatosHPU();
						}
						break;
				
					/***************************************APV************************************************/
					case Constantes.CONCILIACION_ORIGEN_BT_VALOR:
						if(getnTipoArchivoBT() == 1) {
							nRegConsecutivo = servicioCarga.getConsecutivo(getInOrigen());
							strForward = cargarVentas(nRegConsecutivo);
							servicioProcesos.validarCumulosBT();
							enviarArchivoPampa();
							
						} else {
							strForward = actualizarEstatusPampa();
						}
						break;
					/**************************************APV**************************************************/
						
					case Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR:
						if(esDesempleoTDC()) {
							nRegConsecutivo = 0;							
							strForward = cargarCuentasDesempleo();
						} else {
							nRegConsecutivo = servicioCarga.getConsecutivo(getInOrigen());
							strForward = cargarCuentas(nRegConsecutivo);
						}
						break;
						
					case Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR:
						nRegConsecutivo = servicioCarga.getConsecutivo(getInOrigen());
						cargaVentasDesempleoTDC(Constantes.CONCILIACION_DESEMPLEO_TDC, nRegConsecutivo);
						break;
				}	
				setnResultado(10);
			} else {				
				log.debug(getRespuesta());
				if(getArchivo() != null)
					getArchivo().delete();
				setLstArchivos(new ArrayList<Archivo>());
				setnTotalArchivos(listaCuentas.size());
				strForward = NavigationResults.FAILURE;
				setnResultado(getInOrigen());
			}
		} catch (Exception e) {
			setRespuesta("No se puede realizar el proceso de carga." );
			log.error(getRespuesta() + e.getLocalizedMessage(), e);
			if(getArchivo() != null)
				getArchivo().delete();
			setLstArchivos(new ArrayList<Archivo>());
			setnTotalArchivos(listaCuentas.size());
			setnResultado(getInOrigen());
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}

	private void enviarArchivoPampa() {
		List<Object> lstObjetosPampa = null;
		List<String> lstArchivos;
		Archivo objArchivo;
		String ruta;
		
		try {
			lstArchivos = new ArrayList<String>();
			lstObjetosPampa= servicioCarga.consultaDatosPampa();
			ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS)+ File.separator;
			objArchivo = new Archivo(ruta, "PampaTmp" + GestorFechas.formatDate(new Date(), "ddMMyyyy"), ".txt");
			objArchivo.copiarArchivo(new String[0], lstObjetosPampa, "");
			lstArchivos.add(objArchivo.getArchivo().getPath());
			
		    servicioCarga.updatePampa();
		} catch (Exception e) {
			setRespuesta("Hubo un error al enviarArchivoPampa.");
			log.error(getRespuesta(), e);
		}
		
	}

	private String cargarVentas(int nRegConsecutivo) throws Exception {
		List<Object> lstObjetosGuardar = null;
		ArrayList<ArrayList<Resultado>> arlRegistrosCargados;
		String strForward = null;
		String strDelimitador = null, strOrigen = null;
		int ramo = 0;
		
		try {
			lstResultados = new ArrayList<Object>();
			
			switch (getInOrigen()) {
				case Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR:
					strDelimitador = Constantes.CONCILIACION_ALTAMIRA_DELIMITADOR;
					strOrigen = Constantes.CONCILIACION_ALTAMIRA;
					ramo = Constantes.CONCILIACION_ALTAMIRA_RAMO;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR:
					strDelimitador = Constantes.CONCILIACION_ALTAIR_DELIMITADOR;
					strOrigen = Constantes.CONCILIACION_ALTAIR;
					ramo = Constantes.CONCILIACION_ALTAIR_RAMO;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR:
					strDelimitador = Constantes.CONCILIACION_HIPOTECARIO_DELIMITADOR;
					strOrigen = Constantes.CONCILIACION_HIPOTECARIO;
					ramo = Constantes.CONCILIACION_HIPOTECARIO_RAMO;
					break;
				
				case Constantes.CONCILIACION_ORIGEN_BT_VALOR:
					strDelimitador = Constantes.CONCILIACION_BT_DELIMITADOR;
					strOrigen = Constantes.CONCILIACION_BT;
					ramo = Constantes.CONCILIACION_BT_RAMO;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_LCI_VALOR:
					strDelimitador = Constantes.CONCILIACION_LCI_DELIMITADOR;
					strOrigen = Constantes.CONCILIACION_LCI;
					ramo = Constantes.CONCILIACION_LCI_RAMO;
					break;
					
				case Constantes.CONCILIACION_ORIGEN_GAP_VALOR:
					strDelimitador = Constantes.CONCILIACION_GAP_DELIMITADOR;
					strOrigen = Constantes.CONCILIACION_GAP;
					ramo = Constantes.CONCILIACION_GAP_RAMO;
					break;
			}
			 
			arlRegistrosCargados = servicioCarga.cargadasAnteriormente(getInOrigen(), ramo, strOrigen, getFechaIni(), getFechaFin(), getCuentas());
			lstResultados.addAll(arlRegistrosCargados.get(1));
			if(((Resultado)arlRegistrosCargados.get(0).get(0)).getFecha() != null) {	//Si hay fechas para cargar 
				lstObjetosGuardar = leerArchivoConciliacion(arlRegistrosCargados.get(0), nRegConsecutivo, ramo, strOrigen, strDelimitador);
			
				/*if(lstObjetosGuardar == null){
					setRespuesta("Error al procesar archivo, un registro esta fuera del rango de fechas especificado.");
					log.debug(getRespuesta());
					lstResultados.clear();
					setFlgShowResultados(false);
					getArchivo().delete();
					strForward = NavigationResults.FAILURE;
				} else */
				if(!lstObjetosGuardar.isEmpty()){
					servicioCarga.guardarObjetos(lstObjetosGuardar);
					lstObjetosGuardar.clear();
					
					setRespuesta("El proceso de carga finaliz� correctamente.");
					log.info(getRespuesta());
					lstResultados.addAll(servicioCarga.buscarRegistrosGuardados(getInOrigen(), ramo, strOrigen, arlRegistrosCargados.get(0)));
					getArchivo().delete();
					strForward = NavigationResults.SUCCESS;
				} else {
					setRespuesta("El proceso de carga finaliz� correctamente, no se guardo ningun registro.");
					log.info(getRespuesta());
					lstResultados.addAll(servicioCarga.buscarRegistrosGuardados(getInOrigen(), ramo, strOrigen, arlRegistrosCargados.get(0)));
					getArchivo().delete();
					strForward = NavigationResults.SUCCESS;
				}
			} else {
				setRespuesta("Todas las fechas ya fueron cargadas con anterioridad.");
				log.info(getRespuesta());
				getArchivo().delete();
				strForward = NavigationResults.SUCCESS;
			}
		} catch (DataAccessException e) {
			setRespuesta("Hubo un error al guardar los datos.");
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		} catch (Exception e) {
			setRespuesta("No se puede realizar el proceso de carga." + e.getLocalizedMessage());
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}
	
	private String cargarCuentas(int nRegConsecutivo) throws Exception {
		ArrayList<ArrayList<Object>> arlDatos = null;
		List<Object> lstObjetosGuardar = null;
		ArrayList<ArrayList<Resultado>> arlRegistrosCargados;
		ArrayList<Resultado> arlFechas, arlFechas2;
		String strForward = NavigationResults.SUCCESS;
		
		try {
			lstResultados = new ArrayList<Object>();
			lstObjetosGuardar = new ArrayList<Object>();
			arlFechas2 = new ArrayList<Resultado>();
			
		
			arlRegistrosCargados = servicioCarga.cargadasAnteriormente(getInOrigen(), 0, "", getFechaIni(), getFechaFin(), getCuentas());
			for(int i = 0; i < arlRegistrosCargados.size(); i++){
				arlFechas = arlRegistrosCargados.get(i);
				if((i % 2) == 0) {
					if(((Resultado)arlFechas.get(0)).getFecha() != null) {	//Si hay fechas para cargar
						arlDatos = leerArchivoCuentas(arlFechas, nRegConsecutivo);
						lstObjetosGuardar.addAll(arlDatos.get(0));
						lstResultados.addAll(arlDatos.get(1));
						arlFechas2.addAll(arlFechas);
					} else {
						((Resultado)arlFechas.get(0)).setDescripcion("Todas las fechas ya fueron cargadas con anterioridad.");
						lstResultados.add(arlFechas.get(0));
					}
				} else {
					lstResultados.addAll(arlFechas);
				}
			}
			
			servicioCarga.guardarObjetos(lstObjetosGuardar);
			lstObjetosGuardar.clear();
			
			setRespuesta("El proceso de carga finaliz� correctamente.");
			log.info(getRespuesta());
			lstResultados.addAll(servicioCarga.buscarRegistrosGuardados(getInOrigen(), 0, "", arlFechas2));
			lstArchivos.clear();			
		} catch (DataAccessException e) {
			setRespuesta("Hubo un error al guardar los datos.");
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		} catch (Exception e) {
			e.printStackTrace();
			setRespuesta("No se puede realizar el proceso de carga." + e.getLocalizedMessage());
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}

	private String cargarCuentasDesempleo() {
		return NavigationResults.SUCCESS;
	}
	
	private boolean cargaVentasDesempleoTDC(String strOrigen, int consecutivo) {
		List<VtaPrecarga> lista = null;
		//lstResultados = null;
		
		try {
			lista = getDesempleoTDC(strOrigen, consecutivo);
			servicioCarga.guardarObjetos(lista);
			lstResultados = servicioCarga.getPreEmisionDesempleo(getFechaIni(), getFechaFin());			
			return true;
		} catch (Exception e) {
			log.error("Error al guardar lista de objetos vta precar.", e);
			return false;
		} finally {
			if(lista != null) {
				lista.clear();
				lista = null;
			}
		}
	}
	
	private String actualizarDatosHPU() {
		List<Object> lstObjetosGuardar = null;
		String strForward = NavigationResults.SUCCESS;
		
		try {
			lstResultados = new ArrayList<Object>();
			lstObjetosGuardar = new ArrayList<Object>();
			
			lstObjetosGuardar = leerArchivoHPUWF();
			servicioCarga.actualizarObjetos(lstObjetosGuardar);
			lstResultados.add(new Resultado(lstObjetosGuardar.size() + " - " + Constantes.REGISTROS_ACTUALIZADOS, "C"));
			lstObjetosGuardar.clear();
			
			setRespuesta("El proceso de carga finaliz� correctamente.");
			log.info(getRespuesta());
			getArchivo().delete();
		} catch (DataAccessException e) {
			setRespuesta("Hubo un error al guardar los datos.");
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		} catch (Exception e) {
			setRespuesta("No se puede realizar el proceso de carga." + e.getLocalizedMessage());
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}

	/******************************************APV*****************************************************/
	private String actualizarEstatusPampa() {
		List<Object> lstObjetosGuardarPampa = null;
		String strForward = NavigationResults.SUCCESS;
		
		try {
			lstResultados = new ArrayList<Object>();
			lstObjetosGuardarPampa = leerArchivoPampa();// lee archivo de texto 
			servicioCarga.estatusVta();// llama a servicio donde regresa el query con los estatus 2 de Vta_Precarga 
			
			if(lstObjetosGuardarPampa.isEmpty()){
				lstResultados.add(new Resultado("No se encontraron datos para actualizar.", "C")); 
			}
			setRespuesta("El proceso de carga finaliz� correctamente.");
			log.info(getRespuesta());
			getArchivo().delete();
		} catch (DataAccessException e) {
			setRespuesta("Hubo un error al guardar los datos.");
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		} catch (Exception e) {
			setRespuesta("No se puede realizar el proceso de carga." + e.getLocalizedMessage());
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}
	/*********************************************************APV*************************************/
	
	private List<Object> leerArchivoConciliacion(ArrayList<Resultado> arlFechas, int nRegConsecutivo, int nRamo, String strOrigen, String strDelimitador) throws Exception {
		CsvReader csv = null;
		FileReader fr;
		VtaPrecarga objPrecarga;
		List<Object> lista = null;
		Date newFechaIni, newFechaFin;
	
		try {
			lista = new ArrayList<Object>();
			hsmEstados = servicioCarga.getEstados();
			hsmPlazos  = servicioCarga.getPlazos();
			newFechaIni = GestorFechas.sumarRestasDiasAFecha(getFechaIni(), 1, Constantes.FECHA_RESTA_DIAS);
			newFechaFin = GestorFechas.sumarRestasDiasAFecha(getFechaFin(), 1, Constantes.FECHA_SUMA_DIAS);
			
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			
			//Tienen encabazados los archivos
			if(strOrigen.equals(Constantes.CONCILIACION_HIPOTECARIO) || strOrigen.equals(Constantes.CONCILIACION_LCI) || strOrigen.equals(Constantes.CONCILIACION_GAP) || strOrigen.equals(Constantes.CONCILIACION_BT)){ 
				csv.readRecord();
			}
			
			while(csv.readRecord()) {
				objPrecarga = getRegistro(csv.getRawRecord(), strDelimitador, strOrigen, nRamo, newFechaIni, newFechaFin, arlFechas, nRegConsecutivo);
				
				if(objPrecarga != null) {
					if(objPrecarga.getNdato4() != null && objPrecarga.getNdato4().equals(Constantes.FLAG_REGISTRO_CORRECTO)){
						objPrecarga.setNdato4(null);
						lista.add(objPrecarga);
						nRegConsecutivo++;
					} 
					/*else if(objPrecarga.getNdato4().equals(Constantes.FLAG_REGISTRO_FUERA_RANGO)){
						lista = null;
						break;
					}*/
				}
			}
			
			csv.close();
			fr.close();
			getArchivo().delete();
			hsmEstados = null;
			hsmPlazos = null;
		} catch (IOException e) {
			log.error("Error de lectura en el archivo", e);
			FacesUtils.addErrorMessage("Error de lectura en el archivo");
		} catch (Exception e) {
			log.error("No se puede cargar la informacion del archivo.", e);
			FacesUtils.addErrorMessage("No se puede cargar la informacion del archivo.");
		}
		
		return lista;
	}

	private ArrayList<ArrayList<Object>> leerArchivoCuentas(ArrayList<Resultado> arlFechas, int nRegConsecutivo) throws Excepciones {
		ArrayList<ArrayList<Object>> arlDatos = null;
		Resultado objResultado;
		ArrayList<Object> lstGuardar = null;
		ArrayList<Object> lstResultados = null;
		Date newFechaIni, newFechaFin;
		Iterator<Archivo> it;
		Archivo objArchivo;
		boolean existeArchivo = false;
		String filtro;
		Integer nOperCargo, nOperAbono;
		Parametros objParam;
		
		
		try {
			arlDatos = new ArrayList<ArrayList<Object>>();
			lstGuardar = new ArrayList<Object>();
			lstResultados = new ArrayList<Object>();
			
			
			newFechaIni = GestorFechas.sumarRestasDiasAFecha(getFechaIni(), 1, Constantes.FECHA_RESTA_DIAS);
			newFechaFin = GestorFechas.sumarRestasDiasAFecha(getFechaFin(), 1, Constantes.FECHA_SUMA_DIAS);
			
			filtro = " and P.copaIdParametro = 'MOVIMIENTO' \n"
					+ "and P.copaDesParametro = 'CONTABILIDAD' \n"
	                + "and P.copaVvalor1 = 'DEVO_BANCO' \n";
			nOperCargo = ((Parametros)servicioParametros.obtenerObjetos(filtro).get(0)).getCopaNvalor1();
			
			filtro = " and P.copaIdParametro = 'MOVIMIENTO' \n"
					+ "and P.copaDesParametro = 'CONTABILIDAD' \n"
	                + "and P.copaVvalor1 = 'PMA_DEPOSITO' \n";
			nOperAbono = ((Parametros)servicioParametros.obtenerObjetos(filtro).get(0)).getCopaNvalor1();			

					
			objResultado = arlFechas.get(0);
			it = lstArchivos.iterator();
			while(it.hasNext()){
				objArchivo = it.next();
				if(!objResultado.getCuenta().equals(objArchivo.getNombreArchivo().split("_")[0])) {
					continue;
				}
				
				filtro = " and P.copaIdParametro = 'CUENTAS' \n"
						+ "and P.copaDesParametro = 'CONCILIACION' \n"
		                + "and P.copaVvalor3 = 'CARGA_AUT' \n"
		                + "and P.copaVvalor1 = '" + objResultado.getCuenta() + "' \n";
				objParam = (Parametros)servicioParametros.obtenerObjetos(filtro).get(0);
				
				existeArchivo = true;
				procesaArchivo(objArchivo.getArchivo(), newFechaIni, newFechaFin, arlFechas, nRegConsecutivo, objParam, nOperCargo, nOperAbono, lstGuardar);
				objArchivo.getArchivo().delete();
			}
			
			if(!existeArchivo){
				objResultado.setDescripcion("No se cargo Archivo para esta cuenta.");
				objResultado.setFecha(null);
				lstResultados.add(objResultado);
			} 
			
			arlDatos.add(lstGuardar);
			arlDatos.add(lstResultados);
		} catch (Excepciones e) {
			log.error("No se puede cargar la informacion del archivo.", e);
			FacesUtils.addErrorMessage("No se puede cargar la informacion del archivo.");
		} 
		
		return arlDatos;
	}
	
	


	private List<Object> leerArchivoHPUWF() {
		CsvReader csv = null;
		FileReader fr;
		VtaPrecarga objPrecarga = null;
		List<Object> lista = null;
		String[] arrDatos;
		String strLinea;
		int nNoEcontrado = 0, nConseError = 0;
		
		try {
			lista = new ArrayList<Object>();
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			
			csv.readRecord();
			while(csv.readRecord()) {
				strLinea = csv.getRawRecord();
				arrDatos = strLinea.split("\\,");
				
				objPrecarga = servicioCarga.buscarVentaHPU(arrDatos[0]);
				
				if(arrDatos.length == 18 || arrDatos.length == 20){
					if(objPrecarga != null) {
						setDatos(objPrecarga, arrDatos);
						lista.add(objPrecarga);
					} else {
						nNoEcontrado++;
					}
				} else {
					if(objPrecarga != null) { 
						objPrecarga.setCargada(Constantes.CARGADA_ERROR);
						objPrecarga.setCdError(Constantes.ERROR_008_ID);
						objPrecarga.setDesError(Constantes.ERROR_008_DESC);
						lista.add(objPrecarga);
						
						nConseError = servicioCarga.guardarErroresTraspaso(objPrecarga.getTipoVenta(), objPrecarga.getId().getIdCertificado(), objPrecarga.getFeIngreso(), objPrecarga.getSumaAsegurada(), objPrecarga.getVdato6(), Constantes.CODIGO_ERROR_CARGA, objPrecarga.getDesError(), nConseError);
						nConseError++;
					} else {
						nNoEcontrado++;
					}
				}
				
			}
			
			if(nNoEcontrado != 0){
				lstResultados.add(new Resultado(nNoEcontrado + " - " + Constantes.REGISTROS_NO_VENTAS_HPU, "C")); 
			}
			csv.close();
			fr.close();
			getArchivo().delete();
		} catch (IOException e) {
			log.error("Error de lectura en el archivo", e);
			FacesUtils.addErrorMessage("Error de lectura en el archivo");
		} catch (Exception e) {
			log.error("No se puede cargar la informacion del archivo.", e);
			FacesUtils.addErrorMessage("No se puede cargar la informacion del archivo.");
		}
		
		return lista;
	}
	
	/***********************************************APV******************************************/
	private List<Object> leerArchivoPampa() {
		CsvReader csv = null;
		FileReader fr;
		List<Object> lista = null;
		String[] arrDatos;
		String strLinea;
		int cobrado=0;
		int noCobrado=0;
		
		try {
			lista = new ArrayList<Object>();
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			
			while(csv.readRecord()) {
				strLinea = csv.getRawRecord();
				arrDatos = new String[2];
				arrDatos[0]= strLinea.substring(0,15+1);				//Guarda en el primer arreglo los 16 digitos de tarjeta
				arrDatos[1]= strLinea.substring(16, strLinea.length());	// guarda el no cobrado o cobrado
				servicioCarga.buscarCuentaPampa(arrDatos);				//consulta a base por cada registro
				
				if(arrDatos[1].equals("COBRADO")){
					cobrado++;
				} else {
					noCobrado++;
				}
				lista.add(arrDatos);
			}
			
			if(cobrado != 0 || noCobrado != 0){
				lstResultados.add(new Resultado(new Date(), "COBRADOS: "+ cobrado +" Registros actualizados","C"));
				lstResultados.add(new Resultado(new Date(), "NO COBRADOS: "+ noCobrado+" Registros actualizados","C"));
			}
			csv.close();
			fr.close();
			getArchivo().delete();
		} catch (IOException e) {
			log.error("Error de lectura en el archivo", e);
			FacesUtils.addErrorMessage("Error de lectura en el archivo");
		} catch (Exception e) {
			log.error("No se puede cargar la informacion del archivo.", e);
			FacesUtils.addErrorMessage("No se puede cargar la informacion del archivo.");
		}
		
		return lista;
	}
	/*************************************************APV******************************************/
	
	private void setDatos(VtaPrecarga objPrecarga, String[] arrDatos) throws Exception {
		String[] arrNombre;
		
		try {
			arrNombre = arrDatos[11].split("/");
			objPrecarga.setCuentaAlterna(arrDatos[1]);	
			objPrecarga.setPregunta1(arrDatos[2]);
			objPrecarga.setVdato10(arrDatos[3]);
			objPrecarga.setSexoBen(arrDatos[4]);
			objPrecarga.setRelacionBen(arrDatos[5]);
			objPrecarga.setOcupacionAseg(arrDatos[6]);
			objPrecarga.setRelacionAseg(arrDatos[7]);
			objPrecarga.setUsuario(arrDatos[8]);			
			objPrecarga.setPregunta2(arrDatos[9]);
			objPrecarga.setNuContrato(arrDatos[10]);
			if(!objPrecarga.getNuContrato().equals("0")) {
				objPrecarga.setApPaternoAseg(arrNombre[0].equals("") ? "X" : arrNombre[0].trim());
				objPrecarga.setApMaternoAseg(arrNombre[1].equals("") ? "X" : arrNombre[1].trim());
				objPrecarga.setNombreAseg(arrNombre[2].equals("") ? "X" : arrNombre[2].trim());
				objPrecarga.setRfcAseg(arrDatos[12]);
				objPrecarga.setFeNacAseg(arrDatos[13]);
				objPrecarga.setSexoAseg(arrDatos[14].trim().startsWith("F") ? "FE" : "MA" );
			}
			objPrecarga.setPregunta3(arrDatos[15]);
			objPrecarga.setNdato8(new Double(arrDatos[18]) * 10);
			objPrecarga.setVdato11(arrDatos.length == 20 ? "OCUPACION: " + arrDatos[17].toUpperCase() 
					                                     + "|OBLIGADO CON INGRESOS: " + arrDatos[16].toUpperCase() : "");
			objPrecarga.setCargada(Constantes.CARGADA_PARA_CONCILIAR);
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.setDatos:: " + e.getMessage());
		}
		
	}

	private VtaPrecarga getRegistro(String strLinea, String strDelimitador, String strOrigen, int ramo, Date newFechaIni, Date newFechaFin, ArrayList<Resultado> arlFechas, int nRegConsecutivo) throws Exception{
		VtaPrecarga objPrecarga = null;
		String[] arrDatos;
		
		try {
			arrDatos = strLinea.split("\\" + strDelimitador);
			if(strOrigen.equals(Constantes.CONCILIACION_ALTAMIRA)){		
				objPrecarga = getRegistroAltamira(strLinea, strOrigen, ramo, newFechaIni, newFechaFin, arrDatos, arlFechas, nRegConsecutivo);				
			} else if(strOrigen.equals(Constantes.CONCILIACION_ALTAIR)){
				objPrecarga = getRegistroAltair(strLinea, strOrigen, ramo, newFechaIni, newFechaFin, arrDatos, arlFechas, nRegConsecutivo);
			} else if(strOrigen.equals(Constantes.CONCILIACION_HIPOTECARIO)){
				arrDatos = strLinea.replaceAll("\"", "").split("\\" + strDelimitador);
				objPrecarga = getRegistro(strLinea, strOrigen, ramo, newFechaIni, newFechaFin, arrDatos, arlFechas, nRegConsecutivo);
			} else if(strOrigen.equals(Constantes.CONCILIACION_LCI)){
				if(arrDatos.length > 29) {
					objPrecarga = new VtaPrecarga(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + String.valueOf(Constantes.CONCILIACION_BANDERA_DESFASADO), arrDatos[0], strOrigen), strLinea, new Date());
					objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CORRECTO);
				} else {
					objPrecarga = getRegistro(strLinea, strOrigen, ramo, newFechaIni, newFechaFin, arrDatos, arlFechas, nRegConsecutivo);
				}
			} else if(strOrigen.equals(Constantes.CONCILIACION_GAP)){
				arrDatos = strLinea.replaceAll("\"", "").split("\\" + strDelimitador);
				objPrecarga = getRegistro(strLinea, strOrigen, ramo, newFechaIni, newFechaFin, arrDatos, arlFechas, nRegConsecutivo);
			} else if(strOrigen.equals(Constantes.CONCILIACION_BT )){
				objPrecarga = getRegistro(strLinea, strOrigen, ramo, newFechaIni, newFechaFin, arrDatos, arlFechas, nRegConsecutivo);
			}
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.getRegistro:: " + e.getMessage());
		}
		
		return objPrecarga;
	}

	

	

	private VtaPrecarga getRegistro(String strLinea, String strOrigen, int ramo, Date newFechaIni, Date newFechaFin, String[] arrDatos, ArrayList<Resultado> arlFechas, int nRegConsecutivo) throws Exception{
		VtaPrecarga objPrecarga = null;
		Date fechaIngreso;
		
		try {
			objPrecarga = new VtaPrecarga(arrDatos, strLinea, strOrigen, ramo, nRegConsecutivo);
			
			fechaIngreso = GestorFechas.generateDate(objPrecarga.getFeIngreso(), "dd/MM/yyyy");
			if(fechaIngreso.after(newFechaIni) && fechaIngreso.before(newFechaFin)){
				if(GestorFechas.buscaFecha(fechaIngreso, arlFechas)){
//					objPrecarga.setNdato8(calculaTarifa(new Double(objPrecarga.getPrima()), new Double(objPrecarga.getSumaAsegurada())));
//					objPrecarga.setCdEstado(getCodigoEstado(objPrecarga.getVdato7()));							
//					getPlazo(objPrecarga);
					
					objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CORRECTO);
				} else {
					objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CARGADO);
				}
			} else {
				objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_FUERA_RANGO);
			}
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.getRegistro:: " + e.getMessage());
		} 
		
		return objPrecarga;
	}
	
	private VtaPrecarga getRegistroAltamira(String strLinea, String strOrigen, int ramo, Date newFechaIni, Date newFechaFin, String[] arrDatos, ArrayList<Resultado> arlFechas, int nRegConsecutivo) throws Exception{
		VtaPrecarga objPrecarga = null;
		Date fechaIngreso;
		int nConseError = 0;
		
		try {
			if(strLinea.length() == 317) {
				if(arrDatos.length > 20){
					objPrecarga = new VtaPrecarga(strLinea, strOrigen, ramo, nRegConsecutivo);
				} else {
					objPrecarga = new VtaPrecarga(arrDatos, strLinea, strOrigen, ramo, nRegConsecutivo);
				}
				
				fechaIngreso = GestorFechas.generateDate(objPrecarga.getFeIngreso(), "dd/MM/yyyy");
				if(fechaIngreso.after(newFechaIni) && fechaIngreso.before(newFechaFin)){
					if(GestorFechas.buscaFecha(fechaIngreso, arlFechas)){
						if(objPrecarga.getNdato1() != 1){
							objPrecarga.setCargada(Constantes.CARGADA_ERROR);
							objPrecarga.setCdError(Constantes.ERROR_002_ID);
							objPrecarga.setDesError(Constantes.ERROR_002_DESC);
							objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_BAJA);
							
							nConseError = servicioCarga.guardarErroresTraspaso(objPrecarga.getTipoVenta(), objPrecarga.getId().getIdCertificado(), objPrecarga.getFeIngreso(), objPrecarga.getSumaAsegurada(), objPrecarga.getVdato6(), Constantes.CODIGO_ERROR_CARGA, objPrecarga.getDesError(), nConseError);
							nConseError++;
							
						} else {
							objPrecarga.setNdato8(calculaTarifa(new Double(objPrecarga.getPrima()), new Double(objPrecarga.getSumaAsegurada())));
							objPrecarga.setCdEstado(getCodigoEstado(objPrecarga.getVdato7()));							
							getPlazo(objPrecarga);
							objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CORRECTO);
						}
					} else {
						objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CARGADO);
					}
				} else {
					objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_FUERA_RANGO);
				}
			} else {
				fechaIngreso = GestorFechas.generateDate(strLinea.substring(104, 115).contains("?") ? strLinea.substring(117, 127) : strLinea.substring(116, 126), "dd-MM-yyyy");
				if(GestorFechas.buscaFecha(fechaIngreso, arlFechas)){
					objPrecarga = new VtaPrecarga(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + String.valueOf(Constantes.CONCILIACION_BANDERA_DESFASADO), strLinea.substring(104, 115).contains("?") ? strLinea.substring(105, 116) : strLinea.substring(104, 115), strOrigen), strLinea, fechaIngreso);
					objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CORRECTO);
				}
			}	
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.getRegistroAltamira:: " + e.getMessage());
		} 
		
		return objPrecarga;
	}
	
	private VtaPrecarga getRegistroAltair(String strLinea, String strOrigen, int ramo, Date newFechaIni, Date newFechaFin, String[] arrDatos, ArrayList<Resultado> arlFechas, int nRegConsecutivo) throws Exception{
		VtaPrecarga objPrecarga = null;
		Date fechaIngreso;
		
		try {
			if(strLinea.length() == 397) {
				objPrecarga = new VtaPrecarga(arrDatos, strLinea, strOrigen, ramo, nRegConsecutivo);
				
				fechaIngreso = GestorFechas.generateDate(objPrecarga.getFeIngreso(), "dd/MM/yyyy");
//				if(fechaIngreso.after(newFechaIni) && fechaIngreso.before(newFechaFin)){
//					if(GestorFechas.buscaFecha(fechaIngreso, arlFechas)){
						if(objPrecarga.getVdato2().equals("0") || objPrecarga.getVdato2().equals("1") || objPrecarga.getVdato2().equals("4")){
							objPrecarga.setFeEstatus(GestorFechas.formatDate(objPrecarga.getDdato1(), "dd/MM/yyyy"));
							
							objPrecarga.setNdato8(calculaTarifa(new Double(objPrecarga.getPrima()), new Double(objPrecarga.getSumaAsegurada())));
							objPrecarga.setCdEstado(getCodigoEstado(objPrecarga.getVdato7()));							
							getPlazo(objPrecarga);
						} else if(objPrecarga.getVdato2().equals("8") || objPrecarga.getVdato2().equals("9") || objPrecarga.getVdato2().equals("M")){
							objPrecarga.setFeEstatus(GestorFechas.formatDate(objPrecarga.getDdato2(), "dd/MM/yyyy"));
							objPrecarga.setPrima(String.valueOf(new Double(objPrecarga.getPrima()) * -1));							
//							objPrecarga.setCargada(Constantes.CARGADA_ERROR);
//							objPrecarga.setCdError(Constantes.ERROR_002_ID);
//							objPrecarga.setDesError(Constantes.ERROR_002_DESC);
						}
						objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CORRECTO);					
//					} else {
//						objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CARGADO);
//					}
//				} else {
//					objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_FUERA_RANGO);
//				}
			} else {
				fechaIngreso = GestorFechas.generateDate(arrDatos[13], "yyyy-MM-dd");
				if(GestorFechas.buscaFecha(fechaIngreso, arlFechas)){
					objPrecarga = new VtaPrecarga(new VtaPrecargaId(ramo, String.valueOf(nRegConsecutivo) + String.valueOf(Constantes.CONCILIACION_BANDERA_DESFASADO), strLinea.substring(104, 115).contains("?") ? strLinea.substring(105, 116) : strLinea.substring(104, 115), strOrigen), strLinea, fechaIngreso);
					objPrecarga.setNdato4(Constantes.FLAG_REGISTRO_CORRECTO);
				}
			}	
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.getRegistroAltair:: " + e.getMessage());
		} 
		
		return objPrecarga;
	}
	
	private List<VtaPrecarga> getDesempleoTDC(String strOrigen, int nRegConsecutivo){
		CsvReader csv = null;
		FileReader fr;
		VtaPrecargaId id = null;
		VtaPrecarga preCarga = null;
		List<VtaPrecarga> lista = null;
		HashSet<VtaPrecarga> setPrecargas = null;
		StringBuilder query  = null;
		String[] headers = null;
		Map<String, String> mh;
		
		try {						
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			if(csv.readHeaders()) {
				headers = csv.getHeaders();
				mh = new HashMap<String, String>();
				
				for (String h: headers) {
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<VtaPrecarga>();
				setPrecargas = new HashSet<VtaPrecarga>();
				query  =  new StringBuilder();
				
				while(csv.readRecord()) {
					try {
						preCarga = new VtaPrecarga();
						id = new VtaPrecargaId();
						id.setCdSucursal(1);
						id.setCdRamo(Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO);
						id.setNumPoliza(nRegConsecutivo);
						id.setSistemaOrigen(strOrigen);
						if(csv.get(mh.get("identificador")) != null && csv.get(mh.get("identificador")).length() > 0) {
							id.setIdCertificado(csv.get(mh.get("identificador")));
						}
						preCarga.setCargada("0");
						preCarga.setEstatus("0");
						preCarga.setFeCarga(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
						preCarga.setFeEstatus(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
						if(csv.get(mh.get("sucursal")) != null && csv.get(mh.get("sucursal")).length() > 0) {
							preCarga.setCazbCdSucursal(csv.get(mh.get("sucursal")));
						}
						preCarga.setId(id);
						
						if(csv.get(mh.get("fechaingreso")) != null && csv.get(mh.get("fechaingreso")).length() > 0) {
							preCarga.setFeIngreso(csv.get(mh.get("fechaingreso")));
						}
						if(csv.get(mh.get("tipocliente")) != null && csv.get(mh.get("tipocliente")).length() > 0) {
							preCarga.setAnalista(csv.get(mh.get("tipocliente")));
						}
						if(csv.get(mh.get("prima")) != null && csv.get(mh.get("prima")).length() > 0) {
							preCarga.setPrima(csv.get(mh.get("prima")));
						}
						if(csv.get(mh.get("sumaasegurada")) != null && csv.get(mh.get("sumaasegurada")).length() > 0 ) {
							preCarga.setSumaAsegurada(csv.get(mh.get("sumaasegurada")));
						}
						if(csv.get(mh.get("numerocliente")) != null && csv.get(mh.get("numerocliente")).length() > 0) {
							preCarga.setNumCliente(csv.get(mh.get("numerocliente")));
						}
						if(csv.get(mh.get("apellidopaterno")) != null && csv.get(mh.get("apellidopaterno")).length() > 0) {
							preCarga.setApPaterno(csv.get(mh.get("apellidopaterno")));
						}
						if(csv.get(mh.get("apellidomaterno"))!= null && csv.get(mh.get("apellidomaterno")).length() > 0) {
							preCarga.setApMaterno(csv.get(mh.get("apellidomaterno")));
						}
						if(csv.get(mh.get("nombre")) != null && csv.get(mh.get("nombre")).length() > 0) {
							preCarga.setNombre(csv.get(mh.get("nombre")));
						}
						if(csv.get(mh.get("rfc")) != null && csv.get(mh.get("rfc")).length() > 0) {
							preCarga.setRfc(csv.get(mh.get("rfc")));
						}
						if(csv.get(mh.get("fechanacimiento")) != null && csv.get(mh.get("fechanacimiento")).length() > 0) {
							preCarga.setFeNac(csv.get(mh.get("fechanacimiento")));
						}
						if(csv.get(mh.get("sexo")) != null && csv.get(mh.get("sexo")).length() > 0) {
							preCarga.setSexo(csv.get(mh.get("sexo")));
						}
						if(csv.get(mh.get("cuenta")) != null && csv.get(mh.get("cuenta")).length() > 0) {
							preCarga.setCuenta(csv.get(mh.get("cuenta")));
						}
						if(csv.get(mh.get("calle")) != null && csv.get(mh.get("calle")).length() > 0) {
							preCarga.setCalle(csv.get(mh.get("calle")));
						}
						if(csv.get(mh.get("colonia")) != null && csv.get(mh.get("colonia")).length() > 0) {
							preCarga.setColonia(csv.get(mh.get("colonia")));
						}
						if(csv.get(mh.get("delgmun")) != null && csv.get(mh.get("delgmun")).length() > 0) {
							preCarga.setDelmunic(csv.get(mh.get("delgmun")));
						}
						if(csv.get(mh.get("estado")) != null && csv.get(mh.get("estado")).length() > 0) {
							preCarga.setCdEstado(csv.get(mh.get("estado")));
						}
						if(csv.get(mh.get("codigopostal")) != null && csv.get(mh.get("codigopostal")).length() > 0) {
							preCarga.setCp(Utilerias.validaCP(csv.get(mh.get("codigopostal"))));
						}
						if(csv.get(mh.get("contrato")) != null && csv.get(mh.get("contrato")).length() > 0) {
							preCarga.setNuContrato(csv.get(mh.get("contrato")));
						}
						if(csv.get(mh.get("tpmovimiento")) != null && csv.get(mh.get("tpmovimiento")).length() > 0) {
							preCarga.setNdato1(Short.parseShort(csv.get(mh.get("tpmovimiento"))));
						}
						if(csv.get(mh.get("estadocobro")) != null && csv.get(mh.get("estadocobro")).length() > 0) {
							preCarga.setVdato1(csv.get(mh.get("estadocobro")));
						}						
						if(csv.get(mh.get("plastico")) != null && csv.get(mh.get("plastico")).length() > 0) {
							preCarga.setVdato3(csv.get(mh.get("plastico")));
						}
						if(csv.get(mh.get("tarifa")) != null && csv.get(mh.get("tarifa")).length() > 0) {
							preCarga.setVdato5(csv.get(mh.get("tarifa")));
						}
						if(csv.get(mh.get("saldocorte")) != null && csv.get(mh.get("saldocorte")).length() > 0) {
							preCarga.setVdato6(csv.get(mh.get("saldocorte")));
						}
						if(csv.get(mh.get("causanocobro"))  != null && csv.get(mh.get("causanocobro")).length() > 0) {
							preCarga.setVdato7(csv.get(mh.get("causanocobro")));
						}
						if(csv.get(mh.get("fechacorte")) != null && csv.get(mh.get("fechacorte")).length() > 0) {
							preCarga.setDdato1(new SimpleDateFormat("dd/MM/yyyy").parse(csv.get(mh.get("fechacorte"))));
						}
						if(csv.get(mh.get("correoelectronico")) != null && csv.get(mh.get("correoelectronico")).length() > 0) {
							preCarga.setVdato8(csv.get(mh.get("correoelectronico")));
						}						
						
						//Checamos si existe en tabla temporal vta_precarga
						query.append("select VP \n");
						query.append("from VtaPrecarga VP \n");
						query.append("where VP.id.idCertificado ='").append(preCarga.getId().getIdCertificado()).append("'\n");
						List<Object> listaDes = servicioCarga.consultarMapeo(query.toString()); 
						Utilerias.resetMessage(query);
						if(listaDes != null && listaDes.size() > 0) {
							VtaPrecarga vtaPrecargaUpdate = (VtaPrecarga) listaDes.get(0);
							servicioCarga.borrarObjeto(vtaPrecargaUpdate);
						}
						setPrecargas.add(preCarga);
						nRegConsecutivo++;
					} catch (Exception e) {
						log.debug("Error al crear la precarga.");						
					}				
				}//end while csv.record
			}//end if headers
		
			//pasar set a lista
			for(Object o: setPrecargas.toArray()){
				lista.add((VtaPrecarga)o);
			}
		} catch (Exception e) {
			this.log.error("Error al crear objetos precarga", e);
			FacesUtils.addErrorMessage("No se pudo cargar la informacion.");
		} finally{
			setPrecargas.clear();
			setPrecargas = null;
		}
		
		return lista;		
	}
	
	private Double calculaTarifa(Double prima, Double sumaAsegurada) throws Exception{
		try {
			if(sumaAsegurada > 0) {
				return (prima * 1000) / sumaAsegurada;
			} else {
				return 0D;
			}
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.calculaTarifa:: " + e.getMessage());
		}
	}

	private void getPlazo(VtaPrecarga objPrecarga) throws Exception{
		
		try {
			if(hsmPlazos.containsKey(objPrecarga.getRemesa())){
				objPrecarga.setVdato5(hsmPlazos.get(objPrecarga.getRemesa()));
			} else {
				objPrecarga.setCargada(Constantes.CARGADA_ERROR);
				objPrecarga.setCdError(Constantes.ERROR_003_ID);
				objPrecarga.setDesError(Constantes.ERROR_003_DESC);
			}
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.getPlazo:: " + e.getMessage());
		}
		
	}

	private String getCodigoEstado(String strDescEstado) throws Exception{
		String strCodigoEstado = "0";
		String strNombre;
		
		try {
			strNombre = Utilerias.getDescEstado(strDescEstado);
			if(hsmEstados.containsKey(strNombre)){
				strCodigoEstado = hsmEstados.get(strNombre);
			}
			
		} catch (Exception e) {
			throw new Exception("Error:: BeanListaCargaConcilia.getCodigoEstado:: " + e.getMessage());
		}
		
		return strCodigoEstado;
	}
	
	public String limpiaResultados(){
		if(lstResultados != null){
			lstResultados.clear();		
		} 
		
		return NavigationResults.SUCCESS;
	}
	
	private boolean validarDatos() throws Exception{
		boolean blnResultado = true;
		boolean blnValidaFecha = true;
		boolean validaDesempleoTDc = false;
		int rangoFechas = ((Parametros) servicioCarga.getParametrosConciliacion(Constantes.CONCILIA_PARAMETRO_RANGO_FECHAS).get(0)).getCopaNvalor1();
		
		
		if (getInOrigen() == null){			
			setRespuesta("El origen y fechas no son correctos.");
			blnResultado = false;
		} else if((getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR) || getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR) || getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR) || getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_LCI_VALOR) || getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_BT_VALOR))) {
			if(getArchivo() == null) {
				setRespuesta("Favor de cargar un archivo.");
				blnResultado = false;
			}

			if(getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR)) {
				if(getnTipoArchivoHPU() == 0) {
					setRespuesta("Favor de seleccionar un tipo de archivo a cargar.");
					blnResultado = false;
				} else if (getnTipoArchivoHPU() == 2) {
					blnValidaFecha = false;
				}
			}
			
			if(getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_LCI_VALOR) && getnTipoArchivo() == 0) {
				setRespuesta("Favor de seleccionar un tipo de archivo a cargar.");
				blnResultado = false;
			}
			
			if(getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_BT_VALOR)) { /******APV******/
				if(getnTipoArchivoBT() == 0) {
					setRespuesta("Favor de seleccionar un tipo de archivo a cargar.");
					blnResultado = false;
				} else if (getnTipoArchivoBT() == 2) {
					blnValidaFecha = false;
				}
			} /********APV******/
			
		} else if(getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR) && (getLstArchivos().isEmpty() || getCuentas().length == 0)){
			setRespuesta("Favor de cargar un archivo y seleccionar una cuenta.");
			blnResultado = false;
		} else if(getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR)){
			validaDesempleoTDc = esDesempleoTDC();
		}
		
		if(blnValidaFecha){
			if(getFechaIni() == null && getFechaFin() == null){
				setRespuesta("Favor de seleccionar periodo de fechas.");
				blnResultado = false;
			}
			
			if(getFechaIni() != null  && getFechaFin() == null){
				setRespuesta("Favor de ingresar fecha fin.");
				blnResultado = false;
			} 
			
			if(getFechaIni() == null && getFechaFin() != null){
				setRespuesta("Favor de ingresar fecha inicio.");
				blnResultado = false;
			}
			
			if(getFechaIni() != null && getFechaFin() != null) {
				if(getFechaIni().after(getFechaFin())){
					setRespuesta("Fecha Inicio no debe ser mayor a Fecha Fin.");
					blnResultado = false;
				} else if(!validaDesempleoTDc && !(getInOrigen().equals(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR)) && GestorFechas.restarFechas(getFechaIni(), getFechaFin()) >= rangoFechas){
					setRespuesta("El rango entre las fechas no debe ser mayor a " + rangoFechas + " dias.");
					blnResultado = false;
				}
			}
		}
		
		return blnResultado;
	}

	private boolean esDesempleoTDC() {
		String filtro = "";
		List<Object> listaCuentasDesTDC = null;
		StringBuilder queryDesempleo = null;
		
		try {
			queryDesempleo = new StringBuilder();
			if(getCuentas() != null && getCuentas().length > 0) {
				for(String cuenta: getCuentas()) {
					filtro = filtro + String.format("'%s',", cuenta);
				}
				
				if(filtro != null && filtro.length() > 0) {
					filtro = filtro.substring(0, filtro.length() -1);
				}
			}
			
			queryDesempleo.append("  and P.copaDesParametro = 'CONCILIACION' \n");
			queryDesempleo.append("  and P.copaIdParametro = 'CUENTAS' \n");
			queryDesempleo.append("  and P.copaVvalor1 in (").append(filtro).append(") \n");
			queryDesempleo.append("  and P.copaNvalor1 = 1 \n");
			listaCuentasDesTDC = servicioParametros.obtenerObjetos(queryDesempleo.toString());
			
			if(listaCuentasDesTDC != null && listaCuentasDesTDC.size() > 0) {
				return true;
			}
		} catch (Exception e) {
			log.error("Error al validar cuenta.", e);
		} finally{
			listaCuentasDesTDC.clear();
			listaCuentasDesTDC = null;
		}
		
		return false;
	}
	
	
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	public Integer getInOrigen() {
		return inOrigen;
	}
	public void setInOrigen(Integer inOrigen) {
		this.inOrigen = inOrigen;
	}
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	public File getArchivo() {
		return archivo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public List<Object> getListaComboOrigen() {
		listaComboOrigen.clear();
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR, Constantes.CONCILIACION_ORIGEN_ALTAMIRA_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR, Constantes.CONCILIACION_ORIGEN_ALTAIR_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_BT_VALOR, Constantes.CONCILIACION_ORIGEN_BT_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR, Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_DESC));
//		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_LCI_VALOR, Constantes.CONCILIACION_ORIGEN_LCI_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_GAP_VALOR, Constantes.CONCILIACION_ORIGEN_GAP_DESC));
//		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_LE_VALOR, Constantes.CONCILIACION_ORIGEN_LE_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR, Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_DESC));
		listaComboOrigen.add(new SelectItem(Constantes.CONCILIACION_ORIGEN_CUENTAS_VALOR, Constantes.CONCILIACION_ORIGEN_CUENTAS_DESC));
		
		return listaComboOrigen;
	}
	public void setListaComboOrigen(List<Object> listaComboOrigen) {
		this.listaComboOrigen = listaComboOrigen;
	}
	public void setMensaje(String mensaje) {
		this.respuesta = mensaje;
	}
	public String getMensaje() {
		return respuesta;
	}
	public Date getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public List<Object> getListaCuentas() throws Exception{
		listaCuentas.clear();
		listaCuentas = servicioCarga.getCuentas();
		nTotalArchivos = listaCuentas.size();
		return listaCuentas;
	}
	public void setListaCuentas(List<Object> listaCuentas) {
		this.listaCuentas = listaCuentas;
	}
	public String[] getCuentas() {
		return cuentas;
	}
	public void setCuentas(String[] cuentas) {
		this.cuentas = cuentas;
	}
	public Integer getnTotalArchivos() {
		return nTotalArchivos;
	}
	public void setnTotalArchivos(Integer nTotalArchivos) {
		this.nTotalArchivos = nTotalArchivos;
	}
	public ArrayList<Archivo> getLstArchivos() {
		return lstArchivos;
	}
	public void setLstArchivos(ArrayList<Archivo> lstArchivos) {
		this.lstArchivos = lstArchivos;
	}
	public List<Object> getLstResultados() {
		return lstResultados;
	}
	public void setLstResultados(List<Object> lstResultados) {
		this.lstResultados = lstResultados;
	}
	public Integer getnTipoArchivo() {
		return nTipoArchivo;
	}
	public void setnTipoArchivo(Integer nTipoArchivo) {
		this.nTipoArchivo = nTipoArchivo;
	}
	public Integer getnResultado() {
		return nResultado;
	}
	public void setnResultado(Integer nResultado) {
		this.nResultado = nResultado;
	}
	public Integer getnTipoArchivoHPU() {
		return nTipoArchivoHPU;
	}
	public void setnTipoArchivoHPU(Integer nTipoArchivoHPU) {
		this.nTipoArchivoHPU = nTipoArchivoHPU;
	}
	public Integer getnTipoArchivoBT() {
		return nTipoArchivoBT;
	}
	public void setnTipoArchivoBT(Integer nTipoArchivoBT) {
		this.nTipoArchivoBT = nTipoArchivoBT;
	}
}
