package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 * @author SSAF
 *
 */

@Controller
@Scope("session")

public class BeanConciliacion {

   @Resource
	private ServicioParametros servicioParametros;
	private String strRespuesta;
	private String strRespuesta2;
	private Log log = LogFactory.getLog(this.getClass());
	
	
	
	// Mapeo de parameteos 
	
	private BigDecimal copaNvalor6;  // Canal de Venta
    private int        copaNvalor8;  // Ramo
	private Date       copaFvalor1;  // Ultima  Carga de Archivo de Emision
	private Date       copaFvalor2;  // Ultima  Carga de Archivo de Cancelacion
	private Date       copaFvalor3;  // Ultimo  Dia de Emision
	private Date       copaFvalor4;  // Ultimo  Dia de Cancelacion
	private Date       copaFvalor5;  // Ultimo  Dia de Carga de Workflow
	private String     copaRegistro1;
	private String     copaRegistro2;
	private String     copaRegistro3;
	private String     usuario1;
	private String     usuario2;
	private String     usuario3;
	
	private HtmlSelectOneMenu inRamo;
	private Integer inCanal;
	
	
	
	
	
	private List<Object>     listaConciliacion;
	private List<Object>     listaConciliacion2;
	//private List<Object>     listaRutas;
	private List<Parametros> listaParametros;

	private HashMap<String, Object> hmResultados;
	private HashMap<String, Object> hmResultados2;
	private int filaActual;
	private BeanParametros objActual = new BeanParametros();
	private BeanParametros objNuevo = new BeanParametros();
	private Set<Integer> keys = new HashSet<Integer>();
	private int 	numPagina;
	
	
   public BeanConciliacion (){
        
	    FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONCILIACION);
		this.listaConciliacion = new ArrayList<Object>();
		//this.listaConciliacion2 = new ArrayList<Object>();
		this.log.debug("BeanListaConciliacion   creado ");
		this.numPagina = 1;
	
	}
	
   
   //Metodos
   
   
   public void buscarFilaActual(ActionEvent event) {
       String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("canal"));
       filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
       Iterator<Object> it =  getListaConciliacion().iterator();

       while(it.hasNext()) {
       	BeanParametros objParametro = (BeanParametros) it.next();
       	if (objParametro.getCopaCdParametro().toString().equals(clave)){
               objActual = objParametro;
               break;
           }
       }
   }
   
   
	public String consultarConciliacion(){
		
		String filtro = "";
        List<Object> lstResultados;
        Parametros parametro;
        BeanParametros bean;
       
		
		try {
			hmResultados = new HashMap<String, Object>();
			
			filtro   = " and P.copaDesParametro = 'CONCILIACION' \n" +
                       " and P.copaIdParametro != 'CUENTAS'     \n" +
                       " and P.copaVvalor3 = 'CARGA_AUT' " ;
			
			
			
			
			
			lstResultados = this.servicioParametros.obtenerObjetos(filtro);
			listaConciliacion = new ArrayList<Object>();
		//	listaRutas = new ArrayList<Object>();
			Iterator<Object> it = lstResultados.iterator();
			while(it.hasNext()) {
				parametro = (Parametros) it.next();
                BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
				bean.setServicioParametros(this.servicioParametros);
				listaConciliacion.add(bean);
				hmResultados.put(bean.getCopaCdParametro().toString(), bean);
				

		
			}

			setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar las Conciliaciones.", e);
			e.printStackTrace();
			return NavigationResults.FAILURE;
		}
	}
	
 
	public String consultarRutas(){
		
		String filtro = "";
		List<Object> lstResultados;
		Parametros parametro1;
		BeanParametros bean;
		String id_param ="";
		long codigo = 0;
		String ruta="";
		String DscRuta ="";
        
		 
		try {
			
			id_param = objActual.getCopaIdParametro();
			System.out.println(id_param);
			
			hmResultados2 = new HashMap<String, Object>();
			
			filtro   =  " and P.copaDesParametro = 'CONCILIACION' \n" +
					    " and P.copaIdParametro = 'RUTAS' \n" +
					    " and P.copaVvalor3 LIKE'"+id_param+"%'" ;
			
            lstResultados = this.servicioParametros.obtenerObjetos(filtro);
			listaConciliacion2 = new ArrayList<Object>();
			Iterator<Object> it = lstResultados.iterator();
			 int cont =1;
			while(it.hasNext()) {
				parametro1 = (Parametros) it.next();
              
			             if (cont == 1){
							copaRegistro1 = parametro1.getCopaRegistro();
							usuario1      = parametro1.getCopaVvalor6(); 
							codigo        = parametro1.getCopaCdParametro();
							ruta          = parametro1.getCopaIdParametro();
							DscRuta       = parametro1.getCopaVvalor3();
							objActual.setCopaRegistro1(copaRegistro1);   
						    objActual.setUsuario1(usuario1);
						    objActual.setCopaCdParametro1(codigo);
						    objActual.setCopaIdParametro1(ruta);
						    objActual.setCopaVvalor31(DscRuta);
			                 }
			              
			             if (cont == 2){
			 				copaRegistro1 = parametro1.getCopaRegistro();
			 				usuario1      = parametro1.getCopaVvalor6(); 
			 				codigo        = parametro1.getCopaCdParametro();
			 				ruta          = parametro1.getCopaIdParametro();
							DscRuta       = parametro1.getCopaVvalor3();
			 			    objActual.setCopaRegistro2( copaRegistro1);   
			 			    objActual.setUsuario2(usuario1);
			 			    objActual.setCopaCdParametro2(codigo);
						    objActual.setCopaIdParametro2(ruta);
						    objActual.setCopaVvalor32(DscRuta);
			                }
			               
			             if (cont == 3){
			  				copaRegistro1 = parametro1.getCopaRegistro();
			  				usuario1      = parametro1.getCopaVvalor6(); 
			  				codigo        = parametro1.getCopaCdParametro();
			 				ruta          = parametro1.getCopaIdParametro();
							DscRuta       = parametro1.getCopaVvalor3();
                            objActual.setCopaRegistro3( copaRegistro1);   
			  			    objActual.setUsuario3(usuario1);
			 			    objActual.setCopaCdParametro3(codigo);
						    objActual.setCopaIdParametro3(ruta);
						    objActual.setCopaVvalor33(DscRuta);
                            }
                
              //  System.out.println(codigo);
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro1);
				bean.setServicioParametros(this.servicioParametros);
				
				listaConciliacion2.add(bean);
				hmResultados2.put(bean.getCopaCdParametro().toString(), bean);
				//System.out.println(hmResultados);
				cont = cont+1;
			}
       
			setStrRespuesta2("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar las Conciliaciones.", e);
			e.printStackTrace();
			return NavigationResults.FAILURE;
		}
	}
	
	
	
	
	
	
	
	
	public String altaConciliacion(){
		
		Long secConciliacion;
		Long secConciliacion1 = null;
		Long secConciliacion2 = null;
		Long secConciliacion3 = null;
		String filtro = "";
		List<Object> lstResultados;
		Parametros parametro4;
        String  valor ="";
		String  valor2 = "";
		
		Date date = Calendar.getInstance().getTime();
	     // String formato ="dd/mm/yyyy";
		
          List<Parametros> lstRutas = new ArrayList<Parametros>();
		 
		 
		try {
			//GestorFechas.buscaFecha(date, formato)
			secConciliacion = servicioParametros.siguente(); 
			

			valor =objNuevo.getCopaNvalor4().toString();
			
			System.out.println("valor de "+valor.toString());
			
			filtro  =  " and P.copaDesParametro = 'PRIMAUNICA' \n" +
				       " and P.copaIdParametro  = 'IDVENTA' \n" +
				       " and P.copaNvalor1      =  1 \n" +
				       " and P.copaNvalor4  = "+valor+"" ;
			
			lstResultados = this.servicioParametros.obtenerObjetos(filtro);
			Iterator<Object> it = lstResultados.iterator();
			while(it.hasNext()) {
				parametro4 = (Parametros) it.next();
			
			   valor2 = parametro4.getCopaVvalor4();

			}
			
			System.out.println("valor de "+valor2.toString());
			
			
			objNuevo.setCopaNvalor4( Integer.parseInt(valor));
            objNuevo.setCopaIdParametro(valor2);
			objNuevo.setCopaCdParametro(secConciliacion); 	// SIGUIENTE NUMERO 
            objNuevo.setCopaDesParametro("CONCILIACION"); 	// CATALOGO
            objNuevo.setCopaNvalor5(1);
            objNuevo.setCopaVvalor8(valor2);
            objNuevo.setCopaFvalor1(date);
			objNuevo.setCopaFvalor2(date);
			objNuevo.setCopaFvalor3(date);
			objNuevo.setCopaFvalor4(date);
			objNuevo.setCopaFvalor5(date);
            objNuevo.setCopaVvalor6("smb:|"+getObjNuevo().getCopaVvalor6()); //------ USUARIO|PASS 
			
				 
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
            lstRutas.add(parametro);
		
   //-------Ruta copia de Archivos
			Parametros parametro1 = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
			secConciliacion1 = secConciliacion +1; 
			parametro1.setCopaIdParametro(Constantes.CO_RUTA);
		    parametro1.setCopaRegistro(getObjNuevo().getCopaRegistro1());              //----------  Ruta
			parametro1.setCopaCdParametro(secConciliacion1);                           //----------  Secuencia
            parametro1.setCopaVvalor3(valor2+Constantes.CO_COPY);                      //----------  Tipo De Carga
            parametro1.setCopaVvalor6("smb:|"+getObjNuevo().getUsuario1());            //----------  Usuario
            
            lstRutas.add(parametro1);
		   
			

   //-------Ruta de reproceso de Archivos
			
			Parametros parametro2 = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
            secConciliacion2 = secConciliacion1 +1;
            parametro2.setCopaIdParametro(Constantes.CO_RUTA);
            parametro2.setCopaRegistro(getObjNuevo().getCopaRegistro2());               //------- Ruta
			parametro2.setCopaCdParametro(secConciliacion2);                            //------- Secuencia
			parametro2.setCopaVvalor3(valor2+Constantes.CO_REPRO);                      //------- Tipo de Carga
			parametro2.setCopaVvalor6("smb:|"+getObjNuevo().getUsuario2());             //------- Usuario
            
			lstRutas.add(parametro2);
              
   //-------Ruta de Errores de Archivos
			Parametros parametro3 = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
			secConciliacion3 = secConciliacion2 +1;
			parametro3.setCopaIdParametro(Constantes.CO_RUTA);
            parametro3.setCopaRegistro(getObjNuevo().getCopaRegistro3());                //--------- Ruta
			parametro3.setCopaCdParametro(secConciliacion3);                             //--------- Secuencia
			parametro3.setCopaVvalor3(valor2+Constantes.CO_ERROR);                       //--------- Tipo de Carga
			parametro3.setCopaVvalor6("smb:|"+getObjNuevo().getUsuario3());              //--------- Usuario
			
			lstRutas.add(parametro3);

		     this.servicioParametros.guardarObjetos(lstRutas);
			
			setStrRespuesta("Se guardo con EXITO el registro.");
			
	
			objNuevo = new BeanParametros();
			consultarConciliacion();
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			setStrRespuesta("Error Registro duplicado.");
			e.printStackTrace();
			this.log.error("Registro duplicado.", e);
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			setStrRespuesta("No se puede dar de alta la  Conciliacion.");
			e.printStackTrace();
			this.log.error("No se puede dar de alta el Conciliacion.", e);
			return NavigationResults.FAILURE;				
		}
	}
		
	
	
	public void modificar() {
		try {
			Parametros parametros = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual);
			servicioParametros.actualizarObjeto(parametros);
            hmResultados.remove(objActual.getCopaCdParametro().toString());
			hmResultados.put(objActual.getCopaCdParametro().toString(), objActual);
			
		   Parametros parametros1 = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual);
                      parametros1.setCopaCdParametro(objActual.getCopaCdParametro1());
		              parametros1.setCopaRegistro(objActual.getCopaRegistro1());
		              parametros1.setCopaIdParametro(objActual.getCopaIdParametro1());
		              parametros1.setCopaVvalor3(objActual.getCopaVvalor31());
		   servicioParametros.actualizarObjeto(parametros1);
		   

		   Parametros parametros2 = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual);
                      parametros2.setCopaCdParametro(objActual.getCopaCdParametro2());
                      parametros2.setCopaRegistro(objActual.getCopaRegistro2());
                      parametros2.setCopaIdParametro(objActual.getCopaIdParametro2());
                      parametros2.setCopaVvalor3(objActual.getCopaVvalor32());
           servicioParametros.actualizarObjeto(parametros2);
           
		   Parametros parametros3 = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual);
                      parametros3.setCopaCdParametro(objActual.getCopaCdParametro3());
                      parametros3.setCopaRegistro(objActual.getCopaRegistro3());
                      parametros3.setCopaIdParametro(objActual.getCopaIdParametro3());
                      parametros3.setCopaVvalor3(objActual.getCopaVvalor33());
           servicioParametros.actualizarObjeto(parametros3);	
           
			
			
			
			
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
	}
	
	

	public ArrayList<Object> getMapValues2(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados2().values());
        return lstValues;
    }
	

	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados().values());
        return lstValues;
    }
				
	public void afterPropertiesSet() throws Exception {
		try {
		} catch (Exception e) {
			throw new FacesException("No se pueden cargar las Conciliaciones.", e);
		}
	}	
	
	

	
	/**
	 * Getters and Setters
	 * @return
	 */
	
	
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}


	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}


	public String getStrRespuesta() {
		return strRespuesta;
	}


	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}


	public Log getLog() {
		return log;
	}


	public void setLog(Log log) {
		this.log = log;
	}


	public BigDecimal getCopaNvalor6() {
		return copaNvalor6;
	}


	public void setCopaNvalor6(BigDecimal copaNvalor6) {
		this.copaNvalor6 = copaNvalor6;
	}


	public int getCopaNvalor8() {
		return copaNvalor8;
	}


	public void setCopaNvalor8(int copaNvalor8) {
		this.copaNvalor8 = copaNvalor8;
	}


	public Date getCopaFvalor1() {
		return copaFvalor1;
	}


	public void setCopaFvalor1(Date copaFvalor1) {
		this.copaFvalor1 = copaFvalor1;
	}


	public Date getCopaFvalor2() {
		return copaFvalor2;
	}


	public void setCopaFvalor2(Date copaFvalor2) {
		this.copaFvalor2 = copaFvalor2;
	}


	public Date getCopaFvalor3() {
		return copaFvalor3;
	}


	public void setCopaFvalor3(Date copaFvalor3) {
		this.copaFvalor3 = copaFvalor3;
	}


	public Date getCopaFvalor4() {
		return copaFvalor4;
	}


	public void setCopaFvalor4(Date copaFvalor4) {
		this.copaFvalor4 = copaFvalor4;
	}


	public Date getCopaFvalor5() {
		return copaFvalor5;
	}


	public void setCopaFvalor5(Date copaFvalor5) {
		this.copaFvalor5 = copaFvalor5;
	}


	public List<Object> getListaConciliacion() {
		return listaConciliacion;
	}


	public void setListaConciliacion(List<Object> listaConciliacion) {
		this.listaConciliacion = listaConciliacion;
	}


	public List<Parametros> getListaParametros() {
		return listaParametros;
	}


	public void setListaParametros(List<Parametros> listaParametros) {
		this.listaParametros = listaParametros;
	}


	public HashMap<String, Object> getHmResultados() {
		return hmResultados;
	}


	public void setHmResultados(HashMap<String, Object> hmResultados) {
		this.hmResultados = hmResultados;
	}


	public int getFilaActual() {
		return filaActual;
	}


	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}


	public BeanParametros getObjActual() {
		return objActual;
	}


	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}


	public BeanParametros getObjNuevo() {
		return objNuevo;
	}


	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}


	public Set<Integer> getKeys() {
		return keys;
	}


	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}


	public int getNumPagina() {
		return numPagina;
	}


	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}


	public String getCopaRegistro1() {
		return copaRegistro1;
	}


	public void setCopaRegistro1(String copaRegistro1) {
		this.copaRegistro1 = copaRegistro1;
	}


	public String getCopaRegistro2() {
		return copaRegistro2;
	}


	public void setCopaRegistro2(String copaRegistro2) {
		this.copaRegistro2 = copaRegistro2;
	}


	public String getCopaRegistro3() {
		return copaRegistro3;
	}


	public void setCopaRegistro3(String copaRegistro3) {
		this.copaRegistro3 =copaRegistro3;
	}


	public String getUsuario1() {
		return usuario1;
	}


	public void setUsuario1(String usuario1) {
		this.usuario1 = usuario1;
	}


	public String getUsuario2() {
		return usuario2;
	}


	public void setUsuario2(String usuario2) {
		this.usuario2 = usuario2;
	}


	public String getUsuario3() {
		return usuario3;
	}


	public void setUsuario3(String usuario3) {
		this.usuario3 = usuario3;
	}

	/**
	 * @return the inCanal
	 */
	public Integer getInCanal() {
		return inCanal;
	}

	/**
	 * @param inCanal the inCanal to set
	 */
	public void setInCanal(Integer inCanal) {
		this.inCanal = inCanal;
	}

	/**
	 * @return the inRamo
	 */
	public HtmlSelectOneMenu getInRamo() {
		return inRamo;
	}

	/**
	 * @param inRamo the inRamo to set
	 */
	public void setInRamo(HtmlSelectOneMenu inRamo) {
		this.inRamo = inRamo;

	}


	public HashMap<String, Object> getHmResultados2() {
		return hmResultados2;
	}


	public void setHmResultados2(HashMap<String, Object> hmResultados2) {
		this.hmResultados2 = hmResultados2;
	}


	public String getStrRespuesta2() {
		return strRespuesta2;
	}


	public void setStrRespuesta2(String strRespuesta2) {
		this.strRespuesta2 = strRespuesta2;
	}
    
	
	
	
	
}
