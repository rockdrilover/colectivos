package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;


/**
 * @author ACM
 * @Modificado Ing. IBB
 */
@Controller
@Scope("session")
public class BeanCatalogoErrores {
	private Log log = LogFactory.getLog(this.getClass());
	@Resource
	private ServicioParametros servicioParametros;
	private List<Object> listaCatalogoError;
	private String strRespuesta, status;
	private int filaActual;
	private BeanParametros objActual = new BeanParametros();
	private Set<Integer> keys = new HashSet<Integer>();
	
	
	public BeanCatalogoErrores(){
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CATALOGO_ERRORES);
		this.listaCatalogoError= new ArrayList<Object>();
		strRespuesta = "";
	}
	
	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it =  getListaCatalogoError().iterator();
        while(it.hasNext()) {
        	BeanParametros objParametro = (BeanParametros) it.next();
        	if (objParametro.getCopaNvalor2().toString().equals(clave)){
                objActual = objParametro;
                break;
            }
        }
    }
	
	public String consultar(){
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean;
		List<Object> lstResultados = null;
		Parametros parametro;
		Iterator<Object> it;
		
		try {
			getListaCatalogoError().clear();

			filtro.append("and P.copaDesParametro = 'CATALOGO'" );
			filtro.append("and P.copaIdParametro  = 'ERRORES'" );
					
			lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
				
		    it = lstResultados.iterator();
		    while(it.hasNext()) {
		    	parametro = (Parametros) it.next();
		    	bean = new BeanParametros();
		    	
		    	bean.setCopaCdParametro(parametro.getCopaCdParametro());
		    	bean.setCopaIdParametro(parametro.getCopaIdParametro());
		    	bean.setCopaDesParametro(parametro.getCopaDesParametro());
		    	
		    	bean.setCopaNvalor1(parametro.getCopaNvalor1());
		    	bean.setCopaVvalor1(parametro.getCopaVvalor1());	  
		    	bean.setCopaVvalor8(parametro.getCopaVvalor8());
		    	bean.setCopaRegistro(parametro.getCopaRegistro());
		    	objActual = bean;
				listaCatalogoError.add(bean);
			}
		    setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");
		 
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			e.printStackTrace();
			this.log.error(" no es posble dar de alta nuevamente registro.", e);
			FacesUtils.addErrorMessage("no es posble dar de alta nuevamente registro.");
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			e.printStackTrace();
			this.log.error("no es posble dar de alta nuevamente registro.", e);
			FacesUtils.addErrorMessage("no es posble dar de alta nuevamente registro.");
			return NavigationResults.FAILURE;
		}
	}
	
	public void modificar(){
		try {
			if(objActual.getCopaRegistro().length() < 100){
				if (!objActual.getCopaRegistro().equals("")){				
					objActual.getCopaRegistro();
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual) ;				
					servicioParametros.actualizarObjeto(parametro);
					setStrRespuesta("Se modifico con EXITO el registro.");								
				}else{
					setStrRespuesta("No se modifico, favor de asignar una descripción.");
				}
			}else{
				setStrRespuesta("No se modifico, descripción supera los 100 carácteres.");
			}	
			listaCatalogoError.clear();
			listaCatalogoError.add(objActual);
		} catch (Exception e) {
			setStrRespuesta("Error al MODIFICAR registro.");
			e.printStackTrace();
		}
	} 
	
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(listaCatalogoError);
        return lstValues;
    }	

	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Object> getListaCatalogoError() {
		return listaCatalogoError;
	}
	public void setListaCatalogoError(List<Object> listaCatalogoError) {
		this.listaCatalogoError = listaCatalogoError;
	}
	public int getFilaActual() {
		return filaActual;
	}
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}
	public BeanParametros getObjActual() {
		return objActual;
	}
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	public Set<Integer> getKeys() {
		return keys;
	}
	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}
}
