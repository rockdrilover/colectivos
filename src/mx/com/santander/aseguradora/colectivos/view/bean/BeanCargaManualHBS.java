package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.VtaPrecargaId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCargaManualHBS;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;
import com.incesoft.tools.excel.xlsx.Cell;
import com.incesoft.tools.excel.xlsx.Sheet;
import com.incesoft.tools.excel.xlsx.SimpleXLSXWorkbook;
import com.incesoft.tools.excel.xlsx.Sheet.SheetRowReader;

/**
 * @author Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanCargaManualHBS {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioCargaManualHBS servicioCargaManualHBS;	
	
	private File archivo;
	private String nombreArchivo;
	private String respuesta;						
	private Integer nTipoCarga = 1;	
	private Short inRamo;
	private String inRemesa;
	private Integer nResultado;	
	private List<Object> lstResultados;

	public BeanCargaManualHBS() {
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CARGA_MANUAL_HBS);
	}
		
	public void listener(UploadEvent event) throws Exception {
		UploadItem item;
		File archivoTemporal;
		String ruta;
		String nombreArchivo = null;
		String nombreRuta;
		Archivo file;
		
		try {
			item = event.getUploadItem();
			ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS);
			file = new Archivo();
			
			if(item.getData() != null){
				file.setData(item.getData());
				file.setNombreArchivo(item.getFileName());
				file.setTamano(item.getData().length);
				nombreRuta = file.getNombreArchivo() + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
				nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
				archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
				FileUtil.write(archivoTemporal, item.getData());
				setArchivo(archivoTemporal);
				setRespuesta("Archivo cargado al servidor correctamente");
				log.debug(getRespuesta());
			} 
		} catch (Exception e) {
			setRespuesta("Error al cargar Archivo al servidor.");
			log.debug(getRespuesta());
		}
	}	
	
	
	@SuppressWarnings("unchecked")
	public String cargar() {
		String strForward = null;
		HashMap<String, Object> hmRamosPolizas;
		ArrayList<Parametros> arlPolizas;
		Iterator<Parametros> it;
		Parametros objParam;
		String strPolizas = "";
		
		try {
			lstResultados = new ArrayList<Object>();
			setnResultado(3);
			if(validarDatos()) {
				if(getnTipoCarga() == 1) {
					hmRamosPolizas = servicioCargaManualHBS.getRamosPolizas(servicioCargaManualHBS.getParemetros(Constantes.DES_PARAMETRO_BANCOSOFOM_POLIZAS , Constantes.ID_PARAMETRO_BANCOSOFOM));
					if(hmRamosPolizas.containsKey(Constantes.DES_PARAMETRO_BANCOSOFOM_POLIZAS + getInRamo().toString())) {
						leerLayOutManual();
		
						arlPolizas = (ArrayList<Parametros>) hmRamosPolizas.get(Constantes.DES_PARAMETRO_BANCOSOFOM_POLIZAS + getInRamo().toString());
						it = ( arlPolizas).iterator();
						while(it.hasNext()) {
							objParam = it.next();
							strPolizas = strPolizas.equals(Constantes.DEFAULT_STRING) ? objParam.getCopaNvalor3().toString() : strPolizas + ", " + objParam.getCopaNvalor3().toString();
						}
						
						servicioCargaManualHBS.datosMesPasado(getInRamo(), getInRemesa(), strPolizas);
						servicioCargaManualHBS.datosNuevosValidar(getInRamo(), hmRamosPolizas.get(Constantes.DES_PARAMETRO_BANCOSOFOM_POLIZAS + getInRamo().toString()), strPolizas);
						setRespuesta("El archivo se cargo Satisfactoriamente, favor de emitir los registros cargados.");
					} else {
						setRespuesta("No hay configuracion para el ramo seleccionado.");
					}
					
					lstResultados.addAll(servicioCargaManualHBS.getResultadosCarga(getInRamo(), Constantes.CONCILIACION_NOMBRE_ARCHIVO_MANUAL + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy")));
				} else if(getnTipoCarga() == 2){
					int nRegSave = leerArchivoFirmas();
					Resultado objResultado = new Resultado();
					objResultado.setPoliza(Constantes.DEFAULT_STRING_ID);
					objResultado.setTotalregistros(nRegSave);
					objResultado.setDescripcion("Registros Guardados del archivo de firmas.");
					objResultado.setPrima(Constantes.DEFAULT_DOUBLE);
					lstResultados.add(objResultado);
				}
			} else {				
				log.debug(getRespuesta());
				if(getArchivo() != null)
					getArchivo().delete();
				strForward = NavigationResults.FAILURE;
			}
			
		} catch (Exception e) {
			setRespuesta("No se puede realizar el proceso de carga." );
			log.error(getRespuesta() + e.getLocalizedMessage(), e);
			if(getArchivo() != null)
				getArchivo().delete();
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}

	private int leerArchivoFirmas() throws Exception {
		int nRegistrosGuardados = 0;
		SimpleXLSXWorkbook objLibro;
		Sheet objHoja;
		SheetRowReader objFila;
		Cell[] arrFila;
		int nConsecutivo = 1;
		VtaPrecarga objVtaPrecarga = null;
		ArrayList<VtaPrecarga> arlGuardar;
		
		try {
			arlGuardar = new ArrayList<VtaPrecarga>();
			objLibro  = new SimpleXLSXWorkbook(getArchivo());
			objHoja = objLibro.getSheet(0, false);
            objFila = objHoja.newReader();
            arrFila = objFila.readRow();
            while ((arrFila = objFila.readRow()) != null) {
            	objVtaPrecarga = new VtaPrecarga();
				if(arrFila == null || arrFila.length == 0 || arrFila[0] == null || arrFila[0].toString() == null) {
					break;
				} 
				
				setDatos(objVtaPrecarga, arrFila, getArchivo().getName(), nConsecutivo);			
				arlGuardar.add(objVtaPrecarga);
				nConsecutivo++;					
				if(nConsecutivo == 4000){			
					nRegistrosGuardados = nRegistrosGuardados + nConsecutivo;
					servicioCarga.guardarObjetos(arlGuardar);
					nConsecutivo = 1;
					arlGuardar = new ArrayList<VtaPrecarga>();
				}
			}
			       
            nRegistrosGuardados = nRegistrosGuardados + nConsecutivo;
            servicioCarga.guardarObjetos(arlGuardar);
            setRespuesta("El archivo se cargo Satisfactoriamente.");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("BeanCargaManualHBS.leerArchivoFirmas():: " + e.getMessage());
		} 
		
		return nRegistrosGuardados;
	}

	private void setDatos(VtaPrecarga objVtaPrecarga, Cell[] arrFila, String strNombreArchivo, int nConsecutivo) throws Exception {
		VtaPrecargaId pk;
		
		try {
			pk = new VtaPrecargaId();
			pk.setCdSucursal(1);
			pk.setCdRamo(Integer.parseInt(Constantes.PRODUCTO_VIDA));
			pk.setNumPoliza(nConsecutivo);
			pk.setIdCertificado(Utilerias.quitarCerosIzq((String)validarDato(arrFila[0], 4)));						
			pk.setSistemaOrigen("HBS_FIRMAS");
			objVtaPrecarga.setId(pk);
			
			objVtaPrecarga.setCargada("0");
			objVtaPrecarga.setEstatus(Utilerias.quitarCerosIzq((String)validarDato(arrFila[1], 4)));
			objVtaPrecarga.setNuContrato(Utilerias.quitarCerosIzq((String)validarDato(arrFila[2], 4)));
			objVtaPrecarga.setVdato9((String)validarDato(arrFila[3], 2));		
			objVtaPrecarga.setApPaternoAseg((String)validarDato(arrFila[4], 2));
			objVtaPrecarga.setApMaternoAseg((String)validarDato(arrFila[5], 2));			
			objVtaPrecarga.setNombreAseg((String)validarDato(arrFila[6], 2));
			objVtaPrecarga.setVdato11((String)validarDato(arrFila[7], 2));
			objVtaPrecarga.setVdato10((String)validarDato(arrFila[8], 2));
			objVtaPrecarga.setVdato8((String)validarDato(arrFila[9], 2));
			objVtaPrecarga.setVdato5((String)validarDato(arrFila[10], 2));
			objVtaPrecarga.setCdEstado((String)validarDato(arrFila[11], 4));
			objVtaPrecarga.setCp(Utilerias.validaCP((String)validarDato(arrFila[12], 4)));
			objVtaPrecarga.setCuentaAlterna((String)validarDato(arrFila[13], 2));
			objVtaPrecarga.setConductoAlterna((String)validarDato(arrFila[14], 4));
			objVtaPrecarga.setBancoAlterna((String)validarDato(arrFila[15], 4));
			objVtaPrecarga.setNombreBen((String)validarDato(arrFila[16], 2));
			objVtaPrecarga.setApPaternoBen((String)validarDato(arrFila[17], 2));
			objVtaPrecarga.setApMaternoBen((String)validarDato(arrFila[18], 2));
			objVtaPrecarga.setLada((String)validarDato(arrFila[19], 2));
			objVtaPrecarga.setTelefono((String)validarDato(arrFila[20], 2));
			if(arrFila.length == 22) {
				objVtaPrecarga.setSexo((String)validarDato(arrFila[21], 7));				
			}									
			
			objVtaPrecarga.setCdEstado(objVtaPrecarga.getCdEstado().equals(Constantes.DEFAULT_STRING_ID) ? "33" : objVtaPrecarga.getCdEstado());
			objVtaPrecarga.setCp(Utilerias.validaCP(objVtaPrecarga.getCp()));
			objVtaPrecarga.setFeCarga(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
			objVtaPrecarga.setArchivo(strNombreArchivo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("BeanCargaManualHBS.setDatos():: " + e.getMessage());	
		}
	}
	
	private void leerLayOutManual() {
		CsvReader csv = null;
		FileReader fr;
		VtaPrecarga objPrecarga = null;
		VtaPrecargaId pk;
		List<Object> lista = null;
		String[] arrDatos;
		String strLinea;
		int nConsecutivo = 1;
		
		try {
			lista = new ArrayList<Object>();
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			
			csv.readRecord();
			while(csv.readRecord()) {
				strLinea = csv.getRawRecord();
				arrDatos = strLinea.split("\\|");
				
				objPrecarga = new VtaPrecarga();
				pk = new VtaPrecargaId();
				pk.setCdSucursal(1);
				pk.setCdRamo(getInRamo());
				pk.setNumPoliza(nConsecutivo);
				pk.setIdCertificado(arrDatos[1]);						
				pk.setSistemaOrigen("HBS_MANUAL");
				objPrecarga.setId(pk);
				
				objPrecarga.setCargada("0");
				objPrecarga.setEstatus(arrDatos[0]);
				objPrecarga.setRegistro(strLinea);
				objPrecarga.setFeCarga(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));
				
				try {
					switch (Integer.parseInt(objPrecarga.getEstatus())) {
						case Constantes.HBS_ACCION_CREDITO: //Se pasa registro con los mismos datos
							break;
							
						case Constantes.HBS_ACCION_CREDITO_SUMA: //Suma Asegurada: Se cambia suma asegurada y se calcula la prima
							objPrecarga.setSumaAsegurada(arrDatos[2]);
							break;
							
						case Constantes.HBS_ACCION_CREDITO_PRIMA: //Prima: Se cambia la prima al registro
							objPrecarga.setPrima(arrDatos[3]);				
							break;
							
						case Constantes.HBS_ACCION_CREDITO_TARIFA: //Tarifa: Se cambia tarifa y se calcula la prima
							objPrecarga.setVdato1(arrDatos[4]);
							break;
							
						case Constantes.HBS_ACCION_CREDITO_SUMA_TARIFA: //Suma Asegurada y Tarifa: Se cambia suma asegurada y tarifa, se calcula la prima
							objPrecarga.setSumaAsegurada(arrDatos[2]);
							objPrecarga.setVdato1(arrDatos[4]);
							break;
							
						case Constantes.HBS_ACCION_CREDITO_FECHANAC: //Fecha Nacimiento: Se cambia fecha nacimiento.
							if(GestorFechas.validarFecha(arrDatos[5])) {
								objPrecarga.setFeNac(arrDatos[5]);
							} else {
								objPrecarga.setCargada("3");
								objPrecarga.setDesError("Fecha Incorrecta");
							}
							break;
							
						case Constantes.HBS_ACCION_CREDITO_CP://Codigo Postal: Se cambia CP
							objPrecarga.setCp(Utilerias.validaCP(arrDatos[6]));
							break;
					}
				} catch (Exception e) {
					objPrecarga.setCargada("3");
					objPrecarga.setDesError("Hay un error en el registro");
				}
				nConsecutivo++;
				lista.add(objPrecarga);
			}

			servicioCarga.guardarObjetos(lista);
			csv.close();
			fr.close();
			getArchivo().delete();
		} catch (IOException e) {
			log.error("Error de lectura en el archivo", e);
			FacesUtils.addErrorMessage("Error de lectura en el archivo");
		} catch (Exception e) {
			log.error("No se puede cargar la informacion del archivo.", e);
			FacesUtils.addErrorMessage("No se puede cargar la informacion del archivo.");
		}
	}
	
	
	private Object validarDato(Cell cell, int nTipoDato) throws Exception {
		Object objResultado = null;
		boolean blnNulo = false;
		
		try {
			if(cell == null) {
				blnNulo = true;
			} 
			
			switch (nTipoDato) {
				case 1: //Tipo de Dato Long
					objResultado = blnNulo == true ? Constantes.DEFAULT_LONG : cell.getValue() == null ? Constantes.DEFAULT_LONG : new Long(cell.getValue().trim()); 
					break;
				case 2: //Tipo de Dato String-Caracteres
					objResultado = blnNulo == true ? Constantes.DEFAULT_SIN_DATO : cell.getValue() == null ? Constantes.DEFAULT_SIN_DATO : cell.getValue().trim();
					break;
				case 3: //Tipo de Dato Date
//					objResultado = blnNulo == true ? GestorFechas.generateDate("01/01/1900", "dd/MM/yyyy") : GestorFechas.getFechaExcel(cell.getValue() == null ? Constantes.DEFAULT_INT : Integer.parseInt(cell.getValue().trim()));
						break;
				case 4: //Tipo de Dato String-Numeros
					objResultado = blnNulo == true ? Constantes.DEFAULT_STRING_ID : cell.getValue() == null ? Constantes.DEFAULT_STRING_ID : cell.getValue().trim();
					break;
				case 5: //Tipo de Dato String-Numeros Decimales
					objResultado = blnNulo == true ? "0.0" : cell.getValue() == null ? "0.0" : cell.getValue().trim();
					break;
				case 6: //Tipo de Dato String Vacio
					objResultado = blnNulo == true ? Constantes.DEFAULT_STRING : cell.getValue() == null ? Constantes.DEFAULT_STRING : cell.getValue().trim();
					break;
				case 7: //Tipo de Dato Para Sexo
					objResultado = blnNulo == true ? "MA" : cell.getValue() == null ? "MA" : cell.getValue().trim().toUpperCase();
					break;	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("BeanCargaManualHBS.setDatos():: " + e.getMessage());
		}
		
		return objResultado;
		
	}
	
	private boolean validarDatos() throws Exception{
		boolean blnResultado = true;
		
		if(getnTipoCarga() == null) {
			setRespuesta("Favor de Seleccionar un Tipo Carga.");
			blnResultado = false;
		} else if(getnTipoCarga() == 1) {
			if (getInRamo() == null || getInRamo() == 0){			
				setRespuesta("El Ramo no es correcto.");
				blnResultado = false;
			} else if(getInRemesa() == null || getInRemesa().equals(Constantes.DEFAULT_STRING)) {
				setRespuesta("Favor de ingresar Remesa.");
				blnResultado = false;
			} else if(getArchivo() == null) {
				setRespuesta("Favor de cargar un archivo.");
				blnResultado = false;
			}	
		} else if(getnTipoCarga() == 2) {
			if(getArchivo() == null) {
				setRespuesta("Favor de cargar un archivo.");
				blnResultado = false;
			}		 
		}
		
		return blnResultado;
	}
	
	
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}
	public ServicioCargaManualHBS getServicioCargaManualHBS() {
		return servicioCargaManualHBS;
	}
	public void setServicioCargaManualHBS(ServicioCargaManualHBS servicioCargaManualHBS) {
		this.servicioCargaManualHBS = servicioCargaManualHBS;
	}
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	public File getArchivo() {
		return archivo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}	
	public void setMensaje(String mensaje) {
		this.respuesta = mensaje;
	}
	public String getMensaje() {
		return respuesta;
	}
	public Integer getnResultado() {
		return nResultado;
	}
	public void setnResultado(Integer nResultado) {
		this.nResultado = nResultado;
	}
	public Integer getnTipoCarga() {
		return nTipoCarga;
	}
	public void setnTipoCarga(Integer nTipoCarga) {
		this.nTipoCarga = nTipoCarga;
	}
	public Short getInRamo() {
		return inRamo;
	}
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}
	public String getInRemesa() {
		return inRemesa;
	}
	public void setInRemesa(String inRemesa) {
		this.inRemesa = inRemesa;
	}
	public List<Object> getLstResultados() {
		return lstResultados;
	}
	public void setLstResultados(List<Object> lstResultados) {
		this.lstResultados = lstResultados;
	}
}

