package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCiudades;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCertificados;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosClientes;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCiudad;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Mail;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.BeneficiariosDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.CargaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ClienteCertifDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.TipoEndosoDTO;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author
 * @Modify Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanEndosoDatos extends BeanBase{
	
	@Resource
	private ServicioEndosoDatos servicioEndosoDatos;
	@Resource
	private ServicioCiudad servicioCiudad;
	@Resource
	private ServicioAltaContratante servicioAltaContratante;
	@Resource
	private ServicioParametros servicioParametros;
	

	private StringBuilder message;
	
	private String identificador;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	
	private List<ClienteCertifDTO> listaCertificados;
	private List<Object> listaComboCiudades;
	private List<Object> listaComboEstados;
	private List<BeneficiariosDTO> lstBeneficiarios;
	private List<Object> listaCmbParentesco;
	private List<BeneficiariosDTO> lstAddBeneficiario;
	private List<BeneficiariosDTO> lstDelBeneficiario;
	
	private BeneficiariosDTO seleccionado;
	private BeneficiariosDTO nuevo;

	private int numPagina;
	private String respuesta;
	private String respuesta2;
	
	private String criterioBusqueda;
	private String muestraPE;
	
	private String display1 = "none";
	private String tdc;
	
	private String display2 = "none";
	private String buc;
	
	private String display3 = "none";

	private String display4 = "none";
	private String canal; 
	private Short ramo;
	private Integer poliza;
	private String certificado;
	private String polizaEspecifica;
	
	private String display5 = "none";
	private String displayPE = "none";
	
	private String tipoEndoso;
	private String muestraEndoso;
	private String displayE1 = "none";
	private String displayE2 = "none";
	private String displayE3 = "none";;
	private String displayE4 = "none";
	private String displayE5 = "none";
	private String displayE6 = "none";
	private String displayE7 = "none";
	private String displayE8 = "none";
	private String displayE9 = "none";
	private String displayE10 = "none";
	
	private String dBtnEndoso = "none";

	private String calleNumero;
	private String colonia;
	private String delegacionMunicipio;
	private Date fechaNacimiento;
	private String rfc;
	private String sexo;
	private String nombreEndoso;
	private String apellidoPaternoEndoso;
	private String apellidoMaternoEndoso;
	private String correoElectronico;
	private String estado;
	private String ciudad;
	private String codigoPostal;

	private Date fechaDesde;
	private Date fechaHasta;
	private Date fechaSuscripcion;
	private BigDecimal sumaAsegurada;
	private List<Object> lstComboTpEndoso;
	
	private String mostrarModalConfirmar;
	
	private Integer contador;	
	private Integer nNumBeneficiario;
	private Integer numeroCliente;
	private Integer tipoRFC=0;
	private String rfc1;
	private String rcf2;
	private String rfc3;

	public BeanEndosoDatos() {
		this.message = new StringBuilder();
		this.listaCertificados = new ArrayList<>();
		this.numPagina = 1;
		this.listaComboCiudades		= new ArrayList<>();
		this.listaComboEstados      = new ArrayList<>();
		this.listaCmbParentesco		= new ArrayList<>();
		this.lstBeneficiarios	    = new ArrayList<>();
		this.lstAddBeneficiario     = new ArrayList<>();
		this.lstDelBeneficiario     = new ArrayList<>();
		this.lstComboTpEndoso		= new ArrayList<>();
		seleccionado = new BeneficiariosDTO();
		nuevo = new BeneficiariosDTO();
		nuevo.setCobeVcampo4(Constantes.BENEFICIARIO_NUEVO);

		fechaDesde = new Date();
		fechaHasta = new Date();
		fechaSuscripcion = new Date();
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ENDOSO_DATOS);
	}	
	
	public String muestraCriterio(){
		this.respuesta = Constantes.DEFAULT_STRING;
		this.respuesta2 = Constantes.DEFAULT_STRING;
		
		if(getCriterioBusqueda() != null && getCriterioBusqueda().equals("1")){
			setDisplay1("block");
			setDisplay2("none");
			setDisplay3("none");
			setDisplay4("none");
			setDisplay5("none");
			setDisplayPE("none");
		} else {
			setDisplay1("none");
		}
		
		if(getCriterioBusqueda() != null && getCriterioBusqueda().equals("2")){
			setDisplay1("none");
			setDisplay2("block");
			setDisplay3("none");
			setDisplay4("none");
			setDisplay5("none");
			setDisplayPE("none");
		} else {
			setDisplay2("none");
		}
		
		if(getCriterioBusqueda() != null && getCriterioBusqueda().equals("3")){
			setDisplay1("none");
			setDisplay2("none");
			setDisplay3("block");
			setDisplay4("none");
			setDisplay5("none");
			setDisplayPE("none");
		} else {
			setDisplay3("none");
		}
		
		if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("4")){
			setDisplay1("none");
			setDisplay2("none");
			setDisplay3("none");
			setDisplay4("block");
			setDisplay5("none");
			setDisplayPE("none");
		} else {
			setDisplay4("none");
		}
		
		if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("5")){
			setDisplay1("none");
			setDisplay2("none");
			setDisplay3("none");
			setDisplay4("none");
			setDisplay5("block");
			setDisplayPE("none");
		} else {
			setDisplay5("none");
		}

		return getCriterioBusqueda();
	}

	public void muestraPE(){
		if(getPoliza() != null && getPoliza().equals("1")){
			setDisplayPE("block");
		} else {
			setDisplayPE("none");	
		}
	}
	
	public String consultaCertificados(){
		StringBuilder filtro = new StringBuilder();
		this.respuesta = "";
		this.respuesta2 = Constantes.DEFAULT_STRING;
		List<ClienteCertif> lstResultados;
		ClienteCertifDTO objCliente;
		
		try {
			this.lstAddBeneficiario = new ArrayList<BeneficiariosDTO>();
			resetZonas();
			if(getCriterioBusqueda().equals("0")) {
				this.respuesta = "* Favor de Seleccionar un Criterio. \n";
			} else {
				if(getCriterioBusqueda().equals("1")) { //tarjeta de credito
					System.out.println("Entro a consulta numero 1 TDC");								
					if((this.tdc != null && !this.tdc.equalsIgnoreCase(""))) {
						filtro.append(" and CC.certificado.coceNuCuenta = '").append(this.tdc).append("' \n");
					} else {
						this.respuesta = "* Favor de Ingresar TDC. \n";
					}					
				} else if(getCriterioBusqueda().equals("2")) { //BUC
					if((this.buc != null && !this.buc.equalsIgnoreCase(""))){
						filtro.append(" and CC.cliente.cocnBucCliente = '").append(this.buc).append("' \n");//cliente cocn buc
					} else {
						this.respuesta = "* Favor de Ingresar BUC. \n";					
					}
				} else if(getCriterioBusqueda() != null && getCriterioBusqueda().equals("3")) { //folio
					if((this.identificador != null && !this.identificador.equalsIgnoreCase(""))){
						filtro.append(" and CC.id.coccIdCertificado = '").append(this.identificador).append("' \n");
					} else {
						this.respuesta = "* Favor de Ingresar FOLIO. \n";
					}
				} else if (getCriterioBusqueda() != null && getCriterioBusqueda().equals("4")) { //crpc
					if(!this.ramo.equals("0")){
						filtro.append(" and CC.certificado.id.coceCasuCdSucursal = 1 \n");
						filtro.append(" and CC.certificado.id.coceCarpCdRamo = ").append(this.ramo).append(" \n");
					} else {
						this.respuesta = "* Favor de seleccionar RAMO. \n";
					}
					
					if(this.poliza == 1) {
						if((!this.polizaEspecifica.equals(""))){
							filtro.append(" and CC.certificado.id.coceCapoNuPoliza = ").append(this.polizaEspecifica).append(" \n");
						} else {
							this.respuesta = this.respuesta + "* Favor de seleccionar POLIZA ESPECIFICA. \n";
						}
					} else if(this.poliza == 0) {
						this.respuesta = this.respuesta + "* Favor de seleccionar POLIZA. \n";
					} else { 
						filtro.append(" and CC.certificado.id.coceCapoNuPoliza = ").append(this.poliza).append(" \n");
					}
					
					if(!this.certificado.equals("")){
						filtro.append(" and CC.certificado.id.coceNuCertificado = ").append(this.certificado).append(" \n");
					} else {
						this.respuesta = this.respuesta + "* Favor de seleccionar CERTIFICADO. \n";
					}
						
				} else if(getCriterioBusqueda().equals("5")) { //nombre
					if(!this.nombre.equals("")){
						filtro.append(" and CC.cliente.cocnNombre like '%").append(this.nombre.toUpperCase()).append("%'\n");
					} else {
						this.respuesta = this.respuesta + "* Favor de seleccionar NOMBRE. \n";
					}
						
					if(!this.apellidoPaterno.equals("")){
						filtro.append(" and CC.cliente.cocnApellidoPat like '%").append(this.apellidoPaterno.toUpperCase()).append("%'\n");
					} else {
						this.respuesta = this.respuesta + "* Favor de seleccionar APELLIDO PATERNO. \n";
					}
						
					if(!this.apellidoMaterno.equals("")){
						filtro.append(" and CC.cliente.cocnApellidoMat like '%").append(this.apellidoMaterno.toUpperCase()).append("%'\n");
					}
				}
			}
			
			if(this.respuesta.equals("")) {
				this.listaCertificados.clear();

				lstResultados = this.servicioEndosoDatos.obtenerObjetos(filtro.toString());

				for (ClienteCertif c : lstResultados){				
					objCliente = (ClienteCertifDTO) ConstruirObjeto.crearBean(ClienteCertifDTO.class, c);
					if (objCliente.getCoccTpCliente() != null){
						numeroCliente=(int) objCliente.getCliente().getCocnNuCliente();
						this.log.info("valor  RFC Inicial"+objCliente.getCliente().getCocnRfc());
					    String rfcsep = objCliente.getCliente().getCocnRfc(),R1="",R2="",R3="";
					    char [] rfcdiv = rfcsep.toCharArray();
					    //separacion de rfc para editar el endosos
					    if(rfcdiv.length != 0) {
					    	if(rfcdiv.length == 13 || rfcdiv.length == 10) {
					    		for (int i = 0; i < rfcdiv.length; i++) {
									this.log.info("posicion: "+i+"Valor de RFC :"+ rfcdiv[i]);
									if(i <= 3) {
									R1+=rfcdiv[i];
									}else if(i >= 4 && i <= 9) {
										R2+=rfcdiv[i];
									}else if(i >= 10) {
										R3+=rfcdiv[i];
									}
								}
								}else if(rfcdiv.length == 12 || rfcdiv.length == 9) {
									if(rfcdiv.length <= 9) {
							    		for (int i = 0; i < rfcdiv.length; i++) {
											this.log.info("posicion: "+i+"Valor de RFC :"+ rfcdiv[i]);
											if(i <= 2) {
											R1+=rfcdiv[i];
											}else if(i >= 3 && i <= 8) {
												R2+=rfcdiv[i];
											}else if(i >=9) {
												R3+=rfcdiv[i];
											}
										}
							    	}
					    	}					    	
					    } else  {
					    	this.log.info("No se encuentra el rfc");
					    }
						setRfc1(R1);
						setRcf2(R2);
						setRfc3(R3);
						setRfc(R1+R2+R3);
						this.log.info("valores de la separacion: "+R1+" "+R2+" "+R3);
							respuesta2="";
						

						this.log.info("numero de cliente " +numeroCliente);
						if(objCliente.getCoccTpCliente() == 1){
							
							objCliente.setCoccVvalor1("Asegurado");
						} else {
							objCliente.setCoccVvalor1("Obligado Solidario");
						}
					} else {
						objCliente.setCoccVvalor1("No tiene tipo");
					}
					
					this.listaCertificados.add(objCliente);
				}
				
				this.message.append("Consulta realizada correctamente.");
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				Utilerias.resetMessage(message);
			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ENDOSO_DATOS);
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.message.append("No se puede realizar la consulta de los certificados.");
			this.log.error(message.toString(), e);
			this.respuesta = this.message.append("Error interno.").toString();
			Utilerias.resetMessage(message);
			return NavigationResults.RETRY;
		}
	}
	
	/**
	 * Metodo que incialisa variables.
	 */
	public void resetZonas(){	
		setTipoEndoso("0");
		setDisplayE1("none");
		setDisplayE2("none");
		setDisplayE3("none");
		setDisplayE4("none");
		setDisplayE5("none");
		setDisplayE6("none");
		setDisplayE7("none");
		setDisplayE8("none");
		setDisplayE9("none");
		setDisplayE10("none");
		setdBtnEndoso("none");
	}
	
	/**
	 * Metodo que muestra las opciones (controles) para hacer los endosos
	 * @return regresa tipo de vista (falla o exito)
	 * @throws Excepciones errores generales
	 */
	public String muestraDisplayEndoso() throws Excepciones {
		String tipoClie = Constantes.DEFAULT_STRING;
		setRespuesta(Constantes.DEFAULT_STRING);
		setRespuesta2(Constantes.DEFAULT_STRING);
		
		try {
			contador = 0;
			for (ClienteCertifDTO objEndoso : getListaCertificados()) {
				if(objEndoso.isCheckE()){
					contador++;
					tipoClie = setDatosMostrar(objEndoso);
					break;
				}
			}
			
			if(contador == 0){
				setTipoEndoso("0");
				setRespuesta2("SELECCIONE UN CERTIFICADO");
				return NavigationResults.FAILURE;
			} else if(contador == -1){
				setTipoEndoso("0");
				setRespuesta2("NO SE PUEDE REALIZAR ENDOSO DE BENEFICIARIOS PARA ESTE RAMO Y PRODUCTO ");
				return NavigationResults.FAILURE;
			}
			
			if(!tipoClie.equals(Constantes.DEFAULT_STRING)){
				muestraEndoso(tipoClie);
			} else {
				setRespuesta2("SELECCIONE UN TIPO DE CLIENTE VALIDO");
			}
		} catch (Excepciones e) {
			setRespuesta2("Error muestraDisplayEndoso "+ e.getMessage());
			this.log.error(getRespuesta2(), e);
			throw new FacesException(getRespuesta2(), e);
		}
		
		return NavigationResults.SUCCESS;
	}

	private String setDatosMostrar(ClienteCertifDTO objEndoso) throws Excepciones {
		String strRespuesta = null;
		
		try {
			setNombreEndoso((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnNombre(), Constantes.TIPO_DATO_STRING));
			setApellidoPaternoEndoso((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnApellidoPat(), Constantes.TIPO_DATO_STRING));
			setApellidoMaternoEndoso((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnApellidoMat(), Constantes.TIPO_DATO_STRING));
			
			setCalleNumero((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnCalleNum(), Constantes.TIPO_DATO_STRING));
			setColonia((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnColonia(), Constantes.TIPO_DATO_STRING));
			setCodigoPostal((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnCdPostal(), Constantes.TIPO_DATO_STRING));
			setEstado((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnCdEstado(), Constantes.TIPO_DATO_STRING_ID));
			consultaCiudad();
			setCiudad((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnCdCiudad(), Constantes.TIPO_DATO_STRING_ID));
			setDelegacionMunicipio((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnDelegmunic(), Constantes.TIPO_DATO_STRING));
			
			setFechaNacimiento((Date) Utilerias.objectIsNull(objEndoso.getCliente().getCocnFeNacimiento(), Constantes.TIPO_DATO_DATE));
			
			setRfc((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnRfc(), Constantes.TIPO_DATO_STRING));

			setSexo((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnCdSexo(), Constantes.TIPO_DATO_STRING));

			setCorreoElectronico((String) Utilerias.objectIsNull(objEndoso.getCliente().getCocnCorreoElect(), Constantes.TIPO_DATO_STRING));
				
			if(Constantes.TIPO_ENDOSO_BENEFICIARIOS.equals(getTipoEndoso())) {
				if(existeProducto(objEndoso.getCertificado().getPlanes().getId().getAlplCdRamo(), objEndoso.getCertificado().getPlanes().getId().getAlplCdProducto())) {
					lstBeneficiarios = servicioEndosoDatos.getBeneficiarios(objEndoso.getCertificado().getId());
					lstAddBeneficiario = new ArrayList<>();
					lstAddBeneficiario.addAll(lstBeneficiarios);
					listaCmbParentesco = new ArrayList<>();
					listaCmbParentesco.addAll(consultaParentesco());
				} else {
					contador = -1;
				}
			}
			
			setFechaDesde((Date) Utilerias.objectIsNull(objEndoso.getCertificado().getCoceFeDesde(), Constantes.TIPO_DATO_DATE));
			setFechaHasta((Date) Utilerias.objectIsNull(objEndoso.getCertificado().getCoceFeHasta(), Constantes.TIPO_DATO_DATE));
			
			setFechaSuscripcion((Date) Utilerias.objectIsNull(objEndoso.getCertificado().getCoceFeSuscripcion(), Constantes.TIPO_DATO_DATE));
			
			setSumaAsegurada(objEndoso.getCertificado().getCoceMtSumaAsegurada());
			
			if (objEndoso.getCoccTpCliente() != null && objEndoso.getCoccTpCliente() == 1){ 
				mostrarModalConfirmar = "true";
				strRespuesta = "asegurado";
			} else if (objEndoso.getCoccTpCliente() != null){ 
				mostrarModalConfirmar = "true";
				strRespuesta = "obligado";
			}  else {
				mostrarModalConfirmar = "false";
				strRespuesta = "desconocido";
			}		
		} catch (Excepciones e) {
			setRespuesta2("Error setDatosMostrar "+ e.getMessage());
			this.log.error(getRespuesta2(), e);
			throw new Excepciones(getRespuesta2(), e);
		}
		
		return strRespuesta;
	}

	/**
	 * Metodo que manda consultar la descripcion de la ciudad
	 */
	public void consultaCiudad() {
		listaComboCiudades.clear();
		StringBuilder filtro = new StringBuilder();
		
		try{
			BigDecimal cdCiudad = null;
			cdCiudad = new BigDecimal(getEstado());
			
			filtro.append(" and E.id.caciCaesCdEstado = ").append(cdCiudad);
			filtro.append(" ");
			List<CartCiudades> lista = this.servicioCiudad.obtenerObjetos(filtro.toString());
	
			for (CartCiudades cartCiudades : lista) {
				this.listaComboCiudades.add(new SelectItem(new BigDecimal(cartCiudades.getId().getCaciCdCiudad()).toString(),cartCiudades.getCaciDeCiudad()));
			}
		} catch (Excepciones e) {
			this.log.error("No se pueden cargar las ciudades.", e);
			throw new FacesException("No se pueden cargar las ciudades.", e);
		}
	}
	
	/**
	 * Metodo que te muestra los controles dependiendo el tipo endoso. 
	 * @param tipoClie
	 * @return
	 */
	public String muestraEndoso(String tipoClie){
		if ("descosnocido".equals(tipoClie)) {
			setDisplayE1(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE2(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE3(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE4(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE5(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE6(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE7(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE8(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE9(Constantes.ENDOSO_DATOS_OCULTA);
			setDisplayE10(Constantes.ENDOSO_DATOS_OCULTA);
			setdBtnEndoso(Constantes.ENDOSO_DATOS_OCULTA);
			return getTipoEndoso();
		} 
		
		setDisplayE1(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE2(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE3(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE4(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE5(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE6(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE7(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE8(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE9(Constantes.ENDOSO_DATOS_OCULTA);
		setDisplayE10(Constantes.ENDOSO_DATOS_OCULTA);
		setdBtnEndoso(Constantes.ENDOSO_DATOS_MUESTRA);
		
		if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_NOMBRE.equals(getTipoEndoso())) {
			setDisplayE1(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_DOMICILIO.equals(getTipoEndoso())) {
			setDisplayE2(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_FECHA_NAC.equals(getTipoEndoso())) {
			setDisplayE3(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_RFC.equals(getTipoEndoso())) {
			setDisplayE4(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_SEXO.equals(getTipoEndoso())) {
			setDisplayE5(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_CORREO.equals(getTipoEndoso())) {
			setDisplayE6(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_BENEFICIARIOS.equals(getTipoEndoso())) {
			setDisplayE7(Constantes.ENDOSO_DATOS_MUESTRA);
			setdBtnEndoso(Constantes.ENDOSO_DATOS_OCULTA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_VIGENCIAS.equals(getTipoEndoso())) {
			setDisplayE8(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_FECHA_SUS.equals(getTipoEndoso())) {
			setDisplayE9(Constantes.ENDOSO_DATOS_MUESTRA);
		} else if(getTipoEndoso() != null && Constantes.TIPO_ENDOSO_SUMA.equals(getTipoEndoso())) {
			setDisplayE10(Constantes.ENDOSO_DATOS_MUESTRA);
		}
		
		return getTipoEndoso();
	}

	/**
	 * Metodo que genera un endoso por cada cambio
	 * @return
	 * @throws Excepciones
	 */
	public String endosar() throws Excepciones {
		this.respuesta = "";
		String strRespuesta="";
		
		try {
				strRespuesta = NavigationResults.SUCCESS;
				for (ClienteCertifDTO objEndoso : getListaCertificados()) {
					if(objEndoso.isCheckE()) {									
						strRespuesta = setEndoso(objEndoso);
						break;
					}
			}			
			
		} catch (Excepciones e) {
			setRespuesta2("Error al insertar endoso "+e.getMessage());
			this.log.error(getRespuesta2(), e);
			throw new FacesException(getRespuesta2(), e);
		}
		this.log.info("Valor de endoso.-.-.-."+strRespuesta);
		return strRespuesta;
	}

	private String setEndoso(ClienteCertifDTO objEndoso) throws Excepciones {
		String strRegreso = NavigationResults.SUCCESS;
		ClienteCertif objClienteAnt;
		
		try {
			objClienteAnt = servicioEndosoDatos.getClienteAnterior(objEndoso.getId());
			
			objEndoso.getCliente().setCocnNombre(getNombreEndoso().toUpperCase());
			objEndoso.getCliente().setCocnApellidoPat(getApellidoPaternoEndoso().toUpperCase());
			objEndoso.getCliente().setCocnApellidoMat(getApellidoMaternoEndoso().toUpperCase());
			
			objEndoso.getCliente().setCocnCalleNum(getCalleNumero().toUpperCase());
			objEndoso.getCliente().setCocnColonia(getColonia().toUpperCase());
			objEndoso.getCliente().setCocnCdPostal(getCodigoPostal());
			objEndoso.getCliente().setCocnDelegmunic(getDelegacionMunicipio().toUpperCase());
			objEndoso.getCliente().setCocnCdEstado(new Byte(getEstado()));
			objEndoso.getCliente().setCocnCdCiudad(new Short(getCiudad()));
			
			objEndoso.getCliente().setCocnFeNacimiento(getFechaNacimiento());
			
			objEndoso.getCliente().setCocnRfc(getRfc().toUpperCase());
			
			objEndoso.getCliente().setCocnCdSexo(getSexo().toUpperCase());
			
			objEndoso.getCliente().setCocnCorreoElect(getCorreoElectronico().toUpperCase());
			
			objEndoso.getCertificado().setCoceFeDesde(getFechaDesde());
			objEndoso.getCertificado().setCoceFeHasta(getFechaHasta());
			
			objEndoso.getCertificado().setCoceFeSuscripcion(getFechaSuscripcion());
			
			objEndoso.getCertificado().setCoceMtSumaAsegurada(getSumaAsegurada());
			
			switch (getTipoEndoso()) {
				case Constantes.TIPO_ENDOSO_FECHA_NAC:
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					this.log.info("El valor de la fecha:::"+objClienteAnt.getCliente().getCocnFeNacimiento());
					try {
						Date fechanull= sdf.parse("01/01/1900");
						if(objClienteAnt.getCliente().getCocnFeNacimiento() == null) {
							setRespuesta2("La fecha de nacimineto ingresada se encuentra vacia por lo que se toma la fecha por default 01/01/1900.");
							this.log.info("El valor de la fecha:::"+objClienteAnt.getCliente().getCocnFeNacimiento());
							objClienteAnt.getCliente().setCocnFeNacimiento(fechanull);
						}
					} catch (Exception e) {
						this.log.info("Error al convertir fecha a endoso de fecha de nacimiento"+e);
					}
					
					
					/*else {
					if(this.servicioEndosoDatos.validaFecha(objEndoso, (String) Utilerias.objectIsNull(objEndoso.getCertificado().getCoceSubCampana(), Constantes.TIPO_DATO_STRING_ID)) == Constantes.DEFAULT_INT) {
						setRespuesta2("La fecha de Nacimiento no cumple con las edades de aceptacion.");
						objEndoso.getCliente().setCocnFeNacimiento(objClienteAnt.getCliente().getCocnFeNacimiento());
						strRegreso = NavigationResults.FAILURE;
					}
					}*/
					this.log.info("El valor de la fecha:::"+objClienteAnt.getCliente().getCocnFeNacimiento());
					break;
					
				case Constantes.TIPO_ENDOSO_RFC:
					Date objFecha = GestorFechas.generateDate(GestorFechas.generarFechaNacimiento(getRfc().toUpperCase()), "dd/MM/yyyy");
					/*	
					if(GestorFechas.comparaFechas(objFecha, objEndoso.getCliente().getCocnFeNacimiento()) != 0){
						setRespuesta2("La edad del RFC no coincide con la fecha de nacimiento.");
						objEndoso.getCliente().setCocnRfc(objClienteAnt.getCliente().getCocnRfc());
						strRegreso = NavigationResults.FAILURE;
					}*/
					
					if(!getRfc().equals("")) {
						setRespuesta2("Endoso Generado");
						strRegreso = NavigationResults.SUCCESS;
						objEndoso.getCliente().setCocnRfc(getRfc1()+getRcf2()+getRfc3());
						this.log.info("Valor RFCactual...."+objEndoso.getCliente().getCocnRfc()+"RFC resultado");
					}else {
						setRespuesta2("Favor de Generar RFC ya que no se puede endosar con ese formato");
						strRegreso = NavigationResults.FAILURE;
						this.log.info("Valor RFCactual...."+objEndoso.getCliente().getCocnRfc());
					}
					
					break;
					
				case Constantes.TIPO_ENDOSO_VIGENCIAS:
					strRegreso = validaFechaVig(objClienteAnt.getCertificado(), objEndoso);
					break;
					
				case Constantes.TIPO_ENDOSO_FECHA_SUS:
					strRegreso = validaFechaSus(objClienteAnt.getCertificado(), objEndoso);
					
					break;

				case Constantes.TIPO_ENDOSO_SUMA:
					strRegreso = validaSumaAsegurada(objClienteAnt.getCertificado(), objEndoso);
					break;
				
				default:
					strRegreso = NavigationResults.SUCCESS;
					break;
			}
			
			if(strRegreso.equals(NavigationResults.SUCCESS)) {
				aplicaEndoso(objClienteAnt, objEndoso);
			}
		} catch (Excepciones e) {
			throw new Excepciones("Error: setEndoso.", e);
		}
		
		return strRegreso;
				
	}

	private String validaSumaAsegurada(Certificado objAnterior, ClienteCertifDTO objEndoso) throws Excepciones {
		String strResultado = NavigationResults.SUCCESS;
		boolean bResultado = true;
		CargaDTO objPrima;
		
		try {
			if(objAnterior.getEstatus().getAlesCdEstatus() != 1) {
				setRespuesta2("El certificado debe tener estatus ACTIVO/SIN FACTURAR, para poder cambiar la suma asegurada.");
				bResultado = false;
			}
			
			if(!this.servicioEndosoDatos.validaRangoSumas(objEndoso.getCertificado())) {
				setRespuesta2("Suma asegurada no valida o fuera del rango aceptable.");
				bResultado = false;
			}
			
			if(bResultado) {
				objPrima = this.servicioEndosoDatos.calculaNuevaPrima(objEndoso.getCertificado());
				objEndoso.getCertificado().setCoceMtPrimaAnual(new BigDecimal(objPrima.getCocrDato28()));
				objEndoso.getCertificado().setCoceMtPrimaReal(objPrima.getCocrPrimaReal());
				objEndoso.getCertificado().setCoceMtPrimaAnualReal(new BigDecimal(objPrima.getCocrDato30()));
				objEndoso.getCertificado().setCoceMtPrimaPura(new BigDecimal(objPrima.getCocrDato29()));
				objEndoso.getCertificado().setCoceMtPrimaPuraReal(new BigDecimal(objPrima.getCocrDato26()));
				objEndoso.getCertificado().setCoceMtPrimaSubsecuente(objPrima.getCocrPrima());
				objEndoso.getCertificado().setCoceMtPmaUdis(new BigDecimal(objPrima.getCocrDato26()));
				objEndoso.getCertificado().setCoceDiCobro1(objPrima.getCocrDato24());
				objEndoso.getCertificado().setCoceComisiones(objPrima.getCocrComisiones());
				if(objAnterior.getCoceMtSumaAsegurada().equals(objAnterior.getCoceMtSumaAsegSi())) {
					objEndoso.getCertificado().setCoceMtSumaAsegSi(objEndoso.getCertificado().getCoceMtSumaAsegurada());
				}
			} else {
				objEndoso.getCertificado().setCoceMtSumaAsegurada(objAnterior.getCoceMtSumaAsegurada());
				objEndoso.getCertificado().setCoceMtSumaAsegSi(objAnterior.getCoceMtSumaAsegSi());
				strResultado = NavigationResults.FAILURE;
			}
		} catch (Excepciones e) {
			throw new Excepciones("Error: validaSumaAsegurada.", e);
		}
		
		return strResultado;
	}

	private String validaFechaVig(Certificado objAnterior, ClienteCertifDTO objEndoso) throws Excepciones {
		String strResultado = NavigationResults.SUCCESS;
		boolean bResultado = true;
		Object[] arrFechas;
		Date dDesdePol;
		Date dHastaPol;
		
		arrFechas = this.servicioEndosoDatos.getFechasPoliza(objEndoso.getCertificado().getId());
		dDesdePol = (Date) arrFechas[0];
		dHastaPol = (Date) arrFechas[1];
		
		if(objEndoso.getCertificado().getId().getCoceCarpCdRamo() == Constantes.CONCILIACION_DESEMPLEO_TDC_RAMO 
				&& objEndoso.getCertificado().getPlanes().getId().getAlplCdProducto() == Constantes.PRODUCTO_EVENTOS_SAN) {
			
			objEndoso.getCertificado().setCoceFeDesde(dDesdePol);
			objEndoso.getCertificado().setCoceFeHasta(dHastaPol);
			objEndoso.getCertificado().setCoceFeFinCredito(GestorFechas.generateDate(GestorFechas.addMesesaFecha(dHastaPol, 60), "dd/MM/yyyy"));
			
			if(GestorFechas.comparaFechas(dDesdePol, objAnterior.getCoceFeSuscripcion()) == 1) {
				objEndoso.getCertificado().setCoceFeDesde(objAnterior.getCoceFeSuscripcion());
			}
			
			return strResultado;
		}
		
		if(GestorFechas.comparaFechas(objAnterior.getCoceFeSuscripcion(), objEndoso.getCertificado().getCoceFeDesde()) == -1) {
			setRespuesta2("La fecha desde no puede ser menor a la fecha de suscripcion: " + GestorFechas.formatDate(objAnterior.getCoceFeSuscripcion(), Constantes.FORMATO_FECHA_UNO));
			bResultado = false;
		}
		
		if(GestorFechas.comparaFechas(objEndoso.getCertificado().getCoceFeHasta(), objEndoso.getCertificado().getCoceFeDesde()) == 1) {
			setRespuesta2("La fecha desde no puede ser mayor a la fecha hasta.");
			bResultado = false;
		}
		
		if(GestorFechas.comparaFechas( (Date) Utilerias.objectIsNull(objAnterior.getCoceFeAnulacion(), Constantes.TIPO_DATO_DATE), objEndoso.getCertificado().getCoceFeHasta()) == -1) {
			setRespuesta2("La fecha hasta debe ser mayor a fecha anualacion: " + GestorFechas.formatDate(objAnterior.getCoceFeAnulacion(), Constantes.FORMATO_FECHA_UNO));
			bResultado = false;
		}
		
		if(GestorFechas.comparaFechas(dDesdePol, objEndoso.getCertificado().getCoceFeDesde()) == -1) {
			setRespuesta2("La fecha desde no puede ser menor a la fecha desde de la poliza: " + GestorFechas.formatDate(dDesdePol, Constantes.FORMATO_FECHA_UNO));
			bResultado = false;
		}
		
		if(GestorFechas.comparaFechas(dHastaPol, objEndoso.getCertificado().getCoceFeHasta()) == 1) {
			setRespuesta2("La fecha hasta no puede ser mayor a la fecha hasta de la poliza: " + GestorFechas.formatDate(dHastaPol, Constantes.FORMATO_FECHA_UNO));
			bResultado = false;
		}
		
		
		if(!bResultado) {
			objEndoso.getCertificado().setCoceFeDesde(objAnterior.getCoceFeDesde());
			objEndoso.getCertificado().setCoceFeHasta(objAnterior.getCoceFeHasta());
			strResultado =  NavigationResults.FAILURE;
		}
				
		return strResultado;
	}
	
	private String validaFechaSus(Certificado objAnterior, ClienteCertifDTO objEndoso) throws Excepciones {
		String strResultado = NavigationResults.SUCCESS;
		boolean bResultado = true;
		Object[] arrFechas;
		Date dDesdePol;
		Date dHastaPol;
		Date dDesdeIniPol;
		
		try {
			arrFechas = this.servicioEndosoDatos.getFechasPoliza(objEndoso.getCertificado().getId());
			dDesdePol = (Date) arrFechas[0];
			dHastaPol = (Date) arrFechas[1];
			dDesdeIniPol = (Date) arrFechas[2];
			
			if(GestorFechas.comparaFechas(dDesdePol, objEndoso.getCertificado().getCoceFeSuscripcion()) >= 0) {
				objEndoso.getCertificado().setCoceFeDesde(objEndoso.getCertificado().getCoceFeSuscripcion());
			} else {
				objEndoso.getCertificado().setCoceFeDesde(dDesdePol);
			}
			objEndoso.getCertificado().setCoceFeHasta(dHastaPol);
			objEndoso.getCertificado().setCoceFeIniCredito(objEndoso.getCertificado().getCoceFeSuscripcion());
			
			
			if(GestorFechas.comparaFechas(objEndoso.getCertificado().getCoceFeSuscripcion(), dDesdeIniPol) == 1) {
				setRespuesta2("La fecha suscripcion no puede ser menor a la fecha de inicio de poliza: " + GestorFechas.formatDate(dDesdeIniPol, Constantes.FORMATO_FECHA_UNO));
				bResultado = false;
			}
			
			if(GestorFechas.comparaFechas(objEndoso.getCertificado().getCoceFeSuscripcion(), dHastaPol) == -1) {
				setRespuesta2("La fecha suscripcion no puede ser mayor a la fecha hasta actual de la poliza: " + GestorFechas.formatDate(dHastaPol, Constantes.FORMATO_FECHA_UNO));
				bResultado = false;
			}
			
			if(!bResultado) {
				strResultado = NavigationResults.FAILURE;
				objEndoso.getCertificado().setCoceFeSuscripcion(objAnterior.getCoceFeSuscripcion());
			}
			
		} catch (Excepciones e) {
			throw new Excepciones("Error: validaFechaSus.", e);
		}
		
		return strResultado;
	}
	
	private void aplicaEndoso(ClienteCertif objClienteAnt, ClienteCertifDTO objEndoso) throws Excepciones {
		String strNumeroEndoso;
		ArrayList<TipoEndosoDTO> arlAutoriza;
		TipoEndosoDTO objTipoEndoso;
		
		try {
			arlAutoriza = (ArrayList<TipoEndosoDTO>) this.servicioEndosoDatos.getAutorizaTpEndoso(getTipoEndoso());
			objEndoso.getCliente().setCocnFeNacimiento(GestorFechas.isNull(objEndoso.getCliente().getCocnFeNacimiento()));
			
			if(arlAutoriza.isEmpty()) {
				if(getTipoEndoso().equals(Constantes.TIPO_ENDOSO_VIGENCIAS) || 
						getTipoEndoso().equals(Constantes.TIPO_ENDOSO_FECHA_SUS) ||
						getTipoEndoso().equals(Constantes.TIPO_ENDOSO_SUMA)) {
					this.servicioEndosoDatos.actualizarObjeto((Certificado) objEndoso.getCertificado());
				} else {										
					this.servicioEndosoDatos.actualizarObjeto((Cliente) objEndoso.getCliente());
				}				
				strNumeroEndoso = this.servicioEndosoDatos.generaEndoso(objClienteAnt, objEndoso, getTipoEndoso(), Constantes.ENDOSO_ESTATUS_APLICADO);
				setRespuesta2("Endoso realizado con �xito, se genero el siguiente folio de endoso: " + strNumeroEndoso);
			} else {
				StringBuilder sbMsgCorreo = new StringBuilder();
				objTipoEndoso = arlAutoriza.get(0);
				strNumeroEndoso = this.servicioEndosoDatos.generaEndoso(objClienteAnt, objEndoso, getTipoEndoso(), Constantes.ENDOSO_ESTATUS_PENDIENTE);
				
				sbMsgCorreo.append("Buen D�a. \n\n");
				sbMsgCorreo.append("Le fu� asignado un endoso con Folio: ").append(strNumeroEndoso).append(" para su autorizaci�n. \n\n\n");
				sbMsgCorreo.append("Favor de verificar y Autorizar o en caso contrario Rechazar el Endoso. \n\n");
				
				Mail.enviar2(new ArrayList<String>(), sbMsgCorreo, "Autorizar Endoso - Folio: " + strNumeroEndoso, Utilerias.quitarCaracteres(objTipoEndoso.getCorreo(), ",", 2), Constantes.DEFAULT_STRING, Constantes.DEFAULT_STRING);
								
				setRespuesta2("Este tipo de endosos requiere autorizaci�n, se asign� y envi� correo al autorizador. Folio de endoso: " + strNumeroEndoso);
			}
		} catch (Excepciones e) {
			throw new Excepciones("Error: aplicaEndoso.", e);
		}
	}
	
	/**
	 * Metodo que manda guardar los datos capturados en pantalla
	 */
	public void guardar() {
		Integer nValida;
		
		try {
			nValida = validaPorcentaje();
			
			switch (nValida) {
				case 0:
					aplicaEndosoBeneficiario();
					cancelar();
					consultaCertificados();
					break;
				case 1:
					FacesUtils.addErrorMessage("* Suma porcentajes de beneficiarios se pasa del 100%.");
					break;
					
				case 2:
					FacesUtils.addErrorMessage("* Suma porcentajes de beneficiarios debe ser del 100%.");
					break;
					
				default:
					break;
			}
		} catch (UnsupportedEncodingException|Excepciones e) {
			setRespuesta2("Error al GUARDAR registro.");
			this.log.error(getRespuesta2(), e);
			throw new FacesException(getRespuesta2(), e);
		}
		
	}
	
	/**
	 * Metodo que cancela datos capturados en pantalla
	 */
	public void cancelar() {
		
		setNuevo(new BeneficiariosDTO());
		getNuevo().setCobeVcampo4(Constantes.BENEFICIARIO_NUEVO);
		
		lstAddBeneficiario = new ArrayList<>();
		lstAddBeneficiario = lstBeneficiarios;
		
		lstDelBeneficiario = new ArrayList<>();
	}
	
	/**
	 * Metodo que agrega a una lista los beneficiarios a guardar
	 */
	public void agregar() {
		try {
			for (ClienteCertifDTO objEndoso : getListaCertificados()) {
				if(objEndoso.isCheckE()){
					if(validaDatos()) {
						nuevo.getId().setCobeCaceCasuCdSucursal(objEndoso.getId().getCoccCasuCdSucursal());
						nuevo.getId().setCobeCaceCarpCdRamo(new Byte(objEndoso.getId().getCoccCarpCdRamo().toString()));
						nuevo.getId().setCobeCaceCapoNuPoliza(objEndoso.getId().getCoccCapoNuPoliza());
						nuevo.getId().setCobeCaceNuCertificado(objEndoso.getId().getCoccNuCertificado());
						nuevo.setCobeVcampo2(servicioEndosoDatos.getDescParentesco(nuevo.getCobeRelacionBenef()));
						nuevo.setCobeFeDesde(new Date());
						lstAddBeneficiario.add(nuevo);
						
						nuevo = new BeneficiariosDTO();
						nuevo.setCobeVcampo4(Constantes.BENEFICIARIO_NUEVO);
					}
					break;
				}
			}
		} catch (Excepciones e) {
			setRespuesta2("Error al agregar registro.");
			this.log.error(getRespuesta2(), e);
			throw new FacesException(getRespuesta2(), e);
		}
	}

	private boolean validaDatos() {
		boolean bRegreso = true;
				
		if(getNuevo().getCobeNombre().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* NOMBRE - Campo Requerido.");
			bRegreso =  false;
		} else if(getNuevo().getCobeApellidoPat().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* APELLIDO PATERNO - Campo Requerido.");
			bRegreso =  false;
		} else if(getNuevo().getCobeApellidoMat().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* APELLIDO MATERNO - Campo Requerido.");
			bRegreso =  false;
		} else if(getNuevo().getCobeRelacionBenef().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* PARENTESCO - Campo Requerido.");
			bRegreso =  false;
		} else if(getNuevo().getCobePoParticipacion() == null || getNuevo().getCobePoParticipacion().equals(new BigDecimal(Constantes.DEFAULT_INT))) {
			FacesUtils.addErrorMessage("* PORCENTAJE - Campo Requerido.");
			bRegreso =  false;
		}

		getNuevo().setCobeVcampo4((String) Utilerias.objectIsNull(getNuevo().getCobeVcampo4(), Constantes.TIPO_DATO_STRING));
		if(!getNuevo().getCobeVcampo4().equals(Constantes.BENEFICIARIO_MODIFICA)) {
			getNuevo().getId().setCobeNuBeneficiario(lstAddBeneficiario.size() + lstDelBeneficiario.size() +1);
			getNuevo().setCobeVcampo4(Constantes.BENEFICIARIO_NUEVO);
			
		}
		
		return bRegreso;
	}
	
	
	/**
	 * Metodo que quita de la lista de nuevos beneficiarios  el beneficiario seleccionado
	 */
	public void quitarNuevoBeneficiario() {
		ArrayList<BeneficiariosDTO> lstTmp;
		
		lstTmp = new ArrayList<>();
		for (BeneficiariosDTO objBeneficiario : getLstAddBeneficiario()) {
			if(getnNumBeneficiario().equals(objBeneficiario.getId().getCobeNuBeneficiario())) {
				objBeneficiario.setCobeVcampo4(Constantes.BENEFICIARIO_ELIMINA);
				lstDelBeneficiario.add(objBeneficiario);
			} else {
				lstTmp.add(objBeneficiario);
			}
		}
		
		lstAddBeneficiario = new ArrayList<>();
		lstAddBeneficiario.addAll(lstTmp);
	}

	/**
	 * Metodo que quita de la lista de nuevos beneficiarios  el beneficiario seleccionado
	 */
	public void setDatosEdit() {
		ArrayList<BeneficiariosDTO> lstTmp;
		
		lstTmp = new ArrayList<>();
		seleccionado.setCobeVcampo4(Constantes.BENEFICIARIO_MODIFICA);
		setNuevo(seleccionado);

		for (BeneficiariosDTO objBeneficiario : getLstAddBeneficiario()) {
			if(objBeneficiario.getId().getCobeNuBeneficiario() != seleccionado.getId().getCobeNuBeneficiario()) {					
				lstTmp.add(objBeneficiario);
			}
		}
		
		lstAddBeneficiario = new ArrayList<>();
		lstAddBeneficiario.addAll(lstTmp);
	}
	
	/**
	 * Metodo que valida el porcentaje de beneficiarios
	 * @return
	 * 	0 = Porcentaje OK
	 *  1 = Porcentaje mayor a 100
	 *  2 - Porcentaje menor a 100
	 * @throws Excepciones
	 */
	private Integer validaPorcentaje() throws Excepciones {
		Integer nPorcentaje, nResultado;
		
		try {
			nResultado = Constantes.DEFAULT_INT;
			nPorcentaje = Constantes.DEFAULT_INT;
			if(!lstAddBeneficiario.isEmpty()) {
				Iterator<BeneficiariosDTO> it;
				it = lstAddBeneficiario.iterator();
				while(it.hasNext()) {
					nPorcentaje = nPorcentaje + it.next().getCobePoParticipacion().intValue();
				}
			}
			
			if(nPorcentaje > 100) {
				nResultado = 1;
			} else if(nPorcentaje < 100) {
				nResultado = 2;
			}
		} catch (RuntimeException e) {
			throw new Excepciones("Error:  validaPorcentaje.", e);
		}
		
		return nResultado;
	}

	private List<Object> consultaParentesco() throws Excepciones {
		List<Object> arlDatos = null;
		ArrayList<Object> arlResultados;
		Object[] arrDatos;
		
		try{
			arlDatos = new  ArrayList<Object>();
			arlResultados = new ArrayList<Object>();
			arlResultados.addAll(this.servicioAltaContratante.getDatosParentesco());
			if (!arlResultados.isEmpty()){
				for (Object bene : arlResultados){
					arrDatos = (Object[]) bene;
					arlDatos.add(new SelectItem(arrDatos[0].toString(), arrDatos[0] + " - " + arrDatos[1]));
				}
			}
		} catch (Exception e) {			
			this.log.error("No se pueden cargar los datos del Parentesco", e);
			throw new Excepciones("Error:  consultaParentesco.", e);
		}
		
		return arlDatos;
	}
	
	private boolean existeProducto(byte nRamo, int nProducto) {
		Boolean blnResultado = false; 
		StringBuilder filtro = new StringBuilder(100);
		List<Object> lstResultados = null;
		
		try {		
			filtro.append("and P.copaDesParametro = 'BENEFICIARIO' \n");
			filtro.append("and P.copaIdParametro  = 'ENDOSOS' \n");
			filtro.append("and P.copaNvalor1      = ").append(nRamo).append(" \n");
			filtro.append("and P.copaNvalor2      = ").append(nProducto).append(" \n");
			filtro.append("and P.copaVvalor3      = '1' \n");
			
			lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
			if(!lstResultados.isEmpty()) {
				blnResultado = true; 
			}
		} catch (Excepciones e) {
			throw new FacesException("Error al buscar registro.", e);
		}
		
		return blnResultado;
	}
	
	private void consultaTipoEndosos() throws Excepciones {
		ArrayList<Object> arlResultados;
		Object[] arrDatos;
		
		try{
			lstComboTpEndoso = new  ArrayList<>();
			arlResultados = new ArrayList<>();
			arlResultados.addAll(this.servicioEndosoDatos.getTipoEndosos());
			if (!arlResultados.isEmpty()){
				for (Object tpEndo : arlResultados){
					arrDatos = (Object[]) tpEndo;
					lstComboTpEndoso.add(new SelectItem(arrDatos[0].toString(), arrDatos[0] + " - " + arrDatos[1]));
				}
			}
		} catch (Exception e) {			
			this.log.error("No se pueden cargar los tipo de endosos", e);
			throw new Excepciones("Error:  consultaParentesco.", e);
		}
		
	}

	private void aplicaEndosoBeneficiario() throws Excepciones, UnsupportedEncodingException {
		String strRespuesta;
		ArrayList<TipoEndosoDTO> arlAutoriza;
		ArrayList<BeneficiariosDTO> arlBeneficiarios;
		TipoEndosoDTO objTipoEndoso;
		
		arlBeneficiarios = new ArrayList<>();
		arlBeneficiarios.addAll(lstAddBeneficiario);
		arlBeneficiarios.addAll(lstDelBeneficiario);
		
		arlAutoriza = (ArrayList<TipoEndosoDTO>) this.servicioEndosoDatos.getAutorizaTpEndoso(getTipoEndoso());
		if(arlAutoriza.isEmpty()) {
			strRespuesta = endozaBeneficiario(arlBeneficiarios);
		} else {
			objTipoEndoso = arlAutoriza.get(0);
			strRespuesta = notificaEndosaBeneficiario(arlBeneficiarios, objTipoEndoso);
		}		
		
		setRespuesta2(strRespuesta);
	}
	
	private String notificaEndosaBeneficiario(ArrayList<BeneficiariosDTO> arlBeneficiarios, TipoEndosoDTO objTipoEndoso) throws Excepciones, UnsupportedEncodingException {
		String strRespuesta; 
		StringBuilder sbNumeroEndoso = new StringBuilder();
		boolean bSigue = true;
		
		for (BeneficiariosDTO objBeneficiario : arlBeneficiarios) {
			if(objBeneficiario.getCobeVcampo4().equals(Constantes.DEFAULT_SIN_DATO)) {
				bSigue = false;
			} else {
				sbNumeroEndoso.append(this.servicioEndosoDatos.generaEndoso(objBeneficiario, objTipoEndoso)).append(" \n");
				bSigue = true;
			}
		}

		if(bSigue){
			StringBuilder sbMsgCorreo = new StringBuilder();
			
			sbMsgCorreo.append("Buen Dia. \n\n");
			sbMsgCorreo.append("Le fue asignado endosos con Folio: ").append(sbNumeroEndoso.toString()).append(" para su autorizacion. \n\n\n");
			sbMsgCorreo.append("Favor de verificar y Autorizar o en caso contrario Rechazar los Endosos. \n\n");
			
			Mail.enviar2(new ArrayList<String>(), sbMsgCorreo, "Autorizar Endoso Varios Folios" , 
						Utilerias.quitarCaracteres(objTipoEndoso.getCorreo(), ",", 2), Constantes.DEFAULT_STRING, Constantes.DEFAULT_STRING);
		}
		
		if(sbNumeroEndoso.toString().equals(Constantes.DEFAULT_STRING)) {
			strRespuesta = "No hubo cambios para aplicar.";
		} else {
			strRespuesta = "Este tipo de endosos requiere autorizacion, se asigno y envio correo al autorizador. Folios de endosos: " + sbNumeroEndoso.toString();
		}
		
		return strRespuesta;
	}

	private String endozaBeneficiario(ArrayList<BeneficiariosDTO> arlBeneficiarios) throws Excepciones {
		String strRespuesta; 
		StringBuilder sbNumeroEndoso = new StringBuilder();
		boolean bSigue = true;
		
		for (BeneficiariosDTO objBeneficiario : arlBeneficiarios) {
			switch (objBeneficiario.getCobeVcampo4()) {
				case Constantes.BENEFICIARIO_MODIFICA:
					this.servicioEndosoDatos.actualizarObjeto(objBeneficiario);
					bSigue = true;
					break;
				case Constantes.BENEFICIARIO_ELIMINA:
					this.servicioEndosoDatos.borrarObjeto(objBeneficiario);
					bSigue = true;
					break;
				case Constantes.BENEFICIARIO_NUEVO:
					this.servicioEndosoDatos.guardarObjeto(objBeneficiario);
					bSigue = true;
					break;
				default:
					bSigue = false;
					break;
			}
			
			if(bSigue) {
				sbNumeroEndoso.append(this.servicioEndosoDatos.generaEndoso(objBeneficiario, null)).append(" \n");
			}
		}
		
		if(sbNumeroEndoso.toString().equals(Constantes.DEFAULT_STRING)) {
			strRespuesta = "No hubo cambios para aplicar.";
		} else {
			strRespuesta = "Se aplicaron con EXITO los cambios, se generaron los siguiente folios de endoso: \n" + sbNumeroEndoso.toString();
		}
		
		return strRespuesta;
	}
	
	

	public ServicioCiudad getServicioCiudad() {
		return servicioCiudad;
	}
	public void setServicioCiudad(ServicioCiudad servicioCiudad) {
		this.servicioCiudad = servicioCiudad;
	}
	public ServicioEndosoDatos getServicioEndosoDatos() {
		return servicioEndosoDatos;
	}
	public void setServicioEndosoDatos(ServicioEndosoDatos servicioEndosoDatos) {
		this.servicioEndosoDatos = servicioEndosoDatos;
	}
	public List<Object> getListaComboCiudades() {
		return new ArrayList<Object>(listaComboCiudades);
	}
	public void setListaComboCiudades(List<Object> listaComboCiudades) {
		this.listaComboCiudades = new ArrayList<Object>(listaComboCiudades);
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public List<ClienteCertifDTO> getListaCertificados() {
		return new ArrayList<ClienteCertifDTO>(listaCertificados);
	}
	public void setListaCertificados(List<ClienteCertifDTO> listaCertificados) {
		this.listaCertificados = new ArrayList<ClienteCertifDTO>(listaCertificados);
	}
	public int getNumPagina() {
		return numPagina;
	}
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getCriterioBusqueda() {
		return criterioBusqueda;
	}
	public void setCriterioBusqueda(String criterioBusqueda) {
		this.criterioBusqueda = criterioBusqueda;
	}
	public void setTdc(String tdc) {
		this.tdc = tdc;
	}
	public String getTdc() {
		return tdc;
	}
	public void setDisplay2(String display2) {
		this.display2 = display2;
	}
	public String getDisplay2() {
		return display2;
	}
	public void setBuc(String buc) {
		this.buc = buc;
	}
	public String getBuc() {
		return buc;
	}
	public void setDisplay3(String display3) {
		this.display3 = display3;
	}
	public String getDisplay3() {
		return display3;
	}
	public void setDisplay4(String display4) {
		this.display4 = display4;
	}
	public String getDisplay4() {
		return display4;
	}
	public void setDisplay5(String display5) {
		this.display5 = display5;
	}
	public String getDisplay5() {
		return display5;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getCanal() {
		return canal;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setPoliza(Integer poliza) {
		this.poliza = poliza;
	}
	public Integer getPoliza() {
		return poliza;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setDisplay1(String display1) {
		this.display1 = display1;
	}
	public String getDisplay1() {
		return display1;
	}
	public void setPolizaEspecifica(String polizaEspecifica) {
		this.polizaEspecifica = polizaEspecifica;
	}
	public String getPolizaEspecifica() {
		return polizaEspecifica;
	}
	public void setMuestraPE(String muestraPE) {
		this.muestraPE = muestraPE;
	}
	public String getMuestraPE() {
		return muestraPE;
	}
	public void setDisplayPE(String displayPE) {
		this.displayPE = displayPE;
	}
	public String getDisplayPE() {
		return displayPE;
	}
	public void setTipoEndoso(String tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	public String getTipoEndoso() {
		return tipoEndoso;
	}
	public void setMuestraEndoso(String muestraEndoso) {
		this.muestraEndoso = muestraEndoso;
	}
	public String getMuestraEndoso() {
		return muestraEndoso;
	}
	public void setDisplayE1(String displayE1) {
		this.displayE1 = displayE1;
	}
	public String getDisplayE1() {
		return displayE1;
	}
	public void setDisplayE2(String displayE2) {
		this.displayE2 = displayE2;
	}
	public String getDisplayE2() {
		return displayE2;
	}
	public void setDisplayE3(String displayE3) {
		this.displayE3 = displayE3;
	}
	public String getDisplayE3() {
		return displayE3;
	}
	public void setDisplayE4(String displayE4) {
		this.displayE4 = displayE4;
	}
	public String getDisplayE4() {
		return displayE4;
	}
	public void setDisplayE5(String displayE5) {
		this.displayE5 = displayE5;
	}
	public String getDisplayE5() {
		return displayE5;
	}
	public void setDisplayE6(String displayE6) {
		this.displayE6 = displayE6;
	}
	public String getDisplayE6() {
		return displayE6;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getColonia() {
		return colonia;
	}
	public void setDelegacionMunicipio(String delegacionMunicipio) {
		this.delegacionMunicipio = delegacionMunicipio;
	}
	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	public String getCalleNumero() {
		return calleNumero;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getRfc() {
		return rfc;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getSexo() {
		return sexo;
	}
	public void setNombreEndoso(String nombreEndoso) {
		this.nombreEndoso = nombreEndoso;
	}
	public String getNombreEndoso() {
		return nombreEndoso;
	}
	public void setApellidoPaternoEndoso(String apellidoPaternoEndoso) {
		this.apellidoPaternoEndoso = apellidoPaternoEndoso;
	}
	public String getApellidoPaternoEndoso() {
		return apellidoPaternoEndoso;
	}
	public void setApellidoMaternoEndoso(String apellidoMaternoEndoso) {
		this.apellidoMaternoEndoso = apellidoMaternoEndoso;
	}
	public String getApellidoMaternoEndoso() {
		return apellidoMaternoEndoso;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getEstado() {
		return estado;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setListaComboEstados(List<Object> listaComboEstados) {
		this.listaComboEstados = listaComboEstados;
	}
	public List<Object> getListaComboEstados() {
		return listaComboEstados;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public Date getFechaNacimiento() {		
		return fechaNacimiento;
	}
	public void setMostrarModalConfirmar(String mostrarModalConfirmar) {
		this.mostrarModalConfirmar = mostrarModalConfirmar;
	}
	public String getMostrarModalConfirmar() {
		return mostrarModalConfirmar;
	}
	public List<BeneficiariosDTO> getLstBeneficiarios() {
		return new ArrayList<BeneficiariosDTO>(lstBeneficiarios);
	}
	public void setLstBeneficiarios(List<BeneficiariosDTO> lstBeneficiarios) {
		this.lstBeneficiarios = new ArrayList<BeneficiariosDTO>(lstBeneficiarios);
	}
	public BeneficiariosDTO getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(BeneficiariosDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
	public String getDisplayE7() {
		return displayE7;
	}
	public void setDisplayE7(String displayE7) {
		this.displayE7 = displayE7;
	}
	public BeneficiariosDTO getNuevo() {
		return nuevo;
	}
	public void setNuevo(BeneficiariosDTO nuevo) {
		this.nuevo = nuevo;
	}
	public ServicioAltaContratante getServicioAltaContratante() {
		return servicioAltaContratante;
	}
	public void setServicioAltaContratante(ServicioAltaContratante servicioAltaContratante) {
		this.servicioAltaContratante = servicioAltaContratante;
	}
	public List<Object> getListaCmbParentesco() {
		return new ArrayList<Object>(listaCmbParentesco);
	}
	public void setListaCmbParentesco(List<Object> listaCmbParentesco) {
		this.listaCmbParentesco = new ArrayList<Object>(listaCmbParentesco);
	}
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	public String getRespuesta2() {
		return respuesta2;
	}
	public void setRespuesta2(String respuesta2) {
		this.respuesta2 = respuesta2;
	}
	
	/**
	 * Metodo que regresa el valor de la propiedad dBtnEndoso
	 * @return dBtnEndoso
	 */
	public String getdBtnEndoso() {
		return dBtnEndoso;
	}	
	
	/**
	 * Metodo que setea valor a propiedad dBtnEndoso
	 * @param dBtnEndoso
	 */
	public void setdBtnEndoso(String dBtnEndoso) {
		this.dBtnEndoso = dBtnEndoso;
	}
	
	/**
	 * Metodo que regresa la lista de beneficiarios a guardar
	 * @return
	 */
	public List<BeneficiariosDTO> getLstAddBeneficiario() {
		return new ArrayList<>(lstAddBeneficiario);
	}
	
	/**
	 * Metodo que setea un valor a la lista.
	 * @param lstAddBeneficiario
	 */
	public void setLstAddBeneficiario(List<BeneficiariosDTO> lstAddBeneficiario) {
		this.lstAddBeneficiario = new ArrayList<>(lstAddBeneficiario);
	}

	/**
	 * Metodo que regresa la lista de beneficiarios a guardar
	 * @return
	 */
	public List<BeneficiariosDTO> getLstDelBeneficiario() {
		return new ArrayList<>(lstDelBeneficiario);
	}
	
	/**
	 * Metodo que setea un valor a la lista.
	 * @param lstDelBeneficiario
	 */
	public void setLstDelBeneficiario(List<BeneficiariosDTO> lstDelBeneficiario) {
		this.lstDelBeneficiario = new ArrayList<>(lstDelBeneficiario);
	}
	
	/**
	 * Metodo que regresa el valor de la propiedad nNumBeneficiario
	 * @return
	 */
	public Integer getnNumBeneficiario() {
		return nNumBeneficiario;
	}
	
	/**
	 * Metodo que setea valor a propiedad nNumBeneficiario
	 * @param nNumBeneficiario
	 */
	public void setnNumBeneficiario(Integer nNumBeneficiario) {
		this.nNumBeneficiario = nNumBeneficiario;
	}

	public String getDisplayE8() {
		return displayE8;
	}
	public void setDisplayE8(String displayE8) {
		this.displayE8 = displayE8;
	}
	public String getDisplayE9() {
		return displayE9;
	}
	public void setDisplayE9(String displayE9) {
		this.displayE9 = displayE9;
	}
	public String getDisplayE10() {
		return displayE10;
	}
	public void setDisplayE10(String displayE10) {
		this.displayE10 = displayE10;
	}
	public Date getFechaDesde() {
		return new Date(fechaDesde.getTime());
	}
	public void setFechaDesde(Date fechaDesde) {
		if(fechaDesde != null) {
			this.fechaDesde = new Date(fechaDesde.getTime());
		}
	}
	public Date getFechaHasta() {
		return new Date(fechaHasta.getTime());
	}
	public void setFechaHasta(Date fechaHasta) {
		if(fechaHasta != null) {
			this.fechaHasta = new Date(fechaHasta.getTime());
		}
	}
	public Date getFechaSuscripcion() {
		return new Date(fechaSuscripcion.getTime());
	}
	public void setFechaSuscripcion(Date fechaSuscripcion) {
		if(fechaSuscripcion != null) {
			this.fechaSuscripcion = new Date(fechaSuscripcion.getTime());
		}
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public List<Object> getLstComboTpEndoso() throws Excepciones {
		consultaTipoEndosos();
		return new ArrayList<>(lstComboTpEndoso);
	}
	public void setLstComboTpEndoso(List<Object> lstComboTpEndoso) {
		this.lstComboTpEndoso = new ArrayList<>(lstComboTpEndoso);
	}
	
	
	
	/**
	 * @return the tipoRFC
	 */
	public Integer getTipoRFC() {
		return tipoRFC;
	}

	/**
	 * @param tipoRFC the tipoRFC to set
	 */
	public void setTipoRFC(Integer tipoRFC) {
		this.tipoRFC = tipoRFC;
	}
	
	

	/**
	 * @return the rfc1
	 */
	public String getRfc1() {
		return rfc1;
	}

	/**
	 * @param rfc1 the rfc1 to set
	 */
	public void setRfc1(String rfc1) {
		this.rfc1 = rfc1;
	}

	/**
	 * @return the rcf2
	 */
	public String getRcf2() {
		return rcf2;
	}

	/**
	 * @param rcf2 the rcf2 to set
	 */
	public void setRcf2(String rcf2) {
		this.rcf2 = rcf2;
	}

	/**
	 * @return the rfc3
	 */
	public String getRfc3() {
		return rfc3;
	}

	/**
	 * @param rfc3 the rfc3 to set
	 */
	public void setRfc3(String rfc3) {
		this.rfc3 = rfc3;
	}

	/**
	 * Metodo para generar el RFC
	 * @throws Excepciones 
	 * @throws NumberFormatException 
	 * */
	public boolean generaRFC() throws NumberFormatException, Excepciones{
		boolean Generado=false;
		String[] valgene ;
		int numCertificado= numeroCliente;
		String RFCgenerado;
		this.log.info("Valor de RFC: "+getRfc()+ " Certificdo: "+numCertificado+"-.");
		try {
			if(getTipoRFC() == 0) {
				RFCgenerado=servicioEndosoDatos.genRFCCert(null, numCertificado);
				}else {
					RFCgenerado=servicioEndosoDatos.genRFCCert(rfc, numCertificado);
				}
			this.log.info("Valor de rfcgene: "+RFCgenerado);
		} catch (Exception e) {
			throw new Excepciones("Error.", e);
		}
		valgene = RFCgenerado.split("\\|");
			rfc=valgene[0];
			if(valgene[1].equals("null")) {
				respuesta2="";
			}else {
			respuesta2=valgene[1];
			}
			setTipoRFC(1);
			if (valgene[0].equals("null")) {
				Generado= true;
			}
			
			if(!Constantes.DEFAULT_STRING.equals(getRfc())) {
			String R1="",R2="",R3="";
		    char [] rfcdiv = valgene[0].toCharArray();
		    //separacion de rfc para editar el endosos
		    if(rfcdiv.length != 0) {
		    	if(rfcdiv.length == 13 || rfcdiv.length == 10) {
		    		for (int i = 0; i < rfcdiv.length; i++) {
						this.log.info("posicion: "+i+"Valor de RFC :"+ rfcdiv[i]);
						if(i <= 3) {
						R1+=rfcdiv[i];
						}else if(i >= 4 && i <= 9) {
							R2+=rfcdiv[i];
						}else if(i >= 10) {
							R3+=rfcdiv[i];
						}
					}
					}else if(rfcdiv.length == 12 || rfcdiv.length == 9) {
						if(rfcdiv.length <= 9) {
				    		for (int i = 0; i < rfcdiv.length; i++) {
								this.log.info("posicion: "+i+"Valor de RFC :"+ rfcdiv[i]);
								if(i <= 2) {
								R1+=rfcdiv[i];
								}else if(i >= 3 && i <= 8) {
									R2+=rfcdiv[i];
								}else if(i >=9) {
									R3+=rfcdiv[i];
								}
							}
				    	}
		    	}					    	
		    } else  {
		    	this.log.info("No se encuentra el rfc");
		    }
		    setRfc1(R1);
			setRcf2(R2);
			setRfc3(R3);
			setRfc(R1+R2+R3);}
		this.log.info("Valores de regreso GENRFC--*****"+rfc+"--"+valgene[0]+"--"+respuesta2+"--"+valgene[1]+"--"+Generado+"RFCnuevo.."+getRfc());
		return Generado;
	}
	
}
