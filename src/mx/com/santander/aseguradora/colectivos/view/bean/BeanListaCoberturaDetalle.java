package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioHistorialCredito;

@Controller
@Scope("request")
public class BeanListaCoberturaDetalle {

	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private String poliza;
	private String certificado;
	private String credito;

	private List<Object> listaHistorial;
	private List<Object> listaHistorialDetalle;
	private String ramo_cont;

	// /////datos credito
	private String estat_tec;
	private String fecha_anu;
	private String producto;
	private String fefincre;
	private String plan;
	private String cober;
	private String cober_desc;
	private String tarif;
	private String monto_cober;

	
	public Log getLog() {
		return log;
	}
	public void setLog(Log log) {
		this.log = log;
	}
	public StringBuilder getMessage() {
		return message;
	}
	public void setMessage(StringBuilder message) {
		this.message = message;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public String getCredito() {
		return credito;
	}
	public void setCredito(String credito) {
		this.credito = credito;
	}
	public List<Object> getListaHistorial() {
		return listaHistorial;
	}
	public void setListaHistorial(List<Object> listaHistorial) {
		this.listaHistorial = listaHistorial;
	}
	public List<Object> getListaHistorialDetalle() {
		return listaHistorialDetalle;
	}
	public void setListaHistorialDetalle(List<Object> listaHistorialDetalle) {
		this.listaHistorialDetalle = listaHistorialDetalle;
	}
	public String getRamo_cont() {
		return ramo_cont;
	}
	public void setRamo_cont(String ramo_cont) {
		this.ramo_cont = ramo_cont;
	}
	public String getEstat_tec() {
		return estat_tec;
	}
	public void setEstat_tec(String estat_tec) {
		this.estat_tec = estat_tec;
	}
	public String getFecha_anu() {
		return fecha_anu;
	}
	public void setFecha_anu(String fecha_anu) {
		this.fecha_anu = fecha_anu;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getFefincre() {
		return fefincre;
	}
	public void setFefincre(String fefincre) {
		this.fefincre = fefincre;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getCober() {
		return cober;
	}
	public void setCober(String cober) {
		this.cober = cober;
	}
	public String getCober_desc() {
		return cober_desc;
	}
	public void setCober_desc(String cober_desc) {
		this.cober_desc = cober_desc;
	}
	public String getTarif() {
		return tarif;
	}
	public void setTarif(String tarif) {
		this.tarif = tarif;
	}
	public String getMonto_cober() {
		return monto_cober;
	}
	public void setMonto_cober(String monto_cober) {
		this.monto_cober = monto_cober;
	}
}
