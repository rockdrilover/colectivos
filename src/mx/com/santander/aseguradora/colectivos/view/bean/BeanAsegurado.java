package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Beneficiarios;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCiudades;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCiudad;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.BeneficiariosUtil;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Filtros;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

/**
 * @author Leobardo Preciado
 *
 */
@Controller
@Scope("session")
public class BeanAsegurado extends BeanBase{

	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioProcesos servicioProceso;
	@Resource
	private ServicioParametros servicioParametro;
	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioCiudad servicioCiudad;
	@Resource
	private ServicioAltaContratante servicioAltaContratante;

	
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}
	public ServicioAltaContratante getServicioAltaContratante() {
		return servicioAltaContratante;
	}
	public void setServicioAltaContratante(
			ServicioAltaContratante servicioAltaContratante) {
		this.servicioAltaContratante = servicioAltaContratante;
	}

	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private String respuesta;
	private List<Object> resultado;
	private Long totalCarga;
	
	private List<Object> listaCmbCiudadesAseg;
	private List<Object> listaCmbColoniaAseg;
	private String strDescEstadoAseg = "";
	private List<Object> listaCmbParentescoBen1;
	private List<Object> listaCmbCiudadesBen1;
	private List<Object> listaCmbColoniaBen1;
	private String strDescEstadoBen1 = "";
	private List<Object> listaCmbParentescoBen2;
	private List<Object> listaCmbCiudadesBen2;
	private List<Object> listaCmbColoniaBen2;
	private String strDescEstadoBen2 = "";
	private List<Object> listaCmbParentescoBen3;
	private List<Object> listaCmbCiudadesBen3;
	private List<Object> listaCmbColoniaBen3;
	private String strDescEstadoBen3 = "";
	private List<Object> listaCmbParentescoBen4;
	private List<Object> listaCmbCiudadesBen4;
	private List<Object> listaCmbColoniaBen4;
	private String strDescEstadoBen4 = "";
	private List<Object> listaCmbParentescoBen5;
	private List<Object> listaCmbCiudadesBen5;
	private List<Object> listaCmbColoniaBen5;
	private String strDescEstadoBen5 = "";
	private Integer nTipoConsCP;
	
	
	
	private List<Object> listaComboCiudades1;
	private List<Object> listaComboCiudades2;
	private List<Object> listaComboCiudades3;
	private List<Object> listaComboCiudades4;
	private List<Object> listaComboCiudades5;
	
	private List<Object> listaComboEstados;
	
	private BeanPreCarga asegurado;
	private PreCargaId id;
	private Beneficiarios beneficiario1;
	private Beneficiarios beneficiario2;
	private Beneficiarios beneficiario3;
	private Beneficiarios beneficiario4;
	private Beneficiarios beneficiario5;
	
	
	private String estado1;
	private String estado2;
	private String estado3;
	private String estado4;
	private String estado5;
	
	private String ciudad1;
	private String ciudad2;
	private String ciudad3;
	private String ciudad4;
	private String ciudad5;
	
	
	private Integer certif = 0;
	
	private BeanRespuesta beanRespuesta;
	private List<BeanRespuesta> lstRespuesta;
	
	private File archivo;
	private String nombreArchivo;

	
	public BeanAsegurado() {
		this.message 				= new StringBuilder();
		this.resultado 				= new ArrayList<Object>();
		this.listaCmbCiudadesAseg   =  new ArrayList<Object>();
		this.listaCmbColoniaAseg    =  new ArrayList<Object>();
		this.listaCmbCiudadesBen1   =  new ArrayList<Object>();
		this.listaCmbColoniaBen1    =  new ArrayList<Object>();
		this.listaCmbCiudadesBen2   =  new ArrayList<Object>();
		this.listaCmbColoniaBen2    =  new ArrayList<Object>();
		this.listaCmbCiudadesBen3   =  new ArrayList<Object>();
		this.listaCmbColoniaBen3    =  new ArrayList<Object>();
		this.listaCmbCiudadesBen4   =  new ArrayList<Object>();
		this.listaCmbColoniaBen4    =  new ArrayList<Object>();
		this.listaCmbCiudadesBen5   =  new ArrayList<Object>();
		this.listaCmbColoniaBen5    =  new ArrayList<Object>();
		this.listaCmbParentescoBen1 = new ArrayList<Object>();
		this.listaCmbParentescoBen2 = new ArrayList<Object>();
		this.listaCmbParentescoBen3 = new ArrayList<Object>();
		this.listaCmbParentescoBen4 = new ArrayList<Object>();
		this.listaCmbParentescoBen5 = new ArrayList<Object>();
		
		this.listaComboCiudades1	= new ArrayList<Object>();
		this.listaComboCiudades2	= new ArrayList<Object>();
		this.listaComboCiudades3	= new ArrayList<Object>();
		this.listaComboCiudades4	= new ArrayList<Object>();
		this.listaComboCiudades5	= new ArrayList<Object>();
		this.listaComboEstados      = new ArrayList<Object>();
		this.asegurado				= new BeanPreCarga();
		this.id						= new PreCargaId();
		this.beneficiario1			= new Beneficiarios();
		this.beneficiario2			= new Beneficiarios();
		this.beneficiario3			= new Beneficiarios();
		this.beneficiario4			= new Beneficiarios();
		this.beneficiario5			= new Beneficiarios();
		this.beanRespuesta			= new BeanRespuesta();
		this.lstRespuesta			= new ArrayList<BeanRespuesta>();
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ASEGURADO);
	}	
	

	public String consultaCodigoPostal() {
		ArrayList<Object> arlResultados;
		String strEstado, strColonia, strPoblacion, strCp = Constantes.DEFAULT_STRING_ID;
		
		try {
			arlResultados = new ArrayList<Object>();
			
			switch (getnTipoConsCP()) {
				case 0:
					strCp = getAsegurado().getCopcCp();
					break;
				case 1:
					strCp = getBeneficiario1().getCobeNcampo1().toString();
					break;
				case 2:
					strCp = getBeneficiario2().getCobeNcampo1().toString();
					break;
				case 3:
					strCp = getBeneficiario3().getCobeNcampo1().toString();
					break;
				case 4:
					strCp = getBeneficiario4().getCobeNcampo1().toString();
					break;
				case 5:
					strCp = getBeneficiario5().getCobeNcampo1().toString();
					break;
			}
			
			arlResultados.addAll(this.servicioAltaContratante.getDatosCP(strCp));
			if(arlResultados.get(0).toString().equals(Constantes.DEFAULT_STRING_ID)) { //No hay datos con el CP.
				strEstado = "0 - SIN ESTADO";
				strColonia = "0 - SIN ESTADO";
				strPoblacion = "0 - SIN ESTADO";
				switch (getnTipoConsCP()) {
					case 0: //Datos Contratante
						getListaCmbColoniaAseg().clear();
						getListaCmbCiudadesAseg().clear();
						setStrDescEstadoAseg(strEstado);
						break;
					case 1:
						getListaCmbColoniaBen1().clear();
						getListaCmbCiudadesBen1().clear();
						setStrDescEstadoBen1(strEstado);
						break;
					case 2:
						getListaCmbColoniaBen2().clear();
						getListaCmbCiudadesBen2().clear();
						setStrDescEstadoBen2(strEstado);
						break;
					case 3:
						getListaCmbColoniaBen3().clear();
						getListaCmbCiudadesBen3().clear();
						setStrDescEstadoBen3(strEstado);
						break;
					case 4:
						getListaCmbColoniaBen4().clear();
						getListaCmbCiudadesBen4().clear();
						setStrDescEstadoBen4(strEstado);
						break;
					case 5:
						getListaCmbColoniaBen5().clear();
						getListaCmbCiudadesBen5().clear();
						setStrDescEstadoBen5(strEstado);
						break;
				}
			} else {
				strEstado = arlResultados.get(2).toString();
				switch (getnTipoConsCP()) {
					case 0: //Datos Contratante
						getListaCmbColoniaAseg().clear();
						getListaCmbColoniaAseg().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesAseg().clear();
						getListaCmbCiudadesAseg().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoAseg(strEstado);
						getAsegurado().setCopcCdEstado(strEstado.split("-")[0].toString().trim());
						break;
					case 1:
						getListaCmbColoniaBen1().clear();
						getListaCmbColoniaBen1().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesBen1().clear();
						getListaCmbCiudadesBen1().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoBen1(strEstado);
						getBeneficiario1().setCobeCdSexo(strEstado.split("-")[0].toString().trim());
						break;
					case 2:
						getListaCmbColoniaBen2().clear();
						getListaCmbColoniaBen2().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesBen2().clear();
						getListaCmbCiudadesBen2().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoBen2(strEstado);
						getBeneficiario2().setCobeCdSexo(strEstado.split("-")[0].toString().trim());
						break;
					case 3:
						getListaCmbColoniaBen3().clear();
						getListaCmbColoniaBen3().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesBen3().clear();
						getListaCmbCiudadesBen3().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoBen3(strEstado);
						getBeneficiario3().setCobeCdSexo(strEstado.split("-")[0].toString().trim());
						break;
					case 4:
						getListaCmbColoniaBen4().clear();
						getListaCmbColoniaBen4().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesBen4().clear();
						getListaCmbCiudadesBen4().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoBen4(strEstado);
						getBeneficiario4().setCobeCdSexo(strEstado.split("-")[0].toString().trim());
						break;
					case 5:
						getListaCmbColoniaBen5().clear();
						getListaCmbColoniaBen5().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesBen5().clear();
						getListaCmbCiudadesBen5().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoBen5(strEstado);
						getBeneficiario5().setCobeCdSexo(strEstado.split("-")[0].toString().trim());
						break;
				}
			}
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar los datos del CP", e);
			return NavigationResults.FAILURE;
		}
	}
	
	private List<Object> consultaParentesco() throws Exception {
		List<Object> arlDatos = null;
		ArrayList<Object> arlResultados;
		Object[] arrDatos;
		
		try{
			arlDatos = new  ArrayList<Object>();
			arlResultados = new ArrayList<Object>();
			arlResultados.addAll(this.servicioAltaContratante.getDatosParentesco());
			if (!arlResultados.isEmpty()){
				for (Object bene : arlResultados){
					arrDatos = (Object[]) bene;
					arlDatos.add(new SelectItem((arrDatos[0].toString()), arrDatos[0] + " - " + arrDatos[1]));
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar los datos del Parentesco", e);
		}
		
		return arlDatos;
	}
	
	public void consultaCiudades1(){
		getListaComboCiudades1().clear();
		for (CartCiudades cartCiudades : consultaCiudad(getEstado1())) {
			this.listaComboCiudades1.add(new SelectItem(new BigDecimal(cartCiudades.getId().getCaciCdCiudad()).toString(),cartCiudades.getCaciDeCiudad()));
		}

	}

	public void consultaCiudades2(){
		getListaComboCiudades2().clear();
		for (CartCiudades cartCiudades : consultaCiudad(getEstado2())) {
			this.listaComboCiudades2.add(new SelectItem(new BigDecimal(cartCiudades.getId().getCaciCdCiudad()).toString(),cartCiudades.getCaciDeCiudad()));
		}
	}
	public void consultaCiudades3(){
		getListaComboCiudades3().clear();
		for (CartCiudades cartCiudades : consultaCiudad(getEstado3())) {
			this.listaComboCiudades3.add(new SelectItem(new BigDecimal(cartCiudades.getId().getCaciCdCiudad()).toString(),cartCiudades.getCaciDeCiudad()));
		}
	}
	public void consultaCiudades4(){
		getListaComboCiudades4().clear();
		for (CartCiudades cartCiudades : consultaCiudad(getEstado4())) {
			this.listaComboCiudades4.add(new SelectItem(new BigDecimal(cartCiudades.getId().getCaciCdCiudad()).toString(),cartCiudades.getCaciDeCiudad()));
		}
	}
	public void consultaCiudades5(){
		getListaComboCiudades5().clear();
		for (CartCiudades cartCiudades : consultaCiudad(getEstado5())) {
			this.listaComboCiudades5.add(new SelectItem(new BigDecimal(cartCiudades.getId().getCaciCdCiudad()).toString(),cartCiudades.getCaciDeCiudad()));
		}
	}

	
	public List<CartCiudades> consultaCiudad(String estado){
		
		StringBuilder filtro = new StringBuilder();

		try{
			BigDecimal cdCiudad = null;
				System.out.println("-----> Estado:"+estado);
				cdCiudad = new BigDecimal(estado);
			
			filtro.append(" and E.id.caciCaesCdEstado = ").append(cdCiudad);
			filtro.append(" ");
			
			return this.servicioCiudad.obtenerObjetos(filtro.toString());
	
		}catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar las ciudades.", e);
			throw new FacesException("No se pueden cargar las ciudades.", e);
		}
	}

	/* Emision manual de Certificados */
	public void emitir(){
		String strValidaDatos;
		int valPrima = 0;
		int valSuma = 0;
		int valResult = 0;
		int anio,mes,dia;
		
		try{
			strValidaDatos = validarDatos();
			if(strValidaDatos.equals(Constantes.DEFAULT_STRING_ID)) {
				PreCarga preCarga 	   	 = null;
				BeanPreCarga bean      	 = getAsegurado();
				Filtros filtro		   	 = new Filtros();
				BigDecimal idCertificado = null; 
				this.totalCarga			 = 0L;
				Date date = new Date();
					
				bean.setCopcCargada("0");
				bean.setCopcRegistro("");
				bean.setCopcDato16("0"); 		//Dato16 = ID_VENTA
				bean.setCopcTipoCliente("1"); 	//TIPO CLIENTE
				setRespuesta("");
				
				getId().setCopcCdSucursal(new Short("1"));
				getId().setCopcTipoRegistro("PYM");
				
				idCertificado = this.servicioCertificado.maxNumeroDeCredito(
						filtro.maxNumeroDeCredito(getId().getCopcCdRamo(), getId().getCopcNumPoliza()));
	
				getId().setCopcIdCertificado(filtro.nextCertificado(idCertificado,getId().getCopcNumPoliza()));
				
				bean.setId(getId());
				bean.setCopcNuCredito(getId().getCopcIdCertificado()); //Se utilza el mismo valor que IdCertificado
				bean.setCopcCuenta(getId().getCopcIdCertificado());    //Se utilza el mismo valor que IdCertificado
				
				System.out.println("--------- TESTING PASSING OBJECTS -------- ");
				System.out.println(bean.getCopcCargada());
				System.out.println(bean.getCopcApPaterno());
				
			    List<Certificado> ListaprodPlan = this.servicioCertificado.obtenerProdPlan(
			    		filtro.plan(bean.getId().getCopcCdRamo(), bean.getId().getCopcNumPoliza()));
			    
			    for (Certificado certificado : ListaprodPlan) {
					
					bean.setCopcCdPlan(certificado.getPlanes().getId().getAlplCdPlan()+"");
					bean.setCopcCdProducto(certificado.getPlanes().getId().getAlplCdProducto()+"");
					id.setCopcSumaAsegurada(certificado.getCoceMtSumaAsegurada()+"");
					bean.setCopcPrima(certificado.getCoceMtPrimaSubsecuente().toPlainString());
					
					bean.setCopcCazbCdSucursal(certificado.getCoceCazbCdSucursal().toString());
					bean.setCopcFrPago(certificado.getCoceFrPago());
					
					
					
					valPrima = certificado.getCoceMtPrimaSubsecuente().intValue();
					valSuma = certificado.getCoceMtSumaAsegurada().intValue();
					valResult = (valPrima * 1000) / valSuma;
					
					
					bean.setCopcDato7("" + valResult);
					

					//LPV dato 8 y dato 6 26/01/2016
					bean.setCopcDato6(certificado.getCoceMtSumaAsegurada()+"");
					bean.setCopcDato8(certificado.getCoceMtSumaAsegurada()+"");
		
				}
			    
			    BeneficiariosUtil benef = new BeneficiariosUtil();
			    List<Beneficiarios> lstBeneficiarios = new ArrayList<Beneficiarios>();
			    
			    lstBeneficiarios.add(getBeneficiario1());
			    lstBeneficiarios.add(getBeneficiario2());
			    lstBeneficiarios.add(getBeneficiario3());
			    lstBeneficiarios.add(getBeneficiario4());
			    lstBeneficiarios.add(getBeneficiario5());
			    
			    
				bean.setCopcDato12(benef.setNumBenef(lstBeneficiarios).replace("$"," ")); 					//NumeroBeneficiarios 	Numerc 2			 LPV
				bean.setCopcDato13(benef.setNombreBeneficiarios(lstBeneficiarios).replace("$"," "));  		// NombreBeneficiarios	Alfanumerico 300	 LPV	
				bean.setCopcDato14(benef.setRelaciones(lstBeneficiarios).replace("$"," "));  				// RelacionAsegurado 	Alfanumerico 10		 LPV
				bean.setCopcDato15(benef.setPorcentajeParticipacion(lstBeneficiarios).replace("$"," "));  	// PorcentajeParticipacion Alfanumerico 30	 LPV
				bean.setCopcDato25(benef.setFechaNacimientoBeneficiario(lstBeneficiarios).replace("$"," ")); // Fecha de Nacimiento Beneficiario  Alfanumerico  50 	 LPV
				bean.setCopcDato49(benef.setCallesBeneficiarios(lstBeneficiarios).replace("$"," "));  		// Calle Beneficiario				Alfanumerico  300	 LPV
				bean.setCopcDato50(benef.setColoniasBeneficiarios(lstBeneficiarios).replace("$"," "));  		// Colonia Beneficiario				Alfanumerico  300	 LPV
				bean.setCopcDato2(benef.setDelgMunBeneficiarios(lstBeneficiarios).replace("$"," "));		  		// Delg/Mun Beneficiario				Alfanumerico  300	 LPV
				bean.setCopcDato1(benef.setEstadosBeneficiarios(lstBeneficiarios).replace("$"," "));  				// Estado Beneficiario				Alfanumerico  100	 LPV
				bean.setCopcDato3(benef.setCpBeneficiarios(lstBeneficiarios).replace("$",""));  			// CodigoPostal Beneficiario			Alfanumerico  100	 LPV
				
				bean.setCopcDato11("EmisionManualDeCertificados");
				bean.setCopcNuPlazo("12");  								// plazo es para todos el mismo 12 
				bean.setCopcDato10("1"); 									 //tipo de poliza 1 poliza no retroactiva 
				
				DateFormat inputFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
				DateFormat dateFormat  = new SimpleDateFormat("dd/MM/yyyy");
		       	
				bean.setCopcFeIngreso(GestorFechas.formatDate(new Date(), "dd/MM/yyyy"));  								// Fecha de Ingreso SYSDATE LPV
				bean.setCopcFeNac(dateFormat.format(inputFormat.parse(bean.getCopcFeNac())));
				bean.setCopcDato33( (dateFormat.format(date)).toString() );  					// sysdate
				bean.setCopcDato34(GestorFechas.addMesesaFecha(GestorFechas.generateDate(bean.getCopcFeIngreso(), "dd/MM/yyyy"), 12));
				bean.setCopcRemesa(GestorFechas.formatDate(date,"yyyyMM"));
		       
				System.out.println(bean.getCopcFeNac());
				System.out.println(bean.getCopcDato25());
				
				String texto =  bean.getCopcPlazoTiempo().toString()+'|'+bean.getCopcDato5().toString()+'|'+
		        bean.getCopcDato4().toString().toString() +'|'+
		        bean.getCopcDato41().toString();
	
				if ( texto.indexOf("SI")<=0 )
				{
				
					preCarga = (PreCarga) ConstruirObjeto.crearObjeto(PreCarga.class, bean);
					this.servicioCarga.guardarObjeto(preCarga);
					
					System.out.println("CANAL:"+getId().getCopcCdSucursal());
					System.out.println("RAMO:"+getId().getCopcCdRamo());
					System.out.println("POLIZA:"+getId().getCopcNumPoliza());
					System.out.println("IDVENTA:"+getAsegurado().getCopcDato16());
					
					this.servicioProceso.emisionMasiva(getId().getCopcCdSucursal(), getId().getCopcCdRamo(), getId().getCopcNumPoliza(),Integer.parseInt(getAsegurado().getCopcDato16()));
					
					this.resultado.clear();
					this.resultado = this.servicioCarga.estatusEmision(new Short("1"), getId().getCopcCdRamo(), getId().getCopcNumPoliza(), 0, "EmisionManualDeCertificados");
					
					getLstRespuesta().clear();
					
					for (int i=0; i<this.resultado.size(); i++){
						
						setBeanRespuesta(new BeanRespuesta());
						
						getBeanRespuesta().setTipo			((BigDecimal)((Object[])this.resultado.get(i))[0]);
						getBeanRespuesta().setDescripcion   (((Object[])this.resultado.get(i))[1].toString());
						getBeanRespuesta().setPoliza		(((Object[])this.resultado.get(i))[2].toString());
						getBeanRespuesta().setSubtotal		((BigDecimal)((Object[])this.resultado.get(i))[3]);
						
						getLstRespuesta().add(getBeanRespuesta());
						
						System.out.println("-------- LISTA RESPUESTA: "+getLstRespuesta());
	
					}
					   
					System.out.println("-------- LISTA RESPUESTA SIZE: "+getLstRespuesta().size());
					
					this.servicioCarga.respaldarErrores(new Short("1"), getId().getCopcCdRamo(), getId().getCopcNumPoliza(), 0, "EmisionManualDeCertificados");
	
					
					setRespuesta("SE CARGO LA INFORAMCION EN EL SISTEMA, FAVOR DE REVISAR LA TABLA DE RESULTADOS.");
				
				}
				else 
				{
					setRespuesta("EL REGISTRO NO PUEDE SER CARGADO, FAVOR DE VALIDAR PREGUNTAS.");
					this.message.append("El regstro no puede ser cargado validar preguntas");
				}
			} else {					
				setRespuesta(strValidaDatos);
			}
		}catch (Exception e) {
			e.printStackTrace();
			setRespuesta("NO SE PUEDE EMITIR, INTENTE MAS TARDE:"+e);
			this.log.error("No se puede emitir, intente mas tarde.", e);
			throw new FacesException("Error al emitir.", e);
		}
		
	}

	private String validarDatos() throws Exception {
    	try {
    		if(getAsegurado().getCopcNumCliente() == null || getAsegurado().getCopcNumCliente().equals(Constantes.DEFAULT_STRING)) {
    			getAsegurado().setCopcNumCliente(Constantes.DEFAULT_STRING_ID);
    		}

    		if(getId().getCopcCdRamo()== null || getId().getCopcCdRamo()== 0) { return "Favor de Seleccionar Ramo";}
			if(getId().getCopcNumPoliza()== null || getId().getCopcNumPoliza()== 0) { return "Favor de Seleccionar Poliza";}
    		if(getAsegurado().getCopcNombre()== null || getAsegurado().getCopcNombre().trim().equals("")) { return "Favor de ingresar Nombre";}
    		if(getAsegurado().getCopcApPaterno()== null || getAsegurado().getCopcApPaterno().trim().equals("")) { return "Favor de ingresar A. Paterno";}
    		if(getAsegurado().getCopcApMaterno()== null || getAsegurado().getCopcApMaterno().trim().equals("")) { return "Favor de ingresar A. Materno";}
    		if(getAsegurado().getCopcFeNac()== null ) { return "Favor de ingresar Fecha Nac.";}
    		if(getAsegurado().getCopcNuCredito()== null || getAsegurado().getCopcNuCredito().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Seleccionar Edo. Civil";}
    		if(getAsegurado().getCopcSexo()== null || getAsegurado().getCopcSexo().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Seleccionar Sexo";}
    		if(getAsegurado().getCopcRfc()== null || getAsegurado().getCopcRfc().trim().equals("")) { return "Favor de ingresar RFC";}
    		//if(getAsegurado().getCopcDato20()== null || getAsegurado().getCopcDato20().trim().equalsIgnoreCase("")){ return "Favor de ingresar Curp";}
    		//if(getAsegurado().getCopcDato40()== null || getAsegurado().getCopcDato40().trim().equals("")) { return "Favor de ingresar la Ocupaci�n";}
    		if(getAsegurado().getCopcCalle()== null || getAsegurado().getCopcCalle().trim().equals("")) { return "Favor de ingresar Direccion";}
    		if(getAsegurado().getCopcCp()== null || getAsegurado().getCopcCp().trim().equals("0")) { return "Favor de ingresar C.P.";}
    		if(getStrDescEstadoAseg()== null || getStrDescEstadoAseg().trim().equals("")){return "Favor de colocar un CP valido";}
    		if(getAsegurado().getCopcColonia() == null || getAsegurado().getCopcColonia().trim().equals("0")){ return "Favor de Seleccionar Colonia";}
    		if(getAsegurado().getCopcDelmunic()== null || getAsegurado().getCopcDelmunic().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n";}
    		if(getAsegurado().getCopcDato21()== null || getAsegurado().getCopcDato21().trim().equals("")) { return "Favor de ingresar Email";}
    		//if(getAsegurado().getCopcTelefono()== null || getAsegurado().getCopcTelefono().trim().equals("")) { return "Favor de ingresar Tel. Domicilio";}
    				    
		    if(!(getBeneficiario1().getCobeNombre().trim().equals(""))){
	    		if(getBeneficiario1().getCobeNombre() == null || getBeneficiario1().getCobeNombre().trim().equals("")) { return "Favor de ingresar Nombre del Beneficiario 1";}
	    		if(getBeneficiario1().getCobeApellidoPat()== null || getBeneficiario1().getCobeApellidoPat().trim().equals("")) { return "Favor de ingresar A. Paterno del Beneficiario 1";}
	    		if(getBeneficiario1().getCobeApellidoMat()== null || getBeneficiario1().getCobeApellidoMat().trim().equals("")) { return "Favor de ingresar A. Materno del Beneficiario 1";}
	    		if(getBeneficiario1().getCobeFeNacimiento()== null ) { return "Favor de ingresar Fecha Nac. del Beneficiario 1";}
	    		if(getBeneficiario1().getCobeRelacionBenef()== null || getBeneficiario1().getCobeRelacionBenef().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Parentesco del Beneficiario 1";}
	    		if(getBeneficiario1().getCobePoParticipacion()== null || getBeneficiario1().getCobePoParticipacion().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Porcentaje del Beneficiario 1";}
	    		if(getBeneficiario1().getCobeVcampo4()== null || getBeneficiario1().getCobeVcampo4().trim().equals("")) { return "Favor de ingresar Direccion del Beneficiario 1";}
	    		if(getBeneficiario1().getCobeNcampo1()== null || getBeneficiario1().getCobeNcampo1().toString().trim().equals("0")) { return "Favor de ingresar C.P. del Beneficiario 1";}
	    		if(getStrDescEstadoBen1()== null || getStrDescEstadoBen1().trim().equals("")){return "Favor de colocar un CP valido del Beneficiario 1";}
	    		if(getBeneficiario1().getCobeVcampo3()== null || getBeneficiario1().getCobeVcampo3().trim().equals("0")){ return "Favor de Seleccionar Colonia del Beneficiario 1";}
	    		if(getAsegurado().getCopcDelmunic()== null || getAsegurado().getCopcDelmunic().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n del Beneficiario 1";}
		    }
		    if(!(getBeneficiario2().getCobeNombre().trim().equals(""))){
	    		if(getBeneficiario2().getCobeNombre()== null || getBeneficiario2().getCobeNombre().trim().equals("")) { return "Favor de ingresar Nombre del Beneficiario 2";}
	    		if(getBeneficiario2().getCobeApellidoPat()== null || getBeneficiario2().getCobeApellidoPat().trim().equals("")) { return "Favor de ingresar A. Paterno del Beneficiario 2";}
	    		if(getBeneficiario2().getCobeApellidoMat()== null || getBeneficiario2().getCobeApellidoMat().trim().equals("")) { return "Favor de ingresar A. Materno del Beneficiario 2";}
	    		if(getBeneficiario2().getCobeFeNacimiento()== null ) { return "Favor de ingresar Fecha Nac. del Beneficiario 2";}
	    		if(getBeneficiario2().getCobeRelacionBenef()== null || getBeneficiario2().getCobeRelacionBenef().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Parentesco del Beneficiario 2";}
	    		if(getBeneficiario2().getCobePoParticipacion()== null || getBeneficiario2().getCobePoParticipacion().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Porcentaje del Beneficiario 2";}
	    		if(getBeneficiario2().getCobeVcampo4()== null || getBeneficiario2().getCobeVcampo4().trim().equals("")) { return "Favor de ingresar Direccion del Beneficiario 2";}
	    		if(getBeneficiario2().getCobeNcampo1()== null || getBeneficiario2().getCobeNcampo1().toString().trim().equals("0")) { return "Favor de ingresar C.P. del Beneficiario 2";}
	    		if(getStrDescEstadoBen2()== null || getStrDescEstadoBen2().trim().equals("")){return "Favor de colocar un CP valido del Beneficiario 2";}
	    		if(getBeneficiario2().getCobeVcampo3()== null || getBeneficiario2().getCobeVcampo3().trim().equals("0")){ return "Favor de Seleccionar Colonia del Beneficiario 2";}
	    		if(getAsegurado().getCopcDelmunic()== null || getAsegurado().getCopcDelmunic().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n del Beneficiario 2";}
		    }
		    if(!(getBeneficiario3().getCobeNombre().trim().equals(""))){
	    		if(getBeneficiario3().getCobeNombre()== null || getBeneficiario3().getCobeNombre().trim().equals("")) { return "Favor de ingresar Nombre del Beneficiario 3";}
	    		if(getBeneficiario3().getCobeApellidoPat()== null || getBeneficiario3().getCobeApellidoPat().trim().equals("")) { return "Favor de ingresar A. Paterno del Beneficiario 3";}
	    		if(getBeneficiario3().getCobeApellidoMat()== null || getBeneficiario3().getCobeApellidoMat().trim().equals("")) { return "Favor de ingresar A. Materno del Beneficiario 3";}
	    		if(getBeneficiario3().getCobeFeNacimiento()== null ) { return "Favor de ingresar Fecha Nac. del Beneficiario 3";}
	    		if(getBeneficiario3().getCobeRelacionBenef()== null || getBeneficiario3().getCobeRelacionBenef().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Parentesco del Beneficiario 3";}
	    		if(getBeneficiario3().getCobePoParticipacion()== null || getBeneficiario3().getCobePoParticipacion().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Porcentaje del Beneficiario 3";}
	    		if(getBeneficiario3().getCobeVcampo4()== null || getBeneficiario3().getCobeVcampo4().trim().equals("")) { return "Favor de ingresar Direccion del Beneficiario 3";}
	    		if(getBeneficiario3().getCobeNcampo1()== null || getBeneficiario3().getCobeNcampo1().toString().trim().equals("0")) { return "Favor de ingresar C.P. del Beneficiario 3";}
	    		if(getStrDescEstadoBen3()== null || getStrDescEstadoBen3().trim().equals("")){return "Favor de colocar un CP valido del Beneficiario 3";}
	    		if(getBeneficiario3().getCobeVcampo3()== null || getBeneficiario3().getCobeVcampo3().trim().equals("0")){ return "Favor de Seleccionar Colonia del Beneficiario 3";}
	    		if(getAsegurado().getCopcDelmunic()== null || getAsegurado().getCopcDelmunic().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n del Beneficiario 3";}
		    }
		    if(!(getBeneficiario4().getCobeNombre().trim().equals(""))){
	    		if(getBeneficiario4().getCobeNombre()== null || getBeneficiario4().getCobeNombre().trim().equals("")) { return "Favor de ingresar Nombre del Beneficiario 4";}
	    		if(getBeneficiario4().getCobeApellidoPat()== null || getBeneficiario4().getCobeApellidoPat().trim().equals("")) { return "Favor de ingresar A. Paterno del Beneficiario 4";}
	    		if(getBeneficiario4().getCobeApellidoMat()== null || getBeneficiario4().getCobeApellidoMat().trim().equals("")) { return "Favor de ingresar A. Materno del Beneficiario 4";}
	    		if(getBeneficiario4().getCobeFeNacimiento()== null ) { return "Favor de ingresar Fecha Nac. del Beneficiario 4";}
	    		if(getBeneficiario4().getCobeRelacionBenef()== null || getBeneficiario4().getCobeRelacionBenef().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Parentesco del Beneficiario 4";}
	    		if(getBeneficiario4().getCobePoParticipacion()== null || getBeneficiario4().getCobePoParticipacion().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Porcentaje del Beneficiario 4";}
	    		if(getBeneficiario4().getCobeVcampo4()== null || getBeneficiario4().getCobeVcampo4().trim().equals("")) { return "Favor de ingresar Direccion del Beneficiario 4";}
	    		if(getBeneficiario4().getCobeNcampo1()== null || getBeneficiario4().getCobeNcampo1().toString().trim().equals("0")) { return "Favor de ingresar C.P. del Beneficiario 4";}
	    		if(getStrDescEstadoBen4()== null || getStrDescEstadoBen4().trim().equals("")){return "Favor de colocar un CP valido del Beneficiario 4";}
	    		if(getBeneficiario4().getCobeVcampo3()== null || getBeneficiario4().getCobeVcampo3().trim().equals("0")){ return "Favor de Seleccionar Colonia del Beneficiario 4";}
	    		if(getAsegurado().getCopcDelmunic()== null || getAsegurado().getCopcDelmunic().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n del Beneficiario 4";}
		    }
		    if(!(getBeneficiario5().getCobeNombre().trim().equals(""))){
	    		if(getBeneficiario5().getCobeNombre()== null || getBeneficiario5().getCobeNombre().trim().equals("")) { return "Favor de ingresar Nombre del Beneficiario 5";}
	    		if(getBeneficiario5().getCobeApellidoPat()== null || getBeneficiario5().getCobeApellidoPat().trim().equals("")) { return "Favor de ingresar A. Paterno del Beneficiario 5";}
	    		if(getBeneficiario5().getCobeApellidoMat()== null || getBeneficiario5().getCobeApellidoMat().trim().equals("")) { return "Favor de ingresar A. Materno del Beneficiario 5";}
	    		if(getBeneficiario5().getCobeFeNacimiento()== null ) { return "Favor de ingresar Fecha Nac. del Beneficiario 5";}
	    		if(getBeneficiario5().getCobeRelacionBenef()== null || getBeneficiario5().getCobeRelacionBenef().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Parentesco del Beneficiario 5";}
	    		if(getBeneficiario5().getCobePoParticipacion()== null || getBeneficiario5().getCobePoParticipacion().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Ingresar Porcentaje del Beneficiario 5";}
	    		if(getBeneficiario5().getCobeVcampo4()== null || getBeneficiario5().getCobeVcampo4().trim().equals("")) { return "Favor de ingresar Direccion del Beneficiario 5";}
	    		if(getBeneficiario5().getCobeNcampo1()== null || getBeneficiario5().getCobeNcampo1().toString().trim().equals("0")) { return "Favor de ingresar C.P. del Beneficiario 5";}
	    		if(getStrDescEstadoBen5()== null || getStrDescEstadoBen5().trim().equals("")){return "Favor de colocar un CP valido del Beneficiario 5";}
	    		if(getBeneficiario5().getCobeVcampo3()== null || getBeneficiario5().getCobeVcampo3().trim().equals("0")){ return "Favor de Seleccionar Colonia del Beneficiario 5";}
	    		if(getAsegurado().getCopcDelmunic()== null || getAsegurado().getCopcDelmunic().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n del Beneficiario 5";}
		    }
       	} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: BeanAltaContratante.validarDatos(): " + e.getMessage());
		}
    	
//		return "PRUEBA DE VALIDACION DE DATOS";
    	return Constantes.DEFAULT_STRING_ID;
	}

	public String erroresEmision(){
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		Map<String, Object> inParams = new HashMap<String, Object>();
		File objFile;
		StringBuffer nombreSalida, nombreNewSalida;
		FacesContext context;
		HttpServletResponse response;
		ServletOutputStream out;
		
		if(getId().getCopcCdRamo() != null && getId().getCopcNumPoliza() != null){
			try {
				nombreSalida = new StringBuffer();
				nombreNewSalida = new StringBuffer();
				if (this.nombreArchivo == null || this.nombreArchivo.isEmpty())	
					this.nombreArchivo = "*";
				
				inParams.put("canal", new Short("1"));
				inParams.put("ramo", getId().getCopcCdRamo());
				inParams.put("poliza", getId().getCopcNumPoliza());
				inParams.put("idVenta", 0);
				inParams.put("archivo", this.nombreArchivo);
				nombreSalida.append("ErroresCarga_1_" + getId().getCopcCdRamo() + "_");
				nombreSalida.append(getId().getCopcNumPoliza() + ".csv");
				nombreNewSalida.append("Errores_" + getId().getCopcCdRamo() + "_");
				nombreNewSalida.append(getId().getCopcNumPoliza());

				rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/" + nombreSalida);
				rutaReporte = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ErroresCarga.jrxml");
				
				this.servicioCarga.reporteErrores(rutaTemporal, rutaReporte, inParams);
				objFile = crearArchivoErrorCatalogo(new File(rutaTemporal), nombreNewSalida.toString());				
				
				if(objFile != null) {
					context = FacesContext.getCurrentInstance();
					if(!context.getResponseComplete()){
						input = new FileInputStream(objFile);
						bytes = new byte[1000];
						String contentType = "application/vnd.ms-excel";
						response = (HttpServletResponse) context.getExternalContext().getResponse();
						response.setContentType(contentType);
						
						DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
				    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + objFile.getName());
						
						out = response.getOutputStream();
						
						while((read = input.read(bytes))!= -1){
							out.write(bytes, 0 , read);
						}
						
						input.close();
						out.flush();
						out.close();
						objFile.delete();
						
						context.responseComplete();
					}
					// Borramos los datos de la precarga y carga.
					this.servicioCarga.borrarDatosTemporales(new Short("1"), getId().getCopcCdRamo(), getId().getCopcNumPoliza(), 0, "EmisionManualDeCertificados","*");
					Utilerias.resetMessage(message);
					setRespuesta("");
					getLstRespuesta().clear();
					this.message.append("Archivo de errores descargado.");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
				} else {
					this.respuesta = "Ocurrio un error al descargar archivo de errores, intentelo nuevamente.";
				}
				
				return NavigationResults.SUCCESS;
			} catch (Exception e) {
				e.printStackTrace();
				this.message.append("Error al generar el archivo de errores.");
				this.log.error(message.toString(), e);
				this.respuesta = this.message.toString();
				return NavigationResults.FAILURE;
			}			
		} else {
			this.message.append("El archivo de errores s�lo se puede descargar hasta que se haya realizado el proceso de emisi�n.");
			this.log.debug(message.toString());
			this.respuesta = this.message.toString();
			return NavigationResults.RETRY;
		}	
	}
	
	private File crearArchivoErrorCatalogo(File file, String nombreNewSalida) {
		CsvReader csv = null;
		FileReader fr;		
		List<Object> lista = null;
		String[] arrDatos, arrColumnas;
		Integer i = 0;
		String[] headers = null;
		Map<String, String> mh;
		Archivo objArchivo = null;
		
		try {
			arrColumnas = new String[8];
			fr = new FileReader(file);
			csv = new CsvReader(fr, ",".charAt(0));
			if(csv.readHeaders()) {
				headers = csv.getHeaders();
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
					if(!h.equals("Tabla")) {
						arrColumnas[i] = h;
						i++;
					}					
				}
				
				lista = new ArrayList<Object>();
				while(csv.readRecord()) {
					arrDatos = new String[8];
					arrDatos[0] = csv.get(mh.get("canal"));
					arrDatos[1] = csv.get(mh.get("ramo"));
					arrDatos[2] = csv.get(mh.get("poliza"));
					arrDatos[3] = csv.get(mh.get("identificador"));
					arrDatos[4] = csv.get(mh.get("producto"));
					arrDatos[5] = csv.get(mh.get("subproducto"));
					arrDatos[6] = csv.get(mh.get("archivocarga"));
					try {
						arrDatos[7] = csv.get(mh.get("tabla")) + servicioParametro.getErrorCatalogo(csv.get(mh.get("estatus")));
					} catch (Exception e) {
						arrDatos[7] = csv.get(mh.get("tabla"));
					}
					
					lista.add(arrDatos);
				}
				
				csv.close();
				fr.close();
				file.delete();
				
				objArchivo = new Archivo(FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator, nombreNewSalida, ".csv");
				objArchivo.copiarArchivo(arrColumnas, lista, ",");
			}
		} catch (IOException e) {
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		} catch (Exception e) {
			this.message.append("No se puede cargar la informacion del archivo.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
		}

		return objArchivo.getArchivo();
	}
	
	/* GETTERS Y SETTERS*/
	public String getRespuesta() {
		return respuesta;
	}
	
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	public ServicioCiudad getServicioCiudad() {
		return servicioCiudad;
	}
	
	public void setServicioCiudad(ServicioCiudad servicioCiudad) {
		this.servicioCiudad = servicioCiudad;
	}
	/**
	 * @return the servicioProceso
	 */
	public ServicioProcesos getServicioProceso() {
		return servicioProceso;
	}
	/**
	 * @param servicioProceso the servicioProceso to set
	 */
	public void setServicioProceso(ServicioProcesos servicioProceso) {
		this.servicioProceso = servicioProceso;
	}
	
	/**
	 * @return the servicioParametro
	 */
	public ServicioParametros getServicioParametro() {
		return servicioParametro;
	}
	/**
	 * @param servicioParametro the servicioParametro to set
	 */
	public void setServicioParametro(ServicioParametros servicioParametro) {
		this.servicioParametro = servicioParametro;
	}
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public List<Object> getListaComboCiudades1() {
		return listaComboCiudades1;
	}
	
	public void setListaComboCiudades1(List<Object> listaComboCiudades1) {
		this.listaComboCiudades1 = listaComboCiudades1;
	}

	public List<Object> getListaComboCiudades2() {
		return listaComboCiudades2;
	}
	
	public void setListaComboCiudades2(List<Object> listaComboCiudades2) {
		this.listaComboCiudades2 = listaComboCiudades2;
	}
	public List<Object> getListaComboCiudades3() {
		return listaComboCiudades3;
	}
	
	public void setListaComboCiudades3(List<Object> listaComboCiudades3) {
		this.listaComboCiudades3 = listaComboCiudades3;
	}
	public List<Object> getListaComboCiudades4() {
		return listaComboCiudades4;
	}
	
	public void setListaComboCiudades4(List<Object> listaComboCiudades4) {
		this.listaComboCiudades4 = listaComboCiudades4;
	}
	public List<Object> getListaComboCiudades5() {
		return listaComboCiudades5;
	}
	
	public void setListaComboCiudades5(List<Object> listaComboCiudades5) {
		this.listaComboCiudades5 = listaComboCiudades5;
	}
	
	public void setListaComboEstados(List<Object> listaComboEstados) {
		this.listaComboEstados = listaComboEstados;
	}
	public List<Object> getListaComboEstados() {
		return listaComboEstados;
	}

	public BeanPreCarga getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(BeanPreCarga asegurado) {
		this.asegurado = asegurado;
	}

	public void setId(PreCargaId id) {
		this.id = id;
	}
	
	public PreCargaId getId() {
		return id;
	}
	
	public Beneficiarios getBeneficiario1() {
		return beneficiario1;
	}

	public void setBeneficiario1(Beneficiarios beneficiario1) {
		this.beneficiario1 = beneficiario1;
	}

	public Beneficiarios getBeneficiario2() {
		return beneficiario2;
	}

	public void setBeneficiario2(Beneficiarios beneficiario2) {
		this.beneficiario2 = beneficiario2;
	}

	public Beneficiarios getBeneficiario3() {
		return beneficiario3;
	}

	public void setBeneficiario3(Beneficiarios beneficiario3) {
		this.beneficiario3 = beneficiario3;
	}


	public Beneficiarios getBeneficiario4() {
		return beneficiario4;
	}


	public void setBeneficiario4(Beneficiarios beneficiario4) {
		this.beneficiario4 = beneficiario4;
	}


	public Beneficiarios getBeneficiario5() {
		return beneficiario5;
	}


	public void setBeneficiario5(Beneficiarios beneficiario5) {
		this.beneficiario5 = beneficiario5;
	}


	public String getEstado1() {
		return estado1;
	}

	public void setEstado1(String estado1) {
		this.estado1 = estado1;
	}

	public String getEstado2() {
		return estado2;
	}

	public void setEstado2(String estado2) {
		this.estado2 = estado2;
	}

	public String getEstado3() {
		return estado3;
	}

	public void setEstado3(String estado3) {
		this.estado3 = estado3;
	}

	public String getEstado4() {
		return estado4;
	}

	public void setEstado4(String estado4) {
		this.estado4 = estado4;
	}

	public String getEstado5() {
		return estado5;
	}

	public void setEstado5(String estado5) {
		this.estado5 = estado5;
	}

	public String getCiudad1() {
		return ciudad1;
	}
	public void setCiudad1(String ciudad1) {
		this.ciudad1 = ciudad1;
	}

	public String getCiudad2() {
		return ciudad2;
	}

	public void setCiudad2(String ciudad2) {
		this.ciudad2 = ciudad2;
	}

	public String getCiudad3() {
		return ciudad3;
	}

	public void setCiudad3(String ciudad3) {
		this.ciudad3 = ciudad3;
	}

	public String getCiudad4() {
		return ciudad4;
	}

	public void setCiudad4(String ciudad4) {
		this.ciudad4 = ciudad4;
	}

	public String getCiudad5() {
		return ciudad5;
	}

	public void setCiudad5(String ciudad5) {
		this.ciudad5 = ciudad5;
	}
	
	public BeanRespuesta getBeanRespuesta() {
		return beanRespuesta;
	}
	
	public void setBeanRespuesta(BeanRespuesta beanRespuesta) {
		this.beanRespuesta = beanRespuesta;
	}
	
	public List<BeanRespuesta> getLstRespuesta() {
		return lstRespuesta;
	}
	
	public void setLstRespuesta(List<BeanRespuesta> lstRespuesta) {
		this.lstRespuesta = lstRespuesta;
	}
	

	public File getArchivo() {
		return archivo;
	}
	
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	public Integer getnTipoConsCP() {
		return nTipoConsCP;
	}
	public void setnTipoConsCP(Integer nTipoConsCP) {
		this.nTipoConsCP = nTipoConsCP;
	}
	public List<Object> getListaCmbCiudadesAseg() {
		return listaCmbCiudadesAseg;
	}
	public void setListaCmbCiudadesAseg(List<Object> listaCmbCiudadesAseg) {
		this.listaCmbCiudadesAseg = listaCmbCiudadesAseg;
	}
	public List<Object> getListaCmbColoniaAseg() {
		return listaCmbColoniaAseg;
	}
	public void setListaCmbColoniaAseg(List<Object> listaCmbColoniaAseg) {
		this.listaCmbColoniaAseg = listaCmbColoniaAseg;
	}
	public String getStrDescEstadoAseg() {
		return strDescEstadoAseg;
	}
	public void setStrDescEstadoAseg(String strDescEstadoAseg) {
		this.strDescEstadoAseg = strDescEstadoAseg;
	}
	
	public List<Object> getListaCmbCiudadesBen1() {
		return listaCmbCiudadesBen1;
	}


	public void setListaCmbCiudadesBen1(List<Object> listaCmbCiudadesBen1) {
		this.listaCmbCiudadesBen1 = listaCmbCiudadesBen1;
	}


	public List<Object> getListaCmbColoniaBen1() {
		return listaCmbColoniaBen1;
	}


	public void setListaCmbColoniaBen1(List<Object> listaCmbColoniaBen1) {
		this.listaCmbColoniaBen1 = listaCmbColoniaBen1;
	}


	public String getStrDescEstadoBen1() {
		return strDescEstadoBen1;
	}


	public void setStrDescEstadoBen1(String strDescEstadoBen1) {
		this.strDescEstadoBen1 = strDescEstadoBen1;
	}


	public List<Object> getListaCmbCiudadesBen2() {
		return listaCmbCiudadesBen2;
	}


	public void setListaCmbCiudadesBen2(List<Object> listaCmbCiudadesBen2) {
		this.listaCmbCiudadesBen2 = listaCmbCiudadesBen2;
	}


	public List<Object> getListaCmbColoniaBen2() {
		return listaCmbColoniaBen2;
	}


	public void setListaCmbColoniaBen2(List<Object> listaCmbColoniaBen2) {
		this.listaCmbColoniaBen2 = listaCmbColoniaBen2;
	}


	public String getStrDescEstadoBen2() {
		return strDescEstadoBen2;
	}


	public void setStrDescEstadoBen2(String strDescEstadoBen2) {
		this.strDescEstadoBen2 = strDescEstadoBen2;
	}


	public List<Object> getListaCmbCiudadesBen3() {
		return listaCmbCiudadesBen3;
	}


	public void setListaCmbCiudadesBen3(List<Object> listaCmbCiudadesBen3) {
		this.listaCmbCiudadesBen3 = listaCmbCiudadesBen3;
	}


	public List<Object> getListaCmbColoniaBen3() {
		return listaCmbColoniaBen3;
	}


	public void setListaCmbColoniaBen3(List<Object> listaCmbColoniaBen3) {
		this.listaCmbColoniaBen3 = listaCmbColoniaBen3;
	}


	public String getStrDescEstadoBen3() {
		return strDescEstadoBen3;
	}


	public void setStrDescEstadoBen3(String strDescEstadoBen3) {
		this.strDescEstadoBen3 = strDescEstadoBen3;
	}


	public List<Object> getListaCmbCiudadesBen4() {
		return listaCmbCiudadesBen4;
	}


	public void setListaCmbCiudadesBen4(List<Object> listaCmbCiudadesBen4) {
		this.listaCmbCiudadesBen4 = listaCmbCiudadesBen4;
	}


	public List<Object> getListaCmbColoniaBen4() {
		return listaCmbColoniaBen4;
	}

	public void setListaCmbColoniaBen4(List<Object> listaCmbColoniaBen4) {
		this.listaCmbColoniaBen4 = listaCmbColoniaBen4;
	}

	public String getStrDescEstadoBen4() {
		return strDescEstadoBen4;
	}

	public void setStrDescEstadoBen4(String strDescEstadoBen4) {
		this.strDescEstadoBen4 = strDescEstadoBen4;
	}

	public List<Object> getListaCmbCiudadesBen5() {
		return listaCmbCiudadesBen5;
	}

	public void setListaCmbCiudadesBen5(List<Object> listaCmbCiudadesBen5) {
		this.listaCmbCiudadesBen5 = listaCmbCiudadesBen5;
	}

	public List<Object> getListaCmbColoniaBen5() {
		return listaCmbColoniaBen5;
	}

	public void setListaCmbColoniaBen5(List<Object> listaCmbColoniaBen5) {
		this.listaCmbColoniaBen5 = listaCmbColoniaBen5;
	}

	public String getStrDescEstadoBen5() {
		return strDescEstadoBen5;
	}

	public void setStrDescEstadoBen5(String strDescEstadoBen5) {
		this.strDescEstadoBen5 = strDescEstadoBen5;
	}
	
	public List<Object> getListaCmbParentescoBen1() throws Exception {
		listaCmbParentescoBen1.addAll(consultaParentesco());
		return listaCmbParentescoBen1;
	}

	public void setListaCmbParentescoBen1(List<Object> listaCmbParentescoBen1) {
		this.listaCmbParentescoBen1 = listaCmbParentescoBen1;
	}

	public List<Object> getListaCmbParentescoBen2() throws Exception {
		listaCmbParentescoBen2.addAll(consultaParentesco());
		return listaCmbParentescoBen2;
	}

	public void setListaCmbParentescoBen2(List<Object> listaCmbParentescoBen2) {
		this.listaCmbParentescoBen2 = listaCmbParentescoBen2;
	}


	public List<Object> getListaCmbParentescoBen3() throws Exception {
		listaCmbParentescoBen3.addAll(consultaParentesco());
		return listaCmbParentescoBen3;
	}


	public void setListaCmbParentescoBen3(List<Object> listaCmbParentescoBen3) {
		this.listaCmbParentescoBen3 = listaCmbParentescoBen3;
	}


	public List<Object> getListaCmbParentescoBen4() throws Exception {
		listaCmbParentescoBen4.addAll(consultaParentesco());
		return listaCmbParentescoBen4;
	}


	public void setListaCmbParentescoBen4(List<Object> listaCmbParentescoBen4) {
		this.listaCmbParentescoBen4 = listaCmbParentescoBen4;
	}


	public List<Object> getListaCmbParentescoBen5() throws Exception {
		listaCmbParentescoBen5.addAll(consultaParentesco());
		return listaCmbParentescoBen5;
	}


	public void setListaCmbParentescoBen5(List<Object> listaCmbParentescoBen5) {
		this.listaCmbParentescoBen5 = listaCmbParentescoBen5;
	}
}
