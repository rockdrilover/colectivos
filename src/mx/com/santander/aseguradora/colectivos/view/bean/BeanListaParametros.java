/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CatEndosos;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstado;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanListaParametros extends BeanBase implements InitializingBean {

	@Resource
	private ServicioParametros servicioParametros;
	
	@Resource
	private ServicioProducto servicioProducto;
	


	@Resource
	private ServicioPlan servicioPlan;
	
	@Resource
	private ServicioEstado servicioEstado;
	@Resource
	private ServicioCertificado servicioCertificado;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private List<Object> listaComboPolizas;
	private List<Object> listaComboStatus;
	private List<Object> listaTipoPolizas;
	private List<Object> listaTipoEndoso;
	private Integer inCanal;
	private HtmlSelectOneMenu inPoliza;
	private HtmlSelectOneMenu inStatus;
	private HtmlSelectOneMenu inRamo;
	private HtmlSelectOneMenu inIdVenta;
	private HtmlSelectOneMenu inOperEndoso;
	private HtmlSelectOneMenu rreporte;
	
	private HtmlSelectOneMenu inRamoRC;
	private HtmlSelectOneMenu inIdVentaRC;
	private HtmlSelectOneMenu inPolizaRC;
	//dfg
	private HtmlSelectOneMenu inProducto;
	private List<Object> listaComboProductos;
	private Short ramo;
	private Integer producto;
	private Integer poliza;
	private HtmlSelectOneMenu inTipoPrima;
	private HtmlSelectOneMenu inPlan;
	private HtmlSelectOneMenu inEstado;
	private List<Object> listaComboPlan;
	private List<Object> listaComboEstados;
	private HtmlSelectOneMenu cdVenta;
	private HtmlSelectOneMenu descripCdVenta;;
	
	private HtmlInputHidden inPolizaHardCode;

	private List<Object> listaComboIdVenta; 
	private Boolean habilitaComboIdVenta; 
	private Boolean habilitaPoliza;
	private Boolean habilitaStatus;
	private Boolean habilitaComboProducto;
	private Boolean habilitaComboPlan;
	private Boolean habilitaComboEstado;
	
	
	//lpv
	private List<Object> listaComboCentroCostos;
	private List<Object> listaComboSegmento;
	
	private List<Object> listaTipoCobertura;
	
	public BeanListaParametros() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.listaComboPolizas = new ArrayList<Object>();
		this.listaComboStatus = new ArrayList<Object>();
		this.listaTipoPolizas = new ArrayList<Object>();
		this.listaComboIdVenta = new ArrayList<Object>();
		this.listaComboProductos = new ArrayList<Object>();
		this.listaComboPlan = new ArrayList<Object>();
		this.listaComboEstados = new ArrayList<Object>();
		this.listaTipoEndoso = new ArrayList<Object>();
		this.inCanal = 1;
		this.setHabilitaComboIdVenta(true);
		this.setHabilitaPoliza(true);
		this.setHabilitaStatus(true);
		this.ramo = null;
		this.log.debug("BeanListaParametros creado");
		
		//lpv
		this.listaComboCentroCostos = new ArrayList<Object>();
		this.listaComboSegmento		= new ArrayList<Object>();
		this.listaTipoCobertura		= new ArrayList<Object>();

        this.darrsiVigencias = new ArrayList<SelectItem>();
		this.darrsiTipoReporte = new ArrayList<SelectItem>();
        this.subLoadTipoReportes();

	}
	
	/**
	 * @return the servicioParametros
	 */
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	/**
	 * @return the listaComboPolizas
	 */
	public List<Object> getListaComboPolizas() {
		return listaComboPolizas;
	}

	/**
	 * @param listaComboPolizas the listaComboPolizas to set
	 */
	public void setListaComboPolizas(List<Object> listaComboPolizas) {
		this.listaComboPolizas = listaComboPolizas;
	}

	public Boolean getHabilitaPoliza() {
		return habilitaPoliza;
	}

	public void setHabilitaPoliza(Boolean habilitaPoliza) {
		this.habilitaPoliza = habilitaPoliza;
	}
	
	public List<Object> getListaTipoPolizas() {
		return listaTipoPolizas;
	}

	public void setListaTipoPolizas(List<Object> listaTipoPolizas) {
		this.listaTipoPolizas = listaTipoPolizas;
	}
	
	/**
	 * @return the inCanal
	 */
	public Integer getInCanal() {
		return inCanal;
	}

	/**
	 * @param inCanal the inCanal to set
	 */
	public void setInCanal(Integer inCanal) {
		this.inCanal = inCanal;
	}

	/**
	 * @return the inRamo
	 */
	public HtmlSelectOneMenu getInRamo() {
		return inRamo;
	}

	/**
	 * @param inRamo the inRamo to set
	 */
	public void setInRamo(HtmlSelectOneMenu inRamo) {
		this.inRamo = inRamo;
	}

	/**
	 * @return the inIdVenta
	 */
	public HtmlSelectOneMenu getInIdVenta() {
		return inIdVenta;
	}

	/**
	 * @param inIdVenta the inIdVenta to set
	 */
	public void setInIdVenta(HtmlSelectOneMenu inIdVenta) {
		this.inIdVenta = inIdVenta;
	}

	public HtmlSelectOneMenu getRreporte() {
		return rreporte;
	}

	/**
	 * @return the inOperEndoso
	 */
	public HtmlSelectOneMenu getInOperEndoso() {
		return inOperEndoso;
	}

	/**
	 * @param inOperEndoso the inOperEndoso to set
	 */
	public void setInOperEndoso(HtmlSelectOneMenu inOperEndoso) {
		this.inOperEndoso = inOperEndoso;
	}

	public void setRreporte(HtmlSelectOneMenu rreporte) {
		this.rreporte = rreporte;
	}
	
	
	/**
	 * @return the inPoliza
	 */
	public HtmlSelectOneMenu getInPoliza() {
		return inPoliza;
	}

	/**
	 * @param inPoliza the inPoliza to set
	 */
	public void setInPoliza(HtmlSelectOneMenu inPoliza) {
		this.inPoliza = inPoliza;
	}

	/**
	 * @return the listaComboIdVenta
	 */
	public List<Object> getListaComboIdVenta() {
		return listaComboIdVenta;
	}

	/**
	 * @param listaComboIdVenta the listaComboIdVenta to set
	 */
	public void setListaComboIdVenta(List<Object> listaComboIdVenta) {
		this.listaComboIdVenta = listaComboIdVenta;
	}

	/**
	 * @return the listaTipoEndoso
	 */
	public List<Object> getListaTipoEndoso() {
		return listaTipoEndoso;
	}

	/**
	 * @param listaTipoEndoso the listaTipoEndoso to set
	 */
	public void setListaTipoEndoso(List<Object> listaTipoEndoso) {
		this.listaTipoEndoso = listaTipoEndoso;
	}

	/**
	 * @return the habilitaComboIdVenta
	 */
	public Boolean getHabilitaComboIdVenta() {
		return habilitaComboIdVenta;
	}

	/**
	 * @param habilitaComboIdVenta the habilitaComboIdVenta to set
	 */
	public void setHabilitaComboIdVenta(Boolean habilitaComboIdVenta) {
		this.habilitaComboIdVenta = habilitaComboIdVenta;
	}
	
	public Boolean getHabilitaStatus() {
		return habilitaStatus;
	}

	public void setHabilitaStatus(Boolean habilitaStatus) {
		this.habilitaStatus = habilitaStatus;
	}

    public void setInStatus(HtmlSelectOneMenu inStatus) {
		this.inStatus = inStatus;
	}

	public HtmlSelectOneMenu getInStatus() {
		return inStatus;
	}

	public void setListaComboStatus(List<Object> listaComboStatus) {
		this.listaComboStatus = listaComboStatus;
	}

	public List<Object> getListaComboStatus() {
		return listaComboStatus;
	}
	
	/*LPV*/
	public void setInPolizaHardCode(HtmlInputHidden inPolizaHardCode) {
		this.inPolizaHardCode = inPolizaHardCode;
	}
	
	public HtmlInputHidden getInPolizaHardCode() {
		return inPolizaHardCode;
	}
	
	//dfg
	
	public HtmlSelectOneMenu getInProducto() {
		return inProducto;
	}

	public void setInProducto(HtmlSelectOneMenu inProducto) {
		this.inProducto = inProducto;
	}

	public List<Object> getListaComboProductos() {
		return listaComboProductos;
	}

	public void setListaComboProductos(List<Object> listaComboProductos) {
		this.listaComboProductos = listaComboProductos;
	}
	
	public Short getRamo() {
		return ramo;
	}

	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
		

	public Integer getProducto() {
		return producto;
	}

	public void setProducto(Integer producto) {
		this.producto = producto;
	}

	public Integer getPoliza() {
		return poliza;
	}

	public void setPoliza(Integer poliza) {
		this.poliza = poliza;
	}

	public HtmlSelectOneMenu getInPlan() {
		return inPlan;
	}

	public void setInPlan(HtmlSelectOneMenu inPlan) {
		this.inPlan = inPlan;
	}

	public List<Object> getListaComboPlan() {
		return listaComboPlan;
	}

	public void setListaComboPlan(List<Object> listaComboPlan) {
		this.listaComboPlan = listaComboPlan;
	}

	public Boolean getHabilitaComboPlan() {
		return habilitaComboPlan;
	}

	public void setHabilitaComboPlan(Boolean habilitaComboPlan) {
		this.habilitaComboPlan = habilitaComboPlan;
	}

	public Boolean getHabilitaComboProducto() {
		return habilitaComboProducto;
	}

	public void setHabilitaComboProducto(Boolean habilitaComboProducto) {
		this.habilitaComboProducto = habilitaComboProducto;
	}
	

	public HtmlSelectOneMenu getInTipoPrima() {
		return inTipoPrima;
	}

	public void setInTipoPrima(HtmlSelectOneMenu inTipoPrima) {
		this.inTipoPrima = inTipoPrima;
	}
	
	

	public HtmlSelectOneMenu getInEstado() {
		return inEstado;
	}

	public void setInEstado(HtmlSelectOneMenu inEstado) {
		this.inEstado = inEstado;
	}

	public List<Object> getListaComboEstados() {
		return listaComboEstados;
	}

	public void setListaComboEstados(List<Object> listaComboEstados) {
		this.listaComboEstados = listaComboEstados;
	}


	public Boolean getHabilitaComboEstado() {
		return habilitaComboEstado;
	}

	public void setHabilitaComboEstado(Boolean habilitaComboEstado) {
		this.habilitaComboEstado = habilitaComboEstado;
	}


	public HtmlSelectOneMenu getCdVenta() {
		return cdVenta;
	}

	public void setCdVenta(HtmlSelectOneMenu cdVenta) {
		this.cdVenta = cdVenta;
	}

	public HtmlSelectOneMenu getDescripCdVenta() {
		return descripCdVenta;
	}

	public void setDescripCdVenta(HtmlSelectOneMenu descripCdVenta) {
		this.descripCdVenta = descripCdVenta;
	}

	//LPV
	public List<Object> getListaComboCentroCostos() {
		return listaComboCentroCostos;
	}
	public void setListaComboCentroCostos(List<Object> listaComboCentroCostos) {
		this.listaComboCentroCostos = listaComboCentroCostos;
	}
	public List<Object> getListaComboSegmento() {
		return listaComboSegmento;
	}
	public void setListaComboSegmento(List<Object> listaComboSegmento) {
		this.listaComboSegmento = listaComboSegmento;
	}
	
	public List<Object> getListaTipoCobertura() {
		return listaTipoCobertura;
	}
	
	public void setListaTipoCobertura(List<Object> listaTipoCobertura) {
		this.listaTipoCobertura = listaTipoCobertura;
	}
	
	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}

	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}

	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}

	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}

	public ServicioEstado getServicioEstado() {
		return servicioEstado;
	}

	public void setServicioEstado(ServicioEstado servicioEstado) {
		this.servicioEstado = servicioEstado;
	}

	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}

	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}

	/*
	 * INICIO DE METODOS
	 * */
	public String cargaPolizas(){
		
		this.message.append("cargando polizas.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder filtro = new StringBuilder(100);
		Short oRamo;
		this.listaComboPolizas.clear();
		
		if(this.inRamo != null){
			try {
				
				if(this.ramo != null) {
					oRamo = this.ramo;
				} else {				
					oRamo = (Short) this.inRamo.getValue();
				}
				
				if(oRamo == 57 || oRamo == 58) {
					filtro.append("  and C.id.coceCasuCdSucursal 	=").append(this.inCanal).append(" \n");
					filtro.append("  and C.id.coceCarpCdRamo 		= ").append(oRamo).append("\n");
					filtro.append("  and C.id.coceCapoNuPoliza 		> 0 \n");
					filtro.append("  and C.id.coceNuCertificado 	= 0 \n");
					filtro.append("  and C.estatus.alesCdEstatus    = 1 \n");
					
					List<Certificado> listaPolizas = this.servicioCertificado.obtenerObjetos(filtro.toString());
					
					for(Certificado item: listaPolizas) {
						this.listaComboPolizas.add(new SelectItem(item.getId().getCoceCapoNuPoliza().toString(), item.getId().getCoceCapoNuPoliza().toString()));
					}
				} else {
					filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
					filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
					filtro.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
					filtro.append("  and P.copaNvalor2 = ").append(oRamo).append("\n");
					filtro.append("  and P.copaNvalor7 = 0 \n");
					filtro.append("order by P.copaNvalor3");
					
					List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
					
					for(Parametros item: listaPolizas) {
						this.listaComboPolizas.add(new SelectItem(item.getCopaNvalor3().toString(), item.getCopaNvalor3().toString()));
					}
				}
				
				return NavigationResults.SUCCESS;
			} catch (Exception e) {
				message.append("No se pueden cargar los parametros.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}
		} else {
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.RETRY;
		}
		
	}
	
public String cargaPolizasRC(){
		
		this.message.append("cargando polizas.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder filtro = new StringBuilder(100);
		Short oRamo;
		this.listaComboPolizas.clear();
		
		if(this.inRamoRC != null){
			try {
				
				if(this.ramo != null) {
					oRamo = this.ramo;
				} else {				
					oRamo = (Short) this.inRamoRC.getValue();
				}
				
				if(oRamo == 57 || oRamo == 58) {
					filtro.append("  and C.id.coceCasuCdSucursal 	=").append(this.inCanal).append(" \n");
					filtro.append("  and C.id.coceCarpCdRamo 		= ").append(oRamo).append("\n");
					filtro.append("  and C.id.coceCapoNuPoliza 		> 0 \n");
					filtro.append("  and C.id.coceNuCertificado 	= 0 \n");
					filtro.append("  and C.estatus.alesCdEstatus    = 1 \n");
					
					List<Certificado> listaPolizas = this.servicioCertificado.obtenerObjetos(filtro.toString());
					
					for(Certificado item: listaPolizas) {
						this.listaComboPolizas.add(new SelectItem(item.getId().getCoceCapoNuPoliza().toString(), item.getId().getCoceCapoNuPoliza().toString()));
					}
				} else {
					filtro.append("  and P.copaDesParametro= 'POLIZA' \n");
					filtro.append("  and P.copaIdParametro = 'GEP'    \n" );
					filtro.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
					filtro.append("  and P.copaNvalor2 = ").append(oRamo).append("\n");
					filtro.append("  and P.copaNvalor7 = 0 \n");
					filtro.append("order by P.copaNvalor3");
					
					List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro.toString());
					
					for(Parametros item: listaPolizas) {
						this.listaComboPolizas.add(new SelectItem(item.getCopaNvalor3().toString(), item.getCopaNvalor3().toString()));
					}
				}
				
				return NavigationResults.SUCCESS;
			} catch (Exception e) {
				message.append("No se pueden cargar los parametros.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}
		} else {
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.RETRY;
		}
		
	}

public void cargaIdVentaRC(){
	
	StringBuilder filtro = new StringBuilder();
	StringBuilder filtro2 = new StringBuilder();
	
	this.listaComboIdVenta.clear();
	
	try {

		if(this.inPolizaRC != null && (Integer.parseInt(this.inPolizaRC.getValue().toString()) == 0 || 
				Integer.parseInt(this.inPolizaRC.getValue().toString()) == -1)){
			
			habilitaComboIdVenta=false;
			habilitaPoliza=true;
			
			filtro.append("  and P.copaIdParametro = 'IDVENTA'");
			filtro.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
			filtro.append("  and P.copaNvalor2 = ").append(this.inRamoRC.getValue()).append("\n");
			
			List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			for(Parametros param: lista){
				this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
			}
			
			if (lista == null || lista.size() <= 0) habilitaComboIdVenta=true;
			
		}else{
			
			habilitaComboIdVenta = false;
			
			filtro2.append("  and P.copaDesParametro= 'POLIZA' \n");
			filtro2.append("  and P.copaIdParametro = 'GEP'    \n" );
			filtro2.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
			filtro2.append("  and P.copaNvalor2 = ").append(this.inRamoRC.getValue()).append("\n");
			filtro2.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(this.inPolizaRC.getValue()).append(", 1, substr(").append(this.inPolizaRC.getValue()).append(",0,3), 2,substr(").append(this.inPolizaRC.getValue()).append(",0,2))\n");
			filtro2.append("order by P.copaNvalor3");
			
			List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro2.toString());
			
			for(Parametros item: listaPolizas){					
				if(item.getCopaNvalor4().toString().equals("1")){
					habilitaPoliza=false;
				}else{
					habilitaPoliza=true;
				}
			}
		}
		
	} catch (Exception e) {
		// TODO: handle exception
		message.append("No se puede incializar la lista de Parametros.");
		this.log.error(message.toString(), e);
		throw new FacesException(message.toString(), e);
		
	}
	
}
	/**
	 * 
	 */
	public void cargaIdVenta(){
		
		StringBuilder filtro = new StringBuilder();
		StringBuilder filtro2 = new StringBuilder();
		
		this.listaComboIdVenta.clear();
		
		try {

			if(this.inPoliza != null && (Integer.parseInt(this.inPoliza.getValue().toString()) == 0 || 
					Integer.parseInt(this.inPoliza.getValue().toString()) == -1)){
				
				habilitaComboIdVenta=false;
				habilitaPoliza=true;
				
				filtro.append("  and P.copaIdParametro = 'IDVENTA'");
				filtro.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(this.inRamo.getValue()).append("\n");
				
				List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for(Parametros param: lista){
					this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
				}
				
				if (lista == null || lista.size() <= 0) habilitaComboIdVenta=true;
				
			}else{
				
				habilitaComboIdVenta = false;
				
				filtro2.append("  and P.copaDesParametro= 'POLIZA' \n");
				filtro2.append("  and P.copaIdParametro = 'GEP'    \n" );
				filtro2.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
				filtro2.append("  and P.copaNvalor2 = ").append(this.inRamo.getValue()).append("\n");
				filtro2.append("  and P.copaNvalor3 = DECODE(P.copaNvalor7,0,").append(this.inPoliza.getValue()).append(", 1, substr(").append(this.inPoliza.getValue()).append(",0,3), 2,substr(").append(this.inPoliza.getValue()).append(",0,2))\n");
				filtro2.append("order by P.copaNvalor3");
				
				List<Parametros> listaPolizas = this.servicioParametros.obtenerObjetos(filtro2.toString());
				
				for(Parametros item: listaPolizas){					
					if(item.getCopaNvalor4().toString().equals("1")){
						habilitaPoliza=false;
					}else{
						habilitaPoliza=true;
					}
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	

	public void cargaIdVentaRamo(){
		
		StringBuilder filtro = new StringBuilder();
		
		this.listaComboIdVenta.clear();
		
		try {

			if(this.inRamo != null ){
				this.inCanal = 1;
				habilitaComboIdVenta=true;				
				filtro.append("  and P.copaIdParametro = 'IDVENTA' ");
				filtro.append("  and P.copaDesParametro = 'PRIMAUNICA' ");
				filtro.append("  and P.copaNvalor1 = ").append(this.inCanal).append("\n");
				
				List<Parametros> lista = this.servicioParametros.obtenerCanalVenta(filtro.toString());
				 
				Integer i = 0;
				String desc= "";
				for(Parametros param: lista){
					i=i+1;		
					if(i==1){
					  desc = param.getCopaVvalor1();	
					  this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
					}else{
					  if (!desc.equalsIgnoreCase(param.getCopaVvalor1()))	
					     this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
					    desc = param.getCopaVvalor1();
					}
					
					//this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
		            
				}
			 }
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}	
	
	/**
	 * 
	 */
	public void cargaTipoEndoso(){
		
		StringBuilder filtro = new StringBuilder();
		StringBuilder filtro2 = new StringBuilder();
		
		String[] operacion;
		
		this.listaTipoEndoso.clear();
		
		try {
						
			if(this.inPoliza != null && this.inRamo != null && this.inIdVenta != null) {
								
				filtro.append("  and P.copaDesParametro = 'POLIZA'");
				filtro.append("  and P.copaIdParametro  = 'ENDOSO'");
				filtro.append("  and P.copaNvalor1 = ").append(this.inCanal).append(" \n");
				filtro.append("  and P.copaNvalor2 = ").append(this.inRamo.getValue()).append("\n");
				filtro.append("  and P.copaNvalor3 = decode(").append(this.inIdVenta.getValue()).append(",0,")
				                                               .append(this.inPoliza.getValue()).append(",0) \n");
				filtro.append("  and P.copaNvalor4 = ").append(this.inIdVenta.getValue()).append("\n");				
				
				List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
			    
				if (lista != null && lista.size() > 0){
					Parametros param = lista.get(0);
					
					operacion = new String(param.getCopaVvalor3()+'|'+param.getCopaVvalor4()+'|'+param.getCopaVvalor5()).split("\\|");
					
					filtro2.append(" and P.id.coaeCarpCdRamo = ").append(this.inRamo.getValue()).append("\n");
					filtro2.append(" and P.id.coaeCdEndoso   = ");
					
					for (int i=0; i<operacion.length; i++){
						if (Integer.parseInt(operacion[i]) > 0){
							
							this.listaTipoEndoso.add(new SelectItem(operacion[i]
							             ,((CatEndosos)this.servicioParametros.obtenerCatalogoEndoso(filtro2.toString()+operacion[i]).get(0)).getCoaeCampov3()));
							
						}
					}		
				}	
				
				inOperEndoso.getAttributes().put("style", null);
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede incializar la lista de Catalogo de Endosos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	
	public void limpiarOperEndoso(){
		if (this.listaTipoEndoso != null){
			this.listaTipoEndoso.clear();
		}
	}
	
	public void cargaStatus(){
		try {
			if(this.rreporte.getValue().toString().equals("1") || this.rreporte.getValue().toString().equals("4")){
				habilitaStatus=true;
			} else if(this.rreporte.getValue().toString().equals("2")){
				habilitaStatus=false;
			}
		} catch (Exception e) {
			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	
	
	//dfg
	/*
	public void cargaProductos(){
		
		StringBuilder filtro = new StringBuilder();
		System.out.println("Ramo "+ ramo);
		this.listaComboProductos.clear();
		
		try {

			if(this.inRamo != null ){
				this.inCanal = 1;
				habilitaComboProducto=true;		
		
				filtro.append( " and a.alpr_cd_ramo = ").append(this.ramo).append("\n");
				filtro.append( " and co.cocb_capo_nu_poliza = ").append(this.inPoliza.getValue()).append("\n");
				
				List<Producto> lista = this.servicioParametros.obtenerProductos(filtro.toString());
				
				for (Producto producto : lista) {
					this.listaComboProductos.add(new SelectItem(producto.getId().toString(),producto.getAlprDeProducto()));
				}
				
			 }
			
			System.out.println(habilitaComboIdVenta);
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede incializar la lista de productos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	*/
	
	//dfg
	public void cargaProductos(){
		
		StringBuilder filtro = new StringBuilder();
		this.listaComboProductos.clear();
		try {
			
			if(this.inRamo != null ){
				filtro.append(" and P.id.alprCdRamo = ").append(this.inRamo.getValue()).append("\n");
			}
				
			List<Producto> lista = this.servicioProducto.obtenerObjetos(filtro.toString());
				
			for(Producto producto: lista){
			
				BeanProducto bean = (BeanProducto) ConstruirObjeto.crearBean(BeanProducto.class, producto);
				bean.setServicioProducto(this.servicioProducto);
				
				this.listaComboProductos.add(new SelectItem(producto.getId().getAlprCdProducto().toString(),producto.getAlprDeProducto()));
			}
			
								
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los productos.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
		}
		
	}
	
	
	public void cargaPlanes(){
		
		StringBuilder filtro = new StringBuilder();
		this.listaComboPlan.clear();
		
		try {
			
			if(this.inRamo != null ){
				filtro.append(" and P.id.alplCdRamo = ").append(this.inRamo.getValue()).append("\n");
				filtro.append(" and P.id.alplCdProducto = ").append(this.inProducto.getValue()).append("\n");
			}
				
			List<Plan> lista = this.servicioPlan.obtenerObjetos(filtro.toString());

			for(Plan plan: lista){
			
				BeanPlanes bean = (BeanPlanes) ConstruirObjeto.crearBean(BeanPlanes.class, plan);
				bean.setServicioPlan(this.servicioPlan);
				
				this.listaComboPlan.add(new SelectItem(plan.getId().getAlplCdPlan().toString(),plan.getAlplDePlan()));
						
			}
			
		
			
		} catch (Exception e) {

			e.printStackTrace();
			this.message.append("No se pueden recuperar los productos.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
		}
		
	}
	
	//LPV
	public void loadIdVenta(){
		
		StringBuilder filtro = new StringBuilder();

		this.listaComboIdVenta.clear();
		
		try {

			filtro.append("  and P.copaIdParametro = 'IDVENTA'");
			filtro.append("  and P.copaNvalor1 = 1");
			filtro.append("  and P.copaNvalor2 = ").append(this.inRamo.getValue()).append("\n");
			
			List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			this.listaComboIdVenta.add(new SelectItem(0,"PRIMA RECURRENTE"));
			
			for(Parametros param: lista){
				this.listaComboIdVenta.add(new SelectItem(param.getCopaNvalor4(),param.getCopaVvalor1()));
			}
			
			if (lista == null || lista.size() <= 0) habilitaComboIdVenta=true;
			
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}
	
	
	public void afterPropertiesSet() throws Exception {

		StringBuilder filtro = new StringBuilder();

		getListaComboSegmento().clear();
		
		try {
						
				filtro.append("  and P.copaDesParametro = 'CATALOGO'");
				filtro.append("  and P.copaIdParametro  = 'SEGMENTO'");
				
				List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				for (Parametros parametros : lista) { 
					getListaComboSegmento().add(new SelectItem(parametros.getCopaVvalor1(), parametros.getCopaVvalor1()));
				}
				
				cargaTipoCoberturas();
		
		}catch(Exception e){
 
			e.printStackTrace();
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
		}
	}


	//LPV
	public void cargaTipoCoberturas(){
		
		StringBuilder filtro = new StringBuilder();

		this.listaTipoCobertura.clear();
		
		try {

			filtro.append("  and P.copaDesParametro = 'CATALOGO'");
			filtro.append("  and P.copaIdParametro  = 'TP_COBERTURA'");
			
			List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			for(Parametros param: lista){
				this.listaTipoCobertura.add(new SelectItem(param.getCopaNvalor4().toString(),param.getCopaVvalor1().toString()));
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede incializar la lista de Parametros.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
			
		}
		
	}

	/**
	 * @return the inRamoRC
	 */
	public HtmlSelectOneMenu getInRamoRC() {
		return inRamoRC;
	}

	/**
	 * @param inRamoRC the inRamoRC to set
	 */
	public void setInRamoRC(HtmlSelectOneMenu inRamoRC) {
		this.inRamoRC = inRamoRC;
	}

	/**
	 * @return the inIdVentaRC
	 */
	public HtmlSelectOneMenu getInIdVentaRC() {
		return inIdVentaRC;
	}

	/**
	 * @param inIdVentaRC the inIdVentaRC to set
	 */
	public void setInIdVentaRC(HtmlSelectOneMenu inIdVentaRC) {
		this.inIdVentaRC = inIdVentaRC;
	}

	/**
	 * @return the inPolizaRC
	 */
	public HtmlSelectOneMenu getInPolizaRC() {
		return inPolizaRC;
	}

	/**
	 * @param inPolizaRC the inPolizaRC to set
	 */
	public void setInPolizaRC(HtmlSelectOneMenu inPolizaRC) {
		this.inPolizaRC = inPolizaRC;
	}
	
	private ArrayList<SelectItem> darrsiVigencias;
	private ArrayList<SelectItem> darrsiTipoReporte;
    private HtmlSelectOneMenu comboVigencia;

	
	/** 
	 * @author JuanFlores (JJFM - TOwa)
	 * getter Vigencias de poliza recurrente
	 * @return ArrayList<SelectItem>
	 * */
	public ArrayList<SelectItem> getDarrsiVigencias() {
		if (
				this.darrsiVigencias != null
		) {
			return new ArrayList<SelectItem>(this.darrsiVigencias);
		}
		return null;
	}
	
	/** 
	 * @author JuanFlores (JJFM - TOwa)
	 * getter tipos de reporte 
	 * @return ArrayList<SelectItem>
	 * */
	public ArrayList<SelectItem> getDarrsiTipoReporte() {
		if (
				this.darrsiTipoReporte != null
		) {
			return new ArrayList<SelectItem>(darrsiTipoReporte);
		}
		return null;
		
	}
	
    public HtmlSelectOneMenu getComboVigencia() {
		return comboVigencia;
	}


	/** 
	 * @author JuanFlores (JJFM - TOwa)
	 * setter   setDarrsiVigencias
	 * @param darrsiVigencias
	 * */
	public void setDarrsiVigencias(ArrayList<SelectItem> darrsiVigencias) {
		if (
				darrsiVigencias != null
				
		) {
			this.darrsiVigencias = new ArrayList<SelectItem>(darrsiVigencias);
		} else {
			this.darrsiVigencias = null;
		}
		
	}
	
	/** 
	 * @author JuanFlores (JJFM - TOwa)
	 * setter setDarrsiTipoReporte
	 * @param darrsiTipoReporte
	 * */
	public void setDarrsiTipoReporte(ArrayList<SelectItem> darrsiTipoReporte) {
		if (
				darrsiTipoReporte != null
		) {
			this.darrsiTipoReporte = new ArrayList<>(darrsiTipoReporte);
		} else {
			this.darrsiTipoReporte = null;
		}
	}

    public void setComboVigencia(HtmlSelectOneMenu comboVigencia) {
		this.comboVigencia = comboVigencia;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	/**
	 * @author: Towa (Juan Jose Flores)
	 * Evento <OnChange> de combo de poliza.
	 * 
	 * */
	public void comboPolizaOnChangeListener() {
		if ( //
				//											//Reporte DxP
				this.rreporte != null && 
				(int) this.rreporte.getValue() ==  10  || (int) this.rreporte.getValue() ==  11 && 
				this.inPoliza != null && 
				Integer.parseInt(this.inPoliza.getValue().toString()) > 0
						
				
		) {
			this.subGetVigencias();
		} else {
			this.cargaIdVenta();
		}
	}

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	/**
	 * @author TOWA (JJFM)
	 * Metodo de apoyo para la carga de la lista de reportes disponibles.
	 * (Justificacion: mayor limpieza en archivo jsp y mejor control al momento de la carga de datos.)
	 */
	private void subLoadTipoReportes() {
		this.darrsiTipoReporte.clear();
		
		this.darrsiTipoReporte.add(new SelectItem("1" , "Inteligencia Comercial"));
		this.darrsiTipoReporte.add(new SelectItem("2" , "Mensual Emisi�n"));
		this.darrsiTipoReporte.add(new SelectItem("3" , "Reporte de Obligados"));
		this.darrsiTipoReporte.add(new SelectItem("5" , "Reporte Detalle Prefactura"));
		this.darrsiTipoReporte.add(new SelectItem("6" , "Reporte Vigor"));
		this.darrsiTipoReporte.add(new SelectItem("7" , "Reporte Endosos"));
		this.darrsiTipoReporte.add(new SelectItem("8" , "Reporte Endosos Conciliados"));
		this.darrsiTipoReporte.add(new SelectItem("9" , "Reporte Recibos"));
		this.darrsiTipoReporte.add(new SelectItem("10", "Reporte Deudor Por Prima"));
		//this.darrsiTipoReporte.add(new SelectItem("11", "Reporte Deudor Por Prima Carga Inicial"));


	}
	
	//-----------------------------------------------------------------------------------------------------------------
	/**
	 * @author: Towa (Juan Jose Flores)
	 * Metodo para obtener Vigencias de la poliza
	 * 
	 * */
	private void subGetVigencias() {
		
		if (
				this.inRamo == null
		) {
			this.message.append("Ha ocurrido un error al cargar los ramos.");
			return;
		}
		
		if (
				this.inPoliza == null
		) {
			this.message.append("Ha ocurrido un error al cargar las polizas.");
			return;
		}
		
		try {
			
		this.darrsiVigencias.clear();
		
		Short shortRamo = (Short) inRamo.getValue();
		long longPoliza = (long) inPoliza.getValue();

		ArrayList<String> darrstrVig = 
				this.servicioCertificado.arrstrObtenerVigencias(Short.valueOf("1"), shortRamo, longPoliza, (short) 0); 
		
			
		for (String strVigencia : darrstrVig) {
			SelectItem siVigencia = new SelectItem(strVigencia, strVigencia);
			this.darrsiVigencias.add(siVigencia);
		}
			
	
		} catch (Excepciones ex) {
			this.log.error("Ha ocurrido un error con el servicio servicioCertificado.arrstrObtenerVigencias");
			this.log.error(ex.toString());
			this.message.append("Ha ocurrido un error al cargar las Vigencias.");
		}
	}
	//-----------------------------------------------------------------------------------------------------------------
}
//=====================================================================================================================
