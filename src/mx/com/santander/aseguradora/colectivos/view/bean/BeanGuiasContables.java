package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.model.SelectItem;
import javax.servlet.ServletException;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGuiaContable;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.ajax4jsf.component.html.HtmlAjaxCommandButton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.groovy.tools.shell.commands.SetCommand;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")

public class BeanGuiasContables { 
	
	public BeanGuiasContables() {
		this.message = new StringBuilder();
		this.listaPlanes = new ArrayList<Object>();
		this.listaProductos = new ArrayList<Object>();
		this.listaPlanes = new ArrayList<Object>();
		this.listaCoberturas = new ArrayList<Object>();
		this.listaGuiasContables = new ArrayList<Object>();
		this.setDisable(false);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_GUIAS_CONTABLES);
	}
	
	@Resource
	private ServicioGuiaContable servicioGuiaContable;
	
	private StringBuilder message;
	private Log log = LogFactory.getLog(this.getClass());

	private Short ramo;
	private Short producto;
	private Short plan;
	private double totalTarifa;
	private String ramoPoliza;
	private String ramoContable;
	private String cdOperacion;
	private String cdComponente;
	private String deCuenta;
	private String inDebhab;
	private String fechaInicio;
	private String fechaFin;
	
	private boolean disable;
	
	private HtmlAjaxCommandButton muestraBoton;
	private List<Object> listaComboOperaciones;
	private List<Object> listaProductos;
	private List<Object> listaPlanes;
	private List<Object> lista;
	private List<Object> listaCoberturas;
	private List<Object> listaGuiasContables;



	public String cargaProductos(){

		Iterator<Object> iterator;
		Object[] arrProductos;
		
		this.message.append("cargando productos.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		
		this.listaProductos.clear();
		if(this.ramo != null){
			try {
		
				int nDatosRamos = this.ramo.intValue();
				lista = this.servicioGuiaContable.consultarProductos(nDatosRamos);
				
				 iterator = lista.iterator();
				
				while (iterator.hasNext()) {
					
					arrProductos = (Object[]) iterator.next();
					System.out.println(arrProductos[0].toString()+"--"+arrProductos[1]);
					this.listaProductos.add(new SelectItem(arrProductos[0].toString(), arrProductos[0].toString()+ " - " + arrProductos[1]));
				}
				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				message.append("No se pueden cargar los productos.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				return NavigationResults.FAILURE;
			}
		} else{
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			return NavigationResults.RETRY;
		}
	}

	public String cargarPlanes(){

		System.out.println("---- CARGARPLANES");
		
		Iterator<Object> iterator;
		Object[] arrPlanes;
		
		this.message.append("cargando planes.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		
		
		this.listaPlanes.clear();
		if(this.producto != null){
			try {
		
				int nDatosRamos = this.ramo.intValue();
				int nDatosProductos = this.producto.intValue();
				lista = this.servicioGuiaContable.consultarPlanes(nDatosProductos, nDatosRamos);
				
				 iterator = lista.iterator();
				
				while (iterator.hasNext()) {
					
					arrPlanes = (Object[]) iterator.next();
					System.out.println(arrPlanes[0].toString()+"--"+arrPlanes[1]);
					this.listaPlanes.add(new SelectItem(arrPlanes[0].toString(), arrPlanes[0].toString()+ " - " + arrPlanes[1]));
				}
				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				message.append("No se pueden cargar los planes.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				return NavigationResults.FAILURE;
			}
		} else{
			this.message.append("Necesita seleccionar un producto.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			return NavigationResults.RETRY;
		}
	}


	public String consultar() {
		this.message = new StringBuilder();
		Iterator<Object> it, iteratorGuia;
		Object[] arrCoberturas, arrGuias;
		BeanCoberturas beanCoberturas;
		BeanGuiasContables beanGuiasContables;
		disable = true;
		
		this.listaPlanes.clear();
		if(this.plan != null){
		try {
			log.info(ramo);
			listaCoberturas.clear();
			listaGuiasContables.clear();
			
			int nDatosRamos = this.ramo.intValue();
			int nDatosProductos = this.producto.intValue();
			int nDatosPlanes = this.plan.intValue();
			totalTarifa = 0.0;
			
			
			List<Object> lista = this.servicioGuiaContable.obtenerCoberturas(nDatosRamos, nDatosProductos, nDatosPlanes);
			
			it = lista.iterator();
			while (it.hasNext()) {
				arrCoberturas = (Object[]) it.next();
				beanCoberturas = new BeanCoberturas();
				beanCoberturas.setNumeroPoliza(arrCoberturas[0] == null ? "" : arrCoberturas[0].toString());
				beanCoberturas.setRamoPoliza(arrCoberturas[1] == null ? "" : arrCoberturas[1].toString());
				this.ramoPoliza = arrCoberturas[1] == null ? "" : arrCoberturas[1].toString();
				beanCoberturas.setRamoReactor(arrCoberturas[2] == null ? "" : arrCoberturas[2].toString());
				beanCoberturas.setRamoContable(arrCoberturas[3] == null ? "" : arrCoberturas[3].toString());
				this.ramoContable = arrCoberturas[3] == null ? "" : arrCoberturas[3].toString();
				beanCoberturas.setCdCobertura(arrCoberturas[4] == null ? "" : arrCoberturas[4].toString());
				beanCoberturas.setCobertura(arrCoberturas[5] == null ? "" : arrCoberturas[5].toString());
				beanCoberturas.setCentroCostos(arrCoberturas[6] == null ? "" : arrCoberturas[6].toString());
				beanCoberturas.setTarifa(arrCoberturas[7] == null ? 0 : ((BigDecimal)arrCoberturas[7]).doubleValue());
				this.totalTarifa = ((BigDecimal)arrCoberturas[7]).doubleValue()+this.totalTarifa;

//				beanCoberturas.setTotalTarifas(totalTarifa == 0.0 ? 0.0 : totalTarifa);
				
				listaCoberturas.add(beanCoberturas);
				
			}
			
			this.disable= true;
			List<Object> listaGuias = this.servicioGuiaContable.obtenerGuiasContables(ramoPoliza, ramoContable);
			
			iteratorGuia = listaGuias.iterator();
			while (iteratorGuia.hasNext()) {
			arrGuias = (Object[]) iteratorGuia.next(); 
			beanGuiasContables = new BeanGuiasContables();
			beanGuiasContables.setCdOperacion(arrGuias[0] == null ? "" : arrGuias[0].toString());
			beanGuiasContables.setCdComponente(arrGuias[1] == null ? "" : arrGuias[1].toString());
			beanGuiasContables.setDeCuenta(arrGuias[2] == null ? "" : arrGuias[2].toString());
			beanGuiasContables.setInDebhab(arrGuias[3] == null ? "" : arrGuias[3].toString());
			beanGuiasContables.setFechaInicio(arrGuias[4] == null ? "" : arrGuias[4].toString());
			beanGuiasContables.setFechaFin(arrGuias[5] == null ? "" : arrGuias[5].toString());
			this.listaGuiasContables.add(beanGuiasContables);
			
				
			}
			this.totalTarifa=Utilerias.redondear(this.totalTarifa, "4");
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			message.append("No se pueden cargar los planes.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			return NavigationResults.FAILURE;
		}
	} else{
		this.message.append("Necesita seleccionar un producto.");
		this.log.debug(message.toString());
		FacesUtils.addErrorMessage(message.toString());
		return NavigationResults.RETRY;
	}
}
	
//	public String mostrarGuia() {
//		disable = true;
//		System.out.println("Prueba1");
//		this.message = new StringBuilder();
//		Iterator<Object>iteratorGuia;
//		Object[] arrGuias;
//		BeanGuiasContables beanGuiasContables;
//		System.out.println("Entrando en la consulta de guias contables");
//		if(this.ramoPoliza != null || ramoContable !=null){
//		try {
//			log.info(ramoPoliza);
//			log.info(ramoContable);
//			listaGuiasContables.clear();
//			
//			List<Object> listaGuias = this.servicioGuiaContable.obtenerGuiasContables(ramoPoliza, ramoContable);
//			
//			iteratorGuia = listaGuias.iterator();
//			while (iteratorGuia.hasNext()) {
//			arrGuias = (Object[]) iteratorGuia.next(); 
//			beanGuiasContables = new BeanGuiasContables();
//			beanGuiasContables.setCdOperacion(arrGuias[0] == null ? "" : arrGuias[0].toString());
//			beanGuiasContables.setCdComponente(arrGuias[1] == null ? "" : arrGuias[1].toString());
//			beanGuiasContables.setDeCuenta(arrGuias[2] == null ? "" : arrGuias[2].toString());
//			beanGuiasContables.setInDebhab(arrGuias[3] == null ? "" : arrGuias[3].toString());
//			beanGuiasContables.setFechaInicio(arrGuias[4] == null ? "" : arrGuias[4].toString());
//			beanGuiasContables.setFechaFin(arrGuias[5] == null ? "" : arrGuias[5].toString());
//			this.listaGuiasContables.add(beanGuiasContables);
//			
//				
//			}
//			return NavigationResults.SUCCESS;
//			
//		} catch (Exception e) {
//			message.append("No se pueden cargar los planes.");
//			this.log.error(message.toString(), e);
//			FacesUtils.addErrorMessage(message.toString());
//			return NavigationResults.FAILURE;
//		}
//	} else{
//		this.message.append("Necesita seleccionar un producto.");
//		this.log.debug(message.toString());
//		FacesUtils.addErrorMessage(message.toString());
//		return NavigationResults.RETRY;
//	}
//}

		
	
public List<Object> getListaComboOperaciones() {
		return listaComboOperaciones;
	}




	public void setListaComboOperaciones(List<Object> listaComboOperaciones) {
		this.listaComboOperaciones = listaComboOperaciones;
	}


	public String generaGuiaContable() throws ServletException, IOException, Excepciones{
		
		return null;
		
	}


	public List<Object> getListaPlanes() {
		return listaPlanes;
	}


	public void setListaPlanes(List<Object> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}


	public Short getRamo() {
		return ramo;
	}


	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}


	public ServicioGuiaContable getServicioGuiaContable() {
		return servicioGuiaContable;
	}


	public void setServicioGuiaContable(ServicioGuiaContable servicioGuiaContable) {
		this.servicioGuiaContable = servicioGuiaContable;
	}



	public List<Object> getListaProductos() {
		return listaProductos;
	}



	public void setListaProductos(List<Object> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public Short getPlan() {
		return plan;
	}

	public void setPlan(Short plan) {
		this.plan = plan;
	}

	public Short getProducto() {
		return producto;
	}

	public void setProducto(Short producto) {
		this.producto = producto;
	}

	public List<Object> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<Object> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}

	public double getTotalTarifa() {
		return totalTarifa;
	}

	public void setTotalTarifa(double totalTarifa) {
		this.totalTarifa = totalTarifa;
	}

	public String getRamoPoliza() {
		return ramoPoliza;
	}

	public void setRamoPoliza(String ramoPoliza) {
		this.ramoPoliza = ramoPoliza;
	}

	public String getRamoContable() {
		return ramoContable;
	}

	public void setRamoContable(String ramoContable) {
		this.ramoContable = ramoContable;
	}

	public String getCdOperacion() {
		return cdOperacion;
	}

	public void setCdOperacion(String cdOperacion) {
		this.cdOperacion = cdOperacion;
	}

	public String getCdComponente() {
		return cdComponente;
	}

	public void setCdComponente(String cdComponente) {
		this.cdComponente = cdComponente;
	}

	public String getDeCuenta() {
		return deCuenta;
	}

	public void setDeCuenta(String deCuenta) {
		this.deCuenta = deCuenta;
	}

	public String getInDebhab() {
		return inDebhab;
	}

	public void setInDebhab(String inDebhab) {
		this.inDebhab = inDebhab;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<Object> getListaGuiasContables() {
		return listaGuiasContables;
	}

	public void setListaGuiasContables(List<Object> listaGuiasContables) {
		this.listaGuiasContables = listaGuiasContables;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public HtmlAjaxCommandButton getMuestraBoton() {
		return muestraBoton;
	}

	public void setMuestraBoton(HtmlAjaxCommandButton muestraBoton) {
		this.muestraBoton = muestraBoton;
	}

	
}