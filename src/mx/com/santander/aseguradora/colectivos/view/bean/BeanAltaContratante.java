/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Date;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaContratante;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

/**
 * @author dflores
 *
 */
@Controller
@Scope("session")
public class BeanAltaContratante {

	@Resource
	private ServicioAltaContratante servicioAltaContratante;
	private Log log = LogFactory.getLog(this.getClass());
	private String inNacionalidad;
	private String cedulaRif;
	private String inRazonSocial;
	private String inCdVida;
	private String inRfc;
	private String inTpCiente;
	private long inActividad;
	private Date inFechaCons;
	private String inEdoCivil;
	private String inSexo;
	private String inCalleNum;
	private String inColonia;
	private String inPoblacion;
	private String inCdEdo;
	private String inCP;
	private long inCdBanco;
	private String nuCuenta;
	private String tpCuenta;
	private String nacionalidad2;
	private String cedula2;
	private long domicilio2;
	private String mensaje2;
	private String  message;
	private String res;
	
	public BeanAltaContratante() {
		// TODO Auto-generated constructor stub
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ALTA_CONTRATANTE);
		this.inActividad = 42L;
		this.inNacionalidad = "J";
		this.inEdoCivil = "S";
		this.inSexo ="M";
        this.nuCuenta = "56555806964";
        this.tpCuenta = "C";
        this.inCdVida = "V";
        this.inTpCiente = "N";
        this.inCdBanco = 4;

	}
	
	public void setServicioAltaContratante(ServicioAltaContratante servicioAltaContratante) {
		this.servicioAltaContratante = servicioAltaContratante;
	}

	public ServicioAltaContratante getServicioAltaContratante() {
		return servicioAltaContratante;
	}
	
	public String getInNacionalidad() {
		return inNacionalidad;
	}

	public void setInNacionalidad(String inNacionalidad) {
		this.inNacionalidad = inNacionalidad;
	}

	public String getCedulaRif() {
		return cedulaRif;
	}

	public void setCedulaRif(String cedulaRif) {
		this.cedulaRif = cedulaRif;
	}

	public String getInRazonSocial() {
		return inRazonSocial;
	}

	public void setInRazonSocial(String inRazonSocial) {
		this.inRazonSocial = inRazonSocial;
	}

	public String getInCdVida() {
		return inCdVida;
	}

	public void setInCdVida(String inCdVida) {
		this.inCdVida = inCdVida;
	}

	public String getInRfc() {
		return inRfc;
	}

	public void setInRfc(String inRfc) {
		this.inRfc = inRfc;
	}

	public String getInTpCiente() {
		return inTpCiente;
	}

	public void setInTpCiente(String inTpCiente) {
		this.inTpCiente = inTpCiente;
	}

	public long getInActividad() {
		return inActividad;
	}

	public void setInActividad(long inActividad) {
		this.inActividad = inActividad;
	}

	public Date getInFechaCons() {
		return inFechaCons;
	}

	public void setInFechaCons(Date inFechaCons) {
		this.inFechaCons = inFechaCons;
	}

	public String getInEdoCivil() {
		return inEdoCivil;
	}

	public void setInEdoCivil(String inEdoCivil) {
		this.inEdoCivil = inEdoCivil;
	}

	public String getInSexo() {
		return inSexo;
	}

	public void setInSexo(String inSexo) {
		this.inSexo = inSexo;
	}

	public String getInCalleNum() {
		return inCalleNum;
	}

	public void setInCalleNum(String inCalleNum) {
		this.inCalleNum = inCalleNum;
	}

	public String getInColonia() {
		return inColonia;
	}

	public void setInColonia(String inColonia) {
		this.inColonia = inColonia;
	}

	public String getInPoblacion() {
		return inPoblacion;
	}

	public void setInPoblacion(String inPoblacion) {
		this.inPoblacion = inPoblacion;
	}

	public String getInCdEdo() {
		return inCdEdo;
	}

	public void setInCdEdo(String inCdEdo) {
		this.inCdEdo = inCdEdo;
	}

	public String getInCP() {
		return inCP;
	}

	public void setInCP(String inCP) {
		this.inCP = inCP;
	}

	public long getInCdBanco() {
		return inCdBanco;
	}

	public void setInCdBanco(long inCdBanco) {
		this.inCdBanco = inCdBanco;
	}

	public String getNuCuenta() {
		return nuCuenta;
	}

	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}

	public String getTpCuenta() {
		return tpCuenta;
	}

	public void setTpCuenta(String tpCuenta) {
		this.tpCuenta = tpCuenta;
	}

	public String getNacionalidad2() {
		return nacionalidad2;
	}

	public void setNacionalidad2(String nacionalidad2) {
		this.nacionalidad2 = nacionalidad2;
	}

	public String getCedula2() {
		return cedula2;
	}

	public void setCedula2(String cedula2) {
		this.cedula2 = cedula2;
	}

	public long getDomicilio2() {
		return domicilio2;
	}

	public void setDomicilio2(long domicilio2) {
		this.domicilio2 = domicilio2;
	}

	public String getMensaje2() {
		return mensaje2;
	}

	public void setMensaje2(String mensaje2) {
		this.mensaje2 = mensaje2;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	
	public String iniciaAltaContratante(){
		
		this.log.debug("Iniciando proceso de alta contratante");
		
		if(this.inRazonSocial!= null)
		{
		
			try {
				     		
					/* res =  this.servicioAltaContratante.iniciaAltaContratante(this.inNacionalidad, 
							                                                   "1895221", //cedulaRif, 
							                                                   this.inRazonSocial, 
							                                                   this.inCdVida, 
							                                                   this.inRfc, 
							                                                   this.inTpCiente, 
							                                                   this.inActividad, 
							                                                   this.inFechaCons, 
							                                                   this.inEdoCivil, 
							                                                   this.inSexo, 
							                                                   this.inCalleNum, 
							                                                   this.inColonia, 
							                                                   this.inPoblacion, 
							                                                   "5", //this.inCdEdo, 
							                                                   this.inCP, 
							                                                   this.inCdBanco, 
							                                                   this.nuCuenta, 
							                                                   this.tpCuenta, 
							                                                   nacionalidad2, 
							                                                   cedula2, 
							                                                   domicilio2, 
							                                                   mensaje2);*/
					message = "Contratante con Nacionalidad J y cedula " + cedulaRif + " se dio de alta satisfactoriamente.";
					this.log.info(message);
					FacesUtils.addInfoMessage(message);
					return NavigationResults.SUCCESS;
					
				}
				catch (DataAccessException e) {
					// TODO: handle exception
					message = "Hubo un error al al dar de alta contratante." + e.getLocalizedMessage();
					this.log.error(message, e);
					FacesUtils.addErrorMessage(message);
					
					return NavigationResults.FAILURE;
				}
				catch (Exception e) {
					// TODO: handle exception
					message = "No se pudo realizar el alta de contratante. Algun dato es incorrecto.";					
					this.log.error(message, e);
					FacesUtils.addErrorMessage(message);
					
					return NavigationResults.FAILURE;
				}
			
		}
		else
		{
			message = "El ramo o sucursal no son corectos";
			this.log.info(message);
			FacesUtils.addInfoMessage(message);
			
			return NavigationResults.FAILURE;
			
		} 
	}
}
