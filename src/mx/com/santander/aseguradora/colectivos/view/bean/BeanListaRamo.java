/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Ramo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPoliza;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanListaRamo implements InitializingBean{

	@Resource
	private ServicioRamo servicioRamo;
	@Resource
	private ServicioPoliza servicioPoliza;
	@Resource
	private ServicioCertificado servicioCertificado;
		
	private Log log = LogFactory.getLog(this.getClass());
	
	private List<Object> listaComboRamos;
	private List<Object> listaCmbRamos5758;
	private List<Object> listaRamos;
	private List<Object> listaComboPolizas;
	private List<Object> listaPolizasCarga;
	private String colectivo;
	private HtmlSelectOneMenu inRamo;
	private int pagina;
	private StringBuilder message;
	
	public BeanListaRamo() {
		// TODO Auto-generated constructor stub
		this.listaComboRamos = new ArrayList<Object>();
		this.listaRamos = new ArrayList<Object>();
		this.listaComboPolizas = new ArrayList<Object>();
		this.listaPolizasCarga = new ArrayList<Object>();
		this.listaCmbRamos5758 = new ArrayList<Object>();
		this.log.debug("BeanListaRamo creada");
		this.message = new StringBuilder();
		this.pagina = 1;
	}
	
	/**
	 * @return the servicioRamo
	 */
	public ServicioRamo getServicioRamo() {
		return servicioRamo;
	}
	/**
	 * @param servicioRamo the servicioRamo to set
	 */
	public void setServicioRamo(ServicioRamo servicioRamo) {
		this.servicioRamo = servicioRamo;
	}
	/**
	 * @return the servicioPoliza
	 */
	public ServicioPoliza getServicioPoliza() {
		return servicioPoliza;
	}
	/**
	 * @param servicioPoliza the servicioPoliza to set
	 */
	public void setServicioPoliza(ServicioPoliza servicioPoliza) {
		this.servicioPoliza = servicioPoliza;
	}
	/**
	 * @return the listaComboRamos
	 */
	public List<Object> getListaComboRamos() {
		return listaComboRamos;
	}
	/**
	 * @param listaComboRamos the listaComboRamos to set
	 */
	public void setListaComboRamos(List<Object> listaComboRamos) {
		this.listaComboRamos = listaComboRamos;
	}
	/**
	 * @return the listaRamos
	 */
	public List<Object> getListaRamos() {
		return listaRamos;
	}
	/**
	 * @param listaRamos the listaRamos to set
	 */
	public void setListaRamos(List<Object> listaRamos) {
		this.listaRamos = listaRamos;
	}
	/**
	 * @return the listaComboPolizas
	 */
	public List<Object> getListaComboPolizas() {
		return listaComboPolizas;
	}
	/**
	 * @param listaComboPolizas the listaComboPolizas to set
	 */
	public void setListaComboPolizas(List<Object> listaComboPolizas) {
		this.listaComboPolizas = listaComboPolizas;
	}
	/**
	 * @return the colectivo
	 */
	public String getColectivo() {
		return colectivo;
	}
	/**
	 * @param colectivo the colectivo to set
	 */
	public void setColectivo(String colectivo) {
		this.colectivo = colectivo;
	}
	/**
	 * @return the inRamo
	 */
	public HtmlSelectOneMenu getInRamo() {
		return inRamo;
	}
	/**
	 * @param inRamo the inRamo to set
	 */
	public void setInRamo(HtmlSelectOneMenu inRamo) {
		this.inRamo = inRamo;
	}
	/**
	 * @return the pagina
	 */
	public int getPagina() {
		return pagina;
	}
	/**
	 * @param pagina the pagina to set
	 */
	public void setPagina(int pagina) {
		this.pagina = pagina;
	}
	/**
	 * @param listaPolizasCarga the listaPolizasCarga to set
	 */
	public void setListaPolizasCarga(List<Object> listaPolizasCarga) {
		this.listaPolizasCarga = listaPolizasCarga;
	}
	/**
	 * @return the listaPolizasCarga
	 */
	public List<Object> getListaPolizasCarga() {
		return listaPolizasCarga;
	}
	
	/**
	 * @param servicioCertificado the servicioCertificado to set
	 */
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
	/**
	 * @return the servicioCertificado
	 */
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}
	
	public List<Object> getListaCmbRamos5758() {
		return listaCmbRamos5758;
	}
	public void setListaCmbRamos5758(List<Object> listaCmbRamos5758) {
		this.listaCmbRamos5758 = listaCmbRamos5758;
	}
	
	public void afterPropertiesSet() throws Exception {
		this.log.debug("Cargando ramos.");
		
		try {
			List<Ramo> lista = this.servicioRamo.obtenerObjetos(Ramo.class);
			
			for(Ramo ramo: lista){
				
				BeanRamo bean = (BeanRamo) ConstruirObjeto.crearBean(BeanRamo.class, ramo);
				bean.setServicioRamo(servicioRamo);
				this.listaRamos.add(bean);
				this.listaComboRamos.add(new SelectItem(bean.getCarpCdRamo().toString(), bean.toString()));
				if(bean.getCarpCdRamo() ==  Constantes.RAMO_VIDAPYME_57 || bean.getCarpCdRamo() == Constantes.RAMO_VIDAPYME_58) {
					this.listaCmbRamos5758.add(new SelectItem(bean.getCarpCdRamo().toString(), bean.toString()));	
				}
			}
			listaRamos.clear();
		} catch (Exception e) {
			this.log.error("No se pueden cargar los ramos.", e);
			throw new FacesException("No se pueden cargar los ramos.", e);
		}
		
	}
}
