package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


/**
 * 
 * @author SSAF
 *
 */

@Controller
@Scope("session")

public class BeanCtaConciliacion {


	 @Resource
		private ServicioParametros servicioParametros;
		private String strRespuesta;
		private Log log = LogFactory.getLog(this.getClass());
		
		// Mapeo de parameteos 
		
		private BigDecimal copaNvalor6;  // Canal de Venta
	    private int        copaNvalor8;  // Ramo
		private Date       copaFvalor1;  // Ultima  Carga de Archivo de Emision
		private Date       copaFvalor2;  // Ultima  Carga de Archivo de Cancelacion
		private Date       copaFvalor3;  // Ultimo  Dia de Emision
		private Date       copaFvalor4;  // Ultimo  Dia de Cancelacion
		private Date       copaFvalor5;  // Ultimo  Dia de Carga de Workflow

		
		
		
		
		
		private List<Object>     lstaCtaConc;
		private List<Parametros> listaParametros;

		private HashMap<String, Object> hmResultados;
		private int filaActual;
		private BeanParametros objActual = new BeanParametros();
		private BeanParametros objNuevo = new BeanParametros();
		private Set<Integer>   keys = new HashSet<Integer>();
		private int 	       numPagina;

		
		   public BeanCtaConciliacion (){
		        
			    FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CTA_CONCILIACION);
				this.lstaCtaConc = new ArrayList<Object>();
				this.log.debug("BeanLstaConc   creado ");
				this.numPagina = 1;
			
			}
		
		
		 //Metodos de Busqueda
		 public void buscarFilaActual(ActionEvent event) {
		       String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cuenta"));
		       filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
		       Iterator<Object> it =  getLstaCtaConc().iterator();

		       while(it.hasNext()) {
		       	BeanParametros objParametro = (BeanParametros) it.next();
		       	if (objParametro.getCopaCdParametro().toString().equals(clave)){
		               objActual = objParametro;
		               break;
		           }
		       }
		   }


		 
		 public String consultarCtasConc(){
				String filtro = "";
				List<Object> lstResultados;
				Parametros parametro;
				BeanParametros bean;
		        
				
				try {
					hmResultados = new HashMap<String, Object>();
					
					filtro   = " and P.copaDesParametro = 'CONCILIACION' \n" +
		                       " and P.copaIdParametro  = 'CUENTAS'     \n" ;
		                      
					
					lstResultados = this.servicioParametros.obtenerObjetos(filtro);
					lstaCtaConc = new ArrayList<Object>();
					Iterator<Object> it = lstResultados.iterator();
					while(it.hasNext()) {
						parametro = (Parametros) it.next();
						BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
						bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
						bean.setServicioParametros(this.servicioParametros);
						
						lstaCtaConc.add(bean);
						hmResultados.put(bean.getCopaCdParametro().toString(), bean);
						//System.out.println(hmResultados);
					}

					setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
					
					return NavigationResults.SUCCESS;
				} catch (Exception e) {
					this.log.error("No se pueden recuperar las Cuentas.", e);
					e.printStackTrace();
					return NavigationResults.FAILURE;
				}
			}
		 
		 
		 
			public String altaCtaConciliacion(){
				Long secConciliacion;

				
				Date date = Calendar.getInstance().getTime();
				
			    /* String formato ="dd/mm/yyyy";
			
			 
				 String date1 =date.toString();
				 List<Parametros> lstRutas = new ArrayList<Parametros>();*/
				
				 
				try {
					//GestorFechas.buscaFecha(date, formato).buscaFecha(date, formato)
					secConciliacion = servicioParametros.siguente(); 
					
					System.out.println(secConciliacion.toString());
					objNuevo.setCopaCdParametro(secConciliacion); 	// SIGUIENTE NUMERO 
					objNuevo.setCopaIdParametro("CUENTAS");	        // SEGMENTO
					objNuevo.setCopaDesParametro("CONCILIACION"); 	// CATALOGO
					//objNuevo.setCopaNvalor1(1);					// CANAL 1-DEFAULT
					//objNuevo.setCopaVvalor1("VERIFICAR");
                   objNuevo.setCopaFvalor1(date);

					 
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
		            //lstRutas.add(parametro);
				
		   //-------Ruta copia de Archivos
			/*		Parametros parametro1 = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objNuevo);
					secConciliacion1 = secConciliacion +1; 
					System.out.println(secConciliacion1.toString());
					parametro1.setCopaRegistro(getObjNuevo().getCopaRegistro1());      //----------  Ruta
					parametro1.setCopaCdParametro(secConciliacion1);                   //----------  Secuencia
		            parametro1.setCopaVvalor3(getObjNuevo().getCopaIdParametro()+COPY);//----------  Tipo De Carga
		            parametro1.setCopaVvalor6("smb:|"+getObjNuevo().getUsuario1());            //----------  Usuario
		            
		            lstRutas.add(parametro1);*/
				   
					



				     //this.servicioParametros.guardarObjetos(lstRutas);
				     this.servicioParametros.guardarObjeto(parametro);
					setStrRespuesta("Se guardo con EXITO el registro.");
					
			
					objNuevo = new BeanParametros();
					consultarCtasConc();
					return NavigationResults.SUCCESS;
				} catch (ObjetoDuplicado e) {
					setStrRespuesta("Error Registro duplicado.");
					e.printStackTrace();
					this.log.error("Registro duplicado.", e);
					return NavigationResults.RETRY;
				} catch (Excepciones e) {
					setStrRespuesta("No se puede dar de alta la  Cuenta.");
					e.printStackTrace();
					this.log.error("No se puede dar de alta el Cuenta.", e);
					return NavigationResults.FAILURE;				
				}
			}
		 
		 
			
			public void modificar() {
				try {
					Parametros parametros = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual);
					servicioParametros.actualizarObjeto(parametros);
					hmResultados.remove(objActual.getCopaCdParametro().toString());
					hmResultados.put(objActual.getCopaCdParametro().toString(), objActual);
					setStrRespuesta("Se modifico con EXITO el registro.");
				} catch (Exception e) {
					setStrRespuesta("Error al modificar registro.");
					e.printStackTrace();
				}
			}
			
			
			
			
		 
		 
			public ArrayList<Object> getMapValues(){
				ArrayList<Object> lstValues = new ArrayList<Object>();
		        lstValues.addAll(getHmResultados().values());
		        return lstValues;
		    }
						
			public void afterPropertiesSet() throws Exception {
				try {
				} catch (Exception e) {
					throw new FacesException("No se pueden cargar las Cuentas.", e);
				}
			}	
			
		 
		 
		 
		 
		 
		 
		 /**
		  * Getterts & Setters  
		  */ 
		 
		public List<Object> getLstaCtaConc() {
			return lstaCtaConc;
		}


		public void setLstaCtaConc(List<Object> lstaCtaConc) {
			this.lstaCtaConc = lstaCtaConc;
		}


		public String getStrRespuesta() {
			return strRespuesta;
		}


		public void setStrRespuesta(String strRespuesta) {
			this.strRespuesta = strRespuesta;
		}


		public ServicioParametros getServicioParametros() {
			return servicioParametros;
		}


		public void setServicioParametros(ServicioParametros servicioParametros) {
			this.servicioParametros = servicioParametros;
		}


		public Log getLog() {
			return log;
		}


		public void setLog(Log log) {
			this.log = log;
		}


		public List<Parametros> getListaParametros() {
			return listaParametros;
		}


		public void setListaParametros(List<Parametros> listaParametros) {
			this.listaParametros = listaParametros;
		}


		public HashMap<String, Object> getHmResultados() {
			return hmResultados;
		}


		public void setHmResultados(HashMap<String, Object> hmResultados) {
			this.hmResultados = hmResultados;
		}


		public int getFilaActual() {
			return filaActual;
		}


		public void setFilaActual(int filaActual) {
			this.filaActual = filaActual;
		}


		public BeanParametros getObjActual() {
			return objActual;
		}


		public void setObjActual(BeanParametros objActual) {
			this.objActual = objActual;
		}


		public BeanParametros getObjNuevo() {
			return objNuevo;
		}


		public void setObjNuevo(BeanParametros objNuevo) {
			this.objNuevo = objNuevo;
		}


		public Set<Integer> getKeys() {
			return keys;
		}


		public void setKeys(Set<Integer> keys) {
			this.keys = keys;
		}


		public int getNumPagina() {
			return numPagina;
		}


		public void setNumPagina(int numPagina) {
			this.numPagina = numPagina;
		}
	    
		
		
		   
		   
	
		

}
