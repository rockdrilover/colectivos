package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;


import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanProducto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("request")
public class BeanPlanes {

	@Resource
	private ServicioPlan servicioPlan;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private PlanId id;
	private String alplDePlan;
	private Integer alplDato3;
	private Integer plazo;
	
	private BeanProducto producto;
	
	
	public String getAlplDePlan() {
		return alplDePlan;
	}
	public void setAlplDePlan(String alplDePlan) {
		this.alplDePlan = alplDePlan;
	}
	public Integer getAlplDato3() {
		return alplDato3;
	}
	public void setAlplDato3(Integer alplDato3) {
		this.alplDato3 = alplDato3;
	}
	
	public Integer getPlazo() {
		return plazo;
	}
	
	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	
	
	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}
	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}
	
	public BeanPlanes() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		//this.alplDato5 = new Date();
		//this.alplDato6 = new Date();
		
		if(FacesUtils.getBeanSesion().getCurrentBeanPlan() != null){
			
			try {
				ConstruirObjeto.poblarBean(this, FacesUtils.getBeanSesion().getCurrentBeanPlan());
			} catch (Excepciones e) {
				// TODO Auto-generated catch block
				this.message.append("No se puede crear el bean producto.");
				this.log.error(message.toString(), e);
				throw new FacesException(message.toString(), e);
			}
		}
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PLAN);
	}
	public PlanId getId() {
		return id;
	}
	public void setId(PlanId id) {
		this.id = id;
	}
	
	
	
	
}
