/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estatus;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstatus;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.component.html.HtmlCalendar;
import org.richfaces.event.DropEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanCancelacionIndividual extends BeanBase{

	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioClienteCertif servicioClienteCertif;
	@Resource
	private ServicioEstatus servicioEstatus;
	@Resource
	private ServicioProcesos servicioProceso;
	@Resource
	private ServicioParametros servicioParametro;
	@Resource
	private ServicioCarga servicioCarga;
	//Propiedad para indicar el recurso a acceder
	@Resource
	private ServicioGeneric servicioGeneric;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private String identificador;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private Date fechaAnulacion;
	private Short causaAnulacion;
	
	private List<BeanClienteCertif> listaCertificados;
	private List<BeanClienteCertif> listaCertificadosCancelacion;
	
	private HtmlCalendar cal;
	
	// certificado de prueba para calcular cancelaci�n.
	private BeanClienteCertif certifCancelado;
	
	private int numPagina;
	private int numPaginaC;
	private String respuesta;
	private Short  inRamo;
	
	private List<String> opciones;
	private Colectivos.OPC_CANACEL opc;
	private boolean habilitar;
	private boolean habValDev;
	private String idVentaSel;
	
	private Parametros tasaAmortiza;
	private Double primaDevuelta;
	
	private String criterioBusqueda;
	private String muestraPE;
	
	private String display1 = "none";
	private String tdc;
	
	private String display2 = "none";
	private String buc;
	
	private String display3 = "none";

	private String display4 = "none";
	private String canal; 
	private String ramo;
	private String poliza;
	private String certificado;
	private String polizaEspecifica;
	
	private String display5 = "none";
	private String displayPE = "none";
		
	private String calleNumero;
	private String colonia;
	private String delegacionMunicipio;
	private String fechaNacimiento;
	private String rfc;
	private String sexo;
	
	private Double total;
	private Integer qty;
	
	private boolean chkValPmaDev;
	
	/**
	 * @return the servicioCertificado
	 */
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}
	/**
	 * @param servicioCertificado the servicioCertificado to set
	 */
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
	
	public Short getInRamo() {
		return inRamo;
	}
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}
	/**
	 * @return the servicioClienteCertif
	 */
	public ServicioClienteCertif getServicioClienteCertif() {
		return servicioClienteCertif;
	}
	/**
	 * @param servicioClienteCertif the servicioClienteCertif to set
	 */
	public void setServicioClienteCertif(ServicioClienteCertif servicioClienteCertif) {
		this.servicioClienteCertif = servicioClienteCertif;
	}
	
	/**
	 * @return the servicioProceso
	 */
	public ServicioProcesos getServicioProceso() {
		return servicioProceso;
	}
	/**
	 * @param servicioProceso the servicioProceso to set
	 */
	public void setServicioProceso(ServicioProcesos servicioProceso) {
		this.servicioProceso = servicioProceso;
	}
	
	/**
	 * @return the servicioParametro
	 */
	public ServicioParametros getServicioParametro() {
		return servicioParametro;
	}
	/**
	 * @param servicioParametro the servicioParametro to set
	 */
	public void setServicioParametro(ServicioParametros servicioParametro) {
		this.servicioParametro = servicioParametro;
	}
	
	/**
	 * @return the servicioCarga
	 */
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}
	/**
	 * @param servicioCarga the servicioCarga to set
	 */
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}
	/**
	 * @return the identificador
	 */
	public String getIdentificador() {
		return identificador;
	}
	/**
	 * @param identificador the identificador to set
	 */
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * @return the fechaAnulacion
	 */
	public Date getFechaAnulacion() {
		return fechaAnulacion;
	}
	/**
	 * @param fechaAnulacion the fechaAnulacion to set
	 */
	public void setFechaAnulacion(Date fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}
	/**
	 * @return the causaAnulacion
	 */
	public Short getCausaAnulacion() {
		return causaAnulacion;
	}
	/**
	 * @param causaAnulacion the causaAnulacion to set
	 */
	public void setCausaAnulacion(Short causaAnulacion) {
		this.causaAnulacion = causaAnulacion;
	}
	/**
	 * @return the listaCertificados
	 */
	public List<BeanClienteCertif> getListaCertificados() {
		return listaCertificados;
	}
	/**
	 * @param listaCertificados the listaCertificados to set
	 */
	public void setListaCertificados(List<BeanClienteCertif> listaCertificados) {
		this.listaCertificados = listaCertificados;
	}
	/**
	 * @return the listaCertificadosCancelacion
	 */
	public List<BeanClienteCertif> getListaCertificadosCancelacion() {
		return listaCertificadosCancelacion;
	}
	/**
	 * @param listaCertificadosCancelacion the listaCertificadosCancelacion to set
	 */
	public void setListaCertificadosCancelacion(
			List<BeanClienteCertif> listaCertificadosCancelacion) {
		this.listaCertificadosCancelacion = listaCertificadosCancelacion;
	}
	
	/**
	 * @return the certifCancelado
	 */
	public BeanClienteCertif getCertifCancelado() {
		return certifCancelado;
	}
	/**
	 * @param certifCancelado the certifCancelado to set
	 */
	public void setCertifCancelado(BeanClienteCertif certifCancelado) {
		this.certifCancelado = certifCancelado;
	}
	/**
	 * @return the opcx
	 */
	public List<String> getOpciones() {
		return opciones;
	}

	/**
	 * @param opcx the opcx to set
	 */
	public void setOpciones(List<String> opciones) {
		this.opciones = opciones;
	}
	
	/**
	 * @return the habilitar
	 */
	public Boolean getHabilitar() {
		return !this.habilitar;
	}
		
	/**
	 * @return the tasaAmortiza
	 */
	public Parametros getTasaAmortiza() {
		return tasaAmortiza;
	}
	
	/**
	 * @param tasaAmortiza the tasaAmortiza to set
	 */
	public void setTasaAmortiza(Parametros tasaAmortiza) {
		this.tasaAmortiza = tasaAmortiza;
	}
	/**
	 * @return the primaDevuelta
	 */
	public Double getPrimaDevuelta() {
		return primaDevuelta;
	}
	/**
	 * @param primaDevuelta the primaDevuelta to set
	 */
	public void setPrimaDevuelta(Double primaDevuelta) {
		this.primaDevuelta = primaDevuelta;
	}
	/**
	 * @return the cal
	 */
	public HtmlCalendar getCal() {
		return cal;
	}
	/**
	 * @param cal the cal to set
	 */
	public void setCal(HtmlCalendar cal) {
		this.cal = cal;
	}
	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}
	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}
	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	/**
	 * @param numPaginaC the numPaginaC to set
	 */
	public void setNumPaginaC(int numPaginaC) {
		this.numPaginaC = numPaginaC;
	}
	/**
	 * @return the numPaginaC
	 */
	public int getNumPaginaC() {
		return numPaginaC;
	}
	/**
	 * @return the servicioEstatus
	 */
	public ServicioEstatus getServicioEstatus() {
		return servicioEstatus;
	}
	/**
	 * @param servicioEstatus the servicioEstatus to set
	 */
	public void setServicioEstatus(ServicioEstatus servicioEstatus) {
		this.servicioEstatus = servicioEstatus;
	}
	
	public String getCriterioBusqueda() {
		return criterioBusqueda;
	}
	public void setCriterioBusqueda(String criterioBusqueda) {
		this.criterioBusqueda = criterioBusqueda;
	}
	
	
	public BeanCancelacionIndividual() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.listaCertificados = new ArrayList<BeanClienteCertif>();
		this.listaCertificadosCancelacion = new ArrayList<BeanClienteCertif>();
		this.opciones = new ArrayList<String>();
		this.opciones.clear();
		this.opciones.add("1");
		this.numPagina = 1;
		this.numPaginaC = 1;
		this.habilitar = false;
		this.primaDevuelta = 0.0;	
		this.certifCancelado = new BeanClienteCertif();
		this.certifCancelado.setCertificado(new Certificado());
		this.tasaAmortiza = new Parametros();
		this.habValDev = false;
		this.chkValPmaDev = true;
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CANCELACION_INDIVIDUAL);
	}	
	
	public void cambioOpciones(){
		StringBuffer codOpcion = new StringBuffer("");
		
		for (String x:opciones)codOpcion.append(x);
		
		if (codOpcion.indexOf("2")>-1 || codOpcion.indexOf("3")>-1) habilitar =true;
		else habilitar = false;
	}
	
	/**
	 * Metodo que habilitas las opciones a realizar en la cancelacion
	 * @throws Excepciones con error en general
	 */
	public void verificaValDev() throws Excepciones {
		this.opciones = new ArrayList<String>();
		this.opciones.clear();
		this.opciones.add("1");
		
		this.tasaAmortiza.setCopaVvalor2(Constantes.DEFAULT_STRING_ID);	//Bandera que nos indica que panel mostrar. 0-PanelCancela, diferente a 0-Panel NotiSin
		
		if(getIdVentaSel() == null || getIdVentaSel().equals(Constantes.DEFAULT_STRING)) {
			this.habValDev = false;
		} else if("4".equals(getIdVentaSel())) {
			this.habValDev = false;
			this.tasaAmortiza.setCopaVvalor2(this.servicioGeneric.tieneSiniestro(listaCertificados));	//Bandera que nos indica que panel mostrar. 0-PanelCancela 1-Panel NotiSin
		} else {
			this.opciones.add("3");
			this.habValDev = true;
		}
	}
	
	//Mago Manos M�gicas
	public String consultaCertificados(){
		
		StringBuilder filtro = new StringBuilder();
		this.respuesta = "";
		String observaciones1="";
		String observaciones2="";
		
		try {
			setChkValPmaDev(true);
			if(getCriterioBusqueda().equals("0")) {
				this.respuesta = "* Favor de Seleccionar un Criterio. \n";
			} else {
				if(getCriterioBusqueda().equals("1")) { //tarjeta de credito
					System.out.println("Entro a consulta numero 1 TDC");								
					if((this.tdc != null && !this.tdc.equalsIgnoreCase(""))) {
						filtro.append(" and CC.certificado.coceNuCuenta = '").append(this.tdc).append("' \n");
					} else {
						this.respuesta = "* Favor de Ingresar TDC. \n";
					}					
				} else if(getCriterioBusqueda().equals("2")) { //BUC
					if((this.buc != null && !this.buc.equalsIgnoreCase(""))){
						filtro.append(" and CC.cliente.cocnBucCliente = '").append(this.buc).append("' \n");//cliente cocn buc
					} else {
						this.respuesta = "* Favor de Ingresar BUC. \n";					
					}
				} else if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("3")) { //folio
					if((this.identificador != null && !this.identificador.equalsIgnoreCase(""))){
						filtro.append(" and CC.id.coccIdCertificado = '").append(this.identificador).append("' \n");
					} else {
						this.respuesta = "* Favor de Ingresar CREDITO. \n";
					}
				} else if (getCriterioBusqueda()!=null && getCriterioBusqueda().equals("4")) { //crpc
					if(!(this.inRamo == new Short("0"))){
						filtro.append(" and CC.certificado.id.coceCasuCdSucursal = 1 \n");
						filtro.append(" and CC.certificado.id.coceCarpCdRamo = ").append(this.inRamo).append(" \n");
					} else {
						this.respuesta = "* Favor de seleccionar RAMO. \n";
					}
					
					if(this.poliza.equals("1")) {
						if((!this.polizaEspecifica.equals(""))){
							filtro.append(" and CC.certificado.id.coceCapoNuPoliza = ").append(this.polizaEspecifica).append(" \n");
						} else {
							this.respuesta = this.respuesta + "* Favor de seleccionar POLIZA ESPECIFICA. \n";
						}
					} else if(this.poliza.equals("0")) {
						this.respuesta = this.respuesta + "* Favor de seleccionar POLIZA. \n";
					} else { 
						filtro.append(" and CC.certificado.id.coceCapoNuPoliza = ").append(this.poliza).append(" \n");
					}
					
					if(!this.certificado.equals("")){
						filtro.append(" and CC.certificado.id.coceNuCertificado = ").append(this.certificado).append(" \n");
					} else {
						this.respuesta = this.respuesta + "* Favor de seleccionar CERTIFICADO. \n";
					}
						
				} else if(getCriterioBusqueda().equals("5")) { //nombre
						if(!this.nombre.equals("")){
							filtro.append(" and CC.cliente.cocnNombre like '%").append(this.nombre.toUpperCase()).append("%'\n");
						}else {
							this.respuesta = this.respuesta + "* Favor de seleccionar NOMBRE. \n";
						}
						
						if(!this.apellidoPaterno.equals("")){
							filtro.append(" and CC.cliente.cocnApellidoPat like '%").append(this.apellidoPaterno.toUpperCase()).append("%'\n");
						}else {
							this.respuesta = this.respuesta + "* Favor de seleccionar APELLIDO PATERNO. \n";
						}
						
						if(!this.apellidoMaterno.equals("")){
							filtro.append(" and CC.cliente.cocnApellidoMat like '%").append(this.apellidoMaterno.toUpperCase()).append("%'\n");
						}

				}
			}
			
			if(this.respuesta.equals("")) {
				filtro.append(" and CC.certificado.estatus.alesCdEstatus in(1, 2)\n");
				this.listaCertificadosCancelacion.clear();
				this.listaCertificados.clear();
				List<ClienteCertif> certif = this.servicioClienteCertif.obtenerObjetos(filtro.toString());

				for (ClienteCertif c:certif){
					
					BeanClienteCertif clienteCertif = (BeanClienteCertif) ConstruirObjeto.crearBean(BeanClienteCertif.class, c);
					
					observaciones1 = creaObservaciones(clienteCertif.getCertificado().getCoceObservaciones(),1);
					observaciones2 = creaObservaciones(clienteCertif.getCertificado().getCoceObservaciones(),2);
					
					clienteCertif.getCertificado().setCoceObservaciones(observaciones1);
					clienteCertif.setObservaciones2(observaciones2);
					
					this.listaCertificados.add( clienteCertif );
				}
				
				Parametros tasa = new Parametros();
				tasa.setCopaDesParametro("DEVOLUCION");
				tasa.setCopaIdParametro("TASA_AMORTIZA");
				
				String filtroT = " AND P.copaDesParametro = 'DEVOLUCION' AND P.copaIdParametro = 'TASA_AMORTIZA' \n";
		
				this.tasaAmortiza = ((Parametros) this.servicioParametro.obtenerObjetos(filtroT).get(0));	
				this.primaDevuelta = 0.0;
				
				this.message.append("Consulta realizada correctamente.");
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				Utilerias.resetMessage(message);
				
				this.opciones = new ArrayList<String>();
				this.opciones.clear();
				this.opciones.add("1");
				this.habValDev = false;
			}
			
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CANCELACION_INDIVIDUAL);
			
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			this.message.append("No se puede realizar la consulta de los certificados.");
			this.log.error(message.toString(), e);
			this.respuesta = this.message.append("Error interno.").toString();
			Utilerias.resetMessage(message);
			return NavigationResults.RETRY;
		}
	}
	
	public String creaObservaciones(String observacion, int tipo){
		if(observacion!=null){
			int indice = observacion.indexOf("|");
	
			if(tipo==1 && indice>0){
				observacion = observacion.substring(0,indice);
			}
	
			if(tipo==2){
				observacion = observacion.substring(indice+1);
			}
		}
		return observacion;
	}

	/**
	 * Metodo que cancela los certificados seleccionados
	 * @return
	 */
	public String cancelaCertificados2(){
		StringBuilder codOpcion;
		StringBuilder sbQuery;
		Parametros objParam = null;
		
		try {
			codOpcion = new StringBuilder("");
			sbQuery = new StringBuilder();
			
			for (String x:opciones)codOpcion.append(x); 
			
			opc = null;
			
			if (opciones.size() == 0) opc=Colectivos.OPC_CANACEL.ERROR;
			else{
				if (codOpcion.length() == 1){
					switch(Integer.parseInt(codOpcion.toString())){
						case 1:	opc = Colectivos.OPC_CANACEL.CANCELA;	break;
						case 2:	opc = Colectivos.OPC_CANACEL.DEVO;		break;
						case 3:	opc = Colectivos.OPC_CANACEL.COMP;		break;
					}
				}
				
				if (codOpcion.length() == 2){
					if (codOpcion.indexOf("12")>-1 || codOpcion.indexOf("21")>-1) opc = Colectivos.OPC_CANACEL.CAN_DEVO;
				    if (codOpcion.indexOf("13")>-1 || codOpcion.indexOf("31")>-1) opc = Colectivos.OPC_CANACEL.ERROR;
					if (codOpcion.indexOf("23")>-1 || codOpcion.indexOf("32")>-1) opc = Colectivos.OPC_CANACEL.DEV_COMP;
				}
				
				if (codOpcion.length() == 3){
					if (codOpcion.indexOf("1")>-1 && codOpcion.indexOf("2")>-1 && codOpcion.indexOf("3")>-1) opc = Colectivos.OPC_CANACEL.TODAS;
				}
			}
			
			if (opc == null)
				opc = Colectivos.OPC_CANACEL.ERROR;
			
			
			if(this.causaAnulacion != null  && this.causaAnulacion.intValue() > 0 && this.fechaAnulacion != null 
					&& opc != Colectivos.OPC_CANACEL.ERROR)	{
				if(this.listaCertificados.size() > 0){
					
					for(BeanClienteCertif objeto: this.listaCertificados){
						
						if (objeto.isCheck()){
							PreCarga registro = new PreCarga();
							PreCargaId id;
							
							
							id = new PreCargaId(objeto.getCertificado().getId().getCoceCasuCdSucursal(), objeto.getCertificado().getId().getCoceCarpCdRamo()
									   , objeto.getCertificado().getId().getCoceCapoNuPoliza(), objeto.getId().getCoccIdCertificado()
									   , "CAN", objeto.getCertificado().getCoceMtSumaAsegurada().toString());
                            if (objeto.getCertificado().getCoceSubCampana() != null && objeto.getCertificado().getCoceSubCampana().length()>0 && !Constantes.DEFAULT_STRING_ID.equals(objeto.getCertificado().getCoceSubCampana()))
								id.setCopcNumPoliza(0L);
														
							if (objeto.getCertificado().getCoceMtSumaAsegurada() == null) id.setCopcSumaAsegurada("0");
								
							registro.setId(id);
																	
							registro.setCopcCargada("9");  //para diferenciar de la emision.
							
							registro.setCopcFeIngreso(new SimpleDateFormat("dd/MM/yyyy").format(objeto.getCertificado().getCoceFeSuscripcion()));
							registro.setCopcFeStatus(new SimpleDateFormat("dd/MM/yyyy").format(this.fechaAnulacion));
							registro.setCopcNuCredito(objeto.getCertificado().getCoceNuCredito());
							registro.setCopcDato2(objeto.getCertificado().getId().getCoceNuCertificado().toString());
							
							registro.setCopcDato11("CANCELACION INDIV");
							registro.setCopcDato16(objeto.getCertificado().getCoceSubCampana());
							
							if ((objeto.getCertificado().getCoceSubCampana() == null || Constantes.DEFAULT_STRING_ID.equals(objeto.getCertificado().getCoceSubCampana()) 
								   || objeto.getCertificado().getCoceSubCampana().trim().length() == 0) && opc != Colectivos.OPC_CANACEL.CANCELA){
								
								registro.setCopcCargada("10");  //para diferenciar de la emision.
								registro.setCopcRegistro("Error, no se puede aplicar cancelaci�n a este cr�dito con devolucion. No es Prima �nica");  //para diferenciar de la emision.
							
							}else{
								
								if (objeto.getCertificado().getCoceSubCampana() != null && !Constantes.DEFAULT_STRING_ID.equals(objeto.getCertificado().getCoceSubCampana()) 
									   && objeto.getCertificado().getCoceSubCampana().trim().length() > 0){
								
									if (this.primaDevuelta != null && this.primaDevuelta > 0.0) registro.setCopcPrima(this.primaDevuelta.toString());
									
									if (this.tasaAmortiza != null && this.tasaAmortiza.getCopaNvalor7() != null 
											&& this.tasaAmortiza.getCopaNvalor7() > 0.0) registro.setCopcDato1(this.tasaAmortiza.getCopaNvalor7().toString());
									
									if(isChkValPmaDev() == false) {
										sbQuery.append("AND P.copaDesParametro = 'DEVOLUCION' \n");
										sbQuery.append("AND P.copaIdParametro  = 'DIFERENCIA' \n");
										sbQuery.append("AND P.copaNvalor1      = ").append(registro.getId().getCopcCdSucursal()).append(" \n");
										sbQuery.append("AND P.copaNvalor2      = ").append(registro.getId().getCopcCdRamo()).append(" \n");
										sbQuery.append("AND P.copaNvalor4      = ").append(objeto.getCertificado().getCoceSubCampana()).append(" \n");

										objParam = (Parametros) this.servicioParametro.obtenerObjetos(sbQuery.toString()).get(0);
										objParam.setCopaNvalor5(Constantes.DEFAULT_INT);
										
										this.servicioParametro.actualizarObjeto(objParam);
									}
								}
								
							}	
							
							this.servicioCarga.guardarObjeto(registro);
							
							this.servicioProceso.cancelacionMasiva(registro.getId().getCopcCdSucursal(), registro.getId().getCopcCdRamo()
									, registro.getId().getCopcNumPoliza(), this.causaAnulacion.intValue(), this.opc);
							
							StringBuilder filtro = new StringBuilder();
							filtro.append(" AND PC.id.copcCdSucursal = ").append(registro.getId().getCopcCdSucursal());
							filtro.append(" AND PC.id.copcCdRamo = ").append(registro.getId().getCopcCdRamo());
							filtro.append(" AND PC.id.copcNumPoliza = ").append(registro.getId().getCopcNumPoliza());
							filtro.append(" AND PC.id.copcIdCertificado = '").append(registro.getId().getCopcIdCertificado()).append("' ");
							filtro.append(" AND PC.id.copcTipoRegistro = '").append(registro.getId().getCopcTipoRegistro()).append("' ") ;
							filtro.append(" AND PC.id.copcSumaAsegurada = '").append(registro.getId().getCopcSumaAsegurada()).append("'");
							List lstRes = this.servicioCarga.obtenerObjetos(filtro.toString());
							if(lstRes.isEmpty()){
								String obs = objeto.getCertificado().getCoceObservaciones()+"|"+objeto.getObservaciones2();
								objeto.getCertificado().setCoceObservaciones(obs);
								System.out.println(" <<<<---------- ANTES DE ACTUALIZAR CERTIFICADO ");
								this.servicioCertificado.actualizarObs(objeto.getCertificado().getId(),obs);
								System.out.println(" ---------->>>> DESPUES DE ACTUALIZAR CERTIFICADO ");
								
							}

							if(isChkValPmaDev() == false && objParam != null) {
								objParam.setCopaNvalor5(1);
								this.servicioParametro.actualizarObjeto(objParam);	
							}
						}
					}
					
					this.listaCertificadosCancelacion.clear();
					this.servicioCarga.estatusCancelacion(this.listaCertificados,this.listaCertificadosCancelacion);
					
					this.message.append("Proceso de Cancelaci�n Terminado");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
				}else{
					this.message.append("No hay certificados para cancelar.");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
				}
			
				return NavigationResults.SUCCESS;
			}else{
				
				if (opc == Colectivos.OPC_CANACEL.ERROR)
					this.message.append("Opciones de cancelaci�n incorrectas.");
				else
					this.message.append("Seleccione una causa y fecha de cancelaci�n.");
				
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				Utilerias.resetMessage(message);
				
				return NavigationResults.RETRY;
			}
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al cancelar los certifcicados.");
			this.log.error(message.toString(), e);
			this.respuesta = this.message.append("Error interno.").toString();
			return NavigationResults.FAILURE;
		}
	}
	
	
	public String cancelaCertificados(){

		try {
			
			Estatus estatus = this.servicioEstatus.obtenerObjeto(Estatus.class, 11);
			
			if(this.causaAnulacion != null  && this.causaAnulacion.intValue() > 0 && this.fechaAnulacion != null)
			{
				if(this.listaCertificadosCancelacion.size() > 0){
					
					for(BeanClienteCertif objeto: this.listaCertificadosCancelacion){
						
						Certificado certificado = objeto.getCertificado();
						
						//Actualizamos certificado
						certificado.setCoceFeAnulacion(new Date());
						certificado.setCoceFeAnulacionCol(this.fechaAnulacion);
						certificado.setCoceFeEstatus(new Date());
						certificado.setEstatus(estatus);
						certificado.setCoceCdCausaAnulacion(this.causaAnulacion);
						certificado.setCoceCampon2(new Long(2002));
						
						this.servicioCertificado.cancelaCertificado(certificado);
						
					}
					
					this.message.append("Certificados cancelados satisfactoriamente.");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
					
					this.listaCertificadosCancelacion.clear();
				}
				else{
					this.message.append("No hay certificados para cancelar.");
					this.log.debug(message.toString());
					this.respuesta = this.message.toString();
					Utilerias.resetMessage(message);
				}
			
				return NavigationResults.SUCCESS;
			}
			else{
				
				this.message.append("Seleccione una causa y fecha de cancelaci�n.");
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				Utilerias.resetMessage(message);
				
				return NavigationResults.RETRY;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al cancelar los certifcicados.");
			this.log.error(message.toString(), e);
			this.respuesta = this.message.append("Error interno.").toString();
			return NavigationResults.FAILURE;
		}
	}

	public String calculaDevolucion(){
		try {
			
			if(this.certifCancelado.getCertificado().getCoceCdPlazo() != null
			    && this.certifCancelado.getCertificado().getCoceMtPrimaSubsecuente() != null
			    && this.certifCancelado.getCertificado().getCoceFeIniCredito() != null
			    && this.certifCancelado.getCertificado().getCoceFeFinCredito() != null
			    && this.tasaAmortiza.getCopaNvalor7() != null
			    && this.fechaAnulacion != null){
				
				if (this.certifCancelado.getCertificado().getId() == null){
					this.certifCancelado.getCertificado().setId(new CertificadoId(new Short("1")
					                                                             ,new Short("61")
					                                                             ,0L
					                                                             ,0L));
					this.certifCancelado.getCertificado().setCoceSubCampana("6");
				}
				
				this.primaDevuelta = this.servicioProceso.calcularDevolucion(this.certifCancelado.getCertificado()
						, this.tasaAmortiza.getCopaNvalor7()/12, this.fechaAnulacion);
								
				this.message.append("C�lculo realizado");
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				Utilerias.resetMessage(message);
				
			}else{
				this.message.append("Necesita ingresar todos los datos.");
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				Utilerias.resetMessage(message);
			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CANCELACION_INDIVIDUAL);
						
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puedo realizar el calculo de la devoluci�n.");
			this.log.error(message.toString(), e);
			this.respuesta = this.message.append("Error interno.").toString();
			Utilerias.resetMessage(message);
			return NavigationResults.RETRY;
		}
	}
	
	public void determinaFechaFinCredito(){
		
		    Calendar c = Calendar.getInstance();
			if (this.certifCancelado.getCertificado().getCoceFeIniCredito() != null 
					&& this.certifCancelado.getCertificado().getCoceCdPlazo() != null){
				
				c.setTime(this.certifCancelado.getCertificado().getCoceFeIniCredito());
				c.add(Calendar.MONTH, this.certifCancelado.getCertificado().getCoceCdPlazo());
					
				this.certifCancelado.getCertificado().setCoceFeFinCredito(c.getTime());
			}
	}
	
	public void processDrop(DropEvent event){
		
		int existe = -1;
		
		try {
			
			BeanClienteCertif clienteCertif = (BeanClienteCertif) event.getDragValue();
			
			existe = this.listaCertificadosCancelacion.indexOf(clienteCertif);
			
			if(existe < 0){
				this.listaCertificadosCancelacion.add(clienteCertif);
				this.listaCertificados.remove(clienteCertif);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al seleccionar el certificado.");
			this.log.error(message.toString(), e);
			this.respuesta = this.message.toString();
		}	
	}
	
	public String resetCertificadosCancelados(){
		
		this.listaCertificadosCancelacion.clear();
		return null;
	}	

	public String muestraCriterio(){
		
		if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("1")){
			setDisplay1("block");
			setDisplay2("none");
			setDisplay3("none");
			setDisplay4("none");
			setDisplay5("none");
			setDisplayPE("none");
			
		}else{
			setDisplay1("none");
		}
		if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("2")){
			setDisplay1("none");
			setDisplay2("block");
			setDisplay3("none");
			setDisplay4("none");
			setDisplay5("none");
			setDisplayPE("none");
			
		}else{
			setDisplay2("none");
		}
		if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("3")){
			setDisplay1("none");
			setDisplay2("none");
			setDisplay3("block");
			setDisplay4("none");
			setDisplay5("none");
			setDisplayPE("none");
			
		}else{
			setDisplay3("none");
		}
		if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("4")){
			setDisplay1("none");
			setDisplay2("none");
			setDisplay3("none");
			setDisplay4("block");
			setDisplay5("none");
			setDisplayPE("none");
		}else{
			setDisplay4("none");
		}
		if(getCriterioBusqueda()!=null && getCriterioBusqueda().equals("5")){
			setDisplay1("none");
			setDisplay2("none");
			setDisplay3("none");
			setDisplay4("none");
			setDisplay5("block");
			setDisplayPE("none");
		}else{
			setDisplay5("none");
		}
			System.out.println("Entra a muestra criterios:"+getDisplayPE());	
			
			return getCriterioBusqueda();
	}
	
	public void muestraPE(){

		if(getPoliza()!=null && getPoliza().equals("1")){
			setDisplayPE("block");
			System.out.println("Entra a muestra BLOCK Poliza Especifica");
		}else{
			setDisplayPE("none");	
			System.out.println("Entra a muestra NONE Poliza Especifica");
		}
		
	}
	
	public void suma(){
		try{	
			
			Double total=0.0;
			Integer qty = 0;
			
			for(BeanClienteCertif objeto: this.listaCertificados){
				if (objeto.isCheck()){
					total+=objeto.getCertificado().getCoceMtPrimaSubsecuente().doubleValue();
					qty++;
				}
			}
			setTotal(total);
			setQty(qty);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void setTdc(String tdc) {
		this.tdc = tdc;
	}
	public String getTdc() {
		return tdc;
	}
	public void setDisplay2(String display2) {
		this.display2 = display2;
	}
	public String getDisplay2() {
		return display2;
	}
	public void setBuc(String buc) {
		this.buc = buc;
	}
	public String getBuc() {
		return buc;
	}
	public void setDisplay3(String display3) {
		this.display3 = display3;
	}
	public String getDisplay3() {
		return display3;
	}
	public void setDisplay4(String display4) {
		this.display4 = display4;
	}
	public String getDisplay4() {
		return display4;
	}
	public void setDisplay5(String display5) {
		this.display5 = display5;
	}
	public String getDisplay5() {
		return display5;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getCanal() {
		return canal;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getRamo() {
		return ramo;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setDisplay1(String display1) {
		this.display1 = display1;
	}
	public String getDisplay1() {
		return display1;
	}
	public void setPolizaEspecifica(String polizaEspecifica) {
		this.polizaEspecifica = polizaEspecifica;
	}
	public String getPolizaEspecifica() {
		return polizaEspecifica;
	}
	public void setMuestraPE(String muestraPE) {
		this.muestraPE = muestraPE;
	}
	public String getMuestraPE() {
		return muestraPE;
	}
	public void setDisplayPE(String displayPE) {
		this.displayPE = displayPE;
	}
	public String getDisplayPE() {
		return displayPE;
	}
	
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getColonia() {
		return colonia;
	}
	public void setDelegacionMunicipio(String delegacionMunicipio) {
		this.delegacionMunicipio = delegacionMunicipio;
	}
	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	public String getCalleNumero() {
		return calleNumero;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getRfc() {
		return rfc;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getSexo() {
		return sexo;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
    public Double getTotal() {
		return total;
	}
    public void setQty(Integer qty) {
		this.qty = qty;
	}
    public Integer getQty() {
		return qty;
	}
    public boolean isHabValDev() {
		return habValDev;
	}
	public void setHabValDev(boolean habValDev) {
		this.habValDev = habValDev;
	}
	public String getIdVentaSel() {
		return idVentaSel;
	}
	public void setIdVentaSel(String idVentaSel) {
		this.idVentaSel = idVentaSel;
	}
	/**
	 * Indica si selecciono Check
	 * @return
	 */
	public boolean isChkValPmaDev() {
		return chkValPmaDev;
	}
	/**
	 * Manda seleccionar Check
	 * @param chkValPmaDev
	 */
	public void setChkValPmaDev(boolean chkValPmaDev) {
		this.chkValPmaDev = chkValPmaDev;
	}
	
	/**
	 * @param servicioError the servicioError to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
}
