/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstado;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author dflores
 */
@Controller
@Scope("session")
public class BeanListaEstado implements InitializingBean{

	@Resource
	private ServicioEstado servicioEstado;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	//private Short inRamo;
	private HtmlSelectOneMenu inEstado;
	private String desEstado;
	private List<Object> listaEstados;
	private List<Object> listaComboEstados;
	private int numPagina;

	

	public BeanListaEstado() {
		// TODO Auto-generated constructor stub
		this.listaComboEstados = new ArrayList<Object>();
		this.listaEstados = new ArrayList<Object>();
		this.message = new StringBuilder();
		this.log.debug("BeanListaEstados creado");
		this.numPagina = 1;
		
	}
	
	
	
	public List<Object> getListaComboEstados() {
		return listaComboEstados;
	}



	public void setListaComboEstados(List<Object> listaComboEstados) {
		this.listaComboEstados = listaComboEstados;
	}



	/**
	 * @return the servicioEstado
	 */
	public ServicioEstado getServicioEstado() {
		return servicioEstado;
	}

	/**
	 * @param servicioProducto the servicioEstado to set
	 */
	public void setServicioEstado(ServicioEstado servicioEstado) {
		this.servicioEstado = servicioEstado;
	}


	/**
	 * @return the listaEstados
	 */
	public List<Object> getListaEstados() {
		return listaEstados;
	}

	/**
	 * @param listaEstados the listaEstados to set
	 */
	public void setListaEstados(List<Object> listaEstados) {
		this.listaEstados = listaEstados;
	}

	
	
	public HtmlSelectOneMenu getInEstado() {
		return inEstado;
	}

	public void setInEstado(HtmlSelectOneMenu inEstado) {
		this.inEstado = inEstado;
	}

	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}
	

	
	public String getDesEstado() {
		return desEstado;
	}



	public void setDesEstado(String desEstado) {
		this.desEstado = desEstado;
	}



	public String consultarEstados(){
		//this.listaEstados.clear();
		
		try {
			
							
			List<Object> lista = this.servicioEstado.obtenerObjetos();
				
			for(Object estado: lista){
			
				lista = this.servicioEstado.obtenerObjetos();
				
				this.listaEstados.add(estado);
				

			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_ESTADO);
			
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los estados.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			
			return NavigationResults.FAILURE;
		}
		
	}

	/*
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		this.listaEstados.clear();
		
		try {
			
							
			List<Object> lista = this.servicioEstado.obtenerEstados();
				
			for(Object estado: lista){
				
				//this.listaEstados.add(estado);
								
				this.listaEstados.add(new SelectItem(estado.toString()));
				

			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_ESTADO);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden recuperar los estados.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());

		}	
		
	}
	
*/
	
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
	
		try {
			
			//List<Estado> lista = this.servicioEstado.obtenerObjetos(Estado.class);
			List<Estado> lista = this.servicioEstado.obtenerObjetos();
			for(Object estado: lista){
			
				BeanEstado bean = (BeanEstado) ConstruirObjeto.crearBean(BeanEstado.class, estado);
				bean.setServicioEstado(this.servicioEstado);
				this.listaEstados.add(bean);
				this.listaComboEstados.add(new SelectItem(bean.getCaesCdEstado().toString(),bean.toString()));
				

			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se pueden cargar los estados");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}


}
