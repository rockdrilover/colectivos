package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

/**
 * @author Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanComplementarios {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioProcesos servicioProcesos;
	private File archivo;
	private String nombreArchivo;
	private String respuesta;

	public BeanComplementarios() {
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_COMPLEMENTARIOS);
	}

		
	public void listener(UploadEvent event) throws Exception {
		UploadItem item;
		File archivoTemporal;
		String ruta;
		String nombreArchivo = null;
		String nombreRuta;
		Archivo file;
		
		try {
			item = event.getUploadItem();
			ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS);
			file = new Archivo();
			
			if(item.getData() != null){
				file.setData(item.getData());
				file.setNombreArchivo(item.getFileName());
				file.setTamano(item.getData().length);
				nombreRuta = file.getNombreArchivo() + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
				nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
				archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
				FileUtil.write(archivoTemporal, item.getData());
				setArchivo(archivoTemporal);
				setRespuesta("Archivo cargado al servidor correctamente");
				log.debug(getRespuesta());
			} 
		} catch (Exception e) {
			setRespuesta("Error al cargar Archivo al servidor.");
			log.debug(getRespuesta());
		}
	}
	
	
	public String cargar() {
		String strForward = null;
		
		try {
			if(validarDatos()) {
				strForward = actualizarDatosComplementarios();
			} else {				
				log.debug(getRespuesta());
				if(getArchivo() != null)
					getArchivo().delete();
				strForward = NavigationResults.FAILURE;
			}
		} catch (Exception e) {
			setRespuesta("No se puede realizar el proceso de carga." );
			log.error(getRespuesta() + e.getLocalizedMessage(), e);
			if(getArchivo() != null)
				getArchivo().delete();		
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}

	private String actualizarDatosComplementarios() {
		List<Object> lstObjetosGuardar = null;
		String strForward = NavigationResults.SUCCESS;
		
		try {
			lstObjetosGuardar = new ArrayList<Object>();
			lstObjetosGuardar = leerArchivo();
			
			if(!lstObjetosGuardar.isEmpty()) {
				servicioProcesos.actualizarDatosComerciales(lstObjetosGuardar);
				setRespuesta("El proceso de actualizacion finaliz� correctamente, " + lstObjetosGuardar.size() + " registros actualizados.");
				lstObjetosGuardar.clear();
			} else {
				setRespuesta("No se emcontraron registros para actualizar");
			}
			log.info(getRespuesta());
		} catch (DataAccessException e) {
			setRespuesta("Hubo un error al guardar los datos.");
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		} catch (Exception e) {
			setRespuesta("No se puede realizar el proceso de carga." + e.getLocalizedMessage());
			log.error(getRespuesta(), e);
			getArchivo().delete();
			return NavigationResults.FAILURE;
		}
		
		return strForward;
	}
	
	private List<Object> leerArchivo() {
		CsvReader csv = null;
		FileReader fr;
		List<Object> lista = null;
		String[] arrDatos;
		String strLinea;
		
		try {
			lista = new ArrayList<Object>();
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			
			csv.readRecord();
			while(csv.readRecord()) {
				strLinea = csv.getRawRecord();
				arrDatos = strLinea.split("\\,");
				
				if(arrDatos.length == 17){
					lista.add(arrDatos);
				} 
			}

			csv.close();
			fr.close();
			getArchivo().delete();
		} catch (IOException e) {
			log.error("Error de lectura en el archivo", e);
			FacesUtils.addErrorMessage("Error de lectura en el archivo");
		} catch (Exception e) {
			log.error("No se puede cargar la informacion del archivo.", e);
			FacesUtils.addErrorMessage("No se puede cargar la informacion del archivo.");
		}
		
		return lista;
	}
	
	private boolean validarDatos() throws Exception{
		boolean blnResultado = true;
		
		if(getArchivo() == null) {
			setRespuesta("Favor de cargar un archivo.");
			blnResultado = false;
		}
			
		return blnResultado;
	}

	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	public File getArchivo() {
		return archivo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public ServicioProcesos getServicioProcesos() {
		return servicioProcesos;
	}
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}
	public void setMensaje(String mensaje) {
		this.respuesta = mensaje;
	}
	public String getMensaje() {
		return respuesta;
	}

	
}
