/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ProductoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Ramo;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


/**
 * @author dflores
 * Modificacion : Ing. Issac Bautista
 *
 */
@Controller
@Scope("request")
public class BeanProducto {

	@Resource
	private ServicioProducto servicioProducto;
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private Short alprCdRamo;
	private Integer alprCdProducto;
	private ProductoId id;
	private Ramo ramo;
	private String alprDeProducto;
	private String alprDato1;
	private String alprDato2;
	private Integer alprDato3;
	private Long alprDato4;
	private Date alprDato5;
	private Date alprDato6;
	private Set<Plan> planes;
	
	private BeanPlanes plan;
	
	private Short selectedRamo;
	
	private String idProducto;
	
	/**
	 * @param alprCdRamo the alprCdRamo to set
	 */
	public void setAlprCdRamo(Short alprCdRamo) {
		this.alprCdRamo = alprCdRamo;
	}

	/**
	 * @return the alprCdRamo
	 */
	public Short getAlprCdRamo() {
		return alprCdRamo;
	}

	/**
	 * @param alprCdProducto the alprCdProducto to set
	 */
	public void setAlprCdProducto(Integer alprCdProducto) {
		this.alprCdProducto = alprCdProducto;
	}

	/**
	 * @return the alprCdProducto
	 */
	public Integer getAlprCdProducto() {
		return alprCdProducto;
	}

	/**
	 * @return the servicioProducto
	 */
	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}

	/**
	 * @param servicioProducto the servicioProducto to set
	 */
	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}

	/**
	 * @return the id
	 */
	public ProductoId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(ProductoId id) {
		this.id = id;
	}

	/**
	 * @return the ramo
	 */
	public Ramo getRamo() {
		return ramo;
	}

	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(Ramo ramo) {
		this.ramo = ramo;
	}

	/**
	 * @return the alprDeProducto
	 */
	public String getAlprDeProducto() {
		return alprDeProducto;
	}

	/**
	 * @param alprDeProducto the alprDeProducto to set
	 */
	public void setAlprDeProducto(String alprDeProducto) {
		this.alprDeProducto = alprDeProducto;
	}

	/**
	 * @return the alprDato1
	 */
	public String getAlprDato1() {
		return alprDato1;
	}

	/**
	 * @param alprDato1 the alprDato1 to set
	 */
	public void setAlprDato1(String alprDato1) {
		this.alprDato1 = alprDato1;
	}

	/**
	 * @return the alprDato2
	 */
	public String getAlprDato2() {
		return alprDato2;
	}

	/**
	 * @param alprDato2 the alprDato2 to set
	 */
	public void setAlprDato2(String alprDato2) {
		this.alprDato2 = alprDato2;
	}

	/**
	 * @return the alprDato3
	 */
	public Integer getAlprDato3() {
		return alprDato3;
	}

	/**
	 * @param alprDato3 the alprDato3 to set
	 */
	public void setAlprDato3(Integer alprDato3) {
		this.alprDato3 = alprDato3;
	}

	/**
	 * @return the alprDato4
	 */
	public Long getAlprDato4() {
		return alprDato4;
	}

	/**
	 * @param alprDato4 the alprDato4 to set
	 */
	public void setAlprDato4(Long alprDato4) {
		this.alprDato4 = alprDato4;
	}

	/**
	 * @return the alprDato5
	 */
	public Date getAlprDato5() {
		return alprDato5;
	}

	/**
	 * @param alprDato5 the alprDato5 to set
	 */
	public void setAlprDato5(Date alprDato5) {
		this.alprDato5 = alprDato5 == null ? new Date() : alprDato5;
	}

	/**
	 * @return the alprDato6
	 */
	public Date getAlprDato6() {
		return alprDato6;
	}

	/**
	 * @param alprDato6 the alprDato6 to set
	 */
	public void setAlprDato6(Date alprDato6) {
		this.alprDato6 = alprDato6 == null ? new Date() : alprDato6;
	}

	/**
	 * @return the planes
	 */
	public Set<Plan> getPlanes() {
		return planes;
	}

	/**
	 * @param planes the planes to set
	 */
	public void setPlanes(Set<Plan> planes) {
		this.planes = planes;
	}

	/**
	 * @param selectedRamo the selectedRamo to set
	 */
	public void setSelectedRamo(Short selectedRamo) {
		this.selectedRamo = selectedRamo;
	}

	/**
	 * @return the selectedRamo
	 */
	public Short getSelectedRamo() {
		return selectedRamo;
	}
	
	public String getIdProducto() {
		return idProducto;
	}
	
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	
	//lpv
	public void setPlan(BeanPlanes plan) {
		this.plan = plan;
	}
	
	public BeanPlanes getPlan() {
		return plan;
	}
	
	public BeanProducto() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.alprDato5 = new Date();
		this.alprDato6 = new Date();
		
		if(FacesUtils.getBeanSesion().getCurrentBeanProducto() != null){
			
			try {
				ConstruirObjeto.poblarBean(this, FacesUtils.getBeanSesion().getCurrentBeanProducto());
			} catch (Excepciones e) {
				// TODO Auto-generated catch block
				this.message.append("No se puede crear el bean producto.");
				this.log.error(message.toString(), e);
				throw new FacesException(message.toString(), e);
			}
		}
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PRODUCTO);
	}
	
	
	
	public String guardarProducto(){
		
		Integer secProducto;
		
		try {
			
			secProducto = this.servicioProducto.siguenteProducto(this.selectedRamo);
			
			this.id = new ProductoId(this.selectedRamo, secProducto);
			this.alprCdRamo = this.selectedRamo;
			
			Producto producto = (Producto) ConstruirObjeto.crearObjeto(Producto.class, this);
			
			this.servicioProducto.guardarObjeto(producto);
			
			return NavigationResults.SUCCESS;
			
		}
		catch (ObjetoDuplicado e) {
			// TODO: handle exception
			this.message.append("Producto duplicado.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}
		catch (Excepciones e) {
			// TODO: handle exception
			this.message.append("No se puede dar de alta el producto.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.FAILURE;
			
		}
	}
	
	public String actualizarProducto(){
		
		try {
			
			Producto producto = (Producto) ConstruirObjeto.crearObjeto(Producto.class, this);
			this.servicioProducto.actualizarObjeto(producto);
			
			
			FacesUtils.getBeanSesion().setCurrentBeanProducto(null);
			
			return NavigationResults.SUCCESS;
			
		} catch (Excepciones e) {
			// TODO: handle exception
			this.message.append("Error al actualizar el producto.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			FacesUtils.getBeanSesion().setCurrentBeanProducto(null);
			
			return NavigationResults.FAILURE;
		}
	}
	
	public String borrarProducto(){
		
		try {
			
			Producto producto = (Producto) ConstruirObjeto.crearObjeto(Producto.class, this);
			this.servicioProducto.borrarObjeto(producto);
			
			FacesUtils.getBeanSesion().setCurrentBeanProducto(null);
			
			return NavigationResults.SUCCESS;
			
		} catch (Excepciones e) {
			// TODO: handle exception
			this.message.append("No se puede eliminar el producto.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			FacesUtils.getBeanSesion().setCurrentBeanProducto(null);
			
			return NavigationResults.FAILURE;
		}
	}
	public String cancelar(){
		
		((BeanListaProducto)FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PRODUCTO)).getListaProductos().clear();
		return NavigationResults.LISTA_PRODUCTOS;
	}


}
