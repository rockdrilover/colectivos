package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioReporteStock;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.component.html.HtmlProgressBar;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
@Controller
@Scope("session")
public class BeanReporteErrores {

	@Resource
	private ServicioReporteStock servicioReporteStock;
	private Log log = LogFactory.getLog(this.getClass());
	private String mensaje;
	private Short ramo;
	private Long poliza;
	private Integer inIdVenta;
	private Integer inTipoError;
	private Date fecini;
	private Date fecfin;
	private List<Object> lstColumnas;
	private List<Object> lstResultados;
	private ProgressBar pb;
	private ExecutorService pool;
	
	public BeanReporteErrores() {
		this.inIdVenta = 0;
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		this.pool = Executors.newFixedThreadPool(1);
		setLstColumnas(new ArrayList<Object>());
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_REPORTE_ERRORES);
	}
	
	public String limpiar(){
		try {
			new BeanReporteErrores();
			setRamo(new Short("-1"));
			setPoliza(-1L);
			setInIdVenta(-1);
			setInTipoError(-1);
			setFecini(null);
			setFecfin(null);
			return null;	
		} catch (Exception e) {
			e.printStackTrace();			
			return null;
		}
		
	}
	
	public String generaReporte(){
		try {
			if(validarDatos()){				
				getCamposColumnas();
				lstResultados = servicioReporteStock.consultarErrores(getRamo(), getPoliza(), getInIdVenta(), getFecfin(), getFecini(), getInTipoError());
				descargar();
				setMensaje("Reporte se Genero Satisfactoriamente.");
			} 
			
			return null;	
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje("Error al consultar informacion.");
			log.error(getMensaje(), e);
			return null;
		}
		
	}
	
	public void descargar(){
		String strNombre = null, strRutaTemp;
		Iterator<Object> it;
		int contador = 0;
		Archivo objArchivo;
		
		try {
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes") + File.separator;
			String arrColumnas[] = new String[getLstColumnas().size()];
			it = getLstColumnas().iterator();
			while(it.hasNext()){
				arrColumnas[contador] = it.next().toString();
				contador++;
			}
			strNombre = "Reporte Errores_" + getInIdVenta() + " - " + GestorFechas.formatDate(new Date(), "ddMMyyyy"); 
			objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
			objArchivo.copiarArchivo(arrColumnas, getLstResultados(), "|");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
			objArchivo.eliminarArchivo();
			
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje("Error al descargar informacion.");
			log.error(getMensaje(), e);
		}
	}
	
	private boolean validarDatos() {
		boolean blnResultado = true;
		
		if (getRamo() == -1 || getPoliza() == -1 || getFecini() == null || getFecfin() == null){
			setMensaje("Favor de ingresar un filtro de busqueda");
			blnResultado = false;
		} 
		
		if(getInIdVenta() == -1) {
			setInIdVenta(0);
		}
		
		if(getFecini() != null  && getFecfin() == null){
			setMensaje("Favor de ingresar fecha fin");
			blnResultado = false;
		} 
		
		if(getFecini() == null && getFecfin() != null){
			setMensaje("Favor de ingresar fecha inicio");
			blnResultado = false;
		}
		
		if(getFecini() != null && getFecfin() != null) {
			if(getFecini().after(getFecfin())){
				setMensaje("Fecha Inicio no debe ser mayor a Fecha Fin");
				blnResultado = false;
			}
		}
		
		return blnResultado;
	}
	
	private void getCamposColumnas() throws Exception{
		try {
			lstColumnas.clear();
			lstColumnas.add("RAMO");
			lstColumnas.add("CREDITO");
			lstColumnas.add("IDVENTA");
			lstColumnas.add("DESC_ERROR");
			lstColumnas.add("FECHA_INGRESO");
		} catch (Exception e) {
			throw new Exception("BeanReporteErrores.getCamposColumnas()::: " + e.getMessage());
		}
	}
	
	public ServicioReporteStock getServicioReporteStock() {
		return servicioReporteStock;
	}
	public void setServicioReporteStock(ServicioReporteStock servicioReporteStock) {
		this.servicioReporteStock = servicioReporteStock;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getMensaje() {
		return mensaje;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	public Long getPoliza() {
		return poliza;
	}
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	public Integer getInIdVenta() {
		return inIdVenta;
	}
	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}
	public Integer getInTipoError() {
		return inTipoError;
	}
	public void setInTipoError(Integer inTipoError) {
		this.inTipoError = inTipoError;
	}
	public Date getFecini() {
		return fecini;
	}
	public void setFecini(Date fecini) {
		this.fecini = fecini;
	}
	public Date getFecfin() {
		return fecfin;
	}
	public void setFecfin(Date fecfin) {
		this.fecfin = fecfin;
	}
	public List<Object> getLstColumnas() {
		return lstColumnas;
	}
	public void setLstColumnas(List<Object> lstColumnas) {
		this.lstColumnas = lstColumnas;
	}
	public List<Object> getLstResultados() {
		return lstResultados;
	}
	public void setLstResultados(List<Object> lstResultados) {
		this.lstResultados = lstResultados;
	}
	public Integer getProgressValue() {
		return pb.getProgressValue();
	}
	public void setProgressValue(Integer progressValue) {
		this.pb.setProgressValue(progressValue);
	}
	public Boolean getProgressHabilitar() {
		return pb.isProgressHabilitar();
	}
	public void setProgressHabilitar(Boolean progressHabilitar) {
		this.pb.setProgressHabilitar(progressHabilitar);
	}
	public HtmlProgressBar getProgressBar() {
		return pb.getProgressBar();
	}
	public void setProgressBar(HtmlProgressBar progressBar) {
		this.pb.setProgressBar(progressBar);
	}
	
}
