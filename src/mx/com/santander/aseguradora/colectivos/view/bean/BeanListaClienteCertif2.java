package mx.com.santander.aseguradora.colectivos.view.bean;

import javax.annotation.Resource;
import javax.faces.FacesException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-06-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Consulta detalle de creditos.
 * 				consultaDetalleCredito.jsp
 * 				
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	
 * 		Cuando:
 * 		Porque:
 * ------------------------------------------------------------------------
 * 
 */
public class BeanListaClienteCertif2  {

	//recurso para acceder al servicio de genericos
	@Resource
	protected ServicioGeneric servicioGeneric;
	
	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanListaClienteCertif2.class);

	protected BeanClienteCertif objCliCertif;
	
	/**
	 * Constructor de clase
	 */
	public BeanListaClienteCertif2() {
		// Dont Using
	}
	
	/**
	 * Metodo para migrar informacion a RECTOR
	 * @throws Excepciones con error en general
	 */
	public void migraInfo() throws Excepciones {
		
		try {
			servicioGeneric.migraInformacion(getObjCliCertif().getId());
		} catch (Exception e) {
			LOG.error("Error al migrar informacion", e);
			throw new FacesException("Error al migrar informacion", e);
		}
		
	}

	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
	
	/**
	 * @return the objCliCertif
	 */
	public BeanClienteCertif getObjCliCertif() {
		return objCliCertif;
	}
	/**
	 * @param objCliCertif the objCliCertif to set
	 */
	public void setObjCliCertif(BeanClienteCertif objCliCertif) {
		this.objCliCertif = objCliCertif;
	}
}
