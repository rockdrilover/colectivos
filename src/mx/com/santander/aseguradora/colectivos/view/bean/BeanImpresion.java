package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioImpresion;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.SmbUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import jcifs.smb.SmbFile;


/**
 * @author FEBG
 *
 */
@Controller
@Scope("session")
public class BeanImpresion {
	
	@Resource
	private ServicioImpresion servicioImpresion;
	@Resource
	private ServicioClienteCertif servicioCliente;
	
	
	private StringBuilder message;
	
	private Log log = LogFactory.getLog(this.getClass());
	private String respuesta;
	private Short ramoFac;
	private Long inPoliza;
	private String opcion;
	private String id;
	private String strNombreZip = null;
	private List<BeanCertificado> listaCerti;
	private String viewButton="this.disabled=false";
	
	public BeanImpresion() {
		this.respuesta = Constantes.DEFAULT_STRING;
		this.listaCerti = new ArrayList<BeanCertificado>();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_IMPRESION);
	}

	public String cargaPolizas() throws Exception{
		this.listaCerti.clear();
		BeanCertificado beancerti;

		try{
			List<Object[]> polizas = servicioImpresion.cargaPolizasImpresion(ramoFac, inPoliza);
			if(!polizas.isEmpty()){
				this.respuesta = "";
				this.respuesta = "Consulta Exitosa.";
				for (Object[] cargar : polizas) {
					beancerti = new BeanCertificado();
					beancerti.getId().setCoceCasuCdSucursal(((BigDecimal) cargar[0]).shortValue());
					beancerti.getId().setCoceCarpCdRamo(((BigDecimal) cargar[1]).shortValue());
					beancerti.getId().setCoceCapoNuPoliza(((BigDecimal) cargar[2]).longValue());
					beancerti.setCoceCampon1(((BigDecimal) cargar[3]).longValue());
					beancerti.setCoceCampon2(((BigDecimal) cargar[4]).longValue());
					listaCerti.add(beancerti);
				}
			} else {
				this.respuesta = "No se encontraron resultados en la consulta.";
			}
		} catch (Exception e){
			e.printStackTrace();
			log.error("Error al Consultar las polizas.", e);
			FacesUtils.addInfoMessage("Error al Consultar la poliza BeanImpresion.cargaPolizas" );
		}
		
		return null;
	}
		
	public String getReportePlantilla() throws Exception {	
		List<Object> lstResultados = null;
		Iterator<Object> it;
		Object[] arrDatos;
		String rutaApp = "";
		Parametros smbRuta;
		String strNomrePlantilla = "";
		String rutaPlantillas = null;		
		SmbUtil smbUtil;
		SmbFile objDir;
		SmbFile[] arrFiles = null;
		File rutaArchivos = null;
		String strRespuesta = "";
		boolean blnDescarga = false;
		
		int BUFFER_SIZE = 1024;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		ZipOutputStream zipos = null;
		ZipEntry zipEntry = null;
		byte[] buffer = new byte[BUFFER_SIZE];
		String strNombre;
		
		try {
			lstResultados = this.servicioImpresion.obtenerRegistros(ramoFac, inPoliza);
			it = lstResultados.iterator();
			while(it.hasNext()) {
				arrDatos = (Object[]) it.next();
				strNombreZip = "CertificadosZip_" + arrDatos[0].toString() + "_" + arrDatos[1].toString() + "_" + arrDatos[2].toString();
				strNombre = arrDatos[0].toString() + "_" + arrDatos[1].toString() + "_" + arrDatos[2].toString() + "_" + arrDatos[3].toString();
				
				rutaApp = FacesUtils.getServletContext().getRealPath("/WEB-INF/plantillas/") + File.separator;
				smbRuta =  servicioCliente.getRutaPlantilla();
				strNomrePlantilla = servicioCliente.getPlantilla(Short.parseShort(arrDatos[0].toString()),
																Short.parseShort(arrDatos[1].toString()),
																Long.parseLong(arrDatos[1].toString()), 
																Long.parseLong(arrDatos[3].toString()),
																Integer.parseInt(arrDatos[4].toString()), 
																Integer.parseInt(arrDatos[5].toString()), 
																(Date)arrDatos[6]);
				
				smbUtil = new SmbUtil(smbRuta.getCopaVvalor6(), smbRuta.getCopaVvalor7());
				rutaPlantillas = smbRuta.getCopaRegistro();
				objDir = smbUtil.getFile(rutaPlantillas);

				arrFiles = objDir.listFiles();
				if(arrFiles != null) {
					for(SmbFile objFile : arrFiles) {
						if(!objFile.isDirectory()) {
							if(objFile.getName().equals(strNomrePlantilla)){
								System.out.println(":::Copiando Archivo a PathApp::: " + new Date() + " ::" + objFile.getName());
								Archivo objArchivoSmb = new Archivo(smbUtil.getFile(rutaPlantillas + objFile.getName()));
								objArchivoSmb.copiarSmbArchivo2(new File(rutaApp + objFile.getName())); //copiar a carpeta de aplicativo
								System.out.println(":::FIN Copiando Archivo a PathApp::: " + new Date() + " ::" + objFile.getName());
								strRespuesta = "Si hay plantilla en ruta";
								break;
							}
						}
					}
				}

				if(!strRespuesta.equals(Constantes.DEFAULT_STRING_ID)) {
					strRespuesta = servicioCliente.generaCertificado(rutaApp, Short.parseShort(arrDatos[0].toString()),
																	Short.parseShort(arrDatos[1].toString()),
																	Long.parseLong(arrDatos[2].toString()), 
																	Long.parseLong(arrDatos[3].toString()),
																	Integer.parseInt(arrDatos[4].toString()), 
																	Integer.parseInt(arrDatos[5].toString()), 
																	(Date) arrDatos[6], rutaApp + strNombre + ".pdf", strNomrePlantilla, Long.parseLong(arrDatos[1].toString()));
					
					if(!strRespuesta.equals(Constantes.DEFAULT_STRING_ID)) {						
						blnDescarga = true;
					}
				}
			}
			
			if(blnDescarga) {
				rutaArchivos = new File(rutaApp);
				fos = new FileOutputStream(FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator + strNombreZip + ".zip");
				zipos = new ZipOutputStream(fos);
				String[] fileNames = rutaArchivos.list();
				
				if (fileNames.length > 0) {
					for (int i = 0 ; i < fileNames.length ; i++){
						if(fileNames[i].contains(inPoliza.toString())) {
							zipEntry = new ZipEntry(fileNames[i].replace(rutaApp.toString(), ""));
							fis = new FileInputStream(rutaApp + zipEntry.toString());
							zipos.putNextEntry(zipEntry);
							int len = 0;
							while((len = fis.read(buffer, 0, BUFFER_SIZE)) != -1) {
								zipos.write(buffer, 0, len);
							}
							zipos.flush();														
						}
					}
				}
				zipos.close();
				fis.close();
				fos.close();
				this.setRespuesta("El proceso termin� correctamente, para descargar archivo ZIP dar clic en imagen de la columna Descargar Zip");
			}
		} catch (Exception e) {
			System.out.println("Error al generar la impresion de certificados.");
			e.printStackTrace();
		}
		return "success";
	}
	
	
	public void getZip() throws Exception {
		Archivo objArchivo;
		
		try {
			objArchivo = new Archivo(FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator, strNombreZip, ".zip");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 5);
			objArchivo.eliminarArchivo();
			FacesContext.getCurrentInstance().responseComplete();
			System.out.println("Se genero con exito la descarga");
			getDropFile();
		} catch (Exception e) {
			System.out.println("Error al descargar el archivo Zip");
			e.printStackTrace();
		}
		
		
	}
		
	public void getDropFile() throws Exception{
		Archivo objArchivo;
		String rutaPlantillas;
		File rutaArchivos;
		File[] fileNames;
		
		try {
			rutaPlantillas = FacesUtils.getServletContext().getRealPath("/WEB-INF/plantillas/") + File.separator;
			rutaArchivos = new File(rutaPlantillas);
			fileNames = rutaArchivos.listFiles();
			if(fileNames.length > 0) {
				for(int x = 0 ; x < fileNames.length ; x++) {
					if(fileNames[x].getName().contains(".pdf")) {
						objArchivo = new Archivo(fileNames[x]);
						objArchivo.eliminarArchivo();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Error al generar la impresion de certificados.");
			e.printStackTrace();
		}
	}
	
	public void reporteAsegurado() {
		String reporte = "";
		String rutaTemp;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes = new byte[1024];
		int read = 0;
		Archivo objArchivo;
		
		if(this.ramoFac != 0 && this.inPoliza != -1){
			try {
				rutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator; //rutaPlantilla
				rutaReporte = servicioImpresion.reporteAsegurado(rutaTemp, ramoFac, inPoliza);
			    reporte = rutaReporte.split("\\.")[0];
				
				if(rutaReporte != null){
					HttpServletResponse response = FacesUtils.getServletResponse();
					File file = new File(rutaTemp + rutaReporte);
					input = new FileInputStream(file);
					OutputStream out = response.getOutputStream();
					
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", "attachment;filename=\"" + rutaReporte + "\"");
					
					while((read = input.read(bytes)) != -1){
						out.write(bytes,0,read);
				    }
					
					input.close();		
					out.flush();
					out.close();
					objArchivo = new Archivo(rutaTemp, reporte, ".pdf");
					objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 4);
					objArchivo.eliminarArchivo();
					FacesContext.getCurrentInstance().responseComplete();
					this.setRespuesta("Reporte de Asegurados Creado correctamente");
				} else {
					rutaReporte = null;
					this.setRespuesta("Ocurri� un ERROR al Crear el Reporte de Asegurados");
				}

			} catch(Exception e) {
				System.out.println("Error al intentar generar el reporte de asegurados");
				this.setRespuesta("Ocurri� un ERROR al Crear el Reporte de Asegurados");
				e.printStackTrace();
			}
		} else {
			this.setRespuesta("Debe elegir ramo y poliza para generar el reporte de asegurados");
		}
	}
	
	public Short getRamoFac() {
		return ramoFac;
	}
	public void setRamoFac(Short selectedRamoFac) {
		this.ramoFac = selectedRamoFac;
	}
	
	public Long getInPoliza() {
		return inPoliza;
	}
	public void setInPoliza(Long inPoliza) {
		this.inPoliza = inPoliza;
	}
	
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	
	public List<BeanCertificado> getListaCerti() {
		return listaCerti;
	}
	public void setListaCerti(List<BeanCertificado> listaCerti) {
		this.listaCerti = listaCerti;
	}
	
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getRespuesta() {
		return respuesta;
	}
	
	public ServicioImpresion getServicioImpresion() {
		return servicioImpresion;
	}

	public void setServicioImpresion(ServicioImpresion servicioImpresion) {
		this.servicioImpresion = servicioImpresion;
	}

    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getViewButton() {
		return viewButton;
	}
	public void setViewButton(String viewButton) {
		this.viewButton = viewButton;
	}
}