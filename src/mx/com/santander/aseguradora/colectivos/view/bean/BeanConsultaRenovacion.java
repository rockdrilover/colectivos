/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPoliza;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.richfaces.component.html.HtmlProgressBar;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanConsultaRenovacion extends BeanBase {

	@Resource
	private ServicioPoliza servicioPoliza;
	@Resource
	private ServicioCertificado servicioCertificado;
	private List<BeanCertificado> listaPolizasRenovadas;
	private Short canal;
	private Short ramo;
	private Long poliza;
	private Long certificados;
	private String fechaDesde;
	private String fechaHasta;
	private Short numRenovacion;
	private String fechaRenovacion;
	private Double dias;
	private List<BeanCertificado> listaCertificados;
	private StringBuilder message;

	private String respuesta;
	
	private ProgressBar pb;
	private ExecutorService pool;

	/**
	 * @return the canal
	 */
	public Short getCanal() {
		return canal;
	}
	/**
	 * @return the ramo
	 */
	public Short getRamo() {
		return ramo;
	}
	/**
	 * @return the poliza
	 */
	public Long getPoliza() {
		return poliza;
	}
	/**
	 * @return the fechaDesde
	 */
	public String getFechaDesde() {
		return fechaDesde;
	}
	/**
	 * @return the fechaHasta
	 */
	public String getFechaHasta() {
		return fechaHasta;
	}
	/**
	 * @return the numRenovacion
	 */
	public Short getNumRenovacion() {
		return numRenovacion;
	}
	/**
	 * @return the fechaRenovacion
	 */
	public String getFechaRenovacion() {
		return fechaRenovacion;
	}
	/**
	 * @return the dias
	 */
	public Double getDias() {
		return dias;
	}
	/**
	 * @param canal the canal to set
	 */
	public void setCanal(Short canal) {
		this.canal = canal;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	/**
	 * @param fechaDesde the fechaDesde to set
	 */
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	/**
	 * @param fechaHasta the fechaHasta to set
	 */
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	/**
	 * @param numRenovacion the numRenovacion to set
	 */
	public void setNumRenovacion(Short numRenovacion) {
		this.numRenovacion = numRenovacion;
	}
	/**
	 * @param fechaRenovacion the fechaRenovacion to set
	 */
	public void setFechaRenovacion(String fechaRenovacion) {
		this.fechaRenovacion = fechaRenovacion;
	}
	/**
	 * @param dias the dias to set
	 */
	public void setDias(Double dias) {
		this.dias = dias;
	}
	
	/**
	 * @param certificados the certificados to set
	 */

	public void setCertificados(Long certificados) {
		this.certificados = certificados;
	}
	
	public List<BeanCertificado> getListaPolizasRenovadas() {
		return listaPolizasRenovadas;
	}
	public void setListaPolizasRenovadas(List<BeanCertificado> listaPolizasRenovadas) {
		this.listaPolizasRenovadas = listaPolizasRenovadas;
	}
	/**
	 * @return the certificados
	 */
	public Long getCertificados() {
		return certificados;
	}
	
	/**
	 * @return the intBarra
	 */
	public Integer getProgressValue() {
		return pb.getProgressValue();
	}

	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressValue(Integer progressValue) {
		this.pb.setProgressValue(progressValue);
	}
	
	/**
	 * @return the intBarra
	 */
	public Boolean getProgressHabilitar() {
		return pb.isProgressHabilitar();
	}

	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressHabilitar(Boolean progressHabilitar) {
		this.pb.setProgressHabilitar(progressHabilitar);
	}
	
	/**
	 * @return the progressBar
	 */
	public HtmlProgressBar getProgressBar() {
		return pb.getProgressBar();
	}

	/**
	 * @param progressBar the progressBar to set
	 */
	public void setProgressBar(HtmlProgressBar progressBar) {
		this.pb.setProgressBar(progressBar);
	}
	
	public List<BeanCertificado> getListaCertificados() {
		return listaCertificados;
	}
	public void setListaCertificados(List<BeanCertificado> listaCertificados) {
		this.listaCertificados = listaCertificados;
	}
	
	public ServicioPoliza getServicioPoliza() {
		return servicioPoliza;
	}
	public void setServicioPoliza(ServicioPoliza servicioPoliza) {
		this.servicioPoliza = servicioPoliza;
	}
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.respuesta = mensaje;
	}

	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return respuesta;
	}
	
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public BeanConsultaRenovacion() {
		this.message = new StringBuilder();
		this.listaCertificados = new ArrayList<BeanCertificado>();
		this.listaPolizasRenovadas = new ArrayList<BeanCertificado>();
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		this.pool = Executors.newFixedThreadPool(1);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONSULTA_RENOVACION);
		
	}
	
	public String consultaPolizasRenovacion(){
		this.listaCertificados.clear();
		this.listaPolizasRenovadas.clear();
		try {
			//log.info(canal +  ramo);
			List<Object[]> lista= this.servicioPoliza.consultaPolizasRenovacion();
			
			for(Object[] certificado: lista){

				BeanCertificado bean = new BeanCertificado();
				bean.getId().setCoceCarpCdRamo(((BigDecimal)certificado[0]).shortValue());
				bean.getId().setCoceCapoNuPoliza(((BigDecimal)certificado[1]).longValue());
				bean.setCoceFeDesde((Date)certificado[2]);
				bean.setCoceFeHasta((Date)certificado[3]);
				bean.setCoceCampon1(((BigDecimal)certificado[4]).longValue());
				bean.setServicioCertificado(this.servicioCertificado);

				this.listaCertificados.add(bean);
			}	
		return null;
		} catch (Exception e) {
			this.message.append("Error al traer montos de facturación.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	public void renovar() {
		
		this.log.debug("Iniciando proceso de carga (Hilo)");
		
		this.pb.setProgressHabilitar(true);
		this.setProgressValue(100);
		
		Runnable hilo = new HiloProcesos(this,BeanNames.BEAN_CONSULTA_RENOVACION);
		pool.execute(hilo);
	}

	public String upchk () throws Excepciones { 
		
		this.listaPolizasRenovadas.clear();
		//this.listaPrefact.clear();
		System.out.println("Entre al UpCHK --- BeanConsultaRenovacion.java");
		try {
			String resp="";
			for (Object item: listaCertificados){
				BeanCertificado certificado = (BeanCertificado) item;				
				if (certificado.getChkUpdate() == true){				
					System.out.println("Bean Proceso de Renovación --- BeanConsultaRenovacion.java");
					System.out.println("Ramo " + certificado.getId().getCoceCarpCdRamo());
					System.out.println("Poliza " + certificado.getId().getCoceCapoNuPoliza());
					resp = this.servicioPoliza.procesoRenovacionPolizas(certificado.getId().getCoceCarpCdRamo(), certificado.getId().getCoceCapoNuPoliza());
					System.out.println("Regreso a Bean Proceso de Renovación --- BeanConsultaRenovacion.java");
					System.out.println("--------------------------------------------------");
					System.out.println("---------" + resp + "--------------------------------------");
					System.out.println("--------------------------------------------------");
				}
			}
			
			List<Object[]> listaPR= this.servicioPoliza.consultaPolizasRenovadas();
			for(Object[] certificado: listaPR){

				BeanCertificado bean = new BeanCertificado();
				bean.getId().setCoceCasuCdSucursal(((BigDecimal)certificado[0]).shortValue());
				bean.getId().setCoceCarpCdRamo(((BigDecimal)certificado[1]).shortValue());
				bean.getId().setCoceCapoNuPoliza(((BigDecimal)certificado[2]).longValue());
				bean.setCoceCampov1((String) certificado[3]);
				bean.setCoceCampon1(((BigDecimal) certificado[4]).longValue());
				bean.setCoceCampon5((BigDecimal) certificado[5]);
				this.listaPolizasRenovadas.add(bean);
			}	
			
			log.info("Salgo del proceso de Renovación --- Bean");
			//System.out.println(listaPrefact.toString());
			//this.listaCertificados.clear();
			return null;
		}	
		catch (Exception e) {
			this.message.append("Error al Traer los certificados.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	public String detallePolizasRenovadas(){
		this.listaCertificados.clear();
		this.listaPolizasRenovadas.clear();
		String fec			= null;

		try {
			List<Object[]> lista= this.servicioPoliza.detallePolizasRenovadas();
			Calendar now = new GregorianCalendar();
			Date nowDate = now.getTime();

            Format formatter;
            // The day
            //formatter = new SimpleDateFormat("dd");    // 09
            // The month
            //formatter = new SimpleDateFormat("MM");    // 01
            // The year
            //formatter = new SimpleDateFormat("yyyy");  // 2002

            //formatter = new SimpleDateFormat("dd/MM/yyyy");
            formatter = new SimpleDateFormat("ddMMyyyy");
             
            fec = formatter.format(nowDate);
			
            log.info("Detalle Polizas Renovadas");
            
			BufferedWriter  output = null ;
			int a=0;
			 
			String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;

			File file = new File ( ruta + "RENOVACION_POLIZAS_" + fec +  ".csv" ); 

			output = new BufferedWriter ( new FileWriter ( file ));  

			log.info("Se imprimio el archivo " + a);
			output.write("RAMO,POLIZA,CERTIFICADO,ESTATUS,APELLIDO PATERNO,APELLIDO MATERNO,NOMBRE,CREDITO,PRIMA COBRADA,SUMA ASEGURADA");
			output.newLine();

			for(Object[] certificado: lista){
			   	StringBuffer sb = new StringBuffer ();

			   	BeanCertificado bean = new BeanCertificado();
				bean.getId().setCoceCarpCdRamo(((BigDecimal)certificado[0]).shortValue());
				bean.getId().setCoceCapoNuPoliza(((BigDecimal)certificado[1]).longValue());
				bean.getId().setCoceNuCertificado(((BigDecimal)certificado[2]).longValue());
				bean.setCoceCampov1((String) certificado[3]);
				bean.setCoceCampov2((String) certificado[4]);
				bean.setCoceCampov3((String) certificado[5]);
				bean.setCoceCampov4((String) certificado[6]);
				bean.setCoceNuCredito((String) certificado[7]);
				bean.setCoceMtPrimaSubsecuente(((BigDecimal)certificado[8]));
				bean.setCoceMtSumaAsegurada(((BigDecimal)certificado[9]));
				this.listaPolizasRenovadas.add(bean);
				
				sb.append(this.listaPolizasRenovadas.get(a).getId().getCoceCarpCdRamo()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getId().getCoceCapoNuPoliza()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getId().getCoceNuCertificado()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getCoceCampov1()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getCoceCampov2()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getCoceCampov3()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getCoceCampov4()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getCoceNuCredito()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getCoceMtPrimaSubsecuente()+ ",");
				sb.append(this.listaPolizasRenovadas.get(a).getCoceMtSumaAsegurada());

				output.write(sb.toString());
				output.newLine();
				a=a+1;
			}

			output.close (); 
		    log.info ( "El archivo se ha creado correctamente") ; 
		    this.message.append("Archivo creado correctamente");
		    
		    HttpServletResponse response = FacesUtils.getServletResponse();
		    InputStream in = new FileInputStream (file);		    
		    OutputStream out = response.getOutputStream ();
		       
		    response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename=\"RENOVACION_POLIZAS_" + fec +  ".csv" + "\"");
			

			int read = 0;
			byte[] bytes = new byte[2048];
			
			while((read = in.read(bytes)) != -1){
				out.write(bytes,0,read);
			}	        

			in.close();
		
			out.flush();
			out.close();
			
			file.delete();
			
			this.listaPolizasRenovadas.clear();
			
			this.log.debug(this.message.toString());
	        this.respuesta = this.message.toString();
	        Utilerias.resetMessage(message);
			
			FacesContext.getCurrentInstance().responseComplete();
			
	        return null;
		} catch (Exception e) {
			this.message.append("Error al traer informacion de renovacion.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}

}