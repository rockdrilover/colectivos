package mx.com.santander.aseguradora.colectivos.view.bean;

public class BeanCdComponente {

	
	private Integer secuencia;
	private String componente;
	
	public Integer getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}
	
	public String getComponente() {
		return componente;
	}
	public void setComponente(String componente) {
		this.componente = componente;
	}
	
	public BeanCdComponente(Integer sec, String componente) {
		this.secuencia	=sec;
		this.componente =componente;
	}
}
