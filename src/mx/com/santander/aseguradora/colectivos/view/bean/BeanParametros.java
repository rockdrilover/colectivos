/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.Date;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanParametros {
	
	@Resource
	private ServicioParametros servicioParametros;
	private Log log = LogFactory.getLog(this.getClass());
	private Long copaCdParametro;
	private String copaDesParametro;
	private String copaIdParametro;
	private String copaVvalor1;
	private String copaVvalor2;
	private String copaVvalor3;
	private String copaVvalor4;
	private String copaVvalor5;
	private String copaVvalor6;
	private String copaVvalor7;
	private String copaVvalor8;
	private Integer copaNvalor1;
	private Long copaNvalor2;
	private Long copaNvalor3;
	private Integer copaNvalor4;
	private Integer copaNvalor5;
	private Double copaNvalor6;
	private Double copaNvalor7;
	private Double copaNvalor8;
	private Date copaFvalor1;
	private Date copaFvalor2;
	private Date copaFvalor3;
	private Date copaFvalor4;
	private Date copaFvalor5;
	private String copaRegistro;
	private String auxDato;
	//------ Para Act y Generar  Rutas
	private String copaRegistro1;
	private String copaRegistro2;
	private String copaRegistro3;
	private String usuario1;
	private String usuario2;
	private String usuario3;
	private Long   copaCdParametro1;
	private Long   copaCdParametro2;
	private Long   copaCdParametro3;
    private String copaIdParametro1;
    private String copaIdParametro2; 
	private String copaIdParametro3;
	private String copaVvalor31;
	private String copaVvalor32;
	private String copaVvalor33;

	
	
	
	/**
	 * @return the copaCdParametro
	 */
	public Long getCopaCdParametro() {
		return copaCdParametro;
	}
	/**
	 * @return the copaDesParametro
	 */
	public String getCopaDesParametro() {
		return copaDesParametro;
	}
	/**
	 * @return the copaIdParametro
	 */
	public String getCopaIdParametro() {
		return copaIdParametro;
	}
	/**
	 * @return the copaVvalor1
	 */
	public String getCopaVvalor1() {
		return copaVvalor1;
	}
	/**
	 * @return the copaVvalor2
	 */
	public String getCopaVvalor2() {
		return copaVvalor2;
	}
	/**
	 * @return the copaVvalor3
	 */
	public String getCopaVvalor3() {
		return copaVvalor3;
	}
	/**
	 * @return the copaVvalor4
	 */
	public String getCopaVvalor4() {
		return copaVvalor4;
	}
	/**
	 * @return the copaVvalor5
	 */
	public String getCopaVvalor5() {
		return copaVvalor5;
	}
	/**
	 * @return the copaVvalor6
	 */
	public String getCopaVvalor6() {
		return copaVvalor6;
	}
	/**
	 * @return the copaVvalor7
	 */
	public String getCopaVvalor7() {
		return copaVvalor7;
	}
	/**
	 * @return the copaVvalor8
	 */
	public String getCopaVvalor8() {
		return copaVvalor8;
	}
	/**
	 * @return the copaNvalor1
	 */
	public Integer getCopaNvalor1() {
		return copaNvalor1;
	}
	/**
	 * @return the copaNvalor2
	 */
	public Long getCopaNvalor2() {
		return copaNvalor2;
	}
	/**
	 * @return the copaNvalor3
	 */
	public Long getCopaNvalor3() {
		return copaNvalor3;
	}
	/**
	 * @return the copaNvalor4
	 */
	public Integer getCopaNvalor4() {
		return copaNvalor4;
	}
	/**
	 * @return the copaNvalor5
	 */
	public Integer getCopaNvalor5() {
		return copaNvalor5;
	}
	/**
	 * @return the copaNvalor6
	 */
	public Double getCopaNvalor6() {
		return copaNvalor6;
	}
	/**
	 * @return the copaNvalor7
	 */
	public Double getCopaNvalor7() {
		return copaNvalor7;
	}
	/**
	 * @return the copaNvalor8
	 */
	public Double getCopaNvalor8() {
		return copaNvalor8;
	}
	/**
	 * @return the copaFvalor1
	 */
	public Date getCopaFvalor1() {
		return copaFvalor1;
	}
	/**
	 * @return the copaFvalor2
	 */
	public Date getCopaFvalor2() {
		return copaFvalor2;
	}
	/**
	 * @return the copaFvalor3
	 */
	public Date getCopaFvalor3() {
		return copaFvalor3;
	}
	/**
	 * @return the copaFvalor4
	 */
	public Date getCopaFvalor4() {
		return copaFvalor4;
	}
	/**
	 * @return the copaFvalor5
	 */
	public Date getCopaFvalor5() {
		return copaFvalor5;
	}
	/**
	 * @param copaCdParametro the copaCdParametro to set
	 */
	public void setCopaCdParametro(Long copaCdParametro) {
		this.copaCdParametro = copaCdParametro;
	}
	/**
	 * @param copaDesParametro the copaDesParametro to set
	 */
	public void setCopaDesParametro(String copaDesParametro) {
		this.copaDesParametro = copaDesParametro;
	}
	/**
	 * @param copaIdParametro the copaIdParametro to set
	 */
	public void setCopaIdParametro(String copaIdParametro) {
		this.copaIdParametro = copaIdParametro;
	}
	/**
	 * @param copaVvalor1 the copaVvalor1 to set
	 */
	public void setCopaVvalor1(String copaVvalor1) {
		this.copaVvalor1 = copaVvalor1;
	}
	/**
	 * @param copaVvalor2 the copaVvalor2 to set
	 */
	public void setCopaVvalor2(String copaVvalor2) {
		this.copaVvalor2 = copaVvalor2;
	}
	/**
	 * @param copaVvalor3 the copaVvalor3 to set
	 */
	public void setCopaVvalor3(String copaVvalor3) {
		this.copaVvalor3 = copaVvalor3;
	}
	/**
	 * @param copaVvalor4 the copaVvalor4 to set
	 */
	public void setCopaVvalor4(String copaVvalor4) {
		this.copaVvalor4 = copaVvalor4;
	}
	/**
	 * @param copaVvalor5 the copaVvalor5 to set
	 */
	public void setCopaVvalor5(String copaVvalor5) {
		this.copaVvalor5 = copaVvalor5;
	}
	/**
	 * @param copaVvalor6 the copaVvalor6 to set
	 */
	public void setCopaVvalor6(String copaVvalor6) {
		this.copaVvalor6 = copaVvalor6;
	}
	/**
	 * @param copaVvalor7 the copaVvalor7 to set
	 */
	public void setCopaVvalor7(String copaVvalor7) {
		this.copaVvalor7 = copaVvalor7;
	}
	/**
	 * @param copaVvalor8 the copaVvalor8 to set
	 */
	public void setCopaVvalor8(String copaVvalor8) {
		this.copaVvalor8 = copaVvalor8;
	}
	/**
	 * @param copaNvalor1 the copaNvalor1 to set
	 */
	public void setCopaNvalor1(Integer copaNvalor1) {
		this.copaNvalor1 = copaNvalor1;
	}
	/**
	 * @param copaNvalor2 the copaNvalor2 to set
	 */
	public void setCopaNvalor2(Long copaNvalor2) {
		this.copaNvalor2 = copaNvalor2;
	}
	/**
	 * @param copaNvalor3 the copaNvalor3 to set
	 */
	public void setCopaNvalor3(Long copaNvalor3) {
		this.copaNvalor3 = copaNvalor3;
	}
	/**
	 * @param copaNvalor4 the copaNvalor4 to set
	 */
	public void setCopaNvalor4(Integer copaNvalor4) {
		this.copaNvalor4 = copaNvalor4;
	}
	/**
	 * @param copaNvalor5 the copaNvalor5 to set
	 */
	public void setCopaNvalor5(Integer copaNvalor5) {
		this.copaNvalor5 = copaNvalor5;
	}
	/**
	 * @param copaNvalor6 the copaNvalor6 to set
	 */
	public void setCopaNvalor6(Double copaNvalor6) {
		this.copaNvalor6 = copaNvalor6;
	}
	/**
	 * @param copaNvalor7 the copaNvalor7 to set
	 */
	public void setCopaNvalor7(Double copaNvalor7) {
		this.copaNvalor7 = copaNvalor7;
	}
	/**
	 * @param copaNvalor8 the copaNvalor8 to set
	 */
	public void setCopaNvalor8(Double copaNvalor8) {
		this.copaNvalor8 = copaNvalor8;
	}
	/**
	 * @param copaFvalor1 the copaFvalor1 to set
	 */
	public void setCopaFvalor1(Date copaFvalor1) {
		this.copaFvalor1 = copaFvalor1;
	}
	/**
	 * @param copaFvalor2 the copaFvalor2 to set
	 */
	public void setCopaFvalor2(Date copaFvalor2) {
		this.copaFvalor2 = copaFvalor2;
	}
	/**
	 * @param copaFvalor3 the copaFvalor3 to set
	 */
	public void setCopaFvalor3(Date copaFvalor3) {
		this.copaFvalor3 = copaFvalor3;
	}
	/**
	 * @param copaFvalor4 the copaFvalor4 to set
	 */
	public void setCopaFvalor4(Date copaFvalor4) {
		this.copaFvalor4 = copaFvalor4;
	}
	/**
	 * @param copaFvalor5 the copaFvalor5 to set
	 */
	public void setCopaFvalor5(Date copaFvalor5) {
		this.copaFvalor5 = copaFvalor5;
	}
	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	/**
	 * @return the servicioParametros
	 */
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	/**
	 * 
	 */
	public BeanParametros() {
		new StringBuilder();
		this.log.debug("BeanParametro creado");
	}

	public String getAuxDato() {
		return auxDato;
	}
	public void setAuxDato(String auxDato) {
		this.auxDato = auxDato;
	}
	public String getCopaRegistro() {
		return copaRegistro;
	}
	public void setCopaRegistro(String copaRegistro) {
		this.copaRegistro = copaRegistro;
	}
	public String getCopaRegistro1() {
		return copaRegistro1;
	}
	public void setCopaRegistro1(String copaRegistro1) {
		this.copaRegistro1 = copaRegistro1;
	}
	public String getCopaRegistro2() {
		return copaRegistro2;
	}
	public void setCopaRegistro2(String copaRegistro2) {
		this.copaRegistro2 = copaRegistro2;
	}
	public String getCopaRegistro3() {
		return copaRegistro3;
	}
	public void setCopaRegistro3(String copaRegistro3) {
		this.copaRegistro3 = copaRegistro3;
	}
	public String getUsuario1() {
		return usuario1;
	}
	public void setUsuario1(String usuario1) {
		this.usuario1 = usuario1;
	}
	public String getUsuario2() {
		return usuario2;
	}
	public void setUsuario2(String usuario2) {
		this.usuario2 = usuario2;
	}
	public String getUsuario3() {
		return usuario3;
	}
	public void setUsuario3(String usuario3) {
		this.usuario3 = usuario3;
	}
	public Long getCopaCdParametro1() {
		return copaCdParametro1;
	}
	public void setCopaCdParametro1(Long copaCdParametro1) {
		this.copaCdParametro1 = copaCdParametro1;
	}
	public Long getCopaCdParametro2() {
		return copaCdParametro2;
	}
	public void setCopaCdParametro2(Long copaCdParametro2) {
		this.copaCdParametro2 = copaCdParametro2;
	}
	public Long getCopaCdParametro3() {
		return copaCdParametro3;
	}
	public void setCopaCdParametro3(Long copaCdParametro3) {
		this.copaCdParametro3 = copaCdParametro3;
	}
	public String getCopaIdParametro1() {
		return copaIdParametro1;
	}
	public void setCopaIdParametro1(String copaIdParametro1) {
		this.copaIdParametro1 = copaIdParametro1;
	}
	public String getCopaIdParametro2() {
		return copaIdParametro2;
	}
	public void setCopaIdParametro2(String copaIdParametro2) {
		this.copaIdParametro2 = copaIdParametro2;
	}
	public String getCopaIdParametro3() {
		return copaIdParametro3;
	}
	public void setCopaIdParametro3(String copaIdParametro3) {
		this.copaIdParametro3 = copaIdParametro3;
	}
	public String getCopaVvalor31() {
		return copaVvalor31;
	}
	public void setCopaVvalor31(String copaVvalor31) {
		this.copaVvalor31 = copaVvalor31;
	}
	public String getCopaVvalor32() {
		return copaVvalor32;
	}
	public void setCopaVvalor32(String copaVvalor32) {
		this.copaVvalor32 = copaVvalor32;
	}
	public String getCopaVvalor33() {
		return copaVvalor33;
	}
	public void setCopaVvalor33(String copaVvalor33) {
		this.copaVvalor33 = copaVvalor33;
	}
	public boolean getBlnModificar() {		
		if(getCopaNvalor2() == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	
}
