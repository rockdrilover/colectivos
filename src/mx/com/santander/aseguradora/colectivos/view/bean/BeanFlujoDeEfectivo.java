package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioFlujoDeEfectivo;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

@Controller
@Scope("session")
public class BeanFlujoDeEfectivo {

	// Servicios
	@Resource
	private ServicioFlujoDeEfectivo servicioFlujo;

	// Listas
	private List<Object> listaComboCuentas;
	private List<Object> listaComboMes;
	private List<Object> listaCerti;

	// Variables
	private Integer operacion, mes;
	private String cuenta, strRespuesta, strTarea, strMes;
	private Double nEgresos, nContable, nCargo, nEmiCan;

	
	// Getters and Setters
	public ServicioFlujoDeEfectivo getServicioFlujo() {
		return servicioFlujo;
	}
	public void setServicioFlujo(ServicioFlujoDeEfectivo servicioFlujo) {
		this.servicioFlujo = servicioFlujo;
	}
	public List<Object> getListaComboCuentas() throws Exception {
		listaComboCuentas.clear();
		listaComboCuentas.addAll(consultaCuentas());
		return listaComboCuentas;
	}
	public void setListaComboCuentas(List<Object> listaComboCuentas) {
		this.listaComboCuentas = listaComboCuentas;
	}
	public Double getnEgresos() {
		return nEgresos;
	}
	public void setnEgresos(Double nEgresos) {
		this.nEgresos = nEgresos;
	}
	public Double getnContable() {
		return nContable;
	}
	public void setnContable(Double nContable) {
		this.nContable = nContable;
	}
	public Double getnCargo() {
		return nCargo;
	}
	public void setnCargo(Double nCargo) {
		this.nCargo = nCargo;
	}
	public Double getnEmiCan() {
		return nEmiCan;
	}
	public void setnEmiCan(Double nEmiCan) {
		this.nEmiCan = nEmiCan;
	}
	public List<Object> getListaComboMes() throws Exception {
		listaComboMes.clear();
		listaComboMes.addAll(consultaMes());
		return listaComboMes;
	}
	public void setListaComboMes(List<Object> listaComboMes) {
		this.listaComboMes = listaComboMes;
	}
	public List<Object> getListaCerti() {
		return listaCerti;
	}
	public void setListaCerti(List<Object> listaCerti) {
		this.listaCerti = listaCerti;
	}
	public Integer getOperacion() {
		return operacion;
	}
	public void setOperacion(Integer operacion) {
		this.operacion = operacion;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public String getStrTarea() {
		return strTarea;
	}
	public void setStrTarea(String strTarea) {
		this.strTarea = strTarea;
	}
	public String getStrMes() {
		return strMes;
	}
	public void setStrMes(String strMes) {
		this.strMes = strMes;
	}

	// Metodos
	public BeanFlujoDeEfectivo() {
		this.listaComboCuentas = new ArrayList<Object>();
		this.listaComboMes = new ArrayList<Object>();
		cuenta = "0";
		mes = 0;
		operacion = 0;
		strRespuesta = "";
		strTarea = "";
		strMes = "";

		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_FLUJODEEFECTIVO);
	}

	private List<Object> consultaCuentas() throws Exception {
		List<Object> arlDatos = null;
		List<Object> arlResultados;

		try {
			arlDatos = new ArrayList<Object>();
			arlResultados = new ArrayList<Object>();
			arlResultados.addAll(servicioFlujo.consultaCuentas());
			if (!arlResultados.isEmpty()) {
				for (Object bene : arlResultados) {
					arlDatos.add(new SelectItem(bene.toString(), bene.toString()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return arlDatos;
	}

	private List<Object> consultaMes() throws Exception {
		List<Object> arlDatos = null;

		try {
			arlDatos = new ArrayList<Object>();

			arlDatos.add(new SelectItem(1, "Enero"));
			arlDatos.add(new SelectItem(2, "Febrero"));
			arlDatos.add(new SelectItem(3, "Marzo"));
			arlDatos.add(new SelectItem(4, "Abril"));
			arlDatos.add(new SelectItem(5, "Mayo"));
			arlDatos.add(new SelectItem(6, "Junio"));
			arlDatos.add(new SelectItem(7, "Julio"));
			arlDatos.add(new SelectItem(8, "Agosto"));
			arlDatos.add(new SelectItem(9, "Septiembre"));
			arlDatos.add(new SelectItem(10, "Octubre"));
			arlDatos.add(new SelectItem(11, "Noviembre"));
			arlDatos.add(new SelectItem(12, "Diciembre"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return arlDatos;
	}
	
	public void sacarReportes(){
		String valida;
		List<Object[]> flujoEfe;
		String fInicio, fFin;
		Integer nDiasMes;
		BeanCertificado certi; 
		
		try {
			listaCerti = new ArrayList<Object>();
			nEgresos = Constantes.DEFAULT_DOUBLE;
			nContable = Constantes.DEFAULT_DOUBLE;
			nCargo = Constantes.DEFAULT_DOUBLE;
			nEmiCan = Constantes.DEFAULT_DOUBLE;
			
			strRespuesta = "";
			strMes = "";
			valida = validaDatos();

			if (valida.equals("0")){
				fInicio = "01/" + Utilerias.agregarCaracteres(mes.toString(), 1, '0', 1) + "/" + GestorFechas.formatDate(new Date(),"yyyy");
				nDiasMes = (Integer) GestorFechas.getDatosFecha(GestorFechas.generateDate(fInicio, "dd/MM/yyyy"), Constantes.DIAS_MES); 
				fFin =  nDiasMes.toString() + "/" + Utilerias.agregarCaracteres(mes.toString(), 1, '0', 1) + "/" + GestorFechas.formatDate(new Date(),"yyyy");
				strMes = (String) GestorFechas.getDatosFecha(GestorFechas.generateDate(fInicio, "dd/MM/yyyy"), Constantes.NOMBRE_MES_FECHA);
				
				flujoEfe = servicioFlujo.cargaReporteCargoAbono(fInicio, fFin, cuenta, operacion);
				for (Object[] cargar : flujoEfe) {
					certi = new BeanCertificado();
					
					certi.setCoceCampov1((cargar[0]).toString()); // Cuenta
					certi.setCoceCampov2(((BigDecimal)cargar[1]).toString()); // Ingreso
					certi.setCoceCampov3(((BigDecimal)cargar[2]).toString()); // Contable
					certi.setCoceCampov4(((BigDecimal)cargar[3]).toString()); // Prima
					certi.setCoceCampov5(((BigDecimal)cargar[4]).toString()); // EmiCan
					
					nEgresos = nEgresos + ((BigDecimal) cargar[1]).doubleValue();
					nContable = nContable + ((BigDecimal) cargar[2]).doubleValue();
					nCargo = nCargo + ((BigDecimal) cargar[3]).doubleValue();
					nEmiCan = nEmiCan + ((BigDecimal) cargar[4]).doubleValue();
					
					listaCerti.add(certi);
				}
				
				if(operacion == 1){
					strTarea = "Cargos - " + strMes;
				} else {
					strTarea = "Abonos - " + strMes;
				}
				
				if(listaCerti.isEmpty()) {
					listaCerti = null;
				}
			} else {
				strRespuesta = valida;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String validaDatos() {
		try {
			if(operacion == 0){return "Favor de seleccionar una operación";}
			if(mes == 0){return "Favor de seleccionar un mes";}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Constantes.DEFAULT_STRING_ID;
	}
	
	
	public void reporteCargoAbono(){
		String strNombre = null, strRutaTemp;
		Archivo objArchivo;
		String[] arrNomColumnas;
		Object[] arrDatos;
		Iterator<Object> itDatos;
		ArrayList<Object> lstDatos;
		
		try {
			arrNomColumnas = encabezados();
			lstDatos = new ArrayList<Object>();
			if(!listaCerti.isEmpty()){
				itDatos = listaCerti.iterator();
				while(itDatos.hasNext()) {
					arrDatos = crearArreglaDatos((BeanCertificado) itDatos.next(), arrNomColumnas.length);
					lstDatos.add(arrDatos);
				}
				
				arrDatos = new Object[arrNomColumnas.length];
				arrDatos[0] = "Totales";
				arrDatos[1] = nEgresos;
				arrDatos[2] = nContable;
				arrDatos[3] = nCargo;
				arrDatos[4] = nEmiCan;
				lstDatos.add(arrDatos);
			
				strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
				strNombre = "Flujo_De_Efectivo - " + new SimpleDateFormat("ddMMyyyy").format(new Date());
				
				objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
				objArchivo.copiarArchivo(arrNomColumnas, lstDatos, "|");
				objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
				objArchivo.eliminarArchivo();
			} else {
				strRespuesta = "No se tienen datos para generar el reporte";
			}
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Object[] crearArreglaDatos(BeanCertificado objCerti, int nNum) {
		Object[] arrResultado = new Object[nNum];
		
		arrResultado[0] = objCerti.getCoceCampov1();
		arrResultado[1] = objCerti.getCoceCampov2();
		arrResultado[2] = objCerti.getCoceCampov3();
		arrResultado[3] = objCerti.getCoceCampov4();
		arrResultado[4] = objCerti.getCoceCampov5();
		
		return arrResultado;
	}
	public String[] encabezados(){
		String[] arrNomColumnas;
		String columnas;
		
		if(operacion == 1){
			columnas = "CUENTA,INGRESOS,REGISTRADO CONTABLEMENTE,PRIMA EN DEPOSITO,EMISION Y CANCELACION MISMO MES";
			arrNomColumnas = columnas.split(",");
		} else if(operacion == 2){
			columnas = "CUENTA,EGRESOS,REGISTRADO CONTABLEMENTE,CARGOS EN ACLARACION,EMISION Y CANCELACION MISMO MES";
			arrNomColumnas = columnas.split(",");
		} else {
			columnas = "";
			arrNomColumnas = columnas.split(",");
		}
		
		return arrNomColumnas;
	}
}