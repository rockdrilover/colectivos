package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClienteFui;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClienteFuiId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAlta;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioAltaContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCartClienteFui;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCiudad;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.dto.ContratanteDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author MGE
 * Modificado: IBB.
 */

@Controller
@Scope("session")
public class BeanAltaContratanteR57  {
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message = new StringBuilder();
	
	@Resource
	private ServicioCiudad	servicioCiudad;
	
	@Resource
	private ServicioEstado servicioEstado;
	@Resource
	private ServicioCartClienteFui servicioCartClienteFui;
	@Resource
	private ServicioAlta servicioAlta;
	@Resource
	private ServicioParametros servicioParametros;
	@Resource
	private ServicioPlan servicioPlan;
	@Resource
	private ServicioCertificado servicioCertificado;
	@Resource
	private ServicioColectivosComponentes servicioColectivosComponentes;
	@Resource
	private ServicioClienteCertif servicioClienteCertif; // colectivos clietes certif al dar de alta    
    @Resource
   	private ServicioColectivosCoberturas ServicioColectivosCoberturas;   
	@Resource
	private ServicioAltaContratante servicioAltaContratante;
    @Resource
	private ServicioProducto servicioProducto;
    @Resource
	private ServicioProcesos servicioProcesos;
    
    private String strRespuesta;
	private Short   inRamo;
	private Integer nProducto;
	private Integer nPlan;
	private String strCdAnalista;
    private String strAnalista;
    private Integer nCdSucursal;
    private String strSucursal;
    private String strSumaAseg = Constantes.DEFAULT_STRING_ID;
    private String strPrima = Constantes.DEFAULT_STRING_ID;
    private String strCveCliente = Constantes.DEFAULT_STRING_ID + Constantes.DEFAULT_STRING_ID; 
	private ContratanteDTO contratante;
	private String strDescEstadoCont = "";
	private String strDescEstadoFiscal = "";
	private String strDescEstadoEnvio = "";
	private Integer nTipoConsCP;
	private String viewButton="this.disabled=false";
	private List<Object> listaProductos;
	private List<Object> listaComboPlanes;
	private List<Object> listaCmbCiudades;
	private List<Object> listaCmbColonia;
	private List<Object> listaCmbCiudadesEnvio;
	private List<Object> listaCmbColoniaEnvio;
	private List<Object> listaCmbCiudadesFiscal;
	private List<Object> listaCmbColoniaFiscal;
	
	
	public BeanAltaContratanteR57 () {
		this.listaProductos 		= new ArrayList<Object>(); 
		this.listaComboPlanes       = new ArrayList<Object>();
		this.listaCmbCiudades		= new ArrayList<Object>();
		this.listaCmbColonia 		= new ArrayList<Object>();
		this.listaCmbCiudadesEnvio	= new ArrayList<Object>();
		this.listaCmbColoniaEnvio	= new ArrayList<Object>();
		this.listaCmbCiudadesFiscal	= new ArrayList<Object>();
		this.listaCmbColoniaFiscal	= new ArrayList<Object>();
		contratante = new ContratanteDTO();	
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ALTA_CONTRATANTE_R57);
	}
	
    public String cargaProductos() {
		StringBuilder filtro;
		List<Producto> lista;
		
		try {
			getListaProductos().clear();
			filtro = new StringBuilder(100);
			filtro.append(" and P.id.alprCdRamo = ").append(this.getInRamo().intValue()).append("\n");
			lista = this.servicioProducto.obtenerObjetos(filtro.toString());
			
			for(Producto producto: lista){
				getListaProductos().add(new SelectItem(producto.getId().getAlprCdProducto().toString(),  producto.getAlprDeProducto()));
			}
			
			setnProducto(Constantes.DEFAULT_INT);
			setnPlan(Constantes.DEFAULT_INT);
			setStrSumaAseg(Constantes.DEFAULT_STRING_ID);
			setStrPrima(Constantes.DEFAULT_STRING_ID);
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			this.log.error("No se pueden cargar los productos.", e);
			e.printStackTrace();
			setStrRespuesta("Error al buscar productos");
			return NavigationResults.FAILURE;
		}
	}
		
	public String consultaPlanes() {
		StringBuilder filtro;
		List<Plan> listaPlanes;
		
		try {
			getListaComboPlanes().clear();
			filtro = new StringBuilder();
			filtro.append(" and P.id.alplCdRamo = " + getInRamo().intValue() + "\n");
			filtro.append(" and P.id.alplCdProducto = " + getnProducto() + "\n");
			listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
			
			for (Plan plan : listaPlanes) {
				getListaComboPlanes().add(new SelectItem(plan.getId().getAlplCdPlan().toString(), plan.getAlplDePlan()));
			}
			
			setnPlan(Constantes.DEFAULT_INT);
			setStrSumaAseg(Constantes.DEFAULT_STRING_ID);
			setStrPrima(Constantes.DEFAULT_STRING_ID);
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar los planes.", e);
			setStrRespuesta("Error al buscar planes");
			return NavigationResults.FAILURE;
		}
	}

	public String getAnalista() {
		
		try {
			if(getStrCdAnalista().equals(Constantes.DEFAULT_STRING)) {
				setStrAnalista("FUNCIONARIO INCORRECTO");
			} else {
				setStrAnalista(this.servicioAltaContratante.getAnalista(getStrCdAnalista()));
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar el funcionario", e);
			setStrRespuesta("Error al buscar funcionario");
			return NavigationResults.FAILURE;
		}
	}
    
	public String getSucursal() {
		
		try {
			if(getnCdSucursal() == null || getnCdSucursal() == Constantes.DEFAULT_INT) {
				setStrSucursal("SUCURSAL INCORRECTA");
			} else {
				setStrSucursal(this.servicioAltaContratante.getSucursal(getnCdSucursal()));
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar la sucursal", e);
			setStrRespuesta("Error al buscar Sucursal");
			return NavigationResults.FAILURE;
		}
	}
    
	public String getSumaAsegurada() {
		Object[] arrResultado;
		
		try {
			arrResultado = this.servicioAltaContratante.getDatosPoliza(getInRamo(), getnProducto(), getnPlan());
		
			if(arrResultado != null) {
				if(((BigDecimal)arrResultado[0]).intValue() == 0) {
					setStrSumaAseg("SIN SUMA ASEGURADA");
					setStrPrima("SIN PRIMA");
				} else {
					setStrSumaAseg(arrResultado[0].toString());
					setStrPrima(arrResultado[1].toString());
				}
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar la sucursal", e);
			setStrRespuesta("Error al buscar suma asegurada");
			return NavigationResults.FAILURE;
		}
	}
    
	public String consultaCodigoPostal() {
		ArrayList<Object> arlResultados;
		String strEstado, strColonia, strPoblacion, strCp = Constantes.DEFAULT_STRING_ID;
		
		try {
			arlResultados = new ArrayList<Object>();
			
			switch (getnTipoConsCP()) {
				case 0:
					strCp = getContratante().getCacnZnPostalHabitacion();
					break;
				case 1:
					strCp = getContratante().getStrCPEnvio();
					break;
				case 2:
					strCp = getContratante().getCacnZnPostalCobro();
					break;
			}
			
			arlResultados.addAll(this.servicioAltaContratante.getDatosCP(strCp));
			if(arlResultados.get(0).toString().equals(Constantes.DEFAULT_STRING_ID)) { //No hay datos con el CP.
				strEstado = "0 - SIN ESTADO";
				strColonia = "0 - SIN ESTADO";
				strPoblacion = "0 - SIN ESTADO";
				switch (getnTipoConsCP()) {
					case 0: //Datos Contratante
						getListaCmbColonia().clear();
						getListaCmbCiudades().clear();
						setStrDescEstadoCont(strEstado);
						getContratante().setCacnCaesCdEstadoHab(new BigDecimal(strEstado.split("-")[0].toString().trim()));
						getContratante().setCacnDiCobro2(strColonia);
						getContratante().setCacnDatosReg1(strPoblacion);
						break;
					case 1://Datos Envio
						getListaCmbColoniaEnvio().clear();
						getListaCmbCiudadesEnvio().clear();
						setStrDescEstadoEnvio(strEstado);
						getContratante().setnEstadoEnvio(new Integer(strEstado.split("-")[0].toString().trim()));
						getContratante().setStrColoniaEnvio(strColonia);
						getContratante().setStrPoblacionEnvio(strPoblacion);
						break;
					case 2://Datos Fiscal
						getListaCmbColoniaFiscal().clear();
						getListaCmbCiudadesFiscal().clear();
						setStrDescEstadoFiscal(strEstado);
						getContratante().setCacnCaesCdEstadoCob(new BigDecimal(strEstado.split("-")[0].toString().trim()));
						getContratante().setCacnCazpColoniaCob(strColonia);
						getContratante().setCacnCazpPoblacCob(strPoblacion);
						break;
				}
			} else {
				strEstado = arlResultados.get(2).toString();
				switch (getnTipoConsCP()) {
					case 0: //Datos Contratante
						getListaCmbColonia().clear();
						getListaCmbColonia().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudades().clear();
						getListaCmbCiudades().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoCont(strEstado);
						getContratante().setCacnCaesCdEstadoHab(new BigDecimal(strEstado.split("-")[0].toString().trim()));
						break;
					case 1://Datos Envio
						getListaCmbColoniaEnvio().clear();
						getListaCmbColoniaEnvio().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesEnvio().clear();
						getListaCmbCiudadesEnvio().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoEnvio(strEstado);
						getContratante().setnEstadoEnvio(new Integer(strEstado.split("-")[0].toString().trim()));
						break;
					case 2://Datos Fiscal
						getListaCmbColoniaFiscal().clear();
						getListaCmbColoniaFiscal().addAll((Collection<? extends Object>) arlResultados.get(0));
						getListaCmbCiudadesFiscal().clear();
						getListaCmbCiudadesFiscal().addAll((Collection<? extends Object>) arlResultados.get(1));
						setStrDescEstadoFiscal(strEstado);
						getContratante().setCacnCaesCdEstadoCob(new BigDecimal(strEstado.split("-")[0].toString().trim()));
						break;
				}
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar los datos del CP", e);
			setStrRespuesta("Error al buscar datos del CP");
			return NavigationResults.FAILURE;
		}
	}
	
	public String alta() { 
		String strValidaDatos;
		String formaPago="";
	    Integer pago=null;
		Integer nExisNF = 0;
		Integer cedulaRif = 0;
		Map<String, Object> resultadoCliente = null;
		Map<String, Object> resultadoCliente2 = null;
		boolean proceso=false;
		Long numeroPoliza=null;
	    Date date = new Date();

		try {
			this.log.debug("Iniciando proceso de alta cliente ");
			strValidaDatos = validarDatos();
			if(strValidaDatos.equals(Constantes.DEFAULT_STRING_ID)) {
				nExisNF = this.servicioProcesos.buscaNegative(getContratante().getCacnApellidoPat(), getContratante().getCacnApellidoMat(), getContratante().getCacnNombre(), getContratante().getCacnFeNacimiento());
				if(nExisNF == 0){
					getContratante().setCacnNuCedulaRif(cedulaRif.toString());
					getContratante().setCacnCdVida("V");
					getContratante().setCacnCdNacionalidad("C");
					getContratante().setCacnCaaeCdActividad(9998);
					getContratante().setCacnRegistro2(getStrCveCliente());
					resultadoCliente = this.servicioProcesos.buscaCliente(getContratante());
					if(resultadoCliente.get("W_mensaje") == null) {
						getContratante().setCacnCdNacionalidad("A");
						resultadoCliente2 = this.servicioProcesos.buscaCliente(getContratante());
						if(resultadoCliente2.get("W_mensaje") == null) {
							proceso= this.servicioAltaContratante.actualizaCliente(getContratante(), resultadoCliente);
							if(proceso){
								numeroPoliza = this.servicioAlta.iniciaAlta(Constantes.CD_SUCURSAL, getInRamo(), getStrCdAnalista(), getnCdSucursal(), resultadoCliente.get("W_nac").toString(), resultadoCliente.get("W_cedula").toString(), date);
								if(numeroPoliza != null || numeroPoliza !=0){
									this.servicioAltaContratante.actualizaCartCertificados(Constantes.CD_SUCURSAL, getInRamo(), numeroPoliza, resultadoCliente2);
									proceso=this.insertaCartClienteFui(getContratante(), resultadoCliente, numeroPoliza);
								}else {
									strValidaDatos = "Se guardo contratante, pero fallo al generar el n�mero de P�liza";
									proceso=false;
								}
							}
							if(proceso){
								pago=this.servicioAltaContratante.obtenerFormaPago(getInRamo(), getnProducto(), getnPlan());
								formaPago=this.TipoFormaPago(pago);
								proceso= this.servicioAltaContratante.actualizaCertificados(getnProducto(),getnPlan(),getStrSumaAseg(),getStrPrima(),Constantes.CD_SUCURSAL, getInRamo(),numeroPoliza, Constantes.DEFAULT_INT,formaPago);
								strValidaDatos = "Se ha generado el n�mero de P�liza: " + numeroPoliza;
							}
						} else {
							strValidaDatos = "Error al insertar datos del Asegurado.";
						}
					} else {
						strValidaDatos = "Error al insertar datos del Contratante.";
					}
				} else {
					strValidaDatos = "El contratante esta en la base Negative File";
				}
				setStrRespuesta(strValidaDatos);
			} else {					
				setStrRespuesta(strValidaDatos);		    	
			}
			
//			this.viewButton="this.disabled=false";
			this.viewButton="this.disabled=true";
			return NavigationResults.SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se registro correctamete el  registro.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			return NavigationResults.FAILURE;
		}	
	}
	
	private boolean insertaCartClienteFui(ContratanteDTO contratante, Map<String, Object> resultadoPoliza, Long numeroPoliza) throws Exception{
		CartClienteFui cartClienteFui;
		List<CartClienteFui> listaClienteFui = new  ArrayList<CartClienteFui>();
		try{
			cartClienteFui = new CartClienteFui();
			cartClienteFui.setId(new CartClienteFuiId(
			Constantes.CD_SUCURSAL,
			getInRamo(),
			Integer.valueOf(String.valueOf(numeroPoliza)),
			Integer.valueOf(String.valueOf(Constantes.DEFAULT_INT)),
			null,
			null,
			getContratante().getStrPuestoRepLegal(),
			getContratante().getStrDireccionEnvio(),
			getContratante().getStrColoniaEnvio(),
			null,
			null,
			null,
			null,
			getContratante().getStrNombreRepLegal(),
			getContratante().getStrFolioMercantil(),
			getContratante().getStrGiroMercantil(), 
			getContratante().getCacnFeNacimiento(),
			resultadoPoliza.get("W_nac").toString(),
			resultadoPoliza.get("W_cedula").toString(),
			getContratante().getnEstadoEnvio(),
			null,
			getContratante().getStrCPEnvio(),
			null,
			getContratante().getStrPoblacionEnvio(),
			null,
			null,
			getContratante().getStrNacionalidad(),
			null,
			getContratante().getStrNumeroSerie(),
			null,
			"PYME",
			null,
			null,
			null));
			listaClienteFui.add(cartClienteFui);
			this.servicioCartClienteFui.guardarObjetos(listaClienteFui);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			message.append("No se registro correctamete el  registro en cart_cliente_fui.");
			this.log.error(message.toString(), e);
			return false;
		}
		
	}
	
    private String validarDatos() throws Exception {
    	try {
			if(getInRamo()== null || getInRamo()== 0) { return "Favor de Seleccionar Ramo";}
    		if(getnProducto()== null || getnProducto()== 0) { return "Favor de Seleccionar Producto";}
    		if(getnPlan()== null || getnPlan()== 0) { return "Favor de Seleccionar  Plan";}
    		if(getContratante().getStrTpCuenta()== null ||  getContratante().getStrTpCuenta().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Seleccionar Cond. Cobro";}
    		if(getContratante().getStrCuenta()== null ||  getContratante().getStrCuenta().trim().equals("")) { return "Favor de ingresar Cuenta";}
    		if(getStrCdAnalista()== null ||  getStrCdAnalista().equals("")) { return "Favor de ingresar Funcionario";}
    		if(getStrAnalista().trim().equalsIgnoreCase("") || getStrAnalista()== null ||  getStrAnalista().equals(Constantes.VALIDA_FUNCIONARIO)) { return "Favor de ingresar n�mero Funcionario valido";}
    		if(getnCdSucursal()== null ||  getnCdSucursal().equals(Constantes.DEFAULT_INT)) { return "Favor de ingresar Sucursal";}
    		if(getStrSucursal().trim().equalsIgnoreCase("") || getStrSucursal()== null ||  getStrSucursal().equals(Constantes.VALIDA_SUCURSAL)) { return "Favor de ingresar n�mero de Sucursal valido";}
    		if(getStrSumaAseg().trim().equalsIgnoreCase("") || getStrSumaAseg() == null || getStrSumaAseg().trim().equalsIgnoreCase(Constantes.VALIDA_SUMA_ASEGURADA)){return "No se puede dar de alta la p�liza porque no hay una suma asegurada configurada";} 
    		if(getContratante().getCacnTpCliente() == null || getContratante().getCacnTpCliente().equals(Constantes.DEFAULT_STRING_ID)){return "Favor de ingresar Persona";}
    		if(getContratante().getCacnNombre()== null ||  getContratante().getCacnNombre().trim().equals("")) { return "Favor de ingresar Nombre";}
    		
    		if(getContratante().getCacnTpCliente().equals("F")){
    			if(getContratante().getCacnApellidoPat()== null ||  getContratante().getCacnApellidoPat().trim().equals("")) { return "Favor de ingresar A. Paterno";}
        		if(getContratante().getCacnApellidoMat()== null ||  getContratante().getCacnApellidoMat().trim().equals("")) { return "Favor de ingresar A. Materno";}
			}else{
				getContratante().setCacnApellidoMat(" ");
				getContratante().setCacnApellidoPat(" ");
			}
    		
    		if(getContratante().getCacnFeNacimiento()== null ) { return "Favor de ingresar Fecha Nac.";}
    		if(getContratante().getCacnCdEdoCivil()== null ||  getContratante().getCacnCdEdoCivil().equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Seleccionar Edo. Civil";}
    		if(getContratante().getCacnCdSexo()== null ||  getContratante().getCacnCdSexo() .equals(Constantes.DEFAULT_STRING_ID)) { return "Favor de Seleccionar Sexo";}
    		if(getContratante().getCacnRfc()== null ||  getContratante().getCacnRfc().trim().equals("")) { return "Favor de ingresar RFC";}
    		//if(getContratante().getCacnNmPersonaNatural()== null || getContratante().getCacnNmPersonaNatural().trim().equalsIgnoreCase("")){ return "Favor de ingresar Curp";}
    		if(getContratante().getCacnDiHabitacion1()== null ||  getContratante().getCacnDiHabitacion1().trim().equals("")) { return "Favor de ingresar Direccion";}
    		if(getContratante().getCacnNuTelefonoCobro()== null ||  getContratante().getCacnNuTelefonoCobro().trim().equals("")) { return "Favor de ingresar Tel. Domicilio";}
    		if(getContratante().getCacnNuTelefonoHabitacion()== null ||  getContratante().getCacnNuTelefonoHabitacion().equals("")) { return "Favor de ingresar  Tel. Oficina";}
    		if(getContratante().getCacnDiHabitacion2()== null ||  getContratante().getCacnDiHabitacion2().trim().equals("")) { return "Favor de ingresar Email";}
    		//if(getContratante().getStrNumeroSerie()== null ||  getContratante().getStrNumeroSerie().trim().equals("")) { return "Favor de ingresar Num. Serie";}
    		if(getContratante().getStrFolioMercantil()== null ||  getContratante().getStrFolioMercantil().trim().equals("")) { return "Favor de ingresar F. Mercantil";}
    		if(getContratante().getStrGiroMercantil()== null ||  getContratante().getStrGiroMercantil().trim().equals("")) { return "Favor de ingresar G. Mercantil";}
    		if(getContratante().getStrDireccionEnvio()== null ||  getContratante().getStrDireccionEnvio().trim().equals("")) { return "Favor de ingresar Direcci�n Envio";}
    		if(getContratante().getCacnDiCobro1() == null ||  getContratante().getCacnDiCobro1().trim().equals("")) { return "Favor de ingresar Direcci�n de Cobro";}
    		if(getContratante().getStrNombreRepLegal() == null ||  getContratante().getStrNombreRepLegal().trim().equals("")) { return "Favor de Ingresar Nombre Representante Legal";}
    		if(getContratante().getStrNacionalidad() == null ||  getContratante().getStrNacionalidad().trim().equals("")) { return "Favor de ingresar Nacionalidad Representante Legal";}
    		if(getContratante().getStrPuestoRepLegal() == null ||  getContratante().getStrPuestoRepLegal().trim().equals("")) { return "Favor de ingresar Puesto Representante Legal";}	
    		if(getContratante().getCacnZnPostalCobro() == null ||  getContratante().getCacnZnPostalCobro().trim().equals("")) { return "Favor de Ingresar C.P. Cobro";}
    		if(getContratante().getCacnCazpColoniaCob() == null ||  getContratante().getCacnCazpColoniaCob().trim().equals("0")) { return "Favor de Seleccionar Colonia Fiscal";}
    			else getContratante().setCacnCazpColoniaCob(this.reemplazar(getContratante().getCacnCazpColoniaCob()));
    		if(getContratante().getCacnCazpPoblacCob() == null ||  getContratante().getCacnCazpPoblacCob().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n Fiscal";}
    			else getContratante().setCacnCazpPoblacCob(this.reemplazar(getContratante().getCacnCazpPoblacCob()));
    		if(getStrDescEstadoFiscal()== null || getStrDescEstadoFiscal().trim().equals("")){return "Favor de colocar un CP Cobro valido";}
    		if(getContratante().getStrCPEnvio() == null ||  getContratante().getStrCPEnvio().trim().equals("")) { return "Favor de Ingresar C.P. Env�o";}
    		if(getStrDescEstadoEnvio()== null || getStrDescEstadoEnvio().trim().equals("")){return "Favor de colocar un CP Env�o valido";}
    		if(getContratante().getStrColoniaEnvio() == null ||  getContratante().getStrColoniaEnvio().trim().equals("0")) { return "Favor de Seleccionar Colonia Env�o";}
    			else getContratante().setStrColoniaEnvio(this.reemplazar(getContratante().getStrColoniaEnvio()));
    		if(getContratante().getStrPoblacionEnvio() == null ||  getContratante().getStrPoblacionEnvio().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n Env�o";}
    			else getContratante().setStrPoblacionEnvio(this.reemplazar(getContratante().getStrPoblacionEnvio()));
    		if(getContratante().getCacnZnPostalHabitacion()== null ||  getContratante().getCacnZnPostalHabitacion().trim().equals("0")) { return "Favor de ingresar C.P.";}
    		if(getStrDescEstadoCont()== null || getStrDescEstadoCont().trim().equals("")){return "Favor de colocar un CP valido";}
    		if(getContratante().getCacnDiCobro2() == null ||  getContratante().getCacnDiHabitacion1().trim().equals("0")){ return "Favor de Seleccionar Colonia";}
    			else getContratante().setCacnDiCobro2(this.reemplazar(getContratante().getCacnDiCobro2()));
    		if(getContratante().getCacnDatosReg1()== null ||  getContratante().getCacnDatosReg1().trim().equals("0")) { return "Favor de Seleccionar Poblaci�n";}   		
    			else getContratante().setCacnDatosReg1(this.reemplazar(getContratante().getCacnDatosReg1()));
    		if(getContratante().getStrTpCuenta().trim().equalsIgnoreCase(Constantes.CONDUCTO_COBRO_CHEQUES) && getContratante().getStrCuenta().length() > Constantes.LONGITUD_NUMERO_CUENTA){
    			return "EL NUMERO DE CUENTA NO PUEDE SER MAYOR A 11 CARACTERES POR SER CUENTA DE CHEQUES";
    		}
       	} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: BeanAltaContratante.validarDatos(): " + e.getMessage());
		}
    	
    	return Constantes.DEFAULT_STRING_ID;
	}
    
    public String reemplazar(String cadenaCarcter){
    	  if(cadenaCarcter != null && cadenaCarcter != ""){
    		  cadenaCarcter=cadenaCarcter.replace("$$$", " ");
    		  cadenaCarcter=cadenaCarcter.replace("$", " ");
    		  if(cadenaCarcter.length() > 20){
    			  cadenaCarcter=cadenaCarcter.substring(0, 19);
    		  }
    	  }
    	  return cadenaCarcter;
    }
    
    public String TipoFormaPago(Integer formaPago) {
    	String tipoFormaPago="";
    	switch (formaPago) {
	        case 1:
	        	tipoFormaPago="A";
	        break;
	 
	        case 2:
	        	tipoFormaPago="S";
	        break;
	        
	        case 4:
	        	tipoFormaPago="T";
		    break;
		    
	        case 12:
	        	tipoFormaPago="M";
		    break;
		    
	        default:
	        	tipoFormaPago="";
	        break;
	 
	    }
    	return tipoFormaPago;
    }
    
    public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public Short getInRamo() {
		return inRamo;
	}
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}
	public List<Object> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<Object> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public List<Object> getListaComboPlanes() {
		return listaComboPlanes;
	}
	public void setListaComboPlanes(List<Object> listaComboPlanes) {
		this.listaComboPlanes = listaComboPlanes;
	}
	public Integer getnProducto() {
		return nProducto;
	}
	public void setnProducto(Integer nProducto) {
		this.nProducto = nProducto;
	}
	public Integer getnPlan() {
		return nPlan;
	}
	public void setnPlan(Integer nPlan) {
		this.nPlan = nPlan;
	}
	public String getStrCdAnalista() {
		return strCdAnalista;
	}
	public void setStrCdAnalista(String strCdAnalista) {
		this.strCdAnalista = strCdAnalista;
	}
	public String getStrAnalista() {
		return strAnalista;
	}
	public void setStrAnalista(String strAnalista) {
		this.strAnalista = strAnalista;
	}
	public Integer getnCdSucursal() {
		return nCdSucursal;
	}
	public void setnCdSucursal(Integer nCdSucursal) {
		this.nCdSucursal = nCdSucursal;
	}
	public String getStrSucursal() {
		return strSucursal;
	}
	public void setStrSucursal(String strSucursal) {
		this.strSucursal = strSucursal;
	}
	public String getStrSumaAseg() {
		return strSumaAseg;
	}
	public void setStrSumaAseg(String strSumaAseg) {
		this.strSumaAseg = strSumaAseg;
	}
	public String getStrPrima() {
		return strPrima;
	}
	public void setStrPrima(String strPrima) {
		this.strPrima = strPrima;
	}
	public ContratanteDTO getContratante() {
		return contratante;
	}
	public void setContratante(ContratanteDTO contratante) {
		this.contratante = contratante;
	}	
    public String getStrCveCliente() {
		return strCveCliente;
	}
	public void setStrCveCliente(String strCveCliente) {
		this.strCveCliente = strCveCliente;
	}
	public List<Object> getListaCmbCiudades() {
		return listaCmbCiudades;
	}
	public void setListaCmbCiudades(List<Object> listaCmbCiudades) {
		this.listaCmbCiudades = listaCmbCiudades;
	}
	public List<Object> getListaCmbColonia() {
		return listaCmbColonia;
	}
	public void setListaCmbColonia(List<Object> listaCmbColonia) {
		this.listaCmbColonia = listaCmbColonia;
	}
	public List<Object> getListaCmbCiudadesEnvio() {
		return listaCmbCiudadesEnvio;
	}
	public void setListaCmbCiudadesEnvio(List<Object> listaCmbCiudadesEnvio) {
		this.listaCmbCiudadesEnvio = listaCmbCiudadesEnvio;
	}
	public List<Object> getListaCmbColoniaEnvio() {
		return listaCmbColoniaEnvio;
	}
	public void setListaCmbColoniaEnvio(List<Object> listaCmbColoniaEnvio) {
		this.listaCmbColoniaEnvio = listaCmbColoniaEnvio;
	}
	public List<Object> getListaCmbCiudadesFiscal() {
		return listaCmbCiudadesFiscal;
	}
	public void setListaCmbCiudadesFiscal(List<Object> listaCmbCiudadesFiscal) {
		this.listaCmbCiudadesFiscal = listaCmbCiudadesFiscal;
	}
	public List<Object> getListaCmbColoniaFiscal() {
		return listaCmbColoniaFiscal;
	}
	public void setListaCmbColoniaFiscal(List<Object> listaCmbColoniaFiscal) {
		this.listaCmbColoniaFiscal = listaCmbColoniaFiscal;
	}
	public String getStrDescEstadoCont() {
		return strDescEstadoCont;
	}
	public void setStrDescEstadoCont(String strDescEstadoCont) {
		this.strDescEstadoCont = strDescEstadoCont;
	}
	public String getStrDescEstadoFiscal() {
		return strDescEstadoFiscal;
	}
	public void setStrDescEstadoFiscal(String strDescEstadoFiscal) {
		this.strDescEstadoFiscal = strDescEstadoFiscal;
	}
	public String getStrDescEstadoEnvio() {
		return strDescEstadoEnvio;
	}
	public void setStrDescEstadoEnvio(String strDescEstadoEnvio) {
		this.strDescEstadoEnvio = strDescEstadoEnvio;
	}
	public Integer getnTipoConsCP() {
		return nTipoConsCP;
	}
	public void setnTipoConsCP(Integer nTipoConsCP) {
		this.nTipoConsCP = nTipoConsCP;
	}	
	public String getViewButton() {
		return viewButton;
	}
	public void setViewButton(String viewButton) {
		this.viewButton = viewButton;
	}
	public ServicioCiudad getServicioCiudad() {
		return servicioCiudad;
	}

	public void setServicioCiudad(ServicioCiudad servicioCiudad) {
		this.servicioCiudad = servicioCiudad;
	}

	public ServicioEstado getServicioEstado() {
		return servicioEstado;
	}

	public void setServicioEstado(ServicioEstado servicioEstado) {
		this.servicioEstado = servicioEstado;
	}

	public ServicioCartClienteFui getServicioCartClienteFui() {
		return servicioCartClienteFui;
	}

	public void setServicioCartClienteFui(ServicioCartClienteFui servicioCartClienteFui) {
		this.servicioCartClienteFui = servicioCartClienteFui;
	}

	public ServicioAlta getServicioAlta() {
		return servicioAlta;
	}

	public void setServicioAlta(ServicioAlta servicioAlta) {
		this.servicioAlta = servicioAlta;
	}

	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}

	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}

	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}

	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}

	public ServicioColectivosComponentes getServicioColectivosComponentes() {
		return servicioColectivosComponentes;
	}

	public void setServicioColectivosComponentes(ServicioColectivosComponentes servicioColectivosComponentes) {
		this.servicioColectivosComponentes = servicioColectivosComponentes;
	}

	public ServicioClienteCertif getServicioClienteCertif() {
		return servicioClienteCertif;
	}

	public void setServicioClienteCertif(ServicioClienteCertif servicioClienteCertif) {
		this.servicioClienteCertif = servicioClienteCertif;
	}

	public ServicioColectivosCoberturas getServicioColectivosCoberturas() {
		return ServicioColectivosCoberturas;
	}

	public void setServicioColectivosCoberturas(ServicioColectivosCoberturas servicioColectivosCoberturas) {
		ServicioColectivosCoberturas = servicioColectivosCoberturas;
	}

	public ServicioAltaContratante getServicioAltaContratante() {
		return servicioAltaContratante;
	}

	public void setServicioAltaContratante(ServicioAltaContratante servicioAltaContratante) {
		this.servicioAltaContratante = servicioAltaContratante;
	}

	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}

	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}

	public ServicioProcesos getServicioProcesos() {
		return servicioProcesos;
	}

	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}
}
