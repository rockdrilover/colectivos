/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTarifas;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 * @author Ing. Hery Tyler
 *
 */
@Controller
@Scope("session")
public class BeanListaTarifas {

	@Resource
	private ServicioTarifas  servicioTarifas;
	@Resource
	private ServicioProducto servicioProducto;
	
	private StringBuilder message;
	private Log log = LogFactory.getLog(this.getClass());
	
	
	private Integer canal;
	private String strProducto;
	private Integer valorProducto;
	private Short ramo;
	
	private List<Object> listaProductos;
	private List<Object> listaTarifas;
	
	
	public BeanListaTarifas() {
		this.message = new StringBuilder();
		this.listaTarifas = new ArrayList<Object>();
		this.listaProductos = new ArrayList<Object>();
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_TARIFAS);
	}
	
	public String cargaProductos(){
		this.message.append("cargando productos.");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder filtro = new StringBuilder(100);
		
		this.listaProductos.clear();
		if(this.ramo != null){
			try {
		
				filtro.append(" and P.id.alprCdRamo = ").append(this.ramo.intValue()).append("\n");
				List<Producto> lista = this.servicioProducto.obtenerObjetos(filtro.toString());
				
				for(Producto producto: lista){
					this.listaProductos.add(new SelectItem(producto.getId().getAlprCdProducto(), producto.getId().getAlprCdProducto().toString()+ " - " + producto.getAlprDeProducto()));
				 }
				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				message.append("No se pueden cargar los productos.");
				this.log.error(message.toString(), e);
				FacesUtils.addErrorMessage(message.toString());
				return NavigationResults.FAILURE;
			}
		} else{
			this.message.append("Necesita seleccionar un ramo.");
			this.log.debug(message.toString());
			FacesUtils.addErrorMessage(message.toString());
			return NavigationResults.RETRY;
		}
	}
	
	public String consultar() {
		this.message = new StringBuilder();
		Iterator<Object> it;
		Object[] objTarifa;
		BeanTarifas beant;
		
		try {
			log.info(ramo);
			listaTarifas.clear();
			List<Object> lista = this.servicioTarifas.getTarifas(1, ramo, valorProducto);	
			
			it = lista.iterator();
			while(it.hasNext()){
				objTarifa = (Object[]) it.next();
				
				beant = new BeanTarifas();
				beant.setRamo(objTarifa[0] == null ? 0 : ((BigDecimal)objTarifa[0]).shortValue());
				beant.setProducto(objTarifa[1] == null ? 0 : ((BigDecimal)objTarifa[1]).intValue());
				beant.setPlan(objTarifa[2] == null ? 0 : ((BigDecimal)objTarifa[2]).shortValue());
				beant.setDescproducto(objTarifa[3] == null ? "" : objTarifa[3].toString());
				beant.setDescplan(objTarifa[4] == null ? "" : objTarifa[4].toString());
				beant.setDescnegocio(objTarifa[5] == null ? "" : objTarifa[5].toString());
				//beant.setCobertura(objTarifa[3].toString());
				beant.setTarifa1(objTarifa[6] == null ? 0.0 : ((BigDecimal)objTarifa[6]).doubleValue());
				beant.setTarifa2(objTarifa[7] == null ? 0.0 : new Double(objTarifa[7].toString()));
				beant.setFecini((Date) objTarifa[9]);
				beant.setFecfin((Date) objTarifa[10]);
				beant.setCentro(objTarifa[8] == null ? 0 : ((BigDecimal)objTarifa[8]).intValue());
				
				beant.setLstCoberturas(consultarCoberturas(1, beant.getRamo(), beant.getProducto(), beant.getPlan()));
				beant.setLstComponentes(consultarComponentes(1, beant.getRamo(), beant.getProducto(), beant.getPlan()));
				beant.setLstProdPlanes(consultarProdPlanes(beant.getRamo(), beant.getProducto(), beant.getPlan()));
				listaTarifas.add(beant);
			}

			return NavigationResults.SUCCESS;
		}catch (Excepciones e) {
			this.message.append("No se puede recuperar las tarifas.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	private ArrayList<BeanTarifas> consultarCoberturas(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones{
		ArrayList<BeanTarifas> arlCoberturas;
		List<Object> lstResultados;
		
		Iterator<Object> it;
		Object[] arrCobertura;
		BeanTarifas objCobertura;
		
		try {
			this.message = new StringBuilder();
			arlCoberturas = new ArrayList<BeanTarifas>();
			lstResultados = this.servicioTarifas.getCoberturas(canal, ramo, producto, plan);	
			
			it = lstResultados.iterator();
			while(it.hasNext()){
				arrCobertura = (Object[]) it.next();
				
				objCobertura = new BeanTarifas(arrCobertura, 1);
				arlCoberturas.add(objCobertura);
			}
		}catch (Excepciones e) {
			this.message.append("No se pueden recuperar los componentes.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		
		return arlCoberturas;
	}
	

	private ArrayList<BeanTarifas> consultarComponentes(Integer canal, Short ramo, Integer producto, Short plan) throws Excepciones{
		ArrayList<BeanTarifas> arlComponentes;
		List<Object> lstResultados;
		
		Iterator<Object> it;
		Object[] arrComponentes;
		BeanTarifas objComponentes;
		
		try {
			this.message = new StringBuilder();
			arlComponentes = new ArrayList<BeanTarifas>();
			lstResultados = this.servicioTarifas.getComponentes(canal, ramo, producto, plan);	
			
			it = lstResultados.iterator();
			while(it.hasNext()){
				arrComponentes = (Object[]) it.next();
				
				objComponentes = new BeanTarifas(arrComponentes, 2);
				arlComponentes.add(objComponentes);
			}
		}catch (Excepciones e) {
			this.message.append("No se pueden recuperar las coberturas.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		
		return arlComponentes;
	}
	
	private ArrayList<BeanTarifas> consultarProdPlanes(Short ramo, Integer producto, Short plan) throws Excepciones{
		ArrayList<BeanTarifas> arlProdPlanes;
		List<Object> lstResultados;
		
		Iterator<Object> it;
		Object[] arrProdPlanes;
		BeanTarifas objProdPlanes;
		
		try {
			this.message = new StringBuilder();
			arlProdPlanes = new ArrayList<BeanTarifas>();
			lstResultados = this.servicioTarifas.getProdPlanes(ramo, producto, plan);	
			
			it = lstResultados.iterator();
			while(it.hasNext()){
				arrProdPlanes = (Object[]) it.next();
				
				objProdPlanes = new BeanTarifas(arrProdPlanes, 3);
				arlProdPlanes.add(objProdPlanes);
			}
		}catch (Excepciones e) {
			this.message.append("No se pueden recuperar los productos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		
		return arlProdPlanes;
	}
	
	public String generaReporte() throws Exception{
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		
		
		if(this.ramo != null){
			
			try {
				
				StringBuffer nombreSalida = new StringBuffer();
				
				inParams.put("RAMO", this.ramo);
				
				nombreSalida.append("Tarifas ramo " + this.ramo + ".xls");
				
				rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/" + nombreSalida);
				rutaReporte = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/DetalleTarifa.jrxml");
				
				this.servicioTarifas.reporteDetalleTarifas(rutaTemporal, rutaReporte, inParams);
				
				File file = new File(rutaTemporal);
				
				FacesContext context = FacesContext.getCurrentInstance();
				
				if(!context.getResponseComplete()){
				
					input = new FileInputStream(file);
					bytes = new byte[1000];
					
					
					String contentType = "application/vnd.ms-excel";

					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					
					response.setContentType(contentType);
					
					DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
			    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());
			    	
					ServletOutputStream out = response.getOutputStream();
					
					while((read = input.read(bytes))!= -1){
						
						out.write(bytes, 0 , read);
					}
					
					input.close();
					out.flush();
					out.close();
					file.delete();
					
					context.responseComplete();
					
				}
				
				Utilerias.resetMessage(message);
				
				this.message.append("Reporte generado.");
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				// TODO: handle exception
				this.message.append("Error al generar el reporte de tarifas.");
				this.log.error(message.toString(), e);
				return NavigationResults.FAILURE;
			}			

		}
		else
		{
			this.message.append("Seleccione un ramo.");
			this.log.debug(message.toString());
			return NavigationResults.RETRY;
		}			
	}
	
	public void setServicioTarifas(ServicioTarifas servicioTarifas) {
		this.servicioTarifas = servicioTarifas;
	}
	public ServicioTarifas getServicioTarifas() {
		return servicioTarifas;
	}
	
	public Integer getCanal() {
		return canal;
	}
	public void setCanal(Integer canal) {
		this.canal = canal;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	public List<Object> getListaTarifas() {
		return listaTarifas;
	}
	public void setListaTarifas(List<Object> listaTarifas) {
		this.listaTarifas = listaTarifas;
	}
	public Integer getValorProducto() {
		return valorProducto;
	}
	public void setValorProducto(Integer valorProducto) {
		this.valorProducto = valorProducto;
	}
	public List<Object> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<Object> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}
	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}
	public String getStrProducto() {
		return strProducto;
	}
	public void setStrProducto(String strProducto) {
		this.strProducto = strProducto;
	}
	
	
}

