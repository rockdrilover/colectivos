/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

/**
 * @author Sergio Plata
 *
 */
public class NavigationResults {

	public static final String SUCCESS     = "success";
	public static final String FAILURE 	   = "failure";
	public static final String RETRY   	   = "retry";
	public static final String HOME	       = "home";
	public static final String SALIR	   = "salir";
	public static final String LISTA_PRODUCTOS = "lista_Productos";
	
}
