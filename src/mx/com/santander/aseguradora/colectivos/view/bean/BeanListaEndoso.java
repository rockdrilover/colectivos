package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.santander.aseguradora.colectivos.controller.BeanListaEndoso2;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosRecibo;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Endosos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndoso;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;

/**
 * @author Fernando Gomez
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		17-02-2023
 * Description: Se hizo refactorizacion de esta clase para vulnerabilidades 
 * 				de Sonar
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanListaEndoso extends BeanListaEndoso2 implements Serializable {
	
	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	//propiedad estatica para canal
	private static final Short INCANAL = 1;
	//recurso para acceder al servicio
	@Resource
	private transient ServicioProcesos servicioProcesos;
	//recurso para acceder al servicio
	@Resource
	private transient ServicioRecibo servicioRecibo;
	//recurso para acceder al servicio
	@Resource
	private transient ServicioEndoso servicioEndoso;
	//Para escribir en log
	private static final Log LOG = LogFactory.getLog(BeanListaEndoso2.class);
	
	private Integer inOperEndoso;
	private StringBuilder message;

	/**
	 * Constructor de clase
	 */
	public BeanListaEndoso() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicializan variables
		this.message = new StringBuilder();
	}
	
	/**
	 * Metodo para mandar endosar una poliza
	 * @return mensaje del proceso
	 */
	public String endosarPoliza(){
		
		LOG.debug("Iniciando proceso de carga");
		
		String strForward = null;		
		
		try {
			
				Utilerias.resetMessage(message);
				
				if (beanGen.getIdVenta() == null) {
					beanGen.setIdVenta(Constantes.DEFAULT_INT);
				}
				
				try{
					   
					   message.append("Iniciando proceso de endoso....");
					
					   if (inOperEndoso == 200){
						   String salida = this.servicioProcesos.endosarPoliza(INCANAL, beanGen.getRamo(), Long.valueOf(beanGen.getPoliza()), beanGen.getIdVenta()
                                   , null,beanGen.getFeDesde(), beanGen.getFeHasta(), inOperEndoso);
						   
						   message.append(salida);
						   
					   }else{
						   for (Object o: listaRecibos){
							   BeanReciboCol recibo = (BeanReciboCol) o;
							   recibo.setMensajeEndoso("");
							   if (recibo.isEndosar()){
								   recibo.setMensajeEndoso(this.servicioProcesos.endosarPoliza(INCANAL, beanGen.getRamo(), Long.valueOf(beanGen.getPoliza()), beanGen.getIdVenta()
										                                                     , recibo.getId().getcoreNuRecibo(), beanGen.getFeDesde(), beanGen.getFeHasta(), inOperEndoso));
							       if (recibo.getMensajeEndoso() == null || recibo.getMensajeEndoso().trim().length() == 0)
							    	   recibo.setMensajeEndoso("Endosado");
							   }
						   }
					   }
					
					   message.append("Obteniendo endosos....");
						
					   this.consultaEndoso();
					   
					   LOG.debug(message.toString());
					   
						
				
				}catch (DataAccessException e) {
						this.message.append("Error al leer los datos del archivo.");
						LOG.error(message.toString(), e);
						beanGen.setStrRespuesta(this.message.toString());
						Utilerias.resetMessage(message);
						strForward =  NavigationResults.FAILURE;
				}catch (Exception e){
					this.message.append("El proceso de carga no concluy� correctamete. Revise el archivo");					
					LOG.error(message.toString() + " Error interno.", e);
					beanGen.setStrRespuesta(this.message.toString());
					Utilerias.resetMessage(message);
					strForward =  NavigationResults.FAILURE;
				}
				
			
		}catch (Exception e) {
				this.message.append("El proceso de emisi�n no concluy� correctamete.");					
				LOG.error(message.toString() + " Error interno.", e);
				beanGen.setStrRespuesta(this.message.toString());
				Utilerias.resetMessage(message);					
				strForward =  NavigationResults.FAILURE;
		}
			
		return strForward;
		
	}
	
	/**
	 * Metodo para consultar recibos
	 */
	public void consultarRecibo(){
	
		StringBuffer filtro;
		this.listaRecibos.clear();
		
		try{
			filtro = new StringBuffer();
			
			filtro.append(" and R.coreStRecibo in (1,4) ");
			
			filtro.append(" and R.id.coreCasuCdSucursal = 1 \n");
			
			if(beanGen.getRamo() != null && beanGen.getRamo().intValue() > 0){
				filtro.append(" and R.coreCarpCdRamo = ").append(beanGen.getRamo()).append(" \n");
			}
			
			if(beanGen.getPoliza() != null && beanGen.getPoliza() > 0){
				filtro.append(" and R.coreCapoNuPoliza = ").append(beanGen.getPoliza()).append(" \n");
			}
			
			filtro.append(" and R.coreFeEmision >= sysdate - 90 \n");
			
			filtro.append("order by R.coreNuConsecutivoCuota desc \n");
			
			List<ColectivosRecibo> lista = this.servicioRecibo.obtenerObjetos1(filtro.toString());
		
			for(ColectivosRecibo rec: lista){
				BeanReciboCol bean = (BeanReciboCol) ConstruirObjeto.crearBean(BeanReciboCol.class, rec);
				bean.setRecibo(new ColectivosRecibo());
				bean.setReciboAnterior(rec.getcoreNuReciboAnterior());
				bean.getRecibo().setcoreNuConsecutivoCuota(rec.getcoreNuConsecutivoCuota());
				bean.setServicioRecibo(this.servicioRecibo);
				
				if (beanGen.getPoliza() == 0) {
					bean.setPrimaUnica(new Short("1"));
				} else {
					bean.setPrimaUnica(new Short("0"));
				}
				
				this.listaRecibos.add(bean);
			}
			
		}catch (Exception e){
			LOG.error("Error al consular recibos: ",e);
		}
	}
	
	/**
	 * Metodo para consultar endosos
	 */
	public void consultaEndoso(){
		
		StringBuffer filtro;
		this.listaEndosos.clear();
		List<Object> listConsulta;
		
		try{
			filtro = new StringBuffer();
			
			filtro.append(" and e.id.coedCasuCdSucursal = 1 \n");
			filtro.append(" and e.id.coedNuCertificado  = 0 \n");
			
			if(beanGen.getRamo() != null && beanGen.getRamo().intValue() > 0){
				filtro.append(" and e.id.coedCarpCdRamo = ").append(beanGen.getRamo()).append(" \n");
			}
			
			if(beanGen.getPoliza() != null && beanGen.getPoliza() > 0){
				filtro.append(" and e.id.coedCapoNuPoliza = ").append(beanGen.getPoliza()).append(" \n");
			}
			
			filtro.append(" and e.id.coedNuCertificado  = 0 \n");
			
			filtro.append(" and e.coedFeSolicitud >= sysdate - 1 \n");
			
			filtro.append(" and e.coedCampon1 = ").append(this.inOperEndoso).append(" \n");
			
			listConsulta = new ArrayList<Object>();
			
			listConsulta = this.servicioEndoso.obtenerObjetos(filtro.toString());
			
			for (Object oe: listConsulta){
				BeanEndoso be = new BeanEndoso();
				be.setEndoso((Endosos)oe);
				be.setIdVenta(beanGen.getIdVenta());
				
				be.setServicioEndoso(this.servicioEndoso);
				
				listaEndosos.add(be);
			}
		
		}catch (Exception e){
			LOG.error("Error al consular recibos: ",e);
		}
	}
	
	/**
	 * Metodo para generar reporte endosos
	 */
	public void reporteEndosos(){
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		String label;
		
		byte[] bytes;
		int read = 0;
		
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		try{
			label = GestorFechas.formatDate(new Date(), "dd_MM_yyyy");
			
			StringBuffer nombreSalida = new StringBuffer();
			
			inParams.put("canal", INCANAL);
			inParams.put("ramo", beanGen.getRamo().intValue());
			inParams.put("poliza", Long.valueOf(beanGen.getPoliza()));
			inParams.put("idVenta", beanGen.getIdVenta());
			inParams.put("operacion", this.inOperEndoso);
			inParams.put("fe_desde", label);
			inParams.put("fe_hasta", label);
			
			nombreSalida.append("Endosos " + beanGen.getRamo() + "_" + beanGen.getPoliza() +  " (" + label + " " + label + ").xls");
			
			rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/" + nombreSalida);
			if(beanGen.getPoliza() == 57 || beanGen.getPoliza() == 58){
				rutaReporte = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/reporteEndososDetalle5758.jrxml");
			} else {
				rutaReporte = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/reporteEndososDetalle.jrxml");
			}
			
			this.servicioEndoso.reporteEndosos(rutaTemporal, rutaReporte, inParams, Constantes.OPC_FORMATO_REPORTE.XLS);
			
			File file = new File(rutaTemporal);
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			if(!context.getResponseComplete()){
			
				input = new FileInputStream(file);
				bytes = new byte[1000];
				
				
				String contentType = "application/vnd.ms-excel";

				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				
				response.setContentType(contentType);
				
				DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
		    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());
		    	
				ServletOutputStream out = response.getOutputStream();
				
				while((read = input.read(bytes))!= -1){
					
					out.write(bytes, 0 , read);
				}
				
				input.close();
				out.flush();
				out.close();
				file.delete();
				
				context.responseComplete();
				
			}
			
			Utilerias.resetMessage(message);
			
			this.message.append("Reporte generado.");
			
		}catch(Exception e){
			LOG.error("Error en el reporte de endoso",e);
		}
	}
	
	/**
	 * @return the inOperEndoso
	 */
	public Integer getInOperEndoso() {
		return inOperEndoso;
	}

	/**
	 * @param inOperEndoso the inOperEndoso to set
	 */
	public void setInOperEndoso(Integer inOperEndoso) {
		this.inOperEndoso = inOperEndoso;
	}


	/**
	 * @param servicioProcesos the servicioProcesos to set
	 */
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}

	/**
	 * @param servicioRecibo the servicioRecibo to set
	 */
	public void setServicioRecibo(ServicioRecibo servicioRecibo) {
		this.servicioRecibo = servicioRecibo;
	}
	
	/**
	 * @param servicioEndoso the servicioEndoso to set
	 */
	public void setServicioEndoso(ServicioEndoso servicioEndoso) {
		this.servicioEndoso = servicioEndoso;
	}
	
}
