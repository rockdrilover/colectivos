/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Ramo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPoliza;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRamo;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Leobardo Preciado
 *
 */
@Controller
@Scope("session")
public class BeanListaPlanes implements InitializingBean{

	@Resource
	private ServicioPlan servicioPlan;
		
	private Log log = LogFactory.getLog(this.getClass());
	
	private List<Object> listaComboPlanes;
	private List<Object> listaPlanes;
	
	
	private StringBuilder message;
	
	public BeanListaPlanes() {
		// TODO Auto-generated constructor stub
		this.listaComboPlanes = new ArrayList<Object>();
		this.listaPlanes	  = new ArrayList<Object>();
		this.log.debug("BeanListaPlanes creada");
		this.message = new StringBuilder();
	}

	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}

	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}
	
	/**
	 * @return the listaComboRamos
	 */
	public List<Object> getListaComboPlanes() {
		return listaComboPlanes;
	}
	/**
	 * @param listaComboRamos the listaComboRamos to set
	 */
	public void setListaComboPlanes(List<Object> listaComboPlanes) {
		this.listaComboPlanes = listaComboPlanes;
	}
	/**
	 * @return the listaPlanes
	 */
	public List<Object> getListaPlanes() {
		return listaPlanes;
	}
	/**
	 * @param listaPlanes the listaPlanes to set
	 */
	public void setListaPlanes(List<Object> listaPlanes) {
		this.listaPlanes = listaPlanes;
	}



	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
		
		try {
			List<Integer> listaPlan = this.servicioPlan.getIdPlanes();
			
			
			for(Iterator<Integer> i = listaPlan.iterator(); i.hasNext();){
				Integer plan = i.next();
				this.listaComboPlanes.add(new SelectItem( plan.toString() , plan.toString()));
			}
			
			/*for(int i=0; i<listaPlan.size(); i++){
				
				this.listaPlanes.add(listaPlan);
				this.listaComboPlanes.add(new SelectItem(new Integer(listaPlan.get(i).toString()).toString(), "ABC"));
				
			}*/
			
			//listaRamos.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			this.message.append("No se pueden cargar los ramos.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		
	}
}
