/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.HomologaProductosPlanes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Plan;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Producto;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioHomologaProdPlanes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioPlan;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProducto;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

/**
 * @author Ing. Issac Bautista
 */
@Controller
@Scope("session")
public class BeanHomologacion implements InitializingBean{

	private Log log = LogFactory.getLog(this.getClass());
	
	@Resource
	private ServicioHomologaProdPlanes servicioHomologa;
	@Resource
	private ServicioProducto servicioProducto;
	@Resource
	private ServicioPlan servicioPlan;
	
	private File archivo;
	private int numPagina;
	private Short inputRamo;
	private Short inputProducto;
	private Short inputPlan;
	private String inputSubproBanco = Constantes.DEFAULT_STRING;
	private List<Object> listaComboProductos;
	private List<Object> listaComboPlanes;
	private List<Object> listaHomologa;
	private String strRespuesta;
	private HashMap<String, Object> hmResultados;
	private BeanHomologa objActual = new BeanHomologa();
	private BeanHomologa objNuevo = new BeanHomologa();
	private int filaActual;
	private Set<Integer> keys = new HashSet<Integer>();
	private Integer nTipoCarga;
	
	private HashMap<Integer, Integer> hmProducto;
	private HashMap<String, String> hmPlanes;
	
	public BeanHomologacion() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_HOMOLOGACION);
		
		this.log.debug("BeanHpmologacion creado");
		listaComboProductos = new ArrayList<Object>();
		listaComboPlanes = new ArrayList<Object>();
		this.numPagina = 1;
		
	}

	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it = listaHomologa.iterator();
        while(it.hasNext()) {
        	BeanHomologa objHomologa = (BeanHomologa) it.next();
        	if (objHomologa.getId().toString().equals(clave)) {
                objActual = objHomologa;
                break;
            }
        }
    }
	
	public void listener(UploadEvent event) throws Exception {
		UploadItem item;
		File archivoTemporal;
		String ruta;
		String nombreArchivo = null;
		String nombreRuta;
		Archivo file;
		
		try {
			item = event.getUploadItem();
			ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS);
			file = new Archivo();
			
			if(item.getData() != null){
				file.setData(item.getData());
				file.setNombreArchivo(item.getFileName());
				file.setTamano(item.getData().length);
				nombreRuta = file.getNombreArchivo() + "_" + GestorFechas.formatDate(new Date(), "ddMMyyyy");
				nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
				archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
				FileUtil.write(archivoTemporal, item.getData());
				setArchivo(archivoTemporal);
				setStrRespuesta("Archivo cargado al servidor correctamente");
			} 
		} catch (Exception e) {
			setStrRespuesta("Error al cargar Archivo al servidor.");
		}
	}
	
	public String consultarProductos(){
		String filtro = "";
		getListaComboProductos().clear();
		
		try {
			if(this.inputRamo == null) {
				this.inputRamo = (short) objNuevo.getId().getHoppCdRamo();
			}
			
			if(this.inputRamo != null && this.inputRamo.intValue() > 0) {
				filtro = " and P.id.alprCdRamo = " + inputRamo.intValue() + "\n";
				List<Producto> lstProducto = this.servicioProducto.obtenerObjetos(filtro);
				
				hmProducto = new HashMap<Integer, Integer>();
				
				for (Producto producto : lstProducto) {
					this.listaComboProductos.add(new SelectItem(producto.getId().getAlprCdProducto().toString(), producto.toString()));
					hmProducto.put(producto.getId().getAlprCdProducto(), producto.getAlprDato3()==null?0:producto.getAlprDato3());
					
				}
			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los productos.", e);
			return NavigationResults.FAILURE;
		}
	}

	public String consultaPlanes(){
		String filtro = "";
		getListaComboPlanes().clear();
		
		try {
			if(this.inputProducto == null) {
				this.inputProducto = (short) objNuevo.getId().getHoppProdColectivo();
			}
			
			if(this.inputProducto != null && this.inputProducto.intValue() > 0) {
				filtro = " and P.id.alplCdRamo = " + inputRamo.intValue() + "\n";
				filtro = filtro + " and P.id.alplCdProducto = " + inputProducto.intValue() + "\n";
				List<Plan> listaPlanes = this.servicioPlan.obtenerObjetos(filtro.toString());
				
				hmPlanes = new HashMap<String, String>();
				
				for (Plan plan : listaPlanes) {
					this.listaComboPlanes.add(new SelectItem(plan.getId().getAlplCdPlan().toString(), plan.toString()));
					hmPlanes.put(plan.getId().getAlplCdProducto()+""+plan.getId().getAlplCdPlan(), plan.getAlplDato1()==null?"0":plan.getAlplDato1());
				}
			} else {
				setStrRespuesta("Favor de seleccionar un PRODUCTO para poder realizar la consulta.");
			}
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			this.log.error("No se pueden cargar los planes.", e);
			return NavigationResults.FAILURE;
		}
	}
	
	public String consultar(){
		String filtro = "";
		List<Object> lstResultados;
		HomologaProductosPlanes homologa;
		BeanHomologa bean;
		
		try {
			if(this.inputProducto != null && this.inputProducto.intValue() > 0) {
				if(this.inputPlan == null) {
					this.inputPlan = (short) objNuevo.getId().getHoppPlanColectivo();
				}
				
				hmResultados = new HashMap<String, Object>();
				listaHomologa = new ArrayList<Object>();
				
				filtro = " and HP.id.hoppCdRamo = " + this.inputRamo.toString() + "\n";
				filtro = filtro + " and HP.id.hoppProdColectivo = " + this.inputProducto.toString() + "\n";
				if(this.inputPlan > 0) {
					filtro = filtro + " and HP.id.hoppPlanColectivo = " + this.inputPlan.toString() + "\n";
				}
				if(!this.inputSubproBanco.equals(Constantes.DEFAULT_STRING)) {
					filtro = filtro + " and HP.id.hoppPlanBancoRector = '" + this.inputSubproBanco.toString() + "' \n";
				}
				lstResultados = this.servicioHomologa.obtenerObjetos(filtro);
				
				Iterator<Object> it = lstResultados.iterator();
				while(it.hasNext()) {
					homologa = (HomologaProductosPlanes) it.next();
					bean = (BeanHomologa) ConstruirObjeto.crearBean(BeanHomologa.class, homologa);
					bean.setServicioHomologa(this.servicioHomologa);
					
					listaHomologa.add(bean);
					hmResultados.put(bean.getId().toString(), bean);
									
				}
				setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			} else {
				setStrRespuesta("Favor de seleccionar un PRODUCTO para poder realizar la consulta.");
				setHmResultados(null);
			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_HOMOLOGACION);
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.log.error("No se pueden recuperar los datos de Homologacion.", e);
			FacesUtils.addErrorMessage("No se pueden recuperar los datos de Homologacion.");
			
			return NavigationResults.FAILURE;
		}
	}
	
	public void modificar() {
		try {
			HomologaProductosPlanes homologa = (HomologaProductosPlanes) ConstruirObjeto.crearObjeto(HomologaProductosPlanes.class, objActual);
			servicioHomologa.actualizarObjeto(homologa);
			hmResultados.remove(objActual.getId().toString());
			hmResultados.put(objActual.getId().toString(), objActual);
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al MODIFICAR registro.");
			e.printStackTrace();
		}
		
	}
	
	public void guardar() {
		try {
			
			objNuevo.setHoppDato3(hmProducto.get(objNuevo.getId().getHoppProdColectivo())); //ID VENTA
			objNuevo.setHoppDato4(Long.parseLong(hmPlanes.get(objNuevo.getId().getHoppProdColectivo()+""+objNuevo.getId().getHoppPlanColectivo())));
			
			HomologaProductosPlanes homologa = (HomologaProductosPlanes) ConstruirObjeto.crearObjeto(HomologaProductosPlanes.class, objNuevo);
			
			this.servicioHomologa.guardarObjeto(homologa);
				
			setStrRespuesta("Se guardo con EXITO el registro.");
			consultar();
			objNuevo = new BeanHomologa();
		} catch (Exception e) {
			setStrRespuesta("Error al GUARDAR registro.");
			e.printStackTrace();
		}
		
	}
	
	public void guardarArchivo() {
		CsvReader csv = null;
		FileReader fr;
		String[] arrDatos;
		String strLinea;
		
		try {
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			
			csv.readRecord(); //Quitamos encabezados
			while(csv.readRecord()) {
				strLinea = csv.getRawRecord();
				arrDatos = strLinea.split("\\,");
				objNuevo = new BeanHomologa(arrDatos);
				HomologaProductosPlanes homologa = (HomologaProductosPlanes) ConstruirObjeto.crearObjeto(HomologaProductosPlanes.class, objNuevo);
				this.servicioHomologa.guardarObjeto(homologa);
			}
			
			csv.close();
			fr.close();
			
			setStrRespuesta("Se guardo con EXITO el siguiente Lay Out: " + getArchivo().getName());
			consultar();
			objNuevo = new BeanHomologa();
			getArchivo().delete();
		} catch (IOException e) {
			log.error("Error de lectura en el archivo", e);
			FacesUtils.addErrorMessage("Error de lectura en el archivo");
		} catch (Exception e) {
			log.error("No se puede cargar la informacion del archivo.", e);
			FacesUtils.addErrorMessage("No se puede cargar la informacion del archivo.");
		}
	}
	
	public ArrayList<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados().values());
        return lstValues;
    }
	
	public void afterPropertiesSet() throws Exception {
		try {
			
		} catch (Exception e) {
			this.log.error("No se pueden cargar los productos.", e);
			throw new FacesException("No se pueden cargar los productos.", e);
		}
	}

	public ServicioProducto getServicioProducto() {
		return servicioProducto;
	}
	public void setServicioProducto(ServicioProducto servicioProducto) {
		this.servicioProducto = servicioProducto;
	}
	public ServicioHomologaProdPlanes getServicioHomologa() {
		return servicioHomologa;
	}
	public void setServicioHomologa(ServicioHomologaProdPlanes servicioHomologa) {
		this.servicioHomologa = servicioHomologa;
	}
	public ServicioPlan getServicioPlan() {
		return servicioPlan;
	}
	public void setServicioPlan(ServicioPlan servicioPlan) {
		this.servicioPlan = servicioPlan;
	}
	public Short getInputRamo() {
		return inputRamo;
	}
	public void setInputRamo(Short inputRamo) {
		this.inputRamo = inputRamo;
	}
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	public int getNumPagina() {
		return numPagina;
	}
	public HashMap<String, Object> getHmResultados() {
		return hmResultados;
	}
	public void setHmResultados(HashMap<String, Object> hmResultados) {
		this.hmResultados = hmResultados;
	}
	public int getFilaActual() {
		return filaActual;
	}
	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}
	public Set<Integer> getKeys() {
		return keys;
	}
	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}
	public String getStrRespuesta() {
		return strRespuesta;
	}
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
	public Short getInputProducto() {
		return inputProducto;
	}
	public void setInputProducto(Short inputProducto) {
		this.inputProducto = inputProducto;
	}
	public Short getInputPlan() {
		return inputPlan;
	}
	public void setInputPlan(Short inputPlan) {
		this.inputPlan = inputPlan;
	}
	public List<Object> getListaComboProductos() {
		return listaComboProductos;
	}
	public void setListaComboProductos(List<Object> listaComboProductos) {
		this.listaComboProductos = listaComboProductos;
	}
	public List<Object> getListaComboPlanes() {
		return listaComboPlanes;
	}
	public void setListaComboPlanes(List<Object> listaComboPlanes) {
		this.listaComboPlanes = listaComboPlanes;
	}
	public List<Object> getListaHomologa() {
		return listaHomologa;
	}
	public void setListaHomologa(List<Object> listaHomologa) {
		this.listaHomologa = listaHomologa;
	}
	public BeanHomologa getObjActual() {
		return objActual;
	}
	public void setObjActual(BeanHomologa objActual) {
		this.objActual = objActual;
	}
	public BeanHomologa getObjNuevo() {
		return objNuevo;
	}
	public void setObjNuevo(BeanHomologa objNuevo) {
		this.objNuevo = objNuevo;
	}
	public String getInputSubproBanco() {
		return inputSubproBanco;
	}
	public void setInputSubproBanco(String inputSubproBanco) {
		this.inputSubproBanco = inputSubproBanco;
	}
	public Integer getnTipoCarga() {
		return nTipoCarga;
	}
	public void setnTipoCarga(Integer nTipoCarga) {
		this.nTipoCarga = nTipoCarga;
	}
	public File getArchivo() {
		return archivo;
	}
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
}
