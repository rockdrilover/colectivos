package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.*;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.*;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.model.dao.CartCoberturasDao;
import mx.com.santander.aseguradora.colectivos.model.dao.hibernate.*;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log.*;
import org.hibernate.Query;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.SessionFactory; 


@Controller
@Scope("session")
public class BeanCatalogoCoberturas { // implements InitializingBean {  // // 
	//extends HibernateDaoSupport implements 	ContratanteDao
	
	@Resource
	private ServicioCartCoberturas servicioCartCoberturas;
	
	@Resource
	private ServicioColectivosCoberturas servicioColectivosCoberturas;
	
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	//BEANS
	private CartCoberturasId id;
	private ColectivosCoberturasId idCob;
	
	private CartRamosPolizas cartRamosPolizas;
	private String cacbDeCobertura;
	private Byte cacbCaroCdRamo;
	private Long cacbMtMaximo;
	private String cacbInSumarizacion;
	private String cacbInCoberturaSub;
	private String cacbDeCobertura1;
	private String cacbDeCobertura2;
	private String cacbDeCobertura3;
	private String cacbInAumentoAutomatico;
	private Long cacbMtMaximoDolares;
	private String cacbInValidaMontoSin;
	private String cacbInSumapri;
	private String cacbInRestitucion;
	private String cacbTipoCobertura;
	
	private CartCoberturas objNuevo = new CartCoberturas();
	private CartCoberturas objActual = new CartCoberturas();
	
	private Set<Integer> keys = new HashSet<Integer>();
	private int filaActual;
	private List<Object> listaCartCoberturas;

	private int numPagina;
	
	private HashMap<String, Object> hmResultados;
	private  String  strRespuesta;
	private String valComboCobertura;
	private String poliza;
	
	
	// constructor
	
	public BeanCatalogoCoberturas() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_COBERTURAS);
	
		this.message = new StringBuilder();
		this.log.debug("BeanCatalogoCoberturas creado");
		this.numPagina = 1;
		this.id						= new CartCoberturasId();
		this.listaCartCoberturas		= new ArrayList<Object>();
		this.cartRamosPolizas	= new CartRamosPolizas();
		
	}
	
	//METODOS
	public String consultarCartCoberturas(){

		String filtro ;
		hmResultados = new HashMap<String, Object>();
		CartCoberturas bean;
		
	
		try {
	
			
			if(getCartRamosPolizas().getCarpCdRamo() > 0){
				
				filtro  = getCartRamosPolizas().getCarpCdRamo() +" )";
						
				List<CartCoberturas> listaCartCoberturas = this.servicioCartCoberturas.obtenerCartCoberturas(filtro.toString());
				
			
     			for (CartCoberturas cartCoberturas : listaCartCoberturas) {
					
				
					getListaCartCoberturas().add(cartCoberturas);
				
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (CartCoberturas) ConstruirObjeto.crearBean(CartCoberturas.class, cartCoberturas);
				
				hmResultados.put(bean.getId().getCacbCdCobertura().toString(), bean);
				
				}
				
     			setStrRespuesta("Se ejecuto con EXITO la consulta, desplegar los resultados.");
				
			} else {
				setStrRespuesta("Favor de seleccionar un RAMO para poder realizar la consulta.");
			}
			
			FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CATALOGO_COBERTURAS);
			return NavigationResults.SUCCESS;
			
		} catch (Exception e) {
			this.message.append("No se pueden recuperar las coberturas.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());
			e.printStackTrace();

			return NavigationResults.FAILURE;
		}
	}

	
		
	// Metodos
	
	public void modificar() {
		try {
			
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			CartCoberturas cartCoberturas = (CartCoberturas) ConstruirObjeto.crearObjeto(CartCoberturas.class, objActual);
			
			servicioCartCoberturas.actualizarObjeto(cartCoberturas);
						
			hmResultados.remove(objActual.getId().getCacbCdCobertura().toString());
			hmResultados.put(objActual.getId().getCacbCdCobertura().toString(), objActual);
			
			
			setStrRespuesta("Se modifico con EXITO el registro.");
			
			consultarCartCoberturas();
				
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
		
	}
	
	// fla actual
	
	public void buscarFilaActual(ActionEvent event) {
        String clave =(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("clave"));
        filaActual = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("row"));
        Iterator<Object> it = getListaCartCoberturas().iterator();
        while(it.hasNext()) {
        	CartCoberturas objCoberturas = (CartCoberturas) it.next();
        	if (objCoberturas.getId().getCacbCdCobertura().toString().equals(clave)){
                objActual = objCoberturas;
                break;
            }
        }
    }

	//Maprows
		public ArrayList<Object> getMapValues(){
			ArrayList<Object> lstValues = new ArrayList<Object>();
	        lstValues.addAll(getHmResultados().values());
	        return lstValues;
	    }
		
		
		
		public void afterPropertiesSet() throws Exception {
			
			try {
				
			} catch (Exception e) {
				this.message.append("No se pueden cargar las coberturas.");
				this.log.error(message.toString(), e);
				throw new FacesException(message.toString(), e);
			}
		}	
	  

	
	//Getters y  Setters
		
	public ServicioCartCoberturas getServicioCartCoberturas() {
		return servicioCartCoberturas;
	}

	public void setServicioCartCoberturas(
			ServicioCartCoberturas servicioCartCoberturas) {
		this.servicioCartCoberturas = servicioCartCoberturas;
	}

	public ServicioColectivosCoberturas getServicioColectivosCoberturas() {
		return servicioColectivosCoberturas;
	}

	public void setServicioColectivosCoberturas(
			ServicioColectivosCoberturas servicioColectivosCoberturas) {
		this.servicioColectivosCoberturas = servicioColectivosCoberturas;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

		
	public String getValComboCobertura() {
		return valComboCobertura;
	}

	public void setValComboCobertura(String valComboCobertura) {
		this.valComboCobertura = valComboCobertura;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	

	public CartCoberturas getObjNuevo() {
		return objNuevo;
	}


	public void setObjNuevo(CartCoberturas objNuevo) {
		this.objNuevo = objNuevo;
	}


	public Set<Integer> getKeys() {
		return keys;
	}


	public void setKeys(Set<Integer> keys) {
		this.keys = keys;
	}


	public int getFilaActual() {
		return filaActual;
	}


	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}




	public HashMap<String, Object> getHmResultados() {
		return hmResultados;
	}


	public void setHmResultados(HashMap<String, Object> hmResultados) {
		this.hmResultados = hmResultados;
	}


	public String getStrRespuesta() {
		return strRespuesta;
	}


	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}

	
	
	public CartCoberturasId getId() {
		return id;
	}


	public void setId(CartCoberturasId id) {
		this.id = id;
	}


	public CartRamosPolizas getCartRamosPolizas() {
		return cartRamosPolizas;
	}


	public void setCartRamosPolizas(CartRamosPolizas cartRamosPolizas) {
		this.cartRamosPolizas = cartRamosPolizas;
	}


	public String getCacbDeCobertura() {
		return cacbDeCobertura;
	}


	public void setCacbDeCobertura(String cacbDeCobertura) {
		this.cacbDeCobertura = cacbDeCobertura;
	}


	public Byte getCacbCaroCdRamo() {
		return cacbCaroCdRamo;
	}


	public void setCacbCaroCdRamo(Byte cacbCaroCdRamo) {
		this.cacbCaroCdRamo = cacbCaroCdRamo;
	}


	public Long getCacbMtMaximo() {
		return cacbMtMaximo;
	}


	public void setCacbMtMaximo(Long cacbMtMaximo) {
		this.cacbMtMaximo = cacbMtMaximo;
	}


	public String getCacbInSumarizacion() {
		return cacbInSumarizacion;
	}


	public void setCacbInSumarizacion(String cacbInSumarizacion) {
		this.cacbInSumarizacion = cacbInSumarizacion;
	}


	public String getCacbInCoberturaSub() {
		return cacbInCoberturaSub;
	}


	public void setCacbInCoberturaSub(String cacbInCoberturaSub) {
		this.cacbInCoberturaSub = cacbInCoberturaSub;
	}


	public String getCacbDeCobertura1() {
		return cacbDeCobertura1;
	}


	public void setCacbDeCobertura1(String cacbDeCobertura1) {
		this.cacbDeCobertura1 = cacbDeCobertura1;
	}


	public String getCacbDeCobertura2() {
		return cacbDeCobertura2;
	}


	public void setCacbDeCobertura2(String cacbDeCobertura2) {
		this.cacbDeCobertura2 = cacbDeCobertura2;
	}


	public String getCacbDeCobertura3() {
		return cacbDeCobertura3;
	}


	public void setCacbDeCobertura3(String cacbDeCobertura3) {
		this.cacbDeCobertura3 = cacbDeCobertura3;
	}


	public String getCacbInAumentoAutomatico() {
		return cacbInAumentoAutomatico;
	}


	public void setCacbInAumentoAutomatico(String cacbInAumentoAutomatico) {
		this.cacbInAumentoAutomatico = cacbInAumentoAutomatico;
	}


	public Long getCacbMtMaximoDolares() {
		return cacbMtMaximoDolares;
	}


	public void setCacbMtMaximoDolares(Long cacbMtMaximoDolares) {
		this.cacbMtMaximoDolares = cacbMtMaximoDolares;
	}


	public String getCacbInValidaMontoSin() {
		return cacbInValidaMontoSin;
	}


	public void setCacbInValidaMontoSin(String cacbInValidaMontoSin) {
		this.cacbInValidaMontoSin = cacbInValidaMontoSin;
	}


	public String getCacbInSumapri() {
		return cacbInSumapri;
	}


	public void setCacbInSumapri(String cacbInSumapri) {
		this.cacbInSumapri = cacbInSumapri;
	}


	public String getCacbInRestitucion() {
		return cacbInRestitucion;
	}


	public void setCacbInRestitucion(String cacbInRestitucion) {
		this.cacbInRestitucion = cacbInRestitucion;
	}


	public String getCacbTipoCobertura() {
		return cacbTipoCobertura;
	}


	public void setCacbTipoCobertura(String cacbTipoCobertura) {
		this.cacbTipoCobertura = cacbTipoCobertura;
	}

	public CartCoberturas getObjActual() {
		return objActual;
	}


	public void setObjActual(CartCoberturas objActual) {
		this.objActual = objActual;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	public int getNumPagina() {
		return numPagina;
	}

	public List<Object> getListaCartCoberturas() {
		return listaCartCoberturas;
	}

	public void setListaCartCoberturas(List<Object> listaCartCoberturas) {
		this.listaCartCoberturas = listaCartCoberturas;
	}
	
	public ColectivosCoberturasId getIdCob() {
		return idCob;
	}


	public void setIdCob(ColectivosCoberturasId idCob) {
		this.idCob = idCob;
	}


	
	
}
