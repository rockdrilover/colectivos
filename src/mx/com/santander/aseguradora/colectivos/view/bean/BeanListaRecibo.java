package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Recibo;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioRecibo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqCancelacionCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResCancelacionCFDIDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import WSTimbrado.EnviaTimbrado;

/**
 * @author IBB
 *
 */
@Controller
@Scope("session")
public class BeanListaRecibo {

	@Resource
	private ServicioRecibo servicioRecibo;
	
	
	/**
	 * Capa de servicio para timbrado de servicio
	 */
	@Resource
	private ServicioCertificado servicioCertificado;
	
	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	
	private List<Object> listaRecibos;
	private List<Object> listaCoberturas;
	private List<Object> listaComponentes;
	
	private short canal;
	private int certif;
	private Short ramo;
	private Long poliza;
    private Date feDesde;
    private Date feHasta;
    private Double noRecibo;
    private Double noReciboConsulta;
	private String respuesta;
	private int numPagina;
	private Integer idVenta;
	/*
	 * Elemento seleccionado lista
	 */
	private BeanRecibo seleccionado;
	
	public Integer getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}
	public ServicioRecibo getServicioRecibo() {
		return servicioRecibo;
	}
	public void setServicioRecibo(ServicioRecibo servicioRecibo) {
		this.servicioRecibo = servicioRecibo;
	}
	public short getCanal() {
		return canal;
	}
	public void setCanal(short canal) {
		this.canal = canal;
	}
	public int getCertif() {
		return certif;
	}
	public void setCertif(int certif) {
		this.certif = certif;
	}
	public Short getRamo() {
		return ramo;
	}
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	public Long getPoliza() {
		return poliza;
	}
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	public Date getFeDesde() {
		return feDesde;
	}
	public void setFeDesde(Date feDesde) {
		this.feDesde = feDesde;
	}
	public Date getFeHasta() {
		return feHasta;
	}
	public void setFeHasta(Date feHasta) {
		this.feHasta = feHasta;
	}
	public Double getNoRecibo() {
		return noRecibo;
	}
	public void setNoRecibo(Double noRecibo) {
		this.noRecibo = noRecibo;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public int getNumPagina() {
		return numPagina;
	}
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	public void setListaRecibo(List<Object> listaRecibos) {
		this.listaRecibos = listaRecibos;
	}
	public List<Object> getListaRecibos() {
		return listaRecibos;
	}
	public List<Object> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setListaCoberturas(List<Object> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public List<Object> getListaComponentes() {
		return listaComponentes;
	}
	public void setListaComponentes(List<Object> listaComponentes) {
		this.listaComponentes = listaComponentes;
	}
	public Double getNoReciboConsulta() {
		return noReciboConsulta;
	}
	public void setNoReciboConsulta(Double noReciboConsulta) {
		this.noReciboConsulta = noReciboConsulta;
	}
	
	public BeanListaRecibo() {
		this.message = new StringBuilder();
		this.canal = 1;
		this.certif = 0;
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_RECIBO);
	}
	
	public void consulta(){
		
		StringBuilder filtro = new StringBuilder();
		this.listaRecibos.clear();
		
		try {
			
			filtro.append(" and R.id.coreCasuCdSucursal = ").append(this.canal).append(" \n");
			if(this.ramo != null && this.ramo.intValue() > 0){
				filtro.append(" and R.coreCarpCdRamo = ").append(this.ramo).append(" \n");
			}
			if(this.poliza != null && this.poliza.intValue() > 0){
				filtro.append(" and R.coreCapoNuPoliza = ").append(this.poliza).append(" \n");
			}
			if(this.feDesde != null){ //&& this.feDesde.length() > 0){
				filtro.append(" and R.coreFeEmision >= '").append(new SimpleDateFormat("dd/MM/yyyy").format(this.feDesde)).append("' \n");
			}
			if(this.feHasta != null){ //&& this.feDesde.length() > 0){
				filtro.append(" and R.coreFeEmision <= '").append(new SimpleDateFormat("dd/MM/yyyy").format(this.feHasta)).append("' \n");
			}
			if(this.noRecibo != null && this.noRecibo > 0){
				filtro.append(" and R.id.coreNuRecibo = ").append(this.noRecibo).append(" \n");
			}
			filtro.append("order by R.coreFeEmision desc \n");
			List<Recibo> lista = this.servicioRecibo.obtenerObjetos(filtro.toString());
			//System.out.println("Tama�o lista " + lista.size());
			for(Recibo rec: lista){
			// System.out.println("entra para llenar la lista ");
				BeanRecibo bean = (BeanRecibo) ConstruirObjeto.crearBean(BeanRecibo.class, rec);
				bean.setReciboAnterior(rec.getCareNuReciboAnterior());
				bean.setServicioRecibo(this.servicioRecibo);
				
				if (this.poliza.intValue() == 0) bean.setPrimaUnica(new Short("1"));
				else bean.setPrimaUnica(new Short("0"));
				
				this.listaRecibos.add(bean);
			}
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			this.message.append("Error al recuperar los Recibos");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
		//return noRecibo.toString();
	}
	
	/**
	 * Metodo para consultar los recibos segun el filtro seleccionado
	 * @return cadena de respuesta
	 */
	public String consulta2(){
		List<Object[]> lista;
		BeanRecibo bean;
		String strRespuesta;
		
		try {
			strRespuesta = "Se realizo consulta con exito, favor de revisar resultados";
			if(!validaDatos()) {
				listaRecibos = null;
				return null;
			}
			
			listaRecibos = new ArrayList<Object>();
			lista = this.servicioRecibo.consulta2(this.canal, this.ramo, this.poliza, this.idVenta, this.feDesde, this.feHasta, this.noRecibo);
			if(lista.isEmpty()) {
				listaRecibos = null;
				setRespuesta(strRespuesta);
				return null;
			}
			 
			for(Object[] certificado: lista){
				bean = new BeanRecibo();
				bean.getId().setCareCasuCdSucursal(((BigDecimal)certificado[0]).shortValue());
				bean.setCareCarpCdRamo(((BigDecimal)certificado[1]).shortValue());
				bean.setCareCapoNuPoliza(((BigDecimal)certificado[2]).intValue());
				bean.getId().setCareNuRecibo((BigDecimal)certificado[3]);
				bean.setCareStRecibo(certificado[4].toString());
				bean.getEstatus().getId().setRvMeaning(certificado[5].toString());
				bean.setCareFeEmision((Date) certificado[6]);
				bean.setReciboAnterior((BigDecimal)certificado[7]);
				bean.setServicioRecibo(this.servicioRecibo);
				bean.setServicioCertificado(this.servicioCertificado);
				
				bean.setUUID((String)certificado[8]);
				bean.setStCancelado((String)certificado[9]);
				bean.setStCobranza((String)certificado[10]);
				
				if (this.poliza.intValue() == 0) {
					bean.setPrimaUnica(new Short("1"));
				} else {
					bean.setPrimaUnica(new Short("0"));
				}
				
				this.listaRecibos.add(bean);
			}
			
			setRespuesta(strRespuesta);
			return null;
		} catch (Exception e) {
			this.message.append("Error al recuperar certificados para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}
	}
	
	private boolean validaDatos() {
		boolean bRegreso = true;
		try {
			if(this.ramo == 0){
				setRespuesta("Favor de seleccionar un ramo");
				bRegreso = false;
			} else if(this.poliza == -1){
				setRespuesta("Favor de seleccionar una poliza o seleccionar Prima Unica");
				bRegreso = false;
			} else if(this.poliza == 0){
				if(this.idVenta == 0) {
					setRespuesta("Favor de seleccionar un Canal de Venta");
					bRegreso = false;
				}
			}
					
			if(bRegreso) {
				if((this.feDesde == null || this.feHasta == null) && (this.noRecibo < 1)){ 
					setRespuesta("Favor de ingresar fechas o numero de recibo para realizar la consulta");
					bRegreso = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			bRegreso = false;
		}
		
		return bRegreso;
	}

	public void mostrarCoberComp() throws Excepciones {
		try{
			listaCoberturas = new ArrayList<Object>();
			listaComponentes = new ArrayList<Object>();
			if(noReciboConsulta != null) {
				listaCoberturas.addAll(this.servicioRecibo.consultaDetalleCobertura(this.getRamo(), getNoReciboConsulta(), 1));
				if(listaCoberturas.isEmpty()){
					listaCoberturas = null;
				}
				listaComponentes.addAll(this.servicioRecibo.consultaDetalleComponentes(this.getRamo(), getNoReciboConsulta(), 1));
				if(listaComponentes.isEmpty()){
					listaComponentes = null;
				}
				
				setRespuesta("Se realizo consulta para recibo: " + getNoReciboConsulta().toString() + ", favor de revisar resultados.");
			}
		} catch (Exception e) {
			System.out.println("exception " + e);
			this.message.append("Error al recuperar el historial de cr�dito para consulta.");
			this.log.error(message.toString(), e);
			throw new FacesException(message.toString(), e);
		}	
	}
	
	/**
	 * Timbrar CFDI con Version 3.3
	 * @throws Excepciones Error en timbrado
	 */
	 public void sellarRecibos() throws Excepciones {
		
			
			String ip;
			String usuario;
			
	    	try {
	    		
	    		ExternalContext context;
				context = FacesContext.getCurrentInstance().getExternalContext();

				HttpServletRequest request = (HttpServletRequest) context.getRequest();
				HttpSession sesion = request.getSession(true);
				
	    		
	    		ip = request.getHeader("X-FORWARDED-FOR");
	             if (ip == null || "".equals(ip)) {
	            	 ip = request.getRemoteAddr();
	             }
	             
	             if ("0:0:0:0:0:0:0:1".equalsIgnoreCase(ip)) {
	            	    InetAddress inetAddress = InetAddress.getLocalHost();
	            	    String ipAddress = inetAddress.getHostAddress();
	            	    ip = ipAddress;
	            	}
	    		//ip = request.getRemoteAddr();
	    		usuario = (String) sesion.getAttribute("username");
	    		
	    		
	    			
	    				ReqTimbradoCFDIDTO req = new ReqTimbradoCFDIDTO(usuario, Constantes.CFDI_APLICACION, ip,
								String.valueOf(seleccionado.getId().getCareCasuCdSucursal()),
								String.valueOf(seleccionado.getCareCarpCdRamo()),
								String.valueOf(seleccionado.getCareCapoNuPoliza()),
								String.valueOf(0),
								String.format("%.1f", seleccionado.getId().getCareNuRecibo()) , true);

	    				ResTimbradoCFDIDTO res = getServicioCertificado().sellarRecibosV33(req);
	    				
	    				if (Constantes.CFDI_TIMBRAR_EXISTOSO.equals(res.getMensaje())) {
	    					//objBeanFacturacion.setCofaCampov3("Se realizo timbrado con exito");
	    					
	    					setRespuesta("Se realizo timbrado con exito");
	    					consulta2();
	    				} else {
	    					//objBeanFacturacion.setCofaCampov3(res.getMensaje() + " - " + res.getDescripcion());
	    					setRespuesta("Error al sellar recibo: " + res.getMensaje() + " - " + res.getDescripcion());
	    				}
	    				
	    			
	    				/*objTimbrado = this.servicioCertificado.sellarRecibos(objBeanFacturacion.getId().getCofaCasuCdSucursal(), objBeanFacturacion.getId().getCofaCarpCdRamo(), objBeanFacturacion.getCofaNuReciboFiscal());
	    				if(objTimbrado.getCFDI() != null) {
	    					this.servicioCertificado.actualizarDatosCFDI(objTimbrado, objBeanFacturacion.getId().getCofaCasuCdSucursal(), objBeanFacturacion.getId().getCofaCarpCdRamo(), objBeanFacturacion.getCofaNuReciboFiscal());
	    					objBeanFacturacion.setCofaCampov3("Se realizo timbrado con exito");
	    				} else {
	    					objBeanFacturacion.setCofaCampov3("Error: " + objTimbrado.getError());
	    				}*/
	    				
	    			
	        	
			} catch (Exception e) {
				this.message.append("Error al sellar recibos.");
				
				this.log.error(message.toString(), e);
				throw new FacesException(message.toString(), e);
			}
		}
	 
	 /**
		 * Cancelar CFDI con Version 3.3
		 * @throws Excepciones Error en timbrado
		 */
		 public void cancelarRecibos() throws Excepciones {
			
				
				String ip;
				String usuario;
				
		    	try {
		    		
		    		ExternalContext context;
					context = FacesContext.getCurrentInstance().getExternalContext();

					HttpServletRequest request = (HttpServletRequest) context.getRequest();
					HttpSession sesion = request.getSession(true);
					
		    		
		    		ip = request.getHeader("X-FORWARDED-FOR");
		             if (ip == null || "".equals(ip)) {
		            	 ip = request.getRemoteAddr();
		             }
		             
		             if ("0:0:0:0:0:0:0:1".equalsIgnoreCase(ip)) {
		            	    InetAddress inetAddress = InetAddress.getLocalHost();
		            	    String ipAddress = inetAddress.getHostAddress();
		            	    ip = ipAddress;
		            	}
		    		//ip = request.getRemoteAddr();
		    		usuario = (String) sesion.getAttribute("username");
		    		
		    		
		    			
		    				ReqCancelacionCFDIDTO req = new ReqCancelacionCFDIDTO(usuario, Constantes.CFDI_APLICACION, ip,
									String.valueOf(seleccionado.getId().getCareCasuCdSucursal()),
									
									String.format("%.1f", seleccionado.getId().getCareNuRecibo()) );

		    				ResCancelacionCFDIDTO res = getServicioCertificado().cancelaCFDI(req);
		    				
		    				if (Constantes.CFDI_CANCELAR_EXISTOSO.equals(res.getMensaje())) {
		    					//objBeanFacturacion.setCofaCampov3("Se realizo timbrado con exito");
		    					
		    					setRespuesta("Se realizo cancelacion con exito");
		    					consulta2();
		    				} else {
		    					//objBeanFacturacion.setCofaCampov3(res.getMensaje() + " - " + res.getDescripcion());
		    					setRespuesta("Error al cancelar recibo: " + res.getMensaje() + " - " + res.getDescripcion());
		    				}
		    				
		    			
		    				/*objTimbrado = this.servicioCertificado.sellarRecibos(objBeanFacturacion.getId().getCofaCasuCdSucursal(), objBeanFacturacion.getId().getCofaCarpCdRamo(), objBeanFacturacion.getCofaNuReciboFiscal());
		    				if(objTimbrado.getCFDI() != null) {
		    					this.servicioCertificado.actualizarDatosCFDI(objTimbrado, objBeanFacturacion.getId().getCofaCasuCdSucursal(), objBeanFacturacion.getId().getCofaCarpCdRamo(), objBeanFacturacion.getCofaNuReciboFiscal());
		    					objBeanFacturacion.setCofaCampov3("Se realizo timbrado con exito");
		    				} else {
		    					objBeanFacturacion.setCofaCampov3("Error: " + objTimbrado.getError());
		    				}*/
		    				
		    			
		        	
				} catch (Exception e) {
					this.message.append("Error al sellar recibos.");
					
					this.log.error(message.toString(), e);
					throw new FacesException(message.toString(), e);
				}
			}
	/**
	 * @return the seleccionado
	 */
	public BeanRecibo getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(BeanRecibo seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the servicioCertificado
	 */
	public ServicioCertificado getServicioCertificado() {
		return servicioCertificado;
	}
	/**
	 * @param servicioCertificado the servicioCertificado to set
	 */
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
	
	
	
	
	
}
