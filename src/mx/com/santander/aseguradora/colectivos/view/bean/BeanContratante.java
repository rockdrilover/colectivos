/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCiudades;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartCliente;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CartClientePK;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Estado;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCiudad;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioContratante;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEstado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;

/**
 * @author dflores
 * Modifico : Leobardo Preciado 25/03/2015
 * 
 *
 */
@Controller
@Scope("session")
public class BeanContratante implements InitializingBean{
	
	@Resource
	private ServicioContratante servicioContratante;
	@Resource
	private ServicioCiudad	servicioCiudad;
	@Resource
	private ServicioEstado servicioEstado;
	@Resource
	private ServicioProcesos servicioProcesos;
	
	private Log log = LogFactory.getLog(this.getClass());
	private String strRespuesta;

	private CartCliente	cartCliente;
	private CartClientePK id;
	private String cacnApellidoMat;
	private String cacnApellidoPat;
	private BigDecimal cacnCaaeCdActividad;
	private BigDecimal cacnCaciCdCiudadCob;
	private BigDecimal cacnCaciCdCiudadHab;
	private BigDecimal cacnCaesCdEstadoCob;
	private BigDecimal cacnCaesCdEstadoHab;
	private BigDecimal cacnCapaCdPais;
	private String cacnCazpColoniaCob;
	private String cacnCazpPoblacCob;
	private BigDecimal cacnCdDependencia1;
	private BigDecimal cacnCdDependencia2;
	private BigDecimal cacnCdDependencia3;
	private BigDecimal cacnCdDependencia4;
	private String cacnCdEdoCivil;
	private String cacnCdNacionalidadA;
	private String cacnCdSexo;
	private String cacnCdTaxid;
	private String cacnCdVida;
	private String cacnDatosReg1;
	private String cacnDatosReg2;
	private String cacnDiCobro1;
	private String cacnDiCobro2;
	private String cacnDiHabitacion1;
	private String cacnDiHabitacion2;
	private Date cacnFeNacimiento;
	private String cacnInEmpleado;
	private String cacnInTratamientoEspecial;
	private String cacnInUsadoFinanciamiento;
	private BigDecimal cacnNacionalidadFatca;
	private String cacnNmApellidoRazon;
	private String cacnNmPersonaNatural;
	private String cacnNombre;
	private String cacnNuApartadoCobro;
	private String cacnNuApartadoHabitacion;
	private String cacnNuCedulaAnterior;
	private BigDecimal cacnNuConsultas;
	private String cacnNuEmpleadoFicha;
	private String cacnNuFaxCobro;
	private BigDecimal cacnNuLlamadas;
	private BigDecimal cacnNuServicios;
	private String cacnNuTelefonoCobro;
	private String cacnNuTelefonoHabitacion;
	private BigDecimal cacnPaisFatca;
	private String cacnRegistro1;
	private String cacnRegistro2;
	private String cacnRfc;
	private String cacnStActuaNom;
	private String cacnStCliente;
	private String cacnTpCliente;
	private String cacnZnPostalCobro;
	private String cacnZnPostalHabitacion;
	
	private String ciudad;
	private String estado;
	
	private List<Object> listaComboContratantes;
	private List<BeanContratante> listaContratantes;
	private BeanContratante cliente;
	private List<Object> listaComboCiudades;
	private CartCliente newCliente;
	

	public BeanContratante() {
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONTRATANTE);
		this.listaComboContratantes = new ArrayList<Object>();
		this.listaContratantes 		= new ArrayList<BeanContratante>();
		this.listaComboCiudades		= new ArrayList<Object>();
		this.cartCliente			= new CartCliente();
		this.newCliente				= new CartCliente();
		setId(new CartClientePK());
	}
	
	public void cargaContratantes(){
		getListaComboContratantes().clear();
		
		try {
			List<CartCliente> lista = this.servicioContratante.obtenerObjetos(Constantes.CLIENTE_CONSULTA_ALL, null);
			
			for (CartCliente cartCliente : lista) {
				this.listaComboContratantes.add(new SelectItem(cartCliente.getId().getCacnNuCedulaRif(),
															   cartCliente.getId().getCacnNuCedulaRif()+"-"+
															   cartCliente.getCacnNmApellidoRazon() ));
			}
		} catch (Exception e) {
			this.log.error("No se pueden cargar los Contratantes.", e);
			throw new FacesException("No se pueden cargar los Contratantes.", e);
		}
		
	}
	
	public String consultar(){
		getListaContratantes().clear();
		
		try {
			
			List<CartCliente> lista= this.servicioContratante.obtenerObjetos(Constantes.CLIENTE_CONSULTA, getId());
			
			for (CartCliente cartCliente : lista) {
				BeanContratante beanContratante = new BeanContratante();
				beanContratante.setEstado(findEstado(cartCliente.getCacnCaesCdEstadoHab()) );
				beanContratante.setCiudad(findCiudad(cartCliente.getCacnCaesCdEstadoHab(), cartCliente.getCacnCaciCdCiudadHab()));
				beanContratante.setCartCliente (cartCliente );
				getListaContratantes().add(beanContratante);
			}

			setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			setStrRespuesta("Error al intentar ejecutar la consulta, Por favor intente mas tarde.");
			this.log.error("No se pueden cargar los parametros.", e);
			return NavigationResults.FAILURE;
		}
	}
	
	//LPV
	public String findEstado(BigDecimal cdEstado) throws Excepciones{
		StringBuilder filtro = new StringBuilder(100);
		String descEstado 	 = null;
		
		try{		
			filtro.append(" and E.caesCdEstado = ").append(cdEstado);
			List<Estado> estado = this.servicioEstado.obtenerObjetos(filtro.toString());
			
			for (Estado estado2 : estado) {
				descEstado = estado2.getCaesDeEstado();
			}
		
			return descEstado;
		} catch (Exception e) {
			this.log.error("No se puede recuperar el estado.", e);
			throw new Excepciones("No se puede recuperar el estado.", e);
		}
	}

	//LPV
	public String findCiudad(BigDecimal cdEstado, BigDecimal cdCiudad) throws Excepciones{
		StringBuilder filtro = new StringBuilder(100);
		String descCiudad 	 = null;
		
		try{		
			filtro.append(" and E.id.caciCaesCdEstado = ").append(cdEstado);
			filtro.append(" and E.id.caciCdCiudad 	  = ").append(cdCiudad);
			filtro.append(" ");
			
			List<CartCiudades> ciudad = this.servicioCiudad.obtenerObjetos(filtro.toString());
			
			for (CartCiudades cartCiudades : ciudad) {
				descCiudad = cartCiudades.getCaciDeCiudad();
			}
			
			return descCiudad;
		} catch (Exception e) {
			this.log.error("No se puede recuperar el estado.", e);
			throw new Excepciones("No se puede recuperar el estado.", e);
		}
	}

	//LPV
	public void modificar() {
		try {
			servicioContratante.actualizarObjeto(getCartCliente());
			consultar();
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			setStrRespuesta("Error al modificar registro.");
			e.printStackTrace();
		}
	}

	public void consultaCiudad(){
		getListaComboCiudades().clear();
		StringBuilder filtro = new StringBuilder();
		
		try{
			BigDecimal cdCiudad = null;
			
			if(getCartCliente().getCacnCaesCdEstadoHab()!= null){
				cdCiudad = getCartCliente().getCacnCaesCdEstadoHab();
			}

			if(getNewCliente().getCacnCaesCdEstadoHab()!= null){
				cdCiudad = getNewCliente().getCacnCaesCdEstadoHab();
			}
			
			filtro.append(" and E.id.caciCaesCdEstado = ").append(cdCiudad);
			filtro.append(" ");
			List<CartCiudades> lista = this.servicioCiudad.obtenerObjetos(filtro.toString());

			for (CartCiudades cartCiudades : lista) {
				this.listaComboCiudades.add(new SelectItem(new BigDecimal(cartCiudades.getId().getCaciCdCiudad()).toString(),cartCiudades.getCaciDeCiudad()));
			}
		}catch (Exception e) {
			this.log.error("No se pueden cargar las ciudades.", e);
			throw new FacesException("No se pueden cargar las ciudades.", e);
		}
	}
	
	public String guardar(){
		try {
	
			CartClientePK newId = new CartClientePK();
			newId.setCacnCdNacionalidad("J");
			newId.setCacnNuCedulaRif(this.servicioProcesos.numeradorCliente());
			
			getNewCliente().setId(newId);
			getNewCliente().setCacnTpCliente			("J");
			getNewCliente().setCacnInEmpleado			("N");
			getNewCliente().setCacnInTratamientoEspecial("N");
			getNewCliente().setCacnCdVida				("V");
			getNewCliente().setCacnInUsadoFinanciamiento("N");
			getNewCliente().setCacnCaaeCdActividad		(new BigDecimal("47"));
			getNewCliente().setCacnCdEdoCivil			("S");
			getNewCliente().setCacnCdSexo				("M");
			
			this.servicioContratante.guardarObjeto(getNewCliente());
			this.servicioProcesos.actNumeradorCliente();
			cargaContratantes();
			clearBean();
			
			setStrRespuesta("Contratante con Nacionalidad J y cedula " + newId.getCacnNuCedulaRif() + " se dio de alta satisfactoriamente.");
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			setStrRespuesta("Error al intentar guardar el registro, Por favor intente mas tarde.");
			this.log.error("No se pueden cargar los parametros.", e);
			FacesUtils.addErrorMessage("No se pueden cargar los parametros.");
			return NavigationResults.FAILURE;
		}

	}	
	
		
	private void clearBean(){
		setNewCliente(new CartCliente());
	}
	
	public void afterPropertiesSet() throws Exception {
		cargaContratantes();
	}

	/* GETTERS Y SETTERS */
	public ServicioContratante getServicioContratante() {
		return servicioContratante;
	}

	public void setServicioContratante(ServicioContratante servicioContratante) {
		this.servicioContratante = servicioContratante;
	}

	public ServicioCiudad getServicioCiudad() {
		return servicioCiudad;
	}
	public void setServicioCiudad(ServicioCiudad servicioCiudad) {
		this.servicioCiudad = servicioCiudad;
	}
	
	public ServicioEstado getServicioEstado() {
		return servicioEstado;
	}
	public void setServicioEstado(ServicioEstado servicioEstado) {
		this.servicioEstado = servicioEstado;
	}
	
	public ServicioProcesos getServicioProcesos() {
		return servicioProcesos;
	}
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}
	
	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public String getStrRespuesta() {
		return strRespuesta;
	}

	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}

	public CartCliente getCartCliente() {
		return cartCliente;
	}
	public void setCartCliente(CartCliente cartCliente) {
		this.cartCliente = cartCliente;
	}
	
	public CartClientePK getId() {
		return id;
	}

	public void setId(CartClientePK id) {
		this.id = id;
	}

	public String getCacnApellidoMat() {
		return cacnApellidoMat;
	}

	public void setCacnApellidoMat(String cacnApellidoMat) {
		this.cacnApellidoMat = cacnApellidoMat;
	}

	public String getCacnApellidoPat() {
		return cacnApellidoPat;
	}

	public void setCacnApellidoPat(String cacnApellidoPat) {
		this.cacnApellidoPat = cacnApellidoPat;
	}

	public BigDecimal getCacnCaaeCdActividad() {
		return cacnCaaeCdActividad;
	}

	public void setCacnCaaeCdActividad(BigDecimal cacnCaaeCdActividad) {
		this.cacnCaaeCdActividad = cacnCaaeCdActividad;
	}

	public BigDecimal getCacnCaciCdCiudadCob() {
		return cacnCaciCdCiudadCob;
	}

	public void setCacnCaciCdCiudadCob(BigDecimal cacnCaciCdCiudadCob) {
		this.cacnCaciCdCiudadCob = cacnCaciCdCiudadCob;
	}

	public BigDecimal getCacnCaciCdCiudadHab() {
		return cacnCaciCdCiudadHab;
	}

	public void setCacnCaciCdCiudadHab(BigDecimal cacnCaciCdCiudadHab) {
		this.cacnCaciCdCiudadHab = cacnCaciCdCiudadHab;
	}

	public BigDecimal getCacnCaesCdEstadoCob() {
		return cacnCaesCdEstadoCob;
	}

	public void setCacnCaesCdEstadoCob(BigDecimal cacnCaesCdEstadoCob) {
		this.cacnCaesCdEstadoCob = cacnCaesCdEstadoCob;
	}

	public BigDecimal getCacnCaesCdEstadoHab() {
		return cacnCaesCdEstadoHab;
	}

	public void setCacnCaesCdEstadoHab(BigDecimal cacnCaesCdEstadoHab) {
		this.cacnCaesCdEstadoHab = cacnCaesCdEstadoHab;
	}

	public BigDecimal getCacnCapaCdPais() {
		return cacnCapaCdPais;
	}

	public void setCacnCapaCdPais(BigDecimal cacnCapaCdPais) {
		this.cacnCapaCdPais = cacnCapaCdPais;
	}

	public String getCacnCazpColoniaCob() {
		return cacnCazpColoniaCob;
	}

	public void setCacnCazpColoniaCob(String cacnCazpColoniaCob) {
		this.cacnCazpColoniaCob = cacnCazpColoniaCob;
	}

	public String getCacnCazpPoblacCob() {
		return cacnCazpPoblacCob;
	}

	public void setCacnCazpPoblacCob(String cacnCazpPoblacCob) {
		this.cacnCazpPoblacCob = cacnCazpPoblacCob;
	}

	public BigDecimal getCacnCdDependencia1() {
		return cacnCdDependencia1;
	}

	public void setCacnCdDependencia1(BigDecimal cacnCdDependencia1) {
		this.cacnCdDependencia1 = cacnCdDependencia1;
	}

	public BigDecimal getCacnCdDependencia2() {
		return cacnCdDependencia2;
	}

	public void setCacnCdDependencia2(BigDecimal cacnCdDependencia2) {
		this.cacnCdDependencia2 = cacnCdDependencia2;
	}

	public BigDecimal getCacnCdDependencia3() {
		return cacnCdDependencia3;
	}

	public void setCacnCdDependencia3(BigDecimal cacnCdDependencia3) {
		this.cacnCdDependencia3 = cacnCdDependencia3;
	}

	public BigDecimal getCacnCdDependencia4() {
		return cacnCdDependencia4;
	}

	public void setCacnCdDependencia4(BigDecimal cacnCdDependencia4) {
		this.cacnCdDependencia4 = cacnCdDependencia4;
	}

	public String getCacnCdEdoCivil() {
		return cacnCdEdoCivil;
	}

	public void setCacnCdEdoCivil(String cacnCdEdoCivil) {
		this.cacnCdEdoCivil = cacnCdEdoCivil;
	}

	public String getCacnCdNacionalidadA() {
		return cacnCdNacionalidadA;
	}

	public void setCacnCdNacionalidadA(String cacnCdNacionalidadA) {
		this.cacnCdNacionalidadA = cacnCdNacionalidadA;
	}

	public String getCacnCdSexo() {
		return cacnCdSexo;
	}

	public void setCacnCdSexo(String cacnCdSexo) {
		this.cacnCdSexo = cacnCdSexo;
	}

	public String getCacnCdTaxid() {
		return cacnCdTaxid;
	}

	public void setCacnCdTaxid(String cacnCdTaxid) {
		this.cacnCdTaxid = cacnCdTaxid;
	}

	public String getCacnCdVida() {
		return cacnCdVida;
	}

	public void setCacnCdVida(String cacnCdVida) {
		this.cacnCdVida = cacnCdVida;
	}

	public String getCacnDatosReg1() {
		return cacnDatosReg1;
	}

	public void setCacnDatosReg1(String cacnDatosReg1) {
		this.cacnDatosReg1 = cacnDatosReg1;
	}

	public String getCacnDatosReg2() {
		return cacnDatosReg2;
	}

	public void setCacnDatosReg2(String cacnDatosReg2) {
		this.cacnDatosReg2 = cacnDatosReg2;
	}

	public String getCacnDiCobro1() {
		return cacnDiCobro1;
	}

	public void setCacnDiCobro1(String cacnDiCobro1) {
		this.cacnDiCobro1 = cacnDiCobro1;
	}

	public String getCacnDiCobro2() {
		return cacnDiCobro2;
	}

	public void setCacnDiCobro2(String cacnDiCobro2) {
		this.cacnDiCobro2 = cacnDiCobro2;
	}

	public String getCacnDiHabitacion1() {
		return cacnDiHabitacion1;
	}

	public void setCacnDiHabitacion1(String cacnDiHabitacion1) {
		this.cacnDiHabitacion1 = cacnDiHabitacion1;
	}

	public String getCacnDiHabitacion2() {
		return cacnDiHabitacion2;
	}

	public void setCacnDiHabitacion2(String cacnDiHabitacion2) {
		this.cacnDiHabitacion2 = cacnDiHabitacion2;
	}

	public Date getCacnFeNacimiento() {
		return cacnFeNacimiento;
	}

	public void setCacnFeNacimiento(Date cacnFeNacimiento) {
		this.cacnFeNacimiento = cacnFeNacimiento;
	}

	public String getCacnInEmpleado() {
		return cacnInEmpleado;
	}

	public void setCacnInEmpleado(String cacnInEmpleado) {
		this.cacnInEmpleado = cacnInEmpleado;
	}

	public String getCacnInTratamientoEspecial() {
		return cacnInTratamientoEspecial;
	}

	public void setCacnInTratamientoEspecial(String cacnInTratamientoEspecial) {
		this.cacnInTratamientoEspecial = cacnInTratamientoEspecial;
	}

	public String getCacnInUsadoFinanciamiento() {
		return cacnInUsadoFinanciamiento;
	}

	public void setCacnInUsadoFinanciamiento(String cacnInUsadoFinanciamiento) {
		this.cacnInUsadoFinanciamiento = cacnInUsadoFinanciamiento;
	}

	public BigDecimal getCacnNacionalidadFatca() {
		return cacnNacionalidadFatca;
	}

	public void setCacnNacionalidadFatca(BigDecimal cacnNacionalidadFatca) {
		this.cacnNacionalidadFatca = cacnNacionalidadFatca;
	}

	public String getCacnNmApellidoRazon() {
		return cacnNmApellidoRazon;
	}

	public void setCacnNmApellidoRazon(String cacnNmApellidoRazon) {
		this.cacnNmApellidoRazon = cacnNmApellidoRazon;
	}

	public String getCacnNmPersonaNatural() {
		return cacnNmPersonaNatural;
	}

	public void setCacnNmPersonaNatural(String cacnNmPersonaNatural) {
		this.cacnNmPersonaNatural = cacnNmPersonaNatural;
	}

	public String getCacnNombre() {
		return cacnNombre;
	}

	public void setCacnNombre(String cacnNombre) {
		this.cacnNombre = cacnNombre;
	}

	public String getCacnNuApartadoCobro() {
		return cacnNuApartadoCobro;
	}

	public void setCacnNuApartadoCobro(String cacnNuApartadoCobro) {
		this.cacnNuApartadoCobro = cacnNuApartadoCobro;
	}

	public String getCacnNuApartadoHabitacion() {
		return cacnNuApartadoHabitacion;
	}

	public void setCacnNuApartadoHabitacion(String cacnNuApartadoHabitacion) {
		this.cacnNuApartadoHabitacion = cacnNuApartadoHabitacion;
	}

	public String getCacnNuCedulaAnterior() {
		return cacnNuCedulaAnterior;
	}

	public void setCacnNuCedulaAnterior(String cacnNuCedulaAnterior) {
		this.cacnNuCedulaAnterior = cacnNuCedulaAnterior;
	}

	public BigDecimal getCacnNuConsultas() {
		return cacnNuConsultas;
	}

	public void setCacnNuConsultas(BigDecimal cacnNuConsultas) {
		this.cacnNuConsultas = cacnNuConsultas;
	}

	public String getCacnNuEmpleadoFicha() {
		return cacnNuEmpleadoFicha;
	}

	public void setCacnNuEmpleadoFicha(String cacnNuEmpleadoFicha) {
		this.cacnNuEmpleadoFicha = cacnNuEmpleadoFicha;
	}

	public String getCacnNuFaxCobro() {
		return cacnNuFaxCobro;
	}

	public void setCacnNuFaxCobro(String cacnNuFaxCobro) {
		this.cacnNuFaxCobro = cacnNuFaxCobro;
	}

	public BigDecimal getCacnNuLlamadas() {
		return cacnNuLlamadas;
	}

	public void setCacnNuLlamadas(BigDecimal cacnNuLlamadas) {
		this.cacnNuLlamadas = cacnNuLlamadas;
	}

	public BigDecimal getCacnNuServicios() {
		return cacnNuServicios;
	}

	public void setCacnNuServicios(BigDecimal cacnNuServicios) {
		this.cacnNuServicios = cacnNuServicios;
	}

	public String getCacnNuTelefonoCobro() {
		return cacnNuTelefonoCobro;
	}

	public void setCacnNuTelefonoCobro(String cacnNuTelefonoCobro) {
		this.cacnNuTelefonoCobro = cacnNuTelefonoCobro;
	}

	public String getCacnNuTelefonoHabitacion() {
		return cacnNuTelefonoHabitacion;
	}

	public void setCacnNuTelefonoHabitacion(String cacnNuTelefonoHabitacion) {
		this.cacnNuTelefonoHabitacion = cacnNuTelefonoHabitacion;
	}

	public BigDecimal getCacnPaisFatca() {
		return cacnPaisFatca;
	}

	public void setCacnPaisFatca(BigDecimal cacnPaisFatca) {
		this.cacnPaisFatca = cacnPaisFatca;
	}

	public String getCacnRegistro1() {
		return cacnRegistro1;
	}

	public void setCacnRegistro1(String cacnRegistro1) {
		this.cacnRegistro1 = cacnRegistro1;
	}

	public String getCacnRegistro2() {
		return cacnRegistro2;
	}

	public void setCacnRegistro2(String cacnRegistro2) {
		this.cacnRegistro2 = cacnRegistro2;
	}

	public String getCacnRfc() {
		return cacnRfc;
	}

	public void setCacnRfc(String cacnRfc) {
		this.cacnRfc = cacnRfc;
	}

	public String getCacnStActuaNom() {
		return cacnStActuaNom;
	}

	public void setCacnStActuaNom(String cacnStActuaNom) {
		this.cacnStActuaNom = cacnStActuaNom;
	}

	public String getCacnStCliente() {
		return cacnStCliente;
	}

	public void setCacnStCliente(String cacnStCliente) {
		this.cacnStCliente = cacnStCliente;
	}

	public String getCacnTpCliente() {
		return cacnTpCliente;
	}

	public void setCacnTpCliente(String cacnTpCliente) {
		this.cacnTpCliente = cacnTpCliente;
	}

	public String getCacnZnPostalCobro() {
		return cacnZnPostalCobro;
	}

	public void setCacnZnPostalCobro(String cacnZnPostalCobro) {
		this.cacnZnPostalCobro = cacnZnPostalCobro;
	}

	public String getCacnZnPostalHabitacion() {
		return cacnZnPostalHabitacion;
	}

	public void setCacnZnPostalHabitacion(String cacnZnPostalHabitacion) {
		this.cacnZnPostalHabitacion = cacnZnPostalHabitacion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<Object> getListaComboContratantes() {
		return listaComboContratantes;
	}

	public void setListaComboContratantes(List<Object> listaComboContratantes) {
		this.listaComboContratantes = listaComboContratantes;
	}

	public List<BeanContratante> getListaContratantes() {
		return listaContratantes;
	}

	public void setListaContratantes(List<BeanContratante> listaContratantes) {
		this.listaContratantes = listaContratantes;
	}	
	public BeanContratante getCliente() {
		return cliente;
	}
	public void setCliente(BeanContratante cliente) {
		this.cliente = cliente;
	}
	public CartCliente getNewCliente() {
		return newCliente;
	}
	public void setNewCliente(CartCliente newCliente) {
		this.newCliente = newCliente;
	}
	public List<Object> getListaComboCiudades() {
		return listaComboCiudades;
	}
	public void setListaComboCiudades(List<Object> listaComboCiudades) {
		this.listaComboCiudades = listaComboCiudades;
	}
}
