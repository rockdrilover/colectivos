/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.richfaces.component.html.HtmlProgressBar;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csvreader.CsvReader;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCargaId;
import mx.com.santander.aseguradora.colectivos.model.database.Colectivos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.ProgressBar;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("session")
public class BeanCancelacionMasiva extends BeanBase{

	@Resource
	private ServicioProcesos servicioProcesos;
	@Resource
	private ServicioCarga servicioCarga;
	@Resource
	private ServicioParametros servicioParametro;
	
	private Log log = LogFactory.getLog(this.getClass());
	private Short inCanal;
	private Short inRamo;
	private Long inPoliza;
	private Integer inCausa;
	private Integer inIdVenta;
	
	private List<String> opciones;
	
	private Colectivos.OPC_CANACEL opc;
	private File archivo;
	private String nombreArchivo;
	private StringBuilder message;
	private String respuesta;
	private boolean habilitar;
	private boolean habValDev;

	private List<Object> listaEstatusCancelacion;
	private List<String> cifrasControl;
	
	private ProgressBar pb;
	private ExecutorService pool;

	private boolean chkValPmaDev;

	public BeanCancelacionMasiva() {
		// TODO Auto-generated constructor stub
		this.message = new StringBuilder();
		this.setListaEstatusCancelacion(new ArrayList<Object>());
		this.cifrasControl = new ArrayList<String>();
		this.opciones = new ArrayList<String>();
		this.opciones.clear();
		this.opciones.add("1");
		this.pb = new ProgressBar();
		this.pb.setProgressValue(0);
		this.pool = Executors.newFixedThreadPool(1);
		this.habValDev = false;
		this.chkValPmaDev = true;
		
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CANCELACION_MASIVA);
	}
	

	/**
	 * @return the servicioProcesos
	 */
	public ServicioProcesos getServicioProcesos() {
		return servicioProcesos;
	}

	/**
	 * @param servicioProcesos the servicioProcesos to set
	 */
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}

	/**
	 * @return the servicioCarga
	 */
	public ServicioCarga getServicioCarga() {
		return servicioCarga;
	}

	/**
	 * @param servicioCarga the servicioCarga to set
	 */
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	/**
	 * @return the servicioParametros
	 */
	public ServicioParametros getServicioParametro() {
		return servicioParametro;
	}


	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametro) {
		this.servicioParametro = servicioParametro;
	}


	/**
	 * @return the inCanal
	 */
	public Short getInCanal() {
		return inCanal;
	}

	/**
	 * @param inCanal the inCanal to set
	 */
	public void setInCanal(Short inCanal) {
		this.inCanal = inCanal;
	}

	/**
	 * @return the inRamo
	 */
	public Short getInRamo() {
		return inRamo;
	}

	/**
	 * @param inRamo the inRamo to set
	 */
	public void setInRamo(Short inRamo) {
		this.inRamo = inRamo;
	}

	/**
	 * @return the inPoliza
	 */
	public Long getInPoliza() {
		return inPoliza;
	}

	/**
	 * @param inPoliza the inPoliza to set
	 */
	public void setInPoliza(Long inPoliza) {
		this.inPoliza = inPoliza;
		if (this.inPoliza == 0 || this.inPoliza == -1){			
			habilitar = true;
			opciones.clear();
			opciones.add("1");
			opciones.add("3");
			this.habValDev = true;
		}else{
			habilitar = false;			
			opciones.clear();
			opciones.add("1");
			this.habValDev = false;
		}
	}

	/**
	 * @return the inCausa
	 */
	public Integer getInCausa() {
		return inCausa;
	}

	/**
	 * @param inCausa the inCausa to set
	 */
	public void setInCausa(Integer inCausa) {
		this.inCausa = inCausa;
	}

	/**
	 * @return the inIdVenta
	 */
	public Integer getInIdVenta() {
		return inIdVenta;
	}

	/**
	 * @param inIdVenta the inIdVenta to set
	 */
	public void setInIdVenta(Integer inIdVenta) {
		this.inIdVenta = inIdVenta;
	}

	/**
	 * @return the opcx
	 */
	public List<String> getOpciones() {
		return opciones;
	}

	/**
	 * @param opcx the opcx to set
	 */
	public void setOpciones(List<String> opciones) {
		this.opciones = opciones;
	}


	/**
	 * @return the archivo
	 */
	public File getArchivo() {
		return archivo;
	}

	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return the habilitar
	 */
	public Boolean getHabilitar() {
		return !this.habilitar;
	}
	
	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	/**
	 * @param listaEstatusCancelacion the listaEstatusCancelacion to set
	 */
	public void setListaEstatusCancelacion(List<Object> listaEstatusCancelacion) {
		this.listaEstatusCancelacion = listaEstatusCancelacion;
	}

	/**
	 * @return the listaEstatusCancelacion
	 */
	public List<Object> getListaEstatusCancelacion() {
		return listaEstatusCancelacion;
	}
	
	public List<String> getCifrasControl() {
		return cifrasControl;
	}

	/**
	 * @return the intBarra
	 */
	public Integer getProgressValue() {
		return pb.getProgressValue();
	}

	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressValue(Integer progressValue) {
		this.pb.setProgressValue(progressValue);
	}
	
	/**
	 * @return the intBarra
	 */
	public Boolean getProgressHabilitar() {
		return pb.isProgressHabilitar();
	}

	/**
	 * @param intBarra the intBarra to set
	 */
	public void setProgressHabilitar(Boolean progressHabilitar) {
		this.pb.setProgressHabilitar(progressHabilitar);
	}
	
	/**
	 * @return the progressBar
	 */
	public HtmlProgressBar getProgressBar() {
		return pb.getProgressBar();
	}

	/**
	 * @param progressBar the progressBar to set
	 */
	public void setProgressBar(HtmlProgressBar progressBar) {
		this.pb.setProgressBar(progressBar);
	}
	
    public void cancelacionMasivaH() {
		
		this.log.debug("Iniciando proceso de carga (Hilo)");
		
		this.pb.setProgressHabilitar(true);
		this.setProgressValue(100);
		
		Runnable hilo = new HiloProcesos(this,BeanNames.BEAN_CANCELACION_MASIVA);
		pool.execute(hilo);
	}
	
	/**
	 * 
	 */
	public String cancelacionMasiva(){
		
		List<Object> lista = null;
		StringBuffer codOpcion;
		String regresa;
		this.message.append("Iniciando proceso de cancelacion masiva");
		this.log.debug(message.toString());
		Utilerias.resetMessage(message);
		StringBuilder sbQuery;
		Parametros objParam = null;
		
		try {
			lista = new ArrayList<Object>();
			this.listaEstatusCancelacion.clear();
			codOpcion = new StringBuffer("");
			
			for(String x:opciones)codOpcion.append(x); 

			if(opciones.size() == 0) 
				opc=Colectivos.OPC_CANACEL.ERROR;
			else {
				if(codOpcion.indexOf("1") >- 1) {
					if(codOpcion.indexOf("2") >-1 ) {
						if(codOpcion.indexOf("3") >-1 ) {
							opc = Colectivos.OPC_CANACEL.TODAS;
						} else {
							opc = Colectivos.OPC_CANACEL.CAN_DEVO;
						}
					} else {
						opc = Colectivos.OPC_CANACEL.CANCELA;
					}
				} else {
					if(codOpcion.indexOf("2") >-1 ) { 
						if(codOpcion.indexOf("3") >- 1) {
							opc = Colectivos.OPC_CANACEL.DEV_COMP;
						} else {
							opc = Colectivos.OPC_CANACEL.DEVO;
						}
					} else {
						if(codOpcion.indexOf("3") >- 1) {
							opc = Colectivos.OPC_CANACEL.COMP;
						} else {
							opc=Colectivos.OPC_CANACEL.ERROR;
						}
					}
				}
			}
								
			if(this.inRamo   != null  &&
			   this.inPoliza != null  &&
			   this.inCausa  != null  &&
			   ((this.archivo == null && this.inPoliza == -1) ||
			    (this.archivo != null && this.inPoliza != -1)) &&
			   this.opc != Colectivos.OPC_CANACEL.ERROR){
				
				this.inCanal = 1;
				
				if(this.inPoliza != -1) {
					// Leemos archivo con las cancelaciones 
					lista = this.leerArchivoCancelacion(this.archivo, ",");
					// Guardamos cancelaciones en tabla temporal
					message.append("Iniciando proceso de carga....");
					Utilerias.resetMessage(message);
					this.servicioCarga.guardarObjetos(lista);
					message.append("El procedo de carga se realizo correctamente.");
					this.log.debug(message.toString());
					Utilerias.resetMessage(message);
				} else {
					this.inPoliza = 0L;
				}
				
				this.nombreArchivo = inPoliza == -1 ? Constantes.CONCILIACION_NOMBRE_ARCHIVO + "_Bajas" : this.archivo.getName().substring(this.archivo.getName().lastIndexOf("\\") + 1);
				
				sbQuery = new StringBuilder();
				sbQuery.append("AND P.copaDesParametro = 'DEVOLUCION' \n");
				sbQuery.append("AND P.copaIdParametro  = 'DIFERENCIA' \n");
				sbQuery.append("AND P.copaNvalor1      = ").append(this.inCanal).append(" \n");
				sbQuery.append("AND P.copaNvalor2      = ").append(this.inRamo).append(" \n");
				sbQuery.append("AND P.copaNvalor4      = ").append(this.inIdVenta).append(" \n");
				if(isChkValPmaDev() == false && (this.inIdVenta != 0 || this.inIdVenta != -1)) {
					objParam = (Parametros) this.servicioParametro.obtenerObjetos(sbQuery.toString()).get(0);
					objParam.setCopaNvalor5(Constantes.DEFAULT_INT);
					this.servicioParametro.actualizarObjeto(objParam);
				}
				
				message.append("Comenzando proceso cancelacion masiva  ramo:").append(this.inRamo.toString()).append(" poliza:").append(this.inPoliza.toString()).append(".");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
				this.servicioProcesos.cancelacionMasiva(this.inCanal, this.inRamo, this.inPoliza, this.inCausa, this.opc);
				
				if (this.inRamo == 9 && this.inIdVenta == 6) {
					enviarArchivoPampaCancelacion();
				}
				
				// Estatus cancelacion masiva				
				this.listaEstatusCancelacion = this.servicioCarga.estatusCancelacion(this.inCanal, this.inRamo, this.inPoliza, nombreArchivo);

				lista.clear();
				
				if(isChkValPmaDev() == false && (this.inIdVenta != 0 || this.inIdVenta != -1)) {
					objParam = (Parametros) this.servicioParametro.obtenerObjetos(sbQuery.toString()).get(0);
					objParam.setCopaNvalor5(1);
					this.servicioParametro.actualizarObjeto(objParam);
				}
				message.append("Proceso de cancelación finalizó, descargue el archivo de errores para revisar las inconsistencias.\n");				
				this.log.debug(message.toString());
				this.respuesta = this.message.toString();
				Utilerias.resetMessage(message);
				
				return NavigationResults.SUCCESS;
			} else {
				if (this.opc == Colectivos.OPC_CANACEL.ERROR) {
					message.append("Opciones de cancelación incorrectas. elija una");	
				} else { 
					message.append("El ramo, poliza o causa de anulación no pueden ser nulos.");
				}
				
				this.log.debug(message.toString());
				return NavigationResults.RETRY;
			}
		} catch (Exception e) {
			message.append("El proceso de cancelación masiva falló.");
			this.log.error(message.toString(), e);
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void listener(UploadEvent event) throws Exception{
		
		UploadItem item = event.getUploadItem();
		File archivoTemporal;
		String ruta = FacesUtils.getServletContext().getRealPath("/WEB-INF/cargas/");
		String nombreArchivo = null;
		String nombreRuta;
		
		
		
		if(item.isTempFile()){
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			//ystem.out.println("Uno");
		}
		else{
			
			Archivo file = new Archivo();
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);
			
			nombreRuta = file.getNombreArchivo() + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()).toString();
			
			/**
			 * Guardar el archivo en  carpeta del proyecto
			 */
			nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf("\\") + 1);
			
			archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
			FileUtil.write(archivoTemporal, item.getData());
			
			
			
			this.archivo = archivoTemporal;
		}
		
		this.message.append("Archivo cargado correctamente, puede continuar con el proceso de cancelación.");
		this.log.debug(this.message.toString());
		this.respuesta = this.message.toString();
		Utilerias.resetMessage(message);
		
	}
	
	/**
	 * 
	 * @return
	 */
	public String erroresCancelacionMasiva(){
		
		String rutaTemporal;
		String rutaReporte;
		FileInputStream input;
		byte[] bytes;
		int read = 0;
		
		Map<String, Object> inParams = new HashMap<String, Object>();
		
		this.inCanal = 1;
		
		if (this.archivo != null) this.nombreArchivo = this.archivo.getName(); else this.nombreArchivo = "*";	
		
		if(this.inCanal != null && this.inRamo != null && this.inPoliza != null && this.nombreArchivo != null){
		
			
			try {
				
				
				
				inParams.put("canal", this.inCanal.intValue());
				inParams.put("ramo", this.inRamo.intValue());
				inParams.put("poliza", this.inPoliza.intValue());
				inParams.put("idVenta",this.inIdVenta.intValue());
				inParams.put("archivo", this.nombreArchivo);
				
				
				rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ErroresCancelacion.csv");
				rutaReporte	= FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/ErroresCancelacion.jrxml");
				
				this.servicioCarga.reporteErrores(rutaTemporal, rutaReporte, inParams);
							
				File file = new File(rutaTemporal);
				
				FacesContext context = FacesContext.getCurrentInstance();
				
				if(!context.getResponseComplete()){
				
					input = new FileInputStream(file);
					bytes = new byte[1000];
					
					
					String contentType = "application/vnd.ms-excel";

					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					
					response.setContentType(contentType);
					
					DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
			    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + file.getName());
					
					ServletOutputStream out = response.getOutputStream();
					
					while((read = input.read(bytes))!= -1){
						
						out.write(bytes, 0 , read);
					}
					
					input.close();
					out.flush();
					out.close();
					file.delete();

					context.responseComplete();

				}
				
				// Borramos los datos de la precarga y carga.
				this.servicioCarga.borrarDatosTemporales(this.inCanal, this.inRamo, this.inPoliza, this.inIdVenta,this.nombreArchivo, "CAN");
				
				this.message.append("Archivo de errores descargado.");
				this.log.debug(message.toString());
				FacesUtils.addInfoMessage(message.toString());
				Utilerias.resetMessage(message);

				
				return NavigationResults.SUCCESS;
				
			} catch (Exception e) {
				// TODO: handle exception
				this.message.append("Error al generar el archivo de errores.");
				this.log.debug(message.toString());
				FacesUtils.addErrorMessage(message.toString());
				
				return NavigationResults.FAILURE;
			}			

		}
		else
		{
			this.message.append("El archivo sólo se puede descargar hasta que se haya realizado el proceso.");
			this.log.debug(message.toString());
			FacesUtils.addInfoMessage(message.toString());
			Utilerias.resetMessage(message);
			
			return NavigationResults.RETRY;
		}	
	}
	
	public List<Object> leerArchivoCancelacion(File file, String delimitador) {
		// TODO Auto-generated method stub

		CsvReader csv = null;
		FileReader fr;
		BeanPreCarga bean;
		PreCarga preCarga;
		PreCargaId id;
		List<Object> lista = null;
		String nombreArchivo = file.getName().substring(file.getName().lastIndexOf("\\") + 1);
		
		boolean hayCifrasContro = false;
		
		String[] headers = null;
		Map<String, String> mh;
				
		
						
		try 
		{
			this.message.append("Abriendo archivo de carga masiva");
			this.log.debug(message.toString());
			Utilerias.resetMessage(message);
			
			fr = new FileReader(file);
			csv = new CsvReader(fr, delimitador.charAt(0));

			if(csv.readHeaders())
			{
				headers = csv.getHeaders();
				
				mh = new HashMap<String, String>();
				
				for (String h: headers){
					mh.put(h.replaceAll(" ", "").toLowerCase(), h);
				}
				
				lista = new ArrayList<Object>();
				bean = new BeanPreCarga();			
				
				this.message.append("Leyendo datos  pre carga cancelacion masiva.");
				this.log.debug(message.toString());
				Utilerias.resetMessage(message);
								
				cifrasControl.clear();
				
				Parametros tasa = new Parametros();
				tasa.setCopaDesParametro("DEVOLUCION");
				tasa.setCopaIdParametro("TASA_AMORTIZA");
				
				String filtro = " AND P.copaDesParametro = 'DEVOLUCION' AND P.copaIdParametro = 'TASA_AMORTIZA' \n";
				
				Double tasaAmortiza = ((Parametros) this.servicioParametro.obtenerObjetos(filtro).get(0)).getCopaNvalor7();// .obtenerTasaAmortiza();
				
				
				while(csv.readRecord())
				{	
					if (csv.get(0).equals("*")) hayCifrasContro = true;
					
					if (!hayCifrasContro){
						if (!csv.get(0).trim().equals("")){

							id = new PreCargaId(this.inCanal, this.inRamo, this.inPoliza, csv.get(mh.get("identificador")), "CAN", csv.get(mh.get("sumaasegurada")));
							if (csv.get(mh.get("sumaasegurada")) == null || csv.get(mh.get("sumaasegurada")).length() <= 0 ) id.setCopcSumaAsegurada("0");
								
							bean.setId(id);
																	
							bean.setCopcCargada("9");  //para diferenciar de la emision.
							
							bean.setCopcFeIngreso(csv.get(mh.get("fechaingreso")));
							bean.setCopcFeStatus(csv.get(mh.get("fechabaja")));
							bean.setCopcNuCredito(csv.get(mh.get("prestamo")));
							bean.setCopcDato1(csv.get(mh.get("tasaamortizacion")));
														
							if (this.inPoliza == 0 && this.inIdVenta == 6){ 
								if (this.inRamo == 61)
									bean.setCopcPrima(csv.get(mh.get("mtodevueltovida")));
								if (this.inRamo == 9)
									bean.setCopcPrima(csv.get(mh.get("mtodevueltodesempleo")));
								
								bean.getId().setCopcSumaAsegurada(csv.get(mh.get("disposicion")));
								
							}else	bean.setCopcPrima(csv.get(mh.get("devolucion")));
							
							if (bean.getCopcPrima() != null && bean.getCopcPrima().trim().length() > 0 
									&& new Double(bean.getCopcPrima()) > 0 && 
									  (this.opc == Colectivos.OPC_CANACEL.CANCELA || this.opc == Colectivos.OPC_CANACEL.ERROR)){
								bean.setCopcPrima(null);
							}
							
							if (this.inIdVenta > 0 && bean.getCopcPrima() != null && 
									bean.getCopcPrima().length() > 0 && Double.parseDouble(bean.getCopcPrima()) == 0)
								bean.setCopcDato1(tasaAmortiza.toString());
							
							bean.setCopcDato11(nombreArchivo);
							bean.setCopcDato16(this.inIdVenta.toString());
							
							preCarga = (PreCarga) ConstruirObjeto.crearBean(PreCarga.class, bean);
							
							lista.add(preCarga);
						}
					}else{
						if (!csv.get(0).equals("*"))
							cifrasControl.add(csv.get(0)+": "+Double.parseDouble(csv.get(1)));
					}
					
				}
				
				csv.close();
				fr.close();
				file.delete();
			}
			else
			{
				this.message.append("El archivo CSV no tiene encabezados");
				this.log.debug(message.toString());
				
			}
			
		} 
		catch (IOException e) {
			// TODO: handle exception
			message.append("Error de lectura en el archivo");
			this.log.error(message.toString(), e);
			//FacesUtils.addErrorMessage(message.toString());
		}
		catch (Exception e) {
			// TODO: handle exception
			this.message.append("No se puede cargar la informacion del archivo.");
			this.log.error(message.toString(), e);
			//FacesUtils.addErrorMessage(message.toString());
		}
		return lista;
		
	}
	
	private void enviarArchivoPampaCancelacion() {
		List<Object> lstObjetosPampa = null;
		List<String> lstArchivos;
		Archivo objArchivo;
		String ruta;
		
		try {
			lstArchivos = new ArrayList<String>();
			lstObjetosPampa= servicioCarga.consultaDatosPampaCancelacion();
			ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS)+ File.separator;
			objArchivo = new Archivo(ruta, "PampaCan" + GestorFechas.formatDate(new Date(), "ddMMyyyy"), ".txt");
			objArchivo.copiarArchivo(new String[0], lstObjetosPampa, "");
			lstArchivos.add(objArchivo.getArchivo().getPath());
			
		    servicioCarga.updatePampaCancelacion();
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
	}
	
	public boolean isHabValDev() {
		return habValDev;
	}
	public void setHabValDev(boolean habValDev) {
		this.habValDev = habValDev;
	}
	/**
	 * Indica si selecciono Check
	 * @return
	 */
	public boolean isChkValPmaDev() {
		return chkValPmaDev;
	}
	/**
	 * Manda seleccionar Check
	 * @param chkValPmaDev
	 */
	public void setChkValPmaDev(boolean chkValPmaDev) {
		this.chkValPmaDev = chkValPmaDev;
	}
}
