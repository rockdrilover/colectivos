package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.AlternaEstatus;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosComponentesId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PlanId;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioColectivosComponentes;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class BeanComponentes {

	@Resource
	private ServicioColectivosComponentes servicioColectivosComponentes;
	@Resource
	private ServicioParametros servicioParametros;

	private Log log = LogFactory.getLog(this.getClass());
	private StringBuilder message;
	private String strRespuesta;

	// BEANS
	private PlanId idPlan;

	// -- COMPONENTES
	private ColectivosComponentesId id;
	private AlternaEstatus alternaEstatus;
	private byte coctSecNuComponente;
	private long coctPolNuComponente;
	private Date coctFeDesde;
	private Date coctFeHasta;
	private BigDecimal coctTaComponente;
	private Date coctFeBaja;
	private Long coctCampon1;
	private BigDecimal coctCampon2;
	private String coctCampov1;
	private String coctCampov2;
	private Date coctCampof1;
	private Date coctCampof2;

	
	private List<Object> listaComboComponentes;
	private List<ColectivosComponentes> listaComponentes;

	private List<Parametros> listaParamComponentes;

	private Integer idVenta;// idVenta
	private Integer plazo; // plazo
	private String descPlan;

	private String valComboCobertura;
	private String poliza;

	private BeanParametrizacion bean;
	private Parametros componentes;

	private ColectivosComponentes editar;

	private Boolean nuevo;
	
	public BeanComponentes() {

		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_COMPONENTES);

		this.idPlan = new PlanId();
		this.id = new ColectivosComponentesId();
		this.alternaEstatus = new AlternaEstatus();
		this.listaComboComponentes = new ArrayList<Object>();
		this.listaComponentes = new ArrayList<ColectivosComponentes>();
		this.listaParamComponentes = new ArrayList<Parametros>();
		this.message = new StringBuilder();

	}

	public ServicioColectivosComponentes getServicioColectivosComponentes() {
		return servicioColectivosComponentes;
	}

	public void setServicioColectivosComponentes(ServicioColectivosComponentes servicioColectivosComponentes) {
		this.servicioColectivosComponentes = servicioColectivosComponentes;
	}

	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public StringBuilder getMessage() {
		return message;
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public PlanId getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(PlanId idPlan) {
		this.idPlan = idPlan;
	}

	public ColectivosComponentesId getId() {
		return id;
	}

	public void setId(ColectivosComponentesId id) {
		this.id = id;
	}

	public AlternaEstatus getAlternaEstatus() {
		return alternaEstatus;
	}

	public void setAlternaEstatus(AlternaEstatus alternaEstatus) {
		this.alternaEstatus = alternaEstatus;
	}

	public byte getCoctSecNuComponente() {
		return coctSecNuComponente;
	}

	public void setCoctSecNuComponente(byte coctSecNuComponente) {
		this.coctSecNuComponente = coctSecNuComponente;
	}

	public long getCoctPolNuComponente() {
		return coctPolNuComponente;
	}

	public void setCoctPolNuComponente(long coctPolNuComponente) {
		this.coctPolNuComponente = coctPolNuComponente;
	}

	public Date getCoctFeDesde() {
		return coctFeDesde;
	}

	public void setCoctFeDesde(Date coctFeDesde) {
		this.coctFeDesde = coctFeDesde;
	}

	public Date getCoctFeHasta() {
		return coctFeHasta;
	}

	public void setCoctFeHasta(Date coctFeHasta) {
		this.coctFeHasta = coctFeHasta;
	}

	public BigDecimal getCoctTaComponente() {
		return coctTaComponente;
	}

	public void setCoctTaComponente(BigDecimal coctTaComponente) {
		this.coctTaComponente = coctTaComponente;
	}

	public Date getCoctFeBaja() {
		return coctFeBaja;
	}

	public void setCoctFeBaja(Date coctFeBaja) {
		this.coctFeBaja = coctFeBaja;
	}

	public Long getCoctCampon1() {
		return coctCampon1;
	}

	public void setCoctCampon1(Long coctCampon1) {
		this.coctCampon1 = coctCampon1;
	}

	public BigDecimal getCoctCampon2() {
		return coctCampon2;
	}

	public void setCoctCampon2(BigDecimal coctCampon2) {
		this.coctCampon2 = coctCampon2;
	}

	public String getCoctCampov1() {
		return coctCampov1;
	}

	public void setCoctCampov1(String coctCampov1) {
		this.coctCampov1 = coctCampov1;
	}

	public String getCoctCampov2() {
		return coctCampov2;
	}

	public void setCoctCampov2(String coctCampov2) {
		this.coctCampov2 = coctCampov2;
	}

	public Date getCoctCampof1() {
		return coctCampof1;
	}

	public void setCoctCampof1(Date coctCampof1) {
		this.coctCampof1 = coctCampof1;
	}

	public Date getCoctCampof2() {
		return coctCampof2;
	}

	public void setCoctCampof2(Date coctCampof2) {
		this.coctCampof2 = coctCampof2;
	}

	public List<Object> getListaComboComponentes() {
		return listaComboComponentes;
	}

	public void setListaComboComponentes(List<Object> listaComboComponentes) {
		this.listaComboComponentes = listaComboComponentes;
	}

	public List<ColectivosComponentes> getListaComponentes() {
		return listaComponentes;
	}

	public void setListaComponentes(List<ColectivosComponentes> listaComponentes) {
		this.listaComponentes = listaComponentes;
	}

	public Integer getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}

	public Integer getPlazo() {
		return plazo;
	}

	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}

	public String getDescPlan() {
		return descPlan;
	}

	public void setDescPlan(String descPlan) {
		this.descPlan = descPlan;
	}

	public String getValComboCobertura() {
		return valComboCobertura;
	}

	public void setValComboCobertura(String valComboCobertura) {
		this.valComboCobertura = valComboCobertura;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public void setListaParamComponentes(List<Parametros> listaParamComponentes) {
		this.listaParamComponentes = listaParamComponentes;
	}

	public List<Parametros> getListaParamComponentes() {
		return listaParamComponentes;
	}

	public BeanParametrizacion getBean() {
		return bean;
	}

	public void setBean(BeanParametrizacion bean) {
		this.bean = bean;
	}

	public void setComponentes(Parametros componentes) {
		this.componentes = componentes;
	}

	public Parametros getComponentes() {
		return componentes;
	}

	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}

	public String getStrRespuesta() {
		return strRespuesta;
	}

	// ACCIONES
	public String consultaComponentes() {

		StringBuilder filtro = new StringBuilder(100);
		Long poliza;

		List<BeanCdComponente> componentes = new ArrayList<BeanCdComponente>();

		componentes.add(new BeanCdComponente(Integer.parseInt("1"), "DPO"));
		componentes.add(new BeanCdComponente(Integer.parseInt("2"), "RFI"));
		componentes.add(new BeanCdComponente(Integer.parseInt("3"), "IVA"));
		componentes.add(new BeanCdComponente(Integer.parseInt("4"), "COM"));

		getListaComboComponentes().clear();

		// Si es prima unica construye la poliza, de lo contrario utiliza el parametro
		// del formulario
		if (getPoliza() != null && getPoliza().equals("0")) {
			poliza = Long.parseLong(getIdVenta() + "" + getPlazo() + "" + getId().getCoctCapuCdProducto());
		} else {
			poliza = Long.parseLong(getPoliza());
			if (getId().getCoctCarpCdRamo() == 57 || getId().getCoctCarpCdRamo() == 58) {
				poliza = Long.valueOf(Byte.toString(getId().getCoctCarpCdRamo()));
			}
		}

		getId().setCoctCapoNuPoliza(poliza);

		try {

			filtro.append(" and P.id.coctCasuCdSucursal = 1");
			filtro.append("	and P.id.coctCarpCdRamo 	= ").append(getId().getCoctCarpCdRamo());
			filtro.append("	and P.id.coctCapoNuPoliza 	= ").append(getId().getCoctCapoNuPoliza());
			filtro.append("	and P.id.coctCapuCdProducto = ").append(getId().getCoctCapuCdProducto());
			filtro.append("	and P.id.coctCapbCdPlan 	= ").append(getId().getCoctCapbCdPlan());
			filtro.append("	and P.id.coctEstatus 		= 20 ");
			filtro.append(" ");

			List<ColectivosComponentes> listaColectivosComponentes = this.servicioColectivosComponentes
					.obtenerObjetos(filtro.toString());

			for (ColectivosComponentes comp : listaColectivosComponentes) {

				for (int i = 0; i < componentes.size(); i++) {
					if (componentes.get(i).getComponente().equals(comp.getId().getCoctCappCdComponente())) {
						componentes.remove(i);
					}
				}

			}

			for (BeanCdComponente beanComp : componentes) {
				this.listaComboComponentes.add(new SelectItem(beanComp.getSecuencia() + " " + beanComp.getComponente(),
						beanComp.getComponente()));
			}

			setListaComponentes(listaColectivosComponentes);

			return NavigationResults.SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());

			return NavigationResults.FAILURE;
		}

	}

	public String guardaComponentes() {

		String cdCompo = getValComboCobertura().substring(2);
		String siguiente = getValComboCobertura().substring(0, 1);

		setCoctSecNuComponente(Byte.parseByte(siguiente));
		getId().setCoctCappCdComponente(cdCompo);
		getId().setCoctCerNuComponente(1);

		getId().setCoctCasuCdSucursal(Short.parseShort("1"));
		getId().setCoctCaceNuCertificado(0);
		getId().setCoctEstatus(Byte.parseByte("20"));

		try {

			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			ColectivosComponentes componentes = (ColectivosComponentes) ConstruirObjeto
					.crearObjeto(ColectivosComponentes.class, this);

			this.servicioColectivosComponentes.guardarObjeto(componentes);
			this.consultaComponentes();

			setCoctTaComponente(null);

			return NavigationResults.SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());

			return NavigationResults.FAILURE;
		}

	}

	// LPV 05/05/2015
	public String consulta() {

		List<Parametros> tempCompo;

		ELContext elContext = FacesContext.getCurrentInstance().getELContext();

		BeanParametrizacion beanParametrizacion = (BeanParametrizacion) FacesContext.getCurrentInstance()
				.getApplication().getELResolver().getValue(elContext, null, "beanParametrizacion");

		consultaParamComponentes();
		this.nuevo = true;
		try {

			beanParametrizacion.consultaColectivosComponentes();

			List<ColectivosComponentes> listaColectivosComponentes = beanParametrizacion.getListaColComponentes();

			for (ColectivosComponentes comp : listaColectivosComponentes) {

				tempCompo = new ArrayList<Parametros>();

				for (int i = 0; i < getListaParamComponentes().size(); i++) {

					if (!getListaParamComponentes().get(i).getCopaVvalor2()
							.equals(comp.getId().getCoctCappCdComponente())) {
						tempCompo.add(getListaParamComponentes().get(i));
					}
				}

				getListaParamComponentes().clear();
				setListaParamComponentes(tempCompo);
			}

			return NavigationResults.SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			Utilerias.resetMessage(message);
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());

			return NavigationResults.FAILURE;
		}

	}

	public String consultaParamComponentes() {

		StringBuilder filtro = new StringBuilder();

		getListaParamComponentes().clear();

		try {

			filtro.append("  and P.copaDesParametro = 'CATALOGO'");
			filtro.append("  and P.copaIdParametro  = 'CATCOMPONENTES'");

			List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());

			setListaParamComponentes(lista);

			return NavigationResults.SUCCESS;

		} catch (Exception e) {

			e.printStackTrace();
			Utilerias.resetMessage(this.message);
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());

			return NavigationResults.FAILURE;

		}
	}

	public String asociar() {

		try {

			if (getComponentes().getCopaFvalor1() == null) {
				FacesUtils.addErrorMessage("FECHA INICIAL: Valor requerido.");
				return NavigationResults.FAILURE;
			}

			if (GestorFechas.comparaFechas(getComponentes().getCopaFvalor1(),
					getComponentes().getCopaFvalor2()) == -1) {
				FacesUtils.addErrorMessage("La Fecha Hasta no puede ser MENOR a la Fecha Desde");
				return NavigationResults.FAILURE;
			}

			getId().setCoctCasuCdSucursal(Short.parseShort("1"));
			getId().setCoctCarpCdRamo(getBean().getRamo().getCarpCdRamo().byteValue());
			getId().setCoctCapoNuPoliza(getBean().getPolizaUnica());
			getId().setCoctCaceNuCertificado(0);
			getId().setCoctCappCdComponente(getComponentes().getCopaVvalor2());
			getId().setCoctCerNuComponente(1);
			getId().setCoctCapuCdProducto(getBean().getId().getAlprCdProducto());
			getId().setCoctCapbCdPlan(getBean().getIdPlan().getAlplCdPlan());
			setCoctSecNuComponente(getComponentes().getCopaNvalor2().byteValue());
			setCoctPolNuComponente(1);
			setCoctFeDesde(getComponentes().getCopaFvalor1());
			setCoctFeHasta(getComponentes().getCopaFvalor2());
			setCoctTaComponente(new BigDecimal(getComponentes().getCopaVvalor6()));
			getId().setCoctEstatus(Byte.parseByte("20"));
			/**
			 * Cambio 27/09/2017	
			 */
		
			if (nuevo) {
				this.coctCampon1 = new Long(0);
				this.coctCampov1 = "0";
				this.coctCampov2 = "2";
			}
			/**
			 * Cambio 27/09/2017	
			 */
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			
			ColectivosComponentes componentes = (ColectivosComponentes) ConstruirObjeto
					.crearObjeto(ColectivosComponentes.class, this);
			this.servicioColectivosComponentes.borrarObjeto(componentes);
			
			componentes = (ColectivosComponentes) ConstruirObjeto.crearObjeto(ColectivosComponentes.class, this);
			this.servicioColectivosComponentes.guardarObjeto(componentes);

			/**
			 * Valida tipo de comision.
			 */

			getBean().actualizaTipoComision();

			/**
			 * Fin valida tipo comision.
			 */
			consulta();

			setStrRespuesta("NOTA: Recuerde que es necesario asociar TODOS los componentes");
			getBean().setStrRespuesta("SE ASOCIO CON EXITO EL REGISTRO");

			return NavigationResults.SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			getBean().setStrRespuesta("Error al asociar, por favor intente mas tarde");
			Utilerias.resetMessage(this.message);
			message.append("Error Interno: No se pueden asociar los componentes.");
			this.log.error(message.toString(), e);

			FacesUtils.addErrorMessage(message.toString());

			return NavigationResults.FAILURE;
		}

	}

	// Vector - CJPV 27/09/2017
	public String consultaEdicion() {

		List<Parametros> tempCompo;

		ELContext elContext = FacesContext.getCurrentInstance().getELContext();

		BeanParametrizacion beanParametrizacion = (BeanParametrizacion) FacesContext.getCurrentInstance()
				.getApplication().getELResolver().getValue(elContext, null, "beanParametrizacion");

		consultaParamComponentesEditar();
		
		this.nuevo = false;

		try {

			beanParametrizacion.consultaColectivosComponentes();

			List<ColectivosComponentes> listaColectivosComponentes = beanParametrizacion.getListaColComponentes();

			tempCompo = new ArrayList<Parametros>();
			for (ColectivosComponentes comp : listaColectivosComponentes) {

				for (int i = 0; i < getListaParamComponentes().size(); i++) {

					if (getListaParamComponentes().get(i).getCopaVvalor2()
							.equals(comp.getId().getCoctCappCdComponente())) {
						Parametros padd = getListaParamComponentes().get(i);
						// padd.setCopaVvalor6(comp.getCoctTaComponente().toString());
						
						padd.setCopaFvalor1(comp.getCoctFeDesde());
						padd.setCopaFvalor2(comp.getCoctFeHasta());
						this.coctCampon1 = comp.getCoctCampon1();
						this.coctCampov1 = comp.getCoctCampov1();
						this.coctCampov2 = comp.getCoctCampov2();
						
						// padd.setCopaNvalor1(copaNvalor1);
						tempCompo.add(padd);
					}
				}

			}
			getListaParamComponentes().clear();
			setListaParamComponentes(tempCompo);
			return NavigationResults.SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			Utilerias.resetMessage(message);
			message.append("No se pueden cargar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());

			return NavigationResults.FAILURE;
		}

	}

	public String consultaParamComponentesEditar() {

		StringBuilder filtro = new StringBuilder();

		getListaParamComponentes().clear();

		try {

			filtro.append("  and P.copaDesParametro = 'CATALOGO'");
			filtro.append("  and P.copaIdParametro  = 'CATCOMPONENTES'");
			filtro.append(" and P.copaVvalor2  = '");
			filtro.append(editar.getId().getCoctCappCdComponente());
			filtro.append("'");
			List<Parametros> lista = this.servicioParametros.obtenerObjetos(filtro.toString());

			setListaParamComponentes(lista);

			return NavigationResults.SUCCESS;

		} catch (Exception e) {

			e.printStackTrace();
			Utilerias.resetMessage(this.message);
			this.message.append("No se pueden recuperar los parametros.");
			this.log.error(message.toString(), e);
			FacesUtils.addErrorMessage(message.toString());

			return NavigationResults.FAILURE;

		}
	}

	// FIN Vector - CJPV 27/09/2017

	/**
	 * @return the editar
	 */
	public ColectivosComponentes getEditar() {
		return editar;
	}

	/**
	 * @param editar
	 *            the editar to set
	 */
	public void setEditar(ColectivosComponentes editar) {
		this.editar = editar;
	}

	public Boolean getNuevo() {
		return nuevo;
	}

	public void setNuevo(Boolean nuevo) {
		this.nuevo = nuevo;
	}

	
	
}
