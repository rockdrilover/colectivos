/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.FacturacionId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Recibo;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;

/**
 * @author Sergio Plata
 *
 */
@Controller
@Scope("request")
public class BeanFacturacion extends BeanBase  {
	
	private ServicioProcesos servicioProceso;
	private Log log = LogFactory.getLog(this.getClass());
	private boolean chk;
	
	private FacturacionId id;
	private Certificado Certificado;
	private Double cofaNuReciboCol;
	private Double cofaNuReciboFiscal;
	private Short cofaCuotaRecibo;
	private String cofaStRecibo;
	private BigDecimal cofaMtRecibo;
	private BigDecimal cofaTotalCertificados;
	private BigDecimal cofaMtTotSua;
	private BigDecimal cofaMtTotPma;
	private BigDecimal cofaMtTotIva;
	private BigDecimal cofaMtTotDpo;
	private BigDecimal cofaMtTotRfi;
	private BigDecimal cofaMtTotCom;
	private BigDecimal cofaMtTotCom1;
	private Date cofaFeFacturacion;
	private String cofaCdUsuario;
	private String cofaCdNacionalidad;
	private String cofaNuCedulaRif;
	private String cofaNuCuenta;
	private String cofaNuCuentaDep;
	private String cofaNuTrasfer;
	private Long cofaCampon1;
	private Long cofaCampon2;
	private Long cofaCampon3;
	private Long cofaCampon4;
	private BigDecimal cofaCampon5;
	private String cofaCampov1;
	private String cofaCampov2;
	private String cofaCampov3;
	private Date cofaCampof2;
	private Date cofaCampof3;
	private	Recibo	recibo;
	private boolean chkUpdate;
	
	/**
	 * resultado timbrado prefactura
	 */
	private String resTimbrado;
	
	//
	@Override
	protected void init() {
		// TODO Auto-generated method stub		 
		super.init();
		chk = false;
	}
	
	/**
	 * @return the servicioProceso
	 */
	public ServicioProcesos getServicioProceso() {
		return servicioProceso;
	}
	/**
	 * @param servicioProceso the servicioProceso to set
	 */
	public void setServicioProceso(ServicioProcesos servicioProceso) {
		this.servicioProceso = servicioProceso;
	}
	/**
	 * @return the chk
	 */
	public boolean isChk() {
		return chk;
	}
	/**
	 * @param chk the chk to set
	 */
	public void setChk(boolean chk) {
		this.chk = chk;
	}
	
	public boolean getChkUpdate() {
		return chkUpdate;
	}
	public void setChkUpdate(boolean chkUpdate) {
		this.chkUpdate = chkUpdate;
	}
	/**
	 * @return the id
	 */
	public FacturacionId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(FacturacionId id) {
		this.id = id;
	}
	/**
	 * @return the certificado
	 */
	public Certificado getCertificado() {
		return Certificado;
	}
	/**
	 * @param certificado the certificado to set
	 */
	public void setCertificado(Certificado certificado) {
		Certificado = certificado;
	}
	
	public Double getCofaNuReciboCol() {
		return cofaNuReciboCol;
	}

	public void setCofaNuReciboCol(Double cofaNuReciboCol) {
		this.cofaNuReciboCol = cofaNuReciboCol;
	}
	
	/**
	 * @return the cofaNuReciboFiscal
	 */
	public Double getCofaNuReciboFiscal() {
		return cofaNuReciboFiscal;
	}
	/**
	 * @param cofaNuReciboFiscal the cofaNuReciboFiscal to set
	 */
	public void setCofaNuReciboFiscal(Double cofaNuReciboFiscal) {
		this.cofaNuReciboFiscal = cofaNuReciboFiscal;
	}
	/**
	 * @return the cofaCuotaRecibo
	 */
	public Short getCofaCuotaRecibo() {
		return cofaCuotaRecibo;
	}
	/**
	 * @param cofaCuotaRecibo the cofaCuotaRecibo to set
	 */
	public void setCofaCuotaRecibo(Short cofaCuotaRecibo) {
		this.cofaCuotaRecibo = cofaCuotaRecibo;
	}
	/**
	 * @return the cofaStRecibo
	 */
	public String getCofaStRecibo() {
		return cofaStRecibo;
	}
	/**
	 * @param cofaStRecibo the cofaStRecibo to set
	 */
	public void setCofaStRecibo(String cofaStRecibo) {
		this.cofaStRecibo = cofaStRecibo;
	}
	/**
	 * @return the cofaMtRecibo
	 */
	public BigDecimal getCofaMtRecibo() {
		return cofaMtRecibo;
	}
	/**
	 * @param cofaMtRecibo the cofaMtRecibo to set
	 */
	public void setCofaMtRecibo(BigDecimal cofaMtRecibo) {
		this.cofaMtRecibo = cofaMtRecibo;
	}
	/**
	 * @return the cofaTotalCertificados
	 */
	public BigDecimal getCofaTotalCertificados() {
		return cofaTotalCertificados;
	}
	/**
	 * @param cofaTotalCertificados the cofaTotalCertificados to set
	 */
	public void setCofaTotalCertificados(BigDecimal cofaTotalCertificados) {
		this.cofaTotalCertificados = cofaTotalCertificados;
	}
	/**
	 * @return the cofaMtTotSua
	 */
	public BigDecimal getCofaMtTotSua() {
		return cofaMtTotSua;
	}
	/**
	 * @param cofaMtTotSua the cofaMtTotSua to set
	 */
	public void setCofaMtTotSua(BigDecimal cofaMtTotSua) {
		this.cofaMtTotSua = cofaMtTotSua;
	}
	/**
	 * @return the cofaMtTotPma
	 */
	public BigDecimal getCofaMtTotPma() {
		return cofaMtTotPma;
	}
	/**
	 * @param cofaMtTotPma the cofaMtTotPma to set
	 */
	public void setCofaMtTotPma(BigDecimal cofaMtTotPma) {
		this.cofaMtTotPma = cofaMtTotPma;
	}
	/**
	 * @return the cofaMtTotIva
	 */
	public BigDecimal getCofaMtTotIva() {
		return cofaMtTotIva;
	}
	/**
	 * @param cofaMtTotIva the cofaMtTotIva to set
	 */
	public void setCofaMtTotIva(BigDecimal cofaMtTotIva) {
		this.cofaMtTotIva = cofaMtTotIva;
	}
	/**
	 * @return the cofaMtTotDpo
	 */
	public BigDecimal getCofaMtTotDpo() {
		return cofaMtTotDpo;
	}
	/**
	 * @param cofaMtTotDpo the cofaMtTotDpo to set
	 */
	public void setCofaMtTotDpo(BigDecimal cofaMtTotDpo) {
		this.cofaMtTotDpo = cofaMtTotDpo;
	}
	/**
	 * @return the cofaMtTotRfi
	 */
	public BigDecimal getCofaMtTotRfi() {
		return cofaMtTotRfi;
	}
	/**
	 * @param cofaMtTotRfi the cofaMtTotRfi to set
	 */
	public void setCofaMtTotRfi(BigDecimal cofaMtTotRfi) {
		this.cofaMtTotRfi = cofaMtTotRfi;
	}
	/**
	 * @return the cofaMtTotCom
	 */
	public BigDecimal getCofaMtTotCom() {
		return cofaMtTotCom;
	}
	/**
	 * @param cofaMtTotCom the cofaMtTotCom to set
	 */
	public void setCofaMtTotCom(BigDecimal cofaMtTotCom) {
		this.cofaMtTotCom = cofaMtTotCom;
	}
	/**
	 * @return the cofaMtTotCom1
	 */
	public BigDecimal getCofaMtTotCom1() {
		return cofaMtTotCom1;
	}
	/**
	 * @param cofaMtTotCom1 the cofaMtTotCom1 to set
	 */
	public void setCofaMtTotCom1(BigDecimal cofaMtTotCom1) {
		this.cofaMtTotCom1 = cofaMtTotCom1;
	}
	/**
	 * @return the cofaFeFacturacion
	 */
	public Date getCofaFeFacturacion() {
		return cofaFeFacturacion;
	}
	/**
	 * @param cofaFeFacturacion the cofaFeFacturacion to set
	 */
	public void setCofaFeFacturacion(Date cofaFeFacturacion) {
		this.cofaFeFacturacion = cofaFeFacturacion;
	}

	public Date getCofaCampof2() {
		return cofaCampof2;
	}

	public void setCofaCampof2(Date cofaCampof2) {
		this.cofaCampof2 = cofaCampof2;
	}

	public Date getCofaCampof3() {
		return cofaCampof3;
	}

	public void setCofaCampof3(Date cofaCampof3) {
	    this.cofaCampof3 = cofaCampof3;
	}
	/**
	 * @return the cofaCdUsuario
	 */
	public String getCofaCdUsuario() {
		return cofaCdUsuario;
	}
	/**
	 * @param cofaCdUsuario the cofaCdUsuario to set
	 */
	public void setCofaCdUsuario(String cofaCdUsuario) {
		this.cofaCdUsuario = cofaCdUsuario;
	}
	/**
	 * @return the cofaCdNacionalidad
	 */
	public String getCofaCdNacionalidad() {
		return cofaCdNacionalidad;
	}
	/**
	 * @param cofaCdNacionalidad the cofaCdNacionalidad to set
	 */
	public void setCofaCdNacionalidad(String cofaCdNacionalidad) {
		this.cofaCdNacionalidad = cofaCdNacionalidad;
	}
	/**
	 * @return the cofaNuCedulaRif
	 */
	public String getCofaNuCedulaRif() {
		return cofaNuCedulaRif;
	}
	/**
	 * @param cofaNuCedulaRif the cofaNuCedulaRif to set
	 */
	public void setCofaNuCedulaRif(String cofaNuCedulaRif) {
		this.cofaNuCedulaRif = cofaNuCedulaRif;
	}
	/**
	 * @return the cofaNuCuenta
	 */
	public String getCofaNuCuenta() {
		return cofaNuCuenta;
	}
	/**
	 * @param cofaNuCuenta the cofaNuCuenta to set
	 */
	public void setCofaNuCuenta(String cofaNuCuenta) {
		this.cofaNuCuenta = cofaNuCuenta;
	}
	/**
	 * @return the cofaNuCuentaDep
	 */
	public String getCofaNuCuentaDep() {
		return cofaNuCuentaDep;
	}
	/**
	 * @param cofaNuCuentaDep the cofaNuCuentaDep to set
	 */
	public void setCofaNuCuentaDep(String cofaNuCuentaDep) {
		this.cofaNuCuentaDep = cofaNuCuentaDep;
	}
	/**
	 * @return the cofaNuTrasfer
	 */
	public String getCofaNuTrasfer() {
		return cofaNuTrasfer;
	}
	/**
	 * @param cofaNuTrasfer the cofaNuTrasfer to set
	 */
	public void setCofaNuTrasfer(String cofaNuTrasfer) {
		this.cofaNuTrasfer = cofaNuTrasfer;
	}
	/**
	 * @return the cofaCampon1
	 */
	public Long getCofaCampon1() {
		return cofaCampon1;
	}
	/**
	 * @param cofaCampon1 the cofaCampon1 to set
	 */
	public void setCofaCampon1(Long cofaCampon1) {
		this.cofaCampon1 = cofaCampon1;
	}
	/**
	 * @return the cofaCampon2
	 */
	public Long getCofaCampon2() {
		return cofaCampon2;
	}
	/**
	 * @param cofaCampon2 the cofaCampon2 to set
	 */
	public void setCofaCampon2(Long cofaCampon2) {
		this.cofaCampon2 = cofaCampon2;
	}
	/**
	 * @return the cofaCampon3
	 */
	public Long getCofaCampon3() {
		return cofaCampon3;
	}
	/**
	 * @param cofaCampon3 the cofaCampon3 to set
	 */
	public void setCofaCampon3(Long cofaCampon3) {
		this.cofaCampon3 = cofaCampon3;
	}
	/**
	 * @return the cofaCampon4
	 */
	public Long getCofaCampon4() {
		return cofaCampon4;
	}
	/**
	 * @param cofaCampon4 the cofaCampon4 to set
	 */
	public void setCofaCampon4(Long cofaCampon4) {
		this.cofaCampon4 = cofaCampon4;
	}
	/**
	 * @return the cofaCampon5
	 */
	public BigDecimal getCofaCampon5() {
		return cofaCampon5;
	}
	/**
	 * @param cofaCampon5 the cofaCampon5 to set
	 */
	public void setCofaCampon5(BigDecimal cofaCampon5) {
		this.cofaCampon5 = cofaCampon5;
	}
	/**
	 * @return the cofaCampov1
	 */
	public String getCofaCampov1() {
		return cofaCampov1;
	}
	/**
	 * @param cofaCampov1 the cofaCampov1 to set
	 */
	public void setCofaCampov1(String cofaCampov1) {
		this.cofaCampov1 = cofaCampov1;
	}
	/**
	 * @return the cofaCampov2
	 */
	public String getCofaCampov2() {
		return cofaCampov2;
	}
	/**
	 * @param cofaCampov2 the cofaCampov2 to set
	 */
	public void setCofaCampov2(String cofaCampov2) {
		this.cofaCampov2 = cofaCampov2;
	}
	/**
	 * @return the cofaCampov3
	 */
	public String getCofaCampov3() {
		return cofaCampov3;
	}
	/**
	 * @param cofaCampov3 the cofaCampov3 to set
	 */
	public void setCofaCampov3(String cofaCampov3) {
		this.cofaCampov3 = cofaCampov3;
	}
	public void setRecibo(Recibo recibo) {
		this.recibo = recibo;
	}
	public Recibo getRecibo() {
		return recibo;
	}

	// constructor
	
	public BeanFacturacion() {
		new StringBuilder(100);
		id= new FacturacionId();
		this.log.info("BeanFacturacion creado.");

	}

	/**
	 * @return the resTimbrado
	 */
	public String getResTimbrado() {
		return resTimbrado;
	}

	/**
	 * @param resTimbrado the resTimbrado to set
	 */
	public void setResTimbrado(String resTimbrado) {
		this.resTimbrado = resTimbrado;
	}
	
	
	
}