/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.view.bean;

import javax.faces.FacesException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Sergio Plata
 *
 */
public class BeanAplicacion extends BeanBase {

	private Log log = LogFactory.getLog(this.getClass());
	private String message = null;
	private int certificadosPorPagina;
	
	public BeanAplicacion() {
		// TODO Auto-generated constructor stub
		this.log.debug("BeanAplicacion creado");
	}
	
	/**
	 * 
	 */
	protected void init(){
		
		try {
			
			this.log.debug("Inicializando BeanAplicacion");
			
		} catch (Exception e) {
			// TODO: handle exception
			message = "No se puede inicializar el BeanAplicacion " + e.toString();
			this.log.error(message, e);
			throw new FacesException(message, e);
		}
		
		this.log.debug("BeanAplicacion fue inicializado");
	}

	/**
	 * @param certificadosPorPagina the certificadosPorPagina to set
	 */
	public void setCertificadosPorPagina(int certificadosPorPagina) {
		this.certificadosPorPagina = certificadosPorPagina;
	}

	/**
	 * @return the certificadosPorPagina
	 */
	public int getCertificadosPorPagina() {
		return this.certificadosPorPagina;
	}
}
