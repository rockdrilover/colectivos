package mx.com.santander.aseguradora.colectivos.view.bean;

import java.util.List;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BeanMesFiscal {
	
	private Log log = LogFactory.getLog(this.getClass());
	private String mes;
	private Integer plazo;
	private Double primaEmitidaNeta;
	private Double primaBajas;
	private Double primaTotal;
	private List<BeanMesFiscal> bajas;
	
	
	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public Integer getPlazo() {
		return plazo;
	}

	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}

	public Double getPrimaEmitidaNeta() {
		return primaEmitidaNeta;
	}

	public void setPrimaEmitidaNeta(Double primaEmitidaNeta) {
		this.primaEmitidaNeta = primaEmitidaNeta;
	}

	public Double getPrimaBajas() {
		return primaBajas;
	}

	public void setPrimaBajas(Double primaBajas) {
		this.primaBajas = primaBajas;
	}

	public Double getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}

	public List<BeanMesFiscal> getBajas() {
		return bajas;
	}

	public void setBajas(List<BeanMesFiscal> bajas) {
		this.bajas = bajas;
	}
	
	public BeanMesFiscal(){
		setMes(Constantes.DEFAULT_SIN_DATO);
		setPlazo(Constantes.DEFAULT_INT);
		setPrimaBajas(Constantes.DEFAULT_DOUBLE);
		setPrimaEmitidaNeta(Constantes.DEFAULT_DOUBLE);
		setPrimaTotal(Constantes.DEFAULT_DOUBLE);
		this.log.debug("BeanMesFiscal creado");
	}
}
