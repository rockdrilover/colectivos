package mx.com.santander.aseguradora.colectivos.view.builder;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * @author Sergio Plata
 *
 */
public class ConstruirObjeto{


	private static Log log = LogFactory.getLog(ConstruirObjeto.class);
	private static StringBuilder message = new StringBuilder();
	
	/**
	 * 
	 * @param <T>
	 * @param bean
	 * @param objeto
	 * @throws Excepciones
	 */
	public static <T> void  poblarBean(T bean, T objeto) throws Excepciones{
		
		try {
			BeanUtils.copyProperties(bean, objeto);
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede cargar el bean.").append(bean.getClass());
			log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @param bean
	 * @return
	 * @throws Excepciones
	 */
	@SuppressWarnings("unchecked")
	public static <T> T crearObjeto(T objeto, T bean)throws Excepciones{
		
		T obj = nuevaInstancia((Class<T>) objeto);
		
		try {
			
			BeanUtils.copyProperties(obj, bean);
			
			return obj;
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede crear el objeto:").append(objeto.getClass().getName());
			log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	/**
	 * 
	 * @param <T>
	 * @param bean
	 * @param objeto
	 * @return
	 * @throws Excepciones
	 */
	@SuppressWarnings("unchecked")
	public static <T> T crearBean(T bean, T objeto)throws Excepciones{
		
		T newBean = nuevaInstancia((Class<T>) bean);
		
		try {
			
			BeanUtils.copyProperties(newBean, objeto);
			
			return (T) newBean;
			
		} catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede crear el bean:").append(bean.getClass().getName());
			log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
	}
	/**
	 * 
	 * @param <T>
	 * @param objeto
	 * @return
	 * @throws Excepciones
	 */
	@SuppressWarnings("unchecked")
	private static <T> T nuevaInstancia(Class<T> objeto) throws Excepciones{
		
		try {
			
			//System.out.println(objeto.getName());
			Class clase = Class.forName(objeto.getName());
			
			Object cc = clase.newInstance();
			
			return (T) cc;
		}
		catch (Exception e) {
			// TODO: handle exception
			message.append("No se puede crear la instancia de la clase:").append(objeto.getClass().getName());
			log.error(message.toString(), e);
			throw new Excepciones(message.toString(), e);
		}
		
		
		
	}
}


