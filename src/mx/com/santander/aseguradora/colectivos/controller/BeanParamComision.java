package mx.com.santander.aseguradora.colectivos.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametrosComision;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Comisiones.
 * 				parametrizacionComisiones.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	Ing. Issac Bautista
 * 		Cuando: 31-08-2020
 * 		Porque: Adecuaciones a pantalla de acuerdo a lo reunion que tuvimos.
 * ------------------------------------------------------------------------
 * 2.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 */
@Controller
@Scope("session")
public class BeanParamComision extends BeanParamComision2 {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder al servicio de parametros
	@Resource
	private ServicioParametros servicioParametros;
	
	//recurso para acceder al servicio de parametros
	@Resource
	private ServicioGeneric servicioGeneric;

	//recurso para acceder al servicio de parametros
	@Resource
	private ServicioParametrosComision servicioParamComision;
	
	//Log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(BeanParamComision.class);

	/**
	 * Constructor de clase
	 */
	public BeanParamComision() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_PARAM_COMISION);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PARAM_COMISION);
		
		//Se inicialisan valores por default
		init();
	}
	
	/**
	 * Metodo que consulta polizas parametrizadas
	 * @return Exito o Error
	 */
	public String consulta() {
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean;
		List<Object> lstResultados;
		Parametros parametro;
		Iterator<Object> it;
		String[] arrComisiones;
		ArrayList<BeanParametros> arlLista;
		
		try {						
			//Se validan datos para poder hacer consulta
			if(validaDatos()) {
				arlLista = new ArrayList<BeanParametros>();
				
				//Se realiza consulta para mostrar los procesos configurados
				filtro.append("and P.copaDesParametro = 'CATALOGO' \n" );
				filtro.append("and P.copaIdParametro  = 'COMISION' \n" );
				filtro.append("and P.copaNvalor5  = ").append(getTipoParam()).append("\n" );
				filtro.append("and '").append(GestorFechas.formatDate(getFeDesde(), Constantes.FORMATO_FECHA_UNO)).append("' >= P.copaFvalor1").append(" \n");
				filtro.append("and '").append(GestorFechas.formatDate(getFeHasta(), Constantes.FORMATO_FECHA_UNO)).append("' <= P.copaFvalor2").append(" \n");
				
				//se valida si el nombre del grupo se agrega al filtro
				if(!this.getDescripcion().equals(Constantes.DEFAULT_STRING)) {
					filtro.append("and UPPER(P.copaVvalor8) like '%" + this.getDescripcion().trim().toUpperCase() + "%' \n" );
				}
				
				filtro.append("order by P.copaNvalor1,P.copaNvalor2,P.copaNvalor3,P.copaNvalor4,P.copaVvalor1,P.copaVvalor2" );
				lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
				
				//se recorren resultados para crear objeto que se utiliza en la vsita
			    it = lstResultados.iterator();
			    while(it.hasNext()) {
			    	parametro = (Parametros) it.next();
					BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
					bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
					
					arrComisiones = bean.getCopaRegistro().split("\\$");
					bean.setCopaVvalor31(Utilerias.getPorcentaje(arrComisiones[0], Constantes.COMISION_EMISION, Constantes.POR_TOTAL));		//Comision Total Tecnica Emision
					bean.setCopaVvalor32(Utilerias.getPorcentaje(arrComisiones[0], Constantes.COMISION_RENOVACION, Constantes.POR_TOTAL));	//Comision Total Tecnica Renovacion
					bean.setCopaVvalor33(Utilerias.getPorcentaje(arrComisiones[1], Constantes.COMISION_EMISION, Constantes.POR_TOTAL));		//Comision Total Comercial Emision
					bean.setCopaRegistro1(Utilerias.getPorcentaje(arrComisiones[1], Constantes.COMISION_RENOVACION, Constantes.POR_TOTAL));	//Comision Total Comercial Renovacion
					
					arlLista.add(bean);
				}
				
			    //Se setea bandera a 1
				setnEjecuta(1);
				
				if(getnAccion() == Constantes.DEFAULT_INT) {
					//Se manda respuesta a pantalla
			    	setStrRespuesta("Se ejecuto con EXITO la consulta, favor de desplegar los resultados.");
				}
				
				//Se pasa a propiedad de pantalla
				setLstReglas(arlLista);
			}
			
			//se regresa mensage de SUCCESS a la vista
		    return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los reglas..");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}

	/**
	 * Metodo para consultar los productos segun el filtro 
	 * de ramo y idventa
	 */
	public void cargaProductos(){
		List<Object> lstResultados;
		Object[] arrDatos;
		Iterator<Object> it;
		ArrayList<Object> arlLista;
		
		try {
			//Se inicializa objecto 
			arlLista = new ArrayList<Object>();

			//Se realiza consulta para mostrar los procesos configurados
			lstResultados = this.servicioGeneric.consultaSQL(GeneratorQuerys.getQryProductos(getRamo(), getIdVenta()));

			//se recorren resultados para crear objeto que se utiliza en la vsita
		    it = lstResultados.iterator();
		    while(it.hasNext()) {
		    	arrDatos = (Object[]) it.next();
		    	arlLista.add(new SelectItem(arrDatos[0].toString(), arrDatos[1].toString()));
			}
		    
		    //Se inicializa propiedad de pantalla
		    setCmbProductos(arlLista);
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("No se pueden cargar productos", e);
		}
	}
	
	/**
	 * Metodo para consultar las tarifas segun el filtro 
	 * de ramo, idventa y producto
	 */
	public void cargaTarifas(){
		List<Object> lstResultados;
		Object[] arrDatos;
		Iterator<Object> it;
		ArrayList<Object> arlLista;
		
		try {
			if(getObjActual().getCopaNvalor5() == 3) {
				//Se inicializa objecto 
				arlLista = new ArrayList<Object>();

				//Se realiza consulta para mostrar los procesos configurados
				lstResultados = this.servicioGeneric.consultaSQL(GeneratorQuerys.getQryTarifas(getRamo(), getIdVenta(), getObjActual().getCopaVvalor1()));

				//se recorren resultados para crear objeto que se utiliza en la vsita
			    it = lstResultados.iterator();
			    while(it.hasNext()) {
			    	arrDatos = (Object[]) it.next();
			    	arlLista.add(new SelectItem(arrDatos[0].toString(), arrDatos[1].toString() + " - " + arrDatos[0].toString()));
				}
			    
			    //Se setea propiedad de pantalla
			    setCmbTarifas(arlLista);
			}
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("No se pueden cargar tarifas", e);
		}
	}

	/**
	 * Metodo que guarda un registro de grupo
	 */
	public void guardar() {
		Long secParametro;
		StringBuilder sbComisiones;
		String strCobertura;
		
		try { 
			sbComisiones = new StringBuilder();
			//Se validan datos antes de hacer el guardado
			if(validaDatosGuardar()) {
				//se consulta el siguiente id parametro
				secParametro = servicioParametros.siguente();
				
				//Se pasan valores necesarios para el guardado
				getObjActual().setCopaCdParametro(secParametro); 	 
				getObjActual().setCopaIdParametro("COMISION");
				getObjActual().setCopaDesParametro("CATALOGO"); 	
				getObjActual().setCopaNvalor1(1);					
				getObjActual().setCopaNvalor2(new Long(getRamo()));
				
				
				//Set valores segun forma comisionar
				sbComisiones.append(getTecnicaEmi().toString()).append("~");
				sbComisiones.append(getTecnicaReno().toString()).append("$");
				sbComisiones.append(getComercialEmi().toString()).append("~");
				sbComisiones.append(getComercialReno().toString());
				getObjActual().setCopaRegistro(sbComisiones.toString());

				//Se construye objeto para poder realizar el guardado con los nuevos valores asignados
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getObjActual());
				
				if(getTipoCobertura() == 3) {
					strCobertura = parametro.getCopaVvalor8();
					
					//Se gaurda mismos datos para ambas coberturas
					parametro.setCopaNvalor7(Double.valueOf(1));	//Vida
					parametro.setCopaVvalor8(strCobertura + " (Normal)");
					this.servicioParametros.guardarObjeto(parametro);
					
					parametro.setCopaCdParametro(secParametro + 1);
					parametro.setCopaNvalor7(Double.valueOf(2));	//Desempleo
					parametro.setCopaVvalor8(strCobertura + " (Desempleo)");
					this.servicioParametros.guardarObjeto(parametro);
				} else {
					parametro.setCopaNvalor7(new Double(getTipoCobertura()));
					this.servicioParametros.guardarObjeto(parametro);
				}
				
				//Se setea accion a 1
				setnAccion(1);
				//se manda llamar metodo de cancelar para setear valroes por default
				cancelar();

				//Se manda respuesta a pantalla
				setStrRespuesta("Se guardo con EXITO el registro.");
			}
		} catch (Excepciones e) {
			setStrRespuesta("Error al GUARDAR registro.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
		}
	}

	/**
	 * Metodo que modifica un registro de grupo
	 */
	public void modificar(){
		StringBuilder sbComisiones;

		try {
			sbComisiones = new StringBuilder();
			
			//Set valores segun forma comisionar
			sbComisiones.append(getTecnicaEmi().toString()).append("~");
			sbComisiones.append(getTecnicaReno().toString()).append("$");
			sbComisiones.append(getComercialEmi().toString()).append("~");
			sbComisiones.append(getComercialReno().toString());
			getObjActual().setCopaRegistro(sbComisiones.toString());

			//Se construye objeto para poder realizar la modificacion con los nuevos valores asignados 
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getObjActual());
			
			//Se actualiza registro
			this.servicioParametros.actualizarObjeto(parametro);
			
			//Se setea accion a 2
			setnAccion(2);
			
			//se manda llamar metodo de cancelar para setear valroes por default
			cancelar();

			//Se manda respuesta a pantalla
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al MODIFICAR registro.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que carga un LayOut de forma masiva
	 */
	public void cargaMasiva(){
		Archivo archivoSalida;

		try {
			
			//Se valida archivo no sea nulo
			if(getArchivo() == null) {
				FacesUtils.addErrorMessage("* Favor de seleccionar un archivo.");
				return;
			}
			
			//Se manda guardar los registros del archivo
			archivoSalida = this.servicioParamComision.guardaDatosArchivo(getArchivo());
			
			if(archivoSalida.getRuta().equals(Constantes.DEFAULT_STRING)) {
				//Se descarga archivo de errores
				archivoSalida.descargarArchivo(FacesUtils.getServletResponse(), 2);
				
				//Se elimina archivo de errores
				archivoSalida.eliminarArchivo();
			
				//Se manda llamar response complete
				FacesContext.getCurrentInstance().responseComplete();
				
				//Se setea accion a 3
				setnAccion(3);
				
				//se manda llamar metodo de cancelar para setear valroes por default
				cancelar();

				//Se manda respuesta a pantalla
				setStrRespuesta("Se cargo con EXITO el Archivo, favor de revisar archivo de errores.");
			} else {
				FacesUtils.addErrorMessage(archivoSalida.getRuta());
			}

		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al cargar archivo.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
		}
	}
	
	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
	/**
	 * @param servicioParamComision the servicioParamComision to set
	 */
	public void setServicioParamComision(ServicioParametrosComision servicioParamComision) {
		this.servicioParamComision = servicioParamComision;
	}
}