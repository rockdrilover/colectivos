package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import javax.faces.component.html.HtmlSelectOneMenu;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cedula Control.
 * 				cedulaControl.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanCedula
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanCedula3 extends BeanGenerico implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para tomar valor del combo ramos
	private HtmlSelectOneMenu inRamo;
	//Propiedad para tomar valor del combo polizas
	private HtmlSelectOneMenu inPoliza;
	//Propiedad para saber tipo de cedula
	private Integer tipocedula;
	
	/**
	 * Constructor de clase
	 */
	public BeanCedula3() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.inPoliza = new HtmlSelectOneMenu();
		this.inPoliza.setValue(Constantes.DEFAULT_INT);
		this.inRamo = new HtmlSelectOneMenu();
		this.inRamo.setValue(Constantes.PRODUCTO_VIDA);
	}
	
	/**
	 * @return the tipocedula
	 */
	public Integer getTipocedula() {
		return tipocedula;
	}
	/**
	 * @param tipocedula the tipocedula to set
	 */
	public void setTipocedula(Integer tipocedula) {
		this.tipocedula = tipocedula;
	}
	/**
	 * @return the inRamo
	 */
	public HtmlSelectOneMenu getInRamo() {
		return inRamo;
	}
	/**
	 * @param inRamo the inRamo to set
	 */
	public void setInRamo(HtmlSelectOneMenu inRamo) {
		this.inRamo = inRamo;
	}
	/**
	 * @return the inPoliza
	 */
	public HtmlSelectOneMenu getInPoliza() {
		return inPoliza;
	}
	/**
	 * @param inPoliza the inPoliza to set
	 */
	public void setInPoliza(HtmlSelectOneMenu inPoliza) {
		this.inPoliza = inPoliza;
	}
	
}
