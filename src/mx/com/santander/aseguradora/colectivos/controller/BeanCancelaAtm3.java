package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cancelacion Automatica.
 * 				cancelacionAutomatica.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanCancelaAtm
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanCancelaAtm3 extends BeanGenerico implements Serializable {
	
	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para objetos nuevos o modificados
	private ResumenCancelaDTO seleccionado;
	//Propiedad para verificar si se seleccionan todos los registros
	private boolean chkAll;
	//Propiedad para llevar un conteo
	private String conteo;
	
	/**
	 * Constructor de clase
	 */
	public BeanCancelaAtm3() {
		super();
		
		//Se inicialisan valores por default
		this.seleccionado = new ResumenCancelaDTO();
		this.chkAll = false;
		this.conteo = "0 registros seleccionados";
	}
	
	/**
	 * @return the seleccionado
	 */
	public ResumenCancelaDTO getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(ResumenCancelaDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the conteo
	 */
	public String getConteo() {
		return conteo;
	}
	/**
	 * @param conteo the conteo to set
	 */
	public void setConteo(String conteo) {
		this.conteo = conteo;
	}
	/**
	 * @return the chkAll
	 */
	public boolean isChkAll() {
		return chkAll;
	}
	/**
	 * @param chkAll the chkAll to set
	 */
	public void setChkAll(boolean chkAll) {
		this.chkAll = chkAll;
	}
}
