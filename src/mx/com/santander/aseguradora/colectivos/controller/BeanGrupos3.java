package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Configuracion de Grupos.
 * 				confGrupos.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanGrupos
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanGrupos3 extends BeanGenerico implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para nombre de grupo
	private String nombreGrupo;	
	//Propiedad para correo
	private String correo;
	//Propiedad para dar un consecutivo
	private Integer nConsecutivo;	
	//Propiedad para editar objeto
	private BeanParametros objActual = new BeanParametros();
	
	/**
	 * Constructor de clase
	 */
	public BeanGrupos3() {
		super();
		
		//Se inicialisan valores por default
		this.nombreGrupo = Constantes.DEFAULT_STRING;
		this.correo = Constantes.DEFAULT_STRING;
		this.nConsecutivo = Constantes.DEFAULT_INT;
		
	}
	
	/**
	 * @return the nConsecutivo
	 */
	public Integer getnConsecutivo() {
		return nConsecutivo;
	}
	/**
	 * Obtiene el numero consecutivo a generar
	 * @param nConsecutivo the nConsecutivo to set
	 */
	public void setnConsecutivo(Integer nConsecutivo) {
		this.nConsecutivo = nConsecutivo;
	}
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * @return the nombreGrupo
	 */
	public String getNombreGrupo() {
		return nombreGrupo;
	}
	/**
	 * @param nombreGrupo the nombreGrupo to set
	 */
	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}
	/**
	 * @return the objActual
	 */
	public BeanParametros getObjActual() {
		return objActual;
	}
	/**
	 * @param objActual the objActual to set
	 */
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
}