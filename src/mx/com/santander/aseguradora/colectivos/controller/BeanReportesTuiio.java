package mx.com.santander.aseguradora.colectivos.controller;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;

import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-09-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Reportes Tuioo.
 * 				reportestuiio.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanReportesTuiio extends BeanGenerico {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder al servicio
	@Resource
	private ServicioGeneric servicioGeneric;	
	
	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanReportesTuiio.class);
	
	//Propiedad para tipo de reporte
	private Integer tipoReporte;
	

	/**
	 * Constructor de clase
	 */
	public BeanReportesTuiio() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_REPORTES_TUIIO);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_REPORTES_TUIIO);
	}

	/**
	 * Metodo que descarga reporte seleccionadoDescarga detalle de creditos de la cedula
	 */
	public void consulta() {
		//Declaracion de Variables
		String strNombre = null;
		String strRutaTemp;
		Archivo objArchivo;
		String[] arrColumnas = null;
		List<Object> lstResultados;
		String strFeDesde;
		String strFeHasta;
		
		try {
			//Validamos los datos ingresados
			if(validaDatos()) {
				//Declaramos encabezados en array de columnas
				arrColumnas = getNombreEncabezado();
			
				//Se asigna variable para ruta temporal
				strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
				
				//Se formatean fechas
				strFeDesde = GestorFechas.formatDate(getFeDesde(), Constantes.FORMATO_FECHA_UNO);
				strFeHasta = GestorFechas.formatDate(getFeHasta(), Constantes.FORMATO_FECHA_UNO);
				
				//Se asigna variable para nombre de archivo y se consultan datos
				if(getTipoReporte() == 1) {
					strNombre = "ZSMX_TUIIO_RCB_" + GestorFechas.formatDate(new Date(), Constantes.FORMATO_FECHA_REP);
					lstResultados = this.servicioGeneric.consultaSQL(GeneratorQuerys.getQryReporteCOKA(strFeDesde, strFeHasta));
				} else {
					strNombre = "ZSMX_TUIIO_" + GestorFechas.formatDate(new Date(), Constantes.FORMATO_FECHA_REP);
					lstResultados = this.servicioGeneric.consultaSQL(GeneratorQuerys.getQryReporteTecnico(strFeDesde, strFeHasta));
				}
				
				//Se crea instancia de archivo para generar y descargar archivo
				objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
				objArchivo.copiarArchivo(arrColumnas, lstResultados, ",");
				objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
				objArchivo.eliminarArchivo();
				
				setStrRespuesta("Archivo descargado satisfactoriamente.");
				FacesUtils.addErrorMessage(getStrRespuesta());
				FacesContext.getCurrentInstance().responseComplete();
			}
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al generar reporte seleccionado.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);	
			//Se lanza Exception
			throw new FacesException(getStrRespuesta(), e);
		}
	}

	/**
	 * Metodo que genera el nombre de columnas segun tipo de reporte
	 * @return arreglo de columnas 
	 */
	private String[] getNombreEncabezado() {
		String[] arrColumnas = new String[2];
		
		
		if(getTipoReporte() == 1) { //Reporte COKA
			arrColumnas = new String[7];	
			arrColumnas[0] = "SUCURSAL";
			arrColumnas[1] = "NOPRESTAMO";
			arrColumnas[2] = "FECHAEMISIONSEGURO";
			arrColumnas[3] = "FECHAFINSEGURO";
			arrColumnas[4] = "TIPOSEGURO";
			arrColumnas[5] = "GENEROCONTRATANTE";
			arrColumnas[6] = "FECHANACIMIENTO";
		} else { //Reporte Tecnico TUIIO
			arrColumnas = new String[20];	
			arrColumnas[0] = "FECHAEFECTIVA";
			arrColumnas[1] = "FECHAINICIO";
			arrColumnas[2] = "FECHAFIN";
			arrColumnas[3] = "CANAL";
			arrColumnas[4] = "RAMO";
			arrColumnas[5] = "CERTIFICADO";
			arrColumnas[6] = "NOCREDITO";
			arrColumnas[7] = "POLIZA";
			arrColumnas[8] = "PRIMANETA";
			arrColumnas[9] = "PRIMATOTAL";
			arrColumnas[10] = "BUC";
			arrColumnas[11] = "NOMBRE";
			arrColumnas[12] = "APELLIDOPATERNO";
			arrColumnas[13] = "APELLIDOMATERNO";
			arrColumnas[14] = "RFC";
			arrColumnas[15] = "DOMICILIO";
			arrColumnas[16] = "COLONIA";
			arrColumnas[17] = "CP";
			arrColumnas[18] = "FECHANACIMIENTO";
			arrColumnas[19] = "EMAIL";
		}
		
		return arrColumnas;
	}

	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	private boolean validaDatos() {
		boolean bRegreso = true;
		
		//Se valida se selecciona un tipo cedula
		if(this.tipoReporte == Constantes.DEFAULT_INT) {
			setStrRespuesta("Favor de seleccionar Tipo Reporte para poder generar reporte.");
			return false;
		}
		
		//Se validan fechas
		bRegreso = validaFechas();
		
		return bRegreso;
	}
	
	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
	/**
	 * @return the tipoReporte
	 */
	public Integer getTipoReporte() {
		return tipoReporte;
	}
	/**
	 * @param tipoReporte the tipoReporte to set
	 */
	public void setTipoReporte(Integer tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
}
