package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		30-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio para diferentes pantallas.
 * 				Esta clase contine solo propiedades que son genericas 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanGenerico3 extends BeanGenerico4 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para especificar fecha desde
	private Date feDesde;
	//Propiedad para especificar fecha hasta
	private Date feHasta;
	//Propiedad para especificar numero de pagina
	private int numPagina;
	//Propiedad para especificar numero de pagina
	private int numPagina1;

	/**
	 * Constructor de clase
	 */
	public BeanGenerico3() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.numPagina = 1;
		this.numPagina1 = 1;
		this.feDesde = new Date();
		this.feHasta = new Date();
	}

	/**
	 * @return the feDesde
	 */
	public Date getFeDesde() {
		return new Date(feDesde.getTime());
	}
	/**
	 * @param feDesde the feDesde to set
	 */
	public void setFeDesde(Date feDesde) {
		if(feDesde != null) {
			this.feDesde = new Date(feDesde.getTime());
		}
	}
	/**
	 * @return the feHasta
	 */
	public Date getFeHasta() {
		return new Date(feHasta.getTime());
	}
	/**
	 * @param feHasta the feHasta to set
	 */
	public void setFeHasta(Date feHasta) {
		if(feHasta != null) {
			this.feHasta = new Date(feHasta.getTime());
		}
	}
	/**
	 * @return the numPagina
	 */
	public int getNumPagina() {
		return numPagina;
	}
	/**
	 * @param numPagina the numPagina to set
	 */
	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	/**
	 * @return the numPagina1
	 */
	public int getNumPagina1() {
		return numPagina1;
	}
	/**
	 * @param numPagina1 the numPagina1 to set
	 */
	public void setNumPagina1(int numPagina1) {
		this.numPagina1 = numPagina1;
	}
}