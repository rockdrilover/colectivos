package mx.com.santander.aseguradora.colectivos.controller;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.bo.ColectivosCargaCobranza;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCobranza;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		20-11-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Carga Cobranza.
 * 				cargaCobranza.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanCargaCobranza extends BeanCargaCobranza2 implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder al servicio
	@Resource
	private transient ServicioCarga servicioCarga;
	
	//recurso para acceder al servicio
	@Resource
	private transient ServicioCobranza servicioCobranza;
	
	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanReporteCobranza.class);
	
	//Para mostrar resultados de carga
	private static final ArrayList<ResumenCancelaDTO> lstResultados = new ArrayList<ResumenCancelaDTO>();
	
	//Clase para tener acceso a propiedades genericas.
	protected transient BeanGenerico beanGen;

	/**
	 * Constructor de clase
	 */
	public BeanCargaCobranza() {
		super();
		
		//Se agrega el bean a session
		beanGen = new BeanGenerico();
		beanGen.setBean(BeanNames.BEAN_CARGA_COBRANZA);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CARGA_COBRANZA);
	}

	/**
	 * Metodo que se utiliza para cargar archivo de cobranza
	 * @return resultado de operacion
	 */
	public String cargaCobranza() {
		List<Object> lstRegistros = null;
		beanGen.setStrRespuesta(Constantes.DEFAULT_STRING);
		ResumenCancelaDTO objRespuesta;
		
		try {
			
			//Se validan datos
			if(validaDatos()) {
				//Se obtienen registros del archivo a subir
				lstRegistros = this.leerArchivoCobranza(beanGen.getArchivo(), ",");
				
				//Se va llenando lista parar mostrar valores
				objRespuesta = new ResumenCancelaDTO();	
				objRespuesta.setArchivo(getNombreArchivo());
				objRespuesta.setNuPoliza(new BigDecimal(beanGen.getPoliza()));
				
				//Si tiene registros el archivo a cargar
				if(lstRegistros != null && !lstRegistros.isEmpty()) {
					//Se resetea contador de errores
					setErrores(Constantes.DEFAULT_INT);
					
					//se manda guardar la lista de objetos
					this.servicioCarga.guardarObjetos(lstRegistros);
					
					//Se manda ejecutar proceso de Cobranza
					setErrores(servicioCobranza.cargaCobranza(new Short("1"), beanGen.getRamo(), beanGen.getPoliza(), getTipo(), getNombreArchivo()));
					
					//Se agregan cantidades a objeto
					objRespuesta.setCantidad(new BigDecimal(getErrores()));
					objRespuesta.setCdRamo(new BigDecimal(getRegistros()).subtract(new BigDecimal(getErrores())));
					
				} else {
					objRespuesta.setCantidad(BigDecimal.ZERO);
					objRespuesta.setCdRamo(BigDecimal.ZERO);
				}
				
				//Se agrego registro a lista
				lstResultados.add(objRespuesta);
				
				//Se regresa respuesta
				beanGen.setStrRespuesta("El archivo se cargo correctamente, favor de revisar resultados.");
			}
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			beanGen.setStrRespuesta("El proceso de carga fall�.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
			return NavigationResults.FAILURE;
		}

	}
	
	/**
	 * Metodo que sirve para descargar los errores
	 * @return mensaje de proceso
	 */
	public void descargaErroresCarga() {
		String strNombre = null, strRutaTemp;
		Archivo objArchivo;
		List<Object> listaErrores;
		String[] arrColumnas = new String[2];
		
		try {
			//Validamos si hubo errores en la carga de archivo
			if(getErrores() > 0) {
				//Se validan datos
				if(validaDatos()) {
					//Se obtiene ruta temporal
					strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes") + File.separator;
					
					//Se asigna nombre a reporte
					strNombre = "Errores_Cobranza" + beanGen.getPoliza() + " - " + GestorFechas.formatDate(new Date(), "ddMMyyyy"); 
					
					//Agregamos registro de enccabezado
					
					arrColumnas = getNombreColumnas(getTipo());
					
					//Se consultan los errores
					listaErrores = new ArrayList<Object>();
					listaErrores.addAll(servicioCobranza.consultaErroresCobranza(new Short("1"), beanGen.getRamo(), beanGen.getPoliza(), getNombreArchivo(), getTipo()));
					
					//Se inicializa objeto para realizar descarga
					objArchivo = new Archivo(strRutaTemp, strNombre , ".csv");
					objArchivo.copiarArchivo(arrColumnas, listaErrores, ",");
					objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
					objArchivo.eliminarArchivo();
					
					beanGen.setStrRespuesta("El archivo se descargo de manera correcta.");
					FacesContext.getCurrentInstance().responseComplete();
				}
	    	} else {
	    		beanGen.setStrRespuesta("No se puede generar el archivo, no se encontraron errores en el proceso de carga.");
	    	}
			
		} catch (Exception e) {
			beanGen.setStrRespuesta("Error al descargar errores.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	private boolean validaDatos() {
		boolean bRegreso = true;
		
		//Se valida hayan seleccionado ramo
		if(beanGen.getRamo() == 0) {
			beanGen.setStrRespuesta("Favor de seleccionar un ramo.");
			bRegreso = false;
		}
		
		//Se valida hayan seleccionado ramo
		if(beanGen.getPoliza() == 0) {
			beanGen.setStrRespuesta("Favor de seleccionar una poliza.");
			bRegreso = false;
		}
		
		//Se valida archivo no sea nulo
		if(beanGen.getArchivo() == null) {
			beanGen.setStrRespuesta("Favor de seleccionar un archivo.");
			bRegreso = false;
		}
		
		return bRegreso;
	}
	
	/**
	 * Metodo que sirve para leer datos del archivo
	 * @param file archivo a cargar
	 * @param delimitador delimitar de archivo
	 * @return lista de registros
	 * @throws Excepciones con errores en general
	 */
	public List<Object> leerArchivoCobranza(File file, String delimitador) throws Excepciones {
		List<Object[]> arlRegistros;
		Archivo objArchivo;
		List<Object> lista = null;
		Iterator<Object[]> it;
		String strLinea;
		
		try {
			//Se obtiene nombre de archivo
			setNombreArchivo(file.getName().substring(file.getName().lastIndexOf('\\') + 1) + "_" + GestorFechas.formatDate(new Date(), "HHmmss"));
			
			//Se crea instancia de clase archivo
			objArchivo = new Archivo(file);
			
			//Se lee el archivo
			arlRegistros = objArchivo.leerArchivo("?", true);
			
			//Si trae registros el archivo
			if(arlRegistros != null && !arlRegistros.isEmpty()) {
				//Se toma primer registro para validar columnas
				strLinea = Arrays.toString(arlRegistros.get(0)).replace("[", "").replace("]", "");
				
				//Se valida  numero de columnas
				if((getTipo() == 20 && strLinea.split(delimitador).length != 8) || (this.tipo == 30 && strLinea.split(delimitador).length != 7)) {
					beanGen.setStrRespuesta("El archivo que se desea cargar, no corresponde al tipo de carga seleccionado.");
					return Collections.emptyList();
				}
				
				//Se inicializan datos para leer archivo
				lista = new ArrayList<Object>();
				setRegistros(0);
				
				//Se inicializa iterador
				it = arlRegistros.iterator();
				
				//Se registroso de archivo
				while(it.hasNext()) {
					//Se toma la linea del registro
					strLinea = Arrays.toString(it.next()).replace("[", "").replace("]", "");
					//Se manda crear el objeto
					creaObjetoCarga(lista, strLinea, delimitador);
				}
			}

		} catch (Excepciones e) {
			lista = null;
			beanGen.setStrRespuesta("No se puede cargar la informacion del archivo.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
		}
		
		return lista;
	}

	/**
	 * Metodo que crea objeto y lo agrega a la lista
	 * @param lista lista para mandar guardar
	 * @param strLinea linea del registro
	 * @param delimitador para demilitar la linea
	 */
	private void creaObjetoCarga(List<Object> lista, String strLinea, String delimitador) {
		//Se inicialian propiedades
		ColectivosCargaCobranza preCarga;
		Integer getCsv = Constantes.DEFAULT_INT;
		Integer nCanal = 1;
		String strCredito = Constantes.DEFAULT_STRING;
		String[] arrDatos;
		
		//Se delimita linea
		arrDatos = strLinea.split(delimitador);
		
		//Se valida el numero de poliza ingresado en pantalla corresponda al que viene en el archivo
		if (!Constantes.DEFAULT_STRING.equals(arrDatos[0].trim()) && arrDatos[0].trim().equals(beanGen.getPoliza().toString()))  {
			//Se valida el tipo de archivo para tomar los valores
			if(getTipo() == 20) {
				getCsv = 1;
				strCredito = arrDatos[1].trim();
			} else {
				getCsv = 0;
				strCredito = Constantes.DEFAULT_STRING_ID;
			}
			
			//Se inicializa objeto
			preCarga = new ColectivosCargaCobranza();
			
			//Se setean valores para el Id
			preCarga.getId().setCoccFacCanal(nCanal);
			preCarga.getId().setCoccFacRamo(beanGen.getRamo());
			preCarga.getId().setCoccFacPoliza(beanGen.getPoliza().longValue());
			preCarga.getId().setCoccNuCredito(strCredito);
			preCarga.getId().setCoccFacRecibo(new BigDecimal(arrDatos[1 + getCsv].trim()));
			
			//Se complemtan los datos del objeto2
			preCarga.getCcc2().setCoccFacMesContable(new Date());
			preCarga.getCcc2().setCoccFacPrimaNeta(BigDecimal.ZERO);
			preCarga.getCcc2().setCoccFacRfi(BigDecimal.ZERO);
			preCarga.getCcc2().setCoccFacPrimaTotal(new BigDecimal(arrDatos[2 + getCsv].trim()));
			preCarga.getCcc2().setCoccCobCanal(nCanal);
			preCarga.getCcc2().setCoccCobRamo(beanGen.getRamo());
			preCarga.getCcc2().setCoccCobRecibo(new BigDecimal(arrDatos[1 + getCsv].trim()));
			preCarga.getCcc2().setCoccCobFechaCobro(GestorFechas.generateDate(arrDatos[4 + getCsv].trim().toString(), Constantes.FORMATO_FECHA_UNO));
			preCarga.getCcc2().setCoccCobPoliza(beanGen.getPoliza().longValue());
			
			//Se complemtan los datos del objeto3
			preCarga.getCcc3().setCoccNocCanal(nCanal);
			preCarga.getCcc3().setCoccCobPrimaNeta(BigDecimal.ZERO);
			preCarga.getCcc3().setCoccCobRfi(BigDecimal.ZERO);
			preCarga.getCcc3().setCoccCobIva(BigDecimal.ZERO);
			preCarga.getCcc3().setCoccCobPrimaTotal(new BigDecimal(arrDatos[3 + getCsv].trim()));
			preCarga.getCcc3().setCoccNocRecibo(new BigDecimal(arrDatos[1 + getCsv].trim()));
			preCarga.getCcc3().setCoccNocPrimaNeta(BigDecimal.ZERO);
			preCarga.getCcc3().setCoccNocRamo(beanGen.getRamo());
			preCarga.getCcc3().setCoccNocPoliza(beanGen.getPoliza().longValue());
			
			//Se complemtan los datos del objeto4
			preCarga.getCcc4().setCoccNocRfi(BigDecimal.ZERO);
			preCarga.getCcc4().setCoccNocIva(BigDecimal.ZERO);
			preCarga.getCcc4().setCoccSaldoDeudor(BigDecimal.ZERO);
			preCarga.getCcc4().setCoccCargada(getTipo());
			preCarga.getCcc4().setCoccArchivo(getNombreArchivo());
			preCarga.getCcc4().setCoccLinea(strLinea);
			preCarga.getCcc4().setCoccFacIva(BigDecimal.ZERO);
		
			if(Constantes.DEFAULT_STRING.equals(arrDatos[5 + getCsv].trim())) {
				preCarga.getCcc4().setCoccNocPrimaTotal(BigDecimal.ZERO);
				preCarga.getCcc4().setCoccFechaNc(new Date());
			} else {
				preCarga.getCcc4().setCoccNocPrimaTotal(new BigDecimal(arrDatos[5 + getCsv].trim()));
				preCarga.getCcc4().setCoccFechaNc(GestorFechas.generateDate(arrDatos[6 + getCsv].trim().toString(), Constantes.FORMATO_FECHA_UNO));
			}
		
			//Se agrega objeto a la lista
			lista.add(preCarga);
		
			//Se incrementa contador de registros
			setRegistros(getRegistros() + 1);
		}
	}

	/**
	 * @return the lstResultados
	 */
	public List<ResumenCancelaDTO> getLstResultados() {
		return new ArrayList<ResumenCancelaDTO>(lstResultados);
	}
	
	/**
	 * @param servicioCarga the servicioCarga to set
	 */
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}

	/**
	 * @param servicioCobranza the servicioCobranza to set
	 */
	public void setServicioCobranza(ServicioCobranza servicioCobranza) {
		this.servicioCobranza = servicioCobranza;
	}

	/**
	 * @return the beanGen
	 */
	public BeanGenerico getBeanGen() {
		return beanGen;
	}
}