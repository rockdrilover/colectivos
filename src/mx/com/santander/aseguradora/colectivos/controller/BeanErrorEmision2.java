package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.view.dto.BitacoraErroresDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.CifrasControlDTO;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Erores de Emision.
 * 				errorEmision.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanErrorEmision
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanErrorEmision2 extends BeanGenerico implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para editar o guardar objeto
	private CifrasControlDTO seleccionado;
	//Propiedad para nombre de layout
	private String nombreLayout;
	//Propiedad para lista de archivos
	private List<CifrasControlDTO> listaArchivos;
	//Propiedad para lista de detalle errores
	private List<BitacoraErroresDTO> lstDetalleArchivo;
	
	/**
	 * Constructor de clase
	 */
	public BeanErrorEmision2() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.listaArchivos = new ArrayList<CifrasControlDTO>();
		this.lstDetalleArchivo = new ArrayList<BitacoraErroresDTO>();
		this.seleccionado = new CifrasControlDTO();
	}
	
	/**
	 * @return the nombreLayout
	 */
	public String getNombreLayout() {
		return nombreLayout;
	}
	/**
	 * @param nombreLayout the nombreLayout to set
	 */
	public void setNombreLayout(String nombreLayout) {
		this.nombreLayout = nombreLayout;
	}
	/**
	 * @return the seleccionado
	 */
	public CifrasControlDTO getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(CifrasControlDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the listaArchivos
	 */
	public List<CifrasControlDTO> getListaArchivos() {
		return new ArrayList<CifrasControlDTO>(listaArchivos);
	}
	/**
	 * @param listaArchivos the listaArchivos to set
	 */
	public void setListaArchivos(List<CifrasControlDTO> listaArchivos) {
		this.listaArchivos = new ArrayList<CifrasControlDTO>(listaArchivos);
	}
	/**
	 * @return the lstDetalleArchivo
	 */
	public List<BitacoraErroresDTO> getLstDetalleArchivo() {
		return new ArrayList<BitacoraErroresDTO>(lstDetalleArchivo);
	}
	/**
	 * @param lstDetalleArchivo the lstDetalleArchivo to set
	 */
	public void setLstDetalleArchivo(List<BitacoraErroresDTO> lstDetalleArchivo) {
		this.lstDetalleArchivo = new ArrayList<BitacoraErroresDTO>(lstDetalleArchivo);
	}
}
