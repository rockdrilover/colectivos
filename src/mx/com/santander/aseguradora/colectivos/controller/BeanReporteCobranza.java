package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import javax.annotation.Resource;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCobranza;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		05-12-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Reporte Cobranza.
 * 				reporteCobranza.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanReporteCobranza extends BeanReporteCobranza2 implements Serializable {
	
	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder al servicio
	@Resource
	private transient ServicioCobranza servicioCobranza;
	
	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanReporteCobranza.class);
	
	/**
	 * Constructor de clase
	 */
	public BeanReporteCobranza() {
		//Se manda llamar constructor clase padre
		super();
		
	}

	/**
	 * Metodo para realizar la consulta despues de la validacion de fechas y datos obligatorios
	 * @return mensage de exito o falla
	 *
	 */
	public String consultaReporteComisiones() {
		beanGen.setStrRespuesta(Constantes.DEFAULT_STRING);
		String strFechaIni = null, strFechaFin = null;
		
		try {
			if (validarDatos()) {
				strFechaIni = GestorFechas.formatDate(beanGen.getFeDesde(), "dd/MM/yyyy");
				strFechaFin = GestorFechas.formatDate(beanGen.getFeHasta(), "dd/MM/yyyy");
				
				beanGen.setStrRespuesta(servicioCobranza.consultarRegistrosCobranza(getTipo(), beanGen.getRamo(), beanGen.getPoliza(), getEstatus(), strFechaFin, strFechaIni));
			}
		} catch (Excepciones e) {
			beanGen.setStrRespuesta("Error al generar informaci�n.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
			return NavigationResults.FAILURE;
		}
		
		return NavigationResults.SUCCESS;
	}
	
	/**
	 * Metodo que valida datos ingresados en pantalla
	 * @return bandera que nos indica si sigue el flujo o no
	 */
	public boolean validarDatos() {
		//Se valida hayan seleccionado un tipo de reporte
		if(getTipo() == Constantes.DEFAULT_INT) {
			beanGen.setStrRespuesta("Favor de seleccionar un tipo de reporte");
			return false;
		}
		
		//Se valida hayan ingresado ramo y poliza, fechas y estatus
		return (validarRamoPoliza() && beanGen.validaFechas() && validarEstatus());
		
	}

	/**
	 * Valida datos ingresados de Ramo y Poliza
	 * 
	 * @return bandera para seguir flujo
	 */
	private boolean validarRamoPoliza() {
		
		//Estos datos no se valida para tipo de reporte 4 y 7
		if(!(getTipo() == 4 || getTipo() == 7)) {
			//Se valida el ramo
			if(beanGen.getRamo() == null || beanGen.getRamo() <= 0) {
				beanGen.setStrRespuesta("Favor de seleccionar un ramo.");
				return false;
			}
			
			//Se valida la poliza
			if(beanGen.getPoliza().intValue() == -1) {
				beanGen.setStrRespuesta("Favor de seleccionar una p�liza.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Metodo que valida estatus ingresado en pantalla
	 * @return bandera para seguir flujo
	 */
	private boolean validarEstatus() {
		//Este dato se valida para tipo de reporte 2 y 3
		if((getTipo() == 2 || getTipo() == 3) && getEstatus() == 0) {
			//Se manda mensaje si no seleccionaro estatus
			beanGen.setStrRespuesta("Favor de seleccionar un estatus.");
			return false;
		}
		
		return true;
	}

	/**
	 * Metodo parar setear recurso
	 * @param servicioCobranza servicio a utilizar
	 */
	public void setServicioCobranza(ServicioCobranza servicioCobranza) {
		this.servicioCobranza = servicioCobranza;
	}
}
