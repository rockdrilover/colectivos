package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturasId;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCertificadoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		08-03-2021
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Consulta Detalle Credito (Siniestros).
 * 				consDetalleCreditoSin.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanConsDetalleCreSin
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class BeanConsDetalleCreSin3 extends BeanGenerico implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para filtro por appaterno
	private String apellidoPaterno;
	//Propiedad para filtro por apmaterno
	private String apellidoMaterno;
	//Propiedad para cliente seleccionado
	private DetalleCertificadoDTO selCliente;
	//Propiedad para certificado seleccionado
	private Certificado seleccionado;
		
	/**
	 * Constructor de clase
	 */
	public BeanConsDetalleCreSin3() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicializan valores por default
		this.apellidoMaterno = Constantes.DEFAULT_STRING;
		this.apellidoPaterno = Constantes.DEFAULT_STRING;
		this.selCliente		= new DetalleCertificadoDTO();
		this.seleccionado 	= new Certificado();
	}

	/**
	 * Metodo que crea objeto cobertura
	 * @param arrCobertura datos de consulta
	 * @param sumaAseg  suma asegurada 
	 * @param sumaAsegSI suma asegurada saldo insoluto
	 * @param idVenta id venta del certificado
	 * @return objeto cobertura
	 */
	public Object creaObjCobertura(Object[] arrCobertura, BigDecimal sumaAseg, BigDecimal sumaAsegSI, String idVenta) {
		//Se inicializa objeto
		ColectivosCoberturas objResultado = new ColectivosCoberturas();
		
		//Se pasan valores al objeto
		objResultado.setId(new ColectivosCoberturasId());
		objResultado.getId().setCocbCarbCdRamo(( (BigDecimal) arrCobertura[0]).byteValue());
		objResultado.getId().setCocbCacbCdCobertura(arrCobertura[1].toString());
		objResultado.setCocbCampov3(arrCobertura[2].toString());
		objResultado.setCocbTaRiesgo((BigDecimal) arrCobertura[3]);
		objResultado.setCocbCampov2(arrCobertura[4].toString());
		objResultado.setCocbMtFijo(new BigDecimal(arrCobertura[4].toString()));
		objResultado.setCocbCampov1(arrCobertura[5].toString());
		objResultado.setCocbCampon1(( (BigDecimal) arrCobertura[6]).longValue());
		objResultado.setCocbPoComision((BigDecimal) arrCobertura[7]);
		
		//Se calcula suma asegurada
		if(objResultado.getCocbMtFijo().compareTo(Constantes.DEFAULT_BIGDECIMAL) > 0) {
			if("4".equals(idVenta) && 
					objResultado.getId().getCocbCarbCdRamo() == 14 && 
					"012".equals(objResultado.getId().getCocbCacbCdCobertura())) {
				objResultado.setCocbMtFijo(sumaAsegSI);
			}
		} else {
			objResultado.setCocbMtFijo(sumaAseg);
		}
		
		//Se multiplica porcentaje de suma asegurada
		objResultado.setCocbMtFijo(objResultado.getCocbMtFijo().multiply(new BigDecimal(objResultado.getCocbCampov1())));
		
		//S regresa objeto cobertura
		return objResultado;
	}
	
	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * @return the selCliente
	 */
	public DetalleCertificadoDTO getSelCliente() {
		return selCliente;
	}
	/**
	 * @param selCliente the selCliente to set
	 */
	public void setSelCliente(DetalleCertificadoDTO selCliente) {
		this.selCliente = selCliente;
	}
	/**
	 * @return the seleccionado
	 */
	public Certificado getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(Certificado seleccionado) {
		this.seleccionado = seleccionado;
	}
	
}
