package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;
import mx.com.santander.aseguradora.colectivos.view.dto.ComisionesDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Comisiones.
 * 				parametrizacionComisiones.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanParamComision
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanParamComision3 extends BeanParamComision4 implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para lista de combo tarifas
	private List<Object> cmbTarifas;
	//Propiedad para lista de combo reglas
	private List<BeanParametros> lstReglas;
	//Propiedad para objeto a editar
	private BeanParametros objActual;
	//Propiedad para lista de combo productos
	private List<Object> cmbProductos;
	
	/**
	 * Constructor de clase
	 */
	public BeanParamComision3() {
		//se manda ejecutar constructor de clase padre
		super();
	}
	
	/**
	 * Metodo para inicializar propiedades por defautl
	 */
	protected void init() {
		this.objActual = new BeanParametros();
		this.objActual.setCopaNvalor5(Constantes.DEFAULT_INT);
		this.objActual.setCopaFvalor1(new Date());
		this.objActual.setCopaFvalor2(new Date());
		
		setTipoCobertura(1);
		this.cmbTarifas = new ArrayList<Object>();
		this.cmbProductos = new ArrayList<Object>();
		this.lstReglas = new ArrayList<BeanParametros>();
		setTecnicaEmi(new ComisionesDTO());
		setTecnicaReno(new ComisionesDTO());
		setComercialEmi(new ComisionesDTO());
		setComercialReno(new ComisionesDTO());
	}

	/**
	 * @return the cmbTarifas
	 */
	public List<Object> getCmbTarifas() {
		return new ArrayList<>(cmbTarifas);
	}
	/**
	 * @param cmbTarifas the cmbTarifas to set
	 */
	public void setCmbTarifas(List<Object> cmbTarifas) {
		this.cmbTarifas = new ArrayList<>(cmbTarifas);
	}	
	/**
	 * @return the lstReglas
	 */
	public List<BeanParametros> getLstReglas() {
		return new ArrayList<BeanParametros>(lstReglas);
	}
	/**
	 * @param lstReglas the lstReglas to set
	 */
	public void setLstReglas(List<BeanParametros> lstReglas) {
		this.lstReglas = new ArrayList<BeanParametros>(lstReglas);
	}
	/**
	 * @return the objActual
	 */
	public BeanParametros getObjActual() {
		return objActual;
	}
	/**
	 * @param objActual the objActual to set
	 */
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	/**
	 * @return the cmbProductos
	 */
	public List<Object> getCmbProductos() {
		return new ArrayList<>(cmbProductos);
	}
	/**
	 * @param cmbProductos the cmbProductos to set
	 */
	public void setCmbProductos(List<Object> cmbProductos) {
		this.cmbProductos = new ArrayList<>(cmbProductos);
	}
}