package mx.com.santander.aseguradora.colectivos.controller;

import java.io.File;
import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.FileUtil;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		30-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio para diferentes pantallas.
 * 				Esta clase contine solo propiedades que son genericas 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 2.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 09-03-2021
 * 		Porque: Se agrega propiedad filtro
 * ------------------------------------------------------------------------
 */
public class BeanGenerico extends BeanGenerico2 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
		
	//Log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(BeanGenerico.class);
		
	//Propiedad  para indicar nombre de bean
	private String bean;
	//Propiedad para ejecutar consulta
	private int nEjecuta;
	//Propiedad para realizar filtro
	private StringBuilder filtro;
	
	/**
	 * Constructor de clase
	 */
	public BeanGenerico() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.nEjecuta = Constantes.DEFAULT_INT;
		this.filtro = new StringBuilder();
	}
	
	/**
	 * Metodo que carga al servidor el archivo seleccionado
	 * @param event evento del archivo
	 * @throws Exception con error general
	 */
	public void listener(UploadEvent event) throws Exception {
		UploadItem item;
		File archivoTemporal;
		String ruta;
		String nombreArchivo = null;
		String nombreRuta;
		Archivo file;
		
		//Se obtiene archivo seleccionado
		item = event.getUploadItem();
		ruta = FacesUtils.getServletContext().getRealPath(Constantes.RUTA_CARGAS);
		file = new Archivo();
		
		//Se valida si tiene datos el archivo
		if(item.getData() != null) {
			//Se pasan valores a archivo temporal
			file.setData(item.getData());
			file.setNombreArchivo(item.getFileName());
			file.setTamano(item.getData().length);

			//Se da nombre al archivo
			nombreRuta = file.getNombreArchivo();
			nombreArchivo = nombreRuta.substring(nombreRuta.lastIndexOf('\\') + 1);
			archivoTemporal = FileUtil.uniqueFile(new File(ruta), nombreArchivo);
			
			//Se pasas contenido al nuevo archivo
			FileUtil.write(archivoTemporal, item.getData());
			setArchivo(archivoTemporal);
			setStrRespuesta("Archivo cargado al servidor correctamente");
			LOG.debug(getStrRespuesta());
		}
	}
	
	/**
	 * Metodo que valida campos de fechas 	
	 * @return bandera true or false
	 */
	protected boolean validaFechas() {
		boolean bRegreso = true;
		
		//Se valida que las fechas no sean nulas
		if((getFeDesde() == null || getFeHasta() == null)){ 
			setStrRespuesta("Favor de ingresar fechas desde  y hasta para realizar la consulta");
			bRegreso = false;
		}
		
		//Se valida que fecha desde no se mayor a fecha hasta
		if(GestorFechas.comparaFechas(getFeDesde(), getFeHasta()) == -1) {
			setStrRespuesta("La fecha desde no puede ser mayor a la fecha hasta.");
			bRegreso = false;
		}
		
		return bRegreso;
	}
	
	/**
	 * Regresa propiedad nEjecuta
	 * @return the nEjecuta
	 */
	public int getnEjecuta() {
		return nEjecuta;
	}
	/**
	 * Settea la propiedad nEjecuta
	 * @param nEjecuta the nEjecuta to set
	 */
	public void setnEjecuta(int nEjecuta) {
		this.nEjecuta = nEjecuta;
	}
	/**
	 * @return the bean
	 */
	public String getBean() {
		FacesUtils.getBeanSesion().setBean(bean);
		return bean;
	}
	/**
	 * @param bean the bean to set
	 */
	public final void setBean(String bean) {
		this.bean = bean;
	}
	/**
	 * @return the filtro
	 */
	public StringBuilder getFiltro() {
		return filtro;
	}
	/**
	 * @param filtro the filtro to set
	 */
	public void setFiltro(StringBuilder filtro) {
		this.filtro = filtro;
	}
}