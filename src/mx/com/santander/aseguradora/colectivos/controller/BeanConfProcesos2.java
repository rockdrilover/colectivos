package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Configuracion de Procesos.
 * 				confProcesos.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanConfProcesos
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanConfProcesos2 extends BeanGenerico implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para editar o guardar nuevo objeto
	private BeanParametros seleccionado = new BeanParametros();
	//Propiedad para lista para combo de horas
	private List<Object> cmbHoras;
	//Propiedad para lista para combo de minutos
	private List<Object> cmbMinutos;
	//Propiedad para lista para combo de grupos
	private List<Object> cmbGrupos;
	

	/**
     * Constructor de clase
     */
	public BeanConfProcesos2() {
		super();
		
		//Se inicializan variables
		this.cmbHoras = new ArrayList<Object>();
		this.cmbMinutos = new ArrayList<Object>();
		this.cmbGrupos = new ArrayList<Object>();
	}
	
	/**
	 * @return the seleccionado
	 */
	public BeanParametros getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(BeanParametros seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the cmbHoras
	 */
	public List<Object> getCmbHoras() {
		return new ArrayList<>(cmbHoras);
	}
	/**
	 * @param cmbHoras the cmbHoras to set
	 */
	public void setCmbHoras(List<Object> cmbHoras) {
		this.cmbHoras = new ArrayList<>(cmbHoras);
	}
	/**
	 * @return the cmbMinutos
	 */
	public List<Object> getCmbMinutos() {
		return new ArrayList<>(cmbMinutos);
	}
	/**
	 * @param cmbMinutos the cmbMinutos to set
	 */
	public void setCmbMinutos(List<Object> cmbMinutos) {
		this.cmbMinutos = new ArrayList<>(cmbMinutos);
	}
	/**
	 * @return the cmbGrupos
	 */
	public List<Object> getCmbGrupos() {
		return new ArrayList<>(cmbGrupos);
	}
	/**
	 * @param cmbGrupos the cmbGrupos to set
	 */
	public void setCmbGrupos(List<Object> cmbGrupos) {
		this.cmbGrupos = new ArrayList<>(cmbGrupos);
	}
}