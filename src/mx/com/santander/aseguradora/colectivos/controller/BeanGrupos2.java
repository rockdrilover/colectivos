package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.utils.Validaciones2;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Configuracion de Grupos.
 * 				confGrupos.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanGrupos
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanGrupos2 extends BeanGrupos3 implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para lista de correos
	private List<String> lstCorreos;
	//Propiedad para nuevo objeto
	private BeanParametros objNuevo = new BeanParametros();
	
	/**
	 * Constructor de clase
	 */
	public BeanGrupos2() {
		super();
		
		//Se inicialisan valores por default
		this.lstCorreos = new ArrayList<String>();
		
	}
	
	/**
	 * Metodo que llena lista de correos
	 */
	public void setListaCorreos() {
		String[] arrCorreos = getObjActual().getCopaVvalor6().split("\\,");
		for (int i = 0; i < arrCorreos.length; i++) {
			lstCorreos.add(arrCorreos[i]);
		}
	}
		
	/**
	 * Metodo que agrega correo nuevo lista de correos
	 */
	public void agregar() {
		if(Validaciones2.validaEmail(getCorreo())) {
			lstCorreos.add(getCorreo());
			setCorreo(Constantes.DEFAULT_STRING);
		} else {
			FacesUtils.addErrorMessage("* Correo no es valido.");
		}
	}
	
	/**
	 * Metodo que quita de la lista de correos un email
	 */
	public void quitar() {
		List<String> lstTmp = new ArrayList<String>();
		
		for(String strCorreo: lstCorreos) {
			if(!getCorreo().equals(strCorreo)) {
				lstTmp.add(strCorreo);
			}
		}
				
		lstCorreos = new ArrayList<>();
		lstCorreos.addAll(lstTmp);
	}
				
	/**
	 * Metodo que cancela datos capturados en pantalla
	 */
	public void cancelar() {
		setObjNuevo(new BeanParametros());
		setObjActual(new BeanParametros());
		setCorreo(Constantes.DEFAULT_STRING);
		setLstCorreos(new ArrayList<String>());
	}
	
	/**
	 * Metodo que genera cadena de correos separados por ,
	 * @return cadena de correos
	 */
	public String setCorreos() {
		StringBuilder sbCorreos = new StringBuilder();	
		for (Object objCorreo : getLstCorreos()) {
			sbCorreos.append(objCorreo).append(",");
		}
		
		return Utilerias.quitarCaracteres(sbCorreos.toString(), ",", 2);
	}
	
	/**
	 * @return the lstCorreos
	 */
	public List<String> getLstCorreos() {
		return new ArrayList<String>(lstCorreos);
	}
	/**
	 * @param lstCorreos the lstCorreos to set
	 */
	public void setLstCorreos(List<String> lstCorreos) {
		this.lstCorreos = new ArrayList<String>(lstCorreos);
	}
	/**
	 * @return the objNuevo
	 */
	public BeanParametros getObjNuevo() {
		return objNuevo;
	}
	/**
	 * @param objNuevo the objNuevo to set
	 */
	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}
}