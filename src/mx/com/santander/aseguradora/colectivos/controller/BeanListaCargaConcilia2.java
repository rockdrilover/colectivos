package mx.com.santander.aseguradora.colectivos.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCuenta;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCarga;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioProcesos;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Resultado;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		21-11-2021
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Carga Concilicion.
 * 				cargaConciliaion.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanListaCargaConcilia
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class BeanListaCargaConcilia2 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder al servicio de carga
	@Resource
	protected transient ServicioCarga servicioCarga;
	
	//recurso para acceder al servicio de procesos
	@Resource
	protected transient ServicioProcesos servicioProcesos;

	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanListaCargaConcilia2.class);
	
	/**
	 * Constructor de clase
	 */
	public BeanListaCargaConcilia2() {
		//se manda ejecutar constructor de clase padre
		super();
	}
	
	/**
	 * Metodo que procesa un archivo de cuentas
	 * @param archivo archivo a procesar
	 * @param newFechaIni fecha de inicio
	 * @param newFechaFin fecha de fin
	 * @param arlFechas lista de fechas 
	 * @param nRegConsecutivo numero consecutivo
	 * @param objParam parametros de la cuenta
	 * @param nOperCargo operacion de cargo
	 * @param nOperAbono operacion de abono
	 * @param lstGuardar lista para guardar las cuentas
	 * @throws Excepciones con error en general
	 */
	public void procesaArchivo(File archivo, 
				Date newFechaIni, 
				Date newFechaFin, 
				List<Resultado> arlFechas, 
				int nRegConsecutivo, 
				Parametros objParam, 
				Integer nOperCargo, 
				Integer nOperAbono, 
				List<Object> lstGuardar) throws Excepciones {
		//Se inicialisan variables
		Double lMontoCargo = 0.0, lMontoAbono = 0.0;
		ColectivosCuenta objCuentas = null;
		Date fechaHoy = new Date();
		Long nCuentaPago = Constantes.DEFAULT_LONG;
		
		//try con recursos
		try (BufferedReader  br = new BufferedReader(new InputStreamReader(new FileInputStream(archivo), StandardCharsets.UTF_8))) {
			//se recorren lineas de archivo
			while(br.ready()) {
				//se crea objeto cuentas
				objCuentas = getRegistroCuentas(br.readLine(), newFechaIni, newFechaFin, arlFechas, String.valueOf(nRegConsecutivo), objParam);
				//se valida sea un registro correcto
				if(objCuentas.getCocuComentariosBco().equals(String.valueOf(Constantes.FLAG_REGISTRO_CORRECTO))) {
					objCuentas.setCocuComentariosBco(null);
					//se agrega cuenta a la lista
					lstGuardar.add(objCuentas);
					//se calculan montos
					setMontos(lMontoAbono, lMontoCargo, objCuentas.getId().getCocuTipoMovimiento(), objCuentas.getId().getCocuMontoPrima());
					//set cuenta pago
					nCuentaPago = objCuentas.getId().getCocuCuentaPago().longValue();
					//se suma al consecutivo
					nRegConsecutivo++;
				} 
			}
			//Se valida monto de abono
			if(lMontoAbono.compareTo(Constantes.DEFAULT_DOUBLE) > Constantes.DEFAULT_INT){
				//se guarda movimiento contable de abono 
				this.servicioProcesos.cargaCuentas(objParam.getCopaNvalor2(),nOperAbono,lMontoAbono,nCuentaPago,GestorFechas.formatDate(newFechaFin, Constantes.FORMATO_FECHA_UNO),GestorFechas.formatDate(fechaHoy, Constantes.FORMATO_FECHA_UNO));
			}
			//Se valida monto de cargo
			if(lMontoCargo.compareTo(Constantes.DEFAULT_DOUBLE) > Constantes.DEFAULT_INT){
				//se guarda movimiento contable de cargo
				this.servicioProcesos.cargaCuentas(objParam.getCopaNvalor2(),nOperCargo,lMontoCargo,nCuentaPago,GestorFechas.formatDate(newFechaFin, Constantes.FORMATO_FECHA_UNO),GestorFechas.formatDate(fechaHoy, Constantes.FORMATO_FECHA_UNO));
			}
		} catch (Excepciones | IOException e) {
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, "BeanListaCargaConcilia.procesaArchivo error", e);
			//errores de lectura
			FacesUtils.addErrorMessage("Error de lectura en el archivo");
		}	
	}
	
	/**
	 * Metodo que acumula los montos de cargo y abono
	 * @param lMontoAbono monto acumulado de abono
	 * @param lMontoCargo monto acumulado de cargo
	 * @param cocuTipoMovimiento tipo de movimiento
	 * @param cocuMontoPrima prima a sumar
	 */
	private void setMontos(Double lMontoAbono, 
			Double lMontoCargo, 
			String cocuTipoMovimiento,
			BigDecimal cocuMontoPrima) {
		//se valida el tipo moviemto
		if("A".equals(cocuTipoMovimiento)){
			//es movimiento de abono se suma prima a acumulado de abono
			lMontoAbono = lMontoAbono + cocuMontoPrima.doubleValue();
		}else{
			//es movimiento de cargo se suma prima a acumulado de cargo
			lMontoCargo = lMontoCargo + cocuMontoPrima.doubleValue();
		}
	}
	
	/**
	 * Metodo que obtiene un objeto cuenta de una linea del archvio
	 * @param strLinea linea del archivo
	 * @param newFechaIni fecha inicio
	 * @param newFechaFin fecha fin
	 * @param arlFechas lista de fechas 
	 * @param strConsecutivo consecutivo
	 * @param objParam parametros de cuenta 
	 * @return objeto cuenta 
	 * @throws Excepciones con errores de proceso
	 */
	private ColectivosCuenta getRegistroCuentas(String strLinea, Date newFechaIni, Date newFechaFin, List<Resultado> arlFechas, String strConsecutivo, Parametros objParam) throws Excepciones { 
		//se inicializan variables
		ColectivosCuenta objCuentas = null;
		String strFolio, strRemesa;
		
		try {
			
			//se obtiene remesa
			strRemesa = GestorFechas.formatDate(new Date(), "yyyyMM");
			//se valida que parte de la linea sea numerico
			if(!Utilerias.isNumeric(strLinea.substring(113, 149).trim())) {
				//no es numerico se marca linea como no valida
				objCuentas = new ColectivosCuenta();
				objCuentas.setCocuComentariosBco(String.valueOf(Constantes.FLAG_REGISTRO_NO_VALIDO));
				return objCuentas;
			}				
			//Se valida parte de la liena se 0 o que sea cuenta de LE
			if(!(new Integer(strLinea.substring(101, 109)) == 0 || "0014".equals(strLinea.substring(109, 113)))) {
				//no se cumple y se marca como linea no valida
				objCuentas = new ColectivosCuenta();
				objCuentas.setCocuComentariosBco(String.valueOf(Constantes.FLAG_REGISTRO_NO_VALIDO));
				return objCuentas;
			}
			
			//se crea objeto cuenta
			objCuentas = new ColectivosCuenta(strLinea, strConsecutivo);
			//se genera folio
			strFolio = getFolio(strLinea.substring(109, 149).trim(), GestorFechas.formatDate(objCuentas.getId().getCocuFechaIngreso(), "ddMMyyyy"));
			
			objCuentas.setCocuIdVenta(new BigDecimal(Utilerias.getIdVenta(Integer.parseInt(objParam.getCopaNvalor3().toString()), strFolio, strRemesa)));
			objCuentas.getId().setCocuNoCredito(strFolio);
			
			objCuentas.setCocuRegistro(strLinea.substring(32, 72).trim() + " | " + strLinea.substring(109, 149).trim());
			objCuentas.setCocuFechaCarga(new Date());
			objCuentas.setCocuRamo(new BigDecimal(objParam.getCopaNvalor2()));

			//validacion de fecha ingreso
			if(objCuentas.getId().getCocuFechaIngreso().after(newFechaIni) && objCuentas.getId().getCocuFechaIngreso().before(newFechaFin)){
				//se busca fecha en lista de fehas 
				if(GestorFechas.buscaFecha(objCuentas.getId().getCocuFechaIngreso(), new ArrayList<Resultado>(arlFechas) )){	
					//registro valido
					objCuentas.setCocuComentariosBco(String.valueOf(Constantes.FLAG_REGISTRO_CORRECTO));
				} else {
					//registro cargado
					objCuentas.setCocuComentariosBco(String.valueOf(Constantes.FLAG_REGISTRO_CARGADO));
				}
			} else {
				//registro fuera ed rango
				objCuentas.setCocuComentariosBco(String.valueOf(Constantes.FLAG_REGISTRO_FUERA_RANGO));
			}
			
		} catch (Exception e) {
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, "BeanListaCargaConcilia.getRegistroCuentas error", e);
			//error de proceso
			throw new Excepciones("Error:: BeanListaCargaConcilia.getRegistroCuentas:: " + e.getMessage());
		} 
		
		//se regresa objeto cuentas
		return objCuentas;
	}
	
	/**
	 * Metodo que genera folio
	 * @param strDescripcion descripcion de la linea
	 * @param fechaAprobacion fecha de aprobacion
	 * @return regresa numero de folio
	 * @throws Excepciones con error de proceso
	 */
	private String getFolio(String strDescripcion, String fechaAprobacion) {
		//Se declaran variables
		String[] arrDesc = strDescripcion.split(" ");
		String strCredito = Constantes.DEFAULT_STRING_ID;
		String tmpFecha = Constantes.DEFAULT_STRING;
		long tmpConsecutivo = Constantes.DEFAULT_LONG;
		
		if("CRE".equals(arrDesc[0]) || "ANU".equals(arrDesc[0])){
			strCredito = arrDesc[1];
		} else if(arrDesc[0].length() == 1 &&  Utilerias.isNumeric(arrDesc[0])){
			strCredito = arrDesc[0];
		} else if(Constantes.DEFAULT_STRING.equals(arrDesc[0])) {
			if(tmpFecha.equals(fechaAprobacion)){
				tmpConsecutivo++;
			} 
			strCredito = fechaAprobacion + String.valueOf(tmpConsecutivo);
			tmpFecha = fechaAprobacion;
		} else if("0014".equals(arrDesc[0].substring(0,4))) {
			strCredito = arrDesc[0].substring(4, arrDesc[0].length());
		} else if(Utilerias.isNumeric(arrDesc[0])){
			strCredito = arrDesc[0];
		}

		return strCredito;
	}
	
	/**
	 * @param servicioCarga the servicioCarga to set
	 */
	public void setServicioCarga(ServicioCarga servicioCarga) {
		this.servicioCarga = servicioCarga;
	}
	
	/**
	 * @param servicioProcesos the servicioProcesos to set
	 */
	public void setServicioProcesos(ServicioProcesos servicioProcesos) {
		this.servicioProcesos = servicioProcesos;
	}
}
