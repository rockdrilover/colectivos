package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		05-12-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Reporte Cobranza.
 * 				reporteCobranza.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanReporteCobranza
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class BeanReporteCobranza2 implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	//propiedad para seleccionar tipo de reporte
	protected int tipo;
	//propiedad para seleccionar estatus
	protected int estatus;
	//Clase para tener acceso a propiedades genericas.
	protected transient BeanGenerico beanGen;
	/**
	 * Constructor de clase
	 */
	public BeanReporteCobranza2() {
		//Se manda llamar constructpr clase padre
		super();
		
		//Se agrega el bean a session
		beanGen = new BeanGenerico();
		beanGen.setBean(BeanNames.BEAN_COBRANZA_REPORTES);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_COBRANZA_REPORTES);
		
	}
	
	/**
	 * @return the estatus
	 */
	public int getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	/**
	 * @return the tipo
	 */
	public int getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the beanGen
	 */
	public BeanGenerico getBeanGen() {
		return beanGen;
	}
	

}
