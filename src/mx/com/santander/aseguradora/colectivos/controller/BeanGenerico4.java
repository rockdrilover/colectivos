package mx.com.santander.aseguradora.colectivos.controller;

import java.io.File;
import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		30-06-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio para diferentes pantallas.
 * 				Esta clase contine solo propiedades que son genericas 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanGenerico4 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para saber que accion hacer
	private int nAccion;
	//Propiedad para trabajar con archivo
	private File archivo;
	//Propiedad para especificar numero de ramo
	private Short ramo;
	//Propiedad para respuesta en pantalla
	private String strRespuesta;
	
	
	/**
	 * Constructor de clase
	 */
	public BeanGenerico4() {
		//Se inicialisan valores por default
		this.strRespuesta = Constantes.DEFAULT_STRING;
		this.nAccion = Constantes.DEFAULT_INT;
		this.ramo = Constantes.DEFAULT_INT;
	}
	
	/**
	 * Regresa propiedad nAccion
	 * @return the nAccion
	 */
	public int getnAccion() {
		return nAccion;
	}
	/**
	 * Settea la propiedad nAccion
	 * @param nAccion the nAccion to set
	 */
	public void setnAccion(int nAccion) {
		this.nAccion = nAccion;
	}
	/**
	 * @return the ramo
	 */
	public Short getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(Short ramo) {
		this.ramo = ramo;
	}
	/**
	 * @return the archivo
	 */
	public File getArchivo() {
		return archivo;
	}
	/**
	 * @param archivo the archivo to set
	 */
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	/**
	 * Regresa propiedad strRespuesta
	 * @return the strRespuesta
	 */
	public String getStrRespuesta() {
		return strRespuesta;
	}
	/**
	 * Settea la propiedad strRespuesta
	 * @param strRespuesta the strRespuesta to set
	 */
	public void setStrRespuesta(String strRespuesta) {
		this.strRespuesta = strRespuesta;
	}
}