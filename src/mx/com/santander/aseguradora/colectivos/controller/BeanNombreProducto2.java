package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-11-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla producto comision.
 * 				nombreProducto.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	
 * 		Cuando: 
 * 		Porque:
 * ------------------------------------------------------------------------
 * 
 */

public class BeanNombreProducto2 implements Serializable {
	
	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para un nuevo registro
	protected transient BeanParametros objNuevo;
	
	//Propiedad para modificar registro
	protected transient BeanParametros objActual;
	
	//Propiedad para mostrar registros
	protected transient Map<String, Object> hmResultados;
	
	//Propiedad para obtener fila actual
	protected int filaActual;
	
	/**
	 * Constructor de clase
	 */
	public BeanNombreProducto2 () {
		super();
		
		//Se inicializan variables
		objNuevo = new BeanParametros();
		objActual = new BeanParametros();
		
	}
	
	/**
	 * @return the objNuevo
	 */
	public BeanParametros getObjNuevo() {
		return objNuevo;
	}
	/**
	 * @param objNuevo the objNuevo to set
	 */
	public void setObjNuevo(BeanParametros objNuevo) {
		this.objNuevo = objNuevo;
	}
	/**
	 * @return the objActual
	 */
	public BeanParametros getObjActual() {
		return objActual;
	}
	/**
	 * @param objActual the objActual to set
	 */
	public void setObjActual(BeanParametros objActual) {
		this.objActual = objActual;
	}
	/**
	 * @return the hmResultados
	 */
	public Map<String, Object> getHmResultados() {
		return hmResultados;
	}
	/**
	 * @param hmResultados the hmResultados to set
	 */
	public void setHmResultados(Map<String, Object> hmResultados) {
		this.hmResultados = hmResultados;
	}
	
	
}
