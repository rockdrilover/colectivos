package mx.com.santander.aseguradora.colectivos.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.PreCarga;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCancelaAtm;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCancelaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		09-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cancelacion Automatica.
 * 				cancelacionAutomatica.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanCancelaAtm extends BeanCancelaAtm2 {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio
	@Resource
	private ServicioCancelaAtm servicioCancela;	
	
	//propiedad para escribir en archivo log
	private static final Log LOG = LogFactory.getLog(BeanCancelaAtm.class);
	
	/**
	 * Constructor de clase
	 */
	public BeanCancelaAtm() {
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_CANCELA_ATM);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CANCELA_ATM);
	}
	
	/**
	 * Metodo que guarda en base de datos el archivo seleccionado
	 * @return mensage de exito o falla
	 */
	public String cargar() {
		String strForward = null;
		
		//Se validan datos antes de cargar Archivo
		if(validaDatos()) {
			//Se manda guardar los registros del archivo
			setStrRespuesta(this.servicioCancela.guardaDatosArchivo(new Archivo(getArchivo())));
							
			//Pasar a tipo proceso Automatico
			setFeDesde(new Date());
			setFeDesde(new Date());
			setNombreLayout(getArchivo().getName());
			
			strForward = NavigationResults.SUCCESS;
			getArchivo().delete();
		}
		
		return strForward;
	}
	
	/**
	 * Metodo que consulta el resumen de creditos a cancelar
	 * @return mensage de exito o falla
	 */
	public String consulta() {
		List<Object> lista;
		Iterator<Object> it;
		Object[] datos;
		ArrayList<ResumenCancelaDTO> arlLista;
		
		try {
			//Se inicializa lista
			arlLista = new ArrayList<ResumenCancelaDTO>();
			
			//Se validan datos antes de hacer consulta
			if(validaDatos()) {
				//Se realiza consulta para mostrar los procesos configurados
				lista = this.servicioCancela.consulta(getNombreLayout(), getFeDesde(), getFeHasta());
				 
				//se recorren resultados para crear objeto que se utiliza en la vista
				it = lista.iterator();
			    while(it.hasNext()) {
			    	datos = (Object[]) it.next();
			    	arlLista.add(new ResumenCancelaDTO(datos));
				}
			    
				//Se manda respuesta a pantalla
				setStrRespuesta("Se realizo consulta con exito, favor de revisar resultados");
			}

			//Manda lista a propiedad de pantalla
			setListaResumen(arlLista);
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los errores.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	private boolean validaDatos() {
		boolean bRegreso = true;
		
		//Validaciones segun tipo de proceso
		if(getTipoProc() == 1) {
			//Proceso Automatico
			//Se validan fechas
			bRegreso = validaFechas();
		} else {
			//Proceso Manual
			//Se valida archivo no sea nulo
			if(getArchivo() == null) {
				setStrRespuesta("Favor de seleccionar un archivo.");
				bRegreso = false;
			}
		}
		
		return bRegreso;
	}

	/**
	 * Metodo que consulta detalle de errores de un archivo
	 */
	public void getDetalle() {
		List<Object> lista;
		DetalleCancelaDTO dto;
		Iterator<Object> it;
		ArrayList<DetalleCancelaDTO> arlLista;
		
		try {
			//Se inicializan variables
			arlLista = new ArrayList<DetalleCancelaDTO>();
			setConteo("0 registros seleccionados");
			setChkAll(false);
			
			//Se realiza consulta para mostrar el detalle de creditos a cancelar
			lista = this.servicioCancela.getDetalle(getSeleccionado());
			 
			//se recorren resultados para crear objeto que se utiliza en la vista
			it = lista.iterator();
		    while(it.hasNext()) {
		    	dto = new DetalleCancelaDTO( (PreCarga)  it.next());
		    	arlLista.add(dto);
			}
		    
		    //Se manda a propiedad de pantalla
		    setLstDetalle(arlLista);
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar detalle.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
		
	/**
	 * Metodo que actualiza para cancelar los creditos seleccionados
	 */
	public void updateCreditos() {
		List<DetalleCancelaDTO> lista;
		try{
			lista = validaLista();
			
			if("ERR".equals(getSeleccionado().getDescVenta())) {
				FacesUtils.addErrorMessage("Los errores no se pueden cancelar.");
				return;
			}
			
			if(!lista.isEmpty()) {
				//Se realiza el update a los creditos seleccionados
				this.servicioCancela.cancelaCreditos(getSeleccionado(), lista);
				
				//Se limpian resultados
				this.limpiaResultados();
				
				//Se manda respuesta a pantalla
				setStrRespuesta("Se programaron creditos para cancelar.");
			}
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al actualizar creditos.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que elimina los creditos seleccionados con error
	 */
	public void deleteCreditos() {
		List<DetalleCancelaDTO> lista;
		
		try{
			lista = validaLista();
			
			if(!lista.isEmpty()) {
				//Se realiza el delete a los creditos seleccionados
				this.servicioCancela.eliminaCreditos(getSeleccionado(), lista);
				
				//Se limpian resultados
				this.limpiaResultados();
				
				//Se manda respuesta a pantalla
				setStrRespuesta("Se eliminaron creditos correctamente.");
			}
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al eliminar creditos.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}

	/**
	 * Metodo que valida la lista detalle
	 * @return nueva lista con creditos seleccionados
	 */
	private List<DetalleCancelaDTO> validaLista() {
		List<DetalleCancelaDTO> lista = new ArrayList<DetalleCancelaDTO>();
		DetalleCancelaDTO dto;
		Iterator<DetalleCancelaDTO> it;
		
		//Se valida que haya creditos para procesar
		if(getLstDetalle().isEmpty()) {
			FacesUtils.addErrorMessage("No hay creditos para procesar.");
			return lista;
		}

		//Se recorre lista detalle para procesar registros seleccionados
		it = getLstDetalle().iterator();
		while(it.hasNext()) {
			dto = it.next();
			if(dto.isCheck()) {
				lista.add(dto);
			}
		}

		//Se valida que hayan seleccionado al menos un credito para procesar
		if(lista.isEmpty()) {
			FacesUtils.addErrorMessage("Favor de seleccionar un credito de la lista.");
			return lista;
		}
		
		return lista;
	}

	/**
	 * Metodo que limpia los resultados
	 * @return  mensage de exito o falla
	 */
	public String limpiaResultados(){
		if(getListaResumen() != null){
			getListaResumen().clear();		
		} 
		
		setFeDesde(new Date());
		setFeHasta(new Date());
		setNombreLayout(Constantes.DEFAULT_STRING);
		
		return NavigationResults.SUCCESS;
	}
	
	/**
	 * @param servicioError the servicioError to set
	 */
	public void setServicioCancela(ServicioCancelaAtm servicioCancela) {
		this.servicioCancela = servicioCancela;
	}
	
}
