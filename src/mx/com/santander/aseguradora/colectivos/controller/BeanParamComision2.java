package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanListaParametros;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Comisiones.
 * 				parametrizacionComisiones.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanParamComision
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanParamComision2 extends BeanParamComision3 implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
		
	//Log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(BeanParamComision2.class);
	
	//Propiedad para mostrar comisiones
	private Integer showComisiones;
	
	/**
	 * Constructor de clase
	 */
	public BeanParamComision2() {
		//se manda ejecutar constructor de clase padre
		super();

		//Se inicialisan valores por default
		this.showComisiones = Constantes.DEFAULT_INT;
	}

	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	protected boolean validaDatos() {
		boolean bRegreso = true;
		
		//Valida si selecciono un ramo
		if(getTipoParam() == Constantes.DEFAULT_INT) { 
			setStrRespuesta("Favor de seleccionar una forma de comisionar");
			return false;
		}
		
		bRegreso = validaFechas();
		
		return bRegreso;
	}
	
	/**
	 * Metodo que valida los datos del registro a editar
	 * @param objParam datos a validar
	 * @return falso o verdadero
	 */
	protected boolean validaDatosGuardar() {
		boolean bRegreso = true;
		
		//Valida se haya seleccionado una forma de comisionar
		if(getObjActual().getCopaNvalor5() == Constantes.DEFAULT_INT) {
			FacesUtils.addErrorMessage("* FORMA COMISIONAR - Campo Requerido.");
			return  false;
		} 
		
		//Valida se haya seleccionado un ramo
		if(getRamo() == Short.valueOf(Constantes.DEFAULT_STRING_ID)) {
			FacesUtils.addErrorMessage("* RAMO - Campo Requerido.");
			bRegreso = false;
		} 
		
		if(getObjActual().getCopaNvalor5() == 1 && getPoliza() == Constantes.DEFAULT_INT) {
			FacesUtils.addErrorMessage("* Poliza - Campo Requerido.");
			bRegreso = false;
		}
		
		if(bRegreso == false) {
			return bRegreso;
		}
		
		switch (getObjActual().getCopaNvalor5()) {
			case 1:	//Por Poliza
				getObjActual().setCopaNvalor3(new Long(getPoliza()));
				getObjActual().setCopaNvalor4(Constantes.DEFAULT_INT);
				getObjActual().setCopaVvalor1(Constantes.DEFAULT_STRING_ID);
				getObjActual().setCopaVvalor2(Constantes.DEFAULT_STRING_ID);
				getObjActual().setCopaNvalor6(Constantes.DEFAULT_DOUBLE);
				break;
			
			case 2:	//Por Producto
				bRegreso = validaProTar();
				getObjActual().setCopaNvalor3(Constantes.DEFAULT_LONG);
				getObjActual().setCopaNvalor4(getIdVenta());
				getObjActual().setCopaVvalor2("-1");
				getObjActual().setCopaNvalor6((double) -1);
				break;
				
			case 3: //Por tarifa
				bRegreso = validaProTar();
				getObjActual().setCopaNvalor3(Constantes.DEFAULT_LONG);
				getObjActual().setCopaNvalor4(getIdVenta());
				getObjActual().setCopaNvalor6((double) -2);
				break;
				
			default: //Por Anio
				bRegreso = validaProTar();
				getObjActual().setCopaNvalor3(Constantes.DEFAULT_LONG);
				getObjActual().setCopaNvalor4(getIdVenta());
				getObjActual().setCopaVvalor2("-3");
				break;
		}
			
		//Valida se haya ingresado una descripcion
		if(getObjActual().getCopaVvalor8().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* DESCRIPCION - Campo Requerido.");
			bRegreso = false;
		}
		
		return bRegreso;
	}
	
	/**
	 * Metdodo que valida se haya seleccionado idventa, producto o tarifa
	 * @return
	 */
	private boolean validaProTar() {
		boolean bRegreso = true;
		
		if(getIdVenta() == Constantes.DEFAULT_INT) {
			FacesUtils.addErrorMessage("* ID VENTA - Campo Requerido.");
			bRegreso =  false;
		} else if(getObjActual().getCopaVvalor1().equals(Constantes.DEFAULT_STRING_ID)) {
			FacesUtils.addErrorMessage("* PRODUCTO - Campo Requerido.");
			bRegreso =  false;
		} else if(getObjActual().getCopaNvalor5() == 3 && getObjActual().getCopaVvalor2().equals(Constantes.DEFAULT_STRING_ID)) {
			FacesUtils.addErrorMessage("* TARIFA - Campo Requerido.");
			bRegreso =  false;
		} else if(getObjActual().getCopaNvalor5() == 4 && getObjActual().getCopaNvalor6().equals(Constantes.DEFAULT_DOUBLE)) {
			FacesUtils.addErrorMessage("* A�O - Campo Requerido.");
			bRegreso =  false;
		}  
		
		return bRegreso;
	}

	/**
	 * Metodo que limpia combos de pantalla nueva comision
	 */
	public void limpiaCombos() {
		setRamo(Short.valueOf(Constantes.DEFAULT_STRING_ID));
		setPoliza(Constantes.DEFAULT_INT);
		setIdVenta(Constantes.DEFAULT_INT);
		
		((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).getListaComboPolizas().clear();
		((BeanListaParametros) FacesUtils.getManagedBean(BeanNames.BEAN_LISTA_PARAMETROS)).getListaComboIdVenta().clear();
		getCmbProductos().clear();
		getCmbTarifas().clear();
		
		setShowComisiones(Constantes.DEFAULT_INT);
	}
	
	/**
	 * Metodo que cancela datos capturados en pantalla
	 */
	public void cancelar() {
		init();
		limpiaCombos();
	}
	
	/**
	 * Metodo que muestra apartado de comisiones
	 */
	public void setValores() {
		setShowComisiones(1);
	}
	
	/**
	 * Metodo que setea datos al objeto a editar
	 */
	public void setDatosEditar() {
		StringBuilder sb;
		String[] arrComisiones;
		
		try {
			//Set dato auxiliar (identificador de regla)
			sb = new StringBuilder();
			sb.append(getObjActual().getCopaNvalor2().toString()).append(" - ");
			sb.append(getObjActual().getCopaNvalor3().toString()).append(" - ");
			sb.append(getObjActual().getCopaNvalor4().toString()).append(" - ");
			sb.append(getObjActual().getCopaVvalor1()).append(" - ");
			sb.append(getObjActual().getCopaVvalor2());
			getObjActual().setAuxDato(sb.toString());
			
			//Set descripcion cobertura
			getObjActual().setCopaRegistro2("Vida");
			if(getObjActual().getCopaNvalor7() == 2) {
				getObjActual().setCopaRegistro2("Desempleo");
			}
			
			//Set fechas de vigencia
			sb = new StringBuilder();
			sb.append(GestorFechas.formatDate(getObjActual().getCopaFvalor1(), Constantes.FORMATO_FECHA_UNO)).append(" al ");
			sb.append(GestorFechas.formatDate(getObjActual().getCopaFvalor2(), Constantes.FORMATO_FECHA_UNO));
			getObjActual().setCopaRegistro3(sb.toString());
			
			//Set comisiones
			arrComisiones = getObjActual().getCopaRegistro().split("\\$");
			
			//Set Comisiones Tecnicas Emision
			getTecnicaEmi().setComisiones(arrComisiones[0], Constantes.COMISION_EMISION);
			
			//Set Comisiones Tecnicas Renovacion
			getTecnicaReno().setComisiones(arrComisiones[0], Constantes.COMISION_RENOVACION);
			
			//Set Comisiones Comerciales Emision
			getComercialEmi().setComisiones(arrComisiones[1], Constantes.COMISION_EMISION);
			
			//Set Comisiones Comerciales Renovacion
			getComercialReno().setComisiones(arrComisiones[1], Constantes.COMISION_RENOVACION);
		} catch (Exception e) {
			//Cualqueir error se reporta al LOG
			LOG.error("Error al set datos para editar", e);
		}
	}

	/**
	 * @return the showComisiones
	 */
	public Integer getShowComisiones() {
		return showComisiones;
	}
	/**
	 * @param showComisiones the showComisiones to set
	 */
	public void setShowComisiones(Integer showComisiones) {
		this.showComisiones = showComisiones;
	}
}