package mx.com.santander.aseguradora.colectivos.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Configuracion de Procesos.
 * 				confProcesos.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanConfProcesos extends BeanConfProcesos2 {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio
	@Resource
	private ServicioParametros servicioParametros;
	
	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanConfProcesos.class);
	
	//Propiedad para lista de procesos
	private List<BeanParametros> lstProcesos;
	
	/**
     * Constructor de clase
     */
	public BeanConfProcesos() {
		super();
		
		//Se inicializan variables
		this.lstProcesos = new ArrayList<BeanParametros>();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_CONF_PROCESOS);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONF_PROCESOS);
	}

	/**
	 * Metodo para consultar procesos control-m configurados
	 * @param nSetValores badera para saber si se consultan otras tablas
	 */
	public void consultaProcesos(Integer nSetValores){
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean;
		List<Object> lstResultados;
		Parametros parametro;
		Iterator<Object> it;
		
		try {
			//Se hace validacion para no volver a consultar inforamcion de catalogos
			if(nSetValores == Constantes.DEFAULT_INT) {
		    	setCmbHoras();
			    setCmbMinutos();
			    consultaGrupos();
		    }
			
			//Se realiza consulta para mostrar los procesos configurados
			lstProcesos = new ArrayList<BeanParametros>();
			filtro.append("and P.copaDesParametro = 'PROCESOS CONTROL-M'" );
			filtro.append("order by P.copaNvalor1" );
			lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
			
			//se recorren resultados para crear objeto que se utiliza en la vsita
		    it = lstResultados.iterator();
		    while(it.hasNext()) {
		    	parametro = (Parametros) it.next();
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
				bean.setServicioParametros(this.servicioParametros);
				bean.setCopaVvalor31(bean.getCopaVvalor4().split("\\:")[0]);
				bean.setCopaVvalor32(bean.getCopaVvalor4().split("\\:")[1]);
				bean.setCopaVvalor33(getNombreGrupo(bean.getCopaNvalor2()));
				
				lstProcesos.add(bean);
			}
		    
		    //se setaa a valor uno para que no se vuelvan a consultar catalogos
			setnEjecuta(1);
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("No se pueden cargar procesos configurados", e);
		}
	}
	
	/**
	 * Metodo para buscar el nombre del grupo
	 * @param copaNvalor1 id de grupo
	 * @return nombre del grupo
	 */
	private String getNombreGrupo(Long idGrupo) {
		String nombreGrupo = Constantes.DEFAULT_STRING;
		
		//Se valida que el id grupo no venga null
		idGrupo = (Long)Utilerias.objectIsNull(idGrupo, Constantes.TIPO_DATO_LONG);
		
		//Se valida que la lista de grupos no este vacia
		if(!getCmbGrupos().isEmpty()) {
			SelectItem item;
			Iterator<Object> it;
			it = getCmbGrupos().iterator();

			//Se rocorre la lista de grupos para buscar el id de grupo correspondiente
			while(it.hasNext()) {
				item = (SelectItem)it.next();
				if(item.getValue().equals(idGrupo)) {
					nombreGrupo = item.getLabel();
					break;
				}
			}
		}
					
		return nombreGrupo;
	}

	/**
	 * Metodo que crea valores para el combo de horas
	 * Se deja a 24 horas (de 00  23)
	 */
	private void setCmbHoras() {
		ArrayList<Object> lstHoras = new ArrayList<Object>();
		
		//se hace un ciclo para ir generando los items
		for (int i = 0; i < 24; i++) {
			lstHoras.add(new SelectItem(Utilerias.agregarCaracteres(String.valueOf(i), 1, '0', 1), Utilerias.agregarCaracteres(String.valueOf(i), 1, '0', 1)));
		}
		
		//Se pasa lista a propiedad de la vista
		setCmbHoras(lstHoras);
	}
	
	/**
	 * Metodo que crea valores para el combo de minutos
	 * Se deja a 60 minutos (de 00 a 59)
	 */
	private void setCmbMinutos() {
		ArrayList<Object> lstMinutos = new ArrayList<Object>();

		//se hace un ciclo para ir generando los items
		for (int i = 0; i < 60; i++) {
			lstMinutos.add(new SelectItem(Utilerias.agregarCaracteres(String.valueOf(i), 1, '0', 1), Utilerias.agregarCaracteres(String.valueOf(i), 1, '0', 1)));
		}
		
		//Se pasa lista a propiedad de la vista
		setCmbMinutos(lstMinutos);
	}
	
	/**
	 * Metodo para consultar grupos de correo que 
	 * estan dados de alta en la base de datos
	 */
	public void consultaGrupos(){
		StringBuilder filtro = new StringBuilder(100);
		List<Object> lstResultados;
		ArrayList<Object> arlLista;
		Parametros parametro;
		Iterator<Object> it;
		
		try {
			arlLista = new ArrayList<Object>();

			//Se realiza consulta para mostrar los procesos configurados
			filtro.append("and P.copaDesParametro = 'CATALOGO'" );
			filtro.append("and P.copaIdParametro = 'GRUPOS'" );
			filtro.append("order by P.copaNvalor1" );
			lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());

			//se recorren resultados para crear objeto que se utiliza en la vsita
		    it = lstResultados.iterator();
		    while(it.hasNext()) {
		    	parametro = (Parametros) it.next();
		    	arlLista.add(new SelectItem(parametro.getCopaNvalor1().longValue(), parametro.getCopaVvalor8()));
			}
		    
		    //Se pasa lista a propiedad de la vista
		    setCmbGrupos(arlLista);
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("No se pueden cargar gruposs", e);
		}
	}
	
	/**
	 * Metodo que modifica registro seleccionado
	 */
	public void modificar(){
		try {
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);

			//Se pasan valores necesarios para la modificacion
			getSeleccionado().setCopaVvalor4(getSeleccionado().getCopaVvalor31() + ":" + getSeleccionado().getCopaVvalor32());
			getSeleccionado().setCopaNvalor3(Constantes.DEFAULT_LONG);
			
			//Se construye objeto para poder realizar la modificacion con los nuevos valores asignados 
			Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getSeleccionado()) ;
			servicioParametros.actualizarObjeto(parametro);
			
			//Se consultas los procesos
			consultaProcesos(1);
			
			//Se manda respuesta a pantalla
			setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			setStrRespuesta("Error al MODIFICAR registro.");
			LOG.error("No se puede modificar registro", e);
		}
	}
	
	/**
	 * @return the lstProcesos
	 */
	public List<BeanParametros> getLstProcesos() {
		//se valida si se vuelve a ejecutar consulta para llenas lista de procesos
		if(getnEjecuta() == Constantes.DEFAULT_INT) {
			//Se consultas los procesos
			consultaProcesos(Constantes.DEFAULT_INT);
		}
		return new ArrayList<>(lstProcesos);
	}
	/**
	 * @param lstProcesos the lstProcesos to set
	 */
	public void setLstProcesos(List<BeanParametros> lstProcesos) {
		this.lstProcesos = new ArrayList<>(lstProcesos);
	}

	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
}