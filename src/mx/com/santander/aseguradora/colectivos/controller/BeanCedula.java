package mx.com.santander.aseguradora.colectivos.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCedulaControl;
import mx.com.santander.aseguradora.colectivos.utils.Archivo;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.CedulaControlDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cedula Control.
 * 				cedulacontrol.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanCedula extends BeanCedula2 {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio
	@Resource
	private ServicioCedulaControl servicioCedula;	

	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanCedula.class);

	/**
	 * Constructor de clase
	 */
	public BeanCedula() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_CEDULA);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CEDULA);
	}

	/**
	 * Metodo que consulta el resumen de errores por archivo
	 * @return mensage de exito o falla
	 */
	public String consulta(){
		Integer tipoCedHist = 3;
		
		try {
			//Se inicializa lista
			setListaActual(new ArrayList<CedulaControlDTO>());
			setListaHist(new ArrayList<CedulaControlDTO>());
			
			//Se validan datos antes de hacer consulta
			if(this.validaDatos()) {
				//Se realiza consulta para mostrar los procesos configurados
				setListaActual(this.servicioCedula.consulta(getTipocedula(), getIdVenta(), getFeDesde(), getFeHasta(), null));
				
				//Se realiza consulta para mostrar los procesos configurados
				if(getTipocedula() == 2) {
					tipoCedHist = 4;
				}
				setListaHist(this.servicioCedula.consulta(tipoCedHist, getIdVenta(), getFeDesde(), getFeHasta(), null));
				
				//Se manda respuesta a pantalla
				setStrRespuesta("Se realizo consulta con exito, favor de revisar resultados");
			}
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los errores.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}

	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	private boolean validaDatos() {
		boolean bRegreso = true;
		
		//Se vaalida se selecciona un tipo cedula
		if(getTipocedula() == Constantes.DEFAULT_INT) {
			setStrRespuesta("Favor de seleccionar Tipo Cedula para realizar la consulta");
			return false;
		}
		
		bRegreso = validaFechas();
		
		return bRegreso;
	}
	
	/**
	 * Metodo que consulta detalle de errores de un archivo
	 */
	public void getDetalle() {
		ArrayList<CedulaControlDTO> arlLista;
		
		try{
			//Se inicializan variables
			arlLista = new ArrayList<CedulaControlDTO>();
			setListaDetalle(new ArrayList<CedulaControlDTO>());
			
			//Se realiza consulta para mostrar los procesos configurados
			arlLista.addAll(this.servicioCedula.consulta(getSeleccionado().getCoccTipoCedula().intValue(), 
														getIdVenta(), 
														getFeDesde(), 
														getFeHasta(), 
														getSeleccionado().getCoccRemesaCedula()));

			//Se ordena lista por fecha de comercializacion
			Collections.sort(arlLista, new Comparator<CedulaControlDTO>() {
				@Override
				public int compare(CedulaControlDTO o1, CedulaControlDTO o2) {
					return o1.getCoccFeComer().compareTo(o2.getCoccFeComer());
				}
			});
			
			//Se pasa lista a propiedad de pantalla
			setListaDetalle(arlLista);
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar detalle.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}

	/**
	 * Descarga detalle de creditos de la cedula
	 */
	public void descargaDetalle() {
		//Declaracion de Variables
		String strNombre = null;
		String strRutaTemp;
		Archivo objArchivo;
		String[] arrColumnas = null;
		
		try {
			//Declaramos encabezados en array de columnas
			arrColumnas = new String[9];					
			arrColumnas[0] = "CREDITO";
			arrColumnas[1] = "FECHAINGRESO";
			arrColumnas[2] = "FECHAPROCESO";
			arrColumnas[3] = "APLICAPND";
			arrColumnas[4] = "PRIMA";
			arrColumnas[5] = "ESTATUS";
			arrColumnas[6] = "POLIZA";
			arrColumnas[7] = "CERTIFICADO";
			arrColumnas[8] = "RECHAZO";
						
			//Se asigna variable para ruta temporal
			strRutaTemp = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
			//Se asigna variable para nombre de archivo
			strNombre = "Cedula " + getSeleccionado().getCoccRemesaCedula() + " Detalle Creditos - " + new SimpleDateFormat("ddMMyyyy").format(new Date()); 
			
			objArchivo = new Archivo(strRutaTemp, strNombre, ".csv");
			objArchivo.copiarArchivo(arrColumnas, this.servicioCedula.getDetalleCreditos(getSeleccionado().getCoccTipoCedula().intValue(), getIdVenta(), getSeleccionado().getCoccRemesaCedula()), ",");
			objArchivo.descargarArchivo(FacesUtils.getServletResponse(), 2);
			objArchivo.eliminarArchivo();
			
			setStrRespuesta("Archivo descargado satisfactoriamente.");
			FacesUtils.addErrorMessage(getStrRespuesta());
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al generar archivo detalle.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que guarda una foto de la cedula
	 * @return  mensage de exito o falla
	 */
	public void saveFoto(){
		try {
			
			//Si petision viene de histoicos no gace rrespaldo
			if(getSeleccionado().getCoccTipoCedula().intValue() > 2) {
				FacesUtils.addErrorMessage("Para cedulas historicas no se puede guardar una foto.");
				return;
			}
			
			//Se manda llamar servicio
			this.servicioCedula.saveFotoCedula(getTipocedula(), getIdVenta(), getSeleccionado().getCoccRemesaCedula());

			//Se consultan resultados
			consulta();
			
			//Se manda respuesta de Exito
			setStrRespuesta("Se guardo foto satisfactoriamente.");
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al guardar foto.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que limpia los resultados
	 * @return  mensage de exito o falla
	 */
	public String limpiaResultados(){
		if(getListaActual() != null){
			getListaActual().clear();		
		} 
		
		setFeDesde(new Date());
		setFeHasta(new Date());
		
		return NavigationResults.SUCCESS;
	}
	
	/**
	 * @param servicioCedula the servicioCedula to set
	 */
	public void setServicioCedula(ServicioCedulaControl servicioCedula) {
		this.servicioCedula = servicioCedula;
	}
}
