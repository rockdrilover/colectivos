package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		30-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio para diferentes pantallas.
 * 				Esta clase contine solo propiedades que son genericas 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanGenerico2 extends BeanGenerico3 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para numero poliza
	private Integer poliza;
	//Propiedad para numero idventa
	private Integer idVenta;
	//Propiedad para polizas especifica
	private String polEsp;
	//Propiedad para numero de certificado
	private String certificado;
	
	/**
	 * Constructor de clase
	 */
	public BeanGenerico2() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.poliza = Constantes.DEFAULT_INT;
		this.idVenta = Constantes.DEFAULT_INT;
	}

	/**
	 * @return the poliza
	 */
	public Integer getPoliza() {
		return poliza;
	}
	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(Integer poliza) {
		this.poliza = poliza;
	}
	/**
	 * @return the idVenta
	 */
	public Integer getIdVenta() {
		return idVenta;
	}
	/**
	 * @param idVenta the idVenta to set
	 */
	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}
	/**
	 * @return the polEsp
	 */
	public String getPolEsp() {
		return polEsp;
	}
	/**
	 * @param polEsp the polEsp to set
	 */
	public void setPolEsp(String polEsp) {
		this.polEsp = polEsp;
	}
	/**
	 * @return the certificado
	 */
	public String getCertificado() {
		return certificado;
	}
	/**
	 * @param certificado the certificado to set
	 */
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	
}