package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCancelaDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResumenCancelaDTO;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cancelacion Automatica.
 * 				cancelacionAutomatica.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanCancelaAtm
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanCancelaAtm2 extends BeanCancelaAtm3 implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para lista de resumen
	private List<ResumenCancelaDTO> listaResumen;
	//Propiedad para lista de detalle
	private List<DetalleCancelaDTO> lstDetalle;
	//Propiedad para indicar tipo proceso
	private Integer tipoProc;
	//Propiedad para nombre de layout
	private String nombreLayout;
	
	/**
	 * Constructor de clase
	 */
	public BeanCancelaAtm2() {
		super();
		
		//Se inicialisan valores por default
		this.listaResumen = new ArrayList<ResumenCancelaDTO>();
		this.lstDetalle = new ArrayList<DetalleCancelaDTO>();
		this.nombreLayout = Constantes.DEFAULT_STRING;
	}
	
	/**
	 * Metodo que selecciona todos los creditos a cancelar 
	 * @return  mensage de exito o falla
	 */
	public String checkAll(){
		if(!lstDetalle.isEmpty()) {
			Iterator<DetalleCancelaDTO> it;
			it = lstDetalle.iterator();
			
			while(it.hasNext()) {
				if(isChkAll()) {
					it.next().setCheck(true);
					setConteo(lstDetalle.size() +  " registros seleccionados");
				} else {
					it.next().setCheck(false);
					setConteo("0 registros seleccionados");
				}
			}
		}
		
		return NavigationResults.SUCCESS;
	}

	/**
	 * @return the listaResumen
	 */
	public List<ResumenCancelaDTO> getListaResumen() {
		return  new ArrayList<ResumenCancelaDTO>(listaResumen);
	}
	/**
	 * @param listaResumen the listaResumen to set
	 */
	public void setListaResumen(List<ResumenCancelaDTO> listaResumen) {
		this.listaResumen = new ArrayList<ResumenCancelaDTO>(listaResumen);
	}
	/**
	 * @return the lstDetalleArchivo
	 */
	public List<DetalleCancelaDTO> getLstDetalle() {
		return new ArrayList<DetalleCancelaDTO>(lstDetalle);
	}
	/**
	 * @param lstDetalleArchivo the lstDetalleArchivo to set
	 */
	public void setLstDetalle(List<DetalleCancelaDTO> lstDetalle) {
		this.lstDetalle = new ArrayList<DetalleCancelaDTO>(lstDetalle);
	}
	/**
	 * @return the tipoProc
	 */
	public Integer getTipoProc() {
		return tipoProc;
	}
	/**
	 * @param tipoProc the tipoProc to set
	 */
	public void setTipoProc(Integer tipoProc) {
		this.tipoProc = tipoProc;
	}
	/**
	 * @return the nombreLayout
	 */
	public String getNombreLayout() {
		return nombreLayout;
	}
	/**
	 * @param nombreLayout the nombreLayout to set
	 */
	public void setNombreLayout(String nombreLayout) {
		this.nombreLayout = nombreLayout;
	}
}
