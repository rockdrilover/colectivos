package mx.com.santander.aseguradora.colectivos.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.model.SelectItem;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParamTimbrado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;
import mx.com.santander.aseguradora.colectivos.view.dto.ParamTimbradoDTO;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Timbrado.
 * 				paramTimbrado.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanParamTimbrado extends BeanParamTimbrado2 {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder al servicio de parametros
	@Resource
	private ServicioParametros servicioParametros;
	
	//recurso para acceder al servicio de param timbrado
	@Resource
	private ServicioParamTimbrado servicioParamTimbrado;

	//Log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(BeanParamTimbrado.class);
	
	//Propiedad para lista de resultados
	private List<ParamTimbradoDTO> lstPolizas;

	/**
	 * Constructor de clase
	 */
	public BeanParamTimbrado() {
		//se manda ejecutar constructor de clase padre
		super();
		//Se inicialisan valores por default
		this.lstPolizas = new ArrayList<ParamTimbradoDTO>();
		
	}
	
	/**
	 * Metodo que consulta polizas parametrizadas
	 * @return Exito o Error
	 */
	public String consulta() {
		StringBuilder filtro = new StringBuilder(100);
		
		try {
			lstPolizas.clear();
			
			//Se validan datos para poder hacer consulta
			if(validaDatos()) {
				//Se consultan datos
				lstPolizas = this.servicioParamTimbrado.consultaDatos(1, beanGen.getRamo(), beanGen.getPoliza(), beanGen.getIdVenta());
				lstNombreProducto =  this.servicioParamTimbrado.getNombreProducto();
				//se consulta catalogo de metodos de pago
				if(beanGen.getnEjecuta() == Constantes.DEFAULT_INT) {
					setCmbMetodosPago(this.servicioParamTimbrado.getComboCatalogosCFDI(Constantes.CFDI_METODOS_PAGO));
				}
				
				//Se busca el metodo de pago generico (por default)
				filtro.append("and P.copaDesParametro = 'CFDI' " );
				filtro.append("and P.copaIdParametro = 'DATOS'" );
				setMetodoPago(getMetodoPago( ((Parametros)this.servicioParametros.obtenerObjetos(filtro.toString()).get(0)).getCopaVvalor4() ));
				
				beanGen.setnEjecuta(1);
				
				if(beanGen.getnAccion() == Constantes.DEFAULT_INT) {
					//Se manda respuesta a pantalla
					beanGen.setStrRespuesta("Se ejecuto con EXITO la consulta, favor de desplegar los resultados.");
				}
			}
			
			//se regresa mensage de SUCCESS a la vista
		    return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			beanGen.setStrRespuesta("Error al consultar los grupos..");
			//Cualqueir error se reporta al LOG
			LOG.error(beanGen.getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}

	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	private boolean validaDatos() {
		boolean bRegreso = true;
		
		//Valida si selecciono un ramo
		if(beanGen.getRamo() == Constantes.DEFAULT_INT) { 
			beanGen.setStrRespuesta("Favor de seleccionar un ramo");
			bRegreso = false;
		}
		
		//Valida si selecciono un id venta
		if(beanGen.getPoliza() == Constantes.DEFAULT_INT && beanGen.getIdVenta() == Constantes.DEFAULT_INT) { 
			beanGen.setStrRespuesta("Favor de seleccionar un id de venta.");
			bRegreso = false;
		}
		
		return bRegreso;
	}
	
	/**
	 * Metodo para buscar el nombre del metodo de pago
	 * @param copaNvalor1 id de grupo
	 * @return nombre del grupo
	 */
	private String getMetodoPago(String clave) {
		String nomMetodoPago = Constantes.DEFAULT_STRING;
		
		//Se valida que la clave del metodo de pago no venga null
		clave = (String)Utilerias.objectIsNull(clave, Constantes.TIPO_DATO_STRING);
		
		//Se valida que la lista de metodo de pago no este vacia
		if(!getCmbMetodosPago().isEmpty()) {
			SelectItem item;
			Iterator<Object> it;
			it = getCmbMetodosPago().iterator();

			//Se rocorre la lista de metodo de pago para buscar la clave correspondiente
			while(it.hasNext()) {
				item = (SelectItem)it.next();
				if(item.getValue().equals(clave)) {
					nomMetodoPago = clave + " - " + item.getLabel();
					break;
				}
			}
		}
					
		return nomMetodoPago;
	}
	
	/**
	 * Metodo que modifica un registro de grupo
	 */
	public void modificar(){
		Long secParametro;

		try {
			//Se validan datos antes de hacer la modificacion
			if(validaDatos(getObjActual().getObjParam())) {
				//Se construye objeto para poder realizar la modificacion con los nuevos valores asignados 
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getObjActual().getObjParam());
				
				//Validamos si se guarda o actualiza
				if(parametro.getCopaCdParametro() == null) {
					//se consulta el siguiente id parametro
					secParametro = servicioParametros.siguente();
					
					//Se pasan valores necesarios para el guardado
					parametro.setCopaCdParametro(secParametro); 	 
					parametro.setCopaIdParametro("TIMBRADO");
					parametro.setCopaDesParametro("POLIZA");
					
					//Se guarda nuevo registro
					this.servicioParametros.guardarObjeto(parametro);
					
					//Se setea accion a 1
					beanGen.setnAccion(1);
				} else {
					//Se setea accion a 2
					beanGen.setnAccion(2);
					
					//Se actualiza registro
					this.servicioParametros.actualizarObjeto(parametro);
				}
				
				//objeto de  cobranza 
				Parametros  parametroCobranza = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getObjActual().getObjCobranza());
								
				//Validamos si se guarda o actualiza
				if(parametroCobranza.getCopaCdParametro() == null) {
					//se consulta el siguiente id parametro
					secParametro = servicioParametros.siguente();
					
					//Se pasan valores necesarios para el guardado
					parametroCobranza.setCopaNvalor1(parametro.getCopaNvalor1());
					parametroCobranza.setCopaNvalor2(parametro.getCopaNvalor2());
					parametroCobranza.setCopaNvalor3(parametro.getCopaNvalor3());
					parametroCobranza.setCopaNvalor4(parametro.getCopaNvalor4());
					parametroCobranza.setCopaCdParametro(secParametro); 	 
					parametroCobranza.setCopaIdParametro("COBRANZA");
					parametroCobranza.setCopaDesParametro("POLIZA");
					
					//Se guarda nuevo registro
					this.servicioParametros.guardarObjeto(parametroCobranza);
					
					//Se setea accion a 1
					beanGen.setnAccion(1);
				}else {
					//Se setea accion a 2
					beanGen.setnAccion(2);
					
					//Se actualiza registro
					this.servicioParametros.actualizarObjeto(parametroCobranza);
				}
				
				//se manda llamar metodo de cancelar para setear valores por default
				cancelar();
				
				//Se consultas los grupos
				consulta();

				//Se manda respuesta a pantalla
				beanGen.setStrRespuesta("Se modifico con EXITO el registro.");
			}
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			beanGen.setStrRespuesta("Error al MODIFICAR registro.");
			//Cualqueir error se reporta al LOG
			LOG.error(beanGen.getStrRespuesta(), e);
		}
	}

	/**
	 * Metodo que cancela datos capturados en pantalla
	 */
	public void cancelar() {
		setObjActual(new ParamTimbradoDTO());
	}
	
	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	/**
	 * @param servicioParamTimbrado the servicioParamTimbrado to set
	 */
	public void setServicioParamTimbrado(ServicioParamTimbrado servicioParamTimbrado) {
		this.servicioParamTimbrado = servicioParamTimbrado;
	}
	/**
	 * @return the lstPolizas
	 */
	public List<ParamTimbradoDTO> getLstPolizas() {
		return new ArrayList<ParamTimbradoDTO>(lstPolizas);
	}
	/**
	 * @param lstGrupos the lstPolizas to set
	 */
	public void setLstPolizas(List<ParamTimbradoDTO> lstPolizas) {
		this.lstPolizas = new ArrayList<ParamTimbradoDTO>(lstPolizas);
	}
}