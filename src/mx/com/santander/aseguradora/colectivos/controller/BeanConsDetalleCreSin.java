package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ClienteCertifId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.ColectivosCoberturas;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioClienteCertif;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.BeneficiariosDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCertificadoDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		08-03-2021
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Consulta Detalle Credito (Siniestros).
 * 				consDetalleCreditoSin.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
* ------------------------------------------------------------------------
 * 1.-	Por:	 Ing. Issac Bautista
 * 		Cuando:  15-11-2022
 * 		Porque:  Consultar fecha de vencimiento del credito
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanConsDetalleCreSin extends BeanConsDetalleCreSin2 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio de cliente
	@Resource
	private transient ServicioClienteCertif servicioCliente;

	//recurso para acceder al servicio de genericos
	@Resource
	private transient ServicioGeneric servicioGeneric;
	
	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanConsDetalleCreSin.class);

		
	/**
	 * Constructor de clase
	 */
	public BeanConsDetalleCreSin() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_CONS_DETALLE_CRE_SIN);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CONS_DETALLE_CRE_SIN);
	}

	/**
	 * Metodo que consulta el los datos (de cliente y certificados) de un filtro seleccionado
	 * @return mensage de exito o falla
	 */
	public String consulta(){
		List<Object> lista;
		Iterator<Object> it;
		String strQuery;
		List<DetalleCertificadoDTO> arlResultados;
		
		//Creamos HashMap para agrupar certificados por cliente
		HashMap<Long, DetalleCertificadoDTO> hmResultados;
		
		try {
			//Se validan datos antes de hacer consulta
			if(this.validaDatos()) {
			
				//Se inicializa lista
				arlResultados = new ArrayList<DetalleCertificadoDTO>();
				
				//Se inicializa HashMap
				hmResultados = new HashMap<Long, DetalleCertificadoDTO>();
				
				//Obtenemos query
				strQuery = GeneratorQuerys.consDetalleCreditoSin.toString();
				
				//Reemplazamos filtro
				strQuery = strQuery.replace("#filtro ", getFiltro());
				
				//Se realiza consulta para mostrar los certificados
				lista = servicioGeneric.consultaSQL(new StringBuilder(strQuery));
				 
				//se recorren resultados para crear objeto que se utiliza en la vista
				it = lista.iterator();
			    while(it.hasNext()) {
			    	setDatos((Object[]) it.next(), hmResultados, servicioGeneric);
				}

			    //Obtnemoslista de hash map
			    arlResultados.addAll(hmResultados.values());
			    
				//Se setea propiedad para pantalla
				setLstCertificados(arlResultados);
				
				//Se manda respuesta a pantalla
				setStrRespuesta("Se realizo consulta con exito, favor de revisar resultados");
				
				//Se reinicia el filtro
				setFiltro(new StringBuilder());
			}
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los creditos.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo que consulta coberturas de un certificado
	 */
	public void getCoberturas() {
		List<Object> lista;
		Iterator<Object> it;
		List<Object> arlLista;
		String strQuery = Constantes.DEFAULT_STRING;
		Double tarifaTotal = Constantes.DEFAULT_DOUBLE;
		ColectivosCoberturas objCober;
		
		try{
			//Se inicializa lista
			arlLista = new ArrayList<Object>();
			
			//Obtenemos query
			strQuery = GeneratorQuerys.consCoberturas.toString();
			
			//Reemplazamos valores
			strQuery = strQuery.replace("--w_canal--", getSeleccionado().getId().getCoceCasuCdSucursal().toString());
			strQuery = strQuery.replace("--w_ramo--", getSeleccionado().getId().getCoceCarpCdRamo().toString());
			strQuery = strQuery.replace("--w_idventa--", getSeleccionado().getCoceSubCampana());
			strQuery = strQuery.replace("--wpoliza_col--", getSeleccionado().getId().getCoceCapoNuPoliza().toString());
			strQuery = strQuery.replace("--w_prod--", getSeleccionado().getCoceTpProductoBco());
			strQuery = strQuery.replace("--w_plan--", getSeleccionado().getCoceTpSubprodBco());
			strQuery = strQuery.replace("--w_nucob--", String.valueOf(getSeleccionado().getCoceNuCobertura()));
			
			//Se realiza consulta para coberturas
			lista = servicioGeneric.consultaSQL(new StringBuilder(strQuery));
		
			//se recorren resultados para crear objeto que se utiliza en la vista
			it = lista.iterator();
		    while(it.hasNext()) {
		    	objCober = (ColectivosCoberturas) creaObjCobertura((Object[])it.next(), 
									getSeleccionado().getCoceMtSumaAsegurada(),
									getSeleccionado().getCoceMtSumaAsegSi(),
									getSeleccionado().getCoceSubCampana());
		    	
		    	arlLista.add(objCober);
		    	
		    	//Se va sumando tarifa de cada cobertura
				tarifaTotal += objCober.getCocbTaRiesgo().doubleValue();				
			}
		    
		    //Asignamos tarifa total
		    getSeleccionado().setCoceTpPma(tarifaTotal.toString());
		    
		    //Se setea valor a propiedad de pantalla
		    getSelCliente().setLstCoberturas(arlLista);
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar detalle.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}

	/**
	 * Metodo que consulta asistencias de un certificado
	 */
	public void getAsistencias() {
		List<Object> arlLista;
		
		try{
			//Se inicializa lista
			arlLista = new ArrayList<Object>();
			
			//Se realiza consulta para mostrar las asistencias
			arlLista.addAll(servicioCliente.consultaCredAsis(getSeleccionado().getCoceNuCredito(),
					getSeleccionado().getId().getCoceCasuCdSucursal(),
					getSeleccionado().getId().getCoceCarpCdRamo(),
					getSeleccionado().getId().getCoceCapoNuPoliza(),
					getSeleccionado().getId().getCoceNuCertificado()));
			
			//Se setea valor a propiedad de pantalla 
			getSelCliente().setLstAsistencias(arlLista);
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar asistencias.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que consulta asegurados de un certificado
	 */
	public void getAsegurados() {
		List<Object> arlLista;
		
		try{
			//Se inicializa lista
			arlLista = new ArrayList<Object>();
			
			//Se realiza consulta para mostrar los asegurados
			arlLista.addAll(servicioCliente.consultaCredAsegurados(getSeleccionado().getCoceNuCredito(),
					getSeleccionado().getId().getCoceCasuCdSucursal(),
					getSeleccionado().getId().getCoceCarpCdRamo(),
					getSeleccionado().getId().getCoceCapoNuPoliza(),
					getSeleccionado().getId().getCoceNuCertificado()));
			
			//Se setea valor a propiedad de pantalla
			getSelCliente().setLstAsegurados(arlLista);
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar asegurados.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que consulta beneficiarios de un certificado
	 */
	public void getBeneficiarios() {
		List<BeneficiariosDTO> arlLista;
		
		try{
			//Se inicializa lista
			arlLista = new ArrayList<BeneficiariosDTO>();
			
			//Se realiza consulta para mostrar los beneficiarios
			arlLista.addAll(servicioEndosoDatos.getBeneficiarios(getSeleccionado().getId()));
			
		    //Se setea valor a propiedad de pantalla
		    getSelCliente().setLstBeneficiarios(arlLista);
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar beneficiarios.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que consulta endosos de un certificado
	 */
	public void getEndosos() {
		List<EndosoDatosDTO> arlLista;
		
		try{
			//Se inicializa lista
			arlLista = new ArrayList<EndosoDatosDTO>();
			
			//Se realiza consulta para mostrar los endosos
			arlLista.addAll(servicioCliente.getEndosos(
					getSeleccionado().getId().getCoceCasuCdSucursal(),
					getSeleccionado().getId().getCoceCarpCdRamo(),
					getSeleccionado().getId().getCoceCapoNuPoliza(),
					getSeleccionado().getId().getCoceNuCertificado()));

			//Se setea valor a propiedad de pantalla
		    getSelCliente().setLstEndosos(arlLista);
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar endosos.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo para migrar informacion a RECTOR
	 * @throws Excepciones con error en general
	 */
	public void migraInfo() throws Excepciones {
		try {
			//Se manda llamar metodo para migrar informacion
			servicioGeneric.migraInformacion(new ClienteCertifId(getSelCliente().getCliente().getCocnNuCliente(),
																	getSeleccionado().getId().getCoceCasuCdSucursal(),
																	getSeleccionado().getId().getCoceCarpCdRamo().byteValue(),
																	getSeleccionado().getId().getCoceCapoNuPoliza(),
																	getSeleccionado().getId().getCoceNuCertificado(),
																	getSeleccionado().getCoceNuCredito()));
			
			//Se manda respuesta a pantalla
			setStrRespuesta("Se realizo la migracion de informacion a RECTOR.");
			
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("Error al migrar informacion", e);
			throw new FacesException("Error al migrar informacion", e);
		}
		
	}

	/**
	 * @param servicioCliente the servicioCliente to set
	 */
	public void setServicioCliente(ServicioClienteCertif servicioCliente) {
		this.servicioCliente = servicioCliente;
	}
	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
	
}
