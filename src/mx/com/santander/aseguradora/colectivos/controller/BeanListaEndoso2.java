package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCobranza;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanEndoso;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		25-11-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Endosos.
 * 				impresionEndoso.jsp
 * 				Esta clase contine solo propiedades que son utilzadas para
 * 				la generacion del detalle endosos
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class BeanListaEndoso2 implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	//recurso para acceder al servicio
	@Resource
	private transient ServicioCobranza servicioCobranza;
	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanListaEndoso2.class);
	//propiedad donde vienen los datos del endoso
	protected transient Object objActual;
	//Propiedad para lista de recibos
	protected transient List<Object> listaRecibos;
	//Propiedad para lista de endosos
	protected transient List<Object> listaEndosos;
	//Clase para tener acceso a propiedades genericas.
	protected transient BeanGenerico beanGen;
		
	/**
	 * Constructor de clase
	 */
	public BeanListaEndoso2() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicializa lista
		this.listaEndosos = new ArrayList<Object>();
		this.listaRecibos = new ArrayList<Object>();
		
		//Se agrega el bean a session
		beanGen = new BeanGenerico();
		beanGen.setBean(BeanNames.BEAN_LISTA_ENDOSO);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_LISTA_ENDOSO);
	}

	/**
	 * Metodo que genera una peticcion para descargar el detalle del endoso
	 * @return respuesta del proceso
	 */
	public String descargaDetalle() {
		beanGen.setStrRespuesta(Constantes.DEFAULT_STRING);
		BeanEndoso objBean;
		
		try {
			objBean = (BeanEndoso) objActual;
			beanGen.setStrRespuesta(servicioCobranza.generarDetalleEndoso(objBean.getEndoso().getId().getCoedCasuCdSucursal(), 
						objBean.getEndoso().getId().getCoedCarpCdRamo(), 
						objBean.getEndoso().getId().getCoedCapoNuPoliza(), 
						objBean.getEndoso().getCoedNuRecibo()));
		} catch (Excepciones e) {
			beanGen.setStrRespuesta("Error al generar peticion para detella de endoso.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
			return NavigationResults.FAILURE;
		}
		
		return NavigationResults.SUCCESS;
	}
	
	/**
	 * Metodo parar setear recurso
	 * @param servicioCobranza servicio a utilizar
	 */
	public void setServicioCobranza(ServicioCobranza servicioCobranza) {
		this.servicioCobranza = servicioCobranza;
	}
	
	/**
	 * @return the objActual
	 */
	public Object getObjActual() {
		return objActual;
	}

	/**
	 * @param objActual the objActual to set
	 */
	public void setObjActual(Object objActual) {
		this.objActual = objActual;
	}
	
	/**
	 * @return the listaRecibos
	 */
	public List<Object> getListaRecibos() {
		return new ArrayList<Object>(listaRecibos);
	}

	/**
	 * @param listaRecibos the listaRecibos to set
	 */
	public void setListaRecibos(List<Object> listaRecibos) {
		this.listaRecibos = new ArrayList<Object>(listaRecibos);
	}

	/**
	 * @return the listaEndosos
	 */
	public List<Object> getListaEndosos() {
		return new ArrayList<Object>(listaEndosos);
	}

	/**
	 * @param listaEndosos the listaEndosos to set
	 */
	public void setListaEndosos(List<Object> listaEndosos) {
		this.listaEndosos = new ArrayList<Object>(listaEndosos);
	}
	/**
	 * @return the beanGen
	 */
	public BeanGenerico getBeanGen() {
		return beanGen;
	}
}
