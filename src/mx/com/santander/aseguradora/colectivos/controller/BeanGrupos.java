package mx.com.santander.aseguradora.colectivos.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Configuracion de Grupos.
 * 				confGrupos.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanGrupos extends BeanGrupos2 {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio
	@Resource
	private ServicioParametros servicioParametros;
	
	//recurso para acceder al servicio generico
	@Resource
	private ServicioGeneric servicioGeneric;
		
	//Log para escribir mensajes
	private static final Log LOG = LogFactory.getLog(BeanGrupos.class);
	
	//Propiedad para lsita de grupos
	private List<BeanParametros> lstGrupos;

	/**
	 * Constructor de clase
	 */
	public BeanGrupos() {
		super();
		
		//Se inicialisan valores por default
		this.lstGrupos = new ArrayList<BeanParametros>();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_GRUPOS);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_GRUPOS);
	}
	
	/**
	 * Metodo que consulta grupos
	 * @return Exito o Error
	 */
	public String consultaGrupos() {
		StringBuilder filtro = new StringBuilder(100);
		BeanParametros bean = null;
		List<Object> lstResultados;
		Parametros parametro;
		Iterator<Object> it;
		
		try {
			lstGrupos.clear();
			
			//se valida si el nombre del grupo se agrega al filtro
			if(!getNombreGrupo().equals(Constantes.DEFAULT_STRING)) {
				filtro.append("and UPPER(P.copaVvalor8) like '%" + getNombreGrupo().trim().toUpperCase() + "%' \n" );
			}
			
			//Se realiza consulta para mostrar los procesos configurados
			filtro.append("and P.copaDesParametro = 'CATALOGO'" );
			filtro.append("and P.copaIdParametro  = 'GRUPOS'" );
			filtro.append("order by P.copaNvalor1" );
			lstResultados = this.servicioParametros.obtenerObjetos(filtro.toString());
				
			//se recorren resultados para crear objeto que se utiliza en la vista
		    it = lstResultados.iterator();
		    while(it.hasNext()) {
		    	parametro = (Parametros) it.next();
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
				bean.setAuxDato(String.valueOf(bean.getCopaNvalor1()) + " - " + bean.getCopaVvalor8());
				bean.setServicioParametros(this.servicioParametros);
				bean.setCopaVvalor6(bean.getCopaRegistro().split("\\|")[0]);
				bean.setCopaVvalor7(bean.getCopaRegistro().split("\\|")[1]);
				
				lstGrupos.add(bean);
			}
		    
		    //Se setean valors por default
		    setNombreGrupo(Constantes.DEFAULT_STRING);
		    if(getnAccion() == Constantes.DEFAULT_INT) {
				//Se manda respuesta a pantalla
		    	setStrRespuesta("Se ejecuto con EXITO la consulta, favor de desplegar los resultados.");
		    }
		    
		    //se regresa mensage de SUCCESS a la vista
		    return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los grupos..");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo que modifica un registro de grupo
	 */
	public void modificar(){
		try {
			//Se validan datos antes de hacer la modificacion
			if(validaDatos(getObjActual())) {
				//Se pasan valores necesarios para la modificacion
				getObjActual().setCopaRegistro(setCorreos() + "|" + getObjActual().getCopaVvalor7());
				getObjActual().setCopaVvalor6(Constantes.DEFAULT_STRING);
				getObjActual().setCopaVvalor7(Constantes.DEFAULT_STRING);
				getObjActual().setCopaFvalor1(new Date());
				
				//Se construye objeto para poder realizar la modificacion con los nuevos valores asignados 
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				Parametros  parametro =  (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getObjActual()) ;
				servicioParametros.actualizarObjeto(parametro);
				
				//Se setea accion a 2
				setnAccion(2);
				//se manda llamar metodo de cancelar para setear valroes por default
				cancelar();
				//Se consultas los grupos
				consultaGrupos();

				//Se manda respuesta a pantalla
				setStrRespuesta("Se modifico con EXITO el registro.");
			}
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al MODIFICAR registro.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que guarda un registro de grupo
	 */
	public void guardar() {
		Long secParametro;
		
		try {
			//Se validan datos antes de hacer el guardado
			if(validaDatos(getObjNuevo())) {
				//Se aumenta el consecutivo
				if(getnConsecutivo() == Constantes.DEFAULT_INT) {
					setnConsecutivo(getMaxConsecutivo());
				}
				
				//se consulta el siguiente id parametro
				secParametro = servicioParametros.siguente();
				setnConsecutivo(getnConsecutivo() + 1);
				
				//Se pasan valores necesarios para el guardado
				getObjNuevo().setCopaCdParametro(secParametro); 	 
				getObjNuevo().setCopaIdParametro("GRUPOS");
				getObjNuevo().setCopaDesParametro("CATALOGO"); 	
				getObjNuevo().setCopaNvalor1(getnConsecutivo());					
				getObjNuevo().setCopaRegistro(setCorreos() + "|" + getObjNuevo().getCopaVvalor7());
				getObjNuevo().setCopaVvalor6(Constantes.DEFAULT_STRING);
				getObjNuevo().setCopaVvalor7(Constantes.DEFAULT_STRING);
				getObjNuevo().setCopaFvalor1(new Date());

				//Se construye objeto para poder realizar el guardado con los nuevos valores asignados
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getObjNuevo());
				this.servicioParametros.guardarObjeto(parametro);

				//Se setea accion a 1
				setnAccion(1);
				//se manda llamar metodo de cancelar para setear valroes por default
				cancelar();
				//Se consultas los grupos
				consultaGrupos();

				//Se manda respuesta a pantalla
				setStrRespuesta("Se guardo con EXITO el registro.");
			}
		} catch (Excepciones e) {
			setStrRespuesta("Error al GUARDAR registro.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que consulta el Id maximo del catalogo
	 * @return
	 */
	private int getMaxConsecutivo() {
		StringBuilder filtro = new StringBuilder(300);
		List<Object> lstResultados;

		try {			
			//Se realiza consulta para mostrar los procesos configurados
			filtro.append("SELECT NVL(MAX(COPA_NVALOR1), 0) \n");
			filtro.append(" FROM COLECTIVOS_PARAMETROS \n");
			filtro.append(" WHERE COPA_DES_PARAMETRO = 'CATALOGO' \n");
			filtro.append(" AND COPA_ID_PARAMETRO  = 'GRUPOS'");

			//Ejecuta query
			lstResultados = this.servicioGeneric.consultaSQL(filtro);
			
			//regresamos id
			return ((BigDecimal) lstResultados.get(0)).intValue();
			
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			
			//se regresa 0
			return Constantes.DEFAULT_INT;
		}
	}

	/**
	 * Metodo que valida los datos del registro
	 * @param objGrupo datos a validar
	 * @return falso o verdadero
	 */
	private boolean validaDatos(BeanParametros objGrupo) {
		boolean bRegreso = true;
				
		if(objGrupo.getCopaVvalor8() == null || objGrupo.getCopaVvalor8().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* NOMBRE GRUPO - Campo Requerido.");
			bRegreso =  false;
		} else if(objGrupo.getCopaVvalor1() == null || objGrupo.getCopaVvalor1().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* ASUNTO - Campo Requerido.");
			bRegreso =  false;
		} else if(objGrupo.getCopaVvalor7() == null || objGrupo.getCopaVvalor7().equals(Constantes.DEFAULT_STRING)) {
			FacesUtils.addErrorMessage("* MENSAJE - Campo Requerido.");
			bRegreso =  false;
		} else if(getLstCorreos().isEmpty()) {
			FacesUtils.addErrorMessage("* Agregar al menos un correo.");
			bRegreso =  false;
		} 
		
		return bRegreso;
	}
				
	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
	/**
	 * @return the lstGrupos
	 */
	public List<BeanParametros> getLstGrupos() {
		return new ArrayList<BeanParametros>(lstGrupos);
	}
	/**
	 * @param lstGrupos the lstGrupos to set
	 */
	public void setLstGrupos(List<BeanParametros> lstGrupos) {
		this.lstGrupos = new ArrayList<BeanParametros>(lstGrupos);
	}
	
}