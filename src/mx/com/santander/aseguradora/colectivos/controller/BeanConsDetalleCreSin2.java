package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Cliente;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.dto.DetalleCertificadoDTO;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		08-03-2021
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Consulta Detalle Credito (Siniestros).
 * 				consDetalleCreditoSin.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanConsDetalleCreSin
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 Ing. Issac Bautista
 * 		Cuando:  15-11-2022
 * 		Porque:  Consultar fecha de vencimiento del credito
 * ------------------------------------------------------------------------
 * 
 */
public class BeanConsDetalleCreSin2 extends BeanConsDetalleCreSin3 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio de endosos datos
	@Resource
	protected transient ServicioEndosoDatos servicioEndosoDatos;
	
	//Propiedad para lista de resultados
	private List<DetalleCertificadoDTO> lstCertificados;
	//Propiedad para filtro por credito
	private String credito;
	//Propiedad para filtro por nombre
	private String nombre;
		
	/**
	 * Constructor de clase
	 */
	public BeanConsDetalleCreSin2() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicializa lista
		this.lstCertificados = new ArrayList<DetalleCertificadoDTO>();
				
		//Se inicialisan valores por default
		this.credito = Constantes.DEFAULT_STRING;
		this.nombre = Constantes.DEFAULT_STRING;
	}

	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	public boolean validaDatos() {

		//Se valida se haya ingresado valor para realizar consulta
		if(getCredito().equals(Constantes.DEFAULT_STRING) 
				&& getNombre().equals(Constantes.DEFAULT_STRING) 
				&& getApellidoPaterno().equals(Constantes.DEFAULT_STRING)
				&& getApellidoMaterno().equals(Constantes.DEFAULT_STRING)) {
			setStrRespuesta("Favor de ingresar datos para realizar la consulta");
		
			//Se regresa falso
			return false;
		} 
		
		//Se agrega filtro por credito
		if(getCredito().length() > 0){
			getFiltro().append(" AND (B.COCC_ID_CERTIFICADO	= '").append(getCredito()).append("' \n");
			getFiltro().append(" OR A.COCE_BUC_EMPRESA = '").append(getCredito()).append("') \n");
		}
		
		//Se agrega filtro por nombre
		if(getNombre().length() > 0){
			getFiltro().append(" AND C.COCN_NOMBRE	= '").append(getNombre().toUpperCase()).append("' \n");
		}
			
		//Se agrega filtro por apellido paterno
		if(getApellidoPaterno().length() >0){
			getFiltro().append(" AND C.COCN_APELLIDO_PAT = '").append(getApellidoPaterno().toUpperCase()).append("' \n");
		}
		
		//Se agrega filtro por apellido materno
		if(getApellidoMaterno().length() > 0){
			getFiltro().append(" AND C.COCN_APELLIDO_MAT ='").append(getApellidoMaterno().toUpperCase()).append("' \n");
		}

		//Se regresa verdadero
		return true;
	}
	
	/**
	 * Metodo que regresa instancia de objeto DetalleCertificadoDTO
	 * @param objRegistro informacion de base de datos
	 * @param hmResultados hash map con objetos a presentar en pantalla
	 * @param servicioGeneric servicio para ejecutar consulta
	 * @throws Excepciones con error en general
	 */
	public void setDatos(Object[] objRegistro, Map<Long, DetalleCertificadoDTO> hmResultados, ServicioGeneric servicioGeneric) throws Excepciones {
		//Creamos objetos
		DetalleCertificadoDTO dtoCte;
		Certificado objCert;
		List<Certificado> arlCerts = new ArrayList<Certificado>();
		
		//Inicializamos objetos 
		dtoCte = new DetalleCertificadoDTO();
		dtoCte.setCliente(new Cliente());
		objCert = new Certificado();
		
		//Asignamos valores de objeto (cliente) 
		dtoCte.getCliente().setCocnCampov2(objRegistro[0].toString());
		dtoCte.getCliente().setCocnCampov3(objRegistro[1].toString());
		dtoCte.getCliente().setCocnNombre(objRegistro[2].toString());
		dtoCte.getCliente().setCocnApellidoPat(objRegistro[3].toString());
		dtoCte.getCliente().setCocnApellidoMat(objRegistro[4].toString());
		dtoCte.getCliente().setCocnCdSexo(objRegistro[5].toString());
		dtoCte.getCliente().setCocnFeNacimiento((Date) objRegistro[6]);
		dtoCte.getCliente().setCocnRfc(objRegistro[7].toString());
		dtoCte.getCliente().setCocnCalleNum(objRegistro[8].toString());
		dtoCte.getCliente().setCocnColonia(objRegistro[9].toString());
		dtoCte.getCliente().setCocnDelegmunic(objRegistro[10].toString());
		dtoCte.getCliente().setCocnCampov1(objRegistro[11].toString());
		dtoCte.getCliente().setCocnCdPostal(objRegistro[12].toString());
		dtoCte.getCliente().setCocnNuLada(objRegistro[13].toString());
		dtoCte.getCliente().setCocnNuTelefono(objRegistro[14].toString());
		dtoCte.getCliente().setCocnBucCliente(objRegistro[15].toString());
		dtoCte.getCliente().setCocnNuCliente(Long.valueOf(objRegistro[16].toString()));
		dtoCte.getCliente().setCocnCampov4(objRegistro[17].toString());
		
		//Asignamos valores de objeto (certificado)
		objCert.setId(new CertificadoId());
		objCert.getId().setCoceCasuCdSucursal(((BigDecimal)objRegistro[18]).shortValue());
		objCert.getId().setCoceCarpCdRamo(((BigDecimal)objRegistro[19]).shortValue());
		objCert.getId().setCoceCapoNuPoliza(((BigDecimal)objRegistro[20]).longValue());
		objCert.getId().setCoceNuCertificado(((BigDecimal)objRegistro[21]).longValue());
		
		objCert.setCoceNuCredito(objRegistro[0].toString());		
		objCert.setCoceCampon1(Long.valueOf(objRegistro[22].toString()));
		objCert.setCoceCampov1(objRegistro[23].toString());
		objCert.setCoceCampov2(objRegistro[24].toString());
		objCert.setCoceCdCausaAnulacion(((BigDecimal)objRegistro[25]).shortValue());
		objCert.setCoceCampov3(objRegistro[26].toString());
		objCert.setCoceFeAnulacionCol((Date) objRegistro[27]);
		objCert.setCoceTpProductoBco(objRegistro[28].toString());
		objCert.getPlanes().getProducto().getId().setAlprCdProducto(((BigDecimal)objRegistro[28]).intValue());
		objCert.setCoceTpSubprodBco(objRegistro[29].toString());
		objCert.getPlanes().getId().setAlplCdPlan(((BigDecimal)objRegistro[29]).intValue());
		objCert.setCoceFeCarga((Date) objRegistro[30]);
		objCert.setCoceFeSuscripcion((Date) objRegistro[31]);
		objCert.setCoceMtSumaAsegurada(new BigDecimal(objRegistro[32].toString()));
		objCert.setCoceMtSumaAsegSi((BigDecimal)objRegistro[33]);
		objCert.setCoceMtPrimaSubsecuente((BigDecimal)objRegistro[34]);
		objCert.setCoceMtPrimaPura((BigDecimal)objRegistro[35]);
		objCert.setCoceFeDesde((Date) objRegistro[36]);
		objCert.setCoceFeHasta((Date) objRegistro[37]);
		objCert.setCoceFeIniCredito((Date) objRegistro[38]);
		objCert.setCoceFeFinCredito((Date) objRegistro[39]);
		objCert.setCoceNoRecibo((BigDecimal)objRegistro[40]);
		objCert.setCoceCampov4(objRegistro[41].toString());
		objCert.setCoceCazbCdSucursal( ((BigDecimal) Utilerias.objectIsNull(objRegistro[42], Constantes.TIPO_DATO_BIGDECIMAL)).intValue());
		objCert.setCoceFeEmision((Date) objRegistro[43]);
		objCert.setCoceEmpresa(Utilerias.objectIsNull(objRegistro[44], Constantes.TIPO_DATO_STRING).toString());
		objCert.setCoceSubCampana(Utilerias.objectIsNull(objRegistro[45], Constantes.TIPO_DATO_STRING_ID).toString());
		objCert.setCoceNuCobertura(Long.valueOf(objRegistro[46].toString()));
		
		//Se obtine fecha de vencimiento
		objCert.setCoceFeAnulacion(servicioGeneric.getFechaVencimiento(objCert.getId()));
		
		//Verificamos si existe en hash map el cliente
		if(hmResultados.containsKey(dtoCte.getCliente().getCocnNuCliente())) {
			//Obtenemos lista de certificados del cliente
			arlCerts = hmResultados.get(dtoCte.getCliente().getCocnNuCliente()).getLstCertificados();
			
			//Se agrega el nuevo certificado a la lista
			arlCerts.add(objCert);
			
			//Se agrega la lista al hash map
			hmResultados.get(dtoCte.getCliente().getCocnNuCliente()).setLstCertificados(arlCerts);
		} else {
			//Se agrega el nuevo certificado a la lista
			arlCerts.add(objCert);
			
			//Se agrega la lista al hash map
			dtoCte.setLstCertificados(arlCerts);
			hmResultados.put(dtoCte.getCliente().getCocnNuCliente(), dtoCte);
		}
	}
	
	
	/**
	 * @return the lstCertificados
	 */
	public List<DetalleCertificadoDTO> getLstCertificados() {
		return new ArrayList<DetalleCertificadoDTO>(lstCertificados);
	}
	/**
	 * @param lstCertificados the listaCreditos to set
	 */
	public void setLstCertificados(List<DetalleCertificadoDTO> lstCertificados) {
		this.lstCertificados = new ArrayList<DetalleCertificadoDTO>(lstCertificados);
	}
	
	/**
	 * @return the credito
	 */
	public String getCredito() {
		return credito;
	}
	/**
	 * @param credito the credito to set
	 */
	public void setCredito(String credito) {
		this.credito = credito;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @param servicioEndosoDatos the servicioEndosoDatos to set
	 */
	public void setServicioEndosoDatos(ServicioEndosoDatos servicioEndosoDatos) {
		this.servicioEndosoDatos = servicioEndosoDatos;
	}
}
