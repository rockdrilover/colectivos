package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		20-11-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Carga Cobranza.
 * 				cargaCobranza.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class BeanCargaCobranza2 implements Serializable {
	
	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para saber  que tipo de carga se hace
	protected Integer tipo;
	
	//Propiedad que para guardar nombre del archivo
	protected String nombreArchivo;
	
	//Propieddad para conteo de errores
	protected int errores;
	
	//Propiedad para cconteo de registros
	protected int registros;
	
	/**
	 * Contructor de clase
	 */
	public BeanCargaCobranza2() {
		super();
	}
	
	/**
	 * Metodo que regresa columnas para reporte errores
	 * @param tipo tipo carga
	 * @return arreglo con nombre de columnas
	 */
	public String[] getNombreColumnas(Integer tipo) {
		String[] arrColumnas = new String[1];
		
		//Se agrega encabezado
		if(tipo == 20) {
			arrColumnas = new String[9];
			arrColumnas[0] = "POLIZA";
			arrColumnas[1] = "CREDITO";
			arrColumnas[2] = "RECIBO";
			arrColumnas[3] = "PRIMA_FACTURADA";
			arrColumnas[4] = "PRIMA_COBRADA";
			arrColumnas[5] = "FECHA_COBRO";
			arrColumnas[6] = "NOTA_CREDITO";
			arrColumnas[7] = "FECHA_APLICACION";
			arrColumnas[8] = "ERROR";
		} else {
			arrColumnas = new String[8];
			arrColumnas[0] = "POLIZA";
			arrColumnas[1] = "RECIBO";
			arrColumnas[2] = "PRIMA_FACTURADA";
			arrColumnas[3] = "PRIMA_COBRADA";
			arrColumnas[4] = "FECHA_COBRO";
			arrColumnas[5] = "NOTA_CREDITO";
			arrColumnas[6] = "FECHA_APLICACION";
			arrColumnas[7] = "ERROR";
		}
		
		return arrColumnas;
	}
	
	/**
	 * @return the tipo
	 */
	public Integer getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	/**
	 * @return the registros
	 */
	public int getRegistros() {
		return registros;
	}
	/**
	 * @param registros the registros to set
	 */
	public void setRegistros(int registros) {
		this.registros = registros;
	}
	/**
	 * @return the errores
	 */
	public int getErrores() {
		return errores;
	}
	/**
	 * @param errores the errores to set
	 */
	public void setErrores(int errores) {
		this.errores = errores;
	}
	
}
