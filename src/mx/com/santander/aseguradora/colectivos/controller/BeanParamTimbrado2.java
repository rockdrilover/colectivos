package mx.com.santander.aseguradora.colectivos.controller;

import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;
import mx.com.santander.aseguradora.colectivos.view.dto.ParamTimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Timbrado.
 * 				paramTimbrado.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanParamTimbrado
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanParamTimbrado2 extends BeanParamTimbrado3 {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para editar o guardar objeto
	private ParamTimbradoDTO objActual;
	//Propiedad para metodo de pago
	private String metodoPago;
	//Propiedad para lsita de metodos de pago
	private List<Object> cmbMetodosPago;

	
	/**
	 * Constructor de clase
	 */
	public BeanParamTimbrado2() {
		//se manda ejecutar constructor de clase padre
		super();

		//Se inicialisan valores por default
		this.objActual = new ParamTimbradoDTO();
		this.cmbMetodosPago = new ArrayList<Object>();
	}
	
	/**
	 * Metodo que valida los datos del registro a editar
	 * @param objParam datos a validar
	 * @return falso o verdadero
	 */
	public boolean validaDatos(BeanParametros objParam) {
		boolean bRegreso = true;
				
		if(objParam.getCopaNvalor5() == null) {
			FacesUtils.addErrorMessage("* CUOTA ENERO - Campo Requerido.");
			bRegreso =  false;
		} else if(objParam.getCopaFvalor1() == null) {
			FacesUtils.addErrorMessage("* FECHA INICIO - Campo Requerido.");
			bRegreso =  false;
		} else if(objParam.getCopaFvalor2() == null) {
			FacesUtils.addErrorMessage("* FECHA FIN - Campo Requerido.");
			bRegreso =  false;
		} 
		
		return bRegreso;
	}
	
	/**
	 * @return the objActual
	 */
	public ParamTimbradoDTO getObjActual() {
		return objActual;
	}
	/**
	 * @param objActual the objActual to set
	 */
	public void setObjActual(ParamTimbradoDTO objActual) {
		this.objActual = objActual;
	}
	/**
	 * @return the cmbMetodosPago
	 */
	public List<Object> getCmbMetodosPago() {
		return new ArrayList<>(cmbMetodosPago);
	}
	/**
	 * @param cmbMetodosPago the cmbMetodosPago to set
	 */
	public void setCmbMetodosPago(List<Object> cmbMetodosPago) {
		this.cmbMetodosPago = new ArrayList<>(cmbMetodosPago);
	}	
	/**
	 * @return the metodoPago
	 */
	public String getMetodoPago() {
		return metodoPago;
	}
	/**
	 * @param metodoPago the metodoPago to set
	 */
	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}
}