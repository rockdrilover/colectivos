package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCobranza;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		15-12-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Reverso Estatus Cobranza.
 * 				reversoEstatusCobranza.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
* ------------------------------------------------------------------------
 * 1.-	Por:	 
 * 		Cuando:  
 * 		Porque:  
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanReversoCobranza extends BeanReversoCobranza2 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio de cobranza
	@Resource
	private transient ServicioCobranza servicioCobranza;

	//recurso para acceder al servicio de genericos
	@Resource
	private transient ServicioGeneric servicioGeneric;
	
	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanReversoCobranza.class);

		
	/**
	 * Constructor de clase
	 */
	public BeanReversoCobranza() {
		//se manda ejecutar constructor de clase padre
		super();
	}

	/**
	 * Metodo que consulta el los datos (de cliente y certificados) de un filtro seleccionado
	 * @return mensage de exito o falla
	 */
	public String consulta(){
		//Se crean propiedades que ocupa el metodo
		List<Object> lstResultados;
		Iterator<Object> itDatos;
		String strQueryDatos;
		
		try {
			//Se limpia lista de resultados
			setLstCertificados(new ArrayList<Certificado>());
			
			//Se validan datos antes de hacer consulta
			if(this.validaDatos()) {
				
				//Obtenemos query
				strQueryDatos = GeneratorQuerys.queryGetDatosCobranza.toString();
				
				//Reemplazamos filtro
				strQueryDatos = strQueryDatos.replace("#filtro ", beanGen.getFiltro());
				
				//Se realiza consulta para mostrar los certificados
				lstResultados = servicioGeneric.consultaSQL(new StringBuilder(strQueryDatos));
				 
				//se recorren resultados para crear objeto que se utiliza en la vista
				itDatos = lstResultados.iterator();
			    while(itDatos.hasNext()) {
			    	setDatos((Object[]) itDatos.next(), lstCertificados);
				}

				//Se manda respuesta a pantalla
			    beanGen.setStrRespuesta("Se realizo consulta con exito, favor de revisar resultados");
				
				//Se reinicia el filtro
			    beanGen.setFiltro(new StringBuilder());
			}
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			beanGen.setStrRespuesta("Error al consultar los creditos.");
			//Cualqueir error se reporta al LOG
			LOG.error(beanGen.getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo para aplicar cobranza a registros seleccionado
	 * @throws Excepciones con error en general
	 */
	public void aplicaCobranza() throws Excepciones {
		try {
			//Se manda llamar metodo para aplicar cobranza
			servicioCobranza.aplicaCobranza(getSeleccionado().getId().getCoceCasuCdSucursal(),
												getSeleccionado().getId().getCoceCarpCdRamo(),
												getSeleccionado().getId().getCoceCapoNuPoliza(),
												getSeleccionado().getId().getCoceNuCertificado(),
												getSeleccionado().getCoceNuCredito(),
												getSeleccionado().getCoceNoRecibo());
			
			//Se manda respuesta a pantalla
			beanGen.setStrRespuesta("Se realizo la aplicacion de cobranza del registros seleccionado.");
			
			//Limpieamos resultados
			lstCertificados = new ArrayList<Certificado>();
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error("Error al aplica cobranza", e);
			throw new FacesException("Error al aplica cobranza", e);
		}
		
	}
	
	/**
	 * @param servicioCobranza the servicioCobranza to set
	 */
	public void setServicioCobranza(ServicioCobranza servicioCobranza) {
		this.servicioCobranza = servicioCobranza;
	}
	
	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
	
}
