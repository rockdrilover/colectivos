package mx.com.santander.aseguradora.colectivos.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.owasp.esapi.reference.DefaultHTTPUtilities;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.TimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Timbrado.
 * 				timbrado.jsp
 * 				Esta clase contine propiedades y metodos que son utilzadas por 
 * 				la clase BeanTimbrado
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanTimbrado3  extends BeanTimbrado4 implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para tipo de busqueda
	private Integer tipoBusqueda;
	//Variable Para realizar filtros
	private StringBuilder filtro = new StringBuilder();

	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanTimbrado3.class);
		
	/**
	 * Constructor de clase
	 */
	public BeanTimbrado3() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.filtro = new StringBuilder();
	}
	
	/**
	 * Metodo que limpia los resultados
	 * @return  mensage de exito o falla
	 */
	public String limpiaResultados(){
		if(getListaTimbrado() != null){
			getListaTimbrado().clear();		
		} 
		
		setRamo(Short.valueOf(Constantes.DEFAULT_STRING_ID));
		setPoliza(Constantes.DEFAULT_INT);
		setIdVenta(Constantes.DEFAULT_INT);
		setPolEsp(Constantes.DEFAULT_STRING);
		setCertificado(Constantes.DEFAULT_STRING);
		setRecibo(Constantes.DEFAULT_STRING);
		setFeDesde(new Date());
		setFeHasta(new Date());
		setEstatus(Constantes.DEFAULT_STRING);
		setUuid(Constantes.DEFAULT_STRING);
		
		this.filtro = new StringBuilder();
		return NavigationResults.SUCCESS;
	}
	
	/**
	 * Metodo que valida los datos utilizados para hacer la consulta de datos timbrados
	 * @return true or false
	 */
	public boolean validaDatos2() {

		boolean bRegreso = true;
		filtro = new StringBuilder();
		
		//Se valida se haya seleccionado un ramo
		if(getNuevo().getRecibo().getcoreCarpCdRamo() == Constantes.DEFAULT_INT) {
			FacesUtils.addErrorMessage("* Favor de ingresar RAMO.");
			bRegreso = false;
		} else 		
		//Se valida se haya seleccionado una poliza
		if(getNuevo().getRecibo().getcoreCapoNuPoliza() == Constantes.DEFAULT_INT) {
			FacesUtils.addErrorMessage("* Favor de ingresar POLIZA.");
			bRegreso = false;
		} else 				
		//Se valida se haya seleccionado un anio
		if(getNuevo().getFactura().getCofaCampon2() == Constantes.DEFAULT_LONG) {
			FacesUtils.addErrorMessage("* Favor de ingresar A�O.");
			bRegreso = false;
		}

		return bRegreso;
	}
	
	/**
	 * Metodo que genera cadena con valores a timbrar
	 * @param objNuevo objeto con datos 
	 * @return cadena de valores
	 */
	public String getCadenaValores(TimbradoDTO objNuevo) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(objNuevo.getFactura().getCofaCampov1()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCaloCdLocalidad()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCamoCdMoneda()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreTpTramite()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInDevolucion()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCacnCdNacionalidad()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCacnNuCedulaRif()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInEmision()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInFinanciamiento()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCapoCdProvisional()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCjegNuEgreso()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInPagoCoaseguroCedido()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCataTaTasa()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInPagoComision()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInPagoFinanciera()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInPagoPrestamoAutomati()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreInPrecob()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreNuTramite()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaNuTrasfer()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaCampov2()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaCampov3()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreMtSumaAsegurada()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCapoCdFrPago()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaCampon5()).append(Constantes.PIPE)
			.append(GestorFechas.formatDate(objNuevo.getRecibo().getcoreFeEmision(), "yyyy-MM-dd")).append(Constantes.PIPE)
			.append(GestorFechas.formatDate(objNuevo.getRecibo().getcoreFePrecob(), "yyyy-MM-dd")).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaNuCuenta()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaCdNacionalidad()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaNuCuentaDep()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaCdUsuario()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaCampon1()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaNuCedulaRif()).append(Constantes.PIPE)
			.append(GestorFechas.formatDate(objNuevo.getRecibo().getcoreFeDesde(), Constantes.FORMATO_FECHA_UNO)).append(Constantes.PIPE)
			.append(GestorFechas.formatDate(objNuevo.getRecibo().getcoreFeHasta(), Constantes.FORMATO_FECHA_UNO)).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaStRecibo()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreMtPrima()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreMtPrimaPura()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaMtTotCom()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaMtTotDpo()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaMtTotCom1()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaMtTotRfi()).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaMtTotSua()).append(Constantes.PIPE)
			.append(Constantes.DEFAULT_STRING_ID).append(Constantes.PIPE)
			.append(BigDecimal.valueOf(Utilerias.redondear(objNuevo.getFactura().getCofaMtTotIva().doubleValue(), "2"))).append(Constantes.PIPE)
			.append(BigDecimal.valueOf(Utilerias.redondear(objNuevo.getFactura().getCofaMtRecibo().doubleValue(), "2"))).append(Constantes.PIPE)
			.append(BigDecimal.valueOf(Utilerias.redondear(objNuevo.getFactura().getCofaMtTotPma().doubleValue(), "3"))).append(Constantes.PIPE)
			.append(objNuevo.getFactura().getCofaCampov4()).append(Constantes.PIPE)
			.append(objNuevo.getRecibo().getcoreCdMotivo()).append(Constantes.PIPE);
		
		return sb.toString();
	}
	
	/**
	 * Metodo que manda descargar archivo
	 * @param bytes tama�o del archivo en bytes
	 * @param fnombre nombre del archivo
	 * @param aplication tipo de archivo
	 */
	public void descargaArchivo(byte[] bytes, String fnombre, String aplication) {
		HttpServletResponse response;
	    OutputStream out;
		
		try {
			//Se obtiene response
			response = FacesUtils.getServletResponse();
			//Tomamos el output stream
			out = response.getOutputStream ();
			
			//se pasan valores a response
			response.setContentType(aplication);
			
			DefaultHTTPUtilities httpUtils = new DefaultHTTPUtilities();
			httpUtils.setHeader(response, "Content-Disposition", "attachment;filename=" + fnombre);
			
			//se escribe en el output stream
			String bytesStr = ESAPI.encoder().encodeForBase64(bytes, false);
			out.write(ESAPI.encoder().decodeFromBase64(bytesStr));
			out.flush();
			out.close();
		} catch (IOException e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al descargar archivo.");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);			
		}
	}

	/**
	 * Metodo que crea objeto param (accion a realizar en pauete timbrado)
	 * @param siguiente siguiente id
	 * @param nCertificado numore de certificado
	 * @param noRecibo 
	 * @param opcion opcion a generar
	 * @return objeto parametro
	 */
	public Parametros setParametro(Long siguiente, int nCertificado, BigDecimal noRecibo, int opcion) {
		Parametros objParam = new Parametros();
		objParam.setCopaCdParametro(siguiente);	// SIGUIENTE NUMERO
		objParam.setCopaIdParametro("TIMBRADO");// TIMBRADO
		objParam.setCopaDesParametro("ACCION"); // ACCION
		objParam.setCopaNvalor1(opcion);		// CONSULTA 1, INSERTA-2
		objParam.setCopaNvalor4(nCertificado);	// NUM CERTIFICADO
		objParam.setCopaNvalor8(noRecibo.doubleValue()); //NUMERO recibo
		
		return objParam;
	}
	
	/**
	 * @return the tipoBusqueda
	 */
	public Integer getTipoBusqueda() {
		return tipoBusqueda;
	}
	/**
	 * @param tipoBusqueda the tipoBusqueda to set
	 */
	public void setTipoBusqueda(Integer tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}
	/**
	 * @return the filtro
	 */
	public StringBuilder getFiltro() {
		return filtro;
	}
	/**
	 * @param filtro the filtro to set
	 */
	public void setFiltro(StringBuilder filtro) {
		this.filtro = filtro;
	}
	
}
