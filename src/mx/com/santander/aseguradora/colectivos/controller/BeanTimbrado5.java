package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.view.dto.TimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Timbrado.
 * 				timbrado.jsp
 * 				Esta clase contine propiedades y metodos que son utilzadas por 
 * 				la clase BeanTimbrado
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanTimbrado5 extends BeanGenerico implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para objeto modificado
	private TimbradoDTO seleccionado;
	//Propiedad para objeto nuevos
	private TimbradoDTO nuevo;
	//Propiedad para tipo documento
	private String tipoDoc;
	//Propiedad para lista de historicos
	private List<TimbradoDTO> listaHist;
	

	/**
	 * Constructor de clase
	 */
	public BeanTimbrado5() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.seleccionado = new TimbradoDTO();
		this.nuevo = new TimbradoDTO();
		this.listaHist = new ArrayList<TimbradoDTO>();
	}
	
	/**
	 * @return the seleccionado
	 */
	public TimbradoDTO getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(TimbradoDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the nuevo
	 */
	public TimbradoDTO getNuevo() {
		return nuevo;
	}
	/**
	 * @param nuevo the nuevo to set
	 */
	public void setNuevo(TimbradoDTO nuevo) {
		this.nuevo = nuevo;
	}
	/**
	 * @return the listaHist
	 */
	public List<TimbradoDTO> getListaHist() {
		return new ArrayList<TimbradoDTO>(listaHist);
	}
	/**
	 * @param listaHist the listaHist to set
	 */
	public void setListaHist(List<TimbradoDTO> listaHist) {
		this.listaHist = new ArrayList<TimbradoDTO>(listaHist);
	}
	/**
	 * @return the tipoDoc
	 */
	public String getTipoDoc() {
		return tipoDoc;
	}
	/**
	 * @param tipoDoc the tipoDoc to set
	 */
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
}
