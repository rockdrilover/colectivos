package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

import mx.com.santander.aseguradora.colectivos.view.dto.ComisionesDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Comisiones.
 * 				parametrizacionComisiones.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanParamComision
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanParamComision5 extends BeanGenerico implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para comisiones tenicas emision
	private ComisionesDTO tecnicaEmi;
	//Propiedad para comisiones tecnicas renovacion
	private ComisionesDTO tecnicaReno;
	//Propiedad para comisiones comerciales emision
	private ComisionesDTO comercialEmi;
	//Propiedad para comisiones comerciales renovacion
	private ComisionesDTO comercialReno;
	
	/**
	 * Constructor de clase
	 */
	public BeanParamComision5() {
		//se manda ejecutar constructor de clase padre
		super();
	}

	/**
	 * @return the tecnicaEmi
	 */
	public ComisionesDTO getTecnicaEmi() {
		return tecnicaEmi;
	}
	/**
	 * @param tecnicaEmi the tecnicaEmi to set
	 */
	public void setTecnicaEmi(ComisionesDTO tecnicaEmi) {
		this.tecnicaEmi = tecnicaEmi;
	}
	/**
	 * @return the tecnicaReno
	 */
	public ComisionesDTO getTecnicaReno() {
		return tecnicaReno;
	}
	/**
	 * @param tecnicaReno the tecnicaReno to set
	 */
	public void setTecnicaReno(ComisionesDTO tecnicaReno) {
		this.tecnicaReno = tecnicaReno;
	}
	/**
	 * @return the comercialEmi
	 */
	public ComisionesDTO getComercialEmi() {
		return comercialEmi;
	}
	/**
	 * @param comercialEmi the comercialEmi to set
	 */
	public void setComercialEmi(ComisionesDTO comercialEmi) {
		this.comercialEmi = comercialEmi;
	}
	/**
	 * @return the comercialReno
	 */
	public ComisionesDTO getComercialReno() {
		return comercialReno;
	}
	/**
	 * @param comercialReno the comercialReno to set
	 */
	public void setComercialReno(ComisionesDTO comercialReno) {
		this.comercialReno = comercialReno;
	}
	
}