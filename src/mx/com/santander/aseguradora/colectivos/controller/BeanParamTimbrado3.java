package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		09-11-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Timbrado.
 * 				paramTimbrado.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanParamTimbrado
 * ------------------------------------------------------------------------				
 * 
 */
public class BeanParamTimbrado3 implements Serializable{

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	//Lista para el combo de modalPAramTimbrado
	protected transient Map<String, String> lstNombreProducto;

	//Clase para tener acceso a propiedades genericas.
	protected transient BeanGenerico beanGen;
	
	/**
	 * Constructor de clase
	 */
	public BeanParamTimbrado3() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicializan variables
		this.lstNombreProducto = new HashMap<>();
		
		//Se agrega el bean a session
		beanGen = new BeanGenerico();
		beanGen.setBean(BeanNames.BEAN_PARAM_TIMBRADO);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PARAM_TIMBRADO);
	}
	
	/**
	 * @return the lstNombreProducto
	 */
	public Map<String, String> getLstNombreProducto() {
		return lstNombreProducto;
	}

	/**
	 * @param lstNombreProducto the lstNombreProducto to set
	 */
	public void setLstNombreProducto(Map<String, String> lstNombreProducto) {
		this.lstNombreProducto = lstNombreProducto;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * @return the beanGen
	 */
	public BeanGenerico getBeanGen() {
		return beanGen;
	}
	
}
