package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.EndososDatosId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioEndosoDatos;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.utils.GestorFechas;
import mx.com.santander.aseguradora.colectivos.utils.Utilerias;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		22-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cambia de Credito.
 * 				cambiaIdCredito.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanCambiaCredito extends BeanCambiaCredito2 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio de parametros
	@Resource
	private ServicioGeneric servicioGeneric;

	//recurso para acceder al servicio de endoso datos
	@Resource
	private ServicioEndosoDatos servicioEndosoDatos;
	
	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanCambiaCredito.class);

	/**
	 * Constructor de clase
	 */
	public BeanCambiaCredito() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_CAMBIA_CREDITO);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_CAMBIA_CREDITO);
	}
	
	/**
	 * Metodo que consulta el resumen de errores por archivo
	 * @return mensage de exito o falla
	 */
	public String consulta(){
		List<Object> lista;
		EndosoDatosDTO dto;
		Iterator<Object> it;
		ArrayList<EndosoDatosDTO> arlLista;
		
		try {
			//Se validan datos antes de hacer consulta
			if(this.validaDatos()) {
			
				//Se inicializa lista
				arlLista = new ArrayList<EndosoDatosDTO>();
			
				//Se realiza consulta para mostrar los certificados
				lista = servicioGeneric.consultaSQL(GeneratorQuerys.getQryCambioCredito(getCredito()));
				 
				//se recorren resultados para crear objeto que se utiliza en la vista
				it = lista.iterator();
			    while(it.hasNext()) {
			    	dto = setDatos((Object[]) it.next());
			    	arlLista.add(dto);
				}

				//Se setea propiedad para pantalla
				setListaCertificados(arlLista);
				
				//Se manda respuesta a pantalla
				setStrRespuesta("Se realizo consulta con exito, favor de revisar resultados");
			}
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los errores.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo que refgresa instancia de objeto EndosoDatos
	 * @param objRegistro
	 * @return
	 */
	private EndosoDatosDTO setDatos(Object[] objRegistro) throws Excepciones {
		EndosoDatosDTO objEndoso;
		
		objEndoso = new EndosoDatosDTO();
		objEndoso.setCedaCampon1(Constantes.ENDOSO_CD_DATOS);
		objEndoso.setCedaCampov2(Constantes.ENDOSO_ST_DATOS);
		
		objEndoso.getId().setCedaCasuCdSucursal(((BigDecimal) objRegistro[0]).shortValue());
		objEndoso.getId().setCedaCarpCdRamo(((BigDecimal) objRegistro[1]).shortValue());
		objEndoso.getId().setCedaCapoNuPoliza(((BigDecimal) objRegistro[2]).longValue());
		objEndoso.getId().setCedaNuCertificado(((BigDecimal) objRegistro[3]).longValue());
		objEndoso.getId().setCedaNuEndosoGral(new Long(GestorFechas.formatDate(new Date(), "yyyyMM")));
		objEndoso.getId().setCedaCdEndoso(Constantes.ENDOSO_CD_DATOS);
		
		objEndoso.setCedaStEndoso(Constantes.ENDOSO_ESTATUS_APLICADO);
		objEndoso.setCedatpEndoso(Short.valueOf(Constantes.TIPO_ENDOSO_CREDITO));
		objEndoso.setCedaNuRecibo(Constantes.DEFAULT_BIGDECIMAL);
		objEndoso.setCedaFeSolicitud(new Date());
		objEndoso.setCedaFeRecepcion(new Date());
		objEndoso.setCedaFeAplica(new Date());
		objEndoso.setCedaCdUsuarioAplica(FacesUtils.getBeanSesion().getUserName());
		objEndoso.setCedaCorreoAplica(FacesUtils.getBeanSesion().getObjUsuario().getCorreo());
		objEndoso.setCedaCdUsuarioReg(FacesUtils.getBeanSesion().getUserName());
		objEndoso.setCedaCorreoReg(FacesUtils.getBeanSesion().getObjUsuario().getCorreo());

		
		//Numero Credito (ID1)
		objEndoso.setCedaNombreAnt(objRegistro[4].toString());
		
		//Numero Credito (ID2)
		objEndoso.setCedaNombreNvo(objRegistro[5].toString().trim());
		
		//Regresamos objeto
		return objEndoso;
		
	}

	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	public boolean validaDatos() {

		boolean bRegreso = true;
		
		//Se valida se haya ingresado valor para credito
		if(getCredito().equals(Constantes.DEFAULT_STRING)) {
			setStrRespuesta("Favor de ingresar Numero de Credito");
			bRegreso = false;
		} 

		return bRegreso;
	}
	
	
	/**
	 * Metodo que realiza el cambio de creditos
	 * @throws Excepciones con error en general
	 */
	public void cambiar() throws Excepciones {
		try{
			
			//Se valida que credito 2 sea validp para procesar
			if(getSeleccionado().getCedaNombreNvo().equals(Constantes.DEFAULT_STRING) || getSeleccionado().getCedaNombreNvo().equals(Constantes.DEFAULT_STRING_ID)) {
				FacesUtils.addErrorMessage("Credito (ID2) nos es valido para realizar el cambio.");
				return;
			}
			
			//Se realiza cambio
			actualizarTablas(getSeleccionado().getId(), getSeleccionado().getCedaNombreAnt(), getSeleccionado().getCedaNombreNvo());
			
			//Se completan datos para realizar el endoso
			getSeleccionado().setCedaCampov3("CREDITO");	
			getSeleccionado().getId().setCedaNuEndosoDetalle(servicioGeneric.getNumeroEndoso(getSeleccionado().getId().getCedaCarpCdRamo(), getSeleccionado().getCedaCampov2()));		
			getSeleccionado().setCedaCampov1(getSeleccionado().getCedaCampov2() + Utilerias.agregarCaracteres(Short.toString(getSeleccionado().getId().getCedaCarpCdRamo()), 3, '0', 1) + "-" + getSeleccionado().getId().getCedaNuEndosoGral() + getSeleccionado().getId().getCedaNuEndosoDetalle());
			//Se gaurda endoso de datos
			servicioEndosoDatos.guardarObjeto(getSeleccionado());
			
			//Se reinician valores
			setListaCertificados(new ArrayList<EndosoDatosDTO>());
			setCredito(Constantes.DEFAULT_STRING);
			
			//Se manda respuesta a pantalla
			setStrRespuesta("Se realizo con exito el cambio de Credito. No. Endoso: " +  getSeleccionado().getCedaCampov1());
			
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("No se puede realizar el cambio de creditos." );
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que manda actualizar las tablas
	 * @param datos para realizar el update
	 * @param credito1 credito 1, quedara como anterior
	 * @param credito2 credito 2, quedara como nuevo
	 * @throws Excepciones con error en general
	 */
	private void actualizarTablas(EndososDatosId datos, String credito1, String credito2) throws Excepciones {
		//Se declaran variables
		StringBuilder sbQuery;
		
		try{
			//Se inicializa variable
			sbQuery  = new StringBuilder() ;
			//Se va construyendo update para tabla colectivos_cliente_certif
			sbQuery.append("UPDATE COLECTIVOS_CLIENTE_CERTIF SET COCC_ID_CERTIFICADO = '").append(credito2).append("' \n");
			sbQuery.append("WHERE COCC_CASU_CD_SUCURSAL = " + datos.getCedaCasuCdSucursal() + "\n");
			sbQuery.append("  AND COCC_CARP_CD_RAMO 	= " + datos.getCedaCarpCdRamo() + "\n");
			sbQuery.append("  AND COCC_CAPO_NU_POLIZA 	= " + datos.getCedaCapoNuPoliza() + "\n");
			sbQuery.append("  AND COCC_NU_CERTIFICADO 	= " + datos.getCedaNuCertificado());
			//se manda ejecutar el update
			servicioGeneric.updateSinParametros(sbQuery);

			//Se inicializa variable
			sbQuery  = new StringBuilder() ;
			//Se va construyendo update para tabla colectivos_cliente_certif
			if(datos.getCedaCarpCdRamo() == 5 || datos.getCedaCarpCdRamo() == 65 || datos.getCedaCarpCdRamo() == 82) {
				sbQuery.append("UPDATE COLECTIVOS_CERTIFICADOS SET COCE_NU_CUENTA = '").append(credito2).append("', COCE_BUC_EMPRESA = '").append(credito1).append("' \n");
			} else {
				sbQuery.append("UPDATE COLECTIVOS_CERTIFICADOS SET COCE_NU_CREDITO = '").append(credito2).append("', COCE_BUC_EMPRESA = '").append(credito1).append("' \n");
			}
			sbQuery.append("WHERE COCE_CASU_CD_SUCURSAL = " + datos.getCedaCasuCdSucursal()  + "\n");
			sbQuery.append("  AND COCE_CARP_CD_RAMO 	= " + datos.getCedaCarpCdRamo() + "\n");
			sbQuery.append("  AND COCE_CAPO_NU_POLIZA 	= " + datos.getCedaCapoNuPoliza() + "\n");
			sbQuery.append("  AND COCE_NU_CERTIFICADO 	= " + datos.getCedaNuCertificado());
			//se manda ejecutar el update
			servicioGeneric.updateSinParametros(sbQuery);
			
			
			//Se inicializa variable
			sbQuery  = new StringBuilder() ;
			//Se va construyendo update para tabla cart_Certificados
			sbQuery.append("UPDATE CART_CERTIFICADOS SET CACE_DI_COBRO2 = '").append(credito2).append("' \n");
			sbQuery.append("WHERE CACE_CASU_CD_SUCURSAL = " + datos.getCedaCasuCdSucursal() + "\n");
			sbQuery.append("  AND CACE_CARP_CD_RAMO 	= " + datos.getCedaCarpCdRamo() + "\n");
			sbQuery.append("  AND CACE_CAPO_NU_POLIZA 	= " + datos.getCedaCapoNuPoliza() + "\n");
			sbQuery.append("  AND CACE_NU_CERTIFICADO 	= " + datos.getCedaNuCertificado());
			//se manda ejecutar el update
			servicioGeneric.updateSinParametros(sbQuery);
			
			//Se inicializa variable
			sbQuery  = new StringBuilder() ;
			//Se va construyendo update para tabla cart_Certificados
			sbQuery.append("UPDATE CARH_CERTIFICADOS SET CACE_DI_COBRO2 = '").append(credito2).append("' \n");
			sbQuery.append("WHERE CACE_CASU_CD_SUCURSAL = " + datos.getCedaCasuCdSucursal() + "\n");
			sbQuery.append("  AND CACE_CARP_CD_RAMO 	= " + datos.getCedaCarpCdRamo() + "\n");
			sbQuery.append("  AND CACE_CAPO_NU_POLIZA 	= " + datos.getCedaCapoNuPoliza() + "\n");
			sbQuery.append("  AND CACE_NU_CERTIFICADO 	= " + datos.getCedaNuCertificado());
			//se manda ejecutar el update
			servicioGeneric.updateSinParametros(sbQuery);
			
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("No se puede realizar el update." );
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que realiza el cambio de creditos
	 * @throws Excepciones con error en general
	 */
	public void setDatos() throws Excepciones {
		setBean(BeanNames.BEAN_CAMBIA_CREDITO);
	}
	
	
	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}

	/**
	 * @param servicioEndosoDatos the servicioEndosoDatos to set
	 */
	public void setServicioEndosoDatos(ServicioEndosoDatos servicioEndosoDatos) {
		this.servicioEndosoDatos = servicioEndosoDatos;
	}
	
}
