package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.view.dto.CedulaControlDTO;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cedula Control.
 * 				cedulaControl.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanCedula
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanCedula2 extends BeanCedula3 implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para objetos nuevos o modificados
	private CedulaControlDTO seleccionado;
	//Propiedad para lista de cedulas actuales
	private List<CedulaControlDTO> listaActual;
	//Propiedad para lista de cedulas historicas
	private List<CedulaControlDTO> listaHist;
	//Propiedad para lista de detalle
	private List<CedulaControlDTO> listaDetalle;
	
	/**
	 * Constructor de clase
	 */
	public BeanCedula2() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.seleccionado = new CedulaControlDTO();
		this.listaActual = new ArrayList<CedulaControlDTO>();
		this.listaHist = new ArrayList<CedulaControlDTO>();
		this.listaDetalle = new ArrayList<CedulaControlDTO>();
	}
	
	/**
	 * @return the seleccionado
	 */
	public CedulaControlDTO getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(CedulaControlDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the listaActual
	 */
	public List<CedulaControlDTO> getListaActual() {
		return new ArrayList<CedulaControlDTO>(listaActual);
	}
	/**
	 * @param listaActual the listaActual to set
	 */
	public void setListaActual(List<CedulaControlDTO> listaActual) {
		this.listaActual = new ArrayList<CedulaControlDTO>(listaActual);
	}
	/**
	 * @return the listaHist
	 */
	public List<CedulaControlDTO> getListaHist() {
		return new ArrayList<CedulaControlDTO>(listaHist);
	}
	/**
	 * @param listaHist the listaHist to set
	 */
	public void setListaHist(List<CedulaControlDTO> listaHist) {
		this.listaHist = new ArrayList<CedulaControlDTO>(listaHist);
	}
	/**
	 * @return the listaDetalle
	 */
	public List<CedulaControlDTO> getListaDetalle() {
		return new ArrayList<CedulaControlDTO>(listaDetalle);
	}
	/**
	 * @param listaDetalle the listaDetalle to set
	 */
	public void setListaDetalle(List<CedulaControlDTO> listaDetalle) {
		this.listaDetalle = new ArrayList<CedulaControlDTO>(listaDetalle);
	}
}
