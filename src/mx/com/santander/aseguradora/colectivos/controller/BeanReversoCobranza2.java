package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Certificado;
import mx.com.santander.aseguradora.colectivos.model.bussinessObject.CertificadoId;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		15-12-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Reverso Estatus Cobranza.
 * 				reversoEstatusCobranza.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanReversoCobranza
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	  
 * 		Cuando:  
 * 		Porque:  
 * ------------------------------------------------------------------------
 * 
 */
public class BeanReversoCobranza2 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//Propiedad para lista de resultados
	protected transient List<Certificado> lstCertificados;
	//Propiedad para filtro por credito
	private String credito;
	//Propiedad para certificado seleccionado
	private Certificado seleccionado;		
	//Clase para tener acceso a propiedades genericas.
	protected transient BeanGenerico beanGen;
	
	/**
	 * Constructor de clase
	 */
	public BeanReversoCobranza2() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicializa lista
		this.lstCertificados = new ArrayList<Certificado>();
				
		//Se inicialisan valores por default
		this.credito = Constantes.DEFAULT_STRING;
		this.seleccionado 	= new Certificado();
		
		//Se agrega el bean a session
		beanGen = new BeanGenerico();
		beanGen.setBean(BeanNames.BEAN_REVERSO_COBRANZA);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_REVERSO_COBRANZA);
	}

	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	public boolean validaDatos() {

		//Se valida se haya ingresado valor para realizar consulta
		if(getCredito().equals(Constantes.DEFAULT_STRING) ) {
			beanGen.setStrRespuesta("Favor de ingresar datos para realizar la consulta");
		
			//Se regresa falso
			return false;
		} 
		
		//Se agrega filtro por credito
		if(getCredito().length() > 0){
			beanGen.getFiltro().append(" AND D.COCD_COCE_NU_CREDITO	= '").append(getCredito()).append("' \n");
		}

		//Se regresa verdadero
		return true;
	}
	
	/**
	 * Metodo que regresa instancia de objeto DetalleCertificadoDTO
	 * @param objRegistro informacion de base de datos
	 * @param arlCerts hash map con objetos a presentar en pantalla
	 * @throws Excepciones con error en general
	 */
	public void setDatos(Object[] objRegistro, List<Certificado> arlCerts) {
		//Creamos objetos e inicializamos objetos 
		Certificado objCert = new Certificado();
		
		//Asignamos valores de objeto (certificado)
		objCert.setId(new CertificadoId());
		objCert.getId().setCoceCasuCdSucursal(((BigDecimal)objRegistro[0]).shortValue());
		objCert.getId().setCoceCarpCdRamo(((BigDecimal)objRegistro[1]).shortValue());
		objCert.getId().setCoceCapoNuPoliza(((BigDecimal)objRegistro[2]).longValue());
		objCert.getId().setCoceNuCertificado(((BigDecimal)objRegistro[3]).longValue());
		
		objCert.setCoceNoRecibo((BigDecimal)objRegistro[4]);
		objCert.setCoceNuCredito(objRegistro[5].toString());		
		objCert.setCoceCampon1(Long.valueOf(objRegistro[6].toString()));
		objCert.setCoceCampov1(objRegistro[7].toString());
		objCert.setCoceCampov2(objRegistro[8].toString());
		objCert.setCoceCdCausaAnulacion(((BigDecimal)objRegistro[9]).shortValue());
		objCert.setCoceCampov3(objRegistro[10].toString());
		objCert.setCoceFeAnulacionCol((Date) objRegistro[11]);
		objCert.setCoceFeEmision((Date) objRegistro[12]);
		objCert.setCoceMtPrimaPura((BigDecimal)objRegistro[13]);
		objCert.setCoceMtPrimaSubsecuente((BigDecimal)objRegistro[14]);
		objCert.setCoceNuCobertura(Long.valueOf(objRegistro[15].toString()));
		objCert.setCoceCampov4(objRegistro[16].toString());
		objCert.setCoceFeDesde((Date) objRegistro[17]);
		objCert.setCoceFeHasta((Date) objRegistro[18]);
		objCert.setCoceFeCarga((Date) objRegistro[19]);
		objCert.setCoceMtSumaAsegurada(new BigDecimal(objRegistro[20].toString()));
		objCert.setCoceFeFacturacion((Date) objRegistro[21]);
		objCert.setCoceNuMovimiento(Long.valueOf(objRegistro[22].toString()));
		
		if(objCert.getCoceNuCobertura() == 2L) {
			objCert.setCoceMtTotalPrestamo(objCert.getCoceMtPrimaSubsecuente().subtract(objCert.getCoceMtSumaAsegurada()));
		}
		
		//Se agrega el nuevo certificado a la lista
		arlCerts.add(objCert);
	}
	
	
	/**
	 * @return the lstCertificados
	 */
	public List<Certificado> getLstCertificados() {
		return new ArrayList<Certificado>(lstCertificados);
	}
	/**
	 * @param lstCertificados the listaCreditos to set
	 */
	public void setLstCertificados(List<Certificado> lstCertificados) {
		this.lstCertificados = new ArrayList<Certificado>(lstCertificados);
	}
	
	/**
	 * @return the credito
	 */
	public String getCredito() {
		return credito;
	}
	/**
	 * @param credito the credito to set
	 */
	public void setCredito(String credito) {
		this.credito = credito;
	}
	/**
	 * @return the seleccionado
	 */
	public Certificado getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(Certificado seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the beanGen
	 */
	public BeanGenerico getBeanGen() {
		return beanGen;
	}
}
