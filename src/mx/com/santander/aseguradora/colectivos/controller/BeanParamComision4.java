package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-05-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla parametrizacion de Comisiones.
 * 				parametrizacionComisiones.jsp
 * 				Esta clase contine solo propiedades que son utilizadas por 
 * 				la clase BeanParamComision
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanParamComision4 extends BeanParamComision5 implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para tipo parametrizacion
	private Integer tipoParam;
	//Propiedad para tipo cobertura
	private Integer tipoCobertura;
	//Propiedad para tipo comision
	private Integer tipoComision;
	//Propiedad para descripcion
	private String descripcion;
	
	/**
	 * Constructor de clase
	 */
	public BeanParamComision4() {
		//se manda ejecutar constructor de clase padre
		super();
	}
	
	/**
	 * @return the tipoParam
	 */
	public Integer getTipoParam() {
		return tipoParam;
	}
	/**
	 * @param tipoParam the tipoParam to set
	 */
	public void setTipoParam(Integer tipoParam) {
		this.tipoParam = tipoParam;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the tipoCobertura
	 */
	public Integer getTipoCobertura() {
		return tipoCobertura;
	}
	/**
	 * @param tipoCobertura the tipoCobertura to set
	 */
	public void setTipoCobertura(Integer tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	/**
	 * @return the tipoComision
	 */
	public Integer getTipoComision() {
		return tipoComision;
	}
	/**
	 * @param tipoComision the tipoComision to set
	 */
	public void setTipoComision(Integer tipoComision) {
		this.tipoComision = tipoComision;
	}
}