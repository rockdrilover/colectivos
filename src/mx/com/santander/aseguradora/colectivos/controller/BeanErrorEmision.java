package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;

import mx.com.santander.aseguradora.colectivos.model.bo.BitacoraErrores;
import mx.com.santander.aseguradora.colectivos.model.bo.CifrasControl;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioErrorEmision;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.BitacoraErroresDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.CifrasControlDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Erores de Emision.
 * 				errorEmision.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanErrorEmision extends BeanErrorEmision2 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;

	//recurso para acceder al servicio
	@Resource
	private ServicioErrorEmision servicioError;	
	
	//LOG para escribir errores o informacion necesaria
	private static final Log LOG = LogFactory.getLog(BeanErrorEmision.class);

	/**
	 * Constructor de clase
	 */
	public BeanErrorEmision() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_ERROR_EMISION);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_ERROR_EMISION);
	}
	
	/**
	 * Metodo que consulta el resumen de errores por archivo
	 * @return mensage de exito o falla
	 */
	public String consulta(){
		List<Object> lista;
		CifrasControlDTO dto;
		CifrasControl cifras;
		Iterator<Object> it;
		ArrayList<CifrasControlDTO> arlLista;
		
		try {
			//Se inicializa lista
			arlLista = new ArrayList<CifrasControlDTO>();
			
			//Se validan datos antes de hacer consulta
			if(this.validaFechas()) {
				//Se realiza consulta para mostrar los procesos configurados
				lista = this.servicioError.consulta(getNombreLayout(), getFeDesde(), getFeHasta());
				 
				//se recorren resultados para crear objeto que se utiliza en la vista
				it = lista.iterator();
			    while(it.hasNext()) {
			    	cifras = (CifrasControl) it.next();
			    	dto = new CifrasControlDTO(cifras);
			    	arlLista.add(dto);
				}
			    
				//Se manda respuesta a pantalla
				setStrRespuesta("Se realizo consulta con exito, favor de revisar resultados");
			}
			
			//Se setea propiedad para pantalla
			setListaArchivos(arlLista);
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los errores.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo que consulta detalle de errores de un archivo
	 */
	public void getDetalle() {
		List<Object> lista;
		BitacoraErroresDTO dto;
		BitacoraErrores detalle;
		Iterator<Object> it;
		ArrayList<BitacoraErroresDTO> arlLista;
		
		try{
			//Se inicializa lista
			arlLista = new ArrayList<BitacoraErroresDTO>();
			
			//Se realiza consulta para mostrar los procesos configurados
			lista = this.servicioError.getDetalleErrores(getSeleccionado().getCociNombreArchivo());
		
			//se recorren resultados para crear objeto que se utiliza en la vista
			it = lista.iterator();
		    while(it.hasNext()) {
		    	detalle = (BitacoraErrores) it.next();
		    	dto = new BitacoraErroresDTO(detalle);
		    	if(detalle.getBe2().getCoerNvalor1().equals(Constantes.DEFAULT_BIGDECIMAL)) {
		    		dto.setCheckHabilita(true);
		    	} else {
		    		dto.setCheckHabilita(false);
		    	}
		    	
		    	arlLista.add(dto);
			}
		    
		    //Se osetea valor a propiedad de pantalla
		    setLstDetalleArchivo(arlLista);
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar detalle.");
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que emite los errores seleccionados
	 * @throws Excepciones con error en general
	 */
	public void emiteErrores() throws Excepciones {
		List<BitacoraErroresDTO> lista;
		
		try{
			lista = validaLista();
			
			if(lista.isEmpty()) {
				return;
			}
			
			if(getSeleccionado().getCociNombreArchivo().contains("PU") || getSeleccionado().getCociNombreArchivo().contains("SEGUROSZURICH")) {
				setStrRespuesta(this.servicioError.emitePU(getSeleccionado().getCociNombreArchivo(), getSeleccionado().getCociCdControl(), lista));
			} else {
				setStrRespuesta(this.servicioError.emitePR(getSeleccionado().getCociNombreArchivo(), getSeleccionado().getCociCdControl(), lista));
			}
			
			//Se limpian resultados
			this.limpiaResultados();
			
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("No se puede realizar el proceso de emision." );
			//Cualqueir error se reporta al LOG
			LOG.error(getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que valida la lista detalle
	 * @return nueva lista con creditos seleccionados
	 */
	private List<BitacoraErroresDTO> validaLista() {
		List<BitacoraErroresDTO> lista = new ArrayList<BitacoraErroresDTO>();
		BitacoraErroresDTO dto;
		Iterator<BitacoraErroresDTO> it;
		
		//Se valida que haya creditos para procesar
		if(getLstDetalleArchivo().isEmpty()) {
			FacesUtils.addErrorMessage("No hay creditos para procesar.");
			return lista;
		}

		//Se recorre lista detalle para procesar registros seleccionados
		it = getLstDetalleArchivo().iterator();
		while(it.hasNext()) {
			dto = it.next();
			if(dto.isCheck()) {
				lista.add(dto);
			}
		}

		//Se valida que hayan seleccionado al menos un credito para procesar
		if(lista.isEmpty()) {
			FacesUtils.addErrorMessage("Favor de seleccionar un credito de la lista.");
			return lista;
		}
		
		return lista;
	}
	
	/**
	 * Metodo que limpia los resultados
	 * @return  mensage de exito o falla
	 */
	public String limpiaResultados(){
		if(getListaArchivos() != null){
			getListaArchivos().clear();		
		} 
		
		setFeDesde(new Date());
		setFeHasta(new Date());
		setNombreLayout(Constantes.DEFAULT_STRING);
		
		return NavigationResults.SUCCESS;
	}
	
	/**
	 * @param servicioError the servicioError to set
	 */
	public void setServicioError(ServicioErrorEmision servicioError) {
		this.servicioError = servicioError;
	}
	
}
