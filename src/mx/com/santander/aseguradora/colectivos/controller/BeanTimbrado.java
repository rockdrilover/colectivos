package mx.com.santander.aseguradora.colectivos.controller;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioCertificado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioTimbrado;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.utils.GeneratorQuerys;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqCancelacionCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqDescargaCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ReqTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResCancelacionCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResDescargaCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.ResTimbradoCFDIDTO;
import mx.com.santander.aseguradora.colectivos.view.dto.TimbradoDTO;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		09-04-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Timbrado.
 * 				timbrado.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanTimbrado extends BeanTimbrado2 {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder al servicio
	@Resource
	private ServicioTimbrado servicioTimbrado;
	//recurso para acceder a metodos generacion de archivos y cancelacion de CFDI
	@Resource
	private ServicioCertificado servicioCertificado;
	
	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanTimbrado.class);
	
	//Constantes para no repetir codigo de ip
	private static final String IP = "0:0:0:0:0:0:0:1";
	//Constantes para no repetir codigo de formater
	private static final String FORMATER = "X-FORWARDED-FOR";
	
	/**
	 * Constructor de clase
	 */
	public BeanTimbrado() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se agrega el bean a session
		setBean(BeanNames.BEAN_TIMBRADO);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_TIMBRADO);
	}
	
	/**
	 * Metodo que consulta el resumen de creditos a cancelar
	 * @return mensage de exito o falla
	 */
	public String consulta() {
		//Se declaran variables
		List<Object> lista;
		Iterator<Object> it;
		Object[] datos;
		ArrayList<TimbradoDTO> arlLista;
		
		try {
			//Se inicializa lista
			arlLista = new ArrayList<TimbradoDTO>();
			
			//Se validan datos antes de hacer consulta
			if(validaDatos()) {
				//Se realiza consulta para mostrar los procesos configurados
				lista = this.servicioTimbrado.consulta(getFiltro(), getFeDesde(), getFeHasta(), getTipoBusqueda());
				 
				//se recorren resultados para crear objeto que se utiliza en la vista
				it = lista.iterator();
			    while(it.hasNext()) {
			    	datos = (Object[]) it.next();
			    	arlLista.add(new TimbradoDTO(datos, Constantes.DEFAULT_INT));
				}

			    //Se pasa lista a propiedad de pantalla
				setListaTimbrado(arlLista);
				
				//Se manda respuesta a pantalla
				setStrRespuesta("Se realizo consulta con exito, favor de revisar resultados");
			}
			
			//se regresa mensage de SUCCESS a la vista
			return NavigationResults.SUCCESS;
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar los errores.");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);
			//se regresa mensage de FAILURE a la vista
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo que consulta detalle de un timbrado
	 */
	public void getDetalle() {
		try {
			//Se realiza consulta para mostrar el detalle de creditos a cancelar
			setSeleccionado(this.servicioTimbrado.getDetalle(getSeleccionado().getFactura().getCofaNuReciboFiscal()));
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar detalle.");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
		
	/**
	 * Descarga PDF o XML de un CFDI relacionado al recibo
	 */
	public void descargaCFDI() {
		//Declaracion de variables
		String ip = Constantes.DEFAULT_STRING;
		String[] arrDatosPol;
		HttpServletRequest request;
		ReqDescargaCFDIDTO reqdcDTO;
		ResDescargaCFDIDTO resdcDTO; 
		
		try {
			//Obtenemos request
			request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			//Validacones para obtener IP
			ip = request.getHeader(FORMATER);
	        if (ip == null || "".equals(ip)) {
	       	 ip = request.getRemoteAddr();
	        }
	        
	        if (IP.equalsIgnoreCase(ip)) {
	       	    InetAddress inetAddress = InetAddress.getLocalHost();
	       	    String ipAddress = inetAddress.getHostAddress();
	       	    ip = ipAddress;
	       	}
			
	        //Obtenemos datos de poliza
	        arrDatosPol = getSeleccionado().getFactura().getCofaCampov4().split("\\-");

	        //Inicializamos objeto REQ
	        reqdcDTO = new ReqDescargaCFDIDTO(
					FacesUtils.getBeanSesion().getUserName(), 
					Constantes.CFDI_APLICACION, 
					ip, 
					String.valueOf(arrDatosPol[0]), 
					String.valueOf(arrDatosPol[1]), 
					String.valueOf(arrDatosPol[2]), 
					String.valueOf(arrDatosPol[3]), 
					getSeleccionado().getFactura().getCofaNuReciboFiscal().toString(), 
					getSeleccionado().getFactura().getCofaCampov3(), 
					getTipoDoc());
			
			//Se manda llamar servicio para descarga
	        resdcDTO = this.servicioCertificado.descargaCFDI(reqdcDTO);
	        
	        //Se valida respuesta del servicio
	        if (Constantes.CFDI_DESCARGAR_EXISTOSO.equals(resdcDTO.getMensaje())) {
	        	descargaArchivo(resdcDTO.getBytes(), resdcDTO.getFnombre(), "application/" + getTipoDoc());
			} else {
				String resMensaje = resdcDTO.getMensaje() + " - " + resdcDTO.getDescripcion();
				descargaArchivo(resMensaje.getBytes("UTF-8"), "errorDescarga.txt", "application/txt");
				FacesUtils.addInfoMessage(resMensaje);
			}
	        
	        //Se compelta response
	        FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al generar el PDF o XML del CFDI.");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}
	}
	
	
	/**
	 * Metodo que consulta historial de recibos
	 */
	public void consultaHistorial() {
		List<Object> lstResultados;
		ArrayList<TimbradoDTO> arlLista;
		TimbradoDTO objHist;
		Iterator<Object> it;
		
		try {
			//Se validan datos para consultar
			if(validaDatos2()) {
				arlLista = new ArrayList<TimbradoDTO>();
				
				if(getNuevo().getRecibo().getcoreCaceNuCertificado() > 0) {
					//Se realiza consulta para mostrar historial por certificado
					lstResultados = getServicioGeneric().consultaSQL(GeneratorQuerys.getQryHistRecCerti(getNuevo().getRecibo().getcoreCarpCdRamo(), 
																						getNuevo().getRecibo().getcoreCapoNuPoliza(),
																						getNuevo().getRecibo().getcoreCaceNuCertificado(),
																						getNuevo().getFactura().getCofaNuReciboFiscal(),
																						getNuevo().getFactura().getCofaCampon2()));
				} else {
					//Se realiza consulta para mostrar historial por poliza
					lstResultados = getServicioGeneric().consultaSQL(GeneratorQuerys.getQryHistRecPoliza(getNuevo().getRecibo().getcoreCarpCdRamo(), 
																						getNuevo().getRecibo().getcoreCapoNuPoliza(),
																						getNuevo().getRecibo().getcoreCaceNuCertificado(),
																						getNuevo().getFactura().getCofaNuReciboFiscal(),
																						getNuevo().getFactura().getCofaCampon2()));
				}
				
				
				//se recorren resultados para crear objeto que se utiliza en la vsita
			    it = lstResultados.iterator();
			    while(it.hasNext()) {
			    	objHist = new TimbradoDTO((Object[]) it.next(), 2);
			    	objHist.getRecibo().setcoreCarpCdRamo(getNuevo().getRecibo().getcoreCarpCdRamo()); 
			    	objHist.getRecibo().setcoreCapoNuPoliza(getNuevo().getRecibo().getcoreCapoNuPoliza());
			    	objHist.getRecibo().setcoreCaceNuCertificado(getNuevo().getRecibo().getcoreCaceNuCertificado());
			    	objHist.getFactura().setCofaCampon2(getNuevo().getFactura().getCofaCampon2());
					
			    	arlLista.add(objHist);
				}
			    
			    setListaHist(arlLista);
			}
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar datos.");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}
	
	/**
	 * Metodo que consulta datos para nuevo timbrado
	 */
	public void consultaDatosTimbrado() {
		//Declaracion de variables
		Parametros objParam;
		
		try {
			//Se validan datos para consultar
			if(validaDatos2()) {
				//Inserta parametro para saber que es consulta.
				objParam = setParametro(getServicioParametros().siguente(), 
						getNuevo().getRecibo().getcoreCaceNuCertificado(), 
						getNuevo().getFactura().getCofaNuReciboFiscal(),
						1);
				getServicioParametros().guardarObjeto(objParam);

				//Se realiza consulta para mostrar los datos del timbrado
				setNuevo(this.servicioTimbrado.getDatosTimbrado(getNuevo().getRecibo().getcoreCarpCdRamo(), 
														getNuevo().getRecibo().getcoreCapoNuPoliza(),
														getNuevo().getRecibo().getcoreCaceNuCertificado(),
														getNuevo().getFactura().getCofaNuReciboFiscal()));
				
				//Obtenemos razon social del Emisor
				getNuevo().getRecibo().setcoreNuReciboAceptado(getRazonSocialEmisor(getNuevo().getRecibo().getcoreCacnCdNacionalidad()));
				
				//Se valida si hubo error 
				if(!getNuevo().getRecibo().getcoreCdErrorCobro().equals(Constantes.DEFAULT_STRING)) {
					FacesUtils.addErrorMessage(getNuevo().getRecibo().getcoreCdErrorCobro());
				}
			}
		} catch (Excepciones e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al consultar datos.");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}	
	}

	/**
	 * Cancelar CFDI con Version 3.3
	 * @throws Excepciones Error en timbrado
	 */
	public void cancelarRecibos() throws Excepciones {
		//Declaracion de variables
		HttpServletRequest request;
		ReqCancelacionCFDIDTO reqccDTO;
		ResCancelacionCFDIDTO resccDTO;
		String ip;
			
		try {
			//Obtenemos request
			request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			//Validacones para obtener IP
			ip = request.getHeader(FORMATER);
	        if (ip == null || "".equals(ip)) {
	       	 ip = request.getRemoteAddr();
	        }
	        
	        if (IP.equalsIgnoreCase(ip)) {
	       	    InetAddress inetAddress = InetAddress.getLocalHost();
	       	    String ipAddress = inetAddress.getHostAddress();
	       	    ip = ipAddress;
	       	}
			
	        //Inicializamos objeto REQ
			reqccDTO = new ReqCancelacionCFDIDTO(
						FacesUtils.getBeanSesion().getUserName(), 
						Constantes.CFDI_APLICACION, 
						ip,
						String.valueOf(getSeleccionado().getFactura().getCofaCampov4().split("\\-")[0]),
						getSeleccionado().getFactura().getCofaNuReciboFiscal().toString());
			
			//Se manda llamar servicio para descarga
			resccDTO = servicioCertificado.cancelaCFDI(reqccDTO);
			
			//Se valdia respuesta
			if (Constantes.CFDI_CANCELAR_EXISTOSO.equals(resccDTO.getMensaje())) {
				setStrRespuesta("Se realizo cancelacion con exito");
			} else {
				setStrRespuesta("Error al cancelar recibo: " + resccDTO.getMensaje() + " - " + resccDTO.getDescripcion());
			}
						
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al cancelar recibos..");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}
	}
	
	/**
	 * Timbra CFDI con Version 3.3
	 * @throws Excepciones Error en timbrado
	 */
	public void timbrar() throws Excepciones {
		//Declaracion de variables
		HttpServletRequest request;
		ReqTimbradoCFDIDTO reqtcDTO;
		ResTimbradoCFDIDTO restcDTO;
		Parametros objParam;
		String ip;
			
		try {
			//Inserta parametro para saber que es consulta.
			objParam = setParametro(getServicioParametros().siguente(),
					getNuevo().getRecibo().getcoreCaceNuCertificado(), 
					getNuevo().getFactura().getCofaNuReciboFiscal(),
					2);
			objParam.setCopaRegistro(getCadenaValores(getNuevo()));
			getServicioParametros().guardarObjeto(objParam);
			
			//Obtenemos request
			request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			//Validacones para obtener IP
			ip = request.getHeader(FORMATER);
	        if (ip == null || "".equals(ip)) {
	        	ip = request.getRemoteAddr();
	        }
	        
	        if (IP.equalsIgnoreCase(ip)) {
	       	    InetAddress inetAddress = InetAddress.getLocalHost();
	       	    String ipAddress = inetAddress.getHostAddress();
	       	    ip = ipAddress;
	       	}
			
	        //Inicializamos objeto REQ
	        reqtcDTO = new ReqTimbradoCFDIDTO(
						FacesUtils.getBeanSesion().getUserName(), 
						Constantes.CFDI_APLICACION, 
						ip,
						"1",
						String.valueOf(getNuevo().getRecibo().getcoreCarpCdRamo()),
						String.valueOf(getNuevo().getRecibo().getcoreCapoNuPoliza()),
						String.valueOf(getNuevo().getRecibo().getcoreCaceNuCertificado()),
						getNuevo().getFactura().getCofaNuReciboFiscal().toString(),
						true);
			
			//Se manda llamar servicio para descarga
			restcDTO = servicioCertificado.sellarRecibosV33(reqtcDTO);
			
			//Se valdia respuesta
			if (Constantes.CFDI_TIMBRAR_EXISTOSO.equals(restcDTO.getMensaje())) {
				setStrRespuesta("Se realizo timbrado con exito");
			} else {
				setStrRespuesta("Error al timbrar recibo: " + restcDTO.getMensaje() + " - " + restcDTO.getDescripcion());
			}
			
			//Se compelta response
			FacesUtils.addErrorMessage(getStrRespuesta());
		} catch (Exception e) {
			//Se manda respuesta a pantalla
			setStrRespuesta("Error al cancelar recibos..");
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);			
			throw new FacesException(getStrRespuesta(), e);
		}
	}

	/**
	 * @param servicioTimbrado the servicioError to set
	 */
	public void setServicioTimbrado(ServicioTimbrado servicioTimbrado) {
		this.servicioTimbrado = servicioTimbrado;
	}
	/**
	 * @param servicioCertificado the servicioCertificado to set
	 */
	public void setServicioCertificado(ServicioCertificado servicioCertificado) {
		this.servicioCertificado = servicioCertificado;
	}
}
