package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Parametros;
import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.exception.ObjetoDuplicado;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.FacesUtils;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanNames;
import mx.com.santander.aseguradora.colectivos.view.bean.BeanParametros;
import mx.com.santander.aseguradora.colectivos.view.bean.NavigationResults;
import mx.com.santander.aseguradora.colectivos.view.builder.ConstruirObjeto;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-11-2022
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla producto comision.
 * 				nombreProducto.jsp
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
@Controller
@Scope("session")
public class BeanNombreProducto extends BeanNombreProducto2 implements Serializable {

	//Propiedad para implementar serializable
	private static final long serialVersionUID = 1L;
	
	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanReporteCobranza.class);
	
	//recurso para acceder al servicio
	@Resource
	private transient ServicioParametros servicioParametros;

	//Clase para tener acceso a propiedades genericas.
	protected transient BeanGenerico beanGen;
	
	/**
	 * Constructor de clase
	 */
	public BeanNombreProducto () {
		super();

		//Se agrega el bean a session
		beanGen = new BeanGenerico();
		beanGen.setBean(BeanNames.BEAN_PRODUCTO_COMISION);
		FacesUtils.getBeanSesion().setBean(BeanNames.BEAN_PRODUCTO_COMISION);
	}
	
	/**
	 * Metodo que se utiliza para dar de alta un producto
	 * @return regresa resultado de operacion
	 */
	public String altaProducto(){
		Long secParametro;

		try {
			//Se obtiene siguiente id
			secParametro = servicioParametros.siguente(); 
			
			//Se inicializan valores
			getObjNuevo().setCopaCdParametro(secParametro); 	// SIGUIENTE NUMERO 
			getObjNuevo().setCopaIdParametro("NOMBRE_PRODUCTO");// NOMBRE_PRODUCTO
			getObjNuevo().setCopaDesParametro("CATALOGO"); 		// CATALOGO
			getObjNuevo().setCopaNvalor1(1);					// CANAL 1-DEFAULT
				
			//Se obtiene objeto para mandar guardar
			BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
			Parametros parametro = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, getObjNuevo());
			
			//Se manda guardar objneto nuevo
			this.servicioParametros.guardarObjeto(parametro);
			
			//Se muestra respuesta
			beanGen.setStrRespuesta("Se guardo con EXITO el registro.");
			
			//Inicializamos objeto nuevo
			objNuevo = new BeanParametros();
			
			//Consultamos registros
			consultarProducto();
			
			return NavigationResults.SUCCESS;
		} catch (ObjetoDuplicado e) {
			beanGen.setStrRespuesta("Error Registro duplicado.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
			return NavigationResults.RETRY;
		} catch (Excepciones e) {
			beanGen.setStrRespuesta("No se puede dar de alta el segmento.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
			return NavigationResults.FAILURE;				
		}
	}
	
	/**
	 * Metodo que se ocupara para modificar un regsitro
	 */
	public void modificarReg() {
		try {
			//Se crea objeto para poder modificar
			Parametros objParam = (Parametros) ConstruirObjeto.crearObjeto(Parametros.class, objActual);
			
			//Se manda modificar registro
			servicioParametros.actualizarObjeto(objParam);
			
			//Quitamos de los resultados el objeto a modificar
			hmResultados.remove(objActual.getCopaCdParametro().toString());
			
			//Aggregamos el objeto modificado a los resultados
			hmResultados.put(objActual.getCopaCdParametro().toString(), objActual);
			
			//Se envia respuesta
			beanGen.setStrRespuesta("Se modifico con EXITO el registro.");
		} catch (Exception e) {
			beanGen.setStrRespuesta("Error al modificar registro.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
		}
	}
	
	/**
	 * Metodo que se ocupa para consultar registros
	 * @return regresa resultado de operacion
	 */
	public String consultarProducto(){
		String filtro = "";
		List<Object> lstResultados;
		Parametros parametro;
		BeanParametros bean;

		try {
			//Se inicialisa lista de registros
			hmResultados = new HashMap<String, Object>();
			
			//Se va construyendo filttro
			filtro = " and P.copaDesParametro = 'CATALOGO' \n" + " and P.copaIdParametro = 'NOMBRE_PRODUCTO' \n";
			
			//Se ejecuta consulta
			lstResultados = this.servicioParametros.obtenerObjetos(filtro);
			
			//Si recorren resultados para agregar objetos a lista final
			Iterator<Object> it = lstResultados.iterator();
			while(it.hasNext()) {
				parametro = (Parametros) it.next();
				BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
				bean = (BeanParametros) ConstruirObjeto.crearBean(BeanParametros.class, parametro);
				bean.setServicioParametros(this.servicioParametros);
				
				hmResultados.put(bean.getCopaCdParametro().toString(), bean);
			}

			//Se envia respuesta de ejecucion
			beanGen.setStrRespuesta("Se ejecuto con EXITO la consulta, favor de revisar resultados.");
			
			return NavigationResults.SUCCESS;
		} catch (Exception e) {
			beanGen.setStrRespuesta("Error al ejecutar consulta.");
			LOG.error(Logger.SECURITY_FAILURE, beanGen.getStrRespuesta(), e);
			return NavigationResults.FAILURE;
		}
	}
	
	/**
	 * Metodo que obtiene los valores
	 * @return lista de valores
	 */
	public List<Object> getMapValues(){
		ArrayList<Object> lstValues = new ArrayList<Object>();
        lstValues.addAll(getHmResultados().values());
        return lstValues;
    }
	
	/**
	 * Metodo parar setear recurso
	 * @param servicioParametros servicio a utilizar
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}

	/**
	 * @return the beanGen
	 */
	public BeanGenerico getBeanGen() {
		return beanGen;
	}

}
