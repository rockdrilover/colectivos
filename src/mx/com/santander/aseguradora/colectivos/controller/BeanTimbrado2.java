package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioGeneric;
import mx.com.santander.aseguradora.colectivos.model.service.ServicioParametros;
import mx.com.santander.aseguradora.colectivos.utils.Constantes;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-04-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Timbrado.
 * 				timbrado.jsp
 * 				Esta clase contine propiedades y metodos que son utilzadas por 
 * 				la clase BeanTimbrado
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanTimbrado2 extends BeanTimbrado3 implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//recurso para acceder a metodos de Parametros
	@Resource
	private ServicioParametros servicioParametros;
	//recurso para acceder al servicio de parametros
	@Resource
	private ServicioGeneric servicioGeneric;

	//Para escribir en log
	private static final Logger LOG = ESAPI.getLogger(BeanTimbrado2.class);
	
	/**
	 * Constructor de clase
	 */
	public BeanTimbrado2() {
		//se manda ejecutar constructor de clase padre
		super();
	}
	
	/**
	 * Metodo que valida los datos utilizados para hacer la consulta
	 * @return true or false
	 */
	public boolean validaDatos() {
		boolean bRegreso = true;
		setFiltro(new StringBuilder());
		
		//Validaciones segun tipo de busqueda
		switch (getTipoBusqueda()) {
			case 1: //Por Certificado
				bRegreso = validaxCertificado();
				break;
				
			case 2: //Por Recibo
				//Se valida venga un numero de recibo
				if(getRecibo().equals(Constantes.DEFAULT_STRING)){ 
					setStrRespuesta("Favor de ingresar numero de recibo");
					bRegreso = false;
				} else {
					getFiltro().append("WHERE CARTI_NU_RECIBO = ").append(getRecibo()).append(" \n");
				}
				break;
			
			default: //Por UUID
				//Se valida venga un UUID
				if(getUuid().equals(Constantes.DEFAULT_STRING)){ 
					setStrRespuesta("Favor de ingresar UUID");
					bRegreso = false;
				} else {
					getFiltro().append("WHERE CARTI_UUID = '").append(getUuid()).append("' \n");
				}
					
				break;
		}
		
		return bRegreso;
	}

	/**
	 * Metodo que valida campos para busqueda por certificado
	 * @return true or false
	 */
	public boolean validaxCertificado() {
		//Se valida se haya seleccionado un ramo
		if(getRamo() == Constantes.DEFAULT_INT) {
			setStrRespuesta("Favor de seleccionar RAMO.");
			return false;
		} else {
			getFiltro().append("WHERE CARTI_CASU_CD_SUCURSAL = 1 \n");
			getFiltro().append("AND CARTI_CARP_CD_RAMO = ").append(getRamo()).append(" \n");
		}
		
		//Se valida se haya seleccionado una poliza
		if(getPoliza() == -1) {
			if(!getPolEsp().equals(Constantes.DEFAULT_STRING)) {
				getFiltro().append("AND CARTI_CAPO_NU_POLIZA =  ").append(getPolEsp()).append(" \n");
			} else {
				getFiltro().append("AND CARTI_CAPO_NU_POLIZA BETWEEN ").append(getIdVenta()).append("00000000 AND ").append(getIdVenta()).append("99999999 \n");
			}
		} else if(getPoliza() == Constantes.DEFAULT_INT) {
			setStrRespuesta("Favor de seleccionar POLIZA.");
			return false;
		} else { 
			getFiltro().append("AND CARTI_CAPO_NU_POLIZA =  ").append(getPoliza()).append(" \n");
		}
		
		//Se valida se haya seleccionado un certificado
		if(!getCertificado().equals(Constantes.DEFAULT_STRING)) {
			getFiltro().append("AND CARTI_CACE_NU_CERTIFICADO = ").append(getCertificado()).append(" \n");
		} 

		//Se valida se haya seleccionado un estatus
		if(!getEstatus().equals(Constantes.DEFAULT_STRING)) {
			getFiltro().append("AND CARTI_ST_TIMBRADO = '").append(getEstatus()).append("' \n");
		}
		
		//Se validan fechas
		return validaFecha();
	}

	/**
	 * Metodo que valida campos de fechas 	
	 * @return bandera true or false
	 */
	private boolean validaFecha() {
		boolean bRegreso = true;
	
		bRegreso = validaFechas();

		getFiltro().append("AND CARTI_FE_TIMBRADO_T BETWEEN TO_DATE(:feDesde, 'dd/MM/yyyy') AND TO_DATE(:feHasta, 'dd/MM/yyyy') \n");
		
		return bRegreso;
	}
	
	/**
	 * Metodo que obtiene la razon social de un rfc
	 * @param rfc rfc del emisor
	 * @return razon social
	 */
	public String getRazonSocialEmisor(String rfc) {
		StringBuilder filtro = new StringBuilder(300);
		List<Object> lstResultados;

		try {			
			//Se realiza consulta para obtener razon social
			filtro.append("SELECT NOMBRE \n");
			filtro.append(" FROM CART_EMISORES \n");
			filtro.append(" WHERE RFC = '").append(rfc).append("' \n");

			//Ejecuta query
			lstResultados = this.servicioGeneric.consultaSQL(filtro);
			
			//regresamos id
			return lstResultados.get(0).toString();
			
		} catch (Excepciones e) {
			//Cualqueir error se reporta al LOG
			LOG.error(Logger.SECURITY_FAILURE, getStrRespuesta(), e);
			
			//se regresa S/D
			return Constantes.DEFAULT_SIN_DATO;
		}
		
	}
	
	
	/**
	 * @param servicioParametros the servicioParametros to set
	 */
	public void setServicioParametros(ServicioParametros servicioParametros) {
		this.servicioParametros = servicioParametros;
	}
	/**
	 * @param servicioGeneric the servicioGeneric to set
	 */
	public void setServicioGeneric(ServicioGeneric servicioGeneric) {
		this.servicioGeneric = servicioGeneric;
	}
	/**
	 * @return the servicioParametros
	 */
	public ServicioParametros getServicioParametros() {
		return servicioParametros;
	}
	/**
	 * @return the servicioGeneric
	 */
	public ServicioGeneric getServicioGeneric() {
		return servicioGeneric;
	}
}
