package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.view.dto.EndosoDatosDTO;


/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Cambia de Credito.
 * 				cambiaIdCredito.jsp
 * 				Esta clase contine solo propiedades que son utilzadas por 
 * 				la clase BeanCambiaCredito
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	 	
 * 		Cuando: 
 * 		Porque: 
 * ------------------------------------------------------------------------
 * 
 */
public class BeanCambiaCredito2 extends BeanGenerico implements Serializable {

	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para los datos a endosar
	private EndosoDatosDTO seleccionado;
	//Propiedad para filtro por credito
	private String credito;
	//Propiedad para lista de certificados
	private List<EndosoDatosDTO> listaCertificados;

	/**
	 * Constructor de clase
	 */
	public BeanCambiaCredito2() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.listaCertificados = new ArrayList<EndosoDatosDTO>();
		this.seleccionado = new EndosoDatosDTO();
	}

	
	/**
	 * @return the credito
	 */
	public String getCredito() {
		return credito;
	}
	/**
	 * @param credito the credito to set
	 */
	public void setCredito(String credito) {
		this.credito = credito;
	}
	/**
	 * @return the listaCertificados
	 */
	public List<EndosoDatosDTO> getListaCertificados() {
		return new ArrayList<EndosoDatosDTO>(listaCertificados);
	}
	/**
	 * @param listaCertificados the listaCertificados to set
	 */
	public void setListaCertificados(List<EndosoDatosDTO> listaCertificados) {
		this.listaCertificados = new ArrayList<EndosoDatosDTO>(listaCertificados);
	}
	/**
	 * @return the seleccionado
	 */
	public EndosoDatosDTO getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(EndosoDatosDTO seleccionado) {
		this.seleccionado = seleccionado;
	}
}
