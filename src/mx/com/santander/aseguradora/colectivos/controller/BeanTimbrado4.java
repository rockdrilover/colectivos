package mx.com.santander.aseguradora.colectivos.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.santander.aseguradora.colectivos.view.dto.TimbradoDTO;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-03-2020
 * Description: Clase que nos sirve para interactuar entre la vista y la 
 * 				logica de negocio de la pantalla Timbrado.
 * 				timbrado.jsp
 * 				Esta clase contine propiedades y metodos que son utilzadas por 
 * 				la clase BeanTimbrado
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por:	Ing. Issac Bautista 	
 * 		Cuando: 12-10-2020
 * 		Porque: Correccion de observaciones QAT
 * ------------------------------------------------------------------------
 * 
 */
public class BeanTimbrado4 extends BeanTimbrado5 implements Serializable {
	
	//Propiedad para implementar Serializable
	private static final long serialVersionUID = 1L;
	
	//Propiedad para filtro por uuid
	private String uuid;
	//Propiedad para lista de timbrados
	private List<TimbradoDTO> listaTimbrado;
	//Propiedad para filtro por recibo
	private String recibo;
	//Propiedad para filtro por estatus
	private String estatus;

	/**
	 * Constructor de clase
	 */
	public BeanTimbrado4() {
		//se manda ejecutar constructor de clase padre
		super();
		
		//Se inicialisan valores por default
		this.listaTimbrado = new ArrayList<TimbradoDTO>();
		
	}
	
	/**
	 * @return the listaTimbrado
	 */
	public List<TimbradoDTO> getListaTimbrado() {
		return new ArrayList<TimbradoDTO>(listaTimbrado);
	}
	/**
	 * @param listaTimbrado the listaTimbrado to set
	 */
	public void setListaTimbrado(List<TimbradoDTO> listaTimbrado) {
		this.listaTimbrado = new ArrayList<TimbradoDTO>(listaTimbrado);
	}
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * @return the recibo
	 */
	public String getRecibo() {
		return recibo;
	}
	/**
	 * @param recibo the recibo to set
	 */
	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
}
