package mx.com.santander.aseguradora.colectivos.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.faces.model.SelectItem;


/**
 * 
 * @author Ing. Issac Bautista
 *
 */
public class GestorFechas {

	
	/**
	 * Da formato a un objeto de tipo Date, convirtiendolo en cadena con el formato especificado.
	 */
	public static String formatDate(Date pDate, String formato) {
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
	    return sdf.format(pDate);
	}
	
	/**
     * Genera un objeto Date a partir de una fecha de tipo String y con un formato especificado.   
     */
  	public static Date generateDate(String strFecha, String formato){
  		Date fecha = null;
  		
  		try{
  			DateFormat df = new SimpleDateFormat(formato);
  			fecha = (Date) df.parse(strFecha);
  		} catch(Exception e) {
			e.printStackTrace();
  		}
  		
  		return fecha;
    }
  	
  	/**
     * Genera un objeto Date a partir de una fecha de tipo String y con un formato especificado.   
     */
  	public static Date generateDate2(String strFecha, String formato) throws ParseException {
  		Date fecha = null;
  		
		DateFormat df = new SimpleDateFormat(formato);
		df.setLenient(false);
		fecha = (Date) df.parse(strFecha);
  		
  		return fecha;
    }
  	
  	
  	/**
	 * Metodo que resta dos fechas.
	 * 
	 * @param fechaInicial
	 * @param fechaFin
	 * @return
	 * 		La diferencia entre las fechas en DIAS.
	 */
	public static long restarFechas(Date fechaInicial, Date fechaFin) {
		long lngDias = 0;
		long segFechaIni, segFechaFin, segDia;
		
		try {
			segDia = 24*60*60;
			segFechaIni = fechaInicial.getTime() / 1000; 
			segFechaFin = fechaFin.getTime() / 1000;
			
			lngDias = (segFechaFin - segFechaIni ) / segDia ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lngDias;
	}
	
	/**
     * M�todo que suma o resta d�as a una fecha.
     */
	public static Date sumarRestasDiasAFecha(Date fecha, int dias, int operacion) {        
		long msFechaVence;    
		long msDias;
		Date fechaMasDias = null;
      
		try {      
			msDias = dias * 24 * 3600000;
			msFechaVence = fecha.getTime();      
			if(operacion == Constantes.FECHA_SUMA_DIAS){
				fechaMasDias = new Date(msFechaVence + msDias);	
			} else {
				fechaMasDias = new Date(msFechaVence - msDias);
			}
      } catch(Exception e) {
    	  e.printStackTrace();
      }      
      
      return fechaMasDias;
    }

	public static ArrayList<Date> obtenerFechas(Date fechaIni, Date fechaFin) {
		ArrayList<Date> arlFechas = null;
		Date tmpFecha;
		long lngDias;
		
		try {
			arlFechas = new ArrayList<Date>();
			lngDias = restarFechas(fechaIni, fechaFin);
			
			arlFechas.add(fechaIni);
			tmpFecha = fechaIni;
			for(int i = 1; i <= lngDias; i++){
				tmpFecha = sumarRestasDiasAFecha(tmpFecha, 1, Constantes.FECHA_SUMA_DIAS);
				arlFechas.add(tmpFecha);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return arlFechas;
	}

	public static boolean buscaFecha(Date fechaIngreso, ArrayList<Resultado> arlFechas) {
		boolean blnResultado = false;
		Iterator<Resultado> it;
		
		try {
			it = arlFechas.iterator();
			while(it.hasNext()){
				if(fechaIngreso.equals( ((Resultado) it.next()).getFecha()) ){
					blnResultado = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return blnResultado;
	}

	public static String addMesesaFecha(Date fecha, int numMeses) {
		Calendar calFecha = null;
		
		try {
			calFecha = Calendar.getInstance();
			calFecha.setTime(fecha);
			
			calFecha.add(Calendar.MONTH, numMeses);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return formatDate(calFecha.getTime(), "dd/MM/yyyy");
	}

	public static String generarFechaNacimiento(String rfc) {
        String strFechaNan = "";
        String strDia, strMes, strAnio;

        try {
	        if(rfc == null || rfc.length() < 10){
	              strFechaNan = "01/01/1910";
	        } else {
	              String firstNumber = rfc.replaceAll("^\\D*(\\d+).*", "$1");
	
	              if (firstNumber.length() < 6) {
	                    firstNumber += "0" + firstNumber;
	              }
	              strDia = firstNumber.substring(4,6);
	              strMes = firstNumber.substring(2,4);
	              switch (Integer.parseInt(firstNumber.substring(0,1))) {
	              case 1:
	              case 2:
	              case 3:
	              case 4:
	              case 5:
	              case 6:
	              case 7:
	              case 8:
	              case 9:
	                    strAnio = "19" + firstNumber.substring(0,2);
	                    break;
	
	              default:
	                    strAnio = "20" + firstNumber.substring(0,2);
	                    break;
	        }                           
            
	              strFechaNan = strDia + "/" + strMes + "/" + strAnio;
	        }
        } catch(Exception e) {
              strFechaNan = "01/01/1910";
        }
        
        return strFechaNan;
	}

	
	/*
	public static String generarFechaNacimiento(String rfc) {
		String strFechaNan;
		String arrRFC[];
		String strDia, strMes, strAnio;
		
		if(rfc == null || rfc.length() < 10){
			strFechaNan = "01/01/1910";
		} else {
			arrRFC = rfc.split("");
			strDia = arrRFC[9].toString() + arrRFC[10].toString();
			strMes = arrRFC[7].toString() + arrRFC[8].toString();
			
			switch (Integer.parseInt(arrRFC[5].toString())) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
					strAnio = "19" + arrRFC[5].toString() + arrRFC[6].toString();
					break;

				default:
					strAnio = "20" + arrRFC[5].toString() + arrRFC[6].toString();
					break;
			}
			
			strFechaNan = strDia + "/" + strMes + "/" + strAnio;
		}
		return strFechaNan;
	}
	*/
	
	public static int obtenerEdad(String fechanacaseg) throws Exception{
		long mseg, dias;
		int edad;
		
		try {			
			mseg = new Date().getTime() - generateDate(fechanacaseg, "dd/MM/yyyy").getTime();
			dias = mseg / (24 * 60 * 60 * 1000);
			edad = new Long(dias / 365).intValue(); 
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("GestorFechas.obtenerEdad:: " + e.getMessage());
		}
		
		return edad;
	}
	
	public static String generaRemesa(Date fecha) throws Exception {
		String strFecha, strRemesa;
		String[] arrDatos;
		
		try {			
			strFecha = formatDate(fecha, "dd/MM/yyyy");
			arrDatos = strFecha.split("/");
			strRemesa = arrDatos[2] + arrDatos[1];  
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("generaRemesa:: " + e.getMessage());
		}
		
		return strRemesa;
	}
	
	/**
	 * 
	 * @param fecha1
	 * @param fecha2
	 * @return  valor 1: fecha2 es mayor que la fecha1
	 * 			valor 0: las dos fechas son iguales
	 * 			valor -1: fecha2 es menor a la fecha1
	 */
	public static int comparaFechas(Date fecha1, Date fecha2) {		
		return fecha2.compareTo(fecha1);
	}
	
	/**
	 * 
	 * @param dFecha
	 * @return 	1 = Domingo
	 * 		   	2 = Lunes
	 * 			3 = Martes
	 * 			4 = Miercoles
	 * 			5 = Jueves
	 * 			6 = Viernes
	 * 			7 = Sabado
	 * @throws Exception
	 */
	public static int getDiadelaSemana(Date dFecha) throws Exception {
		GregorianCalendar cal;
		int nNumDia = 1;
		try {
			cal = new GregorianCalendar();
			cal.setTime(dFecha);
			nNumDia =  cal.get(Calendar.DAY_OF_WEEK);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return nNumDia;
	}

	public static boolean validarFecha(String strFecha) {
		try {
			generateDate2(strFecha, "dd/MM/yyyy");
			return true;
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	public static final Object getDatosFecha(Date fechaActual, int tipoOperacion){
		Integer nDias = 0;
		String strNombreMes;
		Calendar calFecha = null;
		String[] arrMeses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
		
		try {
			calFecha = Calendar.getInstance();
			calFecha.setTime(fechaActual);
			
			switch (tipoOperacion) {
				case Constantes.DIAS_MES: // Regresa Cuantos dias tiene el Mes
					nDias = calFecha.getActualMaximum(Calendar.DAY_OF_MONTH);
					return nDias;
					
				case Constantes.DIAS_TRANSCURRIDOS_MES: // Regresa los dias transcurridos del mes.
					nDias = calFecha.get(Calendar.DATE);
					return nDias;
				
				case Constantes.MES_FECHA: // Regresa el mes actual.
					nDias = calFecha.get(Calendar.MONTH) + 1;
					return nDias;
					
				case Constantes.ANIO_FECHA: // Regresa el a�o actual.
					nDias = calFecha.get(Calendar.YEAR);
					return nDias;
				
				case Constantes.NOMBRE_MES_FECHA: // Regresa el nombre del mes
					strNombreMes = arrMeses[calFecha.get(Calendar.MONTH) + 1];
					return strNombreMes;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return nDias;
	}

	/**
	 * Metodo que regresa una lista para Combo con los a�os.
	 * @param nDiasMenos
	 * @param nDiasMas
	 * @return lista con los a�os.
	 * @throws Exception
	 */
	public static List<Object> getAnios(int nDiasMenos, int nDiasMas)  {
		List<Object> anios = null;
		Integer anioActual;

		
		anioActual = Integer.parseInt(GestorFechas.formatDate(new Date(), "yyyy"));
		anios = new  ArrayList<Object>();
		
		//Tres a�os menos al actual
		for(int i = nDiasMenos; i > 0 ; i--) {
			anios.add(new SelectItem(anioActual - i));
		}
		
		anios.add(new SelectItem(anioActual));
		
		//3 a�os mas al actual
		for(int i = 1; i <= nDiasMas ; i++) {
			anios.add(new SelectItem(anioActual + i));
		}
		
		return anios;
	}

	/**
	 * Metdo que regresa una lista para Combo con los nombres de  meses 
	 * @return lista de meses
	 */
	public static Collection<? extends Object> getMeses() {	
		List<Object> meses = null;
		
		meses = new ArrayList<Object>();
		meses.add(new SelectItem(1, "Enero"));
		meses.add(new SelectItem(2, "Febrero"));
		meses.add(new SelectItem(3, "Marzo"));
		meses.add(new SelectItem(4, "Abril"));
		meses.add(new SelectItem(5, "Mayo"));
		meses.add(new SelectItem(6, "Junio"));
		meses.add(new SelectItem(7, "Julio"));
		meses.add(new SelectItem(8, "Agosto"));
		meses.add(new SelectItem(9, "Septiembre"));
		meses.add(new SelectItem(10, "Octubre"));
		meses.add(new SelectItem(11, "Noviembre"));
		meses.add(new SelectItem(12, "Diciembre"));
		
		return meses;
	}
	
	/**
	 * Metodo que te regresa el nombre del Mes segun numero de mes. 
	 * @param nNumMes Enero = 1 ... Diciembre = 12
	 * @return
	 * @throws Exception
	 */
	public static String getNombreMes(Integer nNumMes)   {	
		String[] arrMeses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
		return arrMeses[nNumMes - 1];
	}
	
	public static void main(String[] args) {
		try {
			String strDato = getNombreMes(1);
			System.out.println(strDato);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que valida si una fecha es null (01/01/1900)
	 * @param objFecha objeto a validar
	 * @return null si es igual (01/01/1900)
	 */
	public static Date isNull(Date objFecha) {
		if(comparaFechas(generateDate("01/01/1900", Constantes.FORMATO_FECHA_UNO), objFecha) == 0) {
			return null;
		} else {
			return objFecha;
		}
	}
	
}
