package mx.com.santander.aseguradora.colectivos.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;

import com.barcodelib.barcode.QRCode;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRAbstractScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

public class QRGeneracion extends JRDefaultScriptlet {
	public BufferedImage getCBBByte(String cbb)	{
		QRCode barcode = new QRCode(); 
		BufferedImage bufferedImage = null;
		barcode.setData(cbb);
		barcode.setVersion(10);
		barcode.setModuleSize((float)3);
		
		byte[] barcodeBytes = null;
		
		try {	
	        barcodeBytes = barcode.renderBarcodeToBytes();		
			bufferedImage = ImageIO.read(new ByteArrayInputStream(barcodeBytes));
		}catch (Exception e) {
        	e.printStackTrace();
        }
        
		return bufferedImage;
	}

}
