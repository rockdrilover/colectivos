package mx.com.santander.aseguradora.colectivos.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

/*
 *  LPV 16/07/2015
 */
public class SmbUtil {
	
	private String user;
	private String pwd;
	private NtlmPasswordAuthentication auth;
	
	public SmbUtil(String user, String pwd) {
		this.user = user;
		this.pwd  = pwd;
		this.auth = new NtlmPasswordAuthentication("", this.user, this.pwd);
		System.out.println("----> "+this.auth);
	}
	
	public SmbFile getFile(String ruta) throws Exception {
		return new SmbFile(ruta,this.auth);
	}
	
	public InputStreamReader remoteISReader(String ruta) throws Exception {
		SmbFile smbFile = null;
		
		try {
	   		smbFile = new SmbFile(ruta,this.auth);
			return new InputStreamReader(new SmbFileInputStream(smbFile));
		} catch(Exception e){
			e.printStackTrace();
			throw new Exception("Error en de I/O en archivo SMB:"+e.getMessage());
		}
	}

	public InputStreamReader fileReader(SmbFile file) throws Exception {
		return new InputStreamReader(new SmbFileInputStream(file));
	}
	
	public OutputStreamWriter fileWriter(SmbFile file) throws Exception {
		return new OutputStreamWriter(new SmbFileOutputStream(file));
	}

	public InputStream inputStream(String file) throws Exception {
		return new SmbFileInputStream(new SmbFile(file,this.auth));
	}
	
	public void closeISReader(InputStreamReader inputReader) throws Exception {
		try {
			if(inputReader != null) {
				inputReader.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Error al cerrar inputReader SMB:"+e.getMessage());
		}
	}
	
}
