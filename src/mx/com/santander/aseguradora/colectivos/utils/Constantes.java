package mx.com.santander.aseguradora.colectivos.utils;

import java.math.BigDecimal;

/**
 * 
 * @author Ing. Issac Bautista
 *
 */
public class Constantes {

	//Valores Default
	public static final String DEFAULT_STRING_ID = "0";
	public static final String DEFAULT_STRING = "";
	public static final int DEFAULT_INT = 0;
	public static final long DEFAULT_LONG = 0L;
	public static final double DEFAULT_DOUBLE = 0D;
	public static final BigDecimal DEFAULT_BIGDECIMAL = new BigDecimal(0);
	public static final String DEFAULT_SIN_DATO = "S/D";
	
	public static final String RUTA_CARGAS = "/WEB-INF/cargas/";
	public static final String RUTA_PLAZOS = "/WEB-INF/plazos.txt";
	
	//Operaciones par FECHAS
	public static final int FECHA_SUMA_DIAS = 1;
	public static final int FECHA_RESTA_DIAS = 2;
	public static final int DIAS_MES = 1;
	public static final int DIAS_TRANSCURRIDOS_MES = 2;
	public static final int MES_FECHA = 3;
	public static final int ANIO_FECHA = 4;
	public static final int NOMBRE_MES_FECHA = 5;
	public static final String FORMATO_FECHA_UNO = "dd/MM/yyyy";
	public static final String FORMATO_FECHA_REP = "ddMMyyyy";
	
	public static final int CONCILIACION_ORIGEN_ALTAMIRA_VALOR = 1;
	public static final String CONCILIACION_ORIGEN_ALTAMIRA_DESC = "PRIMA UNICA - ALTAMIRA";
	public static final String CONCILIACION_ALTAMIRA = "ALTAMIRA";
	public static final int CONCILIACION_ALTAMIRA_RAMO = 98;
	public static final String CONCILIACION_ALTAMIRA_DELIMITADOR = "?";
	public static final String CUENTAS_ORIGEN_ALTAMIRA_ALTA = "101";
	public static final String CUENTAS_ORIGEN_ALTAMIRA_BAJA = "102";
	
	public static final int CONCILIACION_ORIGEN_ALTAIR_VALOR = 2;
	public static final String CONCILIACION_ORIGEN_ALTAIR_DESC = "PRIMA UNICA - ALTAIR";
	public static final String CONCILIACION_ALTAIR = "ALTAIR";
	public static final int CONCILIACION_ALTAIR_RAMO = 99;
	public static final String CONCILIACION_ALTAIR_DELIMITADOR = "@";
	public static final String CUENTAS_ORIGEN_ALTAIR = "103";
	public static final String CUENTAS_ORIGEN_DESCONOCIDO = "104";
	
	public static final int CONCILIACION_ORIGEN_HIPOTECARIO_VALOR = 5;
	public static final String CONCILIACION_ORIGEN_HIPOTECARIO_DESC = "PRIMA UNICA - HIPOTECARIO";
	public static final String CONCILIACION_HIPOTECARIO = "HPU";
	public static final int CONCILIACION_HIPOTECARIO_RAMO = 97;
	public static final String CONCILIACION_HIPOTECARIO_DELIMITADOR = ",";
	public static final String CONCILIACION_SUCURSAL_HPU_GE = "8871";
	public static final String CONCILIACION_IDENTIFICADOR_HPU_GE = "_GE";
	
	public static final int CONCILIACION_ORIGEN_LCI_VALOR = 3;
	public static final String CONCILIACION_ORIGEN_LCI_DESC = "PRIMA UNICA - LCI";
	public static final String CONCILIACION_LCI = "LCI";
	public static final int CONCILIACION_LCI_RAMO = 96;
	public static final String CONCILIACION_LCI_DELIMITADOR = ",";	
	
	public static final int CONCILIACION_ORIGEN_GAP_VALOR = 6;
	public static final int CONCILIACION_ORIGEN_GAP_VALOR_VID = 7;
	public static final String CONCILIACION_ORIGEN_GAP_DESC = "PRIMA UNICA - GAP";
	public static final String CONCILIACION_GAP = "GAP";
	public static final int CONCILIACION_GAP_RAMO = 95;
	public static final String CONCILIACION_GAP_DELIMITADOR = ",";	
	
	public static final int CONCILIACION_ORIGEN_CUENTAS_VALOR = 4;
	public static final String CONCILIACION_ORIGEN_CUENTAS_DESC = "CUENTAS DEPOSITO";
	
	public static final int CONCILIACION_ORIGEN_LE_VALOR = 8;
	public static final String CONCILIACION_ORIGEN_LE_DESC = "PRIMA UNICA - LINEA EXPRES";
	public static final String CONCILIACION_LE = "LE";
	public static final int CONCILIACION_LE_RAMO = 94;
	public static final String CONCILIACION_LE_DELIMITADOR = ",";	
	
	public static final int TIPO_VENTA_HPU = 7;
	public static final int TIPO_VENTA_GAP = 10;
	public static final int TIPO_VENTA_BT  = 6; //LCI
	public static final int TIPO_VENTA_LEX = 8;
	public static final int TIPO_VENTA_PYM = 5;
	public static final int TIPO_VENTA_LEX_RESTI = 7; //LE RESTITUCION
	public static final int TIPO_VENTA_LCI_OP1 = 11; //LCI PT-OPCION 1
	public static final int TIPO_VENTA_LCI_OP2 = 12; //LCI PT-OPCION 2
	public static final String CONCILIACION_NOMBRE_ARCHIVO_MANUAL = "Carga_Auto_Manual";
	public static final String CONCILIACION_NOMBRE_ARCHIVO = "Carga_Automatica";
	public static final double SUMA_ASEG_LCI_OP1 = 750000;
	public static final double SUMA_ASEG_LCI_OP2 = 500000;
	
	public static final int CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR = 7;
	public static final String CONCILIACION_ORIGEN_DESEMPLEO_TDC_DESC = "DESEMPLEO TDC";
	public static final String CONCILIACION_DESEMPLEO_TDC = "DES_TDC";
	public static final int CONCILIACION_DESEMPLEO_TDC_RAMO = 9;
	public static final String CONCILIACION_DESEMPLEO_TDC_DELIMITADOR = ",";
	public static final String CUENTAS_ORIGEN_DESEMPLEO = "105";
	
	public static final int CONCILIACION_ORIGEN_BT_VALOR = 8;
	public static final String CONCILIACION_ORIGEN_BT_DESC = "PRIMA UNICA - BALANCE TRANSFER";
	public static final String CONCILIACION_BT = "BT";
	public static final int CONCILIACION_BT_RAMO = 93;
	public static final String CONCILIACION_BT_DELIMITADOR = ",";
	
	//Indices Array Conciliar.
	public static final int INDEX_CONCILIA_ALTAMIRA = 0;
	public static final int INDEX_CONCILIA_ALTAIR = 1;
	public static final int INDEX_CONCILIA_HPU = 2;
	public static final int INDEX_CONCILIA_LCI = 3;
	public static final int INDEX_CONCILIA_GAP = 4;
	public static final int INDEX_CONCILIA_DESEMPLEO = 5;
	public static final int INDEX_CONCILIA_LE = 6;
	public static final int INDEX_CONCILIA_BTT = 7;
	
	// Errores proceso CONCILIACION
	public static final String ERROR_001_ID = "001";
	public static final String ERROR_001_DESC = "Registro Desfasado";
	public static final String ERROR_002_ID = "002";
	public static final String ERROR_002_DESC = "Registro Bajas";
	public static final String ERROR_003_ID = "003";
	public static final String ERROR_003_DESC = "Plazo no reconocido";
	public static final String ERROR_004_ID = "004";
	public static final String ERROR_005_ID = "005";
	public static final String ERROR_004_DESC = "Conciliado - Error al separar ramos, (No encontro tarifas asociadas en seguros)";
	public static final String ERROR_005_DESC = "Conciliado - Se tomo en cuenta para cuadrar depositos (No se va para emitir)";
	public static final String ERROR_006_ID = "006";
	public static final String ERROR_006_DESC_01 = "No se ha configurado un rango de edad para esta tarifa: ";
	public static final String ERROR_006_DESC_02 = "La edad del asegurado no esta dentro de las edades de aceptacion.";
	public static final String ERROR_007_ID = "007";
	public static final String ERROR_007_DESC = "No coincide la prima calculada con la prima del deposito.";
	public static final String ERROR_008_ID = "008";
	public static final String ERROR_008_DESC = "El registro en el archivo de WF es incorrecto.";
	public static final String ERROR_009_ID = "009";
	public static final String ERROR_009_DESC = "La prima de la venta es diferente a la prima del deposito.";
	public static final String ERROR_010_ID = "010";
	public static final String ERROR_010_DESC = "Ya se excedio la fecha para emitir el registro.";
	public static final String ERROR_011_ID = "011";
	public static final String ERROR_011_DESC = "Tarifa incorrecta.";
	public static final String ERROR_012_ID = "012";
	public static final String ERROR_012_DESC = "Suma asegurada excede el monto permitido.";
	
	//Errores nuevos para Proceso Conciliacion
	public static final String ERROR_CONCILIACION_001 = "Cr�dito no conciliado"; 
	public static final String ERROR_013_ID = "013";
	public static final String ERROR_013_DESC = "Credito con Falla de Informacion.";
	public static final String ERROR_014_ID = "014";
	public static final String ERROR_014_DESC = "Credito duplicado.";
	
	//Codigos de Error para tabla errores
	public static final String CODIGO_ERROR_CONCILIACION = "1";
	public static final String CODIGO_ERROR_VALIDACION = "2";
	public static final String CODIGO_ERROR_CARGA = "3";
	public static final String CODIGO_ERROR_EMISION = "4";
	
	//Estatus de errores
	public static final Short ESTATUS_ERROR_CORREJIDO = 0;
	public static final Short ESTATUS_ERROR_PROCESO = 1;
	
	public static final String CARGADA_PARA_PRECONCILIAR = "-1";
	public static final String CARGADA_PARA_CONCILIAR = "0";
	public static final String CARGADA_CONCILIADO = "1";
	public static final String CARGADA_PARA_EMITIR = "2";
	public static final String CARGADA_ERROR = "3";
	public static final String CARGADA_CONCILIADO_MANUAL = "4";	
	public static final long CONCILIACION_BANDERA_DESFASADO = 3;
	
	public static final String CARGADA_ERROR_PRIMAS = "2";
	
	public static final String CARGADA_ERROR_VALIDACION = "4";
	//ESTADOS
	public static final String ESTADO_BD_MEXICO = "ESTADO DE MEXICO";
	public static final String ESTADO_ARCHIVO_MEXICO = "MEXICO";
	public static final String ESTADO_BD_BC_NORTE = "BAJA CALIFORNIA NORTE";
	public static final String ESTADO_ARCHIVO_BC_NORTE = "BAJA CALIFORNIA";
	
	public static final String PRODUCTO_VIDA = "61";
	public static final String PRODUCTO_DESEMPLEO = "9";
	public static final String PRODUCTO_DA�OS = "25";
	public static final String PRODUCTO_GAP = "89";
	
	public static final int FLAG_REGISTRO_FUERA_RANGO = 1;
	public static final int FLAG_REGISTRO_CORRECTO = 2;
	public static final int FLAG_REGISTRO_CARGADO = 3;
	public static final int FLAG_REGISTRO_BAJA = 4;
	public static final int FLAG_REGISTRO_NO_VALIDO = 5;
	
	// DESCRIPCIONES DE ERRORES CARGA DE ARCHIVOS
	public static final String REGISTROS_EXISTENTES = "Ya existe informacion cargada con esta fecha.";
	public static final String REGISTROS_NO_CARGADOS = "No se encontraron registros en el archivo con esta fecha.";
	public static final String REGISTROS_CARGADOS = "  Registros guardados.";
	public static final String REGISTROS_NO_VENTAS_HPU = "Registros no se encontraron ventas en el archivo OPERACIONES";
	public static final String REGISTROS_ACTUALIZADOS = "Registros actualizados correctamente.";
	public static final String CUENTA_VENTAS = "C";
	
	//PARAMETROS CONCILIACION
	public static final String CONCILIA_PARAMETRO_RANGO_FECHAS = "RANGO_FECHAS";
	public static final String CONCILIA_PARAMETRO_CUENTAS = "CUENTAS";
	public static final String CONCILIA_PARAMETRO_RUTA_REPROCESO = "RUTA_REPROCESO";
	
	//CONCILIACION TIPO REPORTE CONCILIACION
	public static final int TIPO_REPORTE_DEPOSITOS_NOCONCILIADO_VALOR = 4;
	public static final String TIPO_REPORTE_DEPOSITOS_NOCONCILIADO_DESC = "DEPOSITOS NO CONCILIADOS";
	public static final int TIPO_REPORTE_VENTAS_NOCONCILIADO_VALOR = 1;
	public static final String TIPO_REPORTE_VENTAS_NOCONCILIADO_DESC = "VENTAS NO CONCILIADAS";
	public static final int TIPO_REPORTE_ERRORES_CONCI_VALOR = 5;
	public static final String TIPO_REPORTE_ERRORES_CONCI_DESC = "ERRORES CONCILIACION";
	public static final int TIPO_REPORTE_BAJAS_VALOR = 9;
	public static final String TIPO_REPORTE_BAJAS_DESC = "VENTAS DE BAJA";
	public static final int TIPO_REPORTE_VENTAS_PREEMISION_VALOR = 3;
	public static final String TIPO_REPORTE_VENTAS_PREEMISION_DESC = "VENTAS PARA PRE-EMISION";
	public static final int TIPO_REPORTE_VENTAS_EMISION_VALOR = 10;
	public static final String TIPO_REPORTE_VENTAS_EMISION_DESC = "VENTAS PARA EMISION";
	public static final int TIPO_REPORTE_EMITIDAS_VALOR = 8;
	public static final String TIPO_REPORTE_EMITIDAS_DESC = "EMITIDAS";
	public static final int TIPO_REPORTE_ERRORES_EMISI_VALOR = 7;
	public static final String TIPO_REPORTE_ERRORES_EMISI_DESC = "ERRORES EMISION";	
	public static final int TIPO_REPORTE_VENTAS_LISTOEMITIR_DETALLE = 6;
	
	//REGRESO DE METODO GET TARIFAS
	public static final int TARIFAS_ARRAY_VIDA = 0;
	public static final int TARIFAS_ARRAY_DESEMPELO = 1;
	public static final int TARIFAS_FLAG_ENCONTRO_TARIFA = 2;
	public static final int TARIFAS_FLAG_INSERTA_DES = 3;

	//PARA CARGA MANUAL DE CREDITOS HBS
	public static final String HBS_BANCO = "BANCO";
	public static final String HBS_SOFOM = "SOFOM";
	public static final String HBS_RAMOS = "RAMOS";
	public static final String ID_PARAMETRO_BANCOSOFOM = "HIPBANSOF";
	public static final String DES_PARAMETRO_BANCOSOFOM_POLIZAS = "POLIZAS";
	public static final String HBS_CARGAS_MANUALES = "HBS_MANUAL";
	public static final String HBS_ES_PEMEX = "1";
	public static final int HBS_ACCION_CREDITO = 1;
	public static final int HBS_ACCION_CREDITO_SUMA = 2;
	public static final int HBS_ACCION_CREDITO_PRIMA = 3;
	public static final int HBS_ACCION_CREDITO_TARIFA = 4;
	public static final int HBS_ACCION_CREDITO_SUMA_TARIFA = 5;
	public static final int HBS_ACCION_CREDITO_FECHANAC = 6;
	public static final int HBS_ACCION_CREDITO_CP = 7;
	
	//COSTANTES TU CARRERA
	public static final int TU_CARRERA_POLIZA = 16590021;
	
	public static enum OPC_FORMATO_REPORTE {PDF, XLS, CSV, DOC};
	
	//DATOS PROXY SALIDA INTERNET
//	public static final String PROXY 		= "180.175.236.38";
//	public static final String PROXY_PUERTO = "8010";
	
	public static final String PROXY 		= "proxyd.mx.corp";
	public static final String PROXY_PUERTO = "8050";

	//PARA TODO LO RELACIONADO A 57 Y 58
	public static final int RAMO_VIDAPYME_57 = 57;
	public static final int RAMO_VIDAPYME_58 = 58;
	
	public static final short CD_SUCURSAL=1;
	public static final String VALIDA_SUCURSAL ="NO EXISTE SUCURSAL";
	public static final String VALIDA_FUNCIONARIO ="NO EXISTE FUNCIONARIO";
	public static final String VALIDA_SUMA_ASEGURADA="SIN SUMA ASEGURADA";
	public static final int LONGITUD_NUMERO_CUENTA = 11;
	public static final String CONDUCTO_COBRO_CHEQUES ="C";
	
	//CONSTANTES PARA RUTAS DE CONCILIACION
	public static final String  CO_COPY  ="COPY";
	public static final String  CO_REPRO ="REPRO"; 
	public static final String  CO_ERROR ="ERROR";
	public static final String  CO_RUTA = "RUTAS";
	
	
	//CONSTANTES PARA SUSCRIPCION
	public static final String CADENA_VACIA = "";
	public static final String FORMATO_FECHA_SUSCRIPCION = "dd/MM/yyyy";
	public static final String DICTAMEN_PENDIENTE = "P";
	public static final String REGISTRO_OK = "OK";
	
	

	/**
	 * Error generico lectura archivo
	 */

	public static final String ERORR_LECTURA_SUSCRIPCION = "Error al leer registro. Favor de verificar formato archivo.";

	/**
	 * Errores folio riesgos.
	 */

	public static final int FOLIO_RIESGO = 0;
	public static final String FOLIO_RIESGO_VACIO = "Campo Folio Riesgos es requerido.";

	public static final String FOLIO_RIESGO_NUMERICO = "Campo Folio Riesgos debe ser numerico.";

	/**
	 * Fin Errores folio riesgos.
	 */

	/**
	 * Errores folio ch.
	 */

	public static final int FOLIO_CH = 1;
	public static final String FOLIO_CH_VACIO = "Campo Folio CH es requerido.";

	public static final String FOLIO_CH_NUMERICO = "Campo Folio CH debe ser numerico.";

	/**
	 * Fin Errores folio ch.
	 */
	
	/**
	 * Errores folio ID CONCATENAR.
	 */

	public static final int ID_CONCATENAR = 2;
	public static final String ID_CONCATENAR_VACIO = "Campo  ID CONCATENAR es requerido.";

	/**
	 * Fin Errores folio ID CONCATENAR.
	 */
	
	
	/**
	 * Errores NOMBRE.
	 */

	public static final int NOMBRE = 3;
	public static final String NOMBRE_VACIO = "Campo  Nombre es requerido.";

	/**
	 * Fin NOMBRE.
	 */

	/**
	 * Errores PARTICIPACION
	 */

	public static final int PARTICIPACION = 4;
	public static final String PARTICIPACION_VACIO = "Campo  PARTICIPACION (Tit/OS) es requerido.";

	/**
	 * Fin Errores PARTICIPACION.
	 */
	
	
	/**
	 * Errores FECHA_RECIBIDO_ASEGURADORA
	 */

	public static final int FECHA_RECIBIDO_ASEGURADORA = 5;
	public static final String FECHA_RECIBIDO_ASEGURADORA_VACIO = "Campo  Fecha Recibido en Aseguradora es requerido.";
	public static final String FECHA_RECIBIDO_ASEGURADORA_FORMATO = "Campo  Fecha Recibido en Aseguradora formato invalido (dd/mm/yyyy).";

	/**
	 * Fin FECHA_RECIBIDO_ASEGURADORA
	 */
	
	
	/**
	 * Errores folio Participacion
	 */

	public static final int PLAZA = 6;
	public static final String PLAZA_VACIO = "Campo  Plaza es requerido.";

	/**
	 * Fin Errores folio ID CONCATENAR.
	 */
	
	
	/**
	 * Errores EJECUTIVO
	 */

	public static final int EJECUTIVO = 7;
	public static final String EJECUTIVO_VACIO = "Campo  Sucursal/Ejecutivo es requerido.";

	/**
	 * Fin Errores EJECUTIVO.
	 */
	
	/**
	 * Errores FECHA_NACIMIENTO
	 */

	public static final int FECHA_NACIMIENTO = 8;
	public static final String FECHA_NACIMIENTO_VACIO = "Campo Fecha de Nacimiento es requerido.";
	public static final String FECHA_NACIMIENTO_FORMATO = "Campo Fecha de Nacimiento formato invalido (dd/mm/yyyy).";

	/**
	 * Fin FECHA_NACIMIENTO
	 */
	
	
	/**
	 * Errores MONTO.
	 */

	public static final int MONTO = 10 - 1;
	public static final String MONTO_VACIO = "Campo Monto es requerido.";
	public static final String MONTO_NUMERICO = "Campo Monto debe ser numerico.";

	/**
	 * Fin Errores MONTO.
	 */
	
	/**
	 * Errores FECHA_SOLICITUD
	 */

	public static final int FECHA_SOLICITUD = 11 - 1;
	public static final String FECHA_SOLICITUD_VACIO = "Campo Fecha de Solicitud Credito es requerido.";
	public static final String FECHA_SOLICITUD_FORMATO = "Campo Fecha de Solicitud Credito formato invalido (dd/mm/yyyy).";

	/**
	 * Fin FECHA_NACIMIENTO
	 */
	
	/**
	 * Errores Local / Foraneo
	 */

	public static final int LOCAL = 12 - 1;
	public static final String LOCAL_VACIO = "Campo Local / Foraneo es requerido.";
	public static final String LOCAL_VALOR_INVALIDO = "Campo Local / Foraneo valor invalido.";

	/**
	 * Fin Errores Local / Foraneo.
	 */
	
	
	/**
	 * Errores EXAMEN_MEDICO
	 */

	public static final int EXAMEN_MEDICO = 13 - 1;
	public static final String EXAMEN_MEDICO_VACIO = "Campo Examen medico es requerido.";

	/**
	 * Fin Errores EXAMEN_MEDICO.
	 */
		
	
	/**
	 * Errores FECHA_APLICACION_EXAMEN
	 */

	public static final int FECHA_SOLICITUD_EXAMEN = 14 - 1;
	public static final String FECHA_SOLICITUD_EXAMEN_FORMATO = "Campo Fecha Solicitud de Examen formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_APLICACION_EXAMEN.
	 */
	
	/**
	 * Errores 
	 */

	public static final int FECHA_CITA_EXAMEN = 15 - 1;
	public static final String FECHA_CITA_EXAMEN_FORMATO = "Campo Fecha Cita de Examen formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_CITA_EXAMEN.
	 */
		
	/**
	 * Errores MOTIVO_EXAMEN_MEDICO
	 */

	public static final int MOTIVO_EXAMEN_MEDICO = 16 - 1;

	/**
	 * Fin Errores MOTIVO_EXAMEN_MEDICO.
	 */
	
	
	/**
	 * Errores FECHA_APLICACION_EXAMEN
	 */

	public static final int FECHA_APLICACION_EXAMEN = 17 - 1;
	public static final String FECHA_APLICACION_EXAMEN_FORMATO = "Campo Fecha Aplicacion examen formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_APLICACION_EXAMEN.
	 */
	
	
	/**
	 * Errores FECHA_APLICACION_EXAMEN
	 */

	public static final int FECHA_RESULTADOS_EXAMEN = 18 - 1;
	public static final String FECHA_RESULTADOS_EXAMEN_FORMATO = "Campo Fecha Resultados formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_APLICACION_EXAMEN.
	 */
	
	/**
	 * Errores FECHA_RESPUESTA_DOCTOR
	 */

	public static final int FECHA_RESPUESTA_DOCTOR = 19 - 1;
	public static final String FECHA_RESPUESTA_DOCTOR_EXAMEN_FORMATO = "Campo Fecha Respuesta Doctor formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_RESPUESTA_DOCTOR.
	 */
	
	/**
	 * Errores DICTAMEN
	 */

	public static final int DICTAMEN = 20 - 1;

	/**
	 * Fin Errores DICTAMEN.
	 */
	
	
	/**
	 * Errores FECHA_RESPUESTA_SUCURSAL
	 */

	public static final int FECHA_RESPUESTA_SUCURSAL = 21 - 1;
	public static final String FECHA_RESPUESTA_SUCURSAL_FORMATO = "Campo Fecha Respuesta a Sucursal formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_RESPUESTA_SUCURSAL.
	 */
	
	/**
	 * Errores FECHA_NOTIFICACION_DICTAMEN
	 */

	public static final int FECHA_NOTIFICACION_DICTAMEN = 22 - 1;
	public static final String FECHA_NOTIFICACION_DICTAMEN_FORMATO = "Campo Fecha Notificacion Dictamen formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_NOTIFICACION_DICTAMEN.
	 */
	
	/**
	 * Errores OBSERVACIONES_WF
	 */

	public static final int OBSERVACIONES_WF = 23 - 1;

	/**
	 * Fin Errores OBSERVACIONES_WF.
	 */
	
	/**
	 * Errores TELEFONO1
	 */

	public static final int TELEFONO1 = 24 - 1;
	public static final String TELEFONO1_FORMATO = "Longitud Maxima superada Telefono 1 (20 Caracteres).";

	/**
	 * Fin Errores TELEFONO1.
	 */
	
	/**
	 * Errores TELEFONO2
	 */

	public static final int TELEFONO2 = 25 - 1;
	public static final String TELEFONO2_FORMATO = "Longitud Maxima superada Telefono 2 (20 Caracteres).";

	/**
	 * Fin Errores TELEFONO2.
	 */
	
	/**
	 * Errores CELULAR
	 */

	public static final int CELULAR = 26 - 1;
	public static final String CELULAR_FORMATO = "Longitud Maxima superada Celular (20 Caracteres).";

	/**
	 * Fin Errores CELULAR.
	 */
	
	/**
	 * Errores EMAIL
	 */

	public static final int EMAIL = 27 - 1;
	public static final String EMAIL_FORMATO = "Longitud Maxima superada EMAIL (50 Caracteres).";

	/**
	 * Fin Errores EMAIL.
	 */
	
	/**
	 * Errores OBSERVACIONES
	 */

	public static final int OBSERVACIONES = 28 - 1;
	

	/**
	 * Fin Errores OBSERVACIONES.
	 */
	
	/**
	 * Errores NUM_INTER.
	 */

	public static final int NUM_INTER = 29 - 1;
	public static final String NUM_INTER_VACIO = "Campo NumInter es requerido.";
	public static final String NUM_INTER_NUMERICO = "Campo NumInter debe ser numerico.";

	/**
	 * Fin Errores NUM_INTER.
	 */
	
	/**
	 * Errores FECHA_FIRMA
	 */

	public static final int FECHA_FIRMA = 30 - 1;
	public static final String FECHA_FIRMA_FORMATO = "Campo Fecha Firma formato invalido (dd/mm/yyyy).";

	/**
	 * Fin Errores FECHA_FIRMA.
	 */
	
	/**
	 * Errores MONTO_FIRMADO.
	 */

	public static final int MONTO_FIRMADO = 31 - 1;
	public static final String MONTO_FIRMADO_NUMERICO = "Campo Monto Firmado debe ser numerico.";

	/**
	 * Fin Errores MONTO_FIRMADO.
	 */
	
	
	/**
	 * Errores NUM_CREDITO.
	 */

	public static final int NUM_CREDITO = 32 - 1;

	/**
	 * Fin Errores NUM_CREDITO.
	 */
	
	/**
	 * Errores EXTRA_FIRMA.
	 */

	public static final int EXTRA_FIRMA = 33 - 1;
	public static final String EXTRA_FIRMA_NUMERICO = "Campo Extraprima debe ser numerico.";

	/**
	 * Fin Errores EXTRA_FIRMA.
	 */

	
	/**
	 * CONSTANTES CFDI
	 */
	
	// TIMBRAR
	
	public static final String CFDI_ERROR_LLAMADO = "WS500";
	public static final String CFDI_ERROR_DESCRIPCION = "ERROR AL LLAMAR WS";
	
	public static final String CFDI_APLICACION = "COLECTIVOS";	
	public static final int CFDI_OPERACION_TIMBRAR = 1;
	public static final String CFDI_TIMBRAR_EXISTOSO = "WS000";
	public static final String CFDI_TIMBRAR_URL = "https://%s:1445/WSSegCfdiEmi/WSTimbrado";

	
	// CANCELAR
	public static final int CFDI_OPERACION_CANCELAR = 3;
	public static final String CFDI_CANCELAR_EXISTOSO = "WS000";
	public static final String CFDI_CANCELAR_URL = "https://%s:1443/WSSegCfdiCan/WSCancelado";
	//DESCARGAR
	public static final int CFDI_OPERACION_DESCARGAR = 2;
	public static final String CFDI_DESCARGAR_EXISTOSO = "RC200";
	public static final String CFDI_DESCARGAR_URL = "https://%s:1444/WSSegCfdiDes/WSDescarga";
	
	//TIPO DE DOCUMENTO
	public static final String CFDI_XML = "xml";	
	public static final String CFDI_PDF = "pdf";			

	
	/**
	 * FIN CONSTANTES CFDI
	 */
	
	public static final String RUTA_BATCH_COMISIONES = "COMISIONES";
	public static final String RUTA_BATCH_VIGORES = "VIGORES";
	public static final String RUTA_BATCH_ENDOSOS = "ENDOSOS";
	public static final String RUTA_BATCH_ENDOSOS_CON = "ENDOSOSCONS";
	public static final String RUTA_REPORTES_APP = "/WEB-INF/reportes";
	public static final String REPORTES_CONTENT_TYPE = "application/octet-stream";
	
	//Catalogos tabla Parametros
	public static final int CATALOGO_CAUSAS_ANULACION = 1;
	public static final int CATALOGO_PRODUCTOS_BENEFICIARIOS = 2;
	
	//Endosos Datos
	public static final String ENDOSO_ST_DATOS = "B";
	public static final int ENDOSO_CD_DATOS = 400;
	public static final String BENEFICIARIO_ELIMINA = "ELIMINADO";
	public static final String BENEFICIARIO_MODIFICA = "MODIFICADO";
	public static final String BENEFICIARIO_NUEVO = "NUEVO";
	public static final String ENDOSO_DATOS_MUESTRA = "block";
	public static final String ENDOSO_DATOS_OCULTA = "none";
	
	public static final String TIPO_ENDOSO_NOMBRE = "1";
	public static final String TIPO_ENDOSO_DOMICILIO = "2";
	public static final String TIPO_ENDOSO_FECHA_NAC = "3";
	public static final String TIPO_ENDOSO_RFC = "4";
	public static final String TIPO_ENDOSO_SEXO = "5";
	public static final String TIPO_ENDOSO_CORREO = "6";
	public static final String TIPO_ENDOSO_BENEFICIARIOS = "7";
	public static final String TIPO_ENDOSO_VIGENCIAS = "8";
	public static final String TIPO_ENDOSO_FECHA_SUS = "9";
	public static final String TIPO_ENDOSO_SUMA = "10";
	public static final String TIPO_ENDOSO_CREDITO = "11";
	
	public static final short ENDOSO_ESTATUS_APLICADO = 1;
	public static final short ENDOSO_ESTATUS_PENDIENTE = 0;
	public static final short ENDOSO_ESTATUS_RECHAZADO = 2;
	
	public static final int PRODUCTO_DESEMPLEOTDC = 14;
	public static final int PRODUCTO_EVENTOS_SAN = 16;
	
	//Nombre de parametros Paquetes a ejeuctar
	public static final String PARAM_CANAL = "p_canal";
	public static final String PARAM_RAMO = "p_ramo";
	public static final String PARAM_POLIZA = "p_poliza";
	public static final String PARAM_CERTIFICADO = "p_certif";
	public static final String PARAM_SUMA = "p_suma";
	public static final String PARAM_PRIMA = "p_prima";
	public static final String PARAM_RESPALDO = "p_respaldo";
	public static final String PARAM_RETURN = "returnname";
	
	public static final int TIPO_DATO_STRING = 1;
	public static final int TIPO_DATO_LONG = 2;
	public static final int TIPO_DATO_DOUBLE = 3;
	public static final int TIPO_DATO_INTEGER = 4;
	public static final int TIPO_DATO_DATE = 5;
	public static final int TIPO_DATO_DATE_STRING = 6;
	public static final int TIPO_DATO_BIGDECIMAL = 7;
	public static final int TIPO_DATO_STRING_ID = 9;
	
	public static final String IDVENTA_CEROS = "00000000";
	public static final String IDVENTA_NUEVES = "99999999";
	public static final String SUFIJO_REPEMI = "REPEMI_";
	
	//Nombre de parametros para servicios
	public static final String PARAM_TIPOPROCESO = "tipoProceso";
	public static final String PARAM_REGISTRO = "registro";
	public static final String PARAM_ARCHIVO = "nombreArchivo";

	//Catalogos de CFDI
	public static final int CFDI_METODOS_PAGO = 901;
	
	//Para Saber a que tabla consultar
	public static final int TABLA_CIFRAS_CONTROL = 1;
	public static final int TABLA_BITACORA_ERRORES = 2;
	public static final int TABLA_PRECARGA = 3;
	public static final int TABLA_CARGA = 4;
	public static final int TABLA_CEDULA = 5;
	
	//Datos porcentaje comision
	public static final int POR_TECNICA = 0;
	public static final int POR_CONTINGENTE = 1;
	public static final int POR_COMERCIALIZACION = 2;
	public static final int POR_REPARTOS = 3;
	public static final int POR_TOTAL = 4;
	
	public static final int COMISION_EMISION = 0;
	public static final int COMISION_RENOVACION = 1;
	
	public static final String REPORTE_MSG = "Reporte Generado con exito";
	public static final String GUION_BAJO = "_";
	public static final String PIPE = "|";
	
	//Para nuevos procesos
	public static final String ESTRUCTURA_PU = "EMIVTANVAPU";
	public static final String ESTRUCTURA_LAYOUT_COMISIONES = "LAYOUTCOMI";
	public static final int CODIGO_JSON_OK = 100;
	public static final String PROCESO_MAN = "MAN";
	
	//Parametros para consultas DXP
	public static final String PARAM_SUCURSAL_DXP = "pjSucursal";
	public static final String PARAM_RAMO_DXP = "pjRamo";
	public static final String PARAM_POLIZA_DXP = "pjPoliza";
	
	//Para descarga archivo
	public static final String DOWNLOAD_CONTENT = "Content-Disposition";
	public static final String DOWNLOAD_ATTACH = "attachment;filename=";
	
	//Para consultas de Cliente
	public static final int CLIENTE_CONSULTA_ALL = 1;
	public static final int CLIENTE_CONSULTA = 2;
}
