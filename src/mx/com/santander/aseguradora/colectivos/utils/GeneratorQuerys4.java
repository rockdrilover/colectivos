package mx.com.santander.aseguradora.colectivos.utils;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		01-12-2022
 * Description: Clase que genera cadenas de Querys generales
 * 				Se usa para reportes de cobranza 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	
 * 		Cuando:	
 * 		Porque: 
 * ------------------------------------------------------------------------
 */
public class GeneratorQuerys4 {
	
	//Se definen constantes para no repetir codido
	//Propiedad estatica con
	//Join de tabla 
	//CART_ESTADOS
	public static final String JOIN_ESTADOS = "			LEFT JOIN CART_ESTADOS ES ON(ES.CAES_CD_ESTADO = CL.COCN_CD_ESTADO)  \n";
	//Propiedad estatica con
	//Join de tabla
	//COLECTIVOS_PARAMETROS
	public static final String JOIN_PARAMETROS = "			INNER JOIN COLECTIVOS_PARAMETROS PF ON(PF.COPA_DES_PARAMETRO = 'POLIZA' AND PF.COPA_ID_PARAMETRO = 'GEPREFAC') \n";
	//Propiedad estatica con
	//palabra UNION
	public static final String UNION = " UNION \n";
	//Propiedad estatica con
	//para tabla 
	//COLECTIVOS_COBERTURAS 
	public static final String TABLA_COLECTIVOS_COBERTURAS = "                                    FROM COLECTIVOS_COBERTURAS COB \n";
	//Propiedad estatica con
	//palabra ROUND
	public static final String ROUND = "                ROUND(CASE  \n";
	//Propiedad estatica con
	//filtro de fechas
	public static final String FILTRO_FECHAS = "    AND COCD_FECHA_COBRO  BETWEEN #fechaini AND #fechafin \n";
	//Propiedad estatica con
	//con filtro de poliza
	public static final String FILTRO_POLIZA = "    AND COCD_CAPO_NU_POLIZA = #poliza \n";
	//Propiedad estatica con
	//con filtro de ramo
	public static final String FILTRO_RAMO = "    AND COCD_CARP_CD_RAMO = #ramo \n";
	//Propiedad estatica con
	//con filtro de canal
	public static final String FILTRO_CANAL = "WHERE COCD_CASU_CD_SUCURSAL = #canal \n";
	//Propiedad estatica con
	//Join de tabla
	//COLECTIVOS_PARAMETROS 
	//para datos de cobranza
	public static final String JOIN_PARAMETROS_COBRANZA = "    LEFT JOIN COLECTIVOS_PARAMETROS CP ON(CP.COPA_DES_PARAMETRO = 'POLIZA' AND CP.COPA_ID_PARAMETRO = 'COBRANZA' AND CP.COPA_NVALOR1 = RE.CORE_CASU_CD_SUCURSAL AND CP.COPA_NVALOR2 = RE.CORE_CARP_CD_RAMO AND CP.COPA_NVALOR3 = RE.CORE_CAPO_NU_POLIZA) \n";
	
	
	//Variable para 
	//query reporte 
	//estado de cuenta
	//Se usa para 
	//generar reporte 
	//desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repCobEstadoCuenta =
			new StringBuilder("SELECT RE.CORE_CARP_CD_RAMO AS RAMO, \n") //RAMO
				.append("        RE.CORE_CAPO_NU_POLIZA, \n")	//POLIZA
				.append("		CASE NVL(CP.COPA_VVALOR3, 1) \n") //TIPOPOLIZA
				.append("            WHEN '1' THEN 'NORMAL' \n")
				.append("            ELSE 'COVID' \n")
				.append("        END, \n")
				.append("		DET.COCD_COCE_NU_CREDITO, \n")//CREDITO
				.append("		DET.COCD_COCE_NU_CERTIFICADO, \n")//CERTIFICADO
				.append("		CC.COCE_FE_SUSCRIPCION, \n")//FECHA INGRESO
				.append("		TO_CHAR(RE.CORE_FE_EMISION, 'MMYYYY'), \n")//FECHA EMISIION RECIBO
				.append("		DET.COCD_FECHA_VENCIMIENTO, \n")//FECHA VENCIMIENTO
				.append("		RE.CORE_NU_RECIBO,  \n")//NUMUERO RECIBO
				.append("		TRIM(TO_CHAR(DECODE(CC.COCE_SUB_CAMPANA,'7',TO_NUMBER(CC.COCE_MT_SUMA_ASEG_SI),CC.COCE_MT_SUMA_ASEGURADA),'999999999.99')) SUMA_ASEG, \n")//SUMA ASEGGURADA
				.append("		COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CC.COCE_CASU_CD_SUCURSAL, CC.COCE_CARP_CD_RAMO, CC.COCE_CAPO_NU_POLIZA,	 \n")
				.append("										CC.COCE_CAPU_CD_PRODUCTO, CC.COCE_CAPB_CD_PLAN, CC.COCE_NU_COBERTURA,  \n")	
				.append("										NVL(CC.COCE_SUB_CAMPANA,0), CC.COCE_MT_SUMA_ASEGURADA, CC.COCE_MT_SUMA_ASEG_SI, \n")
				.append("										CC.COCE_MT_PRIMA_PURA, CC.COCE_DI_COBRO1, PF.COPA_NVALOR5, 7, NVL(CC.COCE_MT_BCO_DEVOLUCION, 0)) TARIFA, \n") //TARIFA
				.append("		DET.COCD_MT_COB_PRIMA_TOTAL, \n") //PRIMATOTAL
				.append("		DECODE(CC.COCE_ST_CERTIFICADO, 11, (AE.ALES_CAMPO1 || ' - ' || AEC.ALES_DESCRIPCION), AE.ALES_CAMPO1), \n")//ESTATUS CERTIFICADO
				.append("		CC.COCE_FE_ANULACION, \n")//FECHA ANULACCION
				.append("		DET.COCD_FECHA_COBRO, \n")//FECHA COBRO
				.append("		DET.COCD_FECHA_NC, \n")//FECHA NC
				.append("		DECODE(DET.COCD_ESTATUS, 1, 0, DET.COCD_MT_COB_PRIMA_TOTAL), \n")//PRIMACOBRADA
				.append("		DECODE(DET.COCD_ESTATUS, 1, DET.COCD_MT_COB_PRIMA_TOTAL, 0) \n")//PRIMACOBRADA
				.append("    FROM COLECTIVOS_COBRANZA_DET DET \n")
				.append("    INNER JOIN COLECTIVOS_CERTIFICADOS CC ON(CC.COCE_CASU_CD_SUCURSAL = DET.COCD_CASU_CD_SUCURSAL AND CC.COCE_CARP_CD_RAMO  = DET.COCD_CARP_CD_RAMO AND CC.COCE_CAPO_NU_POLIZA = DET.COCD_CAPO_NU_POLIZA AND CC.COCE_NU_CERTIFICADO = COCD_COCE_NU_CERTIFICADO) \n")
				.append("    INNER JOIN COLECTIVOS_RECIBOS RE ON(RE.CORE_CASU_CD_SUCURSAL = DET.COCD_CASU_CD_SUCURSAL AND RE.CORE_NU_RECIBO = DET.COCD_CORE_NU_RECIBO)  \n")
				.append(JOIN_PARAMETROS)
				.append(JOIN_PARAMETROS_COBRANZA)
				.append("    LEFT JOIN ALTERNA_ESTATUS AE ON(AE.ALES_CD_ESTATUS = CC.COCE_ST_CERTIFICADO) \n")
				.append("    LEFT JOIN ALTERNA_ESTATUS AEC ON(AEC.ALES_CD_ESTATUS = NVL(CC.COCE_CD_CAUSA_ANULACION, 0)) \n")
				.append("WHERE DET.COCD_CASU_CD_SUCURSAL = #canal \n")
				.append("    AND DET.COCD_CARP_CD_RAMO = #ramo \n")
				.append("    AND DET.COCD_CAPO_NU_POLIZA = #poliza \n")
				.append("    AND DET.COCD_FECHA_COBRO BETWEEN #fechaini AND #fechafin \n")
				.append("    AND PF.COPA_NVALOR1 = CC.COCE_CASU_CD_SUCURSAL	 \n")				    
				.append("    AND PF.COPA_NVALOR2 = CC.COCE_CARP_CD_RAMO 	 \n")					
				.append("    AND PF.COPA_NVALOR3 = DECODE(PF.COPA_NVALOR4, 0, CC.COCE_CAPO_NU_POLIZA, 1, SUBSTR(CC.COCE_CAPO_NU_POLIZA,0,3), SUBSTR(CC.COCE_CAPO_NU_POLIZA,0,2)) 	 \n")
				.append("    AND PF.COPA_NVALOR6 = TO_NUMBER(NVL(CC.COCE_SUB_CAMPANA,'0'))  \n");
	
	//Variable para 
	//query reporte 
	//estatus cobranza
	//Se usa para 
	//generar reporte 
	//desde pantalla
	public static final StringBuilder repCobEstatusCobranza =
			new StringBuilder("SELECT COCD_CASU_CD_SUCURSAL, COCD_CARP_CD_RAMO, \n")
				.append("		COCD_CAPO_NU_POLIZA, COCD_COCE_NU_CREDITO, \n") 
				.append("		COCD_MT_COB_PRIMA_NETA, COCD_MT_COB_RFI, \n") 
				.append("		COCD_MT_COB_IVA, COCD_MT_COB_PRIMA_TOTAL, \n") 
				.append("		DECODE(COCD_ESTATUS, 1, 'PENDIENTE COBRO', 2, 'PAGADO', COCD_CONCEPTO),  \n")
				.append("		COCD_FECHA_COBRO \n")
				.append("    FROM   COLECTIVOS_COBRANZA_DET \n")
				.append(FILTRO_CANAL)
				.append(FILTRO_RAMO)
				.append(FILTRO_POLIZA)
				.append("    AND COCD_ESTATUS = #estatus \n")
				.append(FILTRO_FECHAS);
	
	//Variable para 
	//query reporte estatus cobro
	//Se usa para 
	//generar reporte 
	//desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repCobEstatusCobro =
			new StringBuilder("SELECT COCM_CAPO_NU_POLIZA, '0', COCM_CORE_NU_RECIBO, \n")
				.append("		DECODE(COCM_CARGADA, 1, 'PENDIENTE COBRO', 2, 'PAGADO', 'ERROR') \n")
				.append("    FROM COLECTIVOS_COBRANZA    \n")  
				.append("WHERE COCM_CAPO_NU_POLIZA = #poliza \n")
				.append("    AND COCM_CARGADA = #estatus \n")
				.append("    AND COCM_FE_COBRO BETWEEN #fechaini AND #fechafin \n")
				.append(UNION)
				.append("SELECT COCD_CAPO_NU_POLIZA, COCD_COCE_NU_CREDITO,  \n")
				.append("		COCD_CORE_NU_RECIBO, \n")
				.append("		DECODE(COCD_ESTATUS, 1, 'PENDIENTE COBRO', 2, 'PAGADO', COCD_CONCEPTO) \n")
				.append("    FROM COLECTIVOS_COBRANZA_DET \n")
				.append(FILTRO_CANAL)
				.append(FILTRO_RAMO)
				.append(FILTRO_POLIZA)
				.append("    AND COCD_ESTATUS = #estatus \n")
				.append(FILTRO_FECHAS);

	//Variable para 
	//query reporte flujo efectivo
	//Se usa para 
	//generar reporte 
	//desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repCobFlujoEfectivo =
			new StringBuilder("SELECT RE.CORE_CARP_CD_RAMO AS RAM, \n")
				.append("        RE.CORE_CAPO_NU_POLIZA,  \n")
				.append("        DECODE(RE.CORE_CAPO_CD_FR_PAGO, 'M', NVL(PRO.COPA_VVALOR1, 'NO PARAMETRIZADO'), AP.ALPR_DE_PRODUCTO), \n")
				.append("        RE.CORE_NU_RECIBO,  \n")
				.append("        TO_CHAR(RE.CORE_NU_CONSECUTIVO_CUOTA) || ' de ' || DECODE(RE.CORE_CAPO_CD_FR_PAGO, 'M', '12', '1'),  \n")
				.append("        RE.CORE_FE_DESDE,  \n")
				.append("        RE.CORE_FE_HASTA,  \n")
				.append("        SUM(NVL(CO.COCD_MT_COB_PRIMA_NETA, 0)),  \n")
				.append("        SUM(NVL(CO.COCD_MT_COB_RFI, 0)), 0,  \n")
				.append("        SUM(NVL(CO.COCD_MT_COB_IVA, 0)),  \n")
				.append("        SUM(NVL(CO.COCD_MT_COB_PRIMA_TOTAL, 0)) \n")
				.append("    FROM COLECTIVOS_RECIBOS RE \n")
				.append("    INNER JOIN ALTERNA_PRODUCTOS AP ON (AP.ALPR_CD_RAMO = RE.CORE_CARP_CD_RAMO AND AP.ALPR_CD_PRODUCTO = DECODE(RE.CORE_CAPO_CD_FR_PAGO, 'M', 1, SUBSTR(RE.CORE_CAPO_NU_POLIZA, 4, 2))) \n")
				.append("    INNER JOIN COLECTIVOS_COBRANZA_DET CO ON(CO.COCD_CASU_CD_SUCURSAL = RE.CORE_CASU_CD_SUCURSAL AND CO.COCD_CORE_NU_RECIBO = RE.CORE_NU_RECIBO)  \n")
				.append(JOIN_PARAMETROS_COBRANZA)
				.append("    LEFT JOIN COLECTIVOS_PARAMETROS PRO ON (PRO.COPA_CD_PARAMETRO = TO_NUMBER(NVL(CP.COPA_VVALOR4, 0))) \n")
				.append("WHERE RE.CORE_FE_EMISION BETWEEN #fechaini AND #fechafin \n")
				.append("    AND RE.CORE_ST_RECIBO != 7 \n")
				.append("GROUP BY RE.CORE_CARP_CD_RAMO, RE.CORE_CAPO_NU_POLIZA, PRO.COPA_VVALOR1, AP.ALPR_DE_PRODUCTO, RE.CORE_NU_RECIBO, RE.CORE_NU_CONSECUTIVO_CUOTA, RE.CORE_CAPO_CD_FR_PAGO, RE.CORE_FE_DESDE, RE.CORE_FE_HASTA \n");
	
	//Variable para 
	//query reporte pago comisiones
	//Se usa para 
	//generar reporte 
	//desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repCobPagoComisiones = 
			new StringBuilder("SELECT RE.CORE_CARP_CD_RAMO, \n")	//RAMO
				.append("        RE.CORE_CAPO_NU_POLIZA,  \n")		//POLIZA
				.append("		DET.COCD_COCE_NU_CREDITO, \n")		//CREDITO
				.append("		DET.COCD_MT_COB_PRIMA_NETA,  \n")	//PRIMANETA
				.append("		DET.COCD_MT_COB_RFI,  \n")			//RFI
				.append("		DET.COCD_MT_COB_IVA,  \n")			//IVA
				.append("		DET.COCD_MT_COB_PRIMA_TOTAL, \n")	//PRIMATOTAL
				.append("		DECODE(DET.COCD_ESTATUS, 1, 'PENDIENTE COBRO', 2, 'PAGADO', 'CANCELADO') ESTATUS, \n") //ESTATUSCOBRANZA
				.append("		DECODE(DET.COCD_ESTATUS, 1, NULL, 2, COCD_FECHA_COBRO, CC.COCE_FE_ANULACION) FECHA, \n") //FECHA
				.append("		NVL(CP.COPA_VVALOR5, 0), \n") //PROCENTAJECOMISION VIDA
				.append("		NVL(CPD.COPA_VVALOR6, 0), \n")//PROCENTAJECOMISION VIDA
				.append("		((NVL(CC.PRIMA_VIDA, (DET.COCD_MT_COB_PRIMA_NETA + DET.COCD_MT_COB_RFI)) * NVL(CP.COPA_VVALOR5, 0)) / 100) + ((NVL(CC.PRIMA_DESEMPLEO, 0) * NVL(CP.COPA_VVALOR6, 0)) / 100) COMO_TOTAL, \n")//COMISION
				.append("		(NVL(CC.PRIMA_VIDA, (DET.COCD_MT_COB_PRIMA_NETA + DET.COCD_MT_COB_RFI)) * NVL(CP.COPA_VVALOR5, 0)) / 100 COM_VIDA,  \n")//COMISION VIDA
				.append("		(NVL(CC.PRIMA_DESEMPLEO, 0) * NVL(CP.COPA_VVALOR6, 0)) / 100 COM_DESEMPLEO \n") //COMISION DESEMPLEO
				.append("    FROM COLECTIVOS_COBRANZA_DET DET \n") //TABLA COBRANZA DETALLE
				.append("    INNER JOIN ( \n") //JOIN CON CERTIFICADOS Y CERTIFICADOS MOV
				.append("        SELECT CC.COCE_CASU_CD_SUCURSAL, CC.COCE_CARP_CD_RAMO, CC.COCE_CAPO_NU_POLIZA, CC.COCE_NU_CERTIFICADO, CC.COCE_NO_RECIBO,  \n")
				.append(ROUND)
				.append("                        WHEN CC.COCE_CARP_CD_RAMO IN (61,65) THEN  \n")
				.append("                            (DECODE(NVL(CC.COCE_SUB_CAMPANA, '0'), '7', CC.COCE_MT_SUMA_ASEGURADA, CC.COCE_MT_SUMA_ASEG_SI) / 1000) * \n")
				.append("                                (SELECT SUM (COB.COCB_TA_RIESGO) TASAVIDA \n")
				.append(TABLA_COLECTIVOS_COBERTURAS)
				.append("                                WHERE COB.COCB_CASU_CD_SUCURSAL 	=  1 \n")
				.append("                                    AND COB.COCB_CARP_CD_RAMO     	= CC.COCE_CARP_CD_RAMO \n")
				.append("                                    AND COB.COCB_CARB_CD_RAMO     	= 14 \n")
				.append("                                    AND COB.COCB_CAPO_NU_POLIZA     = DECODE(NVL(CC.COCE_SUB_CAMPANA, '0'), 0, CC.COCE_CAPO_NU_POLIZA, SUBSTR(CC.COCE_CAPO_NU_POLIZA, 0, 5)) \n")
				.append("                                    AND COB.COCB_CACB_CD_COBERTURA  <> '003' \n")
				.append("                                    AND COB.COCB_CAPU_CD_PRODUCTO   = CC.COCE_CAPU_CD_PRODUCTO \n")
				.append("                                    AND COB.COCB_CAPB_CD_PLAN       = CC.COCE_CAPB_CD_PLAN \n")
				.append("                                    AND COB.COCB_CER_NU_COBERTURA   = CC.COCE_NU_COBERTURA) \n")
				.append("                    END, 2) PRIMA_VIDA, \n")
				.append(ROUND)
				.append("                        WHEN CC.COCE_CARP_CD_RAMO = 61 THEN  \n")
				.append("                            (DECODE(NVL(CC.COCE_SUB_CAMPANA, '0'), '7', CC.COCE_MT_SUMA_ASEGURADA, CC.COCE_MT_SUMA_ASEG_SI) / 1000) * \n")
				.append("                                (SELECT SUM (COB.COCB_TA_RIESGO) TASADES \n")
				.append(TABLA_COLECTIVOS_COBERTURAS)
				.append("                                WHERE COB.COCB_CASU_CD_SUCURSAL =  1 \n")
				.append("                                    AND COB.COCB_CARP_CD_RAMO     = 61 \n")
				.append("                                    AND COB.COCB_CARB_CD_RAMO     = 14 \n")
				.append("                                    AND COB.COCB_CAPO_NU_POLIZA     = DECODE(NVL(CC.COCE_SUB_CAMPANA, '0'), 0, CC.COCE_CAPO_NU_POLIZA, SUBSTR(CC.COCE_CAPO_NU_POLIZA, 0, 5)) \n")
				.append("                                    AND COB.COCB_CACB_CD_COBERTURA  = '003' \n")
				.append("                                    AND COB.COCB_CAPU_CD_PRODUCTO   = CC.COCE_CAPU_CD_PRODUCTO \n")
				.append("                                    AND COB.COCB_CAPB_CD_PLAN       = CC.COCE_CAPB_CD_PLAN \n")
				.append("                                    AND COB.COCB_CER_NU_COBERTURA   = CC.COCE_NU_COBERTURA) \n")
				.append("                END, 2) PRIMA_DESEMPLEO, CC.COCE_FE_ANULACION \n")
				.append("            FROM COLECTIVOS_CERTIFICADOS CC \n")
				.append("        WHERE CC.COCE_CASU_CD_SUCURSAL 	= #canal \n")
				.append("            AND CC.COCE_CARP_CD_RAMO 	= #ramo \n")
				.append("            AND CC.COCE_CAPO_NU_POLIZA	= #poliza \n")
				.append("            AND CC.COCE_CAMPON2 = 2008 \n")
				.append(UNION)
				.append("        SELECT CM.COCM_CASU_CD_SUCURSAL, CM.COCM_CARP_CD_RAMO, CM.COCM_CAPO_NU_POLIZA, CM.COCM_NU_CERTIFICADO, CM.COCM_NU_RECIBO,  \n")
				.append(ROUND)
				.append("                        WHEN CM.COCM_CARP_CD_RAMO IN (61,65) THEN  \n")
				.append("                            (DECODE(NVL(CM.COCM_SUB_CAMPANA, '0'), '7', CM.COCM_MT_SUMA_ASEGURADA, CM.COCM_MT_SUMA_ASEG_SI) / 1000) * \n")
				.append("                                (SELECT SUM (COB.COCB_TA_RIESGO) TASAVIDA \n")
				.append(TABLA_COLECTIVOS_COBERTURAS)
				.append("                                WHERE COB.COCB_CASU_CD_SUCURSAL 	=  1 \n")
				.append("                                    AND COB.COCB_CARP_CD_RAMO     	= CM.COCM_CARP_CD_RAMO \n")
				.append("                                    AND COB.COCB_CARB_CD_RAMO     	= 14 \n")
				.append("                                    AND COB.COCB_CAPO_NU_POLIZA     = DECODE(NVL(CM.COCM_SUB_CAMPANA, '0'), 0, CM.COCM_CAPO_NU_POLIZA, SUBSTR(CM.COCM_CAPO_NU_POLIZA, 0, 5)) \n")
				.append("                                    AND COB.COCB_CACB_CD_COBERTURA  <> '003' \n")
				.append("                                    AND COB.COCB_CAPU_CD_PRODUCTO   = CM.COCM_CAPU_CD_PRODUCTO \n")
				.append("                                    AND COB.COCB_CAPB_CD_PLAN       = CM.COCM_CAPB_CD_PLAN \n")
				.append("                                    AND COB.COCB_CER_NU_COBERTURA   = CM.COCM_NU_COBERTURA) \n")
				.append("                    END, 2) PRIMA_VIDA, \n")
				.append(ROUND)
				.append("                        WHEN CM.COCM_CARP_CD_RAMO = 61 THEN  \n")
				.append("                            (DECODE(NVL(CM.COCM_SUB_CAMPANA, '0'), '7', CM.COCM_MT_SUMA_ASEGURADA, CM.COCM_MT_SUMA_ASEG_SI) / 1000) * \n")
				.append("                                (SELECT SUM (COB.COCB_TA_RIESGO) TASADES \n")
				.append(TABLA_COLECTIVOS_COBERTURAS)
				.append("                                WHERE COB.COCB_CASU_CD_SUCURSAL =  1 \n")
				.append("                                    AND COB.COCB_CARP_CD_RAMO     = 61 \n")
				.append("                                    AND COB.COCB_CARB_CD_RAMO     = 14 \n")
				.append("                                    AND COB.COCB_CAPO_NU_POLIZA     = DECODE(NVL(CM.COCM_SUB_CAMPANA, '0'), 0, CM.COCM_CAPO_NU_POLIZA, SUBSTR(CM.COCM_CAPO_NU_POLIZA, 0, 5)) \n")
				.append("                                    AND COB.COCB_CACB_CD_COBERTURA  = '003' \n")
				.append("                                    AND COB.COCB_CAPU_CD_PRODUCTO   = CM.COCM_CAPU_CD_PRODUCTO \n")
				.append("                                    AND COB.COCB_CAPB_CD_PLAN       = CM.COCM_CAPB_CD_PLAN \n")
				.append("                                    AND COB.COCB_CER_NU_COBERTURA   = CM.COCM_NU_COBERTURA) \n")
				.append("                END, 2) PRIMA_DESEMPLEO, CM.COCM_FE_ANULACION_REAL \n")
				.append("            FROM COLECTIVOS_CERTIFICADOS_MOV CM \n")
				.append("        WHERE CM.COCM_CASU_CD_SUCURSAL 	= #canal \n")
				.append("            AND CM.COCM_CARP_CD_RAMO 	= #ramo \n")
				.append("            AND CM.COCM_CAPO_NU_POLIZA 	= #poliza \n")
				.append("            AND CM.COCM_CAMPON2 = 2008 \n")
				.append("    ) CC ON(CC.COCE_CASU_CD_SUCURSAL = DET.COCD_CASU_CD_SUCURSAL AND CC.COCE_CARP_CD_RAMO  = DET.COCD_CARP_CD_RAMO AND CC.COCE_CAPO_NU_POLIZA = DET.COCD_CAPO_NU_POLIZA AND CC.COCE_NU_CERTIFICADO = COCD_COCE_NU_CERTIFICADO AND CC.COCE_NO_RECIBO = DET.COCD_CORE_NU_RECIBO) \n")
				.append("    INNER JOIN COLECTIVOS_RECIBOS RE ON(RE.CORE_CASU_CD_SUCURSAL = DET.COCD_CASU_CD_SUCURSAL AND RE.CORE_NU_RECIBO = DET.COCD_CORE_NU_RECIBO)  \n")
				.append(JOIN_PARAMETROS_COBRANZA)
				.append("	LEFT JOIN COLECTIVOS_PARAMETROS CPD ON(CPD.COPA_CD_PARAMETRO = CP.COPA_CD_PARAMETRO) \n")
				.append("WHERE DET.COCD_CASU_CD_SUCURSAL = #canal \n")
				.append("    AND DET.COCD_CARP_CD_RAMO 	= #ramo \n")
				.append("    AND DET.COCD_CAPO_NU_POLIZA	= #poliza \n")
				.append("    AND DET.COCD_FECHA_COBRO BETWEEN #fechaini AND #fechafin \n");
	
	//Variable para query reporte provision emision
	//Se usa para generar reporte desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repCobProvisionEmi = 
			new StringBuilder("SELECT CER.COCN_BUC_CLIENTE, EN.COED_CAPO_NU_POLIZA, EN.COED_NU_CERTIFICADO, \n")
				.append("        EN.PORCOMI, EN.COED_CARP_CD_RAMO, EN.PRODUCTO,  \n")
				.append("        EN.COED_CASU_CD_SUCURSAL, CER.COCE_CAZB_CD_SUCURSAL, EN.COED_NU_FOLIO_ENTRADA,  \n")
				.append("        EN.PRIMAENDOSO, EN.DPO, EN.RFI, 0, EN.IVA, EN.PRIMATOTALENDOSO,  \n")
				.append("        0, 0, 0, 0, 0, 0,  \n")
				.append("        (EN.PRIMAENDOSO * PORCOMI) / 100 AS COMISION, CER.COCE_CAPU_CD_PRODUCTO,  \n")
				.append("        EN.COED_FE_SOLICITUD, 'PRIMA RECURRENTE', '', '', '', \n")
				.append("        EN.COED_CAPJ_CD_SUCURSAL, EN.COED_NU_RECIBO, EN.COED_ID_CERTIFICADO, \n")
				.append("        CER.COCE_BUC_EMPRESA, 'ALTA', CER.COCE_NU_CUENTA, \n")
				.append("        CER.COCE_TP_PRODUCTO_BCO, CER.COCE_TP_SUBPROD_BCO, CER.COCN_APELLIDO_PAT, \n")
				.append("        CER.COCN_APELLIDO_MAT, CER.COCN_NOMBRE, CER.COCN_CD_SEXO, \n")
				.append("        CER.COCN_FE_NACIMIENTO, CER.COCN_RFC, CER.COCE_CD_PLAZO, \n")
				.append("        CER.COCE_FE_DESDEPOL, CER.COCE_FE_HASTAPOL, CER.COCE_FE_SUSCRIPCION, \n")
				.append("        CER.COCE_FE_DESDE, CER.COCE_FE_ANULACION, CER.COCE_FE_ANULACION_COL, \n")	
				.append("        CER.COCE_MT_SUMA_ASEGURADA, CER.COCE_MT_BCO_DEVOLUCION, CER.BASE_CALCULO,  \n")
				.append("        CER.TARIFA, CER.COCN_CD_ESTADO, CER.CAES_DE_ESTADO, CER.COCN_CD_POSTAL,  \n")
				.append("        CER.COCE_CAPU_CD_PRODUCTO, CER.COCE_CAPB_CD_PLAN, CER.CUOTA,  \n")
				.append("        CER.DATOS_OBLIGADOS, '', '', CER.DIFERENCIA_DEVOLUCION \n")
				.append("    FROM ( \n")
				.append("        SELECT ENVI.COED_CAPO_NU_POLIZA, ENVI.COED_NU_CERTIFICADO, ENVI.COED_CAMPON2, \n")
				.append("                NVL(CPC.COPA_VVALOR5, 0) PORCOMI, ENVI.COED_CARP_CD_RAMO, NVL(PRO.COPA_VVALOR1, 'NO PARAMETRIZADO') PRODUCTO,  \n")
				.append("                ENVI.COED_CASU_CD_SUCURSAL, ENVI.COED_NU_FOLIO_ENTRADA,  \n")
				.append("                ENVI.COED_NOMBRE_ANT PRIMAENDOSO, REGEXP_SUBSTR(ENVI.COED_CAMPOV5, '[^|A-Z]+', 1, 9) AS DPO,  \n")
				.append("                REGEXP_SUBSTR(ENVI.COED_CAMPOV5, '[^|A-Z]+', 1, 6) AS RFI, \n")
				.append("                REGEXP_SUBSTR(ENVI.COED_CAMPOV5, '[^|A-Z]+', 1, 3) AS IVA, ENVI.COED_CAMPON4 PRIMATOTALENDOSO,  \n")
				.append("                ENVI.COED_FE_SOLICITUD, ENVI.COED_CAPJ_CD_SUCURSAL, ENVI.COED_NU_RECIBO, ENVI.COED_ID_CERTIFICADO \n")
				.append("            FROM COLECTIVOS_ENDOSOS ENVI \n")
				.append("            LEFT JOIN COLECTIVOS_PARAMETROS CPC ON(CPC.COPA_DES_PARAMETRO = 'POLIZA' AND CPC.COPA_ID_PARAMETRO = 'COBRANZA' AND CPC.COPA_NVALOR1 = ENVI.COED_CASU_CD_SUCURSAL AND CPC.COPA_NVALOR2 = ENVI.COED_CARP_CD_RAMO AND CPC.COPA_NVALOR3 = ENVI.COED_CAPO_NU_POLIZA) \n")
				.append("            LEFT JOIN COLECTIVOS_PARAMETROS PRO ON (PRO.COPA_CD_PARAMETRO = TO_NUMBER(NVL(CPC.COPA_VVALOR4, 0))) \n")
				.append("        WHERE ENVI.COED_CASU_CD_SUCURSAL	= #canal \n")
				.append("            AND ENVI.COED_CARP_CD_RAMO  	= #ramo	 \n")
				.append("            AND ENVI.COED_CAPO_NU_POLIZA  	= #poliza \n")
				.append("            AND ENVI.COED_CD_ENDOSO  	IN(500, 600)  \n")
				.append("            AND ENVI.COED_FE_SOLICITUD BETWEEN #fechaini AND #fechafin \n")
				.append(UNION)
				.append("        SELECT ENDE.COED_CAPO_NU_POLIZA, ENDE.COED_NU_CERTIFICADO, ENDE.COED_CAMPON2, \n")
				.append("                NVL(CPC.COPA_VVALOR6, 0), ENDE.COED_CARP_CD_RAMO, NVL(PRO.COPA_VVALOR1, 'NO PARAMETRIZADO') PRODUCTO,  \n")
				.append("                ENDE.COED_CASU_CD_SUCURSAL, ENDE.COED_NU_FOLIO_ENTRADA,  \n")
				.append("                ENDE.COED_NOMBRE_NVO PRIMAENDOSO, REGEXP_SUBSTR(ENDE.COED_CAMPOV5, '[^|A-Z]+', 1, 9) AS DPO,  \n")
				.append("                REGEXP_SUBSTR(ENDE.COED_CAMPOV5, '[^|A-Z]+', 1, 6) AS RFI, \n")
				.append("                REGEXP_SUBSTR(ENDE.COED_CAMPOV5, '[^|A-Z]+', 1, 3) AS IVA, ENDE.COED_CAMPON4 PRIMATOTALENDOSO,  \n")
				.append("                ENDE.COED_FE_SOLICITUD, ENDE.COED_CAPJ_CD_SUCURSAL, ENDE.COED_NU_RECIBO, ENDE.COED_ID_CERTIFICADO    \n")
				.append("            FROM COLECTIVOS_ENDOSOS ENDE \n")
				.append("            LEFT JOIN COLECTIVOS_PARAMETROS CPC ON(CPC.COPA_DES_PARAMETRO = 'POLIZA' AND CPC.COPA_ID_PARAMETRO = 'COBRANZA' AND CPC.COPA_NVALOR1 = ENDE.COED_CASU_CD_SUCURSAL AND CPC.COPA_NVALOR2 = ENDE.COED_CARP_CD_RAMO AND CPC.COPA_NVALOR3 = ENDE.COED_CAPO_NU_POLIZA) \n")
				.append("            LEFT JOIN COLECTIVOS_PARAMETROS PRO ON (PRO.COPA_CD_PARAMETRO = TO_NUMBER(NVL(CPC.COPA_VVALOR7, 0))) \n")
				.append("        WHERE ENDE.COED_CASU_CD_SUCURSAL	= #canal \n")
				.append("            AND ENDE.COED_CARP_CD_RAMO  	= #ramo	 \n")
				.append("            AND ENDE.COED_CAPO_NU_POLIZA  	= #poliza \n")
				.append("            AND ENDE.COED_CD_ENDOSO  	IN(500, 600)  \n")
				.append("			 AND ENDE.COED_FE_SOLICITUD BETWEEN #fechaini AND #fechafin \n")
				.append("            AND CPC.COPA_NVALOR4 = 2 \n")
				.append("            AND NVL(COED_NOMBRE_NVO,0) > 0 \n")
				.append("    ) EN \n")
				.append("	INNER JOIN ( \n")
				.append("		SELECT C.COCE_CASU_CD_SUCURSAL, C.COCE_CARP_CD_RAMO, C.COCE_CAPO_NU_POLIZA, C.COCE_NU_CERTIFICADO, C.COCE_NU_MOVIMIENTO	 \n")
				.append("				,CL.COCN_BUC_CLIENTE, C.COCE_CAZB_CD_SUCURSAL, C.COCE_CAPU_CD_PRODUCTO, NVL(C.COCE_BUC_EMPRESA,0) AS COCE_BUC_EMPRESA \n")
				.append("				, C.COCE_NU_CUENTA,C.COCE_TP_PRODUCTO_BCO, C.COCE_TP_SUBPROD_BCO, CL.COCN_APELLIDO_PAT, CL.COCN_APELLIDO_MAT, CL.COCN_NOMBRE	 \n")									
				.append("				,CL.COCN_CD_SEXO, CL.COCN_FE_NACIMIENTO, CL.COCN_RFC, C.COCE_CD_PLAZO, HEAD.COCE_FE_DESDE AS COCE_FE_DESDEPOL, HEAD.COCE_FE_HASTA AS COCE_FE_HASTAPOL  \n")		 						
				.append("				,C.COCE_FE_SUSCRIPCION, C.COCE_FE_DESDE, C.COCE_FE_HASTA, C.COCE_FE_ANULACION, C.COCE_FE_ANULACION_COL	 \n")
				.append("				,TRIM(TO_CHAR(DECODE(C.COCE_SUB_CAMPANA,'7',TO_NUMBER(C.COCE_MT_SUMA_ASEG_SI),C.COCE_MT_SUMA_ASEGURADA),'999999999.99')) AS COCE_MT_SUMA_ASEGURADA   \n")
				.append("				,NVL(C.COCE_MT_BCO_DEVOLUCION,0) AS COCE_MT_BCO_DEVOLUCION, DECODE(C.COCE_SUB_CAMPANA,'7',C.COCE_MT_SUMA_ASEGURADA,C.COCE_MT_SUMA_ASEG_SI) AS BASE_CALCULO \n")
				.append("				,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(C.COCE_CASU_CD_SUCURSAL, C.COCE_CARP_CD_RAMO, C.COCE_CAPO_NU_POLIZA,	 \n")
				.append("										C.COCE_CAPU_CD_PRODUCTO,   C.COCE_CAPB_CD_PLAN, C.COCE_NU_COBERTURA,  \n")	
				.append("										NVL(C.COCE_SUB_CAMPANA,0),      C.COCE_MT_SUMA_ASEGURADA,        C.COCE_MT_SUMA_ASEG_SI, \n")
				.append("										C.COCE_MT_PRIMA_PURA, C.COCE_DI_COBRO1, PF.COPA_NVALOR5, 7, NVL(C.COCE_MT_BCO_DEVOLUCION, 0)) AS TARIFA \n")
				.append("				,CL.COCN_CD_ESTADO, ES.CAES_DE_ESTADO, CL.COCN_CD_POSTAL, C.COCE_CAPB_CD_PLAN \n")
				.append("				,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(C.COCE_CASU_CD_SUCURSAL,C.COCE_CARP_CD_RAMO,C.COCE_CAPO_NU_POLIZA, \n")
				.append("										C.COCE_CAPU_CD_PRODUCTO, C.COCE_CAPB_CD_PLAN,     C.COCE_NU_COBERTURA, \n")
				.append("										NVL(C.COCE_SUB_CAMPANA,0),     C.COCE_MT_SUMA_ASEGURADA,      C.COCE_MT_SUMA_ASEG_SI, \n")
				.append("										C.COCE_MT_PRIMA_PURA, C.COCE_DI_COBRO1, PF.COPA_NVALOR5, 3, NVL(C.COCE_MT_BCO_DEVOLUCION, 0)) AS CUOTA \n")
				.append("				,CASE WHEN PF.COPA_NVALOR4 > 0 THEN  \n")	                                          
				.append("					   (SELECT CLN.COCN_FE_NACIMIENTO   ||'|'|| 	 \n")                                  
				.append("							   CLN.COCN_CD_SEXO         ||'|'||    \n")                                
				.append("							   CLC.COCC_TP_CLIENTE      ||'|'||     \n")                               
				.append("							   P.COPA_VVALOR1           ||'|'||      \n")                                             
				.append("							   SUBSTR(C.COCE_EMPRESA,INSTR(C.COCE_EMPRESA,'|OBLIGADO CON INGRESOS: ')+24,2)	  \n")     
				.append("						  FROM COLECTIVOS_CLIENTE_CERTIF CLC 	 \n")                                                    
				.append("							  ,COLECTIVOS_CLIENTES       CLN  \n")	                                                    
				.append("							  ,COLECTIVOS_PARAMETROS     P   	 \n")                                     
				.append("						 WHERE CLC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL	  \n")               
				.append("						   AND CLC.COCC_CARP_CD_RAMO     = C.COCE_CARP_CD_RAMO     \n")	                
				.append("						   AND CLC.COCC_CAPO_NU_POLIZA   = C.COCE_CAPO_NU_POLIZA   \n")	                
				.append("						   AND CLC.COCC_NU_CERTIFICADO   = C.COCE_NU_CERTIFICADO   \n")	                
				.append("						   AND NVL(CLC.COCC_TP_CLIENTE,1)  > 1            \n")         	                
				.append("						   AND CLN.COCN_NU_CLIENTE       = CLC.COCC_NU_CLIENTE    \n") 	                
				.append("						   AND P.COPA_DES_PARAMETRO(+)   = 'ASEGURADO'  \n")           	                
				.append("						   AND P.COPA_ID_PARAMETRO(+)    = 'TIPO'   \n")               	                
				.append("						   AND P.COPA_NVALOR1(+)         = CLC.COCC_TP_CLIENTE)    \n")	                
				.append("					ELSE '' END AS DATOS_OBLIGADOS \n")
				.append("				,ABS( NVL(C.COCE_MT_DEVOLUCION,0) - NVL(C.COCE_MT_BCO_DEVOLUCION,0) ) AS DIFERENCIA_DEVOLUCION \n")
				.append("			FROM COLECTIVOS_CERTIFICADOS C 	 \n")										
				.append("			INNER JOIN COLECTIVOS_CERTIFICADOS HEAD ON(HEAD.COCE_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND HEAD.COCE_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND HEAD.COCE_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND HEAD.COCE_NU_CERTIFICADO = 0)  \n")
				.append("			INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC ON(CC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND CC.COCC_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND CC.COCC_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA	AND CC.COCC_NU_CERTIFICADO = C.COCE_NU_CERTIFICADO AND NVL(CC.COCC_TP_CLIENTE,1) = 1)	 \n")													
				.append("			INNER JOIN COLECTIVOS_CLIENTES CL ON(CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE)	 \n")								
				.append(JOIN_PARAMETROS)
				.append(JOIN_ESTADOS)											
				.append("		WHERE C.COCE_CASU_CD_SUCURSAL 	= #canal	 \n") 							
				.append("			AND C.COCE_CARP_CD_RAMO     = #ramo	 \n")								
				.append("			AND C.COCE_CAPO_NU_POLIZA 	= #poliza \n")
				.append("			AND C.COCE_NU_CERTIFICADO   > 0 			 \n") 															
				.append("			AND PF.COPA_NVALOR1         = C.COCE_CASU_CD_SUCURSAL	 \n")				    
				.append("			AND PF.COPA_NVALOR2         = C.COCE_CARP_CD_RAMO  \n")						
				.append("			AND PF.COPA_NVALOR3         = DECODE(PF.COPA_NVALOR4, 0, C.COCE_CAPO_NU_POLIZA, 1, SUBSTR(C.COCE_CAPO_NU_POLIZA,0,3), SUBSTR(C.COCE_CAPO_NU_POLIZA,0,2)) 	 \n")
				.append("			AND PF.COPA_NVALOR6         = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA,'0')) \n")
				.append(UNION)
				.append("		SELECT CM.COCM_CASU_CD_SUCURSAL, CM.COCM_CARP_CD_RAMO, CM.COCM_CAPO_NU_POLIZA, CM.COCM_NU_CERTIFICADO, CM.COCM_NU_MOVIMIENTO	 \n")
				.append("				,CL.COCN_BUC_CLIENTE, CM.COCM_CAZB_CD_SUCURSAL, CM.COCM_CAPU_CD_PRODUCTO, NVL(CM.COCM_BUC_EMPRESA,0) AS COCE_BUC_EMPRESA \n")
				.append("				, CM.COCM_NU_CUENTA,CM.COCM_TP_PRODUCTO_BCO, CM.COCM_TP_SUBPROD_BCO, CL.COCN_APELLIDO_PAT, CL.COCN_APELLIDO_MAT, CL.COCN_NOMBRE		 \n")								
				.append("				,CL.COCN_CD_SEXO, CL.COCN_FE_NACIMIENTO, CL.COCN_RFC, CM.COCM_CD_PLAZO, HEADM.COCE_FE_DESDE, HEADM.COCE_FE_HASTA  \n")								
				.append("				,CM.COCM_FE_SUSCRIPCION, CM.COCM_FE_DESDE, CM.COCM_FE_HASTA, CM.COCM_FE_ANULACION_REAL, CM.COCM_FE_ANULACION \n")	
				.append("				,TRIM(TO_CHAR(DECODE(CM.COCM_SUB_CAMPANA,'7',TO_NUMBER(CM.COCM_MT_SUMA_ASEG_SI),CM.COCM_MT_SUMA_ASEGURADA),'999999999.99')) AS COCE_MT_SUMA_ASEGURADA  \n") 
				.append("				,NVL(CM.COCM_MT_BCO_DEVOLUCION,0) AS COCE_MT_BCO_DEVOLUCION, DECODE(CM.COCM_SUB_CAMPANA,'7',CM.COCM_MT_SUMA_ASEGURADA,CM.COCM_MT_SUMA_ASEG_SI) AS BASE_CALCULO \n")
				.append("				,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CM.COCM_CASU_CD_SUCURSAL, CM.COCM_CARP_CD_RAMO, CM.COCM_CAPO_NU_POLIZA,	 \n")
				.append("										CM.COCM_CAPU_CD_PRODUCTO,   CM.COCM_CAPB_CD_PLAN, CM.COCM_NU_COBERTURA,  \n")	
				.append("										NVL(CM.COCM_SUB_CAMPANA,0),      CM.COCM_MT_SUMA_ASEGURADA,        CM.COCM_MT_SUMA_ASEG_SI, \n")
				.append("										CM.COCM_MT_PRIMA_PURA, CM.COCM_DI_COBRO1, PF.COPA_NVALOR5, 7, NVL(CM.COCM_MT_BCO_DEVOLUCION, 0)) AS TARIFA \n")
				.append("				,CL.COCN_CD_ESTADO, ES.CAES_DE_ESTADO, CL.COCN_CD_POSTAL, CM.COCM_CAPB_CD_PLAN \n")
				.append("				,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CM.COCM_CASU_CD_SUCURSAL,CM.COCM_CARP_CD_RAMO,CM.COCM_CAPO_NU_POLIZA, \n")
				.append("										CM.COCM_CAPU_CD_PRODUCTO, CM.COCM_CAPB_CD_PLAN,     CM.COCM_NU_COBERTURA, \n")
				.append("										NVL(CM.COCM_SUB_CAMPANA,0),     CM.COCM_MT_SUMA_ASEGURADA,      CM.COCM_MT_SUMA_ASEG_SI, \n")
				.append("										CM.COCM_MT_PRIMA_PURA, CM.COCM_DI_COBRO1, PF.COPA_NVALOR5, 3, NVL(CM.COCM_MT_BCO_DEVOLUCION, 0)) AS CUOTA \n")
				.append("				,CASE WHEN PF.COPA_NVALOR4 > 0 THEN 	 \n")                                          
				.append("					   (SELECT CLN.COCN_FE_NACIMIENTO   ||'|'|| 	 \n")                                  
				.append("							   CLN.COCN_CD_SEXO         ||'|'||      \n")                              
				.append("							   CLC.COCC_TP_CLIENTE      ||'|'||      \n")                              
				.append("							   P.COPA_VVALOR1           ||'|'||       \n")                                            
				.append("							   SUBSTR(CM.COCM_EMPRESA,INSTR(CM.COCM_EMPRESA,'|OBLIGADO CON INGRESOS: ')+24,2) \n")	      
				.append("						  FROM COLECTIVOS_CLIENTE_CERTIF CLC 	  \n")                                                   
				.append("							  ,COLECTIVOS_CLIENTES       CLN 	 \n")                                                    
				.append("							  ,COLECTIVOS_PARAMETROS     P   	    \n")                                  
				.append("						 WHERE CLC.COCC_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL	   \n")              
				.append("						   AND CLC.COCC_CARP_CD_RAMO     = CM.COCM_CARP_CD_RAMO     \n")	                
				.append("						   AND CLC.COCC_CAPO_NU_POLIZA   = CM.COCM_CAPO_NU_POLIZA  	  \n")               
				.append("						   AND CLC.COCC_NU_CERTIFICADO   = CM.COCM_NU_CERTIFICADO   \n")	                
				.append("						   AND NVL(CLC.COCC_TP_CLIENTE,1)  > 1           \n")          	                
				.append("						   AND CLN.COCN_NU_CLIENTE       = CLC.COCC_NU_CLIENTE    	 \n")                
				.append("						   AND P.COPA_DES_PARAMETRO(+)   = 'ASEGURADO'    \n")         	                
				.append("						   AND P.COPA_ID_PARAMETRO(+)    = 'TIPO'       \n")           	                
				.append("						   AND P.COPA_NVALOR1(+)         = CLC.COCC_TP_CLIENTE)   	 \n")                
				.append("					ELSE '' END AS DATOS_OBLIGADOS \n")
				.append("				,ABS( NVL(CM.COCM_MT_DEVOLUCION,0) - NVL(CM.COCM_MT_BCO_DEVOLUCION,0) ) AS DIFERENCIA_DEVOLUCION \n")
				.append("			FROM COLECTIVOS_CERTIFICADOS_MOV CM 	 \n")										
				.append("			INNER JOIN COLECTIVOS_CERTIFICADOS HEADM ON(HEADM.COCE_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL AND HEADM.COCE_CARP_CD_RAMO = CM.COCM_CARP_CD_RAMO AND HEADM.COCE_CAPO_NU_POLIZA = CM.COCM_CAPO_NU_POLIZA AND HEADM.COCE_NU_CERTIFICADO = 0)  \n")
				.append("			INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC ON(CC.COCC_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL AND CC.COCC_CARP_CD_RAMO = CM.COCM_CARP_CD_RAMO AND CC.COCC_CAPO_NU_POLIZA = CM.COCM_CAPO_NU_POLIZA	AND CC.COCC_NU_CERTIFICADO = CM.COCM_NU_CERTIFICADO AND NVL(CC.COCC_TP_CLIENTE,1) = 1)	 \n")													
				.append("			INNER JOIN COLECTIVOS_CLIENTES CL ON(CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE)	 \n")								
				.append(JOIN_PARAMETROS)
				.append(JOIN_ESTADOS)										
				.append("		WHERE CM.COCM_CASU_CD_SUCURSAL 	= #canal \n")	 							
				.append("			AND CM.COCM_CARP_CD_RAMO    = #ramo		 \n")				
				.append("			AND CM.COCM_CAPO_NU_POLIZA 	= #poliza \n")
				.append("			AND CM.COCM_NU_CERTIFICADO   > 0 	 \n")		 															
				.append("			AND PF.COPA_NVALOR1         = CM.COCM_CASU_CD_SUCURSAL \n")					    
				.append("			AND PF.COPA_NVALOR2         = CM.COCM_CARP_CD_RAMO 	 \n")					
				.append("			AND PF.COPA_NVALOR3         = DECODE(PF.COPA_NVALOR4, 0, CM.COCM_CAPO_NU_POLIZA, 1, SUBSTR(CM.COCM_CAPO_NU_POLIZA,0,3), SUBSTR(CM.COCM_CAPO_NU_POLIZA,0,2)) 	 \n")
				.append("			AND PF.COPA_NVALOR6         = TO_NUMBER(NVL(HEADM.COCE_SUB_CAMPANA,'0')) \n")
				.append("	) CER ON(CER.COCE_CASU_CD_SUCURSAL = EN.COED_CASU_CD_SUCURSAL AND CER.COCE_CARP_CD_RAMO = EN.COED_CARP_CD_RAMO AND CER.COCE_CAPO_NU_POLIZA = EN.COED_CAPO_NU_POLIZA AND CER.COCE_NU_CERTIFICADO = EN.COED_NU_CERTIFICADO AND CER.COCE_NU_MOVIMIENTO = EN.COED_CAMPON2) \n");
	
	//Variable para 
	//query reporte provision 
	//y emision contable
	//Se usa para 
	//generar reporte 
	//desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repCobProvisionEmiCon =
			new StringBuilder("SELECT T.*, ((T.TOTAL * T.PORCOM)/100) \n")
				.append("    FROM ( \n")
				.append("        SELECT EN.COED_CARP_CD_RAMO, EN.COED_CAPO_NU_POLIZA, 'MENSUAL',  EN.COED_CAPJ_CD_SUCURSAL,  \n")
				.append("                NVL(PRO.COPA_VVALOR1, 'NO PARAMETRIZADO'),  \n")
				.append("                SUM( DECODE(EN.COED_NU_FOLIO_ENTRADA, 'CANCELADO', 0, DECODE(EN.COED_CARP_CD_RAMO, 61, EN.COED_NOMBRE_ANT, EN.COED_CAMPON3))) PMANET, \n")
				.append("                SUM( DECODE(COED_NU_FOLIO_ENTRADA, 'CANCELADO', DECODE(EN.COED_CARP_CD_RAMO, 61, EN.COED_NOMBRE_ANT, EN.COED_CAMPON3), 0)) PMANETCAN,  \n")
				.append("                0 AS DEV, 0 AS ASI, 0 AS ADM, 0 MOVADM,  \n")
				.append("                SUM( DECODE(EN.COED_NU_FOLIO_ENTRADA, 'CANCELADO', 0, DECODE(EN.COED_CARP_CD_RAMO, 61, EN.COED_NOMBRE_ANT, EN.COED_CAMPON3)))  \n")
				.append("                    + SUM( DECODE(COED_NU_FOLIO_ENTRADA, 'CANCELADO', DECODE(EN.COED_CARP_CD_RAMO, 61, EN.COED_NOMBRE_ANT, EN.COED_CAMPON3), 0)) AS TOTAL,  \n")
				.append("                NVL(CPC.COPA_VVALOR5, 0) AS PORCOM \n")
				.append("            FROM COLECTIVOS_PARAMETROS CPC \n")
				.append("            INNER JOIN COLECTIVOS_ENDOSOS EN ON(EN.COED_CASU_CD_SUCURSAL = CPC.COPA_NVALOR1  AND  EN.COED_CARP_CD_RAMO = CPC.COPA_NVALOR2 AND EN.COED_CAPO_NU_POLIZA = CPC.COPA_NVALOR3 ) \n")
				.append("            LEFT JOIN COLECTIVOS_PARAMETROS PRO ON (PRO.COPA_CD_PARAMETRO = TO_NUMBER(NVL(CPC.COPA_VVALOR4, 0))) \n")
				.append("        WHERE CPC.COPA_DES_PARAMETRO = 'POLIZA'  \n")
				.append("            AND CPC.COPA_ID_PARAMETRO = 'COBRANZA' \n")
				.append("            AND EN.COED_CD_ENDOSO  	IN(500, 600)  \n")
				.append("            AND EN.COED_NU_RECIBO > 0 \n")
				.append("			AND EN.COED_FE_SOLICITUD BETWEEN #fechaini AND #fechafin \n")
				.append("        GROUP BY EN.COED_CARP_CD_RAMO, EN.COED_CAPO_NU_POLIZA, EN.COED_CAPJ_CD_SUCURSAL,PRO.COPA_VVALOR1, CPC.COPA_VVALOR5 \n")
				.append(UNION)
				.append("        SELECT EN.COED_CARP_CD_RAMO, EN.COED_CAPO_NU_POLIZA, 'MENSUAL',  EN.COED_CAPJ_CD_SUCURSAL, \n") 
				.append("                NVL(PRO.COPA_VVALOR1, 'NO PARAMETRIZADO'),  \n")
				.append("                SUM( DECODE(EN.COED_NU_FOLIO_ENTRADA, 'CANCELADO', 0, COED_NOMBRE_NVO)) PMANET, \n")
				.append("                SUM( DECODE(COED_NU_FOLIO_ENTRADA, 'CANCELADO', COED_NOMBRE_NVO, 0)) PMANETCAN,  \n")
				.append("                0 AS DEV, 0 AS ASI, 0 AS ADM, 0 MOVADM,  \n")
				.append("                SUM( DECODE(EN.COED_NU_FOLIO_ENTRADA, 'CANCELADO', 0, COED_NOMBRE_NVO)) +  SUM( DECODE(COED_NU_FOLIO_ENTRADA, 'CANCELADO', COED_NOMBRE_NVO, 0)) AS TOTAL,  \n")
				.append("                NVL(CPC.COPA_VVALOR6, 0) AS PORCOM \n")
				.append("            FROM COLECTIVOS_PARAMETROS CPC \n")
				.append("            INNER JOIN COLECTIVOS_ENDOSOS EN ON(EN.COED_CASU_CD_SUCURSAL = CPC.COPA_NVALOR1  AND  EN.COED_CARP_CD_RAMO = CPC.COPA_NVALOR2 AND EN.COED_CAPO_NU_POLIZA = CPC.COPA_NVALOR3 ) \n")
				.append("            LEFT JOIN COLECTIVOS_PARAMETROS PRO ON (PRO.COPA_CD_PARAMETRO = TO_NUMBER(NVL(CPC.COPA_VVALOR7, 0))) \n")
				.append("        WHERE CPC.COPA_DES_PARAMETRO = 'POLIZA'  \n")
				.append("            AND CPC.COPA_ID_PARAMETRO = 'COBRANZA' \n")
				.append("            AND CPC.COPA_NVALOR4 = 2  \n") 
				.append("            AND EN.COED_CD_ENDOSO  	IN(500, 600)  \n")
				.append("            AND EN.COED_NU_RECIBO > 0 \n")
				.append("			AND EN.COED_FE_SOLICITUD BETWEEN #fechaini AND #fechafin  \n")
				.append("        GROUP BY EN.COED_CARP_CD_RAMO, EN.COED_CAPO_NU_POLIZA, EN.COED_CAPJ_CD_SUCURSAL,PRO.COPA_VVALOR1, CPC.COPA_VVALOR6) T \n");
		
	//Variable para query reporte rehabilitados
	//Se usa para generar reporte desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repCobRehabilitados =
			new StringBuilder("SELECT COCD_CARP_CD_RAMO, COCD_CAPO_NU_POLIZA,  \n")
				.append("		COCD_COCE_NU_CREDITO, COCD_CORE_NU_RECIBO,  \n")
				.append("		'Ajuste Estatus',  \n")
				.append("		DECODE(COCD_ESTATUS, 1, 'PENDIENTE COBRO', 2, 'PAGADO', COCD_CONCEPTO),  \n")
				.append("		COCD_MT_COB_PRIMA_TOTAL, COCD_FECHA_COBRO \n")
				.append("    FROM   COLECTIVOS_COBRANZA_DET \n")
				.append(FILTRO_CANAL)
				.append(FILTRO_RAMO)
				.append(FILTRO_POLIZA)
				.append("    AND COCD_ESTATUS = 2 \n")
				.append("    AND COCD_CONCEPTO = 'Rehabilitado' \n")
				.append(FILTRO_FECHAS);
	
	//Variable para 
	//query detalle endoso
	//Se usa para generar 
	//reporte desde pantalla
	//Este query se regresa 
	//y guarda en una tabla 
	//para que un 
	//proceso controlM
	//lo tome y se genere 
	//reporte en ruta 
	//compartida con usUario
	public static final StringBuilder repDetalleEndoso =
			new StringBuilder("SELECT EN.COED_CARP_CD_RAMO, EN.COED_CAPJ_CD_SUCURSAL, EN.COED_CAPO_NU_POLIZA, EN.COED_NU_SOLICITUD, EN.COED_NU_RECIBO, \n")
				.append("        EN.COED_ID_CERTIFICADO, EN.COED_NU_CERTIFICADO, EN.COED_NU_FOLIO_ENTRADA, CER.COCE_NU_CUENTA, CER.COCE_TP_PRODUCTO_BCO,  \n")
				.append("        CER.COCE_TP_SUBPROD_BCO, CER.COCN_APELLIDO_PAT, CER.COCN_APELLIDO_MAT, CER.COCN_NOMBRE, CER.COCN_CD_SEXO,  \n")
				.append("        CER.COCN_FE_NACIMIENTO, CER.COCN_BUC_CLIENTE, CER.COCN_RFC, CER.COCE_CD_PLAZO, CER.COCE_FE_DESDEPOL, CER.COCE_FE_HASTAPOL,	  \n")						
				.append("        CER.COCE_FE_SUSCRIPCION, CER.COCE_FE_DESDE, CER.COCE_FE_HASTA, CER.COCE_FE_INI_CREDITO, CER.COCE_FE_FIN_CREDITO, \n")
				.append("        CER.COCE_MT_SUMA_ASEGURADA, CER.BASE_CALCULO, EN.COED_CAMPON3, REGEXP_SUBSTR(EN.COED_CAMPOV5, '[^|A-Z]+', 1, 9),  \n")
				.append("        REGEXP_SUBSTR(EN.COED_CAMPOV5, '[^|A-Z]+', 1, 6), REGEXP_SUBSTR(EN.COED_CAMPOV5, '[^|A-Z]+', 1, 3), \n") 
				.append("        EN.COED_CAMPON4, CER.TARIFA, CER.PRIMA_VIDA, CER.PRIMA_DESEMPLEO, CER.COCN_CD_ESTADO, CER.CAES_DE_ESTADO, CER.COCN_CD_POSTAL, \n")
				.append("        CER.COCE_CAPU_CD_PRODUCTO, CER.COCE_CAPB_CD_PLAN, CER.CUOTA, CER.CUOTA_DESEMPLEO \n")
				.append("    FROM COLECTIVOS_ENDOSOS EN \n")
				.append("    INNER JOIN ( \n")
				.append("		SELECT C.COCE_CASU_CD_SUCURSAL, C.COCE_CARP_CD_RAMO, C.COCE_CAPO_NU_POLIZA, C.COCE_NU_CERTIFICADO, C.COCE_NU_MOVIMIENTO	 \n")
				.append("				,CL.COCN_BUC_CLIENTE, C.COCE_CAPU_CD_PRODUCTO, C.COCE_NU_CUENTA, C.COCE_TP_PRODUCTO_BCO, C.COCE_TP_SUBPROD_BCO \n")
				.append("                ,CL.COCN_APELLIDO_PAT, CL.COCN_APELLIDO_MAT, CL.COCN_NOMBRE, CL.COCN_CD_SEXO, CL.COCN_FE_NACIMIENTO, CL.COCN_RFC \n")
				.append("                ,C.COCE_CD_PLAZO, HEAD.COCE_FE_DESDE AS COCE_FE_DESDEPOL, HEAD.COCE_FE_HASTA AS COCE_FE_HASTAPOL 	 \n")	 						
				.append("				,C.COCE_FE_SUSCRIPCION, C.COCE_FE_DESDE, C.COCE_FE_HASTA, C.COCE_FE_INI_CREDITO, C.COCE_FE_FIN_CREDITO	 \n")
				.append("				,TRIM(TO_CHAR(DECODE(C.COCE_SUB_CAMPANA,'7',TO_NUMBER(C.COCE_MT_SUMA_ASEG_SI),C.COCE_MT_SUMA_ASEGURADA),'999999999.99')) AS COCE_MT_SUMA_ASEGURADA  \n") 
				.append("				,NVL(C.COCE_MT_BCO_DEVOLUCION,0) AS COCE_MT_BCO_DEVOLUCION, DECODE(C.COCE_SUB_CAMPANA,'7',C.COCE_MT_SUMA_ASEGURADA,C.COCE_MT_SUMA_ASEG_SI) AS BASE_CALCULO \n")
				.append("				,CL.COCN_CD_ESTADO, ES.CAES_DE_ESTADO, CL.COCN_CD_POSTAL, C.COCE_CAPB_CD_PLAN \n")
				.append("                ,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(C.COCE_CASU_CD_SUCURSAL, C.COCE_CARP_CD_RAMO, C.COCE_CAPO_NU_POLIZA,	 \n")
				.append("								C.COCE_CAPU_CD_PRODUCTO, C.COCE_CAPB_CD_PLAN, C.COCE_NU_COBERTURA, 	 \n")
				.append("								NVL(C.COCE_SUB_CAMPANA,0), C.COCE_MT_SUMA_ASEGURADA, C.COCE_MT_SUMA_ASEG_SI, \n")
				.append("								C.COCE_MT_PRIMA_PURA, C.COCE_DI_COBRO1, PF.COPA_NVALOR5, 7, NVL(C.COCE_MT_BCO_DEVOLUCION, 0)) AS TARIFA \n")
				.append("				,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(C.COCE_CASU_CD_SUCURSAL,C.COCE_CARP_CD_RAMO,C.COCE_CAPO_NU_POLIZA, \n")
				.append("								C.COCE_CAPU_CD_PRODUCTO, C.COCE_CAPB_CD_PLAN, C.COCE_NU_COBERTURA, \n")
				.append("								NVL(C.COCE_SUB_CAMPANA,0), C.COCE_MT_SUMA_ASEGURADA, C.COCE_MT_SUMA_ASEG_SI, \n")
				.append("								C.COCE_MT_PRIMA_PURA, C.COCE_DI_COBRO1, PF.COPA_NVALOR5, 3, NVL(C.COCE_MT_BCO_DEVOLUCION, 0)) AS CUOTA \n")
				.append("                ,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(C.COCE_CASU_CD_SUCURSAL,C.COCE_CARP_CD_RAMO, C.COCE_CAPO_NU_POLIZA,	 \n")
				.append("								C.COCE_CAPU_CD_PRODUCTO,C.COCE_CAPB_CD_PLAN,C.COCE_NU_COBERTURA, 	 \n")
				.append("								NVL(C.COCE_SUB_CAMPANA,0),C.COCE_MT_SUMA_ASEGURADA, C.COCE_MT_SUMA_ASEG_SI,	 \n")
				.append("								C.COCE_MT_PRIMA_PURA, C.COCE_DI_COBRO1, PF.COPA_NVALOR5, 1, NVL(C.COCE_MT_BCO_DEVOLUCION, 0)) PRIMA_VIDA  \n")	
				.append("                ,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(C.COCE_CASU_CD_SUCURSAL,C.COCE_CARP_CD_RAMO,C.COCE_CAPO_NU_POLIZA,	 \n")
				.append("								C.COCE_CAPU_CD_PRODUCTO, C.COCE_CAPB_CD_PLAN, C.COCE_NU_COBERTURA,	 \n")
				.append("								NVL(C.COCE_SUB_CAMPANA,0),C.COCE_MT_SUMA_ASEGURADA,C.COCE_MT_SUMA_ASEG_SI,	 \n")
				.append("								C.COCE_MT_PRIMA_PURA, C.COCE_DI_COBRO1, PF.COPA_NVALOR5, 2, NVL(C.COCE_MT_BCO_DEVOLUCION, 0)) PRIMA_DESEMPLEO  \n")
				.append("                , COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(C.COCE_CASU_CD_SUCURSAL,C.COCE_CARP_CD_RAMO,C.COCE_CAPO_NU_POLIZA,	 \n")
				.append("								C.COCE_CAPU_CD_PRODUCTO,C.COCE_CAPB_CD_PLAN,C.COCE_NU_COBERTURA, \n")
				.append("								NVL(C.COCE_SUB_CAMPANA, 0), C.COCE_MT_SUMA_ASEGURADA, C.COCE_MT_SUMA_ASEG_SI,	 \n")
				.append("								C.COCE_MT_PRIMA_PURA, C.COCE_DI_COBRO1, PF.COPA_NVALOR5, 4, NVL(C.COCE_MT_BCO_DEVOLUCION, 0)) CUOTA_DESEMPLEO  \n")
				.append("			FROM COLECTIVOS_CERTIFICADOS C  \n")											
				.append("			INNER JOIN COLECTIVOS_CERTIFICADOS HEAD ON(HEAD.COCE_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND HEAD.COCE_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND HEAD.COCE_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA AND HEAD.COCE_NU_CERTIFICADO = 0)  \n")
				.append("			INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC ON(CC.COCC_CASU_CD_SUCURSAL = C.COCE_CASU_CD_SUCURSAL AND CC.COCC_CARP_CD_RAMO = C.COCE_CARP_CD_RAMO AND CC.COCC_CAPO_NU_POLIZA = C.COCE_CAPO_NU_POLIZA	AND CC.COCC_NU_CERTIFICADO = C.COCE_NU_CERTIFICADO AND NVL(CC.COCC_TP_CLIENTE,1) = 1)	 \n")													
				.append("			INNER JOIN COLECTIVOS_CLIENTES CL ON(CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE) \n")									
				.append(JOIN_PARAMETROS)
				.append(JOIN_ESTADOS)											
				.append("		WHERE C.COCE_CASU_CD_SUCURSAL 	= #canal	 \n") 							
				.append("			AND C.COCE_CARP_CD_RAMO     = #ramo		 \n")							
				.append("			AND C.COCE_CAPO_NU_POLIZA 	= #poliza \n")
				.append("			AND C.COCE_NU_CERTIFICADO   > 0 			 	 \n")														
				.append("			AND PF.COPA_NVALOR1         = C.COCE_CASU_CD_SUCURSAL	 \n")				    
				.append("			AND PF.COPA_NVALOR2         = C.COCE_CARP_CD_RAMO 		 \n")				
				.append("			AND PF.COPA_NVALOR3         = DECODE(PF.COPA_NVALOR4, 0, C.COCE_CAPO_NU_POLIZA, 1, SUBSTR(C.COCE_CAPO_NU_POLIZA,0,3), SUBSTR(C.COCE_CAPO_NU_POLIZA,0,2)) 	 \n")
				.append("			AND PF.COPA_NVALOR6         = TO_NUMBER(NVL(HEAD.COCE_SUB_CAMPANA,'0')) \n")
				.append(UNION)
				.append("		SELECT CM.COCM_CASU_CD_SUCURSAL, CM.COCM_CARP_CD_RAMO, CM.COCM_CAPO_NU_POLIZA, CM.COCM_NU_CERTIFICADO, CM.COCM_NU_MOVIMIENTO	 \n")
				.append("				,CL.COCN_BUC_CLIENTE, CM.COCM_CAPU_CD_PRODUCTO, CM.COCM_NU_CUENTA, CM.COCM_TP_PRODUCTO_BCO, CM.COCM_TP_SUBPROD_BCO \n")
				.append("                ,CL.COCN_APELLIDO_PAT, CL.COCN_APELLIDO_MAT, CL.COCN_NOMBRE,CL.COCN_CD_SEXO, CL.COCN_FE_NACIMIENTO, CL.COCN_RFC \n")
				.append("                ,CM.COCM_CD_PLAZO, HEADM.COCE_FE_DESDE, HEADM.COCE_FE_HASTA 	 \n")							
				.append("				,CM.COCM_FE_SUSCRIPCION, CM.COCM_FE_DESDE, CM.COCM_FE_HASTA, CM.COCM_FE_INI_CREDITO, CM.COCM_FE_FIN_CREDITO \n")
				.append("				,TRIM(TO_CHAR(DECODE(CM.COCM_SUB_CAMPANA,'7',TO_NUMBER(CM.COCM_MT_SUMA_ASEG_SI),CM.COCM_MT_SUMA_ASEGURADA),'999999999.99')) \n")  
				.append("				,NVL(CM.COCM_MT_BCO_DEVOLUCION,0) AS COCE_MT_BCO_DEVOLUCION, DECODE(CM.COCM_SUB_CAMPANA,'7',CM.COCM_MT_SUMA_ASEGURADA,CM.COCM_MT_SUMA_ASEG_SI) \n")
				.append("				,CL.COCN_CD_ESTADO, ES.CAES_DE_ESTADO, CL.COCN_CD_POSTAL, CM.COCM_CAPB_CD_PLAN \n")
				.append("                ,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CM.COCM_CASU_CD_SUCURSAL, CM.COCM_CARP_CD_RAMO, CM.COCM_CAPO_NU_POLIZA,	 \n")
				.append("								CM.COCM_CAPU_CD_PRODUCTO, CM.COCM_CAPB_CD_PLAN, CM.COCM_NU_COBERTURA, 	 \n")
				.append("								NVL(CM.COCM_SUB_CAMPANA,0), CM.COCM_MT_SUMA_ASEGURADA, CM.COCM_MT_SUMA_ASEG_SI, \n")
				.append("								CM.COCM_MT_PRIMA_PURA, CM.COCM_DI_COBRO1, PF.COPA_NVALOR5, 7, NVL(CM.COCM_MT_BCO_DEVOLUCION, 0)) AS TARIFA \n")
				.append("				,COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CM.COCM_CASU_CD_SUCURSAL,CM.COCM_CARP_CD_RAMO,CM.COCM_CAPO_NU_POLIZA, \n")
				.append("								CM.COCM_CAPU_CD_PRODUCTO, CM.COCM_CAPB_CD_PLAN, CM.COCM_NU_COBERTURA, \n")
				.append("								NVL(CM.COCM_SUB_CAMPANA,0), CM.COCM_MT_SUMA_ASEGURADA, CM.COCM_MT_SUMA_ASEG_SI, \n")
				.append("								CM.COCM_MT_PRIMA_PURA, CM.COCM_DI_COBRO1, PF.COPA_NVALOR5, 3, NVL(CM.COCM_MT_BCO_DEVOLUCION, 0)) AS CUOTA \n")
				.append("                , COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CM.COCM_CASU_CD_SUCURSAL,CM.COCM_CARP_CD_RAMO, CM.COCM_CAPO_NU_POLIZA, \n")	
				.append("								CM.COCM_CAPU_CD_PRODUCTO,CM.COCM_CAPB_CD_PLAN,CM.COCM_NU_COBERTURA,  \n")	
				.append("								NVL(CM.COCM_SUB_CAMPANA,0),CM.COCM_MT_SUMA_ASEGURADA, CM.COCM_MT_SUMA_ASEG_SI,	 \n")
				.append("								CM.COCM_MT_PRIMA_PURA, CM.COCM_DI_COBRO1, PF.COPA_NVALOR5, 1, NVL(CM.COCM_MT_BCO_DEVOLUCION, 0)) PRIMA_VIDA  \n")	
				.append("                , COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CM.COCM_CASU_CD_SUCURSAL,CM.COCM_CARP_CD_RAMO,CM.COCM_CAPO_NU_POLIZA, \n")	
				.append("								CM.COCM_CAPU_CD_PRODUCTO,CM.COCM_CAPB_CD_PLAN,CM.COCM_NU_COBERTURA, \n")
				.append("								NVL(CM.COCM_SUB_CAMPANA,0),CM.COCM_MT_SUMA_ASEGURADA,CM.COCM_MT_SUMA_ASEG_SI,	 \n")
				.append("								CM.COCM_MT_PRIMA_PURA, CM.COCM_DI_COBRO1, PF.COPA_NVALOR5, 2, NVL(CM.COCM_MT_BCO_DEVOLUCION, 0)) PRIMA_DESEMPLEO \n")
				.append("               , COLECTIVOS_UTILERIAS.FGETPRIMAXCOBERTURA(CM.COCM_CASU_CD_SUCURSAL,CM.COCM_CARP_CD_RAMO,CM.COCM_CAPO_NU_POLIZA, \n")	
				.append("								CM.COCM_CAPU_CD_PRODUCTO,CM.COCM_CAPB_CD_PLAN,CM.COCM_NU_COBERTURA, \n")
				.append("								NVL(CM.COCM_SUB_CAMPANA, 0), CM.COCM_MT_SUMA_ASEGURADA, CM.COCM_MT_SUMA_ASEG_SI,	 \n")
				.append("								CM.COCM_MT_PRIMA_PURA, CM.COCM_DI_COBRO1, PF.COPA_NVALOR5, 4, NVL(CM.COCM_MT_BCO_DEVOLUCION, 0)) CUOTA_DESEMPLEO  \n")
				.append("			FROM COLECTIVOS_CERTIFICADOS_MOV CM  \n")											
				.append("			INNER JOIN COLECTIVOS_CERTIFICADOS HEADM ON(HEADM.COCE_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL AND HEADM.COCE_CARP_CD_RAMO = CM.COCM_CARP_CD_RAMO AND HEADM.COCE_CAPO_NU_POLIZA = CM.COCM_CAPO_NU_POLIZA AND HEADM.COCE_NU_CERTIFICADO = 0)  \n")
				.append("			INNER JOIN COLECTIVOS_CLIENTE_CERTIF CC ON(CC.COCC_CASU_CD_SUCURSAL = CM.COCM_CASU_CD_SUCURSAL AND CC.COCC_CARP_CD_RAMO = CM.COCM_CARP_CD_RAMO AND CC.COCC_CAPO_NU_POLIZA = CM.COCM_CAPO_NU_POLIZA	AND CC.COCC_NU_CERTIFICADO = CM.COCM_NU_CERTIFICADO AND NVL(CC.COCC_TP_CLIENTE,1) = 1)	 \n")													
				.append("			INNER JOIN COLECTIVOS_CLIENTES CL ON(CL.COCN_NU_CLIENTE = CC.COCC_NU_CLIENTE) \n")									
				.append(JOIN_PARAMETROS)
				.append(JOIN_ESTADOS)											
				.append("		WHERE CM.COCM_CASU_CD_SUCURSAL 	= #canal	 \n") 							
				.append("			AND CM.COCM_CARP_CD_RAMO    = #ramo		 \n")				
				.append("			AND CM.COCM_CAPO_NU_POLIZA 	= #poliza \n")
				.append("			AND CM.COCM_NU_CERTIFICADO   > 0 		 \n")	 															
				.append("			AND PF.COPA_NVALOR1         = CM.COCM_CASU_CD_SUCURSAL	 \n")				    
				.append("			AND PF.COPA_NVALOR2         = CM.COCM_CARP_CD_RAMO  \n")						
				.append("			AND PF.COPA_NVALOR3         = DECODE(PF.COPA_NVALOR4, 0, CM.COCM_CAPO_NU_POLIZA, 1, SUBSTR(CM.COCM_CAPO_NU_POLIZA,0,3), SUBSTR(CM.COCM_CAPO_NU_POLIZA,0,2))  \n")	
				.append("			AND PF.COPA_NVALOR6         = TO_NUMBER(NVL(HEADM.COCE_SUB_CAMPANA,'0')) \n")
				.append("	) CER ON(CER.COCE_CASU_CD_SUCURSAL = EN.COED_CASU_CD_SUCURSAL AND CER.COCE_CARP_CD_RAMO = EN.COED_CARP_CD_RAMO AND CER.COCE_CAPO_NU_POLIZA = EN.COED_CAPO_NU_POLIZA AND CER.COCE_NU_CERTIFICADO = EN.COED_NU_CERTIFICADO AND CER.COCE_NU_MOVIMIENTO = EN.COED_CAMPON2) \n")
				.append("WHERE COED_CASU_CD_SUCURSAL	= #canal \n")
				.append("    AND COED_CARP_CD_RAMO  	= #ramo \n")
				.append("    AND COED_CAPO_NU_POLIZA = #poliza \n")
				.append("    AND COED_CD_ENDOSO IN(500, 600) \n")
				.append("    AND COED_NU_RECIBO 		= #recibo \n");
	
	//Variable para query 
	//que obtiene fecha de vencimiento
	//Se usa para mostrar 
	//fecha en pantalla
	public static final StringBuilder queryFechaVencimiento =
			new StringBuilder("SELECT COCD_FECHA_VENCIMIENTO + 1 \n")  //SE SUMA UN DIA A FECHA VENCIMENTO
				.append("    FROM COLECTIVOS_COBRANZA_DET \n")  //TABLA DE DETALLA COBRANZA
				.append(FILTRO_CANAL) 	//FILTRO DE CANAL
				.append(FILTRO_RAMO) 	//FILTRO DE RAMO
				.append(FILTRO_POLIZA)	//FILTRO DE POLIZA
				.append("	AND COCD_COCE_NU_CERTIFICADO = #cert \n")
				.append("	AND COCD_CORE_NU_RECIBO  IN( \n")
				.append("		SELECT T.CORE_NU_RECIBO FROM ( \n")
				.append("			SELECT CORE_NU_RECIBO, ROW_NUMBER() OVER(ORDER BY CORE_FE_DESDE DESC) ROW_NUM \n")
				.append("				FROM COLECTIVOS_RECIBOS CR \n")
				.append("			WHERE CR.CORE_CASU_CD_SUCURSAL = #canal \n")
				.append("				AND CR.CORE_CARP_CD_RAMO = #ramo \n")
				.append("				AND CR.CORE_CAPO_NU_POLIZA = #poliza \n")
				.append("				AND CR.CORE_CACE_NU_CERTIFICADO = 0 \n")
				.append("				AND CR.CORE_ST_RECIBO = 4 \n")
				.append("		) T  \n")
				.append("		WHERE T.ROW_NUM = 1) \n");
	
	//Variable para query 
	//que obtiene informacion 
	//de cobranza
	//Se usa para 
	//mostrar detalle 
	//en pantalla reverso estatus
	public static final StringBuilder queryGetDatosCobranza =
			new StringBuilder("SELECT D.COCD_CASU_CD_SUCURSAL,  \n")//CANAL
				.append("       D.COCD_CARP_CD_RAMO,  \n")			//RAMO
				.append("       D.COCD_CAPO_NU_POLIZA,  \n")		//POLIZA
				.append("       D.COCD_COCE_NU_CERTIFICADO,  \n")	//CERTIFICADO
				.append("       D.COCD_CORE_NU_RECIBO,  \n")		//RECIBO
				.append("       D.COCD_COCE_NU_CREDITO ,    \n")    //CREDITO
				.append("       A.COCE_ST_CERTIFICADO,   \n")       //CD ESTATUS CERTIFICADO
				.append("       E.ALES_DESCRIPCION,  \n")           //DESC ESTATUS CERTIFICADO
				.append("       E.ALES_CAMPO1,   \n")               //DESC ESTATUS CERTIFICADO     
				.append("       NVL(A.COCE_CD_CAUSA_ANULACION,0),    \n") //CD CAUSA ANULACION
				.append("       NVL(AE.ALES_DESCRIPCION,' '), \n")       //DESC CAUSA ANULACION
				.append("       A.COCE_FE_ANULACION_COL, \n")       //FECHA CANCELACION 
				.append("       A.COCE_FE_SUSCRIPCION, \n")         //FECHA INGRESO
				.append("       D.COCD_MT_COB_PRIMA_NETA,  \n")     //PRIMA NETA
				.append("       D.COCD_MT_COB_PRIMA_TOTAL, \n")		//PRIMA TOTAL
				.append("       D.COCD_ESTATUS, \n")				//CD ESTATUS COBRANZA
				.append("       DECODE(COCD_ESTATUS, 1, 'PENDIENTE', 2, 'PAGADO', 'NO PAGADO'), \n") //DESC ESTATUS COBRANZA
				.append("       D.COCD_FECHA_VENCIMIENTO,  \n")		//FECHA VENCIMIENTO RECIBO
				.append("       D.COCD_FECHA_COBRO,  \n")			//FECHA COBRO
				.append("       D.COCD_FECHA_NC, \n")				//FECHA NC
				.append("		D.COCD_MT_NOC_PRIMA_TOTAL, \n")		//PRIMA NC
				.append("		CR.CORE_FE_EMISION, \n")			//FECHA EMISION RECIBO
				.append("		CR.CORE_NU_CONSECUTIVO_CUOTA \n")	//CONSECUTIVO
				.append("   FROM COLECTIVOS_COBRANZA_DET D  \n")    //TABLA PARA DETALLA COBRANZA
				.append("   INNER JOIN COLECTIVOS_CERTIFICADOS A ON(A.COCE_CASU_CD_SUCURSAL = D.COCD_CASU_CD_SUCURSAL \n") //JOIN CON TABLA DE CERTIFICADOS		
				.append("			AND A.COCE_CARP_CD_RAMO     = D.COCD_CARP_CD_RAMO  \n") 					
				.append("			AND A.COCE_CAPO_NU_POLIZA   =  D.COCD_CAPO_NU_POLIZA   \n")           	
				.append("			AND A.COCE_NU_CERTIFICADO   = D.COCD_COCE_NU_CERTIFICADO)  \n")	
				.append("	INNER JOIN COLECTIVOS_RECIBOS CR ON(CR.CORE_CASU_CD_SUCURSAL = D.COCD_CASU_CD_SUCURSAL AND CR.CORE_NU_RECIBO =  D.COCD_CORE_NU_RECIBO)   \n")	//JOIN CON TABLA DE RECIBOS
				.append("   LEFT JOIN ALTERNA_ESTATUS AE ON(AE.ALES_CD_ESTATUS  = A.COCE_CD_CAUSA_ANULACION)   \n")     //JOIN CON TABLA DE CAUSAS ANULACION  	
				.append("   LEFT JOIN ALTERNA_ESTATUS E ON(E.ALES_CD_ESTATUS    = A.COCE_ST_CERTIFICADO )    \n")		//JOIN CON TABLA DE ESTATUS CERTIFICADO   
				.append("	WHERE 1=1		\n") 							 
				.append("		#filtro 	\n")
				.append("	ORDER BY 1, 2, 3, 4, 5 DESC	\n");
	
	/**
	 * Constructor privado de clase
	 */
	protected GeneratorQuerys4() {
		// Dont Using
	}

}
