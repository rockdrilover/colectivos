package mx.com.santander.aseguradora.colectivos.utils;

public class RFC {
    
    /**
     * Metodo que genera un RFC de persona fisica.
     * 
     * @param nombre
     * 		String, Nombre de la persona
     * @param apellidoPaterno
     * 		String, Apellido paterno de la persona
     * @param apellidoMaterno
     * 		String, Apellido materno de la persona
     * @param fecha
     * 		String, Fecha Nacimiendo de la persona (dd/MM/yyyy)
     * @return
     * 		String, Cadena con el RFC generado
     */
    public static String getRFC(String nombre, String apellidoPaterno, String apellidoMaterno, String fecha) {
		StringBuffer sbRfc = new StringBuffer();
    	
		//COnvertimos a Mayusculas y quitamos espacios
		nombre = nombre.toUpperCase().trim();
		apellidoPaterno = apellidoPaterno.toUpperCase().trim();
		apellidoMaterno = apellidoMaterno.toUpperCase().trim();

		//Quitamos los articulos.
		nombre = quitarArticulos(nombre);
		apellidoPaterno = quitarArticulos(apellidoPaterno);
		apellidoMaterno = quitarArticulos(apellidoMaterno);

		//Quitamos acentos.
		nombre = quitarAcentos(nombre);
		apellidoPaterno = quitarAcentos(apellidoPaterno);
		apellidoMaterno = quitarAcentos(apellidoMaterno);
		
		//Agregamos el primer caracter del apellido paterno
		sbRfc.append(apellidoPaterno.substring(0, 1));
		
		//Buscamos y agregamos al rfc la primera vocal del primer apellido
		apellidoPaterno = apellidoPaterno.substring(1, apellidoPaterno.length());
        int l = apellidoPaterno.length();
        char c;
		for(int i = 0; i<l; i++) {
			c = apellidoPaterno.charAt(i);
			if (esVocal(c)) {
				sbRfc.append(c);
				break;
			}
		}

		//Agregamos el primer caracter del apellido materno
		sbRfc.append(apellidoMaterno.substring(0, 1));

		//Agregamos el primer caracter del primer nombre
		String[] arrNombres = nombre.split(" ");
		if(arrNombres.length > 1) {
			if(arrNombres[0].equals("JOSE") || arrNombres[0].equals("MARIA") || arrNombres[0].equals("MA") || arrNombres[0].equals("MA.")
					|| arrNombres[0].equals("M.") || arrNombres[0].equals("M")) {
				sbRfc.append(arrNombres[1].substring(0, 1));
			} else {
				sbRfc.append(arrNombres[0].substring(0, 1));
			}
		} else {
			sbRfc.append(nombre.substring(0, 1));
		}
		
		//Agregamos datos de fecha
		sbRfc.append(fecha.substring(8, 10)); //Dos Carateres para A�O
		sbRfc.append(fecha.substring(3, 5)); //Dos Caracteres para MES
		sbRfc.append(fecha.substring(0, 2)); //Dos Caracteres para DIA

		return sbRfc.toString();
	}

    /**
     * Metodo que verifica si un caracter es una vocal
     */
    private static boolean esVocal(char letra) {
		if (letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U' ||
			letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Reemplaza los articulos comunes en los apellidos en Mexico por caracter vacio.
	 */
	private static String quitarArticulos(String palabra) {
		return palabra.replace("DEL ", "").replace("LAS ", "").replace("DE ", "").replace("LOS ", "").replace("Y ", "").replace("LA ", "");
	}
	
	/**
	 * 
	 */
	private static String quitarAcentos(String palabra) {
		return palabra.replace("�", "A").replace("�", "a")
					  .replace("�", "E").replace("�", "e")
					  .replace("�", "I").replace("�", "i")
					  .replace("�", "O").replace("�", "o")
					  .replace("�", "U").replace("�", "u");
	}
}