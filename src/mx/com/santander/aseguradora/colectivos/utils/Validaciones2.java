package mx.com.santander.aseguradora.colectivos.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-01-2020
 * Description: Clase para hacer validaciones generales
 * 				Se realizo por que la clase Validaciones ya tiene mas de 10 metodos
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	
 * 		Cuando:
 * 		Porque:
 * ------------------------------------------------------------------------
 * 
 */
public final class Validaciones2 {

	/**
	 * Constructor privado de clase
	 */
	private Validaciones2() {
	    throw new IllegalStateException("Validaciones2 class");
	}
	
	/**
	 * Metodo que valida si un correo electronico es valido
	 * @param email correo a validar
	 * @return true es valido
	 * 			false no es valido
	 */
	public static boolean validaEmail(String email) {
		 
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
 
        Matcher mather = pattern.matcher(email);
        return mather.find();        
    }
		
}
