package mx.com.santander.aseguradora.colectivos.utils;

import java.math.BigDecimal;
import java.util.Date;

public class Resultado {

	private int totalregistros;
	private String descripcion;
	private Date fecha;
	private String cuenta;
	
	private String numcredito;
	private double prima;
	private double sumaasegurada;
	private double primapura;
	private double sumaaseguradasi;
	private String coderror;
	private String descerror;
	private double tarifa;
	private double primavida;
	private double primadesempleo;
	private String imagen;
	private String poliza;
	private int edad;
	private String nombreCliente;


	public Resultado(){
	}

	public Resultado(String strDesc, String strCuenta){
		setDescripcion(strDesc);
		setCuenta(strCuenta);
	}
	
	public Resultado(Date objDate, String strDesc) {
		setFecha(objDate);
		setDescripcion(strDesc);
	}
	
	public Resultado(Date objDate, String strDesc, String strCuenta) {
		setFecha(objDate);
		setDescripcion(strDesc);
		setCuenta(strCuenta);
	}

	public Resultado(String strCuenta, Date objDate) {
		setCuenta(strCuenta);
		setFecha(objDate);
	}

	public Resultado(String strNumCre, Date objFecha, String strNombre) {
		setFecha(objFecha);
		setNumcredito(strNumCre);
		setNombreCliente(strNombre);
	}
	
	public Resultado(Object[] arrRegistro, Integer nTipoRep, Integer nOrigen) {
		switch (nTipoRep) {
			case Constantes.TIPO_REPORTE_VENTAS_NOCONCILIADO_VALOR:
				setNumcredito(arrRegistro[0].toString());
				setPrima(((BigDecimal) arrRegistro[1]).doubleValue());
				break;
			
//			case Constantes.TIPO_REPORTE_VENTAS_CONCILIADO_ERROR_VALOR:
//				setNumcredito(arrRegistro[0].toString());
//				setPrima(arrRegistro[1] == null ? new Double(0) : ((BigDecimal) arrRegistro[1]).doubleValue());
//				setCoderror(arrRegistro[2] == null ? " " : arrRegistro[2].toString());
//				setDescerror(arrRegistro[3] == null ? " " : arrRegistro[3].toString());
//				setTarifa(arrRegistro[4] == null ? new Double(0) : ((BigDecimal) arrRegistro[4]).doubleValue());
//				break;
				
			case Constantes.TIPO_REPORTE_VENTAS_PREEMISION_VALOR:
			case Constantes.TIPO_REPORTE_VENTAS_EMISION_VALOR:
			case Constantes.TIPO_REPORTE_EMITIDAS_VALOR:
				setDescripcion(arrRegistro[0] == null ? "Total" : arrRegistro[0].toString());
				setTotalregistros(((BigDecimal) arrRegistro[1]).intValue());
				setPrima(arrRegistro[2] == null ? new Double(0) : ((BigDecimal) arrRegistro[2]).doubleValue());
				if(arrRegistro.length == 4) {
					setImagen(arrRegistro[3].toString());
				} else {
					setPrimapura(arrRegistro[3] == null ? new Double(0) : ((BigDecimal) arrRegistro[3]).doubleValue());
					setImagen(arrRegistro[4].toString());
				}
				break;
				
			case Constantes.TIPO_REPORTE_DEPOSITOS_NOCONCILIADO_VALOR:
				if(nOrigen.equals(Constantes.CONCILIACION_ORIGEN_ALTAMIRA_VALOR) || nOrigen.equals(Constantes.CONCILIACION_ORIGEN_HIPOTECARIO_VALOR) ||
						nOrigen.equals(Constantes.CONCILIACION_ORIGEN_LCI_VALOR) || nOrigen.equals(Constantes.CONCILIACION_ORIGEN_LE_VALOR)){
					setCuenta(arrRegistro[0].toString());
					setNumcredito(arrRegistro[1].toString());
					setPrima(arrRegistro[2] == null ? new Double(0) : new Double(arrRegistro[2].toString()));
					setDescripcion(arrRegistro[3].toString());
				}else if(nOrigen.equals(Constantes.CONCILIACION_ORIGEN_ALTAIR_VALOR)){
					setCuenta(arrRegistro[0].toString());
					setNumcredito(arrRegistro[1].toString());
					setPrima(arrRegistro[2] == null ? new Double(0) : new Double(arrRegistro[2].toString()));
				}else if(nOrigen.equals(Constantes.CONCILIACION_ORIGEN_DESEMPLEO_TDC_VALOR)){
					setCuenta(arrRegistro[0].toString());
					setNumcredito(arrRegistro[1].toString());
					setPrima(arrRegistro[2] == null ? new Double(0) : new Double(arrRegistro[2].toString()));
				}
				break;
				
			case Constantes.TIPO_REPORTE_BAJAS_VALOR:
			case Constantes.TIPO_REPORTE_ERRORES_CONCI_VALOR:
			case Constantes.TIPO_REPORTE_ERRORES_EMISI_VALOR:
				setNumcredito(arrRegistro[0].toString());
				setPrima(arrRegistro[1] == null ? new Double(0) : ((BigDecimal) arrRegistro[1]).doubleValue());
				setSumaasegurada(arrRegistro[2] == null ? new Double(0) : ((BigDecimal) arrRegistro[2]).doubleValue());
				setCoderror(arrRegistro[3].toString());
				setDescerror(arrRegistro[4] == null ? "" : arrRegistro[4].toString());
				if(nTipoRep.equals(Constantes.TIPO_REPORTE_ERRORES_CONCI_VALOR)){
					setTarifa(arrRegistro[5] == null ? new Double(0) : ((BigDecimal) arrRegistro[5]).doubleValue());
				} else if(nTipoRep.equals(Constantes.TIPO_REPORTE_ERRORES_EMISI_VALOR)) {
					setEdad(arrRegistro[5] == null ? 0 : ((BigDecimal) arrRegistro[5]).intValue());	
				}
				break;
			
			case Constantes.TIPO_REPORTE_VENTAS_LISTOEMITIR_DETALLE:
				setNumcredito(arrRegistro[0].toString());
				setPrima(arrRegistro[1] == null ? new Double(0) : ((BigDecimal) arrRegistro[1]).doubleValue());
				setSumaasegurada(arrRegistro[2] == null ? new Double(0) : ((BigDecimal) arrRegistro[2]).doubleValue());
				if(arrRegistro.length == 6){
					setPrimavida(arrRegistro[3] == null ? new Double(0) : ((BigDecimal) arrRegistro[3]).doubleValue());
					setPrimadesempleo(arrRegistro[4] == null ? new Double(0) : ((BigDecimal) arrRegistro[4]).doubleValue());
					setPoliza(arrRegistro[5].toString());
				} else {
					setPrimapura(arrRegistro[3] == null ? new Double(0) : ((BigDecimal) arrRegistro[3]).doubleValue());
					setSumaaseguradasi(arrRegistro[4] == null ? new Double(0) : ((BigDecimal) arrRegistro[4]).doubleValue());
					setPrimavida(arrRegistro[5] == null ? new Double(0) : ((BigDecimal) arrRegistro[5]).doubleValue());
					setPrimadesempleo(arrRegistro[6] == null ? new Double(0) : ((BigDecimal) arrRegistro[6]).doubleValue());
					setPoliza(arrRegistro[7].toString());
				}
				break;
			default:
				break;
		}
	}


	public Resultado(Object[] arrDatos) {
		setPoliza(((BigDecimal) arrDatos[0]).toString());
		setTotalregistros(((BigDecimal) arrDatos[1]).intValue());
		setDescripcion(arrDatos[2].toString().equals(Constantes.DEFAULT_STRING_ID) ? "PARA EMITIR" : "ERROR");
		setPrima(((BigDecimal) arrDatos[3]).doubleValue());
	}

	public int getTotalregistros() {
		return totalregistros;
	}
	public void setTotalregistros(int totalregistros) {
		this.totalregistros = totalregistros;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getNumcredito() {
		return numcredito;
	}
	public void setNumcredito(String numcredito) {
		this.numcredito = numcredito;
	}
	public double getPrima() {
		return prima;
	}
	public void setPrima(double prima) {
		this.prima = prima;
	}
	public double getSumaasegurada() {
		return sumaasegurada;
	}
	public void setSumaasegurada(double sumaasegurada) {
		this.sumaasegurada = sumaasegurada;
	}
	public String getCoderror() {
		return coderror;
	}
	public void setCoderror(String coderror) {
		this.coderror = coderror;
	}
	public String getDescerror() {
		return descerror;
	}
	public void setDescerror(String descerror) {
		this.descerror = descerror;
	}
	public double getTarifa() {
		return tarifa;
	}
	public void setTarifa(double tarifa) {
		this.tarifa = tarifa;
	}
	public double getPrimavida() {
		return primavida;
	}
	public void setPrimavida(double primavida) {
		this.primavida = primavida;
	}
	public double getPrimadesempleo() {
		return primadesempleo;
	}
	public void setPrimadesempleo(double primadesempleo) {
		this.primadesempleo = primadesempleo;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getPrimapura() {
		return primapura;
	}
	public void setPrimapura(double primapura) {
		this.primapura = primapura;
	}
	public double getSumaaseguradasi() {
		return sumaaseguradasi;
	}
	public void setSumaaseguradasi(double sumaaseguradasi) {
		this.sumaaseguradasi = sumaaseguradasi;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
}
