/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.reference.DefaultHTTPUtilities;

import mx.com.santander.aseguradora.colectivos.model.exception.Excepciones;

import com.csvreader.CsvReader;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;


/**
 * @author Sergio Plata
 *
 */
public class Archivo {

	private String nombreArchivo;
	private String ruta;
	private String tipo;
	private long tamano;
	private byte[] data;
	private File archivo;
	private SmbFile smbArchivo;
	
	public Archivo(){
	}
	
	public Archivo(File archivo) {
		setArchivo(archivo);
	}
	
	public Archivo(String ruta, String nombre, String tipo){
		setNombreArchivo(nombre);
		setRuta(ruta);
		setTipo(tipo);
		setArchivo(new File(getRuta() + getNombreArchivo() + getTipo()));
	}
	
	public Archivo(SmbFile archivo) {
		setSmbArchivo(archivo);
	}
	
	/**
	 * Metodo que copia una lista de registros a un archivo
	 * @param arrColumnas columnas del archivo
	 * @param lstResultados lista de registros
	 * @param separador separador a utilizar
	 * @throws Excepciones con errores al generar el archivo
	 * @throws IOException con errores de escritura
	 */
	public void copiarArchivo(String[] arrColumnas, List<Object> lstResultados, String separador) throws Excepciones, IOException {
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter pw = null;
		Iterator<Object> it;
		Object registros[];
		String strLinea = "", strDato;
		int nTama�o;
		
		try {
			fw = new FileWriter(getArchivo());
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
			nTama�o = arrColumnas.length - 1;
			
			for(int i = 0; arrColumnas.length > i; i++){
				if(nTama�o == i){
					strLinea = strLinea + arrColumnas[i];
				} else {
					strLinea = strLinea + arrColumnas[i] + separador;
				}
			}
			
			if(!strLinea.equalsIgnoreCase(""))
				pw.println(strLinea);
			
			it = lstResultados.iterator();
			while(it.hasNext()){				
				registros = (Object[]) it.next();
				strLinea = "";
				for(int i = 0; registros.length > i; i++){
					if(registros[i] instanceof Timestamp){
						strDato = GestorFechas.formatDate((Timestamp) registros[i], "dd/MM/yyyy");
					} else {
						strDato = registros[i] == null ? "" : Utilerias.quitarEspacios(registros[i].toString());
						strDato=strDato.replace("#bt", " ");
					}
					 
					if(nTama�o == i){
						strLinea = strLinea + strDato;
					} else {
						strLinea = strLinea + strDato + separador;	
					}
				}
				pw.println(strLinea);
			}
			pw.close();
			bw.close();
			fw.close();
		} catch (IOException e) {
			pw.close();
			bw.close();
			fw.close();
			throw new Excepciones("Error:: Archivo.copiarArchivo():: Error al copiar archivo en ruta temporal.  " + e.getMessage());
		}
	}
	
	public void copiarSmbArchivo2(File archivoDestino) throws SmbException {
		InputStream in;
		OutputStream out;
		
		try {
			in = getSmbArchivo().getInputStream();
            out = new FileOutputStream(archivoDestino.getPath());      
            
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {                            
            	out.write(buf, 0, len);    
            }
            
            in.close();
            out.close();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }
	}
	
	public void copiarSmbArchivo(SmbFile archivoDestino) throws Exception{
		FileChannel fcOrigen;
		FileChannel fcSalida;
		
		try {
			fcOrigen= (new FileInputStream(getArchivo())).getChannel();
			fcSalida = (new FileOutputStream(new File(archivoDestino.getURL().toURI()))).getChannel();
			fcOrigen.transferTo(0, getArchivo().length(), fcSalida);
			fcOrigen.close();
			fcSalida.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public void copiarSmbArchivo2(SmbFile archivoDestino) throws SmbException {
		InputStream in;
		SmbFileOutputStream out;
		
		try {
			in = new FileInputStream(getArchivo());
            out = new SmbFileOutputStream(archivoDestino);      
            
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {                            
            	out.write(buf, 0, len);    
            }
            
            in.close();
            out.close();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }
	}
	
	/**
	 * Metodo que regresa una lista con el contenido de un archivo
	 * @param separador separado en que viene las lineas de archivo
	 * @param encabezado bandera que nos indica si leemos el encabezado
	 * @return lista con lineas de archivo
	 * @throws Excepciones con error en general
	 */
	public List<Object[]> leerArchivo(String separador, boolean encabezado) throws Excepciones {
		List<Object[]> arlDatos;
		FileReader fr = null;
		CsvReader csv = null;
		Object arrDatos[];
		String strLinea;
		
		try {
			arlDatos = new ArrayList<Object[]>();
			fr = new FileReader(getArchivo());
			csv = new CsvReader(fr);
			
			if(!encabezado) {
				csv.readRecord();
			}
			
			while(csv.readRecord()) {
				strLinea = csv.getRawRecord();
				arrDatos = strLinea.split("\\" + separador);
				arlDatos.add(arrDatos);
			}
			csv.close();
			fr.close();
		} catch (Exception e) {
			throw new Excepciones("Error:: Archivo.leerArchivo():: Error al leer archivo.  " + e.getMessage());
		}
		
		return arlDatos;
	}
	
	public void descargarArchivo(HttpServletResponse response, int nTipoArchivo) throws Exception{
		FileInputStream fis;		    
	    ServletOutputStream sos;
	    int longitud;
	    byte[] datos;
	    
		try {
		    fis = new FileInputStream(getArchivo());
		    longitud = fis.available();
		    datos = new byte[longitud];
		    fis.read(datos);
		    fis.close();
		    
		    
		    switch (nTipoArchivo) {
				case 1: // EXCEL
					response.setContentType("application/vnd.ms-excel");
					break;
				
				case 2: // TXT o CSV
					response.setContentType ("application/octet-stream");
					break;
					
				case 3: // XML--jalm
					response.setContentType ("application/xml");
					break;
					
				case 4: // pdf--jalm
					response.setContentType ("application/pdf");
					break;
					
				case 5:
					response.setContentType ("application/zip");
					break;
					
				default:
					break;
			}
			
		    DefaultHTTPUtilities httpUtil = new DefaultHTTPUtilities();
	    	httpUtil.setHeader(response, "Content-Disposition", "attachment;filename=" + getNombreArchivo() + getTipo());
		    
		    sos = response.getOutputStream();
		    sos.write(datos);
		    sos.flush();
		    sos.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error:: Archivo.descargarArchivo():: Error al descargar archivo.  " + e.getMessage());
		}
	}
	
	public void eliminarArchivo() throws Exception{
	    
		try {
			getArchivo().delete();
		} catch (Exception e) {
			throw new Exception("Error:: Archivo.eliminarArchivo():: Error al eliminar archivo.  " + e.getMessage());
		}
		
	}
	
	/**
	 * Convierte archivo de texto a String
	 * @param nombreArchivo Nombre de archivo
	 * @return Cadena
	 * @throws Excepciones Error al abrir archivo
	 */
	public StringBuilder archivoToString(String nombreArchivo) throws Excepciones{
		String line;
  	    StringBuilder sb;
    	BufferedReader br = null;
    	
		try {	    	
	  	    sb = new StringBuilder();
	  	    br = new BufferedReader(new FileReader(FacesUtils.pathPl + File.separator + nombreArchivo));
			
			try {
				while ((line = br.readLine()) != null) {
					sb.append(line).append(" \n");
				}
			} finally {
				br.close();
			}
	    	
	    	return sb;
	    } catch (Exception e) {
	    	throw new Excepciones(e.getMessage());
		} 	    
	}
	
	/**
	 * Metodo que esccrive en un archivo registros de una lista
	 * @param lstResultados regisitros a escribir
	 * @throws Excepciones con error en general
	 */
	public void escribirArchivo(List<String> lstResultados) throws Excepciones {
		try {
			FileUtil.write(getArchivo(), lstResultados);
		} catch (IOException e) {
			throw new Excepciones("Error:: Archivo.escribirArchivo():: Error al escribir archivo en ruta temporal.  " + e);
		}
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public long getTamano() {
		return tamano;
	}
	public byte[] getData() {
		return data;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public void setTamano(long tamano) {
		this.tamano = tamano;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public File getArchivo() {
		return archivo;
	}
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setSmbArchivo(SmbFile smbArchivo) {
		this.smbArchivo = smbArchivo;
	}
	public SmbFile getSmbArchivo() {
		return smbArchivo;
	}
}
