/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sergio Plata
 *
 */
public class Validaciones {
	
	
	/**
	 * Metodo para validar solo letras en un String.
	 * @param cadena paremetro que se va a validar
	 * @return true si la cadena solo contiene letras de lo 
	 * contrario regresa false.
	 */
	public static boolean letras(String cadena) {
		Pattern p = Pattern.compile("[0-9|\\.|\\@_|\\-~#+]");
		Matcher m = p.matcher(cadena);
		return (m.find()? false: true);
	}
	
	public static boolean alfaNumerico(String cadena) {
		Pattern p = Pattern.compile("[\\.\\@_\\-~#=]");
		Matcher m = p.matcher(cadena);
		return (m.find()? false: true);
	}
	
	public static boolean numeros(String cadena) {
		Pattern p = Pattern.compile("[a-zA-z|\\.|\\-~#+]");
		Matcher m = p.matcher(cadena);
		return (m.find()? false: true);
	}
	
	public static boolean decimales(String cadena) {
		Pattern p = Pattern.compile("^\\d{1,9}\\.\\d{1,4}");
		Matcher m = p.matcher(cadena);
		
		if(m.find()) {
			return true;
		} else {
			Pattern patron = Pattern.compile("[a-zA-z|\\-~#+]");
			Matcher match = patron.matcher(cadena);
			return (match.find()? false : true);
		}
	}
	
	public static boolean caracteresEspeciales(String cadena) {
		String REG_EXP = "[#$%�?��|!&=��+*~<>]+";
		Pattern pattern = Pattern.compile(REG_EXP);
		Matcher matcher = pattern.matcher(cadena);
		
		return matcher.find();
	}
	
	public static boolean fecha(String fecha) {
		boolean valida = false;
			
		try {			
			new SimpleDateFormat("dd/MM/yyyy").parse(fecha);	
			valida = true;
		} catch (ParseException e) {
			valida = false;
		}
		
		return valida; 
	}

	public static boolean letrasMayusculas(String cadena) {
		Pattern p = Pattern.compile("[A-Z]");
		Matcher m = p.matcher(cadena);
		return m.find();
	}
	
	public static boolean letrasMinusculas(String cadena) {
		Pattern p = Pattern.compile("[a-z]");
		Matcher m = p.matcher(cadena);
		return m.find();
	}
	
	public static boolean tieneNumeros(String cadena) {
		Pattern p = Pattern.compile("[0-9]");
		Matcher m = p.matcher(cadena);
		return m.find();
	}

	/**
	 * Metodo que valida si una cadena tiene secuencias.
	 * Ej1: "11", "aa", "GG"
	 * Ej2: "34", "CD", "de"
	 * 
	 * @param passwordnew
	 * 
	 * return nResultado
	 * 0 - No tiene Secuencias
	 * 1 - Tiene secuencias Ej1
	 * 2 - Tiene secuencias Ej2
	 * 3 - Tiene ambas secuencias
	 */
	public static int tieneSecuencias(String strCadena) {
		int nResultado;
		char[] arrPassword;
		int nChar, nCharTmp = 0;
		boolean bSecuencia = false;
		boolean bRepetido = false;

		try {
			nResultado = 0;
//			arrPassword = strCadena.toCharArray();
			arrPassword = strCadena.toLowerCase().toCharArray();
			for (int i = 0; i < arrPassword.length; i++) {
				nChar = (int)arrPassword[i];
//				System.out.println("Caracter: " + arrPassword[i] + ", Codigo: " + nChar);
				if(nChar == nCharTmp){
					bRepetido = true;
				}

				if((nCharTmp - nChar) == -1) {
					bSecuencia = true;
				}
				
				nCharTmp = nChar;
			}
			
			if(bRepetido && bSecuencia) {
				nResultado = 3;
			} else if(bRepetido) {
				nResultado = 1;
			} else if(bSecuencia){
				nResultado = 2;
			}
		} catch (Exception e) {
			e.printStackTrace();
			nResultado = 1;
		}
		
		return nResultado;
	}
	
	public static void main(String[] args) {
		String strCadena = "Santana*3031";
		String palabra = "SANTAN";
		
		if(strCadena.contains(palabra)) {
			System.out.println("CONTIEN");
		} else {
			System.out.println("NO CONTIEN");
		}
		/*
		System.out.println(caracteresEspeciales(strCadena));
		System.out.println(tieneNumeros(strCadena));
		System.out.println(letrasMayusculas(strCadena));
		System.out.println(letrasMinusculas(strCadena));
		System.out.println(tieneSecuencias(strCadena));
		*/
	}

	
}
