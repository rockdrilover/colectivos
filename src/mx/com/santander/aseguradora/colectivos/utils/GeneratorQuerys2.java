package mx.com.santander.aseguradora.colectivos.utils;

import java.math.BigDecimal;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase que genera cadenas de Querys generales
 * 				Se realizo para quitar codigo en clases 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	Ing. Issac Bautista
 * 		Cuando:	28-10-2020
 * 		Porque: Correcion a observaciones de Sonar
 * ------------------------------------------------------------------------
 * 2.-	Por: 	Ing. Issac Bautista
 * 		Cuando:	08-03-2021
 * 		Porque: Se agrega consulta para detalle credito siniesrtros
 * ------------------------------------------------------------------------
 */
public class GeneratorQuerys2 extends GeneratorQuerys3 {
	
	//Variable para parentesis de consultas
	//Se puede utilizar para variables
	// y metodos de toda la clase
	private static final String PARENTESIS_CLOSE = "'), \n";
	
	//Variable para query reporte tecnico emision
	//Se puede utilizar para cualquier parte del
	//sistema, es generica
	public static final StringBuilder repTecnicoEmision =
			new StringBuilder("SELECT E.*, #cuopend CUOTAS_PEND, (#cuopend * E.PRIMA_NETA) PMA_CUOTAS_PEND, ((#cuopend * E.PRIMA_NETA) + E.PRIMA_NETA) PMATOT_CUOTAS_PEND	\n")
				.append("	FROM (	\n")
				.append("select c.coce_carp_cd_ramo      RAMO 									\n")	
				.append("		,pl.alpl_dato3            CENTRO_COSTOS 						\n")		
				.append("		,c.coce_capo_nu_poliza    POLIZA 								\n")		
				.append("		,c.coce_no_recibo         RECIBO 								\n")		
				.append("		,cc.cocc_id_certificado   CREDITO 								\n")		
				.append("		,c.coce_nu_certificado    CERTIFICADO 							\n")		
				.append("		,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 	\n")		
				.append("		,est.ales_campo1	       ESTATUS   				 			\n")		
				.append("		,'EMITIDO'                ESTATUS_MOVIMIENTO					\n")		
				.append("		,c.coce_nu_cuenta         CUENTA 								\n")		
				.append("		,c.coce_tp_producto_bco   PRODUCTO 								\n")	
				.append("		,c.coce_tp_subprod_bco    SUBPRODUCTO 							\n")		
				.append("		,c.coce_cazb_cd_sucursal  SUCURSAL 								\n")	
				.append("		,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n")	
				.append("		,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n")	
				.append("		,cl.cocn_nombre           NOMBRE 								\n")		
				.append("		,cl.cocn_cd_sexo          SEXO 									\n")	
				.append("		,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n")	
				.append("		,cl.cocn_buc_cliente      NUMERO_CLIENTE 						\n")		
				.append("		,cl.cocn_rfc              RFC 									\n")		
				.append("		,c.coce_cd_plazo          PLAZO 								\n")		
				.append("		,head.coce_fe_desde       FECHA_INICIO_POLIZA 					\n")		
				.append("		,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n")	
				.append("		,c.coce_fe_suscripcion    FECHA_INGRESO 						\n")		
				.append("		,c.coce_fe_desde          FECHA_DESDE 							\n")		
				.append("		,c.coce_fe_hasta          FECHA_HASTA 							\n")		
				.append("		,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n")	
				.append("		,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 					\n")		
				.append("		,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n")	
				.append("		,c.coce_fe_anulacion      FECHA_ANULACION 						\n")		
				.append("		,c.coce_fe_anulacion_col  FECHA_CANCELACION 					\n")		
				.append("		,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA 	\n")
				.append("		,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO	\n") 							
				.append("		,case when pf.copa_nvalor5 = 0	\n") 									 
				.append("					 then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
				.append("				when pf.copa_nvalor5 = 1	\n")									 
				.append("					  then c.coce_mt_prima_pura		\n")						 
				.append("				else 0 end PRIMA_NETA	\n")									
				.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS 	\n")
				.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS 	\n") 
				.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA      	\n")
				.append("		,case when pf.copa_nvalor5 = 0 then  	\n")
				.append("					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)        	\n")
				.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)        	\n")
				.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)        	\n")
				.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
				.append("				when pf.copa_nvalor5 = 1 then  	\n")
				.append("					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)	\n")  
				.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)	\n")  
				.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)	\n")  
				.append("				   + nvl(c.coce_mt_prima_pura,0)  	\n")
				.append("				else 0 end PRIMA_TOTAL 	\n") 									 			
				.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
				.append("                                           c.coce_capu_cd_producto,c.coce_capb_cd_plan, c.coce_nu_cobertura, 	\n")
				.append("                                            NVL(c.coce_sub_campana,0),c.coce_mt_suma_asegurada,             c.coce_mt_suma_aseg_si,   \n")
				.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 7, NVL(c.coce_mt_bco_devolucion, 0)) TARIFA 	\n")
				.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
				.append("                                          c.coce_capu_cd_producto,  c.coce_capb_cd_plan, c.coce_nu_cobertura, 	\n")
				.append("                                            NVL(c.coce_sub_campana,     0),c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,	      \n")
				.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 1, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_VIDA 	\n")
				.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,	\n")
				.append("                                         c.coce_capu_cd_producto,c.coce_capb_cd_plan,c.coce_nu_cobertura, 	\n")
				.append("                                            NVL(c.coce_sub_campana, 0),c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,	      \n")
				.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 2, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_DESEMPLEO 	\n")										
				.append("		,cl.cocn_cd_estado        CD_ESTADO 	\n")								
				.append("		,es.caes_de_estado        ESTADO 		\n")								
				.append("		,cl.cocn_delegmunic       MUNICIPIO 	\n")								
				.append("		,cl.cocn_cd_postal        CP 			\n")								
				.append("		,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION	\n") 					
				.append("		,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS	\n") 			    
				.append("		,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION	\n")   
				.append("		,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO		  	\n")				
				.append("		,c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA 	\n")     
				.append("		,c.coce_capb_cd_plan             PLAN_ASEGURADORA     	\n")     
				.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,\n")
				.append("                                        c.coce_capu_cd_producto,   c.coce_capb_cd_plan,  c.coce_nu_cobertura, 	\n")
				.append("                                            NVL(c.coce_sub_campana, 0), c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,	  \n")
				.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 3, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_BASICA	\n")
				.append("		,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,\n")
				.append("                                       c.coce_capu_cd_producto,    c.coce_capb_cd_plan,c.coce_nu_cobertura, 	\n")
				.append("                                            NVL(c.coce_sub_campana, 0), c.coce_mt_suma_asegurada, c.coce_mt_suma_aseg_si,	        \n")
				.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 4, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_DESEMPLEO   	\n")
				.append("		,case when pf.copa_nvalor4 > 0 then 	\n")                                          
				.append("		   (select cln.cocn_fe_nacimiento   ||'|'|| 	\n")                                  
				.append("				   cln.cocn_cd_sexo         ||'|'||     \n")                              
				.append("				   clc.cocc_tp_cliente      ||'|'||     \n")                              
				.append("				   p.copa_vvalor1           ||'|'||     \n")                                             
				.append("				   substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)	\n")      
				.append("			  from colectivos_cliente_certif clc 	\n")                                                    
				.append("				  ,colectivos_clientes       cln 	\n")                                                    
				.append("				  ,colectivos_parametros     p   	\n")                                     
				.append("			 where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal	\n")                
				.append("			   and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo    	\n")                
				.append("			   and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza  	\n")                
				.append("			   and clc.cocc_nu_certificado   = c.coce_nu_certificado  	\n")                
				.append("			   and nvl(clc.cocc_tp_cliente,1)  > 1                    	\n")                
				.append("			   and cln.cocn_nu_cliente       = clc.cocc_nu_cliente    	\n")                
				.append("			   and p.copa_des_parametro(+)   = 'ASEGURADO'            	\n")                
				.append("			   and p.copa_id_parametro(+)    = 'TIPO'                 	\n")                
				.append("			   and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)   	\n")                
				.append("		else '' end DATOS_OBLIGADO_PU,	\n")                                             
				.append("		#camposFechas	\n")
				.append("		, (C.COCE_NU_CUOTA || '/' || C.COCE_CD_PLAZO) CUOTAS 	\n")
				.append("	from colectivos_certificados   c 		\n")									
				.append("		,colectivos_cliente_certif cc 		\n")									
				.append("		,colectivos_clientes       cl 		\n")									
				.append("		,cart_estados              es 		\n")									
				.append("		,alterna_estatus           est 		\n")								
				.append("		,colectivos_certificados   head 	\n")									
				.append("		,alterna_planes            pl 		\n")									
				.append("		,colectivos_parametros     pf		\n")									
				.append("where c.coce_casu_cd_sucursal 	= #canal	\n") 							
				.append("	and c.coce_carp_cd_ramo     = #ramo 	\n")							
				.append("	#cadenacan	\n")
				.append("	and c.coce_nu_certificado   > 0 \n")			 							
				.append("	and c.coce_fe_carga    >= to_date('#fecha1','dd/mm/yyyy') 	\n")
				.append("	and c.coce_fe_carga    <= to_date('#fecha2','dd/mm/yyyy') 	\n")
				.append("	and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 		\n")		
				.append("	and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 			\n")		
				.append("	and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 	\n")			
				.append("	and cc.cocc_nu_certificado   = c.coce_nu_certificado 		\n")			
				.append("	and nvl(cc.cocc_tp_cliente,1)       = 1 					\n")		    
				.append("	and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 	\n")			
				.append("	and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 			\n")			
				.append("	and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 			\n")		
				.append("	and head.coce_nu_certificado   = 0 								\n")		
				.append("	and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 				\n")			
				.append("	and es.caes_cd_estado(+)     = cl.cocn_cd_estado 				\n")			
				.append("	and est.ales_cd_estatus      = c.coce_st_certificado 			\n")			
				.append("	and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 				\n")		
				.append("	and pl.alpl_cd_producto      = c.coce_capu_cd_producto 			\n")		
				.append("	and pl.alpl_cd_plan          = c.coce_capb_cd_plan  			\n")																				
				.append("	and pf.copa_des_parametro    = 'POLIZA'							\n")	    
				.append("	and pf.copa_id_parametro     = 'GEPREFAC'						\n")			
				.append("	and pf.copa_nvalor1          = c.coce_casu_cd_sucursal			\n")		    
				.append("	and pf.copa_nvalor2          = c.coce_carp_cd_ramo 				\n")		
				.append("	and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) 	\n")
				.append("	and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) 	\n")
				.append(") E	\n");
	
	//Variable para query reporte vigor
	//Se puede utilizar para cualquier parte del
	//sistema, es generica
	public static final StringBuilder repVigor =
		new StringBuilder("select c.coce_carp_cd_ramo      RAMO 									\n")	
			.append("		,pl.alpl_dato3            CENTRO_COSTOS 						\n")		
			.append("		,c.coce_capo_nu_poliza    POLIZA 								\n")		
			.append("		,c.coce_no_recibo         RECIBO 								\n")		
			.append("		,cc.cocc_id_certificado   CREDITO 								\n")		
			.append("		,c.coce_nu_certificado    CERTIFICADO 							\n")
			.append("		,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO		  			\n")			
			.append("		,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 	\n")		
			.append("		,est.ales_campo1	       ESTATUS   				 			\n")		
			.append("		,'ALTA'                ESTATUS_MOVIMIENTO						\n")		
			.append("		,c.coce_nu_cuenta         CUENTA 								\n")		
			.append("		,c.coce_tp_producto_bco   PRODUCTO 								\n")	
			.append("		,c.coce_tp_subprod_bco    SUBPRODUCTO 							\n")		
			.append("		,c.coce_cazb_cd_sucursal  SUCURSAL 								\n")	
			.append("		,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n")	
			.append("		,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n")	
			.append("		,cl.cocn_nombre           NOMBRE 								\n")		
			.append("		,cl.cocn_cd_sexo          SEXO 									\n")	
			.append("		,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n")	
			.append("		,cl.cocn_buc_cliente      NUMERO_CLIENTE 						\n")		
			.append("		,cl.cocn_rfc              RFC 									\n")		
			.append("		,c.coce_cd_plazo          PLAZO 								\n")		
			.append("		,head.coce_fe_desde       FECHA_INICIO_POLIZA 					\n")		
			.append("		,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n")	
			.append("		,c.coce_fe_suscripcion    FECHA_INGRESO 						\n")		
			.append("		,c.coce_fe_desde          FECHA_DESDE 							\n")		
			.append("		,c.coce_fe_hasta          FECHA_HASTA 							\n")		
			.append("		,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n")	
			.append("		,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 					\n")		
			.append("		,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n")	
			.append("		,c.coce_fe_anulacion      FECHA_ANULACION 						\n")		
			.append("		,c.coce_fe_anulacion_col  FECHA_CANCELACION 					\n")		
			.append("		,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA 	\n")
			.append("		,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION	\n") 					
			.append("		,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS	\n")			
			.append("		,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO	\n") 							
			.append("		,case when pf.copa_nvalor5 = 0	\n") 									 
			.append("					 then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
			.append("				when pf.copa_nvalor5 = 1	\n")									 
			.append("					  then c.coce_mt_prima_pura		\n")						 
			.append("				else 0 end PRIMA_NETA	\n")									
			.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS 	\n")
			.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS 	\n") 
			.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA      	\n")
			.append("		,case when pf.copa_nvalor5 = 0 then  	\n")
			.append("					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)        	\n")
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)        	\n")
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)        	\n")
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
			.append("				when pf.copa_nvalor5 = 1 then  	\n")
			.append("					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)	\n")  
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)	\n")  
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)	\n")  
			.append("				   + nvl(c.coce_mt_prima_pura,0)  	\n")
			.append("				else 0 end PRIMA_TOTAL 	\n") 									 			
			.append("		,  COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
			.append("                                         c.coce_capu_cd_producto,   c.coce_capb_cd_plan, c.coce_nu_cobertura, 	\n")
			.append("                                            NVL(c.coce_sub_campana,0),      c.coce_mt_suma_asegurada,        c.coce_mt_suma_aseg_si,\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 7, NVL(c.coce_mt_bco_devolucion, 0)) TARIFA 	\n")
			.append("		,  COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
			.append("                                        c.coce_capu_cd_producto,c.coce_capb_cd_plan,    c.coce_nu_cobertura, 	\n")
			.append("                                            NVL(c.coce_sub_campana,  0),      c.coce_mt_suma_asegurada,     c.coce_mt_suma_aseg_si,\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 1, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_VIDA 	\n")
			.append("		,  COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,	\n")
			.append("                                     c.coce_capu_cd_producto,     c.coce_capb_cd_plan,     c.coce_nu_cobertura, 	\n")
			.append("                                            NVL(c.coce_sub_campana,0),c.coce_mt_suma_asegurada,           c.coce_mt_suma_aseg_si,	     \n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 2, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_DESEMPLEO 	\n")										
			.append("		,cl.cocn_cd_estado        CD_ESTADO 	\n")								
			.append("		,es.caes_de_estado        ESTADO 		\n")																		
			.append("		,cl.cocn_cd_postal        CP 			\n")											 			    			   							
			.append("		,c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA 	\n")     
			.append("		,c.coce_capb_cd_plan             PLAN_ASEGURADORA     	\n")     
			.append("		,  COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,\n")
			.append("                                            c.coce_capu_cd_producto, c.coce_capb_cd_plan,     c.coce_nu_cobertura,\n")
			.append("                                            NVL(c.coce_sub_campana,0),     c.coce_mt_suma_asegurada,      c.coce_mt_suma_aseg_si,\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 3, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_BASICA	\n")
			.append("		,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,	    \n")
			.append("                                            c.coce_capu_cd_producto, c.coce_capb_cd_plan,c.coce_nu_cobertura, 	      \n")
			.append("                                            NVL(c.coce_sub_campana,0),     c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 4, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_DESEMPLEO   	\n")
			.append("		,case when pf.copa_nvalor4 > 0 then 	\n")                                          
			.append("		   (select cln.cocn_fe_nacimiento   ||'|'|| 	\n")                                  
			.append("				   cln.cocn_cd_sexo         ||'|'||     \n")                              
			.append("				   clc.cocc_tp_cliente      ||'|'||     \n")                              
			.append("				   p.copa_vvalor1           ||'|'||     \n")                                             
			.append("				   substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2)	\n")      
			.append("			  from colectivos_cliente_certif clc 	\n")                                                    
			.append("				  ,colectivos_clientes       cln 	\n")                                                    
			.append("				  ,colectivos_parametros     p   	\n")                                     
			.append("			 where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal	\n")                
			.append("			   and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo    	\n")                
			.append("			   and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza  	\n")                
			.append("			   and clc.cocc_nu_certificado   = c.coce_nu_certificado  	\n")                
			.append("			   and nvl(clc.cocc_tp_cliente,1)  > 1                    	\n")                
			.append("			   and cln.cocn_nu_cliente       = clc.cocc_nu_cliente    	\n")                
			.append("			   and p.copa_des_parametro(+)   = 'ASEGURADO'            	\n")                
			.append("			   and p.copa_id_parametro(+)    = 'TIPO'                 	\n")                
			.append("			   and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)   	\n")                
			.append("		else '' end DATOS_OBLIGADO_PU,	\n")                                             
			.append("		#camposFechas	\n")
			.append("		,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION	\n")
			.append("	from colectivos_certificados   c 		\n")									
			.append("		,colectivos_cliente_certif cc 		\n")									
			.append("		,colectivos_clientes       cl 		\n")									
			.append("		,cart_estados              es 		\n")									
			.append("		,alterna_estatus           est 		\n")								
			.append("		,colectivos_certificados   head 	\n")									
			.append("		,alterna_planes            pl 		\n")									
			.append("		,colectivos_parametros     pf		\n")									
			.append("where c.coce_casu_cd_sucursal 	= #canal	\n") 							
			.append("	and c.coce_carp_cd_ramo     = #ramo 	\n")							
			.append("	#cadenacan	\n")
			.append("	and c.coce_nu_certificado   > 0 \n")			 							
			.append("	and c.COCE_ST_CERTIFICADO in(1,2)  	\n")
			.append("	and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 		\n")		
			.append("	and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 			\n")		
			.append("	and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 	\n")			
			.append("	and cc.cocc_nu_certificado   = c.coce_nu_certificado 		\n")			
			.append("	and nvl(cc.cocc_tp_cliente,1)       = 1 					\n")		    
			.append("	and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 	\n")			
			.append("	and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 			\n")			
			.append("	and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 			\n")		
			.append("	and head.coce_nu_certificado   = 0 								\n")		
			.append("	and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 				\n")			
			.append("	and es.caes_cd_estado(+)     = cl.cocn_cd_estado 				\n")			
			.append("	and est.ales_cd_estatus      = c.coce_st_certificado 			\n")			
			.append("	and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 				\n")		
			.append("	and pl.alpl_cd_producto      = c.coce_capu_cd_producto 			\n")		
			.append("	and pl.alpl_cd_plan          = c.coce_capb_cd_plan  			\n")																				
			.append("	and pf.copa_des_parametro    = 'POLIZA'							\n")	    
			.append("	and pf.copa_id_parametro     = 'GEPREFAC'						\n")			
			.append("	and pf.copa_nvalor1          = c.coce_casu_cd_sucursal			\n")		    
			.append("	and pf.copa_nvalor2          = c.coce_carp_cd_ramo 				\n")		
			.append("	and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) 	\n")
			.append("	and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) 	\n");
	
	//Variable para query reporte tecnico cancelacion
	//Se puede utilizar para cualquier parte del
	//sistema, es generica
	public static final StringBuilder repTecnicoCancelacion =
		new StringBuilder("SELECT C.*, #cuopend CUOTAS_PEND, (#cuopend * C.PRIMA_NETA) PMA_CUOTAS_PEND, ((#cuopend * C.PRIMA_NETA) + C.PRIMA_NETA) PMATOT_CUOTAS_PEND	\n")
			.append("    	FROM (	\n")
			.append("    	\n")
			.append("    select c.coce_carp_cd_ramo      RAMO 										\n")
			.append("    		,pl.alpl_dato3            CENTRO_COSTOS 							\n")	
			.append("    		,c.coce_capo_nu_poliza    POLIZA 									\n")	
			.append("    		,c.coce_no_recibo         RECIBO 									\n")	
			.append("    		,cc.cocc_id_certificado   CREDITO 									\n")	
			.append("    		,c.coce_nu_certificado    CERTIFICADO 								\n")	
			.append("    		,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 		\n")	
			.append("    		,est.ales_campo1	       ESTATUS   				 				\n")	
			.append("    		,'BAJA'                   ESTATUS_MOVIMIENTO						\n")	
			.append("    		,c.coce_nu_cuenta         CUENTA 									\n")	
			.append("    		,c.coce_tp_producto_bco   PRODUCTO 									\n")
			.append("    		,c.coce_tp_subprod_bco    SUBPRODUCTO 								\n")	
			.append("    		,c.coce_cazb_cd_sucursal  SUCURSAL 									\n")
			.append("    		,cl.cocn_apellido_pat     APELLIDO_PATERNO 							\n")
			.append("    		,cl.cocn_apellido_mat     APELLIDO_MATERNO 							\n")
			.append("    		,cl.cocn_nombre           NOMBRE 									\n")	
			.append("    		,cl.cocn_cd_sexo          SEXO 										\n")
			.append("    		,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 							\n")
			.append("    		,cl.cocn_buc_cliente      NUMERO_CLIENTE 							\n")	
			.append("    		,cl.cocn_rfc              RFC 										\n")	
			.append("    		,c.coce_cd_plazo          PLAZO 									\n")	
			.append("    		,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n")	
			.append("    		,head.coce_fe_hasta       FECHA_FIN_POLIZA 							\n")
			.append("    		,c.coce_fe_suscripcion    FECHA_INGRESO 							\n")	
			.append("    		,c.coce_fe_desde          FECHA_DESDE 								\n")	
			.append("    		,c.coce_fe_hasta          FECHA_HASTA 								\n")	
			.append("    		,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 						\n")
			.append("    		,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 						\n")	
			.append("    		,c.coce_campov6		   FECHA_FIN_CREDITO_2 							\n")
			.append("    		,c.coce_fe_anulacion      FECHA_ANULACION 							\n")	
			.append("    		,c.coce_fe_anulacion_col  FECHA_CANCELACION 						\n")	
			.append("    		,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA 	\n")
			.append("    		,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO	\n") 							
			.append("    		,DECODE(NVL(c.coce_sub_campana, '0')	\n")
			.append("                , '0', case when pf.copa_nvalor5 = 0	\n") 									 
			.append("    					 then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
			.append("    				when pf.copa_nvalor5 = 1	\n")									 
			.append("    					  then c.coce_mt_prima_pura	\n")							 
			.append("    				else 0 end	\n")
			.append("                , NVL(c.coce_mt_bco_devolucion, 0) )PRIMA_NETA	\n")									
			.append("    		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS 	\n")
			.append("    		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS  	\n")
			.append("    		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA 	\n")
			.append("    		,DECODE(NVL(c.coce_sub_campana, '0')	\n")
			.append("                , '0', case when pf.copa_nvalor5 = 0 then	\n")  
			.append("    					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)  	\n")
			.append("    				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)  	\n")
			.append("    				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)  	\n")
			.append("    				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
			.append("    				when pf.copa_nvalor5 = 1 then  	\n")
			.append("    					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)  	\n")
			.append("    				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)  	\n")
			.append("    				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)  	\n")
			.append("    				   + nvl(c.coce_mt_prima_pura,0)  	\n")
			.append("    				else 0 end 	\n")
			.append("                , NVL(c.coce_mt_bco_devolucion, 0) )PRIMA_TOTAL 	\n")  									 			
			.append("    	,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza, 	\n")
			.append("                                                c.coce_capu_cd_producto,      c.coce_capb_cd_plan, c.coce_nu_cobertura,\n")
			.append("                                                NVL(c.coce_sub_campana,   0),    c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,	\n")
			.append("                                                c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 7, NVL(c.coce_mt_bco_devolucion, 0)) TARIFA	\n")
			.append("    ,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza, 	\n")
			.append("                                                c.coce_capu_cd_producto,c.coce_capb_cd_plan,     c.coce_nu_cobertura,\n")
			.append("                                                NVL(c.coce_sub_campana,0),c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,	           \n")
			.append("                                                c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 5, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_VIDA	\n") 
			.append("   ,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
			.append("                                                c.coce_capu_cd_producto,c.coce_capb_cd_plan,       c.coce_nu_cobertura, \n")
			.append("                                                NVL(c.coce_sub_campana,0),c.coce_mt_suma_asegurada,            c.coce_mt_suma_aseg_si,	\n")
			.append("                                                c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 6, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_DESEMPLEO	\n") 										
			.append("    		,cl.cocn_cd_estado        CD_ESTADO 	\n")								
			.append("    		,es.caes_de_estado        ESTADO 	\n")									
			.append("    		,cl.cocn_delegmunic       MUNICIPIO	\n") 								
			.append("    		,cl.cocn_cd_postal        CP 	\n")										
			.append("    		,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION	\n") 					
			.append("    		,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS	\n") 			    
			.append("    		,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION 	\n")
			.append("    		,nvl(c.coce_buc_empresa,0)  	  CREDITONUEVO	\n")						
			.append("    		,c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA	\n")      
			.append("    		,c.coce_capb_cd_plan             PLAN_ASEGURADORA 	\n")         
			.append("    		,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
			.append("                                                c.coce_capu_cd_producto,     c.coce_capb_cd_plan,      c.coce_nu_cobertura, \n")
			.append("                                                NVL(c.coce_sub_campana,  0),        c.coce_mt_suma_asegurada, c.coce_mt_suma_aseg_si,	\n")
			.append("                                                c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 3, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_BASICA	\n")
			.append("  ,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
			.append("                                             c.coce_capu_cd_producto,c.coce_capb_cd_plan,     c.coce_nu_cobertura, 	      \n")
			.append("                                                  NVL(c.coce_sub_campana,0),        c.coce_mt_suma_asegurada, c.coce_mt_suma_aseg_si,	\n")
			.append("                                                c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 4, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_DESEMPLEO   	\n")
			.append("    		,case when pf.copa_nvalor4 > 0 then 	\n")                                          
			.append("    		   (select cln.cocn_fe_nacimiento   ||'|'|| 	\n")                                  
			.append("    				   cln.cocn_cd_sexo         ||'|'||	\n")                                   
			.append("    				   clc.cocc_tp_cliente      ||'|'||	\n")                                   
			.append("    				   p.copa_vvalor1           ||'|'||	\n")                                                  
			.append("    				   substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) 	\n")     
			.append("    			  from colectivos_cliente_certif clc	\n")                                                     
			.append("    				  ,colectivos_clientes       cln	\n")                                                     
			.append("    				  ,colectivos_parametros     p  	\n")                                      
			.append("    			 where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal	\n")                
			.append("    			   and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo    	\n")                
			.append("    			   and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza  	\n")                
			.append("    			   and clc.cocc_nu_certificado   = c.coce_nu_certificado  	\n")                
			.append("    			   and nvl(clc.cocc_tp_cliente,1)  > 1                    	\n")                
			.append("    			   and cln.cocn_nu_cliente       = clc.cocc_nu_cliente    	\n")                
			.append("    			   and p.copa_des_parametro(+)   = 'ASEGURADO'            	\n")                
			.append("    			   and p.copa_id_parametro(+)    = 'TIPO'                 	\n")                
			.append("    			   and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)   	\n")                
			.append("    		   else '' end DATOS_OBLIGADO_PU,    	\n")                                         
			.append("    		#camposFechas	\n")
			.append("    		, (C.COCE_NU_CUOTA || '/' || C.COCE_CD_PLAZO) CUOTAS 	\n")
			.append("    	from colectivos_certificados   c 		\n")									
			.append("    		,colectivos_cliente_certif cc 		\n")									
			.append("    		,colectivos_clientes       cl 		\n")									
			.append("    		,cart_estados              es 		\n")									
			.append("    		,alterna_estatus           est 		\n")								
			.append("    		,colectivos_certificados   head 	\n")									
			.append("    		,alterna_planes            pl 		\n")									
			.append("    		,colectivos_parametros     pf		\n")									
			.append("    where c.coce_casu_cd_sucursal	= #canal	\n") 							
			.append("    	and c.coce_carp_cd_ramo     = #ramo 	\n")							
			.append("    	#cadenacan	\n")
			.append("    	and c.coce_nu_certificado   > 0	\n")			 							
			.append("    	and c.coce_st_certificado   in (10,11) 	\n") 	     				        
			.append("    	and c.coce_cd_causa_anulacion <> 45	\n")			  	     				
			.append("    	and ( (c.coce_fe_anulacion between to_date('#fecha1','dd/mm/yyyy') AND to_date('#fecha2','dd/mm/yyyy')) OR 	\n")  
			.append("    	      (c.coce_fe_anulacion_col between to_date('#fecha1','dd/mm/yyyy') AND to_date('#fecha2','dd/mm/yyyy')) )	\n")   
			.append("    	and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 		\n")			
			.append("    	and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 		\n")				
			.append("    	and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	\n")	 				
			.append("    	and cc.cocc_nu_certificado   = c.coce_nu_certificado 		\n")				
			.append("    	and nvl(cc.cocc_tp_cliente,1)       = 1 	\n")							    
			.append("    	and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal	\n") 				
			.append("    	and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 		\n")				
			.append("    	and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 		\n")			
			.append("    	and head.coce_nu_certificado   = 0 							\n")			
			.append("    	and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 			\n")				
			.append("    	and es.caes_cd_estado(+)     = cl.cocn_cd_estado 			\n")				
			.append("    	and est.ales_cd_estatus      = c.coce_st_certificado 		\n")				
			.append("    	and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 			\n")			
			.append("    	and pl.alpl_cd_producto      = c.coce_capu_cd_producto 		\n")			
			.append("    	and pl.alpl_cd_plan          = c.coce_capb_cd_plan  		\n")				
			.append("    	and pf.copa_des_parametro    = 'POLIZA'						\n")		    
			.append("    	and pf.copa_id_parametro     = 'GEPREFAC'					\n")				
			.append("    	and pf.copa_nvalor1          = c.coce_casu_cd_sucursal		\n")			    
			.append("    	and pf.copa_nvalor2          = c.coce_carp_cd_ramo 			\n")			
			.append("    	and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, c.coce_capo_nu_poliza, 1, substr(c.coce_capo_nu_poliza,0,3), substr(c.coce_capo_nu_poliza,0,2)) 	\n")
			.append("    	and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) 	\n")
			.append("    ) C	\n");

	//Variable para query de cedula emision
	//Query que hace la foto de una cedula de control
	public static final StringBuilder declareCedula =
		new StringBuilder("DECLARE \n")
			.append("	paramRemesa     varchar(10) := ?;	\n")
			.append("	paramIdVenta	number      := ?;	\n")
			.append("  	paramTipoCedula number      := ?;   \n")
			.append("    paramTipoCedula2 number     := ?;  \n")
			.append("    noCedula    number;                \n")
			.append("    CURSOR curNewCedulas(numero number) IS \n")
			.append("        SELECT COCC_CD_CEDULA cdCedNew, COCC_UNIDAD_NEGOCIO cdCedOrg \n")
			.append("            FROM COLECTIVOS_CEDULA \n")
			.append("        WHERE COCC_REMESA_CEDULA    = 'HIST' \n")            
			.append("            AND COCC_TIPO_CEDULA    = paramTipoCedula2 \n")
			.append("            AND COCC_PROCESADA      = numero; \n")             
			.append("BEGIN \n")
			.append("    dbms_output.put_line('0. Obtenemos version de historico'); \n")
			.append("    SELECT NVL(MAX(COCC_PROCESADA), 0) + 1 INTO noCedula \n")
			.append("        FROM COLECTIVOS_CEDULA \n")
			.append("    WHERE COCC_REMESA_CEDULA    = paramRemesa \n")
			.append("        AND COCC_UNIDAD_NEGOCIO = paramIdVenta \n")
			.append("        AND COCC_TIPO_CEDULA    = paramTipoCedula2; \n")	
			.append("    dbms_output.put_line('1. Insertamos copia de cedulas. La cdcedula original se respalda en COCC_UNIDAD_NEGOCIO'); \n")
			.append("    INSERT INTO COLECTIVOS_CEDULA \n")
			.append("            (COCC_REMESA_CEDULA, COCC_UNIDAD_NEGOCIO, COCC_FE_REGISTRO,  \n")
			.append("                COCC_FE_ENTREGA, COCC_FE_PROCESO, COCC_NU_REGISTROS,  \n")
			.append("                COCC_NU_REG_PND, COCC_MT_PRIMA, COCC_FE_COMER,  \n")
			.append("                COCC_NU_REG_OK, COCC_MT_PRIMA_OK, COCC_NU_REG_NOOK,  \n")
			.append("                COCC_MT_PRIMA_NOOK, COCC_NU_REG_SINPND, COCC_NU_REG_SINPND_OK,  \n")
			.append("                COCC_NU_REG_SINPND_NOOK, COCC_ARCHIVO, COCC_TIPO_CEDULA,  \n")
			.append("                COCC_PROCESADA, COCC_CASU_CD_SUCURSAL, COCC_CARP_CD_RAMO) \n")
			.append("        SELECT 'HIST', COCC_CD_CEDULA, SYSDATE,  \n")
			.append("                COCC_FE_ENTREGA, COCC_FE_PROCESO, COCC_NU_REGISTROS,  \n")
			.append("                COCC_NU_REG_PND, COCC_MT_PRIMA, COCC_FE_COMER,  \n")
			.append("                COCC_NU_REG_OK, COCC_MT_PRIMA_OK, COCC_NU_REG_NOOK,  \n")
			.append("                COCC_MT_PRIMA_NOOK, COCC_NU_REG_SINPND, COCC_NU_REG_SINPND_OK,  \n")
			.append("                COCC_NU_REG_SINPND_NOOK, COCC_ARCHIVO, paramTipoCedula2,  \n")
			.append("                noCedula, COCC_CASU_CD_SUCURSAL, COCC_CARP_CD_RAMO \n")
			.append("            FROM COLECTIVOS_CEDULA \n")
			.append("        WHERE COCC_REMESA_CEDULA    = paramRemesa \n")
			.append("            AND COCC_UNIDAD_NEGOCIO = paramIdVenta \n")
			.append("            AND COCC_TIPO_CEDULA    = paramTipoCedula;  \n")       
			.append("    dbms_output.put_line('2. Recorremos cursor de nuevas cedulas para obtener el detalle de creditos.');         \n")
			.append("    FOR cedula IN curNewCedulas(noCedula) LOOP  \n")
			.append("        dbms_output.put_line('2.1. Insertamos la foto de creditos a tabla.');             \n")
			.append("        INSERT INTO COLECTIVOS_CEDULA_DETALLE_HIST \n")
			.append("                (COCD_COCC_CD_CEDULA, COCD_NU_CREDITO, COCD_FE_INGRESO,  \n")
			.append("                COCD_FE_PROCESO, COCD_MT_PRIMA, COCD_ESTATUS,  \n")
			.append("                COCD_NU_POLIZA, COCD_NU_CERTIFICADO, COCD_MOTIVO_RECHAZO,  \n")
			.append("                COCD_APLICA_PND) \n")
			.append("            SELECT cedula.cdCedNew, COCU_NO_CREDITO, COCU_FECHA_INGRESO,  \n")
			.append("                    COCU_FECHA_CONCILIA, COCU_MONTO_PRIMA, 'EMITIDO',  \n")
			.append("                    COCU_POLIZA, COCU_CERTIFICADO, '',  \n")
			.append("                    '' \n")
			.append("                FROM COLECTIVOS_CUENTAS \n")
			.append("            WHERE COCU_ESTATUS_SEGURO = cedula.cdCedOrg \n")
			.append("            UNION ALL \n")
			.append("            SELECT cedula.cdCedNew, COCD_NU_CREDITO, COCD_FE_INGRESO,  \n")
			.append("                    COCD_FE_PROCESO, COCD_MT_PRIMA, COCD_ESTATUS,  \n")
			.append("                    COCD_NU_POLIZA, COCD_NU_CERTIFICADO, COCD_MOTIVO_RECHAZO,  \n")
			.append("                    COCD_APLICA_PND  \n")
			.append("                FROM  COLECTIVOS_CEDULA_DETALLE \n")
			.append("            WHERE COCD_COCC_CD_CEDULA = TO_NUMBER(cedula.cdCedOrg);         \n")
			.append("        dbms_output.put_line('2.2. Actualizamos la unidad de negocio correcta.'); \n")            
			.append("        UPDATE COLECTIVOS_CEDULA SET COCC_UNIDAD_NEGOCIO = paramIdVenta, COCC_REMESA_CEDULA = paramRemesa \n")
			.append("        WHERE COCC_CD_CEDULA = cedula.cdCedNew;  \n")                      
			.append("    END LOOP;  \n")   
			.append("    COMMIT; \n")
			.append("EXCEPTION WHEN OTHERS THEN \n")
			.append("        DBMS_OUTPUT.PUT_LINE('ERROR EN ACTUALIZAR CEUNTAS DE PU:  '||SQLCODE||' -ERROR- '||SQLERRM );  \n")
			.append("END; \n");
	
	//Variable para query de consulta detalle credito para siniestros
	public static final StringBuilder consDetalleCreditoSin = 
		new StringBuilder("SELECT B.COCC_ID_CERTIFICADO,		\n") 
			.append("			NVL(A.COCE_BUC_EMPRESA,'---'),		\n")
			.append("			C.COCN_NOMBRE,						\n")
			.append("			C.COCN_APELLIDO_PAT, 			 	\n")
			.append("			C.COCN_APELLIDO_MAT, 			    \n")
			.append("			NVL(C.COCN_CD_SEXO,' '), 		    \n")
			.append("			TRUNC(C.COCN_FE_NACIMIENTO), 	    \n")
			.append("			NVL(C.COCN_RFC,' '), 			    \n")
			.append("			NVL(C.COCN_CALLE_NUM,' '),     	    \n")
			.append("			NVL(C.COCN_COLONIA,' '),   		    \n")
			.append("			NVL(C.COCN_DELEGMUNIC,' '),   	    \n")
			.append("			NVL(D.CAES_DE_ESTADO,' '),		    \n")
			.append("			NVL(C.COCN_CD_POSTAL,' '),		    \n")
			.append("			NVL(C.COCN_NU_LADA,'---'),		    \n")
			.append("			NVL(C.COCN_NU_TELEFONO,'---'),	    \n")
			.append("			NVL(C.COCN_BUC_CLIENTE,' '),	    \n")
			.append("			C.COCN_NU_CLIENTE,				    \n")
			.append("			NVL(A.COCE_NU_CUENTA,' '),          \n")
			.append("			A.COCE_CASU_CD_SUCURSAL,            \n")
			.append("			A.COCE_CARP_CD_RAMO,                \n")
			.append("			A.COCE_CAPO_NU_POLIZA,              \n")
			.append("			A.COCE_NU_CERTIFICADO,              \n")
			.append("			A.COCE_ST_CERTIFICADO,              \n")
			.append("			E.ALES_DESCRIPCION,                 \n")
			.append("			E.ALES_CAMPO1,                      \n")
			.append("			NVL(A.COCE_CD_CAUSA_ANULACION,0),   \n")
			.append("			NVL(AE.ALES_DESCRIPCION,' '),       \n")
			.append("			A.COCE_FE_ANULACION_COL,            \n")
			.append("			A.COCE_CAPU_CD_PRODUCTO,            \n")
			.append("			A.COCE_CAPB_CD_PLAN,                \n")
			.append("			A.COCE_FE_CARGA,                    \n")
			.append("			A.COCE_FE_SUSCRIPCION,              \n")
			.append("			A.COCE_MT_SUMA_ASEGURADA,           \n")
			.append("			A.COCE_MT_SUMA_ASEG_SI,             \n")
			.append("			A.COCE_MT_PRIMA_SUBSECUENTE,        \n")
			.append("			A.COCE_MT_PRIMA_PURA,               \n")
			.append("			A.COCE_FE_DESDE,                    \n")
			.append("			A.COCE_FE_HASTA,                    \n")
			.append("			A.COCE_FE_INI_CREDITO,              \n")
			.append("			A.COCE_FE_FIN_CREDITO,              \n")
			.append("			A.COCE_NO_RECIBO,                   \n")
			.append("			F.CARP_DE_RAMO,                     \n")
			.append("			A.COCE_CAZB_CD_SUCURSAL,            \n")
			.append("			A.COCE_FE_EMISION,                  \n")
			.append("			A.COCE_EMPRESA,                     \n")
			.append("			NVL(A.COCE_SUB_CAMPANA, '0'),       \n")
			.append("			A.COCE_NU_COBERTURA			        \n")
			.append("		FROM COLECTIVOS_CLIENTE_CERTIF B        \n")
			.append("		INNER JOIN COLECTIVOS_CERTIFICADOS A ON(A.COCE_CASU_CD_SUCURSAL = B.COCC_CASU_CD_SUCURSAL		\n")
			.append("	                                AND A.COCE_CARP_CD_RAMO     = B.COCC_CARP_CD_RAMO  					\n")
			.append("	                                AND A.COCE_CAPO_NU_POLIZA   = B.COCC_CAPO_NU_POLIZA             	\n")
			.append("	                                AND A.COCE_NU_CERTIFICADO   = B.COCC_NU_CERTIFICADO             	\n")
			.append("	                                AND 1                       = NVL(B.COCC_TP_CLIENTE,1)) 			\n") 
			.append("		INNER JOIN COLECTIVOS_CLIENTES C ON(C.COCN_NU_CLIENTE   = B.COCC_NU_CLIENTE)  			    	\n")
			.append("		INNER JOIN CART_RAMOS_POLIZAS F ON(A.COCE_CARP_CD_RAMO = F.CARP_CD_RAMO)                    	\n")
			.append("		LEFT JOIN CART_ESTADOS D ON(D.CAES_CD_ESTADO    = C.COCN_CD_ESTADO)  					    	\n")
			.append("		LEFT JOIN ALTERNA_ESTATUS AE ON(AE.ALES_CD_ESTATUS  = A.COCE_CD_CAUSA_ANULACION)            	\n")
			.append("		LEFT JOIN ALTERNA_ESTATUS E ON(E.ALES_CD_ESTATUS    = A.COCE_ST_CERTIFICADO )               	\n")
			.append("	WHERE 1=1		\n") 							 
			.append("		#filtro 	\n")
			.append("	ORDER BY C.COCN_NOMBRE, C.COCN_APELLIDO_PAT, C.COCN_APELLIDO_MAT	\n");
	
	//Variable para query de consulta coberturas
	public static final StringBuilder consCoberturas = 
		new StringBuilder("SELECT COB.COCB_CARB_CD_RAMO,				\n") 
			.append("			COB.COCB_CACB_CD_COBERTURA,				\n")
			.append("			CACB_DE_COBERTURA, 						\n")
			.append("			NVL(COB.COCB_TA_RIESGO, 0), 			\n")
			.append("			NVL(COB.COCB_CAMPOV2, 0), 				\n")
			.append("			TO_NUMBER(NVL(COB.COCB_CAMPOV1, 0)),	\n")
			.append("			NVL(COB.COCB_CAMPON1, 1000),			\n")
			.append("			NVL(COB.COCB_PO_COMISION, 0) 			\n")
			.append("		FROM COLECTIVOS_COBERTURAS COB      \n")
			.append("		INNER JOIN CART_COBERTURAS ON(CACB_CARB_CD_RAMO = COB.COCB_CARB_CD_RAMO	\n")
			.append("	    		AND CACB_CD_COBERTURA = COB.COCB_CACB_CD_COBERTURA)				\n") 
			.append("	WHERE COB.COCB_CASU_CD_SUCURSAL     = --w_canal--  			    			\n")
			.append("		AND COB.COCB_CARP_CD_RAMO       = --w_ramo--                    		\n")
			.append("		AND DECODE(--w_idventa--, 0, --wpoliza_col--, TO_NUMBER(SUBSTR(--wpoliza_col--,1,5)))  = COB.COCB_CAPO_NU_POLIZA	\n")
			.append("		AND COB.COCB_CAPU_CD_PRODUCTO	= --w_prod--           					\n")
			.append("		AND COB.COCB_CAPB_CD_PLAN       = --w_plan--               				\n")
			.append("		AND COB.COCB_CER_NU_COBERTURA   = --w_nucob--							\n");
	
	/**
	 * Constructor privado de clase
	 */
	protected GeneratorQuerys2() {
		// Dont Using
	}
	
	/**
	 * Metodo que genera consulta para historial de recibos por certificados
	 * @param ramo ramo para filtro
	 * @param poliza poliza para filtro
	 * @param certificado para filtro
	 * @param recibo recibo para filtro
	 * @param anio anio para filtro
	 * @return cadena con query
	 */
	public static StringBuilder getQryHistRecCerti(byte ramo, int poliza, int certificado, BigDecimal recibo, Long anio) {
		//Se inicializa variable para query
		StringBuilder sbQuery = new StringBuilder();
		
		//Se va armando query
		sbQuery.append("SELECT CODX_CORE_NU_RECIBO, CODX_NU_CUOTA_POLIZA, CODX_FE_CONTABLE_REG, CORE_FE_DESDE, CORE_FE_HASTA, CORE_ST_RECIBO||'-'||ESSIS.RV_MEANING, NVL(CORE_TP_TRAMITE, 1) ||'-'||ESCOB.RV_MEANING, CODX_MT_PRIMA_TOTAL \n");
			sbQuery.append("FROM COLECTIVOS_DXP \n");
			sbQuery.append("INNER JOIN COLECTIVOS_RECIBOS CR ON(CORE_CASU_CD_SUCURSAL = CODX_CASU_CD_SUCURSAL AND  CORE_NU_RECIBO = CODX_CORE_NU_RECIBO) \n");
			sbQuery.append("LEFT JOIN CG_REF_CODES ESSIS ON(ESSIS.RV_DOMAIN = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO' AND ESSIS.RV_LOW_VALUE = CORE_ST_RECIBO) \n");  
			sbQuery.append("LEFT JOIN CG_REF_CODES ESCOB ON(ESCOB.RV_DOMAIN = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO' AND ESCOB.RV_LOW_VALUE = NVL(CORE_TP_TRAMITE, 1)) \n"); 
		sbQuery.append("WHERE CODX_CASU_CD_SUCURSAL = 1 \n");
			sbQuery.append("AND CODX_CARP_CD_RAMO = ").append(ramo).append(" \n");
			sbQuery.append("AND CODX_CAPO_NU_POLIZA = ").append(poliza).append(" \n"); 
			sbQuery.append("AND CODX_COCE_NU_CERTIFICADO = ").append(certificado).append(" \n");
		
			//Se valida se haya seleccionado un recibo
			if(recibo.intValue() > Constantes.DEFAULT_INT) {
				sbQuery.append("AND CODX_CORE_NU_RECIBO = ").append(recibo).append(" \n");
			}
			
			//Se termina el armado del query
			sbQuery.append("AND CODX_NU_TIPO_DXP = 8 \n");
			sbQuery.append("AND  EXTRACT(YEAR FROM CODX_FE_CONTABLE_REG) = ").append(anio).append(" \n");
		sbQuery.append("ORDER BY CODX_FE_CONTABLE_REG DESC \n");
		
		//Se regresa query a ejecutar
		return sbQuery;
		
	}
	
	/**
	 * Metodo que genera query para reporte de TUIIO COKA
	 * @param strFeDesde fecha desde para busqueda
	 * @param strFeHasta fecha hasta para busqueda
	 * @return cadena con query
	 */
	public static StringBuilder getQryReporteCOKA(String strFeDesde, String strFeHasta) {
		//Se inicializa variable para query
		StringBuilder sbQueryRC = new StringBuilder();
		
		//Se va armando query
		sbQueryRC.append("SELECT COCE_CAZB_CD_SUCURSAL, COCE_NU_CREDITO, \n");
		sbQueryRC.append("TO_CHAR(COCE_FE_INI_CREDITO, '").append(Constantes.FORMATO_FECHA_UNO).append(PARENTESIS_CLOSE);
		sbQueryRC.append("TO_CHAR(COCE_FE_FIN_CREDITO, '").append(Constantes.FORMATO_FECHA_UNO).append(PARENTESIS_CLOSE);
		sbQueryRC.append("'VIDA', SUBSTR(COCN_CD_SEXO, 0, 1), \n");
		sbQueryRC.append("TO_CHAR(COCN_FE_NACIMIENTO, '").append(Constantes.FORMATO_FECHA_UNO).append("') \n");
		
		//Se manda complementar con query de tuiio
		sbQueryRC.append(getQryGenTuiio(strFeDesde, strFeHasta));
		
		//Se regresa query a ejecutar
		return sbQueryRC;
	}

	/**
	 * Metodo que genera query para reporte de TUIIO Tecnico
	 * @param strFeDes fecha desde para busqueda
	 * @param strFeHas fecha hasta para busqueda
	 * @return cadena con query
	 */
	public static StringBuilder getQryReporteTecnico(String strFeDes, String strFeHas) {
		//Se inicializa variable para query
		StringBuilder sbQueryRT = new StringBuilder();
		
		//Se va armando query
		sbQueryRT.append("SELECT TO_CHAR(COCE_FE_SUSCRIPCION, '").append(Constantes.FORMATO_FECHA_UNO).append(PARENTESIS_CLOSE);
		sbQueryRT.append("TO_CHAR(COCE_FE_INI_CREDITO, '").append(Constantes.FORMATO_FECHA_UNO).append(PARENTESIS_CLOSE);
		sbQueryRT.append("TO_CHAR(COCE_FE_FIN_CREDITO, '").append(Constantes.FORMATO_FECHA_UNO).append(PARENTESIS_CLOSE);
		sbQueryRT.append("COCE_CASU_CD_SUCURSAL, COCE_CARP_CD_RAMO, COCE_NU_CERTIFICADO, COCE_NU_CREDITO, \n");
		sbQueryRT.append("COCE_CAPO_NU_POLIZA, COCE_MT_PRIMA_SUBSECUENTE, COCE_MT_PRIMA_SUBSECUENTE, COCN_BUC_CLIENTE, \n");
		sbQueryRT.append("COCN_NOMBRE, COCN_APELLIDO_PAT, COCN_APELLIDO_MAT,  COCN_RFC, COCN_CALLE_NUM, \n");
		sbQueryRT.append("COCN_COLONIA, COCN_CD_POSTAL, \n");
		sbQueryRT.append("TO_CHAR(COCN_FE_NACIMIENTO, '").append(Constantes.FORMATO_FECHA_UNO).append(PARENTESIS_CLOSE);
		sbQueryRT.append("COCN_CORREO_ELECT \n");
		
		//Se manda complementar con query de tuiio
		sbQueryRT.append(getQryGenTuiio(strFeDes, strFeHas));
		
		//Se regresa query a ejecutar
		return sbQueryRT;
	}
	
	/**
	 * Metodo nenerico para consulta de reportes TUIIO
	 * @param strFeDesde fecha desde para busqueda
	 * @param strFeHasta fecha hasta para busqueda
	 * @return cadena con query
	 */
	private static Object getQryGenTuiio(String strFeDesde, String strFeHasta) {
		//Se inicializa variable para query
		StringBuilder sbQueryGT = new StringBuilder();
		
		//Se va armando query
		sbQueryGT.append("FROM COLECTIVOS_CERTIFICADOS \n");
		sbQueryGT.append("INNER JOIN COLECTIVOS_CLIENTE_CERTIF ON(COCC_CASU_CD_SUCURSAL = COCE_CASU_CD_SUCURSAL \n");
			sbQueryGT.append("AND COCC_CARP_CD_RAMO = COCE_CARP_CD_RAMO \n");
			sbQueryGT.append("AND COCC_CAPO_NU_POLIZA = COCE_CAPO_NU_POLIZA \n");
			sbQueryGT.append("AND COCC_NU_CERTIFICADO = COCE_NU_CERTIFICADO) \n");
			sbQueryGT.append("INNER JOIN COLECTIVOS_CLIENTES ON(COCN_NU_CLIENTE = COCC_NU_CLIENTE ) \n");
		sbQueryGT.append("WHERE COCE_CASU_CD_SUCURSAL = 1 \n");
			sbQueryGT.append("AND COCE_CARP_CD_RAMO = 61 \n");
			sbQueryGT.append("AND COCE_CAPO_NU_POLIZA BETWEEN 400000000 AND 499999999 \n");
			sbQueryGT.append("AND COCE_NU_CERTIFICADO > 0 \n");
			sbQueryGT.append("AND COCE_FE_SUSCRIPCION BETWEEN TO_DATE('").append(strFeDesde).append("', 'dd/MM/yyyy') AND TO_DATE('").append(strFeHasta).append("', 'dd/MM/yyyy') \n");
		
		//Se regresa query a ejecutar
		return sbQueryGT;
	}
	
	/**
	 * Metodo que genera consulta para tarifas
	 * @param ramo ramo para filtro
	 * @param idventa id venta para filtro
	 * @param producto producto para filtro
	 * @return cadena con query
	 */
	public static StringBuilder getQryTarifas(Short ramo, Integer idventa, String producto) {
		//Se inicializa variable para query
		StringBuilder sbQueryT = new StringBuilder();
		
		//Se va armando query
		sbQueryT.append("SELECT DISTINCT HOPP_DATO1, HOPP_PLAN_COLECTIVO  \n");
			sbQueryT.append("FROM HOMOLOGA_PRODPLANES \n"); 
		sbQueryT.append("WHERE HOPP_CD_RAMO = ").append(ramo).append(" \n");
			sbQueryT.append("AND HOPP_PROD_COLECTIVO = ").append(producto).append(" \n");
			sbQueryT.append("AND HOPP_DATO3 = ").append(idventa).append(" \n");
			sbQueryT.append("AND HOPP_DATO1 != '0' \n");
		sbQueryT.append("ORDER BY HOPP_PLAN_COLECTIVO \n");
	
		//Se regresa query a ejecutar
		return sbQueryT;
	}

	/**
	 * Metodo que genera consulta para historial de recibos por poliza
	 * @param ramo ramo para filtro
	 * @param poliza poliza para filtro
	 * @param certificado para filtro
	 * @param recibo recibo para filtro
	 * @param anio anio para filtro
	 * @return cadena con query
	 */
	public static StringBuilder getQryHistRecPoliza(byte ramo, int poliza, int certificado, BigDecimal recibo, Long anio) {
		//Se inicializa variable para query
		StringBuilder sbQueryH = new StringBuilder();
		
		//Se va armando query
		sbQueryH.append("SELECT CORE_NU_RECIBO, CORE_NU_CONSECUTIVO_CUOTA, CORE_FE_EMISION, CORE_FE_DESDE, CORE_FE_HASTA, CORE_ST_RECIBO||'-'||ESSIS.RV_MEANING, NVL(CORE_TP_TRAMITE, 1) ||'-'||ESCOB.RV_MEANING, CORE_MT_PRIMA \n");
			sbQueryH.append("FROM COLECTIVOS_RECIBOS CR  \n");
			sbQueryH.append("LEFT JOIN CG_REF_CODES ESSIS ON(ESSIS.RV_DOMAIN = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO' AND ESSIS.RV_LOW_VALUE = CORE_ST_RECIBO) \n");  
			sbQueryH.append("LEFT JOIN CG_REF_CODES ESCOB ON(ESCOB.RV_DOMAIN = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO' AND ESCOB.RV_LOW_VALUE = NVL(CORE_TP_TRAMITE, 1)) \n"); 
		sbQueryH.append("WHERE CORE_CASU_CD_SUCURSAL = 1 \n");
			sbQueryH.append("AND CORE_CARP_CD_RAMO = ").append(ramo).append(" \n");
			sbQueryH.append("AND CORE_CAPO_NU_POLIZA = ").append(poliza).append(" \n"); 
			sbQueryH.append("AND CORE_CACE_NU_CERTIFICADO = ").append(certificado).append(" \n");

			//Se valida se haya seleccionado un recibo
			if(recibo.intValue() > Constantes.DEFAULT_INT) {
				sbQueryH.append("AND CORE_NU_RECIBO = ").append(recibo).append(" \n");
			}
			
			//Se termina el armado del query			
			sbQueryH.append("AND EXTRACT(YEAR FROM CR.CORE_FE_EMISION) = ").append(anio).append(" \n");
			sbQueryH.append("AND CR.CORE_ST_RECIBO   IN(4, 5)  \n");
		sbQueryH.append("ORDER BY CORE_FE_DESDE DESC  \n");
		
		//Se regresa query a ejecutar
		return sbQueryH;
	}

	/**
	 * Metodo que genera consulta para ver certificados para cambio de credito
	 * @param credito numero de credito
	 * @return cadena con query
	 */
	public static StringBuilder getQryCambioCredito(String credito) {
		//Se inicializa variable para query
		StringBuilder sbQueryCC = new StringBuilder();
		
		//Se va armando query
		sbQueryCC.append("SELECT COCE_CASU_CD_SUCURSAL, COCE_CARP_CD_RAMO, COCE_CAPO_NU_POLIZA, COCE_NU_CERTIFICADO, COCC_ID_CERTIFICADO, NVL(COCE_BUC_EMPRESA, ' ') COCE_BUC_EMPRESA \n");
			sbQueryCC.append("FROM COLECTIVOS_CLIENTE_CERTIF \n");
			sbQueryCC.append("INNER JOIN COLECTIVOS_CERTIFICADOS ON(COCE_CASU_CD_SUCURSAL = COCC_CASU_CD_SUCURSAL \n");
			sbQueryCC.append("AND COCE_CARP_CD_RAMO = COCC_CARP_CD_RAMO \n");  
			sbQueryCC.append("AND COCE_CAPO_NU_POLIZA = COCC_CAPO_NU_POLIZA \n"); 
			sbQueryCC.append("AND COCE_NU_CERTIFICADO = COCC_NU_CERTIFICADO) \n");
		sbQueryCC.append("WHERE COCC_ID_CERTIFICADO = '").append(credito).append("' \n");
			sbQueryCC.append("AND NVL(COCC_TP_CLIENTE, 1) = 1 \n"); 
		
		//Se regresa query a ejecutar
		return sbQueryCC;
		
	}

}
