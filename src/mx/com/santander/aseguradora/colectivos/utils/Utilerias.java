/**
 * 
 */
package mx.com.santander.aseguradora.colectivos.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import WSTimbrado.EnviaTimbrado;

/**
 * @author Sergio Plata
 *
 */
public class Utilerias {
	private static StringTokenizer tokens;
	
	/**
	 * Metodo encargado de dividir una cadena en subcadenas.
	 * @param cadena parametro el cual contiene todas la subcadenas
	 * @param separador parametro por el cual se va a dividir la cadena
	 * @return lista de elementos.
	 */
	public static List<String> obtenItems(String cadena, String separador) {
		tokens = new StringTokenizer(cadena, separador);
		List<String> lista = new ArrayList<String>();
		
		while(tokens.hasMoreTokens()) {
			lista.add(tokens.nextToken());
		}
		
		return lista;
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> getValueList(Map data) {
		List<T> lista = new LinkedList<T>();
		Collection<T> values = data.values();
		for(Iterator<T> item = values.iterator(); item.hasNext();){
			lista.add(item.next());
		}

		return lista;
	}
	
	/**
	 * 
	 * @param message
	 */
	public static void resetMessage(StringBuilder message){		
		try{
			message.delete(0, message.length());
		}catch(NullPointerException e){
			message = new StringBuilder();
		}
	}
	
	public static String getDescEstado(String strDescEstado) {
		
		if(strDescEstado.equals(Constantes.ESTADO_ARCHIVO_MEXICO)){
			return Constantes.ESTADO_BD_MEXICO;
		} else if(strDescEstado.equals(Constantes.ESTADO_ARCHIVO_BC_NORTE)){
			return Constantes.ESTADO_BD_BC_NORTE;
		} else {
			return strDescEstado;
		}
		
	}

	public static String getVentaOrigen(String strDescripcion, String strSigno, String strConsecutivo) {
		String[] arrDesc = strDescripcion.split(" ");
		String strVentaOrigen = "100";
		
		if(arrDesc[0].equals("CRE")) {
			if(strSigno.equals("+")) {
				strVentaOrigen = Constantes.CUENTAS_ORIGEN_ALTAMIRA_ALTA;	
			} else {
				strVentaOrigen = Constantes.CUENTAS_ORIGEN_ALTAMIRA_BAJA;
			}			
		} else if(arrDesc[0].equals("ANU")) {
			strVentaOrigen = Constantes.CUENTAS_ORIGEN_ALTAMIRA_BAJA;
		} else if(arrDesc[0].substring(0,4).equals("0014")) {  
			if(strSigno.equals("+")) {
				strVentaOrigen = Constantes.CUENTAS_ORIGEN_ALTAMIRA_ALTA;	
			} else {
				strVentaOrigen = Constantes.CUENTAS_ORIGEN_ALTAMIRA_BAJA;
			}
		} else if(arrDesc[0].equals("")) {
			strVentaOrigen = Constantes.CUENTAS_ORIGEN_ALTAIR;
		} else if(arrDesc.length == 1 && arrDesc[0].length() == 15) {
			strVentaOrigen = Constantes.CUENTAS_ORIGEN_DESEMPLEO;
			strConsecutivo = arrDesc[0].substring(0,2);
		} else if(isNumeric(arrDesc[0].trim())) {
			strVentaOrigen = Constantes.CUENTAS_ORIGEN_DESCONOCIDO;
		}
		
		if(strConsecutivo.length() < 6){
			for(int i = strConsecutivo.length(); i < 6; i++){
				strConsecutivo = "0" + strConsecutivo;
			}
		}	
	
		strVentaOrigen = strConsecutivo + "-" + strVentaOrigen;
		return strVentaOrigen;
	}
	
	public static String generaFormatoHora(String strHora) {
		String[] arrHora = strHora.split("");
		if(arrHora.length == 5) {		
			return arrHora[1] + arrHora[2] + ":" + arrHora[3] + arrHora[4];
		} else {
			return arrHora[0] + arrHora[1] + ":" + arrHora[2] + arrHora[3];
		}		
	}

  	public static boolean isNumeric(String cadena){
		int i = 0;
		
		try {
			if(cadena == null || cadena.equals("")){
				return false;
			}
			
			for(i = 0; i < cadena.length(); i++){
				Integer.parseInt(cadena.substring(i,i+1));
			}
			
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
  	
  	public static String quitarEspacios(String strCadena) {
  		return strCadena.trim();
  	}
  	
  	/**
  	 * 
  	 * @param arlRamos
  	 * @param producto
  	 * @param subproducto
  	 * @param tarifa
  	 * @param nDatoRamos
  	 * @return
  	 * 		Arreglo de objetos:
  	 * 			Posicion 0 - Array con datos de tarifas para VIDA
  	 * 			Posicion 1 - Array con datos de tarifas para DESEMPLEO
  	 * 			Posicion 2 - Bandera que nos indica si encontro tarifa.
  	 * 			Posicion 3 - Bandera que nos indica si se inserta Desempleo.
  	 * @throws Exception
  	 */
  	public static ArrayList<Object> getTarifas(ArrayList<Object> arlRamos, String producto, String subproducto, Double tarifa, int nDatoRamos) throws Exception {
  		ArrayList<Object> arlResultados;
  		Object[] arrDatosRamos, arrDatosRamosVida, arrDatosRamosDes;
		String[] arrComidin;
  		boolean flgInsertaDes = false, flgEncontroTarifa = false;
  		Iterator<Object> itRamos;
  		
  		try {
  			arlResultados = new ArrayList<Object>();
  			
  			if(!arlRamos.isEmpty()){
	  			arrDatosRamosVida = null;
				arrDatosRamosDes = null;
				itRamos = arlRamos.iterator();
				while(itRamos.hasNext()){
					arrDatosRamos = (Object[]) itRamos.next();
					flgInsertaDes = false;
					flgEncontroTarifa = false;
					if(arrDatosRamos[6] != null && arrDatosRamos[7].equals(producto)){
						arrComidin = arrDatosRamos[6].toString().split("\\|");
						for(int i = 0; arrComidin.length > i; i++){
							if((Math.rint(new Double(arrComidin[i])*100)/100) == (Math.rint(tarifa * 100) / 100)){
								if(arrDatosRamos[4].toString().equals(Constantes.PRODUCTO_VIDA)){
									arrDatosRamosVida = arrDatosRamos;
								} else if(arrDatosRamos[4].toString().equals(Constantes.PRODUCTO_DESEMPLEO) || arrDatosRamos[4].toString().equals(Constantes.PRODUCTO_GAP)){								
									arrDatosRamosDes = arrDatosRamos;
								}
								
								if(arrDatosRamosVida != null && arrDatosRamosDes != null){
									flgEncontroTarifa = true;
									flgInsertaDes = true;
								}									
								break;
							}								
						}
					}
					
					if(flgEncontroTarifa){
						break;
					} else {
						if((Math.rint(new Double(arrDatosRamos[2].toString())*100)/100) == (Math.rint(tarifa * 100) / 100) 
								&& (arrDatosRamos[4].toString().equals(Constantes.PRODUCTO_VIDA) || arrDatosRamos[4].toString().equals(Constantes.PRODUCTO_DESEMPLEO))
								&& arrDatosRamos[7].equals(producto)){
							
							if(nDatoRamos == Constantes.TIPO_VENTA_HPU || //HPU
								nDatoRamos == Constantes.TIPO_VENTA_BT || //LCI
								nDatoRamos == Constantes.TIPO_VENTA_LEX) { //LE 
								if(arrDatosRamos[3].equals(subproducto.trim())){
									arrDatosRamosVida = arrDatosRamos;
									flgEncontroTarifa = true;
									break;
								}
							} else {
								arrDatosRamosVida = arrDatosRamos;
								flgEncontroTarifa = true;
								break;
							}
							
						}
					}
				}
				
				arlResultados.add(Constantes.TARIFAS_ARRAY_VIDA, arrDatosRamosVida);
				arlResultados.add(Constantes.TARIFAS_ARRAY_DESEMPELO, arrDatosRamosDes);
				arlResultados.add(Constantes.TARIFAS_FLAG_ENCONTRO_TARIFA, flgEncontroTarifa);
				arlResultados.add(Constantes.TARIFAS_FLAG_INSERTA_DES, flgInsertaDes);
  			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("ERROR:: Utilerias.getTarifas() " + e.getMessage());
		}
  		
		return arlResultados;
  	}
  	
  	public static String comprimirArchivos(List<String> archivos) throws IOException {
		FileInputStream in = null;
		FileOutputStream out = null;
		byte temp[] = new byte[2048];
		ZipOutputStream zip = null;
		ZipEntry entry = null;
		int len = 0;
		String salida = null;
		String rutaTemporal = FacesUtils.getServletContext().getRealPath("/WEB-INF/reportes/") + File.separator;
		
		try {
			if(archivos != null && archivos.size() > 0){
				salida = "reportesEndosos.zip";
				out = new FileOutputStream(rutaTemporal + salida);
				zip = new ZipOutputStream(out);
				
				for(String archivo: archivos){
					in = new FileInputStream(rutaTemporal + archivo);
					entry = new ZipEntry(archivo);
					zip.putNextEntry(entry);
					len = 0;
					while((len = in.read(temp)) != -1){
						zip.write(temp, 0 , len);
					}
				}
				zip.closeEntry();
				zip.close();
			} else {
				salida = null;
			}
			
			return salida;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null){
				in.close();
			}
			if(out != null){
				out.close();
			}
		}
		
		return null;
	}
  	
  	/**
	 * Metodo que agrega caracteres a una cadena de texto, a la Izquierda o a la Derecha de la cadena de Texto 
	 * 
	 * @param strCadena: 
	 * 		Objeto String, Cadena de texto a la que se le agregaran los caracteres.
	 * @param nCantidad: 
	 * 		Objeto int, Cantidad de caracteres a agregar.
	 * @param caracter
	 * 		Objeto Character, caracter que se va agregar a la cadena.
	 * @param nOpcion
	 * 		Objeto int, nos indica si se va agregar el caracter a la izquierda o derecha. 0 - Derecha y 1 - Izquierda. 
	 * 
	 * @return: 
	 */
	public static String agregarCaracteres(String strCadena, int nCantidad, Character caracter, int nOpcion) {
		int nTama�o = strCadena.length();
		
		for(int i = nTama�o; i <= nCantidad; i++){
			if(nOpcion == 1){
				strCadena = caracter + strCadena;
			} else {
				strCadena = strCadena + caracter;
			}
		}
		
		return strCadena;
	}
	
	public static String quitarCerosIzq(String strCadena) {
		Long nCadena = new Long(strCadena);
		return String.valueOf(nCadena);
	}
	
	public static String crearCantidad(String strCadena, int decimales) {
		Double nCadena = 0d;
		switch (decimales) {
			case 1:
				nCadena = new Double(strCadena) / 10;
				break;
			case 2:
				nCadena = new Double(strCadena) / 100;
				break;
			case 3:
				nCadena = new Double(strCadena) / 1000;
				break;
		}
		return String.valueOf(nCadena);
		
  	}
	
	public static String separCampos(String valor) {
		String strResultado = "";
		String[] arrCampos;
		int totalCampos, contCampos = 0;
		
		if(valor.contains("~")){
			arrCampos = valor.split("~");
			totalCampos = arrCampos.length;
			for(int i = 0; arrCampos.length > i; i++){
				contCampos++;
				if(totalCampos == contCampos){
					strResultado = strResultado + arrCampos[i];
				} else {
//					strResultado = strResultado + arrCampos[i] + ", tabla.";
					strResultado = strResultado + arrCampos[i] + " || ' ' || tabla.";
				}
				
			}
		} else {
			 strResultado = valor;
		}
		
		return strResultado;
	}

	public static EnviaTimbrado llamarWSTimbrado( byte[] buffer, String usuarioWS, String passwordWS, String urlWS, String proxy, String puertoProxy) throws Exception {
		EnviaTimbrado timbrado = null;
		
		try {
			System.getProperties().put("https.proxyHost", proxy);
			System.getProperties().put("https.proxyPort", puertoProxy);

            timbrado = new EnviaTimbrado(buffer, usuarioWS, passwordWS, urlWS, true);
            if(timbrado.isTimbradoOK()) {
            	System.out.println("Timbrado con EXITO");
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return timbrado;
	}
	
	public static Double redondear(double nValor, String decimales) {
		Double nResultado = null;
		String strRedondeo;
		
		try {
			strRedondeo = String.format("%." + decimales + "f", nValor);
			nResultado = Double.valueOf(strRedondeo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return nResultado;
	}
	
	/*
	 * Metodo encargado de validar s�lo que el RFC no contenga mas de dos XX
	 */
	public static boolean validaRFC(String strRfc) {
		boolean blnRfcValido = false;
		
		strRfc = strRfc.toUpperCase().trim();
		if(strRfc.length() == 10) {
			blnRfcValido = strRfc.toUpperCase().matches("[A-Z]{4}[0-9]{6}");
		} else if(strRfc.length() == 13) {
			blnRfcValido = strRfc.toUpperCase().matches("[A-Z]{4}[0-9]{6}[A-Z0-9]{3}");
		} 
				
		return blnRfcValido;
	}

	public static Double calcularPrima(String strCapital, String strTarifa, int nConstante) {
		Double nPrima;
		nPrima = (new Double(strCapital) * new Double(strTarifa)) / nConstante; 
		return redondear(nPrima, "2");
  	}

	/**
	 * Metodo que separa una cadena (Nombre Completo) en Nombre, ApPaterno y ApMaterno. 
	 * Obligatorio que strNombreCompelto empiece con Nombre, ApPaterno, ApMaterno  
	 * Ej: Maria de los Angeles Guadalupe Cisneros De La O
	 *  Nombre: Maria de los Angeles Guadalupe
	 *  ApPaterno: Cisneros
	 *  ApMaterno: De La O
	 * @param strNombreCompleto
	 * @return ApPaterno|ApMaterno|Nombre
	 */
	public static String separarNombreCompleto2(String strNombreCompleto) {
		String strNombreSeparado = "X|X|X";
		String strNombre = "X", strApPaterno = "X", strApMaterno = "X";
		String[] arrDatos;
		
		try {
			arrDatos = strNombreCompleto.toUpperCase().split("\\ ");
			if(arrDatos.length == 2) { // Nombre y ApPaterno
				strNombre = arrDatos[0];
				strApPaterno = arrDatos[1];
				strApMaterno = "X";
			} else if(arrDatos.length == 3) { // Nombre, ApPaterno y ApMaterno
				strNombre = arrDatos[0];
				strApPaterno = arrDatos[1];
				strApMaterno = arrDatos[2];
			} else {				
				for (int i = arrDatos.length; i > 0; i--) {
					if(i == arrDatos.length) {
						strApMaterno = arrDatos[i-1];
					} else {
						if(strNombre.equals("X")) {
							if(esArticulo(arrDatos[i-1])) {
								if(strApPaterno.equals("X")) {
									strApMaterno = arrDatos[i-1] + " " + strApMaterno;
								} else {
									strApMaterno = arrDatos[i-1] + " " + strApPaterno;
								}
							} else {
								if(strApPaterno.equals("X")) {
									strApPaterno = arrDatos[i-1];
								} else {
									strNombre = arrDatos[i-1];
								}
							}
						} else {
							strNombre = arrDatos[i-1] + " " + strNombre;
						}						
					}
				}
			}
			
			strNombreSeparado = strApPaterno + "/" + strApMaterno + "/" + strNombre;
//			System.out.println("Nombre Separado: " + strNombreSeparado + ", Nombre Completo: " + strNombreCompleto);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return strNombreSeparado;
	}
	
	public static String separarNombreCompleto(String strNombreCompleto) {
		String strNombreSeparado = "X|X|X";
		String strNombre = "X", strApPaterno = "X", strApMaterno = "X";
		String[] arrDatos;
		
		try {
			arrDatos = strNombreCompleto.toUpperCase().split("\\ ");
			if(arrDatos.length == 2) { // Nombre y ApPaterno
				strNombre = arrDatos[0];
				strApPaterno = arrDatos[1];
				strApMaterno = "X";
			} else if(arrDatos.length == 3) { // Nombre, ApPaterno y ApMaterno
				strNombre = arrDatos[0];
				strApPaterno = arrDatos[1];
				strApMaterno = arrDatos[2];
			} else {				
				for (int i = arrDatos.length; i > 0; i--) {
					if(i == arrDatos.length) {
						strApMaterno = arrDatos[i-1];
					} else {
						if(strNombre.equals("X")) {
							if(esArticulo(arrDatos[i-1])) {
								if(strApPaterno.equals("X")) {
									strApMaterno = arrDatos[i-1] + " " + strApMaterno;
								} else {
									strApMaterno = arrDatos[i-1] + " " + strApPaterno;
								}
							} else {
								if(strApPaterno.equals("X")) {
									strApPaterno = arrDatos[i-1];
								} else {
									strNombre = arrDatos[i-1];
								}
							}
						} else {
							strNombre = arrDatos[i-1] + " " + strNombre;
						}						
					}
				}
			}
			
			strNombreSeparado = strApPaterno + "|" + strApMaterno + "|" + strNombre;
			System.out.println("Nombre Separado: " + strNombreSeparado + ", Nombre Completo: " + strNombreCompleto);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return strNombreSeparado;
	}
	
	private static boolean esArticulo(String strCadena) {
		boolean blnResultado = false;
		String arrArticulos[] = {"de", "la", "del", "las", "los", "mac", "mc", "van", "von", "y", "i", "san", "santa", "den", "der"};
		
		try {
			for (int i = 0; i < arrArticulos.length; i++) {
				if(strCadena.trim().equals(arrArticulos[i].toUpperCase())) {
					blnResultado = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return blnResultado;
	}

	public static String encriptar(String strCadena) {
		String strEncriptado = Constantes.DEFAULT_STRING;
		MessageDigest md;
		byte[] arrEncriptable;
		byte[] arrEncriptado;
		
		try {
			md = MessageDigest.getInstance("SHA1");
			arrEncriptable = strCadena.getBytes();
			md.update(arrEncriptable);
			arrEncriptado = md.digest();
			
			for (byte aux : arrEncriptado) {
				int b = aux & 0xff;
				if(Integer.toHexString(b).length() == 1){
					strEncriptado += Constantes.DEFAULT_STRING_ID;
				}
				strEncriptado += Integer.toHexString(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return strEncriptado;
	}
	
	public static void main(String[] args) {
		
		try {
			
			System.out.println(agregarCaracteres("61", 3, '0', 1));
			System.out.println(agregarCaracteres("5", 3, '0', 1));
			System.out.println(agregarCaracteres("120", 3, '0', 1));
			
//			generaFormatoHora("1245");
//			System.out.println(customFormat("##%", new Double(50)));
//			System.out.println(customFormat("###.##", 123456.789));
//			System.out.println(customFormat("000000.000", 123.78));
//			System.out.println(customFormat("$###,###.###", new Double(20000))); 
		      
//			separarNombreCompleto2("ROSARIO HIDALGO ESPINOSA");
//			separarNombreCompleto2("ANTONIO SERNA");
//			separarNombreCompleto2("MARIA EUGENIA GONZALEZ GONZALEZ");
//			separarNombreCompleto2("ARTURO ZAVALETA DE ANDA");
//			separarNombreCompleto2("FLAVIA ESTRELLA DE LA FE ESPINOSA ESPINOZA"); 
//			separarNombreCompleto2("MARIA DE LOS ANGELES GARCIA DE LA TORRE");
//			separarNombreCompleto2("DAMIAN ULISES NUNEZ MATA Y DE LA MORA");
//			separarNombreCompleto2("JOSE GERARDO DEL SAGRADO CORAZ DE ESESARTE MARTIN DEL CAMPO");
//			
//			separarNombreCompleto2("EDUARDO ALVAREZ TOSTADO RODRIGUEZ");
//			separarNombreCompleto2("JESUS ARMANDO GONZALEZ ZAMBRANO");
//			separarNombreCompleto2("JULIO RAFAEL FIGUEROA PEREZ");
//			separarNombreCompleto2("ARACELI VELASCO MEZA");
//			separarNombreCompleto2("JUAN RIGOBERTO NAVARRO GARCIA");
//			separarNombreCompleto2("JORGE ALVA LOZANO");
//			separarNombreCompleto2("FRANCISCO JAVIER FLORES RUBALCAVA");
//			separarNombreCompleto2("LAURA AGUILAR RODRIGUEZ");
//			separarNombreCompleto2("ADOLFO EUGENIO BUSTAMANTE VILDOSOLA");
//			separarNombreCompleto2("ARMANDO REYES RAMOS");
//			separarNombreCompleto2("ANTONIO ROJAS CRUZ");
//			separarNombreCompleto2("MARIA CRISTINA MORENO MERAZ");
//			separarNombreCompleto2("GABRIEL VAZQUEZ RENDON");
//			separarNombreCompleto2("JOSE ENRIQUE RAMOS PEREZ");
//			separarNombreCompleto2("DIANA FRANCISCA SOTELO COLLINS");

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Integer getIdVenta(int idVenta, String strFolio, String strRemesa)  throws Exception{
		Integer nResultado = Constantes.DEFAULT_INT;
		
		try {
			if(idVenta == Constantes.DEFAULT_INT) {
				if(strFolio.startsWith(strRemesa)) { //FolioGenerico
					nResultado = Constantes.DEFAULT_INT;
				} else if(strFolio.startsWith("13") || strFolio.startsWith("12")) { //LCI
					nResultado = Constantes.TIPO_VENTA_BT;
				} else if(strFolio.startsWith("20") || strFolio.startsWith("21")) { //HPU
					nResultado = Constantes.TIPO_VENTA_HPU;
				} else if(strFolio.startsWith("5")) { //PYME
					nResultado = Constantes.TIPO_VENTA_PYM;
				} 
			} else {
				nResultado = idVenta;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return nResultado;
	}
	
	public static String quitarCaracteres(String strCadena, String strCaracter, int nAccion) {
		
		switch (nAccion) {
			case 1: //NORMAL
				return strCadena.replace(strCaracter, Constantes.DEFAULT_STRING);

			case 2: //Quita la primera coincidencia a la Derecha
				String strUltimoCaracter = strCadena.substring(strCadena.length() - strCaracter.length(), strCadena.length());
				if(strUltimoCaracter.equals(strCaracter)) {
					return strCadena.substring(0 , strCadena.length() - strCaracter.length());
				} else {
					return strCadena;
				}
				
				
			case 3: //Quita la primera coincidencia a la Izquierda
				return strCadena.substring(strCaracter.length(), strCadena.length());
				
		}
		
		return "";
	}
	
	public static String customFormat(String pattern, Double value ) {
		String strResultado = Constantes.DEFAULT_STRING;
		DecimalFormat formatter;
	
		try {
			formatter = new DecimalFormat(pattern);
			strResultado = formatter.format(value);
		} catch (Exception e) {
			e.printStackTrace();
			strResultado = value.toString();
		}

		return strResultado;
	}
	
	public static String validaCP(String cp) {
		String strResultado = Constantes.DEFAULT_STRING;
	
		try {
			if(cp == null || cp.equals("") || cp.equals(Constantes.DEFAULT_STRING_ID)){
				strResultado = "50000";
			} else {
				if(isNumeric(cp)){
					strResultado = agregarCaracteres(cp, 4, '0', 1);
				} else {
					strResultado = cp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			strResultado = cp;
		}

		return strResultado;
	}
	
	/**
	 * Reemplaza cadena en stringbuilder
	 * @param sb String builder
	 * @param pattern cadena a buscar
	 * @param replacement reemplazo
	 */
	public static void replaceAll(StringBuilder sb, Pattern pattern, String replacement) {
	    Matcher m = pattern.matcher(sb);
	    int start = 0;
	    while (m.find(start)) {
	        sb.replace(m.start(), m.end(), replacement);
	        start = m.start() + replacement.length();
	    }
	}
	
	/**
	 * Metodo que valida si un objeto es nulo
	 * @param objecto objeto a validar
	 * @param nTipoDato tipo dato del objeto
	 * @return valor por default
	 */
	public static Object objectIsNull(Object objecto, int nTipoDato) {
		Object objResultado = null;
		
		if(objecto == null) {
			switch (nTipoDato) {
				case Constantes.TIPO_DATO_STRING:
					objResultado = Constantes.DEFAULT_STRING;
					break;
				case Constantes.TIPO_DATO_LONG:
					objResultado = Constantes.DEFAULT_LONG;
					break;
				case Constantes.TIPO_DATO_DOUBLE:
					objResultado = Constantes.DEFAULT_DOUBLE;
					break;
				case Constantes.TIPO_DATO_INTEGER:
					objResultado = Constantes.DEFAULT_INT;
					break;
				case Constantes.TIPO_DATO_DATE:
					objResultado = GestorFechas.generateDate("01/01/1900", Constantes.FORMATO_FECHA_UNO);
					break;
				case Constantes.TIPO_DATO_DATE_STRING:
					objResultado = "01/01/1900";
					break;
				case Constantes.TIPO_DATO_BIGDECIMAL:
					objResultado = new BigDecimal(0);
					break;
				default:
					objResultado = Constantes.DEFAULT_STRING_ID;
					break;
			}
		} else {
			if(nTipoDato == Constantes.TIPO_DATO_STRING_ID) {
				objResultado = objecto.toString();
			} else {
				objResultado = objecto;
			}
			
		}
		
		return objResultado;
	}
	
	/**
	 * Metodo que obtiene valor de la cadena porcentajes comision
	 * @param cadena cadena con porcentajes de comision 
	 * @param accion accion de comision (Emision o Renovacion)
	 * @param tipoPor tipo comision a obtner
	 * @return valor de porcentaje comision
	 */
	public static String getPorcentaje(String cadena, int accion, int tipoPor) {
		return cadena.split("\\~")[accion].split("\\|")[tipoPor].trim();
	}
}
