package mx.com.santander.aseguradora.colectivos.utils;

import org.richfaces.component.html.HtmlProgressBar;

public class ProgressBar {

	private Integer progressValue;
	private boolean progressHabilitar;
	private HtmlProgressBar progressBar;
	/**
	 * @return the progressValue
	 */
	public Integer getProgressValue() {
		return progressValue;
	}
	/**
	 * @param progressValue the progressValue to set
	 */
	public void setProgressValue(Integer progressValue) {
		this.progressValue = progressValue;
	}
	/**
	 * @return the progressBar
	 */
	public HtmlProgressBar getProgressBar() {
		return progressBar;
	}
	/**
	 * @param progressBar the progressBar to set
	 */
	public void setProgressBar(HtmlProgressBar progressBar) {
		this.progressBar = progressBar;
	}
	/**
	 * @return the progressHabilitar
	 */
	public boolean isProgressHabilitar() {
		return progressHabilitar;
	}
	/**
	 * @param progressHabilitar the progressHabilitar to set
	 */
	public void setProgressHabilitar(boolean progressHabilitar) {
		this.progressHabilitar = progressHabilitar;
	}
	
}
