package mx.com.santander.aseguradora.colectivos.utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.util.StringUtils;

import mx.com.santander.aseguradora.colectivos.model.bussinessObject.Beneficiarios;

public class BeneficiariosUtil {

	public String setNumBenef(List<Beneficiarios> lstBeneficiarios){
		int cont=0;
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {
			if(beneficiarios.getCobeNombre()!=null && !beneficiarios.getCobeNombre().equals("")){
				cont++;
			}
		}
		
		System.out.println(cont);
		if(cont ==0) {
			return Constantes.DEFAULT_STRING;
		} else {
			return cont+"";
		}
		
	}
	
	public String setNombreBeneficiarios(List<Beneficiarios> lstBeneficiarios){
		
		
		StringBuffer nombreCompleto = null;
		List<String> nombres		= new ArrayList<String>();
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {

			if(beneficiarios.getCobeNombre()!=null && !beneficiarios.getCobeNombre().equals("")){
				
				nombreCompleto = new StringBuffer();
				
				nombreCompleto.append(beneficiarios.getCobeNombre());
				nombreCompleto.append("/");
				nombreCompleto.append(beneficiarios.getCobeApellidoPat());
				nombreCompleto.append("/");
				nombreCompleto.append(beneficiarios.getCobeApellidoMat());
				
				nombres.add(nombreCompleto.toString());
			}
			
		}
		
		System.out.println("NOMBRES: "+StringUtils.collectionToDelimitedString(nombres, "|"));
		
		return StringUtils.collectionToDelimitedString(nombres, "|");
	}

	public String setRelaciones(List<Beneficiarios> lstBeneficiarios){
		
		List<String> relaciones	= new ArrayList<String>();
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {
			if(beneficiarios.getCobeRelacionBenef()!=null && !beneficiarios.getCobeRelacionBenef().equals("0")){
				relaciones.add(beneficiarios.getCobeRelacionBenef().split("-")[0].toString().trim());
			}
		}
		
		System.out.println("RELACIONES:"+StringUtils.collectionToDelimitedString(relaciones,"|"));
		return StringUtils.collectionToDelimitedString(relaciones,"|");
	}
	
	public String setPorcentajeParticipacion(List<Beneficiarios> lstBeneficiarios){

		 List<BigDecimal> porcentaje = new ArrayList<BigDecimal>();
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {
			if(beneficiarios.getCobePoParticipacion()!=null && beneficiarios.getCobePoParticipacion().compareTo(BigDecimal.ZERO)!=0){
				porcentaje.add(beneficiarios.getCobePoParticipacion());
			}
			
		}
		
		System.out.println("PORCENTAJE: "+StringUtils.collectionToDelimitedString(porcentaje,"|"));
		return StringUtils.collectionToDelimitedString(porcentaje,"|");
	}
	
	/*
	 * */
	public String setFechaNacimientoBeneficiario(List<Beneficiarios> lstBeneficiarios){
		List<String> fecNac = new ArrayList<String>();
		DateFormat inputFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		DateFormat dateFormat  = new SimpleDateFormat("dd/MM/yyyy");
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {
			if(beneficiarios.getCobeFeNacimiento()!=null && !beneficiarios.getCobeFeNacimiento().equals("")){
				try {
					fecNac.add(dateFormat.format(inputFormat.parse(beneficiarios.getCobeFeNacimiento().toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		System.out.println("FECNAC: "+StringUtils.collectionToDelimitedString(fecNac,"|"));
		return StringUtils.collectionToDelimitedString(fecNac,"|");
	}
	
	/*
	 * */
	public String setCallesBeneficiarios(List<Beneficiarios> lstBeneficiarios){
		List<String> calle = new ArrayList<String>();
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {
			if(beneficiarios.getCobeVcampo4()!=null && !beneficiarios.getCobeVcampo4().equals("")){
				calle.add(beneficiarios.getCobeVcampo4());
			}
			
		}
		
		System.out.println("CALLE: "+StringUtils.collectionToDelimitedString(calle,"|"));
		return StringUtils.collectionToDelimitedString(calle,"|");
	}
	
	/*
	 * */
	public String setColoniasBeneficiarios(List<Beneficiarios> lstBeneficiarios){
		List<String> colonias = new ArrayList<String>();
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {
			if(beneficiarios.getCobeVcampo3()!=null && !beneficiarios.getCobeVcampo3().equals("0")){
				colonias.add(beneficiarios.getCobeVcampo3());
			}
		}
		
		System.out.println("COLONIA: "+StringUtils.collectionToDelimitedString(colonias,"|"));
		return StringUtils.collectionToDelimitedString(colonias,"|");
	}
	
	/*
	 * */
	public String setDelgMunBeneficiarios(List<Beneficiarios> lstCiudad){
		List<String> municipios = new ArrayList<String>();
		
		for (Beneficiarios beneficiarios : lstCiudad) {
			if(beneficiarios.getCobeVcampo2()!=null && !beneficiarios.getCobeVcampo2().equals("0")){
				municipios.add(beneficiarios.getCobeVcampo2());
			}
		}
		
		System.out.println("DELEGACION MUNICIPIO: "+StringUtils.collectionToDelimitedString(municipios,"|"));
		return StringUtils.collectionToDelimitedString(municipios,"|");
	}
	
	public String setEstadosBeneficiarios(List<Beneficiarios> lstEstado){
		List<String> estados = new ArrayList<String>();
		
		for (Beneficiarios beneficiarios : lstEstado) {
			if(beneficiarios.getCobeCdSexo()!=null && !beneficiarios.getCobeCdSexo().equals("")){
				estados.add(beneficiarios.getCobeCdSexo());
			}
		}
		
		System.out.println("ESTADOS: "+StringUtils.collectionToDelimitedString(estados,"|"));
		return StringUtils.collectionToDelimitedString(estados,"|");
	}
	
	public String setCpBeneficiarios(List<Beneficiarios> lstBeneficiarios)
	{
		List<Long> codigoPostal = new ArrayList<Long>();
		
		for (Beneficiarios beneficiarios : lstBeneficiarios) {
			if(beneficiarios.getCobeNcampo1()!=null && beneficiarios.getCobeNcampo1()!=0){
				codigoPostal.add(beneficiarios.getCobeNcampo1());
			}
		}
		
		System.out.println("CP: "+ StringUtils.collectionToDelimitedString(codigoPostal,"|"));
		return StringUtils.collectionToDelimitedString(codigoPostal,"|");
	}

	
	public static void main(String[] args) {
		BeneficiariosUtil b = new BeneficiariosUtil();
		
		Beneficiarios bene = new Beneficiarios();
		Beneficiarios bene2 = new Beneficiarios();
		
		List <Beneficiarios> lstBeneficiarios = new ArrayList<Beneficiarios>();

		bene.setCobeApellidoPat("PRECIADO");
		bene.setCobeApellidoMat("PRECIADO");
		bene.setCobeNombre("LEOBARDO");
		bene.setCobeRelacionBenef("H");
		
		bene2.setCobeApellidoPat("ABCD");
		bene2.setCobeApellidoMat("UNO");
		bene2.setCobeNombre("VIDALS");
		bene2.setCobeRelacionBenef("A");
		
		lstBeneficiarios.add(bene);
		
		bene = new Beneficiarios();
		bene.setCobeApellidoPat("GOMEZ");
		bene.setCobeApellidoMat("PEREZ");
		bene.setCobeNombre("JOJO");
		bene.setCobeRelacionBenef("D");

		
		lstBeneficiarios.add(bene2);
		lstBeneficiarios.add(bene);
		lstBeneficiarios.add(new Beneficiarios());
		lstBeneficiarios.add(new Beneficiarios());
		lstBeneficiarios.add(new Beneficiarios());

		
		b.setNombreBeneficiarios(lstBeneficiarios);
		System.out.println(b.setRelaciones(lstBeneficiarios));
	}
}
