package mx.com.santander.aseguradora.colectivos.utils;

import java.util.Date;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		28-03-2020
 * Description: Clase que genera cadenas de Querys generales
 * 				Se realizo para quitar codigo en clases 
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	Ing. Issac Bautista
 * 		Cuando:	28-10-2020
 * 		Porque: Correcion a observaciones de Sonar
 * ------------------------------------------------------------------------
 * 
 */
public final class GeneratorQuerys extends GeneratorQuerys2 {
	
	//Variable para no repetir fecha
	//Se puede utilizar para todas las variables de esta clase
	private static String fechaOracle = "', 'dd/MM/yyyy') \n";
	
	//Variable para parte de select fechas poliza PR
	//Se utiliza para ir armando querys tecnicos de PR
	public static final StringBuilder fechasPolizaPR = 
			new StringBuilder(",(select min(re.core_fe_desde)										\n")
				.append("    from colectivos_recibos re												\n")
				.append("    where re.core_casu_cd_sucursal        = r.core_casu_cd_sucursal	\n")
				.append("      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n")
				.append("      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n")
				.append("      and re.core_cace_nu_certificado     = 0						\n")
				.append("      and re.core_st_recibo               in (1,2,4)					\n")
				.append("      and re.core_nu_consecutivo_cuota    = 1						\n")
				.append("      and re.core_fe_desde                <= r.core_fe_desde			\n")
				.append("      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_INICIO_POLIZA	\n")
				.append(",(select add_months(min(re.core_fe_desde),12)						\n")
				.append("    from colectivos_recibos re												\n")
				.append("    where re.core_casu_cd_sucursal      = r.core_casu_cd_sucursal	\n")
				.append("      and re.core_carp_cd_ramo            = r.core_carp_cd_ramo		\n")
				.append("      and re.core_capo_nu_poliza          = r.core_capo_nu_poliza	\n")
				.append("      and re.core_cace_nu_certificado     = 0						\n")
				.append("      and re.core_st_recibo               in (1,2,4)					\n")
				.append("      and re.core_nu_consecutivo_cuota    = 1						\n")
				.append("      and re.core_fe_desde                <= r.core_fe_desde			\n")
				.append("      and add_months(re.core_fe_desde,12) > r.core_fe_desde)	FECHA_FIN_POLIZA	\n");

	//Variable para parte de select fechas poliza PU
	//Se utiliza para ir armando querys tecnicos de PU
	public static final StringBuilder fechasPolizaPU = 
		new StringBuilder("      ,head.coce_fe_desde       FECHA_INICIO_POLIZA 						\n")
			      .append("      ,head.coce_fe_hasta       FECHA_FIN_POLIZA 						\n");
	
	//Variable para parte de select conerturas polizas PU
	//Se utiliza para ir armando querys tecnicos de PU
	public static final StringBuilder polizaCoberPU =
		new StringBuilder("             and cob.cocb_capo_nu_poliza     = cob.cocb_capo_nu_poliza   \n");
	
	//Variable para parte de select conerturas polizas PR
	//Se utiliza para ir armando querys tecnicos de PR
	public static final StringBuilder polizaCoberPR =
		new StringBuilder("             and cob.cocb_capo_nu_poliza     = c.coce_capo_nu_poliza   \n");
	
	//Variable para parte de select conerturas polizas mov PR
	//Se utiliza para ir armando querys tecnicos de PR
	public static final StringBuilder polizaCoberMovPR =
		new StringBuilder("             and cob.cocb_capo_nu_poliza     = c.cocm_capo_nu_poliza   \n");
	
	//Variable para parte de select campos fechas PU
	//Se utiliza para ir armando querys tecnicos de PU
	public static final StringBuilder camposFehasPU =
			new StringBuilder("	c.coce_fe_emision FECHAEMISION, 											\n")
				.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeEn:',1,1) + 5,instr(c.coce_campoV6, '|',1,6)-instr(c.coce_campoV6, 'FeEn:',1,1) -5) FECHAENVIO, 	\n")
				.append("    substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeCar:',1,1) + 6,instr(c.coce_campoV6, '|',1,8)-instr(c.coce_campoV6, 'FeCar:',1,1) -6) FECHACARGA, 	\n")
				.append("	 substr(c.coce_campoV6,instr(c.coce_campoV6, 'FeRe:',1,1) + 5,instr(c.coce_campoV6, '|',1,7)-instr(c.coce_campoV6, 'FeRe:',1,1) -5) FECHARECIBIDO  \n");
	
	//Variable para parte de select campos fechas PR
	//Se utiliza para ir armando querys tecnicos de PR
	public static final StringBuilder camposFehasPR =
		new StringBuilder("	'' FECHAEMISION,'' FECHAENVIO,'' FECHACARGA,''FECHARECIBIDO					\n");

	//Variable para query tecnico de facturacion
	//Se puede utilizar para cualquier parte del
	//sistema, es generica
	//Variable para query reporte tecnico factruacion
	//Esta se utliza para generar query tecnico
	//de facturacion. Es universal
	public static final StringBuilder repTecnicoFacturacion =
		new StringBuilder("SELECT F.*, #cuopend CUOTAS_PEND, (#cuopend * F.PRIMA_NETA) PMA_CUOTAS_PEND, ((#cuopend * F.PRIMA_NETA) + F.PRIMA_NETA) PMATOT_CUOTAS_PEND	\n")
			.append("	FROM (	\n")
			.append("select c.coce_carp_cd_ramo      RAMO 									\n")	
			.append("		,pl.alpl_dato3            CENTRO_COSTOS 						\n")		
			.append("		,c.coce_capo_nu_poliza    POLIZA 								\n")		
			.append("		,c.coce_no_recibo         RECIBO 								\n")		
			.append("		,cc.cocc_id_certificado   CREDITO 								\n")		
			.append("		,c.coce_nu_certificado    CERTIFICADO 							\n")		
			.append("		,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 	\n")		
			.append("		,est.ales_campo1		     ESTATUS   				 			\n")		
			.append("		,'ALTA'                   ESTATUS_MOVIMIENTO 					\n")		
			.append("		,c.coce_nu_cuenta         CUENTA 								\n")		
			.append("		,c.coce_tp_producto_bco   PRODUCTO 								\n")	
			.append("		,c.coce_tp_subprod_bco    SUBPRODUCTO 							\n")		
			.append("		,c.coce_cazb_cd_sucursal  SUCURSAL 								\n")	
			.append("		,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n")	
			.append("		,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n")	
			.append("		,cl.cocn_nombre           NOMBRE 								\n")		
			.append("		,cl.cocn_cd_sexo          SEXO 									\n")	
			.append("		,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n")	
			.append("		,cl.cocn_buc_cliente      NUMERO_CLIENTE 						\n")		
			.append("		,cl.cocn_rfc              RFC 									\n")		
			.append("		,c.coce_cd_plazo          PLAZO 								\n")		
			.append("		#fechasPoliza                                               	\n")
			.append("		,c.coce_fe_suscripcion    FECHA_INGRESO 						\n")		
			.append("		,c.coce_fe_desde          FECHA_DESDE 							\n")		
			.append("		,c.coce_fe_hasta          FECHA_HASTA 							\n")		
			.append("		,c.coce_fe_ini_credito    FECHA_INICIO_CREDITO 					\n")	
			.append("		,c.coce_fe_fin_credito    FECHA_FIN_CREDITO 					\n")		
			.append("		,c.coce_campov6		   FECHA_FIN_CREDITO_2 						\n")	
			.append("		,c.coce_fe_anulacion      FECHA_ANULACION 						\n")		
			.append("		,c.coce_fe_anulacion_col  FECHA_CANCELACION 					\n")		
			.append("		,trim(to_char(decode(c.coce_sub_campana,'7',to_number(c.coce_mt_suma_aseg_si),c.coce_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA 	\n")
			.append("		,decode(c.coce_sub_campana,'7',c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si) BASE_CALCULO	\n") 							
			.append("		,case when pf.copa_nvalor5 = 0	\n") 									 
			.append("					 then nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
			.append("				when pf.copa_nvalor5 = 1	\n")									 
			.append("					  then c.coce_mt_prima_pura		\n")						 
			.append("				else 0 end PRIMA_NETA	\n")									
			.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1) -instr(c.coce_di_cobro1, 'O',1,1)-1)) DERECHOS 	\n")
			.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2) -instr(c.coce_di_cobro1, 'I',1,1)-1)) RECARGOS 	\n") 
			.append("		,to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)) IVA      	\n")
			.append("		,case when pf.copa_nvalor5 = 0 then  	\n")
			.append("					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)	\n")  
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)	\n")  
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)	\n")  
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3) + 1,instr(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) 	\n") 
			.append("				when pf.copa_nvalor5 = 1 then 	\n") 
			.append("					 nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'O',1,1) + 1,instr(c.coce_di_cobro1, '|',1,1)-instr(c.coce_di_cobro1, 'O',1,1) -1)),0)	\n")  
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'I',1,1) + 1,instr(c.coce_di_cobro1, '|',1,2)-instr(c.coce_di_cobro1, 'I',1,1) -1)),0)	\n")  
			.append("				   + nvl(to_number(substr(c.coce_di_cobro1,instr(c.coce_di_cobro1, 'A',1,1) + 1,instr(c.coce_di_cobro1, '|',1,3)-instr(c.coce_di_cobro1, 'A',1,1) -1)),0)	\n")  
			.append("				   + nvl(c.coce_mt_prima_pura,0) 	\n") 
			.append("				else 0 end PRIMA_TOTAL 	\n") 									 			
			.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal, c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
			.append("                                            c.coce_capu_cd_producto,c.coce_capb_cd_plan, c.coce_nu_cobertura, 	\n")
			.append("                                            NVL(c.coce_sub_campana,0), c.coce_mt_suma_asegurada, c.coce_mt_suma_aseg_si,	\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 7, NVL(c.coce_mt_bco_devolucion, 0)) TARIFA 	\n")
			.append("		,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo, c.coce_capo_nu_poliza,	\n")
			.append("                                            c.coce_capu_cd_producto,c.coce_capb_cd_plan,c.coce_nu_cobertura, 	\n")
			.append("                                            NVL(c.coce_sub_campana,0),c.coce_mt_suma_asegurada, c.coce_mt_suma_aseg_si,	\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 1, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_VIDA 	\n")
			.append("		,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,	\n")
			.append("                                            c.coce_capu_cd_producto, c.coce_capb_cd_plan, c.coce_nu_cobertura,	\n") 
			.append("                                            NVL(c.coce_sub_campana,0),c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,	\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 2, NVL(c.coce_mt_bco_devolucion, 0)) PRIMA_DESEMPLEO 	\n")										
			.append("		,cl.cocn_cd_estado        CD_ESTADO 	\n")								
			.append("		,es.caes_de_estado        ESTADO 		\n")								
			.append("		,cl.cocn_delegmunic       MUNICIPIO 	\n")								
			.append("		,cl.cocn_cd_postal        CP 			\n")								
			.append("		,nvl(c.coce_mt_bco_devolucion,0) MONTO_DEVOLUCION 	\n")					
			.append("		,nvl(c.coce_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 	\n")			    
			.append("		,abs( nvl(c.coce_mt_devolucion,0) - nvl(c.coce_mt_bco_devolucion,0) ) DIFERENCIA_DEVOLUCION 	\n")
			.append("		,nvl(c.coce_buc_empresa,0)		  CREDITONUEVO	\n")			     		
			.append("		,c.coce_capu_cd_producto         PRODUCTO_ASEGURADORA   	\n")   
			.append("		,c.coce_capb_cd_plan             PLAN_ASEGURADORA 	\n")         
			.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,\n")
			.append("                                            c.coce_capu_cd_producto, c.coce_capb_cd_plan, c.coce_nu_cobertura,\n")
			.append("                                            NVL(c.coce_sub_campana,0),c.coce_mt_suma_asegurada,c.coce_mt_suma_aseg_si,\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 3, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_BASICA	\n")
			.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.coce_casu_cd_sucursal,c.coce_carp_cd_ramo,c.coce_capo_nu_poliza,	\n")
			.append("                                            c.coce_capu_cd_producto,c.coce_capb_cd_plan,c.coce_nu_cobertura,\n")
			.append("                                            NVL(c.coce_sub_campana, 0), c.coce_mt_suma_asegurada, c.coce_mt_suma_aseg_si,	\n")
			.append("                                            c.coce_mt_prima_pura, c.coce_di_cobro1, pf.copa_nvalor5, 4, NVL(c.coce_mt_bco_devolucion, 0)) CUOTA_DESEMPLEO 	\n")  
			.append("		,case when pf.copa_nvalor4 > 0 then 	\n")                                          
			.append("		   (select cln.cocn_fe_nacimiento   ||'|'||	\n")                                   
			.append("				   cln.cocn_cd_sexo         ||'|'||	\n")                                   
			.append("				   clc.cocc_tp_cliente      ||'|'||	\n")                                   
			.append("				   p.copa_vvalor1           ||'|'||	\n")                                                  
			.append("				   substr(c.coce_empresa,instr(c.coce_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) 	\n")     
			.append("			  from colectivos_cliente_certif clc 	\n")                                                    
			.append("				  ,colectivos_clientes       cln	\n")                                                     
			.append("				  ,colectivos_parametros     p 	\n")                                       
			.append("			 where clc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 	\n")               
			.append("			   and clc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo     	\n")               
			.append("			   and clc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza   	\n")               
			.append("			   and clc.cocc_nu_certificado   = c.coce_nu_certificado   	\n")               
			.append("			   and nvl(clc.cocc_tp_cliente,1)  > 1                     	\n")               
			.append("			   and cln.cocn_nu_cliente       = clc.cocc_nu_cliente     	\n")               
			.append("			   and p.copa_des_parametro(+)   = 'ASEGURADO'             	\n")               
			.append("			   and p.copa_id_parametro(+)    = 'TIPO'                  	\n")               
			.append("			   and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)    	\n")               
			.append("		   else '' end DATOS_OBLIGADO_PU, 	\n")                                            
			.append("		#camposFechas	\n")		
			.append("		, (C.COCE_NU_CUOTA || '/' || C.COCE_CD_PLAZO) CUOTAS 	\n")
			.append("	from colectivos_certificados   c 			\n")								
			.append("		,colectivos_cliente_certif cc 			\n")								
			.append("		,colectivos_clientes       cl 			\n")								
			.append("		,colectivos_facturacion    fac 			\n")							
			.append("		,colectivos_recibos   			r 		\n")								    
			.append("		,cart_estados              es 			\n")								
			.append("		,alterna_estatus           est 			\n")							
			.append("		,colectivos_certificados   head 		\n")								
			.append("		,alterna_planes            pl 			\n")								
			.append("		,colectivos_parametros     pf			\n")								
			.append("where fac.cofa_casu_cd_sucursal  	= #canal	\n") 						
			.append("	and fac.cofa_carp_cd_ramo      	= #ramo 	\n")							
			.append("	#cadenafac	\n")
			.append("	and fac.cofa_fe_facturacion    >= to_date('#fecha1','dd/mm/yyyy')	\n")
			.append("	and fac.cofa_fe_facturacion    <= to_date('#fecha2','dd/mm/yyyy')	\n")
			.append("	and c.coce_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 			\n")	
			.append("	and c.coce_carp_cd_ramo     = fac.cofa_carp_cd_ramo 				\n")	
			.append("	and c.coce_capo_nu_poliza   = fac.cofa_capo_nu_poliza 				\n")	
			.append("	and c.coce_nu_certificado   > 0 									\n")	
			.append("	and c.coce_no_recibo        = fac.cofa_nu_recibo_fiscal 			\n")	
			.append("	and c.coce_campon2          in (0,2008) 				         	\n")    
			.append("	and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal			 	\n")
			.append("	and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal			 	\n")
			.append("	and r.core_st_recibo        in (1,4)								\n")	
			.append("	and cc.cocc_casu_cd_sucursal = c.coce_casu_cd_sucursal 			 	\n")
			.append("	and cc.cocc_carp_cd_ramo     = c.coce_carp_cd_ramo 				 	\n")
			.append("	and cc.cocc_capo_nu_poliza   = c.coce_capo_nu_poliza	 			\n")	
			.append("	and cc.cocc_nu_certificado   = c.coce_nu_certificado 				\n")	
			.append("	and nvl(cc.cocc_tp_cliente,1)       = 1 						 	\n")   
			.append("	and head.coce_casu_cd_sucursal = c.coce_casu_cd_sucursal 			\n")	
			.append("	and head.coce_carp_cd_ramo     = c.coce_carp_cd_ramo 				\n")	
			.append("	and head.coce_capo_nu_poliza   = c.coce_capo_nu_poliza 			 	\n")
			.append("	and head.coce_nu_certificado   = 0 								 	\n")
			.append("	and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 					\n")	
			.append("	and es.caes_cd_estado(+)     = cl.cocn_cd_estado 					\n")	
			.append("	and est.ales_cd_estatus      = c.coce_st_certificado 				\n")	
			.append("	and pl.alpl_cd_ramo          = c.coce_carp_cd_ramo 				 	\n")
			.append("	and pl.alpl_cd_producto      = c.coce_capu_cd_producto 			 	\n")
			.append("	and pl.alpl_cd_plan          = c.coce_capb_cd_plan  				\n")	
			.append("	and pf.copa_des_parametro    = 'POLIZA'							 	\n")
			.append("	and pf.copa_id_parametro     = 'GEPREFAC'							\n")	
			.append("	and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal			\n")	
			.append("	and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 				\n")	
			.append("	and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, fac.cofa_capo_nu_poliza, 1, substr(fac.cofa_capo_nu_poliza,0,3), substr(fac.cofa_capo_nu_poliza,0,2))	\n") 
			.append("	and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0')) 	\n")
			.append("union all 	\n")
			.append("select c.cocm_carp_cd_ramo      RAMO 									\n")	
			.append("		,pl.alpl_dato3            CENTRO_COSTOS 						\n")		
			.append("		,c.cocm_capo_nu_poliza    POLIZA 								\n")		
			.append("		,c.cocm_nu_recibo         RECIBO 								\n")		
			.append("		,cc.cocc_id_certificado   CREDITO 								\n")		
			.append("		,c.cocm_nu_certificado    CERTIFICADO 							\n")		
			.append("		,nvl(pl.alpl_dato2,pl.alpl_de_plan) IDENTIFICACION_DE_CUOTA 	\n")		
			.append("		,est.ales_campo1		     ESTATUS   				 			\n")		
			.append("		,'ALTA'                   ESTATUS_MOVIMIENTO 					\n")		
			.append("		,c.cocm_nu_cuenta         CUENTA 								\n")		
			.append("		,c.cocm_tp_producto_bco   PRODUCTO 								\n")	
			.append("		,c.cocm_tp_subprod_bco    SUBPRODUCTO 							\n")		
			.append("		,c.cocm_cazb_cd_sucursal  SUCURSAL 								\n")	
			.append("		,cl.cocn_apellido_pat     APELLIDO_PATERNO 						\n")	
			.append("		,cl.cocn_apellido_mat     APELLIDO_MATERNO 						\n")	
			.append("		,cl.cocn_nombre           NOMBRE 								\n")		
			.append("		,cl.cocn_cd_sexo          SEXO 									\n")	
			.append("		,cl.cocn_fe_nacimiento    FECHA_NACIMIENTO 						\n")	
			.append("		,cl.cocn_buc_cliente      NUMERO_CLIENTE 						\n")		
			.append("		,cl.cocn_rfc              RFC 									\n")		
			.append("		,c.cocm_cd_plazo          PLAZO 								\n")						
			.append("		#fechasPoliza                                               	\n")
			.append("		,c.cocm_fe_suscripcion    FECHA_INGRESO 						\n")		
			.append("		,c.cocm_fe_desde          FECHA_DESDE 							\n")		
			.append("		,c.cocm_fe_hasta          FECHA_HASTA 							\n")		
			.append("		,c.cocm_fe_ini_credito    FECHA_INICIO_CREDITO 					\n")	
			.append("		,c.cocm_fe_fin_credito    FECHA_FIN_CREDITO 					\n")		
			.append("		,c.cocm_campov6		   FECHA_FIN_CREDITO_2 						\n")	
			.append("		,c.cocm_fe_anulacion_real FECHA_ANULACION 						\n")		
			.append("		,c.cocm_fe_anulacion      FECHA_CANCELACION 					\n")		
			.append("		,trim(to_char(decode(c.cocm_sub_campana,'7',to_number(c.cocm_mt_suma_aseg_si),c.cocm_mt_suma_asegurada),'999999999.99')) SUMA_ASEGURADA	\n") 
			.append("		,decode(c.cocm_sub_campana,'7',c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si) BASE_CALCULO	\n") 							
			.append("		,case when pf.copa_nvalor5 = 0	\n") 									 
			.append("					 then nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0)	\n")  
			.append("				when pf.copa_nvalor5 = 1	\n")									 
			.append("					  then c.cocm_mt_prima_pura		\n")						 
			.append("				else 0 end PRIMA_NETA	\n")									
			.append("		,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1) -instr(c.cocm_di_cobro1, 'O',1,1)-1)) DERECHOS	\n")
			.append("		,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2) -instr(c.cocm_di_cobro1, 'I',1,1)-1)) RECARGOS	\n") 
			.append("		,to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)) IVA     	\n")
			.append("		,case when pf.copa_nvalor5 = 0 then	\n")  
			.append("					 nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0)	\n")
			.append("				   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0)	\n")
			.append("				   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0) 	\n") 
			.append("				   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3) + 1,instr(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'P',1,3)+1),'|',1,1)-1)),0) 	\n") 
			.append("				when pf.copa_nvalor5 = 1 then 	\n") 
			.append("					 nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'O',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,1)-instr(c.cocm_di_cobro1, 'O',1,1) -1)),0)	\n") 
			.append("				   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'I',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,2)-instr(c.cocm_di_cobro1, 'I',1,1) -1)),0)	\n") 
			.append("				   + nvl(to_number(substr(c.cocm_di_cobro1,instr(c.cocm_di_cobro1, 'A',1,1) + 1,instr(c.cocm_di_cobro1, '|',1,3)-instr(c.cocm_di_cobro1, 'A',1,1) -1)),0)	\n") 
			.append("				   + nvl(c.cocm_mt_prima_pura,0)	\n")  
			.append("				else 0 end PRIMA_TOTAL	\n")  									 			
			.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.cocm_casu_cd_sucursal, c.cocm_carp_cd_ramo, c.cocm_capo_nu_poliza,	\n")
			.append("                                            c.cocm_capu_cd_producto,c.cocm_capb_cd_plan, c.cocm_nu_cobertura, 	\n")
			.append("                                            NVL(c.cocm_sub_campana,0), c.cocm_mt_suma_asegurada, c.cocm_mt_suma_aseg_si,	\n")
			.append("                                            c.cocm_mt_prima_pura, c.cocm_di_cobro1, pf.copa_nvalor5, 7, NVL(c.cocm_mt_bco_devolucion, 0)) TARIFA 	\n")
			.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.cocm_casu_cd_sucursal,c.cocm_carp_cd_ramo, c.cocm_capo_nu_poliza,	\n")
			.append("                                            c.cocm_capu_cd_producto,c.cocm_capb_cd_plan,c.cocm_nu_cobertura, 	\n")
			.append("                                            NVL(c.cocm_sub_campana,0),c.cocm_mt_suma_asegurada, c.cocm_mt_suma_aseg_si,	\n")
			.append("                                            c.cocm_mt_prima_pura, c.cocm_di_cobro1, pf.copa_nvalor5, 1, NVL(c.cocm_mt_bco_devolucion, 0)) PRIMA_VIDA 	\n")
			.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.cocm_casu_cd_sucursal,c.cocm_carp_cd_ramo,c.cocm_capo_nu_poliza,	\n")
			.append("                                            c.cocm_capu_cd_producto,c.cocm_capb_cd_plan,c.cocm_nu_cobertura,\n")
			.append("                                            NVL(c.cocm_sub_campana,0),c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si,	\n")
			.append("                                            c.cocm_mt_prima_pura, c.cocm_di_cobro1, pf.copa_nvalor5, 2, NVL(c.cocm_mt_bco_devolucion, 0)) PRIMA_DESEMPLEO 	\n") 										
			.append("		,cl.cocn_cd_estado        CD_ESTADO 	\n")								
			.append("		,es.caes_de_estado        ESTADO 		\n")								
			.append("		,cl.cocn_delegmunic       MUNICIPIO 	\n")								
			.append("		,cl.cocn_cd_postal        CP 			\n")								
			.append("		,nvl(c.cocm_mt_bco_devolucion,0) MONTO_DEVOLUCION 		\n")				
			.append("		,nvl(c.cocm_mt_devolucion,0)     MONTO_DEVOLUCION_SIS 		\n")		    
			.append("		,abs( nvl(c.cocm_mt_devolucion ,0) - nvl(c.cocm_mt_bco_devolucion ,0) ) DIFERENCIA_DEVOLUCION 	\n")
			.append("		,nvl(c.cocm_buc_empresa,0)		  CREDITONUEVO				\n")     		
			.append("		,c.cocm_capu_cd_producto         PRODUCTO_ASEGURADORA   	\n")   
			.append("		,c.cocm_capb_cd_plan             PLAN_ASEGURADORA   	\n")       
			.append("		, COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.cocm_casu_cd_sucursal,c.cocm_carp_cd_ramo,c.cocm_capo_nu_poliza,\n")
			.append("                                            c.cocm_capu_cd_producto, c.cocm_capb_cd_plan, c.cocm_nu_cobertura, \n")
			.append("                                            NVL(c.cocm_sub_campana,0),c.cocm_mt_suma_asegurada,c.cocm_mt_suma_aseg_si,\n")
			.append("                                            c.cocm_mt_prima_pura, c.cocm_di_cobro1, pf.copa_nvalor5, 3, NVL(c.cocm_mt_bco_devolucion, 0)) CUOTA_BASICA	\n")
			.append("		,COLECTIVOS_UTILERIAS.fGetPrimaxCobertura(c.cocm_casu_cd_sucursal, c.cocm_carp_cd_ramo, c.cocm_capo_nu_poliza,	\n")
			.append("                                            c.cocm_capu_cd_producto,c.cocm_capb_cd_plan, c.cocm_nu_cobertura,\n")
			.append("                                            NVL(c.cocm_sub_campana, 0), c.cocm_mt_suma_asegurada, c.cocm_mt_suma_aseg_si,	\n")
			.append("                                            c.cocm_mt_prima_pura, c.cocm_di_cobro1, pf.copa_nvalor5, 4, NVL(c.cocm_mt_bco_devolucion, 0)) CUOTA_DESEMPLEO  	\n")  
			.append("		,case when pf.copa_nvalor4 > 0 then	\n")                                           
			.append("		   (select cln.cocn_fe_nacimiento   ||'|'|| 	\n")                                 
			.append("				   cln.cocn_cd_sexo         ||'|'|| 	\n")                                 
			.append("				   clc.cocc_tp_cliente      ||'|'|| 	\n")                                 
			.append("				   p.copa_vvalor1           ||'|'|| 	\n")                                                
			.append("				   substr(c.cocm_empresa,instr(c.cocm_empresa,'|OBLIGADO CON INGRESOS: ')+24,2) 	\n")     
			.append("			  from colectivos_cliente_certif clc	\n")                                                     
			.append("				  ,colectivos_clientes       cln	\n")                                                     
			.append("				  ,colectivos_parametros     p  	\n")                                      
			.append("			 where clc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal	\n")               
			.append("			   and clc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo    	\n")               
			.append("			   and clc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza  	\n")               
			.append("			   and clc.cocc_nu_certificado   = c.cocm_nu_certificado  	\n")               
			.append("			   and nvl(clc.cocc_tp_cliente,1)  > 1                    	\n")               
			.append("			   and cln.cocn_nu_cliente       = clc.cocc_nu_cliente    	\n")               
			.append("			   and p.copa_des_parametro(+)   = 'ASEGURADO'            	\n")               
			.append("			   and p.copa_id_parametro(+)    = 'TIPO'                 	\n")               
			.append("			   and p.copa_nvalor1(+)         = clc.cocc_tp_cliente)   	\n")               
			.append("		   else '' end DATOS_OBLIGADO_PU, 	\n")                                            
			.append("		#camposFechas	\n")
			.append("		, (C.COCM_NU_CUOTA || '/' || C.COCM_CD_PLAZO) CUOTAS	\n")
			.append("	from colectivos_certificados_mov c 			\n")								
			.append("		,colectivos_cliente_certif cc 			\n")								
			.append("		,colectivos_clientes       cl 			\n")								
			.append("		,colectivos_facturacion    fac 			\n")							
			.append("		,colectivos_recibos   			r 		\n")								    
			.append("		,cart_estados              es 			\n")								
			.append("		,alterna_estatus           est 			\n")							
			.append("		,colectivos_certificados   head 		\n")								
			.append("		,alterna_planes            pl 			\n")								
			.append("		,colectivos_parametros     pf			\n")								
			.append("where fac.cofa_casu_cd_sucursal  	= #canal	\n") 						
			.append("	and fac.cofa_carp_cd_ramo      	= #ramo 	\n")							
			.append("	#cadenafac	\n")
			.append("	and fac.cofa_fe_facturacion    >= to_date('#fecha1','dd/mm/yyyy')	\n") 
			.append("	and fac.cofa_fe_facturacion    <= to_date('#fecha2','dd/mm/yyyy')	\n") 
			.append("	and r.core_casu_cd_sucursal = fac.cofa_casu_cd_sucursal				\n")	
			.append("	and r.core_nu_recibo        = fac.cofa_nu_recibo_fiscal				\n")	
			.append("	and r.core_st_recibo        in (1,4)								\n")		
			.append("	and c.cocm_casu_cd_sucursal = fac.cofa_casu_cd_sucursal 			\n")		
			.append("	and c.cocm_carp_cd_ramo     = fac.cofa_carp_cd_ramo 				\n")		
			.append("	and c.cocm_capo_nu_poliza   = fac.cofa_capo_nu_poliza 				\n")		
			.append("	and c.cocm_nu_certificado   > 0 									\n")		
			.append("	and c.cocm_nu_recibo        = fac.cofa_nu_recibo_fiscal 			\n")		
			.append("	and c.cocm_campon2          in (0,2008) 				        	\n")       
			.append("	and cc.cocc_casu_cd_sucursal = c.cocm_casu_cd_sucursal 				\n")	
			.append("	and cc.cocc_carp_cd_ramo     = c.cocm_carp_cd_ramo 					\n")	
			.append("	and cc.cocc_capo_nu_poliza   = c.cocm_capo_nu_poliza	 			\n")		
			.append("	and cc.cocc_nu_certificado   = c.cocm_nu_certificado 				\n")		
			.append("	and nvl(cc.cocc_tp_cliente,1)       = 1 							\n")	    
			.append("	and head.coce_casu_cd_sucursal = c.cocm_casu_cd_sucursal 			\n")		
			.append("	and head.coce_carp_cd_ramo     = c.cocm_carp_cd_ramo 				\n")		
			.append("	and head.coce_capo_nu_poliza   = c.cocm_capo_nu_poliza 				\n")	
			.append("	and head.coce_nu_certificado   = 0 									\n")	
			.append("	and cl.cocn_nu_cliente       = cc.cocc_nu_cliente 					\n")		
			.append("	and es.caes_cd_estado(+)     = cl.cocn_cd_estado 					\n")		
			.append("	and est.ales_cd_estatus      = c.cocm_st_certificado 				\n")		
			.append("	and pl.alpl_cd_ramo          = c.cocm_carp_cd_ramo 					\n")	
			.append("	and pl.alpl_cd_producto      = c.cocm_capu_cd_producto 				\n")	
			.append("	and pl.alpl_cd_plan          = c.cocm_capb_cd_plan  				\n")		
			.append("	and pf.copa_des_parametro    = 'POLIZA'								\n")	
			.append("	and pf.copa_id_parametro     = 'GEPREFAC'							\n")		
			.append("	and pf.copa_nvalor1          = fac.cofa_casu_cd_sucursal			\n")		
			.append("	and pf.copa_nvalor2          = fac.cofa_carp_cd_ramo 				\n")		
			.append("	and pf.copa_nvalor3          = decode(pf.copa_nvalor4, 0, fac.cofa_capo_nu_poliza, 1, substr(fac.cofa_capo_nu_poliza,0,3), substr(fac.cofa_capo_nu_poliza,0,2)) 	\n")
			.append("	and pf.copa_nvalor6          = to_number(nvl(head.coce_sub_campana,'0'))	\n")
			.append(") F	\n");
	
	/**
	 * Constructor privado de clase
	 */
	private GeneratorQuerys() {
	    throw new IllegalStateException("GeneratorQuerys class");
	}
	
	/**
	 * Metodo que genera query para consulta de recibos ramos 57-58
	 * @param canal canal para realizar filtro
	 * @param ramo ramo para realizar filtro
	 * @param poliza poliza para realizar filtro
	 * @param feDesde fecha desde para realizar filtro
	 * @param feHasta fecha hasta  para realizar filtro
	 * @param noRecibo recibo  para realizar filtro
	 * @return select  para realizar filtro
	 */
	public static StringBuilder getConsultaRecibos5758(short canal, Short ramo, Long poliza, Date feDesde, Date feHasta, Double noRecibo) {
		//Se declaa variable de filtro
		StringBuilder filtro = new StringBuilder();
		
		//Se va llenando variable con query a aplicar
		filtro.append("select cr.care_casu_cd_sucursal   CANAL                          \n");
		filtro.append("      ,cr.care_carp_cd_ramo       RAMO                           \n");
		filtro.append("      ,cr.care_capo_nu_poliza     POLIZA                         \n");
		filtro.append("      ,cr.care_nu_recibo          RECIBO                         \n");
		filtro.append("      ,cr.care_st_recibo          ESTATUS                        \n");
		filtro.append("      ,crc.rv_meaning             DESC_ESTATUS                   \n");
		filtro.append("      ,trunc(cr.care_fe_emision)  EMISION                        \n");
		filtro.append("      ,cr.care_nu_recibo_anterior RECIBO_ANT                     \n");
		filtro.append("      ,nvl(crt.carti_uuid, '')    CARTI_UUID                     \n");
		filtro.append("      ,nvl(crt.carti_st_cancelado, '') CARTI_ST_CANCELADO        \n");
		filtro.append("      ,'' 	ESTATUS_COB        									\n");
		filtro.append("		from cart_recibos  cr                                       \n");		
		filtro.append("		left join gioseg.cart_recibos_timbrados crt on(crt.carti_casu_cd_sucursal = cr.care_casu_cd_sucursal  \n");
		filtro.append(" 			and crt.carti_carp_cd_ramo = cr.care_carp_cd_ramo \n");
		filtro.append(" 			and crt.carti_capo_nu_poliza = cr.care_capo_nu_poliza \n");
		filtro.append(" 			and crt.carti_cace_nu_certificado = cr.care_cace_nu_certificado \n");				
		filtro.append(" 			and crt.carti_nu_recibo = cr.care_nu_recibo) \n");		
		filtro.append("		,cg_ref_codes  crc                                        \n");
		filtro.append("where cr.care_casu_cd_sucursal = ").append(canal).append(" \n");
		filtro.append("		and cr.care_carp_cd_ramo = ").append(ramo).append(" \n");
		filtro.append(" 	and cr.care_capo_nu_poliza = ").append(poliza).append(" \n");

		//Se validan se hayan seleccionado fechas
		if(feDesde != null || feHasta != null) {
			//Aplica filtro por fechas
			filtro.append(" and cr.care_fe_emision >= to_date('").append(GestorFechas.formatDate(feDesde, Constantes.FORMATO_FECHA_UNO)).append(fechaOracle);
			filtro.append(" and cr.care_fe_emision <= to_date('").append(GestorFechas.formatDate(feHasta, Constantes.FORMATO_FECHA_UNO)).append(fechaOracle);
		}			

		//Se valida se haya seleccionado un recibo
		if(noRecibo != null && noRecibo > 0){
			//Aplica filtro por recibo
			filtro.append(" and cr.care_nu_recibo = ").append(noRecibo).append(" \n");
		}
		
		//Termina de generar query
		filtro.append("    and crc.rv_domain            = 'CART_RECIBOS.CARE_ST_RECIBO'\n");
		filtro.append("    and cr.care_st_recibo        = crc.rv_low_value             \n");			
		filtro.append("order by cr.care_fe_emision desc \n");
		
		//Se regresa query
		return filtro;
    }
	
	/**
	 * Metodo que genera query para consulta de recibos para ramos colectivos
	 * @param canal canal para realizar filtro
	 * @param ramo ramo para realizar filtro
	 * @param poliza poliza para realizar filtro
	 * @param idVenta id venta para realizar filtro
	 * @param feDesde fecha desde para realizar filtro
	 * @param feHasta fecha hasta  para realizar filtro
	 * @param noRecibo recibo  para realizar filtro
	 * @return select  para realizar filtro
	 */
	public static StringBuilder getConsultaRecibos(short canal, Short ramo, Long poliza, Integer idVenta, Date feDesde, Date feHasta, Double noRecibo) {
		//Se inicializa variable para query
		StringBuilder sbQuery = new StringBuilder();
		
		//Se va armando query 
		sbQuery.append("SELECT CR.CORE_CASU_CD_SUCURSAL CANAL, CR.CORE_CARP_CD_RAMO RAMO,CR.CORE_CAPO_NU_POLIZA POLIZA, \n");
				sbQuery.append("CR.CORE_NU_RECIBO RECIBO, CR.CORE_ST_RECIBO ESTATUS, CRC.RV_MEANING DESC_ESTATUS, \n");
				sbQuery.append("TRUNC(CR.CORE_FE_EMISION) EMISION, CR.CORE_NU_RECIBO_ANTERIOR RECIBO_ANT, NVL(CRT.CARTI_UUID, '') CARTI_UUID, \n");
				sbQuery.append("NVL(CRT.CARTI_ST_CANCELADO, '') CARTI_ST_CANCELADO, NVL(CR.CORE_TP_TRAMITE, 4) ||' - '|| CRC2.RV_MEANING ESTATUS_COB  \n");		
			sbQuery.append("FROM COLECTIVOS_RECIBOS  CR \n");
			sbQuery.append("LEFT JOIN GIOSEG.CART_RECIBOS_TIMBRADOS CRT ON(CRT.CARTI_CASU_CD_SUCURSAL = CR.CORE_CASU_CD_SUCURSAL \n");
				sbQuery.append("AND CRT.CARTI_CARP_CD_RAMO = CR.CORE_CARP_CD_RAMO \n");
				sbQuery.append("AND CRT.CARTI_CAPO_NU_POLIZA = CR.CORE_CAPO_NU_POLIZA \n");
				sbQuery.append("AND CRT.CARTI_CACE_NU_CERTIFICADO = CR.CORE_CACE_NU_CERTIFICADO \n");				
				sbQuery.append("AND CRT.CARTI_NU_RECIBO = CR.CORE_NU_RECIBO) \n");
			sbQuery.append(",CG_REF_CODES  CRC, CG_REF_CODES CRC2 \n");
		sbQuery.append("WHERE CR.CORE_CASU_CD_SUCURSAL = ").append(canal).append(" \n");
			sbQuery.append("AND CR.CORE_CARP_CD_RAMO = ").append(ramo).append(" \n");
		
		//Se valida se haya seleccionado una poliza
		if(poliza > Constantes.DEFAULT_LONG) {
			//Aplica Filtro por poliza
			sbQuery.append("AND CR.CORE_CAPO_NU_POLIZA = ").append(poliza).append(" \n");
		} else {
			//Aplica Filtro por id venta
			sbQuery.append("AND CR.CORE_CAPO_NU_POLIZA BETWEEN ").append(idVenta.toString() + Constantes.IDVENTA_CEROS).append(" AND ").append(idVenta.toString() + Constantes.IDVENTA_NUEVES).append(" \n");
		}

		//Se valida se hayn seleccionado fechas
		if(feDesde != null && feHasta != null) {
			//Aplica filtro por fechas
			sbQuery.append("AND CR.CORE_FE_EMISION BETWEEN TO_DATE('").append(GestorFechas.formatDate(feDesde, Constantes.FORMATO_FECHA_UNO)).append("', 'dd/MM/yyyy') AND TO_DATE('").append(GestorFechas.formatDate(feHasta, Constantes.FORMATO_FECHA_UNO)).append(fechaOracle);
		}			

		//Se valida se haya seleccionado un recibo
		if(noRecibo != null && noRecibo > 0){
			//Aplica filtro por recibo
			sbQuery.append("AND CR.CORE_NU_RECIBO = ").append(noRecibo).append(" \n");
		}			
		
		//Termina de armar query
			sbQuery.append("AND CRC.RV_DOMAIN		= 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO' \n");
			sbQuery.append("AND CR.CORE_ST_RECIBO   = CRC.RV_LOW_VALUE \n");			
			sbQuery.append("AND CRC2.RV_DOMAIN      = 'COLECTIVOS_RECIBOS.CORE_ST_RECIBO'\n");
			sbQuery.append("AND NVL(CR.CORE_TP_TRAMITE, 4)        = CRC2.RV_LOW_VALUE \n");
		sbQuery.append("ORDER BY CR.CORE_FE_EMISION DESC \n");
		
		//Se regresa query
		return sbQuery;
    }
	
	/**
	 * Metodo que genera cadena para filtro de poliza
	 * @param poliza numero de poliza
	 * @param idVenta numero de IdVenta
	 * @param opcion PU(0) o PR(1)
	 * @return cadena con filtro de poliza
	 */
	public static StringBuilder getCadenaFac(Long poliza, Integer idVenta, Integer opcion) {
		//Se inicializa variable de query
		StringBuilder sbQuery = new StringBuilder();
		
		//Se valida opcion
		if(opcion == Constantes.DEFAULT_INT) {
			//Aplica filtro por id venta
			sbQuery.append("  and fac.cofa_capo_nu_poliza >= ").append(idVenta).append(Constantes.IDVENTA_CEROS + " and fac.cofa_capo_nu_poliza <= ").append(idVenta).append(Constantes.IDVENTA_NUEVES + " \n");
		} else {
			//Aplica filtro por poliza
			sbQuery.append("  and fac.cofa_capo_nu_poliza = ").append(poliza).append(" \n");
		}
		
		//Se regresa query
		return sbQuery;
	}
	
	/**
	 * Metodo que genera cadena para filtro de poliza
	 * @param poliza numero de poliza
	 * @param idVenta numero de IdVenta
	 * @param opcion PU(0) o PR(1)
	 * @return cadena con filtro de poliza
	 */
	public static StringBuilder getCadenaCan(Long poliza, Integer idVenta, Integer opcion) {
		//Se inicializa variable para el query
		StringBuilder sbQuery = new StringBuilder();
		
		//Se valida opcion
		if(opcion == Constantes.DEFAULT_INT) {
			//Aplica filtro por id venta
			sbQuery.append("  and c.coce_capo_nu_poliza >= ").append(idVenta).append(Constantes.IDVENTA_CEROS + " and c.coce_capo_nu_poliza <= ").append(idVenta).append(Constantes.IDVENTA_NUEVES + " \n");
			sbQuery.append("  and c.coce_sub_campana      = '").append(idVenta).append("' \n");
		} else {
			//Aplica filtro por poliza
			sbQuery.append("  and c.coce_capo_nu_poliza = ").append(poliza).append(" \n");
		}
		
		//Se regresa query a ejecutar
		return sbQuery;
		
	}
	
	/**
	 * Metodo que genera consulta para cuotas pendientes
	 * @param canal canal del credito
	 * @param ramo ramo del credito
	 * @param poliza numero de poliza
	 * @return cadena con query
	 */
	public static StringBuilder getConsultaCuotasPendientes(short canal, Short ramo, Long poliza) {
		//Se inicializa variable para query
		StringBuilder sbQuery = new StringBuilder();
		
		//Se va armando el query
		sbQuery.append("select copa_nvalor1, \n");
		sbQuery.append("       COLECTIVOS_UTILERIAS.fGetCuotaPendiente(").append(canal).append(", ").append(ramo).append(", ").append(poliza).append(") \n");
		sbQuery.append("from colectivos_parametros \n");
		sbQuery.append("where copa_des_parametro 	= 'POLIZA' \n");
		sbQuery.append("  and copa_id_parametro 	= 'GEP' \n");
		sbQuery.append("  and copa_nvalor1			= ").append(canal).append(" \n");
		sbQuery.append("  and copa_nvalor2			= ").append(ramo).append(" \n");
		sbQuery.append("  and copa_nvalor3			= ").append(poliza).append(" \n");
		
		//Se regresa query
		return sbQuery;
	}

	/**
	 * Metodo que genera consulta para saber si un certificado esta en proceso de siniestros
	 * @param canal canal del credito
	 * @param ramo ramo del credito
	 * @param poliza numero poliza 
	 * @param certificado numero certi
	 * @param idVenta id de venta
	 * @return cadena con query
	 */
	public static StringBuilder getConsultaSiniestros(short canal, Short ramo, long poliza, long certificado, String idVenta) {
		//Se inicializa variable para query
		StringBuilder sbQuery = new StringBuilder();
		
		//Se va armando query 
		sbQuery.append("SELECT NOTSIN.* FROM ( \n");
			sbQuery.append("SELECT SINO_CASU_CD_SUCURSAL CANSIN, SINO_CARP_CD_RAMO RAMSIN, SINO_NU_NOTIFICACION NUM_NOTSIN, \n");
					sbQuery.append("SINO_CDSUBRAMO_B RAMCOB, SINO_CDCOBERTU_B COB, SCNO_CD_SUCURSAL CANAL, SCNO_CD_RAMO RAMO, \n"); 
					sbQuery.append("SCNO_NU_POLIZA POL, SCNO_NU_CERTIFICADO CER, SINO_FE_NOTIFICACION FE_NOT, 'NOT' TIPO \n");
				sbQuery.append("FROM SINT_NOTIFICACIONES \n");
				sbQuery.append("INNER JOIN SINT_CERTIFICADO_NOTIFICACION ON(SCNO_SINO_CD_SUCURSAL 		= SINO_CASU_CD_SUCURSAL \n"); 
														sbQuery.append("AND SCNO_SINO_CD_RAMO 			= SINO_CARP_CD_RAMO \n"); 
														sbQuery.append("AND SCNO_SINO_NU_NOTIFICACION	= SINO_NU_NOTIFICACION) \n");                                                        
			sbQuery.append("WHERE SINO_CASU_CD_SUCURSAL = ").append(canal).append(" \n");
				sbQuery.append("AND SINO_CARP_CD_RAMO  	= ").append(ramo).append(" \n");
				sbQuery.append("AND SCNO_NU_POLIZA BETWEEN ").append(idVenta).append("00000000 AND ").append(idVenta).append("99999999 \n");
				sbQuery.append("AND SINO_ST_NOTIFICACION = 'P'	--Estatus Pendintes \n");
			sbQuery.append("UNION \n"); 
			sbQuery.append("SELECT SISI_CASU_CD_SUCURSAL, SISI_CARP_CD_RAMO, SISI_NU_SINIESTRO, \n"); 
					sbQuery.append("SINO_CDSUBRAMO_B, SINO_CDCOBERTU_B, SICE_CACE_CASU_CD_SUCURSAL, SICE_CACE_CARP_CD_RAMO, \n"); 
					sbQuery.append("SICE_CACE_CAPO_NU_POLIZA, SICE_CACE_NU_CERTIFICADO, SISI_FE_NOTIFICACION, 'SIN' \n");
				sbQuery.append("FROM SINT_SINIESTROS \n");
				sbQuery.append("INNER JOIN SINT_CERTIFICADOS_SINIESTROS ON(SICE_SISI_CASU_CD_SUCURSAL = SISI_CASU_CD_SUCURSAL \n"); 
														sbQuery.append("AND SICE_SISI_NU_SINIESTRO = SISI_NU_SINIESTRO) \n");
				sbQuery.append("INNER JOIN SINT_NOTIFICACIONES ON(SINO_CASU_CD_SUCURSAL = SISI_CASU_CD_SUCURSAL \n"); 
														sbQuery.append("AND SINO_CARP_CD_RAMO = SISI_SICS_CARP_CD_RAMO \n"); 
														sbQuery.append("AND SINO_NU_NOTIFICACION = SISI_SINO_NU_NOTIFICACION) \n");
			sbQuery.append("WHERE SISI_CASU_CD_SUCURSAL = ").append(canal).append(" \n");
				sbQuery.append("AND SISI_CARP_CD_RAMO 	= ").append(ramo).append(" \n");
				sbQuery.append("AND SICE_CACE_CAPO_NU_POLIZA BETWEEN ").append(idVenta).append("00000000 AND ").append(idVenta).append("99999999 \n");
				sbQuery.append("AND SISI_ST_SINIESTRO IN (10,11,13,24,25,26) --Estatus de en proceso  \n");
		sbQuery.append(") NOTSIN \n");
		sbQuery.append("WHERE NOTSIN.CANAL = ").append(canal).append(" \n");
			sbQuery.append("AND NOTSIN.RAMO = ").append(ramo).append(" \n");
			sbQuery.append("AND NOTSIN.POL = ").append(poliza).append(" \n"); 
			sbQuery.append("AND NOTSIN.CER = ").append(certificado).append(" \n");
	
		//Se regresa query a ejecutar
		return sbQuery;
	}

	/**
	 * Metodo que genera consulta para productos
	 * @param ramo ramo para filtro
	 * @param idventa id venta para filtro
	 * @return cadena con query
	 */
	public static StringBuilder getQryProductos(Short ramo, Integer idventa) {
		//Se incializa variable para query
		StringBuilder sbQuery = new StringBuilder();
		
		//Se va armando el query
		sbQuery.append("SELECT ALPR_CD_PRODUCTO, ALPR_CD_PRODUCTO || ' - ' || ALPR_DE_PRODUCTO \n");
			sbQuery.append("FROM ALTERNA_PRODUCTOS \n"); 
		sbQuery.append("WHERE ALPR_CD_RAMO = ").append(ramo).append(" \n");
			sbQuery.append("AND ALPR_DATO3 = ").append(idventa).append(" \n");
		sbQuery.append("ORDER BY ALPR_CD_PRODUCTO \n");
	
		//Se regresa query a ejecutar
		return sbQuery;
	}
	
}
