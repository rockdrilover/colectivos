package mx.com.santander.aseguradora.colectivos.utils;

import java.math.BigDecimal;

public class Filtros {

	public String plan(Short ramo, Long poliza){
		
		StringBuilder filtro = new StringBuilder();
		
		filtro.append("  and C.id.coceCasuCdSucursal= 1 \n");
		filtro.append("  and C.id.coceCarpCdRamo = ").append(ramo).append("\n");
		filtro.append("  and C.id.coceCapoNuPoliza = ").append(poliza).append("\n");
	    filtro.append("  and C.id.coceNuCertificado = 0 \n" );
		
		return filtro.toString();
	}

	public String maxNumeroDeCredito(Short ramo, Long poliza){
		
		StringBuilder filtro = new StringBuilder();
		
		filtro.append(" and cer.coce_casu_cd_sucursal = 1 ");
		filtro.append(" and cer.coce_carp_cd_ramo     = ").append(ramo); 
		filtro.append(" and cer.coce_capo_nu_poliza   = ").append(poliza);
		
		return filtro.toString();
	}

	public String nextCertificado(BigDecimal certificado, Long poliza){
		
		String newCertif = certificado.toPlainString();
		
		System.out.println(certificado);
		System.out.println(BigDecimal.ONE);
		System.out.println(certificado.compareTo(BigDecimal.ONE));
		
		if(certificado.compareTo(BigDecimal.ONE) == 0){
			newCertif = Utilerias.agregarCaracteres("1"+poliza+"", 11, '0', 0)+"1";
		}
		
		System.out.println(" ----- EL CERTIFICADO ---->:"+newCertif);
		
		return newCertif;
	}
	
	/*
	public String recibosPorPoliza(BeanListaTimbrados obj){
		
		StringBuilder filtro  = new StringBuilder();
		
		filtro.append(" and fac.cofa_casu_cd_sucursal = ").append(obj.getCanal()).append(" \n");
		
		if(obj.getRamo() != null && obj.getRamo().intValue() > 0){
			filtro.append(" and fac.cofa_carp_cd_ramo = ").append(obj.getRamo()).append(" \n");
		}
		
		if(obj.getPoliza() != null && obj.getPoliza().intValue() > 0){
			filtro.append(" and fac.cofa_capo_nu_poliza = ").append(obj.getPoliza()).append(" \n");
		}
		
		if(obj.getIdVenta()>0){
			filtro.append(" and fac.cofa_capo_nu_poliza  between ");
			filtro.append(obj.getIdVenta()).append("00000000 and "); 
			filtro.append(obj.getIdVenta()).append("99999999 ");
		}
		
		if(obj.getFeDesde() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and fac.cofa_fe_facturacion >= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeDesde())).append("' \n");
		}
		
		if(obj.getFeHasta() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and fac.cofa_fe_facturacion <= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeHasta())).append("' \n");
		}
		
		if(obj.getNoRecibo() != null && obj.getNoRecibo() > 0){
			filtro.append(" and fac.cofa_nu_recibo_fiscal = ").append(obj.getNoRecibo()).append(" \n");
		}
		
		return filtro.toString();
	}
	
	public String recibosPorCertificado(BeanListaTimbrados obj){
		
		StringBuilder filtro  = new StringBuilder();
		
		filtro.append(" and F.id.cofaCasuCdSucursal = ").append(obj.getCanal()).append(" \n");
		
		if(obj.getRamo() != null && obj.getRamo().intValue() > 0){
			filtro.append(" and F.id.cofaCarpCdRamo = ").append(obj.getRamo()).append(" \n");
		}
		
		if(obj.getPoliza() != null && obj.getPoliza().intValue() > 0){
			filtro.append(" and F.id.cofaCapoNuPoliza = ").append(obj.getPoliza()).append(" \n");
		}
		
		if(obj.getIdVenta()>0){
			filtro.append(" and F.id.cofaCapoNuPoliza  between ");
			filtro.append(obj.getIdVenta()).append("00000000 and "); 
			filtro.append(obj.getIdVenta()).append("99999999 ");
		}
		
		if(obj.getFeDesde() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and F.cofaFeFacturacion >= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeDesde())).append("' \n");
		}
		
		if(obj.getFeHasta() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and F.cofaFeFacturacion <= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeHasta())).append("' \n");
		}
		
		if(obj.getNoRecibo() != null && obj.getNoRecibo() > 0){
			filtro.append(" and F.cofaNuReciboFiscal = ").append(obj.getNoRecibo()).append(" \n");
		}
		
		return filtro.toString();
	} 

	
	public String recibosPorCertificadoCFDI(BeanListaTimbrados obj){
		
		StringBuilder filtro  = new StringBuilder();
		
		filtro.append(" and F.id.cofcCasuCdSucursal = ").append(obj.getCanal()).append(" \n");
		
		if(obj.getRamo() != null && obj.getRamo().intValue() > 0){
			filtro.append(" and F.id.cofcCarpCdRamo = ").append(obj.getRamo()).append(" \n");
		}
		
		if(obj.getPoliza() != null && obj.getPoliza().intValue() > 0){
			filtro.append(" and F.id.cofcCapoNuPoliza = ").append(obj.getPoliza()).append(" \n");
		}
		
		if(obj.getIdVenta()>0){
			filtro.append(" and F.id.cofcCapoNuPoliza  between ");
			filtro.append(obj.getIdVenta()).append("00000000 and "); 
			filtro.append(obj.getIdVenta()).append("99999999 ");
		}
		
		if(obj.getFeDesde() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and F.cofcFeFacturacion >= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeDesde())).append("' \n");
		}
		
		if(obj.getFeHasta() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and F.cofcFeFacturacion <= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeHasta())).append("' \n");
		}
		
		if(obj.getNoRecibo() != null && obj.getNoRecibo() > 0){
			filtro.append(" and F.cofcNuReciboFiscal = ").append(obj.getNoRecibo()).append(" \n");
		}
		
		return filtro.toString();
	}
	
	public String recibosPorCFDI(BeanListaTimbrados obj){
		
		StringBuilder filtro  = new StringBuilder();
		
		filtro.append(" and fac.cofc_casu_cd_sucursal = ").append(obj.getCanal()).append(" \n");
		
		if(obj.getRamo() != null && obj.getRamo().intValue() > 0){
			filtro.append(" and fac.cofc_carp_cd_ramo = ").append(obj.getRamo()).append(" \n");
		}
		
		if(obj.getPoliza() != null && obj.getPoliza().intValue() > 0){
			filtro.append(" and fac.cofc_capo_nu_poliza = ").append(obj.getPoliza()).append(" \n");
		}
		
		if(obj.getIdVenta()>0){
			filtro.append(" and fac.cofc_capo_nu_poliza  between ");
			filtro.append(obj.getIdVenta()).append("00000000 and "); 
			filtro.append(obj.getIdVenta()).append("99999999 ");
		}
		
		if(obj.getFeDesde() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and fac.cofc_fe_facturacion >= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeDesde())).append("' \n");
		}
		
		if(obj.getFeHasta() != null){ //&& this.feDesde.length() > 0){
			filtro.append(" and fac.cofc_fe_facturacion <= '").append(new SimpleDateFormat("dd/MM/yyyy").format(obj.getFeHasta())).append("' \n");
		}
		
		if(obj.getNoRecibo() != null && obj.getNoRecibo() > 0){
			filtro.append(" and fac.cofc_nu_recibo_fiscal = ").append(obj.getNoRecibo()).append(" \n");
		}
		
		return filtro.toString();
	}*/

	public static void main(String args[]){
		Filtros filtro = new Filtros();
		filtro.nextCertificado(new BigDecimal("2"), 57048146L);
	}
}
