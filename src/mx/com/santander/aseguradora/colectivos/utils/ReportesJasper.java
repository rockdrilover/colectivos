package mx.com.santander.aseguradora.colectivos.utils;

import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * =======================================================================
 * Autor:		Ing. Issac Bautista
 * Fecha:		10-05-2020
 * Description: Clase para la generacion de reportes 
 * 				Se realizo por que la clase Validaciones ya tiene mas de 10 metodos
 * ------------------------------------------------------------------------				
 * Modificaciones
 * ------------------------------------------------------------------------
 * 1.-	Por: 	
 * 		Cuando:
 * 		Porque:
 * ------------------------------------------------------------------------
 * 
 */
public final class ReportesJasper {

	//Variavle para escribir en el Log
	private static final Log LOG = LogFactory.getLog(ReportesJasper.class);
	
	/**
	 * Constructor privado de clase
	 */
	private ReportesJasper() {
		//No se ocupa constructor
	    throw new IllegalStateException("ReportesJasper class");
	}
	
	/**
	 * Metodo que genera un reporte
	 * @param sbQuery query para obtener datos que iran en el reporte
	 * @param nombre nombre del reporte
	 * @param rutaTemporal ruta donde se genera reporte
	 * @param conn conexion de base de datos
	 */
	public static void generaReporte(String sbQuery, String nombre, String rutaTemporal, Connection conn) {
		File reporte = null;
		JasperReport report = null;
		JasperDesign jasperDesign = null;
	    JRSwapFile swapFile = null;
	    JRSwapFileVirtualizer virtualizer = null;
	    JRDesignQuery query = null;
	    JasperPrint print = null;
	    JRCsvExporter exporter = null;
	    Map<String, Object> parametros;
	    
	    try {
	    	reporte = new File(rutaTemporal + "ReporteTecnicoMensual.jrxml");
	    	query = new JRDesignQuery();
	    	
	    	swapFile = new JRSwapFile(rutaTemporal, 512, 512);
		    virtualizer = new JRSwapFileVirtualizer(4, swapFile, true);
		    
		    parametros = new HashMap<String, Object>();
		    parametros.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		    
		    jasperDesign = JRXmlLoader.load(reporte);
			
			query.setText(sbQuery);
			jasperDesign.setQuery(query);

			report = JasperCompileManager.compileReport(jasperDesign);
			
			print = JasperFillManager.fillReport(report, parametros, conn);
	    
		    exporter = new JRCsvExporter();
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print); 
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, rutaTemporal + nombre);
		    exporter.exportReport();
		    virtualizer.cleanup();
		    swapFile.dispose();
		} catch (JRException e) {
			LOG.error("Error al generar reporte: ", e);
		}
	}
	
	/**
	 * Metodo que regresa el query para reporte tecnico a ejecutar
	 * @param chk1 bandera facturacion
	 * @param chk2 bandera cancelacion
	 * @param chk3 bandera emision
	 * @return query a ejecutar
	 */
	public static Object setQryTecnico(boolean chk1, boolean chk2, boolean chk3) {
		StringBuilder sb = new StringBuilder();
		
		if(chk1) {
			//Query para Reporte Tecnico de Facturacion
			sb.append(GeneratorQuerys.repTecnicoFacturacion);
		}
		
		if(chk1 && chk2) {
			sb.append("  union all \n");
		}
		
		if(chk2) {
			//Query para Reporte Tecnico de Cancelacion
			sb.append(GeneratorQuerys.repTecnicoCancelacion);
		}
		
		if((chk1 || chk2) && chk3) {
			sb.append("  union all \n");
		}
		
		if(chk3) {
			//Query para Reporte Tecnico de Emision
			sb.append(GeneratorQuerys.repTecnicoEmision);
		}
		
		return sb;
	}
	
	/**
	 * Metodo que regresa el estatus para nombre de reporte
	 * @param chk1 bandera facturacion
	 * @param chk2 bandera cancelacion
	 * @param chk3 bandera emision
	 * @return estatus
	 */
	public static String setEstatus(boolean chk1, boolean chk2, boolean chk3) {
		String sts = Constantes.DEFAULT_STRING;
		
		if(chk1) {
			sts += "VIG";
		}
		
		if(chk2) {
			sts += "CAN";
		}
		
		if(chk3) {
			sts += "EMI";
		}
		
		return sts;
	}
}
